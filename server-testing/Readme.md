# Lineage2TS Project Server Testing

## Introduction
- framework to implement client interactions with game and login server, targeting Lineage2TS server implementations
- collection of client interactions that verify and test server functionality of Lineage2TS servers

## How to run
- see `source/interactions` for files you can run, these are strictly for login or game server functionality
- see `features` for cucumber files you can run, or if you want to run all scenarios do `npm run scenarios`
- for any clean-up operations do `npm run scenarios-cleanup`

Please note that scenario files need to be adjusted with your credentials.

## Contributions
Any and all code contributions are welcome. Please see [template](../.gitlab/merge_request_templates/server-testing.md) for code change acceptance actions.

## License
Any and all Lineage2TS Project files use `AGPL-3.0-or-later` license, see [LICENSE](LICENSE) file for legal description