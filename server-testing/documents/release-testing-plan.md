# Release testing plan
Document contains steps and itemized sections for testing specific functionality required
to ensure that new release meets expectations for stability and functionality of game server.

## Character Creation
- character is created using all available races and classes
- ensure starting inventory items are present
- ensure starting location npcs are visible, including any mobs

## Character can attack
- at starting location pick a mob
- attack mob and ensure that hp of mob goes down
- ensure mob is dead
- attack style is dependent on mage vs fighter
- optionally observe mob does damage (in case of fighter, since mage can still one shot mob at level 1)

# Character teleport via scroll
- spawn specific town scroll item (Giran or Gludin, etc.)
- observe scroll use and magic countdown
- on teleport validate general location as town