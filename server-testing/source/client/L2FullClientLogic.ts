import { L2ClientLogic } from './L2ClientLogic'
import { PlayerInventory } from './systems/PlayerInventory'
import { PetInventory } from './systems/PetInventory'
import { PacketListenerMethod } from './packets/PacketMethodTypes'
import { PlayerHenna } from './systems/PlayerHenna'
import { LeaveWorld } from './packets/game/receive/LeaveWorld'
import { PlayerInfo } from './systems/PlayerInfo'
import { ServerClose } from './packets/game/receive/ServerClose'
import { PetInfo } from './systems/PetInfo'
import { RequestKeyMapping } from './packets/game/send/RequestKeyMapping'
import { RequestEnterWorld } from './packets/game/send/RequestEnterWorld'
import { RequestManorList } from './packets/game/send/RequestManorList'
import { L2GameAccount } from './L2GameClient'
import { GameCharacterSelectedEvent } from './packets/game/receive/CharacterSelected'
import { TeleportToLocation, TeleportToLocationEvent } from './packets/game/receive/TeleportToLocation'
import { Appearing } from './packets/game/send/Appearing'
import { L2Player } from './models/L2Player'
import { L2World } from './L2World'
import { ValidatePosition } from './packets/game/send/ValidatePosition'
import { ChatMessages } from './systems/ChatMessages'
import { Logout } from './packets/game/send/Logout'
import { UserInfo } from './packets/game/receive/UserInfo'
import { CurrentPlayerTarget, CurrentPlayerTargetEvent } from './packets/game/receive/CurrentPlayerTarget'
import { TargetSelectedWithPosition } from './packets/game/receive/TargetSelectedWithPosition'
import { TargetUnselectedWithPosition } from './packets/game/receive/TargetUnselectedWithPosition'
import { PickItemUp } from './packets/game/receive/PickItemUp'
import { DropItem } from './packets/game/receive/DropItem'
import { NpcInfo, NpcInfoEvent } from './packets/game/receive/NpcInfo'
import { ServerObjectInfo, ServerObjectInfoEvent } from './packets/game/receive/ServerObjectInfo'
import { SpawnItem, SpawnItemEvent } from './packets/game/receive/SpawnItem'
import { DeleteObject, DeleteObjectEvent } from './packets/game/receive/DeleteObject'
import { StaticObject, StaticObjectEvent } from './packets/game/receive/StaticObject'
import { ValidateLocation, ValidateLocationEvent } from './packets/game/receive/ValidateLocation'
import { PetInfoEvent } from './packets/game/receive/PetInfo'
import { PlayerInfoEvent } from './packets/game/receive/PlayerInfo'
import { L2ObjectType } from './enums/L2ObjectType'
import { RequestRemoveBuff } from './packets/game/send/RequestRemoveBuff'
import { BypassCommand } from './packets/game/send/BypassCommand'
import { CharacterSelectionInfo } from './packets/game/receive/CharacterSelectionInfo'
import { CharacterCreationFailure } from './packets/game/receive/CharacterCreationFailure'
import { CharacterCreationSuccess } from './packets/game/receive/CharacterCreationSuccess'
import { CharacterDeleteFail } from './packets/game/receive/CharacterDeleteFail'
import { CharacterDeleteSuccess } from './packets/game/receive/CharacterDeleteSuccess'
import _ from 'lodash'

export abstract class L2FullClientLogic extends L2ClientLogic {
    inventory: PlayerInventory = null
    petInventory: PetInventory = null
    henna: PlayerHenna = new PlayerHenna()
    info: PlayerInfo = new PlayerInfo()
    petInfo: PetInfo = new PetInfo()
    chat: ChatMessages = new ChatMessages()
    worldEnteredListeners : Record<string, PacketListenerMethod> = {
        [ UserInfo.name ]: this.prepareWorldEntered.bind( this )
    }

    /*
        Certain handle methods are null since packets will update
        game objects data without any additional action from our side.
        We simply need to mention packet names for game client to be
        able to process them.
        Null value will also enable to store packet event in case of later processing.
     */
    allPackets : Record<string, PacketListenerMethod> = {
        [ LeaveWorld.name ]: this.onLeaveWorld.bind( this ),
        [ ServerClose.name ]: this.onServerClose.bind( this ),
        [ TeleportToLocation.name ]: this.onTeleport.bind( this ),
        [ CurrentPlayerTarget.name ]: this.onPlayerSetTarget.bind( this ),
        [ TargetSelectedWithPosition.name ]: null,
        [ TargetUnselectedWithPosition.name ]: null,
        [ PickItemUp.name ]: null,
        [ DropItem.name ]: null,
        [ NpcInfo.name ]: this.addSpatialIndex.bind( this ),
        [ ServerObjectInfo.name ]: this.addSpatialIndex.bind( this ),
        [ PetInfo.name ]: this.addSpatialIndex.bind( this ),
        [ PlayerInfo.name ]: this.addSpatialIndex.bind( this ),
        [ SpawnItem.name ]: this.onSpawnItem.bind( this ),
        [ DeleteObject.name ]: this.onDeleteObject.bind( this ),
        [ StaticObject.name ]: this.addSpatialIndex.bind( this ),
        [ ValidateLocation.name ]: this.addSpatialIndex.bind( this ),

        ...this.info.listeners,
        ...this.petInfo.listeners,
        ...this.chat.listeners,
        ...this.henna.listeners,
    }

    /*
        Once character is selected on server, it is being prepared to be played (special state).
        For client play, we must send additional packet to "Enter Game World",
        which causes start of game session with associated flood of information packets
        to describe nearby npcs, player stats, inventory, Seven Signs, etc.

        One must also be aware of timing when player can start actually interacting with
        game world. In L2 client it takes about four or more seconds to graphically load
        game interface and world. However since we do not have to deal with any additional
        loading, game packets can be issued as early as in half a second. Care must be taken
        to account for actual data being sent to you from server since that may take longer
        than half a second. Additionally, sending packets almost immediately after requesting
        to enter game world may (with server help) mark you as bot right away due to simple
        fact of slow loading of L2 client.
     */
    onReadyToEnterWorld( data: GameCharacterSelectedEvent ) {
        this.inventory = new PlayerInventory( data.objectId, this.gameClient )
        this.petInventory = new PetInventory( data.objectId, this.gameClient )

        this.gameClient.addPacketListeners( this.inventory.listeners )
        this.gameClient.addPacketListeners( this.petInventory.listeners )
        this.gameClient.addPacketListeners( this.getWorldEnteredListeners() )

        /*
            Current packet order is taken from observation of how L2 client
            sends packets to load game character world.
         */
        this.gameClient.sendPacket( RequestKeyMapping() )
        this.gameClient.sendPacket( RequestManorList() )
        this.gameClient.sendPacket( RequestEnterWorld() )
    }

    /*
        Entry method for custom client logic.
        Used to execute any desirable actions on behalf of client.
     */
    abstract onWorldEntered() : void

    /*
        Due to server implementation, UserInfo packet will arrive later than our client may expect,
        after entering game world state. However, such packet is critical to describe all various
        properties of player. Hence, while it is possible to start sending packets or check for data
        from server, player data will be in incomplete state.
     */
    private getWorldEnteredListeners() : Record<string, PacketListenerMethod> {
        return this.worldEnteredListeners
    }

    private prepareWorldEntered() : void {
        this.gameClient.removePacketListeners( this.worldEnteredListeners )

        let player = this.getPlayer()
        this.notifySuccess( 'Client status:', `Player ${player.name} entered world` )

        setImmediate( this.onWorldEntered.bind( this ) )
    }

    getGamePacketListeners(): Record<string, PacketListenerMethod> {
        return this.allPackets
    }

    setGamePacketListeners( packets: Record<string, PacketListenerMethod> ) : void {
        this.allPackets = _.assign( this.allPackets, packets )
    }

    onLeaveWorld() : void {
        this.terminateClient( true, 'Client session ended' )
    }

    onServerClose() : void {
        this.terminateClient( false, 'Server aborted client session' )
    }

    onTeleport( data: TeleportToLocationEvent ) : void {
        if ( data.objectId !== this.gameClient.playerObjectId ) {
            return
        }

        this.gameClient.sendPacket( Appearing() )
        this.gameClient.sendPacket( ValidatePosition( this.getPlayer() ) )
    }

    getPlayer() : L2Player {
        return L2World.getObject( this.gameClient.playerObjectId ) as L2Player
    }

    getPlayerId() : number {
        return this.gameClient.playerObjectId
    }

    getGameProperties() : L2GameAccount {
        return {
            characterName: this.getCharacterName()
        }
    }

    abstract getCharacterName() : string

    performLogOut() : void {
        return this.gameClient.sendPacket( Logout() )
    }

    onPlayerSetTarget( data: CurrentPlayerTargetEvent ) : void {
        let player = this.getPlayer()
        if ( !player ) {
            throw new Error( `Unable to set target of client's player=${this.getPlayerId()}` )
        }

        player.targetId = data.objectId
    }

    addSpatialIndex( data: NpcInfoEvent | ServerObjectInfoEvent | PetInfoEvent | PlayerInfoEvent | StaticObjectEvent | ValidateLocationEvent ) : void {
        let object = L2World.getObject( data.objectId )
        if ( !object ) {
            return
        }

        L2World.removeSpatialIndex( object )
        L2World.addSpatialIndex( object )
    }

    onSpawnItem( data: SpawnItemEvent ) : void {
        // TODO : add client data for items and possibly remove certain fields like price
        let item = L2World.getObject( data.objectId )
        if ( !item ) {
            return
        }

        L2World.addSpatialIndex( item )
    }

    /*
        Deleting object does not mean that client should remove all the data associated with it.
        Primary purpose of such packet is to remove an object from view. In server, it is used
        to indicate that object will no longer be tracked or receive updates in future. However,
        since such packet is used in sequence when item is being picked up, care must be taken
        to preserve existing item data.
     */
    onDeleteObject( data : DeleteObjectEvent ) : void {
        let object = L2World.getObject( data.objectId )

        if ( !object ) {
            return
        }

        L2World.removeSpatialIndex( object )

        if ( object.type === L2ObjectType.Item ) {
            return
        }

        L2World.deleteObject( data.objectId )
    }

    sendAdminCommand( command: string ) : void {
        this.notifySuccess( 'Requesting admin command:', command )
        this.gameClient.sendPacket( BypassCommand( command ) )
    }
}