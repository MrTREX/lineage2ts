import { L2BaseClient, L2BaseClientCallback, L2ConnectionParameters } from './L2BaseClient'
import { GameEncryption } from './security/GameEncryption'
import { PacketHandlingMethod, PacketListenerMethod, PacketMethodMap } from './packets/PacketMethodTypes'
import { GameExtendedPacketMethods, GamePacketMethods } from './packets/GamePacketMethods'
import { GameServerKey, GameServerKeyEvent } from './packets/game/receive/GameServerKey'
import { ProtocolVersion } from './packets/game/send/ProtocolVersion'
import { AuthenticatedLogin } from './packets/game/send/AuthenticatedLogin'
import { CharacterSelectionInfo, CharacterSelectionInfoEvent } from './packets/game/receive/CharacterSelectionInfo'
import { SelectCharacterSlot } from './packets/game/send/SelectCharacterSlot'
import { ServerClose } from './packets/game/receive/ServerClose'
import { CharacterSelected, GameCharacterSelectedEvent } from './packets/game/receive/CharacterSelected'
import _ from 'lodash'
import { AccessFailed, AccessFailedEvent, AccessFailReason } from './packets/game/receive/AccessFailed'
import logSymbols from 'log-symbols'
import chalk from 'chalk'
import { L2ClientPacketProcessor } from './types/PacketProcessor'

const enum GameClientParameters {
    version = 273,
}

export interface L2GameAuthentication {
    playOne: number
    playTwo: number
    loginOne: number
    loginTwo: number
    userName: string
}

export interface L2GameAccount {
    characterName: string
}

export interface L2GameParameters extends L2ConnectionParameters {
    account: L2GameAccount
    authentication: L2GameAuthentication
}

const logGameClientPackets : L2ClientPacketProcessor = ( name: string ) : void => {
    console.log( logSymbols.info, 'GameClient received packet', chalk.yellow( name ) )
}

export class L2GameClient extends L2BaseClient<L2GameParameters> {
    encryption: GameEncryption = new GameEncryption()
    packetMap: PacketMethodMap = GamePacketMethods
    playerObjectId: number = 0

    constructor( parameters: L2GameParameters, callback: L2BaseClientCallback, clientListeners: Record<string, PacketListenerMethod> ) {
        super( parameters, callback )

        let listeners = _.assign( this.getDefaultFlowListeners(), clientListeners )

        this.addPacketListeners( listeners )

        if ( process.env.GameClientLogPackets ) {
            this.addPacketProcessor( logGameClientPackets )
        }
    }

    onConnectionEnd(): void {
        this.encryption = null
        this.connection.destroy()
    }

    getDefaultFlowListeners() : Record<string, PacketListenerMethod> {
        return {
            [ GameServerKey.name ]: this.onGameServerKey.bind( this ),
            [ CharacterSelectionInfo.name ]: this.onShowCharacters.bind( this ),
            [ ServerClose.name ]: this.onServerClosed.bind( this ),
            [ CharacterSelected.name ]: this.onCharacterSelected.bind( this ),
            [ AccessFailed.name ]: this.onAccessFailed.bind( this )
        }
    }

    getPacketMethod( packetData: Buffer ) : PacketHandlingMethod {
        let signature : number = this.getPacketSignature( packetData )
        if ( signature === 0xfe ) {
            return GameExtendedPacketMethods[ packetData.readInt16LE( 1 ) ]
        }

        return this.packetMap[ signature ]
    }

    getPacketOffset( value: number ): number {
        return value === 0xfe ? 3 : 1
    }

    /*
        Step 1. When connection to game server is open.
        - send client version information (to verify if server supports client)
     */
    onConnectionStart() {
        super.onConnectionStart()
        this.sendPacket( ProtocolVersion( GameClientParameters.version ) )
    }

    /*
        Step 2. Receive encryption key
        - check if client is supported (key would not be provided by packet handling method)
        - otherwise send values obtained during login server flow (part of client parameters passed in)
        - once server validates our account, it will show available characters
     */
    onGameServerKey( data: GameServerKeyEvent ) : void {
        if ( !data.key ) {
            return this.callback( {
                message: `Unsupported game client version ${ GameClientParameters.version }`,
                id: '566d88e7-e9cd-4b73-8f2e-ceca953e1022'
            } )
        }

        if ( data.key.length !== 16 ) {
            return this.callback( {
                message: 'Game server provided invalid encryption key',
                id: '14c219e7-660a-4bd5-a8ac-0236fa94ce14'
            } )
        }

        this.encryption.setKey( data.key )
        this.sendPacket( AuthenticatedLogin( this.parameters.authentication ) )
    }

    /*
        Step 3. Receive available characters
        - check to find character identified by name

        Possible actions:
        - select available character index by name
        - delete character
        - create new character
        - restore character

        - send next action (selecting player character via slot)
        - server can respond with ServerClose packet to refuse selection
     */
    onShowCharacters( data : CharacterSelectionInfoEvent ) : void {
        let name = this.parameters.account.characterName
        let index = data.characters.findIndex( ( item ) => item.name === name )

        if ( name && index === -1 ) {
            return this.callback( {
                message: `Unable to find character with name = ${name}`,
                id: '83aaf6b3-04d5-4ec5-a712-853a17f614c1'
            } )
        }

        if ( data.characters.length === 0 ) {
            return this.callback( {
                message: 'No player characters available to select',
                id: 'ef05fe74-a5b7-4688-bad7-70f17ba99d02'
            } )
        }

        this.sendPacket( SelectCharacterSlot( index ) )
    }

    onServerClosed() : void {
        this.callback( {
            message: 'Game server closed connection',
            id: '51b2813b-b5d5-4170-855d-cf3954b634d2'
        } )
    }

    /*
        Step 4. Receive player information
        - player should be already created with provided data available
        - client can send EnterWorld packet to signal server player is logging in
        - client can send additional packets to request more data (normal client behavior)
     */
    onCharacterSelected( data: GameCharacterSelectedEvent ) : void {
        this.playerObjectId = data.objectId
        this.callback( null, data )
    }

    onAccessFailed( data: AccessFailedEvent ) : void {
        this.terminateConnection = true
        this.callback( {
            message: `Game server declined login: ${ AccessFailReason[ data.reason ]}`,
            id: 'd0a1cd92-6361-4895-b04e-c1da2b1b6426'
        } )
    }
}