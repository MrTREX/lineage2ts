import { PacketEvent } from '../packets/PacketMethodTypes'
import * as fs from 'node:fs'
import { NpcInfo, NpcInfoEvent } from '../packets/game/receive/NpcInfo'
import { L2Object } from './L2Object'
import { L2World } from '../L2World'
import { L2ClientPacketProcessor } from '../types/PacketProcessor'

export interface PacketRecord {
    name: string
    data: PacketEvent
    time: number
    worldObject: L2Object | null
}

export class PacketRecorder {
    private items: Array<PacketRecord> = []
    packetProcessor: L2ClientPacketProcessor

    constructor() {
        this.packetProcessor = this.onPacket.bind( this )
    }

    private onPacket( name: string, data: PacketEvent, rawPacket: Buffer ) : void {
        let item = {
            name,
            data,
            time: Date.now(),
            worldObject: null
        }

        switch ( name ) {
            case NpcInfo.name:
                item.worldObject = structuredClone( L2World.getObject( ( data as NpcInfoEvent ).objectId ) )
                break
        }

        this.items.push( item )
    }

    writeJsonFile( name: string ) : Promise<void> {
        let data = {
            packetOrder: this.items.map( item => item.name ),
            packets: this.items,
        }

        return fs.promises.writeFile( name, JSON.stringify( data ) )
    }

    clear() : void {
        this.items.length = 0
    }

    size() : number {
        return this.items.length
    }
}