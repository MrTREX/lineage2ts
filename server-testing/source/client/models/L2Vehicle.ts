import { L2Object } from './L2Object'
import { MovingProperties } from './MovingProperties'

export interface L2Vehicle extends L2Object, MovingProperties {
    moveSpeed: number
    rotationSpeed: number
    fuel: number
    maxFuel: number
}

export interface L2Airship extends L2Vehicle {
    captainId: number
    helmObjectId: number
}