import { ClassId } from '../enums/ClassId'

export interface PointProperties {
    hp: number
    maxHp: number
    mp: number
    maxMp: number
    level: number
}

export interface PlayerPointProperties extends PointProperties {
    maxCp: number
    cp: number
    classId: ClassId
}