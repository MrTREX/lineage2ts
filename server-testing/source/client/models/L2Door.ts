import { L2Character } from './L2Character'

export interface L2Door extends L2Character {
    isClosed: boolean
}