export interface LocationProperties {
    x: number
    y: number
    z: number
}