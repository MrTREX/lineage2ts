import { ReadableClientPacket } from '../packets/ReadableClientPacket'
import { PacketEvent } from '../packets/PacketMethodTypes'

export interface DuelStartEvent extends PacketEvent {
    isPartyDuel: boolean
}
export function DuelStart( packet : ReadableClientPacket ) : DuelStartEvent {
    return {
        isPartyDuel: packet.readD() === 1
    }
}