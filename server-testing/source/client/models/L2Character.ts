import { L2Object } from './L2Object'
import { WaitType } from '../enums/WaitType'
import { MovementType } from '../enums/MovementType'
import { MovingProperties } from './MovingProperties'
import { PointProperties } from './PointProperties'
import { StatusUpdateProperty } from '../enums/StatusUpdateProperty'

export interface L2Character extends L2Object, MovingProperties, PointProperties {
    stats: Record<number, number>
    isRunning: boolean
    waitType: WaitType
    templateId: number
    isAttackable: boolean
    isInCombat: boolean
    isDead: boolean
    name: string
    title: string
    clanId: number
    allyId: number
    movementType: MovementType
    isFlying: boolean
    isTargetable: boolean
    specialEffects: number
    displayEffects: number
    enchantEffects: number
    abnormalEffects: number
}