import { BBox } from 'rbush'
import { LocationProperties } from './LocationProperties'

export interface ISpatialIndexData extends ISpatialBoundingBox {
    objectId: number
}

export interface ISpatialBoundingBox extends BBox {
    minX: number
    minY: number
    maxX: number
    maxY: number
}

export const SpatialIndexHelper = {
    getBoundingBox( object: LocationProperties, radius: number ): ISpatialBoundingBox {
        return this.getBox( object.x, object.y, radius )
    },

    getBox( x: number, y: number, radius: number ): ISpatialBoundingBox {
        return {
            maxX: x + radius,
            maxY: y + radius,
            minX: x - radius,
            minY: y - radius,
        }
    },
}