import { L2Object } from './models/L2Object'
import RBush from 'rbush'
import { ISpatialIndexData, SpatialIndexHelper } from './models/SpatialIndex'
import { L2ObjectType } from './enums/L2ObjectType'

class WorldManager {
    objects: Record<number, L2Object> = {}
    allObjectsGrid: RBush<ISpatialIndexData> = new RBush<ISpatialIndexData>()

    getObject( id: number ) : L2Object {
        return this.objects[ id ]
    }

    setObject( object: L2Object ) : void {
        this.objects[ object.objectId ] = object
    }

    // TODO : implement moving system
    stopMoving( object: L2Object ) : void {

    }

    startMoving( object: L2Object ) : void {

    }

    deleteObject( id: number ) : void {
        delete this.objects[ id ]
    }

    addSpatialIndex( object: L2Object ): void {
        this.adjustSpatialData( object )
        this.allObjectsGrid.insert( object.spatialIndex )
    }

    removeSpatialIndex( object: L2Object ): void {
        this.allObjectsGrid.remove( object.spatialIndex )
    }

    /*
        We need to synchronize spatial data properties with x/y values
        since it is possible for these values to be different, especially
        since spatial data is managed only inside L2World, while client
        packets dealing with objects would only update relevant properties.
     */
    private adjustSpatialData( object: L2Object ) : void {
        object.spatialIndex.minX = object.x
        object.spatialIndex.maxX = object.x

        object.spatialIndex.maxY = object.y
        object.spatialIndex.minY = object.y
    }

    getObjectsByType( location: L2Object, radius: number, type: L2ObjectType ) : Array<L2Object> {
        let box = SpatialIndexHelper.getBoundingBox( location, radius )

        let objects: Array<L2Object> = []
        this.allObjectsGrid.search( box ).forEach( ( data: ISpatialIndexData ) => {
            let object = this.getObject( data.objectId )

            if ( object && object.type === type ) {
                objects.push( object )
            }
        } )

        return objects
    }
}

export const L2World = new WorldManager()