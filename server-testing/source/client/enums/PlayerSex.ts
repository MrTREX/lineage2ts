export const enum PlayerSex {
    Male,
    Female
}

export const AvailablePlayerSexes: Array<PlayerSex> = [
    PlayerSex.Male,
    PlayerSex.Female
]