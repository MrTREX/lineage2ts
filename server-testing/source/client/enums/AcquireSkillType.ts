export const enum AcquireSkillType {
    Class,
    Fishing,
    Pledge,
    Subpledge,
    Transform,
    Transfer,
    Subclass,
    Collect
}