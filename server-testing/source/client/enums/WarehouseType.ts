export const enum WarehouseType {
    Private = 1,
    Clan = 2,
    Castle = 3,
    Normal = 4
}