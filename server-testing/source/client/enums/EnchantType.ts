export const enum EnchantType {
    Normal = 0,
    Safe = 1,
    Untrain = 2,
    ChangeRoute = 3,
}