export const enum ItemState {
    Unknown,
    Dropped,
    PickedUp,
    Selling
}