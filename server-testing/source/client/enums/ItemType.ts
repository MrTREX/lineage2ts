export const enum ItemType {
    Weapon,
    ShieldArmor,
    Accessory,
    Quest,
    Money,
    Other
}