export const enum PlayerRelation {
    PartyMemberLow = 0x00001, // party member
    PartyMemberNormal = 0x00002, // party member
    PartyMemberMiddle = 0x00004, // party member
    PartyMemberTop = 0x00008, // party member
    PartyLeader = 0x00010, // true if is party leader
    InParty = 0x00020, // true if is in party
    InClan = 0x00040, // true if is in clan
    ClanLeader = 0x00080, // true if is clan leader
    ClanMember = 0x00100, // true if is in same clan
    InSiege = 0x00200, // true if in siege
    SiegeAttacker = 0x00400, // true when attacker
    Ally = 0x00800, // blue siege icon, cannot have if red
    Enemy = 0x01000, // true when red icon, doesn't matter with blue
    ClanWar = 0x04000, // double fist
    ClanAgression = 0x08000, // single fist
    InClanAlliance = 0x10000, // clan is in alliance
    TerritoryWar = 0x80000, // show Territory War icon
}