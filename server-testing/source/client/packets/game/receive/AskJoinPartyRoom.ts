import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface AskJoinPartyRoomEvent extends PacketEvent {
    roomName: string
}
export function AskJoinPartyRoom( packet: ReadableClientPacket ) : AskJoinPartyRoomEvent {
    return {
        roomName: packet.readS()
    }
}