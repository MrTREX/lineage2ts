import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { ItemState } from '../../../enums/ItemState'
import { getItemForUpdate } from '../helpers/ItemUpdateHelper'
import { L2World } from '../../../L2World'

export interface DropItemEvent extends PacketEvent {
    playerObjectId: number
    objectId: number
}

export function DropItem( packet: ReadableClientPacket ): DropItemEvent {
    let playerObjectId = packet.readD(),
        objectId = packet.readD()

    let existingItem = getItemForUpdate( objectId, ItemState.Dropped )

    existingItem.itemId = packet.readD()
    existingItem.x = packet.readD()
    existingItem.y = packet.readD()
    existingItem.z = packet.readD()

    existingItem.isStackable = packet.readD() === 0x01
    existingItem.amount = packet.readQ()
    existingItem.state = ItemState.Dropped

    return {
        playerObjectId,
        objectId,
    }
}