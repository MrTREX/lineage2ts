import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface HennaRemoveData {
    dyeId: number
    itemId: number
    cancelQuantity: number
    cancelFee: number
}
export interface HennaRemoveListEvent extends PacketEvent {
    availableAdena: number
    removableHennas: Array<HennaRemoveData>
}
export function HennaRemoveList( packet : ReadableClientPacket ) : HennaRemoveListEvent {
    let availableAdena = packet.readQ()

    packet.skipD( 1 )

    let size = packet.readD()

    return {
        availableAdena,
        removableHennas: _.times( size, () : HennaRemoveData => {
            let dyeId = packet.readD(),
                    itemId = packet.readD(),
                    cancelQuantity = packet.readD()

            packet.skipD( 1 )

            let cancelFee = packet.readD()

            packet.skipD( 2 )

            return {
                dyeId,
                itemId,
                cancelFee,
                cancelQuantity
            }
        } )
    }
}