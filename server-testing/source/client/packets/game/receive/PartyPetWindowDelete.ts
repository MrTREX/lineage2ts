import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { L2World } from '../../../L2World'
import { getSummonForUpdate } from '../helpers/CharacterUpdateHelper'

export interface PartyPetWindowDeleteEvent extends PacketEvent {
    summonObjectId: number
    ownerName: string
}
export function PartyPetWindowDelete( packet: ReadableClientPacket ) : PartyPetWindowDeleteEvent {
    let summonObjectId = packet.readD(),
            ownerName = packet.readS()

    let summon = getSummonForUpdate( summonObjectId )
    summon.ownerObjectId = packet.readD()

    return {
        summonObjectId,
        ownerName
    }
}