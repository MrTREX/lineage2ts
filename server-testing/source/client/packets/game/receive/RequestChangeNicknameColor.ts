import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface RequestChangeNicknameColorEvent extends PacketEvent {
    objectId: number
}
export function RequestChangeNicknameColor( packet: ReadableClientPacket ) : RequestChangeNicknameColorEvent {
    return {
        objectId: packet.readD()
    }
}