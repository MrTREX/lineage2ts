import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface AgathionEnergyData {
    itemObjectId: number
    templateId: number
    remainingEnergy: number
    maxEnergy: number
}

export interface AgathionEnergyInfoEvent extends PacketEvent {
    items: Array<AgathionEnergyData>
}

export function AgathionEnergyInfo( packet : ReadableClientPacket ) : AgathionEnergyInfoEvent {
    let size = packet.readD()

    return {
        items: _.times( size, () : AgathionEnergyData => {
            let itemObjectId = packet.readD(),
                    templateId = packet.readD()

            packet.skipD( 1 )

            let remainingEnergy = packet.readD(),
                    maxEnergy = packet.readD()

            return {
                itemObjectId,
                templateId,
                remainingEnergy,
                maxEnergy
            }
        } )
    }
}