import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { CharacterStatOrder, CharacterStats } from '../../../models/CharacterStats'

export interface HennaRemoveInfoEvent extends PacketEvent {
    currentStats: CharacterStats
    afterStats: CharacterStats
    dyeId: number
    itemId: number
    cancelQuantity: number
    cancelFee: number
    isPermitted: boolean
    availableAdena: number
}
export function HennaRemoveInfo( packet: ReadableClientPacket ) : HennaRemoveInfoEvent {
    let dyeId = packet.readD(),
            itemId = packet.readD(),
            cancelQuantity = packet.readQ(),
            cancelFee = packet.readQ(),
            isPermitted = packet.readD() === 1,
            availableAdena = packet.readQ()

    let currentStats = {} as CharacterStats
    let afterStats = {} as CharacterStats

    CharacterStatOrder.forEach( ( property: string ) => {
        currentStats[ property ] = packet.readD()
        afterStats[ property ] = packet.readC()
    } )

    return {
        dyeId,
        itemId,
        cancelQuantity,
        cancelFee,
        isPermitted,
        availableAdena,
        currentStats,
        afterStats
    }
}