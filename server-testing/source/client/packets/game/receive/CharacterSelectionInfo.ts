import { ReadableClientPacket } from '../../ReadableClientPacket'
import { CharacterSex } from '../../../enums/CharacterSex'
import { CharacterRace } from '../../../enums/CharacterRace'
import { ClassId } from '../../../enums/ClassId'
import { PaperdollOrder } from '../../../enums/Paperdoll'
import { PacketEvent } from '../../PacketMethodTypes'

export interface CharacterDetails extends PacketEvent {
    name: string
    objectId: number
    sessionKey: number
    clanId: number
    sex: CharacterSex
    race: CharacterRace
    baseClassId: ClassId
    x: number
    y: number
    z: number
    hp: number
    mp: number
    sp: number
    exp: number
    expPercentage: number
    level: number
    karma: number
    pkKills: number
    pvpKills: number
    paperdoll: Array<number>
    hairStyle: number
    hairColor: number
    face: number
    maxHp: number
    maxMp: number
    secondsToDelete: number
    classId: ClassId
    isLastUsed: boolean
    enchantLevel: number
    augmentationId: number
    vitalityPoints: number
}

export interface CharacterSelectionInfoEvent extends PacketEvent {
    characterLimit: number
    characters: Array<CharacterDetails>
}

export function CharacterSelectionInfo( packet: ReadableClientPacket ) : CharacterSelectionInfoEvent {
    let amount = packet.readD(),
            characterLimit = packet.readD()

    packet.skipB( 1 )

    return {
        characterLimit,
        characters: createCharacterDetails( packet, amount )
    }
}

function createCharacterDetails( packet: ReadableClientPacket, amount: number ) : Array<CharacterDetails> {
    let details : Array<CharacterDetails> = []

    while ( amount > 0 ) {
        details.push( createCharacter( packet ) )
        amount--
    }

    return details
}

function createCharacter( packet: ReadableClientPacket ) : CharacterDetails {
    let name = packet.readS(),
            objectId = packet.readD()

    packet.skipS()

    let sessionKey = packet.readD(),
            classId = packet.readD()

    packet.skipD( 1 )

    let sex = packet.readD(),
            race = packet.readD(),
            baseClassId = packet.readD()

    packet.skipD( 1 )

    let x = packet.readD(),
            y = packet.readD(),
            z = packet.readD(),
            hp = packet.readF(),
            mp = packet.readF(),
            sp = packet.readD(),
            exp = packet.readQ(),
            expPercentage = packet.readF(),
            level = packet.readD(),
            karma = packet.readD(),
            pkKills = packet.readD(),
            pvpKills = packet.readD()

    packet.skipD( 7 )

    let paperdoll = PaperdollOrder.map( () => packet.readD() ),
            hairStyle = packet.readD(),
            hairColor = packet.readD(),
            face = packet.readD(),
            maxHp = packet.readF(),
            maxMp = packet.readF(),
            secondsToDelete = packet.readD(),
            clanId = packet.readD(),
            isLastUsed = packet.readD() === 0x01,
            enchantLevel = packet.readC(),
            augmentationId = packet.readD()

    packet.skipD( 9 )

    let vitalityPoints = packet.readD()

    return {
        augmentationId,
        baseClassId,
        clanId,
        classId,
        enchantLevel,
        exp,
        expPercentage,
        face,
        hairColor,
        hairStyle,
        hp,
        isLastUsed,
        karma,
        level,
        maxHp,
        maxMp,
        mp,
        name,
        objectId,
        paperdoll,
        pkKills,
        pvpKills,
        race,
        secondsToDelete,
        sessionKey,
        sex,
        sp,
        vitalityPoints,
        x,
        y,
        z
    }
}