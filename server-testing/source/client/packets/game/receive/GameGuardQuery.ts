import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface GameGuardQueryEvent extends PacketEvent {

}

export function GameGuardQuery( packet: ReadableClientPacket ) : GameGuardQueryEvent {
    return {}
}