import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface PutIntensiveResultForVariationMakeEvent extends PacketEvent {
    itemObjectId: number
    itemId: number
    gemStoneItemId: number
    gemStoneAmount: number
}
export function PutIntensiveResultForVariationMake( packet: ReadableClientPacket ) : PutIntensiveResultForVariationMakeEvent {
    let itemObjectId = packet.readD(),
            itemId = packet.readD(),
            gemStoneItemId = packet.readD(),
            gemStoneAmount = packet.readD()

    return {
        itemObjectId,
        itemId,
        gemStoneItemId,
        gemStoneAmount
    }
}