import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { PrivateStoreManageAvailable, PrivateStoreManageItem } from '../../../models/PrivateStore'
import _ from 'lodash'
import { readNormalItem } from '../helpers/ItemUpdateHelper'

export interface PrivateStoreManageListSellEvent extends PacketEvent {
    playerObjectId: number
    playerAdena: number
    availableItems: Array<PrivateStoreManageAvailable>
    sellItems: Array<PrivateStoreManageItem>
    isPackageSale: boolean
}

export function PrivateStoreManageListSell( packet: ReadableClientPacket ) : PrivateStoreManageListSellEvent {
    let playerObjectId = packet.readD(),
            isPackageSale = packet.readD() === 1,
            playerAdena = packet.readQ(),
            size = packet.readD()

    let availableItems : Array<PrivateStoreManageAvailable> = _.times( size, () : PrivateStoreManageAvailable => {
        let item = readNormalItem( packet ),
                referencePrice = packet.readQ()

        return {
            itemObjectId: item.objectId,
            referencePrice
        }
    } )

    size = packet.readD()

    let sellItems : Array<PrivateStoreManageItem> = _.times( size, () : PrivateStoreManageItem => {
        let item = readNormalItem( packet ),
                price = packet.readQ(),
                referencePrice = packet.readQ()

        return {
            itemObjectId: item.objectId,
            price,
            referencePrice
        }
    } )

    return {
        playerAdena,
        playerObjectId,
        isPackageSale,
        availableItems,
        sellItems
    }
}