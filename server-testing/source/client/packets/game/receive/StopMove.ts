import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { L2World } from '../../../L2World'
import { L2Character } from '../../../models/L2Character'

export interface StopMoveEvent extends PacketEvent {
    objectId: number
}
export function StopMove( packet : ReadableClientPacket ) : StopMoveEvent {
    let objectId = packet.readD()

    let existingCharacter = L2World.getObject( objectId ) as L2Character
    if ( existingCharacter ) {
        L2World.stopMoving( existingCharacter )

        existingCharacter.x = packet.readD()
        existingCharacter.y = packet.readD()
        existingCharacter.z = packet.readD()
        existingCharacter.heading = packet.readD()
    }

    return {
        objectId
    }
}