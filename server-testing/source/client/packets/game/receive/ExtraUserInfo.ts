import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { CharacterEventEffect } from '../../../enums/CharacterVisualEffect'

export interface ExtraUserInfoEvent extends PacketEvent {
    playerObjectId: number
    effect: CharacterEventEffect
}

export function ExtraUserInfo( packet: ReadableClientPacket ): ExtraUserInfoEvent {
    let playerObjectId = packet.readD(),
            effect = packet.readD()

    return {
        playerObjectId,
        effect
    }
}