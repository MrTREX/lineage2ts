import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export const enum VoteSystemBonusType {
    None,
    Maintain
}
export interface VoteSystemInfoEvent extends PacketEvent {
    recommendationsLeft: number
    recommendationsHave: number
    bonusTime: number
    bonusValue: number
    bonusType: VoteSystemBonusType
}
export function VoteSystemInfo( packet : ReadableClientPacket ) : VoteSystemInfoEvent {
    let recommendationsLeft = packet.readD(),
            recommendationsHave = packet.readD(),
            bonusTime = packet.readD(),
            bonusValue = packet.readD(),
            bonusType = packet.readD()

    return {
        recommendationsLeft,
        recommendationsHave,
        bonusTime,
        bonusValue,
        bonusType
    }
}