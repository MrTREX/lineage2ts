import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export const enum SystemMessageParameter {
    SystemString = 13,
    PlayerName = 12,
    DoorName = 11,
    InstanceName = 10,
    ElementName = 9,
    ZoneName = 7,
    BigInteger = 6,
    CastleName = 5,
    SkillName = 4,
    ItemName = 3,
    NpcName = 2,
    Number = 1,
    Text = 0,
}

export interface SystemMessageData extends PacketEvent {
    type: SystemMessageParameter
    data: Array<number | string>
}

export interface SystemMessageEvent extends PacketEvent {
    messageId: number
    parameters: Array<SystemMessageData>
    receivedTime: number
}

export function SystemMessage( packet : ReadableClientPacket ) : SystemMessageEvent {
    let messageId = packet.readD(),
            size = packet.readD()

    return {
        messageId,
        parameters: readSystemMessageParameters( packet, size ),
        receivedTime: Date.now()
    }
}

export function readSystemMessageParameters( packet: ReadableClientPacket, size: number ) : Array<SystemMessageData> {
    return _.times( size, () : SystemMessageData => {
        let type = packet.readD()
        let data : Array<number | string> = []

        switch ( type ) {
            case SystemMessageParameter.Text:
            case SystemMessageParameter.PlayerName:
                data.push( packet.readS() )
                break

            case SystemMessageParameter.BigInteger:
                data.push( packet.readQ() )
                break

            case SystemMessageParameter.ItemName:
            case SystemMessageParameter.CastleName:
            case SystemMessageParameter.Number:
            case SystemMessageParameter.NpcName:
            case SystemMessageParameter.ElementName:
            case SystemMessageParameter.SystemString:
            case SystemMessageParameter.InstanceName:
            case SystemMessageParameter.DoorName:
                data.push( packet.readD() )
                break

            case SystemMessageParameter.SkillName:
                data.push( packet.readD() )
                data.push( packet.readD() )
                break

            case SystemMessageParameter.ZoneName:
                data.push( packet.readD() )
                data.push( packet.readD() )
                data.push( packet.readD() )
                break
        }

        return {
            type,
            data
        }
    } )
}