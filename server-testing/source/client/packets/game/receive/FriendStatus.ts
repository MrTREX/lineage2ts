import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export const enum FriendState {
    Offline,
    Online
}

export interface FriendStatusEvent extends PacketEvent {
    objectId: number
    name: string
    state: FriendState
}

export function FriendStatus( packet: ReadableClientPacket ) : FriendStatusEvent {
    let state = packet.readD(),
            name = packet.readS(),
            objectId = packet.readD()

    return {
        objectId,
        name,
        state
    }
}