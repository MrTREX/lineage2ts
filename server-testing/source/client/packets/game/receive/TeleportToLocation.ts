import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { L2World } from '../../../L2World'
import { L2Character } from '../../../models/L2Character'

export interface TeleportToLocationEvent extends PacketEvent {
    objectId: number
}
export function TeleportToLocation( packet : ReadableClientPacket ) : TeleportToLocationEvent {
    let objectId = packet.readD()

    let existingCharacter = L2World.getObject( objectId ) as L2Character

    if ( existingCharacter ) {
        existingCharacter.x = packet.readD()
        existingCharacter.y = packet.readD()
        existingCharacter.z = packet.readD()

        packet.skipD( 1 )
        existingCharacter.heading = packet.readD()
    }

    return {
        objectId
    }
}