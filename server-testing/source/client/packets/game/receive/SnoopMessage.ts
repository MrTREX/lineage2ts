import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { SayTextType } from './SayText'

export interface SnoopMessageEvent extends PacketEvent {
    playerObjectId: number
    type: SayTextType
    originatorName: string
    message: string
}
export function SnoopMessage( packet : ReadableClientPacket ) : SnoopMessageEvent {
    let playerObjectId = packet.readD()

    packet.skipS()

    let type = packet.readD(),
            originatorName = packet.readS(),
            message = packet.readS()

    return {
        playerObjectId,
        type,
        originatorName,
        message
    }
}