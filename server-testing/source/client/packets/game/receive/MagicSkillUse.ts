import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { L2World } from '../../../L2World'

export interface MagicSkillUseEvent extends PacketEvent {
    casterObjectId: number
    targetObjectId: number
    skillId: number
    skillLevel: number
    reuseDelay: number
    hitTime: number
}
export function MagicSkillUse( packet : ReadableClientPacket ) : MagicSkillUseEvent {
    let casterObjectId = packet.readD(),
            targetObjectId = packet.readD(),
            skillId = packet.readD(),
            skillLevel = packet.readD(),
            hitTime = packet.readD(),
            reuseDelay = packet.readD()

    let existingCharacter = L2World.getObject( casterObjectId )
    if ( existingCharacter ) {
        existingCharacter.x = packet.readD()
        existingCharacter.y = packet.readD()
        existingCharacter.z = packet.readD()
    }

    return {
        casterObjectId,
        targetObjectId,
        skillId,
        skillLevel,
        hitTime,
        reuseDelay
    }
}