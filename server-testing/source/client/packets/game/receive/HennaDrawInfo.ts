import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { CharacterStatOrder, CharacterStats } from '../../../models/CharacterStats'

export interface HennaDrawInfoEvent extends PacketEvent {
    currentStats: CharacterStats
    equipStats: CharacterStats
    dyeId: number
    itemId: number
    dyeAmount: number
    adenaRequired: number
    isPermitted: boolean
    availableAdena: number
}
export function HennaDrawInfo( packet: ReadableClientPacket ) : HennaDrawInfoEvent {
    let dyeId = packet.readD(),
            itemId = packet.readD(),
            dyeAmount = packet.readQ(),
            adenaRequired = packet.readQ(),
            isPermitted = packet.readD() === 1,
            availableAdena = packet.readQ()

    let currentStats = {} as CharacterStats
    let equipStats = {} as CharacterStats

    CharacterStatOrder.forEach( ( property: string ) => {
        currentStats[ property ] = packet.readD()
        equipStats[ property ] = packet.readC()
    } )

    return {
        dyeId,
        itemId,
        dyeAmount,
        adenaRequired,
        isPermitted,
        availableAdena,
        currentStats,
        equipStats
    }
}