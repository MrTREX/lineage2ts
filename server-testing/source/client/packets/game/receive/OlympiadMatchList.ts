import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { OlympiadType } from '../../../enums/OlympiadType'
import _ from 'lodash'

export const enum OlympiadMatchListType {
    List,
    Result
}

export const enum OlympiadMatchGameStatus {
    Paused = 1,
    Playing = 2
}

export interface OlympiadMatchGame {
    arenaId: number
    gameType: OlympiadType
    status: OlympiadMatchGameStatus
    playerNames: [ string, string ]
}

export interface OlympiadMatchListEvent extends PacketEvent {
    type: OlympiadMatchListType
    games: Array<OlympiadMatchGame>
}

export function OlympiadMatchList( packet : ReadableClientPacket ) : OlympiadMatchListEvent {
    let type = packet.readD(),
            size = packet.readD()


    return {
        type,
        games: _.times( size, () : OlympiadMatchGame => {
            let arenaId = packet.readD(),
                    gameType = packet.readD(),
                    status = packet.readD()

            return {
                arenaId,
                gameType,
                status,
                playerNames: [ packet.readS(), packet.readS() ]
            }
        } )
    }
}