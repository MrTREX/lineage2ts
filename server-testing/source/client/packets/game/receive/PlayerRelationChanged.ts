import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { PlayerRelation } from '../../../enums/PlayerRelation'
import { getCharacterForUpdate } from '../helpers/CharacterUpdateHelper'
import { L2ObjectType } from '../../../enums/L2ObjectType'
import { L2Player } from '../../../models/L2Player'
import _ from 'lodash'

export interface PlayerRelationData extends PacketEvent {
    relation: PlayerRelation
    objectId: number
}

export interface PlayerRelationChangedEvent extends PacketEvent {
    relations: Array<PlayerRelationData>
}

export function PlayerRelationChanged( packet: ReadableClientPacket ): PlayerRelationChangedEvent {
    let size = packet.readD()
    return {
        relations: _.times( size, () : PlayerRelationData => {
            let objectId = packet.readD(),
                    relation = packet.readD()

            /*
                Players and their pets/summons are possible here, hence
                there is no easy way to distinguish.
             */
            let playerControlled = getCharacterForUpdate( objectId ) as L2Player
            playerControlled.isAttackable = packet.readD() === 1

            if ( playerControlled.type === L2ObjectType.Player ) {
                playerControlled.karma = packet.readD()
                playerControlled.pvpFlag = packet.readD()
            }

            return {
                objectId,
                relation,
            }
        } )
    }
}