import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export const enum MacroCommandType {
    None,
    Skill,
    Action,
    Text,
    Shortcut,
    Item,
    Delay
}

export interface MacroCommand {
    type: MacroCommandType
    id: number
    shortcut: number
    command: string
}

export interface MacroData {
    id: number
    icon: number
    name: string
    description: string
    acronym: string
    commands: Array<MacroCommand>
}
export interface MacroListEvent extends PacketEvent {
    revision: number
    macro: MacroData
}

export function MacroList( packet: ReadableClientPacket ) : MacroListEvent {
    let revision = packet.readD()

    packet.skipB( 2 )

    let hasData = packet.readC() === 1

    if ( !hasData ) {
        return {
            revision,
            macro: null
        }
    }

    let id = packet.readD(),
            name = packet.readS(),
            description = packet.readS(),
            acronym = packet.readS(),
            icon = packet.readC(),
            size = packet.readC()

    return {
        revision,
        macro: {
            id,
            name,
            description,
            acronym,
            icon,
            commands: _.times( size, () : MacroCommand => {
                packet.skipB( 1 )

                let type = packet.readC(),
                        id = packet.readD(),
                        shortcut = packet.readD(),
                        command = packet.readS()

                return {
                    type,
                    id,
                    shortcut,
                    command
                }
            } )
        }
    }
}