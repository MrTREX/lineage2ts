import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface ShowChristmasSealEvent extends PacketEvent {
    itemId: number
}

export function ShowChristmasSeal( packet: ReadableClientPacket ) : ShowChristmasSealEvent {
    return {
        itemId: packet.readD()
    }
}