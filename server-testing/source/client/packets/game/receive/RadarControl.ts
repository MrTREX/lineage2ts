import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { LocationProperties } from '../../../models/LocationProperties'
import { readLocation } from '../helpers/PropertyHelper'

export const enum RadarControlOperation {
    Add,
    Remove,
    RemoveAll
}

export const enum RadarControlType {
    Normal = 1,
    Npc = 2
}

export interface RadarControlEvent extends PacketEvent {
    operation: RadarControlOperation
    type: RadarControlType
    location: LocationProperties
}

export function RadarControl( packet: ReadableClientPacket ) : RadarControlEvent {
    let operation = packet.readD(),
            type = packet.readD()

    return {
        operation,
        type,
        location: readLocation( packet )
    }
}