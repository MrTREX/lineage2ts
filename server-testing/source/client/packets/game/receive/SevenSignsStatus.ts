import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { SevenSignsPeriod } from '../../../enums/SevenSignsPeriod'
import _ from 'lodash'

export const enum SevenSignsStatusPage {
    One = 1,
    Two = 2,
    Three = 3,
    Four = 4
}

export const enum SevenSignsCabalType {
    None = 0,
    Dusk = 1,
    Dawn = 2,
}

export interface SevenSignsStatusOne {
    cycle: number // TODO : add enum
    messageId: number
    endingMessageId: number
    cabalType: SevenSignsCabalType
    sealType: number // TODO : add enum
    submittedStoneAmount: number
    adenaToCollect: number
    duskStoneScore: number
    duskFestivalScore: number
    duskTotalScore: number
    duskTotalPercent: number
    dawnStoneScore: number
    dawnFestivalScore: number
    dawnTotalScore: number
    dawnTotalPercent: number
}

export interface SevenSignsStatusTwoData {
    id: number
    levelScore: number
    duskScore: number
    dawnScore: number
    duskPlayerNames: Array<string>
    dawnPlayerNames: Array<string>
}

export interface SevenSignsStatusTwo {
    festivals: Array<SevenSignsStatusTwoData>
}

export interface SevenSignsStatusThreeData {
    id: number
    sealOwner: number // TODO : add enum
    duskPercentage: number
    dawnPercentage: number
}

export interface SevenSignsStatusThree {
    retainLimit: number
    claimLimit: number
    sealData: Array<SevenSignsStatusThreeData>
}

export interface SevenSignsStatusFourData {
    id: number
    type: SevenSignsCabalType
    messageId: number
}

export interface SevenSignsStatusFour {
    winningCabal: SevenSignsCabalType
    seals: Array<SevenSignsStatusFourData>
}

export type SevenSignsStatusData = SevenSignsStatusOne | SevenSignsStatusTwo | SevenSignsStatusThree | SevenSignsStatusFour
export interface SevenSignStatusEvent extends PacketEvent {
    pageType: SevenSignsStatusPage
    period: SevenSignsPeriod
    data: SevenSignsStatusOne | SevenSignsStatusTwo | SevenSignsStatusThree | SevenSignsStatusFour
}

function readPageOne( packet: ReadableClientPacket ) : SevenSignsStatusOne {
    let cycle = packet.readD(),
            messageId = packet.readD(),
            endingMessageId = packet.readD(),
            cabalType = packet.readC(),
            sealType = packet.readC(),
            submittedStoneAmount = packet.readQ(),
            adenaToCollect = packet.readQ(),
            duskStoneScore = packet.readQ(),
            duskFestivalScore = packet.readQ(),
            duskTotalScore = packet.readQ(),
            duskTotalPercent = packet.readC(),
            dawnStoneScore = packet.readQ(),
            dawnFestivalScore = packet.readQ(),
            dawnTotalScore = packet.readQ(),
            dawnTotalPercent = packet.readC()

    return {
        cycle,
        messageId,
        endingMessageId,
        cabalType,
        sealType,
        submittedStoneAmount,
        adenaToCollect,
        duskStoneScore,
        duskFestivalScore,
        duskTotalScore,
        duskTotalPercent,
        dawnStoneScore,
        dawnFestivalScore,
        dawnTotalScore,
        dawnTotalPercent
    }
}

function readPageTwo( packet: ReadableClientPacket ) : SevenSignsStatusTwo {
    packet.skipH( 1 )

    let size = packet.readC()

    return {
        festivals: _.times( size, () : SevenSignsStatusTwoData => {
            let id = packet.readC(),
                    levelScore = packet.readD(),
                    duskScore = packet.readQ(),
                    duskPlayerSize = packet.readC(),
                    duskPlayerNames = _.times( duskPlayerSize, () : string => packet.readS() ),
                    dawnScore = packet.readQ(),
                    dawnPlayerSize = packet.readC(),
                    dawnPlayerNames = _.times( dawnPlayerSize, () : string => packet.readS() )

            return {
                id,
                levelScore,
                duskScore,
                duskPlayerNames,
                dawnScore,
                dawnPlayerNames
            }
        } )
    }
}

function readPageThree( packet: ReadableClientPacket ) : SevenSignsStatusThree {
    let retainLimit = packet.readC(),
            claimLimit = packet.readC(),
            size = packet.readC()

    return {
        retainLimit,
        claimLimit,
        sealData: _.times( size, () : SevenSignsStatusThreeData => {
            let id = packet.readC(),
                    sealOwner = packet.readC(),
                    duskPercentage = packet.readC(),
                    dawnPercentage = packet.readC()

            return {
                id,
                sealOwner,
                duskPercentage,
                dawnPercentage
            }
        } )
    }
}

function readPageFour( packet: ReadableClientPacket ) : SevenSignsStatusFour {
    let winningCabal = packet.readC(),
            size = packet.readC()

    return {
        winningCabal,
        seals: _.times( size, () : SevenSignsStatusFourData => {
            let id = packet.readC(),
                    type = packet.readC(),
                    messageId = packet.readD()

            return {
                id,
                type,
                messageId
            }
        } )
    }
}

function getData( type: SevenSignsStatusPage, packet: ReadableClientPacket ) : SevenSignsStatusData {
    switch ( type ) {
        case SevenSignsStatusPage.One:
            return readPageOne( packet )

        case SevenSignsStatusPage.Two:
            return readPageTwo( packet )

        case SevenSignsStatusPage.Three:
            return readPageThree( packet )

        case SevenSignsStatusPage.Four:
            return readPageFour( packet )
    }
}

export function SevenSignsStatus( packet: ReadableClientPacket ) : SevenSignStatusEvent {
    let pageType = packet.readC(),
            period = packet.readC()

    return {
        pageType,
        period,
        data: getData( pageType, packet )
    }
}