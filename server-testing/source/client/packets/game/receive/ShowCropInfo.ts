import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface ShowCropData {
    id: number
    amount: number
    startAmount: number
    price: number
    rewardId: number
    seedLevel: number
    seedRewardOne: number
    seedRewardTwo: number
}
export interface ShowCropInfoEvent extends PacketEvent {
    showSalesButton: boolean
    manorId: number
    crops: Array<ShowCropData>
}

export function ShowCropInfo( packet: ReadableClientPacket ) : ShowCropInfoEvent {
    let showSalesButton = packet.readC() === 0,
            manorId = packet.readD()

    packet.skipD( 1 )

    let size = packet.readD()

    return {
        showSalesButton,
        manorId,
        crops: _.times( size, () : ShowCropData => {
            let id = packet.readD(),
                    amount = packet.readQ(),
                    startAmount = packet.readQ(),
                    price = packet.readQ(),
                    rewardId = packet.readC(),
                    seedLevel = packet.readD()

            packet.skipB( 1 )

            let seedRewardOne = packet.readD()

            packet.skipB( 1 )

            let seedRewardTwo = packet.readD()

            return {
                id,
                amount,
                startAmount,
                price,
                rewardId,
                seedLevel,
                seedRewardOne,
                seedRewardTwo
            }
        } )
    }
}