import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface ShowSeedItem {
    id: number
    amountToBuy: number
    startAmount: number
    sellPrice: number
    level: number
    rewardOneId: number
    rewardTwoId: number
}

export interface ShowSeedInfoEvent extends PacketEvent {
    showUIButtons: boolean
    manorId: number
    seeds: Array<ShowSeedItem>
}

export function ShowSeedInfo( packet: ReadableClientPacket ) : ShowSeedInfoEvent {
    let showUIButtons = packet.readD() === 0,
            manorId = packet.readD()

    packet.skipD( 1 )

    let size = packet.readD()

    return {
        showUIButtons,
        manorId,
        seeds: _.times( size, () : ShowSeedItem => {
            let id = packet.readD(),
                    amountToBuy = packet.readQ(),
                    startAmount = packet.readQ(),
                    sellPrice = packet.readQ(),
                    level = packet.readD()

            packet.skipB( 1 )

            let rewardOneId = packet.readD()

            packet.skipB( 1 )

            let rewardTwoId = packet.readD()

            packet.skipB( 1 )

            return {
                id,
                amountToBuy,
                startAmount,
                sellPrice,
                level,
                rewardOneId,
                rewardTwoId
            }
        } )
    }
}