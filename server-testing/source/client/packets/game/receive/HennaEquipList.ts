import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface HennaEquipData {
    dyeId: number
    itemId: number
    quantity: number
    fee: number
    isPermitted: boolean
}
export interface HennaEquipListEvent extends PacketEvent {
    availableAdena: number
    equipItems: Array<HennaEquipData>
}
export function HennaEquipList( packet : ReadableClientPacket ) : HennaEquipListEvent {
    let availableAdena = packet.readQ()

    packet.skipD( 1 )

    let size = packet.readD()

    return {
        availableAdena,
        equipItems: _.times( size, () : HennaEquipData => {
            let dyeId = packet.readD(),
                    itemId = packet.readD(),
                    quantity = packet.readQ(),
                    fee = packet.readQ(),
                    isPermitted = packet.readD() === 1

            return {
                dyeId,
                fee,
                isPermitted,
                itemId,
                quantity,
            }
        } )
    }
}