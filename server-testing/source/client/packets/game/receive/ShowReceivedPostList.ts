import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { PostMessageType } from '../../../enums/PostMessageType'
import _ from 'lodash'

export interface ShowReceivedPostItem {
    id: number
    title: string
    senderName: string
    isLocked: boolean
    expirationInSeconds: number
    isRead: boolean
    hasAttachments: boolean
    isReturned: boolean
    messageType: PostMessageType
}
export interface ShowReceivedPostListEvent extends PacketEvent {
    messages: Array<ShowReceivedPostItem>
}

export function ShowReceivedPostList( packet: ReadableClientPacket ) : ShowReceivedPostListEvent {
    packet.skipD( 1 )

    let size = packet.readD()

    return {
        messages: _.times( size, () : ShowReceivedPostItem => {
            let id = packet.readD(),
                    title = packet.readS(),
                    senderName = packet.readS(),
                    isLocked = packet.readD() === 1,
                    expirationInSeconds = packet.readD(),
                    isRead = packet.readD() === 0

            packet.skipD( 1 )

            let hasAttachments = packet.readD() === 1,
                    isReturned = packet.readD() === 1,
                    messageType = packet.readD()

            packet.skipD( 1 )

            return {
                id,
                title,
                senderName,
                isLocked,
                expirationInSeconds,
                isRead,
                isReturned,
                messageType,
                hasAttachments
            }
        } )
    }
}