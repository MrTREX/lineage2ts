import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { L2World } from '../../../L2World'
import { L2Character } from '../../../models/L2Character'

export interface MoveToLocationEvent extends PacketEvent {
    objectId: number
}
export function MoveToLocation( packet : ReadableClientPacket ) : MoveToLocationEvent {
    let objectId = packet.readD()

    let existingCharacter = L2World.getObject( objectId ) as L2Character
    if ( existingCharacter ) {
        existingCharacter.destinationX = packet.readD()
        existingCharacter.destinationY = packet.readD()
        existingCharacter.destinationZ = packet.readD()

        existingCharacter.x = packet.readD()
        existingCharacter.y = packet.readD()
        existingCharacter.z = packet.readD()
    }

    return {
        objectId
    }
}