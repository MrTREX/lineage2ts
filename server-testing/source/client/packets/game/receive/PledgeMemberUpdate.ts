import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { Race } from '../../../enums/Race'

export interface PledgeMemberUpdateEvent extends PacketEvent {
    name: string
    level: number
    classId: number
    race: Race
    pledgeType: number // TODO : add enum
    playerObjectId: number
    isOnline: boolean
    hasSponsor: boolean
}

export function PledgeMemberUpdate( packet: ReadableClientPacket ) : PledgeMemberUpdateEvent {
    let name = packet.readS(),
            level = packet.readD(),
            classId = packet.readD()

    packet.skipD( 1 )

    let race = packet.readD(),
            playerObjectId = packet.readD(),
            pledgeType = packet.readD(),
            hasSponsor = packet.readD() === 1

    return {
        name,
        level,
        classId,
        race,
        playerObjectId,
        pledgeType,
        hasSponsor,
        isOnline: playerObjectId !== 0
    }
}