import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface PutEnchantTargetItemResultEvent extends PacketEvent {
    itemObjectId: number
}
export function PutEnchantTargetItemResult( packet: ReadableClientPacket ) : PutEnchantTargetItemResultEvent {
    return {
        itemObjectId: packet.readD()
    }
}