import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { WarehouseType } from '../../../enums/WarehouseType'
import _ from 'lodash'
import { readNormalItem } from '../helpers/ItemUpdateHelper'

export interface WareHouseWithdrawalEvent extends PacketEvent {
    type: WarehouseType
    adena: number
    itemObjectIds: Array<number>
}
export function WareHouseWithdrawal( packet : ReadableClientPacket ) : WareHouseWithdrawalEvent {
    let type = packet.readH(),
            adena = packet.readQ(),
            size = packet.readH()

    let itemObjectIds : Array<number> = _.times( size, () => {
        let item = readNormalItem( packet )
        packet.skipD( 1 )

        return item.objectId
    } )

    return {
        type,
        adena,
        itemObjectIds
    }
}