import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export enum AccessFailReason {
    NoReasonSpecified,
    SystemErrorLoginLater,
    PasswordDoesNotMatchThisAccount,
    PasswordDoesNotMatchThisAccount2,
    AccessFailedTryLater,
    IncorrectAccountInfoContactCustomerSupport,
    AccessFailedTryLater2,
    AcountAlreadyInUse,
    AccessFailedTryLater3,
    AccessFailedTryLater4,
    AccessFailedTryLater5,
}

export interface AccessFailedEvent extends PacketEvent {
    reason: AccessFailReason
}
export function AccessFailed( packet : ReadableClientPacket ) : AccessFailedEvent {
    return {
        reason: packet.readD()
    }
}