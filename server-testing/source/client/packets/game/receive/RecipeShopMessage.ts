import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface RecipeShopMessageEvent extends PacketEvent {
    playerObjectId: number
    message: string
}

export function RecipeShopMessage( packet: ReadableClientPacket ) : RecipeShopMessageEvent {
    let playerObjectId = packet.readD(),
            message = packet.readS()

    return {
        playerObjectId,
        message
    }
}