import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface CubeGameChangePointsEvent extends PacketEvent {
    timeLeft: number
    bluePoints: number
    redPoints: number
}
export function CubeGameChangePoints( packet: ReadableClientPacket ) : CubeGameChangePointsEvent {
    // TODO use skipped status
    // 0x02 - normal change of points
    // 0x00 - extended version that includes side, player object id and player points
    packet.skipD( 1 )

    let timeLeft = packet.readD(),
            bluePoints = packet.readD(),
            redPoints = packet.readD()

    return {
        timeLeft,
        bluePoints,
        redPoints
    }
}