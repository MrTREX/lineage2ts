import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export const enum NpcSayType {
    All = 0,
    Shout = 1,
    Tell = 2,
    Party = 3, // #
    Clan = 4, // @
    GM = 5,
    PetitionPlayer = 6,
    PetitionGM = 7,
    Trade = 8,
    Alliance = 9,
    Announce = 10,
    Boat = 11,
    Friend = 12,
    MSNChat = 13,
    PartymatchRoom = 14,
    PartyroomCommander = 15,
    PartyroomAll = 16,
    HeroVoice = 17,
    CriticalAnnouncement = 18,
    ScreenAnnouncement = 19,
    Battlefield = 20,
    MpccRoom = 21,
    NpcAll = 22,
    NpcShout = 23,
}

export interface NpcSayEvent extends PacketEvent {
    type: NpcSayType
    objectId: number
    npcId: number
    stringId: number
    lines: Array<string>
}
export function NpcSay( packet : ReadableClientPacket ) : NpcSayEvent {
    let objectId = packet.readD(),
            type = packet.readD(),
            npcId = packet.readD() - 1000000,
            stringId = packet.readD()

    let lines : Array<string> = []
    while ( packet.hasMoreData() ) {
        lines.push( packet.readS() )
    }

    return {
        objectId,
        type,
        npcId,
        stringId,
        lines
    }
}