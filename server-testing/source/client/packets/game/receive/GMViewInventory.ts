import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { readNormalItem } from '../helpers/ItemUpdateHelper'
import _ from 'lodash'
import { ItemState } from '../../../enums/ItemState'

export interface GMViewInventoryEvent extends PacketEvent {
    name: string
    slotLimit: number
    itemObjectIds: Array<number>
}

export function GMViewInventory( packet: ReadableClientPacket ) : GMViewInventoryEvent {
    let name = packet.readS(),
            slotLimit = packet.readD(),
            size = packet.readD()

    return {
        name,
        slotLimit,
        itemObjectIds: _.times( size ,() : number => {
            return readNormalItem( packet, ItemState.PickedUp ).objectId
        } )
    }
}