import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { PartyDistributionType } from '../../../enums/PartyDistributionType'

export interface AskJoinPartyEvent extends PacketEvent {
    requester: string
    distributionType: PartyDistributionType
}
export function AskJoinParty( packet : ReadableClientPacket ) : AskJoinPartyEvent {
    let requester = packet.readS()
    return {
        requester,
        distributionType: packet.readD()
    }
}