import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { LocationProperties } from '../../../models/LocationProperties'
import { readLocation } from '../helpers/PropertyHelper'

export interface ExitObservationModeEvent extends PacketEvent, LocationProperties {}

export function ExitObservationMode( packet: ReadableClientPacket ) : ExitObservationModeEvent {
    return readLocation( packet ) as ExitObservationModeEvent
}