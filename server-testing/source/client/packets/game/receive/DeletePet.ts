import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { L2SummonType } from '../../../enums/SummonType'
import { L2World } from '../../../L2World'

export interface DeletePetEvent extends PacketEvent {
    objectId: number
    type: L2SummonType
}
export function DeletePet( packet: ReadableClientPacket ) : DeletePetEvent {
    let type = packet.readD(),
            objectId = packet.readD()

    L2World.deleteObject( objectId )

    return {
        type,
        objectId
    }
}