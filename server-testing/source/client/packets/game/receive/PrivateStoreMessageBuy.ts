import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface PrivateStoreMessageBuyEvent extends PacketEvent {
    playerObjectId: number
    message: string
}

export function PrivateStoreMessageBuy( packet: ReadableClientPacket ) : PrivateStoreMessageBuyEvent {
    let playerObjectId = packet.readD(),
            message = packet.readS()

    return {
        playerObjectId,
        message
    }
}