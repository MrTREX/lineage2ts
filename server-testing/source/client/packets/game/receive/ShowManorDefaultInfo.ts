import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface ShowManorDefaultSeed {
    cropId: number
    level: number
    seedReferencePrice: number
    cropReferencePrice: number
    rewardOneId: number
    rewardTwoId: number
}

export interface ShowManorDefaultInfoEvent extends PacketEvent {
    showUIButtons: boolean
    seeds: Array<ShowManorDefaultSeed>
}

export function ShowManorDefaultInfo( packet: ReadableClientPacket ) : ShowManorDefaultInfoEvent {
    let showUIButtons = packet.readD() === 0,
            size = packet.readD()

    return {
        showUIButtons,
        seeds: _.times( size, () : ShowManorDefaultSeed => {
            let cropId = packet.readD(),
                    level = packet.readD(),
                    seedReferencePrice = packet.readD(),
                    cropReferencePrice = packet.readD()

            packet.skipB( 1 )

            let rewardOneId = packet.readD()

            packet.skipB( 1 )

            let rewardTwoId = packet.readD()

            return {
                cropId,
                level,
                seedReferencePrice,
                cropReferencePrice,
                rewardOneId,
                rewardTwoId
            }
        } )
    }
}