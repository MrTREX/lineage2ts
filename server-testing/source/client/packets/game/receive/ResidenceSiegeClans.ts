import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface ResidenceSiegeClanData {
    id: number
    name: string
    leaderName: string
    crestId: number
    allyId: number
    allyName: string
    allyCrestId: number
}

export interface ResidenceSiegeClansEvent extends PacketEvent {
    residenceId: number
    clans: Array<ResidenceSiegeClanData>
}

export function ResidenceSiegeClans( packet: ReadableClientPacket ) : ResidenceSiegeClansEvent {
    let residenceId = packet.readD()

    packet.skipD( 4 )

    let size = packet.readD()

    return {
        residenceId,
        clans: _.times( size, () : ResidenceSiegeClanData => {
            let id = packet.readD(),
                    name = packet.readS(),
                    leaderName = packet.readS(),
                    crestId = packet.readD()

            packet.skipD( 1 )

            let allyId = packet.readD(),
                    allyName = packet.readS()

            packet.skipS()

            let allyCrestId = packet.readD()

            return {
                id,
                name,
                leaderName,
                crestId,
                allyId,
                allyName,
                allyCrestId
            }
        } )
    }
}