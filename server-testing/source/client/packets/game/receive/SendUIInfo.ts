import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export const enum SendUIInfoCountdown {
    CountDown,
    CountUp
}
export interface SendUIInfoEvent extends PacketEvent {
    playerObjectId: number
    isShowing: boolean
    countdownDirection: SendUIInfoCountdown
    startMinutes: string
    startSeconds: string
    endMinutes: string
    endSeconds: string
    npcStringId: number
    textLines: Array<string>
}
export function SendUIInfo( packet: ReadableClientPacket ) : SendUIInfoEvent {
    let playerObjectId = packet.readD(),
            isShowing = packet.readD() === 1

    packet.skipD( 2 )

    let countdownDirection = parseInt( packet.readS() ),
            startMinutes = packet.readS(),
            startSeconds = packet.readS(),
            endMinutes = packet.readS(),
            endSeconds = packet.readS(),
            npcStringId = packet.readD(),
            textLines = []

    while ( packet.hasMoreData() ) {
        textLines.push( packet.readS() )
    }

    return {
        playerObjectId,
        isShowing,
        countdownDirection,
        startMinutes,
        startSeconds,
        endMinutes,
        endSeconds,
        npcStringId,
        textLines
    }
}