import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface DoorEventTriggerEvent extends PacketEvent {
    emitterId: number
    isEnabled: boolean
}

export function DoorEventTrigger( packet: ReadableClientPacket ) : DoorEventTriggerEvent {
    let emitterId = packet.readD(),
            isEnabled = packet.readC() === 1

    return {
        emitterId,
        isEnabled
    }
}