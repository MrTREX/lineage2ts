import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export const enum CharacterCreationFailureReason {
    creationFailed,
    /** You cannot create another character. Please delete the existing character and try again." Removes all settings that were selected (race, class, etc). */
    tooManyCharacters,
    /** This name already exists. */
    nameAlreadyExists,
    /** Your title cannot exceed 16 characters in length. Please try again. */
    nameTooLong,
    /** Incorrect name. Please try again. */
    badNaming,
    /** Characters cannot be created from this server. */
    creationDenied,
    /**
     * Unable to create character. You are unable to create a new character on the selected server. A restriction is in place which restricts users from creating characters on different servers where no previous character exists. Please choose another server.
     */
    otherReason,
}

export const CharacterCreationFailureMessage : Record< number, string> = {
    [ CharacterCreationFailureReason.creationFailed ]: 'Character creation failed.',
    [ CharacterCreationFailureReason.tooManyCharacters ]: 'You cannot create another character. Please delete the existing character and try again." Removes all settings that were selected (race, class, etc).',
    [ CharacterCreationFailureReason.nameAlreadyExists ]: 'This name already exists.',
    [ CharacterCreationFailureReason.nameTooLong ]: 'Your title cannot exceed 16 characters in length. Please try again.',
    [ CharacterCreationFailureReason.badNaming ]: 'Incorrect name. Please try again.',
    [ CharacterCreationFailureReason.creationDenied ]: 'Characters cannot be created from this server.',
    [ CharacterCreationFailureReason.otherReason ]: 'Unable to create character. You are unable to create a new character on the selected server. A restriction is in place which restricts users from creating characters on different servers where no previous character exists. Please choose another server.'

}

export interface CharacterCreationFailureEvent extends PacketEvent {
    reason : CharacterCreationFailureReason
}
export function CharacterCreationFailure( packet : ReadableClientPacket ) : CharacterCreationFailureEvent {
    return {
        reason: packet.readD()
    }
}