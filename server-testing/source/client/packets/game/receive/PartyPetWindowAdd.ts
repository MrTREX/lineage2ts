import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { getSummonForUpdate } from '../helpers/CharacterUpdateHelper'

export interface PartyPetWindowAddEvent extends PacketEvent {
    summonObjectId: number
}
export function PartyPetWindowAdd( packet: ReadableClientPacket ) : PartyPetWindowAddEvent {
    let summonObjectId = packet.readD()

    let summon = getSummonForUpdate( summonObjectId )

    summon.templateId = packet.readD() - 1000000
    summon.summonType = packet.readD()
    summon.ownerObjectId = packet.readD()
    summon.name = packet.readS()

    summon.hp = packet.readD()
    summon.maxHp = packet.readD()
    summon.mp = packet.readD()
    summon.maxHp = packet.readD()
    summon.level = packet.readD()

    return {
        summonObjectId
    }
}