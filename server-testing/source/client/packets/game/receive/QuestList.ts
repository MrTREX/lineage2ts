import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface QuestListItem {
    id: number
    flags: number
}

export interface QuestListEvent extends PacketEvent {
    quests: Array<QuestListItem>
}

export function QuestList( packet: ReadableClientPacket ) : QuestListEvent {
    let size = packet.readH()

    return {
        quests: _.times( size, () : QuestListItem => {
            let id = packet.readD(),
                    flags = packet.readD()

            return {
                id,
                flags
            }
        } )
    }
}