import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { ItemType } from '../../../enums/ItemType'
import { ItemSlots } from '../../../enums/ItemSlots'
import _ from 'lodash'

export interface PreviewItemData {
    itemId: number
    type: ItemType
    slotMask: ItemSlots
    wearPrice: number
}

export interface PreviewItemListEvent extends PacketEvent {
    adena: number
    listId: number
    items: Array<PreviewItemData>
}

export function PreviewItemList( packet: ReadableClientPacket ) : PreviewItemListEvent {
    packet.skipB( 4 )

    let adena = packet.readQ(),
            listId = packet.readD(),
            size = packet.readH()

    return {
        adena,
        listId,
        items: _.times( size, () : PreviewItemData => {
            let itemId = packet.readD(),
                    type = packet.readH(),
                    slotMask = packet.readH(),
                    wearPrice = packet.readQ()

            return {
                itemId,
                type,
                slotMask,
                wearPrice
            }
        } )
    }
}