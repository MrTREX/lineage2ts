import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { getPlayerForUpdate } from '../helpers/CharacterUpdateHelper'

export interface PartySmallWindowUpdateEvent extends PacketEvent {
    memberObjectId: number
}
export function PartySmallWindowUpdate( packet: ReadableClientPacket ) : PartySmallWindowUpdateEvent {
    let memberObjectId = packet.readD()

    let player = getPlayerForUpdate( memberObjectId )

    player.name = packet.readS()
    player.cp = packet.readD()
    player.maxCp = packet.readD()
    player.hp = packet.readD()
    player.maxHp = packet.readD()
    player.mp = packet.readD()
    player.maxMp = packet.readD()
    player.level = packet.readD()
    player.classId = packet.readD()

    return {
        memberObjectId
    }
}