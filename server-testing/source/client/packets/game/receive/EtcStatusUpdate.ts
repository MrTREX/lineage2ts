import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface EtcStatusUpdateEvent extends PacketEvent {
    charges: number
    weightPenalty: number
    inMessageRefusal: boolean
    inDangerZone: boolean
    expertiseWeaponPenalty: number
    expertiseArmoPenalty: number
    hasCharmOfCourage: boolean
    deathPenaltyBuffLevel: number
    chargedSouls: number
}

export function EtcStatusUpdate( packet: ReadableClientPacket ): EtcStatusUpdateEvent {
    let charges = packet.readD(),
            weightPenalty = packet.readD(),
            inMessageRefusal = packet.readD() === 1,
            inDangerZone = packet.readD() === 1,
            expertiseWeaponPenalty = packet.readD(),
            expertiseArmoPenalty = packet.readD(),
            hasCharmOfCourage = packet.readD() === 1,
            deathPenaltyBuffLevel = packet.readD(),
            chargedSouls = packet.readD()

    return {
        charges,
        weightPenalty,
        inMessageRefusal,
        inDangerZone,
        expertiseWeaponPenalty,
        expertiseArmoPenalty,
        hasCharmOfCourage,
        deathPenaltyBuffLevel,
        chargedSouls,
    }
}