import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface ShowFortressMapInfoEvent extends PacketEvent {
    residenceId: number
    isSiegeInProgress: boolean
    barrackSpawnAmount: number
}

export function ShowFortressMapInfo( packet: ReadableClientPacket ) : ShowFortressMapInfoEvent {
    let residenceId = packet.readD(),
            isSiegeInProgress = packet.readD() === 1,
            size = packet.readD()

    let barrackSpawnIndicators = _.times( size, () : number => packet.readD() )

    return {
        residenceId,
        isSiegeInProgress,
        barrackSpawnAmount: _.sum( barrackSpawnIndicators )
    }
}