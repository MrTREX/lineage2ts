import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { LocationProperties } from '../../../models/LocationProperties'
import { FishGroup } from '../../../enums/FishGroup'

export interface FishingStartEvent extends PacketEvent, LocationProperties {
    playerObjectId: number
    fishGroup: FishGroup
    isNightTime: boolean
}
export function FishingStart( packet: ReadableClientPacket ) : FishingStartEvent {
    let playerObjectId = packet.readD(),
            fishGroup = packet.readD(),
            x = packet.readD(),
            y = packet.readD(),
            z = packet.readD(),
            isNightTime = packet.readC() === 1

    return {
        playerObjectId,
        fishGroup,
        x,
        y,
        z,
        isNightTime
    }
}