import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface PartyMatchingWaitingRoomPlayer {
    name: string
    classId: number
    level: number
    instanceIds : Array<number>
}
export interface PartyMatchingWaitingRoomListEvent extends PacketEvent {
    allPlayers: number
    visiblePagePlayers: Array<PartyMatchingWaitingRoomPlayer>
}
export function PartyMatchingWaitingRoomList( packet : ReadableClientPacket ) : PartyMatchingWaitingRoomListEvent {
    let allPlayers = packet.readD(),
            size = packet.readD()

    return {
        allPlayers,
        visiblePagePlayers: _.times( size, () : PartyMatchingWaitingRoomPlayer => {
            let name = packet.readS(),
                    classId = packet.readD(),
                    level = packet.readD()

            packet.skipD( 1 )

            let instanceSize = packet.readD()

            return {
                name,
                classId,
                level,
                instanceIds: _.times( instanceSize, () : number => packet.readD() )
            }
        } )
    }
}