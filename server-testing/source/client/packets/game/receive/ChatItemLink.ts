import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { getItemForUpdate } from '../helpers/ItemUpdateHelper'
import { ItemState } from '../../../enums/ItemState'

export interface ChatItemLinkEvent extends PacketEvent {
    itemObjectId: number
}
export function ChatItemLink( packet: ReadableClientPacket ) : ChatItemLinkEvent {
    let itemObjectId = packet.readD()

    let item = getItemForUpdate( itemObjectId, ItemState.PickedUp )

    item.itemId = packet.readD()

    packet.skipD( 1 )

    item.amount = packet.readQ()
    item.itemType = packet.readH()

    packet.skipH( 1 )

    item.isEquipped = packet.readH() === 1

    packet.skipD( 1 )

    item.enchantLevel = packet.readH()

    packet.skipH( 1 )

    item.augmentationId = packet.readD()

    return {
        itemObjectId
    }
}