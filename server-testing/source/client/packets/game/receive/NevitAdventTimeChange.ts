import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface NevitAdventTimeChangeEvent extends PacketEvent {
    isStarted: boolean
    timeLeft: number
}
export function NevitAdventTimeChange( packet : ReadableClientPacket ) : NevitAdventTimeChangeEvent {
    let isStarted = packet.readC() === 1,
            timeLeft = packet.readD()

    return {
        isStarted,
        timeLeft
    }
}