import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface DuelAskStartEvent extends PacketEvent {
    name: string
    isPartyDuel: boolean
}
export function DuelAskStart( packet : ReadableClientPacket ) : DuelAskStartEvent {
    let name = packet.readS(),
            isPartyDuel = packet.readD() === 1

    return {
        name,
        isPartyDuel
    }
}