import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface ShowTownMapEvent extends PacketEvent {
    name: string
    x: number
    y: number
}

export function ShowTownMap( packet: ReadableClientPacket ) : ShowTownMapEvent {
    let name = packet.readS(),
            x = packet.readD(),
            y = packet.readD()

    return {
        name,
        x,
        y
    }
}