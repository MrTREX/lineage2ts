import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface StopRotationEvent extends PacketEvent {
    objectId: number
    degree: number
    speed: number
}
export function StopRotation( packet : ReadableClientPacket ) : StopRotationEvent {
    let objectId = packet.readD(),
            degree = packet.readD(),
            speed = packet.readD()

    return {
        objectId,
        degree,
        speed
    }
}