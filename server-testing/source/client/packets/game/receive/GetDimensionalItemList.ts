import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { ItemDefinitionProperties } from '../../../models/ItemDefinitionProperties'
import _ from 'lodash'

export interface GetDimentionalItemData extends ItemDefinitionProperties {
    senderName: string
}
export interface GetDimensionalItemListEvent extends PacketEvent {
    items: Array<GetDimentionalItemData>
}
export function GetDimensionalItemList( packet: ReadableClientPacket ) : GetDimensionalItemListEvent {
    let size = packet.readD()

    return {
        items: _.times( size, () : GetDimentionalItemData => {
            packet.skipD( 2 )

            let itemId = packet.readD(),
                    amount = packet.readQ()

            packet.skipD( 1 )

            let senderName = packet.readS()

            return {
                itemId,
                amount,
                senderName
            }
        } )
    }
}