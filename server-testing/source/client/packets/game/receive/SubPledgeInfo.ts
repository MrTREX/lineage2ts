import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface SubPledgeInfoEvent extends PacketEvent {
    id: number
    pledgeName: string
    clanLeaderName: string
}

export function SubPledgeInfo( packet: ReadableClientPacket ) : SubPledgeInfoEvent {
    packet.skipD( 1 )

    let id = packet.readD(),
            pledgeName = packet.readS(),
            clanLeaderName = packet.readS()

    return {
        id,
        pledgeName,
        clanLeaderName
    }
}