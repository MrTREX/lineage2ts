import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { getPlayerForUpdate } from '../helpers/CharacterUpdateHelper'

export interface PlayerRidesMountEvent extends PacketEvent {
    playerObjectId: number
    isMounted: boolean
}

export function PlayerRidesMount( packet: ReadableClientPacket ) : PlayerRidesMountEvent {
    let playerObjectId = packet.readD(),
            isMounted = packet.readD() === 1

    let player = getPlayerForUpdate( playerObjectId )
    player.mountType = packet.readD()
    player.mountNpcId = packet.readD() - 1000000
    player.x = packet.readD()
    player.y = packet.readD()
    player.z = packet.readD()

    return {
        playerObjectId,
        isMounted
    }
}