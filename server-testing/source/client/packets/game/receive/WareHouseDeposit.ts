import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { readNormalItem } from '../helpers/ItemUpdateHelper'
import _ from 'lodash'
import { WarehouseType } from '../../../enums/WarehouseType'

export interface WareHouseDepositEvent extends PacketEvent {
    type: WarehouseType
    adena: number
    itemObjectIds: Array<number>
}
export function WareHouseDeposit( packet: ReadableClientPacket ) : WareHouseDepositEvent {
    let type = packet.readH(),
            adena = packet.readQ(),
            size = packet.readH()

    let itemObjectIds : Array<number> = _.times( size, () => {
        let item = readNormalItem( packet )
        packet.skipD( 1 )

        return item.objectId
    } )

    return {
        type,
        adena,
        itemObjectIds
    }
}