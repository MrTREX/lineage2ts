import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { L2SummonType } from '../../../enums/SummonType'

export interface PetStatusInfoEvent extends PacketEvent {
    summonType: L2SummonType
}

export function PetStatusInfo( packet: ReadableClientPacket ) : PetStatusInfoEvent {
    return {
        summonType: packet.readD()
    }
}