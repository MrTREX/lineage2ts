import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { getPlayerForUpdate } from '../helpers/CharacterUpdateHelper'
import _ from 'lodash'

export const enum PartyRoomMemberOperation {
    Remove,
    Add,
    View
}

export const enum PartyRoomMemberType {
    OtherPartyMember,
    Leader,
    LeaderPartyMember
}

export interface PartyRoomMemberData {
    playerObjectId: number
    bbsLocationId: number
    type: PartyRoomMemberType
}

export interface PartyRoomMemberEvent extends PacketEvent {
    operation: PartyRoomMemberOperation
    members: Array<PartyRoomMemberData>
}
export function PartyRoomMember( packet: ReadableClientPacket ) : PartyRoomMemberEvent {
    let operation = packet.readD(),
            size = packet.readD()

    return {
        operation,
        members: _.times( size, () : PartyRoomMemberData => {
            let playerObjectId = packet.readD()

            let player = getPlayerForUpdate( playerObjectId )

            player.name = packet.readS()
            player.classId = packet.readD()
            player.level = packet.readD()

            let bbsLocationId = packet.readD(),
                    type = packet.readD()

            packet.skipD( 1 )

            return {
                playerObjectId,
                bbsLocationId,
                type
            }
        } )
    }
}