import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface PledgeMemberAddEvent extends PacketEvent {
    name: string
    level: number
    classId: number
    isOnline: boolean
    pledgeType: number // TODO : add enum
}

export function PledgeMemberAdd( packet: ReadableClientPacket ) : PledgeMemberAddEvent {
    let name = packet.readS(),
            level = packet.readD(),
            classId = packet.readD()

    packet.skipD( 2 )

    let isOnline = packet.readD() === 1,
            pledgeType = packet.readD()

    return {
        name,
        level,
        classId,
        isOnline,
        pledgeType
    }
}