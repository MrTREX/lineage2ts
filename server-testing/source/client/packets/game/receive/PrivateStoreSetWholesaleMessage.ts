import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface PrivateStoreSetWholesaleMessageEvent extends PacketEvent {
    playerObjectId: number
    message: string
}
export function PrivateStoreSetWholesaleMessage( packet: ReadableClientPacket ) : PrivateStoreSetWholesaleMessageEvent {
    let playerObjectId = packet.readD(),
            message = packet.readS()

    return {
        playerObjectId,
        message
    }
}