import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface CursedWeaponListEvent extends PacketEvent {
    itemIds: Array<number>
}
export function CursedWeaponList( packet: ReadableClientPacket ) : CursedWeaponListEvent {
    let size = packet.readD()

    return {
        itemIds: _.times( size, () : number => packet.readD() )
    }
}