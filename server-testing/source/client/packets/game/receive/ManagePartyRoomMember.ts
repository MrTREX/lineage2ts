import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { getPlayerForUpdate } from '../helpers/CharacterUpdateHelper'

export const enum ManagePartyRoomMemberOperation {
    Add,
    PromoteLeader,
    Remove
}
export interface ManagePartyRoomMemberEvent extends PacketEvent {
    operation: ManagePartyRoomMemberOperation
    playerObjectId: number
    roomLootType: number // TODO : add enum, value comes from client and is proxied by server
}
export function ManagePartyRoomMember( packet: ReadableClientPacket ) : ManagePartyRoomMemberEvent {
    let operation = packet.readD(),
            playerObjectId = packet.readD()

    let player = getPlayerForUpdate( playerObjectId )

    player.name = packet.readS()
    player.classId = packet.readD()
    player.level = packet.readD()

    let roomLootType = packet.readD()

    return {
        operation,
        playerObjectId,
        roomLootType
    }
}