import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export const enum EnchantResultType {
    Success = 0,
    Failure = 1,
    Cancelled = 2,

    BlessedFailure = 3,
    UnrecoverableFailure = 4,
    SafeFailure = 5
}

export interface EnchantResultEvent extends PacketEvent {
    type: EnchantResultType
    crystalId: number
    crystalAmount: number
}

export function EnchantResult( packet: ReadableClientPacket ) : EnchantResultEvent {
    let type = packet.readD(),
            crystalId = packet.readD(),
            crystalAmount = packet.readQ()

    return {
        type,
        crystalId,
        crystalAmount
    }
}