import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

/*
    Empty event to be able to subscribe.
    Existence of packet signifies that deletion has succeeded.
 */
export interface CharacterDeleteSuccessEvent extends PacketEvent {}

export function CharacterDeleteSuccess( packet: ReadableClientPacket ) : CharacterDeleteSuccessEvent {
    return {}
}