import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface TradeOwnAddEvent extends PacketEvent {
    itemObjectId: number
    itemId: number
    amount: number
}

export function TradeOwnAdd( packet : ReadableClientPacket ) : TradeOwnAddEvent {
    packet.skipD( 1 )

    let itemObjectId = packet.readD(),
            itemId = packet.readD(),
            amount = packet.readQ()

    return {
        amount,
        itemId,
        itemObjectId
    }
}