import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export const enum ShowAuctionHallType {
    Auction,
    War
}

export interface ShowAuctionHallData {
    id: number
    clanName: string
    clanLeaderName: string
    type: ShowAuctionHallType
}
export interface ShowAuctionHallInfoEvent extends PacketEvent {
    clanHalls: Array<ShowAuctionHallData>
}
export function ShowAuctionHallInfo( packet: ReadableClientPacket ) : ShowAuctionHallInfoEvent {
    let size = packet.readD()

    return {
        clanHalls: _.times( size, () : ShowAuctionHallData => {
            let id = packet.readD(),
                    clanName = packet.readS(),
                    clanLeaderName = packet.readS(),
                    type = packet.readD()

            return {
                id,
                clanName,
                clanLeaderName,
                type
            }
        } )
    }
}