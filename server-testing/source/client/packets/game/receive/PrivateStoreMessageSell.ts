import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface PrivateStoreMessageSellEvent extends PacketEvent {
    playerObjectId: number
    message: string
}

export function PrivateStoreMessageSell( packet: ReadableClientPacket ) : PrivateStoreMessageSellEvent {
    let playerObjectId = packet.readD(),
            message = packet.readS()

    return {
        playerObjectId,
        message
    }
}