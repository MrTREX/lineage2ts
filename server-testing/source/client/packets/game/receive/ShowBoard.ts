import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface ShowBoardEvent extends PacketEvent {
    isShowing: boolean
    top: string,
    favorite: string,
    region: string,
    clan: string,
    memo: string,
    mail: string,
    friends: string,
    addFavorite: string
    content: string
}
export function ShowBoard( packet : ReadableClientPacket ) : ShowBoardEvent {
    let isShowing = packet.readC() === 1,
            top = packet.readS(),
            favorite = packet.readS(),
            region = packet.readS(),
            clan = packet.readS(),
            memo = packet.readS(),
            mail = packet.readS(),
            friends = packet.readS(),
            addFavorite = packet.readS(),
            content = packet.readS()

    return {
        isShowing,
        top,
        favorite,
        region,
        clan,
        memo,
        mail,
        friends,
        addFavorite,
        content
    }
}