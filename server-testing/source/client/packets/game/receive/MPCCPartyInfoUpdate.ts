import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { getPlayerForUpdate } from '../helpers/CharacterUpdateHelper'

export const enum MPCCPartyInfoMode {
    Remove,
    Add
}

export interface MPCCPartyInfoUpdateEvent extends PacketEvent {
    leaderPlayerObjectId: number
    memberCount: number
    mode: MPCCPartyInfoMode
}
export function MPCCPartyInfoUpdate( packet: ReadableClientPacket ) : MPCCPartyInfoUpdateEvent {
    let name = packet.readS(),
            leaderPlayerObjectId = packet.readD(),
            memberCount = packet.readD(),
            mode = packet.readD()

    let player = getPlayerForUpdate( leaderPlayerObjectId )
    player.name = name

    return {
        memberCount,
        leaderPlayerObjectId,
        mode
    }
}