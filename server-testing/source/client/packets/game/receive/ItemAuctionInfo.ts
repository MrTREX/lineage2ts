import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { readNormalItem } from '../helpers/ItemUpdateHelper'

export interface ItemAuctionInfoEvent extends PacketEvent {
    isTimerRefreshed: boolean
    npcId: number
    highestBid: number
    remainingTime: number
    itemObjectId: number
    nextAuctionBid: number
    nextAuctionTime: number
    nextAuctionItemObjectId: number
}
export function ItemAuctionInfo( packet: ReadableClientPacket ) : ItemAuctionInfoEvent {
    let isTimerRefreshed = packet.readC() === 1,
            npcId = packet.readD(),
            highestBid = packet.readQ(),
            remainingTime = packet.readD()

    let item = readNormalItem( packet )
    let nextAuctionBid = 0,
            nextAuctionTime = 0,
            nextAuctionItemObjectId = 0

    if ( packet.hasMoreData() ) {
        nextAuctionBid = packet.readQ()
        nextAuctionTime = packet.readD()
        nextAuctionItemObjectId = readNormalItem( packet ).objectId
    }

    return {
        isTimerRefreshed,
        npcId,
        highestBid,
        remainingTime,
        itemObjectId: item.objectId,
        nextAuctionBid,
        nextAuctionTime,
        nextAuctionItemObjectId
    }
}