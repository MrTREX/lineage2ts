import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { readNormalItem } from '../helpers/ItemUpdateHelper'
import _ from 'lodash'
import { ItemState } from '../../../enums/ItemState'

export interface PetItemListEvent extends PacketEvent {
    itemObjectIds: Array<number>
}

export function PetItemList( packet: ReadableClientPacket ) : PetItemListEvent {
    let size = packet.readH()

    return {
        itemObjectIds: _.times( size, () : number => {
            return readNormalItem( packet, ItemState.PickedUp ).objectId
        } )
    }
}