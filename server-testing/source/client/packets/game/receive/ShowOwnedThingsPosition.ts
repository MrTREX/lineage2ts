import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { LocationProperties } from '../../../models/LocationProperties'
import _ from 'lodash'

export interface ShowOwnedThingsPositionEvent extends PacketEvent {
    positions: Array<LocationProperties>
}

export function ShowOwnedThingsPosition( packet: ReadableClientPacket ) : ShowOwnedThingsPositionEvent {
    let size = packet.readD()

    return {
        positions: _.times( size, () : LocationProperties => {
            let x = packet.readD(),
                    y = packet.readD(),
                    z = packet.readD()

            return {
                x,
                y,
                z
            }
        } )
    }
}