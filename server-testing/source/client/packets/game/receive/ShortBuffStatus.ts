import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

/*
    Note that buffs are considered reset when skillId and skillLevel have zero values.
 */
export interface ShortBuffStatusEvent extends PacketEvent {
    skillId: number
    skillLevel: number
    duration: number
}

export function ShortBuffStatus( packet: ReadableClientPacket ) : ShortBuffStatusEvent {
    let skillId = packet.readD(),
            skillLevel = packet.readD(),
            duration = packet.readD()

    return {
        skillId,
        skillLevel,
        duration
    }
}