import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface LeaveWorldEvent extends PacketEvent {}
export function LeaveWorld( packet : ReadableClientPacket ) : LeaveWorldEvent {
    return {}
}