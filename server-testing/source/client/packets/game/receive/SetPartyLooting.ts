import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { PartyDistributionType } from '../../../enums/PartyDistributionType'

export interface SetPartyLootingEvent extends PacketEvent {
    lootType: PartyDistributionType
    isSuccess: boolean
}
export function SetPartyLooting( packet: ReadableClientPacket ) : SetPartyLootingEvent {
    let isSuccess = packet.readD() === 1,
            lootType = packet.readD()

    return {
        isSuccess,
        lootType
    }
}