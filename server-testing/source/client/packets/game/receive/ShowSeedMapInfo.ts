import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { LocationProperties } from '../../../models/LocationProperties'

export interface ShowSeedMapData extends LocationProperties {
    messageId: number
}

export interface ShowSeedMapInfoEvent extends PacketEvent {
    seedOfDestruction: ShowSeedMapData
    seedOfInfinity: ShowSeedMapData
}

function readSeedData( packet: ReadableClientPacket ) : ShowSeedMapData {
    let x = packet.readD(),
            y = packet.readD(),
            z = packet.readD(),
            messageId = packet.readD()

    return {
        x,
        y,
        z,
        messageId
    }
}

export function ShowSeedMapInfo( packet: ReadableClientPacket ) : ShowSeedMapInfoEvent {
    packet.skipD( 1 ) // seed amount

    let seedOfDestruction = readSeedData( packet ),
            seedOfInfinity = readSeedData( packet )

    return {
        seedOfDestruction,
        seedOfInfinity
    }
}