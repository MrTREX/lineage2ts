import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export const enum VehicleStartedState {
    Start,
    Stop
}

export interface VehicleStartedEvent extends PacketEvent {
    objectId: number
    state: VehicleStartedState
}
export function VehicleStarted( packet : ReadableClientPacket ) : VehicleStartedEvent {
    let objectId = packet.readD(),
            state = packet.readD()

    return {
        objectId,
        state
    }
}