import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface DeleteObjectEvent extends PacketEvent {
    objectId: number
}
export function DeleteObject( packet: ReadableClientPacket ) : DeleteObjectEvent {
    return {
        objectId: packet.readD()
    }
}