import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface ShowContactListEvent extends PacketEvent {
    names: Array<string>
}
export function ShowContactList( packet : ReadableClientPacket ) : ShowContactListEvent {
    let size = packet.readD()
    return {
        names: _.times( size, () : string => {
            return packet.readS()
        } )
    }
}