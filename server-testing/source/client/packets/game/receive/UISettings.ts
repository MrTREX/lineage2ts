import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface UISettingKey {
    id: number
    key: number
    toggleOne: number
    toggleTwo: number
    isVisible: boolean
}

export interface UISettingCategory {
    values: Array<number>
}

export interface UISettingsEvent extends PacketEvent {
    keys: Array<UISettingKey>
    categories: Array<UISettingCategory>
}
export function UISettings( packet : ReadableClientPacket ) : UISettingsEvent {
    packet.skipD( 2 )
    let size = packet.readD()

    let categories : Array<UISettingCategory> = []
    let keys : Array<UISettingKey> = []

    _.times( size, () => {
        let size = packet.readC()
        if ( size > 0 ) {
            categories.push( {
                values: _.times( size, () : number => packet.readC() )
            } )
        }

        size = packet.readC()
        if ( size > 0 ) {
            categories.push( {
                values: _.times( size, () : number => packet.readC() )
            } )
        }

        size = packet.readD()
        if ( size > 0 ) {
            _.times( size, () => {
                let id = packet.readD(),
                        key = packet.readD(),
                        toggleOne = packet.readD(),
                        toggleTwo = packet.readD(),
                        isVisible = packet.readD() === 1

                keys.push( {
                    id,
                    key,
                    toggleOne,
                    toggleTwo,
                    isVisible
                } )
            } )
        }
    } )

    return {
        categories,
        keys
    }
}