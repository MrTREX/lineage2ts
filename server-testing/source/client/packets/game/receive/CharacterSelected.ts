import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { getPlayerForUpdate } from '../helpers/CharacterUpdateHelper'

export interface GameCharacterSelectedEvent extends PacketEvent {
    objectId: number
    sessionId: number
}

export function CharacterSelected( packet : ReadableClientPacket ) : GameCharacterSelectedEvent {
    let name = packet.readS(),
            objectId = packet.readD()

    let player = getPlayerForUpdate( objectId )

    player.name = name
    player.title = packet.readS()

    let sessionId = packet.readD()

    player.clanId = packet.readD()

    packet.skipD( 1 )

    player.sex = packet.readD()
    player.race = packet.readD()
    player.classId = packet.readD()

    packet.skipD( 1 )

    player.x = packet.readD()
    player.y = packet.readD()
    player.z = packet.readD()
    player.hp = packet.readF()

    player.mp = packet.readF()
    player.sp = packet.readD()
    player.exp = packet.readQ()
    player.level = packet.readD()

    player.karma = packet.readD()
    player.pkKills = packet.readD()
    player.INT = packet.readD()
    player.STR = packet.readD()

    player.CON = packet.readD()
    player.MEN = packet.readD()
    player.DEX = packet.readD()
    player.WIT = packet.readD()

    return {
        objectId,
        sessionId
    }
}