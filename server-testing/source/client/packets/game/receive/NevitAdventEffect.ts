import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface NevitAdventEffectEvent extends PacketEvent {
    time: number
}

export function NevitAdventEffect( packet: ReadableClientPacket ) : NevitAdventEffectEvent {
    return {
        time: packet.readD()
    }
}