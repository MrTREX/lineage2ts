import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { getPlayerForUpdate } from '../helpers/CharacterUpdateHelper'

export interface RecipeShopItemInfoEvent extends PacketEvent {
    playerObjectId: number
    recipeId: number
    mp: number
    maxMp: number
}

export function RecipeShopItemInfo( packet: ReadableClientPacket ) : RecipeShopItemInfoEvent {
    let playerObjectId = packet.readD(),
            recipeId = packet.readD(),
            mp = packet.readD(),
            maxMp = packet.readD()

    let player = getPlayerForUpdate( playerObjectId )
    player.mp = mp
    player.maxMp = maxMp

    return {
        playerObjectId,
        recipeId,
        mp,
        maxMp
    }
}