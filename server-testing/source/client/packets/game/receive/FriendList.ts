import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface FriendData extends PacketEvent {
    playerId: number
    name: string
    isOnline: boolean
}
export interface FriendListEvent extends PacketEvent {
    friends: Array<FriendData>
}
export function FriendList( packet : ReadableClientPacket ) : FriendListEvent {
    let size = packet.readD()

    return {
        friends: _.times( size, () : FriendData => {
            let playerId = packet.readD(),
                    name = packet.readS(),
                    isOnline = packet.readD() === 1

            packet.skipD( 1 )

            return {
                playerId,
                name,
                isOnline
            }
        } )
    }
}