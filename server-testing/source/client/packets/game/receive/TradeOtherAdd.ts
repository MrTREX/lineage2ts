import { PacketEvent } from '../../PacketMethodTypes'
import { ReadableClientPacket } from '../../ReadableClientPacket'

export interface TradeOtherAddEvent extends PacketEvent {
    itemObjectId: number
    itemId: number
    amount: number
}

export function TradeOtherAdd( packet : ReadableClientPacket ) : TradeOtherAddEvent {
    packet.skipD( 1 )

    let itemObjectId = packet.readD(),
            itemId = packet.readD(),
            amount = packet.readQ()

    return {
        amount,
        itemId,
        itemObjectId
    }
}