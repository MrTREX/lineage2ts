import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { CubeGameSide } from '../../../enums/CubeGameSide'

export interface CubeGameChangeTeamEvent extends PacketEvent {
    playerObjectId: number
    side: CubeGameSide
    isPlayerRemoved: boolean
}
export function CubeGameChangeTeam( packet : ReadableClientPacket ) : CubeGameChangeTeamEvent {
    /*
        Operation status:
        0x05 - player changes sides
        0x02 - player is removed
     */
    let isPlayerRemoved = packet.readD() === 0x02

    let playerObjectId = packet.readD()
    let side = CubeGameSide.Red

    if ( packet.readD() === 0 ) {
        side = CubeGameSide.Blue
    }

    if ( isPlayerRemoved ) {
        playerObjectId = packet.readD()
    }

    return {
        playerObjectId,
        side,
        isPlayerRemoved
    }
}