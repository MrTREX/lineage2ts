import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface PledgeMemberDeleteEvent extends PacketEvent {
    name: string
}

export function PledgeMemberDelete( packet: ReadableClientPacket ) : PledgeMemberDeleteEvent {
    return {
        name: packet.readS()
    }
}