import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface AskCoupleActionEvent extends PacketEvent {
    playerObjectId : number
    actionId: number
}

export function AskCoupleAction( packet: ReadableClientPacket ) : AskCoupleActionEvent {
    let playerObjectId = packet.readD(),
            actionId = packet.readD()

    return {
        playerObjectId,
        actionId
    }
}