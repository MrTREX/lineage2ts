import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface AbnormalStatusEffect extends PacketEvent {
    skillId: number
    skillLevel: number
    remainingSeconds: number
}
export interface AbnormalStatusUpdateEvent extends PacketEvent {
    effects: Array<AbnormalStatusEffect>
    lastUpdate: number
}
export function AbnormalStatusUpdate( packet: ReadableClientPacket ) : AbnormalStatusUpdateEvent {
    let size = packet.readH()

    return {
        effects: _.times( size, () : AbnormalStatusEffect => {
            let skillId = packet.readD(),
                skillLevel = packet.readH(),
                remainingSeconds = packet.readD()
            return {
                skillId,
                skillLevel,
                remainingSeconds
            }
        } ),
        lastUpdate: Date.now()
    }
}