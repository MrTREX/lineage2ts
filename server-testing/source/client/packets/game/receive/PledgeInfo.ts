import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface PledgeInfoEvent extends PacketEvent {
    clanId: number
    clanName: string
    allyName: string
}
export function PledgeInfo( packet : ReadableClientPacket ) : PledgeInfoEvent {
    let clanId = packet.readD(),
            clanName = packet.readS(),
            allyName = packet.readS()

    return {
        clanId,
        clanName,
        allyName
    }
}