import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface ShowCastleData {
    residenceId: number
    clanName: string
    taxPercent: number
    siegeDate: number
}
export interface ShowCastleInfoEvent extends PacketEvent {
    castles: Array<ShowCastleData>
}

export function ShowCastleInfo( packet: ReadableClientPacket ) : ShowCastleInfoEvent {
    let size = packet.readD()

    return {
        castles: _.times( size, () : ShowCastleData => {
            let residenceId = packet.readD(),
                    clanName = packet.readS(),
                    taxPercent = packet.readD(),
                    siegeDate = packet.readD() * 1000

            return {
                residenceId,
                clanName,
                taxPercent,
                siegeDate
            }
        } )
    }
}