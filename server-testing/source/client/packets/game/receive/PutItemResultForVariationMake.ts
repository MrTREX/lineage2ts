import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface PutItemResultForVariationMakeEvent extends PacketEvent {
    itemObjectId: number
    itemId: number
}
export function PutItemResultForVariationMake( packet: ReadableClientPacket ) : PutItemResultForVariationMakeEvent {
    let itemObjectId = packet.readD(),
            itemId = packet.readD()

    return {
        itemObjectId,
        itemId
    }
}