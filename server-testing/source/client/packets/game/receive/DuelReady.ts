import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface DuelReadyEvent extends PacketEvent {
    isPartyDuel: boolean
}
export function DuelReady( packet: ReadableClientPacket ) : DuelReadyEvent {
    return {
        isPartyDuel: packet.readD() === 1
    }
}