import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface PutCommissionResultForVariationMakeEvent extends PacketEvent {
    itemObjectId: number
    amount: number
    itemId: number
}
export function PutCommissionResultForVariationMake( packet: ReadableClientPacket ) : PutCommissionResultForVariationMakeEvent {
    let itemObjectId = packet.readD(),
            itemId = packet.readD(),
            amount = packet.readQ()

    return {
        itemId,
        itemObjectId,
        amount
    }
}