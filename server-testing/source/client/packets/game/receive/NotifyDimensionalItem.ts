import { PacketEvent } from '../../PacketMethodTypes'

export interface NotifyDimensionalItemEvent extends PacketEvent {}
export function NotifyDimensionalItem() : NotifyDimensionalItemEvent {
    return {}
}