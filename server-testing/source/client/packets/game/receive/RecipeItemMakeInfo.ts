import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface RecipeItemMakeInfoEvent extends PacketEvent {
    recipeId: number
    isDwarven: boolean
    currentMp: number
    maxMp: number
    isSuccess: boolean
}
export function RecipeItemMakeInfo( packet: ReadableClientPacket ) : RecipeItemMakeInfoEvent {
    let recipeId = packet.readD(),
            isDwarven = packet.readD() === 0,
            currentMp = packet.readD(),
            maxMp = packet.readD(),
            isSuccess = packet.readD() === 1

    return {
        recipeId,
        isDwarven,
        currentMp,
        maxMp,
        isSuccess
    }
}