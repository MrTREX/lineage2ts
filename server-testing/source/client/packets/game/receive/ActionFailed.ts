import { PacketEvent } from '../../PacketMethodTypes'

/*
    Nothing to read here, however we need to provide packet method
    in order to have listeners act on this packet.
 */
export interface ActionFailedEvent extends PacketEvent {}

export function ActionFailed(): ActionFailedEvent {
    return {}
}