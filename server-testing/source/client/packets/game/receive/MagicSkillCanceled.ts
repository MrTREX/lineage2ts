import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface MagicSkillCanceledEvent extends PacketEvent {
    objectId: number
}

export function MagicSkillCanceled( packet: ReadableClientPacket ) : MagicSkillCanceledEvent {
    return {
        objectId: packet.readD()
    }
}