import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface CharacterCreationSuccessEvent extends PacketEvent {
    isCharacterCreated: boolean
}
export function CharacterCreationSuccess( packet : ReadableClientPacket ) : CharacterCreationSuccessEvent {
    return {
        isCharacterCreated: packet.readD() === 0x01
    }
}