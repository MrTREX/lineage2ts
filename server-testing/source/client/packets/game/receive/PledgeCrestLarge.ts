import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface PledgeCrestLargeEvent extends PacketEvent {
    crestId: number
    crestData: Buffer
}
export function PledgeCrestLarge( packet: ReadableClientPacket ) : PledgeCrestLargeEvent {
    packet.skipD( 1 )

    let crestId = packet.readD(),
            size = packet.readD()

    return {
        crestId,
        crestData: size > 0 ? packet.readB( size ) : null
    }
}