import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface EnchantSkillInfoEvent extends PacketEvent {
    skillId: number
    skillLevel: number
    isEnchantPossible: boolean
    isEnchanted: boolean
    routeLevels: Array<number>
}
export function EnchantSkillInfo( packet: ReadableClientPacket ) : EnchantSkillInfoEvent {
    let skillId = packet.readD(),
            skillLevel = packet.readD(),
            isEnchantPossible = packet.readD() === 1,
            isEnchanted = packet.readD() === 1,
            size = packet.readD()

    return {
        skillId,
        skillLevel,
        isEnchantPossible,
        isEnchanted,
        routeLevels: _.times( size, () : number => packet.readD() )
    }
}