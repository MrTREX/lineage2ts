import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface VitalityPointInfoEvent extends PacketEvent {
    points: number
}

export function VitalityPointInfo( packet: ReadableClientPacket ) : VitalityPointInfoEvent {
    return {
        points: packet.readD()
    }
}