import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface MusicEvent extends PacketEvent {
    name: string
    delay: number
}

export function Music( packet: ReadableClientPacket ) : MusicEvent {
    packet.skipD( 1 )

    let name = packet.readS()

    packet.skipD( 5 )

    let delay = packet.readD()

    return {
        name,
        delay
    }
}