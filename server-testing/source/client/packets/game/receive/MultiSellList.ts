import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface MultiSellListProduct {
    itemId: number
    amount: number
    enchantLevel: number
}

export interface MultiSellListItem {
    entryId: number
    isStackable: boolean
    products: Array<MultiSellListProduct>
    ingredients: Array<MultiSellListProduct>
}
export interface MultiSellListEvent extends PacketEvent {
    items: Array<MultiSellListItem>
    id: number
    pageNumber: number
    isLastPage: boolean
    pageSize: number
}

function readProducts( packet: ReadableClientPacket, size: number ) : Array<MultiSellListProduct> {
    return _.times( size, () : MultiSellListProduct => {
        let itemId = packet.readD()

        packet.skipB( 6 )

        let amount = packet.readQ(),
                enchantLevel = packet.readH()

        return {
            itemId,
            amount,
            enchantLevel
        }
    } )
}

function readIngredients( packet: ReadableClientPacket, size: number ) : Array<MultiSellListProduct> {
    return _.times( size, () : MultiSellListProduct => {
        let itemId = packet.readD()

        packet.skipH( 1 )

        let amount = packet.readQ(),
                enchantLevel = packet.readH()

        return {
            itemId,
            amount,
            enchantLevel
        }
    } )
}

export function MultiSellList( packet: ReadableClientPacket ) : MultiSellListEvent {
    let id = packet.readD(),
            pageNumber = packet.readD(),
            isLastPage = packet.readD() === 1,
            pageSize = packet.readD(),
            size = packet.readD()

    return {
        id,
        pageNumber,
        isLastPage,
        pageSize,
        items: _.times( size, () : MultiSellListItem => {
            let entryId = packet.readD(),
                    isStackable = packet.readC() === 1

            packet.skipB( 26 )

            let productSize = packet.readH(),
                    ingredientSize = packet.readH(),
                    products = readProducts( packet, productSize ),
                    ingredients = readIngredients( packet, ingredientSize )

            return {
                entryId,
                isStackable,
                products,
                ingredients
            }
        } )
    }
}