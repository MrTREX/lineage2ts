import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface ShowCalculatorEvent extends PacketEvent {
    id: number
}

export function ShowCalculator( packet: ReadableClientPacket ) : ShowCalculatorEvent {
    return {
        id: packet.readD()
    }
}