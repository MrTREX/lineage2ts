import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface RedSkyEvent extends PacketEvent {
    duration: number
}
export function RedSky( packet: ReadableClientPacket ) : RedSkyEvent {
    return {
        duration: packet.readD()
    }
}