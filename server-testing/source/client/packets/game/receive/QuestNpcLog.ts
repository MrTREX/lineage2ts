import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface QuestNpcItem {
    npcId: number
    value: number
}

export interface QuestNpcLogEvent extends PacketEvent {
    questId: number
    items: Array<QuestNpcItem>
}

export function QuestNpcLog( packet: ReadableClientPacket ) : QuestNpcLogEvent {
    let questId = packet.readD(),
            size = packet.readC()

    return {
        questId,
        items: _.times( size, () : QuestNpcItem => {
            let npcId = packet.readD() - 1000000

            packet.skipD( 1 )

            return {
                npcId,
                value: packet.readD()
            }
        } )
    }
}