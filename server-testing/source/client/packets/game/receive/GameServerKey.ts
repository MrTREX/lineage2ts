import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface GameServerKeyEvent extends PacketEvent {
    key: Buffer
}
export function GameServerKey( packet : ReadableClientPacket ) : GameServerKeyEvent {
    let isSuccess = packet.readC() === 0x01

    if ( !isSuccess ) {
        return {
            key: null
        }
    }

    let result = packet.readB( 8 )
    let key = Buffer.allocUnsafe( 16 )

    result.copy( key )
    Buffer.from( [ 0xc8, 0x27, 0x93, 0x01, 0xa1, 0x6c, 0x31, 0x97 ] ).copy( key, 8 )

    return {
        key
    }
}