import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface PackageDestinationCharacter {
    objectId: number
    name: string
}

export interface PackageDestinationListEvent extends PacketEvent {
    characters: Array<PackageDestinationCharacter>
}

export function PackageDestinationList( packet: ReadableClientPacket ) : PackageDestinationListEvent {
    let size = packet.readD()

    return {
        characters: _.times( size, () : PackageDestinationCharacter => {
            let objectId = packet.readD(),
                    name = packet.readS()

            return {
                objectId,
                name
            }
        } )
    }
}