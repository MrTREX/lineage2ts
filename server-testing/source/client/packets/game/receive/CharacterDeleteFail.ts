import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export enum CharacterDeleteReason {
    GeneralFailure = 0x01,
    MayNotDeleteClanMember = 0x02,
    ClanLeaderMayNotBeDeleted = 0x03,
}

export interface CharacterDeleteFailEvent extends PacketEvent {
    reason: CharacterDeleteReason
}

export function CharacterDeleteFail( packet: ReadableClientPacket ) : CharacterDeleteFailEvent {
    return {
        reason: packet.readD()
    }
}