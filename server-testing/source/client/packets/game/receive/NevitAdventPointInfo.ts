import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface NevitAdventPointInfoEvent extends PacketEvent {
    points: number
}
export function NevitAdventPointInfo( packet : ReadableClientPacket ) : NevitAdventPointInfoEvent {
    return {
        points: packet.readD()
    }
}