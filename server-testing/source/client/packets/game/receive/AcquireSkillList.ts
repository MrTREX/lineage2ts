import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { AcquireSkillType } from '../../../enums/AcquireSkillType'
import _ from 'lodash'

export interface AcquireSkillData {
    id: number
    nextLevel: number
    maxLevel: number
    spCost: number
    classId: number
}

export interface AcquireSkillListEvent extends PacketEvent {
    type: AcquireSkillType
    skills: Array<AcquireSkillData>
}

export function AcquireSkillList( packet: ReadableClientPacket ) : AcquireSkillListEvent {
    let type = packet.readD(),
            size = packet.readD()

    return {
        type,
        skills: _.times( size, () : AcquireSkillData => {
            let id = packet.readD(),
                    nextLevel = packet.readD(),
                    maxLevel = packet.readD(),
                    spCost = packet.readD(),
                    classId = packet.readD()

            if ( type === AcquireSkillType.Subpledge ) {
                packet.skipD( 1 )
            }

            return {
                id,
                nextLevel,
                maxLevel,
                spCost,
                classId
            }
        } )
    }
}