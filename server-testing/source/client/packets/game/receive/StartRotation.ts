import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface StartRotationEvent extends PacketEvent {
    characterObjectId: number
    heading: number
    isClockwise: boolean
    speed: number
}
export function StartRotation( packet: ReadableClientPacket ) : StartRotationEvent {
    let characterObjectId = packet.readD(),
            heading = packet.readD(),
            isClockwise = packet.readD() === 1,
            speed = packet.readD()

    return {
        characterObjectId,
        heading,
        isClockwise,
        speed
    }
}