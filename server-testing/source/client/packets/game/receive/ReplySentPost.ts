import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'
import { readNormalItem } from '../helpers/ItemUpdateHelper'

export interface ReplySentPostEvent extends PacketEvent {
    messageId: number
    isLocked: boolean
    receiverName: string
    messageTitle: string
    messageContent: string
    itemObjectIds: Array<number>
    requestedAdena: number
}
export function ReplySentPost( packet: ReadableClientPacket ) : ReplySentPostEvent {
    let messageId = packet.readD(),
            isLocked = packet.readD() === 1

    packet.skipD( 1 )

    let receiverName = packet.readS(),
            messageTitle = packet.readS(),
            messageContent = packet.readS(),
            size = packet.readD()

    let itemObjectIds = _.times( size, () : number => {
        let item = readNormalItem( packet )
        packet.skipD( 1 )

        return item.objectId
    } )

    let requestedAdena = packet.readQ()

    return {
        messageId,
        isLocked,
        receiverName,
        messageTitle,
        messageContent,
        requestedAdena,
        itemObjectIds
    }
}