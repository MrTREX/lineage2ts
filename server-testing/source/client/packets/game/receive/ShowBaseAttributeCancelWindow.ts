import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface ShowBaseAttributeCancelData {
    itemObjectId: number
    price: number
}
export interface ShowBaseAttributeCancelWindowEvent extends PacketEvent {
    cancelItems: Array<ShowBaseAttributeCancelData>
}
export function ShowBaseAttributeCancelWindow( packet: ReadableClientPacket ) : ShowBaseAttributeCancelWindowEvent {
    let size = packet.readD()

    return {
        cancelItems: _.times( size, () : ShowBaseAttributeCancelData => {
            let itemObjectId = packet.readD(),
                    price = packet.readQ()

            return {
                itemObjectId,
                price
            }
        } )
    }
}