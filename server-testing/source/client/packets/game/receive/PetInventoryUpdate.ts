import { PacketEvent } from '../../PacketMethodTypes'
import { ReadableClientPacket } from '../../ReadableClientPacket'
import _ from 'lodash'
import { readNormalItem } from '../helpers/ItemUpdateHelper'

export interface PetInventoryUpdateEvent extends PacketEvent {
    addedObjectIds: Array<number>
    modifiedObjectIds: Array<number>
    removedObjectIds : Array<number>
}

const enum UpdateType {
    None,
    Add,
    Modify,
    Remove
}

export function PetInventoryUpdate( packet: ReadableClientPacket ) : PetInventoryUpdateEvent {
    let event : PetInventoryUpdateEvent = {
        addedObjectIds: [],
        modifiedObjectIds: [],
        removedObjectIds: []
    }

    let amount = packet.readH()

    _.times( amount, () => {
        let type : UpdateType = packet.readH()
        let id = readNormalItem( packet ).objectId

        switch ( type ) {
            case UpdateType.Add:
                event.addedObjectIds.push( id )
                break

            case UpdateType.Modify:
                event.modifiedObjectIds.push( id )
                break

            case UpdateType.Remove:
                event.removedObjectIds.push( id )
                break
        }
    } )

    return event
}