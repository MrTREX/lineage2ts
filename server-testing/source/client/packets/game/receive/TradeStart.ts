import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'
import { readNormalItem } from '../helpers/ItemUpdateHelper'
import { ItemState } from '../../../enums/ItemState'

export interface TradeStartEvent extends PacketEvent {
    partnerObjectId: number
    itemObjectIds : Array<number>
}
export function TradeStart( packet : ReadableClientPacket ) : TradeStartEvent {
    let partnerObjectId = packet.readD(),
            amount = packet.readH()

    return {
        partnerObjectId,
        itemObjectIds: _.times( amount, () : number => {
            return readNormalItem( packet, ItemState.PickedUp, partnerObjectId ).objectId
        } )
    }
}