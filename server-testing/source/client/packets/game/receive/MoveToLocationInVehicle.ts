import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { LocationProperties } from '../../../models/LocationProperties'
import { readLocation } from '../helpers/PropertyHelper'

export interface MoveToLocationInVehicleEvent extends PacketEvent {
    playerObjectId: number
    boatObjectId: number
    destination: LocationProperties
    origin: LocationProperties
}

export function MoveToLocationInVehicle( packet: ReadableClientPacket ) : MoveToLocationInVehicleEvent {
    let playerObjectId = packet.readD(),
            boatObjectId = packet.readD(),
            destination = readLocation( packet ),
            origin = readLocation( packet )

    return {
        playerObjectId,
        boatObjectId,
        destination,
        origin
    }
}