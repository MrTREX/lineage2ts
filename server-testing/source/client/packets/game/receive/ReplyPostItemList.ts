import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { readNormalItem } from '../helpers/ItemUpdateHelper'
import _ from 'lodash'

export interface ReplyPostItemListEvent extends PacketEvent {
    itemObjectIds: Array<number>
}
export function ReplyPostItemList( packet: ReadableClientPacket ) : ReplyPostItemListEvent {
    let size = packet.readD()

    return {
        itemObjectIds: _.times( size, () : number => {
            return readNormalItem( packet ).objectId
        } )
    }
}