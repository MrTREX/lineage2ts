import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { getCharacterForUpdate } from '../helpers/CharacterUpdateHelper'

export const enum FlyToLocationType {
    ThrowUpward,
    ThrowHorizontally,
    Dummy,
    Charge
}

export interface FlyToLocationCoordinatesEvent extends PacketEvent {
    objectId: number
    type: FlyToLocationType
}

export function FlyToLocationCoordinates( packet: ReadableClientPacket ) : FlyToLocationCoordinatesEvent {
    let objectId = packet.readD()

    let character = getCharacterForUpdate( objectId )

    character.destinationX = packet.readD()
    character.destinationY = packet.readD()
    character.destinationZ = packet.readD()

    character.x = packet.readD()
    character.y = packet.readD()
    character.z = packet.readD()

    let type = packet.readD()

    return {
        objectId,
        type
    }
}