import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { Race } from '../../../enums/Race'
import _ from 'lodash'

export interface PledgeMemberData {
    name: string
    level: number
    classId: number
    isOnline: boolean
    playerObjectId: number
    race: Race
    hasSponsor: boolean
}

export interface PledgeMemberListEvent extends PacketEvent {
    clanId: number
    clanName: string
    clanLeaderName: string
    crestId: number
    level: number
    castleId: number
    hideoutId: number
    fortId: number
    rank: number
    reputation: number
    allyId: number
    allyName: string
    allyCrestId: number
    isAtWar: boolean
    pledgeType: number // TODO : add enum
    members: Array<PledgeMemberData>
}

export function PledgeMemberList( packet: ReadableClientPacket ) : PledgeMemberListEvent {
    packet.skipD( 1 )

    let clanId = packet.readD(),
            pledgeType = packet.readD(),
            clanName = packet.readS(),
            clanLeaderName = packet.readS(),
            crestId = packet.readD(),
            level = packet.readD(),
            castleId = packet.readD(),
            hideoutId = packet.readD(),
            fortId = packet.readD(),
            rank = packet.readD(),
            reputation = packet.readD()

    packet.skipD( 2 )

    let allyId = packet.readD(),
            allyName = packet.readS(),
            allyCrestId = packet.readD(),
            isAtWar = packet.readD() === 1

    packet.skipD( 1 )

    let size = packet.readD()

    return {
        clanId,
        pledgeType,
        clanName,
        clanLeaderName,
        crestId,
        level,
        castleId,
        hideoutId,
        fortId,
        rank,
        reputation,
        allyId,
        allyName,
        allyCrestId,
        isAtWar,
        members: _.times( size, () : PledgeMemberData => {
            let name = packet.readS(),
                    level = packet.readD(),
                    classId = packet.readD()

            packet.skipD( 1 )

            let race = packet.readD(),
                    playerObjectId = packet.readD(),
                    hasSponsor = packet.readD() === 1

            return {
                name,
                level,
                classId,
                race,
                playerObjectId,
                hasSponsor,
                isOnline: playerObjectId !== 0
            }
        } )
    }
}