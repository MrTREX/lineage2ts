import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { PlayerPointProperties } from '../../../models/PointProperties'

export interface DuelUpdateUserInfoEvent extends PacketEvent, PlayerPointProperties {
    playerObjectId: number
}

export function DuelUpdateUserInfo( packet: ReadableClientPacket ): DuelUpdateUserInfoEvent {
    packet.skipS()

    /*
        Keep in mind, we cannot update player data due to duel state,
        hence we must return all values as part of packet event.
     */
    let playerObjectId = packet.readD(),
            classId = packet.readD(),
            level = packet.readD(),
            hp = packet.readD(),
            maxHp = packet.readD(),
            mp = packet.readD(),
            maxMp = packet.readD(),
            cp = packet.readD(),
            maxCp = packet.readD()

    return {
        playerObjectId,
        classId,
        level,
        hp,
        maxHp,
        mp,
        maxMp,
        cp,
        maxCp
    }
}