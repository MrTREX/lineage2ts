import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface TutorialClientActionEvent extends PacketEvent {
    id: number
}
export function TutorialClientAction( packet: ReadableClientPacket ) : TutorialClientActionEvent {
    return {
        id: packet.readD()
    }
}