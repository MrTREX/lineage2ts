import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { L2World } from '../../../L2World'
import { L2Player } from '../../../models/L2Player'
import { L2ObjectType } from '../../../enums/L2ObjectType'

export interface TargetUnselectedWithPositionEvent extends PacketEvent {
    playerObjectId: number
}
export function TargetUnselectedWithPosition( packet : ReadableClientPacket ) : TargetUnselectedWithPositionEvent {
    let playerObjectId = packet.readD()

    let existingCharacter = L2World.getObject( playerObjectId ) as L2Player
    if ( existingCharacter && existingCharacter.type === L2ObjectType.Player ) {
        existingCharacter.x = packet.readD()
        existingCharacter.y = packet.readD()
        existingCharacter.z = packet.readD()
        existingCharacter.targetId = 0
    }

    return {
        playerObjectId
    }
}