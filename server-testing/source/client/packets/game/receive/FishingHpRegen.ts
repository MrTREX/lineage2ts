import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export const enum FishingGameStatus {
    None,
    Success,
    Failure
}

export const enum FishingGameAction {
    None,
    Reeling,
    Pumping
}

export interface FishingHpRegenEvent extends PacketEvent {
    playerObjectId: number
    time: number
    fishHp: number
    isHpRising: boolean
    status: FishingGameStatus
    action: FishingGameAction
    penalty: number
    isDeceptive: boolean
}
export function FishingHpRegen( packet : ReadableClientPacket ) : FishingHpRegenEvent {
    let playerObjectId = packet.readD(),
            time = packet.readD(),
            fishHp = packet.readD(),
            isHpRising = packet.readC() === 1,
            status = packet.readC(),
            action = packet.readC(),
            penalty = packet.readD(),
            isDeceptive = packet.readC() === 1

    return {
        playerObjectId,
        time,
        fishHp,
        isHpRising,
        status,
        action,
        penalty,
        isDeceptive
    }
}