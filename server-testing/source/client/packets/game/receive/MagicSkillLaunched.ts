import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface MagicSkillLaunchedEvent extends PacketEvent {
    originatorObjectId: number
    targetObjectIds: Array<number>
    skillId: number
    skillLevel: number
}

export function MagicSkillLaunched( packet: ReadableClientPacket ): MagicSkillLaunchedEvent {
    let originatorObjectId = packet.readD(),
            skillId = packet.readD(),
            skillLevel = packet.readD(),
            size = packet.readD()

    return {
        originatorObjectId,
        skillId,
        skillLevel,
        targetObjectIds: _.times( size, () => packet.readD() ),
    }
}