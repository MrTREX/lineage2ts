import { ReadableClientPacket } from '../../ReadableClientPacket'
import { LocationProperties } from '../../../models/LocationProperties'

export function readLocation( packet : ReadableClientPacket ) : LocationProperties {
    let x = packet.readD(),
            y = packet.readD(),
            z = packet.readD()

    return {
        x,
        y,
        z
    }
}