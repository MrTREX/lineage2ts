import { DeclaredServerPacket } from '../../DeclaredServerPacket'

export function Logout() : Buffer {
    return new DeclaredServerPacket( 1 )
            .writeC( 0x00 )
            .getBuffer()
}