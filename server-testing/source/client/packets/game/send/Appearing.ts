import { DeclaredServerPacket } from '../../DeclaredServerPacket'

export function Appearing() : Buffer {
    return new DeclaredServerPacket( 1 )
            .writeC( 0x3a )
            .getBuffer()
}