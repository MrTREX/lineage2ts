import { DeclaredServerPacket, getStringSize } from '../../DeclaredServerPacket'

export function BypassCommand( command: string ) : Buffer {
    return new DeclaredServerPacket( 1 + getStringSize( command ) )
        .writeC( 0x74 )
        .writeS( command )
        .getBuffer()
}