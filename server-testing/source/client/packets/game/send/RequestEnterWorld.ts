import { DeclaredServerPacket } from '../../DeclaredServerPacket'

const longSection = Buffer.allocUnsafeSlow( 104 ).fill( 0 )

export function RequestEnterWorld(): Buffer {
    return new DeclaredServerPacket( 105 )
        .writeC( 0x11 )
        .writeB( longSection )
        .getBuffer()
}