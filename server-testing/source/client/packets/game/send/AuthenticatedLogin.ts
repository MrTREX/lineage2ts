import { DeclaredServerPacket, getStringSize } from '../../DeclaredServerPacket'
import { preserveData } from '../../ReadableClientPacket'
import { L2GameAuthentication } from '../../../L2GameClient'

const staticValues : Buffer = preserveData( Buffer.from( [ 0x3c, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 ] ) )
export function AuthenticatedLogin( data : L2GameAuthentication ) : Buffer {
    return new DeclaredServerPacket( 21 + getStringSize( data.userName ) + staticValues.length )
            .writeC( 0x2b )
            .writeS( data.userName )
            .writeD( data.playTwo )
            .writeD( data.playOne )

            .writeD( data.loginOne )
            .writeD( data.loginTwo )
            .writeD( 1 )
            .writeB( staticValues )
            .getBuffer()
}