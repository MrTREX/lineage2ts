import { DeclaredServerPacket } from '../../DeclaredServerPacket'

export function RequestUseItem( objectId: number, isCtrlPressed: boolean = false ) : Buffer {
    return new DeclaredServerPacket( 9 )
        .writeC( 0x19 )
        .writeD( objectId )
        .writeD( isCtrlPressed ? 1 : 0 )
        .getBuffer()
}