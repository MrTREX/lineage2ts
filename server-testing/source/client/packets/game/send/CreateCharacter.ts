import { Race } from '../../../enums/Race'
import { PlayerSex } from '../../../enums/PlayerSex'
import { PlayerHairColor, PlayerHairStyle } from '../../../enums/PlayerHair'
import { DeclaredServerPacket, getStringSize } from '../../DeclaredServerPacket'
import { PlayerFace } from '../../../enums/PlayerFace'

export interface CreateCharacterData {
    name: string,
    race: Race,
    sex: PlayerSex,
    classId: number,
    hairStyle: PlayerHairStyle,
    hairColor: PlayerHairColor,
    face: PlayerFace
}

export function CreateCharacter( data: CreateCharacterData ) : Buffer {
    return new DeclaredServerPacket( getStringSize( data.name ) + 49 )
            .writeC( 0x0c )
            .writeS( data.name )
            .writeD( data.race )
            .writeD( data.sex )

            .writeD( data.classId )
            .writeD( 0 )
            .writeD( 0 )
            .writeD( 0 )

            .writeD( 0 )
            .writeD( 0 )
            .writeD( 0 )
            .writeD( data.hairStyle )

            .writeD( data.hairColor )
            .writeD( data.face )
            .getBuffer()
}