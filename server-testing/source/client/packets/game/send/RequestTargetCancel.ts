import { DeclaredServerPacket } from '../../DeclaredServerPacket'

export function RequestTargetCancel() : Buffer {
    return new DeclaredServerPacket( 3 )
        .writeC( 0x48 )
        .writeH( 1 )
        .getBuffer()
}