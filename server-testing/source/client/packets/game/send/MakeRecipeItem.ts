import { DeclaredServerPacket } from '../../DeclaredServerPacket'

export function MakeRecipeItem( recipeId: number ) : Buffer {
    return new DeclaredServerPacket( 5 )
        .writeC( 0xb8 )
        .writeD( recipeId )
        .getBuffer()
}