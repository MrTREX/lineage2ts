import { DeclaredServerPacket } from '../../DeclaredServerPacket'

export const enum TargetActionType {
    TargetThenInteract,
    UseShiftKey,
}
export function TargetAction( objectId: number, action: TargetActionType ) : Buffer {
    return new DeclaredServerPacket( 18 )
        .writeC( 0x1f )
        .writeD( objectId )
        .writeD( 0 )
        .writeD( 0 )

        .writeD( 0 )
        .writeC( action )
        .getBuffer()
}