import { DeclaredServerPacket } from '../../DeclaredServerPacket'

export function RequestRemoveBuff( objectId: number, skillId: number, skillLevel: number ) : Buffer {
    return new DeclaredServerPacket( 15 )
        .writeC( 0xd0 )
        .writeH( 0x4b )
        .writeD( objectId )
        .writeD( skillId )

        .writeD( skillLevel )
        .getBuffer()
}