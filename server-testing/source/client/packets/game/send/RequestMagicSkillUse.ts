import { DeclaredServerPacket } from '../../DeclaredServerPacket'

export function RequestMagicSkillUse( skillId: number, isCtrlPressed: boolean = false, isShiftPressed : boolean = false ) : Buffer {
    return new DeclaredServerPacket( 10 )
        .writeC( 0x39 )
        .writeD( skillId )
        .writeD( isCtrlPressed ? 1 : 0 )
        .writeC( isShiftPressed ? 1 : 0 )

        .getBuffer()
}