import { DeclaredServerPacket } from '../../DeclaredServerPacket'
import * as forge from 'node-forge'

const BigInteger = forge.jsbn.BigInteger

export function RequestAuthenticatedLogin( userName: string, password: string, sessionId: number, publicKey: Buffer ) : Buffer {
    let modulus = new BigInteger( publicKey.toString( 'hex' ), 16 )
    const loginInfo = Buffer.allocUnsafe( 128 ).fill( 0 )
    loginInfo[ 0x5b ] = 0x24

    loginInfo.write( userName, 0x5e )
    loginInfo.write( password, 0x6c )

    let credentials = new BigInteger( loginInfo.toString( 'hex' ), 16 )
    let byteData = credentials.modPowInt( 65537, modulus ).toByteArray()
    let data = Buffer.from( byteData.length === 129 ? byteData.slice( 1 ) : byteData )

    return new DeclaredServerPacket( 133 )
            .writeC( 0x00 )
            .writeB( data )
            .writeD( sessionId )
            .getBuffer()
}