import { DeclaredServerPacket } from '../../DeclaredServerPacket'

export function RequestServerLogin( one : number, two: number, serverId: number ) : Buffer {
    return new DeclaredServerPacket( 32 )
            .writeC( 0x02 )
            .writeD( one )
            .writeD( two )
            .writeC( serverId )
            .writeB( Buffer.allocUnsafe( 22 ).fill( 0 ) )
            .getBuffer()
}