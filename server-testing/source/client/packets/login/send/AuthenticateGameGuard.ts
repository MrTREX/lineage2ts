import { DeclaredServerPacket } from '../../DeclaredServerPacket'

export function AuthenticateGameGuard( sessionId: number ) : Buffer {
    return new DeclaredServerPacket( 40 )
            .writeC( 0x07 )
            .writeD( sessionId )
            .writeD( 0x00000123 )
            .writeD( 0x00004567 )

            .writeD( 0x000089ab )
            .writeD( 0x0000cdef )
            .getBuffer()
}