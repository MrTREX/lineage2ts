import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface LoginInitialConfigurationEvent extends PacketEvent {
    sessionId: number
    protocolVersion: number
    publicKey: Buffer
    blowfishKey: Buffer
    unDecodedPublicKey: Buffer
}

function decodeModulus( data: Buffer ): Buffer {
    for ( let index = 0; index < 0x40; index++ ) {
        data[ 0x40 + index ] = data[ 0x40 + index ] ^ data[ index ]
    }

    for ( let index = 0; index < 4; index++ ) {
        data[ 0x0d + index ] = data[ 0x0d + index ] ^ data[ 0x34 + index ]
    }

    for ( let index = 0; index < 0x40; index++ ) {
        data[ index ] = data[ index ] ^ data[ 0x40 + index ]
    }

    for ( let index = 0; index < 4; index++ ) {
        const value: number = data[ index ]
        data[ index ] = data[ 0x4d + index ]
        data[ 0x4d + index ] = value
    }

    return data
}

export function LoginInitialConfiguration( packet: ReadableClientPacket ): LoginInitialConfigurationEvent {
    let sessionId = packet.readD()
    let protocolVersion = packet.readD()
    let rawKey = packet.readB( 128 )
    let unDecodedPublicKey = Buffer.from( rawKey )

    packet.skipD( 4 )

    let blowfishKey = packet.readB( 16 )

    return {
        blowfishKey,
        protocolVersion,
        publicKey: decodeModulus( rawKey ),
        unDecodedPublicKey,
        sessionId,
    }
}