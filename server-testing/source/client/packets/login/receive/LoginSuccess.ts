import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface LoginSuccessEvent extends PacketEvent {
    one: number
    two: number
}

export function generateLoginSuccess() : LoginSuccessEvent {
    return {
        one: Math.random() * Math.pow( 2, 16 ),
        two: Math.random() * Math.pow( 2, 16 )
    }
}

export function LoginSuccess( packet: ReadableClientPacket ): LoginSuccessEvent {
    const one = packet.readD(), two = packet.readD()

    return {
        one,
        two,
    }
}