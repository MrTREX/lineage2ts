import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface LoginGameGuardSuccessEvent extends PacketEvent {
    sessionId: number
}

export function LoginGameGuardSuccess( packet: ReadableClientPacket ) : LoginGameGuardSuccessEvent {
    return {
        sessionId: packet.readD()
    }
}