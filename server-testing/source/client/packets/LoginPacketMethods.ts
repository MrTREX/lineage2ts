import { PacketMethodMap } from './PacketMethodTypes'
import { LoginInitialConfiguration } from './login/receive/InitialConfiguration'
import { LoginFail } from './login/receive/LoginFail'
import { LoginAccountKicked } from './login/receive/AccountKicked'
import { LoginSuccess } from './login/receive/LoginSuccess'
import { LoginServerList } from './login/receive/ServerList'
import { ServerLoginDenied } from './login/receive/ServerLoginDenied'
import { ServerLoginApproved } from './login/receive/ServerLoginApproved'
import { LoginGameGuardSuccess } from './login/receive/GameGuardSuccess'

export const LoginPacketMethods : PacketMethodMap = {
    0x00: LoginInitialConfiguration,
    0x01: LoginFail,
    0x02: LoginAccountKicked,
    0x03: LoginSuccess,
    0x04: LoginServerList,
    0x06: ServerLoginDenied,
    0x07: ServerLoginApproved,
    0x0b: LoginGameGuardSuccess
}