import { EncryptionEngine } from './EncryptionEngine'
import { EncryptionOperations } from './EncryptionOperations'

const staticBlowfishKey: Array<number> = [
    0x6b,
    0x60,
    0xcb,
    0x5b,
    0x82,
    0xce,
    0x90,
    0xb1,
    0xcc,
    0x2b,
    0x6c,
    0x55,
    0x6c,
    0x6c,
    0x6c,
    0x6c,
]

export class LoginEncryption implements EncryptionOperations {
    isFirstTime: boolean = true
    encryption: EncryptionEngine = new EncryptionEngine( Buffer.from( staticBlowfishKey ) )

    decrypt( encryptedData: Buffer ) : Buffer {

        if ( ( encryptedData.length % 8 ) !== 0 ) {
            throw new Error( 'size have to be multiple of 8' )
        }

        let decryptedData: Buffer = this.encryption.decrypt( encryptedData )

        if ( this.isFirstTime ) {
            this.isFirstTime = false

            return this.encryption.decryptXORPass( decryptedData )
        }

        if ( !this.encryption.verifyChecksum( decryptedData ) ) {
            throw new Error( 'Cannot verify checksum' )
        }

        return decryptedData
    }

    encrypt( data: Buffer ) : Buffer {
        this.encryptData( data.subarray( 2 ) )
        return data
    }

    setKey( data: Buffer ): void {
        this.encryption.cipher.setKey( data )
    }

    private encryptData( data: Buffer ) : void {
        return this.encryption.encrypt( this.encryption.appendChecksum( data ) )
    }
}
