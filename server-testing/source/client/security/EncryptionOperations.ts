export interface EncryptionOperations {
    decrypt( encryptedData: Buffer ) : Buffer
    encrypt( data: Buffer ) : Buffer
    setKey( data: Buffer ) : void
}