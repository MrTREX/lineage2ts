import { BlowfishEngine } from './blowfishEngine'

const enum EngineValues {
    blockSize = 8
}

export class EncryptionEngine {
    cipher: BlowfishEngine

    constructor( blowfishKey: Buffer ) {
        this.cipher = new BlowfishEngine( blowfishKey )
    }

    verifyChecksum( dataBuffer: Buffer ): boolean {
        if ( ( dataBuffer.length & 3 ) !== 0 || dataBuffer.length <= 4 ) {
            return false
        }

        let checksumValue = 0
        let lastIndex = dataBuffer.length - 4

        for ( let index = 0; index < lastIndex; index += 4 ) {
            checksumValue ^= dataBuffer.readInt32LE( index )
        }

        return dataBuffer.readInt32LE( lastIndex ) === checksumValue
    }

    appendChecksum( data: Buffer ): Buffer {
        let checksumValue = 0

        let indexLimit = data.length - 4
        for ( let index = 0; index < indexLimit; index += 4 ) {
            checksumValue ^= data.readInt32LE( index )
        }

        data.writeInt32LE( checksumValue, indexLimit )

        return data
    }

    decrypt( data: Buffer ): Buffer {
        for ( let index = 0; index < data.length; index += EngineValues.blockSize ) {
            this.cipher.decryptBlockAtIndex( data, index, data, index )
        }

        return data
    }

    encrypt( data: Buffer ): void {
        for ( let index = 0; index < data.length; index += EngineValues.blockSize ) {
            this.cipher.encryptBlockAtIndex( data, index, data, index )
        }
    }

    decryptXORPass( data: Buffer ) : Buffer {
        let size = data.length
        const stop: number = 4
        let position: number = size - 12
        let edx: number
        let ecx = data.readInt32LE( size - 8 )

        while ( stop <= position ) {
            edx = data.readInt32LE( position ) ^ ecx
            ecx -= edx

            data.writeInt32LE( edx, position )
            position -= 4
        }

        return data
    }
}