import { EncryptionOperations } from './EncryptionOperations'

export class GameEncryption implements EncryptionOperations {
    inKey: Buffer
    outKey: Buffer

    setKey( initialKey: Buffer ) : void {
        this.inKey = Buffer.allocUnsafeSlow( 16 )
        this.outKey = Buffer.allocUnsafeSlow( 16 )

        initialKey.copy( this.inKey )
        initialKey.copy( this.outKey )
    }

    decrypt( data: Buffer ) : Buffer {
        if ( !this.inKey ) {
            return data
        }

        let value = 0
        for ( let index = 0; index < data.length; index++ ) {
            let dataValue = data[ index ]
            data[ index ] = dataValue ^ this.inKey[ index & 15 ] ^ value
            value = dataValue
        }

        let newValue = this.inKey.readInt32LE( 8 ) + data.length
        this.inKey.writeInt32LE( newValue, 8 )

        return data
    }

    private encryptData( data: Buffer ) : Buffer {
        if ( !this.outKey ) {
            return data
        }

        let value = 0
        for ( let index = 0; index < data.length; index++ ) {
            value = data[ index ] ^ this.outKey[ index & 15 ] ^ value
            data[ index ] = value
        }

        let newValue = this.outKey.readInt32LE( 8 ) + data.length
        this.outKey.writeInt32LE( newValue, 8 )

        return data
    }

    encrypt( fullPacket: Buffer ) : Buffer {
        this.encryptData( fullPacket.subarray( 2 ) )
        return fullPacket
    }
}