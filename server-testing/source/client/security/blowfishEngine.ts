import { BlowfishKeys } from './blowfishKeys'

const enum BlowfishParameters {
    PSize = 18,
    SSize = 256
}

export class BlowfishEngine {

    S0: Array<number>
    S1: Array<number>
    S2: Array<number>
    S3: Array<number>
    P: Array<number>

    constructor( key : Buffer ) {
        this.setKey( key )
    }

    encode( value : number ) : number {
        return ( ( ( this.S0[ ( value >>> 24 ) ] + this.S1[ ( value >>> 16 ) & 0xff ] ) ^ this.S2[ ( value >>> 8 ) & 0xff ] ) + this.S3[ value & 0xff ] )
    }

    processTable( xl : number, xr : number, table : Array<number> ) {
        for ( let index = 0; index < table.length; index += 2 ) {
            xl ^= this.P[ 0 ]
            xr ^= this.encode( xl ) ^ this.P[ 1 ]
            xl ^= this.encode( xr ) ^ this.P[ 2 ]
            xr ^= this.encode( xl ) ^ this.P[ 3 ]
            xl ^= this.encode( xr ) ^ this.P[ 4 ]
            xr ^= this.encode( xl ) ^ this.P[ 5 ]
            xl ^= this.encode( xr ) ^ this.P[ 6 ]
            xr ^= this.encode( xl ) ^ this.P[ 7 ]
            xl ^= this.encode( xr ) ^ this.P[ 8 ]
            xr ^= this.encode( xl ) ^ this.P[ 9 ]
            xl ^= this.encode( xr ) ^ this.P[ 10 ]
            xr ^= this.encode( xl ) ^ this.P[ 11 ]
            xl ^= this.encode( xr ) ^ this.P[ 12 ]
            xr ^= this.encode( xl ) ^ this.P[ 13 ]
            xl ^= this.encode( xr ) ^ this.P[ 14 ]
            xr ^= this.encode( xl ) ^ this.P[ 15 ]
            xl ^= this.encode( xr ) ^ this.P[ 16 ]
            xr ^= this.P[ 17 ]

            table[ index ] = xr
            table[ index + 1 ] = xl
            xr = xl // end of cycle swap
            xl = table[ index ]
        }
    }

    setKey( key : Buffer ) {
        // Comments are from _Applied Crypto_, Schneier, p338.
        // Please be careful comparing the two, AC numbers the arrays from 1, the enclosed code from 0.
        // (1) Initialize the S-boxes and the P-array, with a fixed string This string contains the hexadecimal digits of pi (3.141...)
        this.S0 = structuredClone( BlowfishKeys.KS0 )
        this.S1 = structuredClone( BlowfishKeys.KS1 )
        this.S2 = structuredClone( BlowfishKeys.KS2 )
        this.S3 = structuredClone( BlowfishKeys.KS3 )
        this.P = structuredClone( BlowfishKeys.KP )
        // (2) Now, XOR P[0] with the first 32 bits of the key, XOR P[1] with the second 32-bits of the key, and so on for all bits of the key (up to P[17]).
        // Repeatedly cycle through the key bits until the entire P-array has been XOR-ed with the key bits
        let keyLength = key.length
        let keyIndex = 0
        for ( let index = 0; index < BlowfishParameters.PSize; index++ ) {
            // get the 32 bits of the key, in 4 * 8 bit chunks
            let data = 0x0000000
            for ( let counter = 0; counter < 4; counter++ ) {
                // create a 32 bit block
                data = ( data << 8 ) | ( key[ keyIndex++ ] & 0xff )
                // wrap when we get to the end of the key
                if ( keyIndex >= keyLength ) {
                    keyIndex = 0
                }
            }
            // XOR the newly created 32 bit chunk onto the P-array
            this.P[ index ] ^= data
        }
        // (3) Encrypt the all-zero string with the Blowfish algorithm, using the sub-keys described in (1) and (2)
        // (4) Replace P1 and P2 with the output of step (3)
        // (5) Encrypt the output of step(3) using the Blowfish algorithm, with the modified sub-keys.
        // (6) Replace P3 and P4 with the output of step (5)
        // (7) Continue the process, replacing all elements of the P-array and then all four S-boxes in order, with the output of the continuously changing Blowfish algorithm
        this.processTable( 0, 0, this.P )
        this.processTable( this.P[ BlowfishParameters.PSize - 2 ], this.P[ BlowfishParameters.PSize - 1 ], this.S0 )
        this.processTable( this.S0[ BlowfishParameters.SSize - 2 ], this.S0[ BlowfishParameters.SSize - 1 ], this.S1 )
        this.processTable( this.S1[ BlowfishParameters.SSize - 2 ], this.S1[ BlowfishParameters.SSize - 1 ], this.S2 )
        this.processTable( this.S2[ BlowfishParameters.SSize - 2 ], this.S2[ BlowfishParameters.SSize - 1 ], this.S3 )
    }

    encryptBlockAtIndex( sourceData : Buffer, sourceIndex : number, destinationData : Buffer, destinationIndex : number ): void {
        let xl = sourceData.readInt32LE( sourceIndex )
        let xr = sourceData.readInt32LE( sourceIndex + 4 )

        xl ^= this.P[ 0 ]
        xr ^= this.encode( xl ) ^ this.P[ 1 ]
        xl ^= this.encode( xr ) ^ this.P[ 2 ]
        xr ^= this.encode( xl ) ^ this.P[ 3 ]
        xl ^= this.encode( xr ) ^ this.P[ 4 ]
        xr ^= this.encode( xl ) ^ this.P[ 5 ]
        xl ^= this.encode( xr ) ^ this.P[ 6 ]
        xr ^= this.encode( xl ) ^ this.P[ 7 ]
        xl ^= this.encode( xr ) ^ this.P[ 8 ]
        xr ^= this.encode( xl ) ^ this.P[ 9 ]
        xl ^= this.encode( xr ) ^ this.P[ 10 ]
        xr ^= this.encode( xl ) ^ this.P[ 11 ]
        xl ^= this.encode( xr ) ^ this.P[ 12 ]
        xr ^= this.encode( xl ) ^ this.P[ 13 ]
        xl ^= this.encode( xr ) ^ this.P[ 14 ]
        xr ^= this.encode( xl ) ^ this.P[ 15 ]
        xl ^= this.encode( xr ) ^ this.P[ 16 ]
        xr ^= this.P[ 17 ]

        destinationData.writeInt32LE( xr, destinationIndex )
        destinationData.writeInt32LE( xl, destinationIndex + 4 )
    }


    decryptBlockAtIndex( sourceData : Buffer, sourceIndex : number, destinationData : Buffer, destinationIndex : number ) : void {
        let xl : number = sourceData.readInt32LE( sourceIndex )
        let xr : number = sourceData.readInt32LE( sourceIndex + 4 )

        xl ^= this.P[ 17 ]
        xr ^= this.encode( xl ) ^ this.P[ 16 ]
        xl ^= this.encode( xr ) ^ this.P[ 15 ]
        xr ^= this.encode( xl ) ^ this.P[ 14 ]
        xl ^= this.encode( xr ) ^ this.P[ 13 ]
        xr ^= this.encode( xl ) ^ this.P[ 12 ]
        xl ^= this.encode( xr ) ^ this.P[ 11 ]
        xr ^= this.encode( xl ) ^ this.P[ 10 ]
        xl ^= this.encode( xr ) ^ this.P[ 9 ]
        xr ^= this.encode( xl ) ^ this.P[ 8 ]
        xl ^= this.encode( xr ) ^ this.P[ 7 ]
        xr ^= this.encode( xl ) ^ this.P[ 6 ]
        xl ^= this.encode( xr ) ^ this.P[ 5 ]
        xr ^= this.encode( xl ) ^ this.P[ 4 ]
        xl ^= this.encode( xr ) ^ this.P[ 3 ]
        xr ^= this.encode( xl ) ^ this.P[ 2 ]
        xl ^= this.encode( xr ) ^ this.P[ 1 ]
        xr ^= this.P[ 0 ]

        destinationData.writeInt32LE( xr, destinationIndex )
        destinationData.writeInt32LE( xl, destinationIndex + 4 )
    }
}