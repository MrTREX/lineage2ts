import { L2LoginClient, L2LoginParameters } from './L2LoginClient'
import { L2GameAccount, L2GameClient, L2GameParameters } from './L2GameClient'
import { PacketEvent, PacketListenerMethod } from './packets/PacketMethodTypes'
import { L2BaseClientCallback, L2ClientError } from './L2BaseClient'
import chalk from 'chalk'
import { LoginServerApprovedEvent } from './packets/login/receive/ServerLoginApproved'
import logSymbols from 'log-symbols'
import { GameCharacterSelectedEvent } from './packets/game/receive/CharacterSelected'

export type L2ClientExitCallback = () => void

interface L2ClientProperties {
    loginClient: L2LoginClient
    gameClient: L2GameClient
    name: string
    onLoginServerFlowFinished: L2BaseClientCallback
    onGameServerFlowFinished: L2BaseClientCallback
    onClientExit: L2ClientExitCallback
}

const defaultGameAccount : L2GameAccount = {
    characterName: ''
}

export abstract class L2ClientLogic implements L2ClientProperties {
    loginClient: L2LoginClient
    gameClient: L2GameClient
    name: string

    constructor( name : string ) {
        this.name = chalk.green( name )
    }

    start() : void {
        this.loginClient = new L2LoginClient( this.getLoginProperties(), this.onLoginServerFlowFinished.bind( this ), this.getLoginPacketListeners() )
    }

    onLoginServerFlowFinished( error : L2ClientError, data: PacketEvent ) : void {
        if ( error ) {
            console.log( logSymbols.error, 'Login server failure for', this.name )
            console.log( logSymbols.error, 'Error code:', chalk.blue( error.id ) )
            console.log( logSymbols.error, 'Error message:', chalk.red( error.message ) )
        }

        if ( !data ) {
            return this.onClientExit( false )
        }

        let parameters : L2GameParameters = {
            account: this.getGameProperties(),
            authentication: {
                loginOne: this.loginClient.loginConfig.one,
                loginTwo: this.loginClient.loginConfig.two,
                playOne: ( data as LoginServerApprovedEvent ).one,
                playTwo: ( data as LoginServerApprovedEvent ).two,
                userName: this.loginClient.parameters.userName
            },
            host: this.loginClient.gameServer.ip,
            port: this.loginClient.gameServer.port,
        }

        this.gameClient = new L2GameClient( parameters, this.onGameServerFlowFinished.bind( this ), this.getGamePacketListeners() )
    }

    /*
        We need to keep login connection alive for some time for game client to verify authentication parameters.
        Once all login and game flows complete, we can safely terminate login client connection.
     */
    onGameServerFlowFinished( error : L2ClientError, data: GameCharacterSelectedEvent ) : void {
        if ( this.loginClient ) {
            // this.loginClient.abortConnection()
            // this.loginClient = null
        }

        if ( error ) {
            console.log( logSymbols.error, 'Game server failure for', this.name )
            console.log( logSymbols.error, 'Error code:', chalk.blue( error.id ) )
            console.log( logSymbols.error, 'Error message:', chalk.red( error.message ) )

            return this.onClientExit( false )
        }

        this.onReadyToEnterWorld( data )
    }

    onReadyToEnterWorld( data: GameCharacterSelectedEvent ) : void {
        this.onClientExit()
    }

    onClientExit( isSuccess: boolean = true ) : void {
        console.log( logSymbols.success, 'Terminated client', this.name )
        process.exit( isSuccess ? 0 : 1 )
    }

    abstract getLoginProperties() : L2LoginParameters
    getGameProperties() : L2GameAccount {
        return structuredClone( defaultGameAccount )
    }

    notifySuccess( message: string, optionalMessage : string = '' ) : void {
        console.log( logSymbols.success, this.name, chalk.cyan( message ), optionalMessage ? chalk.yellow( optionalMessage ) : '' )
    }

    notifyFailure( message: string, optionalMessage : string = '' ) : void {
        console.log( logSymbols.error, this.name, chalk.red( message ), optionalMessage ? chalk.yellow( optionalMessage ) : '' )
    }

    notifyWarning( message: string, optionalMessage : string = '' ) : void {
        console.log( logSymbols.warning, this.name, chalk.white( message ), optionalMessage ? chalk.yellow( optionalMessage ) : '' )
    }

    getGamePacketListeners() : Record<string, PacketListenerMethod> {
        return {}
    }

    getLoginPacketListeners() : Record<string, PacketListenerMethod> {
        return {}
    }

    abortAllConnections() : void {
        if ( this.loginClient ) {
            this.loginClient.abortConnection()
        }

        if ( this.gameClient ) {
            this.gameClient.abortConnection()
        }
    }

    // TODO : use onGameServerFlowFinished to record error, or terminate client
    terminateClient( isSuccess: boolean, message?: string ) : void {
        this.abortAllConnections()

        if ( message ) {
            if ( isSuccess ) {
                this.notifySuccess( message )
            } else {
                this.notifyFailure( message )
            }
        }

        return this.onClientExit( isSuccess )
    }
}