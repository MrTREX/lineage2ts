import HashArray from 'hasharray'
import { L2Item } from '../../models/L2Item'
import { L2World } from '../../L2World'
import { L2GameClient } from '../../L2GameClient'

export class BaseInventory {
    allItems: typeof HashArray = new HashArray( [ 'objectId', 'itemId' ] )
    ownerId: number
    client: L2GameClient

    constructor( ownerId: number, client: L2GameClient ) {
        this.ownerId = ownerId
        this.client = client
    }

    getItems( itemOrObjectId: number ) : Array<L2Item> {
        return this.allItems.getAsArray( itemOrObjectId )
    }

    hasItem( itemOrObjectId: number ) : boolean {
        return this.allItems.has( itemOrObjectId )
    }

    protected addItemByObjectId( objectId: number ) : void {
        let item = L2World.getObject( objectId ) as L2Item

        if ( !item ) {
            return
        }

        item.ownerObjectId = this.ownerId

        this.allItems.add( item )
    }

    getItemCount( itemOrObjectId: number ) : number {
        let items = this.getItems( itemOrObjectId )

        return items.reduce( ( total: number, item: L2Item ) : number => {
            return total + item.amount
        }, 0 )
    }
}