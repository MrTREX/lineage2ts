import { IGameSystem } from './IGameSystem'
import { BaseInventory } from './base/BaseInventory'
import { PacketListenerMethod } from '../packets/PacketMethodTypes'
import { PetItemList, PetItemListEvent } from '../packets/game/receive/PetItemList'
import { PetInventoryUpdate, PetInventoryUpdateEvent } from '../packets/game/receive/PetInventoryUpdate'
import { L2World } from '../L2World'
import { L2Item } from '../models/L2Item'
import { ItemState } from '../enums/ItemState'

export class PetInventory extends BaseInventory implements IGameSystem {
    listeners: Record<string, PacketListenerMethod> = {
        [ PetItemList.name ]: this.onPetItemList.bind( this ),
        [ PetInventoryUpdate.name ]: this.onPetInventoryUpdate.bind( this )
    }

    onPetItemList( data : PetItemListEvent ) : void {
        this.allItems.removeAll()

        data.itemObjectIds.forEach( this.addItemByObjectId.bind( this ) )
    }

    onPetInventoryUpdate( data: PetInventoryUpdateEvent ) : void {
        data.addedObjectIds.forEach( this.addItemByObjectId.bind( this ) )
        data.removedObjectIds.forEach( ( objectId: number ) => {
            this.allItems.removeByKey( objectId )

            let item = L2World.getObject( objectId ) as L2Item
            if ( !item ) {
                return
            }

            if ( item.state !== ItemState.Dropped ) {
                L2World.deleteObject( objectId )
            }
        } )
    }
}