import { IGameSystem } from './IGameSystem'
import { PacketListenerMethod } from '../packets/PacketMethodTypes'
import { SystemMessage, SystemMessageEvent } from '../packets/game/receive/SystemMessage'
import { SayText, SayTextEvent } from '../packets/game/receive/SayText'
import CircularBuffer from 'mnemonist/circular-buffer'

export class ChatMessages implements IGameSystem {
    systemMessages: CircularBuffer<SystemMessageEvent> = new CircularBuffer<SystemMessageEvent>( Array, 100 )
    textMessages: CircularBuffer<SayTextEvent> = new CircularBuffer<SayTextEvent>( Array, 500 )

    listeners: Record<string, PacketListenerMethod> = {
        [ SystemMessage.name ]: this.onSystemMessage.bind( this ),
        [ SayText.name ]: this.onSayText.bind( this )
    }

    onSystemMessage( data: SystemMessageEvent ): void {
        this.systemMessages.push( data )
    }

    onSayText( data: SayTextEvent ) : void {
        this.textMessages.push( data )
    }

    getLastSystemMessage(): SystemMessageEvent {
        return this.systemMessages.peekLast() as SystemMessageEvent
    }

    hasSystemMessages(): boolean {
        return this.systemMessages.size > 0
    }

    getAllSystemMessages(): Array<SystemMessageEvent> {
        return this.systemMessages.toArray() as Array<SystemMessageEvent>
    }

    getMessagesById( id: number ) : Array<SystemMessageEvent> {
        return this.getAllSystemMessages().filter( ( message : SystemMessageEvent ) : boolean => {
            return message && message.messageId === id
        } )
    }
}