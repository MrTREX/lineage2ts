import { Given, Then, When } from '@cucumber/cucumber'
import { CreateCharacter, CreateCharacterData } from '../client/packets/game/send/CreateCharacter'
import { ClassId } from '../client/enums/ClassId'
import { AvailableFaces } from '../client/enums/PlayerFace'
import { AvailableHairColors, AvailableHairStyles } from '../client/enums/PlayerHair'
import { Race } from '../client/enums/Race'
import { AvailablePlayerSexes, PlayerSex } from '../client/enums/PlayerSex'
import Chance from 'chance'
import { DefaultScenarioClient } from '../client/L2ScenarioRunnerClient'
import { scheduler } from 'node:timers/promises'
import {
    CharacterCreationSuccess,
    CharacterCreationSuccessEvent
} from '../client/packets/game/receive/CharacterCreationSuccess'
import {
    CharacterCreationFailure,
    CharacterCreationFailureEvent,
    CharacterCreationFailureMessage
} from '../client/packets/game/receive/CharacterCreationFailure'
import {
    CharacterSelectionInfo,
    CharacterSelectionInfoEvent
} from '../client/packets/game/receive/CharacterSelectionInfo'
import { DeleteCharacter } from '../client/packets/game/send/DeleteCharacter'
import {
    CharacterDeleteFail,
    CharacterDeleteFailEvent,
    CharacterDeleteReason
} from '../client/packets/game/receive/CharacterDeleteFail'
import { CharacterDeleteSuccess } from '../client/packets/game/receive/CharacterDeleteSuccess'
import { ShowAvailableCharacters } from '../client/packets/game/send/ShowAvailableCharacters'
import { getCharacterSlot, getCharacterSlotForPrefix } from './helpers/characterSelection'

const chance = new Chance()
let currentCharacter: CreateCharacterData

async function deleteCharacterBySlot( slot: number ) : Promise<void> {
    DefaultScenarioClient.sendGamePacket( DeleteCharacter( slot ) )
    await scheduler.wait( 500 )

    let failureData = DefaultScenarioClient.getPacketData( CharacterDeleteFail.name ) as CharacterDeleteFailEvent
    if ( failureData ) {
        throw new Error( `Character name ${ currentCharacter.name } deletion failed with reason : ${ CharacterDeleteReason[ failureData.reason ] }` )
    }

    let successData = DefaultScenarioClient.getPacketData( CharacterDeleteSuccess.name )
    if ( !successData ) {
        throw new Error( `Character name ${ currentCharacter.name } deletion failed with no reason` )
    }
}

When( 'Character.Create using race {string} , class {string} and random parameters', async ( raceName: string, className: string ) => {
    let classId = ClassId[ className ]

    if ( Number.isNaN( classId ) ) {
        throw new Error( `Class ${ className } does not exist ` )
    }

    let race = Race[ raceName ]

    if ( Number.isNaN( race ) ) {
        throw new Error( `Race ${ raceName } does not exist` )
    }

    let sex = chance.pickone( AvailablePlayerSexes )

    if ( race === Race.Kamael ) {
        if ( classId === ClassId.MaleSoldier ) {
            sex = PlayerSex.Male
        }

        if ( classId === ClassId.FemaleSoldier ) {
            sex = PlayerSex.Female
        }
    }

    currentCharacter = {
        classId,
        face: chance.pickone( AvailableFaces ),
        hairColor: chance.pickone( AvailableHairColors ),
        hairStyle: chance.pickone( AvailableHairStyles ),
        name: `Test${ chance.word( { capitalize: true } ) }`,
        race,
        sex,
    }

    DefaultScenarioClient.sendGamePacket( CreateCharacter( currentCharacter ) )
    await scheduler.wait( 500 )

    let failureData: CharacterCreationFailureEvent = DefaultScenarioClient.getPacketData( CharacterCreationFailure.name ) as CharacterCreationFailureEvent
    if ( failureData ) {
        throw new Error( `Server refused to create character with name ${ currentCharacter.name } for reason: ${ CharacterCreationFailureMessage[ failureData.reason ] }` )
    }

    let successData: CharacterCreationSuccessEvent = DefaultScenarioClient.getPacketData( CharacterCreationSuccess.name ) as CharacterCreationSuccessEvent
    if ( !successData || !successData.isCharacterCreated ) {
        throw new Error( `Server cannot create character with name ${ currentCharacter.name } for unknown reason` )
    }
} )

Given( 'Character.Verify exists', () => {
    let playerSlot = getCharacterSlot( currentCharacter.name )
    if ( playerSlot === -1 ) {
        throw new Error( `Unable to find created character using name: ${ currentCharacter.name }` )
    }
} )

Given( 'Character.Verify new one can be created', () => {
    let allCharacters = DefaultScenarioClient.getPacketData( CharacterSelectionInfo.name ) as CharacterSelectionInfoEvent
    if ( allCharacters.characterLimit === allCharacters.characters.length ) {
        throw new Error( `Reached limit of ${allCharacters.characterLimit} possible characters` )
    }
} )

Then( 'Character.Delete current character', async () => {
    let playerSlot = getCharacterSlot( currentCharacter.name )
    if ( playerSlot === -1 ) {
        throw new Error( `Unable to delete created character using name: ${ currentCharacter.name }` )
    }

    await deleteCharacterBySlot( playerSlot )
} )

Then( 'Character.Delete characters with prefix {string}', async ( prefix: string ) => {
    while ( prefix ) {
        let playerSlot = getCharacterSlotForPrefix( prefix )
        if ( playerSlot === -1 ) {
            return
        }

        await deleteCharacterBySlot( playerSlot )
        DefaultScenarioClient.sendGamePacket( ShowAvailableCharacters() )
        await scheduler.wait( 2000 )
    }
} )

Given( 'Character.Exists one with prefix {string}', ( prefix: string ) => {
    let allCharacters = DefaultScenarioClient.getPacketData( CharacterSelectionInfo.name ) as CharacterSelectionInfoEvent
    let outcome = allCharacters.characters.some( data => data.name.startsWith( prefix ) )

    if ( !outcome ) {
        throw new Error( `No characters found for prefix "${prefix}"` )
    }
} )

When( 'Character.Verify in game session',() => {
    if ( !DefaultScenarioClient.isEnteredWorld ) {
        throw new Error( 'Character has not entered world' )
    }
} )

Then( 'Character.Verify an admin user', () => {
    let player = DefaultScenarioClient.getPlayer()
    if ( !player ) {
        throw new Error( 'Unable to location information about current player' )
    }

    if ( !player.isGM ) {
        throw new Error( `Player with name "${player.name}" is not admin user` )
    }
} )

Then( 'Character.Verify target is self', () => {
    let player = DefaultScenarioClient.getPlayer()
    if ( !player ) {
        throw new Error( 'Unable to location information about current player' )
    }

    if ( player.targetId !== player.objectId ) {
        throw new Error( 'Player target is not self' )
    }
} )