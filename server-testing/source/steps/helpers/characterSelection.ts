import { DefaultScenarioClient } from '../../client/L2ScenarioRunnerClient'
import {
    CharacterSelectionInfo,
    CharacterSelectionInfoEvent
} from '../../client/packets/game/receive/CharacterSelectionInfo'

export function getCharacterSlot( name: string ): number {
    let allCharacters = DefaultScenarioClient.getPacketData( CharacterSelectionInfo.name ) as CharacterSelectionInfoEvent
    return allCharacters.characters.findIndex( value => value.name === name )
}

export function getCharacterSlotForPrefix( prefix: string ): number {
    let allCharacters = DefaultScenarioClient.getPacketData( CharacterSelectionInfo.name ) as CharacterSelectionInfoEvent
    return allCharacters.characters.findIndex( value => value.name.startsWith( prefix ) )
}