import { L2ItemIds } from '../../interactions/enums/Item'

export function getItemIdFromName( itemName : string ) : number {
    let itemId = L2ItemIds[ itemName ]
    if ( !Number.isInteger( itemId ) ) {
        throw new Error( `Unable to find item id for name "${itemName}"` )
    }

    return itemId
}