import { Then } from '@cucumber/cucumber'
import { scheduler } from 'node:timers/promises'

Then( 'Wait for {int} seconds', { timeout: 60 * 1000 }, ( seconds: number ): Promise<void> => {
    return scheduler.wait( seconds * 1000 )
} )