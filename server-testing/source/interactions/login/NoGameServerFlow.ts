import { L2ClientLogic } from '../../client/L2ClientLogic'
import { L2LoginParameters } from '../../client/L2LoginClient'
import { L2ServerDefaults } from '../../client/enums/L2ServerDefaults'
import { L2ClientError } from '../../client/L2BaseClient'
import { PacketEvent } from '../../client/packets/PacketMethodTypes'

export class NoGameServerFlow extends L2ClientLogic {
    constructor() {
        super( NoGameServerFlow.name )
    }
    getLoginProperties(): L2LoginParameters {
        return {
            host: L2ServerDefaults.LocalhostIpAddress,
            password: 'admin',
            port: L2ServerDefaults.LoginServerPort,
            serverId: L2ServerDefaults.ServerId,
            userName: 'admin'
        }
    }
    onLoginServerFlowFinished( error : L2ClientError, data: PacketEvent ) : void {
        if ( error && error.id !== 'e403ec46-49c4-41b4-9771-7909225109fa' ) {
            return super.onLoginServerFlowFinished( error, data )
        }

        let isSuccess : boolean = !error
        if ( !isSuccess ) {
            this.notifySuccess( 'verified no servers available' )
        } else {
            this.notifyFailure( 'cannot verify no servers condition' )
        }

        this.onClientExit( isSuccess )
    }
}

new NoGameServerFlow().start()