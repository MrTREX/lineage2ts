import { L2FullClientLogic } from '../../client/L2FullClientLogic'
import { L2LoginParameters } from '../../client/L2LoginClient'
import { L2ServerDefaults } from '../../client/enums/L2ServerDefaults'
import { SayText, SayTextType } from '../../client/packets/game/send/SayText'
import _ from 'lodash'

class ShoutAnnouncer extends L2FullClientLogic {
    announceTimeout: NodeJS.Timeout
    textLines: Array<string> = [
            'Hello there everybody!',
            'I need friends to play!',
            'Give me money please!'
    ]

    amount: number = 5

    constructor() {
        super( ShoutAnnouncer.name )
    }

    getLoginProperties(): L2LoginParameters {
        return {
            host: L2ServerDefaults.LocalhostIpAddress,
            password: 'tester',
            port: L2ServerDefaults.LoginServerPort,
            serverId: L2ServerDefaults.ServerId,
            userName: 'tester'
        }
    }

    onWorldEntered() {
        this.announceTimeout = setInterval( this.onCreateAnnouncement.bind( this ), 2000 )
    }

    onCreateAnnouncement() : void {
        let text = _.sample( this.textLines )

        this.notifySuccess( 'Shouting text:', text )
        this.gameClient.sendPacket( SayText( text, SayTextType.Shout ) )
        this.amount--

        if ( !this.amount ) {
            clearInterval( this.announceTimeout )
            return this.performLogOut()
        }
    }

    getCharacterName(): string {
        return 'tester1'
    }
}

new ShoutAnnouncer().start()