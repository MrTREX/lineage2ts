import { L2ClientLogic } from '../../client/L2ClientLogic'
import { L2LoginParameters } from '../../client/L2LoginClient'
import { L2ServerDefaults } from '../../client/enums/L2ServerDefaults'
import { PacketListenerMethod } from '../../client/packets/PacketMethodTypes'
import { CharacterSelectionInfo, CharacterSelectionInfoEvent } from '../../client/packets/game/receive/CharacterSelectionInfo'
import { CreateCharacter, CreateCharacterData } from '../../client/packets/game/send/CreateCharacter'
import { AvailableFaces } from '../../client/enums/PlayerFace'
import { AvailableHairColors, AvailableHairStyles } from '../../client/enums/PlayerHair'
import { Race } from '../../client/enums/Race'
import { AvailablePlayerSexes } from '../../client/enums/PlayerSex'
import { ClassId } from '../../client/enums/ClassId'
import {
    CharacterCreationFailure,
    CharacterCreationFailureEvent,
    CharacterCreationFailureMessage,
} from '../../client/packets/game/receive/CharacterCreationFailure'
import { CharacterCreationSuccess, CharacterCreationSuccessEvent } from '../../client/packets/game/receive/CharacterCreationSuccess'
import { ShowAvailableCharacters } from '../../client/packets/game/send/ShowAvailableCharacters'
import Chance from 'chance'

const chance = new Chance()

export class CharacterCreation extends L2ClientLogic {
    playerName: string
    selectionTries: number = 0
    constructor() {
        super( CharacterCreation.name )
    }

    getLoginProperties(): L2LoginParameters {
        return {
            host: L2ServerDefaults.LocalhostIpAddress,
            password: 'tester',
            port: L2ServerDefaults.LoginServerPort,
            serverId: L2ServerDefaults.ServerId,
            userName: 'tester',
        }
    }

    getGamePacketListeners(): Record<string, PacketListenerMethod> {
        return {
            [ CharacterSelectionInfo.name ]: this.onCharacterSelectionInfo.bind( this ),
            [ CharacterCreationFailure.name ]: this.onCharacterCreationFailure.bind( this ),
            [ CharacterCreationSuccess.name ]: this.onCharacterCreationSuccess.bind( this ),
        }
    }

    /*
        1. Receive available characters
        - check if there are available slots
        - proceed with character creation

        3. If we have created our character.
        - determine slot to delete since characters can be randomized
        - verify slot for newly created character
     */
    onCharacterSelectionInfo( data: CharacterSelectionInfoEvent ): void {
        switch ( this.selectionTries ) {
            case 0:
                if ( data.characters.length === data.characterLimit ) {
                    return this.terminateClient( false, `No available characters to create. Limit is ${ data.characterLimit }` )
                }

                this.selectionTries++
                return this.gameClient.sendPacket( CreateCharacter( this.getCharacterData() ) )

            default:
                let playerSlot = data.characters.findIndex( value => value.name === this.playerName )
                if ( playerSlot === -1 ) {
                    return this.terminateClient( false, `Unable to find created character using name: ${ this.playerName }` )
                }

                return this.terminateClient( true, `Create character with name = ${this.playerName}` )
        }
    }

    /*
        2. Server responds with character creation success
        - normally such response automatically means character is created, however we can do our own check on status
        - character created had higher index value than previous characters, however due to randomization on server side
          ( server configuration ), we cannot be certain that index is indeed correct.
          Thus, request available characters from server to verify new index. Keep in mind that server will have rate protection
          on both create character and character selection packets, which must be remedied by wait time.
     */
    onCharacterCreationSuccess( data: CharacterCreationSuccessEvent ) : void {
        if ( !data.isCharacterCreated ) {
            return this.terminateClient( false, 'Server refused character creation' )
        }

        this.notifySuccess( 'Created character with name', this.playerName )
        this.gameClient.sendPacket( ShowAvailableCharacters() )
    }

    /*
        Server can reply to CreateCharacter packet to signify no character was created due to:
        - server found invalid character parameters such as name/face/hair and classId
        - server settings no longer allow character creation
        - account cannot create more characters
     */
    onCharacterCreationFailure( data: CharacterCreationFailureEvent ): void {
        this.terminateClient( false, CharacterCreationFailureMessage[ data.reason ] )
    }

    /*
        Character data.
        - race option is used for verification of classId by server
        - invalid values for enums, classId (picking non-starting values) or long name will fail creation
     */
    getCharacterData(): CreateCharacterData {
        this.playerName = 'tester1'

        return {
            classId: ClassId.DarkFighter,
            face: chance.pickone( AvailableFaces ),
            hairColor: chance.pickone( AvailableHairColors ),
            hairStyle: chance.pickone( AvailableHairStyles ),
            name: this.playerName,
            race: Race.DarkElf,
            sex: chance.pickone( AvailablePlayerSexes ),
        }
    }
}

new CharacterCreation().start()