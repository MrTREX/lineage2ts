import { L2ClientLogic } from '../../client/L2ClientLogic'
import { L2LoginParameters } from '../../client/L2LoginClient'
import { L2ServerDefaults } from '../../client/enums/L2ServerDefaults'
import { CharacterSelectionInfo, CharacterSelectionInfoEvent } from '../../client/packets/game/receive/CharacterSelectionInfo'
import { PacketListenerMethod } from '../../client/packets/PacketMethodTypes'

export class VerifyCharactersToSelect extends L2ClientLogic {

    constructor() {
        super( VerifyCharactersToSelect.name )
    }

    getLoginProperties(): L2LoginParameters {
        return {
            host: L2ServerDefaults.LocalhostIpAddress,
            password: 'admin',
            port: L2ServerDefaults.LoginServerPort,
            serverId: L2ServerDefaults.ServerId,
            userName: 'admin'
        }
    }

    getGamePacketListeners(): Record<string, PacketListenerMethod> {
        return {
            [ CharacterSelectionInfo.name ]: this.onCharacterSelectionInfo.bind( this )
        }
    }

    onCharacterSelectionInfo( data: CharacterSelectionInfoEvent ) : void {
        this.abortAllConnections()

        let isSuccess = data.characters.length > 0
        let message = `Account has ${ data.characters.length } playable characters`
        isSuccess ? this.notifySuccess( message ) : this.notifyFailure( message )

        this.onClientExit( isSuccess )
    }
}

new VerifyCharactersToSelect().start()