import { L2FullClientLogic } from '../../../client/L2FullClientLogic'
import { L2LoginParameters } from '../../../client/L2LoginClient'
import { L2ServerDefaults } from '../../../client/enums/L2ServerDefaults'

class VerifyAdminUser extends L2FullClientLogic {
    constructor() {
        super( VerifyAdminUser.name )
    }

    getLoginProperties(): L2LoginParameters {
        return {
            host: L2ServerDefaults.LocalhostIpAddress,
            password: 'tester',
            port: L2ServerDefaults.LoginServerPort,
            serverId: L2ServerDefaults.ServerId,
            userName: 'tester'
        }
    }

    onWorldEntered() {
        let isGM = this.getPlayer().isGM

        if ( isGM ) {
            this.notifySuccess( 'Player is GM' )
            this.performLogOut()

            return
        }

        this.notifyFailure( 'Player is not GM' )
        this.onClientExit( isGM )
    }

    getCharacterName(): string {
        return 'tester1'
    }
}

new VerifyAdminUser().start()