import { L2FullClientLogic } from '../../../client/L2FullClientLogic'
import { L2LoginParameters } from '../../../client/L2LoginClient'
import { DefaultLoginParameters, L2ServerDefaults } from '../../../client/enums/L2ServerDefaults'
import { BasicItem, L2ItemIds } from '../../enums/Item'
import _ from 'lodash'
import { BypassCommand } from '../../../client/packets/game/send/BypassCommand'

const enum CreateDeleteSequence {
    Create,
    VerifyCreate,
    Delete,
    VerifyDelete
}

const itemsToCreate: Array<BasicItem> = [
    {
        id: L2ItemIds.ManaPotion,
        amount: _.random( 1000 )
    }
]

class CreateDeleteItem extends L2FullClientLogic {
    timer: NodeJS.Timeout
    step: CreateDeleteSequence = CreateDeleteSequence.Create

    constructor() {
        super( CreateDeleteItem.name )
    }

    getLoginProperties(): L2LoginParameters {
        return {
            ...DefaultLoginParameters,
            password: 'tester',
            userName: 'tester'
        }
    }

    onWorldEntered() {
        let isGM = this.getPlayer().isGM

        if ( !isGM ) {
            this.notifyFailure( 'Player is not GM' )
            this.onClientExit( false )

            return
        }

        /*
            We must wait for inventory packets to fully arrive before
            we are able to start verification.
         */
        this.timer = setInterval( this.runSequence.bind( this ), 1000 )
    }

    runSequence(): void {
        switch ( this.step ) {
            case CreateDeleteSequence.Create:
                if ( this.shouldRetry() ) {
                    return
                }

                this.notifySuccess( 'Verified no existing items in inventory' )
                this.createItems()

                this.step = CreateDeleteSequence.VerifyCreate
                return

            case CreateDeleteSequence.VerifyCreate:
                if ( this.verifyItemCreation() ) {
                    return this.onClientExit( false )
                }

                this.notifySuccess( `Verified creation for ${ itemsToCreate.length } items` )

                this.step = CreateDeleteSequence.Delete
                return

            case CreateDeleteSequence.Delete:
                itemsToCreate.forEach( ( item: BasicItem ) => this.inventory.destroyAllItemsById( item.id ) )

                this.step = CreateDeleteSequence.VerifyDelete
                return

            case CreateDeleteSequence.VerifyDelete:
                clearInterval( this.timer )

                if ( this.verifyItemDeletion() ) {
                    return this.onClientExit( false )
                }

                this.notifySuccess( `Verified deletion for ${ itemsToCreate.length } items` )
                this.performLogOut()
                return
        }
    }

    getCharacterName(): string {
        return 'tester1'
    }

    shouldRetry(): boolean {
        let shouldRetry = false
        itemsToCreate.forEach( ( item: BasicItem ): void => {
            let amount = this.inventory.getItemCount( item.id )
            if ( amount > 0 ) {
                this.notifyWarning( `Item check: removing existing itemId=${item.id} and amount=${amount}` )
                this.inventory.destroyAllItemsById( item.id )
                shouldRetry = true
            }
        } )

        return shouldRetry
    }

    verifyItemCreation(): boolean {
        return itemsToCreate.some( ( item: BasicItem ): boolean => {
            let amount = this.inventory.getItemCount( item.id )
            let hasError = amount !== item.amount

            if ( hasError ) {
                this.notifyFailure( 'Item creation failure:', `expected=${ item.amount }, received=${ amount } amount for itemId=${ item.id }` )
            }

            return hasError
        } )
    }

    verifyItemDeletion(): boolean {
        return itemsToCreate.some( ( item: BasicItem ): boolean => {
            let amount = this.inventory.getItemCount( item.id )
            let hasError = amount !== 0

            if ( hasError ) {
                this.notifyFailure( 'Item deletion failure:', `received=${ amount } amount for itemId=${ item.id }` )
            }

            return hasError
        } )
    }

    createItems() : void {
        itemsToCreate.forEach( ( item: BasicItem ) => {
            let text = `create-item ${item.id} ${item.amount}`
            this.gameClient.sendPacket( BypassCommand( text ) )
        } )
    }
}

new CreateDeleteItem().start()