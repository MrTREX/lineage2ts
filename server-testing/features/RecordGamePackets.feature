@devTest
Feature: Record packets for inspection
  Scenario: Observe packets when in game
    Given Client.Packets are recorded
    Given Client.Enters game using player "first"
    When Character.Verify in game session
    Then Wait for 5 seconds
    Given Client.Packets are no longer recorded
    Then Client.Packets are written in file "recordedPackets.json"