Feature: As admin user create an item in inventory, drop items on ground, verify picking up and then deleting items in inventory.
  Background:
    Given Client.Enters game using player "tester1"
    When Character.Verify in game session
    Then Character.Verify an admin user

  Scenario:
    Given Client.Request to target self
    Then Character.Verify target is self
    When Inventory.Delete all "Adena"
    Given Client.Request bypass create-givetarget of item "Adena" with quantity 1000
    Then Inventory.Has item "Adena" with quantity 1000
    Given Inventory.Drop item "Adena" in radius 30 from player
    When Wait for 2 seconds
    Then Inventory.Has no item "Adena"
    Then World.Pick dropped item "Adena" with quantity 1000 in radius 50 of player
    When Wait for 5 seconds
    Then Inventory.Has item "Adena" with quantity 1000
    When Inventory.Delete all "Adena"
    When Wait for 1 seconds
    Then Inventory.Has no item "Adena"
