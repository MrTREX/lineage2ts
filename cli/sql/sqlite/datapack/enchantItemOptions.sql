DROP TABLE IF EXISTS `enchant_item_options`;

CREATE TABLE IF NOT EXISTS `enchant_item_options`
(
    `itemId` INTEGER,
    `level` INTEGER,
    `options_json` TEXT
);