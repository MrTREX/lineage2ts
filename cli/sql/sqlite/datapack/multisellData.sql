DROP TABLE IF EXISTS `multisell_data`;

CREATE TABLE IF NOT EXISTS `multisell_data`
(
    `id` INTEGER PRIMARY KEY,
    `applyTaxes` TEXT,
    `maintainEnchantment` TEXT,
    `npcIds_json` TEXT,
    `items_json` TEXT
);