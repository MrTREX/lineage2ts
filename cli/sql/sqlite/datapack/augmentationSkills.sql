DROP TABLE IF EXISTS `augmentation_skills`;

CREATE TABLE IF NOT EXISTS `augmentation_skills`
(
    `id` INTEGER PRIMARY KEY,
    `skillId` INTEGER,
    `skillLevel` INTEGER,
    `type` TEXT
);