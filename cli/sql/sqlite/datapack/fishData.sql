DROP TABLE IF EXISTS `fish_data`;

CREATE TABLE IF NOT EXISTS `fish_data`
(
    `id` INTEGER PRIMARY KEY,
    `itemId` INTEGER,
    `itemName` TEXT,
    `groupId` INTEGER,
    `level` INTEGER,
    `biteRate` REAL,
    `guts` REAL,
    `hp` INTEGER,
    `maxLength` INTEGER,
    `lengthRate` REAL,
    `hpRegeneration` REAL,
    `startCombatTime` INTEGER,
    `combatDuration` INTEGER,
    `gutsCheckTime` INTEGER,
    `gutsCheckProbability` REAL,
    `cheatingProbability` REAL,
    `grade` INTEGER
);