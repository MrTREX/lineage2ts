CREATE TABLE IF NOT EXISTS `versions` (
    `id` TEXT PRIMARY KEY NOT NULL,
    `value` TEXT NOT NULL
);

INSERT OR REPLACE INTO `versions` VALUES
('sql.schema', '26'),
('sql.lastUpdate', strftime('%s', 'now'));

VACUUM;