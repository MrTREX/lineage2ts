DROP TABLE IF EXISTS `npc_routes`;

CREATE TABLE IF NOT EXISTS `npc_routes`
(
    `name` TEXT PRIMARY KEY NOT NULL,
    `type` TEXT NOT NULL,
    `nodes_json` TEXT
);