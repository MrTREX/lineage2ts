DROP TABLE IF EXISTS `product_info`;

CREATE TABLE IF NOT EXISTS `product_info`
(
    `id` INTEGER NOT NULL PRIMARY KEY,
    `category` INTEGER NOT NULL,
    `enabled` INTEGER NOT NULL,
    `price` INTEGER NOT NULL,
    `type` TEXT NOT NULL,
    `items_json` TEXT NOT NULL
);