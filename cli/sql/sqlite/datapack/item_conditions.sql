DROP TABLE IF EXISTS `item_conditions`;

CREATE TABLE IF NOT EXISTS `item_conditions`
(
    `id` INTEGER PRIMARY KEY,
    `definition_json` TEXT,
    `raw` TEXT
);