DROP TABLE IF EXISTS `buylists_data`;

CREATE TABLE IF NOT EXISTS `buylists_data`
(
    `listId` INTEGER PRIMARY KEY,
    `items_json` TEXT,
    `npcId` INTEGER
);