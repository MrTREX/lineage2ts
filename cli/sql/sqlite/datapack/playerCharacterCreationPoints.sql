DROP TABLE IF EXISTS `player_creation_points`;

CREATE TABLE IF NOT EXISTS `player_creation_points` (
   `classId` INTEGER,
   `x` INTEGER,
   `y` INTEGER,
   `z` INTEGER
);