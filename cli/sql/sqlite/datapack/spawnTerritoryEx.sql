DROP TABLE IF EXISTS `spawn_territory_ex`;

CREATE TABLE IF NOT EXISTS `spawn_territory_ex`
(
    `id` TEXT NOT NULL PRIMARY KEY,
    `name` TEXT NOT NULL,
    `points_json` TEXT,
    `minZ` INTEGER,
    `maxZ` INTEGER,
    `geometryPoints` BLOB,
    `parameters_json` TEXT,
    `regionX` INTEGER,
    `regionY` INTEGER
);