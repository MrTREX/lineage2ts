DROP TABLE IF EXISTS `class_list`;

CREATE TABLE IF NOT EXISTS `class_list`
(
    `classId` INTEGER PRIMARY KEY,
    `name` TEXT,
    `parentClassId` INTEGER
);