DROP TABLE IF EXISTS `fishing_monsters`;

CREATE TABLE IF NOT EXISTS `fishing_monsters`
(
    `id` INTEGER PRIMARY KEY,
    `probability` INTEGER,
    `userMinLevel` INTEGER,
    `userMaxLevel` INTEGER
);