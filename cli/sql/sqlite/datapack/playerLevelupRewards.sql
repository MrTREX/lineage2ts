DROP TABLE IF EXISTS `player_levelup_rewards`;

CREATE TABLE IF NOT EXISTS `player_levelup_rewards`
(
    `level` INTEGER,
    `race` TEXT,
    `itemId` INTEGER,
    `itemAmount` INTEGER
);