DROP TABLE IF EXISTS `enchant_item_hpbonus`;

CREATE TABLE IF NOT EXISTS `enchant_item_hpbonus`
(
    `grade` TEXT,
    `values_json` TEXT
);