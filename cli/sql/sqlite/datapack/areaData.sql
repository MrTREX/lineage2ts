DROP TABLE IF EXISTS `areas`;

CREATE TABLE IF NOT EXISTS `areas`
(
    `id` TEXT PRIMARY KEY,
    `type` TEXT,
    `points_json` TEXT,
    `minZ` INTEGER,
    `maxZ` INTEGER,
    `properties_json` TEXT
);