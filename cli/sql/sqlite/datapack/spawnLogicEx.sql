DROP TABLE IF EXISTS `spawn_logic_ex`;

CREATE TABLE IF NOT EXISTS `spawn_logic_ex`
(
    `makerId` TEXT NOT NULL PRIMARY KEY,
    `territories_json` TEXT,
    `logic` TEXT,
    `maxAmount` INTEGER,
    `ai_json` TEXT,
    `flying` INTEGER,
    `eventType` TEXT,
    `avoidTerritories_json` TEXT
);