DROP TABLE IF EXISTS `auction_templates`;

CREATE TABLE IF NOT EXISTS `auction_templates`
(
    `id` INTEGER NOT NULL,
    `type` TEXT NOT NULL,
    `name` TEXT NOT NULL,
    `startingBid` INTEGER NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`,`type`)
);