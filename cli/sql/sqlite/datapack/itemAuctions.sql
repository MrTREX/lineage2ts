DROP TABLE IF EXISTS `item_auctions`;

CREATE TABLE IF NOT EXISTS `item_auctions`
(
    `npcId` INTEGER,
    `auctionId` INTEGER,
    `itemId` INTEGER,
    `amount` INTEGER,
    `initialBid` INTEGER,
    `durationMinutes` INTEGER
);