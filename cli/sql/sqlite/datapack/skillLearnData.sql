DROP TABLE IF EXISTS `skill_learn`;

CREATE TABLE IF NOT EXISTS `skill_learn`
(
    `npcId` INTEGER,
    `classIds_json` TEXT
);