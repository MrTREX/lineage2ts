DROP TABLE IF EXISTS `recipes`;

CREATE TABLE IF NOT EXISTS `recipes`
(
    `id` INTEGER PRIMARY KEY,
    `level` INTEGER,
    `itemId` INTEGER,
    `isCommon` INTEGER,
    `successRate` REAL,
    `inputs_json` TEXT NOT NULL,
    `outputs_json` TEXT NOT NULL,
    `consumeMp` INTEGER,
    `npc_json` TEXT
);