DROP TABLE IF EXISTS `transformations_data`;

CREATE TABLE IF NOT EXISTS `transformations_data`
(
    `id` INTEGER PRIMARY KEY,
    `type` TEXT,
    `canSwim` INTEGER,
    `normalAttackable` INTEGER,
    `spawnHeight` INTEGER,

    `maleCommon_json` TEXT,
    `maleActions` TEXT,
    `maleSkills_json` TEXT,
    `maleAdditionalSkills_json` TEXT,
    `maleItems_json` TEXT,
    `maleLevels_json` TEXT,

    `femaleCommon_json` TEXT,
    `femaleActions` TEXT,
    `femaleSkills_json` TEXT,
    `femaleAdditionalSkills_json` TEXT,
    `femaleItems_json` TEXT,
    `femaleLevels_json` TEXT
);