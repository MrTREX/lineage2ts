DROP TABLE IF EXISTS `option_data`;

CREATE TABLE IF NOT EXISTS `option_data`
(
    `id` INTEGER PRIMARY KEY,
    `for_json` TEXT,
    `active_skill_json` TEXT,
    `passive_skill_json` TEXT,
    `attack_skill_json` TEXT,
    `magic_skill_json` TEXT,
    `critical_skill_json` TEXT
);