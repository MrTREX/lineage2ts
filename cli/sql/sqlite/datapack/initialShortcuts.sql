DROP TABLE IF EXISTS `initial_shortcuts`;

CREATE TABLE IF NOT EXISTS `initial_shortcuts`
(
    `classId` INTEGER,
    `pageId` INTEGER,
    `slot` INTEGER,
    `shortcutType` TEXT,
    `shortcutId` INTEGER,
    `shortcutLevel` INTEGER DEFAULT NULL
);