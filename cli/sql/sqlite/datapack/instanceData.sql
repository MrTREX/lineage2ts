DROP TABLE IF EXISTS `instance_data`;

CREATE TABLE IF NOT EXISTS `instance_data`
(
    `nameId` TEXT PRIMARY KEY,
    `name` TEXT,
    `ejectTime` INTEGER,
    `allowRandomWalk` TEXT,
    `activityTime` INTEGER,
    `allowSummon` TEXT,
    `emptyDestroyTime` INTEGER,
    `exitPoint_json` TEXT,
    `showTimer_json` TEXT,
    `doors_json` TEXT,
    `spawns_json` TEXT,
    `spawnPoints_json` TEXT,
    `reEnter_json` TEXT,
    `removeBuffs_json` TEXT
);
