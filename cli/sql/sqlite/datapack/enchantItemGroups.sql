DROP TABLE IF EXISTS `enchant_item_scroll_groups`;
DROP TABLE IF EXISTS `enchant_rate_groups`;

CREATE TABLE IF NOT EXISTS `enchant_item_scroll_groups`
(
    `rateGroup` TEXT,
    `slot` TEXT,
    `isMagicWeapon` DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS `enchant_rate_groups`
(
    `groupName` TEXT,
    `enchantLevel` TEXT,
    `chance` REAL
);