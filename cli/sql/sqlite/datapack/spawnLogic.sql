DROP TABLE IF EXISTS `spawn_logic`;

CREATE TABLE IF NOT EXISTS `spawn_logic`
(
    `makerId` TEXT NOT NULL PRIMARY KEY,
    `territories_json` TEXT,
    `maxAmount` INTEGER,
    `parameters_json` TEXT
);