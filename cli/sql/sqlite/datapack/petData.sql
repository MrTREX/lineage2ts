DROP TABLE IF EXISTS `pet_data`;

CREATE TABLE IF NOT EXISTS `pet_data`
(
    `id`           INTEGER PRIMARY KEY,
    `itemId`       INTEGER,
    `indexId`      INTEGER,
    `set_json`     TEXT,
    `skills_json`  TEXT,
    `levels_json`  TEXT,
    `minimumLevel` INTEGER
);