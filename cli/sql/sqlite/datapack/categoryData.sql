DROP TABLE IF EXISTS `category_data`;

CREATE TABLE IF NOT EXISTS `category_data`
(
    `name` TEXT,
    `ids_json` TEXT
);