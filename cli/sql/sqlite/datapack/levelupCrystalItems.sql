DROP TABLE IF EXISTS `levelup_crystal_items`;

CREATE TABLE IF NOT EXISTS `levelup_crystal_items`
(
    `itemId` INTEGER PRIMARY KEY,
    `level` INTEGER,
    `nextCrystalItemId` INTEGER
);