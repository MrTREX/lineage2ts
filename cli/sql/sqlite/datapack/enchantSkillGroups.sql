DROP TABLE IF EXISTS `enchant_skill_groups`;

CREATE TABLE IF NOT EXISTS `enchant_skill_groups`
(
    `groupId` INTEGER,
    `level` INTEGER,
    `adena` INTEGER,
    `exp` INTEGER,
    `sp` INTEGER,
    `rates_json` TEXT
);