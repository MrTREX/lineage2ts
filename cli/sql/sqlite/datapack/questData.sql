DROP TABLE IF EXISTS `quest_data`;

CREATE TABLE IF NOT EXISTS `quest_data`
(
    `questId` INTEGER PRIMARY KEY,
    `name` TEXT
);