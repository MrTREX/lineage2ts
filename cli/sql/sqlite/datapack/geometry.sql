DROP TABLE IF EXISTS `geometry`;

CREATE TABLE IF NOT EXISTS `geometry`
(
    `id` TEXT PRIMARY KEY NOT NULL,
    `type` TEXT NOT NULL,
    `size` INTEGER NOT NULL,
    `data` BLOB NOT NULL,
    `parameters_json` TEXT NOT NULL
);