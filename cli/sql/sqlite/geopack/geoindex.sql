DROP TABLE IF EXISTS `geoindex`;

CREATE TABLE IF NOT EXISTS `geoindex`
(
    `regionX` INTEGER,
    `regionY` INTEGER,
    `data` BLOB,
    `statistics_json` TEXT NOT NULL
);