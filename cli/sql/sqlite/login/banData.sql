CREATE TABLE IF NOT EXISTS `ban_data` (
  `name` TEXT NOT NULL PRIMARY KEY,
  `expirationTime` INTEGER
);