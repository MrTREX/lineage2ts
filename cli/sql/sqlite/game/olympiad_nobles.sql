CREATE TABLE IF NOT EXISTS `olympiad_nobles` (
  `charId` INTEGER NOT NULL DEFAULT 0,
  `class_id` INTEGER NOT NULL DEFAULT 0,
  `olympiad_points` INTEGER NOT NULL DEFAULT 0,
  `competitions_done` INTEGER NOT NULL DEFAULT 0,
  `competitions_won` INTEGER NOT NULL DEFAULT 0,
  `competitions_lost` INTEGER NOT NULL DEFAULT 0,
  `competitions_drawn` INTEGER NOT NULL DEFAULT 0,
  `competitions_done_week` INTEGER NOT NULL DEFAULT 0,
  `competitions_done_week_classed` INTEGER NOT NULL DEFAULT 0,
  `competitions_done_week_non_classed` INTEGER NOT NULL DEFAULT 0,
  `competitions_done_week_team` INTEGER NOT NULL DEFAULT 0,
  PRIMARY KEY (`charId`)
);