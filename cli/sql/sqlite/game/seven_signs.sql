CREATE TABLE IF NOT EXISTS `seven_signs` (
  `objectId` INTEGER NOT NULL,
  `side` TEXT NOT NULL,
  `seal` TEXT NOT NULL,
  `redStones` INTEGER NOT NULL DEFAULT 0,
  `greenStones` INTEGER NOT NULL DEFAULT 0,
  `blueStones` INTEGER NOT NULL DEFAULT 0,
  `ancientAdenaAmount` NUMERIC NOT NULL DEFAULT 0,
  `contributionScore` NUMERIC NOT NULL DEFAULT 0,
  `contributionTime` NUMERIC NOT NULL DEFAULT 0,
  `joinTime` NUMERIC NOT NULL DEFAULT 0,
  PRIMARY KEY (`objectId`)
);

CREATE INDEX IF NOT EXISTS `seven_signs_side_seal` ON `seven_signs` (side, seal);