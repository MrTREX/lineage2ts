CREATE TABLE IF NOT EXISTS `nevit_data` (
   `objectId` INTEGER NOT NULL DEFAULT 0,
   `huntingTime` INTEGER NOT NULL DEFAULT 0,
   `effectEndTime` INTEGER NOT NULL DEFAULT 0,
   `huntingPoints` INTEGER NOT NULL DEFAULT 0,
   PRIMARY KEY (`objectId`)
);