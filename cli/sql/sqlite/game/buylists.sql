CREATE TABLE IF NOT EXISTS `buylists` (
	`buylist_id` INTEGER,
	`item_id` INTEGER,
	`count` INTEGER NOT NULL DEFAULT 0,
	`next_restock_time` INTEGER NOT NULL DEFAULT 0,
	PRIMARY KEY (`buylist_id`, `item_id`)
);