CREATE TABLE IF NOT EXISTS `character_shortcuts` (
  `charId` INTEGER NOT NULL DEFAULT 0,
  `slot` INTEGER NOT NULL DEFAULT 0,
  `page` INTEGER NOT NULL DEFAULT 0,
  `type` INTEGER,
  `shortcut_id` INTEGER,
  `level` INTEGER,
  `class_index` INTEGER NOT NULL DEFAULT 0,
  PRIMARY KEY (`charId`,`slot`,`page`,`class_index`,`shortcut_id`)
);