CREATE TABLE IF NOT EXISTS `heroes` (
  `charId` INTEGER NOT NULL DEFAULT 0,
  `class_id` NUMERIC NOT NULL DEFAULT 0,
  `count` NUMERIC NOT NULL DEFAULT 0,
  `played` NUMERIC NOT NULL DEFAULT 0,
  `claimed` TEXT CHECK( claimed IN ('true','false') ) NOT NULL DEFAULT 'false',
  `message` TEXT NOT NULL DEFAULT '',
  PRIMARY KEY (`charId`)
);