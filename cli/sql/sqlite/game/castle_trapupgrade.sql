CREATE TABLE IF NOT EXISTS `castle_trapupgrade` (
  `castleId` INTEGER NOT NULL DEFAULT 0,
  `towerIndex` INTEGER NOT NULL DEFAULT 0,
  `level` INTEGER NOT NULL DEFAULT 0,
  PRIMARY KEY (`towerIndex`,`castleId`)
);