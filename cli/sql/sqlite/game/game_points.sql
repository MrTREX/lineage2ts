CREATE TABLE IF NOT EXISTS `game_points` (
    `id` TEXT NOT NULL,
    `type` INTEGER NOT NULL,
    `amount` INTEGER NOT NULL,
    'lastUpdate' INTEGER NOT NULL,
    `reason` TEXT,
    PRIMARY KEY(`id`,`type`)
);