CREATE TABLE IF NOT EXISTS `bot_reported_char_data` (
	`botId` INTEGER NOT NULL DEFAULT 0,
	`reporterId` INTEGER NOT NULL DEFAULT 0,
	`reportDate` INTEGER NOT NULL DEFAULT 0,
	PRIMARY KEY (`botId`, `reporterId`)
);