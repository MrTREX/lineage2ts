CREATE TABLE IF NOT EXISTS `item_elementals` (
  `objectId` INTEGER NOT NULL DEFAULT 0,
  `type` INTEGER NOT NULL DEFAULT -1,
  `value` INTEGER NOT NULL DEFAULT -1,
  PRIMARY KEY (`objectId`, `type`)
);