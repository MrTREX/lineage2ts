CREATE TABLE IF NOT EXISTS `character_hennas` (
  `objectId` INTEGER NOT NULL DEFAULT 0,
  `classIndex` INTEGER NOT NULL DEFAULT 0,
  `symbolId` INTEGER,
  `slot` INTEGER NOT NULL DEFAULT 0,
  PRIMARY KEY (`objectId`,`classIndex`)
);