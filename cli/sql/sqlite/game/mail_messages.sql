CREATE TABLE IF NOT EXISTS `mail_messages` (
  `messageId` INTEGER NOT NULL PRIMARY KEY,
  `senderId` INTEGER NOT NULL DEFAULT 0,
  `receiverId` INTEGER NOT NULL DEFAULT 0,
  `subject` TEXT,
  `content` TEXT,
  `expirationTime` INTEGER NOT NULL DEFAULT 0,
  `creationTime` INTEGER NOT NULL DEFAULT 0,
  `codAdena` INTEGER NOT NULL DEFAULT 0,
  `attachmentStatus` INTEGER NOT NULL DEFAULT 0,
  `returnStatus` INTEGER NOT NULL DEFAULT 0,
  `messageStatus` INTEGER NOT NULL DEFAULT 0,
  `sendBySystem` INTEGER NOT NULL DEFAULT 0
);

CREATE INDEX IF NOT EXISTS `mail_messages_sender` ON `mail_messages` (senderId);
CREATE INDEX IF NOT EXISTS `mail_messages_receiver` ON `mail_messages` (receiverId);
CREATE INDEX IF NOT EXISTS `mail_messages_expiration` ON `mail_messages` (expirationTime);