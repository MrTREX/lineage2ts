CREATE TABLE IF NOT EXISTS `teleport_bookmarks` (
  `playerId` INTEGER NOT NULL,
  `id` INTEGER NOT NULL,
  `x` INTEGER NOT NULL,
  `y` INTEGER NOT NULL,
  `z` INTEGER NOT NULL,
  `heading` INTEGER NOT NULL,
  `icon` INTEGER NOT NULL,
  `tag` TEXT DEFAULT NULL,
  `name` TEXT NOT NULL,
  PRIMARY KEY (`playerId`,`id`)
);