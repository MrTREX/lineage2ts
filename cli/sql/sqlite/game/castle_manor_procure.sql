CREATE TABLE IF NOT EXISTS `castle_manor_procure` (
 `castle_id` INTEGER NOT NULL DEFAULT 0,
 `crop_id` INTEGER NOT NULL DEFAULT 0,
 `amount` INTEGER NOT NULL DEFAULT 0,
 `start_amount` INTEGER NOT NULL DEFAULT 0,
 `price` INTEGER NOT NULL DEFAULT 0,
 `reward_type` INTEGER NOT NULL DEFAULT 0,
 `next_period` INTEGER NOT NULL DEFAULT '1',
  PRIMARY KEY (`castle_id`,`crop_id`,`next_period`)
);