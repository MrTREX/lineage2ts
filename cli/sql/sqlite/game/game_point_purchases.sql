CREATE TABLE IF NOT EXISTS `game_point_purchases` (
    `accountName` TEXT NOT NULL,
    `type` INTEGER NOT NULL,
    `productId` INTEGER NOT NULL,
    `amount` INTEGER NOT NULL,
    `paidPrice` INTEGER NOT NULL,
    `time` INTEGER NOT NULL,
    `category` INTEGER NOT NULL,
    `playerId` INTEGER NOT NULL
);

CREATE INDEX IF NOT EXISTS `game_point_purchases_account` ON `game_point_purchases` (accountName);