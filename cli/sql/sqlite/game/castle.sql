CREATE TABLE IF NOT EXISTS `castle` (
  `id` INTEGER NOT NULL DEFAULT 0,
  `name` TEXT NOT NULL,
  `taxPercent` INTEGER NOT NULL DEFAULT 15,
  `treasury` INTEGER NOT NULL DEFAULT 0,
  `siegeDate` INTEGER NOT NULL DEFAULT 0,
  `regTimeOver` TEXT CHECK( regTimeOver IN ('true','false') ) NOT NULL DEFAULT 'true',
  `regTimeEnd` INTEGER NOT NULL DEFAULT 0,
  `showNpcCrest` TEXT CHECK( showNpcCrest IN ('true','false') ) NOT NULL DEFAULT 'false',
  `ticketBuyCount` INTEGER NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`,`name`)
);

INSERT OR IGNORE INTO `castle` VALUES
(1,'Gludio',0,0,0,'true',0,'false',0),
(2,'Dion',0,0,0,'true',0,'false',0),
(3,'Giran',0,0,0,'true',0,'false',0),
(4,'Oren',0,0,0,'true',0,'false',0),
(5,'Aden',0,0,0,'true',0,'false',0),
(6,'Innadril',0,0,0,'true',0,'false',0),
(7,'Goddard',0,0,0,'true',0,'false',0),
(8,'Rune',0,0,0,'true',0,'false',0),
(9,'Schuttgart',0,0,0,'true',0,'false',0);