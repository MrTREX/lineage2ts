CREATE TABLE IF NOT EXISTS `punishments` (
  `id` INTEGER PRIMARY KEY AUTOINCREMENT,
  `key` TEXT NOT NULL,
  `affect` TEXT NOT NULL,
  `type` TEXT NOT NULL,
  `expiration`  INTEGER NOT NULL,
  `reason` TEXT NOT NULL,
  `punishedBy` TEXT NOT NULL
) ;