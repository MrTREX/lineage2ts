CREATE TABLE IF NOT EXISTS `player_variables` (
  `playerId` INTEGER PRIMARY KEY NOT NULL,
  `value_json` TEXT NOT NULL
);