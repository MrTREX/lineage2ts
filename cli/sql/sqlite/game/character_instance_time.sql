CREATE TABLE IF NOT EXISTS `character_instance_time` (
  `charId` INTEGER NOT NULL DEFAULT 0,
  `instanceId` INTEGER NOT NULL DEFAULT 0,
  `time` INTEGER NOT NULL DEFAULT 0,
  PRIMARY KEY (`charId`,`instanceId`)
) ;