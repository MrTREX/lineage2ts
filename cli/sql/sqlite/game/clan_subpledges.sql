CREATE TABLE IF NOT EXISTS `clan_subpledges` (
  `clan_id` INTEGER NOT NULL DEFAULT 0,
  `sub_pledge_id` INTEGER NOT NULL DEFAULT 0,
  `name` TEXT,
  `leader_id` INTEGER NOT NULL DEFAULT 0,
  PRIMARY KEY (`clan_id`,`sub_pledge_id`, `leader_id`)
) ;