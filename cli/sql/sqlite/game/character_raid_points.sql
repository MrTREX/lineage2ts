CREATE TABLE IF NOT EXISTS `character_raid_points` (
  `charId` INTEGER NOT NULL DEFAULT 0,
  `boss_id` INTEGER NOT NULL DEFAULT 0,
  `points` INTEGER NOT NULL DEFAULT 0,
  PRIMARY KEY (`charId`,`boss_id`)
);