CREATE TABLE IF NOT EXISTS `petition_feedback` (
  `charName` TEXT NOT NULL,
  `gmName`  TEXT NOT NULL,
  `rate` INTEGER NOT NULL DEFAULT 2,
  `message` TEXT NOT NULL,
  `date` INTEGER NOT NULL DEFAULT 0
);