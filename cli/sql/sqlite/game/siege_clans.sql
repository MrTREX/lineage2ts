CREATE TABLE IF NOT EXISTS `siege_clans` (
   `castle_id` INTEGER NOT NULL DEFAULT 0,
   `clan_id` INTEGER NOT NULL DEFAULT 0,
   `type` INTEGER DEFAULT NULL,
   `castle_owner` INTEGER DEFAULT NULL,
   PRIMARY KEY (`clan_id`,`castle_id`)
);