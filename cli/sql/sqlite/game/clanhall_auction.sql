CREATE TABLE IF NOT EXISTS `clanhall_auction` (
  `id` INTEGER NOT NULL DEFAULT 0,
  `sellerId` INTEGER NOT NULL DEFAULT 0,
  `sellerName` TEXT NOT NULL DEFAULT 'NPC',
  `sellerClanName` TEXT NOT NULL DEFAULT '',
  `itemType` TEXT NOT NULL DEFAULT '',
  `itemId` INTEGER NOT NULL DEFAULT 0,
  `itemObjectId` INTEGER NOT NULL DEFAULT 0,
  `itemName` TEXT NOT NULL DEFAULT '',
  `itemQuantity` INTEGER NOT NULL DEFAULT 0,
  `startingBid` INTEGER NOT NULL DEFAULT 0,
  `currentBid` INTEGER NOT NULL DEFAULT 0,
  `endDate` INTEGER NOT NULL DEFAULT 0,
  PRIMARY KEY (`itemType`,`itemId`,`itemObjectId`)
);