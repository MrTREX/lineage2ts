CREATE TABLE IF NOT EXISTS `cursed_weapons` (
  `itemId` INTEGER,
  `charId` INTEGER NOT NULL DEFAULT 0,
  `playerKarma` INTEGER DEFAULT 0,
  `playerPkKills` INTEGER DEFAULT 0,
  `nbKills` INTEGER DEFAULT 0,
  `endTime` INTEGER NOT NULL DEFAULT 0,
  PRIMARY KEY (`itemId`, `charId`)
);