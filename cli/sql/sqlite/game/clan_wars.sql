CREATE TABLE IF NOT EXISTS `clan_wars` (
  `clan1` INTEGER NOT NULL,
  `clan2` INTEGER NOT NULL,
  `wantspeace1` NUMERIC NOT NULL DEFAULT 0,
  `wantspeace2` NUMERIC NOT NULL DEFAULT 0,
  PRIMARY KEY (`clan1`, `clan2`)
);