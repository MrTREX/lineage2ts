CREATE TABLE IF NOT EXISTS `airships` (
  `owner_id` INTEGER, -- object id of the player or clan, owner of this airship
  `fuel` NUMERIC NOT NULL DEFAULT 600,
  PRIMARY KEY (`owner_id`)
);