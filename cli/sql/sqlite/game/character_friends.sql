CREATE TABLE IF NOT EXISTS `character_friends` (
  `objectId` INTEGER NOT NULL DEFAULT 0,
  `friendId` INTEGER NOT NULL DEFAULT 0,
  `relationType` INTEGER NOT NULL DEFAULT 0,
  PRIMARY KEY (`objectId`,`friendId`)
);