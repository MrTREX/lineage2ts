CREATE TABLE IF NOT EXISTS `characters` (
  `account_name` TEXT DEFAULT NULL,
  `charId` INTEGER NOT NULL DEFAULT 0,
  `char_name` TEXT NOT NULL,
  `level` INTEGER DEFAULT NULL,
  `maxHp` INTEGER DEFAULT NULL,
  `curHp` INTEGER DEFAULT NULL,
  `maxCp` INTEGER DEFAULT NULL,
  `curCp` INTEGER DEFAULT NULL,
  `maxMp` INTEGER DEFAULT NULL,
  `curMp` INTEGER DEFAULT NULL,
  `face` INTEGER DEFAULT NULL,
  `hairStyle` INTEGER DEFAULT NULL,
  `hairColor` INTEGER DEFAULT NULL,
  `sex` INTEGER DEFAULT NULL,
  `heading` INTEGER DEFAULT NULL,
  `x` INTEGER DEFAULT NULL,
  `y` INTEGER DEFAULT NULL,
  `z` INTEGER DEFAULT NULL,
  `exp` INTEGER DEFAULT 0,
  `expBeforeDeath` INTEGER DEFAULT 0,
  `sp` INTEGER NOT NULL DEFAULT 0,
  `karma` INTEGER DEFAULT NULL,
  `fame` INTEGER NOT NULL DEFAULT 0,
  `pvpkills` INTEGER DEFAULT NULL,
  `pkkills` INTEGER DEFAULT NULL,
  `clanid` INTEGER DEFAULT NULL,
  `race` INTEGER DEFAULT NULL,
  `classid` INTEGER DEFAULT NULL,
  `base_class` INTEGER NOT NULL DEFAULT 0,
  `transform_id` INTEGER NOT NULL DEFAULT 0,
  `deletetime` INTEGER NOT NULL DEFAULT 0,
  `cancraft` INTEGER DEFAULT NULL,
  `title` TEXT DEFAULT NULL,
  `title_color` INTEGER NOT NULL DEFAULT 0xECF9A2,
  `accesslevel` INTEGER DEFAULT 0,
  `online` INTEGER DEFAULT NULL,
  `onlinetime` INTEGER DEFAULT NULL,
  `char_slot` INTEGER DEFAULT NULL,
  `lastAccess` INTEGER NOT NULL DEFAULT 0,
  `clan_privs` INTEGER DEFAULT 0,
  `wantspeace` INTEGER DEFAULT 0,
  `isin7sdungeon` INTEGER NOT NULL DEFAULT 0,
  `power_grade` INTEGER DEFAULT NULL,
  `nobless` INTEGER NOT NULL DEFAULT 0,
  `subpledge` INTEGER NOT NULL DEFAULT 0,
  `lvl_joined_academy` INTEGER NOT NULL DEFAULT 0,
  `apprentice`INTEGER NOT NULL DEFAULT 0,
  `sponsor` INTEGER NOT NULL DEFAULT 0,
  `clan_join_expiry_time` INTEGER NOT NULL DEFAULT 0,
  `clan_create_expiry_time` INTEGER NOT NULL DEFAULT 0,
  `death_penalty_level` INTEGER NOT NULL DEFAULT 0,
  `bookmarkslot` INTEGER NOT NULL DEFAULT 0,
  `vitality_points` INTEGER NOT NULL DEFAULT 0,
  `createDate` NUMERIC NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `language` TEXT DEFAULT NULL,
  PRIMARY KEY (`charId`, `account_name`, `char_name`, `clanid`, `online`)
) ;