DROP TABLE IF EXISTS `siegable_clanhall`;

CREATE TABLE IF NOT EXISTS `siegable_clanhall` (
  `hallId` INTEGER NOT NULL DEFAULT 0,
  `name` TEXT DEFAULT NULL,
  `ownerId` INTEGER DEFAULT NULL,
  `description` TEXT DEFAULT NULL,
  `location` TEXT DEFAULT NULL,
  `nextSiege` INTEGER DEFAULT NULL,
  `duration` INTEGER DEFAULT NULL,
  `siegeStartCron` TEXT DEFAULT NULL,
  PRIMARY KEY (`hallId`, `ownerId`)
);

INSERT OR IGNORE INTO `siegable_clanhall` (`hallId`, `name`, `ownerId`, `description`, `location`, `nextSiege`, `duration`, `siegeStartCron`) VALUES
 (21, 'Fortress of Resistance', 0, 'Contestable Clan Hall', 'Dion', 0, 3600000, '0 12 */14 * *'),
 (34, 'Devastated Castle', 0, 'Contestable Clan Hall', 'Aden', 0, 3600000, '0 12 */14 * *'),
 (35, 'Bandit Stronghold', 0, 'Contestable Clan Hall', 'Oren', 0, 3600000, '0 12 */14 * *'),
 (62, 'Rainbow Springs', 0, 'Contestable Clan Hall', 'Goddard', 0, 3600000, '0 12 */14 * *'),
 (63, 'Beast Farm', 0, 'Contestable Clan Hall', 'Rune', 0, 3600000, '0 12 */14 * *'),
 (64, 'Fortress of the Dead', 0, 'Contestable Clan Hall', 'Rune', 0, 3600000, '0 12 */14 * *');