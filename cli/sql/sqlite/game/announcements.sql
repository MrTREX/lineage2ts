DROP TABLE IF EXISTS `announcements`;

CREATE TABLE IF NOT EXISTS `announcements` (
  `id` INTEGER PRIMARY KEY AUTOINCREMENT,
  `type` INTEGER NOT NULL,
  `initial` INTEGER NOT NULL DEFAULT 0,
  `delay` INTEGER NOT NULL DEFAULT 0,
  `repeat` INTEGER NOT NULL DEFAULT 0,
  `author` TEXT NOT NULL,
  `content` TEXT NOT NULL
);

INSERT INTO announcements (`type`, `author`, `content`) VALUES 
(0, 'L2NodeTS', 'Thanks for using L2NodeTS.'),
(1, 'L2NodeTS', 'Lineage2 Nodejs server based on https://gitlab.com/MrTREX/lineage2ts project.');