CREATE TABLE IF NOT EXISTS `clanhall_auction_bid` (
  `id` INTEGER NOT NULL DEFAULT 0,
  `auctionId` INTEGER NOT NULL DEFAULT 0,
  `bidderId` INTEGER NOT NULL DEFAULT 0,
  `bidderName` TEXT NOT NULL,
  `clan_name` TEXT NOT NULL,
  `maxBid` INTEGER NOT NULL DEFAULT 0,
  `time_bid` INTEGER NOT NULL DEFAULT 0,
  PRIMARY KEY  (`auctionId`,`bidderId`)
);