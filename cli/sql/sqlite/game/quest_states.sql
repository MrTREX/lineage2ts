CREATE TABLE IF NOT EXISTS `quest_states` (
  `objectId` INTEGER NOT NULL DEFAULT 0,
  `questName` TEXT NOT NULL DEFAULT '',
  `variables_json`  TEXT NOT NULL DEFAULT '{}',
  `condition` INTEGER DEFAULT -1,
  `stateFlags` INTEGER DEFAULT -1,
  `completionState` INTEGER DEFAULT 0,
  PRIMARY KEY (`objectId`,`questName`)
);