CREATE TABLE IF NOT EXISTS `character_recipeshoplist` (
  `charId` INTEGER NOT NULL DEFAULT 0,
  `recipeId` INTEGER NOT NULL DEFAULT 0,
  `price` INTEGER NOT NULL DEFAULT 0,
  `index` INTEGER NOT NULL DEFAULT 0,
  PRIMARY KEY (`charId`,`recipeId`)
);