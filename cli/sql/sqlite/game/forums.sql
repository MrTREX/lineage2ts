DROP TABLE IF EXISTS `forums`;
CREATE TABLE IF NOT EXISTS `forums` (
  `forum_id` INTEGER NOT NULL,
  `forum_name` TEXT NOT NULL DEFAULT '',
  `forum_parent` INTEGER NOT NULL DEFAULT 0,
  `forum_post` INTEGER NOT NULL DEFAULT 0,
  `forum_type` INTEGER NOT NULL DEFAULT 0,
  `forum_perm` INTEGER NOT NULL DEFAULT 0,
  `forum_owner_id` INTEGER NOT NULL DEFAULT 0,
  PRIMARY KEY (`forum_id`,`forum_owner_id`)
);

INSERT OR IGNORE INTO `forums` VALUES
(1, 'NormalRoot', 0, 0, 0, 1, 0),
(2, 'ClanRoot', 0, 0, 0, 0, 0),
(3, 'MemoRoot', 0, 0, 0, 0, 0),
(4, 'MailRoot', 0, 0, 0, 0, 0);