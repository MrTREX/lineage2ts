CREATE TABLE IF NOT EXISTS `character_skills_save` (
  `charId` INTEGER NOT NULL DEFAULT 0,
  `skill_id` INTEGER NOT NULL DEFAULT 0,
  `skill_level` INTEGER NOT NULL DEFAULT 1,
  `remaining_time` INTEGER NOT NULL DEFAULT 0,
  `reuse_delay` INTEGER NOT NULL DEFAULT 0,
  `systime` INTEGER NOT NULL DEFAULT 0,
  `restore_type` INTEGER NOT NULL DEFAULT 0,
  `class_index` INTEGER NOT NULL DEFAULT 0,
  `buff_index` INTEGER NOT NULL DEFAULT 0,
  PRIMARY KEY (`charId`,`skill_id`,`skill_level`,`class_index`)
);