CREATE TABLE IF NOT EXISTS `character_skills` (
  `charId` INTEGER NOT NULL DEFAULT 0,
  `skill_id` INTEGER NOT NULL DEFAULT 0,
  `skill_level` INTEGER NOT NULL DEFAULT 1,
  `class_index` INTEGER NOT NULL DEFAULT 0,
  PRIMARY KEY (`charId`,`skill_id`,`class_index`)
);