#!/bin/bash

mkdir working
cd working || exit

curl -OL https://bitbucket.org/l2jserver/l2j-server-datapack/get/develop.zip

echo "Downloaded zip file. Unzipping..."
7z x develop.zip

echo "Copying datapack files"
# copy various xml data files
cp -r l2jserver-l2j-server-datapack*/src/main/resources/data ../data
# copy java files with html, since L2J has html files mixed with Java sources in same directory
cp -r l2jserver-l2j-server-datapack*/src/main/java/com/l2jserver/datapack ../data
cd ..

echo "Creating artifact files"
npm run cli -- --provision-datapack --provision-database

mv datapack.database ../datapack.database
mv game.database ../game.database
mv login.database ../login.database