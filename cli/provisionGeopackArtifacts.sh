#!/bin/bash

mkdir working-geo
cd working-geo || exit

curl -OL https://bitbucket.org/l2jgeo/l2j_geodata/get/master.zip

echo "Downloaded zip file. Unzipping..."
7z x master.zip

echo "Copying geodata files"
cp -r l2jgeo-l2j_geodata*/geodata/ ..
cd ..

echo "Creating geopack"
npm run geopack

mv geopack.database ../geopack.database