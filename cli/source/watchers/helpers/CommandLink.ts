import { createSocket, Socket } from 'nanomsg'
import { Packr } from 'msgpackr'
import { CommandLinkInput } from '../enums/CommandLinkParameters'

const packOperation = new Packr()

export class CommandLink {
    socket: Socket

    constructor( address: string ) {
        this.socket = createSocket( 'pair', {
            tcpnodelay: true
        } )

        this.socket.connect( address )
        this.socket.on( 'error', this.onError.bind( this ) )
    }

    /*
        We need to ensure that if we cannot connect we can simply ignore requests to send
        data through sockets.
     */
    onError() : void {
        this.socket.close()
        this.socket = null
    }

    sendData( data: CommandLinkInput ) : void {
        if ( !this.socket ) {
            return
        }

        this.socket.send( packOperation.pack( data ) )
    }

    shutdown() : void {
        this.socket.close()
        this.socket = null
    }
}
