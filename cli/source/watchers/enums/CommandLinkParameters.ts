export enum CommandLinkType {
    admin,
    inquiry
}

export enum CommandLinkStatus {
    Error,
    Success
}

export interface CommandLinkInput {
    type: CommandLinkType
    command: string

    /*
        Used as means of supplying admin player name, or reply id in case of inquiry
     */
    replyId: string
}

export interface CommandLinkOutput {
    data: Array<Object> | Object
    status: CommandLinkStatus
    replyId: string
}