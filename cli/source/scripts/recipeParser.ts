import readline from 'readline'
import fs from 'fs'
import { format } from '@fast-csv/format'
import pkgDir from 'pkg-dir'

const rootDirectory = pkgDir.sync( __dirname )

interface RecipeDefinition {
    id: string
    itemId: string
    level: string
    common: string
    consumeMp: string
    success: string
    inputs: string // input format per ingredient is referenceId:amount|referenceId:amount
    outputs: string // output format is referenceId:amount:chance|referenceId:amount:chance
    npcFee: string // input format per item is referenceId:amount|referenceId:amount
}

function cleanValue( value: string, first: string = '[', second: string = ']' ): string {
    return value.replaceAll( first, '' ).replaceAll( second, '' )
}

function createRecipe( chunks: Array<string> ) : RecipeDefinition {
    let [ unused, name, id, ...rest ] = chunks

    const startingRecipe : RecipeDefinition = {
        id,
        itemId: '',
        level: '',
        common: '',
        success: '',
        consumeMp: '',
        inputs: '',
        npcFee: '',
        outputs: ''
    }

    return rest.reduce( ( recipe: RecipeDefinition, line: string ) : RecipeDefinition => {
        let [ name, value ] = line.split( '=' )

        if ( name && value ) {
            switch ( name ) {
                case 'level':
                    recipe.level = value
                    break

                case 'mp_consume':
                    recipe.consumeMp = value
                    break

                case 'item_id':
                    recipe.itemId = value
                    break

                case 'iscommonrecipe':
                    recipe.common = value
                    break

                case 'material':
                    recipe.inputs = cleanValue( cleanValue( value.replaceAll( '};{', '|' ) ), '{', '}' )
                    break

                case 'product':
                    recipe.outputs = cleanValue( cleanValue( value.replaceAll( '};{', '|' ) ), '{', '}' )
                    break

                case 'success_rate':
                    if ( value === '100' ) {
                        recipe.success = undefined
                        break
                    }

                    recipe.success = value
                    break

                case 'npc_fee':
                    recipe.npcFee = cleanValue( cleanValue( value.replaceAll( '};{', '|' ) ), '{', '}' )
                    break
            }
        }

        return recipe
    }, startingRecipe )
}

async function processFile(): Promise<void> {
    const lines = readline.createInterface( {
        input: fs.createReadStream( `${ rootDirectory }/recipe.txt` ),
        crlfDelay: Infinity
    } )

    const recipeStream = fs.createWriteStream( `${ rootDirectory }/recipes.csv` )

    const recipesCsv = format( {
        delimiter: ',',
        headers: true,
        quote: false
    } )

    recipesCsv.pipe( recipeStream )

    const allRecipes: Array<RecipeDefinition> = []

    for await ( const line of lines ) {
        if ( !line || line.startsWith( '//' ) ) {
            continue
        }

        const chunks = line.split( '\t' )

        if ( chunks[ 0 ] === 'recipe_begin' ) {
            allRecipes.push( createRecipe( chunks ) )
        }
    }


    allRecipes.forEach( ( item: RecipeDefinition ) => {
        recipesCsv.write( item )
    } )

    recipesCsv.end()
}

processFile()