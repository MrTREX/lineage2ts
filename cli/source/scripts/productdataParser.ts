import readline from 'readline'
import fs from 'fs'
import { format } from '@fast-csv/format'
import pkgDir from 'pkg-dir'

const rootDirectory = pkgDir.sync( __dirname )

interface ProductInfo {
    id: string
    name: string
    category: string
    price: string
    productType: string
    enabled: string
    items: string
}

function cleanValue( value: string, first: string = '{', second: string = '}' ): string {
    return value.replaceAll( first, '' ).replaceAll( second, '' )
}

function createProduct( chunks: Array<string> ) : ProductInfo {
    const defaultProduct : ProductInfo = {
        id: '',
        name: '',
        category: '',
        price: '',
        productType: 'none',
        enabled: '',
        items: ''
    }

    return chunks.reduce( ( product: ProductInfo, line: string ) : ProductInfo => {
        let [ name, value ] = line.split( '=' )

        if ( name && value ) {
            switch ( name ) {
                case 'id':
                    product.id = value
                    break

                case 'name':
                    product.name = cleanValue( value )
                    break

                case 'category':
                    product.category = value
                    break

                case 'price':
                    product.price = value
                    break

                /*
                    There are three properties that set a flag for product type,
                    however on server side it is converted as integer, meaning
                    that only single type is permitted, no overlapping types possible.
                 */
                case 'is_event_product':
                    if ( value === '1' ) {
                        product.productType = 'event'
                    }
                    break

                case 'is_best_product':
                    if ( value === '1' ) {
                        product.productType = 'best'
                    }
                    break

                case 'is_new_product':
                    if ( value === '1' ) {
                        product.productType = 'new'
                    }
                    break

                case 'buyable':
                    product.enabled = value
                    break

                case 'itemids':
                    product.items = cleanValue( value.replaceAll( '};{', '|' ) )
                    break
            }
        }

        return product
    }, defaultProduct )
}

async function processFile(): Promise<void> {
    const lines = readline.createInterface( {
        input: fs.createReadStream( `${ rootDirectory }/productdata.txt` ),
        crlfDelay: Infinity
    } )

    const recipeStream = fs.createWriteStream( `${ rootDirectory }/productInfo.csv` )

    const recipesCsv = format( {
        delimiter: ',',
        headers: true,
        quote: false
    } )

    recipesCsv.pipe( recipeStream )

    const allProducts: Array<ProductInfo> = []

    for await ( const line of lines ) {
        if ( !line || line.startsWith( '//' ) ) {
            continue
        }

        const chunks = line.split( '\t' )

        if ( chunks[ 0 ] === 'product_begin' ) {
            allProducts.push( createProduct( chunks ) )
        }
    }


    allProducts.forEach( ( item: ProductInfo ) => {
        recipesCsv.write( item )
    } )

    recipesCsv.end()
}

processFile()