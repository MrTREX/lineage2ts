import readline from 'readline'
import fs from 'fs'
import { format } from '@fast-csv/format'
import pkgDir from 'pkg-dir'
import _ from 'lodash'

const rootDirectory = pkgDir.sync( __dirname )

function cleanValue( value: string, first: string = '[', second: string = ']' ): string {
    return value.replaceAll( first, '' ).replaceAll( second, '' )
}

function createItem( chunks: Array<string>, headers: Set<string> ): Object {
    let [ type, itemId, referenceIdValue, ...properties ] = chunks

    headers.add( 'itemId' )
    headers.add( 'referenceId' )
    headers.add( 'type' )

    let item : Object = {
        itemId,
        referenceId: cleanValue( referenceIdValue ),
        type
    }

    return properties.reduce( ( definition: Object, propertyLine: string ): Object => {
        let [ name, value ] = propertyLine.split( '=' ).map( value => value.trim() ) as [ string, string ]

        if ( name && value ) {
            switch ( name ) {
                case 'capsuled_items':
                case 'use_condition':
                case 'equip_condition':
                    value = cleanValue( cleanValue( value.replaceAll( '};', '}|' ), '{', '}' ), '[', ']' )

                    if ( value === '[]' ) {
                        value = null
                    }
                    break

                case 'html':
                    value = cleanValue( value )
                    if ( value === 'item_default.htm' ) {
                        value = null
                    }

                    break

                case 'base_attribute_defend':
                    if ( value === '{0;0;0;0;0;0}' ) {
                        value = null
                    }

                    break

                case 'base_attribute_attack':
                    if ( value === '{none;0}' ) {
                        value = null
                    }

                    break

                case 'consume_type':
                    value = value.replace( 'consume_type_','' )

                    if ( value === 'normal' ) {
                        value = null
                    }

                    break

                case 'default_action':
                    value = value.replace( 'action_','' )

                    if ( value === 'none' ) {
                        value = null
                    }

                    break

                case 'duration':
                case 'delay_share_group':
                case 'durability':
                    if ( value === '-1' ) {
                        value = null
                    }

                    break

                case 'soulshot_count':
                case 'spiritshot_count':
                case 'crystal_count':
                case 'use_skill_distime':
                case 'hit_modify':
                case 'avoid_modify':
                case 'shield_defense':
                case 'shield_defense_rate':
                case 'attack_range':
                case 'attack_speed':
                case 'reuse_delay':
                case 'mp_consume':
                case 'magical_damage':
                case 'physical_defense':
                case 'physical_damage':
                case 'magical_defense':
                case 'mp_bonus':
                case 'for_npc':
                case 'is_premium':
                case 'can_move':
                case 'equip_reuse_delay':
                case 'price':
                case 'default_price':
                case 'critical':
                case 'random_damage':
                case 'dual_fhit_rate':
                case 'enchant_enable':
                case 'elemental_enable':
                case 'enchanted':
                case 'period':
                case 'weight':
                    if ( value === '0' ) {
                        value = null
                    }

                    break

                /*
                    Fields that either have single value,
                    or have no known functionality associated
                    with item properties.
                 */
                case 'initial_count':
                case 'blessed':
                case 'drop_period':
                case 'damaged':
                    value = null
                    break

                case 'recipe_id':
                    if ( value === '0' ) {
                        value = null
                    }

                    break

                /*
                    keep_type:
                    - bitmask for various warehouse-like operations
                    - none = 0
                    - personal warehouse = 1
                    - clan warehouse = 2
                    - castle = 4
                    - all others ( dimensional merchant, etc.) = 8
                 */
                default:
                    value = cleanValue( cleanValue( value, '{', '}' ), '[', ']' ).replaceAll( '@', '' )

                    if ( value === 'none' ) {
                        value = null
                    }

                    break
            }

            if ( value ) {
                name = _.camelCase( name )
                definition[ name ] = value
                headers.add( name )
            }
        }

        return definition
    }, item )
}

function createSet( chunks: Array<string>, headers: Set<string> ): Object {
    let [ id, ...properties ] = chunks

    headers.add( 'id' )

    let itemSet : Object = {
        id
    }

    return properties.reduce( ( definition: Object, propertyLine: string ): Object => {
        let [ name, value ] = propertyLine.split( '=' ).map( value => value.trim() ) as [ string, string ]

        if ( name && value ) {
            switch ( name ) {
                /*
                    Normal data has set_skill parameter always specified as 's_set_collected',
                    hence there is no need to include such parameter in overall data set.
                 */
                case 'set_skill':
                    value = null
                    break

                case 'str_inc':
                case 'con_inc':
                case 'dex_inc':
                case 'int_inc':
                case 'men_inc':
                case 'wit_inc':
                    value = cleanValue( cleanValue( value, '{', '}' ), '[', ']' )

                    if ( value === '0;0' ) {
                        value = null
                    }

                    name = name.replace( '_inc','' )
                    break

                default:
                    value = cleanValue( cleanValue( value, '{', '}' ), '[', ']' )

                    if ( value === 'none' ) {
                        value = null
                    }

                    break
            }

            if ( value ) {
                name = _.camelCase( name )
                definition[ name ] = value
                headers.add( name )
            }
        }

        return definition
    }, itemSet )
}

async function processFile(): Promise<void> {
    const lines = readline.createInterface( {
        input: fs.createReadStream( `${ rootDirectory }/itemdata.txt` ),
        crlfDelay: Infinity
    } )

    const allItems: Array<Object> = []
    const allSets: Array<Object> = []
    const itemHeaders: Set<string> = new Set<string>()
    const setHeaders: Set<string> = new Set<string>()

    for await ( const line of lines ) {
        const chunks = line.split( /\t|\s/ )

        switch ( chunks[ 0 ] ) {
            case 'item_begin':
                allItems.push( createItem( chunks.slice( 1, chunks.length - 1 ), itemHeaders ) )
                break

            case 'set_begin':
                allSets.push( createSet( chunks.slice( 1, chunks.length - 1 ), setHeaders ) )
                break
        }
    }


    const itemsStream = fs.createWriteStream( `${ rootDirectory }/items.csv` )
    const setsStream = fs.createWriteStream( `${ rootDirectory }/itemSets.csv` )

    const itemsCsv = format( {
        delimiter: ',',
        headers: Array.from( itemHeaders )
    } )

    const itemSetsCsv = format( {
        delimiter: ',',
        headers: Array.from( setHeaders )
    } )

    itemsCsv.pipe( itemsStream )
    itemSetsCsv.pipe( setsStream )

    allItems.forEach( ( item: Object ) => {
        itemsCsv.write( item )
    } )

    allSets.forEach( ( item: Object ) => {
        itemSetsCsv.write( item )
    } )

    itemsCsv.end()
    itemSetsCsv.end()
}

processFile()