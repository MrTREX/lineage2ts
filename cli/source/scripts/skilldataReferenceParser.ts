import readline from 'readline'
import fs from 'fs'
import { format } from '@fast-csv/format'
import pkgDir from 'pkg-dir'

const rootDirectory = pkgDir.sync( __dirname )

interface SkillReferenceDefinition {
    name: string
    skillId: string
    skillLevel: string
}

const fieldMapping: Record<string, string> = {
    'skill_name': 'name',
    'skill_id': 'skillId',
    'level': 'skillLevel',
}

function cleanValue( value: string, first: string = '[', second: string = ']' ): string {
    return value.replaceAll( first, '' ).replaceAll( second, '' )
}

function getProperty( value: string ): [ string, string ] {
    return cleanValue( cleanValue( value, '{', '}' ), '[', ']' )
        .split( '=' )
        .map( value => value.trim() ) as [ string, string ]
}

function createSkill( chunks: Array<string> ): SkillReferenceDefinition {
    return chunks.reduce( ( definition: SkillReferenceDefinition, propertyLine: string ): SkillReferenceDefinition => {
        let [ name, value ] = getProperty( propertyLine )

        if ( name && value ) {
            let mappedKey = fieldMapping[ name ]
            if ( mappedKey ) {
                definition[ mappedKey ] = value
            }
        }

        return definition
    }, {} as SkillReferenceDefinition )
}

async function processFile(): Promise<void> {
    const lines = readline.createInterface( {
        input: fs.createReadStream( `${ rootDirectory }/skilldata.txt` ),
        crlfDelay: Infinity
    } )

    const skillStream = fs.createWriteStream( `${ rootDirectory }/skillReferenceIds.csv` )

    const skillsCsv = format( {
        delimiter: ',',
        headers: true
    } )

    skillsCsv.pipe( skillStream )

    const allCubics: Array<SkillReferenceDefinition> = []

    for await ( const line of lines ) {
        if ( line.startsWith( '//' ) ) {
            continue
        }

        const chunks = line.split( '\t' )

        if ( chunks[ 0 ] === 'skill_begin' ) {
            allCubics.push( createSkill( chunks ) )
        }
    }


    allCubics.forEach( ( item: SkillReferenceDefinition ) => {
        skillsCsv.write( item )
    } )

    skillsCsv.end()
}

processFile()