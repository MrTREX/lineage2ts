import pkgDir from 'pkg-dir'
import * as fs from 'fs'
import * as readline from 'readline'
import { format } from '@fast-csv/format'
import { CsvFormatterStream } from 'fast-csv'
import _ from 'lodash'
import * as blake3 from 'blake3'

const rootDirectory = pkgDir.sync( __dirname )
let makerIndex = 1

interface NpcData {
    id: string
    makerName: string
    parametersJson: string
}

function cleanValue( value: string, first: string = '[', second: string = ']' ) : string {
    return value.replaceAll( first,'' ).replaceAll( second,'' )
}

function getJson( value: string ) : string {
    if ( !value ) {
        return undefined
    }

    let items = value.replace( '{','' ).replace( '}','' ).split( ';' )
    let properties = {}

    items.forEach( ( itemValue:string ) => {
        let [ rawName, rawValue ] = itemValue.split( '=' )

        let nonParsedValue = cleanValue( rawValue )
        properties[ _.camelCase( cleanValue( rawName ) ) ] = nonParsedValue
    } )

    return JSON.stringify( properties )
}

function processNpc( chunks: Array<string>, makerName: string, stream: CsvFormatterStream<any, any> ) : void {
    let [ , idValue, ...values ] = chunks

    let properties = {}

    values.forEach( ( propertyValue : string ) => {
        if ( !propertyValue || propertyValue === 'npc_end' ) {
            return
        }

        let index = propertyValue.indexOf( '=' )
        let key = _.camelCase( propertyValue.slice( 0, index ) )
        let value = cleanValue( propertyValue.slice( index + 1 ) )

        properties[ key ] = value
    } )

    let item : NpcData = {
        id: cleanValue( idValue ),
        makerName,
        parametersJson: JSON.stringify( properties )
    }

    stream.write( item )
}

function getSpawnLogicExName( chunks: Array<string> ) : string {
    let nameProperty = chunks.slice( 2 ).find( ( value: string ) : boolean => {
        return value.startsWith( 'name=' )
    } )

    return cleanValue( nameProperty.replace( 'name=', '' ) )
}


interface NpcLogic {
    name: string
    parametersJson: string
    territoryJson: string
}

function processLogic( chunks: Array<string>, stream: CsvFormatterStream<any, any>, territories: Record<string, string> ) : string {
    let [ , idValue, ...values ] = chunks

    let name : string = `generated_${makerIndex++}`
    idValue.split( ';' ).forEach( ( value: string ) : void => {
        if ( !territories[ cleanValue( value ) ] ) {
            throw new Error( `Unknown territory in ${idValue} maker, available territories ${Object.keys( territories )}` )
        }
    } )

    let properties = {
    }

    values.forEach( ( propertyValue : string ) => {
        if ( !propertyValue ) {
            return
        }

        let index = propertyValue.indexOf( '=' )
        let key = _.camelCase( propertyValue.slice( 0, index ) )

        properties[ key ] = cleanValue( propertyValue.slice( index + 1 ) )
    } )

    let item : NpcLogic = {
        name,
        parametersJson: JSON.stringify( properties ),
        territoryJson: JSON.stringify( territories )
    }

    stream.write( item )

    return name
}

const existingHashes = new Map<string, string>()
let duplicateCounter = 0
function writeTerritories( territories : Record<string, string>, stream: CsvFormatterStream<any, any> ) : Record<string, string> {
    return _.mapValues( territories, ( points: string, key: string ) => {

        let id = blake3.hash( points, { length: 16 } ).toString( 'base64' )

        if ( !existingHashes.has( id ) ) {
            stream.write( {
                id,
                name: key,
                points
            } )

            existingHashes.set( id, points )
        } else {
            if ( existingHashes.get( id ) !== points ) {
                throw new Error( `Bad hash detected for id=${id}, stored value=${existingHashes.get( id )} compared to=${points}` )
            }
            duplicateCounter++
        }

        return id
    } )
}

async function processFile() : Promise<void> {
    const lines = readline.createInterface( {
        input: fs.createReadStream( `${rootDirectory}/npcpos.txt` ),
        crlfDelay: Infinity
    } )

    const npcFileStream = fs.createWriteStream( `${rootDirectory}/npcMaker.csv` )
    const territoryStream = fs.createWriteStream( `${rootDirectory}/spawnTerritories.csv` )
    const npcSpawnStream = fs.createWriteStream( `${rootDirectory}/npcSpawn.csv` )

    const npcCsvStream = format( { delimiter: ',', headers: true } )
    npcCsvStream.pipe( npcFileStream )

    const territoryCsvStream = format( { delimiter: ',', headers: true } )
    territoryCsvStream.pipe( territoryStream )

    const spawnCsvStream = format( { delimiter: ',', headers: true } )
    spawnCsvStream.pipe( npcSpawnStream )

    let territories : Record<string, string> = {}
    let isProcessingMaker = false
    let makerName : string

    for await ( const line of lines ) {
        const chunks = line.split( '\t' )

        switch ( chunks[ 0 ] ) {
            case 'territory_begin':
                if ( isProcessingMaker ) {
                    territories = {}
                    isProcessingMaker = false
                }

                territories[ cleanValue( chunks[ 1 ] ) ] = chunks[ 2 ]
                break

            case 'npc_begin':
                if ( isProcessingMaker ) {
                    processNpc( chunks, makerName, spawnCsvStream )
                }

                break
            case 'npcmaker_end':
                break

            case 'npcmaker_begin':
                if ( !isProcessingMaker ) {
                    territories = writeTerritories( territories, territoryCsvStream )
                    isProcessingMaker = true
                }

                makerName = processLogic( chunks, npcCsvStream, territories )
                break

            default:
                if ( chunks[ 0 ].startsWith( '//' ) ) {
                    break
                }

                territories = {}
                break
        }
    }

    console.log( 'Encountered', duplicateCounter, 'territory points duplicates' )

    territoryCsvStream.end()
    npcCsvStream.end()
    spawnCsvStream.end()
}

processFile()