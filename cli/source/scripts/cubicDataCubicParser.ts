import readline from 'readline'
import fs from 'fs'
import { format } from '@fast-csv/format'
import pkgDir from 'pkg-dir'

const rootDirectory = pkgDir.sync( __dirname )

interface CubicDefinition {
    id: string
    level: string
    durationSeconds: string
    delay: string
    maxActions: string
    power: string
    targetType: string
    hpCondition: string
    skillOne: string
    skillTwo: string
    skillThree: string
}

const fieldMapping: Record<string, string> = {
    'id': 'id',
    'level': 'level',
    'duration': 'durationSeconds',
    'delay': 'delay',
    'max_count': 'maxActions',
    'power': 'power',
    'target_type': 'targetType',
    'op_cond': 'hpCondition',
    'skill1': 'skillOne',
    'skill2': 'skillTwo',
    'skill3': 'skillThree',
}

function cleanValue( value: string, first: string = '[', second: string = ']' ): string {
    return value.replaceAll( first, '' ).replaceAll( second, '' )
}

function getProperty( value: string ): [ string, string ] {
    return cleanValue( cleanValue( value, '{', '}' ), '[', ']' )
        .split( '=' )
        .map( value => value.trim() ) as [ string, string ]
}

function createCubic( chunks: Array<string> ): CubicDefinition {
    return chunks.reduce( ( definition: CubicDefinition, propertyLine: string ): CubicDefinition => {
        let [ name, value ] = getProperty( propertyLine )

        if ( name && value ) {
            let mappedKey = fieldMapping[ name ]
            if ( mappedKey ) {
                definition[ mappedKey ] = value
            }
        }

        return definition
    }, {} as CubicDefinition )
}

async function processFile(): Promise<void> {
    const lines = readline.createInterface( {
        input: fs.createReadStream( `${ rootDirectory }/CubicData.txt` ),
        crlfDelay: Infinity
    } )

    const cubicsStream = fs.createWriteStream( `${ rootDirectory }/cubics.csv` )

    const cubicsCsv = format( {
        delimiter: ',', headers: Object.values( fieldMapping )
    } )

    cubicsCsv.pipe( cubicsStream )

    const allCubics: Array<CubicDefinition> = []

    for await ( const line of lines ) {
        if ( line.startsWith( '//' ) ) {
            continue
        }

        const chunks = line.split( '\t' )

        if ( chunks[ 0 ] === 'cubic_begin' ) {
            allCubics.push( createCubic( chunks ) )
        }
    }


    allCubics.forEach( ( item: CubicDefinition ) => {
        cubicsCsv.write( item )
    } )

    cubicsCsv.end()
}

processFile()