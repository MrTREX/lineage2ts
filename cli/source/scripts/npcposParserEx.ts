import pkgDir from 'pkg-dir'
import * as fs from 'fs'
import * as readline from 'readline'
import { format } from '@fast-csv/format'
import { CsvFormatterStream } from 'fast-csv'
import _ from 'lodash'
import * as blake3 from 'blake3'

const rootDirectory = pkgDir.sync( __dirname )

interface NpcData {
    id: string
    makerName: string
    parametersJson: string
}

function cleanValue( value: string, first: string = '[', second: string = ']' ) : string {
    return value.replaceAll( first,'' ).replaceAll( second,'' )
}

function getJson( value: string ) : string {
    if ( !value ) {
        return undefined
    }

    let items = value.replace( '{','' ).replace( '}','' ).split( ';' )
    let properties = {}

    items.forEach( ( itemValue:string ) => {
        let [ rawName, rawValue ] = itemValue.split( '=' )

        let nonParsedValue = cleanValue( rawValue )
        properties[ _.camelCase( cleanValue( rawName ) ) ] = nonParsedValue
    } )

    return JSON.stringify( properties )
}

function processNpc( chunks: Array<string>, makerName: string, stream: CsvFormatterStream<any, any> ) : void {
    let [ , idValue, ...values ] = chunks

    let properties = {}

    values.forEach( ( propertyValue : string ) => {
        if ( !propertyValue || propertyValue === 'npc_end' ) {
            return
        }

        let index = propertyValue.indexOf( '=' )
        let key = _.camelCase( propertyValue.slice( 0, index ) )
        let value = cleanValue( propertyValue.slice( index + 1 ) )

        properties[ key ] = value
    } )

    let item : NpcData = {
        id: cleanValue( idValue ),
        makerName,
        parametersJson: JSON.stringify( properties )
    }

    stream.write( item )
}

function getSpawnLogicExName( chunks: Array<string> ) : string {
    let nameProperty = chunks.slice( 2 ).find( ( value: string ) : boolean => {
        return value.startsWith( 'name=' )
    } )

    return cleanValue( nameProperty.replace( 'name=', '' ) )
}


interface NpcLogicExtra {
    name: string
    parametersJson: string
    territoryJson: string
}

function processLogicExtra( chunks: Array<string>, stream: CsvFormatterStream<any, any>, territories: Record<string, string> ) : void {
    let [ , idValue, ...values ] = chunks

    idValue.split( ';' ).forEach( ( value: string ) : void => {
        if ( !territories[ cleanValue( value ) ] ) {
            throw new Error( `Unknown territory in ${idValue} maker, available territories ${Object.keys( territories )}` )
        }
    } )

    let properties = {
    }

    let name : string

    values.forEach( ( propertyValue : string ) => {
        if ( !propertyValue ) {
            return
        }

        let index = propertyValue.indexOf( '=' )
        let key = _.camelCase( propertyValue.slice( 0, index ) )
        let value = cleanValue( propertyValue.slice( index + 1 ) )

        if ( key === 'name' ) {
            name = value
            return
        }

        properties[ key ] = value
    } )

    let item : NpcLogicExtra = {
        name,
        parametersJson: JSON.stringify( properties ),
        territoryJson: JSON.stringify( territories )
    }

    stream.write( item )
}

const existingHashes = new Map<string, string>()
let duplicateCounter = 0
function writeTerritories( territories : Record<string, string>, stream: CsvFormatterStream<any, any> ) : Record<string, string> {
    return _.mapValues( territories, ( points: string, name: string ) => {

        let id = blake3.hash( points, { length: 16 } ).toString( 'base64' )

        if ( !existingHashes.has( id ) ) {
            stream.write( {
                id,
                name,
                points
            } )

            existingHashes.set( id, points )
        } else {
            if ( existingHashes.get( id ) !== points ) {
                throw new Error( `Bad hash detected for id=${id}, stored value=${existingHashes.get( id )} compared to=${points}` )
            }
            duplicateCounter++
        }

        return id
    } )
}

async function processFile() : Promise<void> {
    const lines = readline.createInterface( {
        input: fs.createReadStream( `${rootDirectory}/npcpos.txt` ),
        crlfDelay: Infinity
    } )

    const npcFileStream = fs.createWriteStream( `${rootDirectory}/npcMakerEx.csv` )
    const territoryStream = fs.createWriteStream( `${rootDirectory}/spawnTerritoriesEx.csv` )

    const npcCsvStream = format( { delimiter: ',', headers: true } )
    npcCsvStream.pipe( npcFileStream )

    const territoryCsvStream = format( { delimiter: ',', headers: true } )
    territoryCsvStream.pipe( territoryStream )

    let territories : Record<string, string> = {}
    let isProcessingMaker = false

    for await ( const line of lines ) {
        const chunks = line.split( '\t' )

        switch ( chunks[ 0 ] ) {
            case 'territory_begin':
                if ( isProcessingMaker ) {
                    territories = {}
                    isProcessingMaker = false
                }

                territories[ cleanValue( chunks[ 1 ] ) ] = chunks[ 2 ]
                break

            case 'npc_ex_begin':
            case 'npcmaker_ex_end':
                break

            case 'npcmaker_ex_begin':
                if ( !isProcessingMaker ) {
                    territories = writeTerritories( territories, territoryCsvStream )
                    isProcessingMaker = true
                }

                processLogicExtra( chunks, npcCsvStream, territories )
                break

            default:
                if ( chunks[ 0 ].startsWith( '//' ) ) {
                    break
                }

                territories = {}
                break
        }
    }

    console.log( 'Encountered', duplicateCounter, 'territory points duplicates' )

    territoryCsvStream.end()
    npcCsvStream.end()
}

processFile()