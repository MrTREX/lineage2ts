export interface EngineOperations {
    executeLoginQuery( statement : string ) : Promise<void>
    executeGameQuery( statement : string ) : Promise<void>
    start( data : any ) : Promise<void>
    finish() : Promise<void>
    getLoginUser( name : string ) : Promise<any>
    createLoginUser( data : any ) : Promise<void>
    removeLoginUser( name : string ) : Promise<void>
}