import { EngineOperations } from './EngineOperations'
import pkgDir from 'pkg-dir'
import Database from 'better-sqlite3'

const rootDirectory = pkgDir.sync( __dirname )

let loginDatabase
let gameDatabase

const removeUserQueries : Array<string> = [
    'delete from accounts where login = @userName',
    'delete from account_data where account_name = @userName',
    'delete from accounts_ipauth where login = @userName'
]

export const SQLiteEngine : EngineOperations = {
    async executeLoginQuery( query ) : Promise<void> {
        loginDatabase.exec( query )
    },

    async executeGameQuery( query ) : Promise<void> {
        gameDatabase.exec( query )
    },

    start: async () => {
        if ( loginDatabase ) {
            loginDatabase.close()
        }

        if ( gameDatabase ) {
            gameDatabase.close()
        }

        loginDatabase = new Database( `${rootDirectory}/login.database` )
        gameDatabase = new Database( `${rootDirectory}/game.database` )

        loginDatabase.pragma( 'journal_mode = WAL' )
        gameDatabase.pragma( 'journal_mode = WAL' )
    },

    async finish() : Promise<void> {
        loginDatabase.close()
        gameDatabase.close()
    },

    async getLoginUser( userName ) : Promise<unknown> {
        const query = 'SELECT login FROM accounts WHERE login=?'
        return loginDatabase.prepare( query ).get( [ userName ] )
    },

    // TODO : convert to use positional parameters
    async createLoginUser( values ) : Promise<void> {
        const query = 'INSERT INTO accounts (login, password, accessLevel) values (@user, @password, @accessLevel)'
        return loginDatabase.prepare( query ).run( values )
    },

    removeLoginUser: async ( userName ) => {
        const data = { userName }
        removeUserQueries.forEach( ( query : string ) => loginDatabase.prepare( query ).run( data ) )
    }
}
