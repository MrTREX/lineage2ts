import { SQLiteEngine } from './engine/sqlite'
import { EngineOperations } from './engine/EngineOperations'
import { SelectDatabaseDriverPromptOption } from '../ui/operations/prompts/serverDatabase'

const availableEngines : {[ name : string ] : EngineOperations } = {
    [ SelectDatabaseDriverPromptOption.sqlite ]: SQLiteEngine,
}

export function getDatabaseEngine( name ) : EngineOperations {
    return availableEngines[ name ]
}