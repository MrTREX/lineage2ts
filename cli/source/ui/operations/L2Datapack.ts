import { CLIProperties, ICLIOperation } from './ICLIOperation'
import {
    DatapackOperations,
    DatapackOperationsPrompt,
    SelectDatapackDriverPrompt,
    SelectDatapackNamePrompt,
} from './prompts/datapackPrompts'
import { DatapackTransformers } from '../../datapack/transformerManager'
import { TransformerProcess } from '../../datapack/helpers/TransformerProcess'
import { getDatapackEngine } from '../../datapack/engines'
import { SQLiteDefaultParameters } from '../../datapack/engine/sqlite'
import { DatapackEngineName } from '../../datapack/engine/IDatapackEngine'
import aigle from 'aigle'
import _ from 'lodash'
import { searchFiles } from 'find-file-extension'
import pkgDir from 'pkg-dir'
import * as fs from 'fs/promises'
import perfy from 'perfy'
import to from 'await-to-js'
import logSymbols from 'log-symbols'
import cliProgress from 'cli-progress'

export interface L2DatapackProperties extends CLIProperties {
    fileName?: string
}

export class L2Datapack implements ICLIOperation {
    private databaseEngine: string
    private connectionParameters: any

    async getConnectionParameters() {
        switch ( this.databaseEngine ) {

            case DatapackEngineName.sqlite:
                return SelectDatapackNamePrompt().run()

            default:
                throw new Error( `${ this.databaseEngine } database type is not defined to process connection parameters.` )
        }
    }

    async getDriverSelection(): Promise<void> {

        if ( this.databaseEngine && this.connectionParameters ) {
            return
        }

        this.databaseEngine = await SelectDatapackDriverPrompt().run()
        this.connectionParameters = await this.getConnectionParameters()

        return getDatapackEngine( this.databaseEngine ).start( this.connectionParameters )
    }

    async importData() {
        let databaseEngine = this.databaseEngine
        let errors = []
        let progressBar = new cliProgress.SingleBar( {
            format: '{bar} | {percentage}% | {name}',
        }, cliProgress.Presets.shades_classic )

        progressBar.start( _.size( DatapackTransformers ), 0 )

        perfy.start( 'import-data' )
        await aigle.resolve( DatapackTransformers ).eachSeries( async ( transformer: TransformerProcess ) => {
            progressBar.increment( {
                name: `Importing : ${ transformer.getName() }`,
            } )

            let [ error ] = await to( transformer.startProcess( getDatapackEngine( databaseEngine ) ) )

            if ( error ) {
                errors.push( `Failed processing step '${transformer.getName()}' on '${ transformer.getLastProcessedOperation() }' with error: ${ error.stack }` )
            }
        } )
        let metrics = perfy.end( 'import-data' )

        progressBar.update( {
            name: 'Finished',
        } )

        progressBar.stop()

        if ( errors.length > 0 ) {
            _.each( errors, ( message ) => {
                console.log( logSymbols.error, message )
            } )
        }

        console.log( logSymbols.info, `Processed ${ DatapackTransformers.length } data sets with ${ errors.length } errors in ${ metrics.time } seconds` )
    }

    async installTables() {

        let databaseEngine = this.databaseEngine
        const rootDirectory = await pkgDir( __dirname )
        let files = searchFiles( 'sql', `${ rootDirectory }/sql/${ databaseEngine.toLowerCase() }/datapack` )

        let errors = 0
        await aigle.resolve( files ).eachSeries( async ( currentFile ) => {
            let contents = await fs.readFile( currentFile )
            let [ error ] = await to( getDatapackEngine( databaseEngine ).executeQuery( contents.toString() ) )

            if ( error ) {
                errors++
                return console.log( logSymbols.error, `Failed processing file '${ currentFile }' with error:`, error.message )
            }
        } )

        if ( errors > 0 ) {
            console.log( logSymbols.warning, `Processed ${ files.length } datapack setup files with ${ errors } errors` )
        }

        console.log( logSymbols.success, 'Process finished Datapack initialization.' )
    }

    async interactiveRun(): Promise<void> {
        await this.getDriverSelection()

        let command: string = await DatapackOperationsPrompt().run()
        switch ( command ) {
            case DatapackOperations.create:
                await this.installTables()
                await this.importData()

                break
        }

        return this.finishRun()
    }

    async finishRun() : Promise<void> {
        await getDatapackEngine( this.databaseEngine ).finish()
        this.databaseEngine = null
    }

    async silentRun( properties : L2DatapackProperties ): Promise<void> {
        this.databaseEngine = DatapackEngineName.sqlite
        let fileName = _.defaultTo( properties.fileName, SQLiteDefaultParameters.datapackName )

        console.log( logSymbols.info, 'Datapack is using', this.databaseEngine )
        console.log( logSymbols.info, 'Datapack file name:', fileName )

        await getDatapackEngine( this.databaseEngine ).start( fileName )
        await this.installTables()
        await this.importData()

        return this.finishRun()
    }
}