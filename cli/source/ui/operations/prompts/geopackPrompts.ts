import { SQLiteDefaultParameters } from '../../../datapack/engine/sqlite'
// @ts-ignore
import { Select, Input } from 'enquirer'

export function GeopackOperationsPrompt() {
    return new Select( {
        name: 'command',
        message: 'Choose Geopack operation',
        choices: [ 'Install', 'Exit' ]
    } )
}

export function SelectGeopackNamePrompt() {
    return new Input( {
        name: 'connection',
        message: 'Please provide geopack file name:',
        initial: SQLiteDefaultParameters.geopackName,
    } )
}