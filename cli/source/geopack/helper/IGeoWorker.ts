export interface IGeoWorker {
    processFile( filePath: string ) : GeoWorkerOutput
}

export type GeoWorkerOutput = [ ArrayBuffer, string ]