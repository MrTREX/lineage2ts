import { Pool, spawn, Worker } from 'threads'
import { GeoWorkerOutput, IGeoWorker } from './IGeoWorker'

export class GeoWorkerPool {
    pool: any // threads has idiotic type system for pool

    create() : void {
        this.pool = Pool( () => spawn( new Worker( './GeoWorker' ) ), 8 )
    }

    queueWork( fileName: string ) : Promise<GeoWorkerOutput> {
        return this.pool.queue( ( worker : IGeoWorker ) => worker.processFile( fileName ) )
    }

    async shutdown() : Promise<void> {
        await this.pool.terminate( true )
        this.pool = null
    }
}

export const WorkerPool = new GeoWorkerPool()