import _ from 'lodash'
import { GeoPolygonType } from './GeoPolygon'

export const enum GeoConverterValues {
    BlockLength = 128,
    BlockAmount = 64,
    BlocksInRegion = 256 * 256,
    LowestHeight = -32768,
    HighestHeight = 32767,
    Block8BitNoValue = 255
}

/*
    Value that would represent relative difference in sloped polygon that would still allow certain height of characters to pass through.
 */
const enum GeoVariation {
    FullVariation = 254,
    HalfVariation = 128,
}

export const enum GeoBlockType {
    Flat,
    MultiHeight,
    MultiLayer
}

interface LayerMetadata {
    minZ: number
    maxZ: number
    hasBlocking: boolean
}

const enum MultiLevel {
    Size = 5
}

interface MultiLevelData {
    data: Buffer
    info: LayerMetadata
}

class MultiLevelConverter {
    original: Buffer
    layerAmount: number
    multiLayerInfo: Buffer
    offset: number = 2

    constructor( data: Buffer, layerAmount: number ) {
        this.original = data
        this.layerAmount = layerAmount

        this.multiLayerInfo = Buffer.allocUnsafe( 2 + MultiLevel.Size * layerAmount )
        this.multiLayerInfo.writeUInt8( GeoPolygonType.MultiLayer )
        this.multiLayerInfo.writeUInt8( layerAmount, 1 )
    }

    /*
        Removing layer starts at bottom and assuming that 8x8 grid of items have relative height of 255, which
        does not always work correctly, however in most cases it is true.
     */
    private removeBottomLayer( originalData: Array<number>, layerValues: Array<Set<number>>, variation: GeoVariation ) : Array<number> {
        let minValue = Math.min( ...originalData )
        let layer = new Set<number>()

        let modifiedData = originalData.filter( ( value: number ): boolean => {

            if ( ( value - minValue ) < variation ) {
                layer.add( value )

                return false
            }

            return true
        } )

        if ( layer.size > 0 ) {
            layerValues.push( layer )
        }

        return modifiedData
    }

    private separateLayers( values: Array<number>, variation: GeoVariation ) : Array<Set<number>> {
        let modifiedData : Array<number> = structuredClone( values )
        let outcome: Array<Set<number>> = []

        /*
            While separating layers, we can assume to work from bottom to top. However, top layer can have bigger spread
            of values that what we would like ( a roof of a building that has very tall pikes ). Hence, once we determine
            we're about to do top layer, we can safely dump all associated values there to fill layer quota.
         */
        let targetLayerAmount = this.layerAmount - 1
        do {
            if ( outcome.length === targetLayerAmount ) {
                outcome.push( new Set<number>( modifiedData ) )
                break
            }

            modifiedData = this.removeBottomLayer( modifiedData, outcome, variation )
        } while ( modifiedData.length > 0 )

        return outcome
    }

    getLayers() : Array<MultiLevelData> {
        let offset : number = 0

        /*
            L2J geodata format only has layers encoded per each cell. Thus, when we sort such cells into layers
            we will have oddly shaped Z planes that do not correspond to gradually changing Z values in layers.

            Example:
            - a building with roof under which you can walk will be represented as layer of ground and roof, with
            no walkable path under such roof. However, another layer altogether may represent walkable path under roof,
            or at least a chunk of such path. Since characters would not be able to walk onto the roof, such layer presents
            problem with both finding correct layer per requested Z (roof and under have same Z range of values)
            and walk-ability (character can mistakenly get onto roof).

            Solution would be to identify which layers contain similar Z values per polygon layer.
         */

        let allValues : Array<number> = []

        for ( let blockIndex = 0; blockIndex < GeoConverterValues.BlockAmount; blockIndex++ ) {
            let layerCount = this.original.readInt8( offset )

            offset += 1

            // TODO : possible to derive min distance between layer points and use it for layer separation
            for ( let layerIndex = 0; layerIndex < layerCount; layerIndex++ ) {
                let cellValue = this.original.readInt16LE( offset )
                offset += 2

                allValues.push( extractHeight( cellValue ) )
            }
        }

        let layerValues: Array<Set<number>> = this.separateLayers( allValues, GeoVariation.FullVariation )
        /*
            It is possible for layer values to be quite close together, in such case there is no reliable way
            to separate layer Z values.
         */
        if ( layerValues.length < this.layerAmount ) {
            layerValues = this.separateLayers( allValues, GeoVariation.HalfVariation )

            if ( layerValues.length < this.layerAmount ) {
                return null
            }
        }

        let layers: Array<MultiLevelData> = _.times( this.layerAmount, () => {
            return {
                data: Buffer.allocUnsafe( GeoConverterValues.BlockLength ),
                info: {
                    minZ: null,
                    maxZ: null,
                    hasBlocking: false
                }
            }
        } )

        offset = 0
        const usedLayerIndexes = new Set<number>()

        for ( let positionIndex = 0; positionIndex < GeoConverterValues.BlockAmount; positionIndex++ ) {
            let layerCount = this.original.readInt8( offset )

            offset += 1
            const writePosition = positionIndex * 2

            for ( let layerCountIndex = 0; layerCountIndex < layerCount; layerCountIndex++ ) {
                let value = this.original.readInt16LE( offset )
                offset += 2

                /*
                    L2J format has height and blocking data in same 16-bit value.
                 */
                let cleanedValue = extractHeight( value )
                let layerIndex = layerValues.findIndex( ( values: Set<number> ) => values.has( cleanedValue ) )


                /*
                    Index must always be found since data has already been partitioned into sets of numbers.
                 */
                if ( layerIndex === -1 ) {
                    throw new Error( 'Multi-layer block: Unable to determine layer index' )
                }

                /*
                    We cannot process too close grouped Z values, hence we default to L2J multi-region block.
                 */
                if ( usedLayerIndexes.has( layerIndex ) ) {
                    return null
                }


                let currentLayer = layers[ layerIndex ]
                let metadata = currentLayer.info

                metadata.minZ = metadata.minZ === null ? cleanedValue : Math.min( cleanedValue, metadata.minZ )
                metadata.maxZ = metadata.maxZ === null ? cleanedValue : Math.max( cleanedValue, metadata.maxZ )

                if ( !metadata.hasBlocking ) {
                    metadata.hasBlocking = extractBlockingDirection( value ) !== 0x000F
                }

                currentLayer.data.writeInt16LE( value, writePosition )
                usedLayerIndexes.add( layerIndex )
            }

            if ( usedLayerIndexes.size !== this.layerAmount ) {
                for ( let layerIndex = 0; layerIndex < this.layerAmount; layerIndex++ ) {
                    if ( !usedLayerIndexes.has( layerIndex ) ) {
                        layers[ layerIndex ].data.writeInt16LE( GeoConverterValues.LowestHeight, writePosition )
                    }
                }
            }

            usedLayerIndexes.clear()
        }

        if ( layers.length > 1 ) {
            for ( let index = 1; index < ( layers.length - 1 ); index++ ) {
                let first = layers[ index - 1 ]
                let second = layers[ index ]

                if ( first.info.minZ === second.info.minZ ) {
                    throw new Error( `${layers.length} Layers have similar min values: metadata=${JSON.stringify( [ first.info, second.info ] )}, values=${ JSON.stringify( allValues )}` )
                }

                if ( second.info.minZ === first.info.maxZ ) {
                    throw new Error( `${layers.length} Layers have similar min and max values: metadata=${JSON.stringify( [ first.info, second.info ] )}, values=${ JSON.stringify( allValues )}` )
                }

                if ( second.info.maxZ === first.info.maxZ ) {
                    throw new Error( `${layers.length} Layers have similar max values: metadata=${JSON.stringify( [ first.info, second.info ] )}, values=${ JSON.stringify( allValues )}` )
                }
            }
        }

        return layers
    }

    writeMultiLayerValues( type: GeoPolygonType, minZ: number, maxZ: number ) : void {
        this.multiLayerInfo.writeUInt8( type, this.offset )
        this.multiLayerInfo.writeInt16LE( minZ, this.offset + 1 )
        this.multiLayerInfo.writeInt16LE( maxZ, this.offset + 3 )

        this.offset += MultiLevel.Size
    }
}

function extractHeight( value: number ) : number {
    return ( value & 0xFFFFFFF0 ) >> 1
}

function extractBlockingDirection( value: number ) : number {
    return value & 0x0000000F
}

export class GeoConverter {
    currentBuffer: Buffer
    offset: number = 0
    statistics: Record<string, number> = {
        [ GeoPolygonType[ GeoPolygonType.Flat ] ]: 0,
        [ GeoPolygonType[ GeoPolygonType.FlatBlocking ] ]: 0,
        [ GeoPolygonType[ GeoPolygonType.MultiHeight8bit ] ]: 0,
        [ GeoPolygonType[ GeoPolygonType.MultiHeight16bit ] ]: 0,
        [ GeoPolygonType[ GeoPolygonType.MultiHeightBlocking16bit ] ]: 0,
        [ GeoPolygonType[ GeoPolygonType.MultiLayer ] ]: 0,
        [ GeoPolygonType[ GeoPolygonType.MultiLayerL2J ] ]: 0,
    }

    constructor( buffer : Buffer ) {
        this.currentBuffer = buffer
    }

    readId() : GeoBlockType {
        let value = this.currentBuffer.readInt8( this.offset )
        this.offset = this.offset + 1

        return value
    }

    /*
        L2J style flat block is same as GeoPolygon flat block.
     */
    getFlatBlock() : Buffer {
        let endPosition = this.offset + 2
        let block : Buffer = this.currentBuffer.subarray( this.offset, endPosition )

        this.offset = endPosition

        return this.extractFlatBlock( block )
    }

    /*
        Scan block and decide which GeoPolygon to use.
     */
    getComplexBlock() : Buffer {
        let endPosition = this.offset + GeoConverterValues.BlockLength
        let block : Buffer = this.currentBuffer.subarray( this.offset, endPosition )

        this.offset = endPosition

        return this.convertComplexBlock( block )
    }

    getMultilayerBlockMaxLayers() : number {
        let layers = 1

        _.times( GeoConverterValues.BlockAmount, () => {
            let layerDepth = this.readId()
            if ( layerDepth <= 0 || layerDepth > 125 ) {
                throw new Error( `Invalid value in MultilayerBlock position ${this.offset - 1} for layer value = ${layerDepth}` )
            }

            if ( layerDepth > layers ) {
                layers = layerDepth
            }

            this.offset = this.offset + layerDepth * 2
        } )

        return layers
    }

    getMultilayerBlock() : Buffer {
        let startPosition = this.offset
        let layerAmount = this.getMultilayerBlockMaxLayers()
        let layerData : Buffer = this.currentBuffer.subarray( startPosition, this.offset )
        let converter = new MultiLevelConverter( layerData, layerAmount )
        let layers: Array<MultiLevelData> = converter.getLayers()

        /*
            Case when layers cannot be generated due to complicated point geometry,
            we should preserve original layer data.
         */
        if ( !layers ) {
            this.statistics[ GeoPolygonType[ GeoPolygonType.MultiLayerL2J ] ]++

            let block = Buffer.allocUnsafe( 3 + layerData.length )

            block.writeUInt8( GeoPolygonType.MultiLayerL2J, 0 )
            block.writeUint16LE( layerData.length, 1 )

            layerData.copy( block,3 )

            return block
        }

        this.statistics[ GeoPolygonType[ GeoPolygonType.MultiLayer ] ]++

        let convertedLayers : Array<Buffer> = layers.map( ( currentLayer : MultiLevelData, layerIndex: number ) : Buffer => {
            let variation = currentLayer.info

            if ( variation.minZ === null || variation.maxZ === null ) {
                throw new Error( 'Bad layer variation values' )
            }

            let nextLayer = layers[ layerIndex + 1 ]
            let maxZ = nextLayer ? nextLayer.info.minZ : GeoConverterValues.HighestHeight

            /*
                If we have detected any blocks it can still mean that we have flat block.
             */

            if ( variation.hasBlocking ) {
                if ( variation.maxZ === variation.minZ ) {
                    converter.writeMultiLayerValues( GeoPolygonType.FlatBlocking, variation.minZ, maxZ )

                    return this.extractBlockingData( currentLayer.data, variation.minZ )
                }

                converter.writeMultiLayerValues( GeoPolygonType.MultiHeightBlocking16bit, variation.minZ, maxZ )

                return this.extractComplexBlock( currentLayer.data )
            }

            /*
                Possible true flat block without any blocking cells.
             */
            if ( variation.maxZ === variation.minZ ) {
                converter.writeMultiLayerValues( GeoPolygonType.Flat, variation.minZ, maxZ )

                let flatData = Buffer.allocUnsafe( 3 )
                flatData.writeUInt8( GeoPolygonType.Flat )
                flatData.writeInt16LE( variation.maxZ, 1 )

                return flatData
            }

            if ( ( variation.maxZ - variation.minZ ) <= GeoVariation.FullVariation ) {
                converter.writeMultiLayerValues( GeoPolygonType.MultiHeight8bit, variation.minZ, maxZ )
                return this.extractSimpleHeightData( currentLayer.data, variation )
            }

            converter.writeMultiLayerValues( GeoPolygonType.MultiHeight16bit, variation.minZ, maxZ )
            return this.extractComplexHeightData( currentLayer.data )
        } )

        convertedLayers.unshift( converter.multiLayerInfo )

        return Buffer.concat( convertedLayers )
    }

    /*
        Possible GeoPolygon types:
        - flat blocking
        - multi-height 8 or 16 bit
        - multi-height blocking
     */
    convertComplexBlock( data: Buffer ) : Buffer {
        let variation = this.getLayerVariation( data )

        /*
            If we have detected any blocks it can still mean that we have flat block.
         */
        if ( variation.hasBlocking ) {
            if ( variation.maxZ === variation.minZ ) {
                return this.extractBlockingData( data, variation.minZ )
            }

            return this.extractComplexBlock( data )
        }

        /*
            Possible true flat block without any blocking cells.
         */
        if ( variation.maxZ === variation.minZ ) {
            let flatData = Buffer.allocUnsafe( 3 )

            flatData.writeUInt8( GeoPolygonType.Flat )
            flatData.writeInt16LE( variation.maxZ, 1 )
            this.statistics[ GeoPolygonType[ GeoPolygonType.Flat ] ]++

            return flatData
        }

        if ( Math.abs( variation.maxZ - variation.minZ ) <= GeoVariation.FullVariation ) {
            return this.extractSimpleHeightData( data, variation )
        }

        return this.extractComplexHeightData( data )
    }

    extractComplexBlock( data: Buffer ) : Buffer {
        let blockingData = Buffer.allocUnsafe( 129 )

        blockingData.writeUInt8( GeoPolygonType.MultiHeightBlocking16bit )
        data.copy( blockingData, 1, 0 )
        this.statistics[ GeoPolygonType[ GeoPolygonType.MultiHeightBlocking16bit ] ]++

        return blockingData
    }

    extractFlatBlock( data: Buffer ) : Buffer {
        let block = Buffer.allocUnsafe( 3 )

        block.writeUInt8( GeoPolygonType.Flat )
        block.writeInt16LE( data.readInt16LE(), 1 )
        this.statistics[ GeoPolygonType[ GeoPolygonType.Flat ] ]++

        return block
    }

    getLayerVariation( data: Buffer ) : LayerMetadata {
        let offset = 0
        let maxZ : number = null
        let minZ : number = null
        let hasBlocking : boolean = false

        while ( offset < ( data.length - 1 ) ) {
            let value = data.readInt16LE( offset )

            if ( !hasBlocking ) {
                hasBlocking = extractBlockingDirection( value ) !== 0x000F
            }

            if ( value !== GeoConverterValues.LowestHeight ) {
                let height = extractHeight( value )
                maxZ = maxZ === null ? height : Math.max( maxZ, height )
                minZ = minZ === null ? height : Math.min( minZ, height )
            }

            offset += 2
        }

        return {
            minZ,
            maxZ,
            hasBlocking
        }
    }

    extractBlockingData( original: Buffer, height: number ) : Buffer {
        let offset = 0
        let flatBlockingData = Buffer.allocUnsafe( 67 )

        flatBlockingData.writeUInt8( GeoPolygonType.FlatBlocking, 0 )
        flatBlockingData.writeInt16LE( height, 1 )
        this.statistics[ GeoPolygonType[ GeoPolygonType.FlatBlocking ] ]++

        while ( offset < ( original.length - 1 ) ) {
            let value = original.readInt16LE( offset )
            flatBlockingData.writeUInt8( extractBlockingDirection( value ), 3 + ( offset / 2 ) )

            offset += 2
        }

        return flatBlockingData
    }

    extractSimpleHeightData( original: Buffer, info: LayerMetadata ) : Buffer {
        let multiHeightData = Buffer.allocUnsafe( 67 )
        multiHeightData.writeUInt8( GeoPolygonType.MultiHeight8bit )
        multiHeightData.writeInt16LE( info.minZ, 1 )
        this.statistics[ GeoPolygonType[ GeoPolygonType.MultiHeight8bit ] ]++

        let offset = 0

        while ( offset < ( original.length - 1 ) ) {
            let originalValue = original.readInt16LE( offset )
            let adjustedValue = this.computeSimpleHeight( originalValue, info.minZ )

            if ( adjustedValue < 0 || adjustedValue > 255 ) {
                throw new Error( `Bad value extracted: originalValue=${originalValue}, adjustedValue=${adjustedValue}, info=${JSON.stringify( info )}` )
            }

            multiHeightData.writeUInt8( adjustedValue, 3 + ( offset / 2 ) )
            offset += 2
        }

        return multiHeightData
    }

    computeSimpleHeight( value: number, minZ: number ) : number {
        if ( value === GeoConverterValues.LowestHeight ) {
            return GeoConverterValues.Block8BitNoValue
        }

        return extractHeight( value ) - minZ
    }

    extractComplexHeightData( original: Buffer ) : Buffer {
        let multiHeightData = Buffer.allocUnsafe( 129 )
        multiHeightData.writeUInt8( GeoPolygonType.MultiHeight16bit )
        this.statistics[ GeoPolygonType[ GeoPolygonType.MultiHeight16bit ] ]++

        let offset = 0

        while ( offset < ( original.length - 1 ) ) {
            let height = extractHeight( original.readInt16LE( offset ) )

            multiHeightData.writeInt16LE( height , 1 + offset )
            offset += 2
        }

        return multiHeightData
    }
}