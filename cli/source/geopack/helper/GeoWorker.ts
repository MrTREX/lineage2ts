import { expose } from 'threads/worker'
import * as fs from 'fs/promises'
import { createHash } from 'node:crypto'
import _ from 'lodash'
import { GeoBlockType, GeoConverter, GeoConverterValues } from './GeoConverter'
import { GeoWorkerOutput } from './IGeoWorker'

function createDataHash( data: Buffer ) : string {
    return createHash( 'sha512' ).update( data ).digest( 'hex' )
}

function convertData( data: Buffer ) : GeoWorkerOutput {
    const converter = new GeoConverter( data )
    let blocks = _.times( GeoConverterValues.BlocksInRegion, () : Buffer => {
        let type : GeoBlockType = converter.readId()

        switch ( type ) {
            case GeoBlockType.Flat:
                return converter.getFlatBlock()

            case GeoBlockType.MultiHeight:
                return converter.getComplexBlock()

            case GeoBlockType.MultiLayer:
                return converter.getMultilayerBlock()
        }

        throw new Error( `Unknown GeoBlockType value: ${type}` )
    } )

    let convertedData = Buffer.concat( blocks )
    let statistics = {
        ...converter.statistics,
        originalSize: Math.floor( data.length / 1024 ),
        convertedSize: Math.floor( convertedData.length / 1024 ),
        sha512: createDataHash( convertedData )
    }

    return [ convertedData, JSON.stringify( statistics ) ]
}

async function processFile( filePath: string ) : Promise<GeoWorkerOutput> {
    let data : Buffer = await fs.readFile( filePath )

    return convertData( data )
}

expose( {
    processFile
} )