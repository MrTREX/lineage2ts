import { IGeoTransformer } from '../interface/IGeoTransformer'
import { IGeopackEngine } from '../engine/IGeopackEngine'
import _ from 'lodash'
import path from 'path'
import { WorkerPool } from '../helper/WorkerPool'

const columns = [
    'regionX',
    'regionY',
    'data',
    'statistics_json'
]

const query = `INSERT INTO geoindex (${ columns.join( ',' ) }) values (${ _.join( _.times( columns.length, _.constant( '?' ) ), ',' ) })`
export class GeoindexTransformer implements IGeoTransformer {
    currentPath: string

    constructor( path: string ) {
        this.currentPath = path
    }

    getCurrentProcessMessage(): string {
        return this.currentPath
    }

    async startProcess( engine: IGeopackEngine ): Promise<void> {
        let [ regionX, regionY ] = path.basename( this.currentPath )
                .replace( '.l2j', '' )
                .split( '_' )
                .map( value => _.parseInt( value ) )

        let workerData = await WorkerPool.queueWork( this.currentPath )
        return engine.insertOne( query, [ regionX, regionY, ...workerData ] )
    }
}