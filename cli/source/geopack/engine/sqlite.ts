import { IGeopackEngine } from './IGeopackEngine'
import _ from 'lodash'
import pkgDir from 'pkg-dir'
import Database from 'better-sqlite3'

const rootDirectory = pkgDir.sync( __dirname )
let database

export const SQLiteEngine : IGeopackEngine = {
    async start( filename: string ): Promise<void> {
        database = new Database( `${rootDirectory}/${filename}`, {
            fileMustExist: false
        } )

        database.pragma( 'journal_mode = WAL' )
    },

    async insertOne( query: string, parameters: Array<any> ): Promise<void> {
        database.prepare( query ).run( ...parameters )
    },

    async executeQuery ( query ) {
        database.exec( query )
    },

    async finish () {
        database.close()
    },

    async insertMany ( query, items: Array<Array<any>> ) {
        let statement = database.prepare( query )

        const insertBulk = database.transaction( ( allItems ) => _.each( allItems, ( itemParameters ) => {
            statement.run( ...itemParameters )
        } ) )

        insertBulk( items )
    }
}
