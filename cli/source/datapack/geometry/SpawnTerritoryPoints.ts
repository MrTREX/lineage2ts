import { PolygonPoint, QuickHull } from '../libraries/quickHull'
import { hachureLines, Line, Point } from '../libraries/hachureLines'
import { RoaringBitmap32, SerializationFormat } from 'roaring'
import { GeometryType, IGeometryGenerator, TerritoryPointsParameters } from './Interfaces'
import * as Buffer from 'buffer'

export type LinePoint = [ number, number ]
const maxDifference = Math.pow( 2, 16 )

export class SpawnTerritoryPoints implements IGeometryGenerator {
    minX: number = Number.MAX_SAFE_INTEGER
    maxX: number = Number.MIN_SAFE_INTEGER

    minY: number = Number.MAX_SAFE_INTEGER
    maxY: number = Number.MIN_SAFE_INTEGER

    data: RoaringBitmap32 = new RoaringBitmap32()
    distance: number

    constructor( points: Array<LinePoint>, distance: number = 100 ) {
        this.distance = distance
        let polygonPoints = points.map( ( point: LinePoint ) => {
            return {
                x: point[ 0 ],
                y: point[ 1 ]
            }
        } )

        let vertices : Array<Point> = QuickHull( polygonPoints ).map( ( point: PolygonPoint ) : Point => {
            this.minX = Math.min( point.x, this.minX )
            this.minY = Math.min( point.y, this.minY )

            this.maxX = Math.max( point.x, this.maxX )
            this.maxY = Math.max( point.y, this.maxY )

            return [ point.x, point.y ]
        } )

        /*
            Due to how data is packed we can only process data difference fitting into 16-bit value (two values fit into 32-bit integer).
            Hence, check to see if there is data that would not be able to be processed.
         */
        let xDifference = Math.abs( this.maxX - this.minX )
        let yDifference = Math.abs( this.maxY - this.minY )
        if ( xDifference > maxDifference || yDifference > maxDifference ) {
            throw new Error( `Unable to process spawn territory with points: ${JSON.stringify( points )}, due to ${JSON.stringify( { xDifference, yDifference, maxDifference } )}` )
        }

        this.distance = this.getAdjustedDistance( xDifference, yDifference, distance )

        let minimumDistance: number = this.distance / 2
        hachureLines( vertices, this.distance, 0 ).forEach( ( line: Line ) : void => {
            return this.getLinePoints( line[ 0 ], line[ 1 ], this.distance, minimumDistance, this.data, this.minX, this.minY )
        } )
    }

    private getAdjustedDistance( xDifference : number, yDifference : number, distance: number ) : number {
        let smallAreaSize = distance * 4
        if ( xDifference < smallAreaSize || yDifference < smallAreaSize ) {
            return Math.floor( distance * 0.4 )
        }

        let extraLargeSize = distance * 100
        if ( xDifference > extraLargeSize && yDifference > extraLargeSize ) {
            return Math.floor( distance * 2 )
        }

        let largeSize = distance * 70
        if ( xDifference > largeSize && yDifference > largeSize ) {
            return Math.floor( distance * 1.5 )
        }

        let mediumSize = distance * 50
        if ( xDifference > mediumSize && yDifference > mediumSize ) {
            return Math.floor( distance * 1.2 )
        }

        return distance
    }

    /*
        Per https://math.stackexchange.com/questions/175896/finding-a-point-along-a-line-a-certain-distance-away-from-another-point
     */
    private getLinePoints( start: LinePoint, finish: LinePoint, distance: number, minimumDistance: number, data: RoaringBitmap32, minX: number, minY: number ) : void {
        let lineSize = Math.hypot( finish[ 0 ] - start[ 0 ], finish[ 1 ] - start[ 1 ] )

        this.addPoint( start[ 0 ], start[ 1 ], data, minX, minY )

        if ( lineSize < distance && lineSize > minimumDistance ) {
            return this.addPoint( finish[ 0 ], finish[ 1 ], data, minX, minY )
        }

        let currentDistance = distance

        while ( currentDistance < lineSize ) {
            let ratio = currentDistance / lineSize
            let x = Math.round( ( 1 - ratio ) * start[ 0 ] + ratio * finish[ 0 ] )
            let y = Math.round( ( 1 - ratio ) * start[ 1 ] + ratio * finish[ 1 ] )

            this.addPoint( x, y, data, minX, minY )

            currentDistance += distance
        }
    }

    private addPoint( originX: number, originY: number, data: RoaringBitmap32, minX: number, minY: number ) : void {
        let x = originX - minX
        let y = originY - minY

        data.add( x + ( y << 16 ) )
    }

    generatePoints(): Buffer {
        // @ts-ignore
        return this.data.serialize( SerializationFormat.croaring )
    }

    getName(): string {
        return 'SpawnTerritory'
    }

    getParameters(): TerritoryPointsParameters {
        let centerX = Math.abs( this.maxX - this.minX ) / 2 + this.minX
        let centerY = Math.abs( this.maxY - this.minY ) / 2 + this.minY

        return {
            centerX,
            centerY,
            distance: this.distance,
            maxX: this.maxX,
            maxY: this.maxY,
            minX: this.minX,
            minY: this.minY,
            size: this.data.size,
        }
    }

    getSize(): number {
        return this.data.size
    }

    getType(): GeometryType {
        return GeometryType.SpawnTerritory
    }

}