import { InitialEquipmentTransformer } from './transformer/initialEquipmentData'
import { PlayerCreationPointsTransformer } from './transformer/playerCreationPoints'
import { ArmorSetsTransformer } from './transformer/ArmorSetsTransformer'
import { StatBonusTransformer } from './transformer/StatBonus'
import { CategoryDataTransformer } from './transformer/CategoryData'
import { CursedWeaponsTransformer } from './transformer/CursedWeapons'
import { EnchantItemHPBonusTransformer } from './transformer/EnchantItemHPBonus'
import { EnchantItemOptionsTransformer } from './transformer/EnchantItemOptions'
import { EnchantItemGroupsTransformer } from './transformer/EnchantItemGroups'
import { EnchantSkillGroupsTransformer } from './transformer/EnchantSkillGroups'
import { ExperienceDataTransformer } from './transformer/ExperienceData'
import { InitialShortcutsTransformer } from './transformer/InitialShortcuts'
import { InstanceNamesTransformer } from './transformer/InstanceNames'
import { PcKarmaIncreaseTransformer } from './transformer/PcKarmaIncrease'
import { NpcDataTransformer } from './transformer/NpcData'
import { OptionDataTransformer } from './transformer/OptionData'
import { PetDataTransformer } from './transformer/PetData'
import { PlayerTemplateDataTransformer } from './transformer/PlayerTemplateData'
import { SkillDataTransformer } from './transformer/SkillData'
import { SkillTreesTransformer } from './transformer/SkillTrees'
import { TransformsDataTransformer } from './transformer/TransformationsData'
import { UIDataTransformer } from './transformer/UIData'
import { HtmlDataTransformer } from './transformer/HtmlData'
import { SkillLearnTransformer } from './transformer/SkillLearn'
import { DoorDataTransformer } from './transformer/DoorData'
import { FishDataTransformer } from './transformer/FishData'
import { FishingMonsterTransformer } from './transformer/FishingMonsterData'
import { FishingRodsTransformer } from './transformer/FishingRods'
import { StaticObjectsTransformer } from './transformer/StaticObjects'
import { XPLostDataTransformer } from './transformer/XPLostData'
import { HennaDataTransformer } from './transformer/HennaData'
import { BuylistDataTransformer } from './transformer/BuylistData'
import { MerchantPriceTransformer } from './transformer/MerchantPriceData'
import { EnchantItemScrollsTransformer } from './transformer/EnchantItemScrolls'
import { EnchantItemSupportsTransformer } from './transformer/EnchantItemSupports'
import { MultisellDataTransformer } from './transformer/MultisellData'
import { HitConditionBonusTransformer } from './transformer/HitConditionBonus'
import { ClassListDataTransformer } from './transformer/ClassListData'
import { ItemAuctionsTransformer } from './transformer/ItemAuctions'
import { AugmentationWeaponsTransformer } from './transformer/AugmentationWeaponData'
import { AugmentationSkillsTransformer } from './transformer/AugmentationSkillData'
import { AugmentationAccessoryTransformer } from './transformer/AugmentationAccessoryData'
import { ManorSeedsTransformer } from './transformer/ManorSeeds'
import { InstanceDataTransformer } from './transformer/InstanceData'
import { LevelupCrystalItemsTransformer } from './transformer/LevelupCrystalItems'
import { LevelupCrystalNpcsTransformer } from './transformer/LevelupCrystalNpcs'
import { QuestDataTransformer } from './transformer/QuestData'
import { TransformerProcess } from './helpers/TransformerProcess'
import { PlayerLevelupRewardsTransformer } from './transformer/PlayerLevelupRewards'
import { NpcBuffsData } from './transformer/NpcBuffsData'
import { TeleportsTransformer } from './transformer/Teleports'
import { GeometryTransformer } from './transformer/GeometryData'
import { SpawnLogicExTransfomer } from './transformer/SpawnLogicEx'
import { SpawnNpcExTransformer } from './transformer/SpawnNpcEx'
import { SpawnTerritoryExTransformer } from './transformer/SpawnTerritoryEx'
import { NpcRoutesTransformer } from './transformer/NpcRoutes'
import { CycleStepTransformer } from './transformer/CycleSetepData'
import { ServitorSkillsTransformer } from './transformer/ServitorSkills'
import { AgathionsTransformer } from './transformer/Agathions'
import { CubicDataTransformer } from './transformer/CubicData'
import { SpawnLogicTransfomer } from './transformer/SpawnLogic'
import { AuctionTemplatesTransformer } from './transformer/AuctionTemplates'
import { AreaTransformer } from './transformer/AreaData'
import { RespawnPointsTransformer } from './transformer/RespawnPoints'
import { PlayerAccessLevelsTransformer } from './transformer/PlayerAccessLevels'
import { ItemsTransformer } from './transformer/Items'
import { RecipesTransformer } from './transformer/Recipes'
import { ProductInfoTransformer } from './transformer/ProductInfo'

export const DatapackTransformers: Array<TransformerProcess> = [
    InitialEquipmentTransformer,
    PlayerCreationPointsTransformer,
    ArmorSetsTransformer,
    StatBonusTransformer,
    CategoryDataTransformer,
    CursedWeaponsTransformer,
    EnchantItemHPBonusTransformer,
    EnchantItemOptionsTransformer,
    EnchantItemGroupsTransformer,
    EnchantSkillGroupsTransformer,
    ExperienceDataTransformer,
    InitialShortcutsTransformer,
    InstanceNamesTransformer,
    ItemsTransformer,
    PcKarmaIncreaseTransformer,
    NpcDataTransformer,
    OptionDataTransformer,
    PetDataTransformer,
    PlayerTemplateDataTransformer,
    RecipesTransformer,
    SkillDataTransformer,
    SkillTreesTransformer,
    TransformsDataTransformer,
    UIDataTransformer,
    HtmlDataTransformer,
    SkillLearnTransformer,
    DoorDataTransformer,
    FishDataTransformer,
    FishingMonsterTransformer,
    FishingRodsTransformer,
    StaticObjectsTransformer,
    XPLostDataTransformer,
    HennaDataTransformer,
    BuylistDataTransformer,
    MerchantPriceTransformer,
    EnchantItemScrollsTransformer,
    EnchantItemSupportsTransformer,
    MultisellDataTransformer,
    HitConditionBonusTransformer,
    NpcRoutesTransformer,
    ClassListDataTransformer,
    ItemAuctionsTransformer,
    AugmentationSkillsTransformer,
    AugmentationWeaponsTransformer,
    AugmentationAccessoryTransformer,
    ManorSeedsTransformer,
    InstanceDataTransformer,
    LevelupCrystalItemsTransformer,
    LevelupCrystalNpcsTransformer,
    QuestDataTransformer,
    PlayerLevelupRewardsTransformer,
    NpcBuffsData,
    TeleportsTransformer,
    GeometryTransformer,
    SpawnLogicTransfomer,
    SpawnLogicExTransfomer,
    SpawnNpcExTransformer,
    SpawnTerritoryExTransformer,
    CycleStepTransformer,
    ServitorSkillsTransformer,
    AgathionsTransformer,
    CubicDataTransformer,
    AuctionTemplatesTransformer,
    AreaTransformer,
    RespawnPointsTransformer,
    PlayerAccessLevelsTransformer,
    ProductInfoTransformer
]