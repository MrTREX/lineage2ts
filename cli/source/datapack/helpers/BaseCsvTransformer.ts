import { TransformerProcess } from './TransformerProcess'
import { IDatapackEngine } from '../engine/IDatapackEngine'
import * as fs from 'fs/promises'
import pkgDir from 'pkg-dir'
import parser from 'csvtojson'
import { CSVParseParam } from 'csvtojson/src/Parameters'

const rootDirectory = pkgDir.sync( __dirname )
const defaultParseOptions = {
    delimiter: ',',
}

export abstract class BaseCsvTransformer implements TransformerProcess {
    parserOptions: any
    fileName : string

    constructor( fileName : string, parserOptions : any = defaultParseOptions ) {
        this.fileName = fileName
        this.parserOptions = parserOptions
    }

    async startProcess( engine: IDatapackEngine ): Promise<void> {
        let parsedData = await this.getCsvFile( this.fileName, this.parserOptions )

        return engine.insertMany( this.getQuery(), this.transformData( parsedData ) )
    }

    abstract transformData( parsedData: Array<unknown> ) : Array<Array<any>>
    abstract getQuery() : string

    getLastProcessedOperation() {
        return this.fileName
    }

    abstract getName() : string

    async getCsvFile( fileName: string, options : Partial<CSVParseParam> = defaultParseOptions ) : Promise<Array<unknown>> {
        let fileContents = await fs.readFile( `${rootDirectory}/${fileName}` )
        return parser( options ).fromString( fileContents.toString() )
    }
}