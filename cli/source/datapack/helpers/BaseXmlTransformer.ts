import { TransformerProcess } from './TransformerProcess'
import { IDatapackEngine } from '../engine/IDatapackEngine'
import aigle from 'aigle'
import { XMLParser } from 'fast-xml-parser'
import * as fs from 'fs/promises'
import pkgDir from 'pkg-dir'

const rootDirectory = pkgDir.sync( __dirname )

export abstract class BaseXmlTransformer implements TransformerProcess {
    files: { [ key: string ]: string } | Array<any>
    parserOptions: any
    lastFileProcessed: string
    private parser: XMLParser

    constructor( files: { [ key: string ]: string } | Array<any>, parserOptions : any ) {
        this.files = files
        this.parserOptions = parserOptions
        this.parser = new XMLParser( parserOptions )
    }

    async startProcess( engine: IDatapackEngine ): Promise<void> {
        let transformer = this
        await aigle.resolve( this.getFiles() ).eachSeries( async ( fileName: string, tableName: string ) => {
            transformer.lastFileProcessed = fileName
            let xmlData = await fs.readFile( `${rootDirectory}/${fileName}` )
            let parsedData = await this.getParser().parse( xmlData.toString() )

            return engine.insertMany( transformer.getQuery( tableName, fileName ), transformer.transformData( parsedData, fileName ) )
        } )
    }

    abstract transformData( parsedData: any, fileName: string ) : Array<Array<any>>
    abstract getQuery( tableName: string, fileName: string ) : string

    getFiles() {
        return this.files
    }

    getLastProcessedOperation() {
        return this.lastFileProcessed
    }

    abstract getName() : string

    getParser() : XMLParser {
        return this.parser
    }
}