import { IDatapackEngine } from '../engine/IDatapackEngine'

export interface TransformerProcess {
    startProcess( engine: IDatapackEngine ) : Promise<void>
    getLastProcessedOperation() : string
    getName() : string
}