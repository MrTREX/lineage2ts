export interface IDatapackEngine {
    executeQuery( query: string ) : Promise<void>
    finish() : Promise<void>
    insertMany( query: string, items: Array<Array<any>> ) : Promise<void>
    insertOne( query: string, parameters : Array<string | number | boolean> ) : Promise<void>
    getName() : string
    start( parameters : Object | string ) : Promise<void>
    executeWithParameters( query: string, parameters: Array<string | number> ) : Promise<void>
}

export const enum DatapackEngineName {
    sqlite = 'SQLite'
}