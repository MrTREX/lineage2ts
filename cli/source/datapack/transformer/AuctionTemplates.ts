import { TransformerOperations } from '../helpers/TransformerOperations'
import { BaseCsvTransformer } from '../helpers/BaseCsvTransformer'

const parseOptions = {
    delimiter: ',',
}

const query = TransformerOperations.createQuery( 'auction_templates', [
    'id',
    'type',
    'name',
    'startingBid'
], true )

class AuctionTemplates extends BaseCsvTransformer {
    getName(): string {
        return 'Auction Templates'
    }

    getQuery(): string {
        return query
    }

    transformData( parsedData: Array<unknown> ): Array<Array<any>> {
        return parsedData.map( ( item: any ) => {
            return [
                item.id,
                'ClanHall',
                item.name,
                item.startingBid
            ]
        } )
    }
}

export const AuctionTemplatesTransformer = new AuctionTemplates( 'overrides/data/csv/clanHallAuctionTemplate.csv', parseOptions )