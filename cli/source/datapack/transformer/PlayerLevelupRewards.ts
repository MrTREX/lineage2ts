import { BaseCsvTransformer } from '../helpers/BaseCsvTransformer'

import _ from 'lodash'

const parseOptions = {
    delimiter: ',',
}

class Transformer extends BaseCsvTransformer {
    getQuery(): string {
        return 'INSERT into player_levelup_rewards (level, race, itemId, itemAmount) values (?,?,?,?)'
    }

    transformData( parsedData: Array<unknown> ): Array<Array<any>> {
        return _.map( parsedData, ( item: any ) => {
            let { level, race, itemId, itemAmount } = item

            return [ _.parseInt( level ), race, _.parseInt( itemId ), _.parseInt( itemAmount ) ]
        } )
    }

    getName(): string {
        return 'Player LevelUp Rewards'
    }
}

export const PlayerLevelupRewardsTransformer = new Transformer( 'overrides/data/csv/playerLevelupRewards.csv', parseOptions )