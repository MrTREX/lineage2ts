import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import { IDatapackEngine } from '../engine/IDatapackEngine'
import { TransformerOperations } from '../helpers/TransformerOperations'
import _ from 'lodash'
import aigle from 'aigle'
import { searchFiles } from 'find-file-extension'
import * as fs from 'fs/promises'
import pkgDir from 'pkg-dir'
import path from 'path'

const rootDirectory = pkgDir.sync( __dirname )
const fileDirectory = 'data/buylists'

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true
}

const columns = [
    'listId',
    'items_json',
    'npcId'
]

const query = TransformerOperations.createQuery( 'buylists_data', columns )

class Transformer extends BaseXmlTransformer {

    getQuery(): string {
        return query
    }

    async startProcess( engine: IDatapackEngine ): Promise<void> {
        let transformer = this
        const files = searchFiles( 'xml', `${ rootDirectory }/${ fileDirectory }` )

        await aigle.resolve( files ).eachSeries( async ( fileName: string ) => {
            transformer.lastFileProcessed = fileName
            let xmlData = await fs.readFile( fileName )
            let parsedData = await this.getParser().parse( xmlData.toString(), transformer.parserOptions )

            return engine.insertMany( transformer.getQuery(), transformer.transformData( parsedData, fileName ) )
        } )
    }

    transformData( parsedData: any, fileName: string ): Array<Array<any>> {
        let itemsToInsert: Array<Array<any>> = []
        let buylistId = _.parseInt( _.replace( path.basename( fileName ), '.xml', '' ) )
        let template = _.get( parsedData, 'list' )

        let npcs = _.get( template, 'npcs.npc' )

        if ( npcs ) {
            npcs = _.castArray( npcs )

            if ( npcs.length > 1 ) {
                throw Error( `Buylist with id=${ buylistId } has multiple npcs attached.` )
            }
        }

        itemsToInsert.push( [
            buylistId,
            TransformerOperations.getJson( _.castArray( template.item ) ),
            npcs ? ( npcs[ 0 ] as number ).toFixed() : null
        ] )

        return itemsToInsert
    }

    getName(): string {
        return 'Buylist Data'
    }
}

export const BuylistDataTransformer = new Transformer( null, parserOptions )
