import { BaseCsvTransformer } from '../helpers/BaseCsvTransformer'
import { TransformerOperations } from '../helpers/TransformerOperations'
import { npcRouteMethods } from './SpawnNpcEx'

const query = TransformerOperations.createQuery( 'npc_routes', [
    'name',
    'type',
    'nodes_json'
] )

class NpcRoutes extends BaseCsvTransformer {
    getName(): string {
        return 'Npc Walking Routes'
    }

    getQuery(): string {
        return query
    }

    transformData( parsedData: Array<unknown> ): Array<Array<any>> {
        return parsedData.map( ( item: any ) => {
            let type = npcRouteMethods[ `@${ item.type }` ]

            if ( !type ) {
                throw new Error( `Unable to map npc route type using value=${ item.type } for route name=${item.name}` )
            }

            return [
                item.name,
                type,
                item.nodesJson
            ]
        } )
    }
}

export const NpcRoutesTransformer = new NpcRoutes( 'overrides/data/csv/npcWalkingRoutes.csv', {
    delimiter: ',',
} )