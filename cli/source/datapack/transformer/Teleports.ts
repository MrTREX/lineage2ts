import { BaseCsvTransformer } from '../helpers/BaseCsvTransformer'
import _ from 'lodash'
import { TransformerOperations } from '../helpers/TransformerOperations'

const parseOptions = {
    delimiter: ',',
}

const query = TransformerOperations.createQuery( 'teleports', [
    'id',
    'name',
    'x',
    'y',
    'z',
    'itemId',
    'amount',
    'isNoble',
    'showPrice',
] )

class Transformer extends BaseCsvTransformer {
    getQuery(): string {
        return query
    }

    transformData( parsedData: Array<unknown> ): Array<Array<any>> {
        return _.map( parsedData, ( item: any ) => {
            let { name, id, x, y, z, amount, isNoble, itemId, showPrice } = item

            return [
                _.parseInt( id ),
                name,
                _.parseInt( x ),
                _.parseInt( y ),
                _.parseInt( z ),
                _.parseInt( itemId ),
                _.parseInt( amount ),
                _.parseInt( isNoble ),
                _.parseInt( showPrice ),
            ]
        } )
    }

    getName(): string {
        return 'Teleports'
    }
}

export const TeleportsTransformer = new Transformer( 'overrides/data/csv/teleports.csv', parseOptions )