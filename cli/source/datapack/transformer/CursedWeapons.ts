import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import _ from 'lodash'
import { TransformerOperations } from '../helpers/TransformerOperations'

const files = {
    'cursed_weapons': 'data/cursedWeapons.xml'
}

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true
}

const columns = [
    'itemId',
    'name',
    'skillId',
    'disappearChance',
    'dropRate',
    'duration',
    'durationLost',
    'stageKills',
]

const query = TransformerOperations.createQuery( 'cursed_weapons', columns )

class Transformer extends BaseXmlTransformer {

    getQuery(): string {
        return query
    }

    transformData( parsedData: any ): Array<Array<any>> {
        let itemsToInsert: Array<Array<any>> = []

        const items = _.get( parsedData, 'list.item' )
        _.each( _.castArray( items ), ( item: any ) => {
            let { id, name, skillId } = item
            itemsToInsert.push( [
                id,
                name,
                skillId,
                item.disappearChance.val,
                item.dropRate.val,
                item.duration.val,
                item.durationLost.val,
                item.stageKills.val
            ] )
        } )

        return itemsToInsert
    }

    getName(): string {
        return 'Cursed Weapons'
    }
}

export const CursedWeaponsTransformer = new Transformer( files, parserOptions )