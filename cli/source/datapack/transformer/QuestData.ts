import { BaseCsvTransformer } from '../helpers/BaseCsvTransformer'
import _ from 'lodash'

const parseOptions = {
    delimiter: ',',
}

class Transformer extends BaseCsvTransformer {
    getQuery(): string {
        return 'INSERT into quest_data (questId, name) values (?,?)'
    }

    transformData( parsedData: Array<unknown> ): Array<Array<any>> {
        let allIds = new Set<number>()
        return _.map( parsedData, ( item: any ) => {
            let { questId, name } = item

            if ( allIds.has( questId ) ) {
                throw new Error( `repeated quest id detected : ${ questId }` )
            }

            allIds.add( questId )
            return [ _.parseInt( questId ), name ]
        } )
    }

    getName(): string {
        return 'Quest Data'
    }
}

export const QuestDataTransformer = new Transformer( 'overrides/data/csv/questNames.csv', parseOptions )