import { BaseCsvTransformer } from '../helpers/BaseCsvTransformer'
import { TransformerOperations } from '../helpers/TransformerOperations'
import _ from 'lodash'

const parseOptions = {
    delimiter: ',',
}

const query = TransformerOperations.createQuery( 'spawn_logic', [
    'makerId',
    'territories_json',
    'maxAmount',
    'parameters_json'
] )

interface MakerParameters {
    trigger?: {
        type: string
        parameters: Array<string | number>
    }
    eventName?: string
}

export function createParameters( value: Object ) : Object {
    if ( !value ) {
        return {}
    }

    let parameters : MakerParameters = {}

    let eventName = value[ 'eventName' ]
    if ( eventName ) {
        parameters.eventName = eventName
    }

    let spawnTime = value[ 'spawnTime' ]
    if ( spawnTime ) {
        let [ type, data ] = spawnTime.split( '(' )

        parameters.trigger = {
            type: _.camelCase( type ),
            parameters: ( data as string )
                .replace( ')', '' )
                .split( ';' )
                .map( value => {
                    let parameter = value.trim()
                    let parsedValue = parseInt( parameter )
                    if ( Number.isNaN( parsedValue ) ) {
                        return parameter
                    }

                    return parsedValue
                } )
        }
    }

    return parameters
}

class SpawnLogicEx extends BaseCsvTransformer {
    getName(): string {
        return 'Spawn Logic Extra'
    }

    getQuery(): string {
        return query
    }

    transformData( parsedData: Array<unknown> ): Array<Array<any>> {
        return _.map( parsedData, ( item: any ) => {
            let parameters = JSON.parse( item.parametersJson )
            let territories = JSON.parse( item.territoryJson )

            return [
                item.name,
                TransformerOperations.getJson( Object.values( territories ) ),
                parameters.maximumNpc,
                TransformerOperations.getJson( createParameters( parameters ) ),
            ]
        } )
    }

}

export const SpawnLogicTransfomer = new SpawnLogicEx( 'overrides/data/csv/npcMaker.csv', parseOptions )