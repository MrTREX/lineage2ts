import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import { TransformerOperations } from '../helpers/TransformerOperations'
import _ from 'lodash'

const files = {
    'augmentation_skillmap': 'data/stats/augmentation/retailchances.xml',
}

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true,
}

const columns = [
    'classType',
    'stoneId',
    'variationId',
    'categoryProbability',
    'chance_json',
]

const query = TransformerOperations.createQuery( 'augmentation_weapons', columns )

class Transformer extends BaseXmlTransformer {

    getQuery(): string {
        return query
    }

    transformData( parsedData: any ): Array<Array<any>> {
        let itemsToInsert: Array<Array<any>> = []

        let augmentations = _.get( parsedData, 'list.weapon' )
        _.each( _.castArray( augmentations ), ( weapon: any ) => {
            let classType = weapon.type

            _.each( _.castArray( weapon.stone ), ( stoneData: any ) => {
                let stoneId = stoneData.id

                _.each( _.castArray( stoneData.variation ), ( variationData: any ) => {
                    let variationId = variationData.id

                    _.each( _.castArray( variationData.category ), ( categoryData: any ) => {
                        let categoryProbability = categoryData.probability

                        itemsToInsert.push( [
                            classType,
                            stoneId,
                            variationId,
                            categoryProbability,
                            TransformerOperations.getJson( _.castArray( categoryData.augment ) ),
                        ] )
                    } )
                } )
            } )
        } )

        return itemsToInsert
    }

    getName(): string {
        return 'Augmentation Weapons'
    }
}

export const AugmentationWeaponsTransformer = new Transformer( files, parserOptions )