import { BaseCsvTransformer } from '../helpers/BaseCsvTransformer'
import { TransformerOperations } from '../helpers/TransformerOperations'

const query = TransformerOperations.createQuery( 'product_info', [
    'id',
    'category',
    'enabled',
    'price',
    'type',
    'items_json'
] )

const productTypes = new Set( [
    'none',
    'event',
    'best',
    'new'
] )

function ensureProductType( type: string ) : string {
    if ( !productTypes.has( type ) ) {
        throw new Error( `Invalid product type=${type}, valid types=${Array.from( productTypes ).join( ',' )}` )
    }

    return type
}

interface ProductItem {
    id: number
    amount: number
}

function createItems( allItems: string ) : Array<ProductItem> {
    return allItems.split( '|' ).map( ( itemLine: string ) : ProductItem => {
        let values = itemLine.split( ';' )

        if ( values.length !== 2 ) {
            throw new Error( `Invalid product item=${itemLine}, must have "id;amount" syntax` )
        }

        let id = parseInt( values[ 0 ], 10 )
        let amount = parseInt( values[ 1 ], 10 )

        if ( !Number.isInteger( id ) || !Number.isInteger( amount ) ) {
            throw new Error( `Un-parsable integer value for product item=${itemLine}` )
        }

        return {
            id,
            amount
        }
    } )
}

class ProductInfoItems extends BaseCsvTransformer {
    getName(): string {
        return 'PC Points Products'
    }

    getQuery(): string {
        return query
    }

    transformData( parsedData: Array<unknown> ): Array<Array<any>> {
        return parsedData.map( ( item: any ) => {
            return [
                TransformerOperations.parseInt( item.id ),
                TransformerOperations.parseInt( item.category ),
                TransformerOperations.parseInt( item.enabled ),
                TransformerOperations.parseInt( item.price ),
                ensureProductType( item.productType ),
                TransformerOperations.getJson( createItems( item.items ) )
            ]
        } )
    }
}

export const ProductInfoTransformer = new ProductInfoItems( 'overrides/data/csv/productInfo.csv' )