import { BaseCsvTransformer } from '../helpers/BaseCsvTransformer'
import _ from 'lodash'

const parseOptions = {
    delimiter: ',',
}

const fieldNames : Array<string> = [ 'npcId', 'skillId', 'skillLevel', 'consumedItemId', 'consumedItemAmount', 'groupId' ]
class Transformer extends BaseCsvTransformer {
    getQuery(): string {
        return 'INSERT into npcbuffs_data (npcId, skillId, skillLevel, consumedItemId, consumedItemAmount, groupId) values (?,?,?,?,?,?)'
    }

    transformData( parsedData: Array<unknown> ): Array<Array<any>> {
        return _.map( parsedData, ( item: any ) : Array<number> => {
            return _.map( fieldNames, ( propertyName : string ) : number => {
                return _.parseInt( item[ propertyName ] )
            } )
        } )
    }

    getName(): string {
        return 'Npc Buffs Data'
    }
}

export const NpcBuffsData = new Transformer( 'overrides/data/csv/npcBuffs.csv', parseOptions )