import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import _ from 'lodash'
import { TransformerOperations } from '../helpers/TransformerOperations'

const files = {
    'fishing_monsters': 'data/stats/fishing/fishingMonsters.xml'
}

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true
}

const columns = [
    'id',
    'probability',
    'userMinLevel',
    'userMaxLevel'
]

const query = TransformerOperations.createQuery( 'fishing_monsters', columns )


class Transformer extends BaseXmlTransformer {

    getQuery(): string {
        return query
    }

    transformData( parsedData: any ): Array<Array<any>> {
        let itemsToInsert: Array<Array<any>> = []

        let fishItems = _.get( parsedData, 'list.fishingMonster' )
        _.each( _.castArray( fishItems ), ( item: any ) => {

            itemsToInsert.push( [
                item.fishingMonsterId,
                item.probability,
                item.userMinLevel,
                item.userMaxLevel,
            ] )
        } )

        return itemsToInsert
    }

    getName(): string {
        return 'Fishing Monster Data'
    }
}

export const FishingMonsterTransformer = new Transformer( files, parserOptions )