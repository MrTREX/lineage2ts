import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import _ from 'lodash'

const files = {
    'instance_names': 'data/instancenames.xml'
}

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true
}

class Transformer extends BaseXmlTransformer {

    getQuery( tableName: string ): string {
        return `INSERT into ${ tableName } (id, name) values (?,?)`
    }

    transformData( parsedData: any ): Array<Array<any>> {
        let itemsToInsert: Array<Array<any>> = []

        _.each( _.get( parsedData, 'list.instance' ), ( item: any ) => {
            let { id, name } = item
            itemsToInsert.push( [ id, name ] )
        } )

        return itemsToInsert
    }

    getName(): string {
        return 'Instance Names'
    }
}

export const InstanceNamesTransformer = new Transformer( files, parserOptions )