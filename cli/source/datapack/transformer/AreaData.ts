import { TransformerOperations } from '../helpers/TransformerOperations'
import { TransformerProcess } from '../helpers/TransformerProcess'
import { IDatapackEngine } from '../engine/IDatapackEngine'
import fs from 'fs/promises'
import parser from 'csvtojson'
import pkgDir from 'pkg-dir'
import _ from 'lodash'

const query = TransformerOperations.createQuery( 'areas', [
    'id',
    'type',
    'points_json',
    'minZ',
    'maxZ',
    'properties_json'
] )

const parseOptions = {
    delimiter: ',',
}

const rootDirectory = pkgDir.sync( __dirname )

interface SkillItem {
    id: number
    level: number
}

interface Point {
    x: number
    y: number
}

interface AreaProperties {
    points: Array<Point>
    minZ: number
    maxZ: number
    properties: Object
    name: string
}

function asJsonArray( value: string ): string {
    return value.replaceAll( '{', '[' )
        .replaceAll( '}', ']' )
        .replaceAll( ';', ',' )
        .trim()
}

function extractProperties( item: any, area: AreaProperties, skillReferenceMap: Record<string, SkillItem> ): void {
    let data = structuredClone( item )

    delete data.name
    delete data.type
    delete data.region
    delete data.points

    _.each( data, ( value: string, key: string ): void => {
        if ( !value ) {
            return
        }

        let propertyValue: any = value

        switch ( key ) {
            case 'skill_name':
                propertyValue = [ skillReferenceMap[ value ] ]
                key = 'skills'
                break

            case 'skill_prob':
                propertyValue = parseInt( value ) * 0.01
                key = 'skillProbability'
                break

            case 'skill_list':
                propertyValue = value.split( ';' ).map( skillName => skillReferenceMap[ skillName ] )
                key = 'skills'
                break

            case 'teleport_points':
                propertyValue = JSON.parse( asJsonArray( value ) )
                break

            case 'blocked_actions':
                propertyValue = value.replaceAll( '{', '' ).replaceAll( '}', '' ).split( ';' )
                break

            case 'event_id':
            case 'unit_tick':
            case 'instantzone_id':
                propertyValue = parseInt( value )
                break

            case 'banned_range':
                return

            case 'default_status':
                key = 'isEnabled'
                propertyValue = value === 'on'
                break

            default:
                let convertedValue = parseInt( value )
                propertyValue = Number.isNaN( convertedValue ) ? value : convertedValue
                break
        }

        area.properties[ _.camelCase( key ) ] = propertyValue
    } )
}

function extractPoints( item: any, area: AreaProperties ): void {
    let points = JSON.parse( ( item.points as string ).replaceAll( ';', ',' ) ) as Array<Array<number>>

    area.points = points.map( ( values: Array<number> ): Point => {
        let point: Point = {
            x: Math.floor( values[ 0 ] ),
            y: Math.floor( values[ 1 ] )
        }

        let maxZ = item.type === 'water' ? values[ 2 ] : values[ 3 ]

        area.minZ = Math.min( area.minZ, values[ 2 ] )
        area.maxZ = Math.max( area.maxZ, maxZ )

        return point
    } )
}

function processArea( data: any, skillReferenceMap: Record<string, SkillItem> ): AreaProperties {
    const area: AreaProperties = {
        maxZ: Number.MIN_SAFE_INTEGER,
        minZ: Number.MAX_SAFE_INTEGER,
        points: [],
        properties: {},
        name: data.name
    }

    extractPoints( data, area )
    extractProperties( data, area, skillReferenceMap )

    return area
}

function processRestrictedArea( data: any ): AreaProperties {
    const area: AreaProperties = {
        maxZ: Number.MIN_SAFE_INTEGER,
        minZ: Number.MAX_SAFE_INTEGER,
        points: null,
        properties: {},
        name: data.name
    }

    extractPoints( data, area )

    if ( data.messageId ) {
        area.properties[ 'messageId' ] = data.messageId
    }

    if ( !area.name ) {
        area.name = data.type
    }

    return area
}

function processRespawnArea( data: any ): AreaProperties {
    const area: AreaProperties = {
        maxZ: Number.MIN_SAFE_INTEGER,
        minZ: Number.MAX_SAFE_INTEGER,
        points: null,
        properties: JSON.parse( data.propertiesJson ),
        name: data.name
    }

    extractPoints( data, area )

    if ( !area.name ) {
        area.name = data.type
    }

    return area
}

function getName( currentName: string, allNames: Record<string, number> ): string {
    if ( allNames[ currentName ] ) {
        let newName = `${ currentName }-${ allNames[ currentName ] }`
        allNames[ currentName ]++

        return newName
    }

    allNames[ currentName ] = 1

    return currentName
}

interface TeleportLocation {
    x: number
    y: number
    z: number
}

interface TeleportData extends TeleportLocation {
    type?: string
}

type Teleports = Record<string, Array<TeleportLocation>>

function buildTeleports( data: string ): Teleports {
    let items = JSON.parse( data ) as Array<TeleportData>

    return items.reduce( ( allTeleports: Teleports, item: TeleportData ): Teleports => {
        let type = item.type

        if ( !type ) {
            type = 'normal'
        }

        if ( !allTeleports[ type ] ) {
            allTeleports[ type ] = []
        }

        delete item.type

        allTeleports[ type ].push( item )

        return allTeleports
    }, {} )
}

class Transformer implements TransformerProcess {
    operationName: string

    getName(): string {
        return 'Area Data'
    }

    getQuery(): string {
        return query
    }

    getLastProcessedOperation(): string {
        return this.operationName
    }

    async getCsvFile( fileName: string ): Promise<Array<unknown>> {
        let fileContents = await fs.readFile( `${ rootDirectory }/${ fileName }` )
        return parser( parseOptions ).fromString( fileContents.toString() )
    }

    async startProcess( engine: IDatapackEngine ): Promise<void> {
        this.operationName = 'Reading csv data'
        let skillReferences = await this.getCsvFile( 'overrides/data/csv/skillReferenceIds.csv' )

        const allNames: Record<string, number> = {
            respawn: 1,
            landing: 1
        }

        let areasData = await this.getCsvFile( 'overrides/data/csv/areas/areas.csv' )
        this.operationName = 'Populating normal area data'
        await engine.insertMany( this.getQuery(), this.transformNormalData( areasData, skillReferences, allNames ) )

        let restrictedData = await this.getCsvFile( 'overrides/data/csv/areas/restrictedAreas.csv' )
        this.operationName = 'Populating restricted area data'
        await engine.insertMany( this.getQuery(), this.transformRestrictedData( restrictedData, allNames ) )

        let respawnData = await this.getCsvFile( 'overrides/data/csv/areas/respawnAreas.csv' )
        this.operationName = 'Populating respawn area data'
        await engine.insertMany( this.getQuery(), this.transformRespawnData( respawnData, allNames ) )

        let townData = await this.getCsvFile( 'overrides/data/csv/areas/townAreas.csv' )
        this.operationName = 'Populating town area data'
        await engine.insertMany( this.getQuery(), this.transformTownData( townData, allNames ) )

        let residenceData = await this.getCsvFile( 'overrides/data/csv/areas/residenceAreas.csv' )
        this.operationName = 'Populating castle area data'
        await engine.insertMany( this.getQuery(), this.transformResidenceData( residenceData, allNames ) )

        let skillAreas = await this.getCsvFile( 'overrides/data/csv/areas/skillReferenceAreas.csv' )
        this.operationName = 'Populating skill reference area data'
        await engine.insertMany( this.getQuery(), this.transformSkillReferenceData( skillAreas, allNames ) )

        let jailAreas = await this.getCsvFile( 'overrides/data/csv/areas/jailAreas.csv' )
        this.operationName = 'Populating jail area data'
        await engine.insertMany( this.getQuery(), this.transformJailData( jailAreas, allNames ) )

        let olympiadAreas = await this.getCsvFile( 'overrides/data/csv/areas/olympiadAreas.csv' )
        this.operationName = 'Populating jail area data'
        await engine.insertMany( this.getQuery(), this.transformOlympiadData( olympiadAreas, allNames ) )

        let questAreas = await this.getCsvFile( 'overrides/data/csv/areas/questAreas.csv' )
        this.operationName = 'Populating jail area data'
        await engine.insertMany( this.getQuery(), this.transformQuestData( questAreas, allNames ) )
    }

    transformNormalData( parsedData: Array<unknown>, skillReferences: Array<unknown>, allNames: Record<string, number> ): Array<Array<any>> {
        let skillReferenceMap = skillReferences.reduce( ( finalMap: Record<string, SkillItem>, item: any ): Record<string, SkillItem> => {

            finalMap[ item.name ] = {
                id: parseInt( item.skillId ),
                level: parseInt( item.skillLevel ),
            }

            return finalMap
        }, {} ) as Record<string, SkillItem>

        return parsedData.map( ( item: any ) => {
            let data = processArea( item, skillReferenceMap )
            let id = getName( data.name, allNames )

            if ( item.region ) {
                data.properties[ 'region' ] = item.region
            }

            return [
                id,
                _.camelCase( item.type ),
                TransformerOperations.getJson( data.points ),
                data.minZ,
                data.maxZ,
                TransformerOperations.getJson( data.properties )
            ]
        } )
    }

    transformRestrictedData( parsedData: Array<unknown>, allNames: Record<string, number> ): Array<Array<any>> {
        return parsedData.map( ( item: any ) => {
            let data = processRestrictedArea( item )
            let id = getName( data.name, allNames )

            return [
                id,
                _.camelCase( item.type ),
                TransformerOperations.getJson( data.points ),
                data.minZ,
                data.maxZ,
                TransformerOperations.getJson( data.properties )
            ]
        } )
    }

    transformRespawnData( parsedData: Array<unknown>, allNames: Record<string, number> ): Array<Array<any>> {
        return parsedData.map( ( item: any ) => {
            let data = processRespawnArea( item )
            let id = getName( data.name, allNames )

            return [
                id,
                _.camelCase( item.type ),
                TransformerOperations.getJson( data.points ),
                data.minZ,
                data.maxZ,
                TransformerOperations.getJson( data.properties )
            ]
        } )
    }

    transformTownData( parsedData: Array<unknown>, allNames: Record<string, number> ): Array<Array<any>> {
        return parsedData.map( ( item: any ) => {
            let name = _.snakeCase( item.name ).toLowerCase()
            let id = getName( name, allNames )

            let properties = item.propertiesJson ? JSON.parse( item.propertiesJson ) : {}

            properties.name = item.name

            return [
                id,
                item.type,
                item.pointsJson,
                parseInt( item.minZ ),
                parseInt( item.maxZ ),
                TransformerOperations.getJson( properties )
            ]
        } )
    }

    transformResidenceData( parsedData: Array<unknown>, allNames: Record<string, number> ): Array<Array<any>> {
        return parsedData.map( ( item: any ) => {
            let name = _.startCase( item.name )
                .replaceAll( 'Agit', 'Clan Hall' )
                .replaceAll( '00', '' )

            let id = getName( item.name, allNames )

            let properties = item.propertiesJson ? JSON.parse( item.propertiesJson ) : {}

            properties.name = name

            if ( item.teleportsJson ) {
                properties.teleports = buildTeleports( item.teleportsJson )
            }

            return [
                id,
                item.type,
                item.pointsJson,
                parseInt( item.minZ ),
                parseInt( item.maxZ ),
                TransformerOperations.getJson( properties )
            ]
        } )
    }

    transformSkillReferenceData( parsedData: Array<unknown>, allNames: Record<string, number> ): Array<Array<any>> {
        return parsedData.map( ( item: any ) => {
            let referenceId = parseInt( item.referenceId, 10 )
            if ( !referenceId ) {
                throw new Error( `Skill reference area with name=${ item.name } must have referenceId` )
            }

            let id = getName( item.name, allNames )
            let properties = {
                referenceId
            }

            return [
                id,
                item.type,
                item.pointsJson,
                parseInt( item.minZ ),
                parseInt( item.maxZ ),
                TransformerOperations.getJson( properties )
            ]
        } )
    }

    transformJailData( parsedData: Array<unknown>, allNames: Record<string, number> ): Array<Array<any>> {
        return parsedData.map( ( item: any ) => {
            let id = getName( item.name, allNames )

            return [
                id,
                item.type,
                item.pointsJson,
                parseInt( item.minZ, 10 ),
                parseInt( item.maxZ, 10 ),
                null
            ]
        } )
    }

    transformQuestData( parsedData: Array<unknown>, allNames: Record<string, number> ): Array<Array<any>> {
        return parsedData.map( ( item: any ) => {
            let id = getName( item.name, allNames )

            return [
                id,
                item.type,
                item.pointsJson,
                parseInt( item.minZ, 10 ),
                parseInt( item.maxZ, 10 ),
                null
            ]
        } )
    }

    transformOlympiadData( parsedData: Array<unknown>, allNames: Record<string, number> ): Array<Array<any>> {
        return parsedData.map( ( item: any ) => {
            let id = getName( item.name, allNames )

            let properties = item.propertiesJson ? JSON.parse( item.propertiesJson ) : {}
            let teleports = JSON.parse( item.teleportsJson )
            let spectatorLocation = _.find( teleports, item => item.type === 'spectator' )
            let centerPoint = JSON.parse( item.pointsJson )
            let radius = parseInt( item.radius, 10 )

            let maxX = centerPoint.x + radius
            let maxY = centerPoint.y + radius
            let minX = centerPoint.x - radius
            let minY = centerPoint.y - radius

            let allPoints = [
                {
                    x: maxX,
                    y: maxY,
                },
                {
                    x: minX,
                    y: minY
                }
            ]

            delete spectatorLocation.type

            properties.radius = parseInt( item.radius, 10 )
            properties.teleports = _.filter( teleports, item => item.type !== 'spectator' )
            properties.spectator = spectatorLocation
            properties.centerPoint = centerPoint

            return [
                id,
                item.type,
                TransformerOperations.getJson( allPoints ),
                parseInt( item.minZ, 10 ),
                parseInt( item.maxZ, 10 ),
                TransformerOperations.getJson( properties )
            ]
        } )
    }
}

export const AreaTransformer = new Transformer()