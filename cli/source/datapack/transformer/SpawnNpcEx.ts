import { TransformerOperations } from '../helpers/TransformerOperations'
import { cleanValue, getAiParameters } from './SpawnLogicEx'
import { IDatapackEngine } from '../engine/IDatapackEngine'
import fs from 'fs/promises'
import parser from 'csvtojson'
import pkgDir from 'pkg-dir'
import { TransformerProcess } from '../helpers/TransformerProcess'

const parseOptions = {
    delimiter: ',',
}

const rootDirectory = pkgDir.sync( __dirname )

const query = TransformerOperations.createQuery( 'spawn_npc_data_ex', [
        'npcId',
        'makerId',
        'position_json',
        'amount',
        'respawnMs',
        'respawnExtraMs',
        'aiName',
        'ai_json',
        'chaseDistance',
        'recordId',
        'minions_json'
] )

function getPosition( value: string ) : Array<number> {
    if ( !value || value === 'anywhere' ) {
        return null
    }

    return cleanValue( value )
            .split( ';' )
            .map( value => parseInt( value, 10 ) )
}

const enum DurationModifier {
    Second = 'sec',
    Minute = 'min',
    Hour = 'hour'
}

const durationMsMultipliers = [ 1000, 60000, 3600000 ]
const durationModifiers = [ DurationModifier.Second, DurationModifier.Minute, DurationModifier.Hour ]

function getDuration( value: string ) : number {
    if ( !value || value === 'no' ) {
        return null
    }

    let trimmedValue = value.trim()
    let index : number = durationModifiers.findIndex( ( duration: DurationModifier ) : boolean => trimmedValue.endsWith( duration ) )
    if ( index < 0 ) {
        throw new Error( `Unrecognized duration modifier in value=${trimmedValue}` )
    }

    let modifier = durationModifiers[ index ]
    let multiplier = durationMsMultipliers[ index ]
    if ( !multiplier ) {
        throw new Error( `Unable to locate duration multiplier for modifier=${modifier}` )
    }

    let amount = parseInt( trimmedValue.replace( modifier, '' ) )

    return amount * multiplier
}

interface MinionDefinition {
    npcId: number
    amount: number
    durationMs: number
}

function getMinionParameters( value: string, npcReferences: Record<string, number> ) : Array<MinionDefinition> {
    if ( !value ) {
        return null
    }

    return value.split( ';' ).reduce( ( items: Array<MinionDefinition>, currentLine: string ) : Array<MinionDefinition> => {
        if ( !currentLine ) {
            return items
        }

        let [ referenceId, unUsedName, amountValue, durationValue ] = currentLine.split( ':' )

        let npcId = npcReferences[ referenceId ]

        if ( !npcId ) {
            throw new Error( `Unable to find minion by reference id=${referenceId}` )
        }

        items.push( {
            npcId,
            amount: parseInt( amountValue, 10 ),
            durationMs: getDuration( durationValue )
        } )

        return items
    }, [] )
}

export const npcRouteMethods : Record<string, string> = {
    '@MoveSuperPoint_FollowRail': 'follow', // moving from A to B, then moving to B to A, repeat
    '@MoveSuperPoint_Random': 'random', // random selection of next point for movement
    '@MoveSuperPoint_FollowRail_Restart': 'restart', // looping A to B movement
    '@MoveSuperPoint_FollowRail_Restart_Teleport': 'teleport' // starting from A to B, teleporting at end to A
}

function adjustAiParameters( aiParameters: Object, npcRoutesMap: Record<string, string>, npcId: number, referenceId: string ) : Object {
    if ( aiParameters && aiParameters[ 'superPointMethod' ] ) {
        let rawMethod = aiParameters[ 'superPointMethod' ] as string
        let mappedMethod = npcRouteMethods[ rawMethod ]
        if ( !mappedMethod ) {
            throw new Error( `Unable to map superPointMethod using value=${ rawMethod } , using npcId=${npcId} and npcReference=${referenceId}` )
        }

        aiParameters[ 'superPointMethod' ] = mappedMethod
    }

    let routeName = npcRoutesMap[ referenceId ]
    if ( routeName ) {
        if ( !aiParameters ) {
            return {
                superPointName: routeName
            }
        }

        if ( !aiParameters[ 'superPointName' ] ) {
            aiParameters[ 'superPointName' ] = routeName
        }
    }

    return aiParameters
}

class SpawnNpcEx implements TransformerProcess {
    operationName: string
    spawnFileName: string = 'overrides/data/csv/npcSpawnEx.csv'

    getName(): string {
        return 'Spawn Npc Ex Data'
    }

    getQuery(): string {
        return query
    }

    async getCsvFile( fileName: string ) : Promise<Array<unknown>> {
        let fileContents = await fs.readFile( `${rootDirectory}/${fileName}` )
        return parser( parseOptions ).fromString( fileContents.toString() )
    }

    async startProcess( engine: IDatapackEngine ): Promise<void> {
        this.operationName = 'Reading csv data'
        let spawnContents = await this.getCsvFile( this.spawnFileName )
        let npcReferences = await this.getCsvFile( 'overrides/data/csv/npcReferenceIds.csv' )
        let npcRoutes = await this.getCsvFile( 'overrides/data/csv/npcWalkingRoutes.csv' )

        this.operationName = this.spawnFileName
        return engine.insertMany( this.getQuery(), this.transformData( spawnContents, npcReferences, npcRoutes ) )
    }

    transformData( spawnContents: Array<unknown>, npcReferences: Array<unknown>, npcRoutes: Array<unknown> ): Array<Array<any>> {
        let npcReferenceMap = npcReferences.reduce( ( finalMap: Record<string, number>, item: any ) : Record<string, number> => {

            finalMap[ item.referenceId ] = parseInt( item.npcId, 10 )

            return finalMap
        }, {} ) as Record<string, number>

        let npcRoutesMap = npcRoutes.reduce( ( finalMap: Record<string, number>, item: any ) : Record<string, number> => {

            finalMap[ item.npcReferenceId ] = item.name

            return finalMap
        }, {} ) as Record<string, string>

        return spawnContents.map( ( item: any ) => {
            let parameters = JSON.parse( item.parametersJson )
            let npcId = npcReferenceMap[ item.id ]

            if ( !npcId ) {
                throw new Error( `Unable to find npc by reference id=${item.id}` )
            }

            let aiParameters = adjustAiParameters( getAiParameters( parameters.aiParameters ), npcRoutesMap, npcId, item.id )


            return [
                    npcId,
                    item.makerName,
                    TransformerOperations.getJson( getPosition( parameters.pos ) ),
                    parseInt( parameters.total, 10 ),
                    getDuration( parameters.respawn ),
                    getDuration( parameters.respawnRand ),
                    parameters.ai ?? null,
                    TransformerOperations.getJson( aiParameters ),
                    parameters.isChasePc ? parseInt( parameters.isChasePc, 10 ) : null,
                    parameters.dbname ?? null,
                    TransformerOperations.getJson( getMinionParameters( parameters.privates, npcReferenceMap ) )
            ]
        } )
    }

    getLastProcessedOperation(): string {
        return this.operationName
    }
}

export const SpawnNpcExTransformer = new SpawnNpcEx()