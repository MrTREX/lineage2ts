import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import { TransformerOperations } from '../helpers/TransformerOperations'
import _ from 'lodash'

const files = {
    'enchant_item_hpbonus': 'data/stats/enchantHPBonus.xml'
}

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true
}

class Transformer extends BaseXmlTransformer {

    getQuery( tableName: string ): string {
        return `INSERT into ${ tableName } (grade, values_json) values (?,?)`
    }

    transformData( parsedData: any ): Array<Array<any>> {
        let itemsToInsert: Array<Array<any>> = []

        _.each( _.get( parsedData, 'list.enchantHP' ), ( item: any ) => {
            let { grade } = item

            let values = _.castArray( item.bonus )
            itemsToInsert.push( [
                grade,
                TransformerOperations.getJson( values )
            ] )
        } )

        return itemsToInsert
    }

    getName(): string {
        return 'Enchant Item HP Bonuses'
    }
}

export const EnchantItemHPBonusTransformer = new Transformer( files, parserOptions )