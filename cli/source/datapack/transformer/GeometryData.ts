import { TransformerProcess } from '../helpers/TransformerProcess'
import { IDatapackEngine } from '../engine/IDatapackEngine'
import { TransformerOperations } from '../helpers/TransformerOperations'
import { IGeometryGenerator } from '../geometry/Interfaces'
import { DropCircle } from '../geometry/DropCircle'
import { Square } from '../geometry/Square'

const columns = [
    'id',
    'type',
    'size',
    'data',
    'parameters_json',
]

const query = TransformerOperations.createQuery( 'geometry', columns )

const pointGeomentries: Array<IGeometryGenerator> = [
    // Normal mobs
    new DropCircle( 'mob_small', 20, 80, 5 ),
    new DropCircle( 'mob_medium', 30, 90, 5 ),
    new DropCircle( 'mob_large', 40, 100, 5 ),

    // Boss/Raidboss npcs
    new DropCircle( 'raidboss_tiny', 20, 100, 5, 8 ),
    new DropCircle( 'raidboss_small', 30, 120, 5, 8 ),
    new DropCircle( 'raidboss_medium', 40, 140, 5, 8 ),
    new DropCircle( 'raidboss_large', 50, 160, 5, 8 ),
    new DropCircle( 'raidboss_xlarge', 60, 180, 5, 8 ),

    // Grand Raidboss npcs (very large collision radius)
    new DropCircle( 'grandboss_small', 50, 200, 10, 15 ),
    new DropCircle( 'grandboss_medium', 60, 300, 10, 15 ),
    new DropCircle( 'grandboss_large', 70, 400, 10, 15 ),

    // player
    new DropCircle( 'playerDrop', 15, 65, 10 ),

    // spawn points for npc spawns
    new DropCircle( 'npcSpawnSmall', 30, 70, 20, 40 ),
    new DropCircle( 'npcSpawnMedium', 30, 180, 20, 60 ),
    new DropCircle( 'npcSpawnLarge', 30, 270, 30, 80 ),
    new DropCircle( 'npcSpawnXLarge', 40, 320, 40, 80 ),

    // spawn points for raid bosses
    new DropCircle( 'rbSpawnSmall', 120, 180, 60, 60 ),
    new DropCircle( 'rbSpawnMedium', 150, 230, 80, 80 ),

    // movement points for npcs
    new DropCircle( 'npcMoveTiny', 10, 60, 10, 10 ),
    new DropCircle( 'npcMoveSmall', 10, 80, 10, 10 ),
    new DropCircle( 'npcMoveMedium', 10, 100, 10, 10 ),
    new DropCircle( 'npcMoveLarge', 20, 140, 20, 15 ),
    new DropCircle( 'npcMoveXLarge', 20, 180, 20, 15 ),
    new DropCircle( 'npcMoveXXLarge', 20, 200, 20, 15 ),
    new DropCircle( 'npcMoveHuge', 20, 240, 20, 20 ),
    new DropCircle( 'npcMoveXHuge', 20, 280, 20, 20 ),

    // pet manager movement points for pet
    new DropCircle( 'petMoveSmall', 40, 80, 20, 40 ),

    // teleport points
    new DropCircle( 'partyTeleport', 20, 40, 20, 20 ),
    new DropCircle( 'zonePartyTeleport', 20, 80, 20, 20 ),
    new DropCircle( 'spellTeleport', 10, 70, 10, 12 ),
    new DropCircle( 'valakasTeleport', 10, 510, 50, 50 ),

    /*
        Geometries used by admin commands
     */
    new Square( 'geodata256', 256, 256, 8, 8 ),
    new Square( 'geoGrid256', 768, 768, 256, 256 ),
    new DropCircle( 'geoVisibility', 500, 500, 0, 64 ),
]

class Geometry implements TransformerProcess {
    currentGeometry: IGeometryGenerator

    getLastProcessedOperation(): string {
        return this.currentGeometry.getName()
    }

    getName(): string {
        return 'Geometry Data'
    }

    startProcess( engine: IDatapackEngine ): Promise<void> {
        let data: Array<Array<any>> = pointGeomentries.map( ( generator: IGeometryGenerator ) => {
            this.currentGeometry = generator
            return [
                generator.getName(),
                generator.getType(),
                generator.getSize(),
                generator.generatePoints(),
                TransformerOperations.getJson( generator.getParameters() ),
            ]
        } )

        return engine.insertMany( query, data )
    }
}

export const GeometryTransformer = new Geometry()