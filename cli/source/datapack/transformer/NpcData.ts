import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import { IDatapackEngine } from '../engine/IDatapackEngine'
import { TransformerOperations } from '../helpers/TransformerOperations'
import aigle from 'aigle'
import _ from 'lodash'
import { searchFiles } from 'find-file-extension'
import pkgDir from 'pkg-dir'
import * as fs from 'fs/promises'
import parser from 'csvtojson'

const rootDirectory = pkgDir.sync( __dirname )

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true,
}

const columns = [
    'id',
    'referenceId',
    'level',
    'displayId',
    'type',
    'name',
    'usingServerSideName',
    'title',
    'usingServerSideTitle',
    'race',
    'corpsetime',
    'excrteffect',
    'snpcPropHPRate',
    'parameters_json',
    'equipment_json',
    'acquire_json',
    'stats_json',
    'status_json',
    'skilllist_json',
    'shots_json',
    'ai_json',
    'droplists_json',
    'collision_json',
]

const query = TransformerOperations.createQuery( 'npc_data', columns, true )

class Transformer extends BaseXmlTransformer {

    getQuery(): string {
        return query
    }

    async getCsvFile( fileName: string ) : Promise<Array<unknown>> {
        let fileContents = await fs.readFile( `${rootDirectory}/${fileName}` )
        return parser( { delimiter: ',' } ).fromString( fileContents.toString() )
    }

    async startProcess( engine: IDatapackEngine ): Promise<void> {

        const files = [
            searchFiles( 'xml', `${ rootDirectory }/data/stats/npcs` ),
            searchFiles( 'xml', `${ rootDirectory }/overrides/data/npcs` )
        ]

        let npcReferences = await this.getCsvFile( 'overrides/data/csv/npcReferenceIds.csv' )
        let npcReferenceMap = npcReferences.reduce( ( finalMap: Record<string, number>, item: any ) : Record<string, number> => {

            finalMap[ item.npcId ] = item.referenceId

            return finalMap
        }, {} ) as Record<number, string>

        await aigle.resolve( _.flatten( files ) ).eachSeries( async ( fileName: string ) => {
            this.lastFileProcessed = fileName
            let xmlData = await fs.readFile( fileName )
            let parsedData = await this.getParser().parse( xmlData.toString(), this.parserOptions )

            return engine.insertMany( this.getQuery(), this.getData( parsedData, npcReferenceMap ) )
        } )
    }

    transformData(): Array<Array<any>> {
        return undefined
    }

    getData( parsedData: any, npcNameReference: Record<number, string> ): Array<Array<any>> {
        let itemsToInsert: Array<Array<any>> = []

        let npcs = _.get( parsedData, 'list.npc' )

        _.each( _.castArray( npcs ), ( item: any ) => {
            let {
                id,
                level,
                displayId,
                type,
                name,
                title,
                race,
                corpseTime,
                exCrtEffect,
                sNpcPropHpRate,
                usingServerSideName,
                usingServerSideTitle,
            } = item

            if ( item.skillList ) {
                item.skillList = _.reduce( item.skillList.skill, ( properties: any, skill: any ) => {
                    properties[ skill.id ] = skill.level
                    return properties
                }, {} )
            }

            itemsToInsert.push( [
                id,
                npcNameReference[ id ],
                level,
                _.defaultTo( displayId, null ),
                type,
                name,
                _.isUndefined( usingServerSideName ) ? null : _.toString( usingServerSideName ),
                _.defaultTo( title, null ),
                _.isUndefined( usingServerSideTitle ) ? null : _.toString( usingServerSideTitle ),
                _.defaultTo( race, null ),
                _.defaultTo( corpseTime, null ),
                _.isUndefined( exCrtEffect ) ? null : _.toString( exCrtEffect ),
                _.defaultTo( sNpcPropHpRate, null ),
                TransformerOperations.getJson( item.parameters ),
                TransformerOperations.getJson( item.equipment ),
                TransformerOperations.getJson( item.acquire ),
                TransformerOperations.getJson( item.stats ),
                TransformerOperations.getJson( item.status ),
                TransformerOperations.getJson( item.skillList ),
                TransformerOperations.getJson( item.shots ),
                TransformerOperations.getJson( item.ai ),
                TransformerOperations.getJson( item.dropLists ),
                TransformerOperations.getJson( item.collision ),
            ] )
        } )

        return itemsToInsert
    }

    getName(): string {
        return 'Npc Data'
    }
}

export const NpcDataTransformer = new Transformer( null, parserOptions )