import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import { TransformerOperations } from '../helpers/TransformerOperations'
import _ from 'lodash'

const files = {
    'henna_data': 'data/stats/hennaList.xml'
}

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true
}

const columns = [
    'id',
    'name',
    'itemId',
    'level',
    'stats_json',
    'wear_json',
    'cancel_json',
    'classId_json',
]

const query = TransformerOperations.createQuery( 'henna_data', columns )

class Transformer extends BaseXmlTransformer {

    transformData( parsedData: any ): Array<Array<any>> {
        let itemsToInsert : Array<Array<any>> = []

        let hennas = _.get( parsedData, 'list.henna' )

        _.each( _.castArray( hennas ), ( item: any ) => {
            itemsToInsert.push( [
                    item.dyeId,
                    item.dyeName,
                    item.dyeItemId,
                    item.dyeLevel,
                    TransformerOperations.getJson( item.stats ),
                    TransformerOperations.getJson( item.wear ),
                    TransformerOperations.getJson( item.cancel ),
                    TransformerOperations.getJson( item.classId ),
            ] )
        } )

        return itemsToInsert
    }

    getQuery(): string {
        return query
    }

    getName(): string {
        return 'Henna Data'
    }
}

export const HennaDataTransformer = new Transformer( files, parserOptions )