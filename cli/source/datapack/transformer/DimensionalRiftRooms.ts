import { BaseCsvTransformer } from '../helpers/BaseCsvTransformer'
import _ from 'lodash'
import { TransformerOperations } from '../helpers/TransformerOperations'

const parseOptions = {
    delimiter: ',',
}

const query = TransformerOperations.createQuery( 'dimensional_rift_rooms', [
    'type',
    'roomId',
    'xMin',
    'xMax',
    'yMin',
    'yMax',
    'zMin',
    'zMax',
    'x',
    'y',
    'z',
    'hasBoss',
    'description'
] )

class Transformer extends BaseCsvTransformer {
    getQuery(): string {
        return query
    }

    transformData( parsedData: Array<unknown> ): Array<Array<any>> {
        return _.map( parsedData, ( item: any ) => {
            let { type,roomId,xMin,xMax,yMin,yMax,zMin,zMax,x,y,z,hasBoss,description } = item

            return [
                _.parseInt( type ),
                _.parseInt( roomId ),
                _.parseInt( xMin ),
                _.parseInt( xMax ),
                _.parseInt( yMin ),
                _.parseInt( yMax ),
                _.parseInt( zMin ),
                _.parseInt( zMax ),
                _.parseInt( x ),
                _.parseInt( y ),
                _.parseInt( z ),
                _.parseInt( hasBoss ),
                description
            ]
        } )
    }

    getName(): string {
        return 'Dimensional Rift Rooms'
    }
}

export const TeleportsTransformer = new Transformer( 'overrides/data/csv/dimensionalRiftRooms.csv', parseOptions )