import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import { IDatapackEngine } from '../engine/IDatapackEngine'
import { TransformerOperations } from '../helpers/TransformerOperations'
import _ from 'lodash'
import aigle from 'aigle'
import { searchFiles } from 'find-file-extension'
import pkgDir from 'pkg-dir'
import * as fs from 'fs/promises'

const rootDirectory = pkgDir.sync( __dirname )
const fileDirectory = 'data/stats/transformations'

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true
}

const columns = [
    'id',
    'type',
    'canSwim',
    'normalAttackable',
    'spawnHeight',

    'maleCommon_json',
    'maleActions',
    'maleSkills_json',
    'maleAdditionalSkills_json',
    'maleItems_json',
    'maleLevels_json',

    'femaleCommon_json',
    'femaleActions',
    'femaleSkills_json',
    'femaleAdditionalSkills_json',
    'femaleItems_json',
    'femaleLevels_json',
]

const query = `INSERT INTO transformations_data (${ columns.join( ',' ) }) values (${ _.join( _.times( columns.length, _.constant( '?' ) ), ',' ) })`


const normalizeLevels = ( item: any, prefix: string ) => {
    let path = `${ prefix }.levels.level`
    let finalPath = `${ prefix }.levels`
    if ( _.has( item, path ) ) {
        let value = _.get( item, path )
        if ( !_.isArray( value ) ) {
            _.set( item, path, [ value ] )
        }

        let outcome = _.map( _.get( item, path ), ( properties: any ) => {
            let level = properties.val
            _.unset( properties, 'val' )
            _.set( properties, 'level', level )

            return properties
        } )

        _.set( item, finalPath, outcome )
    }
}

class Transformer extends BaseXmlTransformer {

    getQuery(): string {
        return query
    }

    async startProcess( engine: IDatapackEngine ): Promise<void> {
        let transformer = this
        const files = searchFiles( 'xml', `${ rootDirectory }/${ fileDirectory }` )

        return aigle.resolve( files ).eachSeries( async ( fileName: string ) => {
            transformer.lastFileProcessed = fileName
            let xmlData = await fs.readFile( fileName )
            let parsedData = await this.getParser().parse( xmlData.toString(), transformer.parserOptions )

            return engine.insertMany( transformer.getQuery(), transformer.transformData( parsedData ) )
        } )
    }

    transformData( parsedData: any ): Array<Array<any>> {
        let itemsToInsert: Array<Array<any>> = []

        let transforms = _.get( parsedData, 'list.transform' )

        if ( !_.isArray( transforms ) ) {
            transforms = [ transforms ]
        }

        _.each( transforms, ( item: any ) => {
            let { id, type } = item

            TransformerOperations.normalizeValues( item, 'Male.skills.skill', 'Male.skills' )
            TransformerOperations.normalizeValues( item, 'Female.skills.skill', 'Female.skills' )

            normalizeLevels( item, 'Male' )
            normalizeLevels( item, 'Female' )

            TransformerOperations.normalizeValues( item, 'Male.additionalSkills.skill', 'Male.additionalSkills' )
            TransformerOperations.normalizeValues( item, 'Female.additionalSkills.skill', 'Female.additionalSkills' )


            itemsToInsert.push( [
                id,
                type,
                item[ 'can_swim' ],
                item[ 'normal_attackable' ],
                item[ 'spawn_height' ],

                TransformerOperations.getJson( _.get( item, 'Male.common' ) ),
                TransformerOperations.getJson( _.get( item, 'Male.actions' ) ),
                TransformerOperations.getJson( _.get( item, 'Male.skills' ) ),
                TransformerOperations.getJson( _.get( item, 'Male.additionalSkills' ) ),
                TransformerOperations.getJson( _.get( item, 'Male.items' ) ),
                TransformerOperations.getJson( _.get( item, 'Male.levels' ) ),

                TransformerOperations.getJson( _.get( item, 'Female.common' ) ),
                TransformerOperations.getJson( _.get( item, 'Female.actions' ) ),
                TransformerOperations.getJson( _.get( item, 'Female.skills' ) ),
                TransformerOperations.getJson( _.get( item, 'Female.additionalSkills' ) ),
                TransformerOperations.getJson( _.get( item, 'Female.items' ) ),
                TransformerOperations.getJson( _.get( item, 'Female.levels' ) ),
            ] )
        } )

        return itemsToInsert
    }

    getName(): string {
        return 'Transformations Data'
    }
}

export const TransformsDataTransformer = new Transformer( null, parserOptions )