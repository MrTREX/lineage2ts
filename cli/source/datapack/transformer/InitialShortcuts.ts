import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import _ from 'lodash'

const files = {
    'initial_shortcuts': 'data/stats/initialShortcuts.xml'
}

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true
}

class Transformer extends BaseXmlTransformer {

    getQuery( tableName: string ): string {
        return `INSERT into ${ tableName } (classId, pageId, slot, shortcutType, shortcutId, shortcutLevel) values (?,?,?,?,?,?)`
    }

    transformData( parsedData: any ): Array<Array<any>> {
        let itemsToInsert: Array<Array<any>> = []

        _.each( _.get( parsedData, 'list.shortcuts' ), ( item: any ) => {
            let classId = _.get( item, 'classId', null )
            if ( !_.isArray( item.page ) ) {
                item.page = [ item.page ]
            }

            _.each( item.page, ( currentPage: any ) => {
                let { pageId } = currentPage
                if ( !_.isArray( currentPage.slot ) ) {
                    currentPage.slot = [ currentPage.slot ]
                }

                _.each( currentPage.slot, ( slot: any ) => {
                    let { slotId, shortcutType, shortcutId, shortcutLevel } = slot

                    itemsToInsert.push( [
                        classId,
                        pageId,
                        slotId,
                        shortcutType,
                        shortcutId,
                        _.defaultTo( shortcutLevel, 0 )
                    ] )
                } )
            } )
        } )

        return itemsToInsert
    }

    getName(): string {
        return 'Initial Shortcuts'
    }
}

export const InitialShortcutsTransformer = new Transformer( files, parserOptions )