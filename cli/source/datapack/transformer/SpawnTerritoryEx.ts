import { TransformerOperations } from '../helpers/TransformerOperations'
import _ from 'lodash'
import { LinePoint, SpawnTerritoryPoints } from '../geometry/SpawnTerritoryPoints'
import { TerritoryPointsParameters } from '../geometry/Interfaces'
import { L2MapTile, L2WorldLimits } from '../enums/L2WorldLimits'
import { TransformerProcess } from '../helpers/TransformerProcess'
import { IDatapackEngine } from '../engine/IDatapackEngine'
import fs from 'fs/promises'
import parser from 'csvtojson'
import pkgDir from 'pkg-dir'

const parseOptions = {
    delimiter: ',',
}

const rootDirectory = pkgDir.sync( __dirname )

const query = TransformerOperations.createQuery( 'spawn_territory_ex', [
    'id',
    'name',
    'points_json',
    'minZ',
    'maxZ',
    'geometryPoints',
    'parameters_json',
    'regionX',
    'regionY'
], true )

type PointDefinition = [ number, number, number, number ] // x, y, minZ, maxZ
function getTerritoryPoints( value: string ): Array<PointDefinition> {
    let text = value
            .replaceAll( '{', '[' )
            .replaceAll( '}', ']' )
            .replaceAll( ';', ',' )
    let outcome = _.attempt( JSON.parse, text )

    if ( _.isError( outcome ) ) {
        throw new Error( `Unable to parse territory points from data=${ value }, using transformed value=${ text }` )
    }

    return outcome
}

interface MapTile {
    x: number
    y: number
}
function getMapTile( parameters: TerritoryPointsParameters ) : MapTile {
    let xDifference = parameters.centerX - L2WorldLimits.MinX
    let xTile = ( xDifference >> L2MapTile.SizeShift ) + L2MapTile.XMin
    let yDifference = parameters.centerY - L2WorldLimits.MinY
    let yTile = ( yDifference >> L2MapTile.SizeShift ) + L2MapTile.YMin

    return {
        x: xTile,
        y: yTile
    }
}

class SpawnTerritoryEx implements TransformerProcess {
    operationName: string

    getName(): string {
        return 'Spawn Territories Geometry'
    }

    getQuery(): string {
        return query
    }

    transformData( parsedData: Array<unknown> ): Array<Array<any>> {
        return parsedData.map( ( item: any ) => {
            let points = getTerritoryPoints( item.points )
            let minZ = Number.MIN_SAFE_INTEGER
            let maxZ = Number.MAX_SAFE_INTEGER

            let polygonPoints: Array<LinePoint> = points.map( ( point: PointDefinition ): LinePoint => {
                /*
                    Since we have a set of numbers for minimum and maximum values,
                    reverse logic is applicable where we calculate max number out of min values,
                    and min number out of max values. All due to fact that such Z values will be
                    used to determine true position via geodata layer, for which Z is needed.
                 */
                minZ = Math.max( point[ 2 ], minZ )
                maxZ = Math.min( point[ 3 ], maxZ )

                return point.slice( 0, 2 ) as LinePoint
            } )

            let geometry = new SpawnTerritoryPoints( polygonPoints )
            let parameters = geometry.getParameters()
            let mapTile = getMapTile( parameters )

            return [
                item.id,
                item.name,
                TransformerOperations.getJson( polygonPoints ),
                minZ,
                maxZ,
                geometry.generatePoints(),
                TransformerOperations.getJson( parameters ),
                mapTile.x,
                mapTile.y
            ]
        } )
    }

    getLastProcessedOperation(): string {
        return this.operationName
    }

    async startProcess( engine: IDatapackEngine ): Promise<void> {
        let exData = await this.getCsvFile( 'overrides/data/csv/spawnTerritoriesEx.csv' )
        this.operationName = 'Populating spawnTerritoriesEx data'
        await engine.insertMany( this.getQuery(), this.transformData( exData ) )

        let normalData = await this.getCsvFile( 'overrides/data/csv/spawnTerritories.csv' )
        this.operationName = 'Populating spawnTerritories data'
        await engine.insertMany( this.getQuery(), this.transformData( normalData ) )
    }

    async getCsvFile( fileName: string ): Promise<Array<unknown>> {
        let fileContents = await fs.readFile( `${ rootDirectory }/${ fileName }` )
        return parser( parseOptions ).fromString( fileContents.toString() )
    }
}

export const SpawnTerritoryExTransformer = new SpawnTerritoryEx()