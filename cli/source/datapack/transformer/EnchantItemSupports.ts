import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import _ from 'lodash'
import { TransformerOperations } from '../helpers/TransformerOperations'

const files = {
    'enchant_item_support': 'data/enchantItemData.xml'
}

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true
}

const columns = [
    'id',
    'targetGrade',
    'bonusRate',
    'maxEnchant'
]

const query = TransformerOperations.createQuery( 'enchant_item_support', columns )


class Transformer extends BaseXmlTransformer {

    getQuery(): string {
        return query
    }

    transformData( parsedData: any ): Array<Array<any>> {
        let itemsToInsert: Array<Array<any>> = []

        let objects = _.get( parsedData, 'list.support' )
        _.each( _.castArray( objects ), ( item: any ) => {
            itemsToInsert.push( [
                item.id,
                item.targetGrade,
                item.bonusRate,
                item.maxEnchant
            ] )
        } )

        return itemsToInsert
    }

    getName(): string {
        return 'Enchant Item Supporting Data'
    }
}

export const EnchantItemSupportsTransformer = new Transformer( files, parserOptions )