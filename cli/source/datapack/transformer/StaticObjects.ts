import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import _ from 'lodash'

const files = {
    'static_objects': 'data/staticObjects.xml'
}

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true
}

const columns = [
    'id',
    'name',
    'type',
    'x',
    'y',
    'z',
    'texture',
    'mapX',
    'mapY'
]

const query = `INSERT INTO static_objects (${ columns.join( ',' ) }) values (${ _.join( _.times( columns.length, _.constant( '?' ) ), ',' ) })`


class Transformer extends BaseXmlTransformer {

    getQuery(): string {
        return query
    }

    transformData( parsedData: any ): Array<Array<any>> {
        let itemsToInsert: Array<Array<any>> = []

        let objects = _.get( parsedData, 'list.object' )
        _.each( _.castArray( objects ), ( item: any ) => {
            itemsToInsert.push( [
                item.id,
                item.name,
                item.type,
                item.x,
                item.y,
                item.z,
                item.texture,
                item[ 'map_x' ],
                item[ 'map_y' ],
            ] )
        } )

        return itemsToInsert
    }

    getName(): string {
        return 'Static Objects'
    }
}

export const StaticObjectsTransformer = new Transformer( files, parserOptions )