import { BaseCsvTransformer } from '../helpers/BaseCsvTransformer'
import _ from 'lodash'
import { TransformerOperations } from '../helpers/TransformerOperations'

const parseOptions = {
    delimiter: ',',
}

const query = TransformerOperations.createQuery( 'player_access_levels', [
    'level',
    'name',
    'nameColor',
    'titleColor',
    'permissions_json',
    'commands_json'
] )

class Transformer extends BaseCsvTransformer {
    getQuery(): string {
        return query
    }

    transformData( parsedData: Array<unknown> ): Array<Array<any>> {
        return _.map( parsedData, ( item: any ) => {

            let { allowedRegex, restrictedRegex, confirmationRegex } = item

            return [
                _.parseInt( item.level ),
                item.name,
                item.nameColor,
                item.titleColor,
                TransformerOperations.getJson( item.permissions ? item.permissions.split( ';' ) : null ),
                TransformerOperations.getJson( {
                    allowedRegex,
                    restrictedRegex,
                    confirmationRegex
                } )
            ]
        } )
    }

    getName(): string {
        return 'PlayerAccessLevels'
    }
}

export const PlayerAccessLevelsTransformer = new Transformer( 'overrides/data/csv/playerAccessLevels.csv', parseOptions )