import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import { IDatapackEngine } from '../engine/IDatapackEngine'
import aigle from 'aigle'
import _ from 'lodash'
import { searchFiles } from 'find-file-extension'
import pkgDir from 'pkg-dir'
import * as fs from 'fs/promises'
import { TransformerOperations } from '../helpers/TransformerOperations'

const rootDirectory = pkgDir.sync( __dirname )
const fileDirectory = 'data/stats/skills'

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true,
}

const columns = [
    'id',
    'lastLevel',
    'name',
    'enchantGroup1',
    'enchantGroup2',
    'enchantGroup3',
    'enchantGroup4',
    'enchantGroup5',
    'enchantGroup6',
    'enchantGroup7',
    'enchantGroup8',
    'isCustomSkill',

    'table_json',
    'set_json',
    'enchantProperties_json',
    'defaults_json',
    'enchant1_json',
    'enchant2_json',
    'enchant3_json',
    'enchant4_json',
    'enchant5_json',
    'enchant6_json',
    'enchant7_json',
    'enchant8_json',
]

const query = `INSERT INTO skill_data (${ columns.join( ',' ) }) values (${ _.join( _.times( columns.length, _.constant( '?' ) ), ',' ) })`

const getEnchantLevelProperties = ( level: number ) => {
    let prefix = `enchant${ level }`
    let suffixes = [ 'cond', 'Effects', 'startEffects', 'channelingEffects', 'pveEffects', 'pvpEffects', 'endEffects', 'selfEffects' ]
    return _.map( suffixes, ( suffix ) => `${ prefix }${ suffix }` )
}

const enchantLevelProperties = _.map( _.range( 1, 9 ), getEnchantLevelProperties )
const enchantChargeProperties = _.map( _.range( 1, 9 ), ( value: number ) => `enchant${ value }` )
const multiLineParameters = new RegExp( /\r\n\t+/ )
const newLineParameters = new RegExp( /\n\t+/ )

function sanitizeTable( table: any ) : Object {
    /*
        L2J has improper extractable items xml with multi-line breaks instead of delimiter,
        hence we must apply delimiter to normalize data.
     */
    if ( table && table[ '#extractableItems' ] ) {
        table[ '#extractableItems' ] = table[ '#extractableItems' ].split( newLineParameters ).join( ';' )
    }

    return table
}

class Transformer extends BaseXmlTransformer {

    getQuery(): string {
        return query
    }

    async startProcess( engine: IDatapackEngine ): Promise<void> {
        let transformer = this
        const files = searchFiles( 'xml', `${ rootDirectory }/${ fileDirectory }` )

        return aigle.resolve( files ).eachSeries( async ( fileName: string ) => {
            transformer.lastFileProcessed = fileName
            let xmlData = await fs.readFile( fileName )
            let parsedData = await this.getParser().parse( xmlData.toString(), transformer.parserOptions )

            return engine.insertMany( transformer.getQuery(), transformer.transformData( parsedData, fileName ) )
        } )
    }

    transformData( parsedData: any, fileName: string ): Array<Array<any>> {
        let itemsToInsert: Array<Array<any>> = []
        let isCustomSkill = _.toString( _.includes( fileName, 'custom' ) )

        let skills = _.get( parsedData, 'list.skill' )

        if ( !_.isArray( skills ) ) {
            skills = [ skills ]
        }

        _.each( skills, ( item: any ) => {
            let {
                id,
                levels,
                name,
                enchantGroup1,
                enchantGroup2,
                enchantGroup3,
                enchantGroup4,
                enchantGroup5,
                enchantGroup6,
                enchantGroup7,
                enchantGroup8,
            } = item

            if ( item.table ) {
                if ( !_.isArray( item.table ) ) {
                    item.table = [ item.table ]
                }

                item.table = _.reduce( item.table, ( properties: any, parameter: any ) => {
                    if ( multiLineParameters.test( parameter[ '#text' ] ) ) {
                        properties[ parameter.name ] = parameter[ '#text' ].split( multiLineParameters )
                    } else {
                        properties[ parameter.name ] = parameter[ '#text' ]
                    }

                    return properties
                }, {} )
            }

            if ( item.set ) {
                if ( !_.isArray( item.set ) ) {
                    item.set = [ item.set ]
                }

                item.set = _.reduce( item.set, ( properties: any, parameter: any ) => {
                    properties[ parameter.name ] = parameter.val
                    return properties
                }, {} )
            }

            let defaults = _.pick( item, [ 'cond', 'effects', 'startEffects', 'channelingEffects', 'pveEffects', 'pvpEffects', 'endEffects', 'selfEffects' ] )
            let enchantProperties = _.pick( item, enchantChargeProperties )
            let enchant1Values = _.pick( item, enchantLevelProperties[ 0 ] )
            let enchant2Values = _.pick( item, enchantLevelProperties[ 1 ] )
            let enchant3Values = _.pick( item, enchantLevelProperties[ 2 ] )
            let enchant4Values = _.pick( item, enchantLevelProperties[ 3 ] )
            let enchant5Values = _.pick( item, enchantLevelProperties[ 4 ] )
            let enchant6Values = _.pick( item, enchantLevelProperties[ 5 ] )
            let enchant7Values = _.pick( item, enchantLevelProperties[ 6 ] )
            let enchant8Values = _.pick( item, enchantLevelProperties[ 7 ] )

            enchantProperties = _.reduce( enchantProperties, ( properties: any, value: any, key: any ) => {
                if ( !_.isArray( value ) ) {
                    value = [ value ]
                }
                properties[ key ] = _.reduce( value, ( valueProperties: any, currentParameter: any ) => {
                    valueProperties[ currentParameter.name ] = currentParameter.val
                    return valueProperties
                }, {} )
                return properties
            }, {} )

            itemsToInsert.push( [
                id,
                levels,
                name,
                _.defaultTo( enchantGroup1, null ),
                _.defaultTo( enchantGroup2, null ),
                _.defaultTo( enchantGroup3, null ),
                _.defaultTo( enchantGroup4, null ),
                _.defaultTo( enchantGroup5, null ),
                _.defaultTo( enchantGroup6, null ),
                _.defaultTo( enchantGroup7, null ),
                _.defaultTo( enchantGroup8, null ),
                isCustomSkill,
                TransformerOperations.getJson( sanitizeTable( item.table ) ),
                TransformerOperations.getJson( item.set ),
                TransformerOperations.getJson( enchantProperties ),
                TransformerOperations.getJson( defaults ),
                TransformerOperations.getJson( enchant1Values ),
                TransformerOperations.getJson( enchant2Values ),
                TransformerOperations.getJson( enchant3Values ),
                TransformerOperations.getJson( enchant4Values ),
                TransformerOperations.getJson( enchant5Values ),
                TransformerOperations.getJson( enchant6Values ),
                TransformerOperations.getJson( enchant7Values ),
                TransformerOperations.getJson( enchant8Values ),
            ] )
        } )

        return itemsToInsert
    }

    getName(): string {
        return 'Skill Data'
    }
}

export const SkillDataTransformer = new Transformer( null, parserOptions )
