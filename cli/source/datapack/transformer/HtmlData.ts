import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import { IDatapackEngine } from '../engine/IDatapackEngine'
import { searchFiles } from 'find-file-extension'
import pkgDir from 'pkg-dir'
import * as cheerio from 'cheerio'
import aigle from 'aigle'
import * as fs from 'fs/promises'
import _ from 'lodash'
import { TransformerOperations } from '../helpers/TransformerOperations'

const rootDirectory = pkgDir.sync( __dirname )

const columns = [
    'name',
    'contents',
    'actions_json'
]

export const HtmlTransformerQuery = TransformerOperations.createQuery( 'html_cache', columns, true )

const bypassRegex = new RegExp( /(\s)*bypass(\s-h)*\s/ )
const newlinesRegex = new RegExp( /(\r\n)+|\r+|\n+|\t+/g )
const npcIdRegex = new RegExp( /npc_%objectId%_/g )
const htmlCommentsRegex = new RegExp( /<!--[\s\S]*?-->/g )
const whiteSpaceRegex = new RegExp( /\s+/g )
const whiteSpaceBetweenTagsRegex = new RegExp( />\s+</g )

function addAction( page: any, possibleActions: any, element: any ) : boolean {
    let action : string = page( element ).attr( 'action' )
    if ( !action ) {
        return false
    }

    if ( action.includes( '%' ) ) {
        return true
    }

    let startString : string = action.substring( 0, action.indexOf( ' ' ) ).toLowerCase()

    switch ( startString ) {
        case 'bypass':
            if ( action.startsWith( 'bypass -h' ) ) {
                startString = 'bypass -h'
            }

            break

        case 'link':
            action = action.replace( startString, '' ).trim()

            break
    }


    let variableStart = action.indexOf( '$' )
    if ( variableStart !== -1 ) {
        action = action.substring( 0, variableStart - 1 )
    }

    if ( !possibleActions[ startString ] ) {
        possibleActions[ startString ] = []
    }

    possibleActions[ startString ].push( action.replace( bypassRegex, '' ) )

    return false
}

export function cleanHtmlFileName( fileName: string ) : string {
    return fileName
        .replace( rootDirectory, '' )
        .replace( /\\/g, '/' )
        .replace( '/', '' )
}

function createHtmlActions( html: string ) : string {
    let page = cheerio.load( html )
    let possibleActions : { [ bypassId : string ] : Array<string> } = {}
    let addElementAction : Function = addAction.bind( null, page, possibleActions )

    let shouldExit : boolean = _.some( [ page( 'a' ), page( 'button' ) ], ( elements: Array<any> ) : boolean => {
        return _.some( elements, addElementAction )
    } )

    if ( shouldExit ) {
        return null
    }

    return JSON.stringify( possibleActions )
}

function cleanHtmlContents( data: Buffer ) : string {
    return data.toString().replace( htmlCommentsRegex, '' )
        .replace( npcIdRegex, '' )
        .replace( newlinesRegex, '' )
        .replace( whiteSpaceRegex, ' ' )
        .replace( whiteSpaceBetweenTagsRegex, '><' )
}

export async function createHtmlColumnsData( fileName: string ) : Promise<Array<string>> {
    let htmlContents = await fs.readFile( fileName )
    let html : string = cleanHtmlContents( htmlContents )
    return [
        cleanHtmlFileName( fileName ),
        html,
        createHtmlActions( html )
    ]
}

class Transformer extends BaseXmlTransformer {

    getQuery(): string {
        return HtmlTransformerQuery
    }

    async startProcess( engine: IDatapackEngine ): Promise<void> {
        let transformer = this
        const files = [
            searchFiles( 'html', `${ rootDirectory }/data` ),
            searchFiles( 'htm', `${ rootDirectory }/data` ),
            searchFiles( 'html', `${ rootDirectory }/overrides` ),
            searchFiles( 'htm', `${ rootDirectory }/overrides` ),
        ]

        let items : Array<Array<string>> = await aigle.resolve( _.flatten( files ) ).mapLimit( 200, createHtmlColumnsData )
        return engine.insertMany( transformer.getQuery(), items )
    }

    transformData(): Array<Array<any>> {
        // NOT USED
        return null
    }

    getName(): string {
        return 'Html Data (be patient, takes time)'
    }
}

export const HtmlDataTransformer = new Transformer( null, null )