import { BaseCsvTransformer } from '../helpers/BaseCsvTransformer'
import { TransformerOperations } from '../helpers/TransformerOperations'
import { IDatapackEngine } from '../engine/IDatapackEngine'
import _ from 'lodash'

const fields : Array<string> = [
    'id',
    'level',
    'itemId',
    'isCommon',
    'successRate',
    'inputs_json',
    'outputs_json',
    'consumeMp',
    'npc_json'
]

const query = TransformerOperations.createQuery( 'recipes', fields )

interface RecipeInput {
    id: number
    amount: number
}

interface RecipeOutput extends RecipeInput {
    chance: number
}

class Transformer extends BaseCsvTransformer {

    itemNameMap: Record<string, string>

    getQuery(): string {
        return query
    }

    getName(): string {
        return 'Recipes'
    }

    async startProcess( engine: IDatapackEngine ): Promise<void> {
        let recipes = await this.getCsvFile( this.fileName )
        let itemNames = await this.getCsvFile( 'overrides/data/csv/items/items.csv' )

        this.itemNameMap = itemNames.reduce( ( finalMap: Record<string, string>, item: any ) : Record<string, string> => {

            finalMap[ item.referenceId ] = item.itemId

            return finalMap
        }, {} ) as Record<string, string>

        return engine.insertMany( this.getQuery(), this.transformData( recipes ) )
    }

    transformData( parsedData: Array<unknown> ): Array<Array<any>> {
        return parsedData.map( ( item : any ) => {
            return [
                TransformerOperations.parseInt( item.id ),
                TransformerOperations.parseInt( item.level ),
                TransformerOperations.parseInt( item.itemId ),
                TransformerOperations.parseInt( item.common ),
                TransformerOperations.parseChance( item.success ),
                TransformerOperations.getJson( _.compact( this.createInputs( item.inputs ) ) ),
                TransformerOperations.getJson( _.compact( this.createOutputs( item.outputs ) ) ),
                TransformerOperations.parseInt( item.consumeMp ),
                item.npcFee ? TransformerOperations.getJson( _.compact( this.createInputs( item.npcFee ) ) ) : null
            ]
        } )
    }

    createInputs( inputs: string ) : Array<RecipeInput> {
        return inputs.split( '|' ).map( ( value: string ) : RecipeInput => {
            let [ name, rawAmount ] = value.split( ';' )

            if ( rawAmount === '0' ) {
                return null
            }

            if ( !name || !rawAmount ) {
                throw new Error( `Missing parameters in recipe inputs for value=${value}, processing line=${inputs}` )
            }

            let id = parseInt( this.itemNameMap[ name ], 10 )
            let amount = TransformerOperations.parseInt( rawAmount )

            if ( !id || !amount ) {
                throw new Error( `Unable to reference parameters in recipe inputs for value=${value}, processing line=${inputs}` )
            }

            return {
                id,
                amount
            }
        } )
    }

    createOutputs( outputs: string ) : Array<RecipeOutput> {
        return outputs.split( '|' ).map( ( value: string ) : RecipeOutput => {
            let [ name, rawAmount, rawChance ] = value.split( ';' )

            if ( rawAmount === '0' ) {
                return null
            }

            if ( !name || !rawAmount ) {
                throw new Error( `Missing parameters in recipe outputs for value=${value}, processing line=${outputs}` )
            }

            let id = parseInt( this.itemNameMap[ name ], 10 )
            let amount = TransformerOperations.parseInt( rawAmount )
            let chance = rawChance ? TransformerOperations.parseChance( rawChance ) : 1

            if ( !id || !amount || !chance ) {
                throw new Error( `Unable to reference parameters in recipe outputs for value=${value}, processing line=${outputs}` )
            }

            return {
                id,
                amount,
                chance
            }
        } )
    }
}

export const RecipesTransformer = new Transformer( 'overrides/data/csv/recipes.csv' )