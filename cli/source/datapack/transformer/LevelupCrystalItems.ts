import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import _ from 'lodash'

const files = {
    'levelup_crystal_items': 'data/levelUpCrystalData.xml'
}

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true
}

class Transformer extends BaseXmlTransformer {

    getQuery(): string {
        return 'INSERT OR REPLACE into levelup_crystal_items (itemId, level, nextCrystalItemId) values (?, ?, ?)'
    }

    transformData( parsedData: any ): Array<Array<any>> {
        let items = _.get( parsedData, 'list.crystal.item' )

        return _.map( _.castArray( items ), ( item: any ) => {
            return [
                item.itemId,
                item.level,
                item.leveledItemId
            ]
        } )
    }

    getName(): string {
        return 'Levelup Crystal Items'
    }
}

export const LevelupCrystalItemsTransformer = new Transformer( files, parserOptions )