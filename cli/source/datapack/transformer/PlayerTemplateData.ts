import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import { IDatapackEngine } from '../engine/IDatapackEngine'
import { TransformerOperations } from '../helpers/TransformerOperations'
import aigle from 'aigle'
import _ from 'lodash'
import { searchFiles } from 'find-file-extension'
import pkgDir from 'pkg-dir'
import * as fs from 'fs/promises'
import path from 'path'

const rootDirectory = pkgDir.sync( __dirname )
const fileDirectory = 'data/stats/chars/baseStats'

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true
}

const columns = [
    'classId',
    'name',
    'int',
    'str',
    'con',
    'men',
    'dex',
    'wit',
    'powerAttack',
    'critRate',
    'attackType',
    'attackSpeed',
    'powerDefenceChest',
    'powerDefenceLegs',
    'powerDefenceHead',
    'powerDefenceFeet',
    'powerDefenceGloves',
    'powerDefenceUnderwear',
    'powerDefenceCloak',
    'magicAttack',
    'magicDefenceRightEar',
    'magicDefenceLeftEar',
    'magicDefenceRightFinger',
    'magicDefenceLeftFinger',
    'magicDefenceNeck',
    'canPenetrate',
    'attackRange',
    'damageRangeVertical',
    'damageRangeHorizontal',
    'damageRangeDistance',
    'damageRangeWidth',
    'randomDamage',
    'moveSpeedWalk',
    'moveSpeedRun',
    'moveSpeedSlowswim',
    'moveSpeedFastswim',
    'breath',
    'safeFall',
    'collisionMaleRadius',
    'collisionMaleHeight',
    'collisionFemaleRadius',
    'collisionFemaleHeight',
    'levelData_json',
]

const query = `INSERT INTO player_templates (${ columns.join( ',' ) }) values (${ _.join( _.times( columns.length, _.constant( '?' ) ), ',' ) })`

class Transformer extends BaseXmlTransformer {

    getQuery(): string {
        return query
    }

    async startProcess( engine: IDatapackEngine ): Promise<void> {
        let transformer = this
        const files = searchFiles( 'xml', `${ rootDirectory }/${ fileDirectory }` )

        return aigle.resolve( files ).eachSeries( async ( fileName: string ) => {
            transformer.lastFileProcessed = fileName
            let xmlData = await fs.readFile( fileName )
            let parsedData = await this.getParser().parse( xmlData.toString(), transformer.parserOptions )

            return engine.insertMany( transformer.getQuery(), transformer.transformData( parsedData, fileName ) )
        } )
    }

    transformData( parsedData: any, fileName: string ): Array<Array<any>> {
        let itemsToInsert: Array<Array<any>> = []
        let name = _.replace( path.basename( fileName ), '.xml', '' )

        let template = _.get( parsedData, 'list' )
        _.each( _.castArray( template ), ( item: any ) => {

            if ( _.has( item, 'lvlUpgainData.level' ) ) {
                if ( !_.isArray( item.lvlUpgainData.level ) ) {
                    item.lvlUpgainData.level = [ item.lvlUpgainData.level ]
                }

                item.lvlUpgainData = _.map( item.lvlUpgainData.level, ( level: any ) => {
                    let { hp, mp, cp } = level
                    return {
                        level: level.val,
                        hp,
                        mp,
                        cp,
                        hpRegeneration: level.hpRegen,
                        mpRegeneration: level.mpRegen,
                        cpRegeneration: level.cpRegen
                    }
                } )
            }

            itemsToInsert.push( [
                item.classId,
                name,
                _.get( item, 'staticData.baseINT', null ),
                _.get( item, 'staticData.baseSTR', null ),
                _.get( item, 'staticData.baseCON', null ),
                _.get( item, 'staticData.baseMEN', null ),
                _.get( item, 'staticData.baseDEX', null ),
                _.get( item, 'staticData.baseWIT', null ),
                _.get( item, 'staticData.basePAtk', null ),
                _.get( item, 'staticData.baseCritRate', null ),
                _.get( item, 'staticData.baseAtkType', null ),
                _.get( item, 'staticData.basePAtkSpd', null ),
                _.get( item, 'staticData.basePDef.chest', null ),
                _.get( item, 'staticData.basePDef.legs', null ),
                _.get( item, 'staticData.basePDef.head', null ),
                _.get( item, 'staticData.basePDef.feet', null ),
                _.get( item, 'staticData.basePDef.gloves', null ),
                _.get( item, 'staticData.basePDef.underwear', null ),
                _.get( item, 'staticData.basePDef.cloak', null ),
                _.get( item, 'staticData.baseMAtk', null ),
                _.get( item, 'staticData.baseMDef.rear', null ),
                _.get( item, 'staticData.baseMDef.lear', null ),
                _.get( item, 'staticData.baseMDef.rfinger', null ),
                _.get( item, 'staticData.baseMDef.lfinger', null ),
                _.get( item, 'staticData.baseMDef.neck', null ),
                _.get( item, 'staticData.baseCanPenetrate', null ),
                _.get( item, 'staticData.baseAtkRange', null ),
                _.get( item, 'staticData.baseDamRange.verticalDirection', null ),
                _.get( item, 'staticData.baseDamRange.horizontalDirection', null ),
                _.get( item, 'staticData.baseDamRange.distance', null ),
                _.get( item, 'staticData.baseDamRange.width', null ),
                _.get( item, 'staticData.baseRndDam', null ),
                _.get( item, 'staticData.baseMoveSpd.walk', null ),
                _.get( item, 'staticData.baseMoveSpd.run', null ),
                _.get( item, 'staticData.baseMoveSpd.slowSwim', null ),
                _.get( item, 'staticData.baseMoveSpd.fastSwim', null ),
                _.get( item, 'staticData.baseBreath', null ),
                _.get( item, 'staticData.baseSafeFall', null ),
                _.get( item, 'staticData.collisionMale.radius', null ),
                _.get( item, 'staticData.collisionMale.height', null ),
                _.get( item, 'staticData.collisionFemale.radius', null ),
                _.get( item, 'staticData.collisionFemale.height', null ),
                TransformerOperations.getJson( item.lvlUpgainData )
            ] )
        } )

        return itemsToInsert
    }

    getName(): string {
        return 'Player Templates'
    }
}

export const PlayerTemplateDataTransformer = new Transformer( null, parserOptions )
