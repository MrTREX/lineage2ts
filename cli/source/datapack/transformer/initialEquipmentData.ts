import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import _ from 'lodash'

const files = {
    'initial_equipment': 'data/stats/initialEquipment.xml',
    'initial_equipment_event': 'data/stats/initialEquipmentEvent.xml'
}

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true
}

class Transformer extends BaseXmlTransformer {

    getQuery( tableName: string ): string {
        return `INSERT OR REPLACE into ${ tableName } (classId, itemId, count, equipped) values (?,?,?,?)`
    }

    transformData( parsedData: any ): Array<Array<any>> {
        let itemsToInsert: Array<Array<any>> = []

        _.each( _.get( parsedData, 'list.equipment' ), ( item: any ) => {
            let { classId } = item

            _.each( item.item, ( itemData: any ) => {
                let { id, count, equipped } = itemData
                itemsToInsert.push( [ classId, id, count, equipped ? 'true' : 'false' ] )
            } )
        } )

        return itemsToInsert
    }

    getName(): string {
        return 'Initial Equipment'
    }
}

export const InitialEquipmentTransformer = new Transformer( files, parserOptions )