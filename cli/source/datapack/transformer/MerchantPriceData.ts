import { BaseCsvTransformer } from '../helpers/BaseCsvTransformer'
import { TransformerOperations } from '../helpers/TransformerOperations'

const parseOptions = {
    delimiter: ',',
}

const query = TransformerOperations.createQuery( 'merchant_price_data', [
    'name',
    'baseTax',
    'castleId',
    'townId',
] )


class Transformer extends BaseCsvTransformer {

    getQuery(): string {
        return query
    }

    transformData( parsedData: Array<unknown> ): Array<Array<any>> {
        return parsedData.map( ( item: any ) => {
            return [
                item.name,
                parseFloat( item.baseTax ),
                parseInt( item.castleId, 10 ),
                parseInt( item.townId )
            ]
        } )
    }

    getName(): string {
        return 'Merchant Price Data'
    }
}

export const MerchantPriceTransformer = new Transformer( 'overrides/data/csv/merchantPriceConfig.csv', parseOptions )