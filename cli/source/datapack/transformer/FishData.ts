import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import _ from 'lodash'
import { TransformerOperations } from '../helpers/TransformerOperations'

const files = {
    'fish_data': 'data/stats/fishing/fishes.xml'
}

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true
}

const columns = [
    'id',
    'itemId',
    'itemName',
    'groupId',
    'level',
    'biteRate',
    'hp',
    'maxLength',
    'lengthRate',
    'hpRegeneration',
    'startCombatTime',
    'combatDuration',
    'gutsCheckTime',
    'gutsCheckProbability',
    'cheatingProbability',
    'grade',
]

const query = TransformerOperations.createQuery( 'fish_data', columns )


class Transformer extends BaseXmlTransformer {

    getQuery(): string {
        return query
    }

    transformData( parsedData: any ): Array<Array<any>> {
        let itemsToInsert: Array<Array<any>> = []

        let transformer = this
        let fishItems = _.get( parsedData, 'list.fish' )
        _.castArray( fishItems ).forEach( ( item: any ) => {

            itemsToInsert.push( [
                item.fishId,
                item.itemId,
                item.itemName,
                transformer.getGroupId( item.fishGroup ),
                item.fishLevel,
                item.fishBiteRate,
                item.fishHp,
                item.fishMaxLength,
                item.fishLengthRate,
                item.hpRegen,
                item.startCombatTime,
                item.combatDuration,
                item.gutsCheckTime,
                item.gutsCheckProbability,
                item.cheatingProb,
                transformer.getGradeId( item.fishGrade )
            ] )
        } )

        return itemsToInsert
    }

    getGradeId( name: string ) {
        switch ( name ) {
            case 'fish_easy':
                return 0
            case 'fish_hard':
                return 2
            default:
                return 1
        }
    }

    getGroupId( name: string ) {
        switch ( name ) {
            case 'swift':
                return 1
            case 'ugly':
                return 2
            case 'fish_box':
                return 3
            case 'easy_wide':
                return 4
            case 'easy_swift':
                return 5
            case 'easy_ugly':
                return 6
            case 'hard_wide':
                return 7
            case 'hard_swift':
                return 8
            case 'hard_ugly':
                return 9
            case 'hs_fish':
                return 10
            default:
                return 0
        }
    }

    getName(): string {
        return 'Fish Data'
    }
}

export const FishDataTransformer = new Transformer( files, parserOptions )