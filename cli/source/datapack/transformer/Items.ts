import { BaseCsvTransformer } from '../helpers/BaseCsvTransformer'
import { IDatapackEngine } from '../engine/IDatapackEngine'
import fs from 'fs/promises'
import parser from 'csvtojson'
import { TransformerOperations } from '../helpers/TransformerOperations'
import pkgDir from 'pkg-dir'
import { parseInt } from 'lodash'

const rootDirectory = pkgDir.sync( __dirname )

const columns = [
    'id',
    'name',
    'referenceId',
    'type',
    'slot',
    'icon',
    'materialSortOrder',
    'weight',
    'defaultAction',
    'actionType',
    'ssCount',
    'spsCount',
    'immediateEffect',
    'exImmediateEffect',
    'defaultPrice',
    'isTradable',
    'isDroppable',
    'isDestructable',
    'forPrivateStore',
    'storageBitmask',
    'physicalDamage',
    'randomDamage',
    'weaponType',
    'criticalRate',
    'attackRange',
    'attackAngle',
    'attackSpeed',
    'magicDamage',
    'magicDefense',
    'isMagicWeapon',
    'forOlympiad',
    'forNpc',
    'accuracyModify',
    'reuseDelay',
    'mpConsume',
    'itemSkills',
    'itemSkillsEnchant4',
    'etcItemType',
    'isStackable',
    'evasionRate',
    'shieldDefense',
    'shieldDefenseRate',
    'equipConditionId',
    'armorType',
    'physicalDefense',
    'crystalType',
    'crystalCount',
    'isEnchantable',
    'defaultEnchantLevel',
    'dualDamageRatio',
    'hasElementals',
    'isMovable',
    'price',
    'useConditionId',
    'mpBonus',
    'criticalAttackSkill',
    'recipeId',
    'itemHtmlPath',
    'reducedSS',
    'reducedMpConsume',
    'magicSkill_json',
    'equipOption',
    'delayShareGroup',
    'duration',
    'manaConsumeOnSkill',
    'equipReuseDelay',
    'unEquipSkill',
    'defenseAttributes',
    'attackAttributes',
    'lifeTimeMinutes',
    'changeWeaponId'
]

const query = TransformerOperations.createQuery( 'items', columns )

const conditionFields : Array<string> = [
    'id',
    'definition_json',
    'raw'
]

const itemConditionQuery = TransformerOperations.createQuery( 'item_conditions', conditionFields )

interface ExtractableItem {
    itemId: number,
    minAmount: number,
    maxAmount: number,
    chance: number
}

interface MagicSkill {
    id: number
    level: number
    chance: number
}

type ItemConditions = Record<string,string | number | boolean>
enum MaterialSortOrder {
    none,
    steel,
    fine_steel,
    cotton,
    blood_steel,
    bronze,
    silver,
    gold,
    mithril,
    oriharukon,
    paper,
    wood,
    cloth,
    leather,
    bone,
    horn,
    damascus,
    adamantaite,
    chrysolite,
    crystal,
    liquid,
    scale_of_dragon,
    dyestuff,
    cobweb,
    seed,
    fish,
    rune_xp,
    rune_sp,
    rune_remove_penalty
}

enum DepositType {
    None = 0,
    Warehouse = 1,
    ClanWarehouse = 2,
    CastleWarehouse = 4,
    Other = 8
}

class Transformer extends BaseCsvTransformer {
    itemNameMap: Record<string, string>
    skillReferenceMap: Record<string, string>
    itemReferenceMap: Record<string, number>
    itemIconsMap: Record<string, string>
    itemActionTypeMap: Record<string, string>
    convertWeaponIdMap: Record<string, string>

    itemConditions: Map<string,number>
    itemConditionIndex: number

    extractableItems: Map<number, string>

    getName(): string {
        return 'Items'
    }

    getQuery(): string {
        return query
    }

    getExtractableQuery() : string {
        return 'UPDATE items SET extractableItems_json = ? WHERE id = ?'
    }

    getItemConditionsQuery() : string {
        return itemConditionQuery
    }

    async startProcess( engine: IDatapackEngine ): Promise<void> {
        let itemNames = await this.getCsvFile( 'overrides/data/csv/items/itemNames.csv' )
        let itemIcons = await this.getCsvFile( 'overrides/data/csv/items/itemIcons.csv' )
        let itemActionTypes = await this.getCsvFile( 'overrides/data/csv/items/itemActionType.csv' )
        let skillReferences = await this.getCsvFile( 'overrides/data/csv/skillReferenceIds.csv' )
        let convertWeaponIds = await this.getCsvFile( 'overrides/data/csv/items/convertWeaponIds.csv' )

        this.itemNameMap = itemNames.reduce( ( finalMap: Record<string, string>, item: any ) : Record<string, string> => {

            finalMap[ item.id ] = item.name

            return finalMap
        }, {} ) as Record<string, string>

        this.itemIconsMap = itemIcons.reduce( ( finalMap: Record<string, string>, item: any ) : Record<string, string> => {

            finalMap[ item.id ] = item.icon

            return finalMap
        }, {} ) as Record<string, string>

        this.itemActionTypeMap = itemActionTypes.reduce( ( finalMap: Record<string, string>, item: any ) : Record<string, string> => {

            finalMap[ item.id ] = item.actionType

            return finalMap
        }, {} ) as Record<string, string>

        this.skillReferenceMap = skillReferences.reduce( ( finalMap: Record<string, string>, item: any ): Record<string, string> => {
            finalMap[ item.name ] = `${item.skillId}-${item.skillLevel}`

            return finalMap
        }, {} ) as Record<string, string>

        this.convertWeaponIdMap = convertWeaponIds.reduce( ( finalMap: Record<string, string>, item: any ) : Record<string, string> => {

            finalMap[ item.fromId ] = item.toId

            return finalMap
        }, {} ) as Record<string, string>

        this.itemConditions = new Map<string, number>()
        this.itemConditionIndex = 1

        this.extractableItems = new Map<number, string>()
        this.itemReferenceMap = {}

        let fileContents = await fs.readFile( `${rootDirectory}/${this.fileName}` )
        let parsedData = await parser( this.parserOptions ).fromString( fileContents.toString() )

        await engine.insertMany( this.getQuery(), this.transformData( parsedData ) )
        await engine.insertMany( this.getExtractableQuery(), this.transformExtractableItems() )

        return engine.insertMany( this.getItemConditionsQuery() , this.transformItemConditions() )
    }

    transformData( parsedData: Array<unknown> ): Array<Array<any>> {
        return parsedData.map( ( item: any ) => {
            let id = parseInt( item.itemId )
            let referenceId = item.referenceId

            this.itemReferenceMap[ referenceId ] = id

            if ( item.capsuledItems ) {
                this.extractableItems.set( id, item.capsuledItems )
            }

            let magicalDefense = TransformerOperations.parseInt( item.magicalDefense )
            return [
                id,
                this.itemNameMap[ id ],
                referenceId,
                item.itemType,
                item.slotBitType ? item.slotBitType : null,
                this.itemIconsMap[ id ],
                MaterialSortOrder[ item.materialType ] ?? 0,
                TransformerOperations.parseInt( item.weight ),
                item.defaultAction ? item.defaultAction.toUpperCase() : null,
                this.getActionHandle( item ),
                TransformerOperations.parseInt( item.soulshotCount ),
                TransformerOperations.parseInt( item.spiritshotCount ),
                item.immediateEffect === '1' ? 1 : 0,
                item.exImmediateEffect === '1' ? 1 : 0,
                TransformerOperations.parseInt( item.defaultPrice ),
                item.isTrade === '1' ? 1 : 0,
                item.isDrop === '1' ? 1 : 0,
                item.isDestruct === '1' ? 1 : 0,
                item.isPrivateStore === '1' ? 1 : 0,
                TransformerOperations.parseInt( item.keepType ) as DepositType,
                TransformerOperations.parseInt( item.physicalDamage ),
                TransformerOperations.parseInt( item.randomDamage ),
                item.weaponType ? item.weaponType : null,
                TransformerOperations.parseInt( item.critical ),
                TransformerOperations.parseInt( item.attackRange ),
                this.getAttackAngle( item ),
                TransformerOperations.parseInt( item.attackSpeed ),
                TransformerOperations.parseInt( item.magicalDamage ),
                magicalDefense > 1 ? magicalDefense : null, // specific condition to filter out items (all sorts) that should not have value of 1
                item.magicWeapon === '1' ? 1 : 0,
                item.isOlympiadCanUse === '1' ? 1 : 0,
                item.forNpc === '1' ? 1 : 0,
                TransformerOperations.parseFloat( item.hitModify ),
                TransformerOperations.parseInt( item.reuseDelay ),
                TransformerOperations.parseInt( item.mpConsume ),
                this.getItemSkills( item.itemSkill, item.itemMultiSkillList ),
                this.getItemSkills( item.itemSkillEnchantedFour ),
                item.etcitemType ? item.etcitemType.toUpperCase() : null,
                ( item.consumeType === 'stackable' || item.consumeType === 'asset' ) ? 1 : 0,
                TransformerOperations.parseInt( item.avoidModify ),
                TransformerOperations.parseInt( item.shieldDefense ),
                TransformerOperations.parseInt( item.shieldDefenseRate ),
                this.getConditionId( item.equipCondition ),
                item.armorType ? item.armorType : null,
                TransformerOperations.parseInt( item.physicalDefense ),
                item.crystalType ? item.crystalType : null,
                TransformerOperations.parseInt( item.crystalCount ),
                item.enchantEnable === '1' ? 1 : 0,
                TransformerOperations.parseInt( item.enchanted ),
                TransformerOperations.parseChance( item.dualFhitRate ),
                item.elementalEnable === '1' ? 1 : 0,
                item.canMove === '1' ? 1 : 0,
                TransformerOperations.parseInt( item.price ),
                this.getConditionId( item.useCondition ),
                TransformerOperations.parseInt( item.mpBonus ),
                this.getItemSkills( item.criticalAttackSkill ),
                TransformerOperations.parseInt( item.recipeId ),
                item.html ? `${item.itemId}.htm` : null,
                this.getReducedConsumeValue( item.reducedSoulshot ),
                this.getReducedConsumeValue( item.reducedMpConsume ),
                TransformerOperations.getJson( this.getMagicSkill( item.magicSkill ) ),
                item.itemEquipOption ? item.itemEquipOption : null,
                TransformerOperations.parseInt( item.delayShareGroup ),
                TransformerOperations.parseInt( item.duration ),
                TransformerOperations.parseInt( item.useSkillDistime ),
                TransformerOperations.parseInt( item.equipReuseDelay ),
                this.getItemSkills( item.unequipSkill ),
                this.cleanAttributes( item.baseAttributeDefend ),
                this.cleanAttributes( item.baseAttributeAttack ),
                TransformerOperations.parseInt( item.period ),
                this.getChangeWeaponId( referenceId )
            ]
        } )
    }

    getItemSkills( mainSkill: string, otherSkills: string = null ) : string {
        if ( !mainSkill && !otherSkills ) {
            return null
        }

        let allSkills : Array<string> = []

        if ( mainSkill ) {
            let skill = this.skillReferenceMap[ mainSkill ]
            if ( !skill ) {
                throw new Error( `Unable to find item skill for id=${mainSkill}` )
            }

            allSkills.push( skill )
        }

        if ( otherSkills ) {
            otherSkills.split( ';' ).forEach( ( skillValue: string ) => {
                let skill = this.skillReferenceMap[ skillValue ]
                if ( !skill ) {
                    throw new Error( `Unable to find item skill for id=${skillValue}` )
                }

                allSkills.push( skill )
            } )
        }

        return allSkills.join( ';' )
    }

    getConditionId( condition: string ) : number {
        if ( !condition ) {
            return null
        }

        let id = this.itemConditions.get( condition )
        if ( id ) {
            return id
        }

        id = this.itemConditionIndex
        this.itemConditions.set( condition, id )
        this.itemConditionIndex++

        return id
    }

    getExtractableItems( items: string ) : Array<ExtractableItem> {
        return items.split( '|' ).map( ( definition : string ) : ExtractableItem => {
            let values = definition.split( ';' )

            if ( values.length !== 4 ) {
                throw new Error( `Unable to parse extractable item with value=${items}` )
            }

            let maxAmount = TransformerOperations.parseInt( values[ 2 ] )
            if ( !maxAmount ) {
                throw new Error( `Unable to parse extractable max amount with value=${items}` )
            }

            let minAmount = TransformerOperations.parseInt( values[ 1 ] )
            if ( !minAmount ) {
                throw new Error( `Unable to parse extractable min amount with value=${items}` )
            }

            return {
                chance: TransformerOperations.parseChance( values[ 3 ] ),
                maxAmount,
                minAmount,
                itemId: this.itemReferenceMap[ values[ 0 ] ]
            }
        } )
    }

    transformExtractableItems() : Array<Array<any>> {
        let outcome = []

        this.extractableItems.forEach( ( capsuleItems: string, itemId: number ) => {
            outcome.push(
                [
                    TransformerOperations.getJson( this.getExtractableItems( capsuleItems ) ),
                    itemId
                ]
            )
        } )

        return outcome
    }

    updateItemCondition( value: string, existingConditions: ItemConditions ) : void {
        const chunks = value.split( ';' )
        switch ( chunks[ 0 ] ) {
            case 'ec_race':
            case 'uc_race':
                existingConditions.playerRace = chunks.slice( 1 ).join( ',' )
                return

            case 'ec_agit':
                existingConditions.hasClanHall = true
                return

            case 'uc_category':
            case 'ec_category':
                existingConditions.categoryType = chunks[ 1 ].replace( '@', '' ).toUpperCase()
                return

            case 'ec_hero':
                existingConditions.isHero = chunks[ 1 ] === '1'
                return

            case 'ec_castle':
                existingConditions.hasCastle = chunks[ 1 ] === '1'
                return

            case 'ec_castle_num':
                existingConditions.castle = parseInt( chunks[ 1 ] ) ?? -1
                return

            case 'ec_clan_leader':
                existingConditions.playerClanLeader = chunks[ 1 ] === '1'
                return

            case 'ec_social_class':
                existingConditions.pledgeClass = parseInt( chunks[ 1 ] ) ?? -1
                return

            case 'ec_sex':
                existingConditions.sex = parseInt( chunks[ 1 ] ) ?? -1
                return

            case 'ec_nobless':
                existingConditions.isNoblesse = chunks[ 1 ] === '1'
                return

            case 'ec_academy':
                existingConditions.inClanAcademy = chunks[ 1 ] === '1'
                return

            case 'ec_subjob':
                existingConditions.hasSubclass = chunks[ 1 ] === '1'
                return

            case 'uc_requiredlevel':
            case 'ec_requiredlevel':
                existingConditions.requireLevel = parseInt( chunks[ 1 ] ) ?? -1
                return

            case 'ec_agit_num':
                existingConditions.clanHall = parseInt( chunks[ 1 ] ) ?? -1
                return

            case 'ec_fortress':
                existingConditions.hasFortress = chunks[ 1 ] === '1'
                return

            case 'ec_chao':
                existingConditions.chaotic = chunks[ 1 ] === '1'
                return

            case 'uc_inzone_num':
            case 'ec_inzone_num':
                existingConditions.instanceIds = chunks.slice( 1 ).join( ',' )
                return

            case 'uc_transmode_exclude':
                existingConditions.excludedStates = this.createPlayerStates( chunks.slice( 1 ) )
                return

            case 'uc_transmode_include':
                existingConditions.includedStates = this.createPlayerStates( chunks.slice( 1 ) )
                return

            case 'uc_level':
                existingConditions.requireLevelRange = chunks.slice( 1 ).join( '-' )
                return

            case 'uc_in_residence_siege_field':
                existingConditions.inSiegeArea = true
                return

            default:
                existingConditions[ chunks[ 0 ] ] = chunks.length > 1 ? chunks.slice( 1 ).join( ',' ) : true
                break
        }
    }

    /*
        Further work is needed to compress conditions:
        - ec_ vs uc_ (equip vs use conditions) are identical in terms of checks
        - multiple existing conditions should reference existing pre-made conditions to avoid re-creating duplicates
     */
    transformItemConditions() : Array<Array<any>> {
        let outcome = []

        this.itemConditions.forEach( ( id: number, value: string ) => {
            let conditions = value.split( '|' ).reduce( ( allConditions: ItemConditions, condition: string ) : ItemConditions => {
                this.updateItemCondition( condition, allConditions )

                return allConditions
            }, {} )

            if ( conditions.hasCastle && conditions.castle ) {
                delete conditions.hasCastle
            }

            if ( conditions.hasClanHall && conditions.clanHall ) {
                delete conditions.hasClanHall
            }

            outcome.push(
                [
                    id,
                    TransformerOperations.getJson( conditions ),
                    value
                ]
            )
        } )

        return outcome
    }

    getMagicSkill( value: string ) : MagicSkill {
        if ( !value ) {
            return null
        }

        let chunks = value.split( ';' )
        let skill = this.skillReferenceMap[ chunks[ 0 ] ]
        if ( !skill ) {
            throw new Error( `Unable to find magic skill with referenceId=${chunks[ 0 ]}` )
        }

        let chance = TransformerOperations.parseChance( chunks[ 1 ] )
        if ( !chance ) {
            throw new Error( `Unable to parse magic skill chance using value=${chunks[ 1 ]}` )
        }

        let skillValues = skill.split( '-' ).map( skillValue => parseInt( skillValue, 10 ) )

        if ( skillValues.some( skillValue => !Number.isInteger( skillValue ) ) ) {
            throw new Error( `Unable to apply magic skill with bad values=${skill}` )
        }

        return {
            chance,
            id: skillValues[ 0 ],
            level: skillValues[ 1 ]
        }
    }

    getActionHandle( item : Record<string, string> ) : string {
        let action = this.itemActionTypeMap[ item.itemId ]
        if ( action ) {
            return action
        }

        switch ( item.defaultAction ) {
            case 'spiritshot':
                return item.referenceId.includes( 'blessed' ) ? 'blessedSpiritShot' : 'spiritShot'

            case 'summon_soulshot':
                return 'beastSoulShot'

            case 'summon_spiritshot':
                return 'beastSpiritShot'

            case 'show_html':
                return 'showHtml'

            case 'start_quest':
                return 'showHtmlBypass'

            case 'calc':
                return 'calculator'

            case 'keep_exp':
                return 'charmOfCourage'

            case 'hide_name':
                return 'disguise'

            case 'skill_reduce':
                switch ( item.etcitemType ) {
                    case 'scrl_enchant_attr':
                        return 'enchantAttribute'

                    case 'SCRL_ENCHANT_AM':
                    case 'SCRL_ENCHANT_WP':
                    case 'BLESS_SCRL_ENCHANT_WP':
                    case 'BLESS_SCRL_ENCHANT_AM':
                    case 'ANCIENT_CRYSTAL_ENCHANT_WP':
                    case 'ANCIENT_CRYSTAL_ENCHANT_AM':
                        return 'enchantScroll'

                    case 'potion':
                        if ( item.referenceId.includes( 'mana' ) ) {
                            return 'manaPotion'
                        }

                        break
                }

                return 'applySkills'

            case 'capsule':
                return 'applySkills'

            case 'peel':
                return 'extractItems'

            case 'fishingshot':
                return 'fishShot'

            case 'harvest':
                return 'harvester'

            case 'nick_color':
                return 'nicknameColor'

            case 'recipe':
                if ( item.html ) {
                    return 'showHtml'
                }

                if ( item.recipeId ) {
                    return 'recipe'
                }

                break
        }

        switch ( item.etcitemType ) {
            case 'castle_guard':
                return 'mercenary'
        }

        return null
    }

    getAttackAngle( item: Record<string, string> ) : number {
        if ( !item.damageRange ) {
            return null
        }

        /*
            Damage range value has four values, first two are unknown:
            - third is attack range
            - fourth is attack angle

            Example of values:
            - 0;0;66;120
            - 0;0;10;0
         */
        let angleValue = item.damageRange.split( ';' )[ 3 ]
        return TransformerOperations.parseInt( angleValue )
    }

    getReducedConsumeValue( value: string ) : string {
        if ( !value ) {
            return null
        }

        let values = value.split( ';' )
        if ( values.length !== 2 ) {
            throw new Error( `Unable to compute reduced consume value for ${value}` )
        }

        return `${ TransformerOperations.parseChance( values[ 0 ] ) };${ values[ 1 ] }`
    }

    getChangeWeaponId( value: string ) : number {
        let convertedReferenceId = this.convertWeaponIdMap[ value ]
        if ( !convertedReferenceId ) {
            return null
        }

        return this.itemReferenceMap[ convertedReferenceId ]
    }

    cleanAttributes( value: string ) : string {
        if ( !value ) {
            return null
        }

        return value.replace( '{','' ).replace( '}','' )
    }

    createPlayerStates( values: Array<string> ) : string {
        return values.map( value => value.replace( 'tt_', '' ) ).filter( state => state !== 'pure_stat' ).join( ',' )
    }
}

export const ItemsTransformer = new Transformer( 'overrides/data/csv/items/items.csv' )