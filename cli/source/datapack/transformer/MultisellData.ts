import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import { IDatapackEngine } from '../engine/IDatapackEngine'
import { TransformerOperations } from '../helpers/TransformerOperations'
import _ from 'lodash'
import aigle from 'aigle'
import { searchFiles } from 'find-file-extension'
import pkgDir from 'pkg-dir'
import * as fs from 'fs/promises'
import path from 'path'

const rootDirectory = pkgDir.sync( __dirname )

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true,
}

const columns = [
    'id',
    'applyTaxes',
    'maintainEnchantment',
    'npcIds_json',
    'items_json',
]

const query = `INSERT INTO multisell_data (${ columns.join( ',' ) })
               values (${ _.join( _.times( columns.length, _.constant( '?' ) ), ',' ) })`

const normalizeItems = ( items: Array<any> ): Array<any> => {
    return _.map( items, ( item: any ) => {
        return {
            ingredients: _.castArray( item.ingredient ),
            production: _.castArray( item.production ),
        }
    } )
}

class Transformer extends BaseXmlTransformer {

    getQuery(): string {
        return query
    }

    async startProcess( engine: IDatapackEngine ): Promise<void> {
        let transformer = this
        const files = [
            searchFiles( 'xml', `${ rootDirectory }/data/multisell` ),
            searchFiles( 'xml', `${ rootDirectory }/overrides/data/multisell` ),
        ]

        await aigle.resolve( _.flatten( files ) ).eachSeries( async ( fileName: string ) => {
            transformer.lastFileProcessed = fileName
            let xmlData = await fs.readFile( fileName )
            let parsedData = await this.getParser().parse( xmlData.toString(), transformer.parserOptions )

            return engine.insertMany( transformer.getQuery(), transformer.transformData( parsedData, fileName ) )
        } )
    }

    transformData( parsedData: any, fileName: string ): Array<Array<any>> {
        let itemsToInsert: Array<Array<any>> = []
        let id = _.replace( path.basename( fileName ), '.xml', '' )
        let template = _.get( parsedData, 'list' )

        let npcIds = _.get( template, 'npcs.npc' )
        let items = _.compact( _.castArray( _.get( template, 'item' ) ) )

        itemsToInsert.push( [
            id,
            _.toString( template.applyTaxes === true ),
            _.toString( template.maintainEnchantment === true ),
            TransformerOperations.getJson( npcIds ? _.castArray( npcIds ) : null ),
            TransformerOperations.getJson( normalizeItems( items ) ),
        ] )

        return itemsToInsert
    }

    getName(): string {
        return 'Multi-sell Data'
    }
}

export const MultisellDataTransformer = new Transformer( null, parserOptions )
