import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import { TransformerOperations } from '../helpers/TransformerOperations'
import _ from 'lodash'

const files = {
    'ui_data': 'data/ui/ui_en.xml'
}

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true
}

class Transformer extends BaseXmlTransformer {

    getQuery(): string {
        return 'INSERT into ui_data (categoryId, commands, keys_json) values (?,?,?)'
    }

    transformData( parsedData: any ): Array<Array<any>> {
        let itemsToInsert: Array<Array<any>> = []
        let categories = _.get( parsedData, 'list.category' )

        if ( !_.isArray( categories ) ) {
            categories = [ categories ]
        }

        _.each( categories, ( item: any ) => {
            let { id } = item

            TransformerOperations.normalizeValues( item, 'commands.cmd', 'commands' )
            TransformerOperations.normalizeValues( item, 'keys.key', 'keys' )

            itemsToInsert.push( [
                id,
                _.isEmpty( item.commands ) ? null : _.join( item.commands, ',' ),
                TransformerOperations.getJson( item.keys )
            ] )
        } )

        return itemsToInsert
    }

    getName(): string {
        return 'In-game UI Data'
    }
}

export const UIDataTransformer = new Transformer( files, parserOptions )
