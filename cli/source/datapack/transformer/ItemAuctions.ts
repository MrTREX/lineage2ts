import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import _ from 'lodash'

const files = {
    'item_auctions': 'data/ItemAuctions.xml',
}

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true,
}

const columns = [
    'npcId',
    'auctionId',
    'itemId',
    'amount',
    'initialBid',
    'durationMinutes',
]

const query = `INSERT INTO item_auctions (${ columns.join( ',' ) }) values (${ _.join( _.times( columns.length, _.constant( '?' ) ), ',' ) })`


class Transformer extends BaseXmlTransformer {
    getQuery(): string {
        return query
    }

    transformData( parsedData: any ): Array<Array<any>> {
        let itemsToInsert: Array<Array<any>> = []

        let fishItems = _.get( parsedData, 'list.instance' )
        _.each( _.castArray( fishItems ), ( item: any ) => {

            let npcId = item[ 'id' ]

            _.each( _.castArray( item.item ), ( auction: any ) => {
                let { auctionItemId, itemId, itemCount, auctionInitBid, auctionLength } = auction

                itemsToInsert.push( [
                        npcId,
                        auctionItemId,
                        itemId,
                        itemCount,
                        auctionInitBid,
                        auctionLength
                ] )
            } )
        } )

        return itemsToInsert
    }

    getName(): string {
        return 'Item Auctions'
    }
}

export const ItemAuctionsTransformer = new Transformer( files, parserOptions )
