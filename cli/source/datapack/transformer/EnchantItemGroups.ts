import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import { IDatapackEngine } from '../engine/IDatapackEngine'
import _ from 'lodash'
import aigle from 'aigle'
import pkgDir from 'pkg-dir'
import * as fs from 'fs/promises'

const rootDirectory = pkgDir.sync( __dirname )

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true
}

const files = [
    'data/enchantItemGroups.xml'
]

const scrollQuery = 'INSERT OR REPLACE INTO enchant_item_scroll_groups (rateGroup, slot, isMagicWeapon) values (?, ?, ?)'
const itemQuery = 'INSERT OR REPLACE INTO enchant_rate_groups (groupName, enchantLevel, chance) values (?, ?, ?)'

class Transformer extends BaseXmlTransformer {

    getQuery( tableName: string ): string {
        return null
    }

    async startProcess( engine: IDatapackEngine ): Promise<void> {
        let transformer = this
        await aigle.resolve( files ).eachSeries( async ( fileName: string ) => {
            let xmlData = await fs.readFile( `${rootDirectory}/${fileName}` )
            let parsedData = await this.getParser().parse( xmlData.toString(), transformer.parserOptions )

            let [ itemData, scrollData ] = transformer.transformData( parsedData )
            await engine.insertMany( itemQuery, itemData )
            return engine.insertMany( scrollQuery, scrollData )
        } )
    }

    transformData( parsedData: any ): Array<Array<any>> {
        let itemData: Array<Array<any>> = []
        let scrollData: Array<Array<any>> = []

        _.each( _.get( parsedData, 'list.enchantRateGroup' ), ( item: any ) => {
            let { name } = item
            if ( !_.isArray( item.current ) ) {
                item.current = [ item.current ]
            }

            _.each( item.current, ( properties: any ) => {
                let { enchant, chance } = properties
                itemData.push( [ name, _.toString( enchant ), chance ] )
            } )
        } )

        _.each( _.get( parsedData, 'list.enchantScrollGroup.enchantRate' ), ( item: any ) => {
            let { group } = item
            if ( !_.isArray( item.item ) ) {
                item.item = [ item.item ]
            }

            _.each( item.item, ( properties: any ) => {
                let { slot, magicWeapon } = properties
                scrollData.push( [ group, slot, _.isUndefined( magicWeapon ) ? null : _.toString( magicWeapon ) ] )
            } )
        } )

        return [ itemData, scrollData ]
    }

    getName(): string {
        return 'Enchant Item Groups'
    }
}

export const EnchantItemGroupsTransformer = new Transformer( null, parserOptions )