/**
 * Copied from https://github.com/pshihn/hachure-fill and tailored to specific implementation
 * MIT License
 * Original author: https://github.com/pshihn
 */
export type Point = [number, number]
export type Line = [Point, Point]

interface EdgeEntry {
    ymin: number
    ymax: number
    x: number
    islope: number
}

interface ActiveEdgeEntry {
    s: number
    edge: EdgeEntry
}

function rotatePoints( points: Array<Point>, center: Point, degrees: number ): void {
    if ( points && points.length ) {
        const [ cx, cy ] = center
        const angle = ( Math.PI / 180 ) * degrees
        const cos = Math.cos( angle )
        const sin = Math.sin( angle )

        for ( const p of points ) {
            p[ 0 ] = Math.floor( ( ( p[ 0 ] - cx ) * cos ) - ( ( p[ 1 ] - cy ) * sin ) + cx )
            p[ 1 ] = Math.floor( ( ( p[ 0 ] - cx ) * sin ) + ( ( p[ 1 ] - cy ) * cos ) + cy )
        }
    }
}

function rotateLines( lines: Array<Line>, center: Point, degrees: number ): void {
    const points: Array<Point> = []
    lines.forEach( ( line ) => points.push( ...line ) )
    rotatePoints( points, center, degrees )
}

function areSamePoints( p1: Point, p2: Point ): boolean {
    return p1[ 0 ] === p2[ 0 ] && p1[ 1 ] === p2[ 1 ]
}

export function hachureLines( points: Array<Point>, hachureGap: number, hachureAngle: number, hachureStepOffset = 1 ): Array<Line> {
    const angle = hachureAngle
    const gap = Math.max( hachureGap, 0.1 )

    const rotationCenter: Point = [ 0, 0 ]
    if ( angle ) {
        rotatePoints( points, rotationCenter, angle )
    }
    const lines = straightHachureLines( points, gap, hachureStepOffset )
    if ( angle ) {
        rotatePoints( points, rotationCenter, -angle )
        rotateLines( lines, rotationCenter, -angle )
    }

    return lines
}

function straightHachureLines( points: Array<Point>, gap: number, hachureStepOffset: number ): Array<Line> {
    const vertexArray: Array<Array<Point>> = []
    const vertices = structuredClone( points )
    if ( !areSamePoints( vertices[ 0 ], vertices[ vertices.length - 1 ] ) ) {
        vertices.push( [ vertices[ 0 ][ 0 ], vertices[ 0 ][ 1 ] ] )
    }
    if ( vertices.length > 2 ) {
        vertexArray.push( vertices )
    }

    const lines: Array<Line> = []
    gap = Math.max( gap, 0.1 )

    // Create sorted edges table
    const edges: Array<EdgeEntry> = []

    for ( const vertices of vertexArray ) {
        for ( let index = 0; index < vertices.length - 1; index++ ) {
            const p1 = vertices[ index ]
            const p2 = vertices[ index + 1 ]
            if ( p1[ 1 ] !== p2[ 1 ] ) {
                const ymin = Math.min( p1[ 1 ], p2[ 1 ] )
                edges.push( {
                    ymin,
                    ymax: Math.max( p1[ 1 ], p2[ 1 ] ),
                    x: ymin === p1[ 1 ] ? p1[ 0 ] : p2[ 0 ],
                    islope: ( p2[ 0 ] - p1[ 0 ] ) / ( p2[ 1 ] - p1[ 1 ] ),
                } )
            }
        }
    }


    edges.sort( ( e1, e2 ) => {
        if ( e1.ymin < e2.ymin ) {
            return -1
        }
        if ( e1.ymin > e2.ymin ) {
            return 1
        }
        if ( e1.x < e2.x ) {
            return -1
        }
        if ( e1.x > e2.x ) {
            return 1
        }
        if ( e1.ymax === e2.ymax ) {
            return 0
        }
        return ( e1.ymax - e2.ymax ) / Math.abs( ( e1.ymax - e2.ymax ) )
    } )
    if ( !edges.length ) {
        return lines
    }

    // Start scanning
    let activeEdges: Array<ActiveEdgeEntry> = []
    let y = edges[ 0 ].ymin
    let iteration = 0
    while ( activeEdges.length || edges.length ) {
        if ( edges.length ) {
            let ix = -1
            for ( let i = 0; i < edges.length; i++ ) {
                if ( edges[ i ].ymin > y ) {
                    break
                }
                ix = i
            }
            const removed = edges.splice( 0, ix + 1 )
            removed.forEach( ( edge ) => {
                activeEdges.push( { s: y, edge } )
            } )
        }
        activeEdges = activeEdges.filter( ( ae ) => {
            return ae.edge.ymax > y
        } )

        activeEdges.sort( ( ae1, ae2 ) => {
            if ( ae1.edge.x === ae2.edge.x ) {
                return 0
            }
            return ( ae1.edge.x - ae2.edge.x ) / Math.abs( ( ae1.edge.x - ae2.edge.x ) )
        } )

        // fill between the edges
        if ( ( hachureStepOffset !== 1 ) || ( iteration % gap === 0 ) ) {
            if ( activeEdges.length > 1 ) {
                for ( let i = 0; i < activeEdges.length; i = i + 2 ) {
                    const nexti = i + 1
                    if ( nexti >= activeEdges.length ) {
                        break
                    }
                    const ce = activeEdges[ i ].edge
                    const ne = activeEdges[ nexti ].edge
                    lines.push( [
                        [ Math.round( ce.x ), y ],
                        [ Math.round( ne.x ), y ],
                    ] )
                }
            }
        }
        y += hachureStepOffset
        activeEdges.forEach( ( ae ) => {
            ae.edge.x = ae.edge.x + ( hachureStepOffset * ae.edge.islope )
        } )
        iteration++
    }

    return lines
}