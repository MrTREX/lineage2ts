# Lineage II Commemorative Mark
- item id 13419, event item
- comes from giveaway box (6th Anniversary Party Gift Box, multiple years)
- part of event system, where if stored for a year such item can be exchanged for `6th Anniversary Party Earring/Belt` (ids 15428, 15429)

## Anniversary Party Manager
Wording for missing maks from npc:
`6th Anniversary Party Manager:
Oh, sorry~~ I would love nothing more than to give you this souvenir~
but, this belt is only for those who kept the 5th anniversary souvenir for an entire year! 
I'm so sorry, but you'll get your chance! We really do love giving out rewards!`