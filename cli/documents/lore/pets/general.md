# Pet interface
Pet interface window opens when you double-click your pet. There are 3 tabs: Status, Actions, Items.

In Status tab you can see your pet’s HP/MP, XP, and main characteristics.

In Actions tab there are buttons to activate actions and skills of a pet.

In Items tab you can see pet’s inventory, weapon and armor, food, and the items a pet picks up.

Pets can use Beast Soulshots and Spiritshots. Such items are used from master’s inventory.


# Controlling a pet
Stay – a pet either follows its master or, if pressed again, stops at the spot.
Attack – a pet attacks the target.
Stop – stops pet’s current action.
Pickup – pet picks up items from the ground
Return/Unsummon – when a pet is unsummoned, all items .remain in pet’s inventory (only when a pet is not taking part in a combat)
Move to target

# Pet inventory
In a pet inventory there are 12 slots for armor, weapon, food and other items. There are penalties for pets related to the weight they carry.

Weight 50% and more	The speed of HP/MP restoration is reduced
Weight 60% and more	Pet’s speed is reduced by 50%
The speed of HP/MP restoration is reduced
Weight 80% and more	Pet’s speed is reduced by 50%
HP/MP don’t restore

# Summoning a pet
You can summon only 1 pet/servitor at a time
To summon a pet, you need a special item
You cannot summon/unsummon your pet in a combat

# Feeding a pet
If you don’t feed your pet on time and its hunger level is below 10%, your pet will not be getting any XP, you won’t be able to control it, but it WILL NOT DISAPPEAR!
Pet Managers sell food for pets.
Pets consume different amounts of food depending on the state they’re in: a combat or non-combat. In a combat mode your pet’s fullness level reduces about 2 times quicker.
Pets can increase their fullness level automatically. You just need to place food to your pet’s inventory. Your pet will feed when it’s fullness level is below 55%.
Equipping a pet
Pets can use special armor, weapon and accessories. Some pets cannot use weapon and/or armor.
Players can ride on some pets.
Different types of pets use special weapon and armor for their type.
You can equip one weapon, one armor and one accessory on your pet at one time.
When you unsummon and resummon your pet, all the items will be equipped again,
To equip a pet, move all the necessary items to your pet’s inventory and double-click them. If you’ve done it right, the items will be highlighted with a coloured frame. It means your pet is using them.

# Shots for pets
A pet, just like a character, has its own inventory to store different items. However, there are certain restrictions: a pet cannot keep arrows, soulshots, spiritshots, blessed spiritshots or compressed shots.

Special Beast Soulshots, Beast Spiritshots and Blessed Spiritshots for pets can be bought from Pet Managers in any township. You need to use such items from your own inventory. To make a pet use an item, move it to pet’s inventory and double-click it.

# Pet Development
A pet consumes a part of its master’s XP.
A pet doesn’t get XP before its master has completed a quest.
If your pet’s lvl is lower than yours, it gets less XP. The more the difference – the less XP your pet gets.
If your pet’s lvl is higher than yours, it gets more XP. The more the difference – the more XP your pet gets.
If your pet’s lvl is 20 lvls higher than yours (or even higher), the pets stops obeying you.
If your mount’s level is 10+ lvls more than yours, the speed of your movement on the mount decreases.
Some pets evolve as they gain more levels. Some pets need to complete a quest to evolve, and some don’t.
# Transferring a pet
To transfer a pet, you need to transfer the item that summons it.
You need to clean up your pet’s inventory to transfer a pet, send it by mail or sell it via your private trade.
# Pet’s death
You can resurrect a pet within 24 hours from its death. A pet can be resurrected with a Scroll of Resurrection or with healers’ help.
A pet loses a certain amount of XP when it dies.
If a character kills a pet in a non-combat (unflagged) state, the character received a certain amount of karma, but PK counter doesn’t change.
# Dismissal of a pet
To dismiss a pet you need to get rid of the item summoning the pet.
When you dismiss a pet, it gets dismissed with all the items in its inventory. Be careful when you dismiss a pet.
You cannot restore it.

Source: https://l2wiki.lineage2splendor.com/Pets/