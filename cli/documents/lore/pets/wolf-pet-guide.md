# Lineage II Wolf Pet Guide

Hi, I’m the happy owner of three Fenrirs….pleased! ^o^
Today I’d like to introduce you to the world of wolves and so here we start with a non-guide on how to get, raise and evolve your personal loyal shredding-machine.

Unfortunately making them very powerful costs a lot of money and yet they can make you feel proud of them with a reasonable expense. But step by step, no hurry, as it’s a long way to reach the top.

## STEP BY STEP: LEVEL 15 IS A GOOD MOMENT TO START

As you reach level 15 with your toon, your next stop should be the Gludin Village as this is just the right moment to take a wolf with you, one of the best pets, to me.
To receive your new best friend you’ll need to fulfill a quest, a simple but really annoying one: your contact NPC for this quest is Martin, the pet manager in Gludin.
You cant miss him, he’s the Pet Manager on the other side of the square from the gatekeeper. He wont give you a wolf by just asking him, though: he wants you to demonstrate your love for animals and that you’re qualified to raise a pet.

Depending on your race and thus your hometown, he will send you to save innocent animals from being slaughtered by infesting monsters near your belonging village. Now I might be wrong, for I’ve received all of my wolves through the same toon, but I guess it’s the same type of monsters for all the races: spiders. They’re nothing big, just be prepared for a bit of boredom for you’ll have to collect 50 quest items; 20-30 minutes? No more, I think.
Should you forget what monsters you have to hunt, check your “Quest” inventory section for a scroll reporting their name.
Beware: the system wont inform you that it’s time to get back to Martin as for any other quest, so constantly check the number of the items you have in the “Quest” section of your inventory.

Got them all? Now get back to Martin, in Gludin, and ask about the quest again. Now he has a second task for you: you have to meet three members of the “Animal Lovers Association” to learn the basics about pets (just wolves, on the square).
He gives you a list of the three NPCs you have to meet and you can find it in your quest items inventory section. Unfortunately you cant avoid to go meet them, as it’s specifically required: if you don't, Martin wont let you go on with the quest.
Dont worry, they’re easy and safe to find: Bella is the gatekeeper in Gludio, cant really miss her; Metty is one of the guards at the southern gate of Dion town; Ellie works at the jewelry shop in Giran, just check the town map to find it.
It’s important that you write down all the key informations they’re going to tell you for Martin will submit you a Q&A; test: one wrong answer and you’ll have to go speak with all the NPCs again. If you’re too lazy to write down the informations yourself, I’ve written them in another thread in this same guides section: “Cheat your wolf test”.
Ten questions, ten answers, no errors admitted. Thus carefully read each question as they often contain traps like negations and answers that you can guess only through elimination.

Assuming you’ve been a good student, now you’re the lucky owner of a level 15 wolf. You can find the wolf collar needed to summon him (yes, I said HIM!!) in your inventory, just right click on it to make it work.
`IMPORTANT`: asa you get the collar you should buy some “Food for Wolves” from Martin (or any other pet manager in the other villages/towns), 200 will do; I’ll explain you why in a bit, in the next section.

## A BIT OF FLESH, A BIT OF BLOOD AND SOME GOOD BONES: RECIPE FOR A CARNIVORE
First of all let’s help your new best friend to not STARVE!!

When you summon your wolf (as for every other pet type), a new status bar appears for him. Click on the bar to select your friend and then once again to “talk to him” and open another window: this window works as your stats/inventory/commands windows all in one.

We can think of the “Pet Interface” as divided in two halves, the upper one and the lower one.

The upper one has more or less the same content of your own “Character Status” window: HP/MP/EXP/Weigth/SP. But what about the green bar?
That green bar represents his “stomach”, his hunger: as the bar goes down, he gets hungrier. Actually there’s nothing to worry about until the % goes under 55; when the bar reaches below that value, your wolf’s stats will diminish, especially his movement speed, and he’ll start to move like in slow-motion.
If you let the bar go completely down to 0, he’ll feel disregarded and will abandon you FOREVER. This is why I told you to buy the food even before summoning him for the very first time, even if at this time his food bar is full.
You can prevent the abandon by just putting the food you buy in his inventory as he’ll automatically eat it each time the bar goes under 55%.
Remember to check his food every time you summon him, because you cant unsummon a hungry pet: this means that if you’re out hunting and he’s hungry, you’ll have to get back in town at a run or you wont be able to save him from starving.

`IMPORTANT`: before the last L2 Gracia upgrade, logging off was a good trick to unsummon a hungry pet. Now no longer; if you log-off while your pet is summoned, when you log back on he’ll be automatically resummoned as you move your first step around. Feed him!!!!

## JUMP SCRAPPY, JUMP!! GOOD BOY!
As the pet interface is divided in two halves, the lower half is, in turn, divided in three tabs.

The Information tab is just a list of his stats which are very poor, at his “birth level”. No need to dwell on this one.

The Items tab (pet’s inventory) is easy to understand and yet there are many uses you can make of it to exploit the presence of a wolf at its best.
First has to be said that you can manually make your wolf eat by right or double clicking on the food you put in his inventory (you can even shortcut it), in case you want him always well fed. Obvious or not, you can also manually make him use potions by putting them in his inventory and right or double-clicking on them (healing potions, ftw!).
If you don't want him to fight, his inventory still is a great resource as you can use him as your personal mule. At level 15, his weight limit is about 54500, very useful if you want to bring some extra weapon/armor/consumables. Unfortunately, he cant carry shots nor arrows, but wont complain for some spirit/soul ore, scrolls, potions or materials.

`NOTE`: before the last L2 Gracia upgrade, when unsummoning a pet, you had the items in his inventory automatically moved to yours; no need to say that if your inventory was full, you could’ve had some unwanted drops. Now no longer. When you unsummon a pet, the stuff remains in his inventory and you’ll find it there on the next summoning. Very useful and convenient, but beware, you cant drop/trade/sell the collar until his inventory is completely empty.

Moving on to the next tab, we have the Action one. At his birth level, and so up to level 55, wolves’ possible actions are limited to six:
– Change Movement Mode: it’s a switch command that makes your pet follow you or stay put where he is.
– Attack: well, do I….ok, ok! This command makes your wolf attack your same target. You can also use this command to make your wolf follow your current target (only for non-chaotic PC/NPC, obviously).
– Stop: this command makes your wolf to stop the current action, may it be attacking, following or just moving.
– Pickup: useful when he’s closer to the drops than you and/or you’re too lazy to pick them up on your own. After the last game upgrade, this action is way faster and works better. You cant make him pick up items he normally cant carry, anyway.
– Unsummon Pet: easy to guess, this one send
s your wolf back to the dimension where he lives. Beware, though, you cant unsummon a pet while you’re fighting or being attacked and until you’ve got back to “calm”; cant save him from being killed just by unsummoning him, sorry.
– Move: this action makes your pet move toward your current target. It seems, though, that he doesnt just move to, but once reached the target, he stops and stays put as if you’ve changed the movement mode; click on the movement mode to make him follow you again.

`NOTE`: in the upper right part of the Pet Interface is a button which sais “Name”; you can use this button at any level to give your pet a personalized name. For what I know you cant change his name, once given, and that’s why named pets are cheaper than non-named ones. Giving him a name is a sign of dedication, to me, and when I see non-named Great Wolves and Fenrirs I cant stop thinking they’ve been raised only to be sold….so sad and heartless.

This is all that has to be said, about new-born wolves. Now let’s see how to manage your best friend up to level 55.

## TO WOLF OR NOT TO WOLF, THAT IS THE QUESTION….
Let’s call a spade a spade: wolves below level 55 ARE weak!
Even with top armor/weapon (ridiculous!) they still are weak; maybe you could bring out something appreciable if you also buff your wolf, but forget about making him solo!!
Buying him a weapon before level 55 is, thus, completely useless and a waste of money, to me. Buy him just an armor to help him survive some more hits before calling him a goner….if you really want.

For these and many other reasons I don't know, I’m not going to teach you how to make your wolf fight, instead I’ll try to explain you how to keep him safe and raise him up to level 55.

First of all, to raise him the fastest way, I suggest to match him with a toon of the same level (if you don't have a 70+ one). Few levels higher toon still is a good choice, but not of a lower level. This is because, up to level 55, your wolf will grow more or less at your same rate. A higher level wolf would raise slower than he could, and with a too high toon (thus against high level monsters) he’d risk to die all the time losing more experience than he gets. This is just a hint, though, so you’re free to accompany him with the toon you prefer.

`NOTE`: it seems, at least I’ve heard, that in earlier versions of L2 you had to make your wolf attack monsters, for him to get experience. Now no longer. He just has to be within a certain range from you and he’ll get experience just like any other party mate.

As said before, your wolf is really weak, so here are some generic guidelines to make him raise safely:
– avoid open spaces with more than 1 or 2 aggressive species; you’d have to watch both yours and his back all the time, for many aggressive mobs seem to prefer fresh puppy meat, to start the meal.
– if you can, always buff him with all the defensive spells you have; few more HP/PDef points can make the difference.
– if you can, make him stay put in a corner far enough from aggressive monsters, but not too far from you, so that he’ll still get experience.
– forget about bringing him out if you’re going to hunt monsters with area-of-effect skills; it’d be a continuos resuscitating him.

`HINT`: narrow dungeons like catacombs’ rooms and kamaloka instances are the best places where to bring your puppy. In catacombs you can “park” your pet in a corridor “one column” far from the room entrance so that he’ll not get attacked by aggro mobs and will still be near to you enough to get experience. In kamalokas just make him stay put close to the wall and he wont run any risk.
Another place even better to raise pets is Silent Valley (level 71+, from Aden): in this area mobs NEVER attack pets/servitors, not even if they hit first; this is because these monsters specifically search for the pet/servitor’s master.

Nothing more to say, just follow my hints, be patient and you’ll see this thing will work on its own.

## HOW TO EMBALM WOLVES: THE INEVITABLE DEPARTURE(S)

Maybe “weak” is not the right term….SQUISHY!! That’s it!
If you follow my hints and you’re really lucky, you’ll never have to see your beloved furry friend dying. Otherwise, be prepared, because wolves up to level 54 can be killed with one to five hits by any monster of his same level (not to mention PKs).

To be prepared, what you need is a resurrection skill or some “Scrolls of Resurrection” (purchasable in any Grocery Store). You could also use the “Blessed Scrolls of Resurrection for Pets” that you can buy from Dawn and Dusk priests (100% exp regained), but I don't think you’ll want to waste money on them, not before level 50, at least. The wolf gains experience at your same rate, assuming he is of your same level, thus will regain what he loses in no time; from level 50 to 55, instead, seems like you’re going to never get to the goal, so I wont blame you if you decide for a blessed scroll for pets.

If you have the pet on you, and you’re going to resuscitate him on your own, it’s not difficult to select him, as you just have to click on his status bar. If you’re going to use a boxed toon that doesnt own the pet, instead, hold down “Shift” and the game will let you select his corpse by clicking on it (valid for any corpse, monsters included).

`ATTENTION`: This “be prepared” thing is not meant to bore you with useless information!! If you don't resuscitate your pet within 24 hours, he will disappear FOREVER!!! You’re warned.

## LEVEL 55: AND ONE DAY HE JUST BLOSSOMED….

Now, now….you took him to level 55, didn't you? It’s been a long way, huh?

It’s about time to get your reward!!

Go to the pet manager of the nearest town and talk to him about “Evolve pet”, just remember to summon your wolf, before asking for evolution. He’ll then willingly transform your little puppy in a big scary Great Wolf. If you check your inventory, you’ll also notice his summoning item has changed, becoming a “Great Wolf Necklace”.

If you open his Pet Interface, you’ll notice his stats had a nice jump ahead; what really interest you now are HP, P.Def and P.Atk. These last two are not yet something big, to be honest, but what makes the difference is that now you can make him equip Great Wolf/Fenrir stuff.
Unfortunately they’re quite expensive: willing to equip him completely, even buying the cheapest armor/weapon/jewel, you’d spend just a bit less than eight millions. Full top quality equipment costs around one!hundred!sixty! millions!!

More than the equipment you should above all care about proper food and, if you’re planning to make him fight, some soulshots (weapon required).
I said proper food because a Great Wolf eats a specific food: Great Wolf Food. You can just toss, thrash or resell the old food, now.
About the food thing I have something to say: you can actually buy another type of food for your Great Wolf, the “Enriched Pet Food for Wolves” (red sack icon); this food too is meant for Great Wolves and Fenrirs, but it’s different from the normal food as it fills up the whole food bar with just one unit. I suggest you not to buy the enriched type yet, for, making accounts, you’ll easily guess that one enriched unit costs as much as nine normal units. I suggest you to try feeding your Great Wolf from about 55% up to 100% and see how many units are needed; if they’re more than nine, then it’s time to buy the enriched type, otherwise keep using normal one.


`NOTE`: Fighting makes your wolf consume food at a way higher rate than just by standing still; you’ll better notice it when getting closer to level 70, believe me. Moral of the story: carefully chose when it’s worth to involve your wolf in a hunt.

About soulshots: you cant put them in the wolf’s inventory, instead you have to ke
ep them in your inventory and shortcut/right-click them just as for your own shots. This means less room for your own shots and more weight to carry, but keep this in mind: his shots will ALWAYS cost no more than 27 adena each (depending on taxes) and he uses two for each hit. Plus their weight is way lower than normal soulshots. I said this to make you understand that proportions are different, comparing a normal hunt trip’s costs with a wolf aided one’s. A wolf will never ask you to equally share money nor to keep a drop for him; all he needs are supplies and he’ll be your loyal furry DD.

`NOTE`: The furry DD wont get buffed by NPC buffers as he’s not recognized as a servitor; to make him fight, a Shillien Elder in party is the least (Vampire Rage buff), along with some Greater Swift Attack Potions (or Potions of Alacrity, if you cant afford).
It’s worth to mention that with the last Gracia upgrade your wolf wont lose his buffs any longer, when he gets unsummoned; useful if you get disconnected while hunting, but a real pain if you need to “delete” buffs as he cant be unbuffed by holding Alt key and clicking on icons.

60, 65, 70….BINGO!!: STAGES OF A SUFFERED GROWTH

You’ll soon notice on your own, but I tell you because I like to “spoil”, that your newly evolved level 55 Great Wolf doesnt grow at your same rate any longer. Instead he’ll keep staying about three levels below you, from now on; this means that the only way to make him grow higher than you is to accompany him also with the other toons of your “stable” (if you have any….other toon!).

From now on, your next aim is to reach level 70, when your wolf will be able to evolve into a Fenrir. This last evolution should give your pet a further small stats jump, but honestly I’m not sure about this as I forgot to check, when the time came for me (three times!! >.<).

Leveling a wolf after level 55 can be a real boredom, if you’re not yet going to make him fight, but he still has some surprises in store for you: each five levels after level 55 and up to level 70 he’ll change his appearance showing armor parts (equipping or not any armor!); so you’ll see him “naked” at level 55, with a mild harness at level 60, with head/neck armor at level 65 and fully armored/saddled at level 70 (this one only after Fenrir evolution).

But it’s not only a matter of look. Your wolf will also get real skills starting from level 55 up to level 70, one each five levels:
– level 55: Bite Attack -> he will hit the enemy with a powerful bite.
– level 60: Cry of the Wolf -> it seems this skill has a debuffing effect on nearby enemies decreasing their PAtk by 23% (almost never used).
– level 65: Mawl -> this is another attacking skill, a powerful claw attack that can cause bleeding (definitely my favourite!).
– level 70: Awakening -> this is a sort of selfbuff, very similar to the Berserker Spirit given by Prophets; it lasts for 90 seconds and reuse time is 15 minutes. (You get this one after evolving him in Fenrir).

At level 70, finally, you’ll be able to evolve your by-now-very-useful furry friend into a Fenrir, last and top form for a wolf. As for level 55, you just have to summon him, go speak to a Pet Manager and ask for evolving a pet. The summoning item will change name again, now called Fenrir Necklace.
The coolest thing is that now you can ride your wolf, and even the most wretched dwarf will look like the lord of the world, once on a Fenrir!!
To ride your Fenrir just get close to him and type the command /mount; to get off him just type /dismount and he’ll disappear like he’s been unsummoned. You can also use the button in the Action window, if you find it easier.

`NOTE`: When you /mount your Fenrir, his weapon/armor/jewel are “disabled” (low PAtk/PDef/MDef again) and he wont autofeed any longer; further the Pet Interface disappears and a thin green bar reporting his “hunger status” will be shown over your head. If he gets too hungry (below 55%) while you’re riding him, he will start to move very slow and you wont be able to /dismount because that would be like an unsummoning; I thus advise against riding your Fenrir for long travels when his food bar is not at least at 3/4.
If you forget to check and end up riding a hungry Fenrir, the only way to dismount is by logging off; once you come back in-game, you’ll find the wolf unsummoned and he wont be automatically resummoned. Now manually summon him, feed and you’re free to ride him again.
`NEW INFO`: If you don't want your wolf to get hungry while you’re riding him, just keep some food units in your own inventory; once you mount him, your Fenrir stops eating his own food, but it seems he’ll start feeding from your “pockets”. (!!Credited to Coth!!)

Last word: as said before, when dismounting, your Fenrir gets unsummoned; resummoning him, his weapon/armor/jewel will be re-enabled and he’ll start to autofeed again. Please also note that when you mount a Fenrir, your weapon too gets unequipped and moved to your inventory; unlike your wolf, though, your weapon wont be automatically re-equipped, so remember to do that manually.


## FENRIR ADDICTION: COLLATERAL EFFECTS

I hope you didn't think that having a Fenrir on your side would’ve been all positive, because there are some costs (not all to be paid with money) and limits, when you try to take advantage of such a DD:
1. Great Wolves and Fenrirs leech about 30~35% of your experience points (don't affect party mates’ experience), which means you’ll always get 1/3 less exp, when your evolved pet is summoned.
2. If you ride your Fenrir with a mystic class, you’ll notice your MAtk flies very high!! But it’s fake, I’m sorry; my 77 Shillien Saint gets over 3000 MAtk, when rides her Fenrir and she’s buffed, but her healings are not as powerful as when holding her Sword of Valhalla (B Grade).
3. The level limit to control a wolf always is 19: you cant give orders to a wolf that is more than 19 levels higher than you. Cant say if your pet will still get experience, though, I’ve never tried. No easy exp for lowbies, sorry!!
4. If you try to ride a Fenrir that is more than 10 levels (not sure about this number) higher than you, his stats will drop very low, which means you’ll move VERY slowly. And since moving faster is the only sense in riding a Fenrir….
5. As said before, maintaining a Fenrir can be expensive: shots are cheap and top equipped evolved wolves use quite few to kill a single monster, but food is another kettle of fish: when fighting they eat at a more impressive rate as they grow in level; my 77 Fenrir can consume up to 10 enriched food units per hour (if not more), which means about 120k adena.

`NOTE`: has to be said that Great Wolves and Fenrirs are way less sensitive to stuns than normal toons; very useful in catacombs and other places of this sort.

As you can see, not all that glitters is gold, with Great Wolves and Fenrirs. Make a wise use of your furry best friend and you will never regret his presence.
Personally, being a Fortune Seeker, I find his help so useful that I could never think of going solo without him; it’s like having a heavy DD party mate under my direct command! And as Shadow Dancer (subclass), it’s even better, for Hierophant + Shillien Saint + Shadow Dancer buffs/dances make him just an impressive autonomous shredding-machine.

I think this is all I can say about wolves. Even if you don't plan to make such an extended use of this type of pet like me, I suggest you to raise at least a Great Wolf. Then you can put his necklace in your warehouse and forget about it, but I’m sure that soon or later you’ll feel the need for a loyal powerful mate.

Source: https://guidescroll.com/2011/09/lineage-ii-wolf-pet-guide/