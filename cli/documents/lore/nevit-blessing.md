# Nevit’s Blessings: Increase Vitality in the High Five (H5) Chronicle

## Vitality
Vitality has a very complex mechanism in High Five. 
In reality, the mechanism is not that complex, but the way it is depicted in the Nevit’s Blessing window is extremely confusing for new players. 
In this article I will decode what all those messages mean and how to use Nevit’s Blessing to increase Vitality, gain more XP and level up much faster in H5.
First of all we need to clearly define what Vitality is, what the Nevit’s Blessing is and how these two relate to each other.

## What is Vitality
Vitality is depicted by four short horizontal grey bars, to the left of the XP bar, on the top left of your Lineage 2 window. 
There are four states of Vitality corresponding to the number of lines that are visible. 
4 lines give 300% xp bonus, 3 lines give 250% xp bonus, 2 lines 200% xp bonus and 1 line 150% xp bonus. 
When you exhaust your vitality (by killing mobs), no lines are appearing next to the XP bar. In order to xp faster, you need to keep your vitality to the highest level, at 4 bars.
However, Vitality in H5 does not reset every week like in GoD, so how do you refill it? Vitality becomes replenished by:
- killing raid bosses
- doing the Pailaka Quests
- sitting in peace zones for longer periods of time
- being offline for longer periods of time
- using Vitality Herb
- (and this is the most important way) by taking advantage of something that people call a bug in the Nevit’s Blessing mechanism.

## What is the Nevit’s Blessing
The Nevit’s Blessings window shows two completely unrelated things, and this is where it gets confusing for most people. 
The first thing is the <Rec Bonus>. The second is the <Hunting Bonus>.

## Recommendation System or <Rec Bonus>
The Rec bonus is an XP bonus that you receive when you have been recommended at least 50 times by other players. 
This gives you 50% more xp for one hour. 
This one hour starts to count when you are hunting mobs outside a peace zone. 
Within a peace zone it does not count (it pauses). 
This one hour runs, pauses and stops at the same time that your hunting bonus runs. 
This is the only relation between the two. However, the hunting bonus lasts longer and has another function. 
Note that every day, you lose 20 recommendations. So you need to receive at least 20 recommendations every day to retain your recommendations number. 
This way, everyday, your first hunting hour will have 50% xp bonus from the Recommendation System. 
The timer for the Rec  and Hunting Bonus reset every day at 6:30 am.

## The Real Nevit’s Blessings System or <Hunting Bonus>
The Hunting bonus is not really a bonus. It is a bar that fills gradually from 0 to 100% when you start hunting mobs and receive xp from them. 
This bar will keep filling continuously for the first 4 hours of your mob hunting every day. 
It pauses filling when you soe, teleport or relog your character and sit in a peace zone.
The only relation between the Rec Bonus and the Hunting Bonus is that they both start when you first start hunting mobs, and they both pause when you are in a peace zone. 
They share the same timer. What does it mean pause? It means that you stop wasting their bonuses. 
It is like playing a game of chess and pressing your clock after moving a piece. Now your clock stops running and you are saving time. 
Whenever you start hunting mobs again, your bonus timer start running. Every day you have 1 hour available of Rec Bonus  XP and 4 hours of Hunting Bonus.

## So what does the Hunting Bonus give you, if it does not give you any XP bonus? 
When your hunting bonus bar reaches 100% you receive the Nevit’s Blessing. 
This blessing last for 3 minutes and during those minutes your vitality increases in relation to the xp you gain. 
That means that the more mobs you kill, the more your vitality will increase. 
For the average player though, it is impossible to kill so many mobs in 3 minutes, to completely refill all four vitality bars. 

## So how do people refill it? 
They do this simple trick:
- When the Hunting Bonus bar reaches 97% you re-log. You do this to stop the timer from running. Instead of re-logging you can also just using scroll of escape to a peace zone. Re-logging is faster though. So the recommendation is to re-log and then use SOE. 
- Then, you go to an area where there are many high level players killing hard mobs in parties. The two most popular areas are 1) Forge of Gods, 2) Giant’s Cave.

### Forge of Gods
In FoG you need to go to an area where players are gathering trains and once they have the agro, you start spamming aoe attacks on them. 
It does not matter if you are low level and you don’t do any damage. Even a little damage is going to return to you a lot of xp. 
In FoG the mobs spawn a second wave of mobs and in order to receive the xp, you need to attack those at least once as well. 
The first time you do this, your Nevit’s Blessings will activate as you reach the 100%. Your character’s body will start glowing with that red light. 
That is when you need to relog and remain offline for approximately the time that people need to gather a new train of FoG mobs. 
You can do this either approximately, by logging in and out again and checking whether trains are coming (but you will waste some seconds this way), or you can set up an alt in that area, as a camera that will watch when the trains are coming. 
At that point you log in your character again and start using your aoe skills. If your character does not have any aoe skills, you can do the following trick: use a low level wizard in your party to make the aoe attacks. 
This requires some planning and organisation, but it is really worth it, because the xp bonus you will receive from having full vitality is tremendous. When the train is dead you relog again, to save seconds from your 3 minutes Nevit’s Blessing. 
Then you repeat this procedure until your vitality fills up, or your Nevit’s Blessing runs out. Be careful to monitor the duration of the Blessing, because if it runs out and you hit the mobs, you are going to exhaust your vitality. 
You monitor this by hovering the mouse of the Hunting Bonus bar.
Note that sometimes people are not making trains at the entrance of FoG, if you have a summoner alt, you can use it to get to the upper areas. 
Sometimes you may find people selling summons there, that’s also an option you can use. 
Or you can maneuver your character to run quickly through, or run through the empty rooms when people are gathering the mobs for trains.

### Giant’s Cave
Here the principle is the same, but you will not attack the mobs in aoe, because people don’t make trains here. 
You will attack each mob once. If you are lucky, there will be at least two strong parties in Upper or Lower and they will be killing the mobs fast. 
In that case, you can quickly hit each mob once. 
If you are not lucky and there is only one weak party for example, you may still be able to fill 1-2 bars of your vitality.

## About the Complexity
The Nevit’s Blessings system may seem too complex and intimidating in the beginning. 
It may seem like too much work to set up cameras or  get an alt in FoG. Or you may feel weird ks-ing mobs of other players. 
The truth though is that other players are not harmed by your ks-ing, you are not taking their xp, nor their drops. 
On the other hand, the 300% XP benefit that you will receive from having full vitality is enormous and it is the only way to level up relatively fast in H5, without having to grind for ages.
Once you do this a couple of times, you will realise that it becomes easy and extremely beneficial. Make sure to refill your vitality during hours when the server is mostly populated. 
Because late at night you will have troubles finding other parties in FoG or Giant’s Cave.

## Sources
- above is fully taken from https://ankontini.com/nevits-blessings-increase-vitality-in-the-high-five-h5-chronicle/