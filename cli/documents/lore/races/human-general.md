# Human

- Humans in Lineage II are similar to Humans in the modern world. Humans currently have the greatest dominion in the world and the largest population.
- Although Humans do not possess any special abilities, their strong point is the fact that they can transfer to the most specialized classes.
- Humans are well-balanced in all of their characteristics.

## Fighter
- A basic fighter type
- Characters have learned the basic usage of various arms and weapons, also received basic training in physical combat.
- Fighters possess superior strength and dexterity. 
- They pride themselves as being the most skilled in close-range physical combat, mainly using swords and other hand-held weapons. 
- They may also choose to focus on long-range attacks to develop into archers later in their careers.

## Fighter Stats
- INT 21
- STR 40
- CON 43
- MEN 25
- DEX 30
- WIT 11

## Mystic
- Class is interested in the forces encircling the world.
- Characters of this class research and utilize these forces. 
- Since they tend to stay in a back room while reading a thick magic book or conducting various experiments with strange materials, they do not have a very good social reputation.
- They can be considered trainee wizards who are diligently preparing themselves to become more powerful.
- Appearance is that of a character clad in a robe holding a staff, which is used to help with magic casting.
- They can use basic level attack magic and recovery magic.
- Mystics study the ways of sorcery, and may summon magical creatures, cure grievous wounds, cast deadly attack spells, or create other wondrous miracles through the power of their intellect.

## Mystic Stats
- INT 41
- STR 22
- CON 27
- MEN 39
- DEX 21
- WIT 20