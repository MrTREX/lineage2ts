# Elf

- The race of Elves worships the goddess of water and loves nature and aquatic life. 
- The Elves have slim and nimble bodies, long ears, and beautiful features. 
- During the era of giants, among all creatures they held the most power. 
- However, when the giants were destroyed, the power and influence of the Elves were also diminished. 
- Now they only inhabit part of the forest in Aden.
- Compared to other races, Elves can hold their breath longer and are better able to jump from high places.
- Elves are wise and quick-witted. 
- They are fast in attack, movement and casting speeds. 
- They also have superior marksmanship skills and higher evasion ratings. 
- However, their attack strength is inferior to that of other races.

## Fighter
- Elven Fighters are those who train themselves physically to protect the forest and their fellow Elves from various enemies such as monsters, Humans and Dark Elves.
- Fighters are most often young Elves with strong bodies and minds who occasionally leave the forest and journey to remote locations for training.
- Their basic equipment is a sword and leather armor.
- Delicate, willowy, and graceful, Elven Fighters are adept warriors that practice precise, deadly swordsmanship. 
- The mere sight of them wielding their long, slender swords has led some scholars to call their swordfighting the “beautiful dance.”

## Fighter Stats
- INT 23
- STR 36
- CON 36
- MEN 26
- DEX 35
- WIT 14

## Mystic
- Similar to Human Mystics in their use of attack and recovery magic, Elven Mystics are often seen wearing robes and carrying staves.
- In order to protect nature, they borrow the power of the spirits and focus traiing on magical abilities.
- Magic runs deep in Elven blood, and as such the Elven Mystics are perhaps the mightiest sorcerers in Aden. 
- They focus on the strength of their mind, often to the detriment of their constitution, and there may be weak in physical combat. 
- Yet their command of the elemental forces makes them formidable opponents.

## Mystic Stats
- INT 37
- STR 21
- CON 25
- MEN 40
- DEX 24
- WIT 23

Reference link: https://legacy-lineage2.com/Knowledge/race_elf.html