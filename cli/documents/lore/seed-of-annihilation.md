# Freya - Seed of Annihilation
The Seed of Annihilation is located far to the north of the Keucereus Alliance Base. 
It is a huge living body that has driven roots into the ground to absorb the life energy of the earth. 
The absorbed energy is sent to a mother body, called Istina, in the center of the Seed who uses it to spawn all sorts of creatures. 
The various species born from Istina all want to progress on the path of evolution and are in fierce competition with each other, since it is a question of survival or extinction, annihilation.


## Travel
There are basically two ways to reach the Seed of Annihilation from the Keucereus Alliance Base - flying or teleporting. 
The latter possibility is somewhat expensive: being teleported to the Seed of Annihilation will cost you 75,000 adena, the same as to the much nearer Seeds of Infinity and Destruction (In Taiwan the teleportation fee is now 150,000 adena). 
If you don't mind the expenditure, talk to Transmitter Rayonel, a computer-like NPC that has been added near Engineer Lekon and Special Product Broker Kutran at the central fountain of the Base.

With the addition of Transmitter Rayonel there is now, unless you want to do aerial hunting, no more need to fly in Gracia, neither by yourself nor by Airship. 
But on the Korean PTS quite a number of players were seen who considered the teleportation fees too high and preferred to fly in the traditional way. 
Considering the long distance this may seem reckless, but while you are on your way you can admire the landscape of Gracia with its countless wonders.

Landing at the Seed of Annihilation is only possible on a very narrow strip of rock that is stretching out from the cliffs surrounding the Seed of Annihilation, marked green in the screenshot below. 
It took Whip fully 5 minutes to find that place on the south side of the Seed.


## Location
There are several NPCs at the Seed of Annihilation who will provide you with necessary information. 
The first NPC you will encounter at the entrance of the Seed is the Elven Officer Clemis. 
He will teleport players of lvl 80 or above into the Seed, depositing them in an "entrance hall" where they can meet General Ermian and other NPCs.

Nemo will provide you free of charge with a Maguen Plasma Collection Device, giving you tips how to use it, and he will also tell you where to find the NPCs you need for various quests. 
You should absolutely talk to him.


The Maguen Plasma Collection Device is the key to obtaining the newly added Maguen pet. Make sure to get such a Collection Device from Nemo.

When an Elite monster in the Seed of Annihilation is killed by a party of players, there will with a low probability appear a Wild Maguen (in the English version this might work with any monster, not just the Elite group leaders). 
This temporary Maguen gives beneficial buffs, additional to the Seed Efficacies, to the party members (in the English version mostly only to one single, randomly chosen player). 
This is a remotely similar principle to the Trained Cougars etc. at the Beast Farm.

When somebody is acquiring such a Maguen buff, he or she has a (very small) chance to obtain a Maguen Horn Pipe or an Elite Maguen Horn Pipe. 
Those are permanent items with which a regular Maguen resp. Elite Maguen can be summoned. Like a Dragon Flute. 
The regular Maguen has the ability to teleport its master to the outer entrance of the Seed of Annihilation, while the Elite Maguen can teleport its master's whole party to that Seed.

## Difficulty
The Seed of Annihilation has a similar level of difficulty as the Steel Citadel on Hellbound. 
All related quests start at lvl 84, which might seem a bit unkind, but it is really pointless to enter this Seed with a character of lvl 83 or below - all the monsters are lvl 85 and have a high resistance to Sleep and Hold Attacks. 
They also all have a high P.Atk., a high M.Atk. and a high P.Def.; only their M.Def. is low. 
Since all the monsters in the Seed of Annihilation have Earth Attribute, bringing a weapon and armor with Wind Attribute is highly recommended.

Even the starting level of the very first quest, "To the Seed of Annihilation", has been set at 84, although this is a simple letter-delivery-errand-running quest. 
You must have done this quest in order to pick up any other quest at the Seed of Annihilation, so first of all talk to Commander Keubaldir at the Keucereus Alliance Base.

Unlike the Seed of Destruction and the Seed of Infinity, the Seed of Annihilation is not an instanced zone but consists of three normal dungeons - the Bistakon, the Reptilikon and the Cokrakon - where the collection of Energy Compression Stones will be possible every day. 
Apart from that, complete Attribute Crystals can also be obtained as drops from monsters.

At the beginning of the Korean PTS some players thought that it would be possible to collect Energy Compression Stones with low level characters. 
But since after one day the monsters were changed so that they attacked any moving character, regardless whether in party or solo, this hope has come to nothing. 
The only way to escape the monsters' notice would be with the lvl 81 Dagger skill Hide. 
But since Hide has only a duration of 30 seconds and a reuse time of 3 minutes, this is not a practicable method.

Collecting Energy Compression Stones in the Seed of Annihilation is different from the Seeds of Infinity and Destruction. 
In the new Seed it can happen to you with a very high probability that exactly in the moment you are collecting an Energy Compression Stone a monster or, more frequently, a whole group of four monsters, is spawning in this place. 
Don't be surprised when you are suddenly faced with a monster just after you have finished the collecting process. Be prepared!

## Schedules
Each Compound in the Seed of Annihilation is inhabited by different species which are in conflict with each other and have each a Raid Boss, called Taklacan (Bistakon), Torumba (Reptilikon) and Dopagen (Cokrakon), as their leader. 
In every Compound the buffs "Efficacy of Strength", "Efficacy of Wisdom" and "Efficacy of Agility" are given by the Seed, and each "Efficacy" is strongly affecting the hunting efficiency of specific classes:
- Efficacy of Strength - increases the P.Atk. of Melee Damage Dealers
- Efficacy of Wisdom - increases the M.Atk. of Nukers
- Efficacy of Agility - increases the P.Atk. of Archers and Arbalesters

Every Monday at 1 pm real time the buffs given in each Compound are changing, but not in a regular circulation (Intelligence -> Agility etc.) but completely at random.
There is a once-per-day escort quest, "Completely Lost" for 9 person parties where you have to rescue soldiers of the Keucereus Alliance who are trapped in the Seed of Annihilation and get recipes as well as key materials for Vesper armor and jewelry as reward.
But be careful. Some of the Wounded Soldiers are so traumatized that they join the monsters in attacking you.

## Zones
Now let's have a look at the three different zones. 
You can reach them from the "entrance hall" by walking along three corridors. 
The Bistakon in the northern part of the Seed is a hunting ground where you have to deal with lions, buffaloes and other large creatures, half animal, half human. 
The Bistakon is the place where you will, after the Reptile Compound, find the most Seed Energies. 
The problem is that the monsters are very thick on the ground, which means a high probability of being attacked by many of them at the same time. 
Don't go into the Bistakon unless there is an area buff beneficial to your class. When you look at the entrance, you will notice that the lights to the left and right of the gateway are blue in one case and red in the two others.
Those are indicators for the local buff that is on at the time. 
Blue means Efficacy of Wisdom, green means Efficacy of Agility and red means Efficacy of Strength.

They all have Earth Attribute. All monsters in the Seed of Annihilation, no matter whether Elite types or followers, no matter which compound, have a basic Elemental resistance (as tested with Fire) of 216; their resistance to Wind attacks will be around 190. 
Keep that in mind when you build your weapon for this hunting ground.

In summing up it can be said that for players of lvl 85 with suitable equipment it can be highly recommended to try the Seed of Annihilation. 
Of course full 9 person parties are required, but since on a live server the party gets 2,800,000 exp per monster at Vitality 4, and since there are plenty of Seed Energies to be harvested, it is definitely worth the long trip from the Keucereus Base. 
The exp you get in the Seed of Annihilation is twice that at the Monastery of Silence, which has about the same degree of difficulty. 
In other words, the hunt in the Seed of Annihilation is twice as efficient.

Source: https://lineage-ii-freya.blogspot.com/2010/08/freya-seed-of-annihilation.html