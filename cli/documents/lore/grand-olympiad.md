# Grand Olympiad

- The Grand Olympiad is a one-on-one PvP competition held to determine the Hero. It is held in one month periods, and at the end of each period, the Noblesse with the most Olympiad points in each class is appointed Hero. Only Noblesse who have completed the third class transfer can participate in the Olympiad competition.

## Competition Method

- The Olympiad is classified into two competitions: the class competition and the non-class competition. Only members of a certain class can partake in class competitions. There are no class restrictions for participating in non-class competitions.
- Participating in a competition is possible only through the Olympiad Master, and the hours of competition are 6:00 PM CST to 12:00 AM CST (6:00 PM GMT+1 to 12:00 AM GMT+1 on the Teon and Franz servers). No competition requests are taken outside of competition hours.
- Competitors will be matched randomly and summoned to the coliseum for competition. Instructions will be regularly provided through system messages, starting 30 seconds before being summoned to the coliseum. The summoned competitors are allotted 60 seconds of preparation time. The coliseum will then transform into a battlefield and the competition will begin.
- Please note that the number of items in your character's inventory must be below 80% or you will be refused admittance and loss points. So if you have 80 slots available, you must have under 64 items in your inventory in order to be admitted.
- Before the competition begins, HP, MP, and CP are all recovered and the pre-existing buffs and debuffs will be removed. Any summoned pets will be returned, but the servitors/cubics will accompany the player to the coliseum. When being moved to the Olympiad Stadium in the middle of summoning a servitor or cubic, the skill in use is terminated automatically.  The summoner’s buffs will also dissolve before the start of the competition. A buff is provided in games between players of the same class and a Noblesse Gate Pass is rewarded to the winner.
- Once transported to the coliseum, if a character abnormally leaves the game, he will lose the competition. If the server goes down while a competition is in progress, the competition will be nullified and the competitors will restart at the villages.
- The maximum competition time is 6 minutes, and the player whose HP drops to 0 first will lose. If no victor has emerged at the 6 minute mark, the character who has inflicted the most damage will be determined the winner. The damage inflicted on an opponent's servitor is not considered in this determination.
- During the competition, a character whose HP has reached 0 will merely lose the competition, but not die. Upon completion of the competition, the Olympiad point changes will be announced in a system message and the competitors will be returned to the village.
- Hero skills and clan skills cannot be used in an Olympiad game.
- When an Olympiad match cannot be held due to a lack of combatants, the system will notify you of the game delay with an appropriate message.
- When moved to an Olympiad Stadium with the automatic use of Soulshots/Spiritshots or Beast Soulshots/Spiritshots enabled, the automatic use is cancelled.

## Olympiad Requirements

- Only players in their main classes are able to compete. If the player changes from the main class to a subclass after applying for the competition, he/she will be ineligible to compete.
- If the character belongs to a party when summoned to the Coliseum to compete, he/she will automatically withdraw from the party.
- Characters with 0 Olympiad points are ineligible to compete.
- In the Olympiad competitions, no items other than soulshots, spiritshots, Echo Crystals, energy stones, and firecrackers are allowed. Recall and Party Recall abilities also cannot be used in competition.

# Olympiad Points

- At the beginning of each Olympiad period, 18 points are equally awarded to all Noblesse, and 3 points each week thereafter. The loser’s Olympiad points are transferred to the winner upon completion of the competition. Competitors are able to check their current point status with the Grand Olympiad Manager.
- At the end of competition, if a character has accumulated more than 50 Olympiad points, they can be exchanged for Noblesse Gate Passes. The rate of exchange is 1,000 Noblesse Gate Passes for 1 Olympiad point. For example, while 50 Olympiad points is exchangeable for 50,000 Noblesse Gate Passes, and 49 Olympiad points are not worth any Noblesse Gate Passes.
- Noblesse Gate Passes may be used for purchasing items from the Grand Olympiad Manager or while teleporting to various locations via the Gatekeeper.

The following items can be purchased from the Grand Olympiad Manager with Noblesse Gate Passes:

| Item Name                                            | Cost in Gate Passes |
|------------------------------------------------------|---------------------|
| Blessed Scroll: Enchant Weapon (Grade S)             | 420,000             |
| Blessed Scroll: Enchant Weapon (Grade A)             | 195,000             |
| Blessed Scroll: Enchant Weapon (Grade B)             | 84,000              |
| Blessed Scroll: Enchant Armor (Grade S)              | 63,000              |
| Blessed Scroll: Enchant Armor (Grade A)              | 23,400              |
| Blessed Scroll: Enchant Armor (Grade B)              | 9,000               |
| Scroll: Enchant Weapon (Grade S)                     | 70,000              |
| Scroll: Enchant Weapon (Grade A)                     | 23,400              |
| Scroll: Enchant Weapon (Grade B)                     | 6,000               |
| Scroll: Enchant Armor (Grade S)                      | 7,000               |
| Scroll: Enchant Armor (Grade A)                      | 3,120               |
| Scroll: Enchant Armor (Grade B)                      | 960                 |
| Secret Book of Giants                                | 5,500               |
| Ancient Book - Divine Inspiration (Original Version) | 450,000             |

## Watching the Grand Olympiad

- The Olympiad competitions can be watched with the Grand Olympiad Manager stationed in every village. Players are able to see the venue where the current competition is under way, and the name of the participants.
- In the competition spectator mode, the player can view the competitors’ names, HP, CP, and buff status. While watching the competition, the competition window of each competitor can be maximized to show the skills being used and critical hits.
- Players are able to watch competitions under way in other coliseums without leaving the current coliseum.
- The Return button exits spectator mode. If the spectator mode is interrupted by a server reset or other abnormal conditions, you will begin at the village where you entered the spectator mode.
- If a player starts watching a competition after calling a pet, such pet will be dismissed.
- No charge for watching Olympiad competitions.

Reference link: https://legacy-lineage2.com/Knowledge/hero.html