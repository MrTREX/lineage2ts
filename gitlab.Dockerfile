FROM node:22.12.0-bullseye AS build

RUN mkdir -p /opt
WORKDIR /opt

RUN git clone --depth 1 https://gitlab.com/MrTREX/lineage2ts.git

WORKDIR /opt/lineage2ts/
RUN npm ci

WORKDIR /opt/lineage2ts/cli
RUN chmod +x provisionDefaultArtifacts.sh && bash provisionDefaultArtifacts.sh
RUN chmod +x provisionGeopackArtifacts.sh && bash provisionGeopackArtifacts.sh

WORKDIR /opt/lineage2ts
RUN cd game-server && npm run build
RUN cd login-server && npm run build

RUN rm -rf node_modules
RUN npm ci --omit=dev --workspace=login-server
RUN npm ci --omit=dev --workspace=game-server

FROM node:22.12.0-slim

RUN mkdir -p /opt/lineage2ts/game-server && mkdir -p /opt/lineage2ts/login-server
WORKDIR /opt/lineage2ts

COPY --from=build /opt/lineage2ts/game-server /opt/lineage2ts/game-server
COPY --from=build /opt/lineage2ts/login-server /opt/lineage2ts/login-server
COPY --from=build /opt/lineage2ts/node_modules /opt/lineage2ts/node_modules

COPY --from=build /opt/lineage2ts/login.database /opt/lineage2ts/login-server/login.database
COPY --from=build /opt/lineage2ts/game.database /opt/lineage2ts/game-server/game.database
COPY --from=build /opt/lineage2ts/datapack.database /opt/lineage2ts/game-server/datapack.database
COPY --from=build /opt/lineage2ts/geopack.database /opt/lineage2ts/game-server/geopack.database

COPY --from=build /opt/lineage2ts/package.json /opt/lineage2ts/package.json
COPY --from=build /opt/lineage2ts/package-lock.json /opt/lineage2ts/package-lock.json
COPY --from=build /opt/lineage2ts/LICENSE /opt/lineage2ts/LICENSE
COPY --from=build /opt/lineage2ts/Readme.md /opt/lineage2ts/Readme.md

COPY docker-entry.sh docker-entry.sh
RUN chmod +x docker-entry.sh

LABEL maintainer="MrTREX" version="dev"  website="https://gitlab.com/MrTREX/lineage2ts"

EXPOSE 7777 2106
VOLUME ["/opt/lineage2ts/login-server", "/opt/lineage2ts/game-server"]
ENV NODE_ENV=production

CMD [ "/bin/bash", "docker-entry.sh" ]