export const enum ServerAvailabilityStatus {
    STATUS_AUTO = 0x00,
    STATUS_GOOD = 0x01,
    STATUS_NORMAL = 0x02,
    STATUS_FULL = 0x03,
    STATUS_DOWN = 0x04,
    STATUS_GM_ONLY = 0x05,
}

export const enum ServerAgeStatus {
    All = 0x00,
    Age15AndUp = 0x0F,
    Age18AndUp = 0x12,
}

export const enum ServerType {
    None = 0,
    Normal = 0x01,
    Relaxed = 0x02,
    Testing = 0x04,
    Unlabeled = 0x08,
    RestrictedEntry = 0x10,
    EventOnly = 0x20,
    FreeForAll = 0x40,
}

export function getServerListType( line : string ) : ServerType {
    if ( !line ) {
        return ServerType.Normal
    }

    return line.split( ',' ).reduce( ( result: ServerType, currentType ) => {
        switch ( currentType.toLowerCase() ) {
            case 'normal':
                result |= ServerType.Normal
                break
            case 'relax':
                result |= ServerType.Relaxed
                break
            case 'test':
                result |= ServerType.Testing
                break
            case 'nolabel':
                result |= ServerType.Unlabeled
                break
            case 'restricted':
                result |= ServerType.RestrictedEntry
                break
            case 'event':
                result |= ServerType.EventOnly
                break
            case 'free':
                result |= ServerType.FreeForAll
                break
            default:
                result |= ServerType.Normal
                break
        }

        return result
    }, ServerType.None )
}