import { L2RPCApi } from './IRPCApi'
import { L2BinaryConverter } from './interface/BinaryConverter'
import { MessagePackConverter } from './MessagePackConverter'

export const RpcManager : L2RPCApi = {
    getDataConverter(): L2BinaryConverter {
        return MessagePackConverter
    }
}