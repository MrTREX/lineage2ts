import { L2BinaryConverter } from './interface/BinaryConverter'
import { Packr, Unpackr } from 'msgpackr'

const packOperation = new Packr()
const unPackOperation = new Unpackr()

export const MessagePackConverter: L2BinaryConverter = {
    pack( data: any ): Buffer {
        return packOperation.pack( data )
    },

    unpack( data: Buffer ): any {
        return unPackOperation.unpack( data )
    },
}