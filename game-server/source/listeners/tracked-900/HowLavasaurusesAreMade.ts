import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestType } from '../../gameService/enums/QuestType'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'

import _ from 'lodash'

const ROONEY = 32049
const FRAGMENT_STONE = 21909
const FRAGMENT_HEAD = 21910
const FRAGMENT_BODY = 21911
const FRAGMENT_HORN = 21912
const TOTEM_OF_BODY = 21899
const TOTEM_OF_SPIRIT = 21900
const TOTEM_OF_COURAGE = 21901
const TOTEM_OF_FORTITUDE = 21902
const minimumLevel = 76

const monsterRewards: { [ npcId: number ]: number } = {
    18799: FRAGMENT_STONE,
    18800: FRAGMENT_HEAD,
    18801: FRAGMENT_BODY,
    18802: FRAGMENT_HORN,
}

export class HowLavasaurusesAreMade extends ListenerLogic {
    constructor() {
        super( 'Q00901_HowLavasaurusesAreMade', 'listeners/tracked-900/HowLavasaurusesAreMade.ts' )
        this.questId = 901
        this.questItemIds = [
            FRAGMENT_STONE,
            FRAGMENT_HORN,
            FRAGMENT_HEAD,
            FRAGMENT_BODY,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ ROONEY ]
    }

    getTalkIds(): Array<number> {
        return [ ROONEY ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewards ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00901_HowLavasaurusesAreMade'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32049-03.htm':
            case '32049-08.html':
            case '32049-09.html':
            case '32049-10.html':
            case '32049-11.html':
                break

            case '32049-04.htm':
                state.startQuest()
                break

            case '32049-12.html':
                await QuestHelper.rewardSingleItem( player, TOTEM_OF_BODY, 1 )
                await state.exitQuestWithType( QuestType.DAILY, true )

                break

            case '32049-13.html':
                await QuestHelper.rewardSingleItem( player, TOTEM_OF_SPIRIT, 1 )
                await state.exitQuestWithType( QuestType.DAILY, true )

                break

            case '32049-14.html':
                await QuestHelper.rewardSingleItem( player, TOTEM_OF_FORTITUDE, 1 )
                await state.exitQuestWithType( QuestType.DAILY, true )

                break

            case '32049-15.html':
                await QuestHelper.rewardSingleItem( player, TOTEM_OF_COURAGE, 1 )
                await state.exitQuestWithType( QuestType.DAILY, true )

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isCondition( 1 ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        await QuestHelper.rewardUpToLimit( player, monsterRewards[ data.npcId ], 1, 10, data.isChampion )

        if ( this.hasAllItems( player ) ) {
            state.setConditionWithSound( 2, true )
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.COMPLETED:
                if ( !state.isNowAvailable() ) {
                    return this.getPath( '32049-16.html' )
                }

                state.setState( QuestStateValues.CREATED )

            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '32049-01.htm' : '32049-02.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '32049-05.html' )

                    case 2:
                        if ( this.hasAllItems( player ) ) {
                            await QuestHelper.takeMultipleItems( player, -1, FRAGMENT_STONE, FRAGMENT_HEAD, FRAGMENT_BODY, FRAGMENT_HORN )

                            return this.getPath( '32049-06.html' )
                        }

                        return this.getPath( '32049-07.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    hasAllItems( player : L2PcInstance ) : boolean {
        return QuestHelper.getQuestItemsCount( player, FRAGMENT_STONE ) >= 10
                && QuestHelper.getQuestItemsCount( player, FRAGMENT_HEAD ) >= 10
                && QuestHelper.getQuestItemsCount( player, FRAGMENT_BODY ) >= 10
                && QuestHelper.getQuestItemsCount( player, FRAGMENT_HORN ) >= 10
    }
}