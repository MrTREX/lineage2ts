import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestType } from '../../gameService/enums/QuestType'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const npcIds: Array<number> = [
    32864,
    32865,
    32866,
    32867,
    32868,
    32869,
    32870,
    32891,
]

const UNREFINED_RED_DRAGON_BLOOD = 21913
const UNREFINED_BLUE_DRAGON_BLOOD = 21914
const REFINED_RED_DRAGON_BLOOD = 21903
const REFINED_BLUE_DRAGON_BLOOD = 21904
const minimumLevel = 83
const requiredBloodAmount = 10

const monsterRewards: { [ npcId: number ]: number } = {
    22844: UNREFINED_BLUE_DRAGON_BLOOD, // Dragon Knight
    22845: UNREFINED_BLUE_DRAGON_BLOOD, // Dragon Knight
    22846: UNREFINED_BLUE_DRAGON_BLOOD, // Elite Dragon Knight
    22847: UNREFINED_RED_DRAGON_BLOOD, // Dragon Knight Warrior
    22848: UNREFINED_RED_DRAGON_BLOOD, // Drake Leader
    22849: UNREFINED_RED_DRAGON_BLOOD, // Drake Warrior
    22850: UNREFINED_RED_DRAGON_BLOOD, // Drake Scout
    22851: UNREFINED_RED_DRAGON_BLOOD, // Drake Mage
    22852: UNREFINED_BLUE_DRAGON_BLOOD, // Dragon Guard
    22853: UNREFINED_BLUE_DRAGON_BLOOD, // Dragon Mage
}

const variableNames = {
    isWaiting: 'iw',
}

export class RefinedDragonBlood extends ListenerLogic {
    constructor() {
        super( 'Q00905_RefinedDragonBlood', 'listeners/tracked-900/RefinedDragonBlood.ts' )
        this.questId = 905
        this.questItemIds = [
            UNREFINED_RED_DRAGON_BLOOD,
            UNREFINED_BLUE_DRAGON_BLOOD,
        ]
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewards ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00905_RefinedDragonBlood'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32864-04.htm':
            case '32864-09.html':
            case '32864-10.html':
                break

            case '32864-05.htm':
                state.startQuest()
                break

            case '32864-11.html':
                await QuestHelper.rewardSingleItem( player, REFINED_RED_DRAGON_BLOOD, 1 )
                await state.exitQuestWithType( QuestType.DAILY, true )

                break

            case '32864-12.html': {
                await QuestHelper.rewardSingleItem( player, REFINED_BLUE_DRAGON_BLOOD, 1 )
                await state.exitQuestWithType( QuestType.DAILY, true )

                break
            }

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        await QuestHelper.runStateActionForParty( data.playerId, data.targetId, this.getName(), async ( state: QuestState, player: L2PcInstance ): Promise<void> => {
            if ( !state.isCondition( 1 ) ) {
                return
            }

            let itemId: number = monsterRewards[ data.npcId ]
            if ( QuestHelper.getQuestItemsCount( player, itemId ) < requiredBloodAmount ) {
                await QuestHelper.rewardUpToLimit( player, itemId, 1, requiredBloodAmount, data.isChampion )

                if ( QuestHelper.getQuestItemsCount( player, UNREFINED_RED_DRAGON_BLOOD ) >= requiredBloodAmount
                        && QuestHelper.getQuestItemsCount( player, UNREFINED_BLUE_DRAGON_BLOOD ) >= requiredBloodAmount ) {
                    state.setConditionWithSound( 2, true )
                    return
                }

                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
            }
        } )

        return
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.COMPLETED:
                if ( !state.isNowAvailable() ) {
                    return this.getPath( '32864-03.html' )
                }

                state.setState( QuestStateValues.CREATED )

            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() < minimumLevel ? '32864-02.html' : '32864-01.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '32864-06.html' )

                    case 2:
                        if ( !state.getVariable( variableNames.isWaiting ) ) {
                            state.setVariable( variableNames.isWaiting, true )

                            return this.getPath( '32864-07.html' )
                        }

                        return this.getPath( '32864-08.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}