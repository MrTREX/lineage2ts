import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { QuestType } from '../../gameService/enums/QuestType'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import _ from 'lodash'

const MATHIAS = 31340
const minimumLevel = 80
const SHATTERED_BONES = 21997
const CANNIBALISTIC_STAKATO_LDR_CLAW = 21998
const ANAIS_SCROLL = 21999
const PROOF_OF_CHALLENGE = 21750

const monsterRewards : {[ npcId : number ] : number } = {
    25309: SHATTERED_BONES, // Varka's Hero Shadith
    25312: SHATTERED_BONES, // Varka's Commander Mos
    25315: SHATTERED_BONES, // Varka's Chief Horus
    25299: SHATTERED_BONES, // Ketra's Hero Hekaton
    25302: SHATTERED_BONES, // Ketra's Commander Tayr
    25305: SHATTERED_BONES, // Ketra's Chief Brakki
    25667: CANNIBALISTIC_STAKATO_LDR_CLAW, // Cannibalistic Stakato Chief
    25668: CANNIBALISTIC_STAKATO_LDR_CLAW, // Cannibalistic Stakato Chief
    25669: CANNIBALISTIC_STAKATO_LDR_CLAW, // Cannibalistic Stakato Chief
    25670: CANNIBALISTIC_STAKATO_LDR_CLAW, // Cannibalistic Stakato Chief
    25701: ANAIS_SCROLL, // Anais - Master of Splendor
}

export class ReclaimOurEra extends ListenerLogic {
    constructor() {
        super( 'Q00902_ReclaimOurEra', 'listeners/tracked-900/ReclaimOurEra.ts' )
        this.questId = 902
        this.questItemIds = [
                SHATTERED_BONES,
                CANNIBALISTIC_STAKATO_LDR_CLAW,
                ANAIS_SCROLL
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ MATHIAS ]
    }

    getTalkIds(): Array<number> {
        return [ MATHIAS ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewards ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00902_ReclaimOurEra'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '31340-04.htm':
                break

            case '31340-05.html':
                state.startQuest()
                break

            case '31340-06.html':
                state.setConditionWithSound( 2, true )
                break

            case '31340-07.html':
                state.setConditionWithSound( 3, true )
                break

            case '31340-08.html':
                state.setConditionWithSound( 4, true )
                break

            case '31340-10.html':
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        await QuestHelper.runStateActionForParty( data.playerId, data.targetId, this.getName(), async ( state : QuestState, player : L2PcInstance ) : Promise<void> => {
            await QuestHelper.rewardSingleQuestItem( player, monsterRewards[ data.npcId ], 1, data.isChampion )
            state.setConditionWithSound( 5, true )
        } )

        return
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.COMPLETED:
                if ( !state.isNowAvailable() ) {
                    return this.getPath( '31340-02.htm' )
                }

                state.setState( QuestStateValues.CREATED )

            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '31340-01.htm' : '31340-03.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '31340-09.html' )

                    case 2:
                        return this.getPath( '31340-11.html' )

                    case 3:
                        return this.getPath( '31340-12.html' )

                    case 4:
                        return this.getPath( '31340-13.html' )

                    case 5:
                        if ( QuestHelper.hasQuestItem( player, SHATTERED_BONES ) ) {
                            await QuestHelper.rewardSingleItem( player, PROOF_OF_CHALLENGE, 1 )
                            await QuestHelper.giveAdena( player, 134038, true )
                        } else if ( QuestHelper.hasQuestItem( player, CANNIBALISTIC_STAKATO_LDR_CLAW ) ) {
                            await QuestHelper.rewardSingleItem( player, PROOF_OF_CHALLENGE, 3 )
                            await QuestHelper.giveAdena( player, 210119, true )
                        } else if ( QuestHelper.hasQuestItem( player, ANAIS_SCROLL ) ) {
                            await QuestHelper.rewardSingleItem( player, PROOF_OF_CHALLENGE, 3 )
                            await QuestHelper.giveAdena( player, 348155, true )
                        }

                        await state.exitQuestWithType( QuestType.DAILY, true )

                        return this.getPath( '31340-14.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}