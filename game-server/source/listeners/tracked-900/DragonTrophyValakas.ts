import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { QuestType } from '../../gameService/enums/QuestType'

const KLEIN = 31540
const VALAKAS = 29028
const MEDAL_OF_GLORY = 21874
const VACUALITE_FLOATING_STONE = 7267
const minimumLevel = 84

export class DragonTrophyValakas extends ListenerLogic {
    constructor() {
        super( 'Q00907_DragonTrophyValakas', 'listeners/tracked-900/DragonTrophyValakas.ts' )
        this.questId = 907
    }

    getQuestStartIds(): Array<number> {
        return [ KLEIN ]
    }

    getTalkIds(): Array<number> {
        return [ KLEIN ]
    }

    getAttackableKillIds(): Array<number> {
        return [ VALAKAS ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00907_DragonTrophyValakas'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        if ( player.getLevel() < minimumLevel || !QuestHelper.hasQuestItem( player, VACUALITE_FLOATING_STONE ) ) {
            return
        }

        switch ( data.eventName ) {
            case '31540-05.htm':
            case '31540-06.htm':
                break

            case '31540-07.html':
                state.startQuest()
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        await QuestHelper.runStateActionForCommandChannel( data.playerId, data.targetId, this.getName(), async ( state: QuestState ): Promise<void> => {
            state.setConditionWithSound( 2, true )
        } )

        return
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.COMPLETED:
                if ( !state.isNowAvailable() ) {
                    return this.getPath( '31540-03.html' )
                }

                state.setState( QuestStateValues.CREATED )

            case QuestStateValues.CREATED:
                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '31540-02.html' )
                }

                if ( !QuestHelper.hasQuestItem( player, VACUALITE_FLOATING_STONE ) ) {
                    return this.getPath( '31540-04.html' )
                }

                return this.getPath( '31540-01.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '31540-08.html' )

                    case 2:
                        await QuestHelper.rewardSingleItem( player, MEDAL_OF_GLORY, 30 )
                        await state.exitQuestWithType( QuestType.DAILY, true )

                        return this.getPath( '31540-09.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}