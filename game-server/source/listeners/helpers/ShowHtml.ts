import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { DataManager } from '../../data/manager'
import { NpcHtmlMessagePath } from '../../gameService/packets/send/NpcHtmlMessage'
import { ExShowScreenMessage } from '../../gameService/packets/send/ExShowScreenMessage'
import { TutorialShowHtml } from '../../gameService/packets/send/TutorialShowHtml'

export const ShowHtml = {
    showPath( player: L2PcInstance, path: string ): void {
        if ( !DataManager.getHtmlData().hasItem( path ) ) {
            if ( player.isGM() ) {
                player.sendMessage( `No html path: ${path}` )
            }

            return
        }

        if ( !player.getLastFolkNPC() ) {
            return
        }

        let html : string = DataManager.getHtmlData().getItem( path )
        player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), player.getLastFolkNPC() ) )
    },

    showNpcOnScreenMessage ( player: L2PcInstance, messageId: number, position: number, time: number, ...parameters: Array<string> ) : void {
        player.sendOwnedData( ExShowScreenMessage.fromNpcMessageId( messageId, position, time, parameters ) )
    },

    showTutorialHTML( player: L2PcInstance, path: string ) : void {
        if ( DataManager.getHtmlData().hasItem( path ) ) {
            player.sendOwnedData( TutorialShowHtml( DataManager.getHtmlData().getItem( path ), path, player.getObjectId() ) )
        }
    }
}