import { DataManager } from '../../data/manager'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2ItemInstance } from '../../gameService/models/items/instance/L2ItemInstance'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2ManualSpawn } from '../../gameService/models/spawns/type/L2ManualSpawn'
import { L2NpcTemplate } from '../../gameService/models/actor/templates/L2NpcTemplate'
import { ILocational, Location } from '../../gameService/models/Location'
import { Skill } from '../../gameService/models/Skill'
import { L2Item } from '../../gameService/models/items/L2Item'
import { ConfigManager } from '../../config/ConfigManager'
import { ItemInstanceType } from '../../gameService/models/items/ItemInstanceType'
import { L2EtcItem } from '../../gameService/models/items/L2EtcItem'
import { EtcItemType } from '../../gameService/enums/items/EtcItemType'
import { ItemTypes } from '../../gameService/values/InventoryValues'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { ListenerManager } from '../../gameService/instancemanager/ListenerManager'
import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { L2Character } from '../../gameService/models/actor/L2Character'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { TutorialShowQuestionMark } from '../../gameService/packets/send/TutorialShowQuestionMark'
import { PacketDispatcher } from '../../gameService/PacketDispatcher'
import { DefaultEnchantLevel } from './QuestVariables'
import { L2Object } from '../../gameService/models/L2Object'
import { L2Party } from '../../gameService/models/L2Party'
import { PlayerGroupCache } from '../../gameService/cache/PlayerGroupCache'
import { L2World } from '../../gameService/L2World'
import { AggroCache } from '../../gameService/cache/AggroCache'
import { L2NpcValues } from '../../gameService/values/L2NpcValues'
import { EventPoolCache } from '../../gameService/cache/EventPoolCache'
import { EventType, NpcGeneralEvent, NpcReceivingEvent } from '../../gameService/models/events/EventType'
import { ListenerCache } from '../../gameService/cache/ListenerCache'
import { AIEffectHelper } from '../../gameService/aicontroller/helpers/AIEffectHelper'
import aigle from 'aigle'
import _ from 'lodash'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { ItemDefinition } from '../../gameService/interface/ItemDefinition'
import { teleportCharacterToGeometryCoordinates } from '../../gameService/helpers/TeleportHelper'
import { GeometryId } from '../../gameService/enums/GeometryId'
import { ElementalType } from '../../gameService/enums/ElementalType'

const defaultNoQuestMessage : string = 'You are either not on a quest that involves this NPC, or you don\'t meet this NPC\'s minimum quest requirements.'
const defaultCompletedMessage : string = 'This quest has already been completed.'
const noQuestPath : string = 'data/html/noquest.htm'
const alreadyCompletedQuestPath : string = 'data/html/alreadycompleted.htm'
const featureNotImplementedPath : string = 'overrides/html/system/featureNotImplemented.htm'
const adenaCurrency : Set<number> = new Set<number>( [ ItemTypes.Adena, ItemTypes.AncientAdena ] )

export const QuestHelper = {
    getFeatureNotImplementedMessagePath(): string {
        return featureNotImplementedPath
    },

    getNoQuestMessagePath(): string {
        return DataManager.getHtmlData().hasItem( noQuestPath ) ? noQuestPath : defaultNoQuestMessage
    },

    getNoQuestMessage(): string {
        return DataManager.getHtmlData().hasItem( noQuestPath ) ? DataManager.getHtmlData().getItem( noQuestPath ) : defaultNoQuestMessage
    },

    getAlreadyCompletedMessagePath(): string {
        return DataManager.getHtmlData().hasItem( alreadyCompletedQuestPath ) ? alreadyCompletedQuestPath : defaultCompletedMessage
    },

    hasQuestItems( player: L2PcInstance, ...itemIds: Array<number> ): boolean {
        return itemIds.every( ( currentItemId: number ) => {
            return !!player.getInventory().getItemByItemId( currentItemId )
        } )
    },

    hasAtLeastOneQuestItem( player: L2PcInstance, ...itemIds: Array<number> ): boolean {
        return itemIds.some( ( itemId: number ) => {
            return !!player.getInventory().getItemByItemId( itemId )
        } )
    },

    hasQuestItem( player: L2PcInstance, itemId: number ): boolean {
        return !!player.getInventory().getItemByItemId( itemId )
    },

    hasEachQuestItem( player: L2PcInstance, ...itemIds: Array<number> ): Array<boolean> {
        return itemIds.map( ( itemId: number ) => {
            return !!player.getInventory().getItemByItemId( itemId )
        } )
    },

    async takeMultipleItems( player: L2PcInstance, amount: number, ...itemIds: Array<number> ): Promise<void> {
        let items: Array<L2ItemInstance> = player.getInventory().getWithItemIds( itemIds )
        let currentAmounts: { [ itemId: number ]: number } = {}

        await aigle.resolve( items ).each( async ( item: L2ItemInstance ) => {
            if ( item.isEquipped() ) {
                await player.getInventory().unEquipItemInBodySlot( item.getItem().getBodyPart() )
                player.broadcastUserInfo()
            }

            if ( amount < 0 ) {
                await player.destroyItem( item, true, 'QuestHelper.takeMultipleItems' )
                return
            }

            let currentAmount = currentAmounts[ item.getId() ]
            if ( !_.isNumber( currentAmount ) ) {
                currentAmounts[ item.getId() ] = amount
                currentAmount = amount
            }

            if ( currentAmount === 0 ) {
                return
            }

            let amountToDelete = Math.min( currentAmount, item.getCount() )
            await player.destroyItemByItemId( item.getId(), amountToDelete, true, 'QuestHelper.takeMultipleItems' )

            currentAmounts[ item.getId() ] = currentAmount - amountToDelete
        } )
    },

    async takeSingleItem( player: L2PcInstance, itemId: number, amount: number = 1 ): Promise<void> {
        let items: ReadonlyArray<L2ItemInstance> = player.getInventory().getItemsByItemId( itemId )
        let currentAmount = amount

        await aigle.resolve( items ).someSeries( async ( item: L2ItemInstance ) => {
            if ( item.isEquipped() ) {
                await player.getInventory().unEquipItemInBodySlot( item.getItem().getBodyPart() )
                player.broadcastUserInfo()
            }

            if ( currentAmount < 0 ) {
                await player.destroyItemByItemId( itemId, item.getCount(), true, 'QuestHelper.takeSingleItem' )
                return false
            }

            let amountToDelete = Math.min( currentAmount, item.getCount() )
            await player.destroyItemByItemId( itemId, amountToDelete, true, 'QuestHelper.takeSingleItem' )

            currentAmount = currentAmount - amountToDelete

            return currentAmount === 0
        } )
    },

    async giveSingleItem( player: L2PcInstance, itemId: number, amount: number = 1, enchantLevel: number = DefaultEnchantLevel ): Promise<void> {
        let item: L2ItemInstance = await player.getInventory().addItem( itemId, amount, player.getTargetId(), 'QuestHelper.giveSingleItem', enchantLevel )

        if ( enchantLevel > 0 && itemId !== ItemTypes.Adena ) {
            await item.setEnchantLevel( enchantLevel )
        }

        QuestHelper.sendItemGetMessage( player, item, amount )
    },

    async giveMultipleItems( player: L2PcInstance, amount: number, ...itemIds: Array<number> ): Promise<void> {
        let items: Array<L2ItemInstance> = await aigle.resolve( itemIds ).map( async ( itemId: number ): Promise<L2ItemInstance> => {
            return player.getInventory().addItem( itemId, amount, player.getTargetId(), 'QuestHelper.giveMultipleItems', DefaultEnchantLevel )
        } )

        items.forEach( ( item: L2ItemInstance ) => {
            QuestHelper.sendItemGetMessage( player, item, amount )
        } )
    },

    sendItemGetMessage( player: L2PcInstance, item: L2ItemInstance, amount: number ): void {
        if ( item.getId() === ItemTypes.Adena ) {
            let message = new SystemMessageBuilder( SystemMessageIds.EARNED_S1_ADENA )
                    .addNumber( amount )
                    .getBuffer()
            player.sendOwnedData( message )
        } else {
            if ( amount > 1 ) {
                let message = new SystemMessageBuilder( SystemMessageIds.EARNED_S2_S1_S )
                        .addItemInstanceName( item )
                        .addNumber( amount )
                        .getBuffer()
                player.sendOwnedData( message )
            } else {
                let message = new SystemMessageBuilder( SystemMessageIds.EARNED_ITEM_S1 )
                        .addItemInstanceName( item )
                        .getBuffer()
                player.sendOwnedData( message )
            }
        }
    },

    addSpawnAtLocation( npcId: number, location: ILocational, useRandomOffset: boolean = false, despawnDelay: number = 0, isSummonSpawn: boolean = false ): L2Npc {
        return QuestHelper.addGenericSpawn( null, npcId, location.getX(), location.getY(), location.getZ(), location.getHeading(), useRandomOffset, despawnDelay, isSummonSpawn, location.getInstanceId() )
    },

    addSummonedSpawnAtLocation( summoner: L2Npc, npcId: number, location: ILocational, useRandomOffset: boolean = false, despawnDelay: number = 0, isSummonSpawn: boolean = false ): L2Npc {
        return QuestHelper.addGenericSpawn( summoner, npcId, location.getX(), location.getY(), location.getZ(), location.getHeading(), useRandomOffset, despawnDelay, isSummonSpawn, location.getInstanceId() )
    },

    addGenericSpawn( summoner: L2Npc,
            npcId: number,
            x: number,
            y: number,
            z: number,
            heading: number = 0,
            useRandomOffset: boolean = false,
            despawnDelay: number = 0,
            isSummonSpawn: boolean = false,
            instanceId: number = 0 ): L2Npc {

        // TODO : replace with spawn point geometry
        let currentX: number = useRandomOffset ? ( x + _.random( 50, 100 ) * ( Math.random() < 0.5 ? -1 : 1 ) ) : x
        let currentY: number = useRandomOffset ? ( y + _.random( 50, 100 ) * ( Math.random() < 0.5 ? -1 : 1 ) ) : y

        let template: L2NpcTemplate = DataManager.getNpcData().getTemplate( npcId )
        let spawn: L2ManualSpawn = L2ManualSpawn.fromParameters( template, 1, currentX, currentY, z, heading, instanceId, isSummonSpawn )

        spawn.startSpawn()

        let npc: L2Npc = spawn.getNpcs()[ 0 ]
        if ( despawnDelay > 0 ) {
            npc.scheduleDespawn( despawnDelay )
        }

        if ( summoner ) {
            summoner.addSummonedNpc( npc )
        }

        return npc
    },

    getQuestItemsCount( player: L2PcInstance, itemId: number ): number {
        if ( !player ) {
            return 0
        }

        return player.getInventory().getInventoryItemCount( itemId, -1 )
    },

    getItemsSumCount( player: L2PcInstance, ...itemIds: Array<number> ): number {
        return player.getInventory().getWithItemIds( itemIds ).reduce( ( total: number, item: L2ItemInstance ) => {
            return total + item.getCount()
        }, 0 )
    },

    hasQuestItemCount( player: L2PcInstance, itemId: number, count: number ): boolean {
        return QuestHelper.getQuestItemsCount( player, itemId ) >= count
    },

    async castSkill( npc: L2Npc, player: L2PcInstance, skill: Skill ): Promise<void> {
        npc.setTarget( player )
        return npc.doCast( skill )
    },

    giveAdena( player: L2PcInstance, amount: number, applyRates: boolean = true ): Promise<void> {
        if ( applyRates ) {
            return QuestHelper.rewardSingleItem( player, ItemTypes.Adena, amount )
        }

        return QuestHelper.giveSingleItem( player, ItemTypes.Adena, amount )
    },

    async rewardSingleItem( player: L2PcInstance, itemId: number, amount: number = 1 ): Promise<void> {
        let template: L2Item = DataManager.getItems().getTemplate( itemId )
        let count = QuestHelper.getItemRateMultiplier( template ) * amount

        let item: L2ItemInstance = await player.getInventory().addItem( itemId, count, player.getTargetId(), 'QuestHelper.rewardItems', DefaultEnchantLevel )
        if ( !item ) {
            return
        }

        QuestHelper.sendItemGetMessage( player, item, count )
    },

    /*
        Only for rewards of quest specific items as drops from killed monsters that must use default enchantment level.
     */
    async rewardSingleQuestItem( player: L2PcInstance, itemId: number, amount: number, isFromChampionMonster: boolean ): Promise<void> {
        let template: L2Item = DataManager.getItems().getTemplate( itemId )
        let adjustedAmount = QuestHelper.getAdjustedAmount( itemId, amount, isFromChampionMonster )
        let count = QuestHelper.getItemRateMultiplier( template ) * adjustedAmount

        let item: L2ItemInstance = await player.getInventory().addItem( itemId, count, player.getTargetId(), 'QuestHelper.rewardSingleQuestItem', DefaultEnchantLevel )
        if ( !item ) {
            return
        }

        QuestHelper.sendItemGetMessage( player, item, count )
    },

    async rewardMultipleItems( player: L2PcInstance, amount: number, ...itemIds: Array<number> ): Promise<void> {
        let items: Array<L2ItemInstance> = await aigle.resolve( itemIds ).map( async ( itemId: number ): Promise<L2ItemInstance> => {
            let template: L2Item = DataManager.getItems().getTemplate( itemId )
            let count = QuestHelper.getItemRateMultiplier( template ) * amount

            let item: L2ItemInstance = await player.getInventory().addItem( itemId, count, player.getTargetId(), 'QuestHelper.rewardMultipleItems', DefaultEnchantLevel )
            if ( !item ) {
                return
            }

            return item
        } )

        items.forEach( ( item: L2ItemInstance ) => {
            QuestHelper.sendItemGetMessage( player, item, amount )
        } )
    },

    async rewardPossibleItems( player: L2PcInstance, amount: number, itemIds: Array<number> ): Promise<void> {
        let amountOfItems = Math.floor( Math.max( ConfigManager.rates.getRateQuestReward(), 1 ) )
        return QuestHelper.rewardMultipleItems( player, 1, ..._.times( amountOfItems, () => _.sample( itemIds ) ) )
    },

    getItemRateMultiplier( item: L2Item ): number {
        if ( !item ) {
            return 0
        }

        if ( item.getId() === ItemTypes.Adena ) {
            return ConfigManager.rates.getRateQuestRewardAdena()
        }

        if ( ConfigManager.rates.useQuestRewardMultipliers() ) {
            if ( item.isInstanceType( ItemInstanceType.L2EtcItem ) ) {
                switch ( ( item as L2EtcItem ).getItemType() ) {
                    case EtcItemType.POTION:
                        return ConfigManager.rates.getRateQuestRewardPotion()

                    case EtcItemType.SCRL_ENCHANT_WP:
                    case EtcItemType.SCRL_ENCHANT_AM:
                    case EtcItemType.SCROLL:
                        return ConfigManager.rates.getRateQuestRewardScroll()

                    case EtcItemType.RECIPE:
                        return ConfigManager.rates.getRateQuestRewardRecipe()

                    case EtcItemType.MATERIAL:
                        return ConfigManager.rates.getRateQuestRewardMaterial()
                }

            }
        }

        return ConfigManager.rates.getRateQuestReward()
    },

    async removeAllItems( player: L2PcInstance, items: Array<ItemDefinition>, ignoreCount: boolean = false ): Promise<boolean> {
        if ( !items || items.length === 0 ) {
            return false
        }

        if ( !this.hasEachItem( player, items ) ) {
            return false
        }

        await aigle.resolve( items ).each( async ( item: ItemDefinition ) => {
            return QuestHelper.takeSingleItem( player, item.id, ignoreCount ? -1 : item.count )
        } )

        return true
    },

    hasEachItem( player: L2PcInstance, items: Array<ItemDefinition> ): boolean {
        if ( !items || items.length === 0 ) {
            return false
        }

        return !items.some( ( item: ItemDefinition ) => {
            return !this.hasItem( player, item )
        } )
    },

    hasItem( player: L2PcInstance, item: ItemDefinition, includeCount: boolean = true ): boolean {
        if ( !item ) {
            return false
        }

        if ( includeCount ) {
            return this.getQuestItemsCount( player, item.id ) >= item.count
        }

        return this.hasQuestItems( player, item.id )
    },

    checkDistanceToTarget( player: L2PcInstance, target: L2Object ): boolean {
        return !target || GeneralHelper.checkIfInRange( ConfigManager.character.getPartyRange(), player, target, true )
    },

    teleportPlayer( player: L2PcInstance, location: Location, instanceId: number, allowRandomOffset: boolean = true ): Promise<void> {
        if ( allowRandomOffset ) {
            return teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, location.x, location.y, location.z, location.heading, instanceId )
        }

        return player.teleportToLocationCoordinates( location.x, location.y, location.z, location.heading, instanceId )
    },

    addExpAndSp( player: L2PcInstance, expAmount: number, spAmount: number ): Promise<void> {
        let adjustedExp = Math.floor( ConfigManager.rates.getRateQuestRewardXP() * expAmount )
        let adjustedSp = Math.floor( ConfigManager.rates.getRateQuestRewardSP() * spAmount )

        return player.addQuestExpAndSp( adjustedExp, adjustedSp )
    },

    haveMemo( playerId: number, questId: number ): boolean {
        let quest: ListenerLogic = ListenerManager.getQuest( questId )
        return quest && QuestStateCache.hasQuestState( playerId, quest.getName() )
    },

    getOneTimeQuestFlag( playerId: number, questId: number ): number {
        let quest: ListenerLogic = ListenerManager.getQuest( questId )
        if ( quest && quest.getQuestState( playerId, true ).isCompleted() ) {
            return 1
        }

        return 0
    },

    addSkillCastDesire( npc: L2Npc, target: L2Character, skillId: number, skillLevel: number, amount: number ): void {
        AIEffectHelper.notifyMustCastSpell( npc, target, skillId, skillLevel, false, false )
        AggroCache.addAggro( npc.getObjectId(), target.getObjectId(), 1, amount )
    },

    addAttackDesire( npc: L2Npc, character: L2Character, amount: number = 999 ): void {
        AIEffectHelper.notifyAttacked( npc, character, amount )
    },

    getItemIdEquipped( player: L2PcInstance, slot: number ): number {
        return player.getInventory().getPaperdollItemId( slot )
    },

    getEnchantLevel( player: L2PcInstance, itemId: number ): number {
        let item: L2ItemInstance = player.getInventory().getItemByItemId( itemId )
        if ( item ) {
            return item.getEnchantLevel()
        }

        return 0
    },

    getAdjustedAmount( itemId: number, amount: number, isChampion: boolean ): number {
        if ( !ConfigManager.rates.useQuestRewardMultipliers() ) {
            return amount
        }

        let adjustedMin = amount * ConfigManager.rates.getRateQuestDropMinimum()
        let adjustedMax = amount * ConfigManager.rates.getRateQuestDropMaximum()

        if ( isChampion && ConfigManager.customs.championEnable() ) {
            if ( [ ItemTypes.Adena, ItemTypes.AncientAdena ].includes( itemId ) ) {
                adjustedMin *= ConfigManager.customs.getChampionAdenasRewardsAmount()
                adjustedMax *= ConfigManager.customs.getChampionAdenasRewardsAmount()
            } else {
                adjustedMin *= ConfigManager.customs.getChampionQuestRewardsAmount()
                adjustedMax *= ConfigManager.customs.getChampionQuestRewardsAmount()
            }
        }

        return _.random( Math.floor( adjustedMin ), Math.floor( adjustedMax ) )
    },

    getAdjustedChance( itemId: number, chance: number, isFromChampion: boolean ): number {
        if ( !ConfigManager.rates.useQuestRewardMultipliers() ) {
            return chance
        }

        let adjustedChance = chance * ConfigManager.rates.getRateQuestDropChance()

        if ( isFromChampion && ConfigManager.customs.championEnable() ) {
            if ( adenaCurrency.has( itemId ) ) {
                return adjustedChance * ConfigManager.customs.getChampionAdenasRewardsChance()
            }

            return adjustedChance * ConfigManager.customs.getChampionQuestRewardsChance()
        }

        return adjustedChance
    },

    showQuestionMark( playerId: number, id: number ): void {
        PacketDispatcher.sendOwnedData( playerId, TutorialShowQuestionMark( id ) )
    },

    async rewardAndProgressState( player: L2PcInstance, state: QuestState, itemId: number, amount: number, limit: number, isFromChampion: boolean, nextStateValue: number ): Promise<boolean> {
        await QuestHelper.rewardSingleQuestItem( player, itemId, amount, isFromChampion )

        if ( state.isCondition( nextStateValue ) ) {
            return
        }

        if ( QuestHelper.getQuestItemsCount( player, itemId ) >= limit ) {
            state.setConditionWithSound( nextStateValue, true )
            return true
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        return false
    },

    async rewardUpToLimit( player: L2PcInstance, itemId: number, amount: number, limit: number, isFromChampion: boolean ): Promise<boolean> {
        if ( QuestHelper.getQuestItemsCount( player, itemId ) >= limit ) {
            return true
        }

        await QuestHelper.rewardSingleQuestItem( player, itemId, amount, isFromChampion )

        if ( QuestHelper.getQuestItemsCount( player, itemId ) >= limit ) {
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
            return true
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        return false
    },

    async runStateActionForParty( playerId: number, npcObjectId: number, questName: string, actionMethod: ( state: QuestState, player: L2PcInstance, npc: L2Npc ) => Promise<void | boolean> ) {
        let party: L2Party = PlayerGroupCache.getParty( playerId )
        let npc = L2World.getObjectById( npcObjectId ) as L2Npc

        await aigle.resolve( party ? party.getMembers() : [ playerId ] ).each( async ( playerId: number ) => {
            let state: QuestState = QuestStateCache.getQuestState( playerId, questName, false )

            if ( !state ) {
                return
            }

            let player = L2World.getPlayer( playerId )
            if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
                return
            }

            return actionMethod( state, player, npc )
        } )
    },

    async runStateActionForCommandChannel( playerId: number, npcObjectId: number, questName: string, actionMethod: ( state: QuestState, player: L2PcInstance, npc: L2Npc ) => Promise<void | boolean>, range: number = 1500 ) {
        let npc = L2World.getObjectById( npcObjectId ) as L2Npc
        await aigle.resolve( Helper.getCommandChannelPlayers( playerId ) ).each( async ( playerId: number ) => {
            let state: QuestState = QuestStateCache.getQuestState( playerId, questName, false )

            if ( !state ) {
                return
            }

            let player = L2World.getPlayer( playerId )
            if ( !GeneralHelper.checkIfInRange( range, player, npc, true ) ) {
                return
            }

            return actionMethod( state, player, npc )
        } )
    },

    async giveSingleItemWithAttribute( player: L2PcInstance, itemId: number, amount: number = 1, element: ElementalType, attributeLevel: number, enchantLevel: number = DefaultEnchantLevel ): Promise<void> {
        let item: L2ItemInstance = await player.getInventory().addItem( itemId, amount, player.getTargetId(), 'QuestHelper.giveSingleItem', enchantLevel )

        item.setElementAttribute( element, attributeLevel )

        if ( item.isEquipped() ) {
            item.updateElementAttributesBonus( player )
        }

        if ( enchantLevel > 0 && itemId !== ItemTypes.Adena ) {
            await item.setEnchantLevel( enchantLevel )
        }

        QuestHelper.sendItemGetMessage( player, item, amount )
    },

    processQuestEvent( player: L2PcInstance, npc: L2Npc, name: string, eventName: string ): Promise<void> {
        if ( !npc || !player.isInsideRadius( npc, L2NpcValues.interactionDistance ) ) {
            return
        }

        let eventData = EventPoolCache.getData( EventType.NpcEvent ) as NpcGeneralEvent

        eventData.eventName = eventName
        eventData.characterId = npc.getObjectId()
        eventData.playerId = player.getObjectId()
        eventData.characterNpcId = npc.getId()

        return ListenerCache.sendQuestEvent( name, player.getObjectId(), npc.getObjectId(), EventType.NpcEvent, eventData )
    },

    broadcastEvent( target: L2Object, eventName: string, radius: number, reference: L2Object = null, predicate: ( object: L2Object ) => boolean ) {
        let npcs: Array<L2Object> = L2World.getVisibleObjectsByPredicate( target, radius, predicate )

        let senderId = target.getObjectId()
        let referenceId = reference ? reference.getObjectId() : 0
        return aigle.resolve( npcs ).each( ( npc: L2Npc ): Promise<void> => {
            return QuestHelper.sendEvent( senderId, eventName, npc, referenceId )
        } )
    },

    sendEvent( senderId: number, eventName: string, receiver: L2Character, referenceId: number ): Promise<void> {
        if ( !ListenerCache.hasNpcTemplateListeners( receiver.getId(), EventType.NpcReceivingEvent ) ) {
            return
        }

        let data = EventPoolCache.getData( EventType.NpcReceivingEvent ) as NpcReceivingEvent

        data.senderId = senderId
        data.receiverId = receiver.getObjectId()
        data.eventName = eventName
        data.referenceId = referenceId

        return ListenerCache.sendNpcTemplateEvent( receiver.getId(), EventType.NpcReceivingEvent, data )
    },

    markQuestCompleted( questId: number, playerId: number ) : void {
        let quest: ListenerLogic = ListenerManager.getQuest( questId )
        if ( quest ) {
            quest.getQuestState( playerId, true ).setState( QuestStateValues.COMPLETED )
        }
    }
}

const Helper = {
    getCommandChannelPlayers( playerId: number ): Array<number> {
        let party: L2Party = PlayerGroupCache.getParty( playerId )

        if ( party ) {
            if ( party.isInCommandChannel() ) {
                return party.getCommandChannel().getMembers()
            }

            return party.getMembers()
        }

        return [ playerId ]
    },
}