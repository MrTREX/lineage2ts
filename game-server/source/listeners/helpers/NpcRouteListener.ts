import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import { EventType, NpcRouteNodeArrivedEvent, NpcSpawnEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { NpcSay } from '../../gameService/packets/send/builder/NpcSay'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

export abstract class NpcRouteListener extends ListenerLogic {
    /*
        Maps nodeIndex in walked route to npcStringId.
     */
    abstract stringIdMap : Record<number, number>

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.NpcId,
                eventType: EventType.NpcRouteNodeArrived,
                method: this.onNpcRouteNodeArrived.bind( this ),
                ids: this.getNpcIds(),
                triggers: {
                    targetIds: this.getNodeIndexes()
                }
            },
            {
                registerType: ListenerRegisterType.NpcId,
                eventType: EventType.NpcSpawn,
                method: this.onNpcSpawn.bind( this ),
                ids: this.getNpcIds()
            }
        ]
    }

    private getNodeIndexes(): Set<number> {
        return new Set<number>( Object.keys( this.stringIdMap ).map( value => parseInt( value, 10 ) ) )
    }

    abstract getNpcIds() : Array<number>
    abstract isRunningRoute() : boolean

    onNpcSpawn( data: NpcSpawnEvent ) : void {
        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        if ( !npc ) {
            return
        }

        npc.setIsRunning( this.isRunningRoute(), false )
    }

    broadcastNpcSayId( npc: L2Npc, messageId: number ) : void {
        let packet = NpcSay.fromNpcString( npc.getObjectId(), NpcSayType.NpcAll, npc.getId(), messageId )
        BroadcastHelper.dataInLocation( npc, packet.getBuffer(), npc.getBroadcastRadius() )
    }

    onNpcRouteNodeArrived( data: NpcRouteNodeArrivedEvent ): void {
        this.broadcastNodeMessage( data )
    }

    protected broadcastNodeMessage( data: NpcRouteNodeArrivedEvent ) : void {
        let messageId = this.stringIdMap[ data.nodeIndex ]
        if ( !messageId ) {
            return
        }

        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        this.broadcastNpcSayId( npc, messageId )
    }
}