import { ConfigManager } from '../../config/ConfigManager'
import _ from 'lodash'

export const ListenerHelper = {
    getOnDeathAdjustedAmount( amount: number, isChampion: boolean ): number {
        let adjustedMin = amount * ConfigManager.rates.getDeathDropAmountMinMultiplier()
        let adjustedMax = amount * ConfigManager.rates.getDeathDropAmountMaxMultiplier()

        if ( isChampion && ConfigManager.customs.championEnable() ) {
            adjustedMin *= ConfigManager.customs.getChampionRewardsAmount()
            adjustedMax *= ConfigManager.customs.getChampionRewardsAmount()
        }

        return _.random( Math.floor( adjustedMin ), Math.floor( adjustedMax ) )
    },

    getOnDeathAdjustedChance( chance: number, isFromChampion: boolean ): number {
        let adjustedChance = chance * ConfigManager.rates.getDeathDropChanceMultiplier()

        if ( isFromChampion && ConfigManager.customs.championEnable() ) {
            return adjustedChance * ConfigManager.customs.getChampionRewardsChance()
        }

        return adjustedChance
    },
}