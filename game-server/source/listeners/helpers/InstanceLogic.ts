import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { InstanceWorld } from '../../gameService/models/instancezone/InstanceWorld'
import { ConfigManager } from '../../config/ConfigManager'
import { InstanceManager } from '../../gameService/instancemanager/InstanceManager'
import { Instance } from '../../gameService/models/entity/Instance'
import { InstanceReenterType } from '../../gameService/enums/InstanceReenterType'
import { InstanceReenterDayOfWeek, InstanceReenterTime } from '../../gameService/helpers/InstanceReenterHelper'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { PacketDispatcher } from '../../gameService/PacketDispatcher'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { InstanceRemoveBuffType } from '../../gameService/enums/InstanceRemoveBuffType'
import { L2Summon } from '../../gameService/models/actor/L2Summon'
import { L2Playable } from '../../gameService/models/actor/L2Playable'
import { BuffInfo } from '../../gameService/models/skills/BuffInfo'
import aigle from 'aigle'
import _ from 'lodash'
import moment from 'moment'

export abstract class InstanceLogic extends ListenerLogic {
    finishInstance( world: InstanceWorld, lifeDuration: number = ConfigManager.general.getInstanceFinishTime() * 1000 ): void {
        let instance: Instance = InstanceManager.getInstance( world.getInstanceId() )

        if ( instance.type === InstanceReenterType.ON_INSTANCE_FINISH ) {
            this.handleReenterTime( world, instance )
        }

        if ( lifeDuration === 0 ) {
            InstanceManager.destroyInstance( instance.getId() )
            return
        }

        instance.setDuration( lifeDuration )
        instance.emptyDestroyTime = 0
    }

    handleReenterTime( world: InstanceWorld, instance: Instance ): void {
        let time = 0
        _.each( instance.resetData, ( data: InstanceReenterTime ) => {
            if ( data.time > 0 ) {
                time = Date.now() + data.time

                return false
            }

            let date = moment()
                    .hour( data.hour )
                    .minute( data.minute )
                    .second( 0 )

            if ( date.valueOf() < Date.now() ) {
                date.add( 1, 'day' )
            }

            if ( data.day && data.day !== date.day() ) {
                date.day( data.day === InstanceReenterDayOfWeek.SUNDAY ? 0 : data.day + 1 )
            }

            if ( time === 0 ) {
                time = date.valueOf()
                return
            }

            if ( date.valueOf() < time ) {
                time = date.valueOf()
            }
        } )

        if ( time > 0 ) {
            this.setReenterTime( world, instance, time )
        }
    }

    setReenterTime( world: InstanceWorld, instance: Instance, time: number ) {
        let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.INSTANT_ZONE_FROM_HERE_S1_S_ENTRY_HAS_BEEN_RESTRICTED )
                .addString( instance.name )
                .getBuffer()

        world.allowedObjectIds.forEach( ( objectId: number ) => {
            PacketDispatcher.sendCopyData( objectId, packet )
            InstanceManager.setInstanceTime( objectId, instance.getId(), time )
        } )
    }

    async enterInstance( player: L2PcInstance, templatePath: string, id: number ): Promise<void> {
        let playerWorld: InstanceWorld = InstanceManager.getPlayerWorld( player.getObjectId() )
        if ( playerWorld ) {
            if ( playerWorld.getTemplateId() === id ) {
                await this.onEnterInstance( player, playerWorld, false )

                let currentInstance: Instance = InstanceManager.getInstance( playerWorld.getInstanceId() )
                if ( currentInstance.shouldRemoveBuffs() ) {
                    return this.handleRemoveBuffs( player, currentInstance )
                }

                return
            }

            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_HAVE_ENTERED_ANOTHER_INSTANT_ZONE_THEREFORE_YOU_CANNOT_ENTER_CORRESPONDING_DUNGEON ) )
            return
        }

        if ( !this.canEnterPerConditions( player, id ) ) {
            return
        }

        let world = new InstanceWorld()
        world.instanceId = InstanceManager.createDynamicInstance( templatePath )
        world.templateId = id
        world.status = 0
        InstanceManager.addWorld( world )

        await this.onEnterInstance( player, world, true )

        let createdInstance: Instance = InstanceManager.getInstance( world.getInstanceId() )
        if ( createdInstance.type === InstanceReenterType.ON_INSTANCE_ENTER ) {
            this.handleReenterTime( world, createdInstance )
        }

        if ( createdInstance.shouldRemoveBuffs() ) {
            return this.handleRemoveBuffs( player, createdInstance )
        }
    }

    abstract onEnterInstance( player: L2PcInstance, world: InstanceWorld, isEnteringFirstTime: boolean ): Promise<void>

    async onRemoveWhitelistBuffs( playable: L2Playable, instance: Instance, isBlacklisted: boolean ): Promise<void> {
        if ( !playable ) {
            return
        }

        await aigle.resolve( playable.getEffectList().getBuffs() ).each( ( info: BuffInfo ) => {
            if ( instance.exceptionList.includes( info.getSkill().getId() ) === isBlacklisted ) {
                return info.getEffector().getEffectList().stopSkillEffects( true, info.getSkill().getId() )
            }
        } )
    }

    async handleRemoveBuffs( player: L2PcInstance, currentInstance: Instance ) : Promise<void> {
        switch ( currentInstance.removeBuffType ) {
            case InstanceRemoveBuffType.ALL:
                let summon: L2Summon = player.getSummon()
                if ( summon ) {
                    await summon.stopAllEffectsExceptThoseThatLastThroughDeath()
                }

                return player.stopAllEffectsExceptThoseThatLastThroughDeath()

            case InstanceRemoveBuffType.WHITELIST:
                await this.onRemoveWhitelistBuffs( player, currentInstance, false )
                return this.onRemoveWhitelistBuffs( player.getSummon(), currentInstance, false )

            case InstanceRemoveBuffType.BLACKLIST:
                await this.onRemoveWhitelistBuffs( player, currentInstance, true )
                return this.onRemoveWhitelistBuffs( player.getSummon(), currentInstance, true )
        }
    }

    canEnterPerConditions( player: L2PcInstance, id: number = 0 ): boolean {
        return true
    }
}