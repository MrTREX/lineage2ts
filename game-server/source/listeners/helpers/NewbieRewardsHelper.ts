import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'
import { QuestVariables } from './QuestVariables'
import { QuestHelper } from './QuestHelper'
import { Voice } from '../../gameService/enums/audio/Voice'
import { ExShowScreenMessage } from '../../gameService/packets/send/ExShowScreenMessage'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { ItemData } from '../../gameService/interface/ItemData'
import { PacketHelper } from '../../gameService/packets/PacketVariables'
import _ from 'lodash'

const noGradeSoulshots : ItemData = {
    itemId: 5789,
    amount: 6000
}

const noGradeSpiritshots : ItemData = {
    itemId: 5790,
    amount: 3000
}

const messageData: Buffer = PacketHelper.preservePacket( ExShowScreenMessage.fromNpcMessageId( NpcStringIds.ACQUISITION_OF_SOULSHOT_FOR_BEGINNERS_COMPLETE_N_GO_FIND_THE_NEWBIE_GUIDE, 2, 5000, null ) )


export const enum NewbieItemIds {
    EchoCrystalThemeOfBattle = 4412,
    EchoCrystalThemeOfLove = 4413,
    EchoCrystalThemeOfSolitude = 4414,
    EchoCrystalThemeOfFeast = 4415,
    EchoCrystalThemeOfCelebration = 4416,
    SoulshotNoGrade = 1835,
    SpiritshotNoGrade = 2509,
    LesserHealingPotion = 1060,
}

export const EchoCrystalRewards : Array<NewbieItemIds> = [
    NewbieItemIds.EchoCrystalThemeOfBattle,
    NewbieItemIds.EchoCrystalThemeOfLove,
    NewbieItemIds.EchoCrystalThemeOfSolitude,
    NewbieItemIds.EchoCrystalThemeOfFeast,
    NewbieItemIds.EchoCrystalThemeOfCelebration
]

export const NewbieRewardsHelper = {
    async giveNewbieReward( player: L2PcInstance ): Promise<void> {
        let rewards: any = PlayerVariablesManager.get( player.getObjectId(), QuestVariables.newbieRewards )
        if ( player.getLevel() < 25 && !_.get( rewards, 'shots' ) ) {
            if ( player.isMageClass() ) {
                await QuestHelper.rewardSingleItem( player, noGradeSpiritshots.itemId, noGradeSpiritshots.amount )
                player.sendCopyData( Voice.TUTORIAL_VOICE_027_1000 )
            } else {
                await QuestHelper.rewardSingleItem( player, noGradeSoulshots.itemId, noGradeSoulshots.amount )
                player.sendCopyData( Voice.TUTORIAL_VOICE_026_1000 )
            }

            rewards.shots = true
            PlayerVariablesManager.set( player.getObjectId(), QuestVariables.newbieRewards, rewards )
        }

        let mission: number = PlayerVariablesManager.get( player.getObjectId(), QuestVariables.guideMission ) as number
        if ( !mission ) {
            PlayerVariablesManager.set( player.getObjectId(), QuestVariables.guideMission, 1000 )
            player.sendCopyData( messageData )
            return
        }

        if ( Math.floor( ( mission % 10000 ) / 1000 ) !== 1 ) {
            PlayerVariablesManager.set( player.getObjectId(), QuestVariables.guideMission, mission + 1000 )
            player.sendCopyData( messageData )
        }
    }
}