export const TerminatedResultHelper = {
    defaultSuccess: {
        message: undefined,
        priority: 0,
        terminate: true,
    },

    defaultFailure: {
        message: undefined,
        priority: 0,
        terminate: false,
    },
}