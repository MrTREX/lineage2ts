import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import { EventTerminationResult, EventType, NpcBypassEvent } from '../../gameService/models/events/EventType'
import _ from 'lodash'
import { SevenSignsSeal, SevenSignsSide, SevenSignsValues } from '../../gameService/values/SevenSignsValues'
import { InstanceType } from '../../gameService/enums/InstanceType'
import { ClassId } from '../../gameService/models/base/ClassId'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { SevenSigns } from '../../gameService/directives/SevenSigns'
import { ConfigManager } from '../../config/ConfigManager'
import { NpcHtmlMessage } from '../../gameService/packets/send/NpcHtmlMessage'
import { TerminatedResultHelper } from '../helpers/TerminatedResultHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2ItemInstance } from '../../gameService/models/items/instance/L2ItemInstance'
import { DataManager } from '../../data/manager'
import { L2World } from '../../gameService/L2World'
import { L2SignsPriestInstance } from '../../gameService/models/actor/instance/L2SignsPriestInstance'

const npcIds: Array<number> = [
    31078,
    31079,
    31080,
    31081,
    31082,
    31083,
    31084,
    31168,
    31692,
    31694,
    31997,
    31085,
    31086,
    31087,
    31088,
    31089,
    31090,
    31091,
    31169,
    31693,
    31695,
    31998,
    31092,
    31113,
    31126,
]

const defaultResult: EventTerminationResult = TerminatedResultHelper.defaultSuccess
const continueResult: EventTerminationResult = TerminatedResultHelper.defaultFailure

export class SevenSignsPriestBypass extends ListenerLogic {
    constructor() {
        super( 'SevenSignsPriestBypass', 'listeners/bypass/SevenSignsPriest.ts' )
    }

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.NpcId,
                eventType: EventType.NpcBypass,
                method: this.onNpcBypass.bind( this ),
                ids: npcIds,
            },
        ]
    }

    async onNpcBypass( data: NpcBypassEvent ): Promise<EventTerminationResult> {
        let commandChunks: Array<string> = data.command.split( ' ' )
        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.originatorId ) as L2SignsPriestInstance

        switch ( commandChunks[ 0 ] ) {
            case 'SevenSignsDesc':
                this.prepareHtml( player, data.command.substring( 15 ), null, true, npc )
                return defaultResult

            case 'SevenSigns':
                await this.onSevenSigns( player, commandChunks, data, npc )
                return defaultResult

            case 'Chat':
                npc.showChatWindow( player )
                return defaultResult
        }

        return continueResult
    }

    prepareHtml( player: L2PcInstance, path: string, pathSuffix: string, type: boolean, npc: L2SignsPriestInstance ): void {
        let typeValue: string = type ? 'desc_' : 'signs_'
        let suffixValue: string = pathSuffix ? `_${ pathSuffix }` : ''
        let fullPath: string = `${ SevenSignsValues.SEVEN_SIGNS_HTML_PATH }${ typeValue }${ path }${ suffixValue }.htm`

        npc.showChatWindowWithPath( player, fullPath )
    }

    async processContributionCount( command: string, player: L2PcInstance, isDawnPriest: boolean, npc: L2SignsPriestInstance ): Promise<void> {
        let contributionCount = _.parseInt( command.substring( 19 ).trim() )

        if ( !contributionCount || contributionCount === 0 ) {
            if ( isDawnPriest ) {
                return this.prepareHtml( player, '6', 'dawn_failure', false, npc )
            }

            return this.prepareHtml( player, '6', 'dusk_failure', false, npc )
        }

        let contribStonesFound = false

        let redContrib = 0
        let greenContrib = 0
        let blueContrib = 0

        let contribStoneId = _.parseInt( command.substring( 14, 18 ) )

        let contribBlueStones: L2ItemInstance = player.getInventory().getItemByItemId( SevenSignsValues.SEAL_STONE_BLUE_ID )
        let contribGreenStones: L2ItemInstance = player.getInventory().getItemByItemId( SevenSignsValues.SEAL_STONE_GREEN_ID )
        let contribRedStones: L2ItemInstance = player.getInventory().getItemByItemId( SevenSignsValues.SEAL_STONE_RED_ID )

        let contribBlueStoneCount = contribBlueStones ? contribBlueStones.getCount() : 0
        let contribGreenStoneCount = contribGreenStones ? contribGreenStones.getCount() : 0
        let contribRedStoneCount = contribRedStones ? contribRedStones.getCount() : 0

        let score = SevenSigns.getPlayerContributionScore( player.getObjectId() )

        let maximumContribution = ConfigManager.sevenSigns.getMaxPlayerContribution() - score
        switch ( contribStoneId ) {
            case SevenSignsValues.SEAL_STONE_BLUE_ID:
                blueContrib = Math.min( Math.floor( maximumContribution / SevenSignsValues.BLUE_CONTRIB_POINTS ), contribBlueStoneCount )
                break

            case SevenSignsValues.SEAL_STONE_GREEN_ID:
                greenContrib = Math.min( Math.floor( maximumContribution / SevenSignsValues.GREEN_CONTRIB_POINTS ), contribGreenStoneCount )
                break

            case SevenSignsValues.SEAL_STONE_RED_ID:
                redContrib = Math.min( Math.floor( maximumContribution / SevenSignsValues.RED_CONTRIB_POINTS ), contribRedStoneCount )
                break
        }

        if ( redContrib > 0 && await player.destroyItemByItemId( SevenSignsValues.SEAL_STONE_RED_ID, redContrib, false, 'SevenSigns' ) ) {
            contribStonesFound = true

            let message = new SystemMessageBuilder( SystemMessageIds.S2_S1_DISAPPEARED )
                    .addItemNameWithId( SevenSignsValues.SEAL_STONE_RED_ID )
                    .addNumber( redContrib )
                    .getBuffer()
            player.sendOwnedData( message )
        }

        if ( greenContrib > 0 && await player.destroyItemByItemId( SevenSignsValues.SEAL_STONE_GREEN_ID, greenContrib, false, 'SevenSigns' ) ) {
            contribStonesFound = true

            let message = new SystemMessageBuilder( SystemMessageIds.S2_S1_DISAPPEARED )
                    .addItemNameWithId( SevenSignsValues.SEAL_STONE_GREEN_ID )
                    .addNumber( greenContrib )
                    .getBuffer()
            player.sendOwnedData( message )
        }


        if ( blueContrib > 0 && await player.destroyItemByItemId( SevenSignsValues.SEAL_STONE_BLUE_ID, blueContrib, false, 'SevenSigns' ) ) {
            contribStonesFound = true

            let message = new SystemMessageBuilder( SystemMessageIds.S2_S1_DISAPPEARED )
                    .addItemNameWithId( SevenSignsValues.SEAL_STONE_BLUE_ID )
                    .addNumber( blueContrib )
                    .getBuffer()
            player.sendOwnedData( message )
        }

        if ( !contribStonesFound ) {
            if ( isDawnPriest ) {
                return this.prepareHtml( player, '6', 'dawn_low_stones', false, npc )
            }

            return this.prepareHtml( player, '6', 'dusk_low_stones', false, npc )
        }

        score = SevenSigns.addPlayerStoneContribution( player.getObjectId(), blueContrib, greenContrib, redContrib )
        let totalScoreContributedMessage = new SystemMessageBuilder( SystemMessageIds.CONTRIB_SCORE_INCREASED_S1 )
                .addNumber( score )
                .getBuffer()
        player.sendOwnedData( totalScoreContributedMessage )

        if ( isDawnPriest ) {
            return this.prepareHtml( player, '6', 'dawn', false, npc )
        }

        return this.prepareHtml( player, '6', 'dusk', false, npc )
    }

    async processParticularStoneContribution( command: string, player: L2PcInstance, isDawnPriest: boolean, value: string, npc: L2SignsPriestInstance ): Promise<void> {
        let stoneType = _.parseInt( command.substring( 13 ) )

        let blueStones: L2ItemInstance = player.getInventory().getItemByItemId( SevenSignsValues.SEAL_STONE_BLUE_ID )
        let greenStones: L2ItemInstance = player.getInventory().getItemByItemId( SevenSignsValues.SEAL_STONE_GREEN_ID )
        let redStones: L2ItemInstance = player.getInventory().getItemByItemId( SevenSignsValues.SEAL_STONE_RED_ID )

        let blueStoneCount = blueStones ? blueStones.getCount() : 0
        let greenStoneCount = greenStones ? greenStones.getCount() : 0
        let redStoneCount = redStones ? redStones.getCount() : 0

        let contribScore = SevenSigns.getPlayerStoneContribution( player.getObjectId() )
        let stonesFound = false

        if ( contribScore >= ConfigManager.sevenSigns.getMaxPlayerContribution() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CONTRIB_SCORE_EXCEEDED ) )
            return
        }

        let redContribCount: number
        let greenContribCount: number
        let blueContribCount: number

        let contribStoneColor: string
        let stoneColorContr: string

        let stoneCountContr: number = 0
        let stoneIdContr: number = 0

        switch ( stoneType ) {
            case 1:
                contribStoneColor = 'Blue'
                stoneColorContr = 'blue'
                stoneIdContr = SevenSignsValues.SEAL_STONE_BLUE_ID
                stoneCountContr = blueStoneCount
                break

            case 2:
                contribStoneColor = 'Green'
                stoneColorContr = 'green'
                stoneIdContr = SevenSignsValues.SEAL_STONE_GREEN_ID
                stoneCountContr = greenStoneCount
                break

            case 3:
                contribStoneColor = 'Red'
                stoneColorContr = 'red'
                stoneIdContr = SevenSignsValues.SEAL_STONE_RED_ID
                stoneCountContr = redStoneCount
                break

            case 4:
                let tempContribScore = contribScore
                redContribCount = ( ConfigManager.sevenSigns.getMaxPlayerContribution() - tempContribScore ) / SevenSignsValues.RED_CONTRIB_POINTS
                if ( redContribCount > redStoneCount ) {
                    redContribCount = redStoneCount
                }
                tempContribScore += redContribCount * SevenSignsValues.RED_CONTRIB_POINTS
                greenContribCount = ( ConfigManager.sevenSigns.getMaxPlayerContribution() - tempContribScore ) / SevenSignsValues.GREEN_CONTRIB_POINTS
                if ( greenContribCount > greenStoneCount ) {
                    greenContribCount = greenStoneCount
                }
                tempContribScore += greenContribCount * SevenSignsValues.GREEN_CONTRIB_POINTS
                blueContribCount = ( ConfigManager.sevenSigns.getMaxPlayerContribution() - tempContribScore ) / SevenSignsValues.BLUE_CONTRIB_POINTS
                if ( blueContribCount > blueStoneCount ) {
                    blueContribCount = blueStoneCount
                }
                if ( redContribCount > 0 ) {
                    if ( await player.destroyItemByItemId( SevenSignsValues.SEAL_STONE_RED_ID, redContribCount, false, 'SevenSigns' ) ) {
                        stonesFound = true

                        let message = new SystemMessageBuilder( SystemMessageIds.S2_S1_DISAPPEARED )
                                .addItemNameWithId( SevenSignsValues.SEAL_STONE_RED_ID )
                                .addNumber( redContribCount )
                                .getBuffer()
                        player.sendOwnedData( message )
                    }
                }

                if ( greenContribCount > 0 ) {
                    if ( await player.destroyItemByItemId( SevenSignsValues.SEAL_STONE_GREEN_ID, greenContribCount, false, 'SevenSigns' ) ) {
                        stonesFound = true

                        let message = new SystemMessageBuilder( SystemMessageIds.S2_S1_DISAPPEARED )
                                .addItemNameWithId( SevenSignsValues.SEAL_STONE_GREEN_ID )
                                .addNumber( greenContribCount )
                                .getBuffer()
                        player.sendOwnedData( message )
                    }
                }

                if ( blueContribCount > 0 ) {
                    if ( await player.destroyItemByItemId( SevenSignsValues.SEAL_STONE_BLUE_ID, blueContribCount, false, 'SevenSigns' ) ) {
                        stonesFound = true

                        let message = new SystemMessageBuilder( SystemMessageIds.S2_S1_DISAPPEARED )
                                .addItemNameWithId( SevenSignsValues.SEAL_STONE_BLUE_ID )
                                .addNumber( blueContribCount )
                                .getBuffer()
                        player.sendOwnedData( message )
                    }
                }

                if ( !stonesFound ) {
                    if ( isDawnPriest ) {
                        return this.prepareHtml( player, value, 'dawn_no_stones', false, npc )
                    }

                    return this.prepareHtml( player, value, 'dusk_no_stones', false, npc )
                }

                contribScore = SevenSigns.addPlayerStoneContribution( player.getObjectId(), blueContribCount, greenContribCount, redContribCount )

                let message = new SystemMessageBuilder( SystemMessageIds.CONTRIB_SCORE_INCREASED_S1 )
                        .addNumber( contribScore )
                        .getBuffer()
                player.sendOwnedData( message )

                if ( isDawnPriest ) {
                    return this.prepareHtml( player, '6', 'dawn', false, npc )
                }

                return this.prepareHtml( player, '6', 'dusk', false, npc )
        }

        let path: string = `${ SevenSignsValues.SEVEN_SIGNS_HTML_PATH }${ isDawnPriest ? 'signs_6_dawn_contribute.htm' : 'signs_6_dusk_contribute.htm' }`
        let html: string = DataManager.getHtmlData().getItem( path )
                                      .replace( /%contribStoneColor%/g, contribStoneColor )
                                      .replace( /%stoneColor%/g, stoneColorContr )
                                      .replace( /%stoneCount%/g, stoneCountContr.toString() )
                                      .replace( /%stoneItemId%/g, stoneIdContr.toString() )

        player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId(), npc.getObjectId() ) )
    }

    async processStonesToAncientAdena( command: string, player: L2PcInstance, isDawnPriest: boolean, npc: L2SignsPriestInstance ): Promise<void> {
        let stoneType = _.parseInt( command.substring( 14 ) )

        let stoneId = 0
        let stoneCount = 0
        let stoneValue = 0

        let stoneColor: string

        switch ( stoneType ) {
            case 1:
                stoneColor = 'blue'
                stoneId = SevenSignsValues.SEAL_STONE_BLUE_ID
                stoneValue = SevenSignsValues.SEAL_STONE_BLUE_VALUE
                break

            case 2:
                stoneColor = 'green'
                stoneId = SevenSignsValues.SEAL_STONE_GREEN_ID
                stoneValue = SevenSignsValues.SEAL_STONE_GREEN_VALUE
                break

            case 3:
                stoneColor = 'red'
                stoneId = SevenSignsValues.SEAL_STONE_RED_ID
                stoneValue = SevenSignsValues.SEAL_STONE_RED_VALUE
                break

            case 4:
                let blueStonesAll: L2ItemInstance = player.getInventory().getItemByItemId( SevenSignsValues.SEAL_STONE_BLUE_ID )
                let greenStonesAll: L2ItemInstance = player.getInventory().getItemByItemId( SevenSignsValues.SEAL_STONE_GREEN_ID )
                let redStonesAll: L2ItemInstance = player.getInventory().getItemByItemId( SevenSignsValues.SEAL_STONE_RED_ID )

                let blueStoneCountAll = blueStonesAll ? blueStonesAll.getCount() : 0
                let greenStoneCountAll = greenStonesAll ? greenStonesAll.getCount() : 0
                let redStoneCountAll = redStonesAll ? redStonesAll.getCount() : 0

                let ancientAdenaRewardAll = SevenSigns.calculateAncientAdenaReward( blueStoneCountAll, greenStoneCountAll, redStoneCountAll )
                if ( ancientAdenaRewardAll === 0 ) {
                    if ( isDawnPriest ) {
                        return this.prepareHtml( player, '18', 'dawn_no_stones', false, npc )
                    }

                    return this.prepareHtml( player, '18', 'dusk_no_stones', false, npc )
                }

                if ( blueStoneCountAll > 0 ) {
                    await player.destroyItemByItemId( SevenSignsValues.SEAL_STONE_BLUE_ID, blueStoneCountAll, true, 'SevenSigns' )
                }
                if ( greenStoneCountAll > 0 ) {
                    await player.destroyItemByItemId( SevenSignsValues.SEAL_STONE_GREEN_ID, greenStoneCountAll, true )
                }
                if ( redStoneCountAll > 0 ) {
                    await player.destroyItemByItemId( SevenSignsValues.SEAL_STONE_RED_ID, redStoneCountAll, true )
                }

                await player.addAncientAdena( ancientAdenaRewardAll, true, 'SevenSigns', npc.getObjectId() )

                if ( isDawnPriest ) {
                    return this.prepareHtml( player, '18', 'dawn', false, npc )
                }

                return this.prepareHtml( player, '18', 'dusk', false, npc )
        }

        let stoneInstance: L2ItemInstance = player.getInventory().getItemByItemId( stoneId )

        if ( stoneInstance ) {
            stoneCount = stoneInstance.getCount()
        }

        let path: string = `${ SevenSignsValues.SEVEN_SIGNS_HTML_PATH }${ isDawnPriest ? 'signs_17_dawn.htm' : 'signs_17_dusk.htm' }`

        let html: string = DataManager.getHtmlData().getItem( path )
                                      .replace( /%stoneColor%/g, stoneColor )
                                      .replace( /%stoneValue%/g, stoneValue.toString() )
                                      .replace( /%stoneCount%/g, stoneCount.toString() )
                                      .replace( /%stoneItemId%/g, stoneId.toString() )

        player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId(), npc.getObjectId() ) )
    }

    async processExchangeStonesToAncientAdena( command: string, player: L2PcInstance, isDawnPriest: boolean, npc: L2SignsPriestInstance ): Promise<void> {
        let convertStoneId = _.parseInt( command.substring( 14, 18 ) )
        let convertCount = _.parseInt( command.substring( 19 ).trim() )

        if ( !convertCount || convertCount === 0 ) {
            if ( isDawnPriest ) {
                return this.prepareHtml( player, '18', 'dawn_failed', false, npc )
            }

            return this.prepareHtml( player, '18', 'dusk_failed', false, npc )
        }

        let convertItem: L2ItemInstance = player.getInventory().getItemByItemId( convertStoneId )

        if ( convertItem ) {
            let ancientAdenaReward = 0
            let totalCount = convertItem.getCount()

            if ( convertCount <= totalCount && convertCount > 0 ) {
                switch ( convertStoneId ) {
                    case SevenSignsValues.SEAL_STONE_BLUE_ID:
                        ancientAdenaReward = SevenSigns.calculateAncientAdenaReward( convertCount, 0, 0 )
                        break

                    case SevenSignsValues.SEAL_STONE_GREEN_ID:
                        ancientAdenaReward = SevenSigns.calculateAncientAdenaReward( 0, convertCount, 0 )
                        break

                    case SevenSignsValues.SEAL_STONE_RED_ID:
                        ancientAdenaReward = SevenSigns.calculateAncientAdenaReward( 0, 0, convertCount )
                        break
                }

                if ( await player.destroyItemByItemId( convertStoneId, convertCount, true, 'SevenSigns' ) ) {
                    await player.addAncientAdena( ancientAdenaReward, true, 'SevenSigns', npc.getObjectId() )

                    if ( isDawnPriest ) {
                        return this.prepareHtml( player, '18', 'dawn', false, npc )
                    }

                    return this.prepareHtml( player, '18', 'dusk', false, npc )
                }
            }

            if ( isDawnPriest ) {
                return this.prepareHtml( player, '18', 'dawn_low_stones', false, npc )
            }

            return this.prepareHtml( player, '18', 'dusk_low_stones', false, npc )
        }

        if ( isDawnPriest ) {
            return this.prepareHtml( player, '18', 'dawn_no_stones', false, npc )
        }

        return this.prepareHtml( player, '18', 'dusk_no_stones', false, npc )
    }

    async onSevenSigns( player: L2PcInstance, commandChunks: Array<string>, data: NpcBypassEvent, npc: L2SignsPriestInstance ): Promise<void> {
        if ( commandChunks.length < 3 ) {
            return
        }

        let operation = _.parseInt( commandChunks[ 1 ] )
        let cabal = _.defaultTo( _.parseInt( commandChunks[ 2 ] ), SevenSignsSide.None )
        let isDawnPriest: boolean = npc.isInstanceType( InstanceType.L2DawnPriestInstance )
        let participantLevel: number = ClassId.getLevelByClassId( player.getClassId() )

        switch ( operation ) {
            case 2: // Purchase Record of the Seven Signs
                if ( !player.getInventory().validateCapacity( 1 ) ) {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SLOTS_FULL ) )
                    return
                }

                if ( !await player.reduceAdena( SevenSignsValues.RECORD_SEVEN_SIGNS_COST, true ) ) {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ADENA ) )
                    return
                }

                await player.getInventory().addItem( SevenSignsValues.RECORD_SEVEN_SIGNS_ID, 1, npc.getObjectId(), 'SevenSigns' )

                let purchaseMessage = new SystemMessageBuilder( SystemMessageIds.EARNED_ITEM_S1 )
                        .addItemNameWithId( SevenSignsValues.RECORD_SEVEN_SIGNS_ID )
                        .getBuffer()
                player.sendOwnedData( purchaseMessage )

                if ( isDawnPriest ) {
                    return this.prepareHtml( player, commandChunks[ 1 ], 'dawn', false, npc )
                }

                return this.prepareHtml( player, commandChunks[ 1 ], 'dusk', false, npc )

            case 33: // "I want to participate" request
                let oldCabal = SevenSigns.getPlayerSide( player.getObjectId() )

                if ( oldCabal !== SevenSignsSide.None ) {
                    if ( isDawnPriest ) {
                        return this.prepareHtml( player, commandChunks[ 1 ], 'dawn_member', false, npc )
                    }

                    return this.prepareHtml( player, commandChunks[ 1 ], 'dusk_member', false, npc )
                }

                if ( participantLevel === 0 ) {
                    if ( isDawnPriest ) {
                        return this.prepareHtml( player, commandChunks[ 1 ], 'dawn_firstclass', false, npc )
                    }

                    return this.prepareHtml( player, commandChunks[ 1 ], 'dusk_firstclass', false, npc )
                }

                if ( cabal === SevenSignsSide.Dusk
                        && ConfigManager.sevenSigns.castleForDusk()
                        && player.getClan() && player.getClan().getCastleId() > 0 ) {
                    return npc.showChatWindowWithPath( player, `${ SevenSignsValues.SEVEN_SIGNS_HTML_PATH }signs_33_dusk_no.htm` )
                }

                if ( cabal === SevenSignsSide.Dawn
                        && ConfigManager.sevenSigns.castleForDawn()
                        && ( !player.getClan() || player.getClan().getCastleId() === 0 ) ) {
                    return npc.showChatWindowWithPath( player, `${ SevenSignsValues.SEVEN_SIGNS_HTML_PATH }signs_33_dawn_fee.htm` )
                }

                if ( isDawnPriest ) {
                    return this.prepareHtml( player, commandChunks[ 1 ], 'dawn', false, npc )
                }

                return this.prepareHtml( player, commandChunks[ 1 ], 'dusk', false, npc )

            case 34: // Pay the participation fee request
                if ( participantLevel > 0
                        && ( player.getAdena() >= ConfigManager.sevenSigns.getSevenSignsJoinDawnFee()
                                || player.getInventory().getInventoryItemCount( ConfigManager.sevenSigns.getSevenSignsManorsAgreementId(), -1 ) > 0 ) ) {
                    return npc.showChatWindowWithPath( player, `${ SevenSignsValues.SEVEN_SIGNS_HTML_PATH }signs_33_dawn.htm` )
                }

                return npc.showChatWindowWithPath( player, `${ SevenSignsValues.SEVEN_SIGNS_HTML_PATH }signs_33_dawn_no.htm` )

            case 3: // Join Cabal Intro 1
            case 8: // Festival of Darkness Intro - SevenSigns x [0]1
                return this.prepareHtml( player, commandChunks[ 1 ], SevenSigns.getSideShortName( cabal ), false, npc )

            case 4: // Join a Cabal - SevenSigns 4 [0]1 x
                let newSeal = _.parseInt( data.command.substring( 15 ) ) as SevenSignsSeal

                if ( !newSeal ) {
                    return
                }

                if ( participantLevel >= 1 ) {
                    if ( cabal === SevenSignsSide.Dusk
                            && ConfigManager.sevenSigns.castleForDusk()
                            && player.getClan() && player.getClan().getCastleId() > 0 ) {
                        return npc.showChatWindowWithPath( player, `${ SevenSignsValues.SEVEN_SIGNS_HTML_PATH }signs_33_dusk_no.htm` )
                    }

                    // If the player is trying to join the Lords of Dawn, check if they are carrying a Lord's certificate. If not then try to take the required amount of adena instead.
                    if ( ConfigManager.sevenSigns.castleForDawn() && cabal === SevenSignsSide.Dawn ) {
                        let allowJoinDawn = ( player.getClan() && player.getClan().getCastleId() > 0 )
                                || await player.destroyItemByItemId( ConfigManager.sevenSigns.getSevenSignsManorsAgreementId(), 1, true, 'SevenSigns' )
                                || await player.reduceAdena( ConfigManager.sevenSigns.getSevenSignsJoinDawnFee(), true, 'SevenSigns' )

                        if ( !allowJoinDawn ) {
                            return npc.showChatWindowWithPath( player, `${ SevenSignsValues.SEVEN_SIGNS_HTML_PATH }signs_33_dawn_fee.htm` )
                        }
                    }
                }

                SevenSigns.updatePlayerInformation( player.getObjectId(), cabal, newSeal )

                if ( cabal === SevenSignsSide.Dawn ) {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SEVENSIGNS_PARTECIPATION_DAWN ) )
                } else {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SEVENSIGNS_PARTECIPATION_DUSK ) )
                }

                switch ( newSeal ) {
                    case SevenSignsSeal.Avarice:
                        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.FIGHT_FOR_AVARICE ) )
                        break

                    case SevenSignsSeal.Gnosis:
                        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.FIGHT_FOR_GNOSIS ) )
                        break

                    case SevenSignsSeal.Strife:
                        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.FIGHT_FOR_STRIFE ) )
                        break
                }

                return this.prepareHtml( player, '4', SevenSigns.getSideShortName( cabal ), false, npc )

            case 5:
                if ( isDawnPriest ) {
                    if ( SevenSigns.getPlayerSide( player.getObjectId() ) === SevenSignsSide.None ) {
                        return this.prepareHtml( player, commandChunks[ 1 ], 'dawn_no', false, npc )
                    }

                    return this.prepareHtml( player, commandChunks[ 1 ], 'dawn', false, npc )
                }

                if ( SevenSigns.getPlayerSide( player.getObjectId() ) === SevenSignsSide.None ) {
                    return this.prepareHtml( player, commandChunks[ 1 ], 'dusk_no', false, npc )
                }

                return this.prepareHtml( player, commandChunks[ 1 ], 'dusk', false, npc )

            case 21:
                return this.processContributionCount( data.command, player, isDawnPriest, npc )

            case 6:
                return this.processParticularStoneContribution( data.command, player, isDawnPriest, commandChunks[ 1 ], npc )

            case 7: // Exchange Ancient Adena for Adena - SevenSigns 7 xxxxxxx
                let ancientAdenaConvert = _.parseInt( data.command.substring( 13 ).trim() )

                if ( !ancientAdenaConvert || ancientAdenaConvert <= 0 ) {
                    return npc.showChatWindowWithPath( player, `${ SevenSignsValues.SEVEN_SIGNS_HTML_PATH }blkmrkt_3.htm` )
                }

                if ( player.getAncientAdena() < ancientAdenaConvert ) {
                    return npc.showChatWindowWithPath( player, `${ SevenSignsValues.SEVEN_SIGNS_HTML_PATH }blkmrkt_4.htm` )
                }

                await player.reduceAncientAdena( ancientAdenaConvert, true, 'SevenSigns' )
                await player.addAdena( ancientAdenaConvert, 'SevenSigns', npc.getObjectId(), true )

                return npc.showChatWindowWithPath( player, `${ SevenSignsValues.SEVEN_SIGNS_HTML_PATH }blkmrkt_5.htm` )

            case 9: // Receive Contribution Rewards
                let playerSide = SevenSigns.getPlayerSide( player.getObjectId() )
                let winningCabal = SevenSigns.getWinnerSide()

                if ( SevenSigns.isSealValidationPeriod() && playerSide === winningCabal ) {
                    let ancientAdenaReward = SevenSigns.getAncientAdenaReward( player.getObjectId(), true )

                    if ( ancientAdenaReward < 3 ) {
                        if ( isDawnPriest ) {
                            return this.prepareHtml( player, '9', 'dawn_b', false, npc )
                        }

                        return this.prepareHtml( player, '9', 'dusk_b', false, npc )
                    }

                    await player.addAncientAdena( ancientAdenaReward, true, 'SevenSigns', npc.getObjectId() )

                    if ( isDawnPriest ) {
                        return this.prepareHtml( player, '9', 'dawn_a', false, npc )
                    }

                    return this.prepareHtml( player, '9', 'dusk_a', false, npc )
                }

                return

            case 11: // Teleport to Hunting Grounds
                let [ xValue, yValue, zValue, costValue ] = _.split( data.command.substring( 14 ).trim(), ' ' )

                let ancientAdenaCost = _.parseInt( costValue )
                if ( ancientAdenaCost > 0 && await player.reduceAncientAdena( ancientAdenaCost, true, 'SevenSigns' ) ) {
                    return player.teleportToLocationCoordinates( _.parseInt( xValue ), _.parseInt( yValue ), _.parseInt( zValue ) )
                }

                return

            case 16:
                if ( isDawnPriest ) {
                    return this.prepareHtml( player, commandChunks[ 1 ], 'dawn', false, npc )
                }

                return this.prepareHtml( player, commandChunks[ 1 ], 'dusk', false, npc )

            case 17: // Exchange Seal Stones for Ancient Adena (Type Choice) - SevenSigns 17 x
                return this.processStonesToAncientAdena( data.command, player, isDawnPriest, npc )

            case 18: // Exchange Seal Stones for Ancient Adena - SevenSigns 18 xxxx xxxxxx
                return this.processExchangeStonesToAncientAdena( data.command, player, isDawnPriest, npc )

            case 19: // Seal Information (for when joining a cabal)
                let chosenSeal: number = _.parseInt( data.command.substring( 16 ) )
                let sealSuffix = `${ SevenSigns.getSealName( chosenSeal, true ) }_${ SevenSigns.getSideShortName( cabal ) }`
                return this.prepareHtml( player, commandChunks[ 1 ], sealSuffix, false, npc )

            case 20: // Seal Status (for when joining a cabal)
                let html: string
                if ( isDawnPriest ) {
                    html = '<html><body>Priest of Dawn:<br><font color="LEVEL">[ Seal Status ]</font><br>'
                } else {
                    html = '<html><body>Dusk Priestess:<br><font color="LEVEL">[ Status of the Seals ]</font><br>'
                }

                _.times( 4, ( index: number ) => {
                    let sealOwner = SevenSigns.getWinningSealSide( index )
                    let name = sealOwner !== SevenSignsSide.None ? SevenSigns.getSideName( sealOwner ) : 'Absent'

                    html += `[${ SevenSigns.getSealName( index, false ) }: ${ name }]<br>`
                } )

                html += '<a action="bypass -h Chat 0">Go back.</a></body></html>'
                player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId(), npc.getObjectId() ) )

                return

            default:
                return this.prepareHtml( player, commandChunks[ 1 ], null, false, npc )
        }
    }
}