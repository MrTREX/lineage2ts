import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import { EventTerminationResult, EventType, NpcBypassEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { L2DoormenInstance } from '../../gameService/models/actor/instance/L2DoormenInstance'
import { TerminatedResultHelper } from '../helpers/TerminatedResultHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { ActionFailed } from '../../gameService/packets/send/ActionFailed'
import { DataManager } from '../../data/manager'
import { NpcHtmlMessagePath } from '../../gameService/packets/send/NpcHtmlMessage'
import _ from 'lodash'
import { TeleportLocationManager } from '../../gameService/cache/TeleportLocationManager'
import { InstanceType } from '../../gameService/enums/InstanceType'
import { L2ClanHallDoormenInstance } from '../../gameService/models/actor/instance/L2ClanHallDoormenInstance'
import { CharacterSummonCache } from '../../gameService/cache/CharacterSummonCache'
import { L2TeleportData } from '../../data/interface/TeleportsDataApi'
import { DoorManager } from '../../gameService/cache/DoorManager'

const npcIds: Array<number> = [
    30596,
    30772,
    30773,
    31676,
    31680,
    35092,
    35093,
    35094,
    35096,
    35097,
    35134,
    35135,
    35136,
    35138,
    35139,
    35176,
    35177,
    35178,
    35180,
    35181,
    35218,
    35219,
    35220,
    35222,
    35223,
    35261,
    35262,
    35263,
    35264,
    35265,
    35267,
    35268,
    35269,
    35270,
    35271,
    35308,
    35309,
    35310,
    35312,
    35313,
    35352,
    35353,
    35354,
    35356,
    35357,
    35358,
    35359,
    35360,
    35385,
    35387,
    35389,
    35391,
    35393,
    35395,
    35397,
    35399,
    35401,
    35402,
    35404,
    35406,
    35417,
    35418,
    35433,
    35434,
    35435,
    35436,
    35440,
    35442,
    35444,
    35446,
    35448,
    35450,
    35452,
    35454,
    35456,
    35458,
    35460,
    35462,
    35464,
    35466,
    35468,
    35497,
    35498,
    35499,
    35500,
    35501,
    35503,
    35504,
    35505,
    35544,
    35545,
    35546,
    35548,
    35549,
    35550,
    35551,
    35552,
    35567,
    35569,
    35571,
    35573,
    35575,
    35577,
    35579,
    35581,
    35583,
    35585,
    35587,
    35601,
    35602,
    35641,
    35642,
    35667,
    35668,
    35669,
    35699,
    35700,
    35701,
    35736,
    35737,
    35738,
    35768,
    35769,
    35770,
    35805,
    35806,
    35807,
    35836,
    35837,
    35838,
    35868,
    35869,
    35870,
    35905,
    35906,
    35907,
    35937,
    35938,
    35939,
    35975,
    35976,
    35977,
    36012,
    36013,
    36014,
    36044,
    36045,
    36046,
    36082,
    36083,
    36084,
    36119,
    36120,
    36121,
    36150,
    36151,
    36152,
    36182,
    36183,
    36184,
    36220,
    36221,
    36222,
    36258,
    36259,
    36260,
    36295,
    36296,
    36297,
    36327,
    36328,
    36329,
    36365,
    36366,
    36367,
]

const defaultResult: EventTerminationResult = TerminatedResultHelper.defaultSuccess
const continueResult: EventTerminationResult = TerminatedResultHelper.defaultFailure

export class PropertyDoormanBypass extends ListenerLogic {
    constructor() {
        super( 'PropertyDoormanBypass', 'listeners/bypass/PropertyDoorman.ts' )
    }

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.NpcId,
                eventType: EventType.NpcBypass,
                method: this.onNpcBypass.bind( this ),
                ids: npcIds,
            },
        ]
    }

    async onNpcBypass( data: NpcBypassEvent ): Promise<EventTerminationResult> {
        let commandChunks: Array<string> = data.command.split( ' ' )
        let npc = L2World.getObjectById( data.originatorId ) as L2DoormenInstance
        let player = L2World.getPlayer( data.playerId )

        switch ( commandChunks[ 0 ] ) {
            case 'Chat':
                npc.showChatWindowDefault( player )
                return defaultResult

            case 'open_doors':
                this.onOpenDoors( player, data, npc )
                return defaultResult

            case 'close_doors':
                this.onCloseDoors( player, data, npc )
                return defaultResult

            case 'tele':
                await this.onTeleport( player, commandChunks[ 1 ], npc )
                return defaultResult

            case 'evolve':
                if ( npc.isInstanceType( InstanceType.L2ClanHallDoormenInstance ) ) {
                    await this.onPetEvolve( player, commandChunks, npc as L2ClanHallDoormenInstance )
                }

                break
        }

        return continueResult
    }

    onOpenDoors( player: L2PcInstance, data: NpcBypassEvent, npc: L2DoormenInstance ): void {
        if ( !npc.isOwnerClan( player ) ) {
            return
        }

        if ( npc.isUnderSiege() ) {
            return this.cannotManageDoors( player, npc )
        }

        let ids : Array<number> = _.split( data.command.substring( 11 ), ', ' ).map( value => parseInt( value, 10 ) )

        switch ( npc.getInstanceType() ) {
            case InstanceType.L2CastleDoormenInstance:
                ids.forEach( ( id: number ) => {
                    if ( npc.getConquerableHall() ) {
                        npc.getConquerableHall().openCloseDoor( id, true )
                    } else {
                        npc.getCastle().openDoor( player, id )
                    }
                } )

                return

            case InstanceType.L2ClanHallDoormenInstance:
                ( npc as L2ClanHallDoormenInstance ).getClanHall().openCloseDoors( true )

                let path = 'data/html/clanHallDoormen/doormen-opened.htm'
                let html: string = DataManager.getHtmlData().getItem( path )
                player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), npc.getObjectId() ) )

                return

            case InstanceType.L2FortDoormenInstance:
                let fort = npc.getFort()

                ids.forEach( ( id : number ) => {
                    fort.openCloseDoor( player, id, true )
                } )

                return

            default:
                ids.forEach( ( id: number ): void => {
                    let door = DoorManager.getDoor( id )
                    if ( door ) {
                        door.openMe()
                    }
                } )

                return
        }
    }

    cannotManageDoors( player: L2PcInstance, npc: L2DoormenInstance ): void {
        player.sendOwnedData( ActionFailed() )

        let path: string = `data/html/doormen/${ npc.getId() }-busy.htm`
        let html: string = DataManager.getHtmlData().getItem( path )
        player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), npc.getObjectId() ) )
    }

    onCloseDoors( player: L2PcInstance, data: NpcBypassEvent, npc: L2DoormenInstance ): void {
        if ( !npc.isOwnerClan( player ) ) {
            return
        }

        if ( npc.isUnderSiege() ) {
            return this.cannotManageDoors( player, npc )
        }

        let ids : Array<number> = _.split( data.command.substring( 11 ), ', ' ).map( value => parseInt( value, 10 ) )
        switch ( npc.getInstanceType() ) {
            case InstanceType.L2CastleDoormenInstance:
                ids.forEach( ( id: number ) => {
                    if ( npc.getConquerableHall() ) {
                        npc.getConquerableHall().openCloseDoor( id, false )
                    } else {
                        npc.getCastle().closeDoor( player, id )
                    }
                } )

                return

            case InstanceType.L2ClanHallDoormenInstance:
                ( npc as L2ClanHallDoormenInstance ).getClanHall().openCloseDoors( false )

                let path = 'data/html/clanHallDoormen/doormen-closed.htm'
                let html: string = DataManager.getHtmlData().getItem( path )
                player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), npc.getObjectId() ) )

                return

            case InstanceType.L2FortDoormenInstance:
                let fort = npc.getFort()

                ids.forEach( ( id : number ) => {
                    fort.openCloseDoor( player, id, false )
                } )

                return

            default:
                ids.forEach( ( id: number ): void => {
                    let door = DoorManager.getDoor( id )
                    if ( door ) {
                        door.closeMe()
                    }
                } )

                return
        }
    }

    async onTeleport( player: L2PcInstance, teleportId: string, npc: L2DoormenInstance ): Promise<void> {
        if ( !npc.isOwnerClan( player ) ) {
            return
        }

        let whereTo = _.parseInt( teleportId.trim() )
        let teleport: L2TeleportData = TeleportLocationManager.getLocation( whereTo )
        if ( teleport && !player.isAlikeDead() ) {
            await player.teleportToLocationCoordinates( teleport.x, teleport.y, teleport.z, player.getHeading() )
        }

        player.sendOwnedData( ActionFailed() )
    }

    async onPetEvolve( player: L2PcInstance, commandChunks: Array<string>, npc: L2ClanHallDoormenInstance ): Promise<void> {
        if ( !npc.isOwnerClan( player ) || commandChunks.length < 2 ) {
            return
        }

        let clanHall = npc.getClanHall()
        if ( !clanHall || !clanHall.hasPetEvolve ) {
            return
        }

        let isSuccess = await this.isEvolveSuccess( parseInt( commandChunks[ 1 ] ), player, npc )
        let suffix = isSuccess ? 'ok' : 'no'

        let path = `data/html/clanHallDoormen/evolve-${ suffix }`
        let html: string = DataManager.getHtmlData().getItem( path )
        return player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), npc.getObjectId() ) )
    }

    async isEvolveSuccess( value: number, player: L2PcInstance, npc: L2ClanHallDoormenInstance ): Promise<boolean> {
        switch ( value ) {
            case 1:
                return CharacterSummonCache.upgradePet( player, npc, 9882, 10307, 55 )

            case 2:
                return CharacterSummonCache.upgradePet( player, npc, 4422, 10308, 55 )

            case 3:
                return CharacterSummonCache.upgradePet( player, npc, 4423, 10309, 55 )

            case 4:
                return CharacterSummonCache.upgradePet( player, npc, 4424, 10310, 55 )

            case 5:
                return CharacterSummonCache.upgradePet( player, npc, 10426, 10611, 70 )
        }

        return false
    }
}