import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import { EventTerminationResult, EventType, NpcBypassEvent } from '../../gameService/models/events/EventType'
import { DataManager } from '../../data/manager'
import { NpcHtmlMessage, NpcHtmlMessagePath } from '../../gameService/packets/send/NpcHtmlMessage'
import { L2World } from '../../gameService/L2World'
import { L2AuctioneerInstance } from '../../gameService/models/actor/instance/L2AuctioneerInstance'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import _ from 'lodash'
import { ClanHallAuction, getTaxRatio } from '../../gameService/models/auction/ClanHallAuction'
import { AuctionManager } from '../../gameService/instancemanager/AuctionManager'
import { AuctionableHall } from '../../gameService/models/entity/clanhall/AuctionableHall'
import { ClanHallManager } from '../../gameService/instancemanager/ClanHallManager'
import moment from 'moment/moment'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { L2Clan } from '../../gameService/models/L2Clan'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { L2lanHallAuctionBidTableData } from '../../database/interface/ClanHallAuctionBidTableApi'
import { TerminatedResultHelper } from '../helpers/TerminatedResultHelper'
import { getMaxAdena } from '../../gameService/helpers/ConfigurationHelper'
import { createPagination } from '../../gameService/handler/admincommands/helpers/SearchHelper'
import { RespawnRegionCache } from '../../gameService/cache/RespawnRegionCache'
import { L2Object } from '../../gameService/models/L2Object'
import { ClanPrivilege } from '../../gameService/enums/ClanPriviledge'
import { L2AuctioneerState } from '../../gameService/enums/L2AuctioneerState'

const npcIds: Array<number> = [
    30767,
    30768,
    30769,
    30770,
    30771,
]

const disabledResult: EventTerminationResult = {
    message: 'Action is not available.',
    priority: 0,
    terminate: true,
}

const defaultResult: EventTerminationResult = TerminatedResultHelper.defaultSuccess

const enum ClanHallAuctionCommands {
    Start = 'clanh-start',
    ShowAuctions = 'clanh-show-auctions',
    ConfirmAuction = 'clanh-confirm-auction',
    Bidding = 'clanh-bidding',
    PerformBid = 'clanh-bid',
    StartBid = 'clanh-initial-bid',
    MyAuctions = 'clanh-show-selected',
    ShowBids = 'clanh-show-bids',
    CancelBidConfirmation = 'clanh-cancel-bconfirm',
    PerformBidCancel = 'clanh-cancel-bid',
    CancelAuctionConfirmation = 'clanh-cancel-aconfirm',
    PerformAuctionCancel = 'clanh-cancel-auction',
    SaleStepOne = 'clanh-sale-step1',
    SaleStepTwo = 'clanh-sale-step2',
    SaleStepThree = 'clanh-sale-step3',
    LocationInfo = 'clanh-location'
}

export class ClanHallAuctioneerBypass extends ListenerLogic {
    pendingAuctions: Record<number, Record<number, ClanHallAuction>> = {}

    constructor() {
        super( 'ClanHallAuctioneer', 'listeners/bypass/ClanHallAuctioneer.ts' )
    }

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.NpcId,
                eventType: EventType.NpcBypass,
                method: this.onNpcBypass.bind( this ),
                ids: npcIds,
            },
        ]
    }

    async onNpcBypass( data: NpcBypassEvent ): Promise<EventTerminationResult> {
        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.originatorId ) as L2AuctioneerInstance

        switch ( npc.getCurrentState() ) {
            case L2AuctioneerState.Disabled:
                return disabledResult

            case L2AuctioneerState.Siege:
                let path = 'overrides/html/clanhall/auction/busy.htm'
                let html: string = DataManager.getHtmlData().getItem( path )
                player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), data.originatorId ) )

                return defaultResult

            case L2AuctioneerState.Normal:
                await this.showNormalResponse( player, npc, data.command )

                return defaultResult
        }

        return TerminatedResultHelper.defaultFailure
    }

    async showNormalResponse( player: L2PcInstance, npc: L2AuctioneerInstance, line: string ): Promise<void> {
        let [ command, value, ...additionalValues ]: Array<string> = _.split( line, ' ' )

        switch ( command ) {
            case ClanHallAuctionCommands.Start:
                return npc.showChatWindowDefault( player )

            case ClanHallAuctionCommands.ConfirmAuction:
                return this.processConfirmAction( npc.getId(), player.getClan()?.getHideoutId() )

            case ClanHallAuctionCommands.Bidding:
                return this.processCommandBidding( player, value, npc.getObjectId() )

            case ClanHallAuctionCommands.PerformBid:
                return this.processCommandBid( player, value, additionalValues )

            case ClanHallAuctionCommands.StartBid:
                return this.processCommandFirstTimeBid( player, value, npc.getObjectId() )

            case ClanHallAuctionCommands.ShowAuctions:
                return this.processCommandList( player, value, npc.getObjectId() )

            case ClanHallAuctionCommands.ShowBids:
                return this.processCommandBidlist( player, value, npc.getObjectId() )

            case ClanHallAuctionCommands.MyAuctions:
                return this.processCommandSelectedItems( player, npc.getObjectId() )

            case ClanHallAuctionCommands.CancelBidConfirmation:
                return this.processCommandCancelBidInformation( player, npc.getObjectId() )

            case ClanHallAuctionCommands.PerformBidCancel:
                return this.processCommandPerformCancelBid( player, npc.getObjectId() )

            case ClanHallAuctionCommands.CancelAuctionConfirmation:
                return this.processCommandCancelAuctionInformation( player, npc.getObjectId() )

            case ClanHallAuctionCommands.PerformAuctionCancel:
                return this.processCommandPerformCancelAuction( player, npc.getObjectId() )

            case ClanHallAuctionCommands.SaleStepOne:
                return this.processCommandSaleStepOne( player, npc.getObjectId() )

            case ClanHallAuctionCommands.SaleStepTwo:
                return this.processCommandSaleStepTwo( player, npc.getObjectId() )

            case ClanHallAuctionCommands.SaleStepThree:
                return this.processSaleStepThree( player, value, additionalValues, npc.getId(), npc.getObjectId() )

            case 'rebid':
                return this.processCommandRebid( player, npc.getObjectId() )

            case ClanHallAuctionCommands.LocationInfo:
                return this.processCommandLocation( player, npc.getObjectId() )
        }
    }

    processCommandBidding( player: L2PcInstance, value: string, npcObjectId: number ): void {
        if ( _.isEmpty( value ) ) {
            return
        }

        let id: number = _.parseInt( value )
        if ( !id ) {
            return
        }

        let auction: ClanHallAuction = AuctionManager.getAuction( id )
        if ( !auction ) {
            return
        }

        let clanHall: AuctionableHall = ClanHallManager.getClanHallById( auction.data.itemId )
        if ( !clanHall ) {
            return
        }

        let html: string = DataManager.getHtmlData().getItem( 'overrides/html/clanhall/auction/auction-info.htm' )
            .replace( '%hallName%', auction.data.itemName )
            .replace( '%clanName%', auction.data.sellerClanName )
            .replace( '%clanLeaderName%', auction.data.sellerName )
            .replace( '%grade%', ( clanHall.getGrade() * 10 ).toString() )
            .replace( '%fee%', clanHall.getLeaseAmount().toString() )
            .replace( '%location%', clanHall.getLocation() )
            .replace( '%closingDate%', moment( auction.data.endDate ).format( 'dd/MM/yyyy HH:mm' ) )
            .replace( '%timeRemaining%', GeneralHelper.formatSeconds( ( auction.data.endDate - Date.now() ) / 1000, false ).join( ', ' ) )
            .replace( '%minimumBid%', auction.data.startingBid.toString() )
            .replace( '%biddingClans%', _.size( auction.bidders ).toString() )
            .replace( '%description%', clanHall.getDescription() )
            .replace( '%cancelAction%', ClanHallAuctionCommands.ShowAuctions )
            .replace( '%listAction%', `${ ClanHallAuctionCommands.ShowBids } ${ auction.data.id }` )
            .replace( '%bidAction%', `${ ClanHallAuctionCommands.StartBid } ${ auction.data.id }` )

        player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId(), npcObjectId ) )
    }

    async processSaleStepThree( player: L2PcInstance, value: string, additionalValues: Array<string>, npcId: number, npcObjectId: number ): Promise<void> {
        if ( !value ) {
            return
        }

        let days: number = _.parseInt( value )
        if ( !days ) {
            return
        }

        let startingBid: number = 0
        if ( additionalValues.length > 0 ) {
            startingBid = Math.min( _.parseInt( additionalValues[ 0 ] ), getMaxAdena() )
        }

        let clan: L2Clan = player.getClan()
        let createdAuction = ClanHallAuction.fromClan( clan.getHideoutId(),
                clan,
                GeneralHelper.daysToMillis( days ),
                startingBid,
                ClanHallManager.getClanHallByOwner( clan ).getName() )

        _.set( this.pendingAuctions, [ npcId, createdAuction.getId() ], createdAuction )

        let html: string = DataManager.getHtmlData().getItem( 'overrides/html/clanhall/auction/sale-step-three.htm' )
            .replace( '%days%', value )
            .replace( '%closingDate%', moment( createdAuction.data.endDate ).format( 'dd/MM/yyyy HH:mm' ) )
            .replace( '%minimumBid%', createdAuction.data.startingBid.toString() )
            .replace( '%normalBid%', createdAuction.data.startingBid.toString() )
            .replace( '%description%', ClanHallManager.getClanHallByOwner( clan ).getDescription() )
            .replace( '%cancelAction%', ClanHallAuctionCommands.SaleStepTwo )
            .replace( '%confirmAction%', ClanHallAuctionCommands.ConfirmAuction )

        player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId(), npcObjectId ) )
    }

    processCommandBid( player: L2PcInstance, value: string, additionalValues: Array<string> ): Promise<void> {
        if ( _.isEmpty( value ) ) {
            return
        }

        let id: number = _.parseInt( value )
        if ( !id || !AuctionManager.getAuction( id ) ) {
            player.sendMessage( 'Invalid auction.' )
            return
        }

        let currentBid: number = 0
        if ( additionalValues.length > 0 ) {
            currentBid = Math.min( _.parseInt( additionalValues[ 0 ] ), getMaxAdena() )
        }

        if ( !currentBid ) {
            player.sendMessage( 'Invalid bid.' )
            return
        }

        return AuctionManager.getAuction( id ).setBidAmount( player, currentBid )
    }

    processCommandFirstTimeBid( player: L2PcInstance, value: string, npcObjectId: number ): void {
        let clan: L2Clan = player.getClan()
        if ( !clan || clan.getLevel() < 2 ) {
            return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.AUCTION_ONLY_CLAN_LEVEL_2_HIGHER ) )
        }

        if ( _.isEmpty( value ) ) {
            return
        }

        let auctionId: number = _.parseInt( value )
        if ( ( clan.getAuctionIdWithBid() > 0 && clan.getAuctionIdWithBid() !== auctionId ) || clan.getHideoutId() > 0 ) {
            return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ALREADY_SUBMITTED_BID ) )
        }

        let auction: ClanHallAuction = AuctionManager.getAuction( auctionId )
        if ( !auction ) {
            return player.sendMessage( 'Invalid auction.' )
        }

        let minimumBid: number = auction.highestBidder.maxBidAmount === 0 ? auction.data.startingBid : auction.highestBidder.maxBidAmount
        let html: string = DataManager.getHtmlData().getItem( 'overrides/html/clanhall/auction/bid-initial.htm' )
            .replace( '%cancelAction%', `${ClanHallAuctionCommands.Bidding} ${ value }` )
            .replace( '%confirmAction%', ClanHallAuctionCommands.PerformBid )
            .replace( '%warehouseAdena%', clan.getWarehouse().getAdenaAmount().toString() )
            .replace( '%minimumBid%', minimumBid.toString() )

        player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId(), npcObjectId ) )
    }

    processCommandList( player: L2PcInstance, value: string, npcObjectId: number ): void {
        let auctions: Array<Array<ClanHallAuction>> = _.chunk( AuctionManager.getAll(), 15 )
        let currentPage: number = 0
        if ( !_.isEmpty( value ) ) {
            let parsedValue: number = _.parseInt( value )
            if ( _.nth( auctions, parsedValue ) ) {
                currentPage = parsedValue
            }
        }

        let html: string = DataManager.getHtmlData().getItem( 'overrides/html/clanhall/auction/show-auctions.htm' )
            .replace( '%cancelAction%', ClanHallAuctionCommands.Start )
            .replace( '%page%', ( currentPage + 1 ).toString() )
            .replace( '%buttons%', createPagination( auctions.length, `bypass -h ${ClanHallAuctionCommands.ShowAuctions}` ) )

        if ( auctions.length === 0 ) {
            return player.sendOwnedData( NpcHtmlMessage( html.replace( '%items%', '' ), player.getObjectId(), npcObjectId ) )
        }

        let itemHtml = DataManager.getHtmlData().getItem( 'overrides/html/clanhall/auction/show-auctions-item.htm' )

        let items : Array<string> = auctions[ currentPage ].map( ( item: ClanHallAuction ): string => {
            return itemHtml
                .replace( '%location%', ClanHallManager.getClanHallById( item.data.itemId ).getLocation() )
                .replace( '%action%', `${ ClanHallAuctionCommands.Bidding } ${ item.getId() }` )
                .replace( '%name%', item.data.itemName )
                // TODO : compute end date in proper format when date is retrieved from database to avoid formatting on the fly
                .replace( '%closingDate%', moment( item.data.endDate ).format( 'yy/MM/dd' ) )
                .replace( '%bid%', item.data.startingBid.toString() )
        } )

        return player.sendOwnedData( NpcHtmlMessage( html.replace( '%items%', items.join( '' ) ), player.getObjectId(), npcObjectId ) )
    }

    processCommandBidlist( player: L2PcInstance, value: string, npcObjectId: number ): void {
        let auctionId: number = _.parseInt( value )
        if ( !auctionId ) {
            if ( player.getClan().getAuctionIdWithBid() <= 0 ) {
                return
            }

            auctionId = player.getClan().getAuctionIdWithBid()
        }

        let auction: ClanHallAuction = AuctionManager.getAuction( auctionId )
        if ( !auction ) {
            return
        }

        let itemHtml = DataManager.getHtmlData().getItem( 'overrides/html/clanhall/auction/show-bidders-item.htm' )

        let bidLines: string = _.map( auction.bidders, ( data: L2lanHallAuctionBidTableData ): string => {
            return itemHtml
                .replace( '%clanName%', data.clanName )
                .replace( '%playerName%', data.name )
                .replace( '%time%', moment( data.bidTime ).format( 'yy/MM/dd' ) )
                .replace( '%price%', data.maxBidAmount.toString() )
        } ).join( '' )

        let html: string = DataManager.getHtmlData().getItem( 'overrides/html/clanhall/auction/show-bidders.htm' )
                                      .replace( '%items%', bidLines )
                                      .replace( '%backAction%', ClanHallAuctionCommands.Bidding )

        player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId(), npcObjectId ) )
    }

    processCommandSelectedItems( player: L2PcInstance, npcObjectId: number ): void {
        let clan: L2Clan = player.getClan()
        if ( !clan ) {
            return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_PARTICIPATE_IN_AN_AUCTION ) )
        }

        if ( clan.getHideoutId() === 0 ) {

            let auction: ClanHallAuction = AuctionManager.getAuction( clan.getAuctionIdWithBid() )
            if ( auction ) {
                return player.sendOwnedData( NpcHtmlMessage( this.getBidInfoHtml( clan, auction ), player.getObjectId(), npcObjectId ) )
            }

            return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NO_OFFERINGS_OWN_OR_MADE_BID_FOR ) )
        }

        let auction: ClanHallAuction = AuctionManager.getAuction( clan.getHideoutId() )
        if ( !auction ) {
            let hall = ClanHallManager.getClanHallById( clan.getHideoutId() )
            return player.sendOwnedData( NpcHtmlMessage( this.getNormalInfoHtml( clan, hall ), player.getObjectId(), npcObjectId ) )
        }

        return player.sendOwnedData( NpcHtmlMessage( this.getSaleInfoHtml( auction ), player.getObjectId(), npcObjectId ) )
    }

    getBidInfoHtml( clan: L2Clan, auction: ClanHallAuction ): string {
        let hall = ClanHallManager.getClanHallById( auction.data.itemId )
        return DataManager.getHtmlData().getItem( 'overrides/html/clanhall/auction/bid-info.htm' )
            .replace( '%hallName%', auction.data.itemName )
            .replace( '%sellerClan%', auction.data.sellerClanName )
            .replace( '%sellerLeader%', auction.data.sellerName )
            .replace( '%grade%', ( hall.getGrade() * 10 ).toString() )

            .replace( '%fee%', hall.getLeaseAmount().toString() )
            .replace( '%location%', hall.getLocation() )
            .replace( '%closingDate%', moment( auction.data.endDate ).format( 'dd/MM/yyyy HH:mm' ) )
            .replace( '%timeRemaining%', GeneralHelper.formatSeconds( ( auction.data.endDate - Date.now() ) / 1000 ).join( ', ' ) )

            .replace( '%minimumBid%', auction.data.startingBid.toString() )
            .replace( '%currentBid%', auction.bidders[ clan.getId() ].maxBidAmount.toString() )
            .replace( '%description%', hall.getDescription() )
            .replace( '%backAction%', 'bypass -h start' )

            .replace( '%rebidAction%', ClanHallAuctionCommands.StartBid )
            .replace( '%showAction%', ClanHallAuctionCommands.ShowBids )
            .replace( '%cancelAction%', ClanHallAuctionCommands.CancelAuctionConfirmation )
    }

    getSaleInfoHtml( auction: ClanHallAuction ): string {
        let hall = ClanHallManager.getClanHallById( auction.data.itemId )
        return DataManager.getHtmlData().getItem( 'overrides/html/clanhall/auction/sale-info.htm' )
            .replace( '%hallName%', auction.data.itemName )
            .replace( '%sellerClan%', auction.data.sellerClanName )
            .replace( '%sellerLeader%', auction.data.sellerName )
            .replace( '%grade%', ( hall.getGrade() * 10 ).toString() )

            .replace( '%fee%', hall.getLeaseAmount().toString() )
            .replace( '%location%', hall.getLocation() )
            .replace( '%closingDate%', moment( auction.data.endDate ).format( 'dd/MM/yyyy HH:mm' ) )
            .replace( '%timeRemaining%', GeneralHelper.formatSeconds( ( auction.data.endDate - Date.now() ) / 1000 ).join( ', ' ) )

            .replace( '%minimumBid%', auction.data.startingBid.toString() )
            .replace( '%clanAmount%', _.size( auction.bidders ).toString() )
            .replace( '%description%', hall.getDescription() )
            .replace( '%backAction%', ClanHallAuctionCommands.Start )

            .replace( '%cancelAction%', ClanHallAuctionCommands.CancelAuctionConfirmation )
            .replace( '%showAction%', ClanHallAuctionCommands.ShowBids )
    }

    getNormalInfoHtml( clan: L2Clan, hall: AuctionableHall ): string {
        return DataManager.getHtmlData().getItem( 'overrides/html/clanhall/auction/hall-info.htm' )
                          .replace( '%name%', hall.getName() )
                          .replace( '%clanName%', clan.getName() )
                          .replace( '%clanLeaderName%', clan.getLeaderName() )
                          .replace( '%grade%', ( hall.getGrade() * 10 ).toString() )
                          .replace( '%fee%', hall.getLeaseAmount().toString() )
                          .replace( '%location%', hall.getLocation() )
                          .replace( '%backAction%', ClanHallAuctionCommands.Start )
                          .replace( '%sellAction%', ClanHallAuctionCommands.SaleStepOne )
    }

    processCommandCancelBidInformation( player: L2PcInstance, npcObjectId: number ): void {
        let clan: L2Clan = player.getClan()
        if ( !player.hasClanPrivilege( ClanPrivilege.ClanHallAuction ) || !clan ) {
            return this.processNotAuthorized( player, npcObjectId )
        }

        let ratioToKeep = getTaxRatio()
        let amount: number = AuctionManager.getAuction( clan.getAuctionIdWithBid() ).bidders[ clan.getId() ].maxBidAmount

        let html: string = DataManager.getHtmlData().getItem( 'overrides/html/clanhall/auction/cancel-bid.htm' )
            .replace( '%percentage%', Math.floor( ratioToKeep * 100 ).toString() )
            .replace( '%bid%', amount.toString() )
            .replace( '%remaining%', Math.floor( amount * ( 1 - ratioToKeep ) ).toString() )
            .replace( '%backAction%', ClanHallAuctionCommands.MyAuctions )
            .replace( '%cancelAction%', ClanHallAuctionCommands.PerformBidCancel )

        return player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId(), npcObjectId ) )
    }

    async processCommandPerformCancelBid( player: L2PcInstance, npcObjectId: number ): Promise<void> {
        let clan: L2Clan = player.getClan()
        if ( !player.hasClanPrivilege( ClanPrivilege.ClanHallAuction ) || !clan ) {
            return this.processNotAuthorized( player, npcObjectId )
        }

        let auction: ClanHallAuction = AuctionManager.getAuction( clan.getAuctionIdWithBid() )
        if ( !auction ) {
            return
        }

        await auction.cancelBid( clan.getId() )
        return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANCELED_BID ) )
    }

    processCommandCancelAuctionInformation( player: L2PcInstance, npcObjectId: number ): void {
        let clan: L2Clan = player.getClan()
        if ( !player.hasClanPrivilege( ClanPrivilege.ClanHallAuction ) || !clan ) {
            return this.processNotAuthorized( player, npcObjectId )
        }

        let hall: AuctionableHall = ClanHallManager.getClanHallByOwner( clan )
        if ( !hall ) {
            return
        }

        let html: string = DataManager.getHtmlData().getItem( 'overrides/html/clanhall/auction/cancel-auction.htm' )
                                      .replace( '%deposit%', ClanHallManager.getClanHallByOwner( player.getClan() ).getLeaseAmount().toString() )
                                      .replace( '%backAction%', ClanHallAuctionCommands.MyAuctions )
                                      .replace( '%cancelAction%', ClanHallAuctionCommands.PerformAuctionCancel )
        return player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId(), npcObjectId ) )
    }

    async processCommandPerformCancelAuction( player: L2PcInstance, npcObjectId: number ): Promise<void> {
        let clan: L2Clan = player.getClan()
        if ( !player.hasClanPrivilege( ClanPrivilege.ClanHallAuction ) || !clan ) {
            return this.processNotAuthorized( player, npcObjectId )
        }

        let auction: ClanHallAuction = AuctionManager.getAuction( clan.getHideoutId() )
        if ( !auction ) {
            return
        }

        await auction.removeAll()
        player.sendMessage( `Auction for ${ auction.data.itemName } from clan '${ auction.data.sellerClanName }' has been canceled` )
    }

    processCommandSaleStepOne( player: L2PcInstance, npcObjectId: number ): void {
        let clan: L2Clan = player.getClan()
        if ( !player.hasClanPrivilege( ClanPrivilege.ClanHallAuction ) || !clan ) {
            return this.processNotAuthorized( player, npcObjectId )
        }

        let hall: AuctionableHall = ClanHallManager.getClanHallByOwner( clan )
        if ( !hall ) {
            return
        }

        let html: string = DataManager.getHtmlData().getItem( 'overrides/html/clanhall/auction/sale-step-one.htm' )
                                      .replace( '%fee%', hall.getLeaseAmount().toString() )
                                      .replace( '%warehouseAdena%', clan.getWarehouse().getAdenaAmount().toString() )
                                      .replace( '%backAction%', ClanHallAuctionCommands.MyAuctions )
                                      .replace( '%sellAction%', ClanHallAuctionCommands.SaleStepTwo )
        return player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId(), npcObjectId ) )
    }

    processCommandSaleStepTwo( player: L2PcInstance, npcObjectId: number ): void {
        let clan: L2Clan = player.getClan()
        if ( !player.hasClanPrivilege( ClanPrivilege.ClanHallAuction ) || !clan ) {
            return this.processNotAuthorized( player, npcObjectId )
        }

        let hall: AuctionableHall = ClanHallManager.getClanHallByOwner( clan )
        if ( !hall ) {
            return
        }

        let data = DataManager.getAuctionTemplates().getClanHall( hall.getId() )
        let html: string = DataManager.getHtmlData().getItem( 'overrides/html/clanhall/auction/sale-step-two.htm' )
            .replace( '%normalPrice%', data ? data.startingBid.toString() : '0' )
            .replace( '%lastPrice%', hall.getLeaseAmount().toString() )
            .replace( '%backAction%', ClanHallAuctionCommands.SaleStepOne )
            .replace( '%sellAction%', ClanHallAuctionCommands.SaleStepThree )
            .replace( '%taxPercent%', Math.floor( getTaxRatio() * 100 ).toString() )

        return player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId(), npcObjectId ) )
    }

    processCommandRebid( player: L2PcInstance, npcObjectId: number ): void {
        let clan: L2Clan = player.getClan()
        if ( !player.hasClanPrivilege( ClanPrivilege.ClanHallAuction ) || !clan ) {
            return this.processNotAuthorized( player, npcObjectId )
        }

        let auction: ClanHallAuction = AuctionManager.getAuction( clan.getAuctionIdWithBid() )
        if ( !auction ) {
            return
        }

        let html: string = DataManager.getHtmlData().getItem( 'overrides/html/clanhall/auction/update-bid.htm' )
                                      .replace( '%bid%', auction.bidders[ clan.getId() ].maxBidAmount.toString() )
                                      .replace( '%minimumBid%', auction.data.startingBid.toString() )
                                      .replace( '%closingDate%', moment( auction.data.endDate ).format( 'dd/MM/yyyy HH:mm' ) )
                                      .replace( '%backAction%', ClanHallAuctionCommands.MyAuctions )
                                      .replace( '%bidAction%', ClanHallAuctionCommands.StartBid )

        return player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId(), npcObjectId ) )
    }

    processCommandLocation( player: L2PcInstance, npcObjectId: number ): void {
        let html: string = DataManager.getHtmlData().getItem( 'overrides/html/clanhall/auction/location.htm' )
                                      .replace( '%town%', this.getClosestTownName( player ) )
                                      .replace( '%picture%', this.getPictureName( player ) )
                                      .replace( '%action%', ClanHallAuctionCommands.Start )

        return player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId(), npcObjectId ) )
    }

    async processConfirmAction( npcId: number, id: number ): Promise<void> {
        let auction: ClanHallAuction = _.get( this.pendingAuctions, [ npcId, id ] )
        if ( auction ) {
            AuctionManager.addAuction( auction )
            await auction.addAuctionToDatabase()
            _.unset( this.pendingAuctions, [ npcId, id ] )
        }
    }

    getPictureName( player: L2PcInstance ): string {
        let nearestTownId: number = RespawnRegionCache.getLocationMessageId( player )
        switch ( nearestTownId ) {
            case 911:
                return 'GLUDIN'

            case 912:
                return 'GLUDIO'

            case 916:
                return 'DION'

            case 918:
                return 'GIRAN'

            case 1537:
                return 'RUNE'

            case 1538:
                return 'GODARD'

            case 1714:
                return 'SCHUTTGART'
        }

        return 'ADEN'
    }

    processNotAuthorized( player: L2PcInstance, npcObjectId : number ) : void {
        let path = 'overrides/html/clanhall/auction/not_authorized.htm'
        let html: string = DataManager.getHtmlData().getItem( path )
        return player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), npcObjectId ) )
    }

    /*
        Using town zone lookup instead of map region by coordinates since it is possible that current
        object may not be in available map region.
        - possible to get geo region key from zone center point to map it to map region, if desired.
     */
    getClosestTownName( object: L2Object ): string {
        let [ zone ] = L2World.getNearestTowns( object, 1 )
        if ( !zone ) {
            return 'Aden Castle Town'
        }

        return zone.properties.name
    }
}