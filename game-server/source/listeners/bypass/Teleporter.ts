import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import { EventTerminationResult, EventType, NpcBypassEvent } from '../../gameService/models/events/EventType'
import { ActionFailed } from '../../gameService/packets/send/ActionFailed'
import { DataManager } from '../../data/manager'
import { NpcHtmlMessagePath } from '../../gameService/packets/send/NpcHtmlMessage'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { ConfigManager } from '../../config/ConfigManager'
import moment from 'moment/moment'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { TeleportLocationManager } from '../../gameService/cache/TeleportLocationManager'
import { SiegeManager } from '../../gameService/instancemanager/SiegeManager'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2World } from '../../gameService/L2World'
import { L2TeleporterInstance } from '../../gameService/models/actor/instance/L2TeleporterInstance'
import { TerminatedResultHelper } from '../helpers/TerminatedResultHelper'
import { L2TeleportData } from '../../data/interface/TeleportsDataApi'
import { CastleManager } from '../../gameService/instancemanager/CastleManager'
import { AreaType } from '../../gameService/models/areas/AreaType'
import { TeleportRestriction } from '../../gameService/enums/TeleportRestriction'
import { teleportCharacterToGeometryCoordinates } from '../../gameService/helpers/TeleportHelper'
import { GeometryId } from '../../gameService/enums/GeometryId'

const npcIds: Array<number> = [
    4316,
    4317,
    4318,
    4319,
    4320,
    4321,
    4322,
    4323,
    29061,
    30006,
    30059,
    30080,
    30134,
    30146,
    30162,
    30177,
    30233,
    30256,
    30320,
    30427,
    30429,
    30484,
    30485,
    30486,
    30487,
    30540,
    30576,
    30716,
    30719,
    30722,
    30727,
    30836,
    30848,
    30878,
    30899,
    31147,
    31211,
    31212,
    31213,
    31214,
    31215,
    31216,
    31217,
    31218,
    31219,
    31220,
    31221,
    31222,
    31223,
    31224,
    31275,
    31320,
    31376,
    31383,
    31698,
    31699,
    31843,
    31861,
    31964,
    32034,
    32035,
    32036,
    32037,
    32039,
    32040,
    32163,
    32181,
    32184,
    32186,
    32189,
    32239,
    32351,
    32378,
    32534,
    32539,
    32613,
    32614,
    32645,
    32648,
    32649,
    32652,
    32653,
    32654,
    32711,
    32712,
    32714,
    32774,
    32782,
    32864,
    32865,
    32866,
    32867,
    32868,
    32869,
    32870,
    32891,
    35560,
    35561,
    35562,
    35563,
    35564,
    35565,
    36570,
]

export class TeleporterBypass extends ListenerLogic {
    constructor() {
        super( 'TeleporterBypass', 'listeners/bypass/Teleporter.ts' )
    }

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.NpcId,
                eventType: EventType.NpcBypass,
                method: this.onNpcBypass.bind( this ),
                ids: npcIds,
            },
        ]
    }

    async onNpcBypass( data: NpcBypassEvent ): Promise<EventTerminationResult> {
        let player = L2World.getPlayer( data.playerId )


        if ( player.isAffectedBySkill( 6201 ) || player.isAffectedBySkill( 6202 ) || player.isAffectedBySkill( 6203 ) ) {
            let path = 'data/html/teleporter/epictransformed.htm'
            let html: string = DataManager.getHtmlData().getItem( path )
                                          .replace( /%npcname%/g, this.getName() )

            player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), data.originatorId ) )
            return
        }

        let commandChunks: Array<string> = data.command.split( ' ' )
        let npc = L2World.getObjectById( data.originatorId ) as L2TeleporterInstance

        switch ( commandChunks[ 0 ].toLowerCase() ) {
            case 'goto':
                await this.onGoTo( player, commandChunks, npc )
                return TerminatedResultHelper.defaultSuccess

            case 'Chat':
                this.onChat( player, commandChunks, npc )
                return TerminatedResultHelper.defaultSuccess
        }

        return TerminatedResultHelper.defaultFailure
    }

    showNewbieHtml( player: L2PcInstance, npc: L2Npc ): void {
        let path = `data/html/teleporter/free/${ npc.getId() }.htm`

        if ( !DataManager.getHtmlData().getItem( path ) ) {
            path = `data/html/teleporter/${ npc.getId() }-1.htm`
        }

        let html: string = DataManager.getHtmlData().getItem( path )
                                      .replace( /%npcname%/g, npc.getName() )

        player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), npc.getObjectId() ) )
    }

    showHalfPriceHtml( player: L2PcInstance, npc: L2Npc ): void {
        let path = `data/html/teleporter/half/${ npc.getId() }.htm`

        if ( !DataManager.getHtmlData().hasItem( path ) ) {
            path = `data/html/teleporter/${ npc.getId() }-1.htm`
        }

        let html: string = DataManager.getHtmlData().getItem( path )
                                      .replace( /%npcname%/g, npc.getName() )

        player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), npc.getObjectId() ) )
    }

    async doTeleport( player: L2PcInstance, teleportId: number, npc: L2Npc ): Promise<void> {
        let teleport: L2TeleportData = TeleportLocationManager.getLocation( teleportId )

        if ( !teleport ) {
            return player.sendOwnedData( ActionFailed() )
        }

        if ( player.isAlikeDead() ) {
            return
        }

        if ( player.isCombatFlagEquipped() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_TELEPORT_WHILE_IN_POSSESSION_OF_A_WARD ) )
            return
        }

        if ( SiegeManager.getSiege( teleport.x, teleport.y, teleport.z ) ) {
            return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NO_PORT_THAT_IS_IN_SIGE ) )
        }

        if ( CastleManager.townHasCastleInSiege( teleport.x, teleport.y ) && npc.isInArea( AreaType.Town ) ) {
            return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NO_PORT_THAT_IS_IN_SIGE ) )
        }

        if ( !ConfigManager.character.karmaPlayerCanUseGK() && ( player.getKarma() > 0 ) ) {
            return player.sendMessage( 'Go away, you\'re not welcome here.' )
        }

        if ( teleport.nobleOnly && !player.isNoble() ) {
            let path = 'data/html/teleporter/nobleteleporter-no.htm'
            let html: string = DataManager.getHtmlData().getItem( path )
                                          .replace( /%npcname%/g, this.getName() )

            player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), npc.getObjectId() ) )
            return
        }

        let amount = teleport.amount
        if ( player.getLevel() < ConfigManager.tuning.getNewbieTeleportLevelLimit() ) {
            amount = 0
        } else if ( player.getLevel() < ConfigManager.tuning.getReducedCostTeleportLevelLimit() ) {
            amount = Math.floor( amount / 2 )
        } else if ( !teleport.nobleOnly ) {
            let currentDate = moment()
            if ( ( currentDate.hours() >= 20 ) && ( ( currentDate.weekday() === 0 ) || ( currentDate.weekday() === 6 ) ) ) {
                amount = Math.floor( amount / 2 )
            }
        }

        if ( amount === 0
                || ConfigManager.character.freeTeleporting()
                || ( amount > 0 && await player.destroyItemByItemId( teleport.itemId, amount, true ) ) ) {
            await teleportCharacterToGeometryCoordinates( GeometryId.ZonePartyTeleport, player, teleport.x, teleport.y, teleport.z, player.getHeading() )
        }

        player.sendOwnedData( ActionFailed() )
    }

    async onGoTo( player: L2PcInstance, commandChunks: Array<string>, npc: L2TeleporterInstance ): Promise<void> {
        switch ( npc.getId() ) {
            case 32534:
            case 32539:
                if ( player.isFlyingMounted() ) {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_ENTER_SEED_IN_FLYING_TRANSFORM ) )
                    return
                }

                break
        }

        if ( commandChunks.length < 2 ) {
            return
        }

        let whereTo = parseInt( commandChunks[ 1 ], 10 )
        let condition = npc.getTeleportRestriction( player )

        if ( condition === TeleportRestriction.None ) {
            return this.doTeleport( player, whereTo, npc )
        }

        if ( condition === TeleportRestriction.OnlyOwner ) {
            let privilegeLevel = commandChunks.length >= 3 ? parseInt( commandChunks[ 2 ], 10 ) : 0
            if ( 10 >= privilegeLevel ) {
                return this.doTeleport( player, whereTo, npc )
            }

            return player.sendMessage( 'You don\'t have the sufficient access level to teleport there.' )
        }
    }

    onChat( player: L2PcInstance, commandChunks: Array<string>, npc: L2TeleporterInstance ) {
        if ( commandChunks.length < 2 ) {
            return
        }

        let value: number = parseInt( commandChunks[ 1 ], 10 )

        if ( value === 1 ) {
            if ( player.getLevel() < ConfigManager.tuning.getNewbieTeleportLevelLimit() ) {
                return this.showNewbieHtml( player, npc )
            }

            if ( player.getLevel() < ConfigManager.tuning.getReducedCostTeleportLevelLimit() ) {
                return this.showHalfPriceHtml( player, npc )
            }

            let currentDate = moment()
            if ( currentDate.hours() >= 20
                    && ( currentDate.weekday() === 0 || currentDate.weekday() === 6 ) ) {
                return this.showHalfPriceHtml( player, npc )
            }
        }

        return npc.showChatWindow( player, value )
    }
}