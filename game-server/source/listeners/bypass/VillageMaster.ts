import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import { EventTerminationResult, EventType, NpcBypassEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import _ from 'lodash'
import { L2Clan } from '../../gameService/models/L2Clan'
import { ClanCache } from '../../gameService/cache/ClanCache'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { L2ClanValues } from '../../gameService/values/L2ClanValues'
import { L2ClanMember } from '../../gameService/models/L2ClanMember'
import { ConfigManager } from '../../config/ConfigManager'
import { DataManager } from '../../data/manager'
import { NpcHtmlMessage, NpcHtmlMessagePath } from '../../gameService/packets/send/NpcHtmlMessage'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { MagicSkillUse } from '../../gameService/packets/send/MagicSkillUse'
import { MagicSkillLaunched } from '../../gameService/packets/send/MagicSkillLaunched'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { UserInfo } from '../../gameService/packets/send/UserInfo'
import { ExBrExtraUserInfo } from '../../gameService/packets/send/ExBrExtraUserInfo'
import { SiegeManager } from '../../gameService/instancemanager/SiegeManager'
import { CastleManager } from '../../gameService/instancemanager/CastleManager'
import { FortSiegeManager } from '../../gameService/instancemanager/FortSiegeManager'
import { FortManager } from '../../gameService/instancemanager/FortManager'
import { L2VillageMasterInstance } from '../../gameService/models/actor/instance/L2VillageMasterInstance'
import { getAvailableSubclasses, getPlayerClassById, IPlayerClass } from '../../gameService/models/base/PlayerClass'
import { ClassId, IClassId } from '../../gameService/models/base/ClassId'
import { Race } from '../../gameService/enums/Race'
import { SubClass } from '../../gameService/models/base/SubClass'
import { TerminatedResultHelper } from '../helpers/TerminatedResultHelper'
import { ClanPledge } from '../../gameService/models/ClanPledge'

const npcIds: Array<number> = [
    30026,
    30031,
    30037,
    30066,
    30070,
    30109,
    30115,
    30120,
    30154,
    30174,
    30175,
    30176,
    30187,
    30191,
    30195,
    30288,
    30289,
    30290,
    30297,
    30358,
    30373,
    30462,
    30474,
    30498,
    30499,
    30500,
    30503,
    30504,
    30505,
    30508,
    30511,
    30512,
    30513,
    30520,
    30525,
    30565,
    30594,
    30595,
    30676,
    30677,
    30681,
    30685,
    30687,
    30689,
    30694,
    30699,
    30704,
    30845,
    30847,
    30849,
    30854,
    30857,
    30862,
    30865,
    30894,
    30897,
    30900,
    30905,
    30910,
    30913,
    31269,
    31272,
    31276,
    31279,
    31285,
    31288,
    31314,
    31317,
    31321,
    31324,
    31326,
    31328,
    31331,
    31334,
    31336,
    31755,
    31958,
    31961,
    31965,
    31968,
    31974,
    31977,
    31996,
    32092,
    32093,
    32094,
    32095,
    32096,
    32097,
    32098,
    32139,
    32140,
    32145,
    32146,
    32147,
    32150,
    32153,
    32154,
    32157,
    32158,
    32160,
    32171,
    32191,
    32193,
    32196,
    32199,
    32202,
    32205,
    32206,
    32209,
    32210,
    32213,
    32214,
    32217,
    32218,
    32221,
    32222,
    32225,
    32226,
    32229,
    32230,
    32233,
    32234,
]

const defaultResult: EventTerminationResult = TerminatedResultHelper.defaultSuccess
const continueResult: EventTerminationResult = TerminatedResultHelper.defaultFailure

const cancelClanLeaderChangeMessage = '<html><body>You don\'t have clan leader delegation applications submitted yet!</body></html>'

export class VillageMasterBypass extends ListenerLogic {
    constructor() {
        super( 'VillageMasterBypass', 'listeners/bypass/VillageMaster.ts' )
    }

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.NpcId,
                eventType: EventType.NpcBypass,
                method: this.onNpcBypass.bind( this ),
                ids: npcIds,
            },
        ]
    }

    async onNpcBypass( data: NpcBypassEvent ): Promise<EventTerminationResult> {
        let player = L2World.getPlayer( data.playerId )

        let commandChunks: Array<string> = data.command.split( ' ' )

        switch ( commandChunks[ 0 ] ) {
            case 'create_clan':
                return this.onCreateClan( commandChunks, player )

            case 'create_academy':
                return this.onCreateAcademy( commandChunks, player )

            case 'rename_pledge':
                return this.onRenamePledge( commandChunks, player )

            case 'create_royal':
                return this.onCreateRoyal( commandChunks, player )

            case 'create_knight':
                return this.onCreateKnight( commandChunks, player )

            case 'assign_subpl_leader':
                return this.onAssignSubLeader( commandChunks, player )

            case 'create_ally':
                return this.onCreateAlly( commandChunks, player )

            case 'dissolve_ally':
                await player.getClan().dissolveAlly( player )
                return defaultResult

            case 'dissolve_clan':
                await this.dissolveClan( player )
                return defaultResult

            case 'change_clan_leader':
                await this.onChangeClanLeader( commandChunks, player, data.originatorId )
                return defaultResult

            case 'cancel_clan_leader_change':
                await this.onCancelClanLeaderChange( commandChunks, player, data.originatorId )
                return defaultResult

            case 'recover_clan':
                this.recoverClan( player )
                return defaultResult

            case 'increase_clan_level':
                if ( await player.getClan().attemptLevelup( player ) ) {
                    BroadcastHelper.dataToSelfBasedOnVisibility( player, MagicSkillUse( player.getObjectId(), player.getObjectId(), 5103, 1, 0, 0 ) )
                    BroadcastHelper.dataToSelfBasedOnVisibility( player, MagicSkillLaunched( player.getObjectId(), 5103, 1 ) )
                }

                return defaultResult

            case 'learn_clan_skills':
                L2VillageMasterInstance.showPledgeSkillList( player )
                return defaultResult

            case 'Subclass':
                await this.onSubclass( player, data )
                return defaultResult
        }

        return continueResult
    }

    async onCreateClan( commandChunks: Array<string>, player: L2PcInstance ): Promise<EventTerminationResult> {
        if ( commandChunks.length > 1 ) {
            let clanName = commandChunks[ 1 ]
            if ( commandChunks.length > 2 || !ClanCache.isValidClanName( clanName ) ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLAN_NAME_INCORRECT ) )
                return
            }

            await ClanCache.createClan( player, clanName )
        }

        return defaultResult
    }

    async onCreateAcademy( commandChunks: Array<string>, player: L2PcInstance ): Promise<EventTerminationResult> {
        if ( commandChunks.length > 1 ) {
            await this.createSubPledge( player, commandChunks[ 1 ], null, L2ClanValues.SUBUNIT_ACADEMY, 5 )
        }

        return defaultResult
    }

    async createSubPledge( player: L2PcInstance, clanName: string, leaderName: string, pledgeType: number, minimumClanLevel: number ): Promise<void> {
        if ( !player.isClanLeader() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ARE_NOT_AUTHORIZED_TO_DO_THAT ) )
            return
        }

        let clan: L2Clan = player.getClan()
        if ( clan.getLevel() < minimumClanLevel ) {
            if ( pledgeType === L2ClanValues.SUBUNIT_ACADEMY ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_DO_NOT_MEET_CRITERIA_IN_ORDER_TO_CREATE_A_CLAN_ACADEMY ) )
                return
            }

            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_DO_NOT_MEET_CRITERIA_IN_ORDER_TO_CREATE_A_MILITARY_UNIT ) )
            return
        }

        if ( !ClanCache.isValidClanName( clanName ) || clanName.length < 2 ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLAN_NAME_INCORRECT ) )
            return
        }

        if ( clanName.length > L2ClanValues.CLAN_NAME_MAX_LENGTH ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLAN_NAME_TOO_LONG ) )
            return
        }

        let shouldExit: boolean = _.some( ClanCache.getClans(), ( clan: L2Clan ): boolean => {
            return !!clan.getSubPledgeByName( clanName )
        } )

        if ( shouldExit ) {
            if ( pledgeType === L2ClanValues.SUBUNIT_ACADEMY ) {
                let message = new SystemMessageBuilder( SystemMessageIds.S1_ALREADY_EXISTS )
                        .addString( clanName )
                        .getBuffer()
                player.sendOwnedData( message )
                return
            }

            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ANOTHER_MILITARY_UNIT_IS_ALREADY_USING_THAT_NAME ) )
            return
        }

        let memberLeader = leaderName ? clan.getClanMemberByName( leaderName ) : null
        let isNotAcademyPledge = pledgeType !== L2ClanValues.SUBUNIT_ACADEMY

        if ( isNotAcademyPledge ) {
            if ( !memberLeader || memberLeader.getPledgeType() !== 0 ) {
                if ( pledgeType >= L2ClanValues.SUBUNIT_KNIGHT1 ) {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CAPTAIN_OF_ORDER_OF_KNIGHTS_CANNOT_BE_APPOINTED ) )
                    return
                }

                if ( pledgeType >= L2ClanValues.SUBUNIT_ROYAL1 ) {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CAPTAIN_OF_ROYAL_GUARD_CANNOT_BE_APPOINTED ) )
                    return
                }

                return
            }
        }

        let leaderId = ( isNotAcademyPledge && memberLeader ) ? memberLeader.getObjectId() : 0

        if ( !await clan.createSubPledge( player, pledgeType, leaderId, clanName ) ) {
            return
        }

        if ( isNotAcademyPledge && memberLeader ) {
            let leaderPlayer: L2PcInstance = memberLeader.getPlayerInstance()
            if ( leaderPlayer ) {
                leaderPlayer.setPledgeClass( L2ClanMember.calculatePledgeClass( leaderPlayer ) )
                leaderPlayer.sendDebouncedPacket( UserInfo )
                leaderPlayer.sendDebouncedPacket( ExBrExtraUserInfo )
            }
        }

        if ( pledgeType === L2ClanValues.SUBUNIT_ACADEMY ) {
            let message = new SystemMessageBuilder( SystemMessageIds.THE_S1S_CLAN_ACADEMY_HAS_BEEN_CREATED )
                    .addString( player.getClan().getName() )
                    .getBuffer()
            player.sendOwnedData( message )
            return
        }

        if ( pledgeType >= L2ClanValues.SUBUNIT_KNIGHT1 ) {
            let message = new SystemMessageBuilder( SystemMessageIds.THE_KNIGHTS_OF_S1_HAVE_BEEN_CREATED )
                    .addString( player.getClan().getName() )
                    .getBuffer()
            player.sendOwnedData( message )
            return
        }

        if ( pledgeType >= L2ClanValues.SUBUNIT_ROYAL1 ) {
            let message = new SystemMessageBuilder( SystemMessageIds.THE_ROYAL_GUARD_OF_S1_HAVE_BEEN_CREATED )
                    .addString( player.getClan().getName() )
                    .getBuffer()
            player.sendOwnedData( message )
            return
        }

        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLAN_CREATED ) )
    }

    async dissolveClan( player: L2PcInstance ): Promise<void> {
        if ( !player.isClanLeader() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ARE_NOT_AUTHORIZED_TO_DO_THAT ) )
            return
        }

        let clan: L2Clan = player.getClan()
        if ( clan.getAllyId() !== 0 ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_DISPERSE_THE_CLANS_IN_ALLY ) )
            return
        }

        if ( clan.isAtWar() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_DISSOLVE_WHILE_IN_WAR ) )
            return
        }

        if ( clan.getCastleId() !== 0 || clan.getHideoutId() !== 0 || clan.getFortId() !== 0 ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_DISSOLVE_WHILE_OWNING_CLAN_HALL_OR_CASTLE ) )
            return
        }

        let hasSiegeRegistration: boolean = await SiegeManager.checkIsRegisteredForAll( clan, CastleManager.getResidenceIds() )
        if ( hasSiegeRegistration ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_DISSOLVE_WHILE_IN_SIEGE ) )
            return
        }

        let hasFortRegistration: boolean = await FortSiegeManager.checkIsRegisteredForAll( clan, FortManager.getResidenceIds() )
        if ( hasFortRegistration ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_DISSOLVE_WHILE_IN_SIEGE ) )
            return
        }

        if ( player.isInSiegeArea() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_DISSOLVE_WHILE_IN_SIEGE ) )
            return
        }

        if ( clan.getDissolvingExpiryTime() > Date.now() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.DISSOLUTION_IN_PROGRESS ) )
            return
        }

        clan.setDissolvingExpiryTime( Date.now() + ConfigManager.character.getDaysToPassToDissolveAClan() * 86400000 )
        clan.scheduleUpdate()

        /*
            Clan leader should take the XP penalty of a full death
         */
        await player.calculateDeathExpPenalty( null, false )
        ClanCache.scheduleRemoveClan( clan.getId(), player.isGM() )
    }

    async onRenamePledge( commandChunks: Array<string>, player: L2PcInstance ): Promise<EventTerminationResult> {
        if ( commandChunks.length > 1 ) {
            await this.renameSubPledge( player, _.parseInt( commandChunks[ 1 ] ), _.nth( commandChunks, 2 ) )
        }

        return defaultResult
    }

    recoverClan( player: L2PcInstance ): void {
        if ( !player.isClanLeader() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ARE_NOT_AUTHORIZED_TO_DO_THAT ) )
            return
        }

        let clan: L2Clan = player.getClan()
        clan.setDissolvingExpiryTime( 0 )

        clan.scheduleUpdate()
    }

    async renameSubPledge( player: L2PcInstance, pledgeType: number, pledgeName: string ): Promise<void> {
        if ( !player.isClanLeader() || _.isEmpty( pledgeName ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ARE_NOT_AUTHORIZED_TO_DO_THAT ) )
            return
        }

        let clan: L2Clan = player.getClan()
        let subPledge: ClanPledge = player.getClan().getPledge( pledgeType )

        if ( !subPledge ) {
            player.sendMessage( 'Pledge don\'t exists.' )
            return
        }

        if ( !ClanCache.isValidClanName( pledgeName ) || pledgeName.length < 2 ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLAN_NAME_INCORRECT ) )
            return
        }

        if ( pledgeName.length > L2ClanValues.CLAN_NAME_MAX_LENGTH ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLAN_NAME_TOO_LONG ) )
            return
        }

        subPledge.name = pledgeName
        await clan.updateSubPledgeInDatabase( subPledge.type )

        clan.broadcastClanStatus()
        player.sendMessage( 'Pledge name changed.' )
    }

    async onCreateRoyal( commandChunks: Array<string>, player: L2PcInstance ): Promise<EventTerminationResult> {
        if ( commandChunks.length > 1 ) {
            await this.createSubPledge( player, commandChunks[ 1 ], _.nth( commandChunks, 2 ), L2ClanValues.SUBUNIT_ROYAL1, 6 )
        }

        return defaultResult
    }

    async onCreateKnight( commandChunks: Array<string>, player: L2PcInstance ): Promise<EventTerminationResult> {
        if ( commandChunks.length > 1 ) {
            await this.createSubPledge( player, commandChunks[ 1 ], _.nth( commandChunks, 2 ), L2ClanValues.SUBUNIT_KNIGHT1, 7 )
        }

        return defaultResult
    }

    async onAssignSubLeader( commandChunks: Array<string>, player: L2PcInstance ): Promise<EventTerminationResult> {
        if ( commandChunks.length > 1 ) {
            await this.assignSubPledgeLeader( player, commandChunks[ 1 ], _.nth( commandChunks, 2 ) )
        }

        return defaultResult
    }

    async assignSubPledgeLeader( player: L2PcInstance, clanName: string, leaderName: string ): Promise<void> {
        if ( _.isEmpty( leaderName ) ) {
            return
        }

        if ( !player.isClanLeader() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ARE_NOT_AUTHORIZED_TO_DO_THAT ) )
            return
        }

        if ( leaderName.length > 16 ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NAMING_CHARNAME_UP_TO_16CHARS ) )
            return
        }

        if ( player.getName() === leaderName ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CAPTAIN_OF_ROYAL_GUARD_CANNOT_BE_APPOINTED ) )
            return
        }

        let clan: L2Clan = player.getClan()
        let subPledge: ClanPledge = player.getClan().getSubPledgeByName( clanName )

        if ( !subPledge || subPledge.type === L2ClanValues.SUBUNIT_ACADEMY ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLAN_NAME_INCORRECT ) )
            return
        }

        let memberLeader = clan.getClanMemberByName( leaderName )
        if ( !memberLeader || memberLeader.getPledgeType() !== 0 ) {
            if ( subPledge.type >= L2ClanValues.SUBUNIT_KNIGHT1 ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CAPTAIN_OF_ORDER_OF_KNIGHTS_CANNOT_BE_APPOINTED ) )
                return
            }

            if ( subPledge.type >= L2ClanValues.SUBUNIT_ROYAL1 ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CAPTAIN_OF_ROYAL_GUARD_CANNOT_BE_APPOINTED ) )
                return
            }

            return
        }

        subPledge.leaderId = memberLeader.getObjectId()
        await clan.updateSubPledgeInDatabase( subPledge.type )

        let leaderPlayer: L2PcInstance = memberLeader.getPlayerInstance()
        if ( leaderPlayer ) {
            leaderPlayer.setPledgeClass( L2ClanMember.calculatePledgeClass( leaderPlayer ) )
            leaderPlayer.sendDebouncedPacket( UserInfo )
            leaderPlayer.sendDebouncedPacket( ExBrExtraUserInfo )
        }

        clan.broadcastClanStatus()

        let message: Buffer = new SystemMessageBuilder( SystemMessageIds.C1_HAS_BEEN_SELECTED_AS_CAPTAIN_OF_S2 )
                .addString( leaderName )
                .addString( clanName )
                .getBuffer()

        clan.broadcastDataToOnlineMembers( message )
    }

    async onCreateAlly( commandChunks: Array<string>, player: L2PcInstance ): Promise<EventTerminationResult> {
        if ( !player.isClanLeader() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ONLY_CLAN_LEADER_CREATE_ALLIANCE ) )

            return defaultResult
        }

        if ( commandChunks.length > 1 ) {
            await player.getClan().createAlly( player, commandChunks[ 1 ] )
        }

        return defaultResult
    }

    async onChangeClanLeader( commandChunks: Array<string>, player: L2PcInstance, npcObjectId: number ): Promise<void> {
        if ( !player.isClanLeader() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ARE_NOT_AUTHORIZED_TO_DO_THAT ) )
            return
        }

        if ( commandChunks.length === 1 ) {
            return
        }

        let newLeaderName = commandChunks[ 1 ]

        if ( _.toLower( player.getName() ) === _.toLower( newLeaderName ) ) {
            return
        }

        let clan: L2Clan = player.getClan()
        let member: L2ClanMember = clan.getClanMemberByName( newLeaderName )
        if ( !member ) {
            let message = new SystemMessageBuilder( SystemMessageIds.S1_DOES_NOT_EXIST )
                    .addString( newLeaderName )
                    .getBuffer()
            player.sendOwnedData( message )
            return
        }

        if ( !member.isOnline() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INVITED_USER_NOT_ONLINE ) )
            return
        }

        // To avoid clans with null clan leader, academy members shouldn't be eligible for clan leader.
        if ( member.getPlayerInstance().isAcademyMember() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.RIGHT_CANT_TRANSFERRED_TO_ACADEMY_MEMBER ) )
            return
        }

        if ( ConfigManager.character.clanLeaderInstantActivation() ) {
            await clan.setNewLeader( member )
            return
        }

        let inProgressPath = 'data/datapack/village_master/Clan/9000-07-in-progress.htm'
        let changeLeaderHtml: string = DataManager.getHtmlData().getItem( inProgressPath )
        if ( clan.getNewLeaderId() === 0 ) {
            clan.setNewLeaderId( member.getObjectId(), true )
            inProgressPath = 'data/datapack/village_master/Clan/9000-07-success.htm'
            changeLeaderHtml = DataManager.getHtmlData().getItem( inProgressPath )
        }

        player.sendOwnedData( NpcHtmlMessagePath( changeLeaderHtml, inProgressPath, player.getObjectId(), npcObjectId ) )
    }

    async onCancelClanLeaderChange( commandChunks: Array<string>, player: L2PcInstance, npcObjectId: number ): Promise<void> {
        if ( !player.isClanLeader() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ARE_NOT_AUTHORIZED_TO_DO_THAT ) )
            return
        }

        let clan = player.getClan()
        if ( clan.getNewLeaderId() !== 0 ) {
            clan.setNewLeaderId( 0, true )

            let cancelPath = 'data/datapack/village_master/Clan/9000-07-canceled.htm'
            player.sendOwnedData( NpcHtmlMessagePath( DataManager.getHtmlData().getItem( cancelPath ), cancelPath, player.getObjectId(), npcObjectId ) )
            return
        }

        player.sendOwnedData( NpcHtmlMessage( cancelClanLeaderChangeMessage, player.getObjectId(), npcObjectId ) )
        return
    }

    async onSubclass( player: L2PcInstance, data: NpcBypassEvent ): Promise<void> {
        if ( player.isCastingNow() || player.isAllSkillsDisabled() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SUBCLASS_NO_CHANGE_OR_CREATE_WHILE_SKILL_IN_USE ) )
            return
        }

        if ( player.getTransformation() != null ) {
            let path = 'data/html/villagemaster/SubClass_NoTransformed.htm'
            let html: string = DataManager.getHtmlData().getItem( path )
            player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), data.originatorId ) )
            return
        }

        if ( player.hasSummon() ) {
            let path = 'data/html/villagemaster/SubClass_NoSummon.htm'
            let html: string = DataManager.getHtmlData().getItem( path )
            player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), data.originatorId ) )
            return
        }

        if ( !player.isInventoryUnder90( true ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_SUBCLASS_WHILE_INVENTORY_FULL ) )
            return
        }

        if ( player.getWeightPenalty() >= 2 ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_SUBCLASS_WHILE_OVERWEIGHT ) )
            return
        }

        let [ html, path ] = await this.getSubclassHtml( data, player )
        if ( html ) {
            player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), data.originatorId ) )
        }

        return
    }

    getSubClassFail(): [ string, string ] {
        let path = 'data/html/villagemaster/SubClass_Fail.htm'
        return [ DataManager.getHtmlData().getItem( path ), path ]
    }

    async getSubclassHtml( data: NpcBypassEvent, player: L2PcInstance ): Promise<[ string, string ]> {
        let paramOne = 0
        let paramTwo = 0
        let { command } = data
        let cmdChoice = _.parseInt( command.substring( 9, 10 ).trim() )

        let endIndex = command.indexOf( ' ', 11 )
        if ( endIndex === -1 ) {
            endIndex = command.length
        }

        if ( command.length > 11 ) {
            paramOne = _.parseInt( command.substring( 11, endIndex ).trim() )
            if ( command.length > endIndex ) {
                paramTwo = _.parseInt( command.substring( endIndex ).trim() )
            }
        }

        let npc = L2World.getObjectById( data.originatorId ) as L2VillageMasterInstance

        switch ( cmdChoice ) {
            case 0:
                let racePath = this.getSubclassMenu( player.getRace() )
                return [ DataManager.getHtmlData().getItem( racePath ), racePath ]

            case 1: // Add Subclass - Initial
                // Avoid giving player an option to add a new sub class, if they have max sub-classes already.
                if ( player.getTotalSubClasses() >= ConfigManager.character.getMaxSubclass() ) {
                    return this.getSubClassFail()
                }

                let subsAvailable = this.getAvailableSubClasses( player, npc )
                if ( !_.isEmpty( subsAvailable ) ) {
                    let path = 'data/html/villagemaster/SubClass_Add.htm'
                    let html: string = DataManager.getHtmlData().getItem( path )
                    let listHtml: Array<string> = _.map( subsAvailable, ( subClass: IPlayerClass ) => {
                        return `<a action="bypass -h Subclass 4 ${ subClass.classId }" msg="1268;${ DataManager.getClassListData().getClassInfo( subClass.classId ).name }">${ ClassId.getClientCode( subClass.classId ) }</a><br>`
                    } )

                    return [ html.replace( /%list%/g, listHtml.join( '' ) ), path ]
                }

                if ( ( player.getRace() === Race.ELF ) || ( player.getRace() === Race.DARK_ELF ) ) {
                    let path = 'data/html/villagemaster/SubClass_Fail_Elves.htm'
                    return [ DataManager.getHtmlData().getItem( path ), path ]
                }

                if ( player.getRace() === Race.KAMAEL ) {
                    let path = 'data/html/villagemaster/SubClass_Fail_Kamael.htm'
                    return [ DataManager.getHtmlData().getItem( path ), path ]
                }

                player.sendMessage( 'There are no sub classes available at this time.' )
                return

            case 2: // Change Class - Initial
                if ( _.isEmpty( player.getSubClasses() ) ) {
                    let path = 'data/html/villagemaster/SubClass_ChangeNo.htm'
                    return [ DataManager.getHtmlData().getItem( path ), path ]
                }

                let messages: Array<string> = []

                if ( npc.checkVillageMaster( player.getBaseClass() ) ) {
                    messages.push( `<a action="bypass -h Subclass 5 0">${ ClassId.getClientCode( player.getBaseClass() ) }</a><br>` )
                }

                _.each( player.getSubClasses(), ( subClass: SubClass ) => {
                    if ( npc.checkVillageMaster( subClass.getClassId() ) ) {
                        messages.push( `<a action="bypass -h Subclass 5 ${ subClass.getClassIndex() }">${ ClassId.getClientCode( subClass.getClassId() ) }</a><br>` )
                    }
                } )

                if ( messages.length > 0 ) {
                    let path = 'data/html/villagemaster/SubClass_Change.htm'
                    return [ DataManager.getHtmlData().getItem( path ).replace( /%list%/g, messages.join( '' ) ), path ]
                }

                let changeNotFoundPath = 'data/html/villagemaster/SubClass_ChangeNotFound.htm'
                return [ DataManager.getHtmlData().getItem( changeNotFoundPath ), changeNotFoundPath ]

            case 3: // Change/Cancel Subclass - Initial
                if ( _.isEmpty( player.getSubClasses() ) ) {
                    let path = 'data/html/villagemaster/SubClass_ModifyEmpty.htm'
                    return [ DataManager.getHtmlData().getItem( path ), path ]
                }

                if ( player.getTotalSubClasses() > 3 ) {
                    let classIndex = 0
                    let lines: Array<string> = _.map( player.getSubClasses(), ( subClass: SubClass ) => {
                        classIndex++
                        return `Sub-class ${ classIndex }<br><a action="bypass -h Subclass 6 ${ subClass.getClassIndex() }">${ ClassId.getClientCode( subClass.getClassId() ) }</a><br>`
                    } )

                    let path = 'data/html/villagemaster/SubClass_ModifyCustom.htm'
                    return [ DataManager.getHtmlData().getItem( path )
                                        .replace( /%list%/g, lines.join( '' ) ),
                             path,
                    ]
                }

                let modifyPath = 'data/html/villagemaster/SubClass_Modify.htm'
                let modifySubclassHtml: string = DataManager.getHtmlData().getItem( modifyPath )

                if ( player.getSubClasses()[ 1 ] ) {
                    modifySubclassHtml = modifySubclassHtml.replace( /%sub1%/g, ClassId.getClientCode( player.getSubClasses()[ 1 ].getClassId() ) )
                } else {
                    modifySubclassHtml = modifySubclassHtml.replace( '<a action="bypass -h Subclass 6 1">%sub1%</a><br>', '' )
                    modifyPath = ''
                }

                if ( player.getSubClasses()[ 2 ] ) {
                    modifySubclassHtml = modifySubclassHtml.replace( /%sub2%/g, ClassId.getClientCode( player.getSubClasses()[ 2 ].getClassId() ) )
                } else {
                    modifySubclassHtml = modifySubclassHtml.replace( '<a action="bypass -h Subclass 6 2">%sub2%</a><br>', '' )
                    modifyPath = ''
                }

                if ( player.getSubClasses()[ 3 ] ) {
                    modifySubclassHtml = modifySubclassHtml.replace( /%sub3%/g, ClassId.getClientCode( player.getSubClasses()[ 3 ].getClassId() ) )
                } else {
                    modifySubclassHtml = modifySubclassHtml.replace( '<a action="bypass -h Subclass 6 3">%sub3%</a><br>', '' )
                    modifyPath = ''
                }

                return [ modifySubclassHtml, modifyPath ]

            case 4: // Add Subclass - Action (Subclass 4 x[x])
                // If the character is less than level 75 on any of their previously chosen classes then disallow them to change to their most recently added sub-class choice.
                // TODO : rate protection

                let allowAddition = player.getTotalSubClasses() < ConfigManager.character.getMaxSubclass() || player.getLevel() < 75

                if ( allowAddition ) {
                    if ( !_.isEmpty( player.getSubClasses() ) ) {
                        allowAddition = !_.some( player.getSubClasses(), ( subClass: SubClass ) => {
                            return subClass.getLevel() < 75
                        } )
                    }
                }

                // If quest checking is enabled, verify if the character has completed the Mimir's Elixir (Path to Subclass)
                // and Fate's Whisper (A Grade Weapon) quests by checking for instances of their unique reward items.
                // If they both exist, remove both unique items and continue with adding the sub-class.
                if ( allowAddition && !ConfigManager.character.subclassWithoutQuests() ) {
                    allowAddition = npc.checkQuests( player )
                }

                if ( allowAddition && this.isValidNewSubClass( player, paramOne, npc ) ) {
                    if ( !await player.addSubClass( paramOne, player.getTotalSubClasses() + 1 ) ) {
                        return
                    }

                    await player.changeActiveClass( player.getTotalSubClasses() )
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ADD_NEW_SUBCLASS ) )

                    let path = 'data/html/villagemaster/SubClass_AddOk.htm'
                    return [ DataManager.getHtmlData().getItem( path ), path ]
                }

                return this.getSubClassFail()

            case 5: // Change Class - Action
                // If the character is less than level 75 on any of their previously chosen classes then disallow them to change to their most recently added sub-class choice. Note: paramOne = classIndex
                // TODO : rate protection

                if ( player.getClassIndex() === paramOne ) {
                    let path = 'data/html/villagemaster/SubClass_Current.htm'
                    return [ DataManager.getHtmlData().getItem( path ), path ]
                }

                if ( paramOne === 0 ) {
                    if ( !npc.checkVillageMaster( player.getBaseClass() ) ) {
                        return
                    }
                } else {
                    let subClass: SubClass = player.getSubClasses()[ paramOne ]
                    if ( !subClass || !npc.checkVillageMaster( subClass.getClassId() ) ) {
                        return
                    }
                }

                await player.changeActiveClass( paramOne )
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SUBCLASS_TRANSFER_COMPLETED ) )

                return

            case 6: // Change/Cancel Subclass - Choice
                if ( ( paramOne < 1 ) || ( paramOne > ConfigManager.character.getMaxSubclass() ) ) {
                    return
                }

                let subClasses: Array<IPlayerClass> = this.getAvailableSubClasses( player, npc )
                if ( _.isEmpty( subClasses ) ) {
                    player.sendMessage( 'There are no sub classes available at this time.' )
                    return
                }

                let changeSubclassLines: Array<string> = _.map( subClasses, ( subClass: IPlayerClass ) => {
                    return `<a action="bypass -h Subclass 7 ${ paramOne } ${ subClass.classId }" msg="1445;">${ ClassId.getClientCode( subClass.classId ) }</a><br>`
                } )

                let htmlPath: string = 'data/html/villagemaster/SubClass_ModifyChoice.htm'
                switch ( paramOne ) {
                    case 1:
                        htmlPath = 'data/html/villagemaster/SubClass_ModifyChoice1.htm'
                        break

                    case 2:
                        htmlPath = 'data/html/villagemaster/SubClass_ModifyChoice2.htm'
                        break

                    case 3:
                        htmlPath = 'data/html/villagemaster/SubClass_ModifyChoice3.htm'
                        break
                }

                return [
                    DataManager.getHtmlData().getItem( htmlPath ).replace( /%list%/g, changeSubclassLines.join( '' ) ),
                    htmlPath,
                ]

            case 7: // Change Subclass - Action
                // TODO : rate limit performing change of class

                if ( !this.isValidNewSubClass( player, paramTwo, npc ) ) {
                    return
                }

                if ( await player.modifySubClass( paramOne, paramTwo ) ) {

                    player.abortCast()
                    await player.stopAllEffectsExceptThoseThatLastThroughDeath()
                    await player.stopAllEffectsNotStayOnSubclassChange()
                    player.stopCubics()

                    await player.changeActiveClass( paramOne )

                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ADD_NEW_SUBCLASS ) )

                    let path = 'data/html/villagemaster/SubClass_ModifyOk.htm'
                    return [ DataManager.getHtmlData().getItem( path )
                                        .replace( /%name%/g, ClassId.getClientCode( paramTwo ) ), path ]
                }

                await player.changeActiveClass( 0 )
                player.sendMessage( 'The sub class could not be added, you have been reverted to your base class.' )

                return
        }
    }

    getSubclassMenu( race: Race ): string {
        if ( ConfigManager.character.subclassEverywhere() || race !== Race.KAMAEL ) {
            return 'data/html/villagemaster/SubClass.htm'
        }

        return 'data/html/villagemaster/SubClass_NoOther.htm'
    }

    isValidNewSubClass( player: L2PcInstance, classId: number, npc: L2VillageMasterInstance ): boolean {
        if ( !npc.checkVillageMaster( classId ) ) {
            return false
        }

        let shouldExit: boolean = _.some( player.getSubClasses(), ( subClass: SubClass ): boolean => {
            return subClass.getClassId() === classId || ClassId.isChildOf( ClassId.getClassIdByIdentifier( classId ), ClassId.getNameById( subClass.getClassId() ) )
        } )

        if ( shouldExit ) {
            return false
        }

        let currentBaseId = player.getBaseClass()
        let currentClassId: IClassId = ClassId.getClassIdByIdentifier( currentBaseId )

        let baseClassId = currentBaseId
        if ( currentClassId.level > 2 ) {
            baseClassId = ClassId.getClassId( currentClassId.parent ).id
        }

        let basePlayerClass: IPlayerClass = getPlayerClassById( baseClassId )
        let allSubclasses: Array<IPlayerClass> = getAvailableSubclasses( basePlayerClass, player )
        return allSubclasses.some( ( playerClass: IPlayerClass ) => {
            return playerClass.classId === classId
        } )
    }

    getAvailableSubClasses( player: L2PcInstance, npc: L2VillageMasterInstance ): Array<IPlayerClass> {
        let currentBaseId = player.getBaseClass()
        let playerClassId: IClassId = ClassId.getClassIdByIdentifier( currentBaseId )
        let baseClassId: number = playerClassId.level > 2 ? ClassId.getClassId( playerClassId.parent ).id : currentBaseId

        // If the race of your main class is Elf or Dark Elf, you may not select each class as a subclass to the other class.
        // If the race of your main class is Kamael, you may not subclass any other race If the race of your main class is NOT Kamael,
        // you may not subclass any Kamael class You may not select Overlord and Warsmith class as a subclass.
        // You may not select a similar class as the subclass.
        // The occupations classified as similar classes are as follows: Treasure Hunter, Plainswalker and Abyss Walker Hawkeye,
        // Silver Ranger and Phantom Ranger Paladin, Dark Avenger, Temple Knight
        // and Shillien Knight Warlocks, Elemental Summoner and Phantom Summoner Elder and Shillien Elder Swordsinger and Bladedancer Sorcerer,
        // Spellsinger and Spellhowler Also, Kamael have a special, hidden 4 subclass, the inspector, which can only be taken if you have already completed the other two Kamael subclasses
        let basePlayerClass: IPlayerClass = getPlayerClassById( baseClassId )
        let availSubs: Array<IPlayerClass> = getAvailableSubclasses( basePlayerClass, player )

        return availSubs.filter( ( playerClass: IPlayerClass ): boolean => {
            if ( !npc.checkVillageMaster( playerClass.classId ) ) {
                return false
            }

            let currentClassId: IClassId = ClassId.getClassIdByIdentifier( playerClass.classId )

            return _.some( player.getSubClasses(), ( subClass: SubClass ): boolean => {
                return subClass.getClassId() === playerClass.classId || ClassId.isChildOf( currentClassId, ClassId.getNameById( subClass.getClassId() ) )
            } )
        } )
    }
}