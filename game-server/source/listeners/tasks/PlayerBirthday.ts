import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import { EventType, PlayerLoginEvent } from '../../gameService/models/events/EventType'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { L2World } from '../../gameService/L2World'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { MailManager } from '../../gameService/instancemanager/MailManager'
import { MailMessage } from '../../gameService/models/mail/MailMessage'
import { ConfigManager } from '../../config/ConfigManager'
import { SendBySystem } from '../../gameService/models/mail/SendBySystem'
import Timeout = NodeJS.Timeout
import { MailAttachments } from '../../gameService/models/mail/MailAttachments'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import moment from 'moment'
import { DimensionalTransferManager } from '../../gameService/cache/DimensionalTransferManager'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'
import { ExNotifyDimensionalItem } from '../../gameService/packets/send/ExNotifyPremiumItem'
import aigle from 'aigle'
import _ from 'lodash'
import { ExNotifyBirthday } from '../../gameService/packets/send/ExNotifyBirthday'

type GiftDeliveryType = ( player : L2PcInstance ) => Promise<void>

export class PlayerBirthday extends ListenerLogic {
    giveGiftTask : Timeout

    constructor() {
        super( 'PlayerBirthday', 'listeners/tasks/PlayerBirthday.ts' )
        this.giveGiftTask = setInterval( this.runGiftTask.bind( this ), GeneralHelper.minutesToMillis( 60 ) )
    }

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.General,
                eventType: EventType.PlayerLogin,
                method: this.onPlayerLogin.bind( this ),
                ids: null
            }
        ]
    }

    async onPlayerLogin( data : PlayerLoginEvent ) : Promise<void> {
        let player = L2World.getPlayer( data.playerId )

        let daysLeft = Helper.daysTillBirthday( player )
        if ( daysLeft === 0 ) {
            await Helper.getBirthdayGiftDelivery()( player )

            player.sendOwnedData( ExNotifyBirthday () )
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOUR_BIRTHDAY_GIFT_HAS_ARRIVED ) )
            return
        }

        let packet = new SystemMessageBuilder( SystemMessageIds.THERE_ARE_S1_DAYS_UNTIL_YOUR_CHARACTERS_BIRTHDAY )
                .addNumber( daysLeft )
                .getBuffer()
        player.sendOwnedData( packet )
    }

    /*
        TODO : consider offline players, convert to use database query, run once per 24 hours
        - consider if player has not logged in for a year, he would not get a present (stale account)
        - consider dimensional merchant due to mail can be full of messages
     */
    async runGiftTask() {
        const method : GiftDeliveryType = Helper.getBirthdayGiftDelivery()
        return aigle.resolve( L2World.getAllPlayers() ).eachSeries( async ( player : L2PcInstance ) : Promise<void> => {
            if ( Helper.daysTillBirthday( player ) === 0 ) {
                await method( player )
            }
        } )
    }
}

const Helper = {
    async giveBirthdayGiftMail( player : L2PcInstance ) : Promise<void> {
        let content : string = ConfigManager.general.getBirthdayMailText()
                .replace( /%playerName%/, player.getName() )
        let message = MailMessage.asSystemMessage( player.getObjectId(), ConfigManager.general.getBirthdayMailSubject(), content, SendBySystem.Alegria )

        if ( ConfigManager.general.getBirthdayGifts().length > 0 ) {
            let attachments : MailAttachments = message.createAttachments()

            await aigle.resolve( ConfigManager.general.getBirthdayGifts() ).each( async ( itemId : number ) => {
                return attachments.addItem( itemId, 1, 0, 'PlayerBirthday' )
            } )
        }

        Helper.recordGiftDate( player )
        return MailManager.sendMessage( message )
    },

    async giveBirthdayGiftDimensionalTransfer( player : L2PcInstance ) : Promise<void> {
        await aigle.resolve( ConfigManager.general.getBirthdayGifts() ).each( async ( itemId : number ) => {
            return DimensionalTransferManager.addItem( player.getObjectId(), itemId, 1, 'Alegria' )
        } )

        Helper.recordGiftDate( player )
        player.sendOwnedData( ExNotifyDimensionalItem() )
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.BIRTHDAY_GIFT_DELIVERED_VISIT_THE_DIMENSIONAL_MERCHANT ) )
    },

    daysTillBirthday( player : L2PcInstance ) : number {
        let currentDate = moment()
        let lastBirthdayGiftDate : number = PlayerVariablesManager.get( player.getObjectId(), PlayerBirthday.name ) as number
        let createDate = moment( _.defaultTo( lastBirthdayGiftDate, player.getCreateDate() ) ).add( 1, 'year' )

        // Characters with a February 29 creation date will receive a gift on February 28.
        if ( createDate.month() === 1 && createDate.day() === 28 ) {
            createDate.subtract( 1, 'day' )
        }

        if ( createDate.isSame( currentDate, 'day' ) ) {
            return 0
        }

        return Math.max( createDate.diff( currentDate, 'days' ), 0 )
    },

    recordGiftDate( player : L2PcInstance ) : void {
        let lastBirthdayGiftDate : number = PlayerVariablesManager.get( player.getObjectId(), PlayerBirthday.name ) as number
        let createDate = moment( _.defaultTo( lastBirthdayGiftDate, player.getCreateDate() ) ).add( 1, 'year' )

        PlayerVariablesManager.set( player.getObjectId(), PlayerBirthday.name, createDate.valueOf() )
    },

    getBirthdayGiftDelivery() : GiftDeliveryType {
        return ConfigManager.general.getBirthdayGiftDelivery() === 'DimensionalTransfer' ? Helper.giveBirthdayGiftDimensionalTransfer : Helper.giveBirthdayGiftMail
    }
}