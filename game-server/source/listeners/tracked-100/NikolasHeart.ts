import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const HEAD_BLACKSMITH_KUSTO = 30512
const MAESTRO_NIKOLA = 30621
const RESEARCHER_LORAIN = 30673

const LORAINES_CERTIFICATE = 10362
const METALLOGRAPH = 10368

const minimumLevel = 41
const maximumLevel = 47

export class NikolasHeart extends ListenerLogic {
    constructor() {
        super( 'Q00187_NikolasHeart', 'listeners/tracked/NikolasHeart.ts' )
        this.questId = 187
        this.questItemIds = [ METALLOGRAPH ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00187_NikolasHeart'
    }

    getQuestStartIds(): Array<number> {
        return [ RESEARCHER_LORAIN ]
    }

    getTalkIds(): Array<number> {
        return [ HEAD_BLACKSMITH_KUSTO, RESEARCHER_LORAIN, MAESTRO_NIKOLA ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30673-03.htm':
                if ( state.isCreated() ) {
                    state.startQuest()
                    state.setMemoState( 1 )

                    await QuestHelper.giveSingleItem( player, METALLOGRAPH, 1 )
                    await QuestHelper.takeSingleItem( player, LORAINES_CERTIFICATE, -1 )

                    break
                }

                return

            case '30512-02.html':
                if ( state.isMemoState( 2 ) ) {
                    break
                }

                return

            case '30512-03.html':
                if ( state.isMemoState( 2 ) ) {
                    await QuestHelper.giveAdena( player, 93383, true )

                    if ( player.getLevel() < maximumLevel ) {
                        await QuestHelper.addExpAndSp( player, 285935, 18711 )
                    }

                    await state.exitQuest( false, true )
                    break
                }

                return

            case '30621-02.html':
                if ( state.isMemoState( 1 ) ) {
                    break
                }

                return

            case '30621-03.html':
                if ( state.isMemoState( 1 ) ) {
                    state.setMemoState( 2 )
                    state.setConditionWithSound( 2, true )

                    break
                }

                return


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === RESEARCHER_LORAIN ) {
                    if ( player.hasQuestCompleted( 'Q00185_NikolasCooperation' ) && QuestHelper.hasQuestItem( player, LORAINES_CERTIFICATE ) ) {
                        return this.getPath( player.getLevel() >= minimumLevel ? '30673-01.htm' : '30673-02.htm' )
                    }
                }

                break

            case QuestStateValues.STARTED:
                let memoState = state.getMemoState()

                switch ( data.characterNpcId ) {
                    case RESEARCHER_LORAIN:
                        if ( memoState >= 1 ) {
                            return this.getPath( '30673-04.html' )
                        }

                        break

                    case HEAD_BLACKSMITH_KUSTO:
                        if ( memoState === 2 ) {
                            return this.getPath( '30512-01.html' )
                        }

                        break

                    case MAESTRO_NIKOLA:
                        if ( memoState === 1 ) {
                            return this.getPath( '30621-01.html' )
                        }

                        if ( memoState === 2 ) {
                            return this.getPath( '30621-04.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === RESEARCHER_LORAIN ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}