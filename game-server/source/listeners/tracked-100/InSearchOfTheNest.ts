import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const PIERCE = 31553
const SCOUTS_CORPSE = 32015
const KAHMAN = 31554
const SCOUTS_NOTE = 14858

const minimumLevel = 81

export class InSearchOfTheNest extends ListenerLogic {
    constructor() {
        super( 'Q00109_InSearchOfTheNest', 'listeners/tracked/InSearchOfTheNest.ts' )
        this.questId = 109
        this.questItemIds = [
            SCOUTS_NOTE,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00109_InSearchOfTheNest'
    }

    getQuestStartIds(): Array<number> {
        return [ PIERCE ]
    }

    getTalkIds(): Array<number> {
        return [
            PIERCE,
            SCOUTS_CORPSE,
            KAHMAN,
        ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state ) {
            return QuestHelper.getNoQuestMessagePath()
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31553-0.htm':
                state.startQuest()
                break

            case '32015-2.html':
                await QuestHelper.giveSingleItem( player, SCOUTS_NOTE, 1 )
                state.setConditionWithSound( 2, true )
                break

            case '31553-3.html':
                await QuestHelper.takeSingleItem( player, SCOUTS_NOTE, -1 )
                state.setConditionWithSound( 3, true )
                break

            case '31554-2.html':
                await QuestHelper.giveAdena( player, 161500, true )
                await QuestHelper.addExpAndSp( player, 701500, 50000 )
                await state.exitQuest( false, true )
                break
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case PIERCE:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() < minimumLevel ? '31553-0a.htm' : '31553-0b.htm' )

                    case QuestStateValues.STARTED:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '31553-1.html' )

                            case 2:
                                return this.getPath( '31553-2.html' )

                            case 3:
                                return this.getPath( '31553-3a.html' )
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case SCOUTS_CORPSE:
                if ( state.isStarted() ) {
                    if ( state.isCondition( 1 ) ) {
                        return this.getPath( '32015-1.html' )
                    }

                    if ( state.isCondition( 2 ) ) {
                        return this.getPath( '32015-3.html' )
                    }
                }

                break

            case KAHMAN:
                if ( state.isStarted() && state.isCondition( 3 ) ) {
                    return this.getPath( '31554-1.html' )
                }

                break
        }

    }
}