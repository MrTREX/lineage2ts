import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestStateValues } from '../../gameService/models/quest/State'

const OBI = 32052
const ABEY = 32053
const GHOST_OF_A_RAILROAD_ENGINEER = 32054
const GHOST_OF_AN_ANCIENT_RAILROAD_ENGINEER = 32055
const BOX = 32076

const ENGRAVED_HAMMER = 8488
const BOOK_OF_GREY_STAR = 8495

const BANDIT_WARRIOR = 22023
const BANDIT_INSPECTOR = 22024

const minimumLevel = 39
const monsterRewardChances = {
    BANDIT_WARRIOR: 0.179,
    BANDIT_INSPECTOR: 0.1,
}

export class TheOceanOfDistantStars extends ListenerLogic {
    constructor() {
        super( 'Q00117_TheOceanOfDistantStars', 'listeners/tracked/TheOceanOfDistantStars.ts' )
        this.questId = 117
        this.questItemIds = [ ENGRAVED_HAMMER, BOOK_OF_GREY_STAR ]
    }

    getAttackableKillIds(): Array<number> {
        return [ BANDIT_WARRIOR, BANDIT_INSPECTOR ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00117_TheOceanOfDistantStars'
    }

    getQuestStartIds(): Array<number> {
        return [ ABEY ]
    }

    getTalkIds(): Array<number> {
        return [ ABEY, GHOST_OF_A_RAILROAD_ENGINEER, GHOST_OF_AN_ANCIENT_RAILROAD_ENGINEER, BOX, OBI ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), 7, 3, L2World.getObjectById( data.targetId ) as L2Npc )

        if ( !state ) {
            return
        }

        if ( Math.random() > QuestHelper.getAdjustedChance( BOOK_OF_GREY_STAR, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        await QuestHelper.giveSingleItem( state.getPlayer(), BOOK_OF_GREY_STAR, 1 )
        state.setConditionWithSound( 8 )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32053-02.htm':
                state.setMemoState( 1 )
                state.startQuest()

                break

            case '32053-06.html':
                if ( state.isMemoState( 3 ) ) {
                    state.setMemoState( 4 )
                    state.setConditionWithSound( 4, true )
                    break
                }

                return

            case '32053-09.html':
                if ( state.isMemoState( 5 ) && QuestHelper.hasQuestItem( player, ENGRAVED_HAMMER ) ) {
                    state.setMemoState( 6 )
                    state.setConditionWithSound( 6, true )
                    break
                }

                return

            case '32054-02.html':
                if ( state.isMemoState( 9 ) ) {
                    break
                }

                return

            case '32054-03.html':
                if ( state.isMemoState( 9 ) ) {
                    await QuestHelper.giveAdena( player, 17647, true )
                    await QuestHelper.addExpAndSp( player, 107387, 7369 )
                    await state.exitQuest( false, true )

                    break
                }

                return
            case '32055-02.html':
                if ( state.isMemoState( 1 ) ) {
                    state.setMemoState( 2 )
                    state.setConditionWithSound( 2, true )
                    break
                }

                return

            case '32055-05.html':
                if ( state.isMemoState( 8 ) ) {
                    if ( QuestHelper.hasQuestItem( player, ENGRAVED_HAMMER ) ) {
                        state.setMemoState( 9 )
                        state.setConditionWithSound( 10, true )
                        await QuestHelper.takeSingleItem( player, ENGRAVED_HAMMER, -1 )
                        break
                    }

                    return this.getPath( '32055-06.html' )
                }

                return

            case '32076-02.html':
                if ( state.isMemoState( 4 ) ) {
                    state.setMemoState( 5 )
                    state.setConditionWithSound( 5, true )
                    await QuestHelper.giveSingleItem( player, ENGRAVED_HAMMER, 1 )
                    break
                }

                return

            case '32052-02.html':
                if ( state.isMemoState( 2 ) ) {
                    state.setMemoState( 3 )
                    state.setConditionWithSound( 3, true )
                    break
                }

                return

            case '32052-05.html':
                if ( state.isMemoState( 6 ) ) {
                    state.setMemoState( 7 )
                    state.setConditionWithSound( 7, true )
                    break
                }

                return

            case '32052-07.html':
                if ( state.isMemoState( 7 ) && QuestHelper.hasQuestItem( player, BOOK_OF_GREY_STAR ) ) {
                    state.setMemoState( 8 )
                    state.setConditionWithSound( 9, true )
                    await QuestHelper.takeSingleItem( player, BOOK_OF_GREY_STAR, -1 )
                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '32053-01.htm' : '32053-03.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case ABEY:
                        switch ( state.getMemoState() ) {
                            case 1:
                                return this.getPath( '32053-04.html' )

                            case 3:
                                return this.getPath( '32053-05.html' )

                            case 4:
                                return this.getPath( '32053-07.html' )

                            case 5:
                                if ( QuestHelper.hasQuestItem( player, ENGRAVED_HAMMER ) ) {
                                    return this.getPath( '32053-08.html' )
                                }

                                break

                            case 6:
                                return this.getPath( '32053-10.html' )
                        }

                        break

                    case GHOST_OF_A_RAILROAD_ENGINEER:
                        if ( state.isMemoState( 9 ) ) {
                            return this.getPath( '32054-01.html' )
                        }

                        break

                    case GHOST_OF_AN_ANCIENT_RAILROAD_ENGINEER:
                        switch ( state.getMemoState() ) {
                            case 1:
                                return this.getPath( '32055-01.html' )

                            case 2:
                                return this.getPath( '32055-03.html' )

                            case 8:
                                return this.getPath( '32055-04.html' )

                            case 9:
                                return this.getPath( '32055-07.html' )
                        }

                        break

                    case BOX:
                        if ( state.isMemoState( 4 ) ) {
                            return this.getPath( '32076-01.html' )
                        }

                        if ( state.isMemoState( 5 ) ) {
                            return this.getPath( '32076-03.html' )
                        }

                        break

                    case OBI:
                        switch ( state.getMemoState() ) {
                            case 2:
                                return this.getPath( '32052-01.html' )

                            case 3:
                                return this.getPath( '32052-03.html' )

                            case 6:
                                return this.getPath( '32052-04.html' )

                            case 7:
                                if ( QuestHelper.hasQuestItem( player, BOOK_OF_GREY_STAR ) ) {
                                    return this.getPath( '32052-06.html' )
                                }

                                return this.getPath( '32052-08.html' )

                            case 8:
                                return this.getPath( '32052-09.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === ABEY ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}