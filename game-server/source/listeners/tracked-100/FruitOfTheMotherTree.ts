import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'

const ANDELLIA = 30362
const THALIA = 30371

const ANDELLRIAS_LETTER = 1036
const MOTHERTREE_FRUIT = 1037

const minimumLevel = 3

export class FruitOfTheMotherTree extends ListenerLogic {
    constructor() {
        super( 'Q00161_FruitOfTheMotherTree', 'listeners/tracked/FruitOfTheMotherTree.ts' )
        this.questId = 161
        this.questItemIds = [ ANDELLRIAS_LETTER, MOTHERTREE_FRUIT ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00161_FruitOfTheMotherTree'
    }

    getQuestStartIds(): Array<number> {
        return [ ANDELLIA ]
    }

    getTalkIds(): Array<number> {
        return [ ANDELLIA, THALIA ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30362-04.htm':
                state.startQuest()
                let player = L2World.getPlayer( data.playerId )
                await QuestHelper.giveSingleItem( player, ANDELLRIAS_LETTER, 1 )

                break

            case '30371-03.html':
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case ANDELLIA:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        if ( player.getRace() === Race.ELF ) {
                            return this.getPath( player.getLevel() >= minimumLevel ? '30362-03.htm' : '30362-02.htm' )
                        }

                        return this.getPath( '30362-01.htm' )

                    case QuestStateValues.STARTED:
                        if ( state.isCondition( 1 ) ) {
                            return this.getPath( '30362-05.html' )
                        }

                        if ( state.isCondition( 2 ) && await QuestHelper.hasQuestItem( player, MOTHERTREE_FRUIT ) ) {
                            await QuestHelper.giveAdena( player, 1000, true )
                            await QuestHelper.addExpAndSp( player, 1000, 0 )
                            await state.exitQuest( false, true )

                            return this.getPath( '30362-06.html' )
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case THALIA:
                if ( !state.isStarted() ) {
                    break
                }

                if ( state.isCondition( 1 ) && QuestHelper.hasQuestItem( player, ANDELLRIAS_LETTER ) ) {
                    await QuestHelper.takeSingleItem( player, ANDELLRIAS_LETTER, -1 )
                    await QuestHelper.giveSingleItem( player, MOTHERTREE_FRUIT, 1 )
                    state.setConditionWithSound( 2, true )

                    return this.getPath( '30371-01.html' )
                }

                if ( state.isCondition( 2 ) && QuestHelper.hasQuestItem( player, MOTHERTREE_FRUIT ) ) {
                    return this.getPath( '30371-02.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}