import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { TerritoryWarManager } from '../../gameService/instancemanager/TerritoryWarManager'

const RAPIDUS = 36479
const CLOAK = 14603
const minimumLevel = 80

export class StepsForHonor extends ListenerLogic {
    constructor() {
        super( 'Q00176_StepsForHonor', 'listeners/tracked/StepsForHonor.ts' )
        this.questId = 176
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00176_StepsForHonor'
    }

    getQuestStartIds(): Array<number> {
        return [ RAPIDUS ]
    }

    getTalkIds(): Array<number> {
        return [ RAPIDUS ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || data.eventName !== '36479-04.html' ) {
            return
        }

        state.startQuest()

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '36479-03.html' : '36479-02.html' )

            case QuestStateValues.STARTED:
                if ( TerritoryWarManager.isTWInProgress ) {
                    return this.getPath( '36479-05.html' )
                }

                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '36479-06.html' )

                    case 2:
                        state.setConditionWithSound( 3, true )
                        return this.getPath( '36479-07.html' )

                    case 3:
                        return this.getPath( '36479-08.html' )

                    case 4:
                        state.setConditionWithSound( 5, true )
                        return this.getPath( '36479-09.html' )

                    case 5:
                        return this.getPath( '36479-10.html' )

                    case 6:
                        state.setConditionWithSound( 7, true )
                        return this.getPath( '36479-11.html' )

                    case 7:
                        return this.getPath( '36479-12.html' )

                    case 8:
                        await QuestHelper.rewardSingleItem( player, CLOAK, 1 )
                        await state.exitQuest( false, true )

                        return this.getPath( '36479-13.html' )

                }

                break

            case QuestStateValues.COMPLETED:
                return this.getPath( '36479-01.html' )

        }

        return QuestHelper.getNoQuestMessagePath()
    }
}