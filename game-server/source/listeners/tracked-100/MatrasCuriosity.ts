import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

const MATRAS = 32245
const DEMON_PRINCE = 25540
const RANKU = 25542

const FIRE = 10521
const WATER = 10522
const EARTH = 10523
const WIND = 10524
const DARKNESS = 10525
const DIVINITY = 10526
const BLUEPRINT_RANKU = 9800
const BLUEPRINT_PRINCE = 9801

const minimumLevel = 76
const rewardForPrince = 'prince'
const rewardForRanku = 'ranku'

export class MatrasCuriosity extends ListenerLogic {
    constructor() {
        super( 'Q00132_MatrasCuriosity', 'listeners/tracked/MatrasCuriosity.ts' )
        this.questId = 132
        this.questItemIds = [ BLUEPRINT_RANKU, BLUEPRINT_PRINCE ]
    }

    getAttackableKillIds(): Array<number> {
        return [ RANKU, DEMON_PRINCE ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00132_MatrasCuriosity'
    }

    getQuestStartIds(): Array<number> {
        return [ MATRAS ]
    }

    getTalkIds(): Array<number> {
        return [ MATRAS ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player: L2PcInstance

        switch ( data.npcId ) {
            case DEMON_PRINCE:
                player = this.getRandomPartyMemberForVariable( L2World.getPlayer( data.playerId ), rewardForPrince, true )
                if ( player ) {
                    let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), this.getName() )
                    await QuestHelper.giveSingleItem( player, BLUEPRINT_PRINCE, 1 )
                    state.unsetVariable( rewardForPrince )

                    if ( QuestHelper.hasQuestItem( player, BLUEPRINT_RANKU ) ) {
                        state.setConditionWithSound( 2, true )
                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case RANKU:
                player = this.getRandomPartyMemberForVariable( L2World.getPlayer( data.playerId ), rewardForRanku, true )
                if ( player ) {
                    let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), this.getName() )
                    await QuestHelper.giveSingleItem( player, BLUEPRINT_RANKU, 1 )
                    state.unsetVariable( rewardForRanku )

                    if ( QuestHelper.hasQuestItem( player, BLUEPRINT_PRINCE ) ) {
                        state.setConditionWithSound( 2, true )
                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32245-03.htm':
                if ( state.isCompleted() ) {
                    break
                }

                if ( player.getLevel() >= minimumLevel ) {
                    if ( state.isCreated() ) {
                        state.startQuest()
                        state.setVariable( rewardForPrince, true )
                        state.setVariable( rewardForRanku, true )

                        break
                    }

                    return this.getPath( '32245-03a.htm' )
                }

                break

            case '32245-07.htm':
                if ( state.isCompleted() ) {
                    break
                }

                if ( state.isCondition( 3 ) ) {
                    await QuestHelper.giveAdena( player, 65884, true )
                    await QuestHelper.addExpAndSp( player, 50541, 5094 )
                    await QuestHelper.rewardMultipleItems( player, 1, FIRE, WATER, EARTH, WIND, DARKNESS, DIVINITY )
                    await state.exitQuest( false, true )
                }

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '32245-01.htm' : '32245-02.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                    case 2:
                        if ( QuestHelper.hasQuestItems( player, BLUEPRINT_RANKU, BLUEPRINT_PRINCE ) ) {
                            await QuestHelper.takeMultipleItems( player, -1, BLUEPRINT_RANKU, BLUEPRINT_PRINCE )
                            state.setConditionWithSound( 3, true )

                            return this.getPath( '32245-05.htm' )
                        }

                        return this.getPath( '32245-04.htm' )

                    case 3:
                        return this.getPath( '32245-06.htm' )
                }
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}