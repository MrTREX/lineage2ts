import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const ROCKSWELL = 30312
const CRISTEL = 30051
const ROLLFNAN = 30055

const FOX_FUR = 1032
const FOX_FUR_YAM = 1033
const MAIDEN_DOLL = 1034

const ELDER_KELTIR = 20544
const YOUNG_KELTIR = 20545
const KELTIR = 20481

const MAGE_EARING = 113

const minimumLevel = 2

export class SacrificeToTheSea extends ListenerLogic {
    constructor() {
        super( 'Q00154_SacrificeToTheSea', 'listeners/tracked/SacrificeToTheSea.ts' )
        this.questId = 154
        this.questItemIds = [
            FOX_FUR,
            FOX_FUR_YAM,
            MAIDEN_DOLL,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [ ELDER_KELTIR, YOUNG_KELTIR, KELTIR ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00154_SacrificeToTheSea'
    }

    getQuestStartIds(): Array<number> {
        return [ ROCKSWELL ]
    }

    getTalkIds(): Array<number> {
        return [
            ROCKSWELL,
            CRISTEL,
            ROLLFNAN,
        ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), 1, 3, L2World.getObjectById( data.targetId ) as L2Npc )
        if ( !state ) {
            return
        }

        if ( Math.random() > QuestHelper.getAdjustedChance( FOX_FUR, 0.3, data.isChampion ) ) {
            return
        }

        await QuestHelper.rewardAndProgressState( state.getPlayer(), state, FOX_FUR, 1, 10, data.isChampion, 2 )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( state && data.eventName !== '30312-03.htm' ) {
            state.startQuest()
            return this.getPath( data.eventName )
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case ROCKSWELL:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() >= minimumLevel ? '30312-01.htm' : '30312-02.htm' )

                    case QuestStateValues.STARTED:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '30312-04.html' )

                            case 2:
                                return this.getPath( '30312-07.html' )

                            case 3:
                                return this.getPath( '30312-05.html' )

                            case 4:
                                await QuestHelper.takeSingleItem( player, MAIDEN_DOLL, -1 )
                                await QuestHelper.rewardSingleItem( player, MAGE_EARING, 1 )
                                await QuestHelper.addExpAndSp( player, 0, 1000 )
                                await state.exitQuest( false, true )

                                return this.getPath( '30312-06.html' )

                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case CRISTEL:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '30051-02.html' )

                    case 2:
                        await QuestHelper.takeSingleItem( player, FOX_FUR, -1 )
                        await QuestHelper.giveSingleItem( player, FOX_FUR_YAM, 1 )
                        state.setConditionWithSound( 3, true )

                        return this.getPath( '30051-01.html' )

                    case 3:
                        return this.getPath( '30051-03.html' )

                    case 4:
                        return this.getPath( '30051-04.html' )
                }

                break

            case ROLLFNAN:
                switch ( state.getCondition() ) {
                    case 1:
                    case 2:
                        return this.getPath( '30055-03.html' )

                    case 3:
                        await QuestHelper.takeSingleItem( player, FOX_FUR_YAM, -1 )
                        await QuestHelper.giveSingleItem( player, MAIDEN_DOLL, 1 )
                        state.setConditionWithSound( 4, true )

                        return this.getPath( '30055-01.html' )

                    case 4:
                        return this.getPath( '30055-02.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}