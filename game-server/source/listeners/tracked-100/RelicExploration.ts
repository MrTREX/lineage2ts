import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { L2Npc } from '../../gameService/models/actor/L2Npc'

const HEAD_BLACKSMITH_KUSTO = 30512
const MAESTRO_NIKOLA = 30621
const RESEARCHER_LORAIN = 30673

const minimumLevel = 40
const maximumLevel = 46

export class RelicExploration extends ListenerLogic {
    constructor() {
        super( 'Q00183_RelicExploration', 'listeners/tracked/RelicExploration.ts' )
        this.questId = 183
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00183_RelicExploration'
    }

    getQuestStartIds(): Array<number> {
        return [ HEAD_BLACKSMITH_KUSTO ]
    }

    getTalkIds(): Array<number> {
        return [
            HEAD_BLACKSMITH_KUSTO,
            RESEARCHER_LORAIN,
            MAESTRO_NIKOLA,
        ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30512-04.htm':
                state.startQuest()
                state.setMemoState( 1 )
                break

            case '30512-02.htm':
                break

            case '30621-02.html':
                if ( state.isMemoState( 2 ) ) {
                    await QuestHelper.giveAdena( player, 18100, true )
                    if ( player.getLevel() < maximumLevel ) {
                        await QuestHelper.addExpAndSp( player, 60000, 3000 )
                    }

                    await state.exitQuest( false, true )

                    break
                }

                return

            case '30673-02.html':
            case '30673-03.html':
                if ( state.isMemoState( 1 ) ) {
                    break
                }

                return

            case '30673-04.html':
                if ( state.isMemoState( 1 ) ) {
                    state.setMemoState( 2 )
                    state.setConditionWithSound( 2, true )

                    break
                }

                return

            case 'Contract':
            case 'Consideration':
                let quest184: QuestState = QuestStateCache.getQuestState( data.playerId, 'Q00184_ArtOfPersuasion' )
                let quest185: QuestState = QuestStateCache.getQuestState( data.playerId, 'Q00185_NikolasCooperation' )
                if ( !quest184 && !quest185 ) {
                    let nextQuestName = data.eventName === 'Contract' ? 'Q00184_ArtOfPersuasion' : 'Q00185_NikolasCooperation'
                    let npc = L2World.getObjectById( data.characterId ) as L2Npc

                    if ( player.getLevel() >= minimumLevel ) {
                        await QuestHelper.processQuestEvent( player, npc, nextQuestName, '30621-03.htm' )
                        return
                    }

                    await QuestHelper.processQuestEvent( player, npc, nextQuestName, '30621-03a.html' )
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === HEAD_BLACKSMITH_KUSTO ) {
                    return this.getPath( player.getLevel() >= minimumLevel ? '30512-01.htm' : '30512-03.html' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case HEAD_BLACKSMITH_KUSTO:
                        return this.getPath( '30512-05.html' )

                    case MAESTRO_NIKOLA:
                        if ( state.isMemoState( 2 ) ) {
                            return this.getPath( '30621-01.html' )
                        }

                        break

                    case RESEARCHER_LORAIN:
                        if ( state.isMemoState( 1 ) ) {
                            return this.getPath( '30673-01.html' )
                        }

                        if ( state.isMemoState( 2 ) ) {
                            return this.getPath( '30673-05.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}