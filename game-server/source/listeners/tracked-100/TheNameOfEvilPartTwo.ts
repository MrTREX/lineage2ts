import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { MagicSkillUse } from '../../gameService/packets/send/MagicSkillUse'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { L2Npc } from '../../gameService/models/actor/L2Npc'

const SHILENS_STONE_STATUE = 32109
const MUSHIKA = 32114
const ASAMAH = 32115
const ULU_KAIMU = 32119
const BALU_KAIMU = 32120
const CHUTA_KAIMU = 32121
const WARRIORS_GRAVE = 32122

const GAZKH_FRAGMENT = 8782
const BONE_POWDER = 8783

const ENCHANT_WEAPON_A = 729
const minimumLevel = 77

const variableNames = {
    DO: 'do',
    MI: 'mi',
    FA: 'fa',
    SOL: 'so',
    FA2: 'fa2',
    TI: 'ti',
    SOL2: 'so2',
    MI2: 'mi2'
}

export class TheNameOfEvilPartTwo extends ListenerLogic {
    constructor() {
        super( 'Q00126_TheNameOfEvilPartTwo', 'listeners/tracked/TheNameOfEvilPartTwo.ts' )
        this.questId = 126
        this.questItemIds = [ GAZKH_FRAGMENT, BONE_POWDER ]
    }

    castSkill( playerId: number, npcObjectId: number ): void {
        let npc = L2World.getObjectById( npcObjectId ) as L2Npc
        return BroadcastHelper.dataInLocation( npc, MagicSkillUse( npcObjectId, playerId, 5089, 1, 1000, 0 ), npc.getBroadcastRadius() )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00126_TheNameOfEvil2'
    }

    getQuestStartIds(): Array<number> {
        return [ ASAMAH ]
    }

    getTalkIds(): Array<number> {
        return [
            ASAMAH,
            ULU_KAIMU,
            BALU_KAIMU,
            CHUTA_KAIMU,
            WARRIORS_GRAVE,
            SHILENS_STONE_STATUE,
            MUSHIKA,
        ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state ) {
            return QuestHelper.getNoQuestMessagePath()
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32115-1.html':
                state.startQuest()
                break

            case '32115-1b.html':
                if ( state.isCondition( 1 ) ) {
                    state.setConditionWithSound( 2, true )
                }
                break

            case '32119-3.html':
                if ( state.isCondition( 2 ) ) {
                    state.setConditionWithSound( 3, true )
                }
                break

            case '32119-4.html':
                if ( state.isCondition( 3 ) ) {
                    state.setConditionWithSound( 4, true )
                }
                break

            case '32119-4a.html':
            case '32119-5b.html':
                player.sendCopyData( SoundPacket.ETCSOUND_ELROKI_SONG_1ST )
                break

            case '32119-5.html':
                if ( state.isCondition( 4 ) ) {
                    state.setConditionWithSound( 5, true )
                }
                break

            case '32120-3.html':
                if ( state.isCondition( 5 ) ) {
                    state.setConditionWithSound( 6, true )
                }
                break

            case '32120-4.html':
                if ( state.isCondition( 6 ) ) {
                    state.setConditionWithSound( 7, true )
                }
                break

            case '32120-4a.html':
            case '32120-5b.html':
                player.sendCopyData( SoundPacket.ETCSOUND_ELROKI_SONG_2ND )
                break

            case '32120-5.html':
                if ( state.isCondition( 7 ) ) {
                    state.setConditionWithSound( 8, true )
                }
                break

            case '32121-3.html':
                if ( state.isCondition( 8 ) ) {
                    state.setConditionWithSound( 9, true )
                }
                break

            case '32121-4.html':
                if ( state.isCondition( 9 ) ) {
                    state.setConditionWithSound( 10, true )
                }
                break

            case '32121-4a.html':
            case '32121-5b.html':
                player.sendCopyData( SoundPacket.ETCSOUND_ELROKI_SONG_3RD )
                break

            case '32121-5.html':
                if ( state.isCondition( 10 ) ) {
                    await QuestHelper.giveSingleItem( player, GAZKH_FRAGMENT, 1 )
                    state.setConditionWithSound( 11, true )
                }
                break

            case '32122-2a.html':
                this.castSkill( data.playerId, data.characterId )
                break

            case '32122-2d.html':
                await QuestHelper.takeSingleItem( player, GAZKH_FRAGMENT, -1 )
                break

            case '32122-3.html':
                if ( state.isCondition( 12 ) ) {
                    state.setConditionWithSound( 13, true )
                }

                break

            case '32122-4.html':
                if ( state.isCondition( 13 ) ) {
                    state.setConditionWithSound( 14, true )
                }

                break

            case 'DO_One':
                state.setVariable( variableNames.DO, 1 )
                return this.getPath( '32122-4d.html' )

            case 'MI_One':
                state.setVariable( variableNames.MI, 1 )
                return this.getPath( '32122-4f.html' )

            case 'FA_One':
                state.setVariable( variableNames.FA, 1 )
                return this.getPath( '32122-4h.html' )

            case 'SOL_One':
                state.setVariable( variableNames.SOL, 1 )
                return this.getPath( '32122-4j.html' )

            case 'FA2_One':
                state.setVariable( variableNames.FA2, 1 )
                if ( state.isCondition( 14 )
                        && state.getVariable( variableNames.DO )
                        && state.getVariable( variableNames.MI )
                        && state.getVariable( variableNames.FA )
                        && state.getVariable( variableNames.SOL )
                        && state.getVariable( variableNames.FA2 ) ) {

                    state.setConditionWithSound( 15, true )
                    return this.getPath( '32122-4n.html' )
                }

                state.unsetVariables( variableNames.DO, variableNames.MI, variableNames.FA, variableNames.SOL, variableNames.FA2 )
                return this.getPath( '32122-4m.html' )

            case '32122-4m.html':
                state.unsetVariables( variableNames.DO, variableNames.MI, variableNames.FA, variableNames.SOL, variableNames.FA2 )
                break

            case 'FA_Two':
                state.setVariable( variableNames.FA, 1 )
                return this.getPath( '32122-5a.html' )

            case 'SOL_Two':
                state.setVariable( variableNames.SOL, 1 )
                return this.getPath( '32122-5c.html' )

            case 'TI_Two':
                state.setVariable( variableNames.TI, 1 )
                return this.getPath( '32122-5e.html' )

            case 'SOL2_Two':
                state.setVariable( variableNames.SOL2, 1 )
                return this.getPath( '32122-5g.html' )

            case 'FA2_Two':
                state.setVariable( variableNames.FA2, 1 )
                if ( state.isCondition( 15 )
                        && state.getVariable( variableNames.FA )
                        && state.getVariable( variableNames.SOL )
                        && state.getVariable( variableNames.TI )
                        && state.getVariable( variableNames.SOL2 )
                        && state.getVariable( variableNames.FA2 ) ) {

                    state.setConditionWithSound( 16, true )
                    return this.getPath( '32122-5j.html' )
                }

                state.unsetVariables( variableNames.FA, variableNames.SOL, variableNames.TI, variableNames.SOL2, variableNames.FA2 )
                return this.getPath( '32122-5i.html' )

            case '32122-5i.html':
                state.unsetVariables( variableNames.FA, variableNames.SOL, variableNames.TI, variableNames.SOL2, variableNames.FA2 )
                break

            case 'SOL_Three':
                state.setVariable( variableNames.SOL, 1 )
                return this.getPath( '32122-6a.html' )

            case 'FA_Three':
                state.setVariable( variableNames.FA, 1 )
                return this.getPath( '32122-6c.html' )

            case 'MI_Three':
                state.setVariable( variableNames.MI, 1 )
                return this.getPath( '32122-6e.html' )

            case 'FA2_Three':
                state.setVariable( variableNames.FA2, 1 )
                return this.getPath( '32122-6g.html' )

            case 'MI2_Three':
                state.setVariable( variableNames.MI2, 1 )
                if ( state.isCondition( 16 )
                        && state.getVariable( variableNames.SOL )
                        && state.getVariable( variableNames.FA )
                        && state.getVariable( variableNames.MI )
                        && state.getVariable( variableNames.FA2 )
                        && state.getVariable( variableNames.MI2 ) ) {

                    state.setConditionWithSound( 17, true )
                    return this.getPath( '32122-6j.html' )
                }

                state.unsetVariables( variableNames.SOL, variableNames.FA, variableNames.MI, variableNames.FA2, variableNames.MI2 )
                return this.getPath( '32122-6i.html' )

            case '32122-6i.html':
                state.unsetVariables( variableNames.SOL, variableNames.FA, variableNames.MI, variableNames.FA2, variableNames.MI2 )
                break

            case '32122-7.html':
                await QuestHelper.giveSingleItem( player, BONE_POWDER, 1 )
                this.castSkill( data.playerId, data.characterId )

                player.sendCopyData( SoundPacket.ETCSOUND_ELROKI_SONG_FULL )
                break

            case '32122-8.html':
                if ( state.isCondition( 17 ) ) {
                    state.setConditionWithSound( 18, true )
                }

                break

            case '32109-2.html':
                if ( state.isCondition( 18 ) ) {
                    state.setConditionWithSound( 19, true )
                }

                break

            case '32109-3.html':
                if ( state.isCondition( 19 ) ) {
                    await QuestHelper.takeSingleItem( player, BONE_POWDER, -1 )
                    state.setConditionWithSound( 20, true )
                }

                break

            case '32115-4.html':
                if ( state.isCondition( 20 ) ) {
                    state.setConditionWithSound( 21, true )
                }

                break

            case '32115-5.html':
                if ( state.isCondition( 21 ) ) {
                    state.setConditionWithSound( 22, true )
                }

                break

            case '32114-2.html':
                if ( state.isCondition( 22 ) ) {
                    state.setConditionWithSound( 23, true )
                }

                break

            case '32114-3.html':
                await QuestHelper.rewardSingleItem( player, ENCHANT_WEAPON_A, 1 )
                await QuestHelper.giveAdena( player, 460483, true )
                await QuestHelper.addExpAndSp( player, 1015973, 102802 )
                await state.exitQuest( false, true )

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case ASAMAH:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        if ( player.getLevel() < minimumLevel ) {
                            return this.getPath( '32115-0.htm' )
                        }

                        return this.getPath( player.hasQuestCompleted( 'Q00125_TheNameOfEvilPartOne' ) ? '32115-0a.htm' : '32115-0b.htm' )

                    case QuestStateValues.STARTED:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '32115-1d.html' )

                            case 2:
                                return this.getPath( '32115-1c.html' )

                            case 3:
                            case 4:
                            case 5:
                            case 6:
                            case 7:
                            case 8:
                            case 9:
                            case 10:
                            case 11:
                            case 12:
                            case 13:
                            case 14:
                            case 15:
                            case 16:
                            case 17:
                            case 18:
                            case 19:
                                return this.getPath( '32115-2.html' )

                            case 20:
                                return this.getPath( '32115-3.html' )

                            case 21:
                                return this.getPath( '32115-4j.html' )

                            case 22:
                                return this.getPath( '32115-5a.html' )
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case ULU_KAIMU:
                if ( state.isStarted() ) {
                    switch ( state.getCondition() ) {
                        case 1:
                            return this.getPath( '32119-1.html' )

                        case 2:
                            this.castSkill( data.playerId, data.characterId )
                            return this.getPath( '32119-2.html' )

                        case 3:
                            return this.getPath( '32119-3c.html' )

                        case 4:
                            return this.getPath( '32119-4c.html' )

                        case 5:
                            return this.getPath( '32119-5a.html' )
                    }
                }

                break

            case BALU_KAIMU:
                if ( state.isStarted() ) {
                    switch ( state.getCondition() ) {
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                            return this.getPath( '32120-1.html' )

                        case 5:
                            this.castSkill( data.playerId, data.characterId )
                            return this.getPath( '32120-2.html' )

                        case 6:
                            return this.getPath( '32120-3c.html' )

                        case 7:
                            return this.getPath( '32120-4c.html' )

                        default:
                            return this.getPath( '32120-5a.html' )
                    }
                }

                break

            case CHUTA_KAIMU:
                if ( state.isStarted() ) {
                    switch ( state.getCondition() ) {
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                        case 7:
                            return this.getPath( '32121-1.html' )

                        case 8:
                            this.castSkill( data.playerId, data.characterId )
                            return this.getPath( '32121-2.html' )

                        case 9:
                            return this.getPath( '32121-3e.html' )

                        case 10:
                            return this.getPath( '32121-4e.html' )

                        default:
                            return this.getPath( '32121-5a.html' )
                    }
                }

                break

            case WARRIORS_GRAVE:
                if ( state.isStarted() ) {
                    switch ( state.getCondition() ) {
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                        case 7:
                        case 8:
                        case 9:
                        case 10:
                            return this.getPath( '32122-1.html' )

                        case 11:
                            state.setConditionWithSound( 12, true )
                            return this.getPath( '32122-2.html' )

                        case 12:
                            return this.getPath( '32122-2l.html' )

                        case 13:
                            return this.getPath( '32122-3b.html' )

                        case 14:
                            state.unsetVariables( variableNames.DO, variableNames.MI, variableNames.FA, variableNames.SOL, variableNames.FA2 )
                            return this.getPath( '32122-4.html' )

                        case 15:
                            state.unsetVariables( variableNames.FA, variableNames.SOL, variableNames.TI, variableNames.SOL2, variableNames.FA2 )
                            return this.getPath( '32122-5.html' )

                        case 16:
                            state.unsetVariables( variableNames.SOL, variableNames.FA, variableNames.MI, variableNames.FA2, variableNames.MI2 )
                            return this.getPath( '32122-6.html' )

                        case 17:
                            return this.getPath( QuestHelper.hasQuestItem( player, BONE_POWDER ) ? '32122-7.html' : '32122-7b.html' )

                        case 18:
                            return this.getPath( '32122-8.html' )

                        default:
                            return this.getPath( '32122-9.html' )
                    }
                }

                break

            case SHILENS_STONE_STATUE:
                if ( state.isStarted() ) {
                    switch ( state.getCondition() ) {
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                        case 7:
                        case 8:
                        case 9:
                        case 10:
                        case 11:
                        case 12:
                        case 13:
                        case 14:
                        case 15:
                        case 16:
                        case 17:
                            return this.getPath( '32109-1a.html' )

                        case 18:
                            if ( QuestHelper.hasQuestItem( player, BONE_POWDER ) ) {
                                return this.getPath( '32109-1.html' )
                            }

                            break

                        case 19:
                            return this.getPath( '32109-2l.html' )

                        case 20:
                            return this.getPath( '32109-5.html' )

                        default:
                            return this.getPath( '32109-4.html' )
                    }
                }

                break

            case MUSHIKA:
                if ( state.isStarted() ) {
                    if ( state.getCondition() < 22 ) {
                        return this.getPath( '32114-4.html' )
                    }

                    if ( state.isCondition( 22 ) ) {
                        return this.getPath( '32114-1.html' )
                    }

                    return this.getPath( '32114-2.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}