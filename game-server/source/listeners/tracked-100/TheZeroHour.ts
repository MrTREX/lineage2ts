import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2World } from '../../gameService/L2World'
import { QuestStateValues } from '../../gameService/models/quest/State'

const KAHMAN = 31554
const QUEEN_SHYEED = 25671

const KAHMANS_SUPPLY_BOX = 14849
const FANG = 14859
const minimumLevel = 81

export class TheZeroHour extends ListenerLogic {
    constructor() {
        super( 'Q00146_TheZeroHour', 'listeners/tracked/TheZeroHour.ts' )
        this.questId = 146
        this.questItemIds = [ FANG ]
    }

    getAttackableKillIds(): Array<number> {
        return [ QUEEN_SHYEED ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00146_TheZeroHour'
    }

    getQuestStartIds(): Array<number> {
        return [ KAHMAN ]
    }

    getTalkIds(): Array<number> {
        return [ KAHMAN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player: L2PcInstance = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 1 )
        if ( player && !QuestHelper.hasQuestItem( player, FANG ) ) {
            let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), this.getName() )

            await QuestHelper.giveSingleItem( player, FANG, 1 )
            state.setConditionWithSound( 2, true )
        }

        return
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return QuestHelper.getNoQuestMessagePath()
        }

        if ( data.eventName === '31554-03.htm' ) {
            state.startQuest()
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '31554-02.htm' )
                }

                return this.getPath( player.hasQuestCompleted( 'Q00109_InSearchOfTheNest' ) ? '31554-01a.htm' : '31554-04.html' )

            case QuestStateValues.STARTED:
                if ( state.isCondition( 1 ) ) {
                    return this.getPath( '31554-06.html' )
                }

                await QuestHelper.giveSingleItem( player, KAHMANS_SUPPLY_BOX, 1 )
                await QuestHelper.addExpAndSp( player, 154616, 12500 )
                await state.exitQuest( false, true )

                return this.getPath( '31554-05.html' )

            case QuestStateValues.COMPLETED:
                return this.getPath( '31554-01b.htm' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}