import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { SkillHolder } from '../../gameService/models/holders/SkillHolder'
import { NpcApproachedForTalkEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { teleportCharacterToGeometryCoordinates } from '../../gameService/helpers/TeleportHelper'
import { GeometryId } from '../../gameService/enums/GeometryId'

const RAYMOND = 30289
const IASON_HEINE = 30969
const CLAUDIA_ATHEBALDT = 31001
const LIGHT_OF_DAWN = 32575
const JOHN = 32576
const PASSWORD_ENTRY_DEVICE = 32577
const IDENTITY_CONFIRM_DEVICE = 32578
const DARKNESS_OF_DAWN = 32579
const SHELF = 32580

const IDENTITY_CARD = 13822
const SHUNAIMANS_CONTRACT = 13823

const minimumLevel = 79
const TRANSFORMATION = new SkillHolder( 6204 )

export class SevenSignsSecretRitualOfThePriests extends ListenerLogic {
    constructor() {
        super( 'Q00195_SevenSignsSecretRitualOfThePriests', 'listeners/tracked/SevenSignsSecretRitualOfThePriests.ts' )
        this.questId = 195
        this.questItemIds = [ IDENTITY_CARD, SHUNAIMANS_CONTRACT ]
    }

    getApproachedForTalkIds(): Array<number> {
        return [
            IDENTITY_CONFIRM_DEVICE,
            PASSWORD_ENTRY_DEVICE,
            DARKNESS_OF_DAWN,
            SHELF,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00195_SevenSignsSecretRitualOfThePriests'
    }

    getQuestStartIds(): Array<number> {
        return [ CLAUDIA_ATHEBALDT ]
    }

    getTalkIds(): Array<number> {
        return [
            CLAUDIA_ATHEBALDT,
            JOHN,
            RAYMOND,
            IASON_HEINE,
            LIGHT_OF_DAWN,
            DARKNESS_OF_DAWN,
            IDENTITY_CONFIRM_DEVICE,
            PASSWORD_ENTRY_DEVICE,
            SHELF,
        ]
    }

    async onApproachedForTalk( data: NpcApproachedForTalkEvent ): Promise<string> {
        switch ( data.characterNpcId ) {
            case IDENTITY_CONFIRM_DEVICE:
                return this.getPath( '32578-01.html' )

            case PASSWORD_ENTRY_DEVICE:
                return this.getPath( '32577-01.html' )

            case DARKNESS_OF_DAWN:
                return this.getPath( '32579-01.html' )

            case SHELF:
                return this.getPath( '32580-01.html' )
        }

        return
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.characterId ) as L2Npc

        switch ( data.eventName ) {
            case '31001-03.html':
            case '31001-04.html':
            case '31001-05.html':
            case '32580-03.html':
                break

            case '31001-06.html':
                state.startQuest()
                break

            case '32576-02.html':
                if ( state.isCondition( 1 ) ) {
                    await QuestHelper.giveSingleItem( player, IDENTITY_CARD, 1 )
                    state.setConditionWithSound( 2, true )
                    break
                }

                return

            case '30289-02.html':
            case '30289-03.html':
            case '30289-05.html': {
                if ( state.isCondition( 2 ) ) {
                    break
                }
                return
            }
            case '30289-04.html':
                if ( state.isCondition( 2 ) ) {
                    npc.setTarget( player )
                    await npc.doCastWithHolder( TRANSFORMATION )

                    state.setConditionWithSound( 3, true )
                    break
                }

                return

            case '30289-07.html':
                if ( state.isCondition( 3 ) ) {
                    break
                }

                return

            case '30289-08.html':
                if ( state.isCondition( 3 )
                        && QuestHelper.hasQuestItems( player, IDENTITY_CARD, SHUNAIMANS_CONTRACT ) ) {
                    await QuestHelper.takeSingleItem( player, IDENTITY_CARD, -1 )
                    state.setConditionWithSound( 4, true )

                    if ( player.getTransformationId() === 113 ) {
                        await player.stopAllEffects()
                    }

                    break
                }

                return

            case '30289-10.html':
                if ( state.isCondition( 3 ) ) {
                    npc.setTarget( player )
                    await npc.doCastWithHolder( TRANSFORMATION )
                    break
                }
                return

            case '30289-11.html':
                if ( state.isCondition( 3 ) ) {
                    await player.stopAllEffects()
                    break
                }

                return

            case '30969-02.html':
                if ( state.isCondition( 4 ) && QuestHelper.hasQuestItem( player, SHUNAIMANS_CONTRACT ) ) {
                    break
                }

                return

            case 'reward':
                if ( state.isCondition( 4 ) && QuestHelper.hasQuestItem( player, SHUNAIMANS_CONTRACT ) ) {
                    if ( player.getLevel() >= minimumLevel ) {
                        await QuestHelper.addExpAndSp( player, 52518015, 5817677 )
                        await state.exitQuest( false, true )

                        return this.getPath( '30969-03.html' )
                    }

                    return this.getPath( 'level_check.html' )
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === CLAUDIA_ATHEBALDT ) {
                    let canProceed: boolean = player.getLevel() >= minimumLevel && player.hasQuestCompleted( 'Q00194_SevenSignsMammonsContract' )
                    return this.getPath( canProceed ? '31001-01.htm' : '31001-02.html' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case CLAUDIA_ATHEBALDT:
                        if ( state.isCondition( 1 ) ) {
                            return this.getPath( '31001-07.html' )
                        }

                        break

                    case JOHN:
                        switch ( state.getCondition() ) {
                            case 1: {
                                return this.getPath( '32576-01.html' )
                                break
                            }
                            case 2: {
                                return this.getPath( '32576-03.html' )
                                break
                            }
                        }

                        break

                    case RAYMOND:
                        switch ( state.getCondition() ) {
                            case 2:
                                if ( QuestHelper.hasQuestItem( player, IDENTITY_CARD ) && player.getTransformationId() !== 113 ) {
                                    return this.getPath( '30289-01.html' )
                                }

                                break

                            case 3:
                                if ( QuestHelper.hasQuestItem( player, IDENTITY_CARD ) ) {
                                    return this.getPath( QuestHelper.hasQuestItem( player, SHUNAIMANS_CONTRACT ) ? '30289-06.html' : '30289-09.html' )
                                }

                                break

                            case 4:
                                return this.getPath( '30289-12.html' )
                        }

                        break

                    case LIGHT_OF_DAWN:
                        if ( state.isCondition( 3 ) && QuestHelper.hasQuestItem( player, IDENTITY_CARD ) ) {
                            return this.getPath( '31001-07.html' )
                        }

                        break

                    case PASSWORD_ENTRY_DEVICE:
                        if ( state.isCondition( 3 ) && QuestHelper.hasQuestItem( player, IDENTITY_CARD ) ) {
                            await teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, -78240, 205858, -7856 )
                            return this.getPath( '32577-02.html' )
                        }

                        break

                    case SHELF:
                        if ( state.isCondition( 3 ) && !QuestHelper.hasQuestItem( player, SHUNAIMANS_CONTRACT ) ) {
                            await QuestHelper.giveSingleItem( player, SHUNAIMANS_CONTRACT, 1 )
                            return this.getPath( '32580-02.html' )
                        }

                        break

                    case DARKNESS_OF_DAWN:
                        if ( state.isCondition( 3 ) && !QuestHelper.hasQuestItem( player, SHUNAIMANS_CONTRACT ) ) {
                            return this.getPath( '32579-02.html' )
                        }

                        break

                    case IASON_HEINE:
                        if ( state.isCondition( 4 ) && QuestHelper.hasQuestItem( player, SHUNAIMANS_CONTRACT ) ) {
                            return this.getPath( '30969-01.html' )
                        }

                        break

                }

                break

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}