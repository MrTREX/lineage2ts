import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { CycleStepManager } from '../../gameService/cache/CycleStepManager'
import { CycleType } from '../../gameService/enums/CycleType'

const CASIAN = 30612
const GALATE = 32292

const CASIANS_BLUE_CRYSTAL = 12823

const minimumLevel = 78

export class PathToHellbound extends ListenerLogic {
    constructor() {
        super( 'Q00130_PathToHellbound', 'listeners/tracked/PathToHellbound.ts' )
        this.questId = 130
        this.questItemIds = [ CASIANS_BLUE_CRYSTAL ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00130_PathToHellbound/Q00130_PathToHellbound.java'
    }

    getQuestStartIds(): Array<number> {
        return [ CASIAN ]
    }

    getTalkIds(): Array<number> {
        return [ CASIAN, GALATE ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30612-04.htm':
                break

            case '32292-02.html':
                if ( state.isCondition( 1 ) ) {
                    break
                }

                return

            case '32292-06.html':
                if ( state.isCondition( 3 ) ) {
                    break
                }

                return

            case '30612-05.html':
                state.startQuest()
                break

            case '30612-08.html':
                if ( state.isCondition( 2 ) ) {
                    await QuestHelper.giveSingleItem( player, CASIANS_BLUE_CRYSTAL, 1 )
                    state.setConditionWithSound( 3, true )
                    break
                }

                return

            case '32292-03.html':
                if ( state.isCondition( 1 ) ) {
                    state.setConditionWithSound( 2, true )
                    break
                }

                break

            case '32292-07.html':
                if ( state.isCondition( 3 ) && QuestHelper.hasQuestItem( player, CASIANS_BLUE_CRYSTAL ) ) {
                    await state.exitQuest( false, true )
                    break
                }

                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === CASIAN ) {

                    if ( !CycleStepManager.isLocked( CycleType.Hellbound ) ) {
                        return this.getPath( player.getLevel() >= minimumLevel ? '30612-01.htm' : '30612-02.html' )
                    }

                    return this.getPath( '30612-03.html' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case CASIAN:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '30612-06.html' )

                            case 2:
                                return this.getPath( '30612-07.html' )

                            case 3:
                                return this.getPath( '30612-09.html' )
                        }

                        return

                    case GALATE:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '32292-01.html' )

                            case 2:
                                return this.getPath( '32292-04.html' )

                            case 3:
                                return this.getPath( '32292-05.html' )
                        }

                        return
                }

                break

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}