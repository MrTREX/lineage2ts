import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const MAESTRO_NIKOLA = 30621
const RESEARCHER_LORAIN = 30673
const DOROTHY_LOCKSMITH = 30970

const LORAINES_CERTIFICATE = 10362
const BROKEN_METAL_PIECES = 10369

const minimumLevel = 41
const maximumLevel = 47

export class SealRemoval extends ListenerLogic {
    constructor() {
        super( 'Q00188_SealRemoval', 'listeners/tracked/SealRemoval.ts' )
        this.questId = 188
        this.questItemIds = [ BROKEN_METAL_PIECES ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00188_SealRemoval'
    }

    getQuestStartIds(): Array<number> {
        return [ RESEARCHER_LORAIN ]
    }

    getTalkIds(): Array<number> {
        return [ RESEARCHER_LORAIN, MAESTRO_NIKOLA, DOROTHY_LOCKSMITH ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30673-03.htm':
                if ( state.isCreated() ) {
                    state.startQuest()
                    state.setMemoState( 1 )

                    await QuestHelper.giveSingleItem( player, BROKEN_METAL_PIECES, 1 )
                    break
                }

                return

            case '30621-02.html':
                if ( state.isMemoState( 1 ) ) {
                    break
                }

                return

            case '30621-03.html':
                if ( state.isMemoState( 1 ) ) {
                    state.setMemoState( 2 )
                    state.setConditionWithSound( 2, true )

                    break
                }

                return

            case '30621-04.html':
                if ( state.isMemoState( 2 ) ) {
                    break
                }

                return

            case '30970-02.html':
                if ( state.isMemoState( 2 ) ) {
                    break
                }

                return

            case '30970-03.html':
                if ( state.isMemoState( 2 ) ) {
                    await QuestHelper.giveAdena( player, 98583, true )

                    if ( player.getLevel() < maximumLevel ) {
                        await QuestHelper.addExpAndSp( player, 285935, 18711 )
                    }

                    await state.exitQuest( false, true )
                    break
                }

                return


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === RESEARCHER_LORAIN && !QuestHelper.hasQuestItem( player, LORAINES_CERTIFICATE ) ) {
                    let quest184: QuestState = QuestStateCache.getQuestState( data.playerId, 'Q00184_ArtOfPersuasion' )
                    let quest185: QuestState = QuestStateCache.getQuestState( data.playerId, 'Q00185_NikolasCooperation' )
                    let quest186: QuestState = QuestStateCache.getQuestState( data.playerId, 'Q00186_ContractExecution' )
                    let quest187: QuestState = QuestStateCache.getQuestState( data.playerId, 'Q00187_NikolasHeart' )

                    if ( ( quest184 && quest184.isCompleted() )
                            || ( quest185 && quest185.isCompleted() && !quest186 && !quest187 ) ) {
                        return this.getPath( player.getLevel() >= minimumLevel ? '30673-01.htm' : '30673-02.htm' )
                    }
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case RESEARCHER_LORAIN:
                        return this.getPath( '30673-04.html' )

                    case MAESTRO_NIKOLA:
                        if ( state.isMemoState( 1 ) ) {
                            return this.getPath( '30621-01.html' )
                        }

                        if ( state.isMemoState( 2 ) ) {
                            return this.getPath( '30621-05.html' )
                        }

                        break

                    case DOROTHY_LOCKSMITH:
                        if ( state.isMemoState( 2 ) ) {
                            return this.getPath( '30970-01.html' )
                        }

                        break

                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === RESEARCHER_LORAIN ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}