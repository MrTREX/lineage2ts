import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const MARQUEZ = 32113
const MUSHIKA = 32114
const ASAMAH = 32115
const KIRIKACHIN = 32116

const ELROKIAN_TRAP = 8763
const TRAP_STONE = 8764
const DIARY_FRAGMENT = 8768
const EXPEDITION_MEMBERS_LETTER = 8769
const ORNITHOMINUS_CLAW = 8770
const DEINONYCHUS_BONE = 8771
const PACHYCEPHALOSAURUS_SKIN = 8772
const PRACTICE_ELROKIAN_TRAP = 8773

const minimumLevel = 75

type MonsterDrop = [ number, number, number ] // itemId, chance, memoStateValue
const dropMap : { [ npcId: number ] : MonsterDrop } = {
    22196: [ DIARY_FRAGMENT, 0.51, 4 ], // velociraptor_leader
    22197: [ DIARY_FRAGMENT, 0.51, 4 ], // velociraptor
    22198: [ DIARY_FRAGMENT, 0.51, 4 ], // velociraptor_s
    22218: [ DIARY_FRAGMENT, 0.25, 4 ], // velociraptor_n
    22223: [ DIARY_FRAGMENT, 0.26, 4 ], // velociraptor_leader2
    22200: [ ORNITHOMINUS_CLAW, 0.66, 11 ], // ornithomimus_leader
    22201: [ ORNITHOMINUS_CLAW, 0.33, 11 ], // ornithomimus
    22202: [ ORNITHOMINUS_CLAW, 0.66, 11 ], // ornithomimus_s
    22219: [ ORNITHOMINUS_CLAW, 0.33, 11 ], // ornithomimus_n
    22224: [ ORNITHOMINUS_CLAW, 0.33, 11 ], // ornithomimus_leader2
    22203: [ DEINONYCHUS_BONE, 0.65, 11 ], // deinonychus_leader
    22204: [ DEINONYCHUS_BONE, 0.32, 11 ], // deinonychus
    22205: [ DEINONYCHUS_BONE, 0.66, 11 ], // deinonychus_s
    22220: [ DEINONYCHUS_BONE, 0.32, 11 ], // deinonychus_n
    22225: [ DEINONYCHUS_BONE, 0.32, 11 ], // deinonychus_leader2
    22208: [ PACHYCEPHALOSAURUS_SKIN, 0.50, 11 ], // pachycephalosaurus_ldr
    22209: [ PACHYCEPHALOSAURUS_SKIN, 0.50, 11 ], // pachycephalosaurus
    22210: [ PACHYCEPHALOSAURUS_SKIN, 0.50, 11 ], // pachycephalosaurus_s
    22221: [ PACHYCEPHALOSAURUS_SKIN, 0.49, 11 ], // pachycephalosaurus_n
    22226: [ PACHYCEPHALOSAURUS_SKIN, 0.50, 11 ], // pachycephalosaurus_ldr2
}

export class ElrokianHuntersProof extends ListenerLogic {
    constructor() {
        super( 'Q00111_ElrokianHuntersProof', 'listeners/tracked/ElrokianHuntersProof.ts' )
        this.questId = 111
        this.questItemIds = [
            DIARY_FRAGMENT,
            EXPEDITION_MEMBERS_LETTER,
            ORNITHOMINUS_CLAW,
            DEINONYCHUS_BONE,
            PACHYCEPHALOSAURUS_SKIN,
            PRACTICE_ELROKIAN_TRAP,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( dropMap ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00111_ElrokianHuntersProof'
    }

    getQuestStartIds(): Array<number> {
        return [ MARQUEZ ]
    }

    getTalkIds(): Array<number> {
        return [ MARQUEZ, MUSHIKA, ASAMAH, KIRIKACHIN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), -1, 3, L2World.getObjectById( data.targetId ) as L2Npc )

        if ( !state ) {
            return
        }

        let [ itemId, chance, memoState ] = dropMap[ data.npcId ]
        if ( memoState === state.getMemoState() ) {
            if ( state.isCondition( 4 ) ) {
                if ( Math.random() <= QuestHelper.getAdjustedChance( itemId, chance, data.isChampion ) ) {
                    await QuestHelper.rewardAndProgressState( state.getPlayer(), state, itemId, 1, 50, data.isChampion, 5 )
                }

                return
            }

            if ( state.isCondition( 10 ) ) {
                let currentPlayer = state.getPlayer()
                if ( Math.random() <= QuestHelper.getAdjustedChance( itemId, chance, data.isChampion ) ) {
                    await QuestHelper.rewardUpToLimit( currentPlayer, itemId, 1, 10, data.isChampion )
                }

                if ( QuestHelper.getQuestItemsCount( currentPlayer, ORNITHOMINUS_CLAW ) >= 10
                        && QuestHelper.getQuestItemsCount( currentPlayer, DEINONYCHUS_BONE ) >= 10
                        && QuestHelper.getQuestItemsCount( currentPlayer, PACHYCEPHALOSAURUS_SKIN ) >= 10 ) {
                    state.setConditionWithSound( 11 )
                }

                return
            }
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32113-02.htm':
            case '32113-05.htm':
            case '32113-04.html':
            case '32113-10.html':
            case '32113-11.html':
            case '32113-12.html':
            case '32113-13.html':
            case '32113-14.html':
            case '32113-18.html':
            case '32113-19.html':
            case '32113-20.html':
            case '32113-21.html':
            case '32113-22.html':
            case '32113-23.html':
            case '32113-24.html':
            case '32115-08.html':
            case '32116-03.html':
                break

            case '32113-03.html':
                state.startQuest()
                state.setMemoState( 1 )
                break

            case '32113-15.html':
                if ( state.isMemoState( 3 ) ) {
                    state.setMemoState( 4 )
                    state.setConditionWithSound( 4, true )

                    break
                }

                return

            case '32113-25.html':
                if ( state.isMemoState( 5 ) ) {
                    state.setMemoState( 6 )
                    state.setConditionWithSound( 6, true )

                    await QuestHelper.giveSingleItem( player, EXPEDITION_MEMBERS_LETTER, 1 )
                    break
                }

                return

            case '32115-03.html':
                if ( state.isMemoState( 2 ) ) {
                    state.setMemoState( 3 )
                    state.setConditionWithSound( 3, true )

                    break
                }

                return

            case '32115-06.html':
                if ( state.isMemoState( 9 ) ) {
                    state.setMemoState( 10 )
                    state.setConditionWithSound( 9 )
                    player.sendCopyData( SoundPacket.ETCSOUND_ELROKI_SONG_FULL )

                    break
                }

                return

            case '32115-09.html':
                if ( state.isMemoState( 10 ) ) {
                    state.setMemoState( 11 )
                    state.setConditionWithSound( 10, true )

                    return
                }

                return

            case '32116-04.html':
                if ( state.isMemoState( 7 ) ) {
                    state.setMemoState( 8 )
                    player.sendCopyData( SoundPacket.ETCSOUND_ELROKI_SONG_FULL )

                    break
                }

                return

            case '32116-07.html':
                if ( state.isMemoState( 8 ) ) {
                    state.setMemoState( 9 )
                    state.setConditionWithSound( 8, true )

                    break
                }

                return

            case '32116-10.html':
                if ( state.isMemoState( 12 ) && QuestHelper.hasQuestItem( player, PRACTICE_ELROKIAN_TRAP ) ) {
                    await QuestHelper.takeSingleItem( player, PRACTICE_ELROKIAN_TRAP, -1 )

                    await QuestHelper.rewardSingleItem( player, ELROKIAN_TRAP, 1 )
                    await QuestHelper.rewardSingleItem( player, TRAP_STONE, 100 )

                    await QuestHelper.giveAdena( player, 1071691, true )
                    await QuestHelper.addExpAndSp( player, 553524, 55538 )
                    await state.exitQuest( false, true )

                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === MARQUEZ ) {
                    return this.getPath( player.getLevel() >= minimumLevel ? '32113-01.htm' : '32113-06.html' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case MARQUEZ:

                        switch ( state.getMemoState() ) {
                            case 1:
                                return this.getPath( '32113-07.html' )

                            case 2:
                                return this.getPath( '32113-08.html' )

                            case 3:
                                return this.getPath( '32113-09.html' )

                            case 4:
                                if ( QuestHelper.getQuestItemsCount( player, DIARY_FRAGMENT ) < 50 ) {
                                    return this.getPath( '32113-16.html' )
                                }

                                await QuestHelper.takeSingleItem( player, DIARY_FRAGMENT, -1 )
                                state.setMemoState( 5 )

                                return this.getPath( '32113-17.html' )

                            case 5:
                                return this.getPath( '32113-26.html' )

                            case 6:
                                return this.getPath( '32113-27.html' )

                            case 7:
                            case 8:
                                return this.getPath( '32113-28.html' )

                            case 9:
                                return this.getPath( '32113-29.html' )

                            case 10:
                            case 11:
                            case 12:
                                return this.getPath( '32113-30.html' )
                        }

                        break

                    case MUSHIKA:
                        if ( state.isMemoState( 1 ) ) {
                            state.setConditionWithSound( 2, true )
                            state.setMemoState( 2 )

                            return this.getPath( '32114-01.html' )
                        }

                        if ( state.getMemoState() > 1 && state.getMemoState() < 10 ) {
                            return this.getPath( '32114-02.html' )
                        }

                        return this.getPath( '32114-03.html' )

                    case ASAMAH:
                        switch ( state.getMemoState() ) {
                            case 1:
                                return this.getPath( '32115-01.html' )

                            case 2:
                                return this.getPath( '32115-02.html' )

                            case 3:
                            case 4:
                            case 5:
                            case 6:
                            case 7:
                            case 8:
                                return this.getPath( '32115-04.html' )

                            case 9:
                                return this.getPath( '32115-05.html' )

                            case 10:
                                return this.getPath( '32115-07.html' )

                            case 11:
                                if ( QuestHelper.getQuestItemsCount( player, ORNITHOMINUS_CLAW ) < 10
                                        || QuestHelper.getQuestItemsCount( player, DEINONYCHUS_BONE ) < 10
                                        || QuestHelper.getQuestItemsCount( player, PACHYCEPHALOSAURUS_SKIN ) < 10 ) {
                                    return this.getPath( '32115-10.html' )
                                }

                                state.setMemoState( 12 )
                                state.setConditionWithSound( 12, true )

                                await QuestHelper.giveSingleItem( player, PRACTICE_ELROKIAN_TRAP, 1 )
                                await QuestHelper.takeSingleItem( player, ORNITHOMINUS_CLAW, -1 )
                                await QuestHelper.takeSingleItem( player, DEINONYCHUS_BONE, -1 )
                                await QuestHelper.takeSingleItem( player, PACHYCEPHALOSAURUS_SKIN, -1 )

                                return this.getPath( '32115-11.html' )

                            case 12:
                                return this.getPath( '32115-12.html' )
                        }

                        break

                    case KIRIKACHIN:
                        switch ( state.getMemoState() ) {
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                                return this.getPath( '32116-01.html' )

                            case 6:
                                if ( QuestHelper.hasQuestItem( player, EXPEDITION_MEMBERS_LETTER ) ) {
                                    state.setMemoState( 7 )
                                    state.setConditionWithSound( 7, true )
                                    await QuestHelper.takeSingleItem( player, EXPEDITION_MEMBERS_LETTER, -1 )

                                    return this.getPath( '32116-02.html' )
                                }

                                break

                            case 7:
                                return this.getPath( '32116-05.html' )

                            case 8:
                                return this.getPath( '32116-06.html' )

                            case 9:
                            case 10:
                            case 11:
                                return this.getPath( '32116-08.html' )

                            case 12:
                                return this.getPath( '32116-09.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED: {
                if ( data.characterNpcId === MARQUEZ ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
            }
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}