import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'
import { ClassId } from '../../gameService/models/base/ClassId'

const KEKROPUS = 32138
const MENACING_MACHINE = 32258

const minimumLevel = 17
const maximumLevel = 21

const pumaShirt = 391 // Puma Skin Shirt
const pumaGaters = 413 // Puma Skin Gaiters

const earring = 847 // Red Crescent Earring
const ring = 890 // Ring of Devotion
const necklace = 910 // Necklace of Devotion

export class IntoTheLargeCavern extends ListenerLogic {
    constructor() {
        super( 'Q00179_IntoTheLargeCavern', 'listeners/tracked/IntoTheLargeCavern.ts' )
        this.questId = 179
    }

    getQuestStartIds(): Array<number> {
        return [ KEKROPUS ]
    }

    getTalkIds(): Array<number> {
        return [ KEKROPUS, MENACING_MACHINE ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32138-03.html':
                state.startQuest()
                break

            case '32258-08.html':
                await QuestHelper.rewardSingleItem( player, pumaShirt, 1 )
                await QuestHelper.rewardSingleItem( player, pumaGaters, 1 )

                await state.exitQuest( false, true )
                break

            case '32258-09.html':
                await QuestHelper.rewardSingleItem( player, earring, 2 )
                await QuestHelper.rewardSingleItem( player, ring, 2 )
                await QuestHelper.rewardSingleItem( player, necklace, 1 )

                await state.exitQuest( false, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== KEKROPUS ) {
                    break
                }

                if ( player.getRace() !== Race.KAMAEL ) {
                    return this.getPath( '32138-00b.html' )
                }

                let level = player.getLevel()
                if ( player.hasQuestCompleted( 'Q00178_IconicTrinity' )
                        && level >= minimumLevel
                        && level <= maximumLevel
                        && ClassId.getLevelByClassId( player.getClassId() ) === 0 ) {
                    return this.getPath( '32138-01.htm' )
                }

                if ( level < minimumLevel ) {
                    return this.getPath( '32138-00.html' )
                }

                return this.getPath( '32138-00c.html' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case KEKROPUS:
                        if ( state.isCondition( 1 ) ) {
                            return this.getPath( '32138-03.htm' )
                        }

                        break

                    case MENACING_MACHINE:
                        return this.getPath( '32258-01.html' )
                }

                break

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00179_IntoTheLargeCavern'
    }
}