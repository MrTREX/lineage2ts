import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { SkillHolder } from '../../gameService/models/holders/SkillHolder'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcSay } from '../../gameService/packets/send/builder/NpcSay'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2MonsterInstance } from '../../gameService/models/actor/instance/L2MonsterInstance'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { AIEffectHelper } from '../../gameService/aicontroller/helpers/AIEffectHelper'
import _ from 'lodash'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const SHILENS_EVIL_THOUGHTS = 27343
const HOLLINT = 30191
const SIR_GUSTAV_ATHEBALDT = 30760
const CAIN = 32569
const ERIC = 32570

const JACOBS_NECKLACE = 13814
const DEADMANS_HERB = 13816
const SCULPTURE_OF_DOUBT = 14353

const minimumLevel = 79
let isBusy: boolean = false
const NPC_HEAL = new SkillHolder( 4065, 8 )

export class SevenSignsDyingMessage extends ListenerLogic {
    constructor() {
        super( 'Q00193_SevenSignsDyingMessage', 'listeners/tracked/SevenSignsDyingMessage.ts' )
        this.questId = 193
        this.questItemIds = [ JACOBS_NECKLACE, DEADMANS_HERB, SCULPTURE_OF_DOUBT ]
    }

    getAttackableKillIds(): Array<number> {
        return [ SHILENS_EVIL_THOUGHTS ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00193_SevenSignsDyingMessage'
    }

    getQuestStartIds(): Array<number> {
        return [ HOLLINT ]
    }

    getTalkIds(): Array<number> {
        return [ HOLLINT, CAIN, ERIC, SIR_GUSTAV_ATHEBALDT ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player: L2PcInstance = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 4 )
        if ( !player ) {
            return
        }

        let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), this.getName() )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( npc.isInsideRadius( player, 1500, true ) ) {
            await QuestHelper.giveSingleItem( player, SCULPTURE_OF_DOUBT, 1 )
            state.setConditionWithSound( 5 )

            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_FINISH )
        }

        isBusy = false
        this.stopQuestTimers( 'despawn' )
        this.stopQuestTimers( 'heal' )

        let packet = NpcSay.fromNpcString(
                npc.getObjectId(),
                NpcSayType.NpcAll,
                npc.getId(),
                NpcStringIds.S1_YOU_MAY_HAVE_WON_THIS_TIME_BUT_NEXT_TIME_I_WILL_SURELY_CAPTURE_YOU,
                player.getName() ).getBuffer()

        BroadcastHelper.dataInLocation( npc, packet, npc.getBroadcastRadius() )

    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        if ( 'despawn' === data.eventName && data.characterNpcId === SHILENS_EVIL_THOUGHTS ) {
            let npc = L2World.getObjectById( data.characterId ) as L2Npc
            if ( !npc.isDead() ) {
                isBusy = false
                BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.NEXT_TIME_YOU_WILL_NOT_ESCAPE )
                await npc.deleteMe()
            }

            return
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.characterId ) as L2Npc

        switch ( data.eventName ) {
            case '30191-02.html':
                await QuestHelper.giveSingleItem( player, JACOBS_NECKLACE, 1 )
                state.startQuest()

                break

            case '32569-02.html':
            case '32569-03.html':
            case '32569-04.html':
                if ( state.isCondition( 1 ) && QuestHelper.hasQuestItem( player, JACOBS_NECKLACE ) ) {
                    break
                }

                return

            case '32569-05.html':
                if ( state.isCondition( 1 ) && QuestHelper.hasQuestItem( player, JACOBS_NECKLACE ) ) {
                    await QuestHelper.takeSingleItem( player, JACOBS_NECKLACE, -1 )

                    state.setConditionWithSound( 2, true )
                    break
                }

                return

            case 'showmovie':
                if ( state.isCondition( 3 ) && QuestHelper.hasQuestItem( player, DEADMANS_HERB ) ) {
                    await QuestHelper.takeSingleItem( player, DEADMANS_HERB, -1 )
                    state.setConditionWithSound( 4, true )

                    await player.showQuestMovie( 9 )
                }

                return

            case '32569-10.html':
            case '32569-11.html':
                if ( state.isCondition( 5 ) && await QuestHelper.hasQuestItem( player, SCULPTURE_OF_DOUBT ) ) {
                    break
                }

                return

            case '32569-12.html':
                if ( state.isCondition( 5 ) && QuestHelper.hasQuestItem( player, SCULPTURE_OF_DOUBT ) ) {
                    await QuestHelper.takeSingleItem( player, SCULPTURE_OF_DOUBT, -1 )

                    state.setConditionWithSound( 6, true )
                    break
                }

                return

            case '32570-02.html':
                if ( state.isCondition( 2 ) ) {
                    await QuestHelper.giveSingleItem( player, DEADMANS_HERB, 1 )

                    state.setConditionWithSound( 3, true )
                    break
                }

                return

            case 'fight':
                if ( state.isCondition( 4 ) ) {
                    isBusy = true

                    BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.S1_THAT_STRANGER_MUST_BE_DEFEATED_HERE_IS_THE_ULTIMATE_HELP, player.getName() )
                    let mob = QuestHelper.addGenericSpawn( null, SHILENS_EVIL_THOUGHTS, 82425, 47232, -3216, 0, false, 0, false ) as L2MonsterInstance

                    mob.setRunning()

                    AIEffectHelper.notifyAttackedWithTargetId( mob, data.playerId, 0, 999 )
                    BroadcastHelper.broadcastNpcSayStringId( mob, NpcSayType.NpcAll, NpcStringIds.YOU_ARE_NOT_THE_OWNER_OF_THAT_ITEM )

                    this.startQuestTimer( 'heal', 30000 - _.random( 20000 ), data.characterId, data.playerId, false )
                    this.startQuestTimer( 'despawn', 300000, mob.getObjectId(), null, false )
                }

                return this.getPath( '32569-14.html' )

            case 'heal':
                if ( !npc.isInsideRadius( player, 600, true ) ) {
                    BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.LOOK_HERE_S1_DONT_FALL_TOO_FAR_BEHIND, player.getName() )
                }

                if ( !player.isDead() ) {
                    npc.setTarget( player )
                    await npc.doCastWithHolder( NPC_HEAL )
                }

                this.startQuestTimer( 'heal', 30000 - _.random( 20000 ), data.characterId, data.playerId, false )
                return

            case 'reward':
                if ( state.isCondition( 6 ) ) {
                    if ( player.getLevel() >= minimumLevel ) {
                        await QuestHelper.addExpAndSp( player, 52518015, 5817677 )
                        await state.exitQuest( false, true )

                        return this.getPath( '30760-02.html' )
                    }

                    return this.getPath( 'level_check.html' )
                }
                return


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === HOLLINT ) {
                    return this.getPath( player.getLevel() >= minimumLevel && player.hasQuestCompleted( 'Q00192_SevenSignsSeriesOfDoubt' ) ? '30191-01.htm' : '30191-03.html' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case HOLLINT:
                        if ( state.isCondition( 1 ) && QuestHelper.hasQuestItem( player, JACOBS_NECKLACE ) ) {
                            return this.getPath( '30191-04.html' )
                        }

                        break

                    case CAIN:
                        switch ( state.getCondition() ) {
                            case 1:
                                if ( QuestHelper.hasQuestItem( player, JACOBS_NECKLACE ) ) {
                                    return this.getPath( '32569-01.html' )
                                }

                                break

                            case 2:
                                return this.getPath( '32569-06.html' )

                            case 3:
                                if ( QuestHelper.hasQuestItem( player, DEADMANS_HERB ) ) {
                                    return this.getPath( '32569-07.html' )
                                }

                                break

                            case 4:
                                if ( isBusy ) {
                                    return this.getPath( '32569-13.html' )
                                }

                                return this.getPath( '32569-08.html' )

                            case 5:
                                if ( QuestHelper.hasQuestItem( player, SCULPTURE_OF_DOUBT ) ) {
                                    return this.getPath( '32569-09.html' )
                                }

                                break

                        }

                        break

                    case ERIC:
                        switch ( state.getCondition() ) {
                            case 2:
                                return this.getPath( '32570-01.html' )

                            case 3:
                                return this.getPath( '32570-03.html' )
                        }

                        break

                    case SIR_GUSTAV_ATHEBALDT:
                        if ( state.isCondition( 6 ) ) {
                            return this.getPath( '30760-01.html' )
                        }

                        break
                }
                break

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}