import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const NEWYEAR = 31961
const YUMI = 32041
const minimumLevel = 70

export class PavelTheGiant extends ListenerLogic {
    constructor() {
        super( 'Q00121_PavelTheGiant', 'listeners/tracked/PavelTheGiant.ts' )
        this.questId = 121
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00121_PavelTheGiant'
    }

    getQuestStartIds(): Array<number> {
        return [ NEWYEAR ]
    }

    getTalkIds(): Array<number> {
        return [ NEWYEAR, YUMI ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '31961-02.htm':
                state.startQuest()
                break

            case '32041-02.html':
                let player = L2World.getPlayer( data.playerId )
                await QuestHelper.addExpAndSp( player, 346320, 26069 )
                await state.exitQuest( false, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case NEWYEAR:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() >= minimumLevel ? '31961-01.htm' : '31961-00.htm' )

                    case QuestStateValues.STARTED:
                        return this.getPath( '31961-03.html' )

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case YUMI:
                if ( state.isStarted() ) {
                    return this.getPath( '32041-01.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}