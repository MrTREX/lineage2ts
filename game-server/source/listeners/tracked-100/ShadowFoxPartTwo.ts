import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const KLUCK = 30895
const XENOVIA = 30912

const DARK_CRYSTAL = 10347
const DARK_OXYDE = 10348
const CRYPTOGRAM_OF_THE_GODDESS_SWORD = 10349

const mobChanceMap = {
    20789: 45, // Crokian
    20790: 58, // Dailaon
    20791: 100,// Crokian Warrior
    20792: 92, // Farhite
}

const minimumLevel = 37
const maximumLevel = 42
const CHANCE = 8
const CRYSTAL_COUNT = 5
const OXYDE_COUNT = 2

const variableNames = {
    talk: 't'
}

export class ShadowFoxPartTwo extends ListenerLogic {
    constructor() {
        super( 'Q00140_ShadowFoxPart2', 'listeners/tracked/ShadowFoxPartTwo.ts' )
        this.questId = 140
        this.questItemIds = [
            DARK_CRYSTAL,
            DARK_OXYDE,
            CRYPTOGRAM_OF_THE_GODDESS_SWORD,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( mobChanceMap ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00140_ShadowFoxPart2'
    }

    getQuestStartIds(): Array<number> {
        return [ KLUCK ]
    }

    getTalkIds(): Array<number> {
        return [ KLUCK, XENOVIA ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player: L2PcInstance = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 3 )
        if ( !player ) {
            return
        }

        if ( _.random( 100 ) < QuestHelper.getAdjustedChance( DARK_CRYSTAL, mobChanceMap[ data.npcId ], data.isChampion ) ) {
            await QuestHelper.rewardSingleQuestItem( player, DARK_CRYSTAL, 1, data.isChampion )
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30895-05.html':
            case '30895-06.html':
            case '30912-03.html':
            case '30912-04.html':
            case '30912-05.html':
            case '30912-08.html':
            case '30895-10.html':
                break

            case '30895-03.htm':
                state.startQuest()
                break

            case '30895-07.html':
                state.setConditionWithSound( 2, true )
                break

            case '30912-06.html':
                state.setVariable( variableNames.talk, true )
                break

            case '30912-09.html':
                state.unsetVariable( variableNames.talk )
                state.setConditionWithSound( 3, true )
                break

            case '30912-14.html':
                if ( _.random( 10 ) < CHANCE ) {
                    if ( QuestHelper.getQuestItemsCount( player, DARK_OXYDE ) < OXYDE_COUNT ) {
                        await QuestHelper.giveSingleItem( player, DARK_OXYDE, 1 )
                        await QuestHelper.takeSingleItem( player, DARK_CRYSTAL, 5 )
                        return this.getPath( '30912-12.html' )
                    }

                    await QuestHelper.giveSingleItem( player, CRYPTOGRAM_OF_THE_GODDESS_SWORD, 1 )
                    await QuestHelper.takeSingleItem( player, DARK_CRYSTAL, -1 )
                    await QuestHelper.takeSingleItem( player, DARK_OXYDE, -1 )
                    state.setConditionWithSound( 4, true )
                    return this.getPath( '30912-13.html' )
                }

                await QuestHelper.takeSingleItem( player, DARK_CRYSTAL, 5 )
                break

            case '30895-11.html':
                await QuestHelper.giveAdena( player, 18775, true )
                if ( player.getLevel() <= maximumLevel ) {
                    await QuestHelper.addExpAndSp( player, 30000, 2000 )
                }

                await state.exitQuest( false, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case KLUCK:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        if ( player.getLevel() >= minimumLevel ) {
                            return this.getPath( player.hasQuestCompleted( 'Q00139_ShadowFoxPart1' ) ? '30895-01.htm' : '30895-00.htm' )
                        }

                        return this.getPath( '30895-02.htm' )

                    case QuestStateValues.STARTED:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '30895-04.html' )

                            case 2:
                            case 3:
                                return this.getPath( '30895-08.html' )

                            case 4:
                                if ( state.getVariable( variableNames.talk ) ) {
                                    return this.getPath( '30895-10.html' )
                                }

                                await QuestHelper.takeSingleItem( player, CRYPTOGRAM_OF_THE_GODDESS_SWORD, -1 )
                                state.setVariable( variableNames.talk, true )

                                return this.getPath( '30895-09.html' )
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case XENOVIA:
                if ( state.isStarted() ) {
                    break
                }

                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '30912-01.html' )

                    case 2:
                        return this.getPath( state.getVariable( variableNames.talk ) ? '30912-07.html' : '30912-02.html' )

                    case 3:
                        return this.getPath( QuestHelper.getQuestItemsCount( player, DARK_CRYSTAL ) >= CRYSTAL_COUNT ? '30912-11.html' : '30912-10.html' )

                    case 4:
                        return this.getPath( '30912-15.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}