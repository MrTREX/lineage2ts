import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { L2World } from '../../gameService/L2World'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const CREAMEES = 30149
const KIRUNAK = 27021
const KIRUNAK_SKULL = 1044

const minimumLevel = 21

export class BloodFiend extends ListenerLogic {
    constructor() {
        super( 'Q00164_BloodFiend', 'listeners/tracked/BloodFiend.ts' )
        this.questId = 164
        this.questItemIds = [ KIRUNAK_SKULL ]
    }

    getAttackableKillIds(): Array<number> {
        return [ KIRUNAK ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00164_BloodFiend'
    }

    getQuestStartIds(): Array<number> {
        return [ CREAMEES ]
    }

    getTalkIds(): Array<number> {
        return [ CREAMEES ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || !state.isCondition( 1 ) ) {
            return
        }

        BroadcastHelper.broadcastNpcSayStringId( L2World.getObjectById( data.targetId ) as L2Npc, NpcSayType.All, NpcStringIds.I_HAVE_FULFILLED_MY_CONTRACT_WITH_TRADER_CREAMEES )
        await QuestHelper.giveSingleItem( L2World.getPlayer( data.playerId ), KIRUNAK_SKULL, 1 )

        state.setConditionWithSound( 2, true )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || data.eventName !== '30149-04.htm' ) {
            return
        }

        state.startQuest()
        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getRace() !== Race.DARK_ELF ) {
                    return this.getPath( player.getLevel() >= minimumLevel ? '30149-03.htm' : '30149-02.htm' )
                }

                return this.getPath( '30149-00.htm' )

            case QuestStateValues.STARTED:
                if ( state.isCondition( 2 ) && QuestHelper.hasQuestItem( player, KIRUNAK_SKULL ) ) {
                    await QuestHelper.giveAdena( player, 42130, true )
                    await QuestHelper.addExpAndSp( player, 35637, 1854 )
                    await state.exitQuest( false, true )

                    return this.getPath( '30149-06.html' )
                }

                return this.getPath( '30149-05.html' )

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}