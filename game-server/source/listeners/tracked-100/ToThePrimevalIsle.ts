import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { QuestStateValues } from '../../gameService/models/quest/State'

const ANTON = 31338
const MARQUEZ = 32113
const ANCIENT_BOOK = 8777

const minimumLevel = 75

export class ToThePrimevalIsle extends ListenerLogic {
    constructor() {
        super( 'Q00110_ToThePrimevalIsle', 'listeners/tracked/ToThePrimevalIsle.ts' )
        this.questId = 110
        this.questItemIds = [ ANCIENT_BOOK ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00110_ToThePrimevalIsle'
    }

    getQuestStartIds(): Array<number> {
        return [ ANTON ]
    }

    getTalkIds(): Array<number> {
        return [ ANTON, MARQUEZ ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state ) {
            return QuestHelper.getNoQuestMessagePath()
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31338-1.html':
                await QuestHelper.giveSingleItem( player, ANCIENT_BOOK, 1 )
                state.startQuest()

                break

            case '32113-2.html':
            case '32113-2a.html':
                await QuestHelper.giveAdena( player, 191678, true )
                await QuestHelper.addExpAndSp( player, 251602, 25245 )
                await state.exitQuest( false, true )

                break
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case ANTON:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() < minimumLevel ? '31338-0a.htm' : '31338-0b.htm' )

                    case QuestStateValues.STARTED:
                        return this.getPath( '31338-1a.html' )

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case MARQUEZ:
                if ( state.isCondition( 1 ) ) {
                    return this.getPath( '32113-1.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}