import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const npcIds: Array<number> = [
    36481,
    36482,
    36483,
    36484,
    36485,
    36486,
    36487,
    36488,
    36489,
]

const ORDINARY_CERTIFICATE = 13766
const ELITE_CERTIFICATE = 13767

export class PathtoBecominganEliteMercenary extends ListenerLogic {
    constructor() {
        super( 'Q00147_PathtoBecominganEliteMercenary', 'listeners/tracked/PathtoBecominganEliteMercenary.ts' )
        this.questId = 147
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00147_PathtoBecominganEliteMercenary'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return this.getPath( data.eventName )
        }

        let player = L2World.getPlayer( data.playerId )

        if ( data.eventName === 'elite-02.htm' ) {
            if ( QuestHelper.hasQuestItem( player, ORDINARY_CERTIFICATE ) ) {
                return this.getPath( 'elite-02a.htm' )
            }

            await QuestHelper.giveSingleItem( player, ORDINARY_CERTIFICATE, 1 )
        }

        if ( data.eventName === 'elite-04.htm' ) {
            state.startQuest()
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getClan() && player.getClan().getCastleId() > 0 ) {
                    return this.getPath( 'castle.htm' )
                }

                return this.getPath( 'elite-01.htm' )

            case QuestStateValues.STARTED:
                if ( state.getCondition() < 4 ) {
                    return this.getPath( 'elite-05.htm' )
                }

                if ( state.isCondition( 4 ) ) {
                    await QuestHelper.takeSingleItem( player, ORDINARY_CERTIFICATE, -1 )
                    await QuestHelper.giveSingleItem( player, ELITE_CERTIFICATE, 1 )
                    await state.exitQuest( false )

                    return this.getPath( 'elite-06.htm' )
                }

                break

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}