import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2MonsterInstance } from '../../gameService/models/actor/instance/L2MonsterInstance'
import { SkillHolder } from '../../gameService/models/holders/SkillHolder'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { ItemTypes } from '../../gameService/values/InventoryValues'
import { AIEffectHelper } from '../../gameService/aicontroller/helpers/AIEffectHelper'
import _ from 'lodash'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const SHILENS_EVIL_THOUGHTS = 27346
const WOOD = 32593
const FRANZ = 32597

const SCULPTURE_OF_DOUBT = 14355
const DAWNS_BRACELET = 15312

const minimumLevel = 79
const healSkill = new SkillHolder( 4065, 8 )
const eventNames = {
    despawn: 'despawn',
    heal: 'heal',
}

export class SevenSignsEmbryo extends ListenerLogic {
    isBusy: boolean = false

    constructor() {
        super( 'Q00198_SevenSignsEmbryo', 'listeners/tracked/SevenSignsEmbryo.ts' )
        this.questId = 198
        this.questItemIds = [ SCULPTURE_OF_DOUBT ]
    }

    getAttackableKillIds(): Array<number> {
        return [ SHILENS_EVIL_THOUGHTS ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00198_SevenSignsEmbryo'
    }

    getQuestStartIds(): Array<number> {
        return [ WOOD ]
    }

    getTalkIds(): Array<number> {
        return [ WOOD, FRANZ ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player: L2PcInstance = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 3 )
        if ( !player ) {
            return
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let npc = L2World.getObjectById( data.targetId ) as L2Npc
        if ( npc.isInsideRadius( player, 1500, true ) ) {
            await QuestHelper.giveSingleItem( player, SCULPTURE_OF_DOUBT, 1 )
            state.setConditionWithSound( 2, true )
        }

        this.stopQuestTimers( eventNames.despawn )
        this.stopQuestTimers( eventNames.heal )

        BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.S1_YOU_MAY_HAVE_WON_THIS_TIME_BUT_NEXT_TIME_I_WILL_SURELY_CAPTURE_YOU, player.getName() )
        await npc.deleteMe()

        await player.showQuestMovie( 14 )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        if ( data.characterNpcId === SHILENS_EVIL_THOUGHTS && data.eventName === eventNames.despawn ) {

            let npc = L2World.getObjectById( data.characterId ) as L2Npc

            if ( !npc.isDead() ) {
                this.isBusy = false
                BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.NEXT_TIME_YOU_WILL_NOT_ESCAPE )
                await npc.deleteMe()
            }

            return
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.characterId ) as L2Npc

        switch ( data.eventName ) {
            case '32593-02.html':
                state.startQuest()
                break

            case '32597-02.html':
            case '32597-03.html':
            case '32597-04.html':
                if ( state.isCondition( 1 ) ) {
                    break
                }

                return

            case 'fight':
                if ( state.isCondition( 1 ) ) {
                    this.isBusy = true

                    BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.S1_THAT_STRANGER_MUST_BE_DEFEATED_HERE_IS_THE_ULTIMATE_HELP, player.getName() )
                    this.startQuestTimer( eventNames.heal, 30000 - _.random( 20000 ), data.characterId, data.playerId )

                    let monster: L2MonsterInstance = QuestHelper.addGenericSpawn( null, SHILENS_EVIL_THOUGHTS, -23734, -9184, -5384, 0, false, 0, false, npc.getInstanceId() ) as L2MonsterInstance
                    monster.setRunning()

                    BroadcastHelper.broadcastNpcSayStringId( monster, NpcSayType.NpcAll, NpcStringIds.YOU_ARE_NOT_THE_OWNER_OF_THAT_ITEM )
                    AIEffectHelper.notifyAttackedWithTargetId( monster, data.playerId, 0, 999 )

                    this.startQuestTimer( eventNames.despawn, 300000, monster.getObjectId(), 0 )
                }

                return this.getPath( '32597-05.html' )

            case eventNames.heal:
                if ( !npc.isInsideRadius( player, 600, true ) ) {
                    BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.LOOK_HERE_S1_DONT_FALL_TOO_FAR_BEHIND, player.getName() )
                } else if ( !player.isDead() ) {
                    npc.setTarget( player )
                    await npc.doCastWithHolder( healSkill )
                }

                this.startQuestTimer( eventNames.heal, 30000 - _.random( 20000 ), data.characterId, data.playerId )
                return

            case '32597-08.html':
            case '32597-09.html':
            case '32597-10.html':
                if ( state.isCondition( 2 ) && QuestHelper.hasQuestItem( player, SCULPTURE_OF_DOUBT ) ) {
                    break
                }

                return

            case '32597-11.html':
                if ( state.isCondition( 2 ) && QuestHelper.hasQuestItem( player, SCULPTURE_OF_DOUBT ) ) {
                    await QuestHelper.takeSingleItem( player, SCULPTURE_OF_DOUBT, -1 )
                    state.setConditionWithSound( 3, true )

                    BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.WE_WILL_BE_WITH_YOU_ALWAYS )
                    break
                }

                return

            case '32617-02.html':
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === WOOD ) {
                    let canProceed: boolean = player.getLevel() >= minimumLevel && player.hasQuestCompleted( 'Q00197_SevenSignsTheSacredBookOfSeal' )
                    return this.getPath( canProceed ? '32593-01.htm' : '32593-03.html' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case WOOD:
                        switch ( state.getCondition() ) {
                            case 1:
                            case 2:
                                return this.getPath( '32593-04.html' )

                            case 3:
                                if ( player.getLevel() >= minimumLevel ) {
                                    await QuestHelper.addExpAndSp( player, 315108090, 34906059 )
                                    await QuestHelper.rewardSingleItem( player, DAWNS_BRACELET, 1 )
                                    await QuestHelper.rewardSingleItem( player, ItemTypes.AncientAdena, 1500000 )

                                    await state.exitQuest( false, true )
                                    return this.getPath( '32593-05.html' )
                                }

                                return this.getPath( 'level_check.html' )
                        }

                        break

                    case FRANZ:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( this.isBusy ? '32597-06.html' : '32597-01.html' )

                            case 2:
                                if ( QuestHelper.hasQuestItem( player, SCULPTURE_OF_DOUBT ) ) {
                                    return this.getPath( '32597-07.html' )
                                }

                                break

                            case 3:
                                return this.getPath( '32597-12.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}