import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const SHEGFIELD = 30068
const HEAD_BLACKSMITH_KUSTO = 30512
const RESEARCHER_LORAIN = 30673
const DOROTHY_LOCKSMITH = 30970

const REPAIRED_METALLOGRAPH = 10371

const minimumLevel = 42
const maximumLevel = 48

export class VainConclusion extends ListenerLogic {
    constructor() {
        super( 'Q00191_VainConclusion', 'listeners/tracked/VainConclusion.ts' )
        this.questId = 191
        this.questItemIds = [ REPAIRED_METALLOGRAPH ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00191_VainConclusion'
    }

    getQuestStartIds(): Array<number> {
        return [ DOROTHY_LOCKSMITH ]
    }

    getTalkIds(): Array<number> {
        return [ DOROTHY_LOCKSMITH, HEAD_BLACKSMITH_KUSTO, RESEARCHER_LORAIN, SHEGFIELD ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30970-03.htm':
                break

            case '30970-04.htm':
                if ( state.isCreated() ) {
                    state.startQuest()
                    state.setMemoState( 1 )

                    await QuestHelper.giveSingleItem( player, REPAIRED_METALLOGRAPH, 1 )
                    break
                }

                return

            case '30068-02.html':
                if ( state.isMemoState( 2 ) ) {
                    break
                }

                return

            case '30068-03.html':
                if ( state.isMemoState( 2 ) ) {
                    state.setMemoState( 3 )
                    state.setConditionWithSound( 3, true )
                    break
                }

                return

            case '30512-02.html':
                if ( state.isMemoState( 4 ) ) {
                    await QuestHelper.giveAdena( player, 117327, true )

                    if ( player.getLevel() < maximumLevel ) {
                        await QuestHelper.addExpAndSp( player, 309467, 20614 )
                    }

                    await state.exitQuest( false, true )
                    break
                }

                return

            case '30673-02.html':
                if ( state.isMemoState( 1 ) ) {
                    state.setMemoState( 2 )
                    state.setConditionWithSound( 2, true )

                    await QuestHelper.takeSingleItem( player, REPAIRED_METALLOGRAPH, -1 )
                    break
                }

                return


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                let player = L2World.getPlayer( data.playerId )
                if ( data.characterNpcId === DOROTHY_LOCKSMITH && player.hasQuestCompleted( 'Q00188_SealRemoval' ) ) {
                    return this.getPath( player.getLevel() >= minimumLevel ? '30970-01.htm' : '30970-02.htm' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case DOROTHY_LOCKSMITH:
                        if ( state.getMemoState() >= 1 ) {
                            return this.getPath( '30970-05.html' )
                        }

                        break

                    case SHEGFIELD:
                        switch ( state.getCondition() ) {
                            case 2:
                                return this.getPath( '30068-01.html' )

                            case 3:
                                return this.getPath( '30068-04.html' )
                        }

                        break

                    case HEAD_BLACKSMITH_KUSTO:
                        if ( state.isMemoState( 4 ) ) {
                            return this.getPath( '30512-01.html' )
                        }

                        break

                    case RESEARCHER_LORAIN:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '30673-01.html' )

                            case 2:
                                return this.getPath( '30673-03.html' )

                            case 3:
                                state.setMemoState( 4 )
                                state.setConditionWithSound( 4, true )
                                return this.getPath( '30673-04.html' )

                            case 4:
                                return this.getPath( '30673-05.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === DOROTHY_LOCKSMITH ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}