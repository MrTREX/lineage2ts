import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'
import { NewbieRewardsHelper } from '../helpers/NewbieRewardsHelper'

import _ from 'lodash'
import { createItemDefinition, ItemDefinition } from '../../gameService/interface/ItemDefinition'

const ROIEN = 30008
const ALTRAN = 30283

const BROKEN_SWORD_HANDLE = 739
const BROKEN_BLADE_BOTTOM = 740
const BROKEN_BLADE_TOP = 741
const ALTRANS_NOTE = 742
const ROIENS_LETTER = 796
const DIRECTIONS_TO_RUINS = 937

const MONSTERS = [
    20361, // Tunath Orc Marksman
    20362, // Tunath Orc Warrior
]

const questRewards = [
    createItemDefinition( 738, 1 ), // Sword of Solidarity
    createItemDefinition( 1060, 100 ), // Lesser Healing Potion
    createItemDefinition( 4412, 10 ), // Echo Crystal - Theme of Battle
    createItemDefinition( 4413, 10 ), // Echo Crystal - Theme of Love
    createItemDefinition( 4414, 10 ), // Echo Crystal - Theme of Solitude
    createItemDefinition( 4415, 10 ), // Echo Crystal - Theme of Feast
    createItemDefinition( 4416, 10 ), // Echo Crystal - Theme of Celebration
]

const minimumLevel = 9

export class SwordOfSolidarity extends ListenerLogic {
    constructor() {
        super( 'Q00101_SwordOfSolidarity', 'listeners/tracked/SwordOfSolidarity.ts' )
        this.questId = 101
        this.questItemIds = [
            BROKEN_SWORD_HANDLE,
            BROKEN_BLADE_BOTTOM,
            BROKEN_BLADE_TOP,
            ALTRANS_NOTE,
            ROIENS_LETTER,
            DIRECTIONS_TO_RUINS,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return MONSTERS
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00101_SwordOfSolidarity'
    }

    getQuestStartIds(): Array<number> {
        return [ ROIEN ]
    }

    getTalkIds(): Array<number> {
        return [ ROIEN, ALTRAN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )

        if ( state
                && state.isCondition( 2 )
                && _.random( 5 ) < QuestHelper.getAdjustedChance( BROKEN_BLADE_TOP, 1, data.isChampion ) ) {

            let player = L2World.getPlayer( data.playerId )

            if ( !QuestHelper.hasQuestItem( player, BROKEN_BLADE_TOP ) ) {
                await QuestHelper.giveSingleItem( player, BROKEN_BLADE_TOP, 1 )

                if ( QuestHelper.hasQuestItem( player, BROKEN_BLADE_BOTTOM ) ) {
                    state.setConditionWithSound( 3, true )
                    return
                }

                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                return
            }

            if ( !QuestHelper.hasQuestItem( player, BROKEN_BLADE_BOTTOM ) ) {
                await QuestHelper.giveSingleItem( player, BROKEN_BLADE_BOTTOM, 1 )

                if ( QuestHelper.hasQuestItem( player, BROKEN_BLADE_TOP ) ) {
                    state.setConditionWithSound( 3, true )
                    return
                }

                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                return
            }
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30008-03.html':
            case '30008-09.html':
                break

            case '30008-04.htm':
                state.startQuest()
                await QuestHelper.giveSingleItem( player, ROIENS_LETTER, 1 )
                break

            case '30283-02.html':
                if ( state.isCondition( 1 ) && QuestHelper.hasQuestItem( player, ROIENS_LETTER ) ) {
                    await QuestHelper.takeSingleItem( player, ROIENS_LETTER, -1 )
                    await QuestHelper.giveSingleItem( player, DIRECTIONS_TO_RUINS, 1 )
                    state.setConditionWithSound( 2, true )

                    break
                }

                return

            case '30283-07.html':
                if ( state.isCondition( 5 ) && QuestHelper.hasQuestItem( player, BROKEN_SWORD_HANDLE ) ) {
                    await NewbieRewardsHelper.giveNewbieReward( player )

                    _.each( questRewards, ( item: ItemDefinition ) => {
                        QuestHelper.rewardSingleItem( player, item.id, item.count )
                    } )

                    await QuestHelper.addExpAndSp( player, 25747, 2171 )
                    await QuestHelper.giveAdena( player, 10981, true )
                    await state.exitQuest( false, true )

                    break
                }

                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case ROIEN:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        if ( player.getRace() === Race.HUMAN ) {
                            return this.getPath( player.getLevel() >= minimumLevel ? '30008-02.htm' : '30008-08.htm' )
                        }

                        return this.getPath( '30008-01.htm' )

                    case QuestStateValues.STARTED:
                        switch ( state.getCondition() ) {
                            case 1:
                                if ( QuestHelper.hasQuestItem( player, ROIENS_LETTER ) ) {
                                    return this.getPath( '30008-05.html' )
                                }

                                break

                            case 2:
                                if ( QuestHelper.hasAtLeastOneQuestItem( player, BROKEN_BLADE_BOTTOM, BROKEN_BLADE_TOP ) ) {
                                    return this.getPath( '30008-11.html' )
                                }

                                if ( QuestHelper.hasQuestItem( player, DIRECTIONS_TO_RUINS ) ) {
                                    return this.getPath( '30008-10.html' )
                                }

                                break

                            case 3:
                                if ( QuestHelper.hasQuestItems( player, BROKEN_BLADE_BOTTOM, BROKEN_BLADE_TOP ) ) {
                                    return this.getPath( '30008-12.html' )
                                }

                                break

                            case 4:
                                if ( QuestHelper.hasQuestItem( player, ALTRANS_NOTE ) ) {
                                    await QuestHelper.takeSingleItem( player, ALTRANS_NOTE, -1 )
                                    await QuestHelper.giveSingleItem( player, BROKEN_SWORD_HANDLE, 1 )

                                    state.setConditionWithSound( 5, true )
                                    return this.getPath( '30008-06.html' )
                                }

                                break

                            case 5:
                                if ( QuestHelper.hasQuestItem( player, BROKEN_SWORD_HANDLE ) ) {
                                    return this.getPath( '30008-07.html' )
                                }

                                break

                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case ALTRAN:
                switch ( state.getCondition() ) {
                    case 1:
                        if ( QuestHelper.hasQuestItem( player, ROIENS_LETTER ) ) {
                            return this.getPath( '30283-01.html' )
                        }

                        break

                    case 2:
                        if ( QuestHelper.hasAtLeastOneQuestItem( player, BROKEN_BLADE_BOTTOM, BROKEN_BLADE_TOP ) ) {
                            return this.getPath( '30283-08.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, DIRECTIONS_TO_RUINS ) ) {
                            return this.getPath( '30283-03.html' )
                        }

                        break

                    case 3:
                        if ( QuestHelper.hasQuestItems( player, BROKEN_BLADE_BOTTOM, BROKEN_BLADE_TOP ) ) {
                            await QuestHelper.takeMultipleItems( player, -1, DIRECTIONS_TO_RUINS, BROKEN_BLADE_TOP, BROKEN_BLADE_BOTTOM )
                            await QuestHelper.giveSingleItem( player, ALTRANS_NOTE, 1 )

                            state.setConditionWithSound( 4, true )

                            return this.getPath( '30283-04.html' )
                        }

                        break

                    case 4:
                        if ( QuestHelper.hasQuestItem( player, ALTRANS_NOTE ) ) {
                            return this.getPath( '30283-05.html' )
                        }

                        break

                    case 5:
                        if ( QuestHelper.hasQuestItem( player, BROKEN_SWORD_HANDLE ) ) {
                            return this.getPath( '30283-06.html' )
                        }

                        break
                }

                return
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}