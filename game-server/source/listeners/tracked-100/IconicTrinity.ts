import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { DataManager } from '../../data/manager'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'

const HIERARCH_KEKROPUS = 32138
const ICON_OF_THE_PAST = 32255
const ICON_OF_THE_PRESENT = 32256
const ICON_OF_THE_FUTURE = 32257

const SCROLL_ENCHANT_ARMOR_D_GRADE = 956

const minimumLevel = 17
const maximumLevel = 20

export class IconicTrinity extends ListenerLogic {
    constructor() {
        super( 'Q00178_IconicTrinity', 'listeners/tracked/IconicTrinity.ts' )
        this.questId = 178
    }

    getQuestStartIds(): Array<number> {
        return [ HIERARCH_KEKROPUS ]
    }

    getTalkIds(): Array<number> {
        return [
            HIERARCH_KEKROPUS,
            ICON_OF_THE_PAST,
            ICON_OF_THE_PRESENT,
            ICON_OF_THE_FUTURE,
        ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32138-05.htm':
                if ( state.isCreated() ) {
                    state.startQuest()
                    state.setMemoState( 1 )

                    break
                }

                return

            case '32255-11.html':
            case '32256-11.html':
            case '32256-12.html':
            case '32256-13.html':
                return DataManager.getHtmlData().getItem( this.getPath( data.eventName ) )
                        .replace( '%name1%', player.getName() )

            case '32138-14.html':
                if ( ( state.isMemoState( 10 )
                        && player.getLevel() <= maximumLevel
                        && player.getClassId() === ClassIdValues.maleSoldier.id )
                        || player.getClassId() === ClassIdValues.femaleSoldier.id ) {
                    await QuestHelper.rewardSingleItem( player, SCROLL_ENCHANT_ARMOR_D_GRADE, 1 )
                    await QuestHelper.addExpAndSp( player, 20123, 976 )
                    await state.exitQuest( false, true )

                    break
                }

                return

            case '32138-17.html':
                if ( ( state.isMemoState( 10 )
                        && player.getLevel() > maximumLevel
                        && player.getClassId() !== ClassIdValues.maleSoldier.id )
                        || player.getClassId() !== ClassIdValues.femaleSoldier.id ) {
                    await QuestHelper.rewardSingleItem( player, SCROLL_ENCHANT_ARMOR_D_GRADE, 1 )
                    await state.exitQuest( false, true )

                    break
                }

                return

            case '32255-02.html':
                if ( state.isMemoState( 1 ) ) {
                    state.setMemoState( 2 )

                    break
                }

                return

            case '32255-03.html':
                if ( state.isMemoState( 2 ) ) {
                    state.setMemoStateEx( 1, 0 )

                    break
                }

                return

            case 'PASS1_1':
                if ( state.isMemoState( 2 ) ) {
                    state.setMemoStateEx( 1, state.getMemoStateEx( 1 ) + 1 )

                    return this.getPath( '32255-04.html' )
                }

                return

            case 'PASS1_2':
                if ( state.isMemoState( 2 ) ) {
                    state.setMemoStateEx( 1, state.getMemoStateEx( 1 ) + 10 )

                    return this.getPath( '32255-05.html' )
                }

                return

            case 'PASS1_3':
                if ( state.isMemoState( 2 ) ) {
                    state.setMemoStateEx( 1, state.getMemoStateEx( 1 ) + 100 )

                    return this.getPath( '32255-06.html' )
                }

                return

            case 'PASS1_4':
                if ( !state.isMemoState( 2 ) ) {
                    return
                }

                if ( state.getMemoStateEx( 1 ) === 111 ) {
                    state.setMemoState( 3 )
                    state.setMemoStateEx( 1, 0 )

                    return this.getPath( '32255-07.html' )
                }

                return this.getPath( '32255-08.html' )

            case '32255-13.html':
                if ( state.isMemoState( 3 ) ) {
                    state.setMemoState( 4 )
                    state.setConditionWithSound( 2, true )

                    return DataManager.getHtmlData().getItem( this.getPath( data.eventName ) )
                            .replace( '%name1%', player.getName() )
                }

                return

            case '32256-02.html':
                if ( state.isMemoState( 4 ) ) {
                    state.setMemoState( 5 )

                    break
                }

                return

            case '32256-03.html':
                if ( state.isMemoState( 5 ) ) {
                    state.setMemoStateEx( 1, 0 )

                    break
                }

                return

            case 'PASS2_1':
                if ( state.isMemoState( 5 ) ) {
                    state.setMemoStateEx( 1, state.getMemoStateEx( 1 ) + 1 )

                    return this.getPath( '32256-04.html' )
                }

                return

            case 'PASS2_2':
                if ( state.isMemoState( 5 ) ) {
                    state.setMemoStateEx( 1, state.getMemoStateEx( 1 ) + 10 )

                    return this.getPath( '32256-05.html' )
                }

                return

            case 'PASS2_3':
                if ( state.isMemoState( 5 ) ) {
                    state.setMemoStateEx( 1, state.getMemoStateEx( 1 ) + 100 )

                    return this.getPath( '32256-06.html' )
                }

                return

            case 'PASS2_4':
                if ( !state.isMemoState( 5 ) ) {
                    return
                }

                if ( state.getMemoStateEx( 1 ) === 111 ) {
                    state.setMemoState( 6 )
                    state.setMemoStateEx( 1, 0 )

                    return this.getPath( '32256-07.html' )
                }

                return this.getPath( '32256-08.html' )

            case '32256-14.html':
                if ( state.isMemoState( 6 ) ) {
                    state.setMemoState( 7 )
                    state.setConditionWithSound( 3, true )

                    return DataManager.getHtmlData().getItem( this.getPath( data.eventName ) )
                            .replace( '%name1%', player.getName() )
                }


                return

            case '32257-02.html':
                if ( state.isMemoState( 7 ) ) {
                    state.setMemoState( 8 )

                    break
                }

                return

            case '32257-03.html':
                if ( state.isMemoState( 8 ) ) {
                    state.setMemoStateEx( 1, 0 )

                    break
                }

                return

            case 'PASS3_1':
                if ( state.isMemoState( 8 ) ) {
                    state.setMemoStateEx( 1, state.getMemoStateEx( 1 ) + 1 )

                    return this.getPath( '32257-04.html' )
                }

                return

            case 'PASS3_2':
                if ( state.isMemoState( 8 ) ) {
                    state.setMemoStateEx( 1, state.getMemoStateEx( 1 ) + 10 )

                    return this.getPath( '32257-05.html' )
                }

                return

            case 'PASS3_3':
                if ( state.isMemoState( 8 ) ) {
                    state.setMemoStateEx( 1, state.getMemoStateEx( 1 ) + 100 )

                    return this.getPath( '32257-06.html' )
                }

                return

            case 'PASS3_4':
                if ( state.isMemoState( 8 ) ) {
                    state.setMemoStateEx( 1, state.getMemoStateEx( 1 ) + 1000 )

                    return this.getPath( '32257-07.html' )
                }

                return

            case 'PASS3_5':
                if ( !state.isMemoState( 8 ) ) {
                    return
                }

                if ( state.getMemoStateEx( 1 ) === 1111 ) {
                    state.setMemoState( 9 )
                    state.setMemoStateEx( 1, 0 )

                    return this.getPath( '32257-08.html' )
                }

                return this.getPath( '32257-09.html' )

            case '32257-12.html':
                if ( state.isMemoState( 9 ) ) {
                    state.setMemoState( 10 )
                    state.setConditionWithSound( 4, true )

                    return DataManager.getHtmlData().getItem( this.getPath( data.eventName ) )
                            .replace( '%name1%', player.getName() )
                }

                return

            case '32138-13.html':
            case '32138-16.html':
            case '32255-04.html':
            case '32255-05.html':
            case '32255-06.html':
            case '32255-07.html':
            case '32255-08.html':
            case '32255-09.html':
            case '32255-10.html':
            case '32255-12.html':
            case '32256-04.html':
            case '32256-05.html':
            case '32256-06.html':
            case '32256-07.html':
            case '32256-08.html':
            case '32256-09.html':
            case '32256-10.html':
            case '32257-04.html':
            case '32257-05.html':
            case '32257-06.html':
            case '32257-07.html':
            case '32257-08.html':
            case '32257-09.html':
            case '32257-10.html':
            case '32257-11.html':
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== HIERARCH_KEKROPUS ) {
                    break
                }

                if ( player.getRace() !== Race.KAMAEL ) {
                    return this.getPath( '32138-03.htm' )
                }

                if ( player.getLevel() >= minimumLevel ) {
                    return this.getPath( '32138-01.htm' )
                }

                return this.getPath( '32138-02.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case HIERARCH_KEKROPUS:
                        switch ( state.getMemoState() ) {
                            case 1:
                            case 2:
                                return this.getPath( '32138-06.html' )

                            case 3:
                                return this.getPath( '32138-07.html' )

                            case 4:
                            case 5:
                                return this.getPath( '32138-08.html' )

                            case 6:
                                return this.getPath( '32138-09.html' )

                            case 7:
                            case 8:
                                return this.getPath( '32138-10.html' )

                            case 9:
                                return this.getPath( '32138-11.html' )

                            case 10:
                                if ( ( player.getLevel() <= maximumLevel && player.getClassId() === ClassIdValues.maleSoldier.id )
                                        || player.getClassId() === ClassIdValues.femaleSoldier.id ) {
                                    return this.getPath( '32138-12.html' )
                                }

                                return this.getPath( '32138-15.html' )

                        }

                        break

                    case ICON_OF_THE_PAST:
                        switch ( state.getMemoState() ) {
                            case 1:
                                return this.getPath( '32255-01.html' )

                            case 2:
                                state.setMemoStateEx( 1, 0 )
                                return this.getPath( '32255-03.html' )

                            case 3:
                                return this.getPath( '32255-09.html' )

                            case 4:
                            case 5:
                                return this.getPath( '32255-14.html' )

                        }

                        break

                    case ICON_OF_THE_PRESENT:
                        switch ( state.getMemoState() ) {
                            case 4:
                                return this.getPath( '32256-01.html' )

                            case 5:
                                state.setMemoStateEx( 1, 0 )
                                return this.getPath( '32256-03.html' )

                            case 6:
                                return this.getPath( '32256-09.html' )

                            case 7:
                            case 8:
                                return this.getPath( '32256-15.html' )

                        }

                        break

                    case ICON_OF_THE_FUTURE:
                        switch ( state.getMemoState() ) {
                            case 7:
                                return this.getPath( '32257-01.html' )

                            case 8:
                                state.setMemoStateEx( 1, 0 )
                                return this.getPath( '32257-03.html' )

                            case 9:
                                return this.getPath( '32257-10.html' )

                            case 10:
                                return this.getPath( '32257-13.html' )

                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === HIERARCH_KEKROPUS ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00178_IconicTrinity'
    }
}