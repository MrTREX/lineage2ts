import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const NORMAN = 30210
const HAPROCK = 30255
const CARLON = 30350

const CARLONS_LETTER = 1076
const NORMANS_LETTER = 1106

const minimumLevel = 15

export class DwarvenKinship extends ListenerLogic {
    constructor() {
        super( 'Q00167_DwarvenKinship', 'listeners/tracked/DwarvenKinship.ts' )
        this.questId = 167
        this.questItemIds = [ CARLONS_LETTER, NORMANS_LETTER ]
    }

    getQuestStartIds(): Array<number> {
        return [ CARLON ]
    }

    getTalkIds(): Array<number> {
        return [ CARLON, NORMAN, HAPROCK ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30210-02.html':
                if ( state.isCondition( 2 ) && QuestHelper.hasQuestItem( player, NORMANS_LETTER ) ) {
                    await QuestHelper.giveAdena( player, 20000, true )
                    await state.exitQuest( false, true )

                    break
                }

                return

            case '30255-02.html':
                break

            case '30255-03.html':
                if ( state.isCondition( 1 ) && QuestHelper.hasQuestItem( player, CARLONS_LETTER ) ) {
                    await QuestHelper.takeSingleItem( player, CARLONS_LETTER, -1 )
                    await QuestHelper.giveSingleItem( player, NORMANS_LETTER, 1 )
                    await QuestHelper.giveAdena( player, 2000, true )
                    state.setConditionWithSound( 2 )

                    break
                }

                return

            case '30255-04.html':
                if ( state.isCondition( 1 ) && QuestHelper.hasQuestItem( player, CARLONS_LETTER ) ) {
                    await QuestHelper.giveAdena( player, 15000, true )
                    await state.exitQuest( false, true )

                    break
                }


                return

            case '30350-03.htm':
                state.startQuest()
                await QuestHelper.giveSingleItem( player, CARLONS_LETTER, 1 )

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case CARLON:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() >= minimumLevel ? '30350-02.htm' : '30350-01.htm' )

                    case QuestStateValues.STARTED:
                        if ( state.isCondition( 1 ) && QuestHelper.hasQuestItem( player, CARLONS_LETTER ) ) {
                            return this.getPath( '30350-04.html' )
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case HAPROCK:
                if ( state.isCondition( 1 ) && QuestHelper.hasQuestItem( player, CARLONS_LETTER ) ) {
                    return this.getPath( '30255-01.html' )
                }

                if ( state.isCondition( 2 ) && QuestHelper.hasQuestItem( player, NORMANS_LETTER ) ) {
                    return this.getPath( '30255-05.html' )
                }

                break

            case NORMAN:
                if ( state.isCondition( 2 ) && QuestHelper.hasQuestItem( player, NORMANS_LETTER ) ) {
                    return this.getPath( '30210-01.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00167_DwarvenKinship'
    }
}