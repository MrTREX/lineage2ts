import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const MARQUEZ = 32113
const MUSHIKA = 32114
const ASAMAH = 32115
const KARAKAWEI = 32117
const MANTARASA = 32118

const MANTARASA_EGG = 8778
const minimumLevel = 75

export class MeetingTheElroki extends ListenerLogic {
    constructor() {
        super( 'Q00124_MeetingTheElroki', 'listeners/tracked/MeetingTheElroki.ts' )
        this.questId = 124
        this.questItemIds = [ MANTARASA_EGG ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00124_MeetingTheElroki'
    }

    getQuestStartIds(): Array<number> {
        return [ MARQUEZ ]
    }

    getTalkIds(): Array<number> {
        return [ MARQUEZ, MUSHIKA, ASAMAH, KARAKAWEI, MANTARASA ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state ) {
            return QuestHelper.getNoQuestMessagePath()
        }

        switch ( data.eventName ) {
            case '32113-03.html':
                state.startQuest()
                break

            case '32113-04.html':
                if ( state.isCondition( 1 ) ) {
                    state.setConditionWithSound( 2, true )
                }

                break

            case '32114-04.html':
                if ( state.isCondition( 2 ) ) {
                    state.setConditionWithSound( 3, true )
                }

                break

            case '32115-06.html':
                if ( state.isCondition( 3 ) ) {
                    state.setConditionWithSound( 4, true )
                }

                break

            case '32117-05.html':
                if ( state.isCondition( 4 ) ) {
                    state.setConditionWithSound( 5, true )
                }

                break

            case '32118-04.html':
                if ( state.isCondition( 5 ) ) {
                    let player = L2World.getPlayer( data.playerId )
                    await QuestHelper.giveSingleItem( player, MANTARASA_EGG, 1 )
                    state.setConditionWithSound( 6, true )
                }

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case MARQUEZ:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() < minimumLevel ? '32113-01a.htm' : '32113-01.htm' )

                    case QuestStateValues.STARTED:
                        switch ( state.getVariable( 'cond' ) ) {
                            case '1':
                                return this.getPath( '32113-05.html' )
                            case '2':
                                return this.getPath( '32113-06.html' )

                            case '3':
                            case '4':
                            case '5':
                                return this.getPath( '32113-07.html' )
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case MUSHIKA:
                if ( state.isStarted() ) {
                    switch ( state.getVariable( 'cond' ) ) {
                        case '1':
                            return this.getPath( '32114-01.html' )

                        case '2':
                            return this.getPath( '32114-02.html' )

                        default:
                            return this.getPath( '32114-03.html' )
                    }
                }

                break

            case ASAMAH:
                if ( state.isStarted() ) {
                    switch ( state.getCondition() ) {
                        case 1:
                        case 2:
                            return this.getPath( '32115-01.html' )

                        case 3:
                            return this.getPath( '32115-02.html' )

                        case 4:
                            return this.getPath( '32115-07.html' )

                        case 5:
                            return this.getPath( '32115-08.html' )

                        case 6:
                            if ( QuestHelper.hasQuestItem( player, MANTARASA_EGG ) ) {

                                await QuestHelper.giveAdena( player, 100013, true )
                                await QuestHelper.addExpAndSp( player, 301922, 30294 )
                                await state.exitQuest( false, true )

                                return this.getPath( '32115-09.html' )
                            }

                            break
                    }
                }

                break

            case KARAKAWEI:
                if ( state.isStarted() ) {
                    switch ( state.getCondition() ) {
                        case 1:
                        case 2:
                        case 3:
                            return this.getPath( '32117-01.html' )

                        case 4:
                            return this.getPath( '32117-02.html' )

                        case 5:
                            return this.getPath( '32117-07.html' )

                        case 6:
                            return this.getPath( '32117-06.html' )
                    }
                }

                break

            case MANTARASA:
                if ( state.isStarted() ) {
                    switch ( state.getCondition() ) {
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                            return this.getPath( '32118-01.html' )

                        case 5:
                            return this.getPath( '32118-03.html' )

                        case 6:
                            return this.getPath( '32118-02.html' )
                    }
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}