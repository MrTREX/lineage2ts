import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'
import { EchoCrystalRewards, NewbieItemIds } from '../helpers/NewbieRewardsHelper'
import _ from 'lodash'

const KENDNELL = 30218

const KENDELLS_1ST_ORDER = 1836
const KENDELLS_2ND_ORDER = 1837
const KENDELLS_3RD_ORDER = 1838
const KENDELLS_4TH_ORDER = 1839
const KENDELLS_5TH_ORDER = 1840
const KENDELLS_6TH_ORDER = 1841
const KENDELLS_7TH_ORDER = 1842
const KENDELLS_8TH_ORDER = 1843
const KABOO_CHIEFS_1ST_TORQUE = 1844
const KABOO_CHIEFS_2ST_TORQUE = 1845

const drops = {
    27059: KENDELLS_1ST_ORDER, // Uoph (Kaboo Chief)
    27060: KENDELLS_2ND_ORDER, // Kracha (Kaboo Chief)
    27061: KENDELLS_3RD_ORDER, // Batoh (Kaboo Chief)
    27062: KENDELLS_4TH_ORDER, // Tanukia (Kaboo Chief)
    27064: KENDELLS_5TH_ORDER, // Turel (Kaboo Chief)
    27065: KENDELLS_6TH_ORDER, // Roko (Kaboo Chief)
    27067: KENDELLS_7TH_ORDER, // Kamut (Kaboo Chief)
    27068: KENDELLS_8TH_ORDER, // Murtika (Kaboo Chief)
}

const orders: Array<number> = [
    KENDELLS_1ST_ORDER,
    KENDELLS_2ND_ORDER,
    KENDELLS_3RD_ORDER,
    KENDELLS_4TH_ORDER,
    KENDELLS_5TH_ORDER,
    KENDELLS_6TH_ORDER,
    KENDELLS_7TH_ORDER,
    KENDELLS_8TH_ORDER,
]

const enum Rewards {
    RedSunsetSword = 981,
    RedSunsetStaff = 754,
}

const minimumLevel: number = 10

export class SkirmishWithOrcs extends ListenerLogic {
    constructor() {
        super( 'Q00105_SkirmishWithOrcs', 'listeners/tracked/SkirmishWithOrcs.ts' )
        this.questId = 105
        this.questItemIds = orders
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( drops ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00105_SkirmishWithOrcs'
    }

    getQuestStartIds(): Array<number> {
        return [ KENDNELL ]
    }

    getTalkIds(): Array<number> {
        return [ KENDNELL ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, npc, player, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case 27059:
            case 27060:
            case 27061:
            case 27062:
                if ( state.isCondition( 1 ) && QuestHelper.hasQuestItem( player, drops[ npc.getId() ] ) ) {
                    await QuestHelper.giveSingleItem( player, KABOO_CHIEFS_1ST_TORQUE, 1 )
                    state.setConditionWithSound( 2, true )
                }

                return

            case 27064:
            case 27065:
            case 27067:
            case 27068:
                if ( state.isCondition( 3 ) && QuestHelper.hasQuestItem( player, drops[ npc.getId() ] ) ) {
                    await QuestHelper.giveSingleItem( player, KABOO_CHIEFS_2ST_TORQUE, 1 )
                    state.setConditionWithSound( 4, true )
                }

                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state ) {
            return null
        }

        switch ( data.eventName ) {
            case '30218-04.html':
                if ( state.isCreated() ) {
                    state.startQuest()

                    await QuestHelper.giveSingleItem( L2World.getPlayer( data.playerId ), orders[ _.random( 0, 3 ) ], 1 )
                    return this.getPath( data.eventName )
                }

                return
            case '30218-05.html':
                return this.getPath( data.eventName )
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getRace() === Race.ELF ) {
                    return this.getPath( player.getLevel() >= minimumLevel ? '30218-03.htm' : '30218-02.htm' )
                }

                return this.getPath( '30218-01.htm' )

            case QuestStateValues.STARTED:
                if ( QuestHelper.hasAtLeastOneQuestItem( player, KENDELLS_1ST_ORDER, KENDELLS_2ND_ORDER, KENDELLS_3RD_ORDER, KENDELLS_4TH_ORDER ) ) {
                    return this.getPath( '30218-06.html' )
                }

                if ( state.isCondition( 2 ) && QuestHelper.hasQuestItem( player, KABOO_CHIEFS_1ST_TORQUE ) ) {

                    await QuestHelper.takeMultipleItems( player, -1, ...orders.slice( 0, 4 ) )
                    await QuestHelper.takeSingleItem( player, KABOO_CHIEFS_1ST_TORQUE, 1 )
                    await QuestHelper.giveSingleItem( player, orders[ _.random( 4, 7 ) ], 1 )

                    state.setConditionWithSound( 3, true )

                    return this.getPath( '30218-07.html' )
                }

                if ( QuestHelper.hasAtLeastOneQuestItem( player, KENDELLS_5TH_ORDER, KENDELLS_6TH_ORDER, KENDELLS_7TH_ORDER, KENDELLS_8TH_ORDER ) ) {
                    return this.getPath( '30218-08.html' )
                }

                if ( state.isCondition( 4 ) && QuestHelper.hasQuestItem( player, KABOO_CHIEFS_2ST_TORQUE ) ) {
                    player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )

                    await QuestHelper.rewardSingleItem( player, NewbieItemIds.LesserHealingPotion, 100 )
                    await QuestHelper.rewardMultipleItems( player, 10, ...EchoCrystalRewards )

                    if ( player.isMageClass() ) {
                        await QuestHelper.giveSingleItem( player, Rewards.RedSunsetStaff,1 )
                        await QuestHelper.rewardSingleItem( player, NewbieItemIds.SpiritshotNoGrade, 500 )
                    } else {
                        await QuestHelper.giveSingleItem( player, Rewards.RedSunsetSword,1 )
                        await QuestHelper.rewardSingleItem( player, NewbieItemIds.SoulshotNoGrade, 500 )
                    }

                    await QuestHelper.giveAdena( player, 17599, true )
                    await QuestHelper.addExpAndSp( player, 41478, 3555 )
                    await state.exitQuest( false, true )

                    return this.getPath( '30218-09.html' )
                }

                return

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}