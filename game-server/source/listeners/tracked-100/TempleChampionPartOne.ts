import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'

const SYLVAIN = 30070
const mobIds = [
    20083, // Granite Golem
    20144, // Hangman Tree
    20199, // Amber Basilisk
    20200, // Strain
    20201, // Ghoul
    20202, // Dead Seeker
]

const FRAGMENT = 10340
const EXECUTOR = 10334
const MISSIONARY = 10339
const minimumLevel = 35
const maximumLevel = 41

const variableNames = {
    talk: 't',
}

export class TempleChampionPartOne extends ListenerLogic {
    constructor() {
        super( 'Q00137_TempleChampionPart1', 'listeners/tracked/TempleChampionPartOne.ts' )
        this.questId = 137
        this.questItemIds = [ FRAGMENT ]
    }

    getAttackableKillIds(): Array<number> {
        return mobIds
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00137_TempleChampionPart1'
    }

    getQuestStartIds(): Array<number> {
        return [ SYLVAIN ]
    }

    getTalkIds(): Array<number> {
        return [ SYLVAIN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || !state.isStarted() || !state.isCondition( 2 ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        if ( QuestHelper.getQuestItemsCount( player, FRAGMENT ) < 30 ) {
            await QuestHelper.rewardSingleQuestItem( player, FRAGMENT, 1, data.isChampion )

            if ( QuestHelper.getQuestItemsCount( player, FRAGMENT ) >= 30 ) {
                state.setConditionWithSound( 3, true )
                return
            }

            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30070-02.htm':
                state.startQuest()
                break

            case '30070-05.html':
                state.setVariable( variableNames.talk, 1 )
                break

            case '30070-06.html':
                state.setVariable( variableNames.talk, 2 )
                break

            case '30070-08.html':
                state.unsetVariable( variableNames.talk )
                state.setConditionWithSound( 2, true )
                break

            case '30070-16.html':
                let player = L2World.getPlayer( data.playerId )
                if ( state.isCondition( 3 )
                        && QuestHelper.hasQuestItem( player, EXECUTOR )
                        && QuestHelper.hasQuestItem( player, MISSIONARY ) ) {

                    await QuestHelper.takeMultipleItems( player, -1, EXECUTOR, MISSIONARY )
                    await QuestHelper.giveAdena( player, 69146, true )

                    if ( player.getLevel() < maximumLevel ) {
                        await QuestHelper.addExpAndSp( player, 219975, 13047 )
                    }

                    await state.exitQuest( false, true )
                }

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        if ( state.isCompleted() ) {
            return QuestHelper.getAlreadyCompletedMessagePath()
        }

        switch ( state.getCondition() ) {
            case 1:
                switch ( state.getVariable( variableNames.talk ) ) {
                    case 1:
                        return this.getPath( '30070-05.html' )

                    case 2:
                        return this.getPath( '30070-06.html' )
                }

                return this.getPath( '30070-03.html' )

            case 2:
                return this.getPath( '30070-08.html' )

            case 3:
                if ( state.getVariable( variableNames.talk ) === 1 ) {
                    return this.getPath( '30070-10.html' )
                }

                if ( QuestHelper.getQuestItemsCount( player, FRAGMENT ) >= 30 ) {
                    state.setVariable( variableNames.talk, 1 )
                    await QuestHelper.takeSingleItem( player, FRAGMENT, -1 )

                    return this.getPath( '30070-09.html' )
                }

                break

            default:
                let shouldProceed: boolean = player.getLevel() >= minimumLevel && QuestHelper.hasQuestItems( player, EXECUTOR, MISSIONARY )
                return this.getPath( shouldProceed ? '30070-01.htm' : '30070-00.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}