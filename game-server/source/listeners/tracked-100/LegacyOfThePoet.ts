import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'

import _ from 'lodash'

const STARDEN = 30220

const RUMIELS_1ST_POEM = 1038
const RUMIELS_2ND_POEM = 1039
const RUMIELS_3RD_POEM = 1040
const RUMIELS_4TH_POEM = 1041

const minimumLevel = 11

export class LegacyOfThePoet extends ListenerLogic {
    constructor() {
        super( 'Q00163_LegacyOfThePoet', 'listeners/tracked/LegacyOfThePoet.ts' )
        this.questId = 163
        this.questItemIds = [ RUMIELS_1ST_POEM, RUMIELS_2ND_POEM, RUMIELS_3RD_POEM, RUMIELS_4TH_POEM ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            20372, // Baraq Orc Fighter
            20373, // Baraq Orc Warrior Leader
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00163_LegacyOfThePoet'
    }

    getQuestStartIds(): Array<number> {
        return [ STARDEN ]
    }

    getTalkIds(): Array<number> {
        return [ STARDEN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || !state.isCondition( 1 ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        if ( !QuestHelper.hasQuestItem( player, RUMIELS_1ST_POEM )
                && _.random( 10 ) < QuestHelper.getAdjustedChance( RUMIELS_1ST_POEM, 1, data.isChampion ) ) {
            await QuestHelper.giveSingleItem( player, RUMIELS_1ST_POEM, 1 )

            this.progressQuest( player, state )
        }

        if ( !QuestHelper.hasQuestItem( player, RUMIELS_2ND_POEM ) ) {
            await QuestHelper.giveSingleItem( player, RUMIELS_2ND_POEM, 1 )

            this.progressQuest( player, state )
        }

        if ( !QuestHelper.hasQuestItem( player, RUMIELS_3RD_POEM )
                && QuestHelper.getAdjustedChance( RUMIELS_3RD_POEM, _.random( 10 ), data.isChampion ) > 7 ) {
            await QuestHelper.giveSingleItem( player, RUMIELS_3RD_POEM, 1 )

            this.progressQuest( player, state )
        }


        if ( !QuestHelper.hasQuestItem( player, RUMIELS_4TH_POEM )
                && QuestHelper.getAdjustedChance( RUMIELS_4TH_POEM, _.random( 10 ), data.isChampion ) > 5 ) {
            await QuestHelper.giveSingleItem( player, RUMIELS_4TH_POEM, 1 )

            this.progressQuest( player, state )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30220-03.html':
            case '30220-04.html':
                break

            case '30220-05.htm':
                state.startQuest()
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getRace() !== Race.DARK_ELF ) {
                    return this.getPath( player.getLevel() >= minimumLevel ? '30220-02.htm' : '30220-01.htm' )
                }

                return this.getPath( '30220-00.htm' )

            case QuestStateValues.STARTED:
                if ( QuestHelper.hasQuestItems( player, RUMIELS_1ST_POEM, RUMIELS_2ND_POEM, RUMIELS_3RD_POEM, RUMIELS_4TH_POEM ) ) {
                    await QuestHelper.addExpAndSp( player, 21643, 943 )
                    await QuestHelper.giveAdena( player, 13890, true )
                    await state.exitQuest( false, true )

                    return this.getPath( '30220-07.html' )
                }

                return this.getPath( '30220-06.html' )

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    progressQuest( player: L2PcInstance, state: QuestState ): void {
        if ( QuestHelper.hasQuestItems( player, RUMIELS_1ST_POEM, RUMIELS_2ND_POEM, RUMIELS_3RD_POEM, RUMIELS_4TH_POEM ) ) {
            state.setConditionWithSound( 2, true )

            return
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }
}