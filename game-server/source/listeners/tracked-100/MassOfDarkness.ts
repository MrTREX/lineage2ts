import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { Race } from '../../gameService/enums/Race'
import { ExShowScreenMessage } from '../../gameService/packets/send/ExShowScreenMessage'
import { PacketHelper } from '../../gameService/packets/PacketVariables'

const UNDRIAS = 30130
const IRIA = 30135
const DORANKUS = 30139
const TRUDY = 30143

const UNDRIAS_LETTER = 1088
const CEREMONIAL_DAGGER = 1089
const DREVIANT_WINE = 1090
const GARMIELS_SCRIPTURE = 1091

const minimumLevel = 2
const npcItemMap = {
    IRIA: CEREMONIAL_DAGGER,
    DORANKUS: DREVIANT_WINE,
    TRUDY: GARMIELS_SCRIPTURE,
}

const screenMessageData: Buffer = PacketHelper.preservePacket( ExShowScreenMessage.fromNpcMessageId( NpcStringIds.DELIVERY_DUTY_COMPLETE_N_GO_FIND_THE_NEWBIE_GUIDE, 2, 5000, null ) )

export class MassOfDarkness extends ListenerLogic {
    constructor() {
        super( 'Q00166_MassOfDarkness', 'listeners/tracked/MassOfDarkness.ts' )
        this.questId = 166
        this.questItemIds = [
            UNDRIAS_LETTER,
            CEREMONIAL_DAGGER,
            DREVIANT_WINE,
            GARMIELS_SCRIPTURE,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00166_MassOfDarkness'
    }

    getQuestStartIds(): Array<number> {
        return [ UNDRIAS ]
    }

    getTalkIds(): Array<number> {
        return [ UNDRIAS, IRIA, DORANKUS, TRUDY ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || data.eventName !== '30130-03.htm' ) {
            return
        }

        state.startQuest()

        await QuestHelper.giveSingleItem( L2World.getPlayer( data.playerId ), UNDRIAS_LETTER, 1 )
        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case UNDRIAS:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        if ( player.getRace() === Race.DARK_ELF ) {
                            return this.getPath( player.getLevel() >= minimumLevel ? '30130-02.htm' : '30130-01.htm' )
                        }

                        return this.getPath( '30130-00.htm' )

                    case QuestStateValues.STARTED:
                        if ( state.isCondition( 2 )
                                && QuestHelper.hasQuestItems( player, UNDRIAS_LETTER, CEREMONIAL_DAGGER, DREVIANT_WINE, GARMIELS_SCRIPTURE ) ) {
                            player.sendCopyData( screenMessageData )

                            await QuestHelper.addExpAndSp( player, 5672, 466 )
                            await QuestHelper.giveAdena( player, 2966, true )
                            await state.exitQuest( false, true )

                            return this.getPath( '30130-05.html' )
                        }

                        return this.getPath( '30130-04.html' )
                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }
                break

            case IRIA:
            case DORANKUS:
            case TRUDY:
                if ( !state.isStarted() ) {
                    break
                }

                let itemId = npcItemMap[ data.characterNpcId ]
                if ( state.isCondition( 1 ) && !QuestHelper.hasQuestItem( player, itemId ) ) {
                    await QuestHelper.giveSingleItem( player, itemId, 1 )

                    if ( QuestHelper.hasQuestItems( player, CEREMONIAL_DAGGER, DREVIANT_WINE, GARMIELS_SCRIPTURE ) ) {
                        state.setConditionWithSound( 2, true )
                    }

                    return this.getPath( `${ data.characterNpcId }-01.html` )
                }

                return this.getPath( `${ data.characterNpcId }-02.html` )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}