import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const ABELLOS = 30042
const SIR_COLLIN_WINDAWOOD = 30311

const OFFICIAL_LETTER = 1019
const HASTE_POTION = 734

const minimumLevel = 3

export class FindSirWindawood extends ListenerLogic {
    constructor() {
        super( 'Q00155_FindSirWindawood', 'listeners/tracked/FindSirWindawood.ts' )
        this.questId = 155
        this.questItemIds = [ OFFICIAL_LETTER ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00155_FindSirWindawood'
    }

    getQuestStartIds(): Array<number> {
        return [ ABELLOS ]
    }

    getTalkIds(): Array<number> {
        return [ ABELLOS, SIR_COLLIN_WINDAWOOD ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || data.eventName !== '30042-03.htm' ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        state.startQuest()
        await QuestHelper.giveSingleItem( player, OFFICIAL_LETTER, 1 )

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case ABELLOS:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() >= minimumLevel ? '30042-02.htm' : '30042-01.htm' )

                    case QuestStateValues.STARTED:
                        return this.getPath( '30042-04.html' )

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case SIR_COLLIN_WINDAWOOD:
                if ( state.isStarted() && QuestHelper.hasQuestItem( player, OFFICIAL_LETTER ) ) {
                    await QuestHelper.rewardSingleItem( player, HASTE_POTION, 1 )
                    await state.exitQuest( false, true )

                    return this.getPath( '30311-01.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}