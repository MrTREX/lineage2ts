import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const WILFORD = 30005

const GIANT_TOAD = 20121

const BUCKLER = 20
const ADAMANTITE_ORE = 1024

const minimumLevel = 5

export class RecoverSmuggledGoods extends ListenerLogic {
    constructor() {
        super( 'Q00157_RecoverSmuggledGoods', 'listeners/tracked/RecoverSmuggledGoods.ts' )
        this.questId = 157
        this.questItemIds = [ ADAMANTITE_ORE ]
    }

    getAttackableKillIds(): Array<number> {
        return [ GIANT_TOAD ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00157_RecoverSmuggledGoods'
    }

    getQuestStartIds(): Array<number> {
        return [ WILFORD ]
    }

    getTalkIds(): Array<number> {
        return [ WILFORD ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state
                || !state.isCondition( 1 )
                || _.random( 10 ) >= QuestHelper.getAdjustedChance( ADAMANTITE_ORE, 4, data.isChampion ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        if ( QuestHelper.getQuestItemsCount( player, ADAMANTITE_ORE ) < 20 ) {
            await QuestHelper.rewardSingleQuestItem( player, ADAMANTITE_ORE, 1, data.isChampion )

            if ( QuestHelper.getQuestItemsCount( player, ADAMANTITE_ORE ) >= 20 ) {
                state.setConditionWithSound( 2, true )
                return
            }

            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30005-03.htm':
                break

            case '30005-04.htm':
                state.startQuest()
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30005-02.htm' : '30005-01.htm' )

            case QuestStateValues.STARTED:
                if ( state.isCondition( 2 ) && QuestHelper.getQuestItemsCount( player, ADAMANTITE_ORE ) >= 20 ) {
                    await QuestHelper.rewardSingleItem( player, BUCKLER, 1 )
                    await state.exitQuest( false, true )

                    return this.getPath( '30005-06.html' )
                }

                return this.getPath( '30005-05.html' )

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}