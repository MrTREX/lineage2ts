import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'

const JACKSON_ID = 30002
const SILVIA_ID = 30003
const ARNOLD_ID = 30041
const RANT_ID = 30054

const DELIVERY_LIST_ID = 1012
const HEAVY_WOOD_BOX_ID = 1013
const CLOTH_BUNDLE_ID = 1014
const CLAY_POT_ID = 1015
const JACKSONS_RECEIPT_ID = 1016
const SILVIAS_RECEIPT_ID = 1017
const RANTS_RECEIPT_ID = 1018

const SOULSHOT_NO_GRADE_ID = 1835
const RING_OF_KNOWLEDGE_ID = 875
const XP_REWARD_AMOUNT = 600
const minimumLevel = 2

export class DeliverGoods extends ListenerLogic {
    constructor() {
        super( 'Q00153_DeliverGoods', 'listeners/tracked/DeliverGoods.ts' )
        this.questId = 153
        this.questItemIds = [
            DELIVERY_LIST_ID,
            HEAVY_WOOD_BOX_ID,
            CLOTH_BUNDLE_ID,
            CLAY_POT_ID,
            JACKSONS_RECEIPT_ID,
            SILVIAS_RECEIPT_ID,
            RANTS_RECEIPT_ID,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00153_DeliverGoods'
    }

    getQuestStartIds(): Array<number> {
        return [ ARNOLD_ID ]
    }

    getTalkIds(): Array<number> {
        return [ JACKSON_ID, SILVIA_ID, ARNOLD_ID, RANT_ID ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || data.eventName !== '30041-02.html' ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        state.startQuest()
        await QuestHelper.giveMultipleItems( player, 1, DELIVERY_LIST_ID, HEAVY_WOOD_BOX_ID, CLOTH_BUNDLE_ID, CLAY_POT_ID )

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case ARNOLD_ID:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() >= minimumLevel ? '30041-01.htm' : '30041-00.htm' )

                    case QuestStateValues.STARTED:
                        if ( state.isCondition( 1 ) ) {
                            return this.getPath( '30041-03.html' )
                        }

                        if ( state.isCondition( 2 ) ) {
                            await QuestHelper.takeMultipleItems( player, -1, DELIVERY_LIST_ID, JACKSONS_RECEIPT_ID, SILVIAS_RECEIPT_ID, RANTS_RECEIPT_ID )

                            await QuestHelper.giveSingleItem( player, RING_OF_KNOWLEDGE_ID, 1 )
                            await QuestHelper.rewardSingleItem( player, RING_OF_KNOWLEDGE_ID, 1 )
                            await QuestHelper.addExpAndSp( player, XP_REWARD_AMOUNT, 0 )
                            await state.exitQuest( false )

                            return this.getPath( '30041-04.html' )
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case JACKSON_ID:
                if ( QuestHelper.hasQuestItem( player, HEAVY_WOOD_BOX_ID ) ) {
                    await QuestHelper.takeSingleItem( player, HEAVY_WOOD_BOX_ID, -1 )
                    await QuestHelper.giveSingleItem( player, JACKSONS_RECEIPT_ID, 1 )

                    this.notifyPlayer( player, state )
                    return this.getPath( '30002-01.html' )
                }

                return this.getPath( '30002-02.html' )

            case SILVIA_ID:
                if ( QuestHelper.hasQuestItem( player, CLOTH_BUNDLE_ID ) ) {
                    await QuestHelper.takeSingleItem( player, CLOTH_BUNDLE_ID, -1 )
                    await QuestHelper.giveSingleItem( player, SILVIAS_RECEIPT_ID, 1 )
                    await QuestHelper.rewardSingleItem( player, SOULSHOT_NO_GRADE_ID, 3 )

                    this.notifyPlayer( player, state )
                    return this.getPath( '30003-01.html' )
                }

                return this.getPath( '30003-02.html' )

            case RANT_ID:
                if ( QuestHelper.hasQuestItem( player, CLAY_POT_ID ) ) {
                    await QuestHelper.takeSingleItem( player, CLAY_POT_ID, -1 )
                    await QuestHelper.giveSingleItem( player, RANTS_RECEIPT_ID, 1 )

                    this.notifyPlayer( player, state )
                    return this.getPath( '30054-01.html' )
                }

                return this.getPath( '30054-02.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    notifyPlayer( player: L2PcInstance, state: QuestState ): void {
        if ( state.isCondition( 1 )
                && QuestHelper.hasQuestItem( player, JACKSONS_RECEIPT_ID )
                && QuestHelper.hasQuestItem( player, SILVIAS_RECEIPT_ID )
                && QuestHelper.hasQuestItem( player, RANTS_RECEIPT_ID ) ) {
            state.setConditionWithSound( 2, true )
        }
    }
}