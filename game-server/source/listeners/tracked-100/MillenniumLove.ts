import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const LILITH = 30368
const BAENEDES = 30369

const LILITHS_LETTER = 1022
const THEONS_DIARY = 1023
const GREATER_COMP_SOULSHOUT_PACKAGE_NO_GRADE = 5250

const minimumLevel = 15

export class MillenniumLove extends ListenerLogic {
    constructor() {
        super( 'Q00156_MillenniumLove', 'listeners/tracked/MillenniumLove.ts' )
        this.questId = 156
        this.questItemIds = [ LILITHS_LETTER, THEONS_DIARY ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00156_MillenniumLove'
    }

    getQuestStartIds(): Array<number> {
        return [ LILITH ]
    }

    getTalkIds(): Array<number> {
        return [ LILITH, BAENEDES ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30368-02.html':
            case '30368-03.html':
                break

            case '30368-05.htm':
                if ( player.getLevel() >= minimumLevel ) {
                    state.startQuest()
                    await QuestHelper.giveSingleItem( player, LILITHS_LETTER, 1 )

                    break
                }

                return this.getPath( '30368-04.htm' )

            case '30369-02.html':
                if ( state.isCondition( 1 ) && QuestHelper.hasQuestItem( player, LILITHS_LETTER ) ) {
                    await QuestHelper.takeSingleItem( player, LILITHS_LETTER, 1 )
                    await QuestHelper.giveSingleItem( player, THEONS_DIARY, 1 )
                    state.setConditionWithSound( 2, true )

                    break
                }

                return

            case '30369-03.html':
                if ( state.isCondition( 1 ) && QuestHelper.hasQuestItem( player, LILITHS_LETTER ) ) {
                    await QuestHelper.addExpAndSp( player, 3000, 0 )
                    await state.exitQuest( false, true )

                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case LILITH:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( '30368-01.htm' )

                    case QuestStateValues.STARTED:
                        if ( state.isCondition( 1 ) && QuestHelper.hasQuestItem( player, LILITHS_LETTER ) ) {
                            return this.getPath( '30368-06.html' )
                        }

                        if ( state.isCondition( 2 ) && QuestHelper.hasQuestItem( player, THEONS_DIARY ) ) {
                            await QuestHelper.rewardSingleItem( player, GREATER_COMP_SOULSHOUT_PACKAGE_NO_GRADE, 1 )
                            await QuestHelper.addExpAndSp( player, 3000, 0 )
                            await state.exitQuest( false, true )

                            return this.getPath( '30368-07.html' )
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case BAENEDES:
                switch ( state.getCondition() ) {
                    case 1:
                        if ( QuestHelper.hasQuestItem( player, LILITHS_LETTER ) ) {
                            return this.getPath( '30369-01.html' )
                        }

                        break

                    case 2:
                        if ( QuestHelper.hasQuestItem( player, THEONS_DIARY ) ) {
                            return this.getPath( '30369-04.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}