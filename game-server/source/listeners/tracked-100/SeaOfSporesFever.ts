import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'

import _ from 'lodash'

const COBENDELL = 30156
const BERROS = 30217
const VELTRESS = 30219
const RAYEN = 30221
const ALBERIUS = 30284
const GARTRANDELL = 30285

const DRYAD = 20013
const DRYAD_ELDER = 20019

const SWORD_OF_SENTINEL = 743
const STAFF_OF_SENTINEL = 744
const ALBERIUS_LIST = 746
const ALBERIUS_LETTER = 964
const EVERGREEN_AMULET = 965
const DRYADS_TEAR = 966
const LESSER_HEALING_POTION = 1060
const COBENDELLS_MEDICINE1 = 1130
const COBENDELLS_MEDICINE2 = 1131
const COBENDELLS_MEDICINE3 = 1132
const COBENDELLS_MEDICINE4 = 1133
const COBENDELLS_MEDICINE5 = 1134
const SOULSHOT_NO_GRADE = 1835
const SPIRITSHOT_NO_GRADE = 2509
const ECHO_CRYSTAL_THEME_OF_BATTLE = 4412
const ECHO_CRYSTAL_THEME_OF_LOVE = 4413
const ECHO_CRYSTAL_THEME_OF_SOLITUDE = 4414
const ECHO_CRYSTAL_THEME_OF_FEAST = 4415
const ECHO_CRYSTAL_THEME_OF_CELEBRATION = 4416

const minimumLevel = 12
const sentinelToItemId = {
    [ GARTRANDELL ]: COBENDELLS_MEDICINE5,
    [ RAYEN ]: COBENDELLS_MEDICINE4,
    [ VELTRESS ]: COBENDELLS_MEDICINE3,
    [ BERROS ]: COBENDELLS_MEDICINE2,
    [ ALBERIUS ]: COBENDELLS_MEDICINE1,
}

export class SeaOfSporesFever extends ListenerLogic {
    constructor() {
        super( 'Q00102_SeaOfSporesFever', 'listeners/tracked/SeaOfSporesFever.ts' )
        this.questId = 102
        this.questItemIds = [
            ALBERIUS_LIST,
            ALBERIUS_LETTER,
            EVERGREEN_AMULET,
            DRYADS_TEAR,
            COBENDELLS_MEDICINE1,
            COBENDELLS_MEDICINE2,
            COBENDELLS_MEDICINE3,
            COBENDELLS_MEDICINE4,
            COBENDELLS_MEDICINE5,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            DRYAD,
            DRYAD_ELDER,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00102_SeaOfSporesFever'
    }

    getQuestStartIds(): Array<number> {
        return [ ALBERIUS ]
    }

    getTalkIds(): Array<number> {
        return [
            ALBERIUS,
            COBENDELL,
            GARTRANDELL,
            BERROS,
            VELTRESS,
            RAYEN,
        ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )

        if ( state
                && state.isCondition( 2 )
                && _.random( 10 ) < QuestHelper.getAdjustedChance( DRYADS_TEAR, 3, data.isChampion ) ) {
            let player = L2World.getPlayer( data.playerId )

            await QuestHelper.rewardSingleQuestItem( player, DRYADS_TEAR, 1, data.isChampion )

            if ( QuestHelper.getQuestItemsCount( player, DRYADS_TEAR ) < 10 ) {
                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                return
            }

            state.setConditionWithSound( 3, true )
            return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )
        if ( !state ) {
            return
        }

        if ( data.eventName === '30284-02.htm' ) {
            let player = L2World.getPlayer( data.playerId )

            state.startQuest()
            await QuestHelper.giveSingleItem( player, ALBERIUS_LETTER, 1 )
            return this.getPath( data.eventName )
        }

        return
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case ALBERIUS:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        if ( player.getRace() === Race.ELF ) {
                            return this.getPath( player.getLevel() >= minimumLevel ? '30284-07.htm' : '30284-08.htm' )
                        }

                        return this.getPath( '30284-00.htm' )

                    case QuestStateValues.STARTED:
                        switch ( state.getCondition() ) {
                            case 1:
                                if ( QuestHelper.hasQuestItem( player, ALBERIUS_LETTER ) ) {
                                    return this.getPath( '30284-03.html' )
                                }

                                break

                            case 2:
                                if ( QuestHelper.hasQuestItem( player, EVERGREEN_AMULET ) ) {
                                    return this.getPath( '30284-09.html' )
                                }

                                break

                            case 4:
                                if ( QuestHelper.hasQuestItem( player, COBENDELLS_MEDICINE1 ) ) {
                                    await QuestHelper.takeSingleItem( player, COBENDELLS_MEDICINE1, 1 )
                                    await QuestHelper.giveSingleItem( player, ALBERIUS_LIST, 1 )

                                    state.setConditionWithSound( 5 )

                                    return this.getPath( '30284-04.html' )
                                }

                                break

                            case 5:
                                if ( QuestHelper.hasAtLeastOneQuestItem( player,
                                        COBENDELLS_MEDICINE1,
                                        COBENDELLS_MEDICINE2,
                                        COBENDELLS_MEDICINE3,
                                        COBENDELLS_MEDICINE4,
                                        COBENDELLS_MEDICINE5 ) ) {
                                    return this.getPath( '30284-05.html' )
                                }

                                break

                            case 6:
                                if ( !QuestHelper.hasAtLeastOneQuestItem( player,
                                        COBENDELLS_MEDICINE1,
                                        COBENDELLS_MEDICINE2,
                                        COBENDELLS_MEDICINE3,
                                        COBENDELLS_MEDICINE4,
                                        COBENDELLS_MEDICINE5 ) ) {

                                    await QuestHelper.rewardSingleItem( player, LESSER_HEALING_POTION, 100 )
                                    await QuestHelper.rewardSingleItem( player, ECHO_CRYSTAL_THEME_OF_BATTLE, 10 )
                                    await QuestHelper.rewardSingleItem( player, ECHO_CRYSTAL_THEME_OF_LOVE, 10 )
                                    await QuestHelper.rewardSingleItem( player, ECHO_CRYSTAL_THEME_OF_SOLITUDE, 10 )
                                    await QuestHelper.rewardSingleItem( player, ECHO_CRYSTAL_THEME_OF_FEAST, 10 )
                                    await QuestHelper.rewardSingleItem( player, ECHO_CRYSTAL_THEME_OF_CELEBRATION, 10 )

                                    if ( player.isMageClass() ) {
                                        await QuestHelper.giveSingleItem( player, STAFF_OF_SENTINEL, 1 )
                                        await QuestHelper.rewardSingleItem( player, SPIRITSHOT_NO_GRADE, 500 )
                                    } else {
                                        await QuestHelper.giveSingleItem( player, SWORD_OF_SENTINEL, 1 )
                                        await QuestHelper.rewardSingleItem( player, SOULSHOT_NO_GRADE, 500 )
                                    }

                                    await QuestHelper.addExpAndSp( player, 30202, 1339 )
                                    await QuestHelper.giveAdena( player, 6331, true )
                                    await state.exitQuest( false, true )

                                    return this.getPath( '30284-06.html' )
                                }

                                break

                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case COBENDELL:
                switch ( state.getCondition() ) {
                    case 1:
                        if ( QuestHelper.hasQuestItem( player, ALBERIUS_LETTER ) ) {
                            await QuestHelper.takeSingleItem( player, ALBERIUS_LETTER, -1 )
                            await QuestHelper.giveSingleItem( player, EVERGREEN_AMULET, 1 )

                            state.setConditionWithSound( 2, true )

                            return this.getPath( '30156-03.html' )
                        }

                        break

                    case 2:
                        if ( QuestHelper.hasQuestItem( player, EVERGREEN_AMULET ) && QuestHelper.getQuestItemsCount( player, DRYADS_TEAR ) < 10 ) {
                            return this.getPath( '30156-04.html' )
                        }

                        break

                    case 3:
                        if ( QuestHelper.getQuestItemsCount( player, DRYADS_TEAR ) >= 10 ) {
                            await QuestHelper.takeSingleItem( player, EVERGREEN_AMULET, -1 )
                            await QuestHelper.takeSingleItem( player, DRYADS_TEAR, -1 )

                            await QuestHelper.giveSingleItem( player, COBENDELLS_MEDICINE1, 1 )
                            await QuestHelper.giveSingleItem( player, COBENDELLS_MEDICINE2, 1 )
                            await QuestHelper.giveSingleItem( player, COBENDELLS_MEDICINE3, 1 )
                            await QuestHelper.giveSingleItem( player, COBENDELLS_MEDICINE4, 1 )
                            await QuestHelper.giveSingleItem( player, COBENDELLS_MEDICINE5, 1 )

                            state.setConditionWithSound( 4, true )

                            return this.getPath( '30156-05.html' )
                        }

                        break

                    case 4:
                        if ( QuestHelper.hasAtLeastOneQuestItem( player,
                                COBENDELLS_MEDICINE1,
                                COBENDELLS_MEDICINE2,
                                COBENDELLS_MEDICINE3,
                                COBENDELLS_MEDICINE4,
                                COBENDELLS_MEDICINE5 ) ) {
                            return this.getPath( '30156-06.html' )
                        }

                        break

                    case 5:
                        if ( QuestHelper.hasAtLeastOneQuestItem( player,
                                COBENDELLS_MEDICINE1,
                                COBENDELLS_MEDICINE2,
                                COBENDELLS_MEDICINE3,
                                COBENDELLS_MEDICINE4,
                                COBENDELLS_MEDICINE5 ) ) {
                            return this.getPath( '30156-07.html' )
                        }

                        break
                }

                break

            case GARTRANDELL:
            case RAYEN:
            case VELTRESS:
            case BERROS:
                if ( QuestHelper.hasQuestItems( player, ALBERIUS_LIST, sentinelToItemId[ data.characterNpcId ] ) ) {
                    await QuestHelper.takeSingleItem( player, sentinelToItemId[ data.characterNpcId ], -1 )

                    if ( !QuestHelper.hasAtLeastOneQuestItem( player,
                            COBENDELLS_MEDICINE1,
                            COBENDELLS_MEDICINE2,
                            COBENDELLS_MEDICINE3,
                            COBENDELLS_MEDICINE4,
                            COBENDELLS_MEDICINE5 ) ) {
                        state.setConditionWithSound( 6 )
                    }

                    return this.getPath( `${ data.characterNpcId }-01.html` )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}