import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const SHEGFIELD = 30068
const PANO = 30078
const ALEX = 30291
const SONIN = 31773

const STOLEN_CARGO = 10328
const HATE_CRYSTAL = 10329
const OLD_TREASURE_MAP = 10330
const SONINS_CREDENTIALS = 10331
const PANOS_CREDENTIALS = 10332
const ALEXS_CREDENTIALS = 10333
const BADGE_TEMPLE_EXECUTOR = 10334

const mobChanceMap = {
    20781: 439, // Delu Lizardman Shaman
    21104: 439, // Delu Lizardman Supplier
    21105: 504, // Delu Lizardman Special Agent
    21106: 423, // Cursed Seer
    21107: 902, // Delu Lizardman Commander
}

const minimumLevel = 35
const ITEM_COUNT = 10
const MAX_REWARD_LEVEL = 41

const variableNames = {
    talk: 't',
    pano: 'p',
    sonin: 's'
}

export class TempleExecutor extends ListenerLogic {
    constructor() {
        super( 'Q00135_TempleExecutor', 'listeners/tracked/TempleExecutor.ts' )
        this.questId = 135
        this.questItemIds = [ STOLEN_CARGO, HATE_CRYSTAL, OLD_TREASURE_MAP, SONINS_CREDENTIALS, PANOS_CREDENTIALS, ALEXS_CREDENTIALS ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( mobChanceMap ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00135_TempleExecutor'
    }

    getQuestStartIds(): Array<number> {
        return [ SHEGFIELD ]
    }

    getTalkIds(): Array<number> {
        return [ SHEGFIELD, ALEX, SONIN, PANO ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player: L2PcInstance = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 3 )
        if ( !player ) {
            return
        }

        if ( _.random( 1000 ) < QuestHelper.getAdjustedChance( STOLEN_CARGO, mobChanceMap[ data.npcId ], data.isChampion ) ) {
            if ( QuestHelper.getQuestItemsCount( player, STOLEN_CARGO ) < ITEM_COUNT ) {
                await QuestHelper.rewardSingleQuestItem( player, STOLEN_CARGO, 1, data.isChampion )
            } else if ( QuestHelper.getQuestItemsCount( player, HATE_CRYSTAL ) < ITEM_COUNT ) {
                await QuestHelper.rewardSingleQuestItem( player, HATE_CRYSTAL, 1, data.isChampion )
            } else {
                await QuestHelper.rewardSingleQuestItem( player, OLD_TREASURE_MAP, 1, data.isChampion )
            }

            if ( QuestHelper.getQuestItemsCount( player, STOLEN_CARGO ) >= ITEM_COUNT
                    && QuestHelper.getQuestItemsCount( player, HATE_CRYSTAL ) >= ITEM_COUNT
                    && QuestHelper.getQuestItemsCount( player, OLD_TREASURE_MAP ) >= ITEM_COUNT ) {
                let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), this.getName() )

                state.setConditionWithSound( 4, true )
            }
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30291-02a.html':
            case '30291-04.html':
            case '30291-05.html':
            case '30291-06.html':
            case '30068-08.html':
            case '30068-09.html':
            case '30068-10.html':
                break

            case '30068-03.htm':
                state.startQuest()
                break

            case '30068-04.html':
                state.setConditionWithSound( 2, true )
                break

            case '30291-07.html':
                state.unsetVariable( variableNames.talk )
                state.setConditionWithSound( 3, true )
                break

            case '30068-11.html':
                let player = L2World.getPlayer( data.playerId )
                await QuestHelper.giveSingleItem( player, BADGE_TEMPLE_EXECUTOR, 1 )
                await QuestHelper.giveAdena( player, 16924, true )

                if ( player.getLevel() < MAX_REWARD_LEVEL ) {
                    await QuestHelper.addExpAndSp( player, 30000, 2000 )
                }

                await state.exitQuest( false, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case SHEGFIELD:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() >= minimumLevel ? '30068-01.htm' : '30068-02.htm' )

                    case QuestStateValues.STARTED:
                        switch ( state.getCondition() ) {
                            case 1:
                                state.setConditionWithSound( 2, true )
                                return this.getPath( '30068-04.html' )

                            case 2:
                            case 3:
                                return this.getPath( '30068-05.html' )

                            case 4:
                                return this.getPath( '30068-06.html' )

                            case 5:
                                if ( state.getVariable( variableNames.talk ) ) {
                                    return this.getPath( '30068-08.html' )
                                }

                                if ( QuestHelper.hasQuestItems( player, PANOS_CREDENTIALS, SONINS_CREDENTIALS, ALEXS_CREDENTIALS ) ) {
                                    await QuestHelper.takeMultipleItems( player, -1, SONINS_CREDENTIALS, PANOS_CREDENTIALS, ALEXS_CREDENTIALS )
                                    state.setVariable( variableNames.talk, 1 )

                                    return this.getPath( '30068-07.html' )
                                }

                                return this.getPath( '30068-06.html' )
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case ALEX:
                if ( !state.isStarted() ) {
                    break
                }

                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '30291-01.html' )

                    case 2:
                        if ( state.getVariable( variableNames.talk ) ) {
                            return this.getPath( '30291-03.html' )
                        }

                        state.setVariable( variableNames.talk, 1 )
                        return this.getPath( '30291-02.html' )

                    case 3:
                        return this.getPath( '30291-08.html' )

                    case 4:
                        if ( QuestHelper.hasQuestItems( player, PANOS_CREDENTIALS, SONINS_CREDENTIALS ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, OLD_TREASURE_MAP ) < ITEM_COUNT ) {
                                return QuestHelper.getNoQuestMessagePath()
                            }

                            state.setConditionWithSound( 5, true )
                            await QuestHelper.takeSingleItem( player, OLD_TREASURE_MAP, -1 )
                            await QuestHelper.giveSingleItem( player, ALEXS_CREDENTIALS, 1 )

                            return this.getPath( '30291-10.html' )
                        }

                        return this.getPath( '30291-09.html' )

                    case 5:
                        return this.getPath( '30291-11.html' )
                }

                break

            case PANO:
                if ( !state.isStarted() ) {
                    break
                }

                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '30078-01.html' )

                    case 2:
                        return this.getPath( '30078-02.html' )

                    case 3:
                        return this.getPath( '30078-03.html' )

                    case 4:
                        if ( !state.getVariable( variableNames.pano ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, HATE_CRYSTAL ) < ITEM_COUNT ) {
                                return QuestHelper.getNoQuestMessagePath()
                            }

                            await QuestHelper.takeSingleItem( player, HATE_CRYSTAL, -1 )
                            await QuestHelper.giveSingleItem( player, PANOS_CREDENTIALS, 1 )
                            state.setVariable( variableNames.pano, 1 )

                            return this.getPath( '30078-04.html' )
                        }

                        break

                    case 5:
                        return this.getPath( '30078-05.html' )
                }

                break

            case SONIN:
                if ( !state.isStarted() ) {
                    break
                }

                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '31773-01.html' )

                    case 2:
                        return this.getPath( '31773-02.html' )

                    case 3:
                        return this.getPath( '31773-03.html' )

                    case 4:
                        if ( !state.getVariable( variableNames.sonin ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, STOLEN_CARGO ) < ITEM_COUNT ) {
                                break
                            }

                            await QuestHelper.takeSingleItem( player, STOLEN_CARGO, -1 )
                            await QuestHelper.giveSingleItem( player, SONINS_CREDENTIALS, 1 )
                            state.setVariable( variableNames.sonin, 1 )

                            return this.getPath( '31773-04.html' )
                        }

                        break
                    case 5:
                        return this.getPath( '31773-05.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}
