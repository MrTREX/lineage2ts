import { SagaLogic } from '../helpers/SagaLogic'
import { ILocational, Location } from '../../gameService/models/Location'

const npcIds: Array<number> = [
    31592,
    31273,
    31597,
    31597,
    31596,
    31646,
    31648,
    31653,
    31654,
    31655,
    31656,
    31597,
]

const itemIds: Array<number> = [
    7080,
    7607,
    7081,
    7515,
    7298,
    7329,
    7360,
    7391,
    7422,
    7453,
    7108,
    0,
]

const mobIds: Array<number> = [
    27260,
    27249,
    27308,
]

const spawnLocations: Array<ILocational> = [
    new Location( 164650, -74121, -2871 ),
    new Location( 47429, -56923, -2383 ),
    new Location( 47391, -56929, -2370 ),
]

export class SagaOfTheMaestro extends SagaLogic {
    constructor() {
        super( 'Q00100_SagaOfTheMaestro', 'listeners/tracked-100/SagaOfTheMaestro.ts' )
        this.questId = 100
    }

    getAllItemIds(): Array<number> {
        return itemIds
    }

    getAllMobIds(): Array<number> {
        return mobIds
    }

    getAllNpcIds(): Array<number> {
        return npcIds
    }

    getAllSpawnLocations(): Array<ILocational> {
        return spawnLocations
    }

    getClassId(): number {
        return 118
    }

    getPreviousClassId(): number {
        return 0x39
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00100_SagaOfTheMaestro'
    }
}