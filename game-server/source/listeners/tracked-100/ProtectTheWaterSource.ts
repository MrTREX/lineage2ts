import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'

import _ from 'lodash'
const ASTERIOS = 30154

const PLAGUE_ZOMBIE = 27017

const PLAGUE_DUST = 1035
const HYACINTH_CHARM = 1071
const HYACINTH_CHARM2 = 1072

const minimumLevel = 12

export class ProtectTheWaterSource extends ListenerLogic {
    constructor() {
        super( 'Q00159_ProtectTheWaterSource', 'listeners/tracked/ProtectTheWaterSource.ts' )
        this.questId = 159
        this.questItemIds = [ PLAGUE_DUST, HYACINTH_CHARM, HYACINTH_CHARM2 ]
    }

    getAttackableKillIds(): Array<number> {
        return [ PLAGUE_ZOMBIE ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00159_ProtectTheWaterSource'
    }

    getQuestStartIds(): Array<number> {
        return [ ASTERIOS ]
    }

    getTalkIds(): Array<number> {
        return [ ASTERIOS ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( state.getCondition() ) {
            case 1:
                if ( _.random( 100 ) < QuestHelper.getAdjustedChance( PLAGUE_DUST, 40, data.isChampion )
                        && QuestHelper.hasQuestItem( player, HYACINTH_CHARM )
                        && !QuestHelper.hasQuestItem( player, PLAGUE_DUST ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, PLAGUE_DUST, 1, data.isChampion )
                    state.setConditionWithSound( 2, true )
                }

                break

            case 3:
                if ( _.random( 100 ) < QuestHelper.getAdjustedChance( PLAGUE_DUST, 40, data.isChampion )
                        && QuestHelper.getQuestItemsCount( player, PLAGUE_DUST ) < 5
                        && QuestHelper.hasQuestItem( player, HYACINTH_CHARM2 ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, PLAGUE_DUST, 1, data.isChampion )

                    if ( QuestHelper.getQuestItemsCount( player, PLAGUE_DUST ) >= 5 ) {
                        state.setConditionWithSound( 4, true )

                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                break
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || data.eventName !== '30154-04.htm' ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        state.startQuest()
        await QuestHelper.giveSingleItem( player, HYACINTH_CHARM, 1 )

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getRace() === Race.ELF ) {
                    return this.getPath( player.getLevel() >= minimumLevel ? '30154-03.htm' : '30154-02.htm' )
                }

                return this.getPath( '30154-01.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        if ( QuestHelper.hasQuestItem( player, HYACINTH_CHARM ) && !QuestHelper.hasQuestItems( player, PLAGUE_DUST ) ) {
                            return this.getPath( '30154-05.html' )
                        }

                        break

                    case 2:
                        if ( QuestHelper.hasQuestItems( player, HYACINTH_CHARM, PLAGUE_DUST ) ) {
                            await QuestHelper.takeMultipleItems( player, -1, HYACINTH_CHARM, PLAGUE_DUST )
                            await QuestHelper.giveSingleItem( player, HYACINTH_CHARM2, 1 )
                            await state.setConditionWithSound( 3, true )

                            return this.getPath( '30154-06.html' )
                        }

                        break

                    case 3:
                        if ( QuestHelper.hasQuestItem( player, HYACINTH_CHARM2 ) ) {
                            return this.getPath( '30154-07.html' )
                        }

                        break

                    case 4:
                        if ( QuestHelper.hasQuestItem( player, HYACINTH_CHARM2 ) && QuestHelper.getQuestItemsCount( player, PLAGUE_DUST ) >= 5 ) {
                            await QuestHelper.giveAdena( player, 18250, true )
                            await state.exitQuest( false, true )

                            return this.getPath( '30154-08.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}