import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const MOIRA = 31979
const TORRANT = 32016

const FLAME_BOX = 14860
const FIRE_BOX = 8086

const minimumLevel = 80

export class StatusOfTheBeaconTower extends ListenerLogic {
    constructor() {
        super( 'Q00113_StatusOfTheBeaconTower', 'listeners/tracked/StatusOfTheBeaconTower.ts' )
        this.questId = 113
        this.questItemIds = [ FIRE_BOX, FLAME_BOX ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00113_StatusOfTheBeaconTower'
    }

    getQuestStartIds(): Array<number> {
        return [ MOIRA ]
    }

    getTalkIds(): Array<number> {
        return [ MOIRA, TORRANT ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31979-02.htm':
                state.startQuest()
                await QuestHelper.giveSingleItem( player, FLAME_BOX, 1 )

                return this.getPath( data.eventName )

            case '32016-02.html':
                if ( QuestHelper.hasQuestItem( player, FIRE_BOX ) ) {
                    await QuestHelper.giveAdena( player, 21578, true )
                    await QuestHelper.addExpAndSp( player, 76665, 5333 )
                } else {
                    await QuestHelper.giveAdena( player, 154800, true )
                    await QuestHelper.addExpAndSp( player, 619300, 44200 )
                }

                await state.exitQuest( false, true )

                return this.getPath( data.eventName )
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case MOIRA:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() >= minimumLevel ? '31979-01.htm' : '31979-00.htm' )

                    case QuestStateValues.STARTED:
                        return this.getPath( '31979-03.html' )

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case TORRANT:
                if ( state.isStarted() ) {
                    return this.getPath( '32016-01.html' )
                }

                break
        }
    }
}