import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'

import _ from 'lodash'

const NELSYA = 30348

const mobMap = {
    20456: 3, // Ashen Wolf
    20529: 1, // Young Brown Keltir
    20532: 1, // Brown Keltir
    20536: 2, // Elder Brown Keltir
}

const LESSER_HEALING_POTION = 1060
const DARK_BEZOAR = 1160

const minimumLevel = 3
const expectedCount = 13

export class ShilensHunt extends ListenerLogic {
    constructor() {
        super( 'Q00165_ShilensHunt', 'listeners/tracked/ShilensHunt.ts' )
        this.questId = 165
        this.questItemIds = [ DARK_BEZOAR ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( mobMap ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00165_ShilensHunt'
    }

    getQuestStartIds(): Array<number> {
        return [ NELSYA ]
    }

    getTalkIds(): Array<number> {
        return [ NELSYA ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || !state.isCondition( 1 ) ) {
            return
        }

        if ( _.random( 3 ) < QuestHelper.getAdjustedChance( DARK_BEZOAR, mobMap[ data.npcId ], data.isChampion ) ) {
            let player = L2World.getPlayer( data.playerId )
            await QuestHelper.rewardSingleQuestItem( player, DARK_BEZOAR, 1, data.isChampion )

            if ( QuestHelper.getQuestItemsCount( player, DARK_BEZOAR ) >= expectedCount ) {
                state.setConditionWithSound( 2, true )
            }

            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || data.eventName !== '30348-03.htm' ) {
            return
        }

        state.startQuest()
        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getRace() === Race.DARK_ELF ) {
                    return this.getPath( player.getLevel() >= minimumLevel ? '30348-02.htm' : '30348-01.htm' )
                }

                return this.getPath( '30348-00.htm' )

            case QuestStateValues.STARTED:
                if ( state.isCondition( 2 ) && QuestHelper.getQuestItemsCount( player, DARK_BEZOAR ) >= expectedCount ) {
                    await QuestHelper.rewardSingleItem( player, LESSER_HEALING_POTION, 5 )
                    await QuestHelper.addExpAndSp( player, 1000, 0 )
                    await state.exitQuest( false, true )

                    return this.getPath( '30348-05.html' )
                }

                return this.getPath( '30348-04.html' )

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}