import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { InventorySlot } from '../../gameService/values/InventoryValues'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { NewbieRewardsHelper } from '../helpers/NewbieRewardsHelper'
import aigle from 'aigle'
import { createItemDefinition, ItemDefinition } from '../../gameService/interface/ItemDefinition'

const GALLINT = 30017
const ARNOLD = 30041
const JOHNSTONE = 30043
const KENYOS = 30045

const GALLINTS_OAK_WAND = 748
const SPIRITBOUND_WAND1 = 1135
const SPIRITBOUND_WAND2 = 1136
const SPIRITBOUND_WAND3 = 1137

const monsterNpcIds = [
    27003,
    27004,
    27005,
]

const npcIdToItem = {
    27003: SPIRITBOUND_WAND1, // Spirit Of Mirrors
    27004: SPIRITBOUND_WAND2, // Spirit Of Mirrors
    27005: SPIRITBOUND_WAND3, // Spirit Of Mirrors
}

const playerRewards = [
    createItemDefinition( 1060, 100 ), // Lesser Healing Potion
    createItemDefinition( 4412, 10 ), // Echo Crystal - Theme of Battle
    createItemDefinition( 4413, 10 ), // Echo Crystal - Theme of Love
    createItemDefinition( 4414, 10 ), // Echo Crystal - Theme of Solitude
    createItemDefinition( 4415, 10 ), // Echo Crystal - Theme of Feast
    createItemDefinition( 4416, 10 ), // Echo Crystal - Theme of Celebration
    createItemDefinition( 747, 1 ), // Wand of Adept
]

const minimumLevel = 10

export class SpiritOfMirrors extends ListenerLogic {
    constructor() {
        super( 'Q00104_SpiritOfMirrors', 'listeners/tracked/SpiritOfMirrors.ts' )
        this.questId = 104
        this.questItemIds = [
            GALLINTS_OAK_WAND,
            SPIRITBOUND_WAND1,
            SPIRITBOUND_WAND2,
            SPIRITBOUND_WAND3,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return monsterNpcIds
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00104_SpiritOfMirrors'
    }

    getQuestStartIds(): Array<number> {
        return [ GALLINT ]
    }

    getTalkIds(): Array<number> {
        return [ ARNOLD, GALLINT, JOHNSTONE, KENYOS ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )

        if ( state && ( state.isCondition( 1 ) || state.isCondition( 2 ) ) ) {

            let player = L2World.getPlayer( data.playerId )

            if ( QuestHelper.getItemIdEquipped( player, InventorySlot.RightHand ) !== GALLINTS_OAK_WAND ) {
                return
            }

            if ( QuestHelper.hasQuestItem( player, npcIdToItem[ data.npcId ] ) ) {
                return
            }

            await QuestHelper.takeSingleItem( player, GALLINTS_OAK_WAND, 1 )
            await QuestHelper.giveSingleItem( npcIdToItem[ data.npcId ], 1 )

            if ( QuestHelper.hasQuestItems( player, SPIRITBOUND_WAND1, SPIRITBOUND_WAND2, SPIRITBOUND_WAND3 ) ) {
                state.setConditionWithSound( 3, true )
                return
            }

            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state ) {
            return null
        }

        if ( data.eventName === '30017-04.htm' ) {
            state.startQuest()

            let player = L2World.getPlayer( data.playerId )
            await QuestHelper.giveSingleItem( player, GALLINTS_OAK_WAND, 3 )
            return this.getPath( data.eventName )
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case GALLINT:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        if ( player.getRace() === Race.HUMAN ) {
                            return this.getPath( player.getLevel() >= minimumLevel ? '30017-03.htm' : '30017-02.htm' )
                        }

                        return this.getPath( '30017-01.htm' )

                    case QuestStateValues.STARTED:
                        if ( state.isCondition( 3 ) && QuestHelper.hasQuestItems( player, SPIRITBOUND_WAND1, SPIRITBOUND_WAND2, SPIRITBOUND_WAND3 ) ) {
                            await NewbieRewardsHelper.giveNewbieReward( player )

                            await aigle.resolve( playerRewards ).each( ( item: ItemDefinition ) => {
                                return QuestHelper.rewardSingleItem( player, item.id, item.count )
                            } )

                            await QuestHelper.addExpAndSp( player, 39750, 3407 )
                            await QuestHelper.giveAdena( player, 16866, true )
                            await state.exitQuest( false, true )

                            return this.getPath( '30017-06.html' )
                        }

                        return this.getPath( '30017-05.html' )

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case ARNOLD:
            case JOHNSTONE:
            case KENYOS:
                if ( state.isCondition( 1 ) ) {
                    let npc = L2World.getObjectById( data.characterId ) as L2Npc
                    if ( !state.getVariable( npc.getName() ) ) {
                        state.setVariable( npc.getName(), 1 )
                    }

                    if ( state.getVariable( 'Arnold' ) && state.getVariable( 'Johnstone' ) && state.getVariable( 'Kenyos' ) ) {
                        state.setConditionWithSound( 2, true )
                    }
                }

                return this.getPath( `${ data.characterNpcId }-01.html` )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}