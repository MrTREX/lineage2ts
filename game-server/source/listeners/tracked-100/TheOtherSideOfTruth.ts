import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestStateValues } from '../../gameService/models/quest/State'

const MISA = 32018
const RAFFORTY = 32020
const ICE_SCULPTURE1 = 32021
const KIER = 32022
const ICE_SCULPTURE2 = 32077
const ICE_SCULPTURE3 = 32078
const ICE_SCULPTURE4 = 32079

const MISAS_LETTER = 8079
const RAFFORTYS_LETTER = 8080
const PIECE_OF_TABLET = 8081
const REPORT_PIECE = 8082

const minimumLevel = 53
const variableNames = {
    value: 'v',
}

export class TheOtherSideOfTruth extends ListenerLogic {
    constructor() {
        super( 'Q00115_TheOtherSideOfTruth', 'listeners/tracked/TheOtherSideOfTruth.ts' )
        this.questId = 115
        this.questItemIds = [ MISAS_LETTER, RAFFORTYS_LETTER, PIECE_OF_TABLET, REPORT_PIECE ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00115_TheOtherSideOfTruth'
    }

    getQuestStartIds(): Array<number> {
        return [ RAFFORTY ]
    }

    getTalkIds(): Array<number> {
        return [ RAFFORTY, MISA, KIER, ICE_SCULPTURE1, ICE_SCULPTURE2, ICE_SCULPTURE3, ICE_SCULPTURE4 ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32020-02.html':
                state.startQuest()
                break

            case '32020-07.html':
                if ( state.isCondition( 2 ) ) {
                    await QuestHelper.takeSingleItem( player, MISAS_LETTER, -1 )
                    state.setConditionWithSound( 3, true )

                    break
                }

                return

            case '32020-05.html':
                if ( state.isCondition( 2 ) ) {
                    await QuestHelper.takeSingleItem( player, MISAS_LETTER, -1 )
                    await state.exitQuest( true, true )
                    break
                }

                return

            case '32020-10.html':
                if ( state.isCondition( 3 ) ) {
                    state.setConditionWithSound( 4, true )
                    break
                }

                return

            case '32020-11.html':
                if ( state.isCondition( 3 ) ) {
                    state.setConditionWithSound( 4, true )
                    break
                }

                return

            case '32020-12.html':
                if ( state.isCondition( 3 ) ) {
                    await state.exitQuest( true, true )
                    break
                }

                return

            case '32020-08.html':
            case '32020-09.html':
            case '32020-13.html':
            case '32020-14.html':
                break

            case '32020-15.html':
                if ( state.isCondition( 4 ) ) {
                    state.setConditionWithSound( 5, true )

                    player.sendCopyData( SoundPacket.AMBSOUND_WINGFLAP )
                    break
                }

                return

            case '32020-23.html':
                if ( state.isCondition( 9 ) ) {
                    state.setConditionWithSound( 10, true )
                    break
                }

                return

            case 'finish':
                if ( state.isCondition( 10 ) ) {
                    if ( QuestHelper.hasQuestItem( player, PIECE_OF_TABLET ) ) {
                        await QuestHelper.giveAdena( player, 115673, true )
                        await QuestHelper.addExpAndSp( player, 493595, 40442 )
                        await state.exitQuest( false, true )

                        return this.getPath( '32020-25.html' )
                    }

                    state.setConditionWithSound( 11, true )
                    player.sendCopyData( SoundPacket.AMBSOUND_THUNDER )

                    return this.getPath( '32020-26.html' )
                }

                return

            case 'finish2':
                if ( state.isCondition( 10 ) ) {
                    if ( QuestHelper.hasQuestItem( player, PIECE_OF_TABLET ) ) {
                        await QuestHelper.giveAdena( player, 115673, true )
                        await QuestHelper.addExpAndSp( player, 493595, 40442 )
                        await state.exitQuest( false, true )

                        return this.getPath( '32020-27.html' )
                    }

                    state.setConditionWithSound( 11, true )
                    player.sendCopyData( SoundPacket.AMBSOUND_THUNDER )

                    return this.getPath( '32020-28.html' )
                }

                return

            case '32018-05.html':
                if ( state.isCondition( 6 ) && QuestHelper.hasQuestItem( player, RAFFORTYS_LETTER ) ) {
                    await QuestHelper.takeSingleItem( player, RAFFORTYS_LETTER, -1 )
                    state.setConditionWithSound( 7, true )
                    break
                }

                return

            case '32022-02.html':
                if ( state.isCondition( 8 ) ) {
                    await QuestHelper.giveSingleItem( player, REPORT_PIECE, 1 )
                    state.setConditionWithSound( 9, true )
                    break
                }

                return

            case '32021-02.html':
                if ( await this.handleIceSculptureEvent( player, state, data.characterNpcId, true ) ) {
                    break
                }

                return

            case '32021-03.html':
                if ( await this.handleIceSculptureEvent( player, state, data.characterNpcId, false ) ) {
                    break
                }

                return

            case '32021-06.html':
                if ( !state.isCondition( 7 ) || ![ ICE_SCULPTURE1, ICE_SCULPTURE2, ICE_SCULPTURE3, ICE_SCULPTURE4 ].includes( data.characterNpcId ) ) {
                    return
                }

                let value: number = state.getVariable( variableNames.value )
                switch ( value ) {
                    case 7:
                    case 11:
                    case 13:
                    case 14:
                        state.setConditionWithSound( 8 )
                        break

                    default:
                        return
                }

                break
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '32020-01.htm' : '32020-03.html' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case RAFFORTY:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '32020-04.html' )

                            case 2:
                                return this.getPath( QuestHelper.hasQuestItem( player, MISAS_LETTER ) ? '32020-06.html' : '32020-05.html' )

                            case 3:
                                return this.getPath( '32020-16.html' )

                            case 4:
                                return this.getPath( '32020-17.html' )

                            case 5:
                                await QuestHelper.giveSingleItem( player, RAFFORTYS_LETTER, 1 )
                                state.setConditionWithSound( 6, true )

                                return this.getPath( '32020-18.html' )

                            case 6:
                                if ( QuestHelper.hasQuestItem( player, RAFFORTYS_LETTER ) ) {
                                    return this.getPath( '32020-19.html' )
                                }

                                await QuestHelper.giveSingleItem( player, RAFFORTYS_LETTER, 1 )
                                return this.getPath( '32020-20.html' )

                            case 7:
                            case 8:
                                return this.getPath( '32020-21.html' )

                            case 9:
                                if ( QuestHelper.hasQuestItem( player, REPORT_PIECE ) ) {
                                    return this.getPath( '32020-22.html' )
                                }

                                break

                            case 10:
                                return this.getPath( '32020-24.html' )

                            case 11:
                                if ( !QuestHelper.hasQuestItem( player, PIECE_OF_TABLET ) ) {
                                    return this.getPath( '32020-29.html' )
                                }

                                await QuestHelper.giveAdena( player, 115673, true )
                                await QuestHelper.addExpAndSp( player, 493595, 40442 )
                                await state.exitQuest( false, true )

                                return this.getPath( '32020-30.html' )

                        }

                        break

                    case MISA:
                        switch ( state.getCondition() ) {
                            case 1:
                                await QuestHelper.giveSingleItem( player, MISAS_LETTER, 1 )
                                state.setConditionWithSound( 2, true )

                                return this.getPath( '32018-01.html' )

                            case 2:
                                return this.getPath( '32018-02.html' )

                            case 3:
                            case 4:
                                return this.getPath( '32018-03.html' )

                            case 6:
                                if ( QuestHelper.hasQuestItem( player, RAFFORTYS_LETTER ) ) {
                                    return this.getPath( '32018-04.html' )
                                }

                                break

                            case 7:
                                return this.getPath( '32018-06.html' )
                        }

                        break

                    case KIER:
                        switch ( state.getCondition() ) {
                            case 8:
                                return this.getPath( '32022-01.html' )

                            case 9:
                                if ( QuestHelper.hasQuestItem( player, REPORT_PIECE ) ) {
                                    return this.getPath( '32022-03.html' )
                                }

                                await QuestHelper.giveSingleItem( player, REPORT_PIECE, 1 )
                                return this.getPath( '32022-04.html' )

                            case 11:
                                if ( !QuestHelper.hasQuestItem( player, REPORT_PIECE ) ) {
                                    return this.getPath( '32022-05.html' )
                                }

                                break
                        }

                        break

                    case ICE_SCULPTURE1:
                        switch ( state.getCondition() ) {
                            case 7:
                                let value: number = state.getVariable( variableNames.value )
                                if ( ( value % 2 ) <= 1 ) {
                                    if ( [ 6, 10, 12 ].includes( value ) ) {
                                        return this.getPath( '32021-01.html' )
                                    }

                                    if ( value === 14 ) {
                                        return this.getPath( '32021-05.html' )
                                    }

                                    state.setVariable( variableNames.value, value + 1 )
                                    return this.getPath( '32021-07.html' )
                                }

                                return this.getPath( '32021-04.html' )

                            case 8:
                                return this.getPath( '32021-08.html' )

                            case 11:
                                if ( !QuestHelper.hasQuestItem( player, PIECE_OF_TABLET ) ) {
                                    await QuestHelper.giveSingleItem( player, PIECE_OF_TABLET, 1 )

                                    return this.getPath( '32021-09.html' )
                                }

                                return this.getPath( '32021-10.html' )
                        }

                        break

                    case ICE_SCULPTURE2:
                        switch ( state.getCondition() ) {
                            case 7:
                                let value: number = state.getVariable( variableNames.value )
                                if ( ( value % 4 ) <= 1 ) {
                                    if ( [ 5, 9, 12 ].includes( value ) ) {
                                        return this.getPath( '32021-01.html' )
                                    }

                                    if ( value === 13 ) {
                                        return this.getPath( '32021-05.html' )
                                    }

                                    state.setVariable( variableNames.value, value + 2 )
                                    return this.getPath( '32021-07.html' )
                                }

                                return this.getPath( '32021-04.html' )

                            case 8:
                                return this.getPath( '32021-08.html' )

                            case 11:
                                if ( !QuestHelper.hasQuestItem( player, PIECE_OF_TABLET ) ) {
                                    await QuestHelper.giveSingleItem( player, PIECE_OF_TABLET, 1 )

                                    return this.getPath( '32021-09.html' )
                                }

                                return this.getPath( '32021-10.html' )
                        }

                        break

                    case ICE_SCULPTURE3:
                        switch ( state.getCondition() ) {
                            case 7:
                                let value: number = state.getVariable( variableNames.value )
                                if ( ( value % 8 ) <= 3 ) {
                                    if ( [ 3, 9, 10 ].includes( value ) ) {
                                        return this.getPath( '32021-01.html' )
                                    }

                                    if ( value === 11 ) {
                                        return this.getPath( '32021-05.html' )
                                    }

                                    state.setVariable( variableNames.value, value + 4 )
                                    return this.getPath( '32021-07.html' )
                                }

                                return this.getPath( '32021-04.html' )

                            case 8:
                                return this.getPath( '32021-08.html' )

                            case 11:
                                if ( !QuestHelper.hasQuestItem( player, PIECE_OF_TABLET ) ) {
                                    await QuestHelper.giveSingleItem( player, PIECE_OF_TABLET, 1 )

                                    return this.getPath( '32021-09.html' )
                                }

                                return this.getPath( '32021-10.html' )
                        }

                        break

                    case ICE_SCULPTURE4:
                        switch ( state.getCondition() ) {
                            case 7:
                                let value: number = state.getVariable( variableNames.value )
                                if ( ( value % 8 ) <= 3 ) {
                                    if ( [ 3, 5, 6 ].includes( value ) ) {
                                        return this.getPath( '32021-01.html' )
                                    }

                                    if ( value === 7 ) {
                                        return this.getPath( '32021-05.html' )
                                    }

                                    state.setVariable( variableNames.value, value + 8 )
                                    return this.getPath( '32021-07.html' )
                                }

                                return this.getPath( '32021-04.html' )

                            case 8:
                                return this.getPath( '32021-08.html' )

                            case 11:
                                if ( !QuestHelper.hasQuestItem( player, PIECE_OF_TABLET ) ) {
                                    await QuestHelper.giveSingleItem( player, PIECE_OF_TABLET, 1 )

                                    return this.getPath( '32021-09.html' )
                                }

                                return this.getPath( '32021-10.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === RAFFORTY ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async handleIceSculptureEvent( player: L2PcInstance, state: QuestState, npcId: number, shouldGiveItems: boolean = false ): Promise<boolean> {
        if ( !state.isCondition( 7 ) ) {
            return false
        }

        let value: number = state.getVariable( variableNames.value )
        switch ( npcId ) {
            case ICE_SCULPTURE1:
                if ( [ 6, 10, 12 ].includes( value ) ) {
                    state.setVariable( variableNames.value, value + 1 )

                    if ( shouldGiveItems ) {
                        await QuestHelper.giveSingleItem( player, PIECE_OF_TABLET, 1 )
                    }

                    return true
                }

                return false

            case ICE_SCULPTURE2:
                if ( [ 5, 9, 12 ].includes( value ) ) {
                    state.setVariable( variableNames.value, value + 2 )

                    if ( shouldGiveItems ) {
                        await QuestHelper.giveSingleItem( player, PIECE_OF_TABLET, 1 )
                    }

                    return true
                }

                return false

            case ICE_SCULPTURE3:
                if ( [ 3, 9, 10 ].includes( value ) ) {
                    state.setVariable( variableNames.value, value + 4 )

                    if ( shouldGiveItems ) {
                        await QuestHelper.giveSingleItem( player, PIECE_OF_TABLET, 1 )
                    }

                    return true
                }

                return

            case ICE_SCULPTURE4:
                if ( [ 3, 5, 6 ].includes( value ) ) {
                    state.setVariable( variableNames.value, value + 8 )

                    if ( shouldGiveItems ) {
                        await QuestHelper.giveSingleItem( player, PIECE_OF_TABLET, 1 )
                    }

                    return true
                }

                return false
        }

        return false
    }
}