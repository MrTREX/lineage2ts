import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const MIA = 30896
const mobIds = [
    20784, // Tasaba Lizardman
    20785, // Tasaba Lizardman Shaman
    21639, // Tasaba Lizardman
    21640, // Tasaba Lizardman Shaman
]

const FRAGMENT = 10345
const CHEST = 10346

const maximumLevel = 37
const minimumLevel = 42
const dropChance = 68

const variableNames = {
    talk: 't'
}

export class ShadowFoxPartOne extends ListenerLogic {
    constructor() {
        super( 'Q00139_ShadowFoxPart1', 'listeners/tracked/ShadowFoxPartOne.ts' )
        this.questId = 139
        this.questItemIds = [ FRAGMENT, CHEST ]
    }

    getAttackableKillIds(): Array<number> {
        return mobIds
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00139_ShadowFoxPart1'
    }

    getQuestStartIds(): Array<number> {
        return [ MIA ]
    }

    getTalkIds(): Array<number> {
        return [ MIA ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player: L2PcInstance = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 2 )
        if ( !player ) {
            return
        }

        let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), this.getName() )
        let itemId = _.random( 11 ) === 0 ? CHEST : FRAGMENT

        if ( !state.getVariable( variableNames.talk ) && _.random( 100 ) < QuestHelper.getAdjustedChance( itemId, dropChance, data.isChampion ) ) {
            await QuestHelper.rewardSingleQuestItem( player, itemId, 1, data.isChampion )
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30896-02.htm':
                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '30896-03.htm' )
                }
                break

            case '30896-04.htm':
                state.startQuest()
                break

            case '30896-11.html':
                state.setVariable( variableNames.talk, true )
                break

            case '30896-13.html':
                state.setConditionWithSound( 2, true )
                state.unsetVariable( variableNames.talk )
                break

            case '30896-17.html':
                if ( _.random( 20 ) < 3 ) {
                    await QuestHelper.takeSingleItem( player, FRAGMENT, 10 )
                    await QuestHelper.takeSingleItem( player, CHEST, 1 )

                    return this.getPath( '30896-16.html' )
                }

                await QuestHelper.takeMultipleItems( player, -1, FRAGMENT, CHEST )
                state.setVariable( variableNames.talk, true )
                break

            case '30896-19.html':

                if ( player.getLevel() <= maximumLevel ) {
                    await QuestHelper.addExpAndSp( player, 30000, 2000 )
                }

                await QuestHelper.giveAdena( player, 14050, true )
                await state.exitQuest( false, true )
                break

            case '30896-06.html':
            case '30896-07.html':
            case '30896-08.html':
            case '30896-09.html':
            case '30896-10.html':
            case '30896-12.html':
            case '30896-18.html':
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.hasQuestCompleted( 'Q00138_TempleChampionPart1' ) ? '30896-01.htm' : '30896-00.html' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( state.getVariable( variableNames.talk ) ? '30896-11.html' : '30896-05.html' )

                    case 2:
                        if ( state.getVariable( variableNames.talk ) ) {
                            return this.getPath( '30896-18.html' )
                        }

                        let outcome: string = QuestHelper.getQuestItemsCount( player, FRAGMENT ) >= 10 && QuestHelper.getQuestItemsCount( player, CHEST ) >= 1 ? '30896-15.html' : '30896-14.html'
                        return this.getPath( outcome )
                }

                break

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}