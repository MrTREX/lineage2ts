import { InstanceLogic } from '../helpers/InstanceLogic'
import { AttackableKillEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { InstanceWorld } from '../../gameService/models/instancezone/InstanceWorld'
import { InstanceManager } from '../../gameService/instancemanager/InstanceManager'
import { QuestHelper } from '../helpers/QuestHelper'
import { Location } from '../../gameService/models/Location'
import { PlayerGroupCache } from '../../gameService/cache/PlayerGroupCache'
import { L2Party } from '../../gameService/models/L2Party'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2World } from '../../gameService/L2World'
import { FortManager } from '../../gameService/instancemanager/FortManager'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import _ from 'lodash'
import aigle from 'aigle'

const npcToInstanceId: { [ npcId: number ]: number } = {
    36403: 13, // Gludio
    36404: 14, // Dion
    36405: 15, // Giran
    36406: 16, // Oren
    36407: 17, // Aden
    36408: 18, // Innadril
    36409: 19, // Goddard
    36410: 20, // Rune
    36411: 21, // Schuttgart
}

const npcIds: Array<number> = _.keys( npcToInstanceId ).map( value => _.parseInt( value ) )

const phaseOneBossIds: Array<number> = [
    25546, // Rhianna the Traitor
    25549, // Tesla the Deceiver
    25552, // Soul Hunter Chakundel
]

const phaseTwoBossIds: Array<number> = [
    25553, // Durango the Crusher
    25554, // Brutus the Obstinate
    25557, // Ranger Karankawa
    25560, // Sargon the Mad
]

const phaseThreeBossIds: Array<number> = [
    25563, // Beautiful Atrielle
    25566, // Nagen the Tomboy
    25569, // Jax the Destroyer
]

const worldStatusPhases: { [ key: number ]: Array<number> } = {
    0: phaseOneBossIds,
    1: phaseTwoBossIds,
    2: phaseThreeBossIds,
}

const residenceToFortIds: { [ residenceId: number ]: Array<number> } = {
    1: [ 101, 102, 112, 113 ], // Gludio Castle
    2: [ 103, 112, 114, 115 ], // Dion Castle
    3: [ 104, 114, 116, 118, 119 ], // Giran Castle
    4: [ 105, 113, 115, 116, 117 ], // Oren Castle
    5: [ 106, 107, 117, 118 ], // Aden Castle
    6: [ 108, 119 ], // Innadril Castle
    7: [ 109, 117, 120 ], // Goddard Castle
    8: [ 110, 120, 121 ], // Rune Castle
    9: [ 111, 121 ], // Schuttgart Castle
}

const teleportLocations: Array<Location> = [
    new Location( 12188, -48770, -3008 ),
    new Location( 12218, -48770, -3008 ),
    new Location( 12248, -48770, -3008 ),
]

export class CastleDungeon extends InstanceLogic {
    constructor() {
        super( 'CastleDungeon', 'listeners/instances/CastleDungeon.ts' )
    }

    getApproachedForTalkIds(): Array<number> {
        return npcIds
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    getAttackableKillIds(): Array<number> {
        return [
            ...phaseOneBossIds,
            ...phaseTwoBossIds,
            ...phaseThreeBossIds,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/instances/CastleDungeon/CastleDungeon.java'
    }

    async onApproachedForTalk(): Promise<string> {
        return this.getPath( '36403.html' )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let world: InstanceWorld = InstanceManager.getWorld( data.instanceId )
        if ( world.getInstanceName() !== this.getName() ) {
            return
        }

        if ( phaseThreeBossIds.includes( data.npcId ) ) {
            this.finishInstance( world )
            return
        }

        world.status = world.status + 1
        this.spawnWorldMonsters( world )
    }

    spawnWorldMonsters( world: InstanceWorld ): void {
        let npcId: number = _.sample( worldStatusPhases[ world.getStatus() ] )
        QuestHelper.addGenericSpawn( null, npcId, 11793, -49190, -3008, 0, false, 0, false, world.getInstanceId() )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let party: L2Party = PlayerGroupCache.getParty( data.playerId )
        if ( !party ) {
            return this.getPath( '36403-01.html' )
        }

        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        let castle = npc.getCastle()

        if ( castle.getSiege().isInProgress() ) {
            return this.getPath( '36403-04.html' )
        }

        let player = L2World.getPlayer( data.playerId )
        if ( npc.isLord( player ) || ( player.getClan()
                && castle.getResidenceId() === player.getClan().getCastleId()
                && player.getClan().getCastleId() > 0 ) ) {

            let lastIndex: number = [ 1, 5 ].includes( castle.getResidenceId() ) ? 2 : 1
            let fortIds: Array<number> = residenceToFortIds[ castle.getResidenceId() ].slice( 0, lastIndex )
            let hasCorrectFortState = _.some( fortIds, ( fortId: number ) => {
                return FortManager.getFortById( fortId ).getFortState() === 0
            } )

            if ( hasCorrectFortState ) {
                return this.getPath( '36403-05.html' )
            }
        }

        let currentTime = Date.now()
        let response: string

        await aigle.resolve( party.getMembers() ).eachSeries( async ( objectId: number ) => {
            let partyMember = L2World.getPlayer( objectId )
            if ( !partyMember.getClan() || partyMember.getClan().getCastleId() !== castle.getResidenceId() ) {
                response = '36403-02.html'
                return false
            }

            if ( currentTime < await InstanceManager.getInstanceTime( objectId, npcToInstanceId[ data.characterNpcId ] ) ) {
                response = '36403-03.html'
                return false
            }
        } )

        if ( response ) {
            return this.getPath( response )
        }

        await this.enterInstance( player, this.getName(), npcToInstanceId[ data.characterNpcId ] )
    }

    async onEnterInstance( player: L2PcInstance, world: InstanceWorld, isEnteringFirstTime: boolean ): Promise<void> {
        if ( isEnteringFirstTime ) {
            world.status = 0
            this.spawnWorldMonsters( world )
        }

        let party = PlayerGroupCache.getParty( player.getObjectId() )
        if ( !party ) {
            return player.teleportToLocation( _.sample( teleportLocations ), true, world.getInstanceId() )
        }

        _.each( party.getMembers(), ( playerId: number ) => {
            let partyMember = L2World.getPlayer( playerId )
            partyMember.teleportToLocation( _.sample( teleportLocations ), true, world.getInstanceId() )
        } )
    }
}