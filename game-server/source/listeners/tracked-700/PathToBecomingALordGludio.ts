import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { DataManager } from '../../data/manager'
import { TerritoryWarManager } from '../../gameService/instancemanager/TerritoryWarManager'
import { ExShowScreenMessage, ExShowScreenMessagePosition } from '../../gameService/packets/send/ExShowScreenMessage'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { FortManager } from '../../gameService/instancemanager/FortManager'
import { AIEffectHelper } from '../../gameService/aicontroller/helpers/AIEffectHelper'
import _ from 'lodash'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const PINTER = 30298
const BATHIS = 30332
const SAYRES = 35100
const VARNISH = 1865
const ANIMAL_SKIN = 1867
const IRON_ORE = 1869
const COKES = 1879
const HEADLESS_ARMOR = 13848
const HEADLESS_KNIGHT = 27393
const GLUDIO_ID = 81
const SHANTY = 101
const SOUTHERN = 102
const minimumWaitTime = 60000

const variableNames = {
    flag: 'fg',
    value: 'value',
}

export class PathToBecomingALordGludio extends ListenerLogic {
    constructor() {
        super( 'Q00708_PathToBecomingALordGludio', 'listeners/tracked-700/PathToBecomingALordGludio.ts' )
        this.questId = 708
        this.questItemIds = [ HEADLESS_ARMOR ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            20035, // Skeleton Tracker
            20042, // Skeleton Tracker Leader
            20045, // Skeleton Scout
            20051, // Skeleton Bowman
            20054, // Ruin Spartoi
            20060, // Raging Spartoi
            20514, // Shield Skeleton
            20515, // Skeleton Infantryman
            HEADLESS_KNIGHT,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ SAYRES ]
    }

    getTalkIds(): Array<number> {
        return [ SAYRES, PINTER, BATHIS ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00708_PathToBecomingALordGludio'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        let leaderState: QuestState = this.getClanLeaderQuestState( player )

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( state.isCreated()
                        && npc.isLord( player )
                        && npc.getCastle().getResidenceId() === GLUDIO_ID ) {
                    state.startQuest()

                    state.setMemoState( 1 )
                    state.setVariable( variableNames.value, Date.now() )
                    state.setVariable( variableNames.flag, 1 )

                    this.startQuestTimer( '70801', 60000, data.characterId, data.playerId )

                    return this.getPath( '35100-04.htm' )
                }

                return

            case '70801':
                BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.HAVE_YOU_COMPLETED_YOUR_PREPARATIONS_TO_BECOME_A_LORD )
                return

            case '35100-02.htm':
                if ( state.getMemoState() && npc.isLord( player ) && !npc.isDominionOfLord( GLUDIO_ID ) ) {
                    break
                }

                return

            case '35100-08.html':
                if ( state.getMemoState() === 2 && !npc.isDominionOfLord( GLUDIO_ID ) ) {
                    state.setMemoState( 3 )
                    state.setVariable( variableNames.flag, 2 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                    break
                }

                return

            case '35100-12.html':
                if ( leaderState
                        && npc.calculateDistance( leaderState.getPlayer() ) <= 1500
                        && leaderState.isMemoState( 3 )
                        && npc.getCastle().getOwnerId() === player.getClanId()
                        && player.getClanId() ) {
                    BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.S1_NOW_DEPART, player.getName() )

                    leaderState.setMemoState( 4 )
                    leaderState.setVariable( variableNames.flag, 3 )
                    leaderState.setVariable( variableNames.value, data.playerId )
                    leaderState.showQuestionMark( this.getId() )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                    return DataManager.getHtmlData().getItem( this.getPath( data.eventName ) )
                            .replace( '%name%', player.getName() )
                }

                return this.getPath( '35100-13.html' )

            case '35100-23.html':
                if ( state.isMemoState( 49 ) ) {
                    if ( !TerritoryWarManager.isTWInProgress && npc.isLord( player ) ) {
                        BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.S1_HAS_BECOME_LORD_OF_THE_TOWN_OF_GLUDIO_LONG_MAY_HE_REIGN, player.getName() )

                        await state.exitQuest( true, true )
                        break
                    }
                }

                return

            case '30298-04.html':
                if ( leaderState && leaderState.getMemoState() === 4 ) {
                    break
                }

                return

            case '30298-05.html':
                if ( leaderState && leaderState.getMemoState() === 4 ) {
                    let message: Buffer = ExShowScreenMessage.fromNpcMessageId( NpcStringIds.GO_FIND_SAIUS, ExShowScreenMessagePosition.TopCenter, 5000, undefined )
                    player.sendOwnedData( message )

                    state.setMemoState( 5 )
                    state.setVariable( variableNames.value, 0 )
                    state.setVariable( variableNames.flag, 4 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                    break
                }

                return this.getPath( '30298-06.html' )

            case '30298-09.html':
                if ( leaderState && leaderState.getMemoState() % 10 === 5 ) {
                    if ( QuestHelper.getQuestItemsCount( player, ANIMAL_SKIN ) >= 100
                            && QuestHelper.getQuestItemsCount( player, VARNISH ) >= 100
                            && QuestHelper.getQuestItemsCount( player, IRON_ORE ) >= 100
                            && QuestHelper.getQuestItemsCount( player, COKES ) >= 50 ) {
                        await QuestHelper.takeMultipleItems( player, 100, ANIMAL_SKIN, VARNISH, IRON_ORE )
                        await QuestHelper.takeSingleItem( player, COKES, 50 )

                        state.setMemoState( 9 + state.getMemoState() )
                        break
                    }

                    return this.getPath( '30298-10.html' )
                }

                return this.getPath( '30298-11.html' )

            case '30332-02.html':
                if ( Math.floor( state.getMemoState() / 10 ) === 1 ) {
                    state.setMemoState( state.getMemoState() + 10 )
                    state.setVariable( variableNames.flag, 6 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                    break
                }

                return

            case '30332-05.html':
                if ( Math.floor( state.getMemoState() / 10 ) === 3 ) {
                    await QuestHelper.takeSingleItem( player, HEADLESS_ARMOR, -1 )
                    BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.LISTEN_YOU_VILLAGERS_OUR_LIEGE_WHO_WILL_SOON_BECOME_A_LORD_HAS_DEFEATED_THE_HEADLESS_KNIGHT_YOU_CAN_NOW_REST_EASY )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                    state.setMemoState( state.getMemoState() + 10 )


                    if ( ( state.getMemoState() % 10 ) === 9 ) {
                        state.setVariable( variableNames.flag, 9 )
                        break
                    }

                    state.setVariable( variableNames.flag, 8 )
                    return this.getPath( '30332-05.html' )
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    getClanLeaderQuestState( player: L2PcInstance ): QuestState {
        let clan = player.getClan()
        if ( !clan ) {
            return
        }

        return QuestStateCache.getQuestState( clan.getLeaderId(), this.getName(), false )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), -1, 1, L2World.getObjectById( data.targetId ) )
        if ( !state ) {
            return
        }

        let player = state.getPlayer()

        if ( data.npcId === HEADLESS_KNIGHT ) {
            await QuestHelper.rewardSingleQuestItem( player, HEADLESS_ARMOR, 1, data.isChampion )

            state.setMemoState( state.getMemoState() + 10 )
            state.setVariable( variableNames.flag, 7 )
            state.showQuestionMark( this.getId() )

            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
            let npc = L2World.getObjectById( data.targetId ) as L2Npc

            BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.DOES_MY_MISSION_TO_BLOCK_THE_SUPPLIES_END_HERE )
            return
        }

        let value = state.getVariable( variableNames.value ) || 0
        if ( _.random( 100 ) < value ) {
            let headlessKnight = QuestHelper.addSpawnAtLocation( HEADLESS_KNIGHT, L2World.getObjectById( data.targetId ), false, 0 )
            AIEffectHelper.notifyAttacked( headlessKnight, player )
            BroadcastHelper.broadcastNpcSayStringId( headlessKnight, NpcSayType.NpcAll, NpcStringIds.S1_DO_YOU_DARE_DEFY_MY_SUBORDINATES, player.getName() )

            return
        }

        state.setVariable( variableNames.value, value + 1 )
    }

    checkPartyMemberConditions( state: QuestState ): boolean {
        return state.getPlayer().isClanLeader()
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.characterId ) as L2Npc

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( npc.isLord( player ) ) {
                    if ( !npc.isDominionOfLord( GLUDIO_ID ) ) {
                        return this.getPath( '35100-01.htm' )
                    }

                    return this.getPath( '35100-03.htm' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case SAYRES:
                        if ( state.getMemoState() === 1 && npc.isLord( player ) ) {
                            if ( ( Date.now() - state.getVariable( variableNames.value ) ) < minimumWaitTime ) {
                                return this.getPath( '35100-05.html' )
                            }

                            state.setMemoState( 2 )
                            state.setVariable( variableNames.value, 0 )

                            return this.getPath( '35100-06.html' )

                        }

                        if ( state.getMemoState() === 2 ) {
                            return this.getPath( '35100-07.html' )
                        }

                        if ( !npc.isLord( player ) ) {
                            if ( npc.getCastle().getOwnerId() === player.getClanId() && player.getClanId() ) {
                                let leaderState: QuestState = QuestStateCache.getQuestState( player.getClan().getLeaderId(), this.getName(), false )
                                if ( leaderState ) {
                                    if ( leaderState.getMemoState() === 3 ) {
                                        if ( npc.calculateDistance( leaderState.getPlayer() ) <= 1500 ) {
                                            return this.getPath( '35100-11.html' )
                                        }

                                        return this.getPath( '35100-10.html' )
                                    }

                                    if ( leaderState.getMemoState() === 4 ) {
                                        return this.getPath( '35100-13a.html' )
                                    }

                                    return this.getPath( '35100-09.html' )
                                }

                                return this.getPath( '35100-09.html' )
                            }

                            if ( npc.getCastle().getOwnerId() !== player.getClanId() ) {
                                return this.getPath( '35100-09.html' )
                            }

                            break
                        }

                        if ( state.getMemoState() === 3 ) {
                            return this.getPath( '35100-14.html' )
                        }

                        if ( state.getMemoState() === 4 ) {
                            return this.getPath( '35100-15.html' )
                        }

                        if ( state.getMemoState() === 5 ) {
                            state.setMemoState( state.getMemoState() + 10 )
                            state.setVariable( variableNames.flag, 5 )

                            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                            return this.getPath( '35100-16.html' )
                        }

                        let valueDivided: number = Math.floor( state.getMemoState() / 10 )
                        let valueMod: number = state.getMemoState() % 10

                        switch ( valueDivided ) {
                            case 0:
                                state.setMemoState( state.getMemoState() + 10 )
                                state.setVariable( variableNames.flag, 5 )

                                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                            case 1:
                                if ( valueMod === 9 ) {
                                    return this.getPath( '35100-17.html' )
                                }

                                return this.getPath( '35100-18.html' )

                            case 2:
                            case 3:
                                if ( valueMod === 9 ) {
                                    return this.getPath( '35100-19.html' )
                                }

                                return this.getPath( '35100-20.html' )

                            case 4:
                                if ( valueMod !== 9 ) {
                                    return this.getPath( '35100-21.html' )
                                }

                                if ( !npc.isLord( player ) ) {
                                    break
                                }

                                if ( TerritoryWarManager.isTWInProgress || npc.getCastle().getSiege().isInProgress() ) {
                                    return this.getPath( '35100-22a.html' )
                                }

                                if ( FortManager.getFortById( SHANTY ).getFortState() === 0
                                        || FortManager.getFortById( SOUTHERN ).getFortState() === 0 ) {
                                    return this.getPath( '35100-22b.html' )
                                }

                                return this.getPath( '35100-22.html' )
                        }

                        break

                    case PINTER:
                        if ( !npc.isLord( player ) && player.getClan() ) {
                            let leaderState: QuestState = QuestStateCache.getQuestState( player.getClan().getLeaderId(), this.getName(), false )
                            if ( leaderState ) {
                                if ( leaderState.getMemoState() <= 3 ) {
                                    return this.getPath( '30298-02.html' )
                                }

                                if ( leaderState.getMemoState() === 4 ) {
                                    if ( data.playerId === state.getVariable( variableNames.value ) ) {
                                        return this.getPath( '30298-03.html' )
                                    }

                                    return this.getPath( '30298-03a.html' )
                                }

                                let value: number = leaderState.getMemoState() % 10
                                if ( value === 5 ) {
                                    if ( QuestHelper.getQuestItemsCount( player, ANIMAL_SKIN ) >= 100
                                            && QuestHelper.getQuestItemsCount( player, VARNISH ) >= 100
                                            && QuestHelper.getQuestItemsCount( player, IRON_ORE ) >= 100
                                            && QuestHelper.getQuestItemsCount( player, COKES ) >= 50 ) {
                                        return this.getPath( '30298-08.html' )
                                    }

                                    return this.getPath( '30298-07.html' )
                                }

                                if ( value === 9 ) {
                                    return this.getPath( '30298-12.html' )
                                }

                                break
                            }

                            return this.getPath( '30298-01.html' )
                        }

                        return this.getPath( '30298-13.html' )

                    case BATHIS:
                        let stateValue = Math.floor( state.getMemoState() / 10 )
                        switch ( stateValue ) {
                            case 1:
                                return this.getPath( '30332-01.html' )

                            case 2:
                                return this.getPath( '30332-03.html' )

                            case 3:
                                return this.getPath( '30332-04.html' )

                            case 4:
                                return this.getPath( '30332-06.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}