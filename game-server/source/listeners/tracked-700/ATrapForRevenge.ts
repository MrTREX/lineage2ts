import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'

import _ from 'lodash'

const PLENOS = 32563
const LEKON = 32557
const TENIUS = 32555
const DRAKES_FLESH = 13877
const ROTTEN_BLOOD = 13878
const BAIT_FOR_DRAKES = 13879
const VARIANT_DRAKE_WING_HORNS = 13880
const EXTRACTED_RED_STAR_STONE = 14009
const minimumLevel = 78

const monsterRewardChances : {[ npcId: number ] : number } = {
    22612: 0.413,
    22613: 0.440,
    25632: 0.996,
    22610: 0.485,
    22611: 0.451,
    25631: 0.485,
    25626: 1
}

export class ATrapForRevenge extends ListenerLogic {
    constructor() {
        super( 'Q00702_ATrapForRevenge', 'listeners/tracked-600/ATrapForRevenge.ts' )
        this.questId = 702
        this.questItemIds = [
            DRAKES_FLESH,
            ROTTEN_BLOOD,
            BAIT_FOR_DRAKES,
            VARIANT_DRAKE_WING_HORNS,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getQuestStartIds(): Array<number> {
        return [ PLENOS ]
    }

    getTalkIds(): Array<number> {
        return [ PLENOS, LEKON, TENIUS ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00702_ATrapForRevenge'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32563-04.htm':
                state.startQuest()
                break

            case '32563-07.html':
                return this.getPath( QuestHelper.hasQuestItem( player, DRAKES_FLESH ) ? '32563-08.html' : '32563-07.html' )

            case '32563-09.html':
                let adenaAmount = QuestHelper.getQuestItemsCount( player, DRAKES_FLESH ) * 100
                await QuestHelper.takeSingleItem( player, DRAKES_FLESH, -1 )
                await QuestHelper.giveAdena( player, adenaAmount, false )

                break

            case '32563-11.html':
                let hornsAmount: number = QuestHelper.getQuestItemsCount( player, VARIANT_DRAKE_WING_HORNS )
                if ( hornsAmount > 0 ) {
                    let adenaAmount = hornsAmount * 200000
                    await QuestHelper.takeSingleItem( player, VARIANT_DRAKE_WING_HORNS, -1 )
                    await QuestHelper.giveAdena( player, adenaAmount, false )

                    return this.getPath( '32563-12.html' )
                }

                return this.getPath( '32563-11.html' )

            case '32563-14.html':
                await state.exitQuest( true, true )
                break

            case '32557-03.html':
                let hasBlood = QuestHelper.hasQuestItem( player, ROTTEN_BLOOD )
                let stoneAmount: number = QuestHelper.getQuestItemsCount( player, EXTRACTED_RED_STAR_STONE )
                if ( hasBlood ) {
                    if ( stoneAmount < 100 ) {
                        return this.getPath( '32557-04.html' )
                    }

                    await QuestHelper.takeSingleItem( player, ROTTEN_BLOOD, 1 )
                    await QuestHelper.takeSingleItem( player, EXTRACTED_RED_STAR_STONE, 100 )
                    await QuestHelper.rewardSingleItem( player, BAIT_FOR_DRAKES, 1 )

                    return this.getPath( '32557-06.html' )
                }

                if ( stoneAmount < 100 ) {
                    return this.getPath( '32557-03.html' )
                }

                return this.getPath( '32557-05.html' )

            case '32555-03.html':
                state.setConditionWithSound( 2, true )
                break

            case '32555-05.html':
                await state.exitQuest( true, true )
                break

            case '32555-06.html':
                if ( QuestHelper.getQuestItemsCount( player, DRAKES_FLESH ) < 100 ) {
                    return this.getPath( '32555-06.html' )
                }

                return this.getPath( '32555-07.html' )

            case '32555-08.html':
                await QuestHelper.takeSingleItem( player, DRAKES_FLESH, 100 )
                await QuestHelper.giveSingleItem( player, ROTTEN_BLOOD, 1 )

                break

            case '32555-10.html':
                if ( QuestHelper.hasQuestItem( player, VARIANT_DRAKE_WING_HORNS ) ) {
                    return this.getPath( '32555-11.html' )
                }

                return this.getPath( '32555-10.html' )

            case '32555-15.html':
                return this.rewardAndRespond( player )

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async rewardAndRespond( player: L2PcInstance ): Promise<string> {
        await QuestHelper.takeSingleItem( player, VARIANT_DRAKE_WING_HORNS, 1 )

        const chanceOne = _.random( 1000 )
        const chanceTwo = _.random( 1000 )

        if ( chanceOne >= 500 ) {
            await QuestHelper.giveAdena( player, _.random( 49917 ) + 125000, false )

            if ( chanceTwo >= 600 ) {
                if ( chanceTwo < 720 ) {
                    await QuestHelper.rewardSingleItem( player, 9628, _.random( 2 ) + 1 )
                    await QuestHelper.rewardSingleItem( player, 9629, _.random( 2 ) + 1 )
                } else if ( chanceTwo < 840 ) {
                    await QuestHelper.rewardSingleItem( player, 9629, _.random( 2 ) + 1 )
                    await QuestHelper.rewardSingleItem( player, 9630, _.random( 2 ) + 1 )
                } else if ( chanceTwo < 960 ) {
                    await QuestHelper.rewardSingleItem( player, 9628, _.random( 2 ) + 1 )
                    await QuestHelper.rewardSingleItem( player, 9630, _.random( 2 ) + 1 )
                } else {
                    await QuestHelper.rewardSingleItem( player, 9628, _.random( 2 ) + 1 )
                    await QuestHelper.rewardSingleItem( player, 9629, _.random( 2 ) + 1 )
                    await QuestHelper.rewardSingleItem( player, 9630, _.random( 2 ) + 1 )
                }

                return this.getPath( '32555-15.html' )
            }

            if ( chanceTwo < 210 ) {
                // nothing to give
            } else if ( chanceTwo < 340 ) {
                await QuestHelper.rewardSingleItem( player, 9628, _.random( 2 ) + 1 )
            } else if ( chanceTwo < 470 ) {
                await QuestHelper.rewardSingleItem( player, 9629, _.random( 2 ) + 1 )
            } else {
                await QuestHelper.rewardSingleItem( player, 9630, _.random( 2 ) + 1 )
            }

            return this.getPath( '32555-16.html' )
        }

        await QuestHelper.giveAdena( player, _.random( 49917 ) + 25000, false )

        if ( chanceTwo >= 600 ) {
            if ( chanceTwo < 720 ) {
                await QuestHelper.rewardSingleItem( player, 9628, _.random( 2 ) + 1 )
                await QuestHelper.rewardSingleItem( player, 9629, _.random( 2 ) + 1 )
            } else if ( chanceTwo < 840 ) {
                await QuestHelper.rewardSingleItem( player, 9629, _.random( 2 ) + 1 )
                await QuestHelper.rewardSingleItem( player, 9630, _.random( 2 ) + 1 )
            } else if ( chanceTwo < 960 ) {
                await QuestHelper.rewardSingleItem( player, 9628, _.random( 2 ) + 1 )
                await QuestHelper.rewardSingleItem( player, 9630, _.random( 2 ) + 1 )
            } else if ( chanceTwo < 1000 ) {
                await QuestHelper.rewardSingleItem( player, 9628, _.random( 2 ) + 1 )
                await QuestHelper.rewardSingleItem( player, 9629, _.random( 2 ) + 1 )
                await QuestHelper.rewardSingleItem( player, 9630, _.random( 2 ) + 1 )
            }

            return this.getPath( '32555-17.html' )
        }

        if ( chanceTwo < 210 ) {
            // nothing to give
        } else if ( chanceTwo < 340 ) {
            await QuestHelper.rewardSingleItem( player, 9628, _.random( 2 ) + 1 )
        } else if ( chanceTwo < 470 ) {
            await QuestHelper.rewardSingleItem( player, 9629, _.random( 2 ) + 1 )
        } else if ( chanceTwo < 600 ) {
            await QuestHelper.rewardSingleItem( player, 9630, _.random( 2 ) + 1 )
        }

        return this.getPath( '32555-18.html' )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                let canProceed : boolean = player.hasQuestCompleted( 'Q10273_GoodDayToFly' ) && player.getLevel() >= minimumLevel
                return this.getPath( canProceed ? '32563-01.htm' : '32563-02.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case PLENOS:
                        return this.getPath( state.isCondition( 1 ) ? '32563-05.html' : '32563-06.html' )

                    case LEKON:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '32557-01.html' )

                            case 2:
                                return this.getPath( '32557-02.html' )
                        }

                        break

                    case TENIUS:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '32555-01.html' )

                            case 2:
                                return this.getPath( '32555-04.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player : L2PcInstance = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 2 )
        if ( !player ) {
            return
        }

        switch ( data.npcId ) {
            case 25632:
                if ( Math.random() > QuestHelper.getAdjustedChance( DRAKES_FLESH, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
                    return
                }

                await QuestHelper.rewardSingleQuestItem( player, DRAKES_FLESH, 1, data.isChampion )
                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

                return

            case 25626:
                await QuestHelper.rewardSingleQuestItem( player, VARIANT_DRAKE_WING_HORNS, this.getModifiedAmount( data.isChampion ), data.isChampion )
                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

                return
        }

        let amount : number = Math.random() < QuestHelper.getAdjustedChance( DRAKES_FLESH, monsterRewardChances[ data.npcId ], data.isChampion ) ? 2 : 1
        await QuestHelper.rewardSingleQuestItem( player, DRAKES_FLESH, amount, data.isChampion )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    getModifiedAmount( isChampion : boolean ) : number {
        let chance = QuestHelper.getAdjustedChance( VARIANT_DRAKE_WING_HORNS, Math.random(), isChampion )
        if ( chance < 708 ) {
            return _.random( 1 ) + 1
        }

        if ( chance < 978 ) {
            return _.random( 2 ) + 3
        }

        if ( chance < 994 ) {
            return _.random( 3 ) + 6
        }

        if ( chance < 998 ) {
            return _.random( 3 ) + 10
        }

        return _.random( 4 ) + 14
    }
}