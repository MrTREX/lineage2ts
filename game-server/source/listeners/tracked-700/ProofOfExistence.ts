import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const ARTIUS = 32559
const DEADMANS_REMAINS = 13875
const BANSHEE_QUEENS_EYE = 13876
const ENIRA = 25625
const minimumLevel = 78

const monsterRewardChances = {
    22606: 0.518, // Floating Skull
    22607: 0.858, // Floating Skull
    22608: 0.482, // Floating Zombie
    22609: 0.466, // Floating Zombie
    25629: 0.735, // Floating Skull (Enira's Evil Spirit)
    25630: 0.391, // Floating Zombie (Enira's Evil Spirit)
    [ ENIRA ]: 1,
}

export class ProofOfExistence extends ListenerLogic {
    constructor() {
        super( 'Q00701_ProofOfExistence', 'listeners/tracked-700/ProofOfExistence.ts' )
        this.questId = 701
        this.questItemIds = [
            DEADMANS_REMAINS,
            BANSHEE_QUEENS_EYE,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ ARTIUS ]
    }

    getTalkIds(): Array<number> {
        return [ ARTIUS ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00701_ProofOfExistence'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }


        switch ( data.eventName ) {
            case '32559-03.htm':
            case '32559-08.html':
                break

            case '32559-04.htm':
                state.startQuest()
                break

            case '32559-09.html':
                await state.exitQuest( true, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( Math.random() > QuestHelper.getAdjustedChance( DEADMANS_REMAINS, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        let player: L2PcInstance = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 1 )
        if ( !player ) {
            return
        }

        if ( data.npcId === ENIRA ) {
            await QuestHelper.rewardSingleQuestItem( player, BANSHEE_QUEENS_EYE, this.getEniraAmount(), data.isChampion )
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

            return
        }

        await QuestHelper.rewardSingleQuestItem( player, DEADMANS_REMAINS, 1, data.isChampion )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    getEniraAmount(): number {
        let chance = Math.random()
        if ( chance < 0.708 ) {
            return _.random( 1 ) + 1
        }

        if ( chance < 0.978 ) {
            return _.random( 2 ) + 3
        }

        if ( chance < 0.994 ) {
            return _.random( 3 ) + 6
        }

        if ( chance < 0.998 ) {
            return _.random( 3 ) + 10
        }

        return _.random( 4 ) + 14
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                let canProceed: boolean = player.getLevel() >= minimumLevel && player.hasQuestCompleted( 'Q10273_GoodDayToFly' )
                return this.getPath( canProceed ? '32559-01.htm' : '32559-02.htm' )

            case QuestStateValues.STARTED:
                let remainsAmount: number = QuestHelper.getQuestItemsCount( player, DEADMANS_REMAINS )
                let eyeAmount: number = QuestHelper.getQuestItemsCount( player, BANSHEE_QUEENS_EYE )

                if ( eyeAmount > 0 ) {
                    let adenaAmount = remainsAmount * 2500 + eyeAmount * 50000 + 23835
                    await QuestHelper.takeMultipleItems( player, -1, BANSHEE_QUEENS_EYE, DEADMANS_REMAINS )
                    await QuestHelper.giveAdena( player, adenaAmount, true )

                    return this.getPath( '32559-07.html' )
                }

                if ( QuestHelper.hasQuestItem( player, DEADMANS_REMAINS ) ) {
                    let adenaAmount = remainsAmount * 2500
                    await QuestHelper.takeSingleItem( player, DEADMANS_REMAINS, -1 )
                    await QuestHelper.giveAdena( player, adenaAmount, true )

                    return this.getPath( '32559-06.html' )
                }

                return this.getPath( '32559-05.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}