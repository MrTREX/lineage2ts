import { PersistedEventConfiguration, PersistedEventLogic } from '../../gameService/models/listener/PersistedEventLogic'
import { SkillHolder } from '../../gameService/models/holders/SkillHolder'
import { NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { PacketDispatcher } from '../../gameService/PacketDispatcher'
import { DataManager } from '../../data/manager'
import _ from 'lodash'

const npcIds: Array<number> = [
    32477,
]

const GATEKEEPER_TRANSFORMATION_STICK = 12814
const TELEPORTER_TRANSFORM = new SkillHolder( 5655 )

interface GatekeeperConfiguration extends PersistedEventConfiguration {
    adenaPrice: number
    reuseIntervalHours: number
}

export class LoveYourGatekeeper extends PersistedEventLogic<GatekeeperConfiguration> {
    constructor() {
        super( 'LoveYourGatekeeper', 'listeners/events/LoveYourGatekeeper.ts' )
    }

    getDefaultConfiguration(): GatekeeperConfiguration {
        return {
            isEnabled: false,
            eventName: 'Love Your Gatekeeper',
            startDate: 0,
            endDate: 0,
            reuseIntervalHours: 20,
            onEndMessage: 'Love Your Gatekeeper: Event has ended.',
            onStartMessage: 'Love Your Gatekeeper: Event ongoing!',
            adenaPrice: 1000,
            npcs: [
                {
                    npc: 32477,
                    x: -80762,
                    y: 151118,
                    z: -3043,
                    heading: 28672,
                },
                {
                    npc: 32477,
                    x: -84046,
                    y: 150193,
                    z: -3129,
                    heading: 4096,
                },
                {
                    npc: 32477,
                    x: -82675,
                    y: 151652,
                    z: -3129,
                    heading: 46943,
                },
                {
                    npc: 32477,
                    x: -12992,
                    y: 122818,
                    z: -3117,
                    heading: 0,
                },
                {
                    npc: 32477,
                    x: -13964,
                    y: 121947,
                    z: -2988,
                    heading: 32768,
                },
                {
                    npc: 32477,
                    x: -14823,
                    y: 123752,
                    z: -3117,
                    heading: 8192,
                },
                {
                    npc: 32477,
                    x: 18178,
                    y: 145149,
                    z: -3054,
                    heading: 7400,
                },
                {
                    npc: 32477,
                    x: 19185,
                    y: 144377,
                    z: -3097,
                    heading: 32768,
                },
                {
                    npc: 32477,
                    x: 19508,
                    y: 145753,
                    z: -3086,
                    heading: 47999,
                },
                {
                    npc: 32477,
                    x: 17396,
                    y: 170259,
                    z: -3507,
                    heading: 30000,
                },
                {
                    npc: 32477,
                    x: 44150,
                    y: -48708,
                    z: -800,
                    heading: 32999,
                },
                {
                    npc: 32477,
                    x: 44280,
                    y: -47664,
                    z: -792,
                    heading: 49167,
                },
                {
                    npc: 32477,
                    x: 79806,
                    y: 55570,
                    z: -1560,
                    heading: 0,
                },
                {
                    npc: 32477,
                    x: 83328,
                    y: 55824,
                    z: -1525,
                    heading: 32768,
                },
                {
                    npc: 32477,
                    x: 80986,
                    y: 54504,
                    z: -1525,
                    heading: 32768,
                },
                {
                    npc: 32477,
                    x: 83358,
                    y: 149223,
                    z: -3400,
                    heading: 32768,
                },
                {
                    npc: 32477,
                    x: 82277,
                    y: 148598,
                    z: -3467,
                    heading: 0,
                },
                {
                    npc: 32477,
                    x: 81621,
                    y: 148725,
                    z: -3467,
                    heading: 32768,
                },
                {
                    npc: 32477,
                    x: 81680,
                    y: 145656,
                    z: -3533,
                    heading: 32768,
                },
                {
                    npc: 32477,
                    x: 117498,
                    y: 76630,
                    z: -2695,
                    heading: 38000,
                },
                {
                    npc: 32477,
                    x: 119536,
                    y: 76988,
                    z: -2275,
                    heading: 40960,
                },
                {
                    npc: 32477,
                    x: 111585,
                    y: 221011,
                    z: -3544,
                    heading: 16384,
                },
                {
                    npc: 32477,
                    x: 107922,
                    y: 218094,
                    z: -3675,
                    heading: 0,
                },
                {
                    npc: 32477,
                    x: 114920,
                    y: 220020,
                    z: -3632,
                    heading: 32768,
                },
                {
                    npc: 32477,
                    x: 147888,
                    y: -58048,
                    z: -2979,
                    heading: 49000,
                },
                {
                    npc: 32477,
                    x: 147285,
                    y: -56461,
                    z: -2776,
                    heading: 11500,
                },
                {
                    npc: 32477,
                    x: 147120,
                    y: 27312,
                    z: -2192,
                    heading: 40960,
                },
                {
                    npc: 32477,
                    x: 147959,
                    y: 25695,
                    z: -2000,
                    heading: 16384,
                },
                {
                    npc: 32477,
                    x: 87792,
                    y: -142240,
                    z: -1343,
                    heading: 44000,
                },
                {
                    npc: 32477,
                    x: 87557,
                    y: -140657,
                    z: -1542,
                    heading: 20476,
                },
                {
                    npc: 32477,
                    x: 115933,
                    y: 76482,
                    z: -2711,
                    heading: 58999,
                },
            ],
        }
    }

    getApproachedForTalkIds(): Array<number> {
        return npcIds
    }

    getPathPrefix(): string {
        return 'overrides/html/events/loveYourGatekeeper'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onApproachedForTalk(): Promise<string> {
        return this.getPath( '32477.htm' )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        switch ( data.eventName ) {
            case 'transform_stick':
                let previousDate = _.defaultTo( PlayerVariablesManager.get( data.playerId, this.name ), 0 ) as number
                let reuseInterval = GeneralHelper.hoursToMillis( this.configuration.reuseIntervalHours )

                let currentDate = Date.now()
                if ( currentDate > previousDate ) {
                    let player = L2World.getPlayer( data.playerId )

                    if ( player.getAdena() >= this.configuration.adenaPrice ) {
                        await player.getInventory().reduceAdena( this.configuration.adenaPrice )
                        await QuestHelper.giveSingleItem( player, GATEKEEPER_TRANSFORMATION_STICK, 1 )
                        PlayerVariablesManager.set( data.playerId, this.name, currentDate + reuseInterval )

                        return
                    }

                    return DataManager.getHtmlData().getItem( this.getPath( '32477-3.htm' ) )
                            .replace( '%amount%', GeneralHelper.getAdenaFormat( this.configuration.adenaPrice ).toString() )
                }

                let [ hours, minutes ] = GeneralHelper.getTimeDifference( previousDate - currentDate )
                let message = new SystemMessageBuilder( SystemMessageIds.AVAILABLE_AFTER_S1_S2_HOURS_S3_MINUTES )
                        .addItemNameWithId( GATEKEEPER_TRANSFORMATION_STICK )
                        .addNumber( hours )
                        .addNumber( minutes )
                        .getBuffer()
                PacketDispatcher.sendOwnedData( data.playerId, message )

                return

            case 'transform':
                let player = L2World.getPlayer( data.playerId )
                if ( !player.isTransformed() ) {
                    await player.doCast( TELEPORTER_TRANSFORM.getSkill() )
                }

                return
        }

        return DataManager.getHtmlData().getItem( this.getPath( data.eventName ) )
                .replace( /%amount%/g, GeneralHelper.getAdenaFormat( this.configuration.adenaPrice ).toString() )
    }
}