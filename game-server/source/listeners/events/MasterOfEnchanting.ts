import { PersistedEventConfiguration, PersistedEventLogic } from '../../gameService/models/listener/PersistedEventLogic'
import { NpcApproachedForTalkEvent, NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { DataManager } from '../../data/manager'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { InventorySlot } from '../../gameService/values/InventoryValues'
import _ from 'lodash'

const npcIds: Array<number> = [
    32599,
]

const MASTER_YOGI_STAFF = 13539
const MASTER_YOGI_SCROLL = 13540

const shadowRewards: Array<number> = [
    13074,
    13075,
    13076,
]

const eventRewards: Array<number> = [
    13518,
    13519,
    13522,
]

const crystalRewards: Array<number> = [
    9570,
    9571,
    9572,
]

interface EnchantingConfiguration extends PersistedEventConfiguration {
    staffPrice: number
    scrollsTotalPrice: number
    scrollsReuseHours: number
    scrollOnePrice: number
    scrollTenPrice: number
    scrollLimit: number
}

export class MasterOfEnchanting extends PersistedEventLogic<EnchantingConfiguration> {
    constructor() {
        super( 'MasterOfEnchanting', 'listeners/events/MasterOfEnchanting.ts' )
    }

    getDefaultConfiguration(): EnchantingConfiguration {
        return {
            isEnabled: false,
            eventName: 'Master of Enchanting',
            startDate: 0,
            endDate: 0,
            staffPrice: 1000000,
            scrollsTotalPrice: 5000000,
            scrollsReuseHours: 6,
            scrollOnePrice: 500000,
            scrollTenPrice: 5000000,
            scrollLimit: 24,
            drops: [
                {
                    itemId: 13540,
                    minCount: 1,
                    maxCount: 1,
                    chance: 1,
                },
            ],
            npcs: [
                {
                    npc: 32599,
                    x: 16111,
                    y: 142850,
                    z: -2707,
                    heading: 16000,
                },
                {
                    npc: 32599,
                    x: 17275,
                    y: 145000,
                    z: -3037,
                    heading: 25000,
                },
                {
                    npc: 32599,
                    x: 83037,
                    y: 149324,
                    z: -3470,
                    heading: 44000,
                },
                {
                    npc: 32599,
                    x: 82145,
                    y: 148609,
                    z: -3468,
                    heading: 0,
                },
                {
                    npc: 32599,
                    x: 81755,
                    y: 146487,
                    z: -3534,
                    heading: 32768,
                },
                {
                    npc: 32599,
                    x: -81031,
                    y: 150038,
                    z: -3045,
                    heading: 0,
                },
                {
                    npc: 32599,
                    x: -83156,
                    y: 150994,
                    z: -3130,
                    heading: 0,
                },
                {
                    npc: 32599,
                    x: -13727,
                    y: 122117,
                    z: -2990,
                    heading: 16384,
                },
                {
                    npc: 32599,
                    x: -14129,
                    y: 123869,
                    z: -3118,
                    heading: 40959,
                },
                {
                    npc: 32599,
                    x: -84411,
                    y: 244813,
                    z: -3730,
                    heading: 57343,
                },
                {
                    npc: 32599,
                    x: -84023,
                    y: 243051,
                    z: -3730,
                    heading: 4096,
                },
                {
                    npc: 32599,
                    x: 46908,
                    y: 50856,
                    z: -2997,
                    heading: 8192,
                },
                {
                    npc: 32599,
                    x: 45538,
                    y: 48357,
                    z: -3061,
                    heading: 18000,
                },
                {
                    npc: 32599,
                    x: 9929,
                    y: 16324,
                    z: -4576,
                    heading: 62999,
                },
                {
                    npc: 32599,
                    x: 11546,
                    y: 17599,
                    z: -4586,
                    heading: 46900,
                },
                {
                    npc: 32599,
                    x: 81987,
                    y: 53723,
                    z: -1497,
                    heading: 0,
                },
                {
                    npc: 32599,
                    x: 81083,
                    y: 56118,
                    z: -1562,
                    heading: 32768,
                },
                {
                    npc: 32599,
                    x: 147200,
                    y: 25614,
                    z: -2014,
                    heading: 16384,
                },
                {
                    npc: 32599,
                    x: 148557,
                    y: 26806,
                    z: -2206,
                    heading: 32768,
                },
                {
                    npc: 32599,
                    x: 117356,
                    y: 76708,
                    z: -2695,
                    heading: 49151,
                },
                {
                    npc: 32599,
                    x: 115887,
                    y: 76382,
                    z: -2714,
                    heading: 0,
                },
                {
                    npc: 32599,
                    x: -117239,
                    y: 46842,
                    z: 367,
                    heading: 49151,
                },
                {
                    npc: 32599,
                    x: -119494,
                    y: 44882,
                    z: 367,
                    heading: 24576,
                },
                {
                    npc: 32599,
                    x: 111004,
                    y: 218928,
                    z: -3544,
                    heading: 16384,
                },
                {
                    npc: 32599,
                    x: 108426,
                    y: 221876,
                    z: -3600,
                    heading: 49151,
                },
                {
                    npc: 32599,
                    x: -45278,
                    y: -112766,
                    z: -241,
                    heading: 0,
                },
                {
                    npc: 32599,
                    x: -45372,
                    y: -114104,
                    z: -241,
                    heading: 16384,
                },
                {
                    npc: 32599,
                    x: 115096,
                    y: -178370,
                    z: -891,
                    heading: 0,
                },
                {
                    npc: 32599,
                    x: 116199,
                    y: -182694,
                    z: -1506,
                    heading: 0,
                },
                {
                    npc: 32599,
                    x: 86865,
                    y: -142915,
                    z: -1341,
                    heading: 26000,
                },
                {
                    npc: 32599,
                    x: 85584,
                    y: -142490,
                    z: -1343,
                    heading: 0,
                },
                {
                    npc: 32599,
                    x: 147421,
                    y: -55435,
                    z: -2736,
                    heading: 49151,
                },
                {
                    npc: 32599,
                    x: 148206,
                    y: -55786,
                    z: -2782,
                    heading: 61439,
                },
                {
                    npc: 32599,
                    x: 43165,
                    y: -48461,
                    z: -797,
                    heading: 17000,
                },
                {
                    npc: 32599,
                    x: 43966,
                    y: -47709,
                    z: -798,
                    heading: 49999,
                },
            ],
            onStartMessage: 'Master of Enchanting: Event ongoing! Visit Master Yogi!',
            onEndMessage: 'Master of Enchanting: Event has ended.',
        }
    }

    getApproachedForTalkIds(): Array<number> {
        return npcIds
    }

    getPathPrefix(): string {
        return 'overrides/html/events/masterOfEnchanting'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onApproachedForTalk( data: NpcApproachedForTalkEvent ): Promise<string> {
        return this.getPath( `${ data.characterNpcId }.htm` )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'buy_staff':
                if ( !QuestHelper.hasQuestItems( player, MASTER_YOGI_STAFF ) && player.getAdena() >= this.configuration.staffPrice ) {
                    await player.reduceAdena( this.configuration.staffPrice, true, this.name )
                    await QuestHelper.giveSingleItem( player, MASTER_YOGI_STAFF, 1 )

                    return this.getPath( '32599-staffbuyed.htm' )
                }

                return this.getPath( '32599-staffcant.htm' )

            case 'buy_scroll_24':
                if ( this.configuration.startDate > 0 && player.getCreateDate() > this.configuration.startDate ) {
                    return this.getPath( '32599-bidth.htm' )
                }

                let lastUsedDate: number = _.defaultTo( PlayerVariablesManager.get( data.playerId, this.name ), 0 ) as number
                let currentDate = Date.now()

                let timeDifference = currentDate - lastUsedDate
                if ( timeDifference > 0 ) {
                    if ( player.getAdena() >= this.configuration.scrollsTotalPrice ) {
                        await player.reduceAdena( this.configuration.scrollsTotalPrice, null, this.name )
                        await QuestHelper.giveSingleItem( player, MASTER_YOGI_SCROLL, this.configuration.scrollLimit )

                        PlayerVariablesManager.set( data.playerId, this.name, currentDate + GeneralHelper.hoursToMillis( this.configuration.scrollsReuseHours ) )

                        let message = new SystemMessageBuilder( SystemMessageIds.ITEM_PURCHASABLE_IN_S1_HOURS_S2_MINUTES )
                                .addNumber( this.configuration.scrollsReuseHours )
                                .addNumber( 0 )
                                .getBuffer()
                        player.sendOwnedData( message )

                        return DataManager.getHtmlData().getItem( this.getPath( '32599-scroll24.htm' ) )
                                .replace( '%scrollLimit%', this.configuration.scrollLimit.toString() )
                                .replace( '%hours%', this.configuration.scrollsReuseHours.toString() )
                    }

                    return this.getPath( '32599-s24-no.htm' )
                }

                let [ hours, minutes ] = GeneralHelper.getTimeDifference( timeDifference )
                if ( hours > 0 ) {
                    let message = new SystemMessageBuilder( SystemMessageIds.ITEM_PURCHASABLE_IN_S1_HOURS_S2_MINUTES )
                            .addNumber( hours )
                            .addNumber( minutes )
                            .getBuffer()
                    player.sendOwnedData( message )

                } else {
                    let adjustedMinutes = minutes > 0 ? minutes : 1
                    let message = new SystemMessageBuilder( SystemMessageIds.ITEM_PURCHASABLE_IN_S1_MINUTES )
                            .addNumber( adjustedMinutes )
                            .getBuffer()
                    player.sendOwnedData( message )
                }

                return DataManager.getHtmlData().getItem( this.getPath( '32599-scroll24.htm' ) )
                        .replace( '%scrollLimit%', this.configuration.scrollLimit.toString() )
                        .replace( '%hours%', this.configuration.scrollsReuseHours.toString() )

            case 'buy_scroll_1':
                if ( player.getAdena() > this.configuration.scrollOnePrice ) {
                    await player.reduceAdena( this.configuration.scrollOnePrice, true, this.name )
                    await QuestHelper.giveSingleItem( player, MASTER_YOGI_SCROLL, 1 )

                    return this.getPath( '32599-scroll-ok.htm' )
                }

                return this.getPath( '32599-s1-no.htm' )

            case 'buy_scroll_10':
                if ( player.getAdena() > this.configuration.scrollTenPrice ) {
                    await player.reduceAdena( this.configuration.scrollTenPrice, true, this.name )
                    await QuestHelper.giveSingleItem( player, MASTER_YOGI_SCROLL, 10 )

                    return this.getPath( '32599-scroll-ok.htm' )
                }

                return this.getPath( '32599-s10-no.htm' )

            case 'receive_reward':
                if ( QuestHelper.getItemIdEquipped( player, InventorySlot.RightHand ) === MASTER_YOGI_STAFF ) {
                    let level = QuestHelper.getEnchantLevel( player, MASTER_YOGI_STAFF )

                    if ( level < 3 ) {
                        return this.getPath( '32599-rewardnostaff.htm' )
                    }

                    switch ( level ) {
                        case 4:
                            await QuestHelper.giveSingleItem( player, 6406, 1 ) // Firework
                            break

                        case 5:
                            await QuestHelper.giveSingleItem( player, 6406, 2 ) // Firework
                            await QuestHelper.giveSingleItem( player, 6407, 1 ) // Large Firework
                            break

                        case 6:
                            await QuestHelper.giveSingleItem( player, 6406, 3 ) // Firework
                            await QuestHelper.giveSingleItem( player, 6407, 2 ) // Large Firework
                            break

                        case 7:
                            await QuestHelper.giveSingleItem( player, _.sample( shadowRewards ), 1 )
                            break

                        case 8:
                            await QuestHelper.giveSingleItem( player, 955, 1 ) // Scroll: Enchant Weapon (D)
                            break

                        case 9:
                            await QuestHelper.giveSingleItem( player, 955, 1 ) // Scroll: Enchant Weapon (D)
                            await QuestHelper.giveSingleItem( player, 956, 1 ) // Scroll: Enchant Armor (D)
                            break

                        case 10:
                            await QuestHelper.giveSingleItem( player, 951, 1 ) // Scroll: Enchant Weapon (C)
                            break

                        case 11:
                            await QuestHelper.giveSingleItem( player, 951, 1 ) // Scroll: Enchant Weapon (C)
                            await QuestHelper.giveSingleItem( player, 952, 1 ) // Scroll: Enchant Armor (C)
                            break

                        case 12:
                            await QuestHelper.giveSingleItem( player, 948, 1 ) // Scroll: Enchant Armor (B)
                            break

                        case 13:
                            await QuestHelper.giveSingleItem( player, 729, 1 ) // Scroll: Enchant Weapon (A)
                            break

                        case 14:
                            await QuestHelper.giveSingleItem( player, _.sample( eventRewards ), 1 )
                            break

                        case 15:
                            await QuestHelper.giveSingleItem( player, 13992, 1 ) // Grade S Accessory Chest (Event)
                            break

                        case 16:
                            await QuestHelper.giveSingleItem( player, 8762, 1 ) // Top-Grade Life Stone: level 76
                            break

                        case 17:
                            await QuestHelper.giveSingleItem( player, 959, 1 ) // Scroll: Enchant Weapon (S)
                            break

                        case 18:
                            await QuestHelper.giveSingleItem( player, 13991, 1 ) // Grade S Armor Chest (Event)
                            break

                        case 19:
                            await QuestHelper.giveSingleItem( player, 13990, 1 ) // Grade S Weapon Chest (Event)
                            break

                        case 20:
                            await QuestHelper.giveSingleItem( player, _.sample( crystalRewards ), 1 ) // Red/Blue/Green Soul Crystal - Stage 14
                            break

                        case 21:
                            await QuestHelper.giveSingleItem( player, 8762, 1 ) // Top-Grade Life Stone: level 76
                            await QuestHelper.giveSingleItem( player, 8752, 1 ) // High-Grade Life Stone: level 76
                            await QuestHelper.giveSingleItem( player, _.sample( crystalRewards ), 1 ) // Red/Blue/Green Soul Crystal - Stage 14
                            break

                        case 22:
                            await QuestHelper.giveSingleItem( player, 13989, 1 ) // S80 Grade Armor Chest (Event)
                            break

                        case 23:
                            await QuestHelper.giveSingleItem( player, 13988, 1 ) // S80 Grade Weapon Chest (Event)

                        default:
                            if ( level > 23 ) {
                                await QuestHelper.giveSingleItem( player, 13988, 1 ) // S80 Grade Weapon Chest (Event)
                            }

                            break
                    }
                    await QuestHelper.takeSingleItem( player, MASTER_YOGI_STAFF, 1 )
                    return this.getPath( '32599-rewardok.htm' )
                }

                return this.getPath( '32599-rewardnostaff.htm' )

            case '32599-1.htm':
                return DataManager.getHtmlData().getItem( this.getPath( data.eventName ) )
                        .replace( '%staffPrice%', GeneralHelper.getAdenaFormat( this.configuration.staffPrice ) )
                        .replace( '%hours%', this.configuration.scrollsReuseHours.toString() )
                        .replace( '%scrollLimit%', this.configuration.scrollLimit.toString() )
                        .replace( '%scrollTotalPrice%', GeneralHelper.getAdenaFormat( this.configuration.scrollsTotalPrice ) )

            case '32599-participate.htm':
                return DataManager.getHtmlData().getItem( this.getPath( data.eventName ) )
                        .replace( '%staffPrice%', GeneralHelper.getAdenaFormat( this.configuration.staffPrice ) )
                        .replace( '%scrollPrice%', GeneralHelper.getAdenaFormat( this.configuration.scrollOnePrice ) )
                        .replace( '%scrollTenPrice%%', GeneralHelper.getAdenaFormat( this.configuration.scrollTenPrice ) )
                        .replace( '%scrollLimit%', this.configuration.scrollLimit.toString() )
                        .replace( '%scrollTotalPrice%', GeneralHelper.getAdenaFormat( this.configuration.scrollsTotalPrice ) )
        }


        return this.getPath( data.eventName )
    }
}