import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestHelper } from '../helpers/QuestHelper'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import {
    PersistedConfigurationLogic,
    PersistedConfigurationLogicType,
} from '../../gameService/models/listener/PersistedConfigurationLogic'
import { ItemTypes } from '../../gameService/values/InventoryValues'
import { DimensionalTransferManager } from '../../gameService/cache/DimensionalTransferManager'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import aigle from 'aigle'
import _ from 'lodash'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const ALEGRIA = 32600
const npcIds: Array<number> = [
    ALEGRIA,
    30006,
    30059,
    30080,
    30134,
    30146,
    30177,
    30233,
    30256,
    30320,
    30540,
    30576,
    30836,
    30848,
    30878,
    30899,
    31275,
    31320,
    31964,
    32163,
]

const adventurerHat: number = 10250
const birthdayHat: number = 21594
const eventNames = {
    despawn: 'ds',
}

type BirthdayDeliveryMethod = 'inventory' | 'dimensionalMerchant'
interface CharacterBirthdayItem {
    itemId : number
    amount : number
    delivery: BirthdayDeliveryMethod
}

interface CharacterBirthdayConfiguration extends PersistedConfigurationLogicType {
    isEnabled: boolean
    items: Array<CharacterBirthdayItem>
    npcMessage: string
}

export class CharacterBirthday extends PersistedConfigurationLogic<CharacterBirthdayConfiguration> {
    spawnCount: number = 0

    constructor() {
        super( 'CharacterBirthday', 'listeners/events/CharacterBirthday.ts' )
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let npc: L2Npc = L2World.getObjectById( data.characterId ) as L2Npc

        switch ( data.eventName ) {
            case eventNames.despawn:
                await npc.doDie( player )
                this.spawnCount = Math.max( this.spawnCount--, 0 )

                return

            case 'change':
                if ( QuestHelper.hasQuestItems( player, adventurerHat ) ) {
                    await QuestHelper.takeSingleItem( player, adventurerHat, 1 ) // Adventurer Hat (Event)

                    await aigle.resolve( this.configuration.items ).eachSeries( async ( item : CharacterBirthdayItem ) : Promise<void> => {
                        switch ( item.delivery ) {
                            case 'inventory':
                                return QuestHelper.giveSingleItem( player, item.itemId, item.amount )

                            case 'dimensionalMerchant':
                                return DimensionalTransferManager.addItem( data.playerId, item.itemId, item.amount, 'Alegria Birthday Present' )
                        }
                    } )

                    if ( !_.isEmpty( this.configuration.npcMessage ) ) {
                        BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.S1, this.configuration.npcMessage )
                    }

                    await npc.doDie( player )
                    this.spawnCount = Math.max( this.spawnCount--, 0 )

                    return
                }

                return this.getPath( '32600-nohat.htm' )
        }
    }

    getPathPrefix(): string {
        return 'data/datapack/events/CharacterBirthday'
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        if ( !this.configuration.isEnabled || this.spawnCount >= 3 ) {
            return this.getPath( 'busy.htm' )
        }

        let player = L2World.getPlayer( data.playerId )
        let npc: L2Npc = L2World.getObjectById( data.characterId ) as L2Npc
        if ( !GeneralHelper.checkIfInRange( 10, npc, player, true ) ) {
            let npc: L2Npc = QuestHelper.addGenericSpawn(
                    null,
                    ALEGRIA,
                    player.getX() + 10,
                    player.getY() + 10,
                    player.getZ() + 10,
                    0,
                    false,
                    0,
                    true,
                    player.getInstanceId() )

            this.startQuestTimer( eventNames.despawn, 180000, npc.getObjectId(), data.playerId )
            this.spawnCount++

            return
        }

        return this.getPath( 'tooclose.htm' )
    }

    getDefaultConfiguration(): CharacterBirthdayConfiguration {
        return {
            isEnabled: false,
            items: [
                {
                    itemId: birthdayHat,
                    amount: 1,
                    delivery: 'inventory'
                },
                {
                    itemId: ItemTypes.Adena,
                    amount: 1000,
                    delivery: 'dimensionalMerchant'
                }
            ],
            npcMessage: 'There you go!'
        }
    }
}