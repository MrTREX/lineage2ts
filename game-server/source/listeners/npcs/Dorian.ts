import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestHelper } from '../helpers/QuestHelper'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { NpcSeePlayerEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const npcId: number = 25332

const SILVER_CROSS = 7153
const BROKEN_SILVER_CROSS = 7154

export class Dorian extends ListenerLogic {
    constructor() {
        super( 'Dorian', 'listeners/npcs/Dorian.ts' )
    }

    getNpcSeePlayerIds() : Array<number> {
        return [ npcId ]
    }

    async onNpcSeePlayerEvent( data : NpcSeePlayerEvent ): Promise<void> {
        let player : L2PcInstance = L2World.getPlayer( data.playerId )
        let state : QuestState = QuestStateCache.getQuestState( player.getObjectId(),'Q00024_InhabitantsOfTheForestOfTheDead' )
        if ( state && state.isCondition( 3 ) ) {
            await QuestHelper.takeSingleItem( player, SILVER_CROSS, -1 )
            await QuestHelper.giveSingleItem( player, BROKEN_SILVER_CROSS, 1 )

            state.setConditionWithSound( 4, true )

            let npc = L2World.getObjectById( data.npcObjectId ) as L2Npc
            BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.THAT_SIGN )
        }
    }
}