import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { L2VillageMasterInstance } from '../../gameService/models/actor/instance/L2VillageMasterInstance'
import { DataManager } from '../../data/manager'
import { ClassId } from '../../gameService/models/base/ClassId'
import { CategoryType } from '../../gameService/enums/CategoryType'
import { L2ItemInstance } from '../../gameService/models/items/instance/L2ItemInstance'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'
import _ from 'lodash'

const npcIds: Array<number> = [
    30026, 30031, 30037, 30066, 30070, 30109, 30115, 30120, 30154, 30174,
    30175, 30176, 30187, 30191, 30195, 30288, 30289, 30290, 30297, 30358,
    30373, 30462, 30474, 30498, 30499, 30500, 30503, 30504, 30505, 30508,
    30511, 30512, 30513, 30520, 30525, 30565, 30594, 30595, 30676, 30677,
    30681, 30685, 30687, 30689, 30694, 30699, 30704, 30845, 30847, 30849,
    30854, 30857, 30862, 30865, 30894, 30897, 30900, 30905, 30910, 30913,
    31269, 31272, 31276, 31279, 31285, 31288, 31314, 31317, 31321, 31324,
    31326, 31328, 31331, 31334, 31336, 31755, 31958, 31961, 31965, 31968,
    31974, 31977, 31996, 32092, 32093, 32094, 32095, 32096, 32097, 32098,
    32145, 32146, 32147, 32150, 32153, 32154, 32157, 32158, 32160, 32171,
    32193, 32199, 32202, 32213, 32214, 32221, 32222, 32229, 32230, 32233,
    32234,
]

const CERTIFICATE_EMERGENT_ABILITY = 10280
const CERTIFICATE_MASTER_ABILITY = 10612
const minimumLevel = 65

const abilityCertificates: Array<number> = [
    10281, // Certificate - Warrior Ability
    10283, // Certificate - Rogue Ability
    10282, // Certificate - Knight Ability
    10286, // Certificate - Summoner Ability
    10284, // Certificate - Wizard Ability
    10285, // Certificate - Healer Ability
    10287, // Certificate - Enchanter Ability
]

const transformationSealBooks: Array<number> = [
    10289, // Transformation Sealbook: Divine Warrior
    10290, // Transformation Sealbook: Divine Rogue
    10288, // Transformation Sealbook: Divine Knight
    10294, // Transformation Sealbook: Divine Summoner
    10292, // Transformation Sealbook: Divine Wizard
    10291, // Transformation Sealbook: Divine Healer
    10293, // Transformation Sealbook: Divine Enchanter
]

export class SubclassCertification extends ListenerLogic {
    constructor() {
        super( 'SubclassCertification', 'listeners/npcs/SubclassCertification.ts' )
    }

    async doCertification( player: L2PcInstance, state: QuestState, abilityName: string, itemId: number, level: number ): Promise<string> {
        if ( !itemId ) {
            return
        }

        let variableName = `${this.name}:${ abilityName }:${ level }-${ player.getClassIndex() }`
        let variableValue = PlayerVariablesManager.get( player.getObjectId(), variableName )

        if ( variableValue ) {
            return this.getPath( 'AlreadyReceived.html' )
        }

        if ( player.getLevel() < level ) {
            return this.getFormattedHtml( player, 'LowLevel.html', false, level.toString() )
        }

        let item: L2ItemInstance = await player.getInventory().addItem( itemId, 1, 0, 'Quest:SubclassCertification' )
        if ( !item ) {
            return
        }

        let message = new SystemMessageBuilder( SystemMessageIds.EARNED_ITEM_S1 )
                .addItemInstanceName( item )
                .getBuffer()
        player.sendOwnedData( message )

        PlayerVariablesManager.set( player.getObjectId(), variableName, item.getObjectId() )
        return this.getPath( 'GetAbility.html' )
    }

    getClassIndex( player: L2PcInstance ): number {
        if ( player.isInCategory( CategoryType.SUB_GROUP_WARRIOR ) ) {
            return 0
        }

        if ( player.isInCategory( CategoryType.SUB_GROUP_ROGUE ) ) {
            return 1
        }

        if ( player.isInCategory( CategoryType.SUB_GROUP_KNIGHT ) ) {
            return 2
        }

        if ( player.isInCategory( CategoryType.SUB_GROUP_SUMMONER ) ) {
            return 3
        }

        if ( player.isInCategory( CategoryType.SUB_GROUP_WIZARD ) ) {
            return 4
        }

        if ( player.isInCategory( CategoryType.SUB_GROUP_HEALER ) ) {
            return 5
        }

        if ( player.isInCategory( CategoryType.SUB_GROUP_ENCHANTER ) ) {
            return 6
        }

        return
    }

    getFormattedHtml( player: L2PcInstance, path: string, replaceClassValue: boolean, levelValue: string ) {
        let html: string = DataManager.getHtmlData().getItem( this.getPath( path ) )
        if ( replaceClassValue ) {
            html = html.replace( /%class%/g, ClassId.getClientCode( player.getActiveClass() ).toString() )
        }

        if ( levelValue ) {
            html = html.replace( /%level%/g, levelValue )
        }

        return html
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/SubclassCertification'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onNpcEvent( data : NpcGeneralEvent ): Promise<string> {

        let event = data.eventName
        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        let player = L2World.getPlayer( data.playerId )

        let state: QuestState = this.getQuestState( player.getObjectId(), false )
        if ( !state ) {
            return
        }

        switch ( event ) {
            case 'GetCertified':
                if ( !player.isSubClassActive() ) {
                    return this.getPath( 'NotSubclass.html' )
                }

                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( 'NotMinLevel.html' )
                }

                if ( ( npc as L2VillageMasterInstance ).checkVillageMaster( player.getActiveClass() ) ) {
                    return this.getPath( 'CertificationList.html' )
                }

                return this.getPath( 'WrongVillageMaster.html' )

            case 'Obtain65':
                return this.getFormattedHtml( player, 'EmergentAbility.html', true, null )
                        .replace( /%level%/g, '65' )
                        .replace( /%skilltype%/g, 'common skill' )
                        .replace( /%event%/g, 'lvl65Emergent' )


            case 'Obtain70':
                return this.getFormattedHtml( player, 'EmergentAbility.html', true, null )
                        .replace( /%level%/g, '70' )
                        .replace( /%skilltype%/g, 'common skill' )
                        .replace( /%event%/g, 'lvl70Emergent' )

            case 'Obtain75':
                return this.getFormattedHtml( player, 'ClassAbility.html', true, null )

            case 'Obtain80':
                return this.getFormattedHtml( player, 'EmergentAbility.html', true, null )
                        .replace( /%level%/g, '80' )
                        .replace( /%skilltype%/g, 'transformation skill' )
                        .replace( /%event%/g, 'lvl80Class' )

            case 'lvl65Emergent':
                return this.doCertification( player, state, 'EmergentAbility', CERTIFICATE_EMERGENT_ABILITY, 65 )

            case 'lvl70Emergent':
                return this.doCertification( player, state, 'EmergentAbility', CERTIFICATE_EMERGENT_ABILITY, 70 )

            case 'lvl75Master':
                return this.doCertification( player, state, 'ClassAbility', CERTIFICATE_MASTER_ABILITY, 75 )

            case 'lvl75Class':
                return this.doCertification( player, state, 'ClassAbility', _.nth( abilityCertificates, this.getClassIndex( player ) ), 75 )

            case 'lvl80Class':
                return this.doCertification( player, state, 'ClassAbility', _.nth( transformationSealBooks, this.getClassIndex( player ) ), 80 )

            case 'Main.html':
            case 'Explanation.html':
            case 'NotObtain.html': {
                return this.getPath( event )
            }
        }
    }

    async onTalkEvent( data : NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        if ( state ) {
            state.setState( QuestStateValues.STARTED )
            return this.getPath( 'Main.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}