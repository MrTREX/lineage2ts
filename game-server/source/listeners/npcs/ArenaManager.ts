import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { SkillHolder } from '../../gameService/models/holders/SkillHolder'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { QuestHelper } from '../helpers/QuestHelper'
import { NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { ItemTypes } from '../../gameService/values/InventoryValues'
import aigle from 'aigle'
import { AreaType } from '../../gameService/models/areas/AreaType'

const npcIds: Array<number> = [
    31226, // Arena Director (MDT)
    31225, // Arena Manager (Coliseum)
]

const buffs: Array<SkillHolder> = [
    new SkillHolder( 6805 ), // Arena Empower
    new SkillHolder( 6806 ), // Arena Acumen
    new SkillHolder( 6807 ), // Arena Concentration
    new SkillHolder( 6808 ), // Arena Might
    new SkillHolder( 6804 ), // Arena Wind Walk
    new SkillHolder( 6812 ), // Arena Berserker Spirit
]

const cpRecoveryBuff = new SkillHolder( 4380 ) // Arena: CP Recovery
const hpRecoveryBuff = new SkillHolder( 6817 ) // Arena HP Recovery

const CP_COST = 1000
const HP_COST = 1000
const BUFF_COST = 2000

export class ArenaManager extends ListenerLogic {

    constructor() {
        super( 'ArenaManager', 'listeners/npcs/ArenaManager.ts' )
    }

    getApproachedForTalkIds(): Array<number> {
        return npcIds
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let event = data.eventName
        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.characterId ) as L2Npc

        switch ( event ) {
            case 'CPrecovery':
                if ( player.getAdena() >= CP_COST ) {
                    await QuestHelper.takeSingleItem( player, ItemTypes.Adena, CP_COST )
                    this.startQuestTimer( 'CPrecovery_delay', 2000, data.characterId, data.playerId )
                }

                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ADENA ) )
                return

            case 'CPrecovery_delay':
                if ( player && !player.isInArea( AreaType.PVP ) ) {
                    npc.setTarget( player )
                    await npc.doCast( cpRecoveryBuff.getSkill() )
                }
                return

            case 'HPrecovery':
                if ( player.getAdena() >= HP_COST ) {
                    await QuestHelper.takeSingleItem( player, ItemTypes.Adena, HP_COST )
                    this.startQuestTimer( 'HPrecovery_delay', 2000, data.characterId, data.playerId )
                }

                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ADENA ) )
                return

            case 'HPrecovery_delay':
                if ( player && !player.isInArea( AreaType.PVP ) ) {
                    npc.setTarget( player )
                    await npc.doCast( hpRecoveryBuff.getSkill() )
                }

                return

            case 'Buff':
                if ( player.getAdena() >= BUFF_COST ) {
                    await QuestHelper.takeSingleItem( player, ItemTypes.Adena, BUFF_COST )
                    npc.setTarget( player )

                    await aigle.resolve( buffs ).each( ( skill: SkillHolder ): Promise<void> => {
                        return npc.doCast( skill.getSkill() )
                    } )
                }

                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ADENA ) )
                return
        }
    }
}