import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { L2MerchantInstance } from '../../gameService/models/actor/instance/L2MerchantInstance'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { ConfigManager } from '../../config/ConfigManager'
import { L2SkillLearn } from '../../gameService/models/L2SkillLearn'
import { SkillCache } from '../../gameService/cache/SkillCache'
import { AcquireSkillList } from '../../gameService/packets/send/builder/AcquireSkillList'
import { AcquireSkillType } from '../../gameService/enums/AcquireSkillType'
import { DataManager } from '../../data/manager'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { NpcApproachedForTalkEvent, NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { FishingChampionshipManager } from '../../gameService/cache/FishingChampionshipManager'

const npcIds : Array<number> = [
    31562,
    31563,
    31564,
    31565,
    31566,
    31567,
    31568,
    31569,
    31570,
    31571,
    31572,
    31573,
    31574,
    31575,
    31576,
    31577,
    31578,
    31579,
    31696,
    31697,
    31989,
    32007,
    32348
]

export class Fisherman extends ListenerLogic {
    constructor() {
        super( 'Fisherman', 'listeners/npcs/Fisherman.ts' )
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    getApproachedForTalkIds(): Array<number> {
        return npcIds
    }

    getPathPrefix(): string {
        return 'overrides/html/fisherman'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {

        let event = data.eventName
        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        let player = L2World.getPlayer( data.playerId )

        switch ( event ) {
            case 'LearnFishSkill':
                this.showFishSkillList( player )
                return

            case 'BuySellRefund':
                ( npc as L2MerchantInstance ).showBuyWindow( player, npc.getId() * 100, true )
                return

            case 'LastFishingChampionship':
                if ( ConfigManager.general.allowFishingChampionship() ) {
                    await FishingChampionshipManager.showChampionshipScreen( player, npc )
                    return
                }

                return this.getPath( 'championship/no-results.htm' )

            case 'CurrentFishingChampionship':
                if ( ConfigManager.general.allowFishingChampionship() ) {
                    FishingChampionshipManager.showMidResult( player, data.characterId )
                    return
                }

                return this.getPath( 'championship/no-results.htm' )

            case 'FishingReward':
                if ( ConfigManager.general.allowFishingChampionship() ) {
                    let winningPlace : number = FishingChampionshipManager.getWinningPlace( data.playerId )
                    if ( winningPlace < 1 || winningPlace > ConfigManager.general.getFishingChampionshipParticipantAmount() ) {
                        return this.getPath( 'championship/no-reward.htm' )
                    }

                    let amount = 0

                    switch ( winningPlace ) {
                        case 1:
                            amount = ConfigManager.general.getFishingChampionshipReward1()
                            break

                        case 2:
                            amount = ConfigManager.general.getFishingChampionshipReward2()
                            break

                        case 3:
                            amount = ConfigManager.general.getFishingChampionshipReward3()
                            break

                        case 4:
                            amount = ConfigManager.general.getFishingChampionshipReward4()
                            break

                        case 5:
                            amount = ConfigManager.general.getFishingChampionshipReward5()
                            break

                        default:
                            amount = ConfigManager.general.getFishingChampionshipRewardOthers()
                            break
                    }

                    await player.getInventory().addItem(
                            ConfigManager.general.getFishingChampionshipRewardItemId(),
                            amount,
                            data.characterId,
                            'Fisherman.reward' )

                    FishingChampionshipManager.markClaimedReward( data.playerId )

                    return this.getPath( 'championship/reward.htm' )
                }

                return this.getPath( 'championship/no-results.htm' )
        }
    }

    async onApproachedForTalk( data : NpcApproachedForTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )

        if ( ( player.getKarma() > 0 ) && !ConfigManager.character.karmaPlayerCanShop() ) {
            return this.getPath( `${data.characterNpcId}-pk.htm` )
        }

        return this.getPath( `${data.characterNpcId}.htm` )
    }

    showFishSkillList( player: L2PcInstance ) : void {
        let skills : Array<L2SkillLearn> = SkillCache.getAvailableFishingSkills( player )
        let skillList = new AcquireSkillList( AcquireSkillType.Fishing )

        skills.forEach( ( skill: L2SkillLearn ) => {
            if ( SkillCache.getSkill( skill.getSkillId(), skill.getSkillLevel() ) ) {
                skillList.addSkill( skill.getSkillId(), skill.getSkillLevel(), skill.getLevelUpSp(), 1 )
            }
        } )

        if ( skillList.getCount() > 0 ) {
            player.sendOwnedData( skillList.getBuffer() )
            return
        }

        let level = SkillCache.getMinimumLevelForNewSkill( player, DataManager.getSkillTreesData().getFishingSkillTree() )
        if ( level > 0 ) {

            let message = new SystemMessageBuilder( SystemMessageIds.DO_NOT_HAVE_FURTHER_SKILLS_TO_LEARN_S1 )
                    .addNumber( level )
                    .getBuffer()
            player.sendOwnedData( message )
            return
        }

        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NO_MORE_SKILLS_TO_LEARN ) )
    }
}