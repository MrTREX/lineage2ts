import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { QuestHelper } from '../helpers/QuestHelper'
import { NpcApproachedForTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'

const npcId: number = 31555

const GOLDEN_RAM_BADGE_RECRUIT = 7246
const GOLDEN_RAM_BADGE_SOLDIER = 7247

export class Abercrombie extends ListenerLogic {
    constructor() {
        super( 'Abercrombie', 'listeners/npcs/Abercrombie.ts' )
    }

    getApproachedForTalkIds(): Array<number> {
        return [ npcId ]
    }

    async onApproachedForTalk( data: NpcApproachedForTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )

        if ( QuestHelper.hasQuestItems( player, GOLDEN_RAM_BADGE_SOLDIER ) ) {
            return this.getPath( '31555-07.html' )
        }

        if ( QuestHelper.hasQuestItems( player, GOLDEN_RAM_BADGE_RECRUIT ) ) {
            return this.getPath( '31555-01.html' )
        }

        return this.getPath( '31555-09.html' )
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/Abercrombie'
    }
}