import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { SkillHolder } from '../../gameService/models/holders/SkillHolder'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestHelper } from '../helpers/QuestHelper'
import { NpcApproachedForTalkEvent, NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { SkillCache } from '../../gameService/cache/SkillCache'
import { Skill } from '../../gameService/models/Skill'

const npcId: number = 32349
const STAMP = 10013 // Race Stamp
const KEY = 9694 // Secret Key

const minimumLevel: number = 78
const timerName = 'timeout'

export class Rignos extends ListenerLogic {
    constructor() {
        super( 'Rignos', 'listeners/npcs/Rignos.ts' )
    }

    getApproachedForTalkIds(): Array<number> {
        return [ npcId ]
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/Rignos'
    }

    getQuestStartIds(): Array<number> {
        return [ npcId ]
    }

    getTalkIds(): Array<number> {
        return [ npcId ]
    }

    async onApproachedForTalk( data : NpcApproachedForTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )

        if ( QuestHelper.getQuestItemsCount( player, STAMP ) >= 4 ) {
            return this.getPath( '32349-01.html' )
        }

        let path = ( !NpcVariablesManager.get( data.characterId, this.name ) && ( player.getLevel() >= minimumLevel ) ) ? '32349.html' : '32349-02.html'
        return this.getPath( path )
    }

    async onNpcEvent( data : NpcGeneralEvent ): Promise<string> {

        let event = data.eventName
        let player = L2World.getPlayer( data.playerId )

        switch ( event ) {
            case '32349-03.html':
                return this.getPath( event )

            case 'startRace':
                if ( !NpcVariablesManager.get( data.characterId, this.name ) ) {
                    NpcVariablesManager.set( data.characterId, this.name, true )
                    this.startQuestTimer( timerName, 1800000, data.characterId )

                    let skill : Skill = SkillCache.getSkill( 5239, 5 )
                    await skill.applyEffects( player, player )
                    if ( player.hasSummon() ) {
                        await skill.applyEffects( player.getSummon(), player.getSummon() )
                    }

                    if ( QuestHelper.hasQuestItems( player, STAMP ) ) {
                        await QuestHelper.takeSingleItem( player, STAMP, -1 )
                    }
                }
                break

            case 'exchange':
                if ( QuestHelper.getQuestItemsCount( player, STAMP ) >= 4 ) {
                    await QuestHelper.giveSingleItem( player, KEY, 3 )
                    await QuestHelper.takeSingleItem( player, STAMP, -1 )
                }
                break

            case timerName:
                NpcVariablesManager.set( data.characterId, this.name, false )
                break
        }
    }
}