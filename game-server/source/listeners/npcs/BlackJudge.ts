import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { QuestHelper } from '../helpers/QuestHelper'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { EtcStatusUpdate } from '../../gameService/packets/send/EtcStatusUpdate'
import { NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { ItemTypes } from '../../gameService/values/InventoryValues'

const npcId : number = 30981
const penalty : Array<number> = [
    3600, 8640, 25200, 50400, 86400, 144000
]

export class BlackJudge extends ListenerLogic {
    constructor() {
        super( 'BlackJudge', 'listeners/npcs/BlackJudge.ts' )
    }

    getQuestStartIds(): Array<number> {
        return [ npcId ]
    }

    getTalkIds(): Array<number> {
        return [ npcId ]
    }

    getApproachedForTalkIds(): Array<number> {
        return [ npcId ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let event = data.eventName
        let player = L2World.getPlayer( data.playerId )
        let level = ( player.getExpertiseLevel() < 5 ) ? player.getExpertiseLevel() : 5

        switch ( event ) {
            case 'remove_info':
                return this.getPath( `30981-0${level + 1}.html` )

            case 'remove_dp':
                if ( player.getDeathPenaltyBuffLevel() > 0 ) {
                    let cost = penalty[ level ]

                    if ( player.getAdena() >= cost ) {
                        await QuestHelper.takeSingleItem( player, ItemTypes.Adena, cost )

                        player.setDeathPenaltyBuffLevel( player.getDeathPenaltyBuffLevel() - 1 )

                        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.DEATH_PENALTY_LIFTED ) )
                        player.sendDebouncedPacket( EtcStatusUpdate )
                        return
                    }

                    return this.getPath( '30981-07.html' )
                }

                return this.getPath( '30981-08.html' )
        }
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/BlackJudge'
    }
}