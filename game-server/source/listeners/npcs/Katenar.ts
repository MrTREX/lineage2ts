import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { NpcApproachedForTalkEvent, NpcGeneralEvent, NpcSpawnEvent } from '../../gameService/models/events/EventType'

const npcId: number = 32242
const SEALED_DOCUMENT = 9803

export class Katenar extends ListenerLogic {
    constructor() {
        super( 'Katenar', 'listeners/npcs/Katenar.ts' )
    }

    getQuestStartIds(): Array<number> {
        return [ npcId ]
    }

    getTalkIds(): Array<number> {
        return [ npcId ]
    }

    getApproachedForTalkIds(): Array<number> {
        return [ npcId ]
    }

    getSpawnIds(): Array<number> {
        return [ npcId ]
    }

    /*
        TODO : L2J has some weird things going on storing object instances in variables
        TODO : Use objectId references instead and use current class to track these, not instances
     */
    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        return
    }

    async onApproachedForTalk( data: NpcApproachedForTalkEvent ): Promise<string> {
        return
    }

    async onSpawnEvent( data: NpcSpawnEvent ): Promise<void> {
        return
    }
}