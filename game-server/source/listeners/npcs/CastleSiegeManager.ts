import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { NpcApproachedForTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'

const npcIds : Array<number> = [
    35104, // Gludio Castle
    35146, // Dion Castle
    35188, // Giran Castle
    35232, // Oren Castle
    35278, // Aden Castle
    35320, // Innadril Castle
    35367, // Goddard Castle
    35513, // Rune Castle
    35559, // Schuttgard Castle
    35639, // Fortress of the Dead
    35420, // Devastated Castle
]

export class CastleSiegeManager extends ListenerLogic {
    constructor() {
        super( 'CastleSiegeManager', 'listeners/npcs/CastleSiegeManager.ts' )
    }

    getApproachedForTalkIds(): Array<number> {
        return npcIds
    }

    async onApproachedForTalk( data : NpcApproachedForTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let npc : L2Npc = L2World.getObjectById( data.characterId ) as L2Npc

        if ( player.isClanLeader() && ( player.getClanId() === npc.getCastle().getOwnerId() ) ) {
            if ( this.isInSiege( npc ) ) {
                return this.getPath( 'CastleSiegeManager.html' )
            }

            return this.getPath( 'CastleSiegeManager-01.html' )
        }

        if ( this.isInSiege( npc ) ) {
            return this.getPath( 'CastleSiegeManager-02.html' )
        }

        if ( npc.getConquerableHall() ) {
            npc.getConquerableHall().showSiegeInfo( player )
            return
        }

        npc.getCastle().getSiege().listRegisterClan( player )
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/CastleSiegeManager'
    }

    isInSiege( npc: L2Npc ) : boolean {
        if ( npc.getConquerableHall() && npc.getConquerableHall().isInSiege() ) {
            return true
        }

        if ( npc.getCastle().getSiege().isInProgress() ) {
            return true
        }

        return false
    }
}