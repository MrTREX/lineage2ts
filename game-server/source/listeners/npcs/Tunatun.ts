import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'

const TUNATUN = 31537
const BEAST_HANDLERS_WHIP = 15473
const minimumLevel = 82

export class Tunatun extends ListenerLogic {
    constructor() {
        super( 'Tunatun', 'listeners/npcs/Tunatun.ts' )
    }

    getQuestStartIds(): Array<number> {
        return [ TUNATUN ]
    }

    getApproachedForTalkIds(): Array<number> {
        return [ TUNATUN ]
    }

    getTalkIds(): Array<number> {
        return [ TUNATUN ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        if ( data.eventName === 'Whip' ) {

            let player = L2World.getPlayer( data.playerId )
            if ( QuestHelper.hasQuestItems( player, BEAST_HANDLERS_WHIP ) ) {
                return this.getPath( '31537-01.html' )
            }

            let state : QuestState = QuestStateCache.getQuestState( data.playerId,'Q00020_BringUpWithLove' )
            if ( !state && ( player.getLevel() < minimumLevel ) ) {
                return this.getPath( '31537-02.html' )
            }

            if ( state || ( player.getLevel() >= minimumLevel ) ) {
                await QuestHelper.giveSingleItem( player, BEAST_HANDLERS_WHIP, 1 )
                return this.getPath( '31537-03.html' )
            }
        }

        return this.getPath( data.eventName )
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/Tunatun'
    }
}