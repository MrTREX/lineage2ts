import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Race } from '../../gameService/enums/Race'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { ClassId } from '../../gameService/models/base/ClassId'
import { InstanceType } from '../../gameService/enums/InstanceType'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import _ from 'lodash'

const masters: { [ npcId: number ]: Race } = {
    30026: Race.HUMAN, // Blitz, TI Fighter Guild Head Master
    30031: Race.HUMAN, // Biotin, TI Einhasad Temple High Priest
    30154: Race.ELF, // Asterios, Elven Village Tetrarch
    30358: Race.DARK_ELF, // Thifiell, Dark Elf Village Tetrarch
    30565: Race.ORC, // Kakai, Orc Village Flame Lord
    30520: Race.DWARF, // Reed, Dwarven Village Warehouse Chief
    30525: Race.DWARF, // Bronk, Dwarven Village Head Blacksmith
    // Kamael Village NPCs
    32171: Race.DWARF, // Hoffa, Warehouse Chief
    32158: Race.DWARF, // Fisler, Dwarf Guild Warehouse Chief
    32157: Race.DWARF, // Moka, Dwarf Guild Head Blacksmith
    32160: Race.DARK_ELF, // Devon, Dark Elf Guild Grand Magister
    32147: Race.ELF, // Rivian, Elf Guild Grand Master
    32150: Race.ORC, // Took, Orc Guild High Prefect
    32153: Race.HUMAN, // Prana, Human Guild High Priest
    32154: Race.HUMAN, // Aldenia, Human Guild Grand Master
}

const npcIds : Array<number> = _.keys( masters ).map( value => _.parseInt( value ) )

export class FirstClassTransferTalk extends ListenerLogic {
    constructor() {
        super( 'FirstClassTransferTalk', 'listeners/npcs/FirstClassTransferTalk.ts' )
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onNpcEvent( data : NpcGeneralEvent ): Promise<string> {
        return this.getPath( data.eventName )
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/VillageMasters/FirstClassTransferTalk'
    }

    async onTalkEvent( data : NpcTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let npc : L2Npc = L2World.getObjectById( data.characterId ) as L2Npc

        if ( masters[ npc.getId() ] !== player.getRace() ) {
            return this.getDefaultAnswer( npc )
        }

        let defaultFilename = this.getDefaultFilename( npc )

        switch ( masters[ npc.getId() ] ) {
            case Race.HUMAN:
                if ( ClassId.getLevelByClassId( player.getClassId() ) === 0 ) {
                    if ( player.isMageClass() ) {
                        if ( npc.isInstanceType( InstanceType.L2VillageMasterPriestInstance ) ) {
                            return `${defaultFilename}mystic.html`
                        }
                    } else {
                        if ( npc.isInstanceType( InstanceType.L2VillageMasterFighterInstance ) ) {
                            return `${defaultFilename}fighter.html`
                        }
                    }

                    return this.getDefaultAnswer( npc )
                }

                if ( ClassId.getLevelByClassId( player.getClassId() ) === 1 ) {
                    return `${defaultFilename}transfer_1.html`
                }

                return `${defaultFilename}transfer_2.html`

            case Race.ELF:
            case Race.DARK_ELF:
            case Race.ORC: {
                if ( ClassId.getLevelByClassId( player.getClassId() ) === 0 ) {
                    if ( player.isMageClass() ) {
                        return `${defaultFilename}mystic.html`
                    }

                    return `${defaultFilename}fighter.html`
                }

                if ( ClassId.getLevelByClassId( player.getClassId() ) === 1 ) {
                    return `${defaultFilename}transfer_1.html`
                }

                return `${defaultFilename}transfer_2.html`
            }
            case Race.DWARF: {
                if ( ClassId.getLevelByClassId( player.getClassId() ) === 0 ) {
                    return `${defaultFilename}fighter.html`
                }

                if ( ClassId.getLevelByClassId( player.getClassId() ) === 1 ) {
                    return `${defaultFilename}transfer_1.html`
                }

                return `${defaultFilename}transfer_2.html`
            }
        }

        return `${defaultFilename}no.html`
    }

    getDefaultFilename( npc: L2Npc ) : string {
        return this.getPath( `${npc.getId()}_` )
    }

    getDefaultAnswer( npc: L2Npc ) : string {
        return `${this.getDefaultFilename( npc )}no.html`
    }
}