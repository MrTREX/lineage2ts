import { ListenerDescription } from '../../gameService/models/ListenerLogic'
import { ConfigManager } from '../../config/ConfigManager'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import {
    EventType,
    NpcApproachedForTalkEvent,
    NpcGeneralEvent,
    PlayerChangedLevelEvent,
    PlayerLoginEvent,
    PlayerTutorialLinkEvent,
} from '../../gameService/models/events/EventType'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { TutorialCloseHtml } from '../../gameService/packets/send/TutorialCloseHtml'
import { ClassId, ClassIdValues, IClassId } from '../../gameService/models/base/ClassId'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { TutorialShowQuestionMark } from '../../gameService/packets/send/TutorialShowQuestionMark'
import { DataManager } from '../../data/manager'
import { UserInfo } from '../../gameService/packets/send/UserInfo'
import { ExBrExtraUserInfo } from '../../gameService/packets/send/ExBrExtraUserInfo'
import { L2Item } from '../../gameService/models/items/L2Item'
import { TutorialShowHtml } from '../../gameService/packets/send/TutorialShowHtml'
import { L2World } from '../../gameService/L2World'
import { PersistedConfigurationLogic } from '../../gameService/models/listener/PersistedConfigurationLogic'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { InventoryAction } from '../../gameService/enums/InventoryAction'
import { FastRateLimit } from 'fast-ratelimit'
import aigle from 'aigle'
import _ from 'lodash'
import { SpawnEventManager } from '../../gameService/models/spawns/SpawnEventManager'
import { NamedEventAction } from '../../gameService/models/spawns/SpawnEventType'

const tutorialLinkLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 5, // time-to-live value of token bucket (in seconds)
} )

const enum Values {
    MR_CAT = 31756,
    MISS_QUEEN = 31757,
    EventId = 1001
}


interface ClassMasterItem {
    itemId: number
    amount: number
}

interface ClassMasterConfiguration {
    rewards: { [ level: number ]: Array<ClassMasterItem> }
    requireItems: { [ level: number ]: Array<ClassMasterItem> }
    isEnabled: boolean
    allowedProfessionLevels: Array<number>
}

function validateClassId( currentClassId: number, futureClassId: number ): boolean {
    let oldClassId = ClassId.getClassIdByIdentifier( currentClassId )
    let newCLassId = ClassId.getClassIdByIdentifier( futureClassId )
    return newCLassId
            && oldClassId
            && newCLassId.race
            && ( ( newCLassId.parent && oldClassId.id === ClassId.getClassId( newCLassId.parent ).id )
                    || ( ConfigManager.character.allowEntireTree() && ClassId.isChildOf( newCLassId, oldClassId.parent ) ) )

}

export class ClassMaster extends PersistedConfigurationLogic<ClassMasterConfiguration> {
    getDefaultConfiguration(): ClassMasterConfiguration {
        return {
            requireItems: {
                1: [ {
                    itemId: 57,
                    amount: 100000,
                } ],
                2: [ {
                    itemId: 57,
                    amount: 1000000,
                } ],
                3: [ {
                    itemId: 57,
                    amount: 10000000,
                } ],
            },
            rewards: {},
            isEnabled: true,
            allowedProfessionLevels: [ 1, 2, 3 ],
        }
    }

    constructor() {
        super( 'ClassMaster', 'listeners/npcs/ClassMaster.ts' )
    }

    async checkAndChangeClass( player: L2PcInstance, futureClassId: number ): Promise<boolean> {
        let currentClassId: number = player.getClassId()
        let currentClassIdLevel = ClassId.getLevelByClassId( currentClassId )
        if ( ( this.getMinimumLevel( currentClassIdLevel ) > player.getLevel() ) && !ConfigManager.character.allowEntireTree() ) {
            return false
        }

        if ( !validateClassId( currentClassId, futureClassId ) ) {
            return false
        }

        let newLevel = currentClassIdLevel + 1

        let rewards: Array<ClassMasterItem> = this.configuration.rewards[ newLevel ]
        if ( !_.isEmpty( rewards ) && !player.isInventoryUnder90( false ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INVENTORY_LESS_THAN_80_PERCENT ) )
            return false
        }

        let requiredItems: Array<ClassMasterItem> = this.configuration.requireItems[ newLevel ]
        let shouldStop: boolean = requiredItems.some( ( item: ClassMasterItem ) => {
            return player.getInventory().getInventoryItemCount( item.itemId, -1 ) < item.amount
        } )

        if ( shouldStop ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ITEMS ) )
            return false
        }

        shouldStop = await aigle.resolve( requiredItems ).someSeries( async ( item: ClassMasterItem ) => {
            return !( await player.destroyItemByItemId( item.itemId, item.amount, true, 'Quest:ClassMaster' ) )
        } )

        if ( shouldStop ) {
            return false
        }

        await aigle.resolve( rewards ).eachSeries( async ( item: ClassMasterItem ) => {
            return player.addItem( item.itemId, item.amount, -1, player.getObjectId(), InventoryAction.QuestReward )
        } )

        await player.setClassId( futureClassId )

        if ( player.isSubClassActive() ) {
            player.getSubClasses()[ player.getClassIndex() ].setClassId( player.getActiveClass() )
        } else {
            player.setBaseClass( player.getActiveClass() )
        }

        player.broadcastUserInfo()

        if ( this.isEnabled()
            && this.configuration.allowedProfessionLevels.includes( newLevel )
            && ( ( currentClassIdLevel === 1 && player.getLevel() >= 40 ) || ( currentClassIdLevel === 2 && player.getLevel() >= 76 ) ) ) {
            this.showQuestionMark( player )
        }

        return true
    }

    getCustomListeners(): Array<ListenerDescription> {
        if ( this.isEnabled() ) {
            return [
                {
                    registerType: ListenerRegisterType.General,
                    eventType: EventType.PlayerLogin,
                    method: this.onEnterWorldEvent.bind( this ),
                    ids: null,
                },
                {
                    registerType: ListenerRegisterType.General,
                    eventType: EventType.PlayerTutorialLink,
                    method: this.onPlayerTutorialEvent.bind( this ),
                    ids: null,
                },
                {
                    registerType: ListenerRegisterType.General,
                    eventType: EventType.PlayerTutorialQuestionMark,
                    method: this.onPlayerTutorialQuestionMarkEvent.bind( this ),
                    ids: null,
                },
                {
                    registerType: ListenerRegisterType.General,
                    eventType: EventType.PlayerChangedLevel,
                    method: this.onLevelUp.bind( this ),
                    ids: null,
                },
            ]
        }

        return []
    }

    getApproachedForTalkIds(): Array<number> {
        return [ Values.MR_CAT, Values.MISS_QUEEN ]
    }

    getPathPrefix(): string {
        return 'overrides/html/npc/classMaster'
    }

    getQuestStartIds(): Array<number> {
        return [ Values.MR_CAT, Values.MISS_QUEEN ]
    }

    getTalkIds(): Array<number> {
        return [ Values.MR_CAT, Values.MISS_QUEEN ]
    }

    isEnabled() : boolean {
        return this.configuration.isEnabled
    }

    async initialize(): Promise<void> {
        if ( !this.isEnabled() ) {
            return
        }

        /*
            TODO : add ability for GM to change configuration and force despawn if necessary
            - if listener is not enabled, issue named event to despawn npcs and vice versa
         */
        SpawnEventManager.callNamedTrigger( 'test_2nd_class', NamedEventAction.Start )
    }

    async onEnterWorldEvent( data: PlayerLoginEvent ): Promise<string> {
        this.showQuestionMark( L2World.getPlayer( data.playerId ) )

        return
    }

    async onLevelUp( data: PlayerChangedLevelEvent ): Promise<void> {
        this.showQuestionMark( L2World.getPlayer( data.playerId ) )
    }

    async onApproachedForTalk( data: NpcApproachedForTalkEvent ): Promise<string> {
        return this.getPath( `${ data.characterNpcId }.htm` )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {

        let event = data.eventName
        let player = L2World.getPlayer( data.playerId )

        if ( event.endsWith( '.htm' ) ) {
            return this.getPath( event )
        }

        if ( event.startsWith( '1stClass' ) ) {
            return this.showHtmlMenu( player, data.characterId, 1 )
        }

        if ( event.startsWith( '2ndClass' ) ) {
            return this.showHtmlMenu( player, data.characterId, 2 )
        }

        if ( event.startsWith( '3rdClass' ) ) {
            return this.showHtmlMenu( player, data.characterId, 3 )
        }

        if ( event.startsWith( 'change_class' ) ) {
            let classId = _.parseInt( event.substring( 13 ) )
            if ( await this.checkAndChangeClass( player, classId ) ) {
                return DataManager.getHtmlData().getItem( this.getPath( 'ok.htm' ) )
                                  .replace( /%name%/g, ClassId.getClientCode( classId ) )
            }

            return
        }

        if ( event.startsWith( 'become_noble' ) ) {
            if ( !player.isNoble() ) {
                await player.setNoble( true )
                player.sendDebouncedPacket( UserInfo )
                player.sendDebouncedPacket( ExBrExtraUserInfo )
                return this.getPath( 'nobleok.htm' )
            }

            return
        }

        if ( event.startsWith( 'learn_skills' ) ) {
            await player.giveAvailableSkills( ConfigManager.character.autoLearnForgottenScrollSkills(), true )
            return
        }

        if ( event.startsWith( 'increase_clan_level' ) ) {
            if ( !player.isClanLeader() ) {
                return this.getPath( 'noclanleader.htm' )
            }
            if ( player.getClan().getLevel() >= 5 ) {
                return this.getPath( 'noclanlevel.htm' )
            }

            await player.getClan().changeLevel( 5 )

            return
        }
    }

    getHtmlMessage( level: number ): string {
        switch ( level ) {
            case 0:
                if ( this.configuration.allowedProfessionLevels.includes( 1 ) ) {
                    return 'Come back here when you reached level 20 to change your class.<br>'
                }

                if ( this.configuration.allowedProfessionLevels.includes( 2 ) ) {
                    return 'Come back after your first occupation change.<br>'
                }

                if ( this.configuration.allowedProfessionLevels.includes( 3 ) ) {
                    return 'Come back after your second occupation change.<br>'
                }

                return 'I can\'t change your occupation.<br>'

            case 1:
                if ( this.configuration.allowedProfessionLevels.includes( 2 ) ) {
                    return 'Come back here when you reached level 40 to change your class.<br>'
                }

                if ( this.configuration.allowedProfessionLevels.includes( 3 ) ) {
                    return 'Come back after your second occupation change.<br>'
                }

                return 'I can\'t change your occupation.<br>'

            case 2:
                if ( this.configuration.allowedProfessionLevels.includes( 3 ) ) {
                    return 'Come back here when you reached level 76 to change your class.<br>'
                }

                return 'I can\'t change your occupation.<br>'

            case 3:
                return 'There is no class change available for you anymore.<br>'
        }

        return ''
    }

    getMinimumLevel( level: number ): number {
        switch ( level ) {
            case 0:
                return 20

            case 1:
                return 40

            case 2:
                return 76
        }

        return Number.MAX_SAFE_INTEGER
    }

    getRequiredItemsHtml( level: number ): string {
        return this.getItemsHtml( this.configuration.requireItems[ level ] )
    }

    getRewardedItemsHtml( level: number ): string {
        return this.getItemsHtml( this.configuration.rewards[ level ] )
    }

    getItemsHtml( items: Array<ClassMasterItem> ): string {
        if ( _.isEmpty( items ) ) {
            return '<tr><td>none</td></tr>'
        }

        let html: Array<string> = _.map( items, ( item: ClassMasterItem ) => {
            let template: L2Item = DataManager.getItems().getTemplate( item.itemId )
            if ( !template ) {
                return ''
            }

            return `<tr><td><font color="LEVEL">${ GeneralHelper.getAdenaFormat( item.amount ) }</font></td><td>${ template.getName() }</td></tr>`
        } )

        return html.join( '' )
    }

    async onPlayerTutorialEvent( data: PlayerTutorialLinkEvent ) {
        if ( !this.isEnabled()
            || !tutorialLinkLimiter.consumeSync( data.playerId.toString() )
            || !data.command.startsWith( 'CO' ) ) {
            return
        }

        let value = _.parseInt( data.command.substring( 2 ) )
        let player = L2World.getPlayer( data.playerId )

        await this.checkAndChangeClass( player, value )
        player.sendOwnedData( TutorialCloseHtml( player.getObjectId() ) )
    }

    async onPlayerTutorialQuestionMarkEvent( player: L2PcInstance, tutorialEventId: number ): Promise<string> {
        if ( !this.isEnabled() || tutorialEventId !== Values.EventId ) {
            return
        }

        this.showTutorialHtml( player )
    }

    showHtmlMenu( player: L2PcInstance, objectId: number, level: number ): string {
        let playerClassIdLevel = ClassId.getLevelByClassId( player.getClassId() )
        if ( !this.configuration.allowedProfessionLevels.includes( level ) ) {
            return `<html><body>${ this.getHtmlMessage( playerClassIdLevel ) }</body></html>`
        }

        if ( playerClassIdLevel >= level ) {
            return this.getPath( 'nomore.htm' )
        }

        let minLevel = this.getMinimumLevel( playerClassIdLevel )
        if ( player.getLevel() >= minLevel || ConfigManager.character.allowEntireTree() ) {

            let menu: Array<string> = []
            _.each( ClassIdValues, ( currentClassId: IClassId, key: string ) => {
                if ( key === 'inspector' && player.getTotalSubClasses() < 2 ) {
                    return
                }

                if ( validateClassId( player.getClassId(), currentClassId.id ) ) {
                    menu.push( `<a action="bypass -h Quest ClassMaster change_class ${ currentClassId.id }">${ _.escape( ClassId.getClientCode( currentClassId.id ) ) }</a><br>` )
                }
            } )

            if ( menu.length > 0 ) {
                return DataManager.getHtmlData().getItem( this.getPath( 'template.htm' ) )
                                  .replace( /%name%/g, ClassId.getClientCode( player.getClassId() ) )
                                  .replace( /%menu%/g, menu.join( '' ) )
                                  .replace( /%requiredItems%/g, this.getRequiredItemsHtml( level ) )
                                  .replace( /%rewardedItems%/g, this.getRewardedItemsHtml( level ) )

            }

            return DataManager.getHtmlData().getItem( this.getPath( 'comebacklater.htm' ) )
                              .replace( /%level%/g, this.getMinimumLevel( level - 1 ).toString() )
        }

        if ( minLevel < Number.MAX_SAFE_INTEGER ) {
            return DataManager.getHtmlData().getItem( this.getPath( 'comebacklater.htm' ) )
                              .replace( /%level%/g, minLevel.toString() )
        }

        return this.getPath( 'nomore.htm' )
    }

    showQuestionMark( player: L2PcInstance ) {
        if ( !this.isEnabled() ) {
            return
        }

        let classId = player.getClassId()
        let currentClassIdLevel = ClassId.getLevelByClassId( classId )
        if ( this.getMinimumLevel( currentClassIdLevel ) > player.getLevel() ) {
            return
        }

        if ( !this.configuration.allowedProfessionLevels.includes( currentClassIdLevel + 1 ) ) {
            return
        }

        player.sendOwnedData( TutorialShowQuestionMark( Values.EventId ) )
    }

    showTutorialHtml( player: L2PcInstance ): void {
        let classId = player.getClassId()
        let currentClassIdLevel = ClassId.getLevelByClassId( classId )
        if ( ( this.getMinimumLevel( currentClassIdLevel ) > player.getLevel() ) && !ConfigManager.character.allowEntireTree() ) {
            return
        }

        let menu: Array<string> = []
        _.each( ClassIdValues, ( currentClassId: IClassId, key: string ) => {
            if ( key === 'inspector' && player.getTotalSubClasses() < 2 ) {
                return
            }

            if ( validateClassId( classId, currentClassId.id ) ) {
                menu.push( `<a action="link CO${ currentClassId.id }">${ _.escape( ClassId.getClientCode( currentClassId.id ) ) }</a><br>` )
            }
        } )

        let path = this.getPath( 'tutorialtemplate.htm' )
        let html: string = DataManager.getHtmlData().getItem( path )
                                      .replace( /%name%/g, _.escape( ClassId.getClientCode( classId ) ) )
                                      .replace( /%menu%/g, menu.join( '' ) )
                                      .replace( /%requiredItems%/g, this.getRequiredItemsHtml( currentClassIdLevel + 1 ) )
                                      .replace( /%rewardedItems%/g, this.getRewardedItemsHtml( currentClassIdLevel + 1 ) )

        player.sendOwnedData( TutorialShowHtml( html, path, player.getObjectId() ) )
    }
}