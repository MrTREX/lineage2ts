import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { PlayerActionOverride } from '../../gameService/values/PlayerConditions'
import { NpcApproachedForTalkEvent, NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { ClanPrivilege } from '../../gameService/enums/ClanPriviledge'

const npcIds: Array<number> = [
    35098, // Blacksmith (Gludio)
    35140, // Blacksmith (Dion)
    35182, // Blacksmith (Giran)
    35224, // Blacksmith (Oren)
    35272, // Blacksmith (Aden)
    35314, // Blacksmith (Innadril)
    35361, // Blacksmith (Goddard)
    35507, // Blacksmith (Rune)
    35553, // Blacksmith (Schuttgart)
]

export class CastleBlacksmith extends ListenerLogic {
    constructor() {
        super( 'CastleBlacksmith', 'listeners/npcs/CastleBlacksmith.ts' )
    }

    getApproachedForTalkIds(): Array<number> {
        return npcIds
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/CastleBlacksmith'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onApproachedForTalk( data : NpcApproachedForTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.characterId ) as L2Npc

        return this.getPath( this.hasRights( player, npc ) ? `${ npc.getId() }-01.html` : 'no.html' )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let event = data.eventName
        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.characterId ) as L2Npc

        return ( event === `${ npc.getId() }-02.html` && this.hasRights( player, npc ) ) ? this.getPath( event ) : null
    }

    hasRights( player: L2PcInstance, npc: L2Npc ): boolean {
        return player.hasActionOverride( PlayerActionOverride.CastleRights )
                || npc.isLord( player )
                || ( player.getClanId() === npc.getCastle().getOwnerId() && player.hasClanPrivilege( ClanPrivilege.CastleManor ) )
    }
}