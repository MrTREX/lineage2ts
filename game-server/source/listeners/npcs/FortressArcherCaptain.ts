import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { NpcApproachedForTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'

const npcIds: Array<number> = [
    35661, // Shanty Fortress
    35692, // Southern Fortress
    35730, // Hive Fortress
    35761, // Valley Fortress
    35799, // Ivory Fortress
    35830, // Narsell Fortress
    35861, // Bayou Fortress
    35899, // White Sands Fortress
    35930, // Borderland Fortress
    35968, // Swamp Fortress
    36006, // Archaic Fortress
    36037, // Floran Fortress
    36075, // Cloud Mountain
    36113, // Tanor Fortress
    36144, // Dragonspine Fortress
    36175, // Antharas's Fortress
    36213, // Western Fortress
    36251, // Hunter's Fortress
    36289, // Aaru Fortress
    36320, // Demon Fortress
    36358, // Monastic Fortress
]

export class FortressArcherCaptain extends ListenerLogic {
    constructor() {
        super( 'FortressArcherCaptain', 'listeners/npcs/FortressArcherCaptain.ts' )
    }

    getApproachedForTalkIds(): Array<number> {
        return npcIds
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/FortressArcherCaptain'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    async onApproachedForTalk( data : NpcApproachedForTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let npc : L2Npc = L2World.getObjectById( data.characterId ) as L2Npc
        let fortOwner = npc.getFort().getOwnerClan() ? npc.getFort().getOwnerClan().getId() : 0
        let path = ( player.getClan() && ( player.getClanId() === fortOwner ) ) ? 'FortressArcherCaptain.html' : 'FortressArcherCaptain-01.html'

        return this.getPath( path )
    }
}