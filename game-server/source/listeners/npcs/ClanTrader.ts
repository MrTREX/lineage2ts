import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { ConfigManager } from '../../config/ConfigManager'
import { QuestHelper } from '../helpers/QuestHelper'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { NpcApproachedForTalkEvent, NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { ClanPrivilege } from '../../gameService/enums/ClanPriviledge'

const npcIds: Array<number> = [
    32024, // Mulia
    32025, // Ilia
]

const BLOOD_ALLIANCE = 9911 // Blood Alliance
const BLOOD_ALLIANCE_COUNT = 1 // Blood Alliance Count
const BLOOD_OATH = 9910 // Blood Oath
const BLOOD_OATH_COUNT = 10 // Blood Oath Count
const KNIGHTS_EPAULETTE = 9912 // Knight's Epaulette
const KNIGHTS_EPAULETTE_COUNT = 100 // Knight's Epaulette Count

export class ClanTrader extends ListenerLogic {
    constructor() {
        super( 'ClanTrader', 'listeners/npcs/ClanTrader.ts' )
    }

    getApproachedForTalkIds(): Array<number> {
        return npcIds
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/ClanTrader'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onApproachedForTalk( data : NpcApproachedForTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )

        if ( player.isClanLeader() || player.hasClanPrivilege( ClanPrivilege.NameTroops ) ) {
            return this.getPath( `${ data.characterNpcId }.html` )
        }

        return this.getPath( `${ data.characterNpcId }-01.html` )
    }

    async onNpcEvent( data : NpcGeneralEvent ): Promise<string> {

        let event = data.eventName
        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        let player = L2World.getPlayer( data.playerId )

        switch ( event ) {
            case '32024.html':
            case '32024-02.html':
            case '32025.html':
            case '32025-02.html':
                return this.getPath( event )

            case 'repinfo':
                return this.getPath( ( player.getClan().getLevel() > 4 ) ? `${ npc.getId() }-02.html` : `${ npc.getId() }-05.html` )

            case 'exchange-ba':
                return this.getPath( await this.giveReputation( npc, player, ConfigManager.clan.getBloodAlliancePoints(), BLOOD_ALLIANCE, BLOOD_ALLIANCE_COUNT ) )

            case 'exchange-bo':
                return this.getPath( await this.giveReputation( npc, player, ConfigManager.clan.getBloodOathPoints(), BLOOD_OATH, BLOOD_OATH_COUNT ) )

            case 'exchange-ke':
                return this.getPath( await this.giveReputation( npc, player, ConfigManager.clan.getKnightsEpaulettePoints(), KNIGHTS_EPAULETTE, KNIGHTS_EPAULETTE_COUNT ) )
        }
    }

    async giveReputation( npc: L2Npc, player: L2PcInstance, amount: number, itemId: number, itemAmount: number ): Promise<string> {
        if ( QuestHelper.getQuestItemsCount( player, itemId ) >= itemAmount ) {
            await QuestHelper.takeSingleItem( player, itemId, itemAmount )
            await player.getClan().addReputationScore( amount, true )

            let message = new SystemMessageBuilder( SystemMessageIds.CLAN_ADDED_S1S_POINTS_TO_REPUTATION_SCORE )
                    .addNumber( amount )
                    .getBuffer()
            player.sendOwnedData( message )

            return `${ npc.getId() }-04.html`
        }

        return `${ npc.getId() }-03.html`
    }
}