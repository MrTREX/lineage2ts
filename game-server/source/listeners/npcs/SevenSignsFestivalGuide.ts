import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import { SkillHolder } from '../../gameService/models/holders/SkillHolder'
import _, { DebouncedFunc } from 'lodash'
import { SevenSigns } from '../../gameService/directives/SevenSigns'
import {
    EventType,
    NpcApproachedForTalkEvent,
    NpcFinishedSkillEvent,
    NpcGeneralEvent,
    NpcSpawnEvent,
    NpcTalkEvent
} from '../../gameService/models/events/EventType'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { QuestHelper } from '../helpers/QuestHelper'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { DataManager } from '../../data/manager'
import { SevenSignsSide, SevenSignsValues } from '../../gameService/values/SevenSignsValues'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { SevenSignsFestival } from '../../gameService/directives/SevenSignsFestival'
import { CachedResult } from '../../gameService/models/CachedResult'
import moment from 'moment'
import { CharacterNamesCache } from '../../gameService/cache/CharacterNamesCache'
import { ItemTypes } from '../../gameService/values/InventoryValues'
import { AllFestivalLevels } from '../../gameService/values/SevenSignsFestivalValues'
import { Location } from '../../gameService/models/Location'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import { InstanceType } from '../../gameService/enums/InstanceType'
import { PlayerGroupCache } from '../../gameService/cache/PlayerGroupCache'
import { ConfigManager } from '../../config/ConfigManager'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { SpawnMakerCache } from '../../gameService/cache/SpawnMakerCache'
import { L2NpcTemplate } from '../../gameService/models/actor/templates/L2NpcTemplate'
import { L2ManualSpawn } from '../../gameService/models/spawns/type/L2ManualSpawn'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'
import { teleportCharacterToGeometryCoordinates } from '../../gameService/helpers/TeleportHelper'
import { GeometryId } from '../../gameService/enums/GeometryId'

const dawnNpcIds: Array<number> = [
    31127, 31128, 31129, 31130, 31131
]

const duskNpcIds: Array<number> = [
    31137, 31138, 31139, 31140, 31141
]

const SevenSignsSkill = new SkillHolder( 4334, 1 )

const BLOOD_OF_OFFERING = 5901
const DIMENSIONAL_FRAGMENT = 7079
const THE_STAFF = 214

const WEIGHT_LIMIT = 0.80
const QUEST_LIMIT = 0.90
const MaxActiveQuests = 40

const npcIdDawnMapping: Record<number, string> = {
    31127: 'dawn_acolyte1001.htm',
    31128: 'dawn_acolyte2001.htm',
    31129: 'dawn_acolyte3001.htm',
    31130: 'dawn_acolyte4001.htm',
    31131: 'dawn_acolyte5001.htm',
    31137: 'dusk_acolyte1001.htm',
    31138: 'dusk_acolyte2001.htm',
    31139: 'dusk_acolyte3001.htm',
    31140: 'dusk_acolyte4001.htm',
    31141: 'dusk_acolyte5001.htm',
}

const npcIdEventMapping: Record<number, string> = {
    31127: 'ssq_main_event_acolyte_q0505_20f.htm',
    31128: 'ssq_main_event_acolyte_q0505_20g.htm',
    31129: 'ssq_main_event_acolyte_q0505_20h.htm',
    31130: 'ssq_main_event_acolyte_q0505_20i.htm',
    31131: 'ssq_main_event_acolyte_q0505_20j.htm',
    31137: 'ssq_main_event_acolyte_q0505_20a.htm',
    31138: 'ssq_main_event_acolyte_q0505_20b.htm',
    31139: 'ssq_main_event_acolyte_q0505_20c.htm',
    31140: 'ssq_main_event_acolyte_q0505_20d.htm',
    31141: 'ssq_main_event_acolyte_q0505_20e.htm',
}

const roomIdMapping: Record<number, string> = {
    1: 'Level 31 or lower',
    2: 'Level 42 or lower',
    3: 'Level 53 or lower',
    4: 'Level 64 or lower',
    5: 'No Level Limit',
}

const npcIdLevelMapping: Record<number, string> = {
    31127: '32',
    31137: '32',
    31128: '43',
    31138: '43',
    31129: '54',
    31139: '54',
    31130: '65',
    31140: '65',
}

const npcIdLevelPathMapping: Record<number, string> = {
    31127: 'ssq_main_event_acolyte_q0505_04a.htm',
    31137: 'ssq_main_event_acolyte_q0505_04a.htm',
    31128: 'ssq_main_event_acolyte_q0505_04b.htm',
    31138: 'ssq_main_event_acolyte_q0505_04b.htm',
    31129: 'ssq_main_event_acolyte_q0505_04c.htm',
    31139: 'ssq_main_event_acolyte_q0505_04c.htm',
    31130: 'ssq_main_event_acolyte_q0505_04d.htm',
    31140: 'ssq_main_event_acolyte_q0505_04d.htm',
}

const npcIdToMinLevel: Record<number, number> = {
    31127: 31,
    31137: 31,
    31128: 42,
    31138: 42,
    31129: 53,
    31139: 53,
    31130: 64,
    31140: 64,
    31131: 99, // TODO : why 99 level?
    31141: 99,
}

const blueStoneMapping: Record<number, number> = {
    31127: 900,
    31137: 900,
    31128: 1500,
    31138: 1500,
    31129: 3000,
    31139: 3000,
    31130: 4500,
    31140: 4500,
    31131: 6000,
    31141: 6000,
}

const greenStoneMapping: Record<number, number> = {
    31127: 540,
    31137: 540,
    31128: 900,
    31138: 900,
    31129: 1800,
    31139: 1800,
    31130: 2700,
    31140: 2700,
    31131: 3600,
    31141: 3600,
}

const redStoneMapping: Record<number, number> = {
    31127: 270,
    31137: 270,
    31128: 450,
    31138: 450,
    31129: 900,
    31139: 900,
    31130: 1350,
    31140: 1350,
    31131: 1800,
    31141: 1800,
}

const roomBonusMapping: Record<number, number> = {
    31127: 2700,
    31137: 2700,
    31128: 4500,
    31138: 4500,
    31129: 9000,
    31139: 9000,
    31130: 13500,
    31140: 13500,
    31131: 18000,
    31141: 18000,
}

const allNpcIds: Array<number> = [
    ...duskNpcIds,
    ...dawnNpcIds
]

const enum TimeValues {
    CloseToFinishTime = 20 * 60 * 1000,
    CloseToEventTime = 18 * 60 * 1000
}

const enum ActionTypes {
    Inquiry = 'ask',
    Return = 'return'
}

const enum QuestId {
    Q00505_BloodOffering = 'Q00505_BloodOffering',
}

const adjustmentScoreMap : Record<number, Location> = {
    1: new Location( -41443, 210030, -5080 ),
    2: new Location( -53034, -250421, -7935 ),
    3: new Location( 45160, 123605, -5408 ),
    4: new Location( 46488, 170184, -4976 ),
    5: new Location( 111521, 173905, -5432 ),
    6: new Location( -20395, -250930, -8191 ),
    7: new Location( -21482, 77253, -5168 ),
    8: new Location( 140688, 79565, -5424 ),
    9: new Location( -52007, 78986, -4736 ),
    10: new Location( 118547, 132669, -4824 ),
    11: new Location( 172562, -17730, -4896 ),
    12: new Location( 83344, 209110, -5432 ),
    13: new Location( -19154, 13415, -4896 ),
    14: new Location( 12747, -248614, -9607 ),
}

const enum StoneParticipation {
    Red = 'red',
    Blue = 'blue',
    Green = 'green'
}

const enum VariableName {
    ParticipationType = 'ss.color',
    LastContributionTime = 'ss.time',
    LastRoom = 'ss.room'
}

export class SevenSignsFestivalGuide extends ListenerLogic {
    debounceOnSpawn: DebouncedFunc<any>
    cachedInquiryFestival: CachedResult<string>
    onIntervalTimeout: NodeJS.Timeout

    spawnedIds: Set<number> = new Set<number>()
    intervalIds: Set<number> = new Set<number>()

    objectIdsWithMessages: Set<number> = new Set<number>()

    constructor() {
        super( 'SSFestivalGuide', 'listeners/npcs/SevenSignsFestivalGuide.ts' )

        this.debounceOnSpawn = _.debounce( this.onNpcSay.bind( this, true ), 1000 )
        this.cachedInquiryFestival = new CachedResult( this.refreshInquiryFestival.bind( this ), 30000 )
    }

    getSpawnIds(): Array<number> {
        return allNpcIds
    }

    getTalkIds(): Array<number> {
        return allNpcIds
    }

    getApproachedForTalkIds(): Array<number> {
        return allNpcIds
    }

    getQuestStartIds(): Array<number> {
        return allNpcIds
    }

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.NpcId,
                eventType: EventType.NpcSkillFinished,
                method: this.onSkillCastFinished.bind( this ),
                ids: allNpcIds,
                triggers: {
                    targetIds: new Set<number>( [ SevenSignsSkill.getId() ] )
                }
            }
        ]
    }

    private getNpcMenuPath( npcId: number ): string {
        return npcIdDawnMapping[ npcId ]
    }

    private getRoomDescription( id: number ): string {
        return roomIdMapping[ id ]
    }

    private participationHtmlPath( npcId: number ): string {
        return npcIdEventMapping[ npcId ]
    }

    private fStringLevel( npcId: number ): string {
        return npcIdLevelMapping[ npcId ]
    }

    private getIncorrectLevelPath( npcId: number ): string {
        return npcIdLevelPathMapping[ npcId ]
    }

    private getMinLevel( npcId: number ): number {
        return npcIdToMinLevel[ npcId ] ?? 0
    }

    private getBlueStoneCount( npcId: number ): number {
        return blueStoneMapping[ npcId ] ?? 0
    }

    private getGreenStoneCount( npcId: number ): number {
        return greenStoneMapping[ npcId ] ?? 0
    }

    private getRedStoneCount( npcId ): number {
        return redStoneMapping[ npcId ] ?? 0
    }

    private getRoomBonus( npcId: number ): number {
        return roomBonusMapping[ npcId ] ?? 0
    }

    async onSpawnEvent( data: NpcSpawnEvent ): Promise<void> {
        if ( !SevenSigns.isCompetitionPeriod() ) {
            return
        }

        this.spawnedIds.add( data.characterId )
        this.debounceOnSpawn()
    }

    getPathPrefix(): string {
        return 'overrides/html/npc/festivalGuide'
    }

    onNpcSay( useSpawnIds: boolean ) : void {
        /*
            TODO : use seven signs event to only enable interval timer on competition period
         */
        if ( SevenSigns.isCompetitionPeriod() ) {
            let ids = useSpawnIds ? this.spawnedIds : this.intervalIds
            let currentMinute = GeneralHelper.getCurrentMinute()
            let millisToPeriodChange = SevenSigns.getTimeToPeriodChange()

            ids.forEach( ( objectId: number ) : void => {
                let npc = L2World.getObjectById( objectId ) as L2Npc
                if ( !npc ) {
                    return
                }

                /*
                    There are only two npcs that have shout enabled, these are festival guides
                    that represent lowest level festival rooms.
                 */
                let ableToShout = npc.getTemplate().getParameters()[ 'ShoutSysMsg' ] === 1
                let needsShout = !this.objectIdsWithMessages.has( objectId )
                if ( ( currentMinute === 18 || currentMinute === 38 || currentMinute === 58 ) && millisToPeriodChange >= TimeValues.CloseToEventTime ) {
                    if ( needsShout ) {
                        if ( ableToShout ) {
                            BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcShout, NpcStringIds.THE_MAIN_EVENT_WILL_START_IN_2_MINUTES_PLEASE_REGISTER_NOW )
                        }

                        this.objectIdsWithMessages.add( objectId )
                    }

                    return
                }

                if ( ( currentMinute === 0 || currentMinute === 20 || currentMinute === 40 ) && millisToPeriodChange >= TimeValues.CloseToEventTime ) {
                    if ( needsShout ) {
                        if ( ableToShout ) {
                            BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcShout, NpcStringIds.THE_MAIN_EVENT_IS_NOW_STARTING )
                        }

                        this.objectIdsWithMessages.add( objectId )

                        /*
                            Spawns festival witch npc. There are two variables that seem to contain what npc to spawn:
                            - `SibylSilhouette` which should already contain npcId
                            - `SibylName` which contains npc reference id
                         */
                        let witchNpcId = npc.getTemplate().getParameters()[ 'SibylSilhouette' ] as number
                        if ( witchNpcId ) {
                            let spawn = SpawnMakerCache.getFirstSpawn( witchNpcId )
                            if ( !spawn ) {
                                let x = npc.getTemplate().getParameters()[ 'SibylPosX' ] as number
                                let y = npc.getTemplate().getParameters()[ 'SibylPosY' ] as number
                                let z = npc.getTemplate().getParameters()[ 'SibylPosZ' ] as number

                                let template: L2NpcTemplate = DataManager.getNpcData().getTemplate( witchNpcId )
                                let spawn: L2ManualSpawn = L2ManualSpawn.fromParameters( template, 1, x, y, z )

                                SpawnMakerCache.addDynamicSpawn( spawn )
                            }

                            spawn.startSpawn()
                        }
                    }

                    return
                }

                if ( currentMinute === 13 || currentMinute === 33 || currentMinute === 53 ) {
                    if ( needsShout ) {
                        if ( ableToShout ) {
                            BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcShout, NpcStringIds.THE_MAIN_EVENT_WILL_CLOSE_IN_5_MINUTES )
                        }

                        this.objectIdsWithMessages.add( objectId )
                    }

                    return
                }

                if ( currentMinute === 16 || currentMinute === 36 || currentMinute === 56 ) {
                    if ( needsShout && ableToShout ) {
                        let stringId = millisToPeriodChange >= TimeValues.CloseToFinishTime
                            ? NpcStringIds.THE_MAIN_EVENT_WILL_FINISH_IN_2_MINUTES_PLEASE_PREPARE_FOR_THE_NEXT_GAME
                            : NpcStringIds.THE_FESTIVAL_OF_DARKNESS_WILL_END_IN_TWO_MINUTES

                        BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcShout, stringId )
                        this.objectIdsWithMessages.add( objectId )
                    }

                    return
                }

                this.objectIdsWithMessages.delete( objectId )
            } )
        }

        if ( this.onIntervalTimeout ) {
            return
        }

        this.onIntervalTimeout = setInterval( this.onNpcSay.bind( this, false ), 7000 )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        const values : Array<string> = data.eventName.split( ';' )
        let operationId = parseInt( values[ 1 ], 10 )
        let player = L2World.getPlayer( data.playerId )

        if ( !player ) {
            return
        }

        if ( player.getInventory().getSize() >= ( player.getQuestInventoryLimit() * QUEST_LIMIT )
            || player.getInventory().getSize() >= ( player.getInventoryLimit() * WEIGHT_LIMIT )
            || player.getCurrentLoad() >= ( player.getMaxLoad() * WEIGHT_LIMIT ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CAN_PROCEED_ONLY_WHEN_THE_INVENTORY_WEIGHT_IS_BELOW_80_PERCENT_AND_THE_QUANTITY_IS_BELOW_90_PERCENT ) )
            return
        }

        switch ( values[ 0 ] ) {
            case ActionTypes.Inquiry:
                return this.onInquiry( player, operationId, data )

            case ActionTypes.Return:
                return this.onReturn( player, operationId, data )
        }
    }

    onInquiryParticipation( data: NpcGeneralEvent ) : string {
        if ( !SevenSigns.isCompetitionPeriod() ) {
            return this.getPath( 'ssq_main_event_acolyte_q0505_02.htm' )
        }

        let currentMinute = GeneralHelper.getCurrentMinute()
        if ( ( currentMinute >= 0 && currentMinute < 18 )
            || ( currentMinute >= 20 && currentMinute < 38 )
            || ( currentMinute >= 40 && currentMinute < 58 )
            || SevenSigns.getTimeToPeriodChange() <= 120000 ) {
            return this.getPath( 'ssq_main_event_acolyte_q0505_22.htm' )
        }

        return this.getPath( this.participationHtmlPath( data.characterNpcId ) )
    }

    async onInquiryScoreRegistration( player: L2PcInstance, data: NpcGeneralEvent ) : Promise<string> {
        let playerState = QuestStateCache.getQuestState( data.playerId, QuestId.Q00505_BloodOffering, true )
        let expectedSide = DataManager.getNpcData().getTemplate( data.characterNpcId ).getParameters()[ 'part_type' ] === 'DAWN' ? SevenSignsSide.Dawn : SevenSignsSide.Dusk
        let party = player.getParty()

        if ( party ) {
            for ( const playerId of party.getMembers() ) {
                let memberState = QuestStateCache.getQuestState( playerId, QuestId.Q00505_BloodOffering, true )
                if ( memberState.getMemoState() !== playerState.getMemoState() ) {
                    return
                }
            }
        }

        if ( SevenSigns.getPlayerSide( player.getObjectId() ) !== expectedSide ) {
            return this.getPath( 'ssq_main_event_acolyte_q0505_08.htm' )
        }

        if ( !SevenSigns.isCompetitionPeriod() ) {
            return this.getPath( 'ssq_main_event_acolyte_q0505_02.htm' )
        }

        if ( !party || !party.isLeader( player ) ) {
            return this.getPath( 'ssq_main_event_acolyte_q0505_10.htm' )
        }

        if ( ( ( ( Date.now() / 1000 ) - ( playerState.getMemoState() ?? 0 ) ) / 60 ) > 40 ) {
            return this.getPath( 'ssq_main_event_acolyte_q0505_11.htm' )
        }

        let roomIndex = DataManager.getNpcData().getTemplate( data.characterNpcId ).getParameters()[ 'RoomIndex' ] as number
        let festivalId = roomIndex - 1
        if ( playerState.getMemoStateEx( 1 ) !== festivalId ) {
            return this.getPath( 'ssq_main_event_acolyte_q0505_28.htm' )
        }

        if ( party ) {
            for ( const playerId of party.getMembers() ) {
                // TODO : consider removing quest state since it would be created anew each time
                let memberState = QuestStateCache.getQuestState( playerId, QuestId.Q00505_BloodOffering, true )
                await memberState.exitQuest( true, false )
            }
        }

        let itemAmount = QuestHelper.getQuestItemsCount( player, BLOOD_OF_OFFERING )
        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        switch ( data.characterNpcId ) {
            case 31127:
            case 31128:
            case 31129:
            case 31130:
            case 31137:
            case 31138:
            case 31139:
            case 31140:
                BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.Shout, NpcStringIds.S1_HAS_WON_THE_MAIN_EVENT_FOR_PLAYERS_UNDER_LEVEL_S2_AND_EARNED_S3_POINTS, player.getName(), this.fStringLevel( data.characterNpcId ), itemAmount.toString() )
                break

            case 31131:
                case 31141:
                    BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.Shout, NpcStringIds.S1_HAS_EARNED_S2_POINTS_IN_THE_MAIN_EVENT_FOR_UNLIMITED_LEVELS, player.getName(), itemAmount.toString() )
                break
        }

        await QuestHelper.takeSingleItem( player, BLOOD_OF_OFFERING, -1 )

        if ( SevenSigns.getSealTotal( festivalId, expectedSide ) > QuestHelper.getQuestItemsCount( player, BLOOD_OF_OFFERING ) ) {
            if ( itemAmount >= 3000 ) {
                return this.getPath( 'ssq_main_event_acolyte_q0505_12a.htm' )
            }

            return this.getPath( 'ssq_main_event_acolyte_q0505_12.htm' )
        }

        player.sendCopyData( SoundPacket.SKILLSOUND_JEWEL_CELEBRATE )

        SevenSignsFestival.updateSideScore( expectedSide, festivalId, itemAmount, party.getMembers() )

        if ( itemAmount >= 3000 ) {
            return this.getPath( 'ssq_main_event_acolyte_q0505_13a.htm' )
        }

        return this.getPath( 'ssq_main_event_acolyte_q0505_13.htm' )
    }

    getScoreNames( playerIds: ReadonlyArray<number> ) : Array<string> {
        if ( playerIds.length === 0 ) {
            return []
        }

        let playerNames : Array<string> = playerIds.reduce( ( allNames: Array<string>, playerId: number ) : Array<string> => {
            let name = CharacterNamesCache.getCachedNameById( playerId )
            if ( name ) {
                allNames.push( name )
            }

            return allNames
        }, [] )

        return _.chunk( playerNames, 3 ).map( ( names: [ string, string, string] ) : string => {
            return `<TR><TD>${names.join( ', ' )}</TD></TR>`
        } )
    }

    async onInquiryScores( data: NpcGeneralEvent ) : Promise<string> {
        let roomIndex = DataManager.getNpcData().getTemplate( data.characterNpcId ).getParameters()[ 'RoomIndex' ] as number
        let festivalId = roomIndex - 1
        let dawnData = SevenSignsFestival.getScoreData( SevenSignsSide.Dawn, festivalId )
        let duskData = SevenSignsFestival.getScoreData( SevenSignsSide.Dusk, festivalId )
        let topData = SevenSignsFestival.getTopHistoricalData( festivalId )

        await CharacterNamesCache.prefetchNames( [ ...dawnData.members, ...duskData.members, ...topData.members ] )

        let dawnPlayerNames : Array<string> = this.getScoreNames( dawnData.members )
        let duskPlayerNames : Array<string> = this.getScoreNames( duskData.members )
        let topPlayerNames : Array<string> = this.getScoreNames( topData.members )

        return DataManager.getHtmlData().getItem( this.getPath( 'ssq_main_event_acolyte_q0505_14.htm' ) )
            .replace( '%dawnDate%', dawnData.side === SevenSignsSide.Dawn ? moment( dawnData.date ).format( 'YYYY/M/D' ) : 'No record exists' )
            .replace( '%duskDate%', duskData.side === SevenSignsSide.Dusk ? moment( duskData.date ).format( 'YYYY/M/D' ) : 'No record exists' )
            .replace( '%topDate%', topData.side !== SevenSignsSide.None ? moment( topData.date ).format( 'YYYY/M/D' ) : 'No record exists' )
            .replace( '%dawnScore%', dawnData.score.toString() )
            .replace( '%duskScore%', duskData.score.toString() )
            .replace( '%topScore%', topData.score.toString() )
            .replace( '%dawnPlayerNames%', dawnPlayerNames.join( '' ) )
            .replace( '%duskPlayerNames%', duskPlayerNames.join( '' ) )
            .replace( '%topPlayerNames%', topPlayerNames.join( '' ) )
            .replace( '%topWinnerSide%', topData.side !== SevenSignsSide.None ? ( topData.side === SevenSignsSide.Dusk ? 'Dusk' : 'Dawn' ) : '' )
    }

    onInquiryFestival() : string {
        return this.cachedInquiryFestival.getResult()
    }

    refreshInquiryFestival() : string {
        let html = DataManager.getHtmlData().getItem( this.getPath( 'ssq_main_event_acolyte_q0505_15.htm' ) )

        for ( let festivalId = 1; festivalId < 6; festivalId++ ) {
            html = html.replace( `#Room${festivalId}#`, this.getRoomDescription( festivalId ) )

            let duskScore = SevenSignsFestival.getHighestScore( SevenSignsSide.Dusk, festivalId - 1 )
            html = html.replace( `#Score${festivalId}1#`, duskScore.toString() )
            let dawnScore = SevenSignsFestival.getHighestScore( SevenSignsSide.Dawn, festivalId - 1 )
            html = html.replace( `#Score${festivalId}2#`, dawnScore.toString() )

            let winnerId = `#Winner${festivalId}#`
            if ( duskScore > dawnScore ) {
                html = html.replace( winnerId, 'Dusk' )
            } else {
                if ( duskScore < dawnScore ) {
                    html = html.replace( winnerId, 'Dawn' )
                } else {
                    html = html.replace( winnerId, 'Draw' )
                }
            }
        }

        return html
    }

    async onInquiryPlayerScore( player: L2PcInstance, data: NpcGeneralEvent ) : Promise<string> {
        if ( !SevenSigns.isSealValidationPeriod() ) {
            return this.getPath( 'ssq_main_event_acolyte_q0505_23.htm' )
        }

        let expectedSide = DataManager.getNpcData().getTemplate( data.characterNpcId ).getParameters()[ 'part_type' ] === 'DAWN' ? SevenSignsSide.Dawn : SevenSignsSide.Dusk
        if ( SevenSigns.getPlayerSide( player.getObjectId() ) !== expectedSide ) {
            return this.getPath( 'ssq_main_event_acolyte_q0505_08.htm' )
        }

        if ( !SevenSigns.hasWinningSide( player.getObjectId() ) ) {
            return this.getPath( 'ssq_main_event_acolyte_q0505_24.htm' )
        }

        let roomIndex = DataManager.getNpcData().getTemplate( data.characterNpcId ).getParameters()[ 'RoomIndex' ] as number
        let festivalId = roomIndex - 1

        if ( !SevenSignsFestival.hasScore( player.getObjectId(), festivalId ) ) {
            return this.getPath( 'ssq_main_event_acolyte_q0505_17.htm' )
        }

        if ( SevenSignsFestival.hasReceivedReward( player.getObjectId(), festivalId ) ) {
            return this.getPath( 'ssq_main_event_acolyte_q0505_19.htm' )
        }

        let scoreData = SevenSignsFestival.getScoreData( SevenSigns.getPlayerSide( player.getObjectId() ), festivalId )
        let bonusAmount = Math.floor( SevenSignsFestival.getAccumulatedBonus( roomIndex - 1 ) / scoreData.members.length )

        player.setFame( player.getFame() + 1080 )

        if ( bonusAmount > 0 ) {
            await QuestHelper.rewardSingleItem( player, ItemTypes.AncientAdena, bonusAmount )
        }

        SevenSignsFestival.recordReceivedReward( player.getObjectId(), festivalId )

        return this.getPath( 'ssq_main_event_acolyte_q0505_18.htm' )
    }

    onInquiryStoneParticipation( player: L2PcInstance, data: NpcGeneralEvent, type: StoneParticipation ) : string {
        let party = player.getParty()
        if ( party ) {
            let expectedSide = DataManager.getNpcData().getTemplate( data.characterNpcId ).getParameters()[ 'part_type' ] === 'DAWN' ? SevenSignsSide.Dawn : SevenSignsSide.Dusk

            for ( const memberId of party.getMembers() ) {
                let memberPlayer = L2World.getPlayer( memberId )
                if ( !memberPlayer ) {
                    continue
                }

                if ( SevenSigns.getPlayerSide( memberId ) !== expectedSide ) {
                    return DataManager.getHtmlData().getItem( this.getPath( 'ssq_main_event_acolyte_q0505_01.htm' ) )
                        .replace( '#name#', memberPlayer.getName() )
                }

                if ( memberPlayer.getLevel() > this.getMinLevel( data.characterNpcId ) ) {
                    return DataManager.getHtmlData().getItem( this.getPath( this.getIncorrectLevelPath( data.characterNpcId ) ) )
                        .replace( '#name#', memberPlayer.getName() )
                }
            }
        }

        if ( !SevenSigns.isCompetitionPeriod() ) {
            return this.getPath( 'ssq_main_event_acolyte_q0505_02.htm' )
        }

        if ( !party || !party.isLeader( player ) ) {
            return this.getPath( 'ssq_main_event_acolyte_q0505_03.htm' )
        }

        let state = QuestStateCache.getQuestState( player.getObjectId(), 'Q00505_BloodOffering', true )
        state.setVariable( VariableName.ParticipationType, type )

        return this.getPath( 'ssq_main_event_acolyte_q0505_06.htm' )
    }

    onInquiryMenu( npcId: number ) : string {
        let minutes = SevenSignsFestival.getMinutesToNextFestival()
        return DataManager.getHtmlData().getItem( this.getNpcMenuPath( npcId ) )
            .replace( '#minute#', minutes > 0 ? minutes.toString() : 'mere second' )
    }

    onInquiryAccumulatedBonus() : string {
        let html = DataManager.getHtmlData().getItem( this.getPath( 'ssq_main_event_acolyte_q0505_27.htm' ) )

        for ( const level of AllFestivalLevels ) {
            html = html.replace( `#Room${level}#`, this.getRoomDescription( level ) )
                .replace( `#Money${level}#`, SevenSignsFestival.getAccumulatedBonus( level ).toString() )
        }

        return html
    }

    async onInquiry( player: L2PcInstance, operationId: number, data: NpcGeneralEvent ) : Promise<string> {
        switch ( operationId ) {
            case 1:
                return this.onInquiryParticipation( data )

            case 2:
                return this.onInquiryScoreRegistration( player, data )

            case 3:
                return this.onInquiryScores( data )

            case 4:
                return this.onInquiryFestival()

            case 6:
                return this.onInquiryPlayerScore( player, data )

            case 7:
                return this.onInquiryStoneParticipation( player, data, StoneParticipation.Blue )

            case 8:
                return this.onInquiryStoneParticipation( player, data, StoneParticipation.Green )

            case 9:
                return this.onInquiryStoneParticipation( player, data, StoneParticipation.Red )

            case 10:
                return this.onInquiryMenu( data.characterNpcId )

            case 11:
                return this.onInquiryAccumulatedBonus()

            case 12:
                let party = player.getParty()
                if ( party ) {
                    for ( const memberId of party.getMembers() ) {
                        if ( QuestStateCache.getAllActiveStatesSize( memberId ) >= MaxActiveQuests
                            && !QuestStateCache.hasQuestState( memberId, 'Q00635_IntoTheDimensionalRift' ) ) {

                            return DataManager.getHtmlData().getItem( this.getPath( 'ssq_main_event_acolyte_q0505_13e.htm' ) )
                                .replace( '#name#', CharacterNamesCache.getCachedNameById( memberId ) )
                        }
                    }

                    for ( const memberId of party.getMembers() ) {
                        let state = QuestStateCache.getQuestState( memberId, 'Q00635_IntoTheDimensionalRift', true )
                        state.setMemoState( 2 )
                        state.setMemoStateEx( 1, Date.now() )
                    }

                    await party.teleportMembersToCoordinates( -114796, -179334, -6752, 1000 )

                    return this.getPath( 'ssq_main_event_acolyte_q0505_13b.htm' )
                }

            case 13:
                let receivingParty = player.getParty()
                if ( receivingParty ) {
                    for ( const memberId of receivingParty.getMembers() ) {
                        let memberPlayer = L2World.getPlayer( memberId )
                        if ( memberPlayer ) {
                            await QuestHelper.rewardSingleItem( memberPlayer, DIMENSIONAL_FRAGMENT, 4 )
                        }
                    }

                    return this.getPath( 'ssq_main_event_acolyte_q0505_13c.htm' )
                }

            case 14:
                await teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, -114796, -179334, -6752 )
                return this.getPath( 'ssq_main_event_acolyte_q0505_13d.htm' )
        }
    }

    async onReturn( player: L2PcInstance, operationId: number, data: NpcGeneralEvent ) : Promise<string> {
        switch ( operationId ) {
            case 2:
                return this.getPath( 'ssq_main_event_acolyte_q0507_28.htm' )

            case 3:
                return this.onReturnToVillage( player, data )
        }
    }

    async onReturnToVillage( player: L2PcInstance, data: NpcGeneralEvent ) : Promise<string> {
        let bloodOfferingState = QuestStateCache.getQuestState( player.getObjectId(), 'Q00505_BloodOffering', false )
        if ( bloodOfferingState ) {
            await bloodOfferingState.exitQuest( true, false )
        }

        await QuestHelper.takeSingleItem( player, BLOOD_OF_OFFERING, -1 )

        let tutorialState = QuestStateCache.getQuestState( player.getObjectId(), 'Q00255_Tutorial', true )

        let rawValue = tutorialState.getMemoStateEx( 1 ) ?? 0
        let score = rawValue % 10000
        let chance = Math.random() < 0.5

        if ( score >= 95 && score < 195 ) {
            if ( dawnNpcIds.includes( data.characterNpcId ) ) {
                await ( chance ? teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, -80542, 150315, -3040 ) : teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, -80602, 150352, -3040 ) )
                return
            }

            await ( chance ? teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, -82340, 151575, -3120 ) : teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, -82392, 151584, -3120 ) )
            return
        }

        if ( score >= 195 && score < 295 ) {
            if ( dawnNpcIds.includes( data.characterNpcId ) ) {
                await ( chance ? teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, -13996, 121413, -2984 ) : teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, -13998, 121479, -2984 ) )
                return
            }

            await ( chance ? teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, -14727, 124002, -3112 ) : teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, -14727, 124012, -3112 ) )
            return
        }

        if ( score >= 295 && score < 395 ) {
            if ( dawnNpcIds.includes( data.characterNpcId ) ) {
                await ( chance ? teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, 16320, 142915, -2696 ) : teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, 16383, 142899, -2696 ) )
                return
            }

            await ( chance ? teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, 18501, 144673, -3056 ) : teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, 18523, 144624, -3056 ) )
            return
        }

        if ( score >= 395 && score < 495 ) {
            if ( dawnNpcIds.includes( data.characterNpcId ) ) {
                await ( chance ? teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, 83312, 149236, -3400 ) : teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, 83313, 149304, -3400 ) )
                return
            }
            await ( chance ? teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, 81572, 148580, -3464 ) : teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, 81571, 148641, -3464 ) )
            return
        }

        if ( score >= 495 && score < 595 ) {
            if ( dawnNpcIds.includes( data.characterNpcId ) ) {
                await ( chance ? teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, 111359, 220959, -3544 ) : teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, 111411, 220955, -3544 ) )
                return
            }

            await ( chance ? teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, 112441, 220149, -3544 ) : teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, 112452, 220204, -3592 ) )
            return
        }

        if ( score >= 595 && score < 695 ) {
            if ( dawnNpcIds.includes( data.characterNpcId ) ) {
                await ( chance ? teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, 83057, 53983, -1488 ) : teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, 83069, 54043, -1488 ) )
                return
            }

            await ( chance ? teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, 82842, 54613, -1520 ) : teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, 82791, 54616, -1520 ) )
            return
        }

        if ( score >= 695 && score < 795 ) {
            if ( dawnNpcIds.includes( data.characterNpcId ) ) {
                await ( chance ? teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, 146955, 26690, -2200 ) : teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, 147015, 26689, -2200 ) )
                return
            }

            await ( chance ? teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, 147528, 28899, -2264 ) : teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, 147528, 28962, -2264 ) )
            return
        }

        if ( score >= 795 && score < 895 ) {
            if ( dawnNpcIds.includes( data.characterNpcId ) ) {
                await ( chance ? teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, 115206, 74775, -2600 ) : teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, 115174, 74722, -2608 ) )
                return
            }

            await ( chance ? teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, 116651, 77512, -2688 ) : teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, 116597, 77539, -2688 ) )
            return
        }

        if ( score >= 995 && score < 1095 ) {
            if ( dawnNpcIds.includes( data.characterNpcId ) ) {
                await teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, 148326, -55533, -2776 )
            } else {
                await teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, 149968, -56645, -2976 )
            }

            return
        }

        if ( score >= 1095 && score < 1195 ) {
            if ( dawnNpcIds.includes( data.characterNpcId ) ) {
                await teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, 45605, -50360, -792 )
            } else {
                await teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, 44505, -48331, -792 )
            }

            return
        }

        if ( score >= 1195 && score < 1295 ) {
            if ( dawnNpcIds.includes( data.characterNpcId ) ) {
                await teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, 86730, -143148, -1336 )
            } else {
                await teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, 85048, -142046, -1536 )
            }

            return
        }

        let adjustmentScore = Math.floor( ( ( rawValue - score ) + 5 ) / 10000 )
        let location = adjustmentScoreMap[ adjustmentScore ]
        if ( location ) {
            await player.teleportToLocationWithOffset( location )
            return
        }

        return this.getPath( 'ssq_main_event_acolyte_q0507_29.htm' )
    }

    async onApproachedForTalk( data: NpcApproachedForTalkEvent ): Promise<string> {
        return this.onInquiryMenu( data.characterNpcId )
    }

    async onSkillCastFinished( data: NpcFinishedSkillEvent ) : Promise<void> {
        if ( data.targetInstanceType !== InstanceType.L2PcInstance ) {
            return
        }

        let template = DataManager.getNpcData().getTemplate( data.attackerNpcId )
        if ( !template ) {
            return
        }

        let parameters = template.getParameters()
        let x = parameters[ 'SibylPosX' ] as number
        let y = parameters[ 'SibylPosY' ] as number
        let z = parameters[ 'SibylPosZ' ] as number

        if ( !Number.isInteger( x ) || !Number.isInteger( y ) || !Number.isInteger( z ) ) {
            return
        }

        let party = PlayerGroupCache.getParty( data.targetId )
        if ( !party ) {
            return
        }

        return party.teleportMembersToCoordinates( x,y,z )
    }

    exitQuest( playerId: number ) : Promise<void> {
        let state = QuestStateCache.getQuestState( playerId, 'Q00505_BloodOffering', false )
        if ( state ) {
            return state.exitQuest( true, false )
        }
    }

    async takeStonesFromPlayer( player: L2PcInstance, itemId: number, amount: number ) : Promise<string> {
        if ( !QuestHelper.hasQuestItemCount( player, itemId, amount ) ) {
            return this.getPath( 'ssq_main_event_acolyte_q0505_21a.htm' )
        }

        await QuestHelper.takeSingleItem( player, itemId, amount )
    }

    processPlayerStoneParticipation( player: L2PcInstance, state: QuestState, npcId: number ) : Promise<string> {
        let stoneType = state.getVariable( VariableName.ParticipationType ) as StoneParticipation

        switch ( stoneType ) {
            case StoneParticipation.Blue:
                return this.takeStonesFromPlayer( player, SevenSignsValues.SEAL_STONE_BLUE_ID, this.getBlueStoneCount( npcId ) )

            case StoneParticipation.Green:
                return this.takeStonesFromPlayer( player, SevenSignsValues.SEAL_STONE_GREEN_ID, this.getGreenStoneCount( npcId ) )

            case StoneParticipation.Red:
                return this.takeStonesFromPlayer( player, SevenSignsValues.SEAL_STONE_RED_ID, this.getRedStoneCount( npcId ) )
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )

        if ( !player ) {
            return
        }

        if ( player.getInventory().getSize() >= ( player.getQuestInventoryLimit() * QUEST_LIMIT )
            || player.getInventory().getSize() >= ( player.getInventoryLimit() * WEIGHT_LIMIT )
            || player.getCurrentLoad() >= ( player.getMaxLoad() * WEIGHT_LIMIT ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CAN_PROCEED_ONLY_WHEN_THE_INVENTORY_WEIGHT_IS_BELOW_80_PERCENT_AND_THE_QUANTITY_IS_BELOW_90_PERCENT ) )
            return
        }

        let party = player.getParty()
        if ( !party ) {
            return
        }

        let template = DataManager.getNpcData().getTemplate( data.characterNpcId )
        let expectedSide = template.getParameters()[ 'part_type' ] === 'DAWN' ? SevenSignsSide.Dawn : SevenSignsSide.Dusk

        for ( const memberId of party.getMembers() ) {
            let memberPlayer = L2World.getPlayer( memberId )
            if ( !memberPlayer ) {
                continue
            }


            await QuestHelper.takeSingleItem( memberPlayer, BLOOD_OF_OFFERING, -1 )

            if ( SevenSigns.getPlayerSide( memberId ) !== expectedSide ) {
                await this.exitQuest( memberId )

                return DataManager.getHtmlData().getItem( this.getPath( 'ssq_main_event_acolyte_q0505_01.htm' ) )
                    .replace( '#name#', memberPlayer.getName() )
            }

            if ( memberPlayer.getLevel() > this.getMinLevel( data.characterNpcId ) ) {
                await this.exitQuest( memberId )

                return DataManager.getHtmlData().getItem( this.getPath( this.getIncorrectLevelPath( data.characterNpcId ) ) )
                    .replace( '#name#', memberPlayer.getName() )
            }

            if ( QuestStateCache.getAllActiveStatesSize( memberId ) >= MaxActiveQuests ) {
                await this.exitQuest( memberId )

                return DataManager.getHtmlData().getItem( this.getPath( 'ssq_main_event_acolyte_q0505_04e.htm' ) )
                    .replace( '#name#', memberPlayer.getName() )
            }

            if ( memberPlayer.getSummon() ) {
                await this.exitQuest( memberId )

                return this.getPath( 'ssq_main_event_acolyte_q0505_31.htm' )
            }
        }

        let minutes = GeneralHelper.getCurrentMinute()
        if ( ( minutes >= 0 && minutes < 18 )
            || ( minutes >= 20 && minutes < 38 )
            || ( minutes >= 40 && minutes < 58 ) ) {
            await this.exitQuest( data.playerId )

            return this.getPath( 'ssq_main_event_acolyte_q0505_22.htm' )
        }

        if ( ( party.getMembers().length < ConfigManager.sevenSigns.getFestivalMinPlayer() ) && !QuestHelper.hasQuestItem( player, THE_STAFF ) ) {
            await this.exitQuest( data.playerId )

            return this.getPath( 'ssq_main_event_acolyte_q0505_30.htm' )
        }

        if ( !SevenSigns.isCompetitionPeriod() || !party.isLeader( player ) ) {
            return
        }

        let roomIndex = template.getParameters()[ 'RoomIndex' ] as number
        let festivalId = roomIndex - 1
        if ( !SevenSignsFestival.isRoomOccupied( expectedSide, festivalId ) ) {
            return this.getPath( 'ssq_main_event_acolyte_q0505_05.htm' )
        }

        let state = QuestStateCache.getQuestState( data.playerId, 'Q00505_BloodOffering', true )
        let result : string = await this.processPlayerStoneParticipation( player, state, data.characterNpcId )
        if ( result ) {
            return result
        }

        for ( const memberId of party.getMembers() ) {
            let state = QuestStateCache.getQuestState( memberId, 'Q00505_BloodOffering', true )

            state.setVariable( VariableName.LastContributionTime, Date.now() )
        }

        state.startQuest()
        state.setVariable( VariableName.LastRoom, festivalId )

        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        if ( !npc ) {
            return
        }

        if ( npc.canCastSkill( SevenSignsSkill.getSkill() ) ) {
            npc.setTarget( player )
            await npc.doCast( SevenSignsSkill.getSkill() )
        }

        SevenSignsFestival.addPartyToEventRoom( expectedSide, festivalId, party )
        SevenSignsFestival.increaseBonus( festivalId, this.getRoomBonus( data.characterNpcId ) )
    }
}