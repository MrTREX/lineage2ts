import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { DataManager } from '../../data/manager'
import { QuestHelper } from '../helpers/QuestHelper'
import { NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'

const npcIds : Array<number> = [
    35099, // Warehouse Keeper (Gludio)
    35141, // Warehouse Keeper (Dion)
    35183, // Warehouse Keeper (Giran)
    35225, // Warehouse Keeper (Oren)
    35273, // Warehouse Keeper (Aden)
    35315, // Warehouse Keeper (Inadril)
    35362, // Warehouse Keeper (Goddard)
    35508, // Warehouse Keeper (Rune)
    35554, // Warehouse Keeper (Schuttgart)
]

const BLOOD_OATH = 9910
const BLOOD_ALLIANCE = 9911

export class CastleWarehouse extends ListenerLogic {
    constructor() {
        super( 'CastleWarehouse', 'listeners/npcs/CastleWarehouse.ts' )
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    getApproachedForTalkIds(): Array<number> {
        return npcIds
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/CastleWarehouse'
    }

    async onApproachedForTalk(): Promise<string> {
        return this.getPath( 'warehouse-01.html' )
    }

    async onNpcEvent( data : NpcGeneralEvent ): Promise<string> {

        let event = data.eventName
        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        let player = L2World.getPlayer( data.playerId )

        switch ( event ) {
            case 'warehouse-01.html':
            case 'warehouse-02.html':
            case 'warehouse-03.html':
                return this.getPath( event )

            case 'warehouse-04.html':
                if ( !npc.isLord( player ) ) {
                    return this.getPath( 'warehouse-no.html' )
                }

                return DataManager.getHtmlData().getItem( this.getPath( 'warehouse-04.html' ) )
                        .replace( /%blood%/g, player.getClan().getBloodAllianceCount().toString() )

            case 'Receive':
                if ( !npc.isLord( player ) ) {
                    return this.getPath( 'warehouse-no.html' )
                }

                if ( player.getClan().getBloodAllianceCount() === 0 ) {
                    return this.getPath( 'warehouse-05.html' )
                }

                await QuestHelper.giveSingleItem( player, BLOOD_ALLIANCE, player.getClan().getBloodAllianceCount() )
                await player.getClan().resetBloodAllianceCount()
                return this.getPath( 'warehouse-06.html' )

            case 'Exchange':
                if ( !npc.isLord( player ) ) {
                    return this.getPath( 'warehouse-no.html' )
                }

                if ( !QuestHelper.hasQuestItems( player, BLOOD_ALLIANCE ) ) {
                    return this.getPath( 'warehouse-08.html' )
                }

                await QuestHelper.takeSingleItem( player, BLOOD_ALLIANCE, 1 )
                await QuestHelper.giveSingleItem( player, BLOOD_OATH, 30 )
                return this.getPath( 'warehouse-07.html' )
        }
    }
}