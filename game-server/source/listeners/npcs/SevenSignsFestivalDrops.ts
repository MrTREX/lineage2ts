import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcSpawnEvent } from '../../gameService/models/events/EventType'
import { PlayerGroupCache } from '../../gameService/cache/PlayerGroupCache'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { CategoryType } from '../../gameService/enums/CategoryType'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { QuestHelper } from '../helpers/QuestHelper'
import { SevenSignsFestival } from '../../gameService/directives/SevenSignsFestival'
import { DataManager } from '../../data/manager'
import { SevenSignsSide } from '../../gameService/values/SevenSignsValues'
import { AITraitCache } from '../../gameService/cache/AITraitCache'
import { AIIntent } from '../../gameService/aicontroller/enums/AIIntent'
import { RunAwayTrait } from '../../gameService/aicontroller/traits/npc/RunAwayTrait'
import { TraitSettings } from '../../gameService/aicontroller/interface/IAITrait'

const npcIds: Array<number> = [
    18099, 18101, 18102, 18103, 18104, 18105, 18106, 18107, 18108, 18317,
    18089, 18091, 18092, 18093, 18094, 18095, 18096, 18097, 18098, 18315,
    18079, 18081, 18082, 18083, 18084, 18085, 18086, 18087, 18088, 18313,
    18069, 18071, 18072, 18073, 18074, 18075, 18076, 18077, 18078, 18311,
    18059, 18061, 18062, 18063, 18064, 18065, 18066, 18067, 18068, 18309,
    18049, 18051, 18052, 18053, 18054, 18055, 18056, 18057, 18058, 18307,
    18039, 18041, 18042, 18043, 18044, 18045, 18046, 18047, 18048, 18305,
    18029, 18031, 18032, 18033, 18034, 18035, 18036, 18037, 18038, 18303,
    18019, 18021, 18022, 18023, 18024, 18025, 18026, 18027, 18028, 18301,
    18009, 18011, 18012, 18013, 18014, 18015, 18016, 18017, 18018, 18299
]

const higherAmountNpcIds: Set<number> = new Set<number>( [
    18014,
    18018,
    18025,
    18028,
    18034,
    18035,
    18038,
    18044,
    18045,
    18048,
    18054,
    18055,
    18058,
    18064,
    18065,
    18068,
    18074,
    18075,
    18078,
    18084,
    18085,
    18088,
    18094,
    18095,
    18098,
    18104,
    18105,
    18108
] )

const useHigherAmountNpcIds: Set<number> = new Set<number>( [
    18012,
    18014,
    18022,
    18024,
    18032,
    18034,
    18042,
    18044,
    18052,
    18054,
    18062,
    18064,
    18072,
    18074,
    18082,
    18084,
    18092,
    18094,
    18102,
    18104
] )

const useLowerAmountNpcIds: Set<number> = new Set<number>( [
    18017,
    18018,
    18027,
    18028,
    18037,
    18038,
    18047,
    18048,
    18057,
    18058,
    18067,
    18068,
    18077,
    18078,
    18087,
    18088,
    18097,
    18098,
    18107,
    18108
] )

const treasureBoxIds: Array<number> = [
    18118, 18117, 18116, 18115, 18114,
    18113, 18112, 18111, 18110, 18109
]

const treasureBoxNpcIds: Set<number> = new Set<number>( treasureBoxIds )

const enum Values {
    ItemId = 5901,
    DefaultAmount = 3,
    HighestAmount = 18,
    HigherAmount = 13,
    LowerAmount = 7,
    LowestAmount = 2,
    TreasureAmount = 37,
    TreasureBoxLifetime = 120000
}

const enum Event {
    DespawnTreasureBox = 'dtb'
}

export class SevenSignsFestivalDrops extends ListenerLogic {
    constructor() {
        super( 'SevenSignsFestivalDrops', 'listeners/npcs/SevenSignsFestivalDrops.ts' )
    }

    async initialize(): Promise<void> {
        let traits : TraitSettings = {
            ...AITraitCache.createEmptyTraits(),
            [ AIIntent.WAITING ]: new RunAwayTrait( 100, Values.TreasureBoxLifetime )
        }

        AITraitCache.setManyNpcTraits( treasureBoxIds, traits )
    }

    getSpawnIds(): Array<number> {
        return treasureBoxIds
    }

    getAttackableKillIds(): Array<number> {
        return [
            ...npcIds,
            ...treasureBoxIds
        ]
    }

    async onSpawnEvent( data: NpcSpawnEvent ): Promise<void> {
        this.startQuestTimer( Event.DespawnTreasureBox, Values.TreasureBoxLifetime, data.characterId )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        this.stopQuestTimer( Event.DespawnTreasureBox, data.targetId )

        let party = PlayerGroupCache.getParty( data.playerId )
        if ( !party ) {
            return
        }

        let state = QuestStateCache.getQuestState( party.getLeaderObjectId(), 'Q00505_BloodOffering', false )
        if ( !state ) {
            return
        }

        let playerLeader = party.getLeader()
        if ( !playerLeader ) {
            return
        }

        let lastAttacker = L2World.getObjectById( data.attackerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        /*
            Penalty starting amount of dropped item is calculated from both if pet has killed the mob (summon npcs are not
            teleported into room) and if party composition is still the same one as entered festival room (if leader changes,
            or possibly other players quit party, penalty is applied).
         */
        let applyPenalty = lastAttacker.isPet()
        if ( !applyPenalty ) {
            let side = DataManager.getNpcData().getTemplate( data.npcId ).getParameters()[ 'part_type' ] === 'DAWN' ? SevenSignsSide.Dawn : SevenSignsSide.Dusk
            let festivalId = DataManager.getNpcData().getTemplate( data.npcId ).getParameters()[ 'RoomIndex' ] as number - 1

            applyPenalty = SevenSignsFestival.getRoomPartyId( side, festivalId ) !== party.getId()
        }

        let amount = applyPenalty ? this.getPenalizedStartingAmount( data.npcId ) : this.getNormalStartingAmount( data.npcId )

        /*
            Player leaders of wizard classes are penalized significantly more than others,
            so care must be taken to arrange leader before coming to festival room.
         */
        let distance = playerLeader.calculateDistance( npc, true )
        if ( playerLeader.isInCategory( CategoryType.WIZARD_GROUP ) ) {
            if ( distance < 40 ) {
                amount = Math.floor( amount * 1.3 )
            }
        } else if ( distance < 80 ) {
            amount = Math.floor( amount * 1.3 )
        }

        let chance = Math.random() > 0.5
        if ( playerLeader.isInCategory( CategoryType.WIZARD_GROUP ) ) {
            amount = amount + Math.floor( amount * ( chance ? 0.07 : -0.15 ) )
        } else {
            amount = amount + Math.floor( amount * ( chance ? -0.07 : 0.15 ) )
        }

        let packet = new SystemMessageBuilder( SystemMessageIds.LEADER_OBTAINED_S2_OF_S1 )
            .addItemNameWithId( Values.ItemId )
            .addNumber( amount )

        party.broadcastPacket( packet.getBuffer() )

        await QuestHelper.rewardSingleQuestItem( playerLeader, Values.ItemId, amount, data.isChampion )
    }

    /*
        Depending on what npc has died, starting amount can be vastly different. For example,
        half of "heavy_trp" (in npc reference id) npcs have ability to start with 18 items, while
        majority of npcs default to just three starting items. Treasure boxes though have highest
        starting amount of 37.
     */
    private getNormalStartingAmount( npcId: number ) : number {
        if ( treasureBoxNpcIds.has( npcId ) ) {
            return Values.TreasureAmount
        }

        if ( !useHigherAmountNpcIds.has( npcId ) ) {
            if ( useLowerAmountNpcIds.has( npcId ) && higherAmountNpcIds.has( npcId ) ) {
                return Values.LowerAmount
            }

            return Values.DefaultAmount
        }

        return higherAmountNpcIds.has( npcId ) ? Values.HighestAmount : Values.HigherAmount
    }

    private getPenalizedStartingAmount( npcId: number ) : number {
        return useLowerAmountNpcIds.has( npcId ) ? Values.LowestAmount : Values.DefaultAmount
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        if ( data.eventName !== Event.DespawnTreasureBox ) {
            return
        }

        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        if ( !npc || npc.isDead() ) {
            return
        }

        npc.getNpcSpawn().startDespawn()
    }
}