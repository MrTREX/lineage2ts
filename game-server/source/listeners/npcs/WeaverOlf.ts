import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestHelper } from '../helpers/QuestHelper'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { ItemTypes } from '../../gameService/values/InventoryValues'
import aigle from 'aigle'
import _ from 'lodash'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const npcIds: Array<number> = [
    32610, // Olf Kanore
    32612, // Olf Adams
]

const unsealPrice: Array<number> = [
    3200,
    11800,
    26500,
    136600,
]

const chances: Array<number> = [
    1, // top
    10, // high
    40, // mid
    100, // low
]

const allPins: Array<Array<number>> = [
    [
        13898, // Sealed Magic Pin (C-Grade)
        13905, // Top-Grade Magic Pin (C-Grade)
        13904, // High-Grade Magic Pin (C-Grade)
        13903, // Mid-Grade Magic Pin (C-Grade)
        13902, // Low-Grade Magic Pin (C-Grade)
    ],
    [
        13899, // Sealed Magic Pin (B-Grade)
        13909, // Top-Grade Magic Pin (B-Grade)
        13908, // High-Grade Magic Pin (B-Grade)
        13907, // Mid-Grade Magic Pin (B-Grade)
        13906, // Low-Grade Magic Pin (B-Grade)
    ],
    [
        13900, // Sealed Magic Pin (A-Grade)
        13913, // Top-Grade Magic Pin (A-Grade)
        13912, // High-Grade Magic Pin (A-Grade)
        13911, // Mid-Grade Magic Pin (A-Grade)
        13910, // Low-Grade Magic Pin (A-Grade)
    ],
    [
        13901, // Sealed Magic Pin (S-Grade)
        13917, // Top-Grade Magic Pin (S-Grade)
        13916, // High-Grade Magic Pin (S-Grade)
        13915, // Mid-Grade Magic Pin (S-Grade)
        13914, // Low-Grade Magic Pin (S-Grade)
    ],
]

const allPouches: Array<Array<number>> = [
    [
        13918, // Sealed Magic Pouch (C-Grade)
        13925, // Top-Grade Magic Pouch (C-Grade)
        13924, // High-Grade Magic Pouch (C-Grade)
        13923, // Mid-Grade Magic Pouch (C-Grade)
        13922, // Low-Grade Magic Pouch (C-Grade)
    ],
    [
        13919, // Sealed Magic Pouch (B-Grade)
        13929, // Top-Grade Magic Pouch (B-Grade)
        13928, // High-Grade Magic Pouch (B-Grade)
        13927, // Mid-Grade Magic Pouch (B-Grade)
        13926, // Low-Grade Magic Pouch (B-Grade)
    ],
    [
        13920, // Sealed Magic Pouch (A-Grade)
        13933, // Top-Grade Magic Pouch (A-Grade)
        13932, // High-Grade Magic Pouch (A-Grade)
        13931, // Mid-Grade Magic Pouch (A-Grade)
        13930, // Low-Grade Magic Pouch (A-Grade)
    ],
    [
        13921, // Sealed Magic Pouch (S-Grade)
        13937, // Top-Grade Magic Pouch (S-Grade)
        13936, // High-Grade Magic Pouch (S-Grade)
        13935, // Mid-Grade Magic Pouch (S-Grade)
        13934, // Low-Grade Magic Pouch (S-Grade)
    ],
]

const clipOrnaments: Array<Array<number>> = [
    [
        14902, // Sealed Magic Rune Clip (A-Grade)
        14909, // Top-level Magic Rune Clip (A-Grade)
        14908, // High-level Magic Rune Clip (A-Grade)
        14907, // Mid-level Magic Rune Clip (A-Grade)
        14906, // Low-level Magic Rune Clip (A-Grade)
    ],
    [
        14903, // Sealed Magic Rune Clip (S-Grade)
        14913, // Top-level Magic Rune Clip (S-Grade)
        14912, // High-level Magic Rune Clip (S-Grade)
        14911, // Mid-level Magic Rune Clip (S-Grade)
        14910, // Low-level Magic Rune Clip (S-Grade)
    ],
    [
        14904, // Sealed Magic Ornament (A-Grade)
        14917, // Top-grade Magic Ornament (A-Grade)
        14916, // High-grade Magic Ornament (A-Grade)
        14915, // Mid-grade Magic Ornament (A-Grade)
        14914, // Low-grade Magic Ornament (A-Grade)
    ],
    [
        14905, // Sealed Magic Ornament (S-Grade)
        14921, // Top-grade Magic Ornament (S-Grade)
        14920, // High-grade Magic Ornament (S-Grade)
        14919, // Mid-grade Magic Ornament (S-Grade)
        14918, // Low-grade Magic Ornament (S-Grade)
    ],
]

export class WeaverOlf extends ListenerLogic {
    constructor() {
        super( 'WeaverOlf', 'listeners/npcs/WeaverOlf.ts' )
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/WeaverOlf'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let event = data.eventName
        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        let player = L2World.getPlayer( data.playerId )

        if ( event.includes( '_grade_' ) ) {
            let grade = _.parseInt( event.substring( 0, 1 ) )
            let price
            let itemIds: Array<number>

            if ( event.endsWith( '_pin' ) ) {
                price = unsealPrice[ grade ]
                itemIds = allPins[ grade ]
            }

            if ( event.endsWith( '_pouch' ) ) {
                price = unsealPrice[ grade ]
                itemIds = allPouches[ grade ]
            }

            if ( event.endsWith( '_clip' ) ) {
                price = unsealPrice[ grade ]
                itemIds = clipOrnaments[ grade - 2 ]
            }

            if ( event.endsWith( '_ornament' ) ) {
                price = unsealPrice[ grade ]
                itemIds = clipOrnaments[ grade ]
            }

            if ( price && itemIds ) {
                if ( QuestHelper.hasQuestItems( player, itemIds[ 0 ] ) ) {
                    if ( player.getAdena() > price ) {
                        await QuestHelper.takeSingleItem( player, ItemTypes.Adena, price )
                        await QuestHelper.takeSingleItem( player, itemIds[ 0 ], 1 )

                        let value = _.random( 200 )
                        await aigle.resolve( chances ).each( ( chance: number, currentIndex: number ) => {
                            if ( value <= chance ) {
                                return QuestHelper.giveSingleItem( player, itemIds[ currentIndex + 1 ], 1 )
                            }
                        } )

                        BroadcastHelper.broadcastNpcSayStringId( npc,
                                NpcSayType.NpcAll,
                                NpcStringIds.WHAT_A_PREDICAMENT_MY_ATTEMPTS_WERE_UNSUCCESSFUL )

                        return
                    }

                    return this.getPath( `${ npc.getId() }-low.htm` )
                }

                return this.getPath( `${ npc.getId() }-no.htm` )
            }

            return
        }

        return this.getPath( event )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        return this.getPath( `${ npc.getId() }-1.htm` )
    }
}