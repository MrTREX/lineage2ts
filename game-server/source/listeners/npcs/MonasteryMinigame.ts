import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { SkillHolder } from '../../gameService/models/holders/SkillHolder'
import { Location } from '../../gameService/models/Location'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2World } from '../../gameService/L2World'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'

const SUMIEL = 32758
const BURNER = 18913
const TREASURE_BOX = 18911

const UNLIT_TORCHLIGHT = 15540
const TORCHLIGHT = 15485
const SKILL_TORCH_LIGHT = 9059

const TRIGGER_MIRAGE = new SkillHolder( 5144 )
const TELEPORT1 = new Location( 113187, -85388, -3424, 0 )
const TELEPORT2 = new Location( 118833, -80589, -2688, 0 )

const TIMER_INTERVAL = 3
const MAX_ATTEMPTS = 3

/*
    TODO: implement whole thing
 */
export class MonasteryMinigame extends ListenerLogic {
    rooms: Array<MinigameRoom> = []

    constructor() {
        super( 'MonasteryMinigame', 'listeners/npcs/MonasteryMinigame.ts' )
    }

    getQuestStartIds(): Array<number> {
        return [ SUMIEL ]
    }

    getApproachedForTalkIds(): Array<number> {
        return [ SUMIEL ]
    }

    getTalkIds(): Array<number> {
        return [ SUMIEL ]
    }

    getSpawnIds(): Array<number> {
        return [ SUMIEL, TREASURE_BOX ]
    }
}

// TODO : implement me
class MinigameRoom {
    burnerIds: Array<number> = []
    managerId: number
    participantId: number
    isStarted: boolean = false
    attempts: number = 1
    currentPot: number = 0
    order: Array<number> = []

    getBurnerPosition( npc: L2Npc ) : number {
        return this.burnerIds.indexOf( npc.getObjectId() )
    }

    burnAll() {
        this.burnerIds.forEach( ( objectId: number ) => {
            let npc: L2Npc = L2World.getObjectById( objectId ) as L2Npc
            if ( npc ) {
                npc.setDisplayEffect( 1 )
                npc.setIsRunning( false )
            }
        } )
    }

    getParticipant() : L2PcInstance {
        return L2World.getPlayer( this.participantId )
    }

    reset() {

    }
}