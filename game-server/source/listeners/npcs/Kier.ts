import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { NpcApproachedForTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'

const npcId: number = 32022

export class Kier extends ListenerLogic {
    constructor() {
        super( 'Kier', 'listeners/npcs/Kier.ts' )
    }

    getApproachedForTalkIds(): Array<number> {
        return [ npcId ]
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/Kier'
    }

    async onApproachedForTalk( data : NpcApproachedForTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, 'Q00115_TheOtherSideOfTruth' )
        if ( !state ) {
            return this.getPath( '32022-02.html' )
        }

        if ( !state.isCompleted() ) {
            return this.getPath( '32022-01.html' )
        }

        let otherState: QuestState = QuestStateCache.getQuestState( data.playerId, 'Q10283_RequestOfIceMerchant' )
        if ( otherState ) {
            if ( otherState.isMemoState( 2 ) ) {
                return this.getPath( '32022-03.html' )
            }

            if ( otherState.isCompleted() ) {
                return this.getPath( '32022-04.html' )
            }
        }
    }
}