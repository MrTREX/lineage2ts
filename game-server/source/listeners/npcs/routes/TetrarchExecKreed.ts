import { NpcRouteListener } from '../../helpers/NpcRouteListener'

export class TetrarchExecKreed extends NpcRouteListener {
    constructor() {
        super( 'TetrarchExecKreed', 'listeners/npcs/routes/TetrarchExecKreed.ts' )
    }

    stringIdMap: Record<number, number> = {
        3: 1010214
    }

    getNpcIds(): Array<number> {
        return [ 31361 ]
    }

    isRunningRoute(): boolean {
        return true
    }
}