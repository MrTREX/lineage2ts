import { NpcRouteListener } from '../../helpers/NpcRouteListener'

export class BardCasiel extends NpcRouteListener {
    constructor() {
        super( 'BardCasiel', 'listeners/npcs/routes/BardCasiel.ts' )
    }

    stringIdMap: Record<number, number> = {
        5: 1010210,
        9: 1010211
    }

    getNpcIds(): Array<number> {
        return [ 31358 ]
    }

    isRunningRoute(): boolean {
        return false
    }
}