import { NpcRouteListener } from '../../helpers/NpcRouteListener'

export class ProphetRune extends NpcRouteListener {
    constructor() {
        super( 'ProphetRune', 'listeners/npcs/routes/ProphetRune.ts' )
    }

    stringIdMap: Record<number, number> = {
        1: 1010223,
        3: 1010224,
        4: 1010225
    }

    getNpcIds(): Array<number> {
        return [ 4312 ]
    }

    isRunningRoute(): boolean {
        return false
    }
}