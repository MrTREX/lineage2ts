import { NpcRouteListener } from '../../helpers/NpcRouteListener'

export class ProphetAden extends NpcRouteListener {
    constructor() {
        super( 'ProphetAden', 'listeners/npcs/routes/ProphetAden.ts' )
    }

    stringIdMap: Record<number, number> = {
        1: 1010227,
        2: 1010221,
        4: 1010222
    }

    getNpcIds(): Array<number> {
        return [ 4311 ]
    }

    isRunningRoute(): boolean {
        return false
    }
}