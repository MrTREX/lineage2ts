import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { QuestHelper } from '../helpers/QuestHelper'
import { NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import aigle from 'aigle'
import _ from 'lodash'
import { createItemDefinition, ItemDefinition } from '../../gameService/interface/ItemDefinition'

const npcId: number = 30098 // Alexandria, Armor Merchant

const requiredItems: Array<ItemDefinition> = [
    createItemDefinition( 57, 7500000 ),
    createItemDefinition( 5094, 50 ),
    createItemDefinition( 6471, 25 ),
    createItemDefinition( 9814, 4 ),
    createItemDefinition( 9815, 3 ),
    createItemDefinition( 9816, 5 ),
    createItemDefinition( 9817, 5 ),
]

const itemMap: { [ key: string ]: Array<Array<number>> } = {
    littleAngel: [
        [ 10315, 600, 1, 10408 ], // itemId, chance, amount, additionalItemId
        [ 10316, 10 ], // itemId, chance
        [ 10317, 10 ],
        [ 10318, 5 ],
        [ 10319, 5 ],
        [ 10320, 370 ],
    ],
    littleDevil: [
        [ 10321, 600, 1, 10408 ], // itemId, chance, amount, additionalItemId
        [ 10322, 10 ],
        [ 10323, 10 ],
        [ 10324, 5 ],
        [ 10325, 5 ],
        [ 10326, 370 ],
    ],
}

export class Alexandria extends ListenerLogic {

    constructor() {
        super( 'Alexandria', 'listeners/npcs/Alexandria.ts' )
    }

    getApproachedForTalkIds(): Array<number> {
        return [ npcId ]
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/Alexandria'
    }

    getQuestStartIds(): Array<number> {
        return [ npcId ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let event = data.eventName
        let player = L2World.getPlayer( data.playerId )

        if ( event === '30098-02.html' ) {
            return this.getPath( event )
        }

        let items: Array<Array<number>> = itemMap[ event ]

        if ( !items ) {
            return
        }

        let staticChance = _.random( 1000 )
        let chanceTwo = 0
        let chanceThree = 0

        let outcome: string
        await aigle.resolve( items ).eachSeries( async ( itemDefinition: Array<number> ) => {
            let [ itemId, itemChance, amount, additionalItemId ] = itemDefinition
            chanceThree += itemChance

            if ( ( chanceTwo <= staticChance ) && ( staticChance < chanceThree ) ) {
                if ( await QuestHelper.removeAllItems( player, requiredItems ) ) {
                    await QuestHelper.giveSingleItem( player, itemId, amount )

                    if ( additionalItemId ) {
                        await QuestHelper.giveSingleItem( player, additionalItemId, 1 )
                        outcome = this.getPath( '30098-03a.html' )

                        return false
                    }

                    outcome = this.getPath( '30098-03.html' )
                    return false
                }

                outcome = this.getPath( '30098-04.html' )
                return false
            }

            chanceTwo += itemChance
        } )

        return outcome
    }
}

