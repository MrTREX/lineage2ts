import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { QuestHelper } from '../helpers/QuestHelper'
import { NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import _ from 'lodash'

const npcIds: Array<number> = [
    31690,
    31769,
    31770,
    31771,
    31772
]

const WINGS_OF_DESTINY_CIRCLET = 6842
const weaponIds : Array<number> = [
    6611, // Infinity Blade
    6612, // Infinity Cleaver
    6613, // Infinity Axe
    6614, // Infinity Rod
    6615, // Infinity Crusher
    6616, // Infinity Scepter
    6617, // Infinity Stinger
    6618, // Infinity Fang
    6619, // Infinity Bow
    6620, // Infinity Wing
    6621, // Infinity Spear
    9388, // Infinity Rapier
    9389, // Infinity Sword
    9390, // Infinity Shooter
]

export class MonumentOfHeroes extends ListenerLogic {
    constructor() {
        super( 'MonumentOfHeroes', 'listeners/npcs/MonumentOfHeroes.ts' )
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onNpcEvent( data : NpcGeneralEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'HeroWeapon':
                if ( player.isHero() ) {
                    return this.getPath( QuestHelper.hasAtLeastOneQuestItem( player, ...weaponIds ) ? 'already_have_weapon.htm' : 'weapon_list.htm' )
                }

                return this.getPath( 'no_hero_weapon.htm' )

            case 'HeroCirclet':
                if ( player.isHero() ) {
                    if ( !QuestHelper.hasQuestItems( player, WINGS_OF_DESTINY_CIRCLET ) ) {
                        await QuestHelper.giveSingleItem( player, WINGS_OF_DESTINY_CIRCLET, 1 )
                    }

                    return this.getPath( 'already_have_circlet.htm' )
                }

                return this.getPath( 'no_hero_circlet.htm' )

            default:
                let weaponId = _.parseInt( data.eventName )
                if ( weaponIds.includes( weaponId ) ) {
                    await QuestHelper.giveSingleItem( player, weaponId, 1 )
                }

                break
        }
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/MonumentOfHeroes'
    }
}