import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcSpawnEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'

export class CorpseOfDeadman extends ListenerLogic {
    constructor() {
        super( CorpseOfDeadman.prototype.name, '' )
    }

    getSpawnIds(): Array<number> {
        return [
            18119
        ]
    }

    async onSpawnEvent( data: NpcSpawnEvent ): Promise<void> {
        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        await npc.doDie( npc )
    }
}