import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { NpcApproachedForTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import _ from 'lodash'

const ASHAS = 31377 // Hierarch
const NARAN = 31378 // Messenger
const UDAN = 31379 // Buffer
const DIYABU = 31380 // Grocer
const HAGOS = 31381 // Warehouse Keeper
const SHIKON = 31382 // Trader
const TERANU = 31383 // Teleporter

const SEED = 7187

const marks: Array<number> = [
    7221, // Mark of Varka's Alliance - Level 1
    7222, // Mark of Varka's Alliance - Level 2
    7223, // Mark of Varka's Alliance - Level 3
    7224, // Mark of Varka's Alliance - Level 4
    7225, // Mark of Varka's Alliance - Level 5
]

// TODO : implement ai controller support to cast buffs
const buffs: Array<[ number, number ]> = [
    [ 4359, 2 ], // Focus: Requires 2 Nepenthese Seeds
    [ 4360, 2 ], // Death Whisper: Requires 2 Nepenthese Seeds
    [ 4345, 3 ], // Might: Requires 3 Nepenthese Seeds
    [ 4355, 3 ], // Acumen: Requires 3 Nepenthese Seeds
    [ 4352, 3 ], // Berserker: Requires 3 Nepenthese Seeds
    [ 4354, 3 ], // Vampiric Rage: Requires 3 Nepenthese Seeds
    [ 4356, 6 ], // Empower: Requires 6 Nepenthese Seeds
    [ 4357, 6 ], // Haste: Requires 6 Nepenthese Seeds
]

export class VarkaSilenosSupport extends ListenerLogic {
    constructor() {
        super( 'VarkaSilenosSupport', 'listeners/npcs/VarkaSilenosSupport.ts' )
    }

    getAllianceLevel( player: L2PcInstance ): number {
        let value = 0
        _.each( marks, ( markValue: number, index: number ) => {
            if ( QuestHelper.hasQuestItems( player, markValue ) ) {
                value = -( index + 1 )
                return false
            }
        } )

        return value
    }

    getApproachedForTalkIds(): Array<number> {
        return [ ASHAS, NARAN, UDAN, DIYABU, HAGOS, SHIKON, TERANU ]
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/VarkaSilenosSupport'
    }

    getQuestStartIds(): Array<number> {
        return [ HAGOS, TERANU ]
    }

    getTalkIds(): Array<number> {
        return [ UDAN, HAGOS, TERANU ]
    }

    async onApproachedForTalk( data : NpcApproachedForTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let level = this.getAllianceLevel( player )

        switch ( data.characterNpcId ) {
            case ASHAS:
                return this.getPath( ( level < 0 ) ? '31377-friend.html' : '31377-no.html' )

            case NARAN:
                return this.getPath( ( level < 0 ) ? '31378-friend.html' : '31378-no.html' )

            case UDAN:
                return this.getPath( ( level < 0 ) ? ( level > -3 ) ? '31379-01.html' : '31379-04.html' : '31379-03.html' )

            case DIYABU:
                return this.getPath( ( level < 0 ) ? '31380-friend.html' : '31380-no.html' )

            case HAGOS:
                return this.getPath( ( level < 0 ) ? ( level === -1 ) ? '31381-01.html' : '31381-02.html' : '31381-no.html' )

            case SHIKON:
                switch ( level ) {
                    case -1:
                    case -2:
                        return this.getPath( '31382-01.html' )

                    case -3:
                    case -4:
                        return this.getPath( '31382-02.html' )

                    case -5:
                        return this.getPath( '31382-03.html' )
                }
                return this.getPath( '31382-no.html' )

            case TERANU:
                switch ( level ) {
                    case -1:
                    case -2:
                    case -3:
                        return this.getPath( '31383-01.html' )

                    case -4:
                        return this.getPath( '31383-02.html' )

                    case -5:
                        return this.getPath( '31383-03.html' )
                }
                return this.getPath( '31383-no.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}