import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { Castle } from '../../gameService/models/entity/Castle'
import { NpcHtmlMessagePath } from '../../gameService/packets/send/NpcHtmlMessage'
import { DataManager } from '../../data/manager'
import { SevenSigns } from '../../gameService/directives/SevenSigns'
import { L2MerchantInstance } from '../../gameService/models/actor/instance/L2MerchantInstance'
import { PlayerActionOverride } from '../../gameService/values/PlayerConditions'
import { SevenSignsSeal, SevenSignsSide } from '../../gameService/values/SevenSignsValues'
import { NpcApproachedForTalkEvent, NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import _ from 'lodash'
import { ClanPrivilege } from '../../gameService/enums/ClanPriviledge'

const npcIds: Array<number> = [
    35102, // Greenspan
    35144, // Sanford
    35186, // Arvid
    35228, // Morrison
    35276, // Eldon
    35318, // Solinus
    35365, // Rowell
    35511, // Gompus
    35557, // Kendrew
]

export class CastleMercenaryManager extends ListenerLogic {
    constructor() {
        super( 'CastleMercenaryManager', 'listeners/npcs/CastleMercenaryManager.ts' )
    }

    getCastlePath( name: string ): string {
        if ( name === 'aden' ) {
            return this.getPath( 'mercmanager-aden-limit.html' )
        }

        if ( name === 'rune' ) {
            return this.getPath( 'mercmanager-rune-limit.html' )
        }

        return this.getPath( 'mercmanager-limit.html' )
    }

    getApproachedForTalkIds(): Array<number> {
        return npcIds
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/CastleMercenaryManager'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onApproachedForTalk( data : NpcApproachedForTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let npc : L2Npc = L2World.getObjectById( data.characterId ) as L2Npc

        if ( player.hasActionOverride( PlayerActionOverride.CastleRights )
                || ( ( player.getClanId() === npc.getCastle().getOwnerId() ) && player.hasClanPrivilege( ClanPrivilege.CastleMercenaries ) ) ) {

            if ( npc.getCastle().getSiege().isInProgress() ) {
                return this.getPath( 'mercmanager-siege.html' )
            }

            switch ( SevenSigns.getWinningSealSide( SevenSignsSeal.Strife ) ) {
                case SevenSignsSide.Dusk:
                    return this.getPath( 'mercmanager-dusk.html' )

                case SevenSignsSide.Dawn:
                    return this.getPath( 'mercmanager-dawn.html' )

            }

            return this.getPath( 'mercmanager.html' )
        }

        return this.getPath( 'mercmanager-no.htm' )
    }

    async onNpcEvent( data : NpcGeneralEvent ): Promise<string> {

        let event = data.eventName
        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        let player = L2World.getPlayer( data.playerId )

        let chunks: Array<string> = _.split( event, ' ' )
        switch ( chunks[ 0 ] ) {
            case 'limit':
                let castle: Castle = npc.getCastle()
                let path = this.getCastlePath( castle.getName() )
                let html = DataManager.getHtmlData().getItem( path )
                        .replace( /%feud_name%/g, ( 1001000 + castle.getResidenceId() ).toString() )
                player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), npc.getObjectId() ) )
                return

            case 'buy':
                if ( SevenSigns.isSealValidationPeriod() ) {
                    ( npc as L2MerchantInstance ).showBuyWindow( player, npc.getId() + _.parseInt( _.nth( chunks, 1 ) ), false ) // NOTE: Not affected by Castle Taxes, baseTax is 20% (done in merchant buylists)
                    return
                }

                return this.getPath( 'mercmanager-ssq.html' )

            case 'main':
                return this.onApproachedForTalk( data )

            case 'mercmanager-01.html':
                return this.getPath( event )
        }
    }
}