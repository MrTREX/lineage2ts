import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcApproachedForTalkEvent, NpcGeneralEvent, NpcSpawnEvent, } from '../../gameService/models/events/EventType'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { Fort } from '../../gameService/models/entity/Fort'
import { DataManager } from '../../data/manager'
import { NpcHtmlMessagePath } from '../../gameService/packets/send/NpcHtmlMessage'
import { Castle } from '../../gameService/models/entity/Castle'
import { L2World } from '../../gameService/L2World'
import { L2Object } from '../../gameService/models/L2Object'
import { QuestHelper } from '../helpers/QuestHelper'
import { FortState } from '../../gameService/enums/FortState'

const npcIds: Array<number> = [
    36393, 36394, 36437, 36435, // Gludio
    36395, 36436, 36439, 36441, // Dion
    36396, 36440, 36444, 36449, 36451, // Giran
    36397, 36438, 36442, 36443, 36446, // Oren
    36398, 36399, 36445, 36448, // Aden
    36400, 36450, // Innadril
    36401, 36447, 36453, // Goddard
    36433, 36452, 36454, // Rune
    36434, 36455, // Schuttgart
]

// TODO : review despawn logic since it should be governed by spawns

export class CastleAmbassador extends ListenerLogic {
    constructor() {
        super( 'CastleAmbassador', 'listeners/npcs/CastleAmbassador.ts' )
    }

    getApproachedForTalkIds(): Array<number> {
        return npcIds
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getSpawnIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onApproachedForTalk( data : NpcApproachedForTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        let fort: Fort = npc.getFort()
        let fortOwner = fort.getOwnerClan() ? fort.getOwnerClan().getId() : 0

        let path: string
        if ( player.isClanLeader() && ( player.getClanId() === fortOwner ) ) {
            path = this.getPath( fort.isBorderFortress() ? 'ambassador-01.html' : 'ambassador.html' )
        } else {
            path = this.getPath( 'ambassador-03.html' )
        }

        let html = DataManager.getHtmlData().getItem( path )
                .replace( /%castleName%/g, fort.getCastleByAmbassador( npc.getId() ).getName() )

        player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), npc.getObjectId() ) )
        return
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        if ( npc ) {

            let path: string = await this.getEventHtml( data.eventName, npc )
            if ( path ) {
                let player = L2World.getPlayer( data.playerId )
                let fort: Fort = npc.getFort()
                let html = DataManager.getHtmlData().getItem( path )
                        .replace( /%castleName%/g, fort.getCastleByAmbassador( npc.getId() ).getName() )
                player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), npc.getObjectId() ) )
            }

            return
        }
    }

    onSpawnEvent( data : NpcSpawnEvent ): Promise<void> {
        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        let castle: Castle = npc.getFort().getCastleByAmbassador( npc.getId() )
        if ( !castle ) {
            return
        }

        if ( castle.getOwnerId() === 0 ) {
            return npc.deleteMe()
        }

        this.startQuestTimer( 'DESPAWN', 3600000, npc.getObjectId() )
    }

    async getEventHtml( event: string, npc: L2Npc ): Promise<string> {
        let fort: Fort = npc.getFort()
        switch ( event ) {
            case 'signed':
                if ( fort.getFortState() === FortState.NpcOwned ) {
                    fort.setFortState( FortState.ClanClaimed, fort.getCastleIdByAmbassador( npc.getId() ) )
                    this.stopQuestTimer( 'DESPAWN', npc.getObjectId() )
                    this.startQuestTimer( 'DESPAWN', 3000, npc.getObjectId() )
                    return this.getPath( 'ambassador-05.html' )
                }

                if ( fort.getFortState() === FortState.Abandoned ) {
                    return this.getPath( 'ambassador-04.html' )
                }

                return

            case 'rejected':
                if ( fort.getFortState() === 0 ) {
                    fort.setFortState( 1, fort.getCastleIdByAmbassador( npc.getId() ) )
                    this.stopQuestTimer( 'DESPAWN', npc.getObjectId() )
                    this.startQuestTimer( 'DESPAWN', 3000, npc.getObjectId() )

                    return this.getPath( 'ambassador-02.html' )
                }

                if ( fort.getFortState() === 2 ) {
                    return this.getPath( 'ambassador-02.html' )
                }

                return

            case 'DESPAWN':
                if ( fort.getFortState() === 0 ) {
                    fort.setFortState( 1, fort.getCastleIdByAmbassador( npc.getId() ) )
                }

                this.stopQuestTimer( 'DESPAWN', npc.getObjectId() )
                await QuestHelper.broadcastEvent( npc, 'DESPAWN', 1000, null, this.isCorrectNpc )
                await npc.deleteMe()
                return
        }
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/CastleAmbassador'
    }

    isCorrectNpc( object : L2Object ) : boolean {
        return npcIds.includes( object.getId() )
    }
}