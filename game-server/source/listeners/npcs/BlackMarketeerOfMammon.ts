import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestType } from '../../gameService/enums/QuestType'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { QuestHelper } from '../helpers/QuestHelper'
import { NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { ItemTypes } from '../../gameService/values/InventoryValues'
import moment from 'moment'

const npcId: number = 31092
const minimumLevel = 60

export class BlackMarketeerOfMammon extends ListenerLogic {
    constructor() {
        super( 'BlackMarketeerOfMammon', 'listeners/npcs/BlackMarketeerOfMammon.ts' )
    }

    getQuestStartIds(): Array<number> {
        return [ npcId ]
    }

    getTalkIds(): Array<number> {
        return [ npcId ]
    }

    async onTalkEvent(): Promise<string> {
        return this.getPath( this.isExchangeAvailable() ? '31092-01.html' : '31092-02.html' )
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/BlackMarketeerOfMammon'
    }

    isExchangeAvailable() : boolean {
        let startTime = moment().startOf( 'day' ).hours( 20 )
        let endTime = moment().add( 1, 'day' ).startOf( 'day' )

        return moment().isBetween( startTime, endTime )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let event = data.eventName
        let player = L2World.getPlayer( data.playerId )

        if ( 'exchange' === event ) {
            if ( this.isExchangeAvailable() ) {
                if ( player.getLevel() >= minimumLevel ) {
                    let state : QuestState = this.getQuestState( data.playerId, true )
                    if ( !state.isNowAvailable() ) {
                        return this.getPath( '31092-03.html' )
                    }

                    if ( player.getAdena() >= 2000000 ) {
                        state.setState( QuestStateValues.STARTED )
                        await QuestHelper.takeSingleItem( player, ItemTypes.Adena, 2000000 )
                        await QuestHelper.giveSingleItem( player, ItemTypes.AncientAdena, 500000 )
                        await state.exitQuestWithType( QuestType.DAILY, false )

                        return this.getPath( '31092-04.html' )
                    }

                    return this.getPath( '31092-05.html' )
                }

                return this.getPath( '31092-06.html' )
            }

            return this.getPath( '31092-02.html' )
        }

        return
    }
}