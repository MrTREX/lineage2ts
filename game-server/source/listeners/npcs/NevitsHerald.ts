import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { SkillHolder } from '../../gameService/models/holders/SkillHolder'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { ExShowScreenMessage } from '../../gameService/packets/send/ExShowScreenMessage'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2EffectType } from '../../gameService/enums/effects/L2EffectType'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { AttackableKillEvent, NpcGeneralEvent } from '../../gameService/models/events/EventType'
import aigle from 'aigle'
import _ from 'lodash'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const npcId: number = 4326
const locations: Array<Location> = [
    new Location( 86971, -142772, -1336, 20480 ), // Town of Schuttgart
    new Location( 44165, -48494, -792, 32768 ), // Rune Township
    new Location( 148017, -55264, -2728, 49152 ), // Town of Goddard
    new Location( 147919, 26631, -2200, 16384 ), // Town of Aden
    new Location( 82325, 53278, -1488, 16384 ), // Town of Oren
    new Location( 81925, 148302, -3464, 49152 ), // Town of Giran
    new Location( 111678, 219197, -3536, 49152 ), // Heine
    new Location( 16254, 142808, -2696, 16384 ), // Town of Dion
    new Location( -13865, 122081, -2984, 32768 ), // Town of Gludio
    new Location( -83248, 150832, -3136, 32768 ), // Gludin Village
    new Location( 116899, 77256, -2688, 49152 ), // Hunters Village
]

const ANTHARAS = 29068 // Antharas Strong (85)
const VALAKAS = 29028 // Valakas (85)
const sayLines: Array<number> = [
    NpcStringIds.SHOW_RESPECT_TO_THE_HEROES_WHO_DEFEATED_THE_EVIL_DRAGON_AND_PROTECTED_THIS_ADEN_WORLD,
    NpcStringIds.SHOUT_TO_CELEBRATE_THE_VICTORY_OF_THE_HEROES,
    NpcStringIds.PRAISE_THE_ACHIEVEMENT_OF_THE_HEROES_AND_RECEIVE_NEVITS_BLESSING,
]

const FALL_OF_THE_DRAGON = new SkillHolder( 23312 )

export class NevitsHerald extends ListenerLogic {
    isActive: boolean = false
    spawnedNpcs: Array<number>

    constructor() {
        super( 'NevitsHerald', 'listeners/npcs/NevitsHerald.ts' )
    }

    getAttackableKillIds(): Array<number> {
        return [ ANTHARAS, VALAKAS ]
    }

    getApproachedForTalkIds(): Array<number> {
        return [ npcId ]
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/NevitsHerald'
    }

    getQuestStartIds(): Array<number> {
        return [ npcId ]
    }

    getTalkIds(): Array<number> {
        return [ npcId ]
    }

    async onAttackableKillEvent( data : AttackableKillEvent ): Promise<string> {
        let messageId: number = data.npcId === VALAKAS ? NpcStringIds.THE_EVIL_FIRE_DRAGON_VALAKAS_HAS_BEEN_DEFEATED : NpcStringIds.THE_EVIL_LAND_DRAGON_ANTHARAS_HAS_BEEN_DEFEATED
        let message: Buffer = ExShowScreenMessage.fromNpcMessageId( messageId, 2, 10000, null )

        _.each( L2World.getAllPlayers(), ( player: L2PcInstance ) => {
            if ( player ) {
                player.sendCopyData( message )
            }
        } )

        if ( !this.isActive ) {
            this.isActive = true

            this.spawnedNpcs = locations.map( ( currentLocation: Location ): number => {
                let npc: L2Npc = QuestHelper.addSpawnAtLocation( npcId, currentLocation, false, 0 )
                if ( npc ) {
                    return npc.getObjectId()
                }

                return 0
            } )


            this.startQuestTimer( 'despawn', 14400000, data.targetId, data.playerId )
            this.startQuestTimer( 'text_spam', 3000, data.targetId, data.playerId )
        }

        return
    }

    async onApproachedForTalk(): Promise<string> {
        return this.getPath( '4326.html' )
    }

    async onNpcEvent( data : NpcGeneralEvent ): Promise<string> {

        let event = data.eventName
        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        let player = L2World.getPlayer( data.playerId )

        if ( npc.getId() === npcId && _.toLower( event ) === 'buff' ) {
            if ( player.getEffectList().getFirstEffect( L2EffectType.Nevit ) ) {
                return this.getPath( '4326-1.html' )
            }

            npc.setTarget( player )
            await npc.doCast( FALL_OF_THE_DRAGON.getSkill() )
            return
        }

        if ( event === 'text_spam' ) {
            this.stopQuestTimer( 'text_spam', data.characterId, data.playerId )
            BroadcastHelper.broadcastNpcSayStringId( npc,
                    NpcSayType.Shout,
                    _.sample( sayLines ) )

            this.startQuestTimer( 'text_spam', 60000, data.characterId, data.playerId )
            return
        }

        if ( event === 'despawn' ) {
            await aigle.resolve( this.spawnedNpcs ).each( ( objectId: number ) => {
                let npc: L2Npc = L2World.getObjectById( objectId ) as L2Npc
                if ( npc ) {
                    return npc.deleteMe()
                }
            } )
        }

        return this.getPath( event )
    }
}