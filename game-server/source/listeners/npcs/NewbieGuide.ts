import { SkillHolder } from '../../gameService/models/holders/SkillHolder'
import { Location } from '../../gameService/models/Location'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { Race } from '../../gameService/enums/Race'
import { ClassId, ClassIdValues } from '../../gameService/models/base/ClassId'
import { L2Summon } from '../../gameService/models/actor/L2Summon'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { ShowHtml } from '../helpers/ShowHtml'
import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Voice } from '../../gameService/enums/audio/Voice'
import { MultisellCache } from '../../gameService/cache/MultisellCache'
import { NpcApproachedForTalkEvent, NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'

const SUFFIX_FIGHTER_5_LEVEL = '-f05.htm'
const SUFFIX_FIGHTER_10_LEVEL = '-f10.htm'
const SUFFIX_FIGHTER_15_LEVEL = '-f15.htm'
const SUFFIX_FIGHTER_20_LEVEL = '-f20.htm'
const SUFFIX_MAGE_7_LEVEL = '-m07.htm'
const SUFFIX_MAGE_14_LEVEL = '-m14.htm'
const SUFFIX_MAGE_20_LEVEL = '-m20.htm'

const FIRST_COUPON_SIZE = 5
const SECOND_COUPON_SIZE = 1

const NEWBIE_GUIDE_HUMAN = 30598
const NEWBIE_GUIDE_ELF = 30599
const NEWBIE_GUIDE_DARK_ELF = 30600
const NEWBIE_GUIDE_DWARF = 30601
const NEWBIE_GUIDE_ORC = 30602
const NEWBIE_GUIDE_KAMAEL = 32135
const NEWBIE_GUIDE_GLUDIN = 31076
const NEWBIE_GUIDE_GLUDIO = 31077
const ADVENTURERS_GUIDE = 32327

const GUIDE_MISSION = 41

const SOULSHOT_NO_GRADE_FOR_BEGINNERS = 5789
const SPIRITSHOT_NO_GRADE_FOR_BEGINNERS = 5790
const SCROLL_RECOVERY_NO_GRADE = 8594

const APPRENTICE_ADVENTURERS_WEAPON_EXCHANGE_COUPON = 7832
const ADVENTURERS_MAGIC_ACCESSORY_EXCHANGE_COUPON = 7833

const WIND_WALK_FOR_BEGINNERS = new SkillHolder( 4322 )
const SHIELD_FOR_BEGINNERS = new SkillHolder( 4323 )
const BLESS_THE_BODY_FOR_BEGINNERS = new SkillHolder( 4324 )
const VAMPIRIC_RAGE_FOR_BEGINNERS = new SkillHolder( 4325 )
const REGENERATION_FOR_BEGINNERS = new SkillHolder( 4326 )
const HASTE_FOR_BEGINNERS = new SkillHolder( 4327 )
const BLESS_THE_SOUL_FOR_BEGINNERS = new SkillHolder( 4328 )
const ACUMEN_FOR_BEGINNERS = new SkillHolder( 4329 )
const CONCENTRATION_FOR_BEGINNERS = new SkillHolder( 4330 )
const EMPOWER_FOR_BEGINNERS = new SkillHolder( 4331 )
const LIFE_CUBIC_FOR_BEGINNERS = new SkillHolder( 4338 )
const BLESSING_OF_PROTECTION = new SkillHolder( 5182 )
const ADVENTURERS_HASTE = new SkillHolder( 5632 )
const ADVENTURERS_MAGIC_BARRIER = new SkillHolder( 5637 )

const weaponBuylistId = 305986001
const accesoryBuylistId = 305986002

const humanTalkingIsland = new Location( -84113, 243261, -3723, 58341 )
const darkElfVillage = new Location( 12121, 16745, -4582, 46934 )
const dwarfVillage = new Location( 115662, -178053, -905, 20690 )
const elfVillage = new Location( 45528, 48370, -3060, 34013 )
const orcVillage = new Location( -45100, -113602, -192, 0 )
const kamaelVillage = new Location( -119691, 44558, 380, 49151 )

const teleportMap: { [ race: string ]: Location } = {
    human: humanTalkingIsland,
    darkelf: darkElfVillage,
    dwarf: dwarfVillage,
    elf: elfVillage,
    orc: orcVillage,
    kamael: kamaelVillage
}

const npcIds = [
    NEWBIE_GUIDE_HUMAN,
    NEWBIE_GUIDE_ELF,
    NEWBIE_GUIDE_DARK_ELF,
    NEWBIE_GUIDE_DWARF,
    NEWBIE_GUIDE_ORC,
    NEWBIE_GUIDE_KAMAEL,
    NEWBIE_GUIDE_GLUDIN,
    NEWBIE_GUIDE_GLUDIO,
    ADVENTURERS_GUIDE,
]

const enum Variables {
    Started = 'ng.started',
    Value = 'ng.value'
}

export class NewbieGuide extends ListenerLogic {

    constructor() {
        super( 'NewbieGuide', 'listeners/npcs/NewbieGuide.ts' )
    }

    eventGuideDelf( id: number, state: QuestState ) {
        switch ( id ) {
            case 10:
                return this.getPath( '30600-04.htm' )
            case 11:
                return this.getPath( '30600-04a.htm' )
            case 12:
                return this.getPath( '30600-04b.htm' )
            case 13:
                return this.getPath( '30600-04c.htm' )
            case 14:
                return this.getPath( '30600-04d.htm' )
            case 15:
                return this.getPath( '30600-04e.htm' )
            case 16:
                return this.getPath( '30600-04f.htm' )
            case 17:
                return this.getPath( '30600-04g.htm' )
            case 18:
                return this.getPath( '30600-04h.htm' )
            case 31:
                state.clearRadar()
                state.addRadar( 9670, 15537, -4574 )
                return this.getPath( '30600-05.htm' )
            case 32:
                state.clearRadar()
                state.addRadar( 15120, 15656, -4376 )
                return this.getPath( '30600-05.htm' )
            case 33:
                state.clearRadar()
                state.addRadar( 17306, 13592, -3724 )
                return this.getPath( '30600-05.htm' )
            case 34:
                state.clearRadar()
                state.addRadar( 15272, 16310, -4377 )
                return this.getPath( '30600-05.htm' )
            case 35:
                state.clearRadar()
                state.addRadar( 6449, 19619, -3694 )
                return this.getPath( '30600-05.htm' )
            case 36:
                state.clearRadar()
                state.addRadar( -15404, 71131, -3445 )
                return this.getPath( '30600-05.htm' )
            case 37:
                state.clearRadar()
                state.addRadar( 7496, 17388, -4377 )
                return this.getPath( '30600-05.htm' )
            case 38:
                state.clearRadar()
                state.addRadar( 17102, 13002, -3743 )
                return this.getPath( '30600-05.htm' )
            case 39:
                state.clearRadar()
                state.addRadar( 6532, 19903, -3693 )
                return this.getPath( '30600-05.htm' )
            case 40:
                state.clearRadar()
                state.addRadar( -15648, 71405, -3451 )
                return this.getPath( '30600-05.htm' )
            case 41:
                state.clearRadar()
                state.addRadar( 7644, 18048, -4377 )
                return this.getPath( '30600-05.htm' )
            case 42:
                state.clearRadar()
                state.addRadar( -1301, 75883, -3566 )
                return this.getPath( '30600-05.htm' )
            case 43:
                state.clearRadar()
                state.addRadar( -1152, 76125, -3566 )
                return this.getPath( '30600-05.htm' )
            case 44:
                state.clearRadar()
                state.addRadar( 10580, 17574, -4554 )
                return this.getPath( '30600-05.htm' )
            case 45:
                state.clearRadar()
                state.addRadar( 12009, 15704, -4554 )
                return this.getPath( '30600-05.htm' )
            case 46:
                state.clearRadar()
                state.addRadar( 11951, 15661, -4554 )
                return this.getPath( '30600-05.htm' )
            case 47:
                state.clearRadar()
                state.addRadar( 10761, 17970, -4554 )
                return this.getPath( '30600-05.htm' )
            case 48:
                state.clearRadar()
                state.addRadar( 10823, 18013, -4554 )
                return this.getPath( '30600-05.htm' )
            case 49:
                state.clearRadar()
                state.addRadar( 11283, 14226, -4242 )
                return this.getPath( '30600-05.htm' )
            case 50:
                state.clearRadar()
                state.addRadar( 10447, 14620, -4242 )
                return this.getPath( '30600-05.htm' )
            case 51:
                state.clearRadar()
                state.addRadar( 11258, 14431, -4242 )
                return this.getPath( '30600-05.htm' )
            case 52:
                state.clearRadar()
                state.addRadar( 10344, 14445, -4242 )
                return this.getPath( '30600-05.htm' )
            case 53:
                state.clearRadar()
                state.addRadar( 10315, 14293, -4242 )
                return this.getPath( '30600-05.htm' )
            case 54:
                state.clearRadar()
                state.addRadar( 10775, 14190, -4242 )
                return this.getPath( '30600-05.htm' )
            case 55:
                state.clearRadar()
                state.addRadar( 11235, 14078, -4242 )
                return this.getPath( '30600-05.htm' )
            case 56:
                state.clearRadar()
                state.addRadar( 11012, 14128, -4242 )
                return this.getPath( '30600-05.htm' )
            case 57:
                state.clearRadar()
                state.addRadar( 13380, 17430, -4542 )
                return this.getPath( '30600-05.htm' )
            case 58:
                state.clearRadar()
                state.addRadar( 13464, 17751, -4541 )
                return this.getPath( '30600-05.htm' )
            case 59:
                state.clearRadar()
                state.addRadar( 13763, 17501, -4542 )
                return this.getPath( '30600-05.htm' )
            case 60:
                state.clearRadar()
                state.addRadar( -44225, 79721, -3652 )
                return this.getPath( '30600-05.htm' )
            case 61:
                state.clearRadar()
                state.addRadar( -44015, 79683, -3652 )
                return this.getPath( '30600-05.htm' )
            case 62:
                state.clearRadar()
                state.addRadar( 25856, 10832, -3724 )
                return this.getPath( '30600-05.htm' )
            case 63:
                state.clearRadar()
                state.addRadar( 12328, 14947, -4574 )
                return this.getPath( '30600-05.htm' )
            case 64:
                state.clearRadar()
                state.addRadar( 13081, 18444, -4573 )
                return this.getPath( '30600-05.htm' )
            case 65:
                state.clearRadar()
                state.addRadar( 12311, 17470, -4574 )
                return this.getPath( '30600-05.htm' )
        }

        return ''
    }

    eventGuideDwarf( id: number, state: QuestState ) {
        switch ( id ) {
            case 10:
                return this.getPath( '30601-04.htm' )
            case 11:
                return this.getPath( '30601-04a.htm' )
            case 12:
                return this.getPath( '30601-04b.htm' )
            case 13:
                return this.getPath( '30601-04c.htm' )
            case 14:
                return this.getPath( '30601-04d.htm' )
            case 15:
                return this.getPath( '30601-04e.htm' )
            case 16:
                return this.getPath( '30601-04f.htm' )
            case 17:
                return this.getPath( '30601-04g.htm' )
            case 18:
                return this.getPath( '30601-04h.htm' )
            case 31:
                state.clearRadar()
                state.addRadar( 115072, -178176, -906 )
                return this.getPath( '30601-05.htm' )
            case 32:
                state.clearRadar()
                state.addRadar( 117847, -182339, -1537 )
                return this.getPath( '30601-05.htm' )
            case 33:
                state.clearRadar()
                state.addRadar( 116617, -184308, -1569 )
                return this.getPath( '30601-05.htm' )
            case 34:
                state.clearRadar()
                state.addRadar( 117826, -182576, -1537 )
                return this.getPath( '30601-05.htm' )
            case 35:
                state.clearRadar()
                state.addRadar( 116378, -184308, -1571 )
                return this.getPath( '30601-05.htm' )
            case 36:
                state.clearRadar()
                state.addRadar( 115183, -176728, -791 )
                return this.getPath( '30601-05.htm' )
            case 37:
                state.clearRadar()
                state.addRadar( 114969, -176752, -790 )
                return this.getPath( '30601-05.htm' )
            case 38:
                state.clearRadar()
                state.addRadar( 117366, -178725, -1118 )
                return this.getPath( '30601-05.htm' )
            case 39:
                state.clearRadar()
                state.addRadar( 117378, -178914, -1120 )
                return this.getPath( '30601-05.htm' )
            case 40:
                state.clearRadar()
                state.addRadar( 116226, -178529, -948 )
                return this.getPath( '30601-05.htm' )
            case 41:
                state.clearRadar()
                state.addRadar( 116190, -178441, -948 )
                return this.getPath( '30601-05.htm' )
            case 42:
                state.clearRadar()
                state.addRadar( 116016, -178615, -948 )
                return this.getPath( '30601-05.htm' )
            case 43:
                state.clearRadar()
                state.addRadar( 116190, -178615, -948 )
                return this.getPath( '30601-05.htm' )
            case 44:
                state.clearRadar()
                state.addRadar( 116103, -178407, -948 )
                return this.getPath( '30601-05.htm' )
            case 45:
                state.clearRadar()
                state.addRadar( 116103, -178653, -948 )
                return this.getPath( '30601-05.htm' )
            case 46:
                state.clearRadar()
                state.addRadar( 115468, -182446, -1434 )
                return this.getPath( '30601-05.htm' )
            case 47:
                state.clearRadar()
                state.addRadar( 115315, -182155, -1444 )
                return this.getPath( '30601-05.htm' )
            case 48:
                state.clearRadar()
                state.addRadar( 115271, -182692, -1445 )
                return this.getPath( '30601-05.htm' )
            case 49:
                state.clearRadar()
                state.addRadar( 115900, -177316, -915 )
                return this.getPath( '30601-05.htm' )
            case 50:
                state.clearRadar()
                state.addRadar( 116268, -177524, -914 )
                return this.getPath( '30601-05.htm' )
            case 51:
                state.clearRadar()
                state.addRadar( 115741, -181645, -1344 )
                return this.getPath( '30601-05.htm' )
            case 52:
                state.clearRadar()
                state.addRadar( 116192, -181072, -1344 )
                return this.getPath( '30601-05.htm' )
            case 53:
                state.clearRadar()
                state.addRadar( 115205, -180024, -870 )
                return this.getPath( '30601-05.htm' )
            case 54:
                state.clearRadar()
                state.addRadar( 114716, -180018, -871 )
                return this.getPath( '30601-05.htm' )
            case 55:
                state.clearRadar()
                state.addRadar( 114832, -179520, -871 )
                return this.getPath( '30601-05.htm' )
            case 56:
                state.clearRadar()
                state.addRadar( 115717, -183488, -1483 )
                return this.getPath( '30601-05.htm' )
            case 57:
                state.clearRadar()
                state.addRadar( 115618, -183265, -1483 )
                return this.getPath( '30601-05.htm' )
            case 58:
                state.clearRadar()
                state.addRadar( 114348, -178537, -813 )
                return this.getPath( '30601-05.htm' )
            case 59:
                state.clearRadar()
                state.addRadar( 114990, -177294, -854 )
                return this.getPath( '30601-05.htm' )
            case 60:
                state.clearRadar()
                state.addRadar( 114426, -178672, -812 )
                return this.getPath( '30601-05.htm' )
            case 61:
                state.clearRadar()
                state.addRadar( 114409, -178415, -812 )
                return this.getPath( '30601-05.htm' )
            case 62:
                state.clearRadar()
                state.addRadar( 117061, -181867, -1413 )
                return this.getPath( '30601-05.htm' )
            case 63:
                state.clearRadar()
                state.addRadar( 116164, -184029, -1507 )
                return this.getPath( '30601-05.htm' )
            case 64:
                state.clearRadar()
                state.addRadar( 115563, -182923, -1448 )
                return this.getPath( '30601-05.htm' )
            case 65:
                state.clearRadar()
                state.addRadar( 112656, -174864, -611 )
                return this.getPath( '30601-05.htm' )
            case 66:
                state.clearRadar()
                state.addRadar( 116852, -183595, -1566 )
                return this.getPath( '30601-05.htm' )
        }

        return ''
    }

    eventGuideElf( id: number, state: QuestState ) {
        switch ( id ) {
            case 10:
                return this.getPath( '30599-04.htm' )
            case 11:
                return this.getPath( '30599-04a.htm' )
            case 12:
                return this.getPath( '30599-04b.htm' )
            case 13:
                return this.getPath( '30599-04c.htm' )
            case 14:
                return this.getPath( '30599-04d.htm' )
            case 15:
                return this.getPath( '30599-04e.htm' )
            case 16:
                return this.getPath( '30599-04f.htm' )
            case 17:
                return this.getPath( '30599-04g.htm' )
            case 18:
                return this.getPath( '30599-04h.htm' )
            case 31:
                state.clearRadar()
                state.addRadar( 46926, 51511, -2977 )
                return this.getPath( '30599-05.htm' )
            case 32:
                state.clearRadar()
                state.addRadar( 44995, 51706, -2803 )
                return this.getPath( '30599-05.htm' )
            case 33:
                state.clearRadar()
                state.addRadar( 45727, 51721, -2803 )
                return this.getPath( '30599-05.htm' )
            case 34:
                state.clearRadar()
                state.addRadar( 42812, 51138, -2996 )
                return this.getPath( '30599-05.htm' )
            case 35:
                state.clearRadar()
                state.addRadar( 45487, 46511, -2996 )
                return this.getPath( '30599-05.htm' )
            case 36:
                state.clearRadar()
                state.addRadar( 47401, 51764, -2996 )
                return this.getPath( '30599-05.htm' )
            case 37:
                state.clearRadar()
                state.addRadar( 42971, 51372, -2996 )
                return this.getPath( '30599-05.htm' )
            case 38:
                state.clearRadar()
                state.addRadar( 47595, 51569, -2996 )
                return this.getPath( '30599-05.htm' )
            case 39:
                state.clearRadar()
                state.addRadar( 45778, 46534, -2996 )
                return this.getPath( '30599-05.htm' )
            case 40:
                state.clearRadar()
                state.addRadar( 44476, 47153, -2984 )
                return this.getPath( '30599-05.htm' )
            case 41:
                state.clearRadar()
                state.addRadar( 42700, 50057, -2984 )
                return this.getPath( '30599-05.htm' )
            case 42:
                state.clearRadar()
                state.addRadar( 42766, 50037, -2984 )
                return this.getPath( '30599-05.htm' )
            case 43:
                state.clearRadar()
                state.addRadar( 44683, 46952, -2981 )
                return this.getPath( '30599-05.htm' )
            case 44:
                state.clearRadar()
                state.addRadar( 44667, 46896, -2982 )
                return this.getPath( '30599-05.htm' )
            case 45:
                state.clearRadar()
                state.addRadar( 45725, 52105, -2795 )
                return this.getPath( '30599-05.htm' )
            case 46:
                state.clearRadar()
                state.addRadar( 44823, 52414, -2795 )
                return this.getPath( '30599-05.htm' )
            case 47:
                state.clearRadar()
                state.addRadar( 45000, 52101, -2795 )
                return this.getPath( '30599-05.htm' )
            case 48:
                state.clearRadar()
                state.addRadar( 45919, 52414, -2795 )
                return this.getPath( '30599-05.htm' )
            case 49:
                state.clearRadar()
                state.addRadar( 44692, 52261, -2795 )
                return this.getPath( '30599-05.htm' )
            case 50:
                state.clearRadar()
                state.addRadar( 47780, 49568, -2983 )
                return this.getPath( '30599-05.htm' )
            case 51:
                state.clearRadar()
                state.addRadar( 47912, 50170, -2983 )
                return this.getPath( '30599-05.htm' )
            case 52:
                state.clearRadar()
                state.addRadar( 47868, 50167, -2983 )
                return this.getPath( '30599-05.htm' )
            case 53:
                state.clearRadar()
                state.addRadar( 28928, 74248, -3773 )
                return this.getPath( '30599-05.htm' )
            case 54:
                state.clearRadar()
                state.addRadar( 43673, 49683, -3046 )
                return this.getPath( '30599-05.htm' )
            case 55:
                state.clearRadar()
                state.addRadar( 45610, 49008, -3059 )
                return this.getPath( '30599-05.htm' )
            case 56:
                state.clearRadar()
                state.addRadar( 50592, 54986, -3376 )
                return this.getPath( '30599-05.htm' )
            case 57:
                state.clearRadar()
                state.addRadar( 42978, 49115, -2994 )
                return this.getPath( '30599-05.htm' )
            case 58:
                state.clearRadar()
                state.addRadar( 46475, 50495, -3058 )
                return this.getPath( '30599-05.htm' )
            case 59:
                state.clearRadar()
                state.addRadar( 45859, 50827, -3058 )
                return this.getPath( '30599-05.htm' )
            case 60:
                state.clearRadar()
                state.addRadar( 51210, 82474, -3283 )
                return this.getPath( '30599-05.htm' )
            case 61:
                state.clearRadar()
                state.addRadar( 49262, 53607, -3216 )
                return this.getPath( '30599-05.htm' )
        }

        return ''
    }

    eventGuideHuman( id: number, state: QuestState ) {
        switch ( id ) {
            case 10:
                return this.getPath( '30598-04.htm' )
            case 11:
                return this.getPath( '30598-04a.htm' )
            case 12:
                return this.getPath( '30598-04b.htm' )
            case 13:
                return this.getPath( '30598-04c.htm' )
            case 14:
                return this.getPath( '30598-04d.htm' )
            case 15:
                return this.getPath( '30598-04e.htm' )
            case 16:
                return this.getPath( '30598-04f.htm' )
            case 17:
                return this.getPath( '30598-04g.htm' )
            case 18:
                return this.getPath( '30598-04h.htm' )
            case 19:
                return this.getPath( '30598-04i.htm' )
            case 31:
                state.clearRadar()
                state.addRadar( -84108, 244604, -3729 )
                return this.getPath( '30598-05.htm' )
            case 32:
                state.clearRadar()
                state.addRadar( -82236, 241573, -3728 )
                return this.getPath( '30598-05.htm' )
            case 33:
                state.clearRadar()
                state.addRadar( -82515, 241221, -3728 )
                return this.getPath( '30598-05.htm' )
            case 34:
                state.clearRadar()
                state.addRadar( -82319, 244709, -3727 )
                return this.getPath( '30598-05.htm' )
            case 35:
                state.clearRadar()
                state.addRadar( -82659, 244992, -3717 )
                return this.getPath( '30598-05.htm' )
            case 36:
                state.clearRadar()
                state.addRadar( -86114, 244682, -3727 )
                return this.getPath( '30598-05.htm' )
            case 37:
                state.clearRadar()
                state.addRadar( -86328, 244448, -3724 )
                return this.getPath( '30598-05.htm' )
            case 38:
                state.clearRadar()
                state.addRadar( -86322, 241215, -3727 )
                return this.getPath( '30598-05.htm' )
            case 39:
                state.clearRadar()
                state.addRadar( -85964, 240947, -3727 )
                return this.getPath( '30598-05.htm' )
            case 40:
                state.clearRadar()
                state.addRadar( -85026, 242689, -3729 )
                return this.getPath( '30598-05.htm' )
            case 41:
                state.clearRadar()
                state.addRadar( -83789, 240799, -3717 )
                return this.getPath( '30598-05.htm' )
            case 42:
                state.clearRadar()
                state.addRadar( -84204, 240403, -3717 )
                return this.getPath( '30598-05.htm' )
            case 43:
                state.clearRadar()
                state.addRadar( -86385, 243267, -3717 )
                return this.getPath( '30598-05.htm' )
            case 44:
                state.clearRadar()
                state.addRadar( -86733, 242918, -3717 )
                return this.getPath( '30598-05.htm' )
            case 45:
                state.clearRadar()
                state.addRadar( -84516, 245449, -3714 )
                return this.getPath( '30598-05.htm' )
            case 46:
                state.clearRadar()
                state.addRadar( -84729, 245001, -3726 )
                return this.getPath( '30598-05.htm' )
            case 47:
                state.clearRadar()
                state.addRadar( -84965, 245222, -3726 )
                return this.getPath( '30598-05.htm' )
            case 48:
                state.clearRadar()
                state.addRadar( -84981, 244764, -3726 )
                return this.getPath( '30598-05.htm' )
            case 49:
                state.clearRadar()
                state.addRadar( -85186, 245001, -3726 )
                return this.getPath( '30598-05.htm' )
            case 50:
                state.clearRadar()
                state.addRadar( -83326, 242964, -3718 )
                return this.getPath( '30598-05.htm' )
            case 51:
                state.clearRadar()
                state.addRadar( -83020, 242553, -3718 )
                return this.getPath( '30598-05.htm' )
            case 52:
                state.clearRadar()
                state.addRadar( -83175, 243065, -3718 )
                return this.getPath( '30598-05.htm' )

            case 53:
                state.clearRadar()
                state.addRadar( -82809, 242751, -3718 )
                return this.getPath( '30598-05.htm' )

            case 54:
                state.clearRadar()
                state.addRadar( -81895, 243917, -3721 )
                return this.getPath( '30598-05.htm' )

            case 55:
                state.clearRadar()
                state.addRadar( -81840, 243534, -3721 )
                return this.getPath( '30598-05.htm' )

            case 56:
                state.clearRadar()
                state.addRadar( -81512, 243424, -3720 )
                return this.getPath( '30598-05.htm' )

            case 57:
                state.clearRadar()
                state.addRadar( -84436, 242793, -3729 )
                return this.getPath( '30598-05.htm' )

            case 58:
                state.clearRadar()
                state.addRadar( -78939, 240305, -3443 )
                return this.getPath( '30598-05.htm' )

            case 59:
                state.clearRadar()
                state.addRadar( -85301, 244587, -3725 )
                return this.getPath( '30598-05.htm' )

            case 60:
                state.clearRadar()
                state.addRadar( -83163, 243560, -3728 )
                return this.getPath( '30598-05.htm' )

            case 61:
                state.clearRadar()
                state.addRadar( -97131, 258946, -3622 )
                return this.getPath( '30598-05.htm' )

            case 62:
                state.clearRadar()
                state.addRadar( -114685, 222291, -2925 )
                return this.getPath( '30598-05.htm' )

            case 63:
                state.clearRadar()
                state.addRadar( -84057, 242832, -3729 )
                return this.getPath( '30598-05.htm' )

            case 64:
                state.clearRadar()
                state.addRadar( -100332, 238019, -3573 )
                return this.getPath( '30598-05.htm' )

            case 65:
                state.clearRadar()
                state.addRadar( -82041, 242718, -3725 )
                return this.getPath( '30598-05.htm' )
        }

        return ''
    }

    eventGuideKamael( id: number, state: QuestState ) {
        switch ( id ) {
            case 10:
                return this.getPath( '32135-04.htm' )
            case 11:
                return this.getPath( '32135-04a.htm' )
            case 12:
                return this.getPath( '32135-04b.htm' )
            case 13:
                return this.getPath( '32135-04c.htm' )
            case 14:
                return this.getPath( '32135-04d.htm' )
            case 15:
                return this.getPath( '32135-04e.htm' )
            case 16:
                return this.getPath( '32135-04f.htm' )
            case 17:
                return this.getPath( '32135-04g.htm' )
            case 18:
                return this.getPath( '32135-04h.htm' )
            case 19:
                return this.getPath( '32135-04i.htm' )
            case 20:
                return this.getPath( '32135-04j.htm' )
            case 21:
                return this.getPath( '32135-04k.htm' )
            case 22:
                return this.getPath( '32135-04l.htm' )
            case 31:
                state.clearRadar()
                state.addRadar( -116879, 46591, 380 )
                return this.getPath( '32135-05.htm' )
            case 32:
                state.clearRadar()
                state.addRadar( -119378, 49242, 22 )
                return this.getPath( '32135-05.htm' )
            case 33:
                state.clearRadar()
                state.addRadar( -119774, 49245, 22 )
                return this.getPath( '32135-05.htm' )
            case 34:
                state.clearRadar()
                state.addRadar( -119830, 51860, -787 )
                return this.getPath( '32135-05.htm' )
            case 35:
                state.clearRadar()
                state.addRadar( -119362, 51862, -780 )
                return this.getPath( '32135-05.htm' )
            case 36:
                state.clearRadar()
                state.addRadar( -112872, 46850, 68 )
                return this.getPath( '32135-05.htm' )
            case 37:
                state.clearRadar()
                state.addRadar( -112352, 47392, 68 )
                return this.getPath( '32135-05.htm' )
            case 38:
                state.clearRadar()
                state.addRadar( -110544, 49040, -1124 )
                return this.getPath( '32135-05.htm' )
            case 39:
                state.clearRadar()
                state.addRadar( -110536, 45162, -1132 )
                return this.getPath( '32135-05.htm' )
            case 40:
                state.clearRadar()
                state.addRadar( -115888, 43568, 524 )
                return this.getPath( '32135-05.htm' )
            case 41:
                state.clearRadar()
                state.addRadar( -115486, 43567, 525 )
                return this.getPath( '32135-05.htm' )
            case 42:
                state.clearRadar()
                state.addRadar( -116920, 47792, 464 )
                return this.getPath( '32135-05.htm' )
            case 43:
                state.clearRadar()
                state.addRadar( -116749, 48077, 462 )
                return this.getPath( '32135-05.htm' )
            case 44:
                state.clearRadar()
                state.addRadar( -117153, 48075, 463 )
                return this.getPath( '32135-05.htm' )
            case 45:
                state.clearRadar()
                state.addRadar( -119104, 43280, 559 )
                return this.getPath( '32135-05.htm' )
            case 46:
                state.clearRadar()
                state.addRadar( -119104, 43152, 559 )
                return this.getPath( '32135-05.htm' )
            case 47:
                state.clearRadar()
                state.addRadar( -117056, 43168, 559 )
                return this.getPath( '32135-05.htm' )
            case 48:
                state.clearRadar()
                state.addRadar( -117060, 43296, 559 )
                return this.getPath( '32135-05.htm' )
            case 49:
                state.clearRadar()
                state.addRadar( -118192, 42384, 838 )
                return this.getPath( '32135-05.htm' )
            case 50:
                state.clearRadar()
                state.addRadar( -117968, 42384, 838 )
                return this.getPath( '32135-05.htm' )
            case 51:
                state.clearRadar()
                state.addRadar( -118132, 42788, 723 )
                return this.getPath( '32135-05.htm' )
            case 52:
                state.clearRadar()
                state.addRadar( -118028, 42788, 720 )
                return this.getPath( '32135-05.htm' )
            case 53:
                state.clearRadar()
                state.addRadar( -114802, 44821, 524 )
                return this.getPath( '32135-05.htm' )
            case 54:
                state.clearRadar()
                state.addRadar( -114975, 44658, 524 )
                return this.getPath( '32135-05.htm' )
            case 55:
                state.clearRadar()
                state.addRadar( -114801, 45031, 525 )
                return this.getPath( '32135-05.htm' )
            case 56:
                state.clearRadar()
                state.addRadar( -120432, 45296, 416 )
                return this.getPath( '32135-05.htm' )
            case 57:
                state.clearRadar()
                state.addRadar( -120706, 45079, 419 )
                return this.getPath( '32135-05.htm' )
            case 58:
                state.clearRadar()
                state.addRadar( -120356, 45293, 416 )
                return this.getPath( '32135-05.htm' )
            case 59:
                state.clearRadar()
                state.addRadar( -120604, 44960, 423 )
                return this.getPath( '32135-05.htm' )
            case 60:
                state.clearRadar()
                state.addRadar( -120294, 46013, 384 )
                return this.getPath( '32135-05.htm' )
            case 61:
                state.clearRadar()
                state.addRadar( -120157, 45813, 355 )
                return this.getPath( '32135-05.htm' )
            case 62:
                state.clearRadar()
                state.addRadar( -120158, 46221, 354 )
                return this.getPath( '32135-05.htm' )
            case 63:
                state.clearRadar()
                state.addRadar( -120400, 46921, 415 )
                return this.getPath( '32135-05.htm' )
            case 64:
                state.clearRadar()
                state.addRadar( -120407, 46755, 423 )
                return this.getPath( '32135-05.htm' )
            case 65:
                state.clearRadar()
                state.addRadar( -120442, 47125, 422 )
                return this.getPath( '32135-05.htm' )
            case 66:
                state.clearRadar()
                state.addRadar( -118720, 48062, 473 )
                return this.getPath( '32135-05.htm' )
            case 67:
                state.clearRadar()
                state.addRadar( -118918, 47956, 474 )
                return this.getPath( '32135-05.htm' )
            case 68:
                state.clearRadar()
                state.addRadar( -118527, 47955, 473 )
                return this.getPath( '32135-05.htm' )
            case 69:
                state.clearRadar()
                state.addRadar( -117605, 48079, 472 )
                return this.getPath( '32135-05.htm' )
            case 70:
                state.clearRadar()
                state.addRadar( -117824, 48080, 476 )
                return this.getPath( '32135-05.htm' )
            case 71:
                state.clearRadar()
                state.addRadar( -118030, 47930, 465 )
                return this.getPath( '32135-05.htm' )
            case 72:
                state.clearRadar()
                state.addRadar( -119221, 46981, 380 )
                return this.getPath( '32135-05.htm' )
            case 73:
                state.clearRadar()
                state.addRadar( -118080, 42835, 720 )
                return this.getPath( '32135-05.htm' )
        }

        return ''
    }

    eventGuideOrc( id: number, state: QuestState ) {
        switch ( id ) {
            case 10:
                return this.getPath( '30602-04.htm' )
            case 11:
                return this.getPath( '30602-04a.htm' )
            case 12:
                return this.getPath( '30602-04b.htm' )
            case 13:
                return this.getPath( '30602-04c.htm' )
            case 14:
                return this.getPath( '30602-04d.htm' )
            case 15:
                return this.getPath( '30602-04e.htm' )
            case 16:
                return this.getPath( '30602-04f.htm' )
            case 17:
                return this.getPath( '30602-04g.htm' )
            case 18:
                return this.getPath( '30602-04h.htm' )
            case 19:
                return this.getPath( '30602-04i.htm' )
            case 31:
                state.clearRadar()
                state.addRadar( -45264, -112512, -235 )
                return this.getPath( '30602-05.htm' )
            case 32:
                state.clearRadar()
                state.addRadar( -46576, -117311, -242 )
                return this.getPath( '30602-05.htm' )
            case 33:
                state.clearRadar()
                state.addRadar( -47360, -113791, -237 )
                return this.getPath( '30602-05.htm' )
            case 34:
                state.clearRadar()
                state.addRadar( -47360, -113424, -235 )
                return this.getPath( '30602-05.htm' )
            case 35:
                state.clearRadar()
                state.addRadar( -45744, -117165, -236 )
                return this.getPath( '30602-05.htm' )
            case 36:
                state.clearRadar()
                state.addRadar( -46528, -109968, -250 )
                return this.getPath( '30602-05.htm' )
            case 37:
                state.clearRadar()
                state.addRadar( -45808, -110055, -255 )
                return this.getPath( '30602-05.htm' )
            case 38:
                state.clearRadar()
                state.addRadar( -45731, -113844, -237 )
                return this.getPath( '30602-05.htm' )
            case 39:
                state.clearRadar()
                state.addRadar( -45728, -113360, -237 )
                return this.getPath( '30602-05.htm' )
            case 40:
                state.clearRadar()
                state.addRadar( -45952, -114784, -199 )
                return this.getPath( '30602-05.htm' )
            case 41:
                state.clearRadar()
                state.addRadar( -45952, -114496, -199 )
                return this.getPath( '30602-05.htm' )
            case 42:
                state.clearRadar()
                state.addRadar( -45863, -112621, -200 )
                return this.getPath( '30602-05.htm' )
            case 43:
                state.clearRadar()
                state.addRadar( -45864, -112540, -199 )
                return this.getPath( '30602-05.htm' )
            case 44:
                state.clearRadar()
                state.addRadar( -43264, -112532, -220 )
                return this.getPath( '30602-05.htm' )
            case 45:
                state.clearRadar()
                state.addRadar( -43910, -115518, -194 )
                return this.getPath( '30602-05.htm' )
            case 46:
                state.clearRadar()
                state.addRadar( -43950, -115457, -194 )
                return this.getPath( '30602-05.htm' )
            case 47:
                state.clearRadar()
                state.addRadar( -44416, -111486, -222 )
                return this.getPath( '30602-05.htm' )
            case 48:
                state.clearRadar()
                state.addRadar( -43926, -111794, -222 )
                return this.getPath( '30602-05.htm' )
            case 49:
                state.clearRadar()
                state.addRadar( -43109, -113770, -221 )
                return this.getPath( '30602-05.htm' )
            case 50:
                state.clearRadar()
                state.addRadar( -43114, -113404, -221 )
                return this.getPath( '30602-05.htm' )
            case 51:
                state.clearRadar()
                state.addRadar( -46768, -113610, -3 )
                return this.getPath( '30602-05.htm' )
            case 52:
                state.clearRadar()
                state.addRadar( -46802, -114011, -112 )
                return this.getPath( '30602-05.htm' )
            case 53:
                state.clearRadar()
                state.addRadar( -46247, -113866, -21 )
                return this.getPath( '30602-05.htm' )
            case 54:
                state.clearRadar()
                state.addRadar( -46808, -113184, -112 )
                return this.getPath( '30602-05.htm' )
            case 55:
                state.clearRadar()
                state.addRadar( -45328, -114736, -237 )
                return this.getPath( '30602-05.htm' )
            case 56:
                state.clearRadar()
                state.addRadar( -44624, -111873, -238 )
                return this.getPath( '30602-05.htm' )
        }

        return ''
    }

    getApproachedForTalkIds(): Array<number> {
        return npcIds
    }

    getPathPrefix(): string {
        return 'overrides/html/npc/teleports/newbieGuide'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onApproachedForTalk( data : NpcApproachedForTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, 'Q00255_Tutorial' )
        if ( state ) {
            if ( data.characterNpcId === ADVENTURERS_GUIDE ) {
                return this.getPath( '32327.htm' )
            }

            let player = L2World.getPlayer( data.playerId )
            return this.talkGuide( player, state )
        }

        return super.onApproachedForTalk( data )
    }

    async onNpcEvent( data : NpcGeneralEvent ): Promise<string> {

        if ( data.eventName.endsWith( '.htm' ) ) {
            return this.getPath( data.eventName )
        }

        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        let player = L2World.getPlayer( data.playerId )

        if ( data.eventName.startsWith( 'teleport' ) ) {
            let [ unused, race ] : Array<string> = data.eventName.split( ' ' )
            return this.teleportRequest( npc, player, race )
        }

        let state: QuestState = this.getQuestState( data.playerId, true )
        let [ ask, reply ] = data.eventName.split( ';' ).map( value => parseInt( value, 10 ) )

        let outcome : string = await this.processEventReply( npc, player, ask, reply, state )
        if ( outcome ) {
            return outcome
        }

        switch ( npc.getId() ) {
            case NEWBIE_GUIDE_HUMAN:
                return this.eventGuideHuman( reply, state )

            case NEWBIE_GUIDE_ELF:
                return this.eventGuideElf( reply, state )

            case NEWBIE_GUIDE_DARK_ELF:
                return this.eventGuideDelf( reply, state )

            case NEWBIE_GUIDE_DWARF:
                return this.eventGuideDwarf( reply, state )

            case NEWBIE_GUIDE_ORC:
                return this.eventGuideOrc( reply, state )

            case NEWBIE_GUIDE_KAMAEL:
                return this.eventGuideKamael( reply, state )
        }

        return
    }

    async processEventReply( npc: L2Npc, player: L2PcInstance, ask: number, reply: number, state: QuestState ): Promise<string> {
        switch ( ask ) {
            case -7:
                switch ( reply ) {
                    case 1:
                        if ( player.getRace() !== npc.getRace() ) {
                            return this.getPath( '32135-003.htm' )
                        }

                        if ( player.getLevel() > 20
                                || player.getRace() !== Race.KAMAEL
                                || ClassId.getLevelByClassId( player.getClassId() ) !== 0 ) {
                            return this.getPath( '32135-002.htm' )
                        }

                        if ( player.getClassId() === ClassIdValues.maleSoldier.id ) {
                            if ( player.getLevel() <= 5 ) {
                                return this.getPath( '32135-kmf05.htm' )
                            }

                            if ( player.getLevel() <= 10 ) {
                                return this.getPath( '32135-kmf10.htm' )
                            }

                            if ( player.getLevel() <= 15 ) {
                                return this.getPath( '32135-kmf15.htm' )
                            }

                            return this.getPath( '32135-kmf20.htm' )
                        }

                        if ( player.getClassId() === ClassIdValues.femaleSoldier.id ) {
                            if ( player.getLevel() <= 5 ) {
                                return this.getPath( '32135-kff05.htm' )
                            }

                            if ( player.getLevel() <= 10 ) {
                                return this.getPath( '32135-kff10.htm' )
                            }

                            if ( player.getLevel() <= 15 ) {
                                return this.getPath( '32135-kff15.htm' )
                            }

                            return this.getPath( '32135-kff20.htm' )
                        }

                        if ( !player.isMageClass() ) {
                            if ( player.getLevel() <= 5 ) {
                                return this.getPath( `${ npc.getId() }${ SUFFIX_FIGHTER_5_LEVEL }` )
                            }

                            if ( player.getLevel() <= 10 ) {
                                return this.getPath( `${ npc.getId() }${ SUFFIX_FIGHTER_10_LEVEL }` )
                            }

                            if ( player.getLevel() <= 15 ) {
                                return this.getPath( `${ npc.getId() }${ SUFFIX_FIGHTER_15_LEVEL }` )
                            }

                            return this.getPath( `${ npc.getId() }${ SUFFIX_FIGHTER_20_LEVEL }` )
                        }

                        if ( player.getLevel() <= 7 ) {
                            return this.getPath( `${ npc.getId() }${ SUFFIX_MAGE_7_LEVEL }` )
                        }

                        if ( player.getLevel() <= 14 ) {
                            return this.getPath( `${ npc.getId() }${ SUFFIX_MAGE_14_LEVEL }` )
                        }

                        return this.getPath( `${ npc.getId() }${ SUFFIX_MAGE_20_LEVEL }` )

                    case 2:
                        if ( player.getLevel() <= 75 ) {
                            if ( player.getLevel() < 6 ) {
                                return this.getPath( 'buffs-low-level.htm' )
                            }

                            if ( !player.isMageClass() && ClassId.getLevelByClassId( player.getClassId() ) < 3 ) {
                                npc.setTarget( player )

                                // TODO : cast once and apply these in succession so that there is no wait time
                                await npc.doCast( WIND_WALK_FOR_BEGINNERS.getSkill() )
                                await npc.doCast( SHIELD_FOR_BEGINNERS.getSkill() )
                                await npc.doCast( ADVENTURERS_MAGIC_BARRIER.getSkill() )
                                await npc.doCast( BLESS_THE_BODY_FOR_BEGINNERS.getSkill() )
                                await npc.doCast( VAMPIRIC_RAGE_FOR_BEGINNERS.getSkill() )
                                await npc.doCast( REGENERATION_FOR_BEGINNERS.getSkill() )

                                if ( ( player.getLevel() >= 6 ) && ( player.getLevel() <= 39 ) ) {
                                    await npc.doCast( HASTE_FOR_BEGINNERS.getSkill() )
                                }
                                if ( ( player.getLevel() >= 40 ) && ( player.getLevel() <= 75 ) ) {
                                    await npc.doCast( ADVENTURERS_HASTE.getSkill() )
                                }
                                if ( ( player.getLevel() >= 16 ) && ( player.getLevel() <= 34 ) ) {
                                    await npc.doCast( LIFE_CUBIC_FOR_BEGINNERS.getSkill() )
                                }
                            }

                            if ( player.isMageClass() && ClassId.getLevelByClassId( player.getClassId() ) < 3 ) {
                                npc.setTarget( player )

                                // TODO : cast once and apply these in succession so that there is no wait time
                                await npc.doCast( WIND_WALK_FOR_BEGINNERS.getSkill() )
                                await npc.doCast( SHIELD_FOR_BEGINNERS.getSkill() )
                                await npc.doCast( ADVENTURERS_MAGIC_BARRIER.getSkill() )
                                await npc.doCast( BLESS_THE_SOUL_FOR_BEGINNERS.getSkill() )
                                await npc.doCast( ACUMEN_FOR_BEGINNERS.getSkill() )
                                await npc.doCast( CONCENTRATION_FOR_BEGINNERS.getSkill() )
                                await npc.doCast( EMPOWER_FOR_BEGINNERS.getSkill() )

                                if ( ( player.getLevel() >= 16 ) && ( player.getLevel() <= 34 ) ) {
                                    await npc.doCast( LIFE_CUBIC_FOR_BEGINNERS.getSkill() )
                                }
                            }

                            return
                        }

                        return this.getPath( 'buffs-big-level.htm' )

                    case 3:
                        if ( ( player.getLevel() <= 39 ) && ClassId.getLevelByClassId( player.getClassId() ) < 3 ) {
                            npc.setTarget( player )
                            await npc.doCast( BLESSING_OF_PROTECTION.getSkill() )

                            return
                        }

                        return this.getPath( 'pk-protection-002.htm' )

                    case 4:
                        let summon: L2Summon = player.getSummon()
                        if ( summon && !summon.isPet() ) {
                            if ( ( player.getLevel() < 6 ) || ( player.getLevel() > 75 ) ) {
                                return this.getPath( 'buffs-big-level.htm' )
                            }

                            npc.setTarget( player )

                            // TODO : cast once and apply these in succession so that there is no wait time
                            await npc.doCast( WIND_WALK_FOR_BEGINNERS.getSkill() )
                            await npc.doCast( SHIELD_FOR_BEGINNERS.getSkill() )
                            await npc.doCast( ADVENTURERS_MAGIC_BARRIER.getSkill() )
                            await npc.doCast( BLESS_THE_BODY_FOR_BEGINNERS.getSkill() )
                            await npc.doCast( VAMPIRIC_RAGE_FOR_BEGINNERS.getSkill() )
                            await npc.doCast( REGENERATION_FOR_BEGINNERS.getSkill() )
                            await npc.doCast( BLESS_THE_SOUL_FOR_BEGINNERS.getSkill() )
                            await npc.doCast( ACUMEN_FOR_BEGINNERS.getSkill() )
                            await npc.doCast( CONCENTRATION_FOR_BEGINNERS.getSkill() )
                            await npc.doCast( EMPOWER_FOR_BEGINNERS.getSkill() )

                            if ( ( player.getLevel() >= 6 ) && ( player.getLevel() <= 39 ) ) {
                                await npc.doCast( HASTE_FOR_BEGINNERS.getSkill() )
                            }
                            if ( ( player.getLevel() >= 40 ) && ( player.getLevel() <= 75 ) ) {
                                await npc.doCast( ADVENTURERS_HASTE.getSkill() )
                            }

                            return
                        }

                        return this.getPath( 'buffs-no-pet.htm' )
                }

                break

            case -1000:
                switch ( reply ) {
                    case 1:
                        if ( player.getLevel() > 5
                                && player.getLevel() < 20
                                && ClassId.getLevelByClassId( player.getClassId() ) === 0 ) {
                            if ( QuestHelper.getOneTimeQuestFlag( player.getObjectId(), 207 ) === 0 ) {
                                await QuestHelper.giveSingleItem( player, APPRENTICE_ADVENTURERS_WEAPON_EXCHANGE_COUPON, FIRST_COUPON_SIZE )

                                QuestHelper.markQuestCompleted( 207, player.getObjectId() )


                                state.incrementVariable( Variables.Value, 100 )

                                ShowHtml.showNpcOnScreenMessage( player, NpcStringIds.ACQUISITION_OF_WEAPON_EXCHANGE_COUPON_FOR_BEGINNERS_COMPLETE_N_GO_SPEAK_WITH_THE_NEWBIE_GUIDE, 2, 5000, '' )
                                return this.getPath( 'newbie-guide-002.htm' )
                            }

                            return this.getPath( 'newbie-guide-004.htm' )
                        }

                        return this.getPath( 'newbie-guide-003.htm' )

                    case 2:
                        if ( ClassId.getLevelByClassId( player.getClassId() ) === 1 ) {
                            if ( player.getLevel() < 40 ) {
                                if ( QuestHelper.getOneTimeQuestFlag( player.getObjectId(), 208 ) === 0 ) {
                                    await QuestHelper.giveSingleItem( player, ADVENTURERS_MAGIC_ACCESSORY_EXCHANGE_COUPON, SECOND_COUPON_SIZE )
                                    QuestHelper.markQuestCompleted( 208, player.getObjectId() )

                                    return this.getPath( 'newbie-guide-011.htm' )
                                }

                                return this.getPath( 'newbie-guide-013.htm' )
                            }

                            return this.getPath( 'newbie-guide-012.htm' )
                        }
                        return this.getPath( 'newbie-guide-012.htm' )
                }

                break

            case -303: {
                switch ( reply ) {
                    case 528:
                        if ( player.getLevel() > 5 ) {
                            if ( player.getLevel() < 20 && ClassId.getLevelByClassId( player.getClassId() ) === 0 ) {
                                MultisellCache.separateAndSend( weaponBuylistId, player, npc, false )
                            }

                            return this.getPath( 'newbie-guide-005.htm' )
                        }

                        return this.getPath( 'newbie-guide-005.htm' )

                    case 529:
                        if ( player.getLevel() > 5 ) {
                            if ( ( player.getLevel() < 40 ) && ClassId.getLevelByClassId( player.getClassId() ) === 1 ) {
                                MultisellCache.separateAndSend( accesoryBuylistId, player, npc, false )
                            }

                            return this.getPath( 'newbie-guide-014.htm' )
                        }

                        return this.getPath( 'newbie-guide-014.htm' )
                }
                break
            }
        }
    }

    async talkGuide( player: L2PcInstance, tutorialQS: QuestState ): Promise<string> {
        if ( QuestHelper.getOneTimeQuestFlag( player.getObjectId(), GUIDE_MISSION ) !== 0 ) {
            return
        }

        let questState: QuestState = this.getQuestState( player.getObjectId(), true )
        if ( ( tutorialQS.getMemoStateEx( 1 ) < 5 ) ) {
            if ( !player.isMageClass() ) {
                player.sendCopyData( Voice.TUTORIAL_VOICE_026_1000 )

                await QuestHelper.giveSingleItem( player, SOULSHOT_NO_GRADE_FOR_BEGINNERS, 200 )
                await QuestHelper.giveSingleItem( player, SCROLL_RECOVERY_NO_GRADE, 2 )

                tutorialQS.setMemoStateEx( 1, 5 )
                if ( player.getLevel() <= 1 ) {
                    await QuestHelper.addExpAndSp( player, 68, 50 )
                } else {
                    await QuestHelper.addExpAndSp( player, 0, 50 )
                }
            }

            if ( player.isMageClass() ) {
                if ( player.getClassId() === ClassIdValues.orcMage.id ) {
                    player.sendCopyData( Voice.TUTORIAL_VOICE_026_1000 )
                    await QuestHelper.giveSingleItem( player, SOULSHOT_NO_GRADE_FOR_BEGINNERS, 200 )
                } else {
                    player.sendCopyData( Voice.TUTORIAL_VOICE_027_1000 )
                    await QuestHelper.giveSingleItem( player, SPIRITSHOT_NO_GRADE_FOR_BEGINNERS, 100 )
                }

                await QuestHelper.giveSingleItem( player, SCROLL_RECOVERY_NO_GRADE, 2 )
                tutorialQS.setMemoStateEx( 1, 5 )

                if ( player.getLevel() <= 1 ) {
                    await QuestHelper.addExpAndSp( player, 68, 50 )
                } else {
                    await QuestHelper.addExpAndSp( player, 0, 50 )
                }
            }

            if ( player.getLevel() < 6 ) {
                return this.onBelowSixLevel( questState, player )
            }

            if ( player.getLevel() < 10 ) {
                return this.onBelowTenLevel( questState, player )
            }

            this.incrementValue( questState, 0 )
            return this.getPath( 'newbie-guide-06.htm' )
        }

        if ( player.getLevel() < 6 ) {
            return this.progressedBelowSixLevel( questState, player )
        }

        if ( player.getLevel() < 10 ) {
            return this.progressedBelowTenLevel( questState, player )
        }

        if ( player.getLevel() < 15 ) {
            return this.progressedBelowFifteenLevel( questState, player )
        }

        if ( player.getLevel() < 18 ) {
            return this.progressedBelowEighteenLevel( questState, player )
        }

        this.incrementValue( questState, 0 )

        if ( ClassId.getLevelByClassId( player.getClassId() ) === 1 ) {
            return this.getPath( 'newbie-guide-13.htm' )
        }

        return this.getPath( 'newbie-guide-14.htm' )
    }

    private async progressedBelowEighteenLevel( questState: QuestState, player: L2PcInstance ) {
        if ( ( questState.getVariable( Variables.Value ) % 100000000 ) / 10000000 === 1 ) {
            if ( ( questState.getVariable( Variables.Value ) % 1000000000 ) / 100000000 === 1 ) {
                return this.getPath( 'newbie-guide-13.htm' )
            }

            if ( player.getLevel() >= 17 ) {
                await QuestHelper.giveAdena( player, 22996 )
                await QuestHelper.addExpAndSp( player, 113712, 5518 )
            } else if ( player.getLevel() >= 16 ) {
                await QuestHelper.giveAdena( player, 10018 )
                await QuestHelper.addExpAndSp( player, 208133, 42237 )
            } else {
                await QuestHelper.giveAdena( player, 13648 )
                await QuestHelper.addExpAndSp( player, 285670, 58155 )
            }

            this.incrementValue( questState, 100000000 )
            return this.getPath( 'newbie-guide-12.htm' )
        }

        switch ( player.getRace() ) {
            case Race.HUMAN:
                questState.addRadar( -84057, 242832, -3729 )
                return this.getPath( 'newbie-guide-11a.htm' )

            case Race.ELF: {
                questState.addRadar( 45859, 50827, -3058 )
                return this.getPath( 'newbie-guide-11b.htm' )
            }

            case Race.DARK_ELF: {
                questState.addRadar( 11258, 14431, -4242 )
                return this.getPath( 'newbie-guide-11c.htm' )
            }

            case Race.ORC: {
                questState.addRadar( -45863, -112621, -200 )
                return this.getPath( 'newbie-guide-11d.htm' )
            }

            case Race.DWARF: {
                questState.addRadar( 116268, -177524, -914 )
                return this.getPath( 'newbie-guide-11e.htm' )
            }

            case Race.KAMAEL: {
                questState.addRadar( -125872, 38208, 1251 )
                return this.getPath( 'newbie-guide-11f.htm' )
            }
        }

        return
    }

    private async progressedBelowFifteenLevel( questState: QuestState, player: L2PcInstance ) {
        if ( ( questState.getVariable( Variables.Value ) % 1000000 ) / 100000 === 1 ) {
            if ( ( questState.getVariable( Variables.Value ) % 10000000 ) / 1000000 === 1 ) {
                return this.getPath( 'newbie-guide-15.htm' )
            }

            if ( player.getLevel() >= 14 ) {
                await QuestHelper.giveAdena( player, 13002 )
                await QuestHelper.addExpAndSp( player, 62876, 2891 )
            } else if ( player.getLevel() >= 13 ) {
                await QuestHelper.giveAdena( player, 23468 )
                await QuestHelper.addExpAndSp( player, 113137, 5161 )
            } else if ( player.getLevel() >= 12 ) {
                await QuestHelper.giveAdena( player, 31752 )
                await QuestHelper.addExpAndSp( player, 152653, 6914 )
            } else if ( player.getLevel() >= 11 ) {
                await QuestHelper.giveAdena( player, 38180 )
                await QuestHelper.addExpAndSp( player, 183128, 8242 )
            } else {
                await QuestHelper.giveAdena( player, 43054 )
                await QuestHelper.addExpAndSp( player, 206101, 9227 )
            }

            this.incrementValue( questState, 1000000 )

            switch ( player.getRace() ) {
                case Race.HUMAN:
                    questState.addRadar( -84057, 242832, -3729 )
                    return this.getPath( 'newbie-guide-11a.htm' )

                case Race.ELF:
                    questState.addRadar( 45859, 50827, -3058 )
                    return this.getPath( 'newbie-guide-11b.htm' )

                case Race.DARK_ELF:
                    questState.addRadar( 11258, 14431, -4242 )
                    return this.getPath( 'newbie-guide-11c.htm' )

                case Race.ORC:
                    questState.addRadar( -45863, -112621, -200 )
                    return this.getPath( 'newbie-guide-11d.htm' )

                case Race.DWARF:
                    questState.addRadar( 116268, -177524, -914 )
                    return this.getPath( 'newbie-guide-11e.htm' )

                case Race.KAMAEL:
                    questState.addRadar( -125872, 38208, 1251 )
                    return this.getPath( 'newbie-guide-11f.htm' )
            }
        }

        this.incrementValue( questState, 0 )

        switch ( player.getRace() ) {
            case Race.HUMAN:
                if ( !player.isMageClass() ) {
                    questState.addRadar( -71384, 258304, -3109 )
                    return this.getPath( 'newbie-guide-10a.htm' )
                }
                questState.addRadar( -91008, 248016, -3568 )
                return this.getPath( 'newbie-guide-10b.htm' )

            case Race.ELF:
                questState.addRadar( 47595, 51569, -2996 )
                return this.getPath( 'newbie-guide-10c.htm' )

            case Race.DARK_ELF:
                if ( !player.isMageClass() ) {
                    questState.addRadar( 10580, 17574, -4554 )
                    return this.getPath( 'newbie-guide-10d.htm' )
                }
                questState.addRadar( 10775, 14190, -4242 )
                return this.getPath( 'newbie-guide-10e.htm' )

            case Race.ORC:
                questState.addRadar( -46808, -113184, -112 )
                return this.getPath( 'newbie-guide-10f.htm' )

            case Race.DWARF:
                questState.addRadar( 115717, -183488, -1483 )
                return this.getPath( 'newbie-guide-10g.htm' )

            case Race.KAMAEL:
                questState.addRadar( -118080, 42835, 720 )
                return this.getPath( 'newbie-guide-10h.htm' )
        }

        return
    }

    private async progressedBelowTenLevel( questState: QuestState, player: L2PcInstance ) {
        if ( ( ( questState.getVariable( Variables.Value ) % 100000 ) / 10000 ) === 1 ) {
            return this.getPath( 'newbie-guide-09g.htm' )
        }

        if ( ( questState.getVariable( Variables.Value ) % 1000 ) / 100 === 1 ) {
            if ( ( questState.getVariable( Variables.Value ) % 10000 ) / 1000 === 1 ) {
                if ( player.getLevel() >= 9 ) {
                    await QuestHelper.giveAdena( player, 5563 )
                    await QuestHelper.addExpAndSp( player, 16851, 711 )
                } else if ( player.getLevel() >= 8 ) {
                    await QuestHelper.giveAdena( player, 9290 )
                    await QuestHelper.addExpAndSp( player, 28806, 1207 )
                } else if ( player.getLevel() >= 7 ) {
                    await QuestHelper.giveAdena( player, 11567 )
                    await QuestHelper.addExpAndSp( player, 36942, 1541 )
                } else {
                    await QuestHelper.giveAdena( player, 12928 )
                    await QuestHelper.addExpAndSp( player, 42191, 1753 )
                }

                this.incrementValue( questState, 10000 )

                switch ( player.getRace() ) {
                    case Race.HUMAN:
                        if ( !player.isMageClass() ) {
                            questState.addRadar( -71384, 258304, -3109 )
                            return this.getPath( 'newbie-guide-10a.htm' )
                        }

                        questState.addRadar( -91008, 248016, -3568 )
                        return this.getPath( 'newbie-guide-10b.htm' )

                    case Race.ELF:
                        questState.addRadar( 47595, 51569, -2996 )
                        return this.getPath( 'newbie-guide-10c.htm' )

                    case Race.DARK_ELF:
                        if ( !player.isMageClass() ) {
                            questState.addRadar( 10580, 17574, -4554 )
                            return this.getPath( 'newbie-guide-10d.htm' )
                        }

                        questState.addRadar( 10775, 14190, -4242 )
                        return this.getPath( 'newbie-guide-10e.htm' )

                    case Race.ORC:
                        questState.addRadar( -46808, -113184, -112 )
                        return this.getPath( 'newbie-guide-10f.htm' )

                    case Race.DWARF:
                        questState.addRadar( 115717, -183488, -1483 )
                        return this.getPath( 'newbie-guide-10g.htm' )

                    case Race.KAMAEL:
                        questState.addRadar( -118080, 42835, 720 )
                        return this.getPath( 'newbie-guide-10h.htm' )
                }

                return
            }

            this.incrementValue( questState, 0 )


            switch ( player.getRace() ) {
                case Race.HUMAN:
                    questState.addRadar( -82236, 241573, -3728 )
                    return this.getPath( 'newbie-guide-09a.htm' )
                case Race.ELF:
                    questState.addRadar( 42812, 51138, -2996 )
                    return this.getPath( 'newbie-guide-09b.htm' )
                case Race.DARK_ELF:
                    questState.addRadar( 7644, 18048, -4377 )
                    return this.getPath( 'newbie-guide-09c.htm' )
                case Race.ORC:
                    questState.addRadar( -46802, -114011, -112 )
                    return this.getPath( 'newbie-guide-09d.htm' )
                case Race.DWARF:
                    questState.addRadar( 116103, -178407, -948 )
                    return this.getPath( 'newbie-guide-09e.htm' )
                case Race.KAMAEL:
                    questState.addRadar( -119378, 49242, 22 )
                    return this.getPath( 'newbie-guide-09f.htm' )
            }

            return
        }

        this.incrementValue( questState, 0 )

        return this.getPath( 'newbie-guide-08.htm' )
    }

    private async progressedBelowSixLevel( questState: QuestState, player: L2PcInstance ) {
        if ( ( questState.getVariable( Variables.Value ) % 10 ) === 1 ) {
            if ( player.getLevel() >= 5 ) {
                await QuestHelper.giveAdena( player, 695 )
                await QuestHelper.addExpAndSp( player, 3154, 127 )
            } else if ( player.getLevel() >= 4 ) {
                await QuestHelper.giveAdena( player, 1041 )
                await QuestHelper.addExpAndSp( player, 4870, 195 )
            } else if ( player.getLevel() >= 3 ) {
                await QuestHelper.giveAdena( player, 1186 )
                await QuestHelper.addExpAndSp( player, 5675, 227 )
            } else {
                await QuestHelper.giveAdena( player, 1240 )
                await QuestHelper.addExpAndSp( player, 5970, 239 )
            }

            this.incrementValue( questState, 10 )

            return this.getPath( 'newbie-guide-08.htm' )
        }

        this.incrementValue( questState, 0 )

        switch ( player.getRace() ) {
            case Race.HUMAN:
                questState.addRadar( -84436, 242793, -3729 )
                return this.getPath( 'newbie-guide-07a.htm' )

            case Race.ELF:
                questState.addRadar( 42978, 49115, 2994 )
                return this.getPath( 'newbie-guide-07b.htm' )

            case Race.DARK_ELF:
                questState.addRadar( 25790, 10844, -3727 )
                return this.getPath( 'newbie-guide-07c.htm' )

            case Race.ORC:
                questState.addRadar( -47360, -113791, -237 )
                return this.getPath( 'newbie-guide-07d.htm' )

            case Race.DWARF:
                questState.addRadar( 112656, -174864, -611 )
                return this.getPath( 'newbie-guide-07e.htm' )

            case Race.KAMAEL:
                questState.addRadar( -119378, 49242, 22 )
                return this.getPath( 'newbie-guide-07f.htm' )
        }

        return
    }

    private async onBelowTenLevel( questState: QuestState, player: L2PcInstance ) {
        this.incrementValue( questState, 0 )

        if ( ( questState.getVariable( Variables.Value ) % 1000 ) / 100 === 1 ) {

            if ( ( ( questState.getVariable( Variables.Value ) % 10000 ) / 100 ) === 1 ) {
                if ( player.getLevel() >= 9 ) {
                    await QuestHelper.giveAdena( player, 5563 )
                    await QuestHelper.addExpAndSp( player, 16851, 711 )
                } else if ( player.getLevel() >= 8 ) {
                    await QuestHelper.giveAdena( player, 9290 )
                    await QuestHelper.addExpAndSp( player, 28806, 1207 )
                } else if ( player.getLevel() >= 7 ) {
                    await QuestHelper.giveAdena( player, 11567 )
                    await QuestHelper.addExpAndSp( player, 36942, 1541 )
                } else {
                    await QuestHelper.giveAdena( player, 12928 )
                    await QuestHelper.addExpAndSp( player, 42191, 1753 )
                }

                this.incrementValue( questState, 10000 )

                switch ( player.getRace() ) {
                    case Race.HUMAN:
                        if ( !player.isMageClass() ) {
                            questState.addRadar( -71384, 258304, -3109 )
                            return this.getPath( 'newbie-guide-05a.htm' )
                        }

                        questState.addRadar( -91008, 248016, -3568 )
                        return this.getPath( 'newbie-guide-05b.htm' )

                    case Race.ELF:
                        questState.addRadar( 47595, 51569, -2996 )
                        return this.getPath( 'newbie-guide-05c.htm' )

                    case Race.DARK_ELF:
                        if ( !player.isMageClass() ) {
                            questState.addRadar( 10580, 17574, -4554 )
                            return this.getPath( 'newbie-guide-05d.htm' )
                        }

                        questState.addRadar( 10775, 14190, -4242 )
                        return this.getPath( 'newbie-guide-05e.htm' )

                    case Race.ORC:
                        questState.addRadar( 46808, -113184, -112 )
                        return this.getPath( 'newbie-guide-05f.htm' )

                    case Race.DWARF:
                        questState.addRadar( 115717, -183488, -1483 )
                        return this.getPath( 'newbie-guide-05g.htm' )

                    case Race.KAMAEL:
                        questState.addRadar( 115717, -183488, -1483 )
                        return this.getPath( 'newbie-guide-05h.htm' )
                }

                return
            }

            switch ( player.getRace() ) {
                case Race.HUMAN:
                    questState.addRadar( -82236, 241573, -3728 )
                    return this.getPath( 'newbie-guide-04a.htm' )

                case Race.ELF:
                    questState.addRadar( 42812, 51138, -2996 )
                    return this.getPath( 'newbie-guide-04b.htm' )

                case Race.DARK_ELF:
                    questState.addRadar( 7644, 18048, -4377 )
                    return this.getPath( 'newbie-guide-04c.htm' )

                case Race.ORC:
                    questState.addRadar( -46802, -114011, -112 )
                    return this.getPath( 'newbie-guide-04d.htm' )

                case Race.DWARF:
                    questState.addRadar( 116103, -178407, -948 )
                    return this.getPath( 'newbie-guide-04e.htm' )

                case Race.KAMAEL:
                    questState.addRadar( -119378, 49242, 22 )
                    return this.getPath( 'newbie-guide-04f.htm' )
            }

            return
        }

        return this.getPath( 'newbie-guide-03.htm' )
    }

    private async onBelowSixLevel( questState: QuestState, player: L2PcInstance ) {
        if ( ( questState.getVariable( Variables.Value ) % 10 ) === 1 ) {
            if ( player.getLevel() >= 5 ) {
                await QuestHelper.giveAdena( player, 695 )
                await QuestHelper.addExpAndSp( player, 3154, 127 )
            } else if ( player.getLevel() >= 4 ) {
                await QuestHelper.giveAdena( player, 1041 )
                await QuestHelper.addExpAndSp( player, 4870, 195 )
            } else if ( player.getLevel() >= 3 ) {
                await QuestHelper.giveAdena( player, 1240 )
                await QuestHelper.addExpAndSp( player, 5970, 239 )
            }

            this.incrementValue( questState, 10 )

            return this.getPath( 'newbie-guide-02.htm' )
        }

        this.incrementValue( questState, 0 )

        switch ( player.getRace() ) {
            case Race.HUMAN:
                questState.addRadar( -84436, 242793, -3729 )
                return this.getPath( 'newbie-guide-01a.htm' )

            case Race.ELF:
                questState.addRadar( 42978, 49115, 2994 )
                return this.getPath( 'newbie-guide-01b.htm' )

            case Race.DARK_ELF:
                questState.addRadar( 25790, 10844, -3727 )
                return this.getPath( 'newbie-guide-01c.htm' )

            case Race.ORC:
                questState.addRadar( -47360, -113791, -237 )
                return this.getPath( 'newbie-guide-01d.htm' )

            case Race.DWARF:
                questState.addRadar( 112656, -174864, -611 )
                return this.getPath( 'newbie-guide-01e.htm' )

            case Race.KAMAEL:
                questState.addRadar( -119378, 49242, 22 )
                return this.getPath( 'newbie-guide-01f.htm' )
        }

        return
    }

    async teleportRequest( npc: L2Npc, player: L2PcInstance, race: string ): Promise<string> {
        if ( !race ) {
            return this.getPath( `${ npc.getId() }-teleport.htm` )
        }

        if ( player.getLevel() >= 20 ) {
            return this.getPath( 'teleport-big-level.htm' )
        }

        if ( [ 111, 112, 124 ].includes( player.getTransformationId() ) ) {
            return this.getPath( 'frog-teleport.htm' )
        }

        let location = teleportMap[ race ]
        if ( location ) {
            await player.teleportToLocation( location, false, location.getInstanceId() )
        }
    }

    incrementValue( state: QuestState, value: number ) : void {
        if ( !state.getVariable( Variables.Started ) ) {
            state.setVariable( Variables.Started, true )
            state.setVariable( Variables.Value, value )

            return
        }

        state.incrementVariable( Variables.Value, value )
    }
}