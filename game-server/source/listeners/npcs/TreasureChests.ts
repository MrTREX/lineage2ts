import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { TreasureChestDrops, TreasureChestItemDrop } from './data/TreasureChestData'
import { AttackableAttackedEvent, NpcGeneralEvent, NpcSpawnEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { QuestHelper } from '../helpers/QuestHelper'
import { ConfigManager } from '../../config/ConfigManager'
import _ from 'lodash'

const spawnDuration = 14400000
const attackSpawnTime = 5000
const playerLevelLimit = 78
const maestroKeySkillId = 22271

const despawnName = 'one'
const spawnTimeLapsed = 'two'
const maestroKeyUsed = 'keyUsed'
const npcIds : Array<number> = _.keys( TreasureChestDrops ).map( value => _.parseInt( value ) )

export class TreasureChests extends ListenerLogic {
    constructor() {
        super( 'TreasureChests', '/listeners/npcs/TreasureChests.ts' )
    }

    getSpawnIds(): Array<number> {
        return npcIds
    }

    getAttackableAttackIds(): Array<number> {
        return npcIds
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        switch ( data.eventName ) {
            case despawnName:
            case spawnTimeLapsed:
                let npc = L2World.getObjectById( data.characterId ) as L2Npc
                if ( npc ) {
                    await npc.deleteMe()
                }

                return
        }
    }

    async onSpawnEvent( data: NpcSpawnEvent ): Promise<void> {
        this.startQuestTimer( spawnTimeLapsed, spawnDuration, data.characterId, 0 )
    }

    async onAttackableAttackedEvent( data: AttackableAttackedEvent ): Promise<void> {
        if ( NpcVariablesManager.get( data.targetId, maestroKeyUsed ) ) {
            return
        }

        let player = L2World.getPlayer( data.attackerPlayerId )
        let levelDifference : number = player.getLevel() < playerLevelLimit ? 6 : 5
        if ( data.skillId === maestroKeySkillId ) {
            NpcVariablesManager.set( data.targetId, maestroKeyUsed, true )
            this.startQuestTimer( despawnName, attackSpawnTime, data.targetId )

            let npc = L2World.getObjectById( data.targetId ) as L2Npc
            if ( ( npc.getLevel() - levelDifference ) > player.getLevel() ) {
                QuestHelper.addSkillCastDesire( npc, player,4143, ( npc.getLevel() % 10 ) + 1, 1000000 )
                return
            }

            if ( _.random( 100 ) < 10 ) {
                await npc.doDie( null )

                _.each( TreasureChestDrops[ npc.getId() ], ( dropData : TreasureChestItemDrop ) => {
                    let [ itemId, chance, amount = 1 ] = dropData
                    if ( _.random( 10000 ) < ( chance * ConfigManager.rates.getDeathDropChanceMultiplier() ) ) {
                        let correctedAmount : number = amount * _.random( ConfigManager.rates.getDeathDropAmountMinMultiplier(), ConfigManager.rates.getDeathDropAmountMaxMultiplier(), true )
                        npc.dropSingleItem( itemId, Math.floor( correctedAmount ), data.attackerPlayerId )
                    }
                } )

                return
            }

            QuestHelper.addSkillCastDesire( npc, player,4143, ( npc.getLevel() % 10 ) + 1, 1000000 )
            return
        }

        if ( _.random( 100 ) < 30 ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.IF_YOU_HAVE_A_MAESTROS_KEY_YOU_CAN_USE_IT_TO_OPEN_THE_TREASURE_CHEST ) )
        }

        return
    }
}