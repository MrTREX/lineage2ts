import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { ConfigManager } from '../../config/ConfigManager'
import { PlayerActionOverride } from '../../gameService/values/PlayerConditions'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import { EventType, NpcApproachedForTalkEvent, NpcGeneralEvent, NpcManorBypassEvent } from '../../gameService/models/events/EventType'
import { CastleManorManager } from '../../gameService/instancemanager/CastleManorManager'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { L2MerchantInstance } from '../../gameService/models/actor/instance/L2MerchantInstance'
import { BuyListSeed } from '../../gameService/packets/send/BuyListSeed'
import { ExShowSellCropList } from '../../gameService/packets/send/ExShowSellCropList'
import { ExShowSeedInfo } from '../../gameService/packets/send/ExShowSeedInfo'
import { ExShowCropInfo } from '../../gameService/packets/send/ExShowCropInfo'
import { ExShowManorDefaultInfo } from '../../gameService/packets/send/ExShowManorDefaultInfo'
import { ExShowProcureCropDetail } from '../../gameService/packets/send/ExShowProcureCropDetail'
import { L2World } from '../../gameService/L2World'
import _ from 'lodash'

const npcIds : Array<number> = [
    35644,
    35645,
    35319,
    35366,
    36456,
    35512,
    35558,
    35229,
    35230,
    35231,
    35277,
    35103,
    35145,
    35187
]

export class ManorManager extends ListenerLogic {
    constructor() {
        super( 'ManorManager', 'listeners/npcs/ManorManager.ts' )
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getApproachedForTalkIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onNpcEvent( data : NpcGeneralEvent ): Promise<string> {
        let event = data.eventName
        switch ( event ) {
            case 'manager-help-01.htm':
            case 'manager-help-02.htm':
            case 'manager-help-03.htm':
                return this.getPath( event )
        }
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/ManorManager'
    }

    async onApproachedForTalk( data : NpcApproachedForTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let npc : L2Npc = L2World.getObjectById( data.characterId ) as L2Npc

        if ( ConfigManager.general.allowManor() ) {
            let castleId = npc.getTemplate().getParameters()[ 'manor_id' ] as string
            if ( !player.hasActionOverride( PlayerActionOverride.CastleRights )
                    && player.isClanLeader()
                    && _.parseInt( castleId ) === player.getClan().getCastleId() ) {
                return this.getPath( 'manager-lord.htm' )
            }

            return this.getPath( 'manager.htm' )
        }

        return 'data/html/npcdefault.htm'
    }

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.NpcId,
                eventType: EventType.NpcManorBypass,
                method: this.onNpcManorBypass.bind( this ),
                ids: npcIds
            }
        ]
    }

    async onNpcManorBypass( data : NpcManorBypassEvent ) : Promise<void> {
        let player = L2World.getPlayer( data.playerId )
        let npc : L2Npc = L2World.getObjectById( data.characterId ) as L2Npc
        let { manorId, requestId, isNextPeriod } = data

        if ( CastleManorManager.isUnderMaintenance() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THE_MANOR_SYSTEM_IS_CURRENTLY_UNDER_MAINTENANCE ) )
            return
        }

        let templateId = _.parseInt( npc.getTemplate().getParameters()[ 'manor_id' ] as string )
        let castleId = manorId === -1 ? templateId : manorId

        switch ( requestId ) {
            case 1: // Seed purchase
                if ( templateId !== castleId ) {
                    let message = new SystemMessageBuilder( SystemMessageIds.HERE_YOU_CAN_BUY_ONLY_SEEDS_OF_S1_MANOR )
                            .addCastleId( templateId )
                            .getBuffer()

                    player.sendOwnedData( message )
                    return
                }

                player.sendOwnedData( BuyListSeed( player.getObjectId(), castleId ) )
                break

            case 2: // Crop sales
                player.sendOwnedData( ExShowSellCropList( player, castleId ) )
                break

            case 3: // Seed info
                player.sendOwnedData( ExShowSeedInfo( castleId, isNextPeriod, false ) )
                break

            case 4: // Crop info
                player.sendOwnedData( ExShowCropInfo( castleId, isNextPeriod, false ) )
                break

            case 5: // Basic info
                player.sendOwnedData( ExShowManorDefaultInfo( false ) )
                break

            case 6: // Buy harvester
                if ( npc.isMerchant() ) {
                    ( npc as L2MerchantInstance ).showBuyWindow( player, 300000 + npc.getId() )
                }

                break

            case 9: // Edit sales (Crop sales)
                player.sendOwnedData( ExShowProcureCropDetail( templateId ) )
                break
        }
    }
}