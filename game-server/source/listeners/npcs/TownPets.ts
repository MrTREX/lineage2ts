import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { ConfigManager } from '../../config/ConfigManager'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { NpcSpawnEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { L2Attackable } from '../../gameService/models/actor/L2Attackable'
import { AIIntent } from '../../gameService/aicontroller/enums/AIIntent'
import { RandomWalkTraitAction } from '../../gameService/aicontroller/traits/npc/RandomWalkTrait'
import { DataManager } from '../../data/manager'
import { L2ManualSpawn } from '../../gameService/models/spawns/type/L2ManualSpawn'
import { PointGeometry } from '../../gameService/models/drops/PointGeometry'
import { GeometryId } from '../../gameService/enums/GeometryId'
import { SpawnMakerCache } from '../../gameService/cache/SpawnMakerCache'
import { ILocational, Location } from '../../gameService/models/Location'
import { AITraitCache } from '../../gameService/cache/AITraitCache'
import { TraitSettings } from '../../gameService/aicontroller/interface/IAITrait'

const npcIds: Array<number> = [
    30731,
    30827,
    30828,
    30829,
    30830,
    30831,
    30869,
    31067,
    31265,
    31309,
    31954,
    36478
]

class TownPetMovementAction extends RandomWalkTraitAction {

    getInterval(): number {
        return 5000
    }

    getName(): string {
        return 'TownPetMovementAction'
    }

    // TODO : consider that pet must walk around pet manager, not through
    // TODO : consider using multiplier if radius is too large
    performMove( npc: L2Attackable ): void {
        return this.moveInGeometry( npc )
    }

    hasRandomWalk( npc: L2Npc ) : boolean {
        return !npc.isDead() && npc.isMoveCapable()
    }

    getMovementGeometry(): GeometryId {
        return GeometryId.PetMoveSmall
    }

    getMovementOrigin( npc: L2Npc ): ILocational {
        return ( npc.getNpcSpawn() as PetSpawn ).origin
    }
}

class PetSpawn extends L2ManualSpawn {
    origin: Location

    protected resetNpc( npc: L2Npc ) : void {
        super.resetNpc( npc )

        npc.setRunning()
    }

    setOrigin( x: number, y: number ) : void {
        this.origin = new Location( x, y, 0 )
    }
}

const defaultTraits : TraitSettings = {
    ...AITraitCache.createEmptyTraits(),
    [ AIIntent.WAITING ]: new TownPetMovementAction(),
}

export class TownPets extends ListenerLogic {
    constructor() {
        super( 'TownPets', 'listeners/npcs/TownPets.ts' )
    }

    getSpawnIds(): Array<number> {
        if ( ConfigManager.general.allowPetWalkers() ) {
            return npcIds
        }

        return null
    }

    async onSpawnEvent( data: NpcSpawnEvent ): Promise<void> {
        let template = DataManager.getNpcData().getTemplate( data.npcId )
        let petNpcId = template.getParameters()[ 'silhouette' ] as number
        if ( !petNpcId ) {
            return
        }

        let petTemplate = DataManager.getNpcData().getTemplate( petNpcId )
        if ( !petTemplate ) {
            return
        }

        let petManager = L2World.getObjectById( data.characterId )
        if ( !petManager ) {
            return
        }

        AITraitCache.setNpcTraits( petNpcId, defaultTraits )

        let geometry = PointGeometry.acquire( GeometryId.PetMoveSmall )
        geometry.prepareNextPoint()

        let x: number = geometry.getX() + petManager.getX()
        let y: number = geometry.getY() + petManager.getY()

        geometry.release()

        let spawn = PetSpawn.fromParameters<PetSpawn>( petTemplate, 1, x, y, petManager.getZ(), petManager.getHeading(), petManager.getInstanceId(), false, false, PetSpawn )

        spawn.setOrigin( petManager.getX(), petManager.getY() )
        spawn.startSpawn()

        SpawnMakerCache.addDynamicSpawn( spawn )
    }
}