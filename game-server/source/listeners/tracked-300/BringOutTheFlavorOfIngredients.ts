import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const ROLLAND = 30069
const ANTIDOTE = 1831
const RITRON_FRUIT = 5895
const MOON_FLOWER = 5896
const LEECH_FLUIDS = 5897
const RITRON_RECIPE = 5959
const RITRON_DESSERT = 5960
const minimumLevel = 24

type MonsterReward = [ number, number, number ] // itemId, chance, amount
const monsterRewards: { [ npcId: number ]: MonsterReward } = {
    20205: [ RITRON_FRUIT, 0.1, 4 ], // Dire Wolf
    20206: [ MOON_FLOWER, 0.5, 20 ], // Kadif Werewolf
    20225: [ LEECH_FLUIDS, 0.5, 10 ], // Giant Mist Leech
}

export class BringOutTheFlavorOfIngredients extends ListenerLogic {
    constructor() {
        super( 'Q00380_BringOutTheFlavorOfIngredients', 'listeners/tracked-300/BringOutTheFlavorOfIngredients.ts' )
        this.questId = 380
        this.questItemIds = [
            RITRON_FRUIT,
            MOON_FLOWER,
            LEECH_FLUIDS,
        ]
    }

    checkPartyMember( state: QuestState ): boolean {
        return state.getCondition() < 4
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewards ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00380_BringOutTheFlavorOfIngredients'
    }

    getQuestStartIds(): Array<number> {
        return [ ROLLAND ]
    }

    getTalkIds(): Array<number> {
        return [ ROLLAND ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let [ itemId, chance, amount ]: MonsterReward = monsterRewards[ data.npcId ]

        if ( Math.random() > QuestHelper.getAdjustedChance( itemId, chance, data.isChampion ) ) {
            return
        }

        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), -1, 3, L2World.getObjectById( data.targetId ) )
        if ( !state ) {
            return
        }

        let player = state.getPlayer()
        await QuestHelper.rewardSingleQuestItem( player, itemId, amount, data.isChampion )

        if ( QuestHelper.getQuestItemsCount( player, RITRON_FRUIT ) >= 3
                && QuestHelper.getQuestItemsCount( player, MOON_FLOWER ) >= 20
                && QuestHelper.getQuestItemsCount( player, LEECH_FLUIDS ) >= 10 ) {
            state.setConditionWithSound( state.getCondition() + 1, true )
            return
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30069-03.htm':
            case '30069-04.htm':
            case '30069-06.html':
                break

            case '30069-05.htm':
                state.startQuest()
                break

            case '30069-13.html':
                if ( state.isCondition( 9 ) ) {
                    await QuestHelper.rewardSingleItem( player, RITRON_RECIPE, 1 )
                    await state.exitQuest( true, true )

                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30069-02.htm' : '30069-01.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                        if ( QuestHelper.getQuestItemsCount( player, ANTIDOTE ) >= 2
                                && QuestHelper.getQuestItemsCount( player, RITRON_FRUIT ) >= 4
                                && QuestHelper.getQuestItemsCount( player, MOON_FLOWER ) >= 20
                                && QuestHelper.getQuestItemsCount( player, LEECH_FLUIDS ) >= 10 ) {
                            await QuestHelper.takeSingleItem( player, ANTIDOTE, 2 )
                            await QuestHelper.takeMultipleItems( player, -1, RITRON_FRUIT, MOON_FLOWER, LEECH_FLUIDS )

                            state.setConditionWithSound( 5, true )
                            return this.getPath( '30069-08.html' )
                        }

                        return this.getPath( '30069-07.html' )

                    case 5:
                        state.setConditionWithSound( 6, true )
                        return this.getPath( '30069-09.html' )

                    case 6:
                        state.setConditionWithSound( 7, true )
                        return this.getPath( '30069-10.html' )

                    case 7:
                        state.setConditionWithSound( 8, true )
                        return this.getPath( '30069-11.html' )

                    case 8:
                        await QuestHelper.rewardSingleItem( player, RITRON_DESSERT, 1 )
                        if ( _.random( 100 ) < 56 ) {
                            await state.exitQuest( true, true )

                            return this.getPath( '30069-15.html' )
                        }

                        state.setConditionWithSound( 9, true )
                        return this.getPath( '30069-12.html' )

                    case 9:
                        return this.getPath( '30069-12.html' )
                }

                break

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}