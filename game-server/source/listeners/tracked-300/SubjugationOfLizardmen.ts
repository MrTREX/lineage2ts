import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestStateValues } from '../../gameService/models/quest/State'

const HIGH_PRIESTESS_LEVIAN = 30037
const PRIEST_ADONIUS = 30375
const GUARD_WEISZ = 30385
const CHEST_OF_BIFRONS = 30989
const TRADE_CARGO = 4255
const AGNESS_HOLY_SYMBOL = 4256
const AGNESS_ROSARY = 4257
const SINISTER_TOTEM = 4258
const FELIM_LIZARDMAN = 20008
const FELIM_LIZARDMAN_SCOUT = 20010
const FELIM_LIZARDMAN_WARRIOR = 20014
const LANGK_LIZARDMAN_WARRIOR = 20024
const LANGK_LIZARDMAN_SCOUT = 20027
const LANGK_LIZARDMAN = 20030
const SERPENT_DEMON_BIFRONS = 25146
const minimumLevel = 17
const tradeCargoLimit = 30

export class SubjugationOfLizardmen extends ListenerLogic {
    constructor() {
        super( 'Q00340_SubjugationOfLizardmen', 'listeners/tracked-300/SubjugationOfLizardmen.ts' )
        this.questId = 340
        this.questItemIds = [
            TRADE_CARGO,
            AGNESS_HOLY_SYMBOL,
            AGNESS_ROSARY,
            SINISTER_TOTEM,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            FELIM_LIZARDMAN,
            FELIM_LIZARDMAN_SCOUT,
            FELIM_LIZARDMAN_WARRIOR,
            LANGK_LIZARDMAN_WARRIOR,
            LANGK_LIZARDMAN_SCOUT,
            LANGK_LIZARDMAN,
            SERPENT_DEMON_BIFRONS,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00340_SubjugationOfLizardmen'
    }

    getQuestStartIds(): Array<number> {
        return [ GUARD_WEISZ ]
    }

    getTalkIds(): Array<number> {
        return [
            GUARD_WEISZ,
            HIGH_PRIESTESS_LEVIAN,
            PRIEST_ADONIUS,
            CHEST_OF_BIFRONS,
        ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case FELIM_LIZARDMAN:
            case FELIM_LIZARDMAN_SCOUT:
            case FELIM_LIZARDMAN_WARRIOR:
                if ( !state.isMemoState( 1 ) ) {
                    return
                }

                let chance = data.npcId === FELIM_LIZARDMAN_WARRIOR ? 0.68 : 0.63
                if ( Math.random() > QuestHelper.getAdjustedChance( TRADE_CARGO, chance, data.isChampion ) ) {
                    return
                }

                await QuestHelper.rewardUpToLimit( player, TRADE_CARGO, 1, 30, data.isChampion )
                return

            case LANGK_LIZARDMAN_WARRIOR:
                if ( !state.isMemoState( 3 ) ) {
                    return
                }

                await this.onLangkLizardmanKilled( player, 0.19, data.isChampion )
                return

            case LANGK_LIZARDMAN_SCOUT:
            case LANGK_LIZARDMAN:
                if ( !state.isMemoState( 3 ) ) {
                    return
                }

                await this.onLangkLizardmanKilled( player, 0.18, data.isChampion )
                return

            case SERPENT_DEMON_BIFRONS:
                QuestHelper.addSpawnAtLocation( CHEST_OF_BIFRONS, npc, true, 30000 )
                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30385-03.htm':
                state.startQuest()
                state.setMemoState( 1 )
                break

            case '30385-04.html':
            case '30385-08.html':
                break

            case '30385-07.html':
                await QuestHelper.takeSingleItem( player, TRADE_CARGO, -1 )
                state.setMemoState( 2 )
                state.setConditionWithSound( 2, true )
                break

            case '30385-09.html':
                if ( QuestHelper.getQuestItemsCount( player, TRADE_CARGO ) >= tradeCargoLimit ) {
                    await QuestHelper.giveAdena( player, 4090, true )
                    await QuestHelper.takeSingleItem( player, TRADE_CARGO, -1 )

                    state.setMemoState( 1 )

                    break
                }

                return

            case '30385-10.html':
                if ( QuestHelper.getQuestItemsCount( player, TRADE_CARGO ) >= tradeCargoLimit ) {
                    await QuestHelper.giveAdena( player, 4090, true )
                    await QuestHelper.takeSingleItem( player, TRADE_CARGO, -1 )
                    await state.exitQuest( false, true )

                    break
                }

                return

            case '30037-02.html':
                state.setMemoState( 5 )
                state.setConditionWithSound( 5, true )
                break

            case '30375-02.html':
                state.setMemoState( 3 )
                state.setConditionWithSound( 3, true )
                break

            case '30989-02.html':
                if ( state.isMemoState( 5 ) ) {
                    await QuestHelper.giveSingleItem( player, SINISTER_TOTEM, 1 )

                    state.setMemoState( 6 )
                    state.setConditionWithSound( 6, true )

                    break
                }

                return this.getPath( '30989-03.html' )
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== GUARD_WEISZ ) {
                    break
                }

                return this.getPath( player.getLevel() >= minimumLevel ? '30385-02.htm' : '30385-01.htm' )

            case QuestStateValues.STARTED:
                const memoState = state.getMemoState()
                switch ( data.characterNpcId ) {
                    case GUARD_WEISZ:
                        switch ( memoState ) {
                            case 1:
                                if ( QuestHelper.getQuestItemsCount( player, TRADE_CARGO ) < tradeCargoLimit ) {
                                    return this.getPath( '30385-05.html' )
                                }

                                return this.getPath( '30385-06.html' )

                            case 2:
                                return this.getPath( '30385-11.html' )

                            case 3:
                            case 4:
                            case 5:
                            case 6:
                                return this.getPath( '30385-12.html' )

                            case 7:
                                await QuestHelper.giveAdena( player, 14700, true )
                                await state.exitQuest( false, true )

                                return this.getPath( '30385-13.html' )
                        }

                        break

                    case HIGH_PRIESTESS_LEVIAN:
                        switch ( memoState ) {
                            case 4:
                                return this.getPath( '30037-01.html' )

                            case 5:
                                return this.getPath( '30037-03.html' )

                            case 6:
                                await QuestHelper.takeSingleItem( player, SINISTER_TOTEM, 1 )

                                state.setMemoState( 7 )
                                state.setConditionWithSound( 7, true )

                                return this.getPath( '30037-04.html' )

                            case 7:
                                return this.getPath( '30037-05.html' )
                        }

                        break

                    case PRIEST_ADONIUS:
                        switch ( memoState ) {
                            case 2:
                                return this.getPath( '30375-01.html' )

                            case 3:
                                if ( QuestHelper.hasQuestItems( player, AGNESS_HOLY_SYMBOL, AGNESS_ROSARY ) ) {
                                    await QuestHelper.takeMultipleItems( player, -1, AGNESS_HOLY_SYMBOL, AGNESS_ROSARY )

                                    state.setMemoState( 4 )
                                    state.setConditionWithSound( 4, true )

                                    return this.getPath( '30375-04.html' )
                                }

                                return this.getPath( '30375-03.html' )

                            case 4:
                                return this.getPath( '30375-05.html' )
                        }

                        if ( memoState >= 5 ) {
                            return this.getPath( '30375-06.html' )
                        }

                        break

                    case CHEST_OF_BIFRONS:
                        if ( memoState === 5 ) {
                            return this.getPath( '30989-01.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === GUARD_WEISZ ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async onLangkLizardmanKilled( player: L2PcInstance, mainChance: number, isFromChampion: boolean ): Promise<void> {
        if ( Math.random() < QuestHelper.getAdjustedChance( AGNESS_HOLY_SYMBOL, mainChance, isFromChampion )
                && !QuestHelper.hasQuestItem( player, AGNESS_HOLY_SYMBOL ) ) {
            await QuestHelper.giveSingleItem( player, AGNESS_HOLY_SYMBOL, 1 )

            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
            return
        }

        if ( Math.random() < QuestHelper.getAdjustedChance( AGNESS_ROSARY, mainChance, isFromChampion )
                && QuestHelper.hasQuestItem( player, AGNESS_HOLY_SYMBOL )
                && !QuestHelper.hasQuestItem( player, AGNESS_ROSARY ) ) {
            await QuestHelper.giveSingleItem( player, AGNESS_ROSARY, 1 )

            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
            return
        }
    }
}