import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { ItemTypes } from '../../gameService/values/InventoryValues'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const XENOVIA = 30912
const DOROTHY = 30970
const ORPHEUS = 30971
const MEDIUM_JAR = 30973
const IMPERIAL_DIAMOND = 3456
const VICTIMS_ARM_BONE = 4274
const VICTIMS_THIGH_BONE = 4275
const VICTIMS_SKULL = 4276
const VICTIMS_RIB_BONE = 4277
const VICTIMS_SPINE = 4278
const USELESS_BONE_PIECES = 4280
const POWDER_TO_SUMMON_DEAD_SOULS = 4281
const BILL_OF_IASON_HEINE = 4407
const minimumLevel = 35
const CROKIAN = 20789
const CROKIAN_WARRIOR = 20791

type MonsterItem = [ number, number ] // chance value, itemId
const monsterRewardItems: Array<MonsterItem> = [
    [ 0.05, VICTIMS_ARM_BONE ],
    [ 0.11, VICTIMS_THIGH_BONE ],
    [ 0.17, VICTIMS_SKULL ],
    [ 0.23, VICTIMS_RIB_BONE ],
    [ 0.29, VICTIMS_SPINE ],
]

export class MethodToRaiseTheDead extends ListenerLogic {
    constructor() {
        super( 'Q00345_MethodToRaiseTheDead', 'listeners/tracked-300/MethodToRaiseTheDead.ts' )
        this.questId = 345
        this.questItemIds = [
            VICTIMS_ARM_BONE,
            VICTIMS_THIGH_BONE,
            VICTIMS_SKULL,
            VICTIMS_RIB_BONE,
            VICTIMS_SPINE,
            USELESS_BONE_PIECES,
            POWDER_TO_SUMMON_DEAD_SOULS,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [ CROKIAN, CROKIAN_WARRIOR ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00345_MethodToRaiseTheDead'
    }

    getQuestStartIds(): Array<number> {
        return [ DOROTHY ]
    }

    getTalkIds(): Array<number> {
        return [ DOROTHY, ORPHEUS, MEDIUM_JAR, XENOVIA ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), 1, 3, L2World.getObjectById( data.targetId ) as L2Npc )
        if ( !state ) {
            return
        }

        let chance = Math.random()
        let player = state.getPlayer()

        let reward: MonsterItem = _.find( monsterRewardItems, ( item: MonsterItem ): boolean => {
            let [ itemChance, itemId ] = item
            return chance <= QuestHelper.getAdjustedChance( itemId, itemChance, data.isChampion )
        } )

        if ( reward ) {
            let [ , itemId ] = reward
            if ( !QuestHelper.hasQuestItems( player, itemId ) ) {
                await QuestHelper.giveSingleItem( player, itemId, 1 )
            } else {
                await QuestHelper.rewardSingleQuestItem( player, USELESS_BONE_PIECES, 1, data.isChampion )
            }


            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
            return
        }

        if ( chance <= QuestHelper.getAdjustedChance( USELESS_BONE_PIECES, 60, data.isChampion ) ) {
            await QuestHelper.rewardSingleQuestItem( player, USELESS_BONE_PIECES, 1, data.isChampion )
            return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30970-02.htm':
                state.startQuest()
                break

            case '30970-03.html':
                state.setMemoState( 1 )
                break

            case '30970-07.html':
                if ( QuestHelper.hasQuestItems( player, VICTIMS_ARM_BONE, VICTIMS_THIGH_BONE, VICTIMS_SKULL, VICTIMS_RIB_BONE, VICTIMS_SPINE ) ) {
                    state.setMemoState( 2 )
                    state.setConditionWithSound( 2, true )

                    break
                }
                return

            case '30971-02.html':
            case '30912-05.html':
                break

            case '30971-03.html':
                let itemCount = QuestHelper.getQuestItemsCount( player, USELESS_BONE_PIECES )
                if ( itemCount > 0 ) {
                    let adenaAmount = itemCount * 104
                    await QuestHelper.takeSingleItem( player, USELESS_BONE_PIECES, -1 )
                    await QuestHelper.giveAdena( player, adenaAmount, true )

                    break
                }

                return

            case '30973-02.html':
                let memoStateEx = state.getMemoStateEx( 1 )

                if ( memoStateEx === 1 ) {
                    break
                }

                if ( memoStateEx === 2 ) {
                    return this.getPath( '30973-04.html' )
                }

                if ( memoStateEx === 3 ) {
                    return this.getPath( '30973-06.html' )
                }

                return

            case '30973-03.html':
                if ( state.isMemoState( 7 ) && state.getMemoStateEx( 1 ) === 1 ) {
                    state.setMemoState( 8 )
                    state.setConditionWithSound( 6, true )

                    break
                }

                return

            case '30973-05.html':
                if ( state.isMemoState( 7 ) && state.getMemoStateEx( 1 ) === 2 ) {
                    state.setMemoState( 8 )
                    state.setConditionWithSound( 6, true )

                    break
                }

                return

            case '30973-07.html':
                if ( state.isMemoState( 7 ) && state.getMemoStateEx( 1 ) === 3 ) {
                    state.setMemoState( 8 )
                    state.setConditionWithSound( 7, true )

                    break
                }

                return

            case '30912-02.html':
                if ( !state.isMemoState( 2 ) ) {
                    return
                }

                break

            case '30912-03.html':
                if ( state.isMemoState( 2 ) ) {

                    if ( player.getAdena() >= 1000 ) {
                        await QuestHelper.giveSingleItem( player, POWDER_TO_SUMMON_DEAD_SOULS, 1 )
                        await QuestHelper.takeSingleItem( player, ItemTypes.Adena, 1000 )

                        state.setMemoState( 3 )
                        state.setConditionWithSound( 3, true )

                        break
                    }

                    return this.getPath( '30912-04.html' )
                }

                return


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30970-01.htm' : '30970-04.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case DOROTHY:
                        switch ( state.getMemoState() ) {
                            case 0:
                                state.setMemoState( 1 )
                                return this.getPath( '30970-03.html' )

                            case 1:
                                let canProceed: boolean = QuestHelper.hasQuestItems( player, VICTIMS_ARM_BONE, VICTIMS_THIGH_BONE, VICTIMS_SKULL, VICTIMS_RIB_BONE, VICTIMS_SPINE )
                                return this.getPath( canProceed ? '30970-06.html' : '30970-05.html' )

                            case 2:
                                return this.getPath( '30970-08.html' )

                            case 3:
                                return this.getPath( '30970-09.html' )

                            case 7:
                                return this.getPath( '30970-10.html' )

                            case 8:
                                let adenaAmount: number = 70 * QuestHelper.getQuestItemsCount( player, USELESS_BONE_PIECES )

                                switch ( state.getMemoStateEx( 1 ) ) {
                                    case 1:
                                    case 2:
                                        await QuestHelper.rewardSingleItem( player, BILL_OF_IASON_HEINE, 3 )
                                        await QuestHelper.giveAdena( player, 5390 + adenaAmount, true )
                                        await state.exitQuest( true, true )

                                        return this.getPath( '30970-11.html' )

                                    case 3:
                                        if ( Math.random() <= 0.92 ) {
                                            await QuestHelper.rewardSingleItem( player, BILL_OF_IASON_HEINE, 5 )
                                        } else {
                                            await QuestHelper.rewardSingleItem( player, IMPERIAL_DIAMOND, 1 )
                                        }

                                        await QuestHelper.giveAdena( player, 3040 + adenaAmount, true )
                                        await state.exitQuest( true, true )

                                        return this.getPath( '30970-12.html' )
                                }

                                break
                        }

                        break

                    case ORPHEUS:
                        if ( QuestHelper.hasQuestItem( player, USELESS_BONE_PIECES ) ) {
                            return this.getPath( '30971-01.html' )
                        }

                        break

                    case MEDIUM_JAR:
                        switch ( state.getMemoState() ) {
                            case 3:
                                await QuestHelper.takeMultipleItems( player, -1, POWDER_TO_SUMMON_DEAD_SOULS, VICTIMS_ARM_BONE, VICTIMS_THIGH_BONE, VICTIMS_SKULL, VICTIMS_RIB_BONE, VICTIMS_SPINE )
                                state.setMemoState( 7 )

                                let chance = Math.random()

                                if ( chance <= 39 ) {
                                    state.setMemoStateEx( 1, 1 )
                                } else if ( chance <= 79 ) {
                                    state.setMemoStateEx( 1, 2 )
                                } else {
                                    state.setMemoStateEx( 1, 3 )
                                }

                                return this.getPath( '30973-01.html' )

                            case 7:
                                switch ( state.getMemoStateEx( 1 ) ) {
                                    case 1:
                                        return this.getPath( '30973-08.html' )

                                    case 2:
                                        return this.getPath( '30973-09.html' )

                                    case 3:
                                        return this.getPath( '30973-10.html' )
                                }

                                break

                            case 8:
                                return this.getPath( '30973-11.html' )
                        }

                        break

                    case XENOVIA:
                        if ( state.isMemoState( 2 ) ) {
                            return this.getPath( '30912-01.html' )
                        }

                        if ( [ 7, 8 ].includes( state.getMemoState() ) || QuestHelper.hasQuestItem( player, POWDER_TO_SUMMON_DEAD_SOULS ) ) {
                            return this.getPath( '30912-06.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}