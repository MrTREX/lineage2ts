import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { PlayerRadarCache } from '../../gameService/cache/PlayerRadarCache'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const DROPH = 32711
const GORGOLOS = 25681
const LAST_TITAN_UTENUS = 25684
const GIANT_MARPANAK = 25680
const HEKATON_PRIME = 25687
const SUPPORT_ITEMS = 14850
const CET_1_SHEET = 14851
const CET_2_SHEET = 14852
const CET_3_SHEET = 14853
const RESPAWN_DELAY = GeneralHelper.hoursToMillis( 1 )
const minimumLevel = 79

export class ControlDeviceOfTheGiants extends ListenerLogic {
    objectId: number = 0
    futureRespawnTime: number = 0

    constructor() {
        super( 'Q00307_ControlDeviceOfTheGiants', 'listeners/tracked-300/ControlDeviceOfTheGiants.ts' )
        this.questId = 307
    }

    getAttackableKillIds(): Array<number> {
        return [
            GORGOLOS,
            LAST_TITAN_UTENUS,
            GIANT_MARPANAK,
            HEKATON_PRIME,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00307_ControlDeviceOfTheGiants'
    }

    getQuestStartIds(): Array<number> {
        return [ DROPH ]
    }

    getTalkIds(): Array<number> {
        return [ DROPH ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 1 )
        if ( !player ) {
            return
        }

        switch ( data.npcId ) {
            case GORGOLOS:
                await QuestHelper.giveSingleItem( player, CET_1_SHEET, 1 )
                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                break

            case LAST_TITAN_UTENUS:
                await QuestHelper.giveSingleItem( player, CET_2_SHEET, 1 )
                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                break

            case GIANT_MARPANAK:
                await QuestHelper.giveSingleItem( player, CET_3_SHEET, 1 )
                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                break

            case HEKATON_PRIME:
                if ( !player.getParty() ) {
                    return
                }

                let questName = this.getName()
                _.each( player.getParty().getMembers(), ( playerId: number ) => {
                    let state: QuestState = QuestStateCache.getQuestState( playerId, questName, false )
                    if ( !state || !state.isCondition( 1 ) ) {
                        return
                    }

                    state.setConditionWithSound( 2, true )
                } )

                this.futureRespawnTime = Date.now() + RESPAWN_DELAY
                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32711-04.html':
                if ( player.getLevel() >= minimumLevel ) {
                    state.startQuest()

                    return this.getPath( QuestHelper.hasQuestItems( player, CET_1_SHEET, CET_2_SHEET, CET_3_SHEET ) ? '32711-04a.html' : '32711-04.html' )
                }

                break

            case '32711-05a.html':
                PlayerRadarCache.getRadar( data.playerId ).addNpcMarker( 186214, 61591, -4152 )
                break

            case '32711-05b.html':
                PlayerRadarCache.getRadar( data.playerId ).addNpcMarker( 187554, 60800, -4984 )
                break

            case '32711-05c.html':
                PlayerRadarCache.getRadar( data.playerId ).addNpcMarker( 193432, 53922, -4368 )
                break

            case 'spawn':
                if ( !QuestHelper.hasQuestItems( player, CET_1_SHEET, CET_2_SHEET, CET_3_SHEET ) ) {
                    return QuestHelper.getNoQuestMessagePath()
                }

                let npc = L2World.getObjectById( this.objectId ) as L2Npc
                if ( npc && !npc.isDead() ) {
                    return this.getPath( '32711-09.html' )
                }

                if ( Date.now() - this.futureRespawnTime > 0 ) {
                    return this.getPath( '32711-09a.html' )
                }

                npc = QuestHelper.addGenericSpawn( null, HEKATON_PRIME, 191777, 56197, -7624, 0, false, 0 )
                this.objectId = npc.getObjectId()

                await QuestHelper.takeMultipleItems( player, -1, CET_1_SHEET, CET_2_SHEET, CET_3_SHEET )
                return this.getPath( '32711-09.html' )

            case '32711-03.htm':
            case '32711-05.html':
            case '32711-06.html':
                break

            default:
                break
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '32711-01.htm' : '32711-02.htm' )

            case QuestStateValues.STARTED:
                let npc = L2World.getObjectById( this.objectId ) as L2Npc
                if ( npc && !npc.isDead() ) {
                    return this.getPath( '32711-09.html' )
                }

                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( QuestHelper.hasQuestItems( player, CET_1_SHEET, CET_2_SHEET, CET_3_SHEET ) ? '32711-08.html' : '32711-07.html' )

                    case 2:
                        await QuestHelper.giveSingleItem( player, SUPPORT_ITEMS, 1 )
                        await state.exitQuest( true, true )

                        return this.getPath( '32711-10.html' )
                }

                break
        }
    }
}