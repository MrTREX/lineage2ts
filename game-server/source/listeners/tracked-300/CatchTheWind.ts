import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

const RIZRAELL = 30361
const WIND_SHARD = 1078
const minimumLevel = 18

export class CatchTheWind extends ListenerLogic {
    constructor() {
        super( 'Q00317_CatchTheWind', 'listeners/tracked-300/CatchTheWind.ts' )
        this.questId = 317
        this.questItemIds = [
            WIND_SHARD,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            20036, // Lirein
            20044, // Lirein Elder
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00317_CatchTheWind'
    }

    getQuestStartIds(): Array<number> {
        return [ RIZRAELL ]
    }

    getTalkIds(): Array<number> {
        return [ RIZRAELL ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), -1, 3, L2World.getObjectById( data.targetId ) as L2Npc )
        if ( !state ) {
            return
        }

        if ( Math.random() > QuestHelper.getAdjustedChance( WIND_SHARD, 0.5, data.isChampion ) ) {
            return
        }

        let player = state.getPlayer()
        await QuestHelper.rewardSingleQuestItem( player, WIND_SHARD, 1, data.isChampion )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30361-04.htm':
                state.startQuest()
                break

            case '30361-08.html':
            case '30361-09.html':
                let player = L2World.getPlayer( data.playerId )
                let shardCount = QuestHelper.getQuestItemsCount( player, WIND_SHARD )

                if ( shardCount > 0 ) {
                    let amount = ( shardCount * 40 ) + ( shardCount >= 10 ? 2988 : 0 )
                    await QuestHelper.giveAdena( player, amount, true )
                    await QuestHelper.takeSingleItem( player, WIND_SHARD, -1 )
                }

                if ( data.eventName === '30361-08.html' ) {
                    await state.exitQuest( true, true )
                }

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30361-03.htm' : '30361-02.htm' )

            case QuestStateValues.STARTED:
                return this.getPath( QuestHelper.hasQuestItem( player, WIND_SHARD ) ? '30361-07.html' : '30361-05.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}