import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { ConfigManager } from '../../config/ConfigManager'

import _ from 'lodash'

const OLTRAN = 30862
const SNAKE_SCALE = 5868
const minimumLevel = 63
const requireItemAmount = 108
const rewardItemIds = [
    5364, // Recipe: Sealed Dark Crystal Shield(60%)
    5366, // Recipe: Sealed Shield of Nightmare(60%)
    6329, // Recipe: Sealed Phoenix Necklace(70%)
    6331, // Recipe: Sealed Phoenix Earring(70%)
    6333, // Recipe: Sealed Phoenix Ring(70%)
    6335, // Recipe: Sealed Majestic Necklace(70%)
    6337, // Recipe: Sealed Majestic Earring(70%)
    6339, // Recipe: Sealed Majestic Ring(70%)
]

const monsterChances = {
    20672: 0.71,
    20673: 0.74,
}

export class IllegitimateChildOfTheGoddess extends ListenerLogic {
    constructor() {
        super( 'Q00358_IllegitimateChildOfTheGoddess', 'listeners/tracked-300/IllegitimateChildOfTheGoddess.ts' )
        this.questId = 358
        this.questItemIds = [
            SNAKE_SCALE,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00358_IllegitimateChildOfTheGoddess'
    }

    getQuestStartIds(): Array<number> {
        return [ OLTRAN ]
    }

    getTalkIds(): Array<number> {
        return [ OLTRAN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), 1, 3, L2World.getObjectById( data.targetId ) as L2Npc )
        if ( !state ) {
            return
        }

        if ( Math.random() > QuestHelper.getAdjustedChance( SNAKE_SCALE, monsterChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        let player = state.getPlayer()
        await QuestHelper.rewardAndProgressState( player, state, SNAKE_SCALE, 1, requireItemAmount, data.isChampion, 2 )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30862-02.htm':
            case '30862-03.htm':
                break

            case '30862-04.htm':
                state.startQuest()
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30862-01.htm' : '30862-05.html' )

            case QuestStateValues.STARTED:
                if ( !state.isCondition( 2 ) || QuestHelper.getQuestItemsCount( player, SNAKE_SCALE ) < requireItemAmount ) {
                    return this.getPath( '30862-06.html' )
                }

                await QuestHelper.rewardPossibleItems( player, 1, rewardItemIds )
                await state.exitQuest( true, true )

                return this.getPath( '30862-07.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}