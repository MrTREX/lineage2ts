import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const PANO = 30078
const BEAR_SKIN = 4259
const minimumLevel = 20
const adenaReward = 3710
const amountNeeded = 20

const monsterRewardChances = {
    20203: 0.99,
    20310: 0.87,
    20021: 0.83,
    20335: 0.87,
}

export class HuntingForWildBeasts extends ListenerLogic {
    constructor() {
        super( 'Q00341_HuntingForWildBeasts', 'listeners/tracked-300/HuntingForWildBeasts.ts' )
        this.questId = 341
        this.questItemIds = [
            BEAR_SKIN,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00341_HuntingForWildBeasts'
    }

    getQuestStartIds(): Array<number> {
        return [ PANO ]
    }

    getTalkIds(): Array<number> {
        return [ PANO ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isCondition( 1 ) ) {
            return
        }

        if ( Math.random() > QuestHelper.getAdjustedChance( BEAR_SKIN, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        await QuestHelper.rewardAndProgressState( player, state, BEAR_SKIN, 1, amountNeeded, data.isChampion, 2 )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30078-03.htm':
                break

            case '30078-04.htm':
                state.startQuest()
                break
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30078-01.html' : '30078-02.htm' )

            case QuestStateValues.STARTED:
                if ( state.isCondition( 2 ) && QuestHelper.getQuestItemsCount( player, BEAR_SKIN ) >= amountNeeded ) {
                    await QuestHelper.giveAdena( player, adenaReward, true )
                    await state.exitQuest( true, true )

                    return this.getPath( '30078-05.html' )
                }

                return this.getPath( '30078-06.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}