import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const SILVA = 30686
const JADE_CRYSTAL = 5867
const minimumLevel = 47
const adenaPerCrystal = 425

const monsterRewardChances = {
    20594: 0.577, // Forest Runner
    20595: 0.6, // Fline Elder
    20596: 0.638, // Liele Elder
    20597: 0.062, // Valley Treant Elder
}

export class WarehouseKeepersAmbition extends ListenerLogic {
    constructor() {
        super( 'Q00357_WarehouseKeepersAmbition', 'listeners/tracked-300/WarehouseKeepersAmbition.ts' )
        this.questId = 357
        this.questItemIds = [
            JADE_CRYSTAL,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00357_WarehouseKeepersAmbition'
    }

    getQuestStartIds(): Array<number> {
        return [ SILVA ]
    }

    getTalkIds(): Array<number> {
        return [ SILVA ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player: L2PcInstance = this.getRandomPartyMemberPerCondition( L2World.getPlayer( data.playerId ), -1, 3, L2World.getObjectById( data.targetId ) as L2Npc )
        if ( !player ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, JADE_CRYSTAL, 1, data.isChampion )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30686-01.htm':
            case '30686-03.htm':
            case '30686-04.htm':
            case '30686-10.html':
                break

            case '30686-05.htm':
                state.startQuest()
                break

            case '30686-09.html':
                let crystalAmount = QuestHelper.getQuestItemsCount( player, JADE_CRYSTAL )
                if ( crystalAmount === 0 ) {
                    return
                }

                let adena = crystalAmount * adenaPerCrystal + ( crystalAmount >= 100 ? 40500 : 13500 )

                await QuestHelper.giveAdena( player, adena, true )
                await QuestHelper.takeSingleItem( player, JADE_CRYSTAL, -1 )

                if ( crystalAmount < 100 ) {
                    return this.getPath( '30686-08.html' )
                }

                break

            case '30686-11.html':
                let itemAmount = QuestHelper.getQuestItemsCount( player, JADE_CRYSTAL )
                if ( itemAmount > 0 ) {
                    let adena = itemAmount * adenaPerCrystal + ( itemAmount >= 100 ? 40500 : 0 )

                    await QuestHelper.giveAdena( player, adena, true )
                    await QuestHelper.takeSingleItem( player, JADE_CRYSTAL, -1 )
                }

                await state.exitQuest( true, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() < minimumLevel ? '30686-01.html' : '30686-02.htm' )

            case QuestStateValues.STARTED:
                return this.getPath( QuestHelper.hasQuestItems( player, JADE_CRYSTAL ) ? '30686-07.html' : '30686-06.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}