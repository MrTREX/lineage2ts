import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const LEOPOLD = 30435
const RED_CROSS_BADGE = 1359
const BLUE_CROSS_BADGE = 1360
const BLACK_CROSS_BADGE = 1361
const BLACK_LION_MARK = 1369
const minimumLevel = 21

type MonsterReward = [ number, number ] // chance, itemId
const monsterRewardChances: { [ npcId: number ]: MonsterReward } = {
    20053: [ 0.61, RED_CROSS_BADGE ], // Ol Mahum Patrol
    20058: [ 0.61, RED_CROSS_BADGE ], // Ol Mahum Guard
    20061: [ 0.57, BLUE_CROSS_BADGE ], // Ol Mahum Remnants
    20063: [ 0.63, BLUE_CROSS_BADGE ], // Ol Mahum Shooter
    20066: [ 0.59, BLACK_CROSS_BADGE ], // Ol Mahum Captain
    20436: [ 0.55, BLUE_CROSS_BADGE ], // Ol Mahum Supplier
    20437: [ 0.59, RED_CROSS_BADGE ], // Ol Mahum Recruit
    20438: [ 0.60, BLACK_CROSS_BADGE ], // Ol Mahum General
    20439: [ 0.62, BLUE_CROSS_BADGE ], // Ol Mahum Officer
}

export class VanquishRemnants extends ListenerLogic {
    constructor() {
        super( 'Q00326_VanquishRemnants', 'listeners/tracked-300/VanquishRemnants.ts' )
        this.questId = 326
        this.questItemIds = [
            RED_CROSS_BADGE,
            BLUE_CROSS_BADGE,
            BLACK_CROSS_BADGE,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00326_VanquishRemnants'
    }

    getQuestStartIds(): Array<number> {
        return [ LEOPOLD ]
    }

    getTalkIds(): Array<number> {
        return [ LEOPOLD ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isCondition( 1 ) ) {
            return
        }

        let [ chance, itemId ] = monsterRewardChances[ data.npcId ]
        if ( Math.random() >= QuestHelper.getAdjustedChance( itemId, chance, data.isChampion ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        await QuestHelper.rewardSingleQuestItem( player, itemId, 1, data.isChampion )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30435-03.htm':
                state.startQuest()
                break

            case '30435-07.html':
                await state.exitQuest( true, true )
                break

            case '30435-08.html':
                break
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30435-02.htm' : '30435-01.htm' )

            case QuestStateValues.STARTED: {
                const red = QuestHelper.getQuestItemsCount( player, RED_CROSS_BADGE )
                const blue = QuestHelper.getQuestItemsCount( player, BLUE_CROSS_BADGE )
                const black = QuestHelper.getQuestItemsCount( player, BLACK_CROSS_BADGE )
                const total = red + blue + black

                if ( total === 0 ) {
                    return this.getPath( '30435-04.html' )
                }

                if ( total >= 100 && !QuestHelper.hasQuestItem( player, BLACK_LION_MARK ) ) {
                    await QuestHelper.giveSingleItem( player, BLACK_LION_MARK, 1 )
                }

                let amount = ( red * 46 ) + ( blue * 52 ) + ( black * 58 ) + ( ( total >= 10 ) ? 4320 : 0 )
                await QuestHelper.giveAdena( player, amount, true )
                await QuestHelper.takeMultipleItems( player, -1, RED_CROSS_BADGE, BLUE_CROSS_BADGE, BLACK_CROSS_BADGE )

                if ( total < 100 ) {
                    return this.getPath( '30435-05.html' )
                }

                return this.getPath( QuestHelper.hasQuestItem( player, BLACK_LION_MARK ) ? '30435-09.html' : '30435-06.html' )
            }
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}