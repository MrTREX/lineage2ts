import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import {
    AttackableKillEvent,
    NpcGeneralEvent,
    NpcSpawnEvent,
    NpcTalkEvent,
} from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestHelper } from '../helpers/QuestHelper'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import _ from 'lodash'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const TORAI = 30557
const ALCHEMIST_MATILD = 30738
const FAIRY_RUPINA = 30742
const WISDOM_CHEST = 30743

const WHISPERING_WIND = 20078
const ANT_SOLDIER = 20087
const ANT_WARRIOR_CAPTAIN = 20088
const SILENOS = 20168
const TYRANT = 20192
const TYRANT_KINGPIN = 20193
const AMBER_BASILISK = 20199
const MIST_HORROR_RIPPER = 20227
const TURAK_BUGBEAR = 20248
const TURAK_BUGBEAR_WARRIOR = 20249
const GRIMA = 27135
const GLASS_JAGUAR = 20250
const SUCCUBUS_OF_SEDUCTION = 27136
const GREAT_DEMON_KING = 27138
const SECRET_KEEPER_TREE = 27139
const DLORD_ALEXANDROSANCHES = 27153
const ABYSSKING_BONAPARTERIUS = 27154
const EVILOVERLORD_RAMSEBALIUS = 27155

const Q_WISH_POTION = 3467
const Q_ANCIENT_CROWN = 3468
const Q_CERTIFICATE_OF_ROYALTY = 3469
const Q_ALCHEMY_TEXT = 3678
const Q_SECRET_BOOK_OF_POTION = 3679
const Q_POTION_RECIPE_1 = 3680
const Q_POTION_RECIPE_2 = 3681
const Q_MATILDS_ORB = 3682
const Q_FOBBIDEN_LOVE_SCROLL = 3683

const Q_AMBER_SCALE = 3684
const Q_WIND_SOULSTONE = 3685
const Q_GLASS_EYE = 3686
const Q_HORROR_ECTOPLASM = 3687
const Q_SILENOS_HORN = 3688
const Q_ANT_SOLDIER_APHID = 3689
const Q_TYRANTS_CHITIN = 3690
const Q_BUGBEAR_BLOOD = 3691

const NECKLACE_OF_GRACE = 931
const DEMONS_TUNIC_FABRIC = 1979
const DEMONS_HOSE_PATTERN = 1980
const DEMONS_BOOTS_FABRIC = 2952
const DEMONS_GLOVES_FABRIC = 2953
const Q_MUSICNOTE_LOVE = 4408
const Q_MUSICNOTE_BATTLE = 4409
const Q_GOLD_CIRCLET = 12766
const Q_SILVER_CIRCLET = 12767

const DEMONS_TUNIC = 441
const DEMONS_HOSE = 472
const DEMONS_BOOTS = 2435
const DEMONS_GLOVES = 2459

const minimumLevel = 30

const enum VariableNames {
    flag = 'f',
    quest = 'q',
    exchange = 'x',
}

const eventNames = {
    first: 'first',
    second: 'second',
    third: 'third',
}

export class TheWishingPotion extends ListenerLogic {
    constructor() {
        super( 'Q00334_TheWishingPotion', 'listeners/tracked-300/TheWishingPotion.ts' )
        this.questId = 334
        this.questItemIds = [
            Q_SECRET_BOOK_OF_POTION,
            Q_AMBER_SCALE,
            Q_GLASS_EYE,
            Q_HORROR_ECTOPLASM,
            Q_SILENOS_HORN,
            Q_ANT_SOLDIER_APHID,
            Q_TYRANTS_CHITIN,
            Q_BUGBEAR_BLOOD,
            Q_WISH_POTION,
            Q_POTION_RECIPE_1,
            Q_POTION_RECIPE_2,
            Q_MATILDS_ORB,
            Q_ALCHEMY_TEXT,
        ]
    }

    async broadcastAndReward( player: L2PcInstance, npc: L2Npc, npcStringId: number, npcId: number ): Promise<void> {
        BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, npcStringId )

        if ( Math.random() < 0.5 ) {
            QuestHelper.addSpawnAtLocation( npcId, npc, true, 0 )
            return
        }

        return this.rewardPlayer( player )
    }

    getAttackableKillIds(): Array<number> {
        return [
            WHISPERING_WIND,
            ANT_SOLDIER,
            ANT_WARRIOR_CAPTAIN,
            SILENOS,
            TYRANT,
            TYRANT_KINGPIN,
            AMBER_BASILISK,
            MIST_HORROR_RIPPER,
            TURAK_BUGBEAR,
            TURAK_BUGBEAR_WARRIOR,
            GRIMA,
            GLASS_JAGUAR,
            SUCCUBUS_OF_SEDUCTION,
            GREAT_DEMON_KING,
            SECRET_KEEPER_TREE,
            DLORD_ALEXANDROSANCHES,
            ABYSSKING_BONAPARTERIUS,
            EVILOVERLORD_RAMSEBALIUS,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00334_TheWishingPotion'
    }

    getQuestStartIds(): Array<number> {
        return [ ALCHEMIST_MATILD ]
    }

    getSpawnIds(): Array<number> {
        return [
            GRIMA,
            SUCCUBUS_OF_SEDUCTION,
            GREAT_DEMON_KING,
            DLORD_ALEXANDROSANCHES,
            ABYSSKING_BONAPARTERIUS,
            EVILOVERLORD_RAMSEBALIUS,
            FAIRY_RUPINA,
        ]
    }

    getTalkIds(): Array<number> {
        return [
            ALCHEMIST_MATILD,
            TORAI,
            FAIRY_RUPINA,
            WISDOM_CHEST,
        ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let npc = L2World.getObjectById( data.targetId ) as L2Npc
        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), -1, 2, npc )

        if ( !state ) {
            return
        }

        let player = state.getPlayer()

        switch ( data.npcId ) {
            case WHISPERING_WIND:
                await this.rewardAndProgressQuest( player, state, Q_WIND_SOULSTONE )
                return

            case ANT_SOLDIER:
            case ANT_WARRIOR_CAPTAIN:
                await this.rewardAndProgressQuest( player, state, Q_ANT_SOLDIER_APHID )
                return

            case SILENOS:
                await this.rewardAndProgressQuest( player, state, Q_SILENOS_HORN )
                return

            case TYRANT:
            case TYRANT_KINGPIN:
                await this.rewardAndProgressQuest( player, state, Q_TYRANTS_CHITIN )
                return

            case AMBER_BASILISK:
                await this.rewardAndProgressQuest( player, state, Q_AMBER_SCALE )
                return

            case MIST_HORROR_RIPPER:
                await this.rewardAndProgressQuest( player, state, Q_HORROR_ECTOPLASM )
                return

            case TURAK_BUGBEAR:
            case TURAK_BUGBEAR_WARRIOR:
                await this.rewardAndProgressQuest( player, state, Q_BUGBEAR_BLOOD )
                return

            case GLASS_JAGUAR:
                await this.rewardAndProgressQuest( player, state, Q_GLASS_EYE )
                return

            case GRIMA:
                if ( state.isMemoState( 2 )
                        && state.getVariable( VariableNames.flag ) === 2
                        && _.random( 1000 ) < 33 ) {

                    let adenaAmount = _.random( 1000 ) === 0 ? 100000000 : 900000
                    await QuestHelper.giveAdena( player, adenaAmount, true )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                    state.setVariable( VariableNames.flag, 0 )
                }

                return

            case SUCCUBUS_OF_SEDUCTION:
                if ( state.isMemoState( 2 )
                        && !QuestHelper.hasQuestItem( player, Q_FOBBIDEN_LOVE_SCROLL )
                        && state.getVariable( VariableNames.flag ) === 1
                        && _.random( 1000 ) < 28 ) {
                    await QuestHelper.giveSingleItem( player, Q_FOBBIDEN_LOVE_SCROLL, 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                    state.setVariable( VariableNames.flag, 0 )
                }

                return

            case GREAT_DEMON_KING:
                if ( state.isMemoState( 2 ) && state.getVariable( VariableNames.flag ) === 3 ) {
                    await QuestHelper.giveAdena( player, 1406956, true )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                    state.setVariable( VariableNames.flag, 0 )
                }

                return

            case SECRET_KEEPER_TREE:
                if ( state.isMemoState( 1 ) && !QuestHelper.hasQuestItem( player, Q_SECRET_BOOK_OF_POTION ) ) {
                    await QuestHelper.giveSingleItem( player, Q_SECRET_BOOK_OF_POTION, 1 )

                    state.setConditionWithSound( 2, true )
                    state.showQuestionMark( 334 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case DLORD_ALEXANDROSANCHES:
                if ( state.isMemoState( 2 ) && state.getVariable( VariableNames.flag ) === 3 ) {
                    await this.broadcastAndReward( player, npc, NpcStringIds.BONAPARTERIUS_ABYSS_KING_WILL_PUNISH_YOU, ABYSSKING_BONAPARTERIUS )
                }

                return

            case ABYSSKING_BONAPARTERIUS:
                if ( state.isMemoState( 2 ) && state.getVariable( VariableNames.flag ) === 3 ) {
                    await this.broadcastAndReward( player, npc, NpcStringIds.REVENGE_IS_OVERLORD_RAMSEBALIUS_OF_THE_EVIL_WORLD, EVILOVERLORD_RAMSEBALIUS )
                }

                return

            case EVILOVERLORD_RAMSEBALIUS:
                if ( state.isMemoState( 2 ) && state.getVariable( VariableNames.flag ) === 3 ) {
                    await this.broadcastAndReward( player, npc, NpcStringIds.OH_GREAT_DEMON_KING, GREAT_DEMON_KING )
                }

                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let npc = L2World.getObjectById( data.characterId ) as L2Npc

        switch ( data.characterNpcId ) {
            case FAIRY_RUPINA:
            case GRIMA:
            case SUCCUBUS_OF_SEDUCTION:
            case GREAT_DEMON_KING:
                await npc.deleteMe()
                return

            case DLORD_ALEXANDROSANCHES:
                BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.OH_ITS_NOT_AN_OPPONENT_OF_MINE_HA_HA_HA )
                await npc.deleteMe()
                return

            case ABYSSKING_BONAPARTERIUS:
                BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.OH_ITS_NOT_AN_OPPONENT_OF_MINE_HA_HA_HA )
                await npc.deleteMe()
                return

            case EVILOVERLORD_RAMSEBALIUS:
                BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.OH_ITS_NOT_AN_OPPONENT_OF_MINE_HA_HA_HA )
                await npc.deleteMe()
                return

            case WISDOM_CHEST:
                BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.DONT_INTERRUPT_MY_REST_AGAIN )
                BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.YOURE_A_GREAT_DEVIL_NOW )
                await npc.deleteMe()
                return
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        if ( data.eventName === 'QUEST_ACCEPTED' ) {
            state.startQuest()
            state.setMemoState( 1 )
            state.showQuestionMark( 334 )

            if ( !QuestHelper.hasQuestItem( player, Q_ALCHEMY_TEXT ) ) {
                await QuestHelper.giveSingleItem( player, Q_ALCHEMY_TEXT, 1 )
            }

            return this.getPath( '30738-04.htm' )
        }

        switch ( data.eventName ) {
            case '1':
                return this.getPath( '30738-03.htm' )

            case '2':
                await QuestHelper.takeMultipleItems( player, -1, Q_SECRET_BOOK_OF_POTION, Q_ALCHEMY_TEXT )
                await QuestHelper.giveMultipleItems( player, 1, Q_POTION_RECIPE_1, Q_POTION_RECIPE_2 )

                state.setMemoState( 2 )
                state.setConditionWithSound( 3, true )
                state.showQuestionMark( 334 )

                return this.getPath( '30738-07.html' )

            case '3':
                return this.getPath( '30738-10.html' )

            case '4':
                if ( !QuestHelper.hasQuestItems( player, Q_AMBER_SCALE, Q_GLASS_EYE, Q_HORROR_ECTOPLASM, Q_SILENOS_HORN, Q_ANT_SOLDIER_APHID, Q_TYRANTS_CHITIN, Q_BUGBEAR_BLOOD, Q_WIND_SOULSTONE, Q_POTION_RECIPE_1, Q_POTION_RECIPE_2 ) ) {
                    return
                }

                await QuestHelper.giveSingleItem( player, Q_WISH_POTION, 1 )

                if ( !QuestHelper.hasQuestItem( player, Q_MATILDS_ORB ) ) {
                    await QuestHelper.giveSingleItem( player, Q_MATILDS_ORB, 1 )
                }

                await QuestHelper.takeMultipleItems( player, -1, Q_AMBER_SCALE, Q_GLASS_EYE, Q_HORROR_ECTOPLASM, Q_SILENOS_HORN, Q_ANT_SOLDIER_APHID, Q_TYRANTS_CHITIN, Q_BUGBEAR_BLOOD, Q_WIND_SOULSTONE, Q_POTION_RECIPE_1, Q_POTION_RECIPE_2 )

                state.setMemoState( 2 )
                state.setConditionWithSound( 5, true )
                state.showQuestionMark( 334 )

                return this.getPath( '30738-11.html' )

            case '5':
                if ( QuestHelper.hasQuestItem( player, Q_WISH_POTION ) ) {

                    state.setVariable( VariableNames.quest, 0 )
                    return this.getPath( '30738-13.html' )
                }

                return this.getPath( '30738-14.html' )

            case '6':
                if ( QuestHelper.hasQuestItem( player, Q_WISH_POTION ) ) {
                    return this.getPath( '30738-15a.html' )
                }

                await QuestHelper.giveMultipleItems( player, 1, Q_POTION_RECIPE_1, Q_POTION_RECIPE_2 )

                return this.getPath( '30738-15.html' )

            case '7':
                if ( QuestHelper.hasQuestItem( player, Q_WISH_POTION ) ) {
                    if ( !state.getVariable( VariableNames.exchange ) ) {

                        await QuestHelper.takeSingleItem( player, Q_WISH_POTION, 1 )
                        state.setVariable( VariableNames.quest, 1 )
                        state.setVariable( VariableNames.flag, 1 )
                        state.setVariable( VariableNames.exchange, 1 )

                        this.startQuestTimer( eventNames.first, 3000, data.characterId, data.playerId )
                        return this.getPath( '30738-16.html' )
                    }

                    return this.getPath( '30738-20.html' )
                }

                return this.getPath( '30738-14.html' )

            case '8':
                if ( QuestHelper.hasQuestItem( player, Q_WISH_POTION ) ) {
                    if ( !state.getVariable( VariableNames.exchange ) ) {

                        await QuestHelper.takeSingleItem( player, Q_WISH_POTION, 1 )

                        state.setVariable( VariableNames.quest, 2 )
                        state.setVariable( VariableNames.flag, 2 )
                        state.setVariable( VariableNames.exchange, 1 )

                        this.startQuestTimer( eventNames.first, 3000, data.characterId, data.playerId )
                        return this.getPath( '30738-17.html' )
                    }

                    return this.getPath( '30738-20.html' )
                }

                return this.getPath( '30738-14.html' )

            case '9':
                if ( QuestHelper.hasQuestItem( player, Q_WISH_POTION ) ) {
                    if ( !state.getVariable( VariableNames.exchange ) ) {

                        await QuestHelper.takeSingleItem( player, Q_WISH_POTION, 1 )

                        state.setVariable( VariableNames.quest, 3 )
                        state.setVariable( VariableNames.flag, 3 )
                        state.setVariable( VariableNames.exchange, 1 )

                        this.startQuestTimer( eventNames.first, 3000, data.characterId, data.playerId )
                        return this.getPath( '30738-18.html' )
                    }

                    return this.getPath( '30738-20.html' )
                }

                return this.getPath( '30738-14.html' )

            case '10':
                if ( QuestHelper.hasQuestItem( player, Q_WISH_POTION ) ) {
                    if ( !state.getVariable( VariableNames.exchange ) ) {

                        await QuestHelper.takeSingleItem( player, Q_WISH_POTION, 1 )

                        state.setVariable( VariableNames.quest, 4 )
                        state.setVariable( VariableNames.flag, 4 )
                        state.setVariable( VariableNames.exchange, 1 )

                        this.startQuestTimer( eventNames.first, 3000, data.characterId, data.playerId )
                        return this.getPath( '30738-19.html' )
                    }

                    return this.getPath( '30738-20.html' )
                }

                return this.getPath( '30738-14.html' )

            case eventNames.first:
                BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.OK_EVERYBODY_PRAY_FERVENTLY )
                this.startQuestTimer( eventNames.second, 4000, data.characterId, data.playerId )

                break

            case eventNames.second:
                BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.BOTH_HANDS_TO_HEAVEN_EVERYBODY_YELL_TOGETHER )
                this.startQuestTimer( eventNames.third, 4000, data.characterId, data.playerId )

                break

            case eventNames.third:
                BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.ONE_TWO_MAY_YOUR_DREAMS_COME_TRUE )
                let chance: number = this.getQuestChance( state )
                switch ( chance ) {
                    case 0:
                        switch ( state.getVariable( VariableNames.quest ) ) {
                            case 1:
                                QuestHelper.addSpawnAtLocation( FAIRY_RUPINA, npc, true, 0 )
                                state.setVariable( VariableNames.exchange, 0 )

                                break

                            case 2:
                                QuestHelper.addSpawnAtLocation( GRIMA, npc, true, 0 )
                                QuestHelper.addSpawnAtLocation( GRIMA, npc, true, 0 )
                                QuestHelper.addSpawnAtLocation( GRIMA, npc, true, 0 )

                                state.setVariable( VariableNames.exchange, 0 )
                                break

                            case 3:
                                await QuestHelper.giveSingleItem( player, Q_CERTIFICATE_OF_ROYALTY, 1 )
                                state.setVariable( VariableNames.exchange, 0 )

                                break

                            case 4:
                                QuestHelper.addSpawnAtLocation( WISDOM_CHEST, npc, true, 0 )
                                state.setVariable( VariableNames.exchange, 0 )

                                break
                        }

                        break

                    case 1:
                        switch ( state.getVariable( VariableNames.quest ) ) {
                            case 1:
                                QuestHelper.addSpawnAtLocation( SUCCUBUS_OF_SEDUCTION, npc, true, 0 )
                                QuestHelper.addSpawnAtLocation( SUCCUBUS_OF_SEDUCTION, npc, true, 0 )
                                QuestHelper.addSpawnAtLocation( SUCCUBUS_OF_SEDUCTION, npc, true, 0 )
                                QuestHelper.addSpawnAtLocation( SUCCUBUS_OF_SEDUCTION, npc, true, 0 )

                                state.setVariable( VariableNames.exchange, 0 )
                                break

                            case 2:
                                await QuestHelper.giveAdena( player, 10000, true )
                                state.setVariable( VariableNames.exchange, 0 )

                                break

                            case 3:
                                QuestHelper.addSpawnAtLocation( DLORD_ALEXANDROSANCHES, npc, true, 0 )
                                state.setVariable( VariableNames.exchange, 0 )

                                break

                            case 4:
                                QuestHelper.addSpawnAtLocation( WISDOM_CHEST, npc, true, 0 )
                                state.setVariable( VariableNames.exchange, 0 )

                                break
                        }
                        break

                    case 2:
                        switch ( state.getVariable( VariableNames.quest ) ) {
                            case 2:
                                await QuestHelper.giveAdena( player, 10000, true )
                                state.setVariable( VariableNames.exchange, 0 )

                                break

                            case 3:
                                await QuestHelper.giveSingleItem( player, Q_ANCIENT_CROWN, 1 )
                                state.setVariable( VariableNames.exchange, 0 )

                                break

                            case 4:
                                QuestHelper.addSpawnAtLocation( WISDOM_CHEST, npc, true, 0 )
                                state.setVariable( VariableNames.exchange, 0 )
                                break
                        }
                        break
                }

                break
        }

        return this.getPath( data.eventName )
    }

    async onSpawnEvent( data: NpcSpawnEvent ): Promise<void> {
        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        switch ( data.npcId ) {
            case GRIMA:
                return BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.OH_OH_OH )

            case SUCCUBUS_OF_SEDUCTION:
                return BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.DO_YOU_WANT_US_TO_LOVE_YOU_OH )

            case GREAT_DEMON_KING:
                return BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.WHO_KILLED_MY_UNDERLING_DEVIL )

            case DLORD_ALEXANDROSANCHES:
                return BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.WHO_IS_CALLING_THE_LORD_OF_DARKNESS )

            case ABYSSKING_BONAPARTERIUS:
                return BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.I_AM_A_GREAT_EMPIRE_BONAPARTERIUS )

            case EVILOVERLORD_RAMSEBALIUS:
                return BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.LET_YOUR_HEAD_DOWN_BEFORE_THE_LORD )

            case FAIRY_RUPINA:
                return BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.I_WILL_MAKE_YOUR_LOVE_COME_TRUE_LOVE_LOVE_LOVE )

            case WISDOM_CHEST:
                return BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.I_HAVE_WISDOM_IN_ME_I_AM_THE_BOX_OF_WISDOM )
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case TORAI:
                if ( QuestHelper.hasQuestItem( player, Q_FOBBIDEN_LOVE_SCROLL ) ) {
                    await QuestHelper.giveAdena( player, 500000, true )
                    await QuestHelper.takeSingleItem( player, Q_FOBBIDEN_LOVE_SCROLL, 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                    return this.getPath( '30557-01.html' )
                }

                break

            case ALCHEMIST_MATILD:
                if ( state.isCreated() ) {
                    if ( player.getLevel() < minimumLevel ) {
                        return this.getPath( '30738-01.htm' )
                    }

                    return this.getPath( '30738-02.html' )
                }

                if ( QuestHelper.hasQuestItem( player, Q_ALCHEMY_TEXT ) ) {
                    let hasBook = QuestHelper.hasQuestItem( player, Q_SECRET_BOOK_OF_POTION )
                    return this.getPath( hasBook ? '30738-06.html' : '30738-05.html' )
                }

                if ( QuestHelper.hasQuestItems( player, Q_POTION_RECIPE_1, Q_POTION_RECIPE_2, Q_AMBER_SCALE, Q_WIND_SOULSTONE, Q_WIND_SOULSTONE, Q_GLASS_EYE, Q_GLASS_EYE, Q_HORROR_ECTOPLASM, Q_SILENOS_HORN, Q_ANT_SOLDIER_APHID, Q_TYRANTS_CHITIN, Q_BUGBEAR_BLOOD ) ) {
                    return this.getPath( '30738-09.html' )
                }

                let needsOtherItems: boolean = ( QuestHelper.hasQuestItem( player, Q_WIND_SOULSTONE ) && !QuestHelper.hasQuestItem( player, Q_GLASS_EYE ) )
                        || !QuestHelper.hasQuestItems( player, Q_AMBER_SCALE, Q_HORROR_ECTOPLASM, Q_SILENOS_HORN, Q_ANT_SOLDIER_APHID, Q_TYRANTS_CHITIN, Q_BUGBEAR_BLOOD )

                if ( needsOtherItems ) {
                    if ( QuestHelper.hasQuestItems( player, Q_POTION_RECIPE_1, Q_POTION_RECIPE_2 ) ) {
                        return this.getPath( '30738-08.html' )
                    }

                    if ( QuestHelper.hasQuestItem( player, Q_MATILDS_ORB ) ) {
                        return this.getPath( '30738-12.html' )
                    }
                }

                break

            case FAIRY_RUPINA:
                if ( state.getVariable( VariableNames.flag ) === 1 ) {

                    if ( _.random( 4 ) < 4 ) {
                        await QuestHelper.rewardSingleItem( player, NECKLACE_OF_GRACE, 1 )
                        state.setVariable( VariableNames.flag, 0 )

                        return this.getPath( '30742-01.html' )
                    }

                    await this.rewardPlayer( player )
                    await ( L2World.getObjectById( data.characterId ) as L2Npc ).deleteMe()

                    state.setVariable( VariableNames.flag, 0 )
                    return this.getPath( '30742-02.html' )
                }

                break

            case WISDOM_CHEST:
                if ( state.getVariable( VariableNames.flag ) === 4 ) {
                    let random = _.random( 100 )
                    if ( random < 10 ) {
                        await QuestHelper.giveSingleItem( player, Q_FOBBIDEN_LOVE_SCROLL, 1 )
                        return this.getPath( '30743-02.html' )
                    }

                    if ( random < 50 ) {
                        await this.rewardPlayer( player )
                        return this.getPath( '30743-03.html' )
                    }

                    if ( random < 85 ) {
                        let itemId: number = Math.random() < 0.5 ? Q_MUSICNOTE_LOVE : Q_MUSICNOTE_BATTLE

                        await QuestHelper.giveSingleItem( player, itemId, 1 )
                        return this.getPath( '30743-04.html' )
                    }

                    if ( random < 95 ) {
                        switch ( _.random( 3 ) ) {
                            case 0:
                                await QuestHelper.rewardSingleItem( player, DEMONS_TUNIC, 1 )
                                break

                            case 1:
                                await QuestHelper.rewardSingleItem( player, DEMONS_HOSE, 1 )
                                break

                            case 2:
                                await QuestHelper.rewardSingleItem( player, DEMONS_BOOTS, 1 )
                                break

                            case 3:
                                await QuestHelper.rewardSingleItem( player, DEMONS_GLOVES, 1 )
                                break
                        }

                        return this.getPath( '30743-05.html' )
                    }

                    await ( L2World.getObjectById( data.characterId ) as L2Npc ).deleteMe()

                    state.setVariable( VariableNames.flag, 0 )

                    let itemId: number = Math.random() < 0.5 ? Q_GOLD_CIRCLET : Q_SILVER_CIRCLET
                    await QuestHelper.giveSingleItem( player, itemId, 1 )

                    return this.getPath( '30743-06.htm' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getQuestChance( state: QuestState ): number {
        switch ( state.getVariable( VariableNames.quest ) ) {
            case 1:
                return _.random( 1 )

            case 2:
            case 3:
            case 4:
                return _.random( 2 )
        }

        return 0
    }

    async rewardAndProgressQuest( player: L2PcInstance, state: QuestState, itemId: number ): Promise<void> {
        if ( _.random( 10 ) !== 0
                || QuestHelper.hasQuestItem( player, itemId )
                || !QuestHelper.hasQuestItems( player, Q_POTION_RECIPE_1, Q_POTION_RECIPE_2 ) ) {
            return
        }

        await QuestHelper.giveSingleItem( player, itemId, 1 )

        if ( QuestHelper.hasQuestItems( player, Q_AMBER_SCALE, Q_GLASS_EYE, Q_HORROR_ECTOPLASM, Q_SILENOS_HORN, Q_ANT_SOLDIER_APHID, Q_TYRANTS_CHITIN, Q_BUGBEAR_BLOOD ) ) {
            state.setConditionWithSound( 4, true )
            state.showQuestionMark( 334 )
            return
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async rewardPlayer( player: L2PcInstance ): Promise<void> {
        switch ( _.random( 3 ) ) {
            case 0:
                await QuestHelper.rewardSingleItem( player, DEMONS_TUNIC_FABRIC, 1 )
                break

            case 1:
                await QuestHelper.rewardSingleItem( player, DEMONS_HOSE_PATTERN, 1 )
                break

            case 2:
                await QuestHelper.rewardSingleItem( player, DEMONS_BOOTS_FABRIC, 1 )
                break

            case 3:
                await QuestHelper.rewardSingleItem( player, DEMONS_GLOVES_FABRIC, 1 )
                break
        }
    }
}