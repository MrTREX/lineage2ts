import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const SANDRA = 30090
const SORINT = 30232
const COLLECTOR_MEMBERSHIP_1 = 3813
const KAILS_COIN = 5899
const FOUR_LEAF_COIN = 7569
const COIN_ALBUM = 5900
const ANCIENT_GARGOYLE = 21018
const FALLEN_CHIEF_VERGUS = 27316
const ROYAL_MEMBERSHIP = 5898
const minimumLevel = 55

export class LetsBecomeARoyalMember extends ListenerLogic {
    constructor() {
        super( 'Q00381_LetsBecomeARoyalMember', 'listeners/tracked-300/LetsBecomeARoyalMember.ts' )
        this.questId = 381
        this.questItemIds = [
            KAILS_COIN,
            FOUR_LEAF_COIN,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [ ANCIENT_GARGOYLE, FALLEN_CHIEF_VERGUS ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00381_LetsBecomeARoyalMember'
    }

    getQuestStartIds(): Array<number> {
        return [ SORINT ]
    }

    getTalkIds(): Array<number> {
        return [ SORINT, SANDRA ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case ANCIENT_GARGOYLE:
                if ( Math.random() > QuestHelper.getAdjustedChance( KAILS_COIN, 0.05, data.isChampion )
                        || QuestHelper.hasQuestItems( player, KAILS_COIN ) ) {
                    return
                }

                await QuestHelper.giveSingleItem( player, KAILS_COIN, 1 )
                return

            case FALLEN_CHIEF_VERGUS:
                if ( state.isMemoState( 2 ) && !QuestHelper.hasQuestItems( player, FOUR_LEAF_COIN ) ) {
                    await QuestHelper.giveSingleItem( player, FOUR_LEAF_COIN, 1 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                }

                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30232-03.htm':
                state.startQuest()
                state.setMemoState( 1 )
                break

            case '30090-02.html':
                if ( state.isMemoState( 1 ) && !QuestHelper.hasQuestItem( player, COIN_ALBUM ) ) {
                    state.setMemoState( 2 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case SORINT:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        if ( player.getLevel() < minimumLevel || !QuestHelper.hasQuestItem( player, COLLECTOR_MEMBERSHIP_1 ) ) {
                            return this.getPath( '30232-02.html' )
                        }

                        if ( !QuestHelper.hasQuestItem( player, ROYAL_MEMBERSHIP ) ) {
                            return this.getPath( '30232-01.htm' )
                        }

                        break

                    case QuestStateValues.STARTED:
                        let hasAlbum = QuestHelper.hasQuestItem( player, COIN_ALBUM )
                        let hasCoin = QuestHelper.hasQuestItem( player, KAILS_COIN )

                        if ( hasAlbum && hasCoin ) {
                            await QuestHelper.takeMultipleItems( player, -1, KAILS_COIN, COIN_ALBUM )
                            await QuestHelper.giveSingleItem( player, ROYAL_MEMBERSHIP, 1 )
                            await state.exitQuest( false, true )

                            return this.getPath( '30232-06.html' )
                        }

                        if ( hasAlbum || hasCoin ) {
                            return this.getPath( '30232-05.html' )
                        }

                        return this.getPath( '30232-04.html' )

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case SANDRA:
                switch ( state.getMemoState() ) {
                    case 1:
                        return this.getPath( '30090-01.html' )

                    case 2:
                        if ( QuestHelper.hasQuestItem( player, COIN_ALBUM ) ) {
                            return this.getPath( '30090-05.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, FOUR_LEAF_COIN ) ) {
                            await QuestHelper.takeSingleItem( player, FOUR_LEAF_COIN, -1 )
                            await QuestHelper.giveSingleItem( player, COIN_ALBUM, 1 )

                            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                            return this.getPath( '30090-04.html' )
                        }

                        return this.getPath( '30090-03.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}