import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const ACCESSORY_MERCHANT_SONIA = 30062
const PRIESTESS_GLYVKA = 30067
const MAGISTER_ROLLANT = 30069
const GUARD_JACOB = 30073
const GROCER_PANO = 30078
const MAGISTER_MIRIEN = 30461
const JONAS = 30469
const INGREDIENT_LIST = 1420
const SONIAS_BOTANY_BOOK = 1421
const RED_MANDRAGORA_ROOT = 1422
const WHITE_MANDRAGORA_ROOT = 1423
const RED_MANDRAGORA_SAP = 1424
const WHITE_MANDRAGORA_SAP = 1425
const JACOBS_INSECT_BOOK = 1426
const NECTAR = 1427
const ROYAL_JELLY = 1428
const HONEY = 1429
const GOLDEN_HONEY = 1430
const PANOS_CONTRACT = 1431
const HOBGOBLIN_AMULET = 1432
const DIONIAN_POTATO = 1433
const GLYVKAS_BOTANY_BOOK = 1434
const GREEN_MARSH_MOSS = 1435
const BROWN_MARSH_MOSS = 1436
const GREEN_MOSS_BUNDLE = 1437
const BROWN_MOSS_BUNDLE = 1438
const ROLLANTS_CREATURE_BOOK = 1439
const BODY_OF_MONSTER_EYE = 1440
const MONSTER_EYE_MEAT = 1441
const JONASS_1ST_STEAK_DISH = 1442
const JONASS_2ND_STEAK_DISH = 1443
const JONASS_3RD_STEAK_DISH = 1444
const JONASS_4TH_STEAK_DISH = 1445
const JONASS_5TH_STEAK_DISH = 1446
const MIRIENS_REVIEW_1 = 1447
const MIRIENS_REVIEW_2 = 1448
const MIRIENS_REVIEW_3 = 1449
const MIRIENS_REVIEW_4 = 1450
const MIRIENS_REVIEW_5 = 1451
const JONASS_SALAD_RECIPE = 1455
const JONASS_SAUCE_RECIPE = 1456
const JONASS_STEAK_RECIPE = 1457
const HOBGOBLIN = 20147
const MANDRAGORA_SPROUT1 = 20154
const MANDRAGORA_SAPLING = 20155
const MANDRAGORA_BLOSSOM = 20156
const BLOODY_BEE = 20204
const MANDRAGORA_SPROUT2 = 20223
const GRAY_ANT = 20226
const GIANT_CRIMSON_ANT = 20228
const STINGER_WASP = 20229
const MONSTER_EYE_SEARCHER = 20265
const MONSTER_EYE_GAZER = 20266
const minimumLevel = 24

export class AdeptOfTaste extends ListenerLogic {
    constructor() {
        super( 'Q00330_AdeptOfTaste', 'listeners/tracked-300/AdeptOfTaste.ts' )
        this.questId = 330
        this.questItemIds = [
            INGREDIENT_LIST,
            SONIAS_BOTANY_BOOK,
            RED_MANDRAGORA_ROOT,
            WHITE_MANDRAGORA_ROOT,
            RED_MANDRAGORA_SAP,
            WHITE_MANDRAGORA_SAP,
            JACOBS_INSECT_BOOK,
            NECTAR,
            ROYAL_JELLY,
            HONEY,
            GOLDEN_HONEY,
            PANOS_CONTRACT,
            HOBGOBLIN_AMULET,
            DIONIAN_POTATO,
            GLYVKAS_BOTANY_BOOK,
            GREEN_MARSH_MOSS,
            BROWN_MARSH_MOSS,
            GREEN_MOSS_BUNDLE,
            BROWN_MOSS_BUNDLE,
            ROLLANTS_CREATURE_BOOK,
            BODY_OF_MONSTER_EYE,
            MONSTER_EYE_MEAT,
            JONASS_1ST_STEAK_DISH,
            JONASS_2ND_STEAK_DISH,
            JONASS_3RD_STEAK_DISH,
            JONASS_4TH_STEAK_DISH,
            JONASS_5TH_STEAK_DISH,
            MIRIENS_REVIEW_1,
            MIRIENS_REVIEW_2,
            MIRIENS_REVIEW_3,
            MIRIENS_REVIEW_4,
            MIRIENS_REVIEW_5,
        ]
    }

    async decideDishReward( player: L2PcInstance ): Promise<boolean> {
        await QuestHelper.takeMultipleItems( player, -1,
                INGREDIENT_LIST,
                RED_MANDRAGORA_SAP,
                WHITE_MANDRAGORA_SAP,
                HONEY,
                GOLDEN_HONEY,
                DIONIAN_POTATO,
                GREEN_MOSS_BUNDLE,
                BROWN_MOSS_BUNDLE,
                MONSTER_EYE_MEAT )

        return Math.random() < 0.5
    }

    getAttackableKillIds(): Array<number> {
        return [
            HOBGOBLIN,
            MANDRAGORA_SPROUT1,
            MANDRAGORA_SAPLING,
            MANDRAGORA_BLOSSOM,
            BLOODY_BEE,
            MANDRAGORA_SPROUT2,
            GRAY_ANT,
            GIANT_CRIMSON_ANT,
            STINGER_WASP,
            MONSTER_EYE_SEARCHER,
            MONSTER_EYE_GAZER,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00330_AdeptOfTaste'
    }

    getQuestStartIds(): Array<number> {
        return [ JONAS ]
    }

    getTalkIds(): Array<number> {
        return [
            JONAS,
            ACCESSORY_MERCHANT_SONIA,
            PRIESTESS_GLYVKA,
            MAGISTER_ROLLANT,
            GUARD_JACOB,
            GROCER_PANO,
            MAGISTER_MIRIEN,
        ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isCondition( 1 ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        if ( !this.needsRequiredItems( player ) ) {
            return
        }

        switch ( data.npcId ) {
            case HOBGOBLIN:
                if ( QuestHelper.hasQuestItems( player, INGREDIENT_LIST, PANOS_CONTRACT )
                        && QuestHelper.getQuestItemsCount( player, HOBGOBLIN_AMULET ) < 30 ) {
                    await this.rewardItems( player, HOBGOBLIN_AMULET, 30, data.isChampion )
                }

                return

            case MANDRAGORA_SPROUT1:
            case MANDRAGORA_SPROUT2:
                if ( QuestHelper.hasQuestItems( player, INGREDIENT_LIST, SONIAS_BOTANY_BOOK )
                        && !QuestHelper.hasAtLeastOneQuestItem( player, RED_MANDRAGORA_SAP, WHITE_MANDRAGORA_SAP ) ) {
                    let chance = _.random( 100 )
                    if ( chance < 70 ) {
                        await this.rewardItems( player, RED_MANDRAGORA_ROOT, 40, data.isChampion )
                        return
                    }

                    if ( chance < 77 ) {
                        await this.rewardItems( player, WHITE_MANDRAGORA_ROOT, 40, data.isChampion )
                        return
                    }
                }

                return

            case MANDRAGORA_SAPLING:
                if ( QuestHelper.hasQuestItems( player, INGREDIENT_LIST, SONIAS_BOTANY_BOOK )
                        && !QuestHelper.hasAtLeastOneQuestItem( player, RED_MANDRAGORA_SAP, WHITE_MANDRAGORA_SAP ) ) {
                    let chance = _.random( 100 )
                    if ( chance < 77 ) {
                        await this.rewardItems( player, RED_MANDRAGORA_ROOT, 40, data.isChampion )
                        return
                    }

                    if ( chance < 85 ) {
                        await this.rewardItems( player, WHITE_MANDRAGORA_ROOT, 40, data.isChampion )
                        return
                    }
                }

                return

            case MANDRAGORA_BLOSSOM:
                if ( QuestHelper.hasQuestItems( player, INGREDIENT_LIST, SONIAS_BOTANY_BOOK )
                        && !QuestHelper.hasAtLeastOneQuestItem( player, RED_MANDRAGORA_SAP, WHITE_MANDRAGORA_SAP ) ) {
                    let chance = _.random( 100 )
                    if ( chance < 87 ) {
                        await this.rewardItems( player, RED_MANDRAGORA_ROOT, 40, data.isChampion )
                        return
                    }

                    if ( chance < 96 ) {
                        await this.rewardItems( player, WHITE_MANDRAGORA_ROOT, 40, data.isChampion )
                        return
                    }
                }

                return

            case BLOODY_BEE:
                if ( QuestHelper.hasQuestItems( player, INGREDIENT_LIST, JACOBS_INSECT_BOOK ) ) {
                    let chance = _.random( 100 )
                    if ( chance < 80 ) {
                        await this.rewardItems( player, NECTAR, 20, data.isChampion )
                        return
                    }

                    if ( chance < 95 ) {
                        await this.rewardItems( player, ROYAL_JELLY, 10, data.isChampion )
                        return
                    }
                }

                return

            case GRAY_ANT:
                if ( QuestHelper.hasQuestItems( player, INGREDIENT_LIST, GLYVKAS_BOTANY_BOOK ) ) {
                    let chance = _.random( 100 )
                    if ( chance < 87 ) {
                        await this.rewardItems( player, GREEN_MARSH_MOSS, 20, data.isChampion )
                        return
                    }

                    if ( chance < 96 ) {
                        await this.rewardItems( player, BROWN_MARSH_MOSS, 20, data.isChampion )
                        return
                    }
                }
                return

            case GIANT_CRIMSON_ANT:
                if ( QuestHelper.hasQuestItems( player, INGREDIENT_LIST, GLYVKAS_BOTANY_BOOK ) ) {
                    if ( _.random( 100 ) < 90 ) {
                        await this.rewardItems( player, GREEN_MARSH_MOSS, 20, data.isChampion )
                    }

                    await this.rewardItems( player, BROWN_MARSH_MOSS, 20, data.isChampion )
                    return
                }

                return

            case STINGER_WASP:
                if ( QuestHelper.hasQuestItems( player, INGREDIENT_LIST, JACOBS_INSECT_BOOK ) ) {
                    if ( _.random( 100 ) < 92 ) {
                        await this.rewardItems( player, NECTAR, 20, data.isChampion )
                        return
                    }

                    await this.rewardItems( player, ROYAL_JELLY, 10, data.isChampion )
                    return
                }

                return

            case MONSTER_EYE_SEARCHER:
                if ( QuestHelper.hasQuestItems( player, INGREDIENT_LIST, ROLLANTS_CREATURE_BOOK )
                        && QuestHelper.getQuestItemsCount( player, BODY_OF_MONSTER_EYE ) < 30 ) {
                    let chance = _.random( 100 )
                    if ( chance < 77 ) {
                        await this.rewardItems( player, BODY_OF_MONSTER_EYE, 30, data.isChampion )
                        return
                    }

                    if ( chance < 97 ) {
                        await this.rewardItems( player, BODY_OF_MONSTER_EYE, 30, data.isChampion, 3 )
                        return
                    }
                }

                return

            case MONSTER_EYE_GAZER:
                if ( QuestHelper.hasQuestItems( player, INGREDIENT_LIST, ROLLANTS_CREATURE_BOOK )
                        && QuestHelper.getQuestItemsCount( player, BODY_OF_MONSTER_EYE ) < 30 ) {
                    let amount = _.random( 100 ) < 7 ? 1 : 2
                    await this.rewardItems( player, BODY_OF_MONSTER_EYE, 30, data.isChampion, amount )
                }

                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30469-03.htm':
                state.startQuest()
                await QuestHelper.giveSingleItem( player, INGREDIENT_LIST, 1 )
                break

            case '30062-05.html':
                if ( QuestHelper.hasQuestItem( player, SONIAS_BOTANY_BOOK )
                        && QuestHelper.getItemsSumCount( player, RED_MANDRAGORA_ROOT, WHITE_MANDRAGORA_ROOT ) >= 40
                        && QuestHelper.getQuestItemsCount( player, WHITE_MANDRAGORA_ROOT ) < 40 ) {

                    await QuestHelper.takeMultipleItems( player, -1, SONIAS_BOTANY_BOOK, RED_MANDRAGORA_ROOT, WHITE_MANDRAGORA_ROOT )
                    await QuestHelper.giveSingleItem( player, RED_MANDRAGORA_SAP, 1 )
                    break
                }

                return

            case '30067-05.html':
                if ( QuestHelper.hasQuestItem( player, GLYVKAS_BOTANY_BOOK )
                        && QuestHelper.getItemsSumCount( player, GREEN_MARSH_MOSS, BROWN_MARSH_MOSS ) >= 20
                        && QuestHelper.getQuestItemsCount( player, BROWN_MARSH_MOSS ) < 20 ) {

                    await QuestHelper.takeMultipleItems( player, -1, GLYVKAS_BOTANY_BOOK, GREEN_MARSH_MOSS, BROWN_MARSH_MOSS )
                    await QuestHelper.giveSingleItem( player, GREEN_MOSS_BUNDLE, 1 )
                    break
                }

                return

            case '30073-05.html':
                if ( QuestHelper.hasQuestItem( player, JACOBS_INSECT_BOOK )
                        && QuestHelper.getQuestItemsCount( player, NECTAR ) >= 20
                        && QuestHelper.getQuestItemsCount( player, ROYAL_JELLY ) < 10 ) {

                    await QuestHelper.takeMultipleItems( player, -1, JACOBS_INSECT_BOOK, NECTAR, ROYAL_JELLY )
                    await QuestHelper.giveSingleItem( player, HONEY, 1 )
                    break
                }

                return

            case '30062-04.html':
            case '30067-04.html':
            case '30073-04.html':
            case '30469-04.html':
            case '30469-04t1.html':
            case '30469-04t2.html':
            case '30469-04t3.html':
            case '30469-04t4.html':
            case '30469-04t5.html':
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== JONAS ) {
                    break
                }

                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '30469-01.htm' )
                }

                return this.getPath( '30469-02.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case JONAS:
                        if ( QuestHelper.hasQuestItem( player, INGREDIENT_LIST ) ) {
                            if ( this.needsRequiredItems( player ) ) {
                                return this.getPath( '30469-04.html' )
                            }

                            let ingredientAmount = QuestHelper.getItemsSumCount( player, WHITE_MANDRAGORA_SAP, GOLDEN_HONEY, BROWN_MOSS_BUNDLE )
                            let rewardBetterDish = await this.decideDishReward( player )

                            switch ( ingredientAmount ) {
                                case 0:
                                    if ( rewardBetterDish ) {
                                        await QuestHelper.giveSingleItem( player, JONASS_2ND_STEAK_DISH, 1 )
                                        return this.getPath( '30469-05t2.html' )
                                    }

                                    await QuestHelper.giveSingleItem( player, JONASS_1ST_STEAK_DISH, 1 )
                                    return this.getPath( '30469-05t1.html' )

                                case 1:
                                    if ( rewardBetterDish ) {
                                        await QuestHelper.giveSingleItem( player, JONASS_3RD_STEAK_DISH, 1 )
                                        return this.getPath( '30469-05t3.html' )
                                    }

                                    await QuestHelper.giveSingleItem( player, JONASS_2ND_STEAK_DISH, 1 )
                                    return this.getPath( '30469-05t2.html' )

                                case 2:
                                    if ( rewardBetterDish ) {
                                        await QuestHelper.giveSingleItem( player, JONASS_4TH_STEAK_DISH, 1 )
                                        return this.getPath( '30469-05t4.html' )
                                    }

                                    await QuestHelper.giveSingleItem( player, JONASS_3RD_STEAK_DISH, 1 )
                                    return this.getPath( '30469-05t3.html' )

                                case 3:
                                    if ( rewardBetterDish ) {
                                        await QuestHelper.giveSingleItem( player, JONASS_5TH_STEAK_DISH, 1 )
                                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_JACKPOT )
                                        return this.getPath( '30469-05t5.html' )
                                    }

                                    await QuestHelper.giveSingleItem( player, JONASS_4TH_STEAK_DISH, 1 )
                                    return this.getPath( '30469-05t4.html' )
                            }

                            break
                        }

                        if ( this.hasSomeRequiredItems( player ) ) {
                            break
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, JONASS_1ST_STEAK_DISH, JONASS_2ND_STEAK_DISH, JONASS_3RD_STEAK_DISH, JONASS_4TH_STEAK_DISH, JONASS_5TH_STEAK_DISH ) ) {
                            if ( !QuestHelper.hasAtLeastOneQuestItem( player, MIRIENS_REVIEW_1, MIRIENS_REVIEW_2, MIRIENS_REVIEW_3, MIRIENS_REVIEW_4, MIRIENS_REVIEW_5 ) ) {
                                return this.getPath( '30469-06.html' )
                            }

                            break
                        }

                        if ( QuestHelper.hasQuestItem( player, MIRIENS_REVIEW_1 ) ) {
                            await QuestHelper.takeSingleItem( player, MIRIENS_REVIEW_1, 1 )
                            await QuestHelper.giveAdena( player, 10000, true )

                            await state.exitQuest( true, true )
                            return this.getPath( '30469-06t1.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, MIRIENS_REVIEW_2 ) ) {
                            await QuestHelper.takeSingleItem( player, MIRIENS_REVIEW_2, 1 )
                            await QuestHelper.giveAdena( player, 14870, true )

                            await state.exitQuest( true, true )
                            return this.getPath( '30469-06t2.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, MIRIENS_REVIEW_3 ) ) {
                            await QuestHelper.takeSingleItem( player, MIRIENS_REVIEW_3, 1 )
                            await QuestHelper.giveAdena( player, 6490, true )
                            await QuestHelper.giveSingleItem( player, JONASS_SALAD_RECIPE, 1 )

                            await state.exitQuest( true, true )
                            return this.getPath( '30469-06t3.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, MIRIENS_REVIEW_4 ) ) {
                            await QuestHelper.takeSingleItem( player, MIRIENS_REVIEW_4, 1 )
                            await QuestHelper.giveAdena( player, 12220, true )
                            await QuestHelper.giveSingleItem( player, JONASS_SAUCE_RECIPE, 1 )

                            await state.exitQuest( true, true )
                            return this.getPath( '30469-06t4.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, MIRIENS_REVIEW_5 ) ) {
                            await QuestHelper.takeSingleItem( player, MIRIENS_REVIEW_5, 1 )
                            await QuestHelper.giveAdena( player, 16540, true )
                            await QuestHelper.giveSingleItem( player, JONASS_STEAK_RECIPE, 1 )

                            await state.exitQuest( true, true )
                            return this.getPath( '30469-06t5.html' )
                        }

                    case ACCESSORY_MERCHANT_SONIA:
                        if ( !this.needsRequiredItems( player ) || !QuestHelper.hasQuestItem( player, INGREDIENT_LIST ) ) {
                            break
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, SONIAS_BOTANY_BOOK, RED_MANDRAGORA_SAP, WHITE_MANDRAGORA_SAP ) ) {
                            await QuestHelper.giveSingleItem( player, SONIAS_BOTANY_BOOK, 1 )
                            return this.getPath( '30062-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, SONIAS_BOTANY_BOOK )
                                && !QuestHelper.hasAtLeastOneQuestItem( player, RED_MANDRAGORA_SAP, WHITE_MANDRAGORA_SAP ) ) {
                            if ( QuestHelper.getItemsSumCount( player, RED_MANDRAGORA_ROOT, WHITE_MANDRAGORA_ROOT ) < 40 ) {
                                return this.getPath( '30062-02.html' )
                            }

                            if ( QuestHelper.getQuestItemsCount( player, WHITE_MANDRAGORA_ROOT ) < 40 ) {
                                return this.getPath( '30062-03.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, -1, SONIAS_BOTANY_BOOK, RED_MANDRAGORA_ROOT, WHITE_MANDRAGORA_ROOT )
                            await QuestHelper.giveSingleItem( player, WHITE_MANDRAGORA_SAP, 1 )

                            return this.getPath( '30062-06.html' )
                        }

                        if ( !QuestHelper.hasQuestItems( player, SONIAS_BOTANY_BOOK )
                                && QuestHelper.hasAtLeastOneQuestItem( player, RED_MANDRAGORA_SAP, WHITE_MANDRAGORA_SAP ) ) {
                            return this.getPath( '30062-07.html' )
                        }

                        break

                    case PRIESTESS_GLYVKA:
                        if ( !this.needsRequiredItems( player ) || !QuestHelper.hasQuestItem( player, INGREDIENT_LIST ) ) {
                            break
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, GLYVKAS_BOTANY_BOOK, GREEN_MOSS_BUNDLE, BROWN_MOSS_BUNDLE ) ) {
                            await QuestHelper.giveSingleItem( player, GLYVKAS_BOTANY_BOOK, 1 )
                            return this.getPath( '30067-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, GLYVKAS_BOTANY_BOOK ) ) {
                            if ( QuestHelper.getItemsSumCount( player, GREEN_MARSH_MOSS, BROWN_MARSH_MOSS ) < 20 ) {
                                return this.getPath( '30067-02.html' )
                            }

                            if ( QuestHelper.getQuestItemsCount( player, BROWN_MARSH_MOSS ) < 20 ) {
                                return this.getPath( '30067-03.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, -1, GLYVKAS_BOTANY_BOOK, GREEN_MARSH_MOSS, BROWN_MARSH_MOSS )
                            await QuestHelper.giveSingleItem( player, BROWN_MOSS_BUNDLE, 1 )

                            return this.getPath( '30067-06.html' )
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, GREEN_MOSS_BUNDLE, BROWN_MOSS_BUNDLE ) ) {
                            return this.getPath( '30067-07.html' )
                        }

                        break

                    case MAGISTER_ROLLANT:
                        if ( !this.needsRequiredItems( player ) || !QuestHelper.hasQuestItem( player, INGREDIENT_LIST ) ) {
                            break
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, ROLLANTS_CREATURE_BOOK, MONSTER_EYE_MEAT ) ) {
                            await QuestHelper.giveSingleItem( player, ROLLANTS_CREATURE_BOOK, 1 )
                            return this.getPath( '30069-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, ROLLANTS_CREATURE_BOOK ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, BODY_OF_MONSTER_EYE ) < 30 ) {
                                return this.getPath( '30069-02.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, -1, ROLLANTS_CREATURE_BOOK, BODY_OF_MONSTER_EYE )
                            await QuestHelper.giveSingleItem( player, MONSTER_EYE_MEAT, 1 )

                            return this.getPath( '30069-03.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, MONSTER_EYE_MEAT ) ) {
                            return this.getPath( '30069-04.html' )
                        }

                        break

                    case GUARD_JACOB:
                        if ( !this.needsRequiredItems( player ) || !QuestHelper.hasQuestItem( player, INGREDIENT_LIST ) ) {
                            break
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, JACOBS_INSECT_BOOK, HONEY, GOLDEN_HONEY ) ) {
                            await QuestHelper.giveSingleItem( player, JACOBS_INSECT_BOOK, 1 )
                            return this.getPath( '30073-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, JACOBS_INSECT_BOOK ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, NECTAR ) < 20 ) {
                                return this.getPath( '30073-02.html' )
                            }

                            if ( QuestHelper.getQuestItemsCount( player, ROYAL_JELLY ) < 10 ) {
                                return this.getPath( '30073-03.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, -1, JACOBS_INSECT_BOOK, NECTAR, ROYAL_JELLY )
                            await QuestHelper.giveSingleItem( player, GOLDEN_HONEY, 1 )

                            return this.getPath( '30073-06.html' )
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, HONEY, GOLDEN_HONEY ) ) {
                            return this.getPath( '30073-07.html' )
                        }

                        break

                    case GROCER_PANO:
                        if ( !this.needsRequiredItems( player ) ) {
                            break
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, PANOS_CONTRACT, DIONIAN_POTATO ) ) {
                            await QuestHelper.giveSingleItem( player, PANOS_CONTRACT, 1 )
                            return this.getPath( '30078-01.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, INGREDIENT_LIST, PANOS_CONTRACT ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, HOBGOBLIN_AMULET ) < 30 ) {
                                return this.getPath( '30078-02.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, -1, PANOS_CONTRACT, HOBGOBLIN_AMULET )
                            await QuestHelper.giveSingleItem( player, DIONIAN_POTATO, 1 )

                            return this.getPath( '30078-03.html' )
                        }

                        if ( !QuestHelper.hasQuestItem( player, PANOS_CONTRACT ) ) {
                            return this.getPath( '30078-04.html' )
                        }

                        break

                    case MAGISTER_MIRIEN:
                        if ( QuestHelper.hasQuestItem( player, INGREDIENT_LIST ) ) {
                            return this.getPath( '30461-01.html' )
                        }

                        if ( this.hasSomeRequiredItems( player ) ) {
                            break
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, MIRIENS_REVIEW_1, MIRIENS_REVIEW_2, MIRIENS_REVIEW_3, MIRIENS_REVIEW_4, MIRIENS_REVIEW_5 ) ) {
                            return this.getPath( '30461-04.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, JONASS_1ST_STEAK_DISH ) ) {
                            await QuestHelper.takeSingleItem( player, JONASS_1ST_STEAK_DISH, 1 )
                            await QuestHelper.giveSingleItem( player, MIRIENS_REVIEW_1, 1 )
                            return this.getPath( '30461-02t1.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, JONASS_2ND_STEAK_DISH ) ) {
                            await QuestHelper.takeSingleItem( player, JONASS_2ND_STEAK_DISH, 1 )
                            await QuestHelper.giveSingleItem( player, MIRIENS_REVIEW_2, 1 )
                            return this.getPath( '30461-02t2.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, JONASS_3RD_STEAK_DISH ) ) {
                            await QuestHelper.takeSingleItem( player, JONASS_3RD_STEAK_DISH, 1 )
                            await QuestHelper.giveSingleItem( player, MIRIENS_REVIEW_3, 1 )
                            return this.getPath( '30461-02t3.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, JONASS_4TH_STEAK_DISH ) ) {
                            await QuestHelper.takeSingleItem( player, JONASS_4TH_STEAK_DISH, 1 )
                            await QuestHelper.giveSingleItem( player, MIRIENS_REVIEW_4, 1 )
                            return this.getPath( '30461-02t4.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, JONASS_5TH_STEAK_DISH ) ) {
                            await QuestHelper.takeSingleItem( player, JONASS_5TH_STEAK_DISH, 1 )
                            await QuestHelper.giveSingleItem( player, MIRIENS_REVIEW_5, 1 )
                            return this.getPath( '30461-02t5.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    hasSomeRequiredItems( player: L2PcInstance ): boolean {
        return QuestHelper.hasAtLeastOneQuestItem( player, RED_MANDRAGORA_SAP, WHITE_MANDRAGORA_SAP, HONEY, GOLDEN_HONEY, DIONIAN_POTATO, GREEN_MOSS_BUNDLE, BROWN_MOSS_BUNDLE, MONSTER_EYE_MEAT )
    }

    needsRequiredItems( player: L2PcInstance ): boolean {
        return QuestHelper.getItemsSumCount( player, RED_MANDRAGORA_SAP, WHITE_MANDRAGORA_SAP, HONEY, GOLDEN_HONEY, DIONIAN_POTATO, GREEN_MOSS_BUNDLE, BROWN_MOSS_BUNDLE, MONSTER_EYE_MEAT ) < 5
    }

    async rewardItems( player: L2PcInstance, itemId: number, limit: number, isFromChampion: boolean, rewardAmount: number = 1 ): Promise<void> {
        if ( QuestHelper.getQuestItemsCount( player, itemId ) >= limit ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, itemId, rewardAmount, isFromChampion )

        if ( QuestHelper.getQuestItemsCount( player, itemId ) >= limit ) {
            return player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
        }

        return player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }
}