import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const ASTARON = 30351
const VENOM_SAC = 1077
const minimumLevel = 18
const questItemLimit = 10

const monsterRewardChances = {
    20034: 0.26,
    20038: 0.29,
    20043: 0.30,
}

export class SweetestVenom extends ListenerLogic {
    constructor() {
        super( 'Q00324_SweetestVenom', 'listeners/tracked-300/SweetestVenom.ts' )
        this.questId = 324
        this.questItemIds = [
            VENOM_SAC,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00324_SweetestVenom'
    }

    getQuestStartIds(): Array<number> {
        return [ ASTARON ]
    }

    getTalkIds(): Array<number> {
        return [ ASTARON ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isCondition( 1 ) ) {
            return
        }

        if ( Math.random() >= QuestHelper.getAdjustedChance( VENOM_SAC, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        await QuestHelper.rewardAndProgressState( L2World.getPlayer( data.playerId ), state, VENOM_SAC, 1, questItemLimit, data.isChampion, 2 )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || data.eventName !== '30351-04.htm' ) {
            return
        }

        state.startQuest()
        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() < minimumLevel ? '30351-02.html' : '30351-03.htm' )

            case QuestStateValues.STARTED:
                if ( QuestHelper.getQuestItemsCount( player, VENOM_SAC ) < questItemLimit ) {
                    return this.getPath( '30351-05.html' )
                }

                await QuestHelper.giveAdena( player, 5810, true )
                await state.exitQuest( true, true )

                return this.getPath( '30351-06.html' )

        }

        return QuestHelper.getNoQuestMessagePath()
    }
}