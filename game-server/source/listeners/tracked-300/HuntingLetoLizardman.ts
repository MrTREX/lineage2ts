import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { ItemTypes } from '../../gameService/values/InventoryValues'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const RATH = 30126
const BRACELET_OF_LIZARDMAN = 7139

const minimumLevel = 34
const REQUIRED_BRACELET_COUNT = 60

type QuestReward = [ number, number, number ] // chance, itemId, amount
const questRewards: Array<QuestReward> = [
    [ 500, ItemTypes.Adena, 30000 ],
    [ 750, 1872, 50 ],
    [ 1000, 1867, 50 ],
]

const monsterRewardChances = {
    20577: 360, // Leto Lizardman
    20578: 390, // Leto Lizardman Archer
    20579: 410, // Leto Lizardman Soldier
    20580: 790, // Leto Lizardman Warrior
    20582: 890, // Leto Lizardman Overlord
}

export class HuntingLetoLizardman extends ListenerLogic {
    constructor() {
        super( 'Q00300_HuntingLetoLizardman', 'listeners/tracked-300/HuntingLetoLizardman.ts' )
        this.questId = 300
        this.questItemIds = [
            BRACELET_OF_LIZARDMAN,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00300_HuntingLetoLizardman'
    }

    getQuestStartIds(): Array<number> {
        return [ RATH ]
    }

    getTalkIds(): Array<number> {
        return [ RATH ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 1 )

        if ( !player || _.random( 1000 ) >= QuestHelper.getAdjustedChance( BRACELET_OF_LIZARDMAN, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), this.getName(), false )
        await QuestHelper.rewardAndProgressState( player, state, BRACELET_OF_LIZARDMAN, 1, REQUIRED_BRACELET_COUNT, data.isChampion, 2 )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30126-03.htm':
                state.startQuest()
                break

            case '30126-06.html':
                let player = L2World.getPlayer( data.playerId )
                if ( QuestHelper.getQuestItemsCount( player, BRACELET_OF_LIZARDMAN ) < REQUIRED_BRACELET_COUNT ) {
                    return this.getPath( '30126-07.html' )
                }

                await QuestHelper.takeSingleItem( player, BRACELET_OF_LIZARDMAN, -1 )

                let chance = _.random( 1000 )
                let reward = _.find( questRewards, ( item: QuestReward ): boolean => {
                    return item[ 0 ] > chance
                } )

                if ( reward ) {
                    let [ , itemId, amount ] = reward
                    await QuestHelper.rewardSingleItem( player, itemId, amount )
                }

                await state.exitQuest( true, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30126-01.htm' : '30126-02.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '30126-04.html' )

                    case 2:
                        if ( QuestHelper.getQuestItemsCount( player, BRACELET_OF_LIZARDMAN ) >= REQUIRED_BRACELET_COUNT ) {
                            return this.getPath( '30126-05.html' )
                        }

                        return this.getPath( '30126-04.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}