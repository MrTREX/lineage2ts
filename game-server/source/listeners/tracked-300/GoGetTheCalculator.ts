import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { ItemTypes } from '../../gameService/values/InventoryValues'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { L2Npc } from '../../gameService/models/actor/L2Npc'

const BRUNON = 30526
const SILVERA = 30527
const SPIRON = 30532
const BALANKI = 30533
const STOLEN_CALCULATOR = 4285
const GEMSTONE = 4286
const GEMSTONE_BEAST = 20540
const CALCULATOR = 4393
const adenaReward = 1500
const minimumLevel = 12

export class GoGetTheCalculator extends ListenerLogic {
    constructor() {
        super( 'Q00347_GoGetTheCalculator', 'listeners/tracked-300/GoGetTheCalculator.ts' )
        this.questId = 347
        this.questItemIds = [
            STOLEN_CALCULATOR,
            GEMSTONE,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [ GEMSTONE_BEAST ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00347_GoGetTheCalculator'
    }

    getQuestStartIds(): Array<number> {
        return [ BRUNON ]
    }

    getTalkIds(): Array<number> {
        return [
            BRUNON,
            SILVERA,
            SPIRON,
            BALANKI,
        ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), 4, 3, L2World.getObjectById( data.targetId ) as L2Npc )
        if ( !state ) {
            return
        }

        if ( Math.random() > QuestHelper.getAdjustedChance( GEMSTONE, 0.4, data.isChampion ) ) {
            return
        }

        await QuestHelper.rewardAndProgressState( state.getPlayer(), state, GEMSTONE, 1, 10, data.isChampion, 5 )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30526-03.htm':
            case '30526-04.htm':
            case '30526-05.htm':
            case '30526-06.htm':
            case '30526-07.htm':
            case '30532-03.html':
            case '30532-04.html':
                break

            case '30526-08.htm':
                state.startQuest()
                break

            case '30526-10.html':
                if ( state.isCondition( 6 ) ) {
                    await QuestHelper.takeSingleItem( player, STOLEN_CALCULATOR, -1 )
                    await QuestHelper.rewardSingleItem( player, CALCULATOR, 1 )
                    await state.exitQuest( true, true )

                    break
                }

                return this.getPath( '30526-09.html' )

            case '30526-11.html':
                if ( state.isCondition( 6 ) ) {
                    await QuestHelper.takeSingleItem( player, STOLEN_CALCULATOR, -1 )
                    await QuestHelper.giveAdena( player, adenaReward, true )
                    await state.exitQuest( true, true )

                    break
                }

                return

            case '30532-02.html':
                if ( state.isCondition( 1 ) ) {
                    state.setConditionWithSound( 2, true )

                    break
                }

                return

            case '30533-02.html':
                if ( state.isCondition( 2 ) && player.getAdena() > 100 ) {
                    await QuestHelper.takeSingleItem( player, ItemTypes.Adena, 100 )

                    state.setConditionWithSound( 3, true )
                    break
                }

                return


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== BRUNON ) {
                    break
                }

                return this.getPath( player.getLevel() >= minimumLevel ? '30526-01.htm' : '30526-02.html' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case BRUNON:
                        if ( QuestHelper.hasQuestItem( player, CALCULATOR ) ) {
                            state.setConditionWithSound( 6 )
                        }

                        switch ( state.getCondition() ) {
                            case 1:
                            case 2:
                                return this.getPath( '30526-13.html' )

                            case 3:
                            case 4:
                                return this.getPath( '30526-14.html' )

                            case 5:
                                return this.getPath( '30526-15.html' )

                            case 6:
                                return this.getPath( '30526-09.html' )
                        }

                        break

                    case SPIRON:
                        return this.getPath( state.isCondition( 1 ) ? '30532-01.html' : '30532-05.html' )

                    case BALANKI:
                        if ( state.isCondition( 2 ) ) {
                            return this.getPath( '30533-01.html' )
                        }

                        if ( state.getCondition() > 2 ) {
                            return this.getPath( '30533-04.html' )
                        }

                        return this.getPath( '30533-03.html' )

                    case SILVERA:
                        switch ( state.getCondition() ) {
                            case 1:
                            case 2:
                                return this.getPath( '30527-01.html' )

                            case 3:
                                state.setConditionWithSound( 4, true )
                                return this.getPath( '30527-02.html' )

                            case 4:
                                return this.getPath( '30527-04.html' )

                            case 5:
                                await QuestHelper.takeSingleItem( player, GEMSTONE, -1 )
                                await QuestHelper.giveSingleItem( player, STOLEN_CALCULATOR, 1 )
                                state.setConditionWithSound( 6, true )
                                return this.getPath( '30527-03.html' )

                            case 6:
                                return this.getPath( '30527-05.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}