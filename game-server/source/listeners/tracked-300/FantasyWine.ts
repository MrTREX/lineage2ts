import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'
import { createItemDefinition, ItemDefinition } from '../../gameService/interface/ItemDefinition'

const HARLAN = 30074
const LEAF_OF_EUCALYPTUS = createItemDefinition( 5893, 80 )
const STONE_OF_CHILL = createItemDefinition( 5894, 100 )
const OLD_WINE_15_YEAR = 5956
const OLD_WINE_30_YEAR = 5957
const OLD_WINE_60_YEAR = 5958
const ENKU_ORC_CHAMPION = 20291
const ENKU_ORC_SHAMAN = 20292
const minimumLevel = 20

export class FantasyWine extends ListenerLogic {
    constructor() {
        super( 'Q00379_FantasyWine', 'listeners/tracked-300/FantasyWine.ts' )
        this.questId = 379
        this.questItemIds = [
            LEAF_OF_EUCALYPTUS.id,
            STONE_OF_CHILL.id,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            ENKU_ORC_CHAMPION,
            ENKU_ORC_SHAMAN,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00379_FantasyWine'
    }

    getQuestStartIds(): Array<number> {
        return [ HARLAN ]
    }

    getTalkIds(): Array<number> {
        return [ HARLAN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        let reward: ItemDefinition = data.npcId === ENKU_ORC_CHAMPION ? LEAF_OF_EUCALYPTUS : STONE_OF_CHILL
        await QuestHelper.rewardUpToLimit( player, reward.id, 1, reward.count, data.isChampion )

        if ( QuestHelper.hasEachItem( player, [ LEAF_OF_EUCALYPTUS, STONE_OF_CHILL ] ) ) {
            state.setConditionWithSound( 2 )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30074-02.htm':
            case '30074-03.htm':
            case '30074-05.html':
                break

            case '30074-04.htm':
                state.startQuest()
                break

            case '30074-11.html':
                let player = L2World.getPlayer( data.playerId )
                if ( QuestHelper.hasEachItem( player, [ LEAF_OF_EUCALYPTUS, STONE_OF_CHILL ] ) ) {

                    await QuestHelper.removeAllItems( player, [ LEAF_OF_EUCALYPTUS, STONE_OF_CHILL ], true )

                    let [ itemId, nextEvent ] = this.getReward( data.eventName )

                    await QuestHelper.rewardSingleItem( player, itemId, 1 )
                    await state.exitQuest( true, true )

                    return this.getPath( nextEvent )
                }

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30074-01.htm' : '30074-06.html' )

            case QuestStateValues.STARTED:
                const hasLeafOfEucalyptus = QuestHelper.hasItem( player, LEAF_OF_EUCALYPTUS )
                const hasStoneOfChill = QuestHelper.hasItem( player, STONE_OF_CHILL )

                if ( !hasLeafOfEucalyptus && !hasStoneOfChill ) {
                    return this.getPath( '30074-07.html' )
                }

                if ( hasLeafOfEucalyptus && !hasStoneOfChill ) {
                    return this.getPath( '30074-08.html' )
                }

                if ( !hasLeafOfEucalyptus && hasStoneOfChill ) {
                    return this.getPath( '30074-09.html' )
                }

                return this.getPath( '30074-10.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getReward( defaultEvent: string ): [ number, string ] {
        let chance = _.random( 10 )

        if ( chance < 3 ) {
            return [ OLD_WINE_15_YEAR, defaultEvent ]
        }

        if ( chance < 9 ) {
            return [ OLD_WINE_30_YEAR, '30074-12.html' ]
        }

        return [ OLD_WINE_60_YEAR, '30074-13.html' ]
    }
}