import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const GALIBREDO = 30181
const PATRIN = 30929
const GALFREDO_ROMERS_BUST = 4252
const SCULPTOR_BERONA = 4350
const ANCIENT_STATUE_PROTOTYPE = 4351
const ANCIENT_STATUE_ORIGINAL = 4352
const ANCIENT_STATUE_REPLICA = 4353
const ANCIENT_STATUE_FORGERY = 4354
const minimumLevel = 36

type MonsterChances = [ number, number ] // first, second
const monsterRewardChances: { [ npcId: number ]: MonsterChances } = {
    20767: [ 560, 684 ], // timak orc troop leader
    20768: [ 530, 650 ], // timak orc troop shaman
    20769: [ 420, 516 ], // timak orc troop warrior
    20770: [ 440, 560 ], // timak orc troop archer
}

export class FamilyHonor extends ListenerLogic {
    constructor() {
        super( 'Q00355_FamilyHonor', 'listeners/tracked-300/FamilyHonor.ts' )
        this.questId = 355
        this.questItemIds = [
            GALFREDO_ROMERS_BUST,
        ]
    }

    getAdenaReward( amount: number ): number {
        return amount * 120
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00355_FamilyHonor'
    }

    getQuestStartIds(): Array<number> {
        return [ GALIBREDO ]
    }

    getTalkIds(): Array<number> {
        return [ GALIBREDO, PATRIN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        let [ firstChance, secondChance ] = monsterRewardChances[ data.npcId ]
        let random = _.random( 1000 )

        if ( random < QuestHelper.getAdjustedChance( GALFREDO_ROMERS_BUST, firstChance, data.isChampion ) ) {
            await QuestHelper.rewardSingleQuestItem( player, GALFREDO_ROMERS_BUST, 1, data.isChampion )
            return
        }

        if ( random < QuestHelper.getAdjustedChance( SCULPTOR_BERONA, secondChance, data.isChampion ) ) {
            await QuestHelper.rewardSingleQuestItem( player, SCULPTOR_BERONA, 1, data.isChampion )
            return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30181-02.htm':
            case '30181-09.html':
            case '30929-01.html':
            case '30929-02.html':
                break

            case '30181-03.htm':
                state.startQuest()
                break

            case '30181-06.html':
                let itemCount = QuestHelper.getQuestItemsCount( player, GALFREDO_ROMERS_BUST )

                if ( itemCount === 0 ) {
                    break
                }

                await QuestHelper.takeSingleItem( player, GALFREDO_ROMERS_BUST, -1 )

                if ( itemCount >= 100 ) {
                    await QuestHelper.giveAdena( player, this.getAdenaReward( itemCount ) + 7800, true )
                    return this.getPath( '30181-07.html' )
                }

                await QuestHelper.giveAdena( player, this.getAdenaReward( itemCount ) + 2800, true )
                return this.getPath( '30181-08.html' )

            case '30181-10.html':
                let bustCount = QuestHelper.getQuestItemsCount( player, GALFREDO_ROMERS_BUST )

                if ( bustCount > 0 ) {
                    await QuestHelper.giveAdena( player, this.getAdenaReward( bustCount ), true )
                }

                await QuestHelper.takeSingleItem( player, GALFREDO_ROMERS_BUST, -1 )
                await state.exitQuest( true, true )
                break

            case '30929-03.html':
                if ( !QuestHelper.hasQuestItem( player, SCULPTOR_BERONA ) ) {
                    return this.getPath( '30929-08.html' )
                }

                await QuestHelper.takeSingleItem( player, SCULPTOR_BERONA, -1 )

                let random = _.random( 100 )
                if ( random < 2 ) {
                    await QuestHelper.giveSingleItem( player, ANCIENT_STATUE_PROTOTYPE, 1 )
                    break
                }

                if ( random < 32 ) {
                    await QuestHelper.giveSingleItem( player, ANCIENT_STATUE_ORIGINAL, 1 )
                    return this.getPath( '30929-04.html' )
                }

                if ( random < 62 ) {
                    await QuestHelper.giveSingleItem( player, ANCIENT_STATUE_REPLICA, 1 )
                    return this.getPath( '30929-05.html' )
                }

                if ( random < 77 ) {
                    await QuestHelper.giveSingleItem( player, ANCIENT_STATUE_FORGERY, 1 )
                    return this.getPath( '30929-06.html' )
                }

                return this.getPath( '30929-07.html' )

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30181-01.htm' : '30181-04.html' )

            case QuestStateValues.STARTED:
                if ( data.characterNpcId === GALIBREDO ) {
                    if ( QuestHelper.hasQuestItem( player, SCULPTOR_BERONA ) ) {
                        return this.getPath( '30181-11.html' )
                    }

                    return this.getPath( '30181-05.html' )
                }

                return this.getPath( '30929-01.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}