import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const GAUEN = 30717
const CARNIVORE_SPORE = 5865
const HERBIVOROUS_SPORE = 5866
const ROTTING_TREE = 20558
const SPORE_ZOMBIE = 20562
const minimumLevel = 43
const requiredAmount = 50

type MonsterRewardChance = [ number, number ] // chance, itemId
const monsterChanceRewards: { [ npcId: number ]: MonsterRewardChance } = {
    [ ROTTING_TREE ]: [ 0.73, HERBIVOROUS_SPORE ],
    [ SPORE_ZOMBIE ]: [ 0.94, CARNIVORE_SPORE ],
}

export class DigUpTheSeaOfSpores extends ListenerLogic {
    constructor() {
        super( 'Q00356_DigUpTheSeaOfSpores', 'listeners/tracked-300/DigUpTheSeaOfSpores.ts' )
        this.questId = 356
        this.questItemIds = [
            HERBIVOROUS_SPORE,
            CARNIVORE_SPORE,
        ]
    }

    async finishQuest( player: L2PcInstance ): Promise<string> {

        let value = _.random( 100 )
        if ( value < 20 ) {
            await QuestHelper.giveAdena( player, 44000, true )
            return this.getPath( '30717-15.html' )
        }

        if ( value < 70 ) {
            await QuestHelper.giveAdena( player, 20950, true )
            return this.getPath( '30717-16.html' )
        }

        await QuestHelper.giveAdena( player, 10400, true )
        return this.getPath( '30717-17.html' )
    }

    getAttackableKillIds(): Array<number> {
        return [ ROTTING_TREE, SPORE_ZOMBIE ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00356_DigUpTheSeaOfSpores'
    }

    getQuestStartIds(): Array<number> {
        return [ GAUEN ]
    }

    getTalkIds(): Array<number> {
        return [ GAUEN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isStarted() || state.isCondition( 3 ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        let [ chance, itemId ] = monsterChanceRewards[ data.npcId ]
        if ( Math.random() > QuestHelper.getAdjustedChance( itemId, chance, data.isChampion ) ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, itemId, 1, data.isChampion )
        if ( QuestHelper.getQuestItemsCount( player, HERBIVOROUS_SPORE ) >= requiredAmount
                && QuestHelper.getQuestItemsCount( player, CARNIVORE_SPORE ) >= requiredAmount ) {
            state.setConditionWithSound( 3, true )
            return
        }

        if ( state.isCondition( 1 ) ) {
            state.setConditionWithSound( 2 )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30717-02.htm':
            case '30717-03.htm':
            case '30717-04.htm':
            case '30717-10.html':
            case '30717-18.html':
                break

            case '30717-05.htm':
                state.startQuest()
                break

            case '30717-09.html':
                await QuestHelper.addExpAndSp( player, 31850, 0 )
                await QuestHelper.takeMultipleItems( player, -1, CARNIVORE_SPORE, HERBIVOROUS_SPORE )
                break

            case '30717-11.html':
                await state.exitQuest( true, true )
                break

            case '30717-14.html':
                await QuestHelper.addExpAndSp( player, 45500, 2600 )
                await state.exitQuest( true, true )
                break

            case 'FINISH':
                await state.exitQuest( true, true )
                return this.finishQuest( player )


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30717-01.htm' : '30717-06.htm' )

            case QuestStateValues.STARTED:
                if ( !state.isCondition( 3 ) ) {
                    return this.getPath( '30717-07.html' )
                }

                let hasFirstKind: boolean = QuestHelper.getQuestItemsCount( player, HERBIVOROUS_SPORE ) >= requiredAmount
                let hasSecondKind: boolean = QuestHelper.getQuestItemsCount( player, CARNIVORE_SPORE ) >= requiredAmount

                if ( hasFirstKind && hasSecondKind ) {
                    return this.getPath( '30717-13.html' )
                }

                if ( hasSecondKind ) {
                    return this.getPath( '30717-12.html' )
                }

                if ( hasFirstKind ) {
                    return this.getPath( '30717-08.html' )
                }

                return this.getPath( '30717-07.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}