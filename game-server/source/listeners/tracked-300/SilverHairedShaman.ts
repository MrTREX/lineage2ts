import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const DIETER = 30111
const SAIRONS_SILVER_HAIR = 5874
const minimumLevel = 48

const monsterRewardChances = {
    20986: 0.8, // saitnn
    20987: 0.73, // saitnn doll
    20988: 0.8, // saitnn puppet
}

export class SilverHairedShaman extends ListenerLogic {
    constructor() {
        super( 'Q00366_SilverHairedShaman', 'listeners/tracked-300/SilverHairedShaman.ts' )
        this.questId = 366
        this.questItemIds = [
            SAIRONS_SILVER_HAIR,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00366_SilverHairedShaman'
    }

    getQuestStartIds(): Array<number> {
        return [ DIETER ]
    }

    getTalkIds(): Array<number> {
        return [ DIETER ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( Math.random() > QuestHelper.getAdjustedChance( SAIRONS_SILVER_HAIR, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        let player: L2PcInstance = this.getRandomPartyMemberPerCondition( L2World.getPlayer( data.playerId ), -1, 3, L2World.getObjectById( data.targetId ) as L2Npc )
        if ( !player ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, SAIRONS_SILVER_HAIR, 1, data.isChampion )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30111-02.htm':
                state.startQuest()
                break

            case '30111-05.html':
                await state.exitQuest( true, true )
                break

            case '30111-06.html':
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30111-01.htm' : '30111-03.html' )

            case QuestStateValues.STARTED:
                let amount: number = QuestHelper.getQuestItemsCount( player, SAIRONS_SILVER_HAIR )
                if ( amount > 0 ) {
                    let adena = amount * 500 + 29000
                    await QuestHelper.giveAdena( player, adena, true )
                    await QuestHelper.takeSingleItem( player, SAIRONS_SILVER_HAIR, -1 )

                    return this.getPath( '30111-04.html' )
                }

                return this.getPath( '30111-07.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}