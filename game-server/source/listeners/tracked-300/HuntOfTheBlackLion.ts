import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { CategoryType } from '../../gameService/enums/CategoryType'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { ItemTypes } from '../../gameService/values/InventoryValues'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const ABYSSAL_CELEBRANT_UNDRIAS = 30130
const BLACKSMITH_RUPIO = 30471
const IRON_GATES_LOCKIRIN = 30531
const MERCENARY_CAPTAIN_SOPHYA = 30735
const MERCENARY_REEDFOOT = 30736
const GUILDSMAN_MORGON = 30737
const BLACK_LION_MARK = 1369
const CARGO_BOX_1ST = 3440
const CARGO_BOX_2ND = 3441
const CARGO_BOX_3RD = 3442
const CARGO_BOX_4TH = 3443
const STATUE_OF_SHILEN_HEAD = 3457
const STATUE_OF_SHILEN_TORSO = 3458
const STATUE_OF_SHILEN_ARM = 3459
const STATUE_OF_SHILEN_LEG = 3460
const COMPLETE_STATUE_OF_SHILEN = 3461
const FRAGMENT_OF_ANCIENT_TABLET_1ST_PIECE = 3462
const FRAGMENT_OF_ANCIENT_TABLET_2ND_PIECE = 3463
const FRAGMENT_OF_ANCIENT_TABLET_3RD_PIECE = 3464
const FRAGMENT_OF_ANCIENT_TABLET_4TH_PIECE = 3465
const COMPLETE_ANCIENT_TABLET = 3466
const SOPHYAS_1ST_ORDER = 3671
const SOPHYAS_2ND_ORDER = 3672
const SOPHYAS_3RD_ORDER = 3673
const SOPHYAS_4TH_ORDER = 3674
const LIONS_CLAW = 3675
const LIONS_EYE = 3676
const GUILD_COIN = 3677
const UNDEAD_ASH = 3848
const BLOODY_AXE_INSIGNIA = 3849
const DELU_LIZARDMAN_FANG = 3850
const STAKATO_TALON = 3851
const ALACRITY_POTION = 735
const SCROLL_OF_ESCAPE = 736
const HEALING_POTION = 1061
const SOULSHOT_D_GRADE = 1463
const SPIRITSHOT_D_GRADE = 2510
const GLUDIO_APPLES = 3444
const DION_CORN_MEAL = 3445
const DIRE_WOLF_PELTS = 3446
const MOONSTONE = 3447
const GLUDIO_WHEAT_FLOUR = 3448
const SPIDERSILK_ROPE = 3449
const ALEXANDRITE = 3450
const SILVER_TEA_SERVICE = 3451
const MECHANIC_GOLEM_SPACE_PARTS = 3452
const FIRE_EMERALD = 3453
const AVELLAN_SILK_FROCK = 3454
const FERIOTIC_PORCELAIN_URM = 3455
const IMPERIAL_DIAMOND = 3456
const MARSH_STAKATO = 20157
const NEER_CRAWLER = 20160
const SPECTER = 20171
const SORROW_MAIDEN = 20197
const NEER_CRAWLER_BERSERKER = 20198
const STRAIN = 20200
const GHOUL = 20201
const OL_MAHUM_GUERILLA = 20207
const OL_MAHUM_RAIDER = 20208
const OL_MAHUM_MARKSMAN = 20209
const OL_MAHUM_SERGEANT = 20210
const OL_MAHUM_CAPTAIN = 20211
const MARSH_STAKATO_WORKER = 20230
const MARSH_STAKATO_SOLDIER = 20232
const MARSH_STAKATO_DRONE = 20234
const DELU_LIZARDMAN = 20251
const DELU_LIZARDMAN_SCOUT = 20252
const DELU_LIZARDMAN_WARRIOR = 20253
const DELU_LIZARDMAN_HEADHUNTER = 27151
const MARSH_STAKATO_MARQUESS = 27152
const minimumLevel = 25

export class HuntOfTheBlackLion extends ListenerLogic {
    constructor() {
        super( 'Q00333_HuntOfTheBlackLion', 'listeners/tracked-300/HuntOfTheBlackLion.ts' )
        this.questId = 333
        this.questItemIds = [
            BLACK_LION_MARK,
            CARGO_BOX_1ST,
            CARGO_BOX_2ND,
            CARGO_BOX_3RD,
            CARGO_BOX_4TH,
            STATUE_OF_SHILEN_HEAD,
            STATUE_OF_SHILEN_TORSO,
            STATUE_OF_SHILEN_ARM,
            STATUE_OF_SHILEN_LEG,
            COMPLETE_STATUE_OF_SHILEN,
            FRAGMENT_OF_ANCIENT_TABLET_1ST_PIECE,
            FRAGMENT_OF_ANCIENT_TABLET_2ND_PIECE,
            FRAGMENT_OF_ANCIENT_TABLET_3RD_PIECE,
            FRAGMENT_OF_ANCIENT_TABLET_4TH_PIECE,
            COMPLETE_ANCIENT_TABLET,
            SOPHYAS_1ST_ORDER,
            SOPHYAS_2ND_ORDER,
            SOPHYAS_3RD_ORDER,
            SOPHYAS_4TH_ORDER,
            LIONS_CLAW,
            LIONS_EYE,
            GUILD_COIN,
            UNDEAD_ASH,
            BLOODY_AXE_INSIGNIA,
            DELU_LIZARDMAN_FANG,
            STAKATO_TALON,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            MARSH_STAKATO,
            NEER_CRAWLER,
            SPECTER,
            SORROW_MAIDEN,
            NEER_CRAWLER_BERSERKER,
            STRAIN,
            GHOUL,
            OL_MAHUM_GUERILLA,
            OL_MAHUM_RAIDER,
            OL_MAHUM_MARKSMAN,
            OL_MAHUM_SERGEANT,
            OL_MAHUM_CAPTAIN,
            MARSH_STAKATO_WORKER,
            MARSH_STAKATO_SOLDIER,
            MARSH_STAKATO_DRONE,
            DELU_LIZARDMAN,
            DELU_LIZARDMAN_SCOUT,
            DELU_LIZARDMAN_WARRIOR,
            DELU_LIZARDMAN_HEADHUNTER,
            MARSH_STAKATO_MARQUESS,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00333_HuntOfTheBlackLion'
    }

    getQuestStartIds(): Array<number> {
        return [ MERCENARY_CAPTAIN_SOPHYA ]
    }

    getTalkIds(): Array<number> {
        return [
            MERCENARY_CAPTAIN_SOPHYA,
            ABYSSAL_CELEBRANT_UNDRIAS,
            BLACKSMITH_RUPIO,
            IRON_GATES_LOCKIRIN,
            MERCENARY_REEDFOOT,
            GUILDSMAN_MORGON,
        ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( npc.getId() ) {
            case MARSH_STAKATO:
                if ( QuestHelper.hasQuestItem( player, SOPHYAS_4TH_ORDER ) ) {
                    if ( _.random( 100 ) < QuestHelper.getAdjustedChance( STAKATO_TALON, 55, data.isChampion ) ) {
                        await QuestHelper.rewardSingleQuestItem( player, STAKATO_TALON, 1, data.isChampion )
                    }

                    if ( _.random( 100 ) < QuestHelper.getAdjustedChance( CARGO_BOX_4TH, 12, data.isChampion ) ) {
                        await QuestHelper.rewardSingleQuestItem( player, CARGO_BOX_4TH, 1, data.isChampion )
                    }

                    if ( _.random( 100 ) < 2 ) {
                        QuestHelper.addSpawnAtLocation( MARSH_STAKATO_MARQUESS, npc, true, 0 )
                    }
                }

                return

            case NEER_CRAWLER:
                if ( QuestHelper.hasQuestItem( player, SOPHYAS_1ST_ORDER ) ) {
                    if ( _.random( 1 ) === 0 ) {
                        await QuestHelper.rewardSingleQuestItem( player, UNDEAD_ASH, 1, data.isChampion )
                    }

                    if ( _.random( 100 ) < QuestHelper.getAdjustedChance( CARGO_BOX_1ST, 11, data.isChampion ) ) {
                        await QuestHelper.rewardSingleQuestItem( player, CARGO_BOX_1ST, 1, data.isChampion )
                    }
                }

                return

            case SPECTER:
                if ( QuestHelper.hasQuestItem( player, SOPHYAS_1ST_ORDER ) ) {
                    if ( _.random( 100 ) < QuestHelper.getAdjustedChance( UNDEAD_ASH, 60, data.isChampion ) ) {
                        await QuestHelper.rewardSingleQuestItem( player, UNDEAD_ASH, 1, data.isChampion )
                    }

                    if ( _.random( 100 ) < QuestHelper.getAdjustedChance( CARGO_BOX_1ST, 8, data.isChampion ) ) {
                        await QuestHelper.rewardSingleQuestItem( player, CARGO_BOX_1ST, 1, data.isChampion )
                    }
                }

                return

            case SORROW_MAIDEN:
                if ( QuestHelper.hasQuestItem( player, SOPHYAS_1ST_ORDER ) ) {
                    if ( _.random( 100 ) < QuestHelper.getAdjustedChance( UNDEAD_ASH, 60, data.isChampion ) ) {
                        await QuestHelper.rewardSingleQuestItem( player, UNDEAD_ASH, 1, data.isChampion )
                    }

                    if ( _.random( 100 ) < QuestHelper.getAdjustedChance( CARGO_BOX_1ST, 9, data.isChampion ) ) {
                        await QuestHelper.rewardSingleQuestItem( player, CARGO_BOX_1ST, 1, data.isChampion )
                    }
                }

                return

            case NEER_CRAWLER_BERSERKER:
                if ( QuestHelper.hasQuestItem( player, SOPHYAS_1ST_ORDER ) ) {
                    if ( _.random( 1 ) === 0 ) {
                        await QuestHelper.rewardSingleQuestItem( player, UNDEAD_ASH, 1, data.isChampion )
                    }

                    if ( _.random( 100 ) < QuestHelper.getAdjustedChance( CARGO_BOX_1ST, 12, data.isChampion ) ) {
                        await QuestHelper.rewardSingleQuestItem( player, CARGO_BOX_1ST, 1, data.isChampion )
                    }
                }

                return

            case STRAIN:
                if ( QuestHelper.hasQuestItem( player, SOPHYAS_1ST_ORDER ) ) {
                    if ( _.random( 1 ) === 0 ) {
                        await QuestHelper.rewardSingleQuestItem( player, UNDEAD_ASH, 1, data.isChampion )
                    }

                    if ( _.random( 100 ) < QuestHelper.getAdjustedChance( CARGO_BOX_1ST, 13, data.isChampion ) ) {
                        await QuestHelper.rewardSingleQuestItem( player, CARGO_BOX_1ST, 1, data.isChampion )
                    }
                }

                return

            case GHOUL:
                if ( QuestHelper.hasQuestItem( player, SOPHYAS_1ST_ORDER ) ) {
                    if ( _.random( 1 ) === 0 ) {
                        await QuestHelper.rewardSingleQuestItem( player, UNDEAD_ASH, 1, data.isChampion )
                    }

                    if ( _.random( 100 ) < QuestHelper.getAdjustedChance( CARGO_BOX_1ST, 15, data.isChampion ) ) {
                        await QuestHelper.rewardSingleQuestItem( player, CARGO_BOX_1ST, 1, data.isChampion )
                    }
                }

                return

            case OL_MAHUM_GUERILLA:
                if ( QuestHelper.hasQuestItem( player, SOPHYAS_2ND_ORDER ) ) {
                    if ( _.random( 1 ) === 0 ) {
                        await QuestHelper.rewardSingleQuestItem( player, BLOODY_AXE_INSIGNIA, 1, data.isChampion )
                    }

                    if ( _.random( 100 ) < QuestHelper.getAdjustedChance( CARGO_BOX_2ND, 9, data.isChampion ) ) {
                        await QuestHelper.rewardSingleQuestItem( player, CARGO_BOX_2ND, 1, data.isChampion )
                    }
                }

                return

            case OL_MAHUM_RAIDER:
                if ( QuestHelper.hasQuestItem( player, SOPHYAS_2ND_ORDER ) ) {
                    if ( _.random( 1 ) === 0 ) {
                        await QuestHelper.rewardSingleQuestItem( player, BLOODY_AXE_INSIGNIA, 1, data.isChampion )
                    }

                    if ( _.random( 100 ) < QuestHelper.getAdjustedChance( CARGO_BOX_2ND, 10, data.isChampion ) ) {
                        await QuestHelper.rewardSingleQuestItem( player, CARGO_BOX_2ND, 1, data.isChampion )
                    }
                }

                return

            case OL_MAHUM_MARKSMAN:
                if ( QuestHelper.hasQuestItem( player, SOPHYAS_2ND_ORDER ) ) {
                    if ( _.random( 1 ) === 0 ) {
                        await QuestHelper.rewardSingleQuestItem( player, BLOODY_AXE_INSIGNIA, 1, data.isChampion )
                    }

                    if ( _.random( 100 ) < QuestHelper.getAdjustedChance( CARGO_BOX_2ND, 11, data.isChampion ) ) {
                        await QuestHelper.rewardSingleQuestItem( player, CARGO_BOX_2ND, 1, data.isChampion )
                    }
                }

                return

            case OL_MAHUM_SERGEANT:
                if ( QuestHelper.hasQuestItem( player, SOPHYAS_2ND_ORDER ) ) {
                    if ( _.random( 1 ) === 0 ) {
                        await QuestHelper.rewardSingleQuestItem( player, BLOODY_AXE_INSIGNIA, 1, data.isChampion )
                    }

                    if ( _.random( 100 ) < QuestHelper.getAdjustedChance( CARGO_BOX_2ND, 12, data.isChampion ) ) {
                        await QuestHelper.rewardSingleQuestItem( player, CARGO_BOX_2ND, 1, data.isChampion )
                    }
                }

                return

            case OL_MAHUM_CAPTAIN:
                if ( QuestHelper.hasQuestItem( player, SOPHYAS_2ND_ORDER ) ) {
                    if ( _.random( 1 ) === 0 ) {
                        await QuestHelper.rewardSingleQuestItem( player, BLOODY_AXE_INSIGNIA, 1, data.isChampion )
                    }

                    if ( _.random( 100 ) < QuestHelper.getAdjustedChance( CARGO_BOX_2ND, 13, data.isChampion ) ) {
                        await QuestHelper.rewardSingleQuestItem( player, CARGO_BOX_2ND, 1, data.isChampion )
                    }
                }

                return

            case MARSH_STAKATO_WORKER:
                if ( QuestHelper.hasQuestItem( player, SOPHYAS_4TH_ORDER ) ) {
                    if ( _.random( 100 ) < QuestHelper.getAdjustedChance( STAKATO_TALON, 60, data.isChampion ) ) {
                        await QuestHelper.rewardSingleQuestItem( player, STAKATO_TALON, 1, data.isChampion )
                    }

                    if ( _.random( 100 ) < QuestHelper.getAdjustedChance( CARGO_BOX_4TH, 13, data.isChampion ) ) {
                        await QuestHelper.rewardSingleQuestItem( player, CARGO_BOX_4TH, 1, data.isChampion )
                    }

                    if ( _.random( 100 ) < 2 ) {
                        QuestHelper.addSpawnAtLocation( MARSH_STAKATO_MARQUESS, npc, true, 0 )
                    }
                }

                return

            case MARSH_STAKATO_SOLDIER:
                if ( QuestHelper.hasQuestItem( player, SOPHYAS_4TH_ORDER ) ) {
                    if ( _.random( 100 ) < QuestHelper.getAdjustedChance( STAKATO_TALON, 56, data.isChampion ) ) {
                        await QuestHelper.rewardSingleQuestItem( player, STAKATO_TALON, 1, data.isChampion )
                    }

                    if ( _.random( 100 ) < QuestHelper.getAdjustedChance( CARGO_BOX_4TH, 14, data.isChampion ) ) {
                        await QuestHelper.rewardSingleQuestItem( player, CARGO_BOX_4TH, 1, data.isChampion )
                    }

                    if ( _.random( 100 ) < 2 ) {
                        QuestHelper.addSpawnAtLocation( MARSH_STAKATO_MARQUESS, npc, true, 0 )
                    }
                }

                return

            case MARSH_STAKATO_DRONE:
                if ( QuestHelper.hasQuestItem( player, SOPHYAS_4TH_ORDER ) ) {
                    if ( _.random( 100 ) < QuestHelper.getAdjustedChance( STAKATO_TALON, 60, data.isChampion ) ) {
                        await QuestHelper.rewardSingleQuestItem( player, STAKATO_TALON, 1, data.isChampion )
                    }

                    if ( _.random( 100 ) < QuestHelper.getAdjustedChance( CARGO_BOX_4TH, 15, data.isChampion ) ) {
                        await QuestHelper.rewardSingleQuestItem( player, CARGO_BOX_4TH, 1, data.isChampion )
                    }

                    if ( _.random( 100 ) < 2 ) {
                        QuestHelper.addSpawnAtLocation( MARSH_STAKATO_MARQUESS, npc, true, 0 )
                    }
                }

                return

            case DELU_LIZARDMAN:
            case DELU_LIZARDMAN_SCOUT:
                if ( QuestHelper.hasQuestItem( player, SOPHYAS_3RD_ORDER ) ) {
                    if ( _.random( 1 ) === 0 ) {
                        await QuestHelper.rewardSingleQuestItem( player, DELU_LIZARDMAN_FANG, 1, data.isChampion )
                    }

                    if ( _.random( 100 ) < QuestHelper.getAdjustedChance( CARGO_BOX_3RD, 14, data.isChampion ) ) {
                        await QuestHelper.rewardSingleQuestItem( player, CARGO_BOX_3RD, 1, data.isChampion )
                    }

                    if ( _.random( 100 ) < 3 ) {
                        QuestHelper.addSpawnAtLocation( DELU_LIZARDMAN_HEADHUNTER, npc, true, 0 )
                        QuestHelper.addSpawnAtLocation( DELU_LIZARDMAN_HEADHUNTER, npc, true, 0 )
                    }
                }

                return

            case DELU_LIZARDMAN_WARRIOR:
                if ( QuestHelper.hasQuestItem( player, SOPHYAS_3RD_ORDER ) ) {
                    if ( _.random( 1 ) === 0 ) {
                        await QuestHelper.rewardSingleQuestItem( player, DELU_LIZARDMAN_FANG, 1, data.isChampion )
                    }

                    if ( _.random( 100 ) < QuestHelper.getAdjustedChance( CARGO_BOX_3RD, 15, data.isChampion ) ) {
                        await QuestHelper.rewardSingleQuestItem( player, CARGO_BOX_3RD, 1, data.isChampion )
                    }

                    if ( _.random( 100 ) < 3 ) {
                        QuestHelper.addSpawnAtLocation( DELU_LIZARDMAN_HEADHUNTER, npc, true, 0 )
                        QuestHelper.addSpawnAtLocation( DELU_LIZARDMAN_HEADHUNTER, npc, true, 0 )
                    }
                }

                return

            case DELU_LIZARDMAN_HEADHUNTER:
                if ( QuestHelper.hasQuestItem( player, SOPHYAS_3RD_ORDER ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, DELU_LIZARDMAN_FANG, 4, data.isChampion )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case MARSH_STAKATO_MARQUESS:
                if ( QuestHelper.hasQuestItem( player, SOPHYAS_4TH_ORDER ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, STAKATO_TALON, 8, data.isChampion )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30735-04.htm':
                state.startQuest()
                break

            case '30735-05.html':
            case '30735-06.html':
            case '30735-07.html':
            case '30735-08.html':
            case '30735-09.html':
            case '30130-05.html':
            case '30531-05.html':
            case '30735-21.html':
            case '30735-24a.html':
            case '30735-25b.html':
            case '30736-06.html':
            case '30736-09.html':
            case '30737-07.html':
                break

            case '30735-10.html':
                if ( !QuestHelper.hasQuestItem( player, SOPHYAS_1ST_ORDER ) ) {
                    await QuestHelper.giveSingleItem( player, SOPHYAS_1ST_ORDER, 1 )
                    break
                }

                return

            case '30735-11.html':
                if ( !QuestHelper.hasQuestItem( player, SOPHYAS_2ND_ORDER ) ) {
                    await QuestHelper.giveSingleItem( player, SOPHYAS_2ND_ORDER, 1 )
                    break
                }

                return

            case '30735-12.html':
                if ( !QuestHelper.hasQuestItem( player, SOPHYAS_3RD_ORDER ) ) {
                    await QuestHelper.giveSingleItem( player, SOPHYAS_3RD_ORDER, 1 )
                    break
                }

                return

            case '30735-13.html':
                if ( !QuestHelper.hasQuestItem( player, SOPHYAS_4TH_ORDER ) ) {
                    await QuestHelper.giveSingleItem( player, SOPHYAS_4TH_ORDER, 1 )
                    break
                }

                return

            case '30735-16.html':
                if ( QuestHelper.getQuestItemsCount( player, LIONS_CLAW ) < 10 ) {
                    break
                }

                await QuestHelper.takeSingleItem( player, LIONS_CLAW, 10 )

                if ( QuestHelper.getQuestItemsCount( player, LIONS_EYE ) < 4 ) {
                    await QuestHelper.giveSingleItem( player, LIONS_EYE, 1 )
                    await this.rewardItems( player, 20, 1, 20, 3 )

                    return this.getPath( '30735-17a.html' )
                }

                if ( QuestHelper.getQuestItemsCount( player, LIONS_EYE ) <= 7 ) {
                    await QuestHelper.giveSingleItem( player, LIONS_EYE, 1 )
                    await QuestHelper.takeSingleItem( player, LIONS_CLAW, 10 )
                    await this.rewardItems( player, 25, 2, 20, 3 )

                    return this.getPath( '30735-18b.html' )
                }

                await QuestHelper.takeSingleItem( player, LIONS_EYE, 8 )
                await this.rewardItems( player, 50, 4, 30, 4 )

                return this.getPath( '30735-19b.html' )

            case '30735-20.html':
                await QuestHelper.takeMultipleItems( player, -1, SOPHYAS_1ST_ORDER, SOPHYAS_2ND_ORDER, SOPHYAS_3RD_ORDER, SOPHYAS_4TH_ORDER )
                break

            case '30735-26.html':
                if ( QuestHelper.hasQuestItem( player, BLACK_LION_MARK ) ) {
                    await QuestHelper.giveAdena( player, 12400, true )
                    await state.exitQuest( true, true )

                    break
                }
                return

            case '30130-04.html':
                if ( QuestHelper.hasQuestItem( player, COMPLETE_STATUE_OF_SHILEN ) ) {
                    await QuestHelper.giveAdena( player, 30000, true )
                    await QuestHelper.takeSingleItem( player, COMPLETE_STATUE_OF_SHILEN, -1 )

                    break
                }

                return

            case '30471-03.html':
                if ( !QuestHelper.hasQuestItems( player, STATUE_OF_SHILEN_HEAD, STATUE_OF_SHILEN_TORSO, STATUE_OF_SHILEN_ARM, STATUE_OF_SHILEN_LEG ) ) {
                    break
                }

                await QuestHelper.takeMultipleItems( player, STATUE_OF_SHILEN_HEAD, STATUE_OF_SHILEN_TORSO, STATUE_OF_SHILEN_ARM, STATUE_OF_SHILEN_LEG )
                if ( Math.random() < 0.5 ) {
                    await QuestHelper.giveSingleItem( player, COMPLETE_STATUE_OF_SHILEN, 1 )
                    return this.getPath( '30471-04.html' )
                }

                return this.getPath( '30471-05.html' )

            case '30471-06.html':
                if ( !QuestHelper.hasQuestItems( player, FRAGMENT_OF_ANCIENT_TABLET_1ST_PIECE, FRAGMENT_OF_ANCIENT_TABLET_2ND_PIECE, FRAGMENT_OF_ANCIENT_TABLET_3RD_PIECE, FRAGMENT_OF_ANCIENT_TABLET_4TH_PIECE ) ) {
                    break
                }

                await QuestHelper.takeMultipleItems( player, 1, FRAGMENT_OF_ANCIENT_TABLET_1ST_PIECE, FRAGMENT_OF_ANCIENT_TABLET_2ND_PIECE, FRAGMENT_OF_ANCIENT_TABLET_3RD_PIECE, FRAGMENT_OF_ANCIENT_TABLET_4TH_PIECE )

                if ( Math.random() < 0.5 ) {
                    await QuestHelper.giveSingleItem( player, COMPLETE_ANCIENT_TABLET, 1 )

                    return this.getPath( '30471-07.html' )
                }

                return this.getPath( '30471-08.html' )

            case '30531-04.html':
                if ( QuestHelper.hasQuestItem( player, COMPLETE_ANCIENT_TABLET ) ) {
                    await QuestHelper.giveAdena( player, 30000, true )
                    await QuestHelper.takeSingleItem( player, COMPLETE_ANCIENT_TABLET, 1 )

                    break
                }

                return

            case '30736-03.html':
                if ( !QuestHelper.hasAtLeastOneQuestItem( player, CARGO_BOX_1ST, CARGO_BOX_2ND, CARGO_BOX_3RD, CARGO_BOX_4TH ) ) {
                    return this.getPath( '30736-05.html' )
                }

                if ( player.getAdena() < 650 ) {
                    break
                }

                await QuestHelper.takeSingleItem( player, ItemTypes.Adena, 650 )
                await QuestHelper.takeMultipleItems( player, 1, CARGO_BOX_1ST, CARGO_BOX_2ND, CARGO_BOX_3RD, CARGO_BOX_4TH )

                let cargoBoxRewardChance = Math.random()
                let chance = Math.random()
                if ( cargoBoxRewardChance < 0.40 ) {
                    let chance = Math.random()
                    if ( chance < 0.33 ) {
                        await QuestHelper.giveSingleItem( player, GLUDIO_APPLES, 1 )
                        return this.getPath( '30736-04a.html' )
                    }

                    if ( chance < 0.66 ) {
                        await QuestHelper.giveSingleItem( player, DION_CORN_MEAL, 1 )
                        return this.getPath( '30736-04b.html' )
                    }

                    await QuestHelper.giveSingleItem( player, DIRE_WOLF_PELTS, 1 )
                    return this.getPath( '30736-04c.html' )
                }

                if ( cargoBoxRewardChance < 0.60 ) {
                    if ( chance < 0.33 ) {
                        await QuestHelper.giveSingleItem( player, MOONSTONE, 1 )
                        return this.getPath( '30736-04d.html' )
                    }

                    if ( chance < 0.66 ) {
                        await QuestHelper.giveSingleItem( player, GLUDIO_WHEAT_FLOUR, 1 )
                        return this.getPath( '30736-04e.html' )
                    }

                    await QuestHelper.giveSingleItem( player, SPIDERSILK_ROPE, 1 )
                    return this.getPath( '30736-04f.html' )
                }

                if ( cargoBoxRewardChance < 0.70 ) {
                    if ( chance < 0.33 ) {
                        await QuestHelper.giveSingleItem( player, ALEXANDRITE, 1 )
                        return this.getPath( '30736-04g.html' )
                    }

                    if ( chance < 0.66 ) {
                        await QuestHelper.giveSingleItem( player, SILVER_TEA_SERVICE, 1 )
                        return this.getPath( '30736-04h.html' )
                    }

                    await QuestHelper.giveSingleItem( player, MECHANIC_GOLEM_SPACE_PARTS, 1 )
                    return this.getPath( '30736-04i.html' )
                }

                if ( cargoBoxRewardChance < 0.75 ) {
                    if ( chance < 0.33 ) {
                        await QuestHelper.giveSingleItem( player, FIRE_EMERALD, 1 )
                        return this.getPath( '30736-04j.html' )
                    }

                    if ( chance < 0.66 ) {
                        await QuestHelper.giveSingleItem( player, AVELLAN_SILK_FROCK, 1 )
                        return this.getPath( '30736-04k.html' )
                    }

                    await QuestHelper.giveSingleItem( player, FERIOTIC_PORCELAIN_URM, 1 )
                    return this.getPath( '30736-04l.html' )
                }

                if ( cargoBoxRewardChance < 0.76 ) {
                    await QuestHelper.giveSingleItem( player, IMPERIAL_DIAMOND, 1 )
                    return this.getPath( '30736-04m.html' )
                }

                if ( Math.random() < 0.50 ) {
                    if ( chance < 0.25 ) {
                        await QuestHelper.giveSingleItem( player, STATUE_OF_SHILEN_HEAD, 1 )
                    } else if ( chance < 0.50 ) {
                        await QuestHelper.giveSingleItem( player, STATUE_OF_SHILEN_TORSO, 1 )
                    } else if ( chance < 0.75 ) {
                        await QuestHelper.giveSingleItem( player, STATUE_OF_SHILEN_ARM, 1 )
                    } else {
                        await QuestHelper.giveSingleItem( player, STATUE_OF_SHILEN_LEG, 1 )
                    }

                    return this.getPath( '30736-04n.html' )
                }

                if ( chance < 25 ) {
                    await QuestHelper.giveSingleItem( player, FRAGMENT_OF_ANCIENT_TABLET_1ST_PIECE, 1 )
                } else if ( chance < 50 ) {
                    await QuestHelper.giveSingleItem( player, FRAGMENT_OF_ANCIENT_TABLET_2ND_PIECE, 1 )
                } else if ( chance < 75 ) {
                    await QuestHelper.giveSingleItem( player, FRAGMENT_OF_ANCIENT_TABLET_3RD_PIECE, 1 )
                } else {
                    await QuestHelper.giveSingleItem( player, FRAGMENT_OF_ANCIENT_TABLET_4TH_PIECE, 1 )
                }

                return this.getPath( '30736-04o.html' )

            case '30736-07.html':
                let requiredAdena = 200 + ( state.getMemoState() * 200 )
                if ( player.getAdena() < requiredAdena ) {
                    break
                }

                if ( ( state.getMemoState() * 100 ) > 200 ) {
                    return this.getPath( '30736-08.html' )
                }

                await QuestHelper.takeSingleItem( player, ItemTypes.Adena, requiredAdena )
                state.setMemoState( state.getMemoState() + 1 )

                let takenAdenaChance = Math.random()
                if ( takenAdenaChance < 0.05 ) {
                    return this.getPath( '30736-08a.html' )
                }

                if ( takenAdenaChance < 0.10 ) {
                    return this.getPath( '30736-08b.html' )
                }

                if ( takenAdenaChance < 0.15 ) {
                    return this.getPath( '30736-08c.html' )
                }

                if ( takenAdenaChance < 0.20 ) {
                    return this.getPath( '30736-08d.html' )
                }

                if ( takenAdenaChance < 0.25 ) {
                    return this.getPath( '30736-08e.html' )
                }

                if ( takenAdenaChance < 0.30 ) {
                    return this.getPath( '30736-08f.html' )
                }

                if ( takenAdenaChance < 0.35 ) {
                    return this.getPath( '30736-08g.html' )
                }

                if ( takenAdenaChance < 0.40 ) {
                    return this.getPath( '30736-08h.html' )
                }

                if ( takenAdenaChance < 0.45 ) {
                    return this.getPath( '30736-08i.html' )
                }

                if ( takenAdenaChance < 0.50 ) {
                    return this.getPath( '30736-08j.html' )
                }

                if ( takenAdenaChance < 0.55 ) {
                    return this.getPath( '30736-08k.html' )
                }

                if ( takenAdenaChance < 0.60 ) {
                    return this.getPath( '30736-08l.html' )
                }

                if ( takenAdenaChance < 0.65 ) {
                    return this.getPath( '30736-08m.html' )
                }

                if ( takenAdenaChance < 0.70 ) {
                    return this.getPath( '30736-08n.html' )
                }

                if ( takenAdenaChance < 0.75 ) {
                    return this.getPath( '30736-08o.html' )
                }

                if ( takenAdenaChance < 0.80 ) {
                    return this.getPath( '30736-08p.html' )
                }

                if ( takenAdenaChance < 0.85 ) {
                    return this.getPath( '30736-08q.html' )
                }

                if ( takenAdenaChance < 0.90 ) {
                    return this.getPath( '30736-08r.html' )
                }

                if ( takenAdenaChance < 0.95 ) {
                    return this.getPath( '30736-08s.html' )
                }

                return this.getPath( '30736-08t.html' )

            case '30737-06.html':
                if ( !QuestHelper.hasAtLeastOneQuestItem( player, CARGO_BOX_1ST, CARGO_BOX_2ND, CARGO_BOX_3RD, CARGO_BOX_4TH ) ) {
                    break
                }

                await QuestHelper.takeMultipleItems( player, 1, CARGO_BOX_1ST, CARGO_BOX_2ND, CARGO_BOX_3RD, CARGO_BOX_4TH )

                if ( QuestHelper.getQuestItemsCount( player, GUILD_COIN ) < 80 ) {
                    await QuestHelper.giveSingleItem( player, GUILD_COIN, 1 )
                } else {
                    await QuestHelper.takeSingleItem( player, GUILD_COIN, 80 )
                }

                let guildCoinAmount = QuestHelper.getQuestItemsCount( player, GUILD_COIN )
                if ( guildCoinAmount < 40 ) {
                    await QuestHelper.giveAdena( player, 100, true )
                    return this.getPath( '30737-03.html' )
                }

                if ( guildCoinAmount < 80 ) {
                    await QuestHelper.giveAdena( player, 200, true )
                    return this.getPath( '30737-04.html' )
                }

                await QuestHelper.giveAdena( player, 300, true )
                return this.getPath( '30737-05.html' )

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== MERCENARY_CAPTAIN_SOPHYA ) {
                    break
                }

                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '30735-01.htm' )
                }

                if ( !QuestHelper.hasQuestItem( player, BLACK_LION_MARK ) ) {
                    return this.getPath( '30735-02.htm' )
                }

                return this.getPath( '30735-03.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case MERCENARY_CAPTAIN_SOPHYA:
                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, SOPHYAS_1ST_ORDER, SOPHYAS_2ND_ORDER, SOPHYAS_3RD_ORDER, SOPHYAS_4TH_ORDER ) ) {
                            return this.getPath( '30735-14.html' )
                        }

                        let hasAnyCargoBoxes: boolean = QuestHelper.hasAtLeastOneQuestItem( player, CARGO_BOX_1ST, CARGO_BOX_2ND, CARGO_BOX_3RD, CARGO_BOX_4TH )
                        let hasAnyMonsterItems: boolean = QuestHelper.hasAtLeastOneQuestItem( player, UNDEAD_ASH, BLOODY_AXE_INSIGNIA, DELU_LIZARDMAN_FANG, STAKATO_TALON )
                        if ( !hasAnyMonsterItems ) {
                            if ( !hasAnyCargoBoxes ) {
                                return this.getPath( '30735-15.html' )
                            }

                            return this.getPath( '30735-15a.html' )
                        }

                        const ash = QuestHelper.getQuestItemsCount( player, UNDEAD_ASH )
                        const insignia = QuestHelper.getQuestItemsCount( player, BLOODY_AXE_INSIGNIA )
                        const fang = QuestHelper.getQuestItemsCount( player, DELU_LIZARDMAN_FANG )
                        const talon = QuestHelper.getQuestItemsCount( player, STAKATO_TALON )
                        const totalAmount = ash + insignia + fang + talon

                        if ( totalAmount >= 20 ) {
                            let itemAmount = 0

                            if ( totalAmount < 50 ) {
                                itemAmount = 1
                            } else if ( totalAmount < 100 ) {
                                itemAmount = 2
                            } else {
                                itemAmount = 3
                            }

                            await QuestHelper.rewardSingleItem( player, LIONS_CLAW, itemAmount )
                        }


                        let adenaAmount = ( ash * 35 ) + ( insignia * 35 ) + ( fang + 35 ) + ( talon * 35 )
                        await QuestHelper.giveAdena( player, adenaAmount, true )
                        await QuestHelper.takeMultipleItems( player, -1, UNDEAD_ASH, BLOODY_AXE_INSIGNIA, DELU_LIZARDMAN_FANG, STAKATO_TALON )

                        state.setMemoState( 0 )

                        return this.getPath( hasAnyCargoBoxes ? '30735-23.html' : '30735-22.html' )

                    case ABYSSAL_CELEBRANT_UNDRIAS:
                        if ( !QuestHelper.hasQuestItem( player, COMPLETE_STATUE_OF_SHILEN ) ) {
                            if ( QuestHelper.hasAtLeastOneQuestItem( player, STATUE_OF_SHILEN_HEAD, STATUE_OF_SHILEN_TORSO, STATUE_OF_SHILEN_ARM, STATUE_OF_SHILEN_LEG ) ) {
                                return this.getPath( '30130-02.html' )
                            }

                            return this.getPath( '30130-01.html' )
                        }

                        return this.getPath( '30130-03.html' )

                    case BLACKSMITH_RUPIO:
                        if ( QuestHelper.hasAtLeastOneQuestItem( player, STATUE_OF_SHILEN_HEAD, STATUE_OF_SHILEN_TORSO, STATUE_OF_SHILEN_ARM, STATUE_OF_SHILEN_LEG )
                                || QuestHelper.hasAtLeastOneQuestItem( player, FRAGMENT_OF_ANCIENT_TABLET_1ST_PIECE, FRAGMENT_OF_ANCIENT_TABLET_2ND_PIECE, FRAGMENT_OF_ANCIENT_TABLET_3RD_PIECE, FRAGMENT_OF_ANCIENT_TABLET_4TH_PIECE ) ) {
                            return this.getPath( '30471-02.html' )
                        }

                        return this.getPath( '30471-01.html' )

                    case IRON_GATES_LOCKIRIN:
                        if ( !QuestHelper.hasQuestItem( player, COMPLETE_ANCIENT_TABLET ) ) {
                            if ( QuestHelper.hasAtLeastOneQuestItem( player, FRAGMENT_OF_ANCIENT_TABLET_1ST_PIECE, FRAGMENT_OF_ANCIENT_TABLET_2ND_PIECE, FRAGMENT_OF_ANCIENT_TABLET_3RD_PIECE, FRAGMENT_OF_ANCIENT_TABLET_4TH_PIECE ) ) {
                                return this.getPath( '30531-02.html' )
                            }

                            return this.getPath( '30531-01.html' )
                        }

                        return this.getPath( '30531-03.html' )

                    case MERCENARY_REEDFOOT:
                        if ( QuestHelper.hasAtLeastOneQuestItem( player, CARGO_BOX_1ST, CARGO_BOX_2ND, CARGO_BOX_3RD, CARGO_BOX_4TH ) ) {
                            return this.getPath( '30736-02.html' )
                        }

                        return this.getPath( '30736-01.html' )

                    case GUILDSMAN_MORGON:
                        if ( QuestHelper.hasAtLeastOneQuestItem( player, CARGO_BOX_1ST, CARGO_BOX_2ND, CARGO_BOX_3RD, CARGO_BOX_4TH ) ) {
                            return this.getPath( '30737-02.html' )
                        }

                        return this.getPath( '30737-01.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async rewardItems( player: L2PcInstance, healingAmount: number, ssMultiplier: number, scrollAmount: number, alacrityAmount: number ): Promise<void> {
        let chance = Math.random()
        if ( chance < 0.25 ) {
            await QuestHelper.rewardSingleItem( player, HEALING_POTION, healingAmount )
            return
        }

        if ( chance < 0.50 ) {
            if ( player.isInCategory( CategoryType.FIGHTER_GROUP ) ) {
                await QuestHelper.rewardSingleItem( player, SOULSHOT_D_GRADE, 100 * ssMultiplier )
                return
            }

            if ( player.isInCategory( CategoryType.MAGE_GROUP ) ) {
                await QuestHelper.rewardSingleItem( player, SPIRITSHOT_D_GRADE, 50 * ssMultiplier )
                return
            }

            return
        }

        if ( chance < 0.75 ) {
            await QuestHelper.rewardSingleItem( player, SCROLL_OF_ESCAPE, scrollAmount )
            return
        }

        await QuestHelper.rewardSingleItem( player, ALACRITY_POTION, alacrityAmount )
    }
}