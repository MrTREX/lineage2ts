import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const COLEMAN = 30873
const minimumLevel = 52
const RECIPE_OF_SUPPLY = 5870
const SUPPLY_ITEMS = 5872
const SUSPICIOUS_DOCUMENT_PIECE = 5871

const monsterRewardChances = {
    20666: 0.5, // Taik Orc Seeker
    20669: 0.75, // Taik Orc Supply Leader
}

export class PlunderTheirSupplies extends ListenerLogic {
    constructor() {
        super( 'Q00360_PlunderTheirSupplies', 'listeners/tracked-300/PlunderTheirSupplies.ts' )
        this.questId = 360
        this.questItemIds = [
            SUPPLY_ITEMS,
            SUSPICIOUS_DOCUMENT_PIECE,
            RECIPE_OF_SUPPLY,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00360_PlunderTheirSupplies'
    }

    getQuestStartIds(): Array<number> {
        return [ COLEMAN ]
    }

    getTalkIds(): Array<number> {
        return [ COLEMAN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        if ( Math.random() < QuestHelper.getAdjustedChance( SUPPLY_ITEMS, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            await QuestHelper.rewardSingleQuestItem( player, SUPPLY_ITEMS, 1, data.isChampion )
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        }

        if ( Math.random() < QuestHelper.getAdjustedChance( SUSPICIOUS_DOCUMENT_PIECE, 0.1, data.isChampion ) ) {
            if ( QuestHelper.getQuestItemsCount( player, SUSPICIOUS_DOCUMENT_PIECE ) < 4 ) {
                await QuestHelper.rewardSingleQuestItem( player, SUSPICIOUS_DOCUMENT_PIECE, 1, data.isChampion )
            } else {
                await QuestHelper.takeSingleItem( player, SUSPICIOUS_DOCUMENT_PIECE, -1 )
                await QuestHelper.rewardSingleQuestItem( player, RECIPE_OF_SUPPLY, 1, data.isChampion )
            }

            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30873-03.htm':
            case '30873-09.html':
                break

            case '30873-04.htm':
                state.startQuest()
                break

            case '30873-10.html':
                await state.exitQuest( false, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30873-02.htm' : '30873-01.html' )

            case QuestStateValues.STARTED:
                let supplyAmount = QuestHelper.getQuestItemsCount( player, SUPPLY_ITEMS )
                let recipeAmount = QuestHelper.getQuestItemsCount( player, RECIPE_OF_SUPPLY )

                if ( ( supplyAmount + recipeAmount ) > 0 ) {
                    let adena = supplyAmount * 100 + 6000 + recipeAmount * 6000
                    await QuestHelper.giveAdena( player, adena, true )
                    await QuestHelper.takeMultipleItems( player, -1, SUPPLY_ITEMS, RECIPE_OF_SUPPLY )

                    return this.getPath( '30873-07.html' )
                }

                if ( recipeAmount > 0 ) {
                    await QuestHelper.giveAdena( player, recipeAmount * 6000, true )
                    await QuestHelper.takeSingleItem( player, RECIPE_OF_SUPPLY, -1 )

                    return this.getPath( '30873-08.html' )
                }

                if ( supplyAmount > 0 ) {
                    let adena = ( supplyAmount * 100 ) + 6000
                    await QuestHelper.giveAdena( player, adena, true )
                    await QuestHelper.takeSingleItem( player, SUPPLY_ITEMS, -1 )

                    return this.getPath( '30873-06.html' )
                }

                return this.getPath( '30873-05.html' )

        }

        return QuestHelper.getNoQuestMessagePath()
    }
}