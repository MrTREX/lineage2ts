import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const RESTINA = 30926
const BLADE_STAKATO_FANG = 5881
const minimumLevel = 36

const monsterRewardChances = {
    20794: 0.60, // blade stakato
    20795: 0.57, // blade stakato worker
    20796: 0.61, // blade stakato soldier
    20797: 0.93, // blade stakato drone
}

export class TrespassingIntoTheHolyGround extends ListenerLogic {
    constructor() {
        super( 'Q00368_TrespassingIntoTheHolyGround', 'listeners/tracked-300/TrespassingIntoTheHolyGround.ts' )
        this.questId = 368
        this.questItemIds = [
            BLADE_STAKATO_FANG,
        ]
    }

    getAttackableAttackIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00368_TrespassingIntoTheHolyGround'
    }

    getQuestStartIds(): Array<number> {
        return [ RESTINA ]
    }

    getTalkIds(): Array<number> {
        return [ RESTINA ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30926-02.htm':
                state.startQuest()
                break

            case '30926-05.html':
                await state.exitQuest( true, true )
                break

            case '30926-06.html':
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( Math.random() > QuestHelper.getAdjustedChance( BLADE_STAKATO_FANG, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        let isWorker = [ 20795, 20797 ].includes( data.npcId )
        let player : L2PcInstance = this.getRandomPartyMemberPerCondition( L2World.getPlayer( data.playerId ), -1, isWorker ? 1 : 3, L2World.getObjectById( data.targetId ) as L2Npc )
        if ( !player ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, BLADE_STAKATO_FANG, 1, data.isChampion )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30926-01.htm' : '30926-03.html' )

            case QuestStateValues.STARTED:
                let count = QuestHelper.getQuestItemsCount( player, BLADE_STAKATO_FANG )
                if ( count > 0 ) {
                    let adena = count * 250 + ( count >= 10 ? 9450 : 2000 )
                    await QuestHelper.giveAdena( player, adena, true )
                    await QuestHelper.takeSingleItem( player, BLADE_STAKATO_FANG, -1 )

                    return this.getPath( '30926-04.html' )
                }

                return this.getPath( '30926-07.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}