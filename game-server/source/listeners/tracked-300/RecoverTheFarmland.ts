import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import _ from 'lodash'

const IRIS = 30034
const ASHA = 30313
const NESTLE = 30314
const LEIKAN = 30382
const PIOTUR = 30597
const TUREK_ORK_WARLORD = 20495
const TUREK_ORK_ARCHER = 20496
const TUREK_ORK_SKIRMISHER = 20497
const TUREK_ORK_SUPPLIER = 20498
const TUREK_ORK_FOOTMAN = 20499
const TUREK_ORK_SENTINEL = 20500
const TUREK_ORK_SHAMAN = 20501

const TUREK_DOG_TAG = 1846
const TUREK_MEDALLION = 1847
const LEIKANS_LETTER = 5012
const CLAY_URN_FRAGMENT = 1848
const BRASS_TRINKET_PIECE = 1849
const BRONZE_MIRROR_PIECE = 1850
const JADE_NECKLACE_BEAD = 1851
const ANCIENT_CLAY_URN = 1852
const ANCIENT_BRASS_TIARA = 1853
const ANCIENT_BRONZE_MIRROR = 1854
const ANCIENT_JADE_NECKLACE = 1855
const QUICK_STEP_POTION = 734
const SWIFT_ATTACK_POTION = 735
const SCROLL_OF_ESCAPE = 736
const SCROLL_OF_RESURRECTION = 737
const HEALING_POTION = 1061
const SOULSHOT_D = 1463
const SPIRITSHOT_D = 2510

const minimumLevel = 25

const monsterRewardChances = {
    [ TUREK_ORK_ARCHER ]: 21,
    [ TUREK_ORK_FOOTMAN ]: 19,
    [ TUREK_ORK_SENTINEL ]: 18,
    [ TUREK_ORK_SHAMAN ]: 22,
    [ TUREK_ORK_SKIRMISHER ]: 21,
    [ TUREK_ORK_SUPPLIER ]: 20,
    [ TUREK_ORK_WARLORD ]: 26,
}

type EventMultiplier = [ number, number ] // itemId, multiplier value
const expEventMultipliers: { [ event: string ]: EventMultiplier } = {
    '30034-03.html': [ CLAY_URN_FRAGMENT, 307 ],
    '30034-04.html': [ BRASS_TRINKET_PIECE, 368 ],
    '30034-05.html': [ BRONZE_MIRROR_PIECE, 368 ],
    '30034-06.html': [ JADE_NECKLACE_BEAD, 430 ],
}

type QuestReward = [ number, number ]
const questRewards : Array<QuestReward> = [
    [ ANCIENT_CLAY_URN , 2766 ],
    [ ANCIENT_BRASS_TIARA , 3227 ],
    [ ANCIENT_BRONZE_MIRROR , 3227 ],
    [ ANCIENT_JADE_NECKLACE , 3919 ]
]

export class RecoverTheFarmland extends ListenerLogic {
    constructor() {
        super( 'Q00327_RecoverTheFarmland', 'listeners/tracked-300/RecoverTheFarmland.ts' )
        this.questId = 327
        this.questItemIds = [
            TUREK_DOG_TAG,
            TUREK_MEDALLION,
            LEIKANS_LETTER,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00327_RecoverTheFarmland'
    }

    getQuestStartIds(): Array<number> {
        return [ LEIKAN, PIOTUR ]
    }

    getTalkIds(): Array<number> {
        return [ LEIKAN, PIOTUR, IRIS, ASHA, NESTLE ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.npcId ) {
            case TUREK_ORK_SHAMAN:
            case TUREK_ORK_WARLORD:
                await QuestHelper.rewardSingleQuestItem( player, TUREK_MEDALLION, 1, data.isChampion )
                break

            default:
                await QuestHelper.rewardSingleQuestItem( player, TUREK_DOG_TAG, 1, data.isChampion )
        }

        let itemId = _.random( CLAY_URN_FRAGMENT, JADE_NECKLACE_BEAD )
        if ( _.random( 100 ) >= QuestHelper.getAdjustedChance( itemId, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, itemId, 1, data.isChampion )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30034-01.html':
            case '30313-01.html':
            case '30314-02.html':
            case '30314-08.html':
            case '30314-09.html':
            case '30382-05a.html':
            case '30382-05b.html':
            case '30597-03.html':
            case '30597-07.html':
                break

            case '30382-03.htm':
                state.startQuest()
                await QuestHelper.giveSingleItem( player, LEIKANS_LETTER, 1 )
                state.setConditionWithSound( 2 )
                break

            case '30597-03.htm':
                state.startQuest()
                break

            case '30597-06.html':
                await state.exitQuest( true, true )
                break

            case '30034-03.html':
            case '30034-04.html':
            case '30034-05.html':
            case '30034-06.html':
                let [ itemId, multiplier ] = expEventMultipliers[ data.eventName ]
                let eventItemCount = QuestHelper.getQuestItemsCount( player, itemId )
                if ( eventItemCount === 0 ) {
                    return this.getPath( '30034-02.html' )
                }

                await QuestHelper.addExpAndSp( player, eventItemCount * multiplier, 0 )
                await QuestHelper.takeSingleItem( player, itemId, -1 )

                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                break

            case '30034-07.html':
                let itemIds = []
                let totalExp = 0

                questRewards.forEach( ( reward: QuestReward ) => {
                    let [ itemId, multiplier ] = reward
                    let amount = QuestHelper.getQuestItemsCount( player, itemId )

                    if ( amount > 0 ) {
                        itemIds.push( itemId )
                        totalExp += amount * multiplier
                    }
                } )

                await QuestHelper.addExpAndSp( player, totalExp, 0 )
                await QuestHelper.takeMultipleItems( player, -1, ...itemIds )

                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                return this.getPath( totalExp > 0 ? data.eventName : '30034-02.html' )

            case '30313-03.html':
                return _.defaultTo( this.processNeededItem(
                        player,
                        CLAY_URN_FRAGMENT,
                        5,
                        ANCIENT_CLAY_URN,
                        5,
                        '30313-02.html',
                        '30313-10.html',
                ), data.eventName )

            case '30313-05.html':
                return _.defaultTo( this.processNeededItem(
                        player,
                        BRASS_TRINKET_PIECE,
                        5,
                        ANCIENT_BRASS_TIARA,
                        6,
                        '30313-04.html',
                        '30313-10.html',
                ), data.eventName )

            case '30313-07.html':
                return _.defaultTo( this.processNeededItem(
                        player,
                        BRONZE_MIRROR_PIECE,
                        5,
                        ANCIENT_BRONZE_MIRROR,
                        6,
                        '30313-06.html',
                        '30313-10.html',
                ), data.eventName )

            case '30313-09.html':
                return _.defaultTo( this.processNeededItem(
                        player,
                        JADE_NECKLACE_BEAD,
                        5,
                        ANCIENT_JADE_NECKLACE,
                        7,
                        '30313-08.html',
                        '30313-10.html',
                ), data.eventName )

            case '30314-03.html':
                if ( !QuestHelper.hasQuestItem( player, ANCIENT_CLAY_URN ) ) {
                    return this.getPath( '30314-07.html' )
                }

                await QuestHelper.rewardSingleItem( player, SOULSHOT_D, _.random( 70, 110 ) )
                await QuestHelper.takeSingleItem( player, ANCIENT_CLAY_URN, 1 )
                break

            case '30314-04.html':
                if ( !QuestHelper.hasQuestItem( player, ANCIENT_BRASS_TIARA ) ) {
                    return this.getPath( '30314-07.html' )
                }

                await QuestHelper.rewardSingleItem( player, this.getTiaraReward(), 1 )
                await QuestHelper.takeSingleItem( player, ANCIENT_BRASS_TIARA, 1 )
                break

            case '30314-05.html':
                if ( !QuestHelper.hasQuestItem( player, ANCIENT_BRONZE_MIRROR ) ) {
                    return this.getPath( '30314-07.html' )
                }

                await QuestHelper.rewardSingleItem( player, _.random( 100 ) < 59 ? SCROLL_OF_ESCAPE : SCROLL_OF_RESURRECTION, 1 )
                await QuestHelper.takeSingleItem( player, ANCIENT_BRONZE_MIRROR, 1 )
                break

            case '30314-06.html':
                if ( !QuestHelper.hasQuestItem( player, ANCIENT_JADE_NECKLACE ) ) {
                    return this.getPath( '30314-07.html' )
                }

                await QuestHelper.rewardSingleItem( player, SPIRITSHOT_D, _.random( 50, 90 ) )
                await QuestHelper.takeSingleItem( player, ANCIENT_JADE_NECKLACE, 1 )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                switch ( data.characterNpcId ) {
                    case LEIKAN:
                        return this.getPath( player.getLevel() >= minimumLevel ? '30382-02.htm' : '30382-01.htm' )

                    case PIOTUR:
                        return this.getPath( player.getLevel() >= minimumLevel ? '30597-02.htm' : '30597-01.htm' )

                    case IRIS:
                        return this.getPath( '30034-01.html' )

                    case ASHA:
                        return this.getPath( '30313-01.html' )

                    case NESTLE:
                        return this.getPath( '30314-01.html' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case LEIKAN:
                        if ( QuestHelper.hasQuestItem( player, LEIKANS_LETTER ) ) {
                            return this.getPath( '30382-04.html' )
                        }

                        state.setConditionWithSound( 5, true )
                        return this.getPath( '30382-05.html' )

                    case PIOTUR:
                        if ( QuestHelper.hasQuestItem( player, LEIKANS_LETTER ) ) {
                            await QuestHelper.takeSingleItem( player, LEIKANS_LETTER, -1 )

                            state.setConditionWithSound( 3, true )
                            return this.getPath( '30597-03a.htm' )
                        }

                        const dogTags = QuestHelper.getQuestItemsCount( player, TUREK_DOG_TAG )
                        const medallions = QuestHelper.getQuestItemsCount( player, TUREK_MEDALLION )
                        const totalAmount = dogTags + medallions

                        if ( totalAmount === 0 ) {
                            return this.getPath( '30597-04.html' )
                        }

                        const adena = ( dogTags * 40 ) + ( medallions * 50 ) + ( ( totalAmount >= 10 ) ? 619 : 0 )

                        await QuestHelper.giveAdena( player, adena, true )
                        await QuestHelper.takeMultipleItems( player, -1, TUREK_DOG_TAG, TUREK_MEDALLION )

                        state.setConditionWithSound( 4, true )
                        return this.getPath( '30597-05.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getTiaraReward(): number {
        let tiaraChance = _.random( 100 )
        if ( tiaraChance < 40 ) {
            return HEALING_POTION
        }

        if ( tiaraChance < 84 ) {
            return QUICK_STEP_POTION
        }

        return SWIFT_ATTACK_POTION
    }

    async processNeededItem( player: L2PcInstance, itemId: number, limit: number, rewardId: number, chance: number, failResponse: string, defaultResponse: string ): Promise<string> {
        if ( QuestHelper.getQuestItemsCount( player, itemId ) < limit ) {
            return this.getPath( failResponse )
        }

        await QuestHelper.takeSingleItem( player, itemId, limit )
        if ( _.random( chance ) < chance ) {
            await QuestHelper.giveSingleItem( player, rewardId, 1 )
            return null
        }

        return this.getPath( defaultResponse )
    }
}