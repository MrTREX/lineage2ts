import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const KINTAIJIN = 32640
const GROW_ACCELERATOR = 14832
const MULTI_COLORED_JEWEL = 14835
const DIRTY_BEAD = 14880
const minimumLevel = 81

const monsterRewardChances = {
    22617: 646,
    22618: 646,
    22619: 646,
    22620: 666,
    22621: 630,
    22622: 940,
    22623: 622,
    22624: 630,
    22625: 678,
    22626: 940,
    22627: 646,
    22628: 646,
    22629: 646,
    22630: 638,
    22631: 880,
    22632: 722,
    22633: 638,
}

export class OnlyWhatRemains extends ListenerLogic {
    constructor() {
        super( 'Q00310_OnlyWhatRemains', 'listeners/tracked-300/OnlyWhatRemains.ts' )
        this.questId = 310
        this.questItemIds = [
            DIRTY_BEAD,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00310_OnlyWhatRemains'
    }

    getQuestStartIds(): Array<number> {
        return [ KINTAIJIN ]
    }

    getTalkIds(): Array<number> {
        return [ KINTAIJIN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 1 )
        if ( !player ) {
            return
        }

        if ( _.random( 1000 ) >= QuestHelper.getAdjustedChance( DIRTY_BEAD, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, DIRTY_BEAD, 1, data.isChampion )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '32640-04.htm':
                state.startQuest()
                break

            case '32640-quit.html':
                await state.exitQuest( true, true )
                break

            case '32640-02.htm':
            case '32640-03.htm':
            case '32640-05.html':
            case '32640-06.html':
            case '32640-07.html':
                break

            default:
                break
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                let shouldProceed = player.getLevel() >= minimumLevel && player.hasQuestCompleted( 'Q00240_ImTheOnlyOneYouCanTrust' )
                return this.getPath( shouldProceed ? '32640-01.htm' : '32640-00.htm' )

            case QuestStateValues.STARTED:
                let existingAmount = QuestHelper.getQuestItemsCount( player, DIRTY_BEAD )
                if ( existingAmount === 0 ) {
                    return this.getPath( '32640-08.html' )
                }

                if ( existingAmount < 500 ) {
                    return this.getPath( '32640-09.html' )
                }

                await QuestHelper.takeSingleItem( player, DIRTY_BEAD, 500 )
                await QuestHelper.rewardSingleItem( player, GROW_ACCELERATOR, 1 )
                await QuestHelper.rewardSingleItem( player, MULTI_COLORED_JEWEL, 1 )

                return this.getPath( '32640-10.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}