import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { DataManager } from '../../data/manager'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { ConfigManager } from '../../config/ConfigManager'

import _ from 'lodash'

const WAREHOUSE_KEEPER_ROMP = 30843
const CRIMSON_DRAKE = 20670
const KADIOS = 20671
const HUNGRY_CORPSE = 20954
const PAST_KNIGHT = 20956
const BLADE_DEATH = 20958
const DARK_GUARD = 20959
const BLOODY_GHOST = 20960
const BLOODY_LORD = 20963
const PAST_CREATURE = 20967
const GIANT_SHADOW = 20969
const ANCIENTS_SOLDIER = 20970
const ANCIENTS_WARRIOR = 20971
const SPITE_SOUL_LEADER = 20974
const SPITE_SOUL_WIZARD = 20975
const WRECKED_ARCHER = 21001
const FLOAT_OF_GRAVE = 21003
const GRAVE_PREDATOR = 21005
const FALLEN_ORC_SHAMAN = 21020
const SHARP_TALON_TIGER = 21021
const GLOW_WISP = 21108
const MARSH_PREDATOR = 21110
const HAMES_ORC_SNIPER = 21113
const CURSED_GUARDIAN = 21114
const HAMES_ORC_CHIEFTAIN = 21116
const FALLEN_ORC_SHAMAN_TRANS = 21258
const SHARP_TALON_TIGER_TRANS = 21259
const Q_STOLEN_INF_ORE = 6363
const DRAGON_SLAYER_EDGE = 5529
const METEOR_SHOWER_HEAD = 5532
const ELYSIAN_HEAD = 5533
const SOUL_BOW_SHAFT = 5534
const CARNIUM_BOW_SHAFT = 5535
const BLOODY_ORCHID_HEAD = 5536
const SOUL_SEPARATOR_HEAD = 5537
const DRAGON_GRINDER_EDGE = 5538
const BLOOD_TORNADO_EDGE = 5539
const TALLUM_GLAIVE_EDGE = 5541
const HALBARD_EDGE = 5542
const DASPARIONS_STAFF_HEAD = 5543
const WORLDTREES_BRANCH_HEAD = 5544
const DARK_LEGIONS_EDGE_EDGE = 5545
const SWORD_OF_MIRACLE_EDGE = 5546
const ELEMENTAL_SWORD_EDGE = 5547
const TALLUM_BLADE_EDGE = 5548
const INFERNO_MASTER_BLADE = 8331
const EYE_OF_SOUL_PIECE = 8341
const DRAGON_FLAME_HEAD_PIECE = 8342
const DOOM_CRUSHER_HEAD = 8349
const HAMMER_OF_DESTROYER_PIECE = 8346
const SIRR_BLADE_BLADE = 8712
const SWORD_OF_IPOS_BLADE = 8713
const BARAKIEL_AXE_PIECE = 8714
const TUNING_FORK_OF_BEHEMOTH_PIECE = 8715
const NAGA_STORM_PIECE = 8716
const TIPHON_SPEAR_EDGE = 8717
const SHYID_BOW_SHAFT = 8718
const SOBEKK_HURRICANE_EDGE = 8719
const TONGUE_OF_THEMIS_PIECE = 8720
const HAND_OF_CABRIO_HEAD = 8721
const CRYSTAL_OF_DEAMON_PIECE = 8722

const minimumLevel = 58
const variableNames = {
    selectedNumbers: 'sn',
    generatedNumbers: 'gn',
}

const playerChanceReward: Array<number> = [
    DRAGON_SLAYER_EDGE,
    METEOR_SHOWER_HEAD,
    ELYSIAN_HEAD,
    SOUL_BOW_SHAFT,
    CARNIUM_BOW_SHAFT,
    BLOODY_ORCHID_HEAD,
    SOUL_SEPARATOR_HEAD,
    DRAGON_GRINDER_EDGE,
    BLOOD_TORNADO_EDGE,
    TALLUM_GLAIVE_EDGE,
    HALBARD_EDGE,
    HALBARD_EDGE,
    DASPARIONS_STAFF_HEAD,
    WORLDTREES_BRANCH_HEAD,
    DARK_LEGIONS_EDGE_EDGE,
    SWORD_OF_MIRACLE_EDGE,
    ELEMENTAL_SWORD_EDGE,
    TALLUM_BLADE_EDGE,
    INFERNO_MASTER_BLADE,
    EYE_OF_SOUL_PIECE,
    DRAGON_FLAME_HEAD_PIECE,
    DOOM_CRUSHER_HEAD,
    HAMMER_OF_DESTROYER_PIECE,
    SIRR_BLADE_BLADE,
    SWORD_OF_IPOS_BLADE,
    BARAKIEL_AXE_PIECE,
    TUNING_FORK_OF_BEHEMOTH_PIECE,
    NAGA_STORM_PIECE,
    TIPHON_SPEAR_EDGE,
    SHYID_BOW_SHAFT,
    SOBEKK_HURRICANE_EDGE,
    TONGUE_OF_THEMIS_PIECE,
    HAND_OF_CABRIO_HEAD,
    CRYSTAL_OF_DEAMON_PIECE,
]

const monsterRewardChances: { [ npcId: number ]: number } = {
    [ CRIMSON_DRAKE ]: 0.0202,
    [ KADIOS ]: 0.211,
    [ HUNGRY_CORPSE ]: 0.184,
    [ PAST_KNIGHT ]: 0.216,
    [ BLADE_DEATH ]: 0.17,
    [ DARK_GUARD ]: 0.273,
    [ BLOODY_GHOST ]: 0.149,
    [ BLOODY_LORD ]: 0.199,
    [ PAST_CREATURE ]: 0.257,
    [ GIANT_SHADOW ]: 0.205,
    [ ANCIENTS_SOLDIER ]: 0.208,
    [ ANCIENTS_WARRIOR ]: 0.299,
    [ SPITE_SOUL_LEADER ]: 0.44,
    [ SPITE_SOUL_WIZARD ]: 0.39,
    [ WRECKED_ARCHER ]: 0.214,
    [ FLOAT_OF_GRAVE ]: 0.173,
    [ GRAVE_PREDATOR ]: 0.211,
    [ FALLEN_ORC_SHAMAN ]: 0.478,
    [ SHARP_TALON_TIGER ]: 0.234,
    [ GLOW_WISP ]: 0.245,
    [ MARSH_PREDATOR ]: 0.26,
    [ HAMES_ORC_SNIPER ]: 0.37,
    [ CURSED_GUARDIAN ]: 0.352,
    [ HAMES_ORC_CHIEFTAIN ]: 0.487,
    [ FALLEN_ORC_SHAMAN_TRANS ]: 0.487,
    [ SHARP_TALON_TIGER_TRANS ]: 0.487,
}

export class StolenDignity extends ListenerLogic {
    constructor() {
        super( 'Q00386_StolenDignity', 'listeners/tracked-300/StolenDignity.ts' )
        this.questId = 386
    }

    async beforeReward( player: L2PcInstance, state: QuestState, value: number ): Promise<string> {
        if ( !this.isSelectedBingoNumber( state, value ) ) {
            this.selectBingoNumber( state, value )

            let matchedLines: number = this.getMatchedBingoLineCount( state )
            let isCorrectCount: boolean = this.getBingoSelectCount( state ) === 6

            if ( matchedLines === 3 && isCorrectCount ) {
                await this.rewardPlayer( player, state, matchedLines )
                return this.colorBoard( state, '30843-22.html' )
            }

            if ( matchedLines === 0 && isCorrectCount ) {
                await this.rewardPlayer( player, state, matchedLines )
                return this.colorBoard( state, '30843-24.html' )
            }

            return this.colorBoard( state, '30843-23.html' )
        }

        return this.fillBoard( state, '30843-25.html' )
    }

    colorBoard( state: QuestState, htmlPath: string ): string {
        let html = DataManager.getHtmlData().getItem( this.getPath( htmlPath ) )
        let quest = this

        _.each( state.getVariable( variableNames.generatedNumbers ), ( value: number, index: number ) => {
            let color: string = quest.isSelectedBingoNumber( state, value ) ? 'ff0000' : 'ffffff'
            html = html
                    .replace( `<?FontColor${ index + 1 }?>`, color )
                    .replace( `<?Cell${ index + 1 }?>`, value.toString() )
        } )

        return html
    }

    createBingoBoard( state: QuestState ): void {
        state.setVariable( variableNames.selectedNumbers, _.times( 9, _.constant( 0 ) ) )
        state.setVariable( variableNames.generatedNumbers, _.shuffle( [ 1, 2, 3, 4, 5, 6, 7, 8, 9 ] ) )
    }

    fillBoard( state: QuestState, htmlPath: string ): string {
        let html = DataManager.getHtmlData().getItem( this.getPath( htmlPath ) )
        let quest = this

        _.each( state.getVariable( variableNames.generatedNumbers ), ( value: number, index: number ) => {
            let outcome: string = quest.isSelectedBingoNumber( state, value ) ? value.toString() : '?'
            html = html.replace( `<?Cell${ index + 1 }?>`, outcome )
        } )

        return html
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00386_StolenDignity'
    }

    getQuestStartIds(): Array<number> {
        return [ WAREHOUSE_KEEPER_ROMP ]
    }

    getTalkIds(): Array<number> {
        return [ WAREHOUSE_KEEPER_ROMP ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( Math.random() > QuestHelper.getAdjustedChance( Q_STOLEN_INF_ORE, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        let player: L2PcInstance = this.getRandomPartyMemberPerCondition( L2World.getPlayer( data.playerId ), -1, 2, L2World.getObjectById( data.targetId ) )
        if ( !player ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, Q_STOLEN_INF_ORE, 1, data.isChampion )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        if ( data.eventName === 'QUEST_ACCEPTED' ) {
            state.startQuest()
            state.setMemoState( 386 )
            state.showQuestionMark( 386 )

            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ACCEPT )
            return this.getPath( '30843-05.htm' )
        }

        if ( data.eventName.endsWith( '.htm' ) ) {
            return this.getPath( data.eventName )
        }

        let value: number = _.parseInt( data.eventName )

        switch ( value ) {
            case 3:
                return this.getPath( '30843-09a.html' )

            case 5:
                return this.getPath( '30843-03.html' )

            case 6:
                await state.exitQuest( true )
                return this.getPath( '30843-08.html' )

            case 9:
                return this.getPath( '30843-09.htm' )

            case 8:
                if ( QuestHelper.getQuestItemsCount( player, Q_STOLEN_INF_ORE ) >= 100 ) {
                    await QuestHelper.takeSingleItem( player, Q_STOLEN_INF_ORE, 100 )
                    this.createBingoBoard( state )

                    return this.getPath( '30843-12.html' )
                }

                return this.getPath( '30843-11.html' )

            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
                this.selectBingoNumber( state, ( value - 9 ) )
                return this.fillBoard( state, '30843-13.html' )

            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
                return this.takeHtml( state, ( value - 18 ) )

            case 55:
            case 56:
            case 57:
            case 58:
            case 59:
            case 60:
            case 61:
            case 62:
            case 63:
                return this.beforeReward( player, state, ( value - 54 ) )
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getLevel() >= minimumLevel ) {
                    return this.getPath( '30843-01.htm' )
                }

                return this.getPath( '30843-04.html' )

            case QuestStateValues.STARTED:
                if ( QuestHelper.getQuestItemsCount( player, Q_STOLEN_INF_ORE ) < 100 ) {
                    return this.getPath( '30843-06.html' )
                }

                return this.getPath( '30843-07.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getBingoSelectCount( state: QuestState ): number {
        return _.reduce( state.getVariable( variableNames.selectedNumbers ), ( amount: number, currentNumber: number ): number => {
            if ( currentNumber !== 0 ) {
                amount++
            }

            return amount
        }, 0 )
    }

    getMatchedBingoLineCount( state: QuestState ): number {
        let found: number = 0
        let grid: Array<number> = state.getVariable( variableNames.selectedNumbers )

        /*
            3 x 3 grid
            First match rows to see if all of them contain non-zero values.
            Second match is for columns, again for non-zero values.
            Third match is diagonal...
         */

        if ( !grid.slice( 0, 3 ).includes( 0 ) ) {
            found++
        }

        if ( !grid.slice( 3, 6 ).includes( 0 ) ) {
            found++
        }

        if ( !grid.slice( 6, 9 ).includes( 0 ) ) {
            found++
        }

        if ( ![ grid[ 0 ], grid[ 3 ], grid[ 6 ] ].includes( 0 ) ) {
            found++
        }

        if ( ![ grid[ 1 ], grid[ 4 ], grid[ 7 ] ].includes( 0 ) ) {
            found++
        }

        if ( ![ grid[ 2 ], grid[ 5 ], grid[ 8 ] ].includes( 0 ) ) {
            found++
        }

        if ( ![ grid[ 0 ], grid[ 4 ], grid[ 8 ] ].includes( 0 ) ) {
            found++
        }

        if ( ![ grid[ 2 ], grid[ 4 ], grid[ 6 ] ].includes( 0 ) ) {
            found++
        }

        return found
    }

    isSelectedBingoNumber( state: QuestState, value: number ): boolean {
        return state.getVariable( variableNames.selectedNumbers ).includes( value )
    }

    rewardPlayer( player: L2PcInstance, state: QuestState, amount: number ): Promise<void> {
        let amountOfItems = Math.floor( Math.max( ConfigManager.rates.getRateQuestReward(), 1 ) )
        return QuestHelper.rewardMultipleItems( player, amount, ..._.times( amountOfItems, () => _.sample( playerChanceReward ) ) )
    }

    selectBingoNumber( state: QuestState, value: number ): void {
        let position = _.findIndex( state.getVariable( variableNames.generatedNumbers ), value )

        if ( position < 0 ) {
            position = 0
        }

        state.getVariable( variableNames.selectedNumbers )[ position ] = value
    }

    takeHtml( state: QuestState, value: number ): string {
        if ( !this.isSelectedBingoNumber( state, value ) ) {
            this.selectBingoNumber( state, value )
            let countValue: number = this.getBingoSelectCount( state )

            switch ( countValue ) {
                case 2:
                    return this.fillBoard( state, '30843-14.html' )

                case 3:
                    return this.fillBoard( state, '30843-16.html' )

                case 4:
                    return this.fillBoard( state, '30843-18.html' )

                case 5:
                    return this.fillBoard( state, '30843-20.html' )
            }

            return
        }

        let countValue: number = this.getBingoSelectCount( state )
        switch ( countValue ) {
            case 1:
                return this.fillBoard( state, '30843-15.html' )

            case 2:
                return this.fillBoard( state, '30843-17.html' )

            case 3:
                return this.fillBoard( state, '30843-19.html' )

            case 4:
                return this.fillBoard( state, '30843-21.html' )
        }

        return
    }
}