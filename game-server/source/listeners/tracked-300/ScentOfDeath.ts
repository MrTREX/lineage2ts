import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const MINALESS = 30138
const MARSH_ZOMBIE = 20015
const MARSH_ZOMBIE_LORD = 20020
const ZOMBIES_SKIN = 1045
const LESSER_HEALING_POTION = 1060
const minimumLevel = 11
const REQUIRED_ITEM_COUNT = 5

export class ScentOfDeath extends ListenerLogic {
    constructor() {
        super( 'Q00319_ScentOfDeath', 'listeners/tracked-300/ScentOfDeath.ts' )
        this.questId = 319
        this.questItemIds = [
            ZOMBIES_SKIN,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            MARSH_ZOMBIE,
            MARSH_ZOMBIE_LORD,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00319_ScentOfDeath'
    }

    getQuestStartIds(): Array<number> {
        return [ MINALESS ]
    }

    getTalkIds(): Array<number> {
        return [ MINALESS ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        if ( Math.random() >= QuestHelper.getAdjustedChance( ZOMBIES_SKIN, 0.7, data.isChampion ) ) {
            return
        }

        await QuestHelper.rewardAndProgressState( player, state, ZOMBIES_SKIN, 1, REQUIRED_ITEM_COUNT, data.isChampion, 2 )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || data.eventName !== '30138-04.htm' ) {
            return
        }

        state.startQuest()
        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30138-03.htm' : '30138-02.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '30138-05.html' )

                    case 2:
                        await QuestHelper.giveAdena( player, 3350, false )
                        await QuestHelper.rewardSingleItem( player, LESSER_HEALING_POTION, 1 )
                        await state.exitQuest( true, true )

                        return this.getPath( '30138-06.html' )
                }
                break

        }

        return QuestHelper.getNoQuestMessagePath()
    }
}