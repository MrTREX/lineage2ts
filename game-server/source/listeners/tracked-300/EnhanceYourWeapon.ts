import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import {
    AttackableKillEvent, EventType,
    NpcGeneralEvent,
    NpcSeeSkillEvent,
    NpcTalkEvent,
} from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { AbstractVariablesMap } from '../../gameService/variables/AbstractVariablesManager'
import { DataManager } from '../../data/manager'
import {
    L2SoulCrystalAbsorbType,
    L2SoulCrystalDataItem,
    L2SoulCrystalDataNpc,
} from '../../data/interface/SouldCrystalDataApi'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2ItemInstance } from '../../gameService/models/items/instance/L2ItemInstance'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { L2Party } from '../../gameService/models/L2Party'
import { PlayerGroupCache } from '../../gameService/cache/PlayerGroupCache'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import _ from 'lodash'
import aigle from 'aigle'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'

const startingRedCrystal = 4629
const startingGreenCrystal = 4640
const startingBlueCrystal = 4651

const npcIds: Array<number> = [
    30115,
    30856,
    30194,
]

const requiredItems: Array<number> = _.range( 4629, 4665 )
const requiredSkillId = 2096 // Soul Crystal


interface ValidPlayerData extends AbstractVariablesMap {
    [ playerId: number ]: boolean
}

export class EnhanceYourWeapon extends ListenerLogic {

    npcIdsRequiringSkill: Set<number>
    upgradableItemIds: Set<number>

    constructor() {
        super( 'Q00350_EnhanceYourWeapon', 'listeners/tracked-300/EnhanceYourWeapon.ts' )
        this.questId = 350
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( DataManager.getSoulCrystalData().getNpcs() ).map( value => _.parseInt( value ) )
    }

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.NpcId,
                eventType: EventType.NpcSeeSkill,
                method: this.onNpcSeeSkill.bind( this ),
                ids: Array.from( this.npcIdsRequiringSkill ),
                triggers: {
                    targetIds: new Set( [ requiredSkillId ] )
                }
            }
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00350_EnhanceYourWeapon'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }


    getTalkIds(): Array<number> {
        return npcIds
    }

    async load(): Promise<void> {
        this.npcIdsRequiringSkill = _.reduce( DataManager.getSoulCrystalData().getNpcs(), ( ids: Set<number>, items: Array<L2SoulCrystalDataNpc> ): Set<number> => {
            if ( _.some( items, 'requiresSkill' ) ) {
                ids.add( items[ 0 ].npcId )
            }

            return ids
        }, new Set<number>() )

        let maximumLevel: number = _.max( _.map( DataManager.getSoulCrystalData().getItems(), 'level' ) )
        this.upgradableItemIds = _.reduce( DataManager.getSoulCrystalData().getItems(), ( itemIds: Set<number>, item: L2SoulCrystalDataItem ) => {
            if ( item.level !== maximumLevel ) {
                itemIds.add( item.itemId )
            }

            return itemIds
        }, new Set<number>() )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( this.npcIdsRequiringSkill.has( data.npcId ) ) {
            if ( !_.get( NpcVariablesManager.get( data.targetId, this.getName() ), data.playerId ) ) {
                return
            }

            NpcVariablesManager.remove( data.targetId, this.getName() )
        }


        if ( this.isOnlyPartyTypeCrystal( data.npcId ) ) {
            await this.processPartyCrystal( data )
            return
        }

        let player = L2World.getPlayer( data.playerId )
        if ( !QuestHelper.checkDistanceToTarget( player, L2World.getObjectById( data.targetId ) ) ) {
            return
        }

        await this.processIndividualCrystal( player, data.npcId, data.isChampion )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        if ( data.eventName === 'exit.htm' ) {
            await state.exitQuest( true )
            return this.getPath( data.eventName )
        }

        let [ , suffix ] = data.eventName.split( '-' )
        let player = L2World.getPlayer( data.playerId )

        switch ( suffix ) {
            case '04.htm':
                state.startQuest()
                break

            case '09.htm':
                await QuestHelper.giveSingleItem( player, startingRedCrystal, 1 )
                break

            case '10.htm':
                await QuestHelper.giveSingleItem( player, startingGreenCrystal, 1 )
                break

            case '11.htm':
                await QuestHelper.giveSingleItem( player, startingBlueCrystal, 1 )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onNpcSeeSkill( data: NpcSeeSkillEvent ): Promise<void> {
        if ( data.skillId !== requiredSkillId || data.playerId === 0 ) {
            return
        }

        let npc = L2World.getObjectById( data.receiverId ) as L2Npc
        let player = L2World.getPlayer( data.playerId )

        if ( !npc
                || npc.isDead()
                || !player
                || player.isDead()
                || npc.getCurrentHp() > ( npc.getMaxHp() / 2 ) ) {
            return
        }

        let absorbData: ValidPlayerData = NpcVariablesManager.get( data.receiverId, this.getName() ) as ValidPlayerData

        if ( !absorbData ) {
            absorbData = {}

            NpcVariablesManager.set( data.receiverId, this.getName(), absorbData )
        }

        absorbData[ data.playerId ] = true
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        if ( state.isCondition( 0 ) ) {
            return this.getPath( `${ data.characterNpcId }-01.htm` )
        }

        if ( QuestHelper.hasQuestItems( player, ...requiredItems ) ) {
            return this.getPath( `${ data.characterNpcId }-03.htm` )
        }

        if ( !QuestHelper.hasAtLeastOneQuestItem( player, startingRedCrystal, startingGreenCrystal, startingBlueCrystal ) ) {
            return this.getPath( `${ data.characterNpcId }-21.htm` )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getCurrentSoulCrystal( player: L2PcInstance ): L2SoulCrystalDataItem {
        let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        let quest = this
        let currentCrystal: L2SoulCrystalDataItem

        _.each( player.getInventory().getItems(), ( item: L2ItemInstance ) => {
            if ( !item || !quest.upgradableItemIds.has( item.getId() ) ) {
                return
            }

            /*
                Following implies player has two crystals; player must only have one crystal at a time
             */
            if ( currentCrystal ) {
                currentCrystal = null

                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SOUL_CRYSTAL_ABSORBING_FAILED_RESONATION ) )
                return false
            }

            currentCrystal = DataManager.getSoulCrystalData().getItems()[ item.getId() ]
        } )

        return currentCrystal
    }

    isOnlyPartyTypeCrystal( npcId: number ): boolean {
        return !_.some( DataManager.getSoulCrystalData().getNpcs()[ npcId ], ( data: L2SoulCrystalDataNpc ): boolean => {
            return data.type === L2SoulCrystalAbsorbType.lastHitPlayer
        } )
    }

    async processIndividualCrystal( player: L2PcInstance, npcId: number, isFromChampion: boolean ): Promise<void> {
        if ( !player ) {
            return
        }

        if ( player.getLevel() - DataManager.getNpcData().getTemplate( npcId ).getLevel() > 8 ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SOUL_CRYSTAL_ABSORBING_REFUSED ) )
            return
        }

        let currentCrystalData: L2SoulCrystalDataItem = this.getCurrentSoulCrystal( player )
        if ( !currentCrystalData ) {
            return
        }

        let npcData: L2SoulCrystalDataNpc = _.find( DataManager.getSoulCrystalData().getNpcs()[ npcId ], ( definition: L2SoulCrystalDataNpc ): boolean => {
            return definition.levels.includes( currentCrystalData.level )
        } )

        if ( !npcData ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SOUL_CRYSTAL_ABSORBING_REFUSED ) )
            return
        }

        if ( _.random( 100 ) > QuestHelper.getAdjustedChance( currentCrystalData.nextCrystalItemId, npcData.chance, isFromChampion ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SOUL_CRYSTAL_ABSORBING_FAILED ) )
            return
        }

        await QuestHelper.takeSingleItem( player, currentCrystalData.itemId, 1 )
        await QuestHelper.giveSingleItem( player, currentCrystalData.nextCrystalItemId, 1 )

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async processPartyCrystal( data: AttackableKillEvent ): Promise<void> {
        /*
            Assume absorption type stays same among npc chance definitions.
            Otherwise, we are presented with unsolvable problem if types are different, such as :
            - each player in party vs last hit
            - each player in party vs random player in party

            L2J solves above problem in simple way keying off highest level of crystal in party,
            which only presents problems since it is used to determine if npc even has ability to
            level up such a crystal:
            - one player has highest crystal level 12 which current npc cannot level up, hence must be
            kicked out from party for others to continue leveling their crystals which are 11 level

            Current approach allows each member to level up a crystal of level that is applicable.
         */

        let npcData: L2SoulCrystalDataNpc = _.head( DataManager.getSoulCrystalData().getNpcs()[ data.npcId ] )
        let party: L2Party = PlayerGroupCache.getParty( data.playerId )
        let npc = L2World.getObjectById( data.targetId )

        if ( !party ) {
            if ( npcData.type === L2SoulCrystalAbsorbType.randomPlayerPartyChance
                    && Math.random() >= QuestHelper.getAdjustedChance( 0, 0.33, data.isChampion ) ) {
                return
            }

            let player = L2World.getPlayer( data.playerId )
            if ( !QuestHelper.checkDistanceToTarget( player, npc ) ) {
                return
            }

            return this.processIndividualCrystal( player, data.npcId, data.isChampion )
        }

        switch ( npcData.type ) {
            case L2SoulCrystalAbsorbType.eachPlayerInParty:
                await aigle.resolve( party.getMembers() ).each( ( playerId: number ) => {
                    let player = L2World.getPlayer( playerId )
                    if ( !QuestHelper.checkDistanceToTarget( player, npc ) ) {
                        return
                    }

                    return this.processIndividualCrystal( player, data.npcId, data.isChampion )
                } )

                return

            case L2SoulCrystalAbsorbType.randomPlayerInParty:
                let randomPlayerIds: Array<number> = _.filter( party.getMembers(), ( playerId: number ): boolean => {
                    let state: QuestState = QuestStateCache.getQuestState( playerId, this.getName(), false )
                    if ( !state || !state.isStarted() ) {
                        return
                    }

                    let player = L2World.getPlayer( playerId )
                    return QuestHelper.checkDistanceToTarget( player, npc )
                } )

                return this.processIndividualCrystal( L2World.getPlayer( _.sample( randomPlayerIds ) ), data.npcId, data.isChampion )

            case L2SoulCrystalAbsorbType.randomPlayerPartyChance:
                let randomChanceIds: Array<number> = _.filter( party.getMembers(), ( playerId: number ): boolean => {
                    let state: QuestState = QuestStateCache.getQuestState( playerId, this.getName(), false )

                    if ( !state || !state.isStarted() ) {
                        return false
                    }

                    return Math.random() < QuestHelper.getAdjustedChance( 0, 0.33, data.isChampion )
                } )

                await aigle.resolve( randomChanceIds ).each( ( playerId: number ) => {
                    let player = L2World.getPlayer( playerId )
                    if ( !QuestHelper.checkDistanceToTarget( player, npc ) ) {
                        return
                    }

                    return this.processIndividualCrystal( player, data.npcId, data.isChampion )
                } )

                return
        }
    }
}