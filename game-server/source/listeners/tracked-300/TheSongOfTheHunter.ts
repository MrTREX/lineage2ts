import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { DataManager } from '../../data/manager'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { CreatureSay } from '../../gameService/packets/send/builder/CreatureSay'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { QuestStateValues } from '../../gameService/models/quest/State'
import _ from 'lodash'
import aigle from 'aigle'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const GREY = 30744
const TOR = 30745
const CYBELLIN = 30746

const BREKA_ORC_SHAMAN = 20269
const BREKA_ORC_WARRIOR = 20271
const GUARDIAN_BASILISK = 20550
const FETTERED_SOUL = 20552
const WINDSUS = 20553
const GRANDIS = 20554
const GIANT_FUNGUS = 20555
const GIANT_MONSTEREYE = 20556
const DIRE_WYRM = 20557
const ROTTING_TREE = 20558
const TRISALIM_SPIDER = 20560
const TRISALIM_TARANTULA = 20561
const SPORE_ZOMBIE = 20562
const MANASHEN_GARGOYLE = 20563
const ENCHANTED_STONE_GOLEM = 20565
const ENCHANTED_GARGOYLE = 20567
const TARLK_BUGBEAR_WARRIOR = 20571
const LETO_LIZARDMAN_ARCHER = 20578
const LETO_LIZARDMAN_SOLDIER = 20579
const LETO_LIZARDMAN_SHAMAN = 20581
const LETO_LIZARDMAN_OVERLORD = 20582
const TIMAK_ORC_WARRIOR = 20586
const TIMAK_ORC_OVERLORD = 20588
const FLINE = 20589
const LIELE = 20590
const VALLEY_TREANT = 20591
const SATYR = 20592
const UNICORN = 20593
const FOREST_RUNNER = 20594
const VALLEY_TREANT_ELDER = 20597
const SATYR_ELDER = 20598
const UNICORN_ELDER = 20599
const KARUL_BUGBEAR = 20600
const TAMLIN_ORC = 20601
const TAMLIN_ORC_ARCHER = 20602
const KRONBE_SPIDER = 20603
const TAIK_ORC_ARCHER = 20631
const TAIK_ORC_WARRIOR = 20632
const TAIK_ORC_SHAMAN = 20633
const TAIK_ORC_CAPTAIN = 20634
const MIRROR = 20639
const HARIT_LIZARDMAN_GRUNT = 20641
const HARIT_LIZARDMAN_ARCHER = 20642
const HARIT_LIZARDMAN_WARRIOR = 20643
const GRAVE_WANDERER = 20659
const ARCHER_OF_GREED = 20660
const HATAR_RATMAN_THIEF = 20661
const HATAR_RATMAN_BOSS = 20662
const DEPRIVE = 20664
const FARCRAN = 20667
const TAIRIM = 20675
const JUDGE_OF_MARSH = 20676
const VANOR_SILENOS_GRUNT = 20682
const VANOR_SILENOS_SCOUT = 20683
const VANOR_SILENOS_WARRIOR = 20684
const VANOR_SILENOS_CHIEFTAIN = 20686
const BREKA_OVERLORD_HAKA = 27140
const BREKA_OVERLORD_JAKA = 27141
const BREKA_OVERLORD_MARKA = 27142
const WINDSUS_ALEPH = 27143
const TARLK_RAIDER_ATHU = 27144
const TARLK_RAIDER_LANKA = 27145
const TARLK_RAIDER_TRISKA = 27146
const TARLK_RAIDER_MOTURA = 27147
const TARLK_RAIDER_KALATH = 27148
const GREMLIN_FILCHER = 27149
const BLACK_LEGION_STORMTROOPER = 27150
const LETO_SHAMAN_KETZ = 27156
const LETO_CHIEF_NARAK = 27157
const TIMAK_RAIDER_KAIKEE = 27158
const TIMAK_OVERLORD_OKUN = 27159
const GOK_MAGOK = 27160
const TAIK_OVERLORD_KAKRAN = 27161
const HATAR_CHIEFTAIN_KUBEL = 27162
const VANOR_ELDER_KERUNOS = 27163
const KARUL_CHIEF_OROOTO = 27164

const minimumLevel = 35
const maximumLevel = 45
const CYBELLINS_DAGGER = 3471
const FIRST_CIRCLE_HUNTER_LICENSE = 3692
const SECOND_CIRCLE_HUNTER_LICENSE = 3693
const LAUREL_LEAF_PIN = 3694
const TEST_INSTRUCTIONS_1 = 3695
const TEST_INSTRUCTIONS_2 = 3696
const CYBELLINS_REQUEST = 3697
const BLOOD_CRYSTAL_PURITY_1 = 3698
const BLOOD_CRYSTAL_PURITY_2 = 3699
const BLOOD_CRYSTAL_PURITY_3 = 3700
const BLOOD_CRYSTAL_PURITY_4 = 3701
const BLOOD_CRYSTAL_PURITY_5 = 3702
const BLOOD_CRYSTAL_PURITY_6 = 3703
const BLOOD_CRYSTAL_PURITY_7 = 3704
const BLOOD_CRYSTAL_PURITY_8 = 3705
const BLOOD_CRYSTAL_PURITY_9 = 3706
const BLOOD_CRYSTAL_PURITY_10 = 3707
const BROKEN_BLOOD_CRYSTAL = 3708
const GUARDIAN_BASILISK_SCALE = 3709
const KARUT_WEED = 3710
const HAKAS_HEAD = 3711
const JAKAS_HEAD = 3712
const MARKAS_HEAD = 3713
const WINDSUS_ALEPH_SKIN = 3714
const INDIGO_SPIRIT_ORE = 3715
const SPORESEA_SEED = 3716
const TIMAK_ORC_TOTEM = 3717
const TRISALIM_SILK = 3718
const AMBROSIUS_FRUIT = 3719
const BALEFIRE_CRYSTAL = 3720
const IMPERIAL_ARROWHEAD = 3721
const ATHUS_HEAD = 3722
const LANKAS_HEAD = 3723
const TRISKAS_HEAD = 3724
const MOTURAS_HEAD = 3725
const KALATHS_HEAD = 3726
const FIRST_CIRCLE_REQUEST_1C = 3727
const FIRST_CIRCLE_REQUEST_2C = 3728
const FIRST_CIRCLE_REQUEST_3C = 3729
const FIRST_CIRCLE_REQUEST_4C = 3730
const FIRST_CIRCLE_REQUEST_5C = 3731
const FIRST_CIRCLE_REQUEST_6C = 3732
const FIRST_CIRCLE_REQUEST_7C = 3733
const FIRST_CIRCLE_REQUEST_8C = 3734
const FIRST_CIRCLE_REQUEST_9C = 3735
const FIRST_CIRCLE_REQUEST_10C = 3736
const FIRST_CIRCLE_REQUEST_11C = 3737
const FIRST_CIRCLE_REQUEST_12C = 3738
const FIRST_CIRCLE_REQUEST_1B = 3739
const FIRST_CIRCLE_REQUEST_2B = 3740
const FIRST_CIRCLE_REQUEST_3B = 3741
const FIRST_CIRCLE_REQUEST_4B = 3742
const FIRST_CIRCLE_REQUEST_5B = 3743
const FIRST_CIRCLE_REQUEST_6B = 3744
const FIRST_CIRCLE_REQUEST_1A = 3745
const FIRST_CIRCLE_REQUEST_2A = 3746
const FIRST_CIRCLE_REQUEST_3A = 3747
const SECOND_CIRCLE_REQUEST_1C = 3748
const SECOND_CIRCLE_REQUEST_2C = 3749
const SECOND_CIRCLE_REQUEST_3C = 3750
const SECOND_CIRCLE_REQUEST_4C = 3751
const SECOND_CIRCLE_REQUEST_5C = 3752
const SECOND_CIRCLE_REQUEST_6C = 3753
const SECOND_CIRCLE_REQUEST_7C = 3754
const SECOND_CIRCLE_REQUEST_8C = 3755
const SECOND_CIRCLE_REQUEST_9C = 3756
const SECOND_CIRCLE_REQUEST_10C = 3757
const SECOND_CIRCLE_REQUEST_11C = 3758
const SECOND_CIRCLE_REQUEST_12C = 3759
const SECOND_CIRCLE_REQUEST_1B = 3760
const SECOND_CIRCLE_REQUEST_2B = 3761
const SECOND_CIRCLE_REQUEST_3B = 3762
const SECOND_CIRCLE_REQUEST_4B = 3763
const SECOND_CIRCLE_REQUEST_5B = 3764
const SECOND_CIRCLE_REQUEST_6B = 3765
const SECOND_CIRCLE_REQUEST_1A = 3766
const SECOND_CIRCLE_REQUEST_2A = 3767
const SECOND_CIRCLE_REQUEST_3A = 3768
const CHARM_OF_KADESH = 3769
const TIMAK_JADE_NECKLACE = 3770
const ENCHANTED_GOLEM_SHARD = 3771
const GIANT_MONSTER_EYE_MEAT = 3772
const DIRE_WYRM_EGG = 3773
const GUARDIAN_BASILISK_TALON = 3774
const REVENANTS_CHAINS = 3775
const WINDSUS_TUSK = 3776
const GRANDISS_SKULL = 3777
const TAIK_OBSIDIAN_AMULET = 3778
const KARUL_BUGBEAR_HEAD = 3779
const TAMLIN_IVORY_CHARM = 3780
const FANG_OF_NARAK = 3781
const ENCHANTED_GARGOYLES_HORN = 3782
const COILED_SERPENT_TOTEM = 3783
const TOTEM_OF_KADESH = 3784
const KAIKIS_HEAD = 3785
const KRONBE_VENOM_SAC = 3786
const EVAS_CHARM = 3787
const TITANS_TABLET = 3788
const BOOK_OF_SHUNAIMAN = 3789
const ROTTING_TREE_SPORES = 3790
const TRISALIM_VENOM_SAC = 3791
const TAIK_ORC_TOTEM = 3792
const HARIT_BARBED_NECKLACE = 3793
const COIN_OF_OLD_EMPIRE = 3794
const SKIN_OF_FARCRAN = 3795
const TEMPEST_SHARD = 3796
const TSUNAMI_SHARD = 3797
const SATYR_MANE = 3798
const HAMADRYAD_SHARD = 3799
const VANOR_SILENOS_MANE = 3800
const TALK_BUGBEAR_TOTEM = 3801
const OKUNS_HEAD = 3802
const KAKRANS_HEAD = 3803
const NARCISSUSS_SOULSTONE = 3804
const DEPRIVE_EYE = 3805
const UNICORNS_HORN = 3806
const KERUNOSS_GOLD_MANE = 3807
const SKULL_OF_EXECUTED = 3808
const BUST_OF_TRAVIS = 3809
const SWORD_OF_CADMUS = 3810

// required itemId, counted itemId, amount needed, adena reward
type PlayerRewardParameters = [ number, number, number, number ]
const preconditionRewards: Array<PlayerRewardParameters> = [
    [ FIRST_CIRCLE_REQUEST_1C, CHARM_OF_KADESH, 40, 2090 ],
    [ FIRST_CIRCLE_REQUEST_3C, ENCHANTED_GOLEM_SHARD, 50, 9480 ],
    [ FIRST_CIRCLE_REQUEST_4C, GIANT_MONSTER_EYE_MEAT, 30, 9110 ],
    [ FIRST_CIRCLE_REQUEST_5C, DIRE_WYRM_EGG, 40, 8690 ],
    [ FIRST_CIRCLE_REQUEST_6C, GUARDIAN_BASILISK_TALON, 100, 9480 ],
    [ FIRST_CIRCLE_REQUEST_7C, REVENANTS_CHAINS, 50, 11280 ],
    [ FIRST_CIRCLE_REQUEST_8C, WINDSUS_TUSK, 30, 9640 ],
    [ FIRST_CIRCLE_REQUEST_9C, GRANDISS_SKULL, 100, 9180 ],
    [ FIRST_CIRCLE_REQUEST_10C, TAIK_OBSIDIAN_AMULET, 50, 5160 ],
    [ FIRST_CIRCLE_REQUEST_11C, KARUL_BUGBEAR_HEAD, 30, 3140 ],
    [ FIRST_CIRCLE_REQUEST_12C, TAMLIN_IVORY_CHARM, 40, 3160 ],
    [ FIRST_CIRCLE_REQUEST_1B, FANG_OF_NARAK, 1, 6370 ],
    [ FIRST_CIRCLE_REQUEST_2B, ENCHANTED_GARGOYLES_HORN, 50, 19080 ],
    [ FIRST_CIRCLE_REQUEST_3B, COILED_SERPENT_TOTEM, 50, 19080 ],
    [ FIRST_CIRCLE_REQUEST_4B, TOTEM_OF_KADESH, 1, 5790 ],
    [ FIRST_CIRCLE_REQUEST_5B, KAIKIS_HEAD, 1, 8560 ],
    [ FIRST_CIRCLE_REQUEST_6B, KRONBE_VENOM_SAC, 30, 8320 ],
    [ FIRST_CIRCLE_REQUEST_1A, EVAS_CHARM, 30, 30310 ],
    [ FIRST_CIRCLE_REQUEST_2A, TITANS_TABLET, 1, 27540 ],
    [ FIRST_CIRCLE_REQUEST_3A, BOOK_OF_SHUNAIMAN, 1, 20560 ],
    [ SECOND_CIRCLE_REQUEST_1C, ROTTING_TREE_SPORES, 40, 6850 ],
    [ SECOND_CIRCLE_REQUEST_2C, TRISALIM_VENOM_SAC, 40, 7250 ],
    [ SECOND_CIRCLE_REQUEST_3C, TAIK_ORC_TOTEM, 50, 7160 ],
    [ SECOND_CIRCLE_REQUEST_4C, HARIT_BARBED_NECKLACE, 40, 6580 ],
    [ SECOND_CIRCLE_REQUEST_5C, COIN_OF_OLD_EMPIRE, 20, 10100 ],
    [ SECOND_CIRCLE_REQUEST_6C, SKIN_OF_FARCRAN, 30, 13000 ],
    [ SECOND_CIRCLE_REQUEST_7C, TEMPEST_SHARD, 40, 7660 ],
    [ SECOND_CIRCLE_REQUEST_8C, TSUNAMI_SHARD, 40, 7660 ],
    [ SECOND_CIRCLE_REQUEST_9C, SATYR_MANE, 40, 11260 ],
    [ SECOND_CIRCLE_REQUEST_10C, HAMADRYAD_SHARD, 40, 7660 ],
    [ SECOND_CIRCLE_REQUEST_11C, VANOR_SILENOS_MANE, 30, 8810 ],
    [ SECOND_CIRCLE_REQUEST_12C, TALK_BUGBEAR_TOTEM, 30, 7350 ],
    [ SECOND_CIRCLE_REQUEST_1B, OKUNS_HEAD, 1, 8760 ],
    [ SECOND_CIRCLE_REQUEST_2B, KAKRANS_HEAD, 1, 9380 ],
    [ SECOND_CIRCLE_REQUEST_3B, NARCISSUSS_SOULSTONE, 40, 17820 ],
    [ SECOND_CIRCLE_REQUEST_4B, DEPRIVE_EYE, 20, 17540 ],
    [ SECOND_CIRCLE_REQUEST_5B, UNICORNS_HORN, 20, 14160 ],
    [ SECOND_CIRCLE_REQUEST_6B, KERUNOSS_GOLD_MANE, 1, 15960 ],
    [ SECOND_CIRCLE_REQUEST_1A, SKULL_OF_EXECUTED, 20, 39100 ],
    [ SECOND_CIRCLE_REQUEST_2A, BUST_OF_TRAVIS, 1, 39550 ],
    [ SECOND_CIRCLE_REQUEST_3A, SWORD_OF_CADMUS, 10, 41200 ],
].reverse() as Array<PlayerRewardParameters>

// prerequisite itemId, itemId rewarded, minimum amount, maximum amount, chance
type MonsterReward = [ number, number, number, number, number ]
const monsterRewardItems: { [ npcId: number ]: Array<MonsterReward> } = {
    [ BREKA_ORC_SHAMAN ]: [ [ FIRST_CIRCLE_REQUEST_3B, COILED_SERPENT_TOTEM, 1, 50, 93 ] ],
    [ BREKA_ORC_WARRIOR ]: [ [ FIRST_CIRCLE_REQUEST_3B, COILED_SERPENT_TOTEM, 1, 50, 100 ] ],
    [ GUARDIAN_BASILISK ]: [ [ TEST_INSTRUCTIONS_1, GUARDIAN_BASILISK_SCALE, 1, 40, 90 ], [ FIRST_CIRCLE_REQUEST_6C, GUARDIAN_BASILISK_TALON, 1, 100, 100 ] ],
    [ FETTERED_SOUL ]: [ [ FIRST_CIRCLE_REQUEST_7C, REVENANTS_CHAINS, 1, 50, 100 ] ],
    [ WINDSUS ]: [ [ FIRST_CIRCLE_REQUEST_8C, WINDSUS_TUSK, 1, 30, 63 ] ],
    [ GRANDIS ]: [ [ FIRST_CIRCLE_REQUEST_9C, GRANDISS_SKULL, 2, 100, 100 ] ],
    [ GIANT_FUNGUS ]: [ [ TEST_INSTRUCTIONS_1, SPORESEA_SEED, 1, 30, 84 ] ],
    [ GIANT_MONSTEREYE ]: [ [ FIRST_CIRCLE_REQUEST_4C, GIANT_MONSTER_EYE_MEAT, 1, 30, 60 ] ],
    [ DIRE_WYRM ]: [ [ FIRST_CIRCLE_REQUEST_5C, DIRE_WYRM_EGG, 1, 40, 90 ] ],
    [ ROTTING_TREE ]: [ [ SECOND_CIRCLE_REQUEST_1C, ROTTING_TREE_SPORES, 1, 40, 77 ] ],
    [ TRISALIM_SPIDER ]: [ [ TEST_INSTRUCTIONS_2, TRISALIM_SILK, 1, 20, 60 ], [ SECOND_CIRCLE_REQUEST_2C, TRISALIM_VENOM_SAC, 1, 40, 76 ] ],
    [ TRISALIM_TARANTULA ]: [ [ TEST_INSTRUCTIONS_2, TRISALIM_SILK, 1, 20, 60 ], [ SECOND_CIRCLE_REQUEST_2C, TRISALIM_VENOM_SAC, 1, 40, 85 ] ],
    [ SPORE_ZOMBIE ]: [ [ SECOND_CIRCLE_REQUEST_2C, TRISALIM_VENOM_SAC, 1, 30, 60 ] ],
    [ MANASHEN_GARGOYLE ]: [ [ TEST_INSTRUCTIONS_1, INDIGO_SPIRIT_ORE, 1, 20, 60 ] ],
    [ ENCHANTED_STONE_GOLEM ]: [ [ FIRST_CIRCLE_REQUEST_3C, ENCHANTED_GOLEM_SHARD, 1, 50, 100 ], [ TEST_INSTRUCTIONS_1, INDIGO_SPIRIT_ORE, 1, 20, 62 ] ],
    [ ENCHANTED_GARGOYLE ]: [ [ FIRST_CIRCLE_REQUEST_2B, ENCHANTED_GARGOYLES_HORN, 1, 50, 60 ] ],
    [ TARLK_BUGBEAR_WARRIOR ]: [ [ SECOND_CIRCLE_REQUEST_12C, TALK_BUGBEAR_TOTEM, 1, 30, 73 ] ],
    [ LETO_LIZARDMAN_ARCHER ]: [ [ FIRST_CIRCLE_REQUEST_1C, CHARM_OF_KADESH, 1, 40, 90 ] ],
    [ LETO_LIZARDMAN_SOLDIER ]: [ [ FIRST_CIRCLE_REQUEST_1C, CHARM_OF_KADESH, 1, 40, 93 ] ],
    [ LETO_LIZARDMAN_SHAMAN ]: [ [ TEST_INSTRUCTIONS_1, KARUT_WEED, 1, 20, 60 ] ],
    [ LETO_LIZARDMAN_OVERLORD ]: [ [ TEST_INSTRUCTIONS_1, KARUT_WEED, 1, 20, 60 ] ],
    [ TIMAK_ORC_WARRIOR ]: [ [ FIRST_CIRCLE_REQUEST_2C, TIMAK_JADE_NECKLACE, 1, 50, 95 ], [ TEST_INSTRUCTIONS_2, TIMAK_ORC_TOTEM, 1, 20, 60 ] ],
    [ TIMAK_ORC_OVERLORD ]: [ [ FIRST_CIRCLE_REQUEST_2C, TIMAK_JADE_NECKLACE, 1, 50, 100 ] ],
    [ FLINE ]: [ [ SECOND_CIRCLE_REQUEST_7C, TEMPEST_SHARD, 1, 40, 59 ] ],
    [ LIELE ]: [ [ SECOND_CIRCLE_REQUEST_8C, TSUNAMI_SHARD, 1, 40, 61 ] ],
    [ VALLEY_TREANT ]: [ [ TEST_INSTRUCTIONS_2, AMBROSIUS_FRUIT, 1, 30, 85 ] ],
    [ SATYR ]: [ [ SECOND_CIRCLE_REQUEST_9C, SATYR_MANE, 1, 40, 90 ] ],
    [ UNICORN ]: [ [ SECOND_CIRCLE_REQUEST_5B, UNICORNS_HORN, 1, 20, 78 ] ],
    [ FOREST_RUNNER ]: [ [ SECOND_CIRCLE_REQUEST_10C, HAMADRYAD_SHARD, 1, 40, 74 ] ],
    [ VALLEY_TREANT_ELDER ]: [ [ TEST_INSTRUCTIONS_2, AMBROSIUS_FRUIT, 1, 30, 85 ] ],
    [ SATYR_ELDER ]: [ [ SECOND_CIRCLE_REQUEST_9C, SATYR_MANE, 1, 40, 100 ] ],
    [ UNICORN_ELDER ]: [ [ SECOND_CIRCLE_REQUEST_5B, UNICORNS_HORN, 1, 20, 96 ] ],
    [ KARUL_BUGBEAR ]: [ [ FIRST_CIRCLE_REQUEST_11C, KARUL_BUGBEAR_HEAD, 1, 30, 60 ] ],
    [ TAMLIN_ORC ]: [ [ FIRST_CIRCLE_REQUEST_12C, TAMLIN_IVORY_CHARM, 1, 40, 72 ] ],
    [ TAMLIN_ORC_ARCHER ]: [ [ FIRST_CIRCLE_REQUEST_12C, TAMLIN_IVORY_CHARM, 1, 40, 90 ] ],
    [ KRONBE_SPIDER ]: [ [ FIRST_CIRCLE_REQUEST_6B, KRONBE_VENOM_SAC, 1, 30, 60 ] ],
    [ TAIK_ORC_ARCHER ]: [ [ FIRST_CIRCLE_REQUEST_10C, TAIK_OBSIDIAN_AMULET, 1, 50, 100 ] ],
    [ TAIK_ORC_WARRIOR ]: [ [ FIRST_CIRCLE_REQUEST_10C, TAIK_OBSIDIAN_AMULET, 1, 50, 93 ] ],
    [ TAIK_ORC_SHAMAN ]: [ [ SECOND_CIRCLE_REQUEST_3C, TAIK_ORC_TOTEM, 1, 50, 63 ] ],
    [ TAIK_ORC_CAPTAIN ]: [ [ SECOND_CIRCLE_REQUEST_3C, TAIK_ORC_TOTEM, 1, 50, 99 ] ],
    [ MIRROR ]: [ [ SECOND_CIRCLE_REQUEST_3B, NARCISSUSS_SOULSTONE, 1, 40, 96 ] ],
    [ HARIT_LIZARDMAN_GRUNT ]: [ [ SECOND_CIRCLE_REQUEST_4C, HARIT_BARBED_NECKLACE, 1, 40, 98 ] ],
    [ HARIT_LIZARDMAN_ARCHER ]: [ [ SECOND_CIRCLE_REQUEST_4C, HARIT_BARBED_NECKLACE, 1, 40, 98 ] ],
    [ HARIT_LIZARDMAN_WARRIOR ]: [ [ SECOND_CIRCLE_REQUEST_4C, HARIT_BARBED_NECKLACE, 1, 40, 100 ] ],
    [ GRAVE_WANDERER ]: [ [ SECOND_CIRCLE_REQUEST_1A, SKULL_OF_EXECUTED, 1, 20, 83 ] ],
    [ ARCHER_OF_GREED ]: [ [ TEST_INSTRUCTIONS_2, IMPERIAL_ARROWHEAD, 1, 20, 60 ] ],
    [ HATAR_RATMAN_THIEF ]: [ [ SECOND_CIRCLE_REQUEST_5C, COIN_OF_OLD_EMPIRE, 1, 20, 60 ] ],
    [ HATAR_RATMAN_BOSS ]: [ [ SECOND_CIRCLE_REQUEST_5C, COIN_OF_OLD_EMPIRE, 1, 20, 62 ] ],
    [ DEPRIVE ]: [ [ SECOND_CIRCLE_REQUEST_4B, DEPRIVE_EYE, 1, 20, 87 ] ],
    [ FARCRAN ]: [ [ SECOND_CIRCLE_REQUEST_6C, SKIN_OF_FARCRAN, 1, 30, 100 ] ],
    [ TAIRIM ]: [ [ TEST_INSTRUCTIONS_2, BALEFIRE_CRYSTAL, 1, 20, 60 ] ],
    [ JUDGE_OF_MARSH ]: [ [ SECOND_CIRCLE_REQUEST_3A, SWORD_OF_CADMUS, 1, 10, 74 ] ],
    [ VANOR_SILENOS_GRUNT ]: [ [ SECOND_CIRCLE_REQUEST_11C, VANOR_SILENOS_MANE, 1, 30, 80 ] ],
    [ VANOR_SILENOS_SCOUT ]: [ [ SECOND_CIRCLE_REQUEST_11C, VANOR_SILENOS_MANE, 1, 30, 95 ] ],
    [ VANOR_SILENOS_WARRIOR ]: [ [ SECOND_CIRCLE_REQUEST_11C, VANOR_SILENOS_MANE, 1, 30, 100 ] ],
    [ BREKA_OVERLORD_HAKA ]: [ [ TEST_INSTRUCTIONS_1, HAKAS_HEAD, 1, 1, 100 ] ],
    [ BREKA_OVERLORD_JAKA ]: [ [ TEST_INSTRUCTIONS_1, JAKAS_HEAD, 1, 1, 100 ] ],
    [ BREKA_OVERLORD_MARKA ]: [ [ TEST_INSTRUCTIONS_1, MARKAS_HEAD, 1, 1, 100 ] ],
    [ WINDSUS_ALEPH ]: [ [ TEST_INSTRUCTIONS_1, WINDSUS_ALEPH_SKIN, 1, 1, 100 ] ],
    [ TARLK_RAIDER_ATHU ]: [ [ TEST_INSTRUCTIONS_2, ATHUS_HEAD, 1, 1, 100 ] ],
    [ TARLK_RAIDER_LANKA ]: [ [ TEST_INSTRUCTIONS_2, LANKAS_HEAD, 1, 1, 100 ] ],
    [ TARLK_RAIDER_TRISKA ]: [ [ TEST_INSTRUCTIONS_2, TRISKAS_HEAD, 1, 1, 100 ] ],
    [ TARLK_RAIDER_MOTURA ]: [ [ TEST_INSTRUCTIONS_2, MOTURAS_HEAD, 1, 1, 100 ] ],
    [ TARLK_RAIDER_KALATH ]: [ [ TEST_INSTRUCTIONS_2, KALATHS_HEAD, 1, 1, 100 ] ],
    [ LETO_SHAMAN_KETZ ]: [ [ FIRST_CIRCLE_REQUEST_4B, TOTEM_OF_KADESH, 1, 1, 100 ] ],
    [ LETO_CHIEF_NARAK ]: [ [ FIRST_CIRCLE_REQUEST_1B, FANG_OF_NARAK, 1, 1, 100 ] ],
    [ TIMAK_RAIDER_KAIKEE ]: [ [ FIRST_CIRCLE_REQUEST_5B, KAIKIS_HEAD, 1, 1, 100 ] ],
    [ TIMAK_OVERLORD_OKUN ]: [ [ SECOND_CIRCLE_REQUEST_1B, OKUNS_HEAD, 1, 1, 100 ] ],
    [ GOK_MAGOK ]: [ [ FIRST_CIRCLE_REQUEST_2A, TITANS_TABLET, 1, 1, 100 ] ],
    [ TAIK_OVERLORD_KAKRAN ]: [ [ SECOND_CIRCLE_REQUEST_2B, KAKRANS_HEAD, 1, 1, 100 ] ],
    [ HATAR_CHIEFTAIN_KUBEL ]: [ [ SECOND_CIRCLE_REQUEST_2A, BUST_OF_TRAVIS, 1, 1, 100 ] ],
    [ VANOR_ELDER_KERUNOS ]: [ [ SECOND_CIRCLE_REQUEST_6B, KERUNOSS_GOLD_MANE, 1, 1, 100 ] ],
    [ KARUL_CHIEF_OROOTO ]: [ [ FIRST_CIRCLE_REQUEST_3A, BOOK_OF_SHUNAIMAN, 1, 1, 100 ] ],
}

const npcLinks: { [ npcId: number ]: string } = {
    33520: '<a action="bypass -h Quest Q00335_TheSongOfTheHunter 30745-10a.html">C: 40 Totems of Kadesh</a><br>',
    33521: '<a action="bypass -h Quest Q00335_TheSongOfTheHunter 30745-10b.html">C: 50 Jade Necklaces of Timak</a><br>',
    33522: '<a action="bypass -h Quest Q00335_TheSongOfTheHunter 30745-10c.html">C: 50 Enchanted Golem Shards</a><br>',
    33523: '<a action="bypass -h Quest Q00335_TheSongOfTheHunter 30745-10d.html">C: 30 Pieces Monster Eye Meat</a><br>',
    33524: '<a action="bypass -h Quest Q00335_TheSongOfTheHunter 30745-10e.html">C: 40 Eggs of Dire Wyrm</a><br>',
    33525: '<a action="bypass -h Quest Q00335_TheSongOfTheHunter 30745-10f.html">C: 100 Claws of Guardian Basilisk</a><br>',
    33526: '<a action="bypass -h Quest Q00335_TheSongOfTheHunter 30745-10g.html">C: 50 Revenant Chains </a><br>',
    33527: '<a action="bypass -h Quest Q00335_TheSongOfTheHunter 30745-10h.html">C: 30 Windsus Tusks</a><br>',
    33528: '<a action="bypass -h Quest Q00335_TheSongOfTheHunter 30745-10i.html">C: 100 Skulls of Grandis</a><br>',
    33529: '<a action="bypass -h Quest Q00335_TheSongOfTheHunter 30745-10j.html">C: 50 Taik Obsidian Amulets</a><br>',
    33530: '<a action="bypass -h Quest Q00335_TheSongOfTheHunter 30745-10k.html">C: 30 Heads of Karul Bugbear</a><br>',
    33531: '<a action="bypass -h Quest Q00335_TheSongOfTheHunter 30745-10l.html">C: 40 Ivory Charms of Tamlin</a><br>',
    33532: '<a action="bypass -h Quest Q00335_TheSongOfTheHunter 30745-11a.html">B: Situation Preparation - Leto Chief</a><br>',
    33533: '<a action="bypass -h Quest Q00335_TheSongOfTheHunter 30745-11b.html">B: 50 Enchanted Gargoyle Horns</a><br>',
    33534: '<a action="bypass -h Quest Q00335_TheSongOfTheHunter 30745-11c.html">B: 50 Coiled Serpent Totems</a><br>',
    33535: '<a action="bypass -h Quest Q00335_TheSongOfTheHunter 30745-11d.html">B: Situation Preparation - Sorcerer Catch of Leto</a><br>',
    33536: '<a action="bypass -h Quest Q00335_TheSongOfTheHunter 30745-11e.html">B: Situation Preparation - Timak Raider Kaikee</a><br>',
    33537: '<a action="bypass -h Quest Q00335_TheSongOfTheHunter 30745-11f.html">B: 30 Kronbe Venom Sacs</a><br>',
    33538: '<a action="bypass -h Quest Q00335_TheSongOfTheHunter 30745-12a.html">A: 30 Charms of Eva</a><br>',
    33539: '<a action="bypass -h Quest Q00335_TheSongOfTheHunter 30745-12b.html">A: Titan\'s Tablet</a><br>',
    33540: '<a action="bypass -h Quest Q00335_TheSongOfTheHunter 30745-12c.html">A: Book of Shunaiman</a><br>',
    33541: '<a action="bypass -h Quest Q00335_TheSongOfTheHunter 30745-13a.html">C: 40 Rotted Tree Spores</a><br>',
    33542: '<a action="bypass -h Quest Q00335_TheSongOfTheHunter 30745-13b.html">C: 40 Trisalim Venom Sacs</a><br>',
    33543: '<a action="bypass -h Quest Q00335_TheSongOfTheHunter 30745-13c.html">C: 50 Totems of Taik Orc</a><br>',
    33544: '<a action="bypass -h Quest Q00335_TheSongOfTheHunter 30745-13d.html">C: 40 Harit Barbed Necklaces</a><br>',
    33545: '<a action="bypass -h Quest Q00335_TheSongOfTheHunter 30745-13e.html">C: 20 Coins of Ancient Empire</a><br>',
    33546: '<a action="bypass -h Quest Q00335_TheSongOfTheHunter 30745-13f.html">C: 30 Skins of Farkran</a><br>',
    33547: '<a action="bypass -h Quest Q00335_TheSongOfTheHunter 30745-13g.html">C: 40 Tempest Shards</a><br>',
    33548: '<a action="bypass -h Quest Q00335_TheSongOfTheHunter 30745-13k.html">C: 30 Vanor Silenos Manes</a><br>',
    33549: '<a action="bypass -h Quest Q00335_TheSongOfTheHunter 30745-13i.html">C: 40 Manes of Pan Ruem</a><br>',
    33550: '<a action="bypass -h Quest Q00335_TheSongOfTheHunter 30745-13j.html">C: hamadryad shards</a><br>',
    33551: '<a action="bypass -h Quest Q00335_TheSongOfTheHunter 30745-13k.html">C: 30 Manes of Vanor Silenos</a><br>',
    33552: '<a action="bypass -h Quest Q00335_TheSongOfTheHunter 30745-13l.html">C: 30 Totems of Talk Bugbears</a><br>',
    33553: '<a action="bypass -h Quest Q00335_TheSongOfTheHunter 30745-14a.html">B: Situation Preparation - Overlord Okun of Timak</a><br>',
    33554: '<a action="bypass -h Quest Q00335_TheSongOfTheHunter 30745-14b.html">B: Situation Preparation - Overlord Kakran of Taik</a><br>',
    33555: '<a action="bypass -h Quest Q00335_TheSongOfTheHunter 30745-14c.html">B: 40 Narcissus Soulstones</a><br>',
    33556: '<a action="bypass -h Quest Q00335_TheSongOfTheHunter 30745-14d.html">B: 20 Eyes of Deprived</a><br>',
    33557: '<a action="bypass -h Quest Q00335_TheSongOfTheHunter 30745-14e.html">B: 20 Unicorn Horns</a><br>',
    33558: '<a action="bypass -h Quest Q00335_TheSongOfTheHunter 30745-14f.html">B: Kerunos\'s Gold Mane</a><br>',
}

const purityCrystalItems: Array<number> = [
    BLOOD_CRYSTAL_PURITY_2,
    BLOOD_CRYSTAL_PURITY_3,
    BLOOD_CRYSTAL_PURITY_4,
    BLOOD_CRYSTAL_PURITY_5,
    BLOOD_CRYSTAL_PURITY_6,
    BLOOD_CRYSTAL_PURITY_7,
    BLOOD_CRYSTAL_PURITY_8,
    BLOOD_CRYSTAL_PURITY_9,
    BLOOD_CRYSTAL_PURITY_10,
]

const purityCrystalProgresion: Array<number> = [
    BLOOD_CRYSTAL_PURITY_1,
    BLOOD_CRYSTAL_PURITY_2,
    BLOOD_CRYSTAL_PURITY_3,
    BLOOD_CRYSTAL_PURITY_4,
    BLOOD_CRYSTAL_PURITY_5,
    BLOOD_CRYSTAL_PURITY_6,
    BLOOD_CRYSTAL_PURITY_7,
    BLOOD_CRYSTAL_PURITY_8,
    BLOOD_CRYSTAL_PURITY_9,
    BLOOD_CRYSTAL_PURITY_10,
]

type ProgressionItem = [ number, number ] // itemId, required amount
const firstProgressionItemCheck: Array<ProgressionItem> = [
    [ GUARDIAN_BASILISK_SCALE, 40 ],
    [ KARUT_WEED, 20 ],
    [ INDIGO_SPIRIT_ORE, 20 ],
    [ SPORESEA_SEED, 30 ],
]

const secondProgressionItemCheck: Array<ProgressionItem> = [
    [ TIMAK_ORC_TOTEM, 20 ],
    [ TRISALIM_SILK, 20 ],
    [ AMBROSIUS_FRUIT, 30 ],
    [ BALEFIRE_CRYSTAL, 20 ],
    [ IMPERIAL_ARROWHEAD, 20 ],
]

export class TheSongOfTheHunter extends ListenerLogic {
    constructor() {
        super( 'Q00335_TheSongOfTheHunter', 'listeners/tracked-300/TheSongOfTheHunter.ts' )
        this.questId = 335
        this.questItemIds = [
            CYBELLINS_DAGGER,
            FIRST_CIRCLE_HUNTER_LICENSE,
            SECOND_CIRCLE_HUNTER_LICENSE,
            LAUREL_LEAF_PIN,
            TEST_INSTRUCTIONS_1,
            TEST_INSTRUCTIONS_2,
            CYBELLINS_REQUEST,
            BLOOD_CRYSTAL_PURITY_1,
            BLOOD_CRYSTAL_PURITY_2,
            BLOOD_CRYSTAL_PURITY_3,
            BLOOD_CRYSTAL_PURITY_4,
            BLOOD_CRYSTAL_PURITY_5,
            BLOOD_CRYSTAL_PURITY_6,
            BLOOD_CRYSTAL_PURITY_7,
            BLOOD_CRYSTAL_PURITY_8,
            BLOOD_CRYSTAL_PURITY_9,
            BLOOD_CRYSTAL_PURITY_10,
            BROKEN_BLOOD_CRYSTAL,
            GUARDIAN_BASILISK_SCALE,
            KARUT_WEED,
            HAKAS_HEAD,
            JAKAS_HEAD,
            MARKAS_HEAD,
            WINDSUS_ALEPH_SKIN,
            INDIGO_SPIRIT_ORE,
            SPORESEA_SEED,
            TIMAK_ORC_TOTEM,
            TRISALIM_SILK,
            AMBROSIUS_FRUIT,
            BALEFIRE_CRYSTAL,
            IMPERIAL_ARROWHEAD,
            ATHUS_HEAD,
            LANKAS_HEAD,
            TRISKAS_HEAD,
            MOTURAS_HEAD,
            KALATHS_HEAD,
            FIRST_CIRCLE_REQUEST_1C,
            FIRST_CIRCLE_REQUEST_2C,
            FIRST_CIRCLE_REQUEST_3C,
            FIRST_CIRCLE_REQUEST_4C,
            FIRST_CIRCLE_REQUEST_5C,
            FIRST_CIRCLE_REQUEST_6C,
            FIRST_CIRCLE_REQUEST_7C,
            FIRST_CIRCLE_REQUEST_8C,
            FIRST_CIRCLE_REQUEST_9C,
            FIRST_CIRCLE_REQUEST_10C,
            FIRST_CIRCLE_REQUEST_11C,
            FIRST_CIRCLE_REQUEST_12C,
            FIRST_CIRCLE_REQUEST_1B,
            FIRST_CIRCLE_REQUEST_2B,
            FIRST_CIRCLE_REQUEST_3B,
            FIRST_CIRCLE_REQUEST_4B,
            FIRST_CIRCLE_REQUEST_5B,
            FIRST_CIRCLE_REQUEST_6B,
            FIRST_CIRCLE_REQUEST_1A,
            FIRST_CIRCLE_REQUEST_2A,
            FIRST_CIRCLE_REQUEST_3A,
            SECOND_CIRCLE_REQUEST_1C,
            SECOND_CIRCLE_REQUEST_2C,
            SECOND_CIRCLE_REQUEST_3C,
            SECOND_CIRCLE_REQUEST_4C,
            SECOND_CIRCLE_REQUEST_5C,
            SECOND_CIRCLE_REQUEST_6C,
            SECOND_CIRCLE_REQUEST_7C,
            SECOND_CIRCLE_REQUEST_8C,
            SECOND_CIRCLE_REQUEST_9C,
            SECOND_CIRCLE_REQUEST_10C,
            SECOND_CIRCLE_REQUEST_11C,
            SECOND_CIRCLE_REQUEST_12C,
            SECOND_CIRCLE_REQUEST_1B,
            SECOND_CIRCLE_REQUEST_2B,
            SECOND_CIRCLE_REQUEST_3B,
            SECOND_CIRCLE_REQUEST_4B,
            SECOND_CIRCLE_REQUEST_5B,
            SECOND_CIRCLE_REQUEST_6B,
            SECOND_CIRCLE_REQUEST_1A,
            SECOND_CIRCLE_REQUEST_2A,
            SECOND_CIRCLE_REQUEST_3A,
            CHARM_OF_KADESH,
            TIMAK_JADE_NECKLACE,
            ENCHANTED_GOLEM_SHARD,
            GIANT_MONSTER_EYE_MEAT,
            DIRE_WYRM_EGG,
            GUARDIAN_BASILISK_TALON,
            REVENANTS_CHAINS,
            WINDSUS_TUSK,
            GRANDISS_SKULL,
            TAIK_OBSIDIAN_AMULET,
            KARUL_BUGBEAR_HEAD,
            TAMLIN_IVORY_CHARM,
            FANG_OF_NARAK,
            ENCHANTED_GARGOYLES_HORN,
            COILED_SERPENT_TOTEM,
            TOTEM_OF_KADESH,
            KAIKIS_HEAD,
            KRONBE_VENOM_SAC,
            EVAS_CHARM,
            TITANS_TABLET,
            BOOK_OF_SHUNAIMAN,
            ROTTING_TREE_SPORES,
            TRISALIM_VENOM_SAC,
            TAIK_ORC_TOTEM,
            HARIT_BARBED_NECKLACE,
            COIN_OF_OLD_EMPIRE,
            SKIN_OF_FARCRAN,
            TEMPEST_SHARD,
            TSUNAMI_SHARD,
            SATYR_MANE,
            HAMADRYAD_SHARD,
            VANOR_SILENOS_MANE,
            TALK_BUGBEAR_TOTEM,
            OKUNS_HEAD,
            KAKRANS_HEAD,
            NARCISSUSS_SOULSTONE,
            DEPRIVE_EYE,
            UNICORNS_HORN,
            KERUNOSS_GOLD_MANE,
            SKULL_OF_EXECUTED,
            BUST_OF_TRAVIS,
            SWORD_OF_CADMUS,
        ]
    }

    composeHtml( path: string, ...links: [ number, number, number, number, number ] ): string {
        return DataManager.getHtmlData().getItem( path )
                .replace( '<?reply1?>', npcLinks[ links[ 0 ] ] )
                .replace( '<?reply2?>', npcLinks[ links[ 1 ] ] )
                .replace( '<?reply3?>', npcLinks[ links[ 2 ] ] )
                .replace( '<?reply4?>', npcLinks[ links[ 3 ] ] )
                .replace( '<?reply5?>', npcLinks[ links[ 4 ] ] )
    }

    computeFirstListHtml( state: QuestState, leafPinCount: number ): string {
        let one = 0
        let two = 0
        let three = 0
        let four = 0
        let five = 0

        if ( state.isMemoState( 0 ) ) {
            while ( one === two
            || two === three
            || three === four
            || four === five
            || one === five
            || one === three
            || one === four
            || two === four
            || two === five
            || three === five ) {

                if ( leafPinCount === 0 ) {
                    one = _.random( 10 )
                    two = _.random( 10 )
                    three = _.random( 5 )
                    four = _.random( 5 ) + 5
                    five = _.random( 10 )
                } else if ( leafPinCount < 4 ) {
                    if ( _.random( 100 ) < 20 ) {
                        one = _.random( 6 ) + 10
                        two = _.random( 10 )
                        three = _.random( 5 )
                        four = _.random( 5 ) + 5
                        five = _.random( 10 )
                    } else {
                        one = _.random( 10 )
                        two = _.random( 10 )
                        three = _.random( 5 )
                        four = _.random( 5 ) + 5
                        five = _.random( 10 )
                    }
                } else if ( _.random( 100 ) < 20 ) {
                    one = _.random( 6 ) + 10
                    if ( _.random( 20 ) === 0 ) {
                        two = _.random( 3 ) + 16
                    } else {
                        two = _.random( 10 )
                    }
                    three = _.random( 5 )
                    four = _.random( 5 ) + 5
                    five = _.random( 10 )
                } else {
                    one = _.random( 10 )
                    if ( _.random( 20 ) === 0 ) {
                        two = _.random( 3 ) + 16
                    } else {
                        two = _.random( 10 )
                    }
                    three = _.random( 5 )
                    four = _.random( 5 ) + 5
                    five = _.random( 10 )
                }

                state.setMemoState( ( one * 32 * 32 * 32 * 32 ) + ( two * 32 * 32 * 32 ) + ( three * 32 * 32 ) + ( four * 32 ) + five )
            }

            one = 33520 + ( one + 20 )
            two = 33520 + ( two + 20 )
            three = 33520 + ( three + 20 )
            four = 33520 + ( four + 20 )
            five = 33520 + ( five + 20 )

            return this.composeHtml( '30745-16.html', one, two, three, four, five )
        }

        let six = state.getMemoState()
        one = six % 32
        six = six / 32
        two = six % 32
        six = six / 32
        three = six % 32
        six = six / 32
        four = six % 32
        six = six / 32
        five = six % 32

        one = 33520 + ( one + 20 )
        two = 33520 + ( two + 20 )
        three = 33520 + ( three + 20 )
        four = 33520 + ( four + 20 )
        five = 33520 + ( five + 20 )

        return this.composeHtml( '30745-16.html', five, four, three, two, one )
    }

    computeSecondListHtml( state: QuestState, leafPinCount: number ): string {
        let one = 0
        let two = 0
        let three = 0
        let four = 0
        let five = 0

        if ( state.isMemoState( 0 ) ) {
            while ( one === two
            || two === three
            || three === four
            || four === five
            || one === five
            || one === three
            || one === four
            || two === four
            || two === five
            || three === five ) {

                if ( leafPinCount === 0 ) {
                    one = _.random( 12 )
                    two = _.random( 12 )
                    three = _.random( 12 )
                    four = _.random( 12 )
                    five = _.random( 12 )
                } else if ( leafPinCount < 4 ) {
                    if ( _.random( 100 ) < 20 ) {
                        one = _.random( 6 ) + 12
                        two = _.random( 12 )
                        three = _.random( 6 )
                        four = _.random( 6 ) + 6
                        five = _.random( 12 )
                    } else {
                        one = _.random( 12 )
                        two = _.random( 12 )
                        three = _.random( 12 )
                        four = _.random( 12 )
                        five = _.random( 12 )
                    }
                } else if ( _.random( 100 ) < 20 ) {
                    one = _.random( 6 ) + 12
                    if ( _.random( 20 ) === 0 ) {
                        two = _.random( 2 ) + 18
                    } else {
                        two = _.random( 12 )
                    }
                    three = _.random( 6 )
                    four = _.random( 6 ) + 6
                    five = _.random( 12 )
                } else {
                    one = _.random( 12 )
                    if ( _.random( 20 ) === 0 ) {
                        two = _.random( 2 ) + 18
                    } else {
                        two = _.random( 12 )
                    }
                    three = _.random( 6 )
                    four = _.random( 6 ) + 6
                    five = _.random( 12 )
                }

                state.setMemoState( ( one * 32 * 32 * 32 * 32 ) + ( two * 32 * 32 * 32 ) + ( three * 32 * 32 ) + ( four * 32 ) + five )
            }

            one = 33520 + one
            two = 33520 + two
            three = 33520 + three
            four = 33520 + four
            five = 33520 + five

            return this.composeHtml( '30745-16.html', one, two, three, four, five )
        }

        let six = state.getMemoState()
        one = six % 32
        six = six / 32
        two = six % 32
        six = six / 32
        three = six % 32
        six = six / 32
        four = six % 32
        six = six / 32
        five = six % 32

        one = 33520 + one
        two = 33520 + two
        three = 33520 + three
        four = 33520 + four
        five = 33520 + five

        return this.composeHtml( '30745-16.html', five, four, three, two, one )
    }

    async evolveBloodCrystal( player: L2PcInstance, isFromChampion: boolean ): Promise<void> {
        let weapon = player.getActiveWeaponItem()
        if ( !weapon
                || weapon.getId() !== CYBELLINS_DAGGER
                || !QuestHelper.hasAtLeastOneQuestItem( player, FIRST_CIRCLE_HUNTER_LICENSE, SECOND_CIRCLE_HUNTER_LICENSE )
                || !QuestHelper.hasQuestItem( player, CYBELLINS_REQUEST ) ) {
            return
        }

        await aigle.resolve( purityCrystalProgresion ).eachSeries( async ( neededItemId: number, index: number ) => {
            if ( index === ( purityCrystalProgresion.length - 1 ) || !QuestHelper.hasQuestItem( player, neededItemId ) ) {
                return
            }

            let rewardId = purityCrystalProgresion[ index + 1 ]

            if ( _.random( 100 ) >= QuestHelper.getAdjustedChance( rewardId, 60, isFromChampion ) ) {
                await QuestHelper.takeMultipleItems( player, -1, BLOOD_CRYSTAL_PURITY_1, BLOOD_CRYSTAL_PURITY_2, BLOOD_CRYSTAL_PURITY_3, BLOOD_CRYSTAL_PURITY_4, BLOOD_CRYSTAL_PURITY_5, BLOOD_CRYSTAL_PURITY_6, BLOOD_CRYSTAL_PURITY_7, BLOOD_CRYSTAL_PURITY_8, BLOOD_CRYSTAL_PURITY_9 )
                await QuestHelper.giveSingleItem( player, BROKEN_BLOOD_CRYSTAL, 1 )

                return false
            }

            await QuestHelper.takeSingleItem( player, neededItemId, -1 )
            await QuestHelper.giveSingleItem( player, rewardId, 1 )

            if ( index > 4 ) {
                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_JACKPOT )
            }

            return false
        } )
    }

    getAttackableKillIds(): Array<number> {
        return [
            BREKA_ORC_SHAMAN,
            BREKA_ORC_WARRIOR,
            GUARDIAN_BASILISK,
            FETTERED_SOUL,
            WINDSUS,
            GRANDIS,
            GIANT_FUNGUS,
            GIANT_MONSTEREYE,
            DIRE_WYRM,
            ROTTING_TREE,
            TRISALIM_SPIDER,
            TRISALIM_TARANTULA,
            SPORE_ZOMBIE,
            MANASHEN_GARGOYLE,
            ENCHANTED_STONE_GOLEM,
            ENCHANTED_GARGOYLE,
            TARLK_BUGBEAR_WARRIOR,
            LETO_LIZARDMAN_ARCHER,
            LETO_LIZARDMAN_SOLDIER,
            LETO_LIZARDMAN_SHAMAN,
            LETO_LIZARDMAN_OVERLORD,
            TIMAK_ORC_WARRIOR,
            TIMAK_ORC_OVERLORD,
            FLINE,
            LIELE,
            VALLEY_TREANT,
            SATYR,
            UNICORN,
            FOREST_RUNNER,
            VALLEY_TREANT_ELDER,
            SATYR_ELDER,
            UNICORN_ELDER,
            KARUL_BUGBEAR,
            TAMLIN_ORC,
            TAMLIN_ORC_ARCHER,
            KRONBE_SPIDER,
            TAIK_ORC_ARCHER,
            TAIK_ORC_WARRIOR,
            TAIK_ORC_SHAMAN,
            TAIK_ORC_CAPTAIN,
            MIRROR,
            HARIT_LIZARDMAN_GRUNT,
            HARIT_LIZARDMAN_ARCHER,
            HARIT_LIZARDMAN_WARRIOR,
            GRAVE_WANDERER,
            ARCHER_OF_GREED,
            HATAR_RATMAN_THIEF,
            HATAR_RATMAN_BOSS,
            DEPRIVE,
            FARCRAN,
            TAIRIM,
            JUDGE_OF_MARSH,
            VANOR_SILENOS_GRUNT,
            VANOR_SILENOS_SCOUT,
            VANOR_SILENOS_WARRIOR,
            BREKA_OVERLORD_HAKA,
            BREKA_OVERLORD_JAKA,
            BREKA_OVERLORD_MARKA,
            WINDSUS_ALEPH,
            TARLK_RAIDER_ATHU,
            TARLK_RAIDER_LANKA,
            TARLK_RAIDER_TRISKA,
            TARLK_RAIDER_MOTURA,
            TARLK_RAIDER_KALATH,
            GREMLIN_FILCHER,
            LETO_SHAMAN_KETZ,
            LETO_CHIEF_NARAK,
            TIMAK_RAIDER_KAIKEE,
            TIMAK_OVERLORD_OKUN,
            GOK_MAGOK,
            TAIK_OVERLORD_KAKRAN,
            HATAR_CHIEFTAIN_KUBEL,
            VANOR_ELDER_KERUNOS,
            KARUL_CHIEF_OROOTO,
            VANOR_SILENOS_CHIEFTAIN,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00335_TheSongOfTheHunter'
    }

    getQuestStartIds(): Array<number> {
        return [ GREY ]
    }

    getTalkIds(): Array<number> {
        return [ GREY, TOR, CYBELLIN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let npc = L2World.getObjectById( data.targetId ) as L2Npc
        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), -1, 3, npc )
        if ( !state ) {
            return
        }

        let player = state.getPlayer()
        await this.rewardMonsterItem( player, monsterRewardItems[ data.npcId ], data.npcId, data.isChampion )

        switch ( data.npcId ) {
            case BREKA_ORC_WARRIOR:
                if ( _.random( 9 ) < 2 && QuestHelper.hasQuestItem( player, TEST_INSTRUCTIONS_1 ) ) {
                    if ( !QuestHelper.hasQuestItem( player, HAKAS_HEAD ) ) {
                        QuestHelper.addGenericSpawn( null, BREKA_OVERLORD_HAKA, npc.getX(), npc.getY(), npc.getZ() + 10, npc.getHeading(), false, 0, true )
                        return
                    }

                    if ( !QuestHelper.hasQuestItem( player, JAKAS_HEAD ) ) {
                        QuestHelper.addGenericSpawn( null, BREKA_OVERLORD_JAKA, npc.getX(), npc.getY(), npc.getZ() + 10, npc.getHeading(), false, 0, true )
                        return
                    }

                    if ( !QuestHelper.hasQuestItem( player, MARKAS_HEAD ) ) {
                        QuestHelper.addGenericSpawn( null, BREKA_OVERLORD_MARKA, npc.getX(), npc.getY(), npc.getZ() + 10, npc.getHeading(), false, 0, true )
                        return
                    }
                }

                return

            case WINDSUS:
                if ( _.random( 9 ) < 2
                        && QuestHelper.hasQuestItem( player, TEST_INSTRUCTIONS_1 )
                        && !QuestHelper.hasQuestItem( player, WINDSUS_ALEPH_SKIN ) ) {
                    QuestHelper.addGenericSpawn( null, WINDSUS_ALEPH, npc.getX(), npc.getY(), npc.getZ() + 10, npc.getHeading(), false, 0, true )
                }

                return

            case GRANDIS:
                if ( _.random( 9 ) < 2
                        && QuestHelper.hasQuestItem( player, FIRST_CIRCLE_REQUEST_2A )
                        && !QuestHelper.hasQuestItem( player, TITANS_TABLET ) ) {
                    QuestHelper.addGenericSpawn( null, GOK_MAGOK, npc.getX(), npc.getY(), npc.getZ() + 10, npc.getHeading(), false, 0, true )
                }

                return

            case TARLK_BUGBEAR_WARRIOR:
                if ( _.random( 9 ) < 2 && QuestHelper.hasQuestItem( player, TEST_INSTRUCTIONS_2 ) ) {

                    if ( !QuestHelper.hasQuestItem( player, ATHUS_HEAD ) ) {
                        QuestHelper.addGenericSpawn( null, TARLK_RAIDER_ATHU, npc.getX(), npc.getY(), npc.getZ() + 10, npc.getHeading(), false, 0, true )
                        return
                    }

                    if ( !QuestHelper.hasQuestItem( player, LANKAS_HEAD ) ) {
                        QuestHelper.addGenericSpawn( null, TARLK_RAIDER_LANKA, npc.getX(), npc.getY(), npc.getZ() + 10, npc.getHeading(), false, 0, true )
                        return
                    }

                    if ( !QuestHelper.hasQuestItem( player, TRISKAS_HEAD ) ) {
                        QuestHelper.addGenericSpawn( null, TARLK_RAIDER_TRISKA, npc.getX(), npc.getY(), npc.getZ() + 10, npc.getHeading(), false, 0, true )
                        return
                    }

                    if ( !QuestHelper.hasQuestItem( player, MOTURAS_HEAD ) ) {
                        QuestHelper.addGenericSpawn( null, TARLK_RAIDER_MOTURA, npc.getX(), npc.getY(), npc.getZ() + 10, npc.getHeading(), false, 0, true )
                        return
                    }

                    if ( !QuestHelper.hasQuestItem( player, KALATHS_HEAD ) ) {
                        QuestHelper.addGenericSpawn( null, TARLK_RAIDER_KALATH, npc.getX(), npc.getY(), npc.getZ() + 10, npc.getHeading(), false, 0, true )
                        return
                    }
                }

                return

            case LETO_LIZARDMAN_SHAMAN:
                if ( _.random( 9 ) < 2
                        && QuestHelper.hasQuestItem( player, FIRST_CIRCLE_REQUEST_4B )
                        && !QuestHelper.hasQuestItem( player, TOTEM_OF_KADESH ) ) {
                    QuestHelper.addGenericSpawn( null, LETO_SHAMAN_KETZ, npc.getX(), npc.getY(), npc.getZ() + 10, npc.getHeading(), false, 0, true )
                }

                await this.evolveBloodCrystal( player, data.isChampion )
                return

            case LETO_LIZARDMAN_OVERLORD:
                if ( _.random( 9 ) < 2
                        && QuestHelper.hasQuestItem( player, FIRST_CIRCLE_REQUEST_1B )
                        && !QuestHelper.hasQuestItem( player, FANG_OF_NARAK ) ) {
                    QuestHelper.addGenericSpawn( null, LETO_CHIEF_NARAK, npc.getX(), npc.getY(), npc.getZ() + 10, npc.getHeading(), false, 0, true )
                }

                await this.evolveBloodCrystal( player, data.isChampion )
                return

            case TIMAK_ORC_WARRIOR:
                if ( _.random( 9 ) < 2
                        && QuestHelper.hasQuestItem( player, FIRST_CIRCLE_REQUEST_5B )
                        && !QuestHelper.hasQuestItem( player, KAIKIS_HEAD ) ) {
                    QuestHelper.addGenericSpawn( null, TIMAK_RAIDER_KAIKEE, npc.getX(), npc.getY(), npc.getZ() + 10, npc.getHeading(), false, 0, true )
                }

                return

            case TIMAK_ORC_OVERLORD:
                if ( _.random( 9 ) === 0
                        && QuestHelper.hasQuestItem( player, SECOND_CIRCLE_REQUEST_1B )
                        && !QuestHelper.hasQuestItem( player, OKUNS_HEAD ) ) {
                    QuestHelper.addGenericSpawn( null, TIMAK_OVERLORD_OKUN, npc.getX(), npc.getY(), npc.getZ() + 10, npc.getHeading(), false, 0, true )
                }

                return

            case FLINE:
            case LIELE:
                if ( _.random( 19 ) < 2
                        && QuestHelper.hasQuestItem( player, SECOND_CIRCLE_REQUEST_7C )
                        && QuestHelper.getQuestItemsCount( player, TEMPEST_SHARD ) < 40 ) {
                    QuestHelper.addGenericSpawn( null, GREMLIN_FILCHER, npc.getX(), npc.getY(), npc.getZ() + 10, npc.getHeading(), false, 0, true )
                    BroadcastHelper.dataInLocation( npc, CreatureSay.fromNpcName( data.targetId, NpcSayType.NpcAll, npc.getName(), NpcStringIds.SHOW_ME_THE_PRETTY_SPARKLING_THINGS_THEYRE_ALL_MINE ).getBuffer(), npc.getBroadcastRadius() )
                }

                return

            case FOREST_RUNNER:
                if ( _.random( 19 ) < 2
                        && QuestHelper.hasQuestItem( player, SECOND_CIRCLE_REQUEST_10C )
                        && QuestHelper.getQuestItemsCount( player, HAMADRYAD_SHARD ) < 40 ) {
                    QuestHelper.addGenericSpawn( null, GREMLIN_FILCHER, npc.getX(), npc.getY(), npc.getZ() + 10, npc.getHeading(), false, 0, true )
                    BroadcastHelper.dataInLocation( npc, CreatureSay.fromNpcName( data.targetId, NpcSayType.NpcAll, npc.getName(), NpcStringIds.SHOW_ME_THE_PRETTY_SPARKLING_THINGS_THEYRE_ALL_MINE ).getBuffer(), npc.getBroadcastRadius() )
                }

                return

            case KARUL_BUGBEAR:
                if ( _.random( 9 ) < 2
                        && QuestHelper.hasQuestItem( player, FIRST_CIRCLE_REQUEST_3A )
                        && !QuestHelper.hasQuestItem( player, BOOK_OF_SHUNAIMAN ) ) {
                    QuestHelper.addGenericSpawn( null, KARUL_CHIEF_OROOTO, npc.getX(), npc.getY(), npc.getZ() + 10, npc.getHeading(), false, 0, true )
                }

                return

            case TAIK_ORC_CAPTAIN:
                if ( _.random( 9 ) < 2
                        && QuestHelper.hasQuestItem( player, SECOND_CIRCLE_REQUEST_2B )
                        && !QuestHelper.hasQuestItem( player, KAKRANS_HEAD ) ) {
                    QuestHelper.addGenericSpawn( null, TAIK_OVERLORD_KAKRAN, npc.getX(), npc.getY(), npc.getZ() + 10, npc.getHeading(), false, 0, true )
                }

                return

            case MIRROR:
                if ( _.random( 19 ) < 2
                        && QuestHelper.hasQuestItem( player, SECOND_CIRCLE_REQUEST_3B )
                        && QuestHelper.getQuestItemsCount( player, NARCISSUSS_SOULSTONE ) < 40 ) {
                    QuestHelper.addGenericSpawn( null, GREMLIN_FILCHER, npc.getX(), npc.getY(), npc.getZ() + 10, npc.getHeading(), false, 0, true )
                    BroadcastHelper.dataInLocation( npc, CreatureSay.fromNpcName( data.targetId, NpcSayType.NpcAll, npc.getName(), NpcStringIds.SHOW_ME_THE_PRETTY_SPARKLING_THINGS_THEYRE_ALL_MINE ).getBuffer(), npc.getBroadcastRadius() )
                }

                return

            case HATAR_RATMAN_THIEF:
                if ( _.random( 19 ) < 2
                        && QuestHelper.hasQuestItem( player, SECOND_CIRCLE_REQUEST_5C )
                        && QuestHelper.getQuestItemsCount( player, COIN_OF_OLD_EMPIRE ) < 20 ) {
                    QuestHelper.addGenericSpawn( null, GREMLIN_FILCHER, npc.getX(), npc.getY(), npc.getZ() + 10, npc.getHeading(), false, 0, true )
                    BroadcastHelper.dataInLocation( npc, CreatureSay.fromNpcName( data.targetId, NpcSayType.NpcAll, npc.getName(), NpcStringIds.SHOW_ME_THE_PRETTY_SPARKLING_THINGS_THEYRE_ALL_MINE ).getBuffer(), npc.getBroadcastRadius() )
                }

                return

            case HATAR_RATMAN_BOSS:
                if ( _.random( 19 ) < 2
                        && QuestHelper.hasQuestItem( player, SECOND_CIRCLE_REQUEST_5C )
                        && QuestHelper.getQuestItemsCount( player, COIN_OF_OLD_EMPIRE ) < 20 ) {
                    QuestHelper.addGenericSpawn( null, GREMLIN_FILCHER, npc.getX(), npc.getY(), npc.getZ() + 10, npc.getHeading(), false, 0, true )
                    BroadcastHelper.dataInLocation( npc, CreatureSay.fromNpcName( data.targetId, NpcSayType.NpcAll, npc.getName(), NpcStringIds.SHOW_ME_THE_PRETTY_SPARKLING_THINGS_THEYRE_ALL_MINE ).getBuffer(), npc.getBroadcastRadius() )
                }

                if ( _.random( 9 ) < 2
                        && QuestHelper.hasQuestItem( player, SECOND_CIRCLE_REQUEST_2A )
                        && !QuestHelper.hasQuestItem( player, BUST_OF_TRAVIS ) ) {
                    QuestHelper.addGenericSpawn( null, HATAR_CHIEFTAIN_KUBEL, npc.getX(), npc.getY(), npc.getZ() + 10, npc.getHeading(), false, 0, true )
                }

                return

            case VANOR_SILENOS_CHIEFTAIN:
                if ( _.random( 9 ) < 2
                        && QuestHelper.hasQuestItem( player, SECOND_CIRCLE_REQUEST_6B )
                        && !QuestHelper.hasQuestItem( player, KERUNOSS_GOLD_MANE ) ) {
                    QuestHelper.addGenericSpawn( null, VANOR_ELDER_KERUNOS, npc.getX(), npc.getY(), npc.getZ() + 10, npc.getHeading(), false, 0, true )
                }

                return

            case GREMLIN_FILCHER:
                if ( QuestHelper.hasQuestItem( player, SECOND_CIRCLE_REQUEST_7C )
                        && QuestHelper.getQuestItemsCount( player, TEMPEST_SHARD ) < 40 ) {
                    await this.rewardPlayer( player, TEMPEST_SHARD, 5, 40, data.isChampion )
                    BroadcastHelper.dataInLocation( npc, CreatureSay.fromNpcName( data.targetId, NpcSayType.NpcAll, npc.getName(), NpcStringIds.PRETTY_GOOD ).getBuffer(), npc.getBroadcastRadius() )
                }

                if ( QuestHelper.hasQuestItem( player, SECOND_CIRCLE_REQUEST_8C )
                        && QuestHelper.getQuestItemsCount( player, TSUNAMI_SHARD ) < 40 ) {
                    await this.rewardPlayer( player, TSUNAMI_SHARD, 5, 40, data.isChampion )
                    BroadcastHelper.dataInLocation( npc, CreatureSay.fromNpcName( data.targetId, NpcSayType.NpcAll, npc.getName(), NpcStringIds.PRETTY_GOOD ).getBuffer(), npc.getBroadcastRadius() )
                }

                if ( QuestHelper.hasQuestItem( player, SECOND_CIRCLE_REQUEST_10C )
                        && QuestHelper.getQuestItemsCount( player, HAMADRYAD_SHARD ) < 40 ) {
                    await this.rewardPlayer( player, HAMADRYAD_SHARD, 5, 40, data.isChampion )
                    BroadcastHelper.dataInLocation( npc, CreatureSay.fromNpcName( data.targetId, NpcSayType.NpcAll, npc.getName(), NpcStringIds.PRETTY_GOOD ).getBuffer(), npc.getBroadcastRadius() )
                }

                if ( QuestHelper.hasQuestItem( player, SECOND_CIRCLE_REQUEST_3B )
                        && QuestHelper.getQuestItemsCount( player, NARCISSUSS_SOULSTONE ) < 40 ) {
                    await this.rewardPlayer( player, NARCISSUSS_SOULSTONE, 5, 40, data.isChampion )
                    BroadcastHelper.dataInLocation( npc, CreatureSay.fromNpcName( data.targetId, NpcSayType.NpcAll, npc.getName(), NpcStringIds.PRETTY_GOOD ).getBuffer(), npc.getBroadcastRadius() )
                }

                if ( QuestHelper.hasQuestItem( player, SECOND_CIRCLE_REQUEST_5C )
                        && QuestHelper.getQuestItemsCount( player, COIN_OF_OLD_EMPIRE ) < 20 ) {
                    await this.rewardPlayer( player, COIN_OF_OLD_EMPIRE, 3, 20, data.isChampion )
                    BroadcastHelper.dataInLocation( npc, CreatureSay.fromNpcName( data.targetId, NpcSayType.NpcAll, npc.getName(), NpcStringIds.PRETTY_GOOD ).getBuffer(), npc.getBroadcastRadius() )
                }

                return

            case GOK_MAGOK:
                if ( Math.random() < 0.5
                        && QuestHelper.hasQuestItem( player, FIRST_CIRCLE_REQUEST_2A )
                        && !QuestHelper.hasQuestItem( player, TITANS_TABLET ) ) {
                    QuestHelper.addGenericSpawn( null, BLACK_LEGION_STORMTROOPER, npc.getX(), npc.getY(), npc.getZ() + 10, npc.getHeading(), false, 0, true )
                    QuestHelper.addGenericSpawn( null, BLACK_LEGION_STORMTROOPER, npc.getX(), npc.getY(), npc.getZ() + 10, npc.getHeading(), false, 0, true )
                    BroadcastHelper.dataInLocation( npc, CreatureSay.fromNpcName( data.targetId, NpcSayType.NpcAll, npc.getName(), NpcStringIds.WELL_TAKE_THE_PROPERTY_OF_THE_ANCIENT_EMPIRE ).getBuffer(), npc.getBroadcastRadius() )
                }

                return
            case HATAR_CHIEFTAIN_KUBEL:
                if ( Math.random() < 0.5
                        && QuestHelper.hasQuestItem( player, SECOND_CIRCLE_REQUEST_2A )
                        && !QuestHelper.hasQuestItem( player, BUST_OF_TRAVIS ) ) {
                    QuestHelper.addGenericSpawn( null, BLACK_LEGION_STORMTROOPER, npc.getX(), npc.getY(), npc.getZ() + 10, npc.getHeading(), false, 0, true )
                    QuestHelper.addGenericSpawn( null, BLACK_LEGION_STORMTROOPER, npc.getX(), npc.getY(), npc.getZ() + 10, npc.getHeading(), false, 0, true )
                    BroadcastHelper.dataInLocation( npc, CreatureSay.fromNpcName( data.targetId, NpcSayType.NpcAll, npc.getName(), NpcStringIds.WELL_TAKE_THE_PROPERTY_OF_THE_ANCIENT_EMPIRE ).getBuffer(), npc.getBroadcastRadius() )
                }

                return

            case KARUL_CHIEF_OROOTO:
                if ( _.random( 9 ) < 2
                        && QuestHelper.hasQuestItem( player, FIRST_CIRCLE_REQUEST_3A )
                        && !QuestHelper.hasQuestItem( player, BOOK_OF_SHUNAIMAN ) ) {
                    QuestHelper.addGenericSpawn( null, BLACK_LEGION_STORMTROOPER, npc.getX(), npc.getY(), npc.getZ() + 10, npc.getHeading(), false, 0, true )
                    QuestHelper.addGenericSpawn( null, BLACK_LEGION_STORMTROOPER, npc.getX(), npc.getY(), npc.getZ() + 10, npc.getHeading(), false, 0, true )
                    BroadcastHelper.dataInLocation( npc, CreatureSay.fromNpcName( data.targetId, NpcSayType.NpcAll, npc.getName(), NpcStringIds.WELL_TAKE_THE_PROPERTY_OF_THE_ANCIENT_EMPIRE ).getBuffer(), npc.getBroadcastRadius() )
                }

                return

            case LETO_LIZARDMAN_ARCHER:
            case LETO_LIZARDMAN_SOLDIER:
            case HARIT_LIZARDMAN_GRUNT:
            case HARIT_LIZARDMAN_ARCHER:
            case HARIT_LIZARDMAN_WARRIOR:
                await this.evolveBloodCrystal( player, data.isChampion )
                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30744-03.htm':
                state.startQuest()
                if ( !QuestHelper.hasQuestItem( player, TEST_INSTRUCTIONS_1 ) ) {
                    await QuestHelper.giveSingleItem( player, TEST_INSTRUCTIONS_1, 1 )
                }

                state.setMemoState( 0 )
                break

            case '30744-04.html':
            case '30744-04a.html':
            case '30744-04b.html':
            case '30744-04c.html':
            case '30744-04d.html':
            case '30744-04e.html':
            case '30744-04f.html':
            case '30744-07.html':
            case '30744-07a.html':
            case '30744-07b.html':
            case '30744-08.html':
            case '30744-08a.html':
            case '30744-10.html':
            case '30744-10a.html':
            case '30744-10b.html':
            case '30744-10c.html':
            case '30744-10d.html':
            case '30744-10e.html':
            case '30744-10f.html':
            case '30744-14.html':
            case '30744-14a.html':
            case '30744-15.html':
            case '30744-18.html':
            case '30745-09.html':
            case '30746-03a.html':
            case '30746-07.html':
            case '30745-04.html':
            case '30745-05a.html':
            case '30745-05c.html':
                break

            case '30744-09.html':
                if ( !QuestHelper.hasQuestItems( player,
                        FIRST_CIRCLE_REQUEST_1C,
                        FIRST_CIRCLE_REQUEST_2C,
                        FIRST_CIRCLE_REQUEST_3C,
                        FIRST_CIRCLE_REQUEST_4C,
                        FIRST_CIRCLE_REQUEST_4C,
                        FIRST_CIRCLE_REQUEST_6C,
                        FIRST_CIRCLE_REQUEST_7C,
                        FIRST_CIRCLE_REQUEST_8C,
                        FIRST_CIRCLE_REQUEST_9C,
                        FIRST_CIRCLE_REQUEST_10C,
                        FIRST_CIRCLE_REQUEST_11C,
                        FIRST_CIRCLE_REQUEST_12C,
                        FIRST_CIRCLE_REQUEST_1B,
                        FIRST_CIRCLE_REQUEST_2B,
                        FIRST_CIRCLE_REQUEST_3B,
                        FIRST_CIRCLE_REQUEST_4B,
                        FIRST_CIRCLE_REQUEST_5B,
                        FIRST_CIRCLE_REQUEST_6B,
                        FIRST_CIRCLE_REQUEST_1A,
                        FIRST_CIRCLE_REQUEST_2A,
                        FIRST_CIRCLE_REQUEST_3A ) ) {
                    await QuestHelper.giveSingleItem( player, TEST_INSTRUCTIONS_2, 1 )
                    break
                }

                return this.getPath( '30744-09a.html' )

            case '30744-16.html':
                await state.exitQuest( true, true )
                await QuestHelper.giveAdena( player, 20000, true )

                return this.getPath( '30744-17.html' )

            case '30745-02.html':
                if ( QuestHelper.hasQuestItem( player, TEST_INSTRUCTIONS_2 ) ) {
                    return this.getPath( '30745-03.html' )
                }

                break

            case 'LIST_1':
                return this.computeFirstListHtml( state, QuestHelper.getQuestItemsCount( player, LAUREL_LEAF_PIN ) )

            case 'LIST_2':
                return this.computeSecondListHtml( state, QuestHelper.getQuestItemsCount( player, LAUREL_LEAF_PIN ) )

            case '30746-03.html':
                if ( !QuestHelper.hasQuestItem( player, CYBELLINS_DAGGER ) ) {
                    await QuestHelper.giveSingleItem( player, CYBELLINS_DAGGER, 1 )
                }

                if ( !QuestHelper.hasQuestItem( player, CYBELLINS_REQUEST ) ) {
                    await QuestHelper.giveSingleItem( player, CYBELLINS_REQUEST, 1 )
                }

                await QuestHelper.giveSingleItem( player, BLOOD_CRYSTAL_PURITY_1, 1 )

                if ( QuestHelper.hasQuestItem( player, BROKEN_BLOOD_CRYSTAL ) ) {
                    await QuestHelper.takeSingleItem( player, BROKEN_BLOOD_CRYSTAL, -1 )
                }

                break

            case '30746-06.html':
                let index = _.findIndex( purityCrystalItems, ( itemId: number ): boolean => {
                    return QuestHelper.hasQuestItem( player, itemId )
                } )

                await QuestHelper.giveAdena( player, 3400 * Math.pow( 2, index ), true )
                await QuestHelper.takeMultipleItems( player, -1,
                        BLOOD_CRYSTAL_PURITY_2,
                        BLOOD_CRYSTAL_PURITY_3,
                        BLOOD_CRYSTAL_PURITY_4,
                        BLOOD_CRYSTAL_PURITY_5,
                        BLOOD_CRYSTAL_PURITY_6,
                        BLOOD_CRYSTAL_PURITY_7,
                        BLOOD_CRYSTAL_PURITY_8,
                        BLOOD_CRYSTAL_PURITY_9,
                        BLOOD_CRYSTAL_PURITY_10 )
                break

            case '30746-10.html':
                await QuestHelper.takeMultipleItems( player, -1, BLOOD_CRYSTAL_PURITY_1, CYBELLINS_DAGGER, CYBELLINS_REQUEST )
                break

            case '30745-05b.html':
                if ( QuestHelper.hasQuestItem( player, LAUREL_LEAF_PIN ) ) {
                    await QuestHelper.takeSingleItem( player, LAUREL_LEAF_PIN, 1 )
                }

                await QuestHelper.takeMultipleItems( player,
                        -1,
                        FIRST_CIRCLE_REQUEST_1C,
                        FIRST_CIRCLE_REQUEST_2C,
                        FIRST_CIRCLE_REQUEST_3C,
                        FIRST_CIRCLE_REQUEST_4C,
                        FIRST_CIRCLE_REQUEST_5C,
                        FIRST_CIRCLE_REQUEST_6C,
                        FIRST_CIRCLE_REQUEST_7C,
                        FIRST_CIRCLE_REQUEST_8C,
                        FIRST_CIRCLE_REQUEST_9C,
                        FIRST_CIRCLE_REQUEST_10C,
                        FIRST_CIRCLE_REQUEST_11C,
                        FIRST_CIRCLE_REQUEST_12C,
                        FIRST_CIRCLE_REQUEST_1B,
                        FIRST_CIRCLE_REQUEST_2B,
                        FIRST_CIRCLE_REQUEST_3B,
                        FIRST_CIRCLE_REQUEST_4B,
                        FIRST_CIRCLE_REQUEST_5B,
                        FIRST_CIRCLE_REQUEST_6B,
                        FIRST_CIRCLE_REQUEST_1A,
                        FIRST_CIRCLE_REQUEST_2A,
                        FIRST_CIRCLE_REQUEST_3A,
                        SECOND_CIRCLE_REQUEST_1C,
                        SECOND_CIRCLE_REQUEST_2C,
                        SECOND_CIRCLE_REQUEST_3C,
                        SECOND_CIRCLE_REQUEST_4C,
                        SECOND_CIRCLE_REQUEST_5C,
                        SECOND_CIRCLE_REQUEST_6C,
                        SECOND_CIRCLE_REQUEST_7C,
                        SECOND_CIRCLE_REQUEST_8C,
                        SECOND_CIRCLE_REQUEST_9C,
                        SECOND_CIRCLE_REQUEST_10C,
                        SECOND_CIRCLE_REQUEST_11C,
                        SECOND_CIRCLE_REQUEST_12C,
                        SECOND_CIRCLE_REQUEST_1B,
                        SECOND_CIRCLE_REQUEST_2B,
                        SECOND_CIRCLE_REQUEST_3B,
                        SECOND_CIRCLE_REQUEST_4B,
                        SECOND_CIRCLE_REQUEST_5B,
                        SECOND_CIRCLE_REQUEST_6B,
                        SECOND_CIRCLE_REQUEST_1A,
                        SECOND_CIRCLE_REQUEST_2A,
                        SECOND_CIRCLE_REQUEST_3A,
                        CHARM_OF_KADESH,
                        TIMAK_JADE_NECKLACE,
                        ENCHANTED_GOLEM_SHARD,
                        GIANT_MONSTER_EYE_MEAT,
                        DIRE_WYRM_EGG,
                        GUARDIAN_BASILISK_TALON,
                        REVENANTS_CHAINS,
                        WINDSUS_TUSK,
                        GRANDISS_SKULL,
                        TAIK_OBSIDIAN_AMULET,
                        KARUL_BUGBEAR_HEAD,
                        TAMLIN_IVORY_CHARM,
                        FANG_OF_NARAK,
                        ENCHANTED_GARGOYLES_HORN,
                        COILED_SERPENT_TOTEM,
                        TOTEM_OF_KADESH,
                        KAIKIS_HEAD,
                        KRONBE_VENOM_SAC,
                        EVAS_CHARM,
                        TITANS_TABLET,
                        BOOK_OF_SHUNAIMAN,
                        ROTTING_TREE_SPORES,
                        TRISALIM_VENOM_SAC,
                        TAIK_ORC_TOTEM,
                        HARIT_BARBED_NECKLACE,
                        COIN_OF_OLD_EMPIRE,
                        SKIN_OF_FARCRAN,
                        TEMPEST_SHARD,
                        TSUNAMI_SHARD,
                        SATYR_MANE,
                        HAMADRYAD_SHARD,
                        VANOR_SILENOS_MANE,
                        TALK_BUGBEAR_TOTEM,
                        OKUNS_HEAD,
                        KAKRANS_HEAD,
                        NARCISSUSS_SOULSTONE,
                        DEPRIVE_EYE,
                        UNICORNS_HORN,
                        KERUNOSS_GOLD_MANE,
                        SKULL_OF_EXECUTED,
                        BUST_OF_TRAVIS,
                        SWORD_OF_CADMUS )
                break

            case '30745-10a.html':
                await QuestHelper.giveSingleItem( player, FIRST_CIRCLE_REQUEST_1C, 1 )
                break

            case '30745-10b.html':
                await QuestHelper.giveSingleItem( player, FIRST_CIRCLE_REQUEST_2C, 1 )
                break

            case '30745-10c.html':
                await QuestHelper.giveSingleItem( player, FIRST_CIRCLE_REQUEST_3C, 1 )
                break

            case '30745-10d.html':
                await QuestHelper.giveSingleItem( player, FIRST_CIRCLE_REQUEST_4C, 1 )
                break

            case '30745-10e.html':
                await QuestHelper.giveSingleItem( player, FIRST_CIRCLE_REQUEST_5C, 1 )
                break

            case '30745-10f.html':
                await QuestHelper.giveSingleItem( player, FIRST_CIRCLE_REQUEST_6C, 1 )
                break

            case '30745-10g.html':
                await QuestHelper.giveSingleItem( player, FIRST_CIRCLE_REQUEST_7C, 1 )
                break

            case '30745-10h.html':
                await QuestHelper.giveSingleItem( player, FIRST_CIRCLE_REQUEST_8C, 1 )
                break

            case '30745-10i.html':
                await QuestHelper.giveSingleItem( player, FIRST_CIRCLE_REQUEST_9C, 1 )
                break

            case '30745-10j.html':
                await QuestHelper.giveSingleItem( player, FIRST_CIRCLE_REQUEST_10C, 1 )
                break

            case '30745-10k.html':
                await QuestHelper.giveSingleItem( player, FIRST_CIRCLE_REQUEST_11C, 1 )
                break

            case '30745-10l.html':
                await QuestHelper.giveSingleItem( player, FIRST_CIRCLE_REQUEST_12C, 1 )
                break

            case '30745-11a.html':
                await QuestHelper.giveSingleItem( player, FIRST_CIRCLE_REQUEST_1B, 1 )
                break

            case '30745-11b.html':
                await QuestHelper.giveSingleItem( player, FIRST_CIRCLE_REQUEST_2B, 1 )
                break

            case '30745-11c.html':
                await QuestHelper.giveSingleItem( player, FIRST_CIRCLE_REQUEST_3B, 1 )
                break

            case '30745-11d.html':
                await QuestHelper.giveSingleItem( player, FIRST_CIRCLE_REQUEST_4B, 1 )
                break

            case '30745-11e.html':
                await QuestHelper.giveSingleItem( player, FIRST_CIRCLE_REQUEST_5B, 1 )
                break

            case '30745-11f.html':
                await QuestHelper.giveSingleItem( player, FIRST_CIRCLE_REQUEST_6B, 1 )
                break

            case '30745-12a.html':
                await QuestHelper.giveSingleItem( player, FIRST_CIRCLE_REQUEST_1A, 1 )
                break

            case '30745-12b.html':
                await QuestHelper.giveSingleItem( player, FIRST_CIRCLE_REQUEST_2A, 1 )
                break

            case '30745-12c.html':
                await QuestHelper.giveSingleItem( player, FIRST_CIRCLE_REQUEST_3A, 1 )
                break

            case '30745-13a.html':
                await QuestHelper.giveSingleItem( player, SECOND_CIRCLE_REQUEST_1C, 1 )
                break

            case '30745-13b.html':
                await QuestHelper.giveSingleItem( player, SECOND_CIRCLE_REQUEST_2C, 1 )
                break

            case '30745-13c.html':
                await QuestHelper.giveSingleItem( player, SECOND_CIRCLE_REQUEST_3C, 1 )
                break

            case '30745-13d.html':
                await QuestHelper.giveSingleItem( player, SECOND_CIRCLE_REQUEST_4C, 1 )
                break

            case '30745-13e.html':
                await QuestHelper.giveSingleItem( player, SECOND_CIRCLE_REQUEST_5C, 1 )
                break

            case '30745-13f.html':
                await QuestHelper.giveSingleItem( player, SECOND_CIRCLE_REQUEST_6C, 1 )
                break

            case '30745-13g.html':
                await QuestHelper.giveSingleItem( player, SECOND_CIRCLE_REQUEST_7C, 1 )
                break

            case '30745-13k.html':
                await QuestHelper.giveSingleItem( player, SECOND_CIRCLE_REQUEST_11C, 1 )
                break

            case '30745-13i.html':
                await QuestHelper.giveSingleItem( player, SECOND_CIRCLE_REQUEST_9C, 1 )
                break

            case '30745-13j.html':
                await QuestHelper.giveSingleItem( player, SECOND_CIRCLE_REQUEST_10C, 1 )
                break

            case '30745-13l.html':
                await QuestHelper.giveSingleItem( player, SECOND_CIRCLE_REQUEST_12C, 1 )
                break

            case '30745-14a.html':
                await QuestHelper.giveSingleItem( player, SECOND_CIRCLE_REQUEST_1B, 1 )
                break

            case '30745-14b.html':
                await QuestHelper.giveSingleItem( player, SECOND_CIRCLE_REQUEST_2B, 1 )
                break

            case '30745-14c.html':
                await QuestHelper.giveSingleItem( player, SECOND_CIRCLE_REQUEST_3B, 1 )
                break

            case '30745-14d.html':
                await QuestHelper.giveSingleItem( player, SECOND_CIRCLE_REQUEST_4B, 1 )
                break

            case '30745-14e.html':
                await QuestHelper.giveSingleItem( player, SECOND_CIRCLE_REQUEST_5B, 1 )
                break

            case '30745-14f.html':
                await QuestHelper.giveSingleItem( player, SECOND_CIRCLE_REQUEST_6B, 1 )
                break


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() < minimumLevel ? '30744-01.htm' : '30744-02.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case GREY:
                        if ( QuestHelper.hasQuestItem( player, TEST_INSTRUCTIONS_1 ) ) {
                            let successCount = 0

                            _.each( firstProgressionItemCheck, ( item: ProgressionItem ) => {
                                let [ itemId, requiredAmount ] = item
                                if ( QuestHelper.getQuestItemsCount( player, itemId ) >= requiredAmount ) {
                                    successCount++
                                }

                                if ( successCount >= 3 ) {
                                    return false
                                }
                            } )

                            if ( successCount < 3 && QuestHelper.hasQuestItem( player, WINDSUS_ALEPH_SKIN ) ) {
                                successCount++
                            }

                            if ( successCount < 3 && !QuestHelper.hasQuestItems( player, HAKAS_HEAD, JAKAS_HEAD, MARKAS_HEAD ) ) {
                                return this.getPath( '30744-05.html' )
                            }

                            state.setConditionWithSound( 2, true )
                            await QuestHelper.giveSingleItem( player, FIRST_CIRCLE_HUNTER_LICENSE, 1 )
                            await QuestHelper.takeMultipleItems( player, -1,
                                    GUARDIAN_BASILISK_SCALE,
                                    KARUT_WEED,
                                    HAKAS_HEAD,
                                    JAKAS_HEAD,
                                    MARKAS_HEAD,
                                    WINDSUS_ALEPH_SKIN,
                                    INDIGO_SPIRIT_ORE,
                                    SPORESEA_SEED,
                                    TEST_INSTRUCTIONS_1 )
                            return this.getPath( '30744-06.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, FIRST_CIRCLE_HUNTER_LICENSE ) ) {
                            if ( player.getLevel() < maximumLevel ) {
                                return this.getPath( '30744-07.html' )
                            }

                            if ( !QuestHelper.hasQuestItem( player, TEST_INSTRUCTIONS_2 ) ) {
                                return this.getPath( '30744-08.html' )
                            }
                        }

                        if ( QuestHelper.hasQuestItem( player, TEST_INSTRUCTIONS_2 ) ) {

                            let successCount = 0
                            _.each( secondProgressionItemCheck, ( item: ProgressionItem ) => {
                                let [ itemId, requiredAmount ] = item
                                if ( QuestHelper.getQuestItemsCount( player, itemId ) >= requiredAmount ) {
                                    successCount++
                                }

                                if ( successCount >= 3 ) {
                                    return false
                                }
                            } )

                            if ( successCount < 3 && !QuestHelper.hasQuestItems( player, ATHUS_HEAD, LANKAS_HEAD, TRISKAS_HEAD, MOTURAS_HEAD, KALATHS_HEAD ) ) {
                                return this.getPath( '30744-11.html' )
                            }

                            state.setConditionWithSound( 3, true )
                            await QuestHelper.giveSingleItem( player, SECOND_CIRCLE_HUNTER_LICENSE, 1 )
                            await QuestHelper.takeMultipleItems( player, -1,
                                    TRISALIM_SILK,
                                    TIMAK_ORC_TOTEM,
                                    AMBROSIUS_FRUIT,
                                    BALEFIRE_CRYSTAL,
                                    IMPERIAL_ARROWHEAD,
                                    ATHUS_HEAD,
                                    LANKAS_HEAD,
                                    TRISKAS_HEAD,
                                    MOTURAS_HEAD,
                                    KALATHS_HEAD,
                                    TEST_INSTRUCTIONS_2,
                                    FIRST_CIRCLE_HUNTER_LICENSE,
                            )

                            return this.getPath( '30744-12.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, SECOND_CIRCLE_HUNTER_LICENSE ) ) {
                            return this.getPath( '30744-14.html' )
                        }

                        break

                    case CYBELLIN:
                        if ( !QuestHelper.hasQuestItems( player, SECOND_CIRCLE_HUNTER_LICENSE, FIRST_CIRCLE_HUNTER_LICENSE ) ) {
                            return this.getPath( '30746-01.html' )
                        }

                        if ( !QuestHelper.hasQuestItem( player, CYBELLINS_REQUEST ) ) {
                            return this.getPath( '30746-02.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, BLOOD_CRYSTAL_PURITY_1 ) ) {
                            return this.getPath( '30746-04.html' )
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem(
                                player,
                                BLOOD_CRYSTAL_PURITY_2,
                                BLOOD_CRYSTAL_PURITY_3,
                                BLOOD_CRYSTAL_PURITY_4,
                                BLOOD_CRYSTAL_PURITY_5,
                                BLOOD_CRYSTAL_PURITY_6,
                                BLOOD_CRYSTAL_PURITY_7,
                                BLOOD_CRYSTAL_PURITY_8,
                                BLOOD_CRYSTAL_PURITY_9 ) ) {
                            return this.getPath( '30746-05.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, BLOOD_CRYSTAL_PURITY_10 ) ) {
                            await QuestHelper.giveAdena( player, 870400, true )
                            await QuestHelper.takeSingleItem( player, BLOOD_CRYSTAL_PURITY_10, -1 )

                            return this.getPath( '30746-05a.html' )
                        }

                        if ( !QuestHelper.hasQuestItem( player, BROKEN_BLOOD_CRYSTAL ) ) {
                            return this.getPath( '30746-08.html' )
                        }

                        await QuestHelper.takeSingleItem( player, BROKEN_BLOOD_CRYSTAL, -1 )
                        return this.getPath( '30746-09.html' )

                    case TOR:
                        if ( QuestHelper.hasQuestItem( player, FIRST_CIRCLE_HUNTER_LICENSE ) ) {
                            if ( !this.hasAnyRequestedItems( player ) ) {
                                if ( player.getLevel() < maximumLevel ) {
                                    return this.getPath( '30745-01b.html' )
                                }

                                if ( QuestHelper.hasQuestItem( player, SECOND_CIRCLE_HUNTER_LICENSE ) ) {
                                    return this.getPath( '30745-03.html' )
                                }

                                return this.getPath( '30745-03a.html' )
                            }

                            return this.rewardWithPrecondition( player, state )
                        }

                        if ( QuestHelper.hasQuestItem( player, SECOND_CIRCLE_HUNTER_LICENSE ) ) {
                            if ( !this.hasAnyRequestedItems( player ) ) {
                                return this.getPath( '30745-03b.html' )
                            }

                            return this.rewardWithPrecondition( player, state )
                        }

                        return this.getPath( '30745-01a.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async rewardMonsterItem( player: L2PcInstance, items: Array<MonsterReward>, npcId: number, isFromChampion: boolean ): Promise<void> {
        await aigle.resolve( items ).each( async ( item: MonsterReward ) => {
            let [ requiredItemId, givenItemId, minimumAmount, maximumAmount, chance ] = item

            if ( npcId === GUARDIAN_BASILISK && requiredItemId === FIRST_CIRCLE_REQUEST_6C ) {
                minimumAmount = _.random( 100 ) < QuestHelper.getAdjustedChance( givenItemId, 60, isFromChampion ) ? 2 : 1
            }

            if ( !QuestHelper.hasQuestItem( player, requiredItemId )
                    || QuestHelper.getQuestItemsCount( player, givenItemId ) >= maximumAmount
                    || _.random( 100 ) > QuestHelper.getAdjustedChance( givenItemId, chance, isFromChampion ) ) {
                return
            }

            await QuestHelper.rewardSingleQuestItem( player, givenItemId, _.random( minimumAmount, maximumAmount ), isFromChampion )

            if ( QuestHelper.getQuestItemsCount( player, givenItemId ) >= maximumAmount ) {
                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
            }
        } )
    }

    async rewardPlayer( player: L2PcInstance, itemId: number, amount: number, maximumAmount: number, isFromChampion: boolean ): Promise<void> {
        await QuestHelper.rewardSingleQuestItem( player, itemId, amount, isFromChampion )

        if ( QuestHelper.getQuestItemsCount( player, itemId ) >= maximumAmount ) {
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
        }
    }

    async rewardWithPrecondition( player: L2PcInstance, state: QuestState ): Promise<string> {
        let reward = _.find( preconditionRewards, ( item: PlayerRewardParameters ): boolean => {
            let [ requiredItemId ] = item
            return QuestHelper.hasQuestItem( player, requiredItemId )
        } )

        if ( !reward ) {
            return
        }

        let [ requiredItemId, countedItemId, amountNeeded, adenaReward ] = reward
        if ( QuestHelper.getQuestItemsCount( player, countedItemId ) < amountNeeded ) {
            return this.getPath( '30745-05.html' )
        }

        await QuestHelper.giveSingleItem( player, LAUREL_LEAF_PIN, 1 )
        await QuestHelper.giveAdena( player, adenaReward, true )
        await QuestHelper.takeSingleItem( player, requiredItemId, -1 )

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
        state.setMemoState( 0 )

        let removeAmount = countedItemId === GIANT_MONSTER_EYE_MEAT ? amountNeeded : -1
        await QuestHelper.takeSingleItem( player, countedItemId, removeAmount )

        return this.getPath( QuestHelper.hasQuestItem( player, FIRST_CIRCLE_HUNTER_LICENSE ) ? '30745-06a.html' : '30745-06b.html' )
    }

    hasAnyRequestedItems( player : L2PcInstance ) : boolean {
        return QuestHelper.hasAtLeastOneQuestItem( player,
                FIRST_CIRCLE_REQUEST_1C,
                FIRST_CIRCLE_REQUEST_2C,
                FIRST_CIRCLE_REQUEST_3C,
                FIRST_CIRCLE_REQUEST_4C,
                FIRST_CIRCLE_REQUEST_5C,
                FIRST_CIRCLE_REQUEST_6C,
                FIRST_CIRCLE_REQUEST_7C,
                FIRST_CIRCLE_REQUEST_8C,
                FIRST_CIRCLE_REQUEST_9C,
                FIRST_CIRCLE_REQUEST_10C,
                FIRST_CIRCLE_REQUEST_11C,
                FIRST_CIRCLE_REQUEST_12C,
                FIRST_CIRCLE_REQUEST_1B,
                FIRST_CIRCLE_REQUEST_2B,
                FIRST_CIRCLE_REQUEST_3B,
                FIRST_CIRCLE_REQUEST_4B,
                FIRST_CIRCLE_REQUEST_5B,
                FIRST_CIRCLE_REQUEST_6B,
                FIRST_CIRCLE_REQUEST_1A,
                FIRST_CIRCLE_REQUEST_2A,
                FIRST_CIRCLE_REQUEST_3A,
                SECOND_CIRCLE_REQUEST_1C,
                SECOND_CIRCLE_REQUEST_2C,
                SECOND_CIRCLE_REQUEST_3C,
                SECOND_CIRCLE_REQUEST_4C,
                SECOND_CIRCLE_REQUEST_5C,
                SECOND_CIRCLE_REQUEST_6C,
                SECOND_CIRCLE_REQUEST_7C,
                SECOND_CIRCLE_REQUEST_8C,
                SECOND_CIRCLE_REQUEST_9C,
                SECOND_CIRCLE_REQUEST_10C,
                SECOND_CIRCLE_REQUEST_11C,
                SECOND_CIRCLE_REQUEST_12C,
                SECOND_CIRCLE_REQUEST_1B,
                SECOND_CIRCLE_REQUEST_2B,
                SECOND_CIRCLE_REQUEST_3B,
                SECOND_CIRCLE_REQUEST_4B,
                SECOND_CIRCLE_REQUEST_5B,
                SECOND_CIRCLE_REQUEST_6B,
                SECOND_CIRCLE_REQUEST_1A,
                SECOND_CIRCLE_REQUEST_2A,
                SECOND_CIRCLE_REQUEST_3A )
    }
}