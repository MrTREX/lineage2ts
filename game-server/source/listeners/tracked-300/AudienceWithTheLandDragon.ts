import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import {
    AttackableAttackedEvent,
    AttackableKillEvent,
    NpcGeneralEvent,
    NpcTalkEvent,
} from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const WAREHOUSE_CHIEF_MOKE = 30498
const BLACKSMITH_HELTON = 30678
const PREFECT_CHAKIRIS = 30705
const MAGISTER_KAIENA = 30720
const GABRIELLE = 30753
const ANTHARAS_WATCHMAN_GILMORE = 30754
const ANTHARAS_WATCHMAN_THEODRIC = 30755
const MASTER_KENDRA = 30851
const HIGH_PRIEST_ORVEN = 30857
const FEATHER_OF_GABRIELLE = 3852
const MARSH_STALKER_HORN = 3853
const MARSH_DRAKE_TALONS = 3854
const KRANROT_SKIN = 3855
const HAMRUT_LEG = 3856
const REMAINS_OF_SACRAFICE = 3857
const TOTEM_OF_LAND_DRAGON = 3858
const FRAGMENT_OF_ABYSS_JEWEL_1ST = 3859
const FRAGMENT_OF_ABYSS_JEWEL_2ND = 3860
const FRAGMENT_OF_ABYSS_JEWEL_3RD = 3861
const MARA_FANG = 3862
const MUSFEL_FANG = 3863
const MARK_OF_WATCHMAN = 3864
const HERALD_OF_SLAYER = 3890
const PORTAL_STONE = 3865
const BLOOD_QUEEN = 18001
const CAVE_MAIDEN = 20134
const CAVE_KEEPER = 20246
const CAVE_KEEPER_HOLD = 20277
const CAVE_MAIDEN_HOLD = 20287
const HARIT_LIZARDMAN_SHAMAN = 20644
const HARIT_LIZARDMAN_MATRIARCH = 20645
const HAMRUT = 20649
const KRANROT = 20650
const MARSH_STALKER = 20679
const MARSH_DRAKE = 20680
const ABYSSAL_JEWEL_1 = 27165
const ABYSSAL_JEWEL_2 = 27166
const ABYSSAL_JEWEL_3 = 27167
const JEWEL_GUARDIAN_MARA = 27168
const JEWEL_GUARDIAN_MUSFEL = 27169
const JEWEL_GUARDIAN_PYTON = 27170
const GHOST_OF_OFFERING = 27171
const HARIT_LIZARDMAN_ZEALOT = 27172
const minimumLevel = 50

const eventNames = {
    despawnOne: 'do',
    despawnTwo: 'dt',
}

export class AudienceWithTheLandDragon extends ListenerLogic {
    constructor() {
        super( 'Q00337_AudienceWithTheLandDragon',
                'listeners/tracked-300/AudienceWithTheLandDragon.ts' )
        this.questId = 337
        this.questItemIds = [
            FEATHER_OF_GABRIELLE,
            MARSH_STALKER_HORN,
            MARSH_DRAKE_TALONS,
            KRANROT_SKIN,
            HAMRUT_LEG,
            REMAINS_OF_SACRAFICE,
            TOTEM_OF_LAND_DRAGON,
            FRAGMENT_OF_ABYSS_JEWEL_1ST,
            FRAGMENT_OF_ABYSS_JEWEL_2ND,
            FRAGMENT_OF_ABYSS_JEWEL_3RD,
            MARA_FANG,
            MUSFEL_FANG,
            MARK_OF_WATCHMAN,
            HERALD_OF_SLAYER,
        ]
    }

    getAttackableAttackIds(): Array<number> {
        return [
            ABYSSAL_JEWEL_1,
            ABYSSAL_JEWEL_2,
            ABYSSAL_JEWEL_3,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            BLOOD_QUEEN,
            CAVE_MAIDEN,
            CAVE_KEEPER,
            CAVE_KEEPER_HOLD,
            CAVE_MAIDEN_HOLD,
            HARIT_LIZARDMAN_SHAMAN,
            HARIT_LIZARDMAN_MATRIARCH,
            HAMRUT,
            KRANROT,
            MARSH_STALKER,
            MARSH_DRAKE,
            ABYSSAL_JEWEL_1,
            ABYSSAL_JEWEL_2,
            ABYSSAL_JEWEL_3,
            JEWEL_GUARDIAN_MARA,
            JEWEL_GUARDIAN_MUSFEL,
            JEWEL_GUARDIAN_PYTON,
            GHOST_OF_OFFERING,
            HARIT_LIZARDMAN_ZEALOT,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00337_AudienceWithTheLandDragon'
    }

    getQuestStartIds(): Array<number> {
        return [ GABRIELLE ]
    }

    getTalkIds(): Array<number> {
        return [
            GABRIELLE,
            WAREHOUSE_CHIEF_MOKE,
            BLACKSMITH_HELTON,
            PREFECT_CHAKIRIS,
            MAGISTER_KAIENA,
            ANTHARAS_WATCHMAN_GILMORE,
            ANTHARAS_WATCHMAN_THEODRIC,
            MASTER_KENDRA,
            HIGH_PRIEST_ORVEN,
        ]
    }

    async onAttackableAttackedEvent( data: AttackableAttackedEvent ): Promise<void> {
        let state: QuestState = QuestStateCache.getQuestState( data.attackerPlayerId, this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        let npc = L2World.getObjectById( data.targetId ) as L2Npc
        let player = L2World.getPlayer( data.attackerPlayerId )

        switch ( data.targetNpcId ) {
            case ABYSSAL_JEWEL_1:
                if ( state.isMemoState( 40000 ) || state.isMemoState( 40001 ) ) {
                    await this.onJewelAttacked( player, npc, FRAGMENT_OF_ABYSS_JEWEL_1ST, 20, JEWEL_GUARDIAN_MARA )
                }

                if ( npc.getCurrentHp() < npc.getMaxHp() * 0.1 ) {
                    await npc.deleteMe()
                }

                return

            case ABYSSAL_JEWEL_2:
                if ( state.isMemoState( 40000 ) || state.isMemoState( 40010 ) ) {
                    await this.onJewelAttacked( player, npc, FRAGMENT_OF_ABYSS_JEWEL_2ND, 20, JEWEL_GUARDIAN_MUSFEL )
                }

                if ( npc.getCurrentHp() < npc.getMaxHp() * 0.1 ) {
                    await npc.deleteMe()
                }

                return

            case ABYSSAL_JEWEL_3:
                if ( state.isMemoState( 70000 ) ) {
                    await this.onJewelAttacked( player, npc, FRAGMENT_OF_ABYSS_JEWEL_3RD, 4, JEWEL_GUARDIAN_PYTON )
                }

                if ( npc.getCurrentHp() < npc.getMaxHp() * 0.1 ) {
                    await npc.deleteMe()
                }

                return
        }
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case BLOOD_QUEEN:
                switch ( state.getMemoState() ) {
                    case 21011:
                    case 21010:
                    case 21001:
                    case 21000:
                    case 20011:
                    case 20010:
                    case 20001:
                    case 20000:
                        if ( !QuestHelper.hasQuestItem( player, REMAINS_OF_SACRAFICE ) ) {
                            _.times( 8, () => {
                                QuestHelper.addSpawnAtLocation( GHOST_OF_OFFERING, npc, true, 180000 )
                            } )
                        }

                        return
                }

                return

            case CAVE_MAIDEN:
            case CAVE_KEEPER:
            case CAVE_KEEPER_HOLD:
            case CAVE_MAIDEN_HOLD:
                if ( state.isMemoState( 70000 )
                        && !QuestHelper.hasQuestItem( player, FRAGMENT_OF_ABYSS_JEWEL_3RD )
                        && _.random( 4 ) === 0 ) {
                    QuestHelper.addSpawnAtLocation( ABYSSAL_JEWEL_3, npc, true, 180000 )
                }

                return

            case HARIT_LIZARDMAN_SHAMAN:
                switch ( state.getMemoState() ) {
                    case 21110:
                    case 21100:
                    case 21010:
                    case 21000:
                    case 20110:
                    case 20100:
                    case 20010:
                    case 20000:
                        if ( !QuestHelper.hasQuestItem( player, TOTEM_OF_LAND_DRAGON ) ) {
                            _.times( 3, () => {
                                QuestHelper.addAttackDesire( QuestHelper.addSpawnAtLocation( HARIT_LIZARDMAN_ZEALOT, npc, true, 180000 ), player )
                            } )
                        }

                        return
                }

                return

            case HARIT_LIZARDMAN_MATRIARCH:
                switch ( state.getMemoState() ) {
                    case 21110:
                    case 21100:
                    case 21010:
                    case 21000:
                    case 20110:
                    case 20100:
                    case 20010:
                    case 20000:
                        if ( !QuestHelper.hasQuestItem( player, TOTEM_OF_LAND_DRAGON ) && _.random( 4 ) === 0 ) {
                            _.times( 3, () => {
                                QuestHelper.addAttackDesire( QuestHelper.addSpawnAtLocation( HARIT_LIZARDMAN_ZEALOT, npc, true, 180000 ), player )
                            } )
                        }

                        return
                }

                return

            case HAMRUT:
                switch ( state.getMemoState() ) {
                    case 21101:
                    case 21100:
                    case 21001:
                    case 21000:
                    case 20101:
                    case 20100:
                    case 20001:
                    case 20000:
                        if ( !QuestHelper.hasQuestItem( player, HAMRUT_LEG ) ) {
                            await QuestHelper.giveSingleItem( player, HAMRUT_LEG, 1 )
                            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                        }

                        return
                }

                return

            case KRANROT:
                switch ( state.getMemoState() ) {
                    case 21101:
                    case 21100:
                    case 21001:
                    case 21000:
                    case 20101:
                    case 20100:
                    case 20001:
                    case 20000:
                        if ( !QuestHelper.hasQuestItem( player, KRANROT_SKIN ) ) {
                            await QuestHelper.giveSingleItem( player, KRANROT_SKIN, 1 )
                            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                        }

                        return
                }

                return

            case MARSH_STALKER:
                switch ( state.getMemoState() ) {
                    case 20111:
                    case 20110:
                    case 20101:
                    case 20100:
                    case 20011:
                    case 20010:
                    case 20001:
                    case 20000:
                        if ( !QuestHelper.hasQuestItem( player, MARSH_STALKER_HORN ) ) {
                            await QuestHelper.giveSingleItem( player, MARSH_STALKER_HORN, 1 )
                            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                        }

                        return
                }

                return

            case MARSH_DRAKE:
                switch ( state.getMemoState() ) {
                    case 20111:
                    case 20110:
                    case 20101:
                    case 20100:
                    case 20011:
                    case 20010:
                    case 20001:
                    case 20000:
                        if ( !QuestHelper.hasQuestItem( player, MARSH_DRAKE_TALONS ) ) {
                            await QuestHelper.giveSingleItem( player, MARSH_DRAKE_TALONS, 1 )
                            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                        }

                        return
                }

                return

            case JEWEL_GUARDIAN_MARA:
                switch ( state.getMemoState() ) {
                    case 40000:
                    case 40001:
                        if ( !QuestHelper.hasQuestItem( player, MARA_FANG ) ) {
                            await QuestHelper.giveSingleItem( player, MARA_FANG, 1 )
                            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                        }

                        return
                }

                return

            case JEWEL_GUARDIAN_MUSFEL:
                switch ( state.getMemoState() ) {
                    case 40000:
                    case 40010:
                        if ( !QuestHelper.hasQuestItem( player, MUSFEL_FANG ) ) {
                            await QuestHelper.giveSingleItem( player, MUSFEL_FANG, 1 )
                            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                        }

                        return
                }

                return

            case GHOST_OF_OFFERING:
                switch ( state.getMemoState() ) {
                    case 21011:
                    case 21010:
                    case 21001:
                    case 21000:
                    case 20011:
                    case 20010:
                    case 20001:
                    case 20000:
                        if ( !QuestHelper.hasQuestItem( player, REMAINS_OF_SACRAFICE ) ) {
                            await QuestHelper.giveSingleItem( player, REMAINS_OF_SACRAFICE, 1 )
                            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                        }

                        return
                }

                return

            case HARIT_LIZARDMAN_ZEALOT:
                switch ( state.getMemoState() ) {
                    case 21110:
                    case 21100:
                    case 21010:
                    case 21000:
                    case 20110:
                    case 20100:
                    case 20010:
                    case 20000:
                        if ( !QuestHelper.hasQuestItem( player, TOTEM_OF_LAND_DRAGON ) ) {
                            await QuestHelper.giveSingleItem( player, TOTEM_OF_LAND_DRAGON, 1 )
                            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                        }
                        return
                }

                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        if ( [ eventNames.despawnOne, eventNames.despawnTwo ].includes( data.eventName ) ) {
            let npc = L2World.getObjectById( data.characterId ) as L2Npc
            await npc.deleteMe()

            return
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30753-05.htm':
                await QuestHelper.giveSingleItem( player, FEATHER_OF_GABRIELLE, 1 )
                state.startQuest()
                state.setMemoState( 20000 )
                break

            case '30753-09.html':
                await QuestHelper.takeSingleItem( player, MARK_OF_WATCHMAN, -1 )
                state.setMemoState( 40000 )
                state.setConditionWithSound( 2, true )
                break

            case '30754-03.html':
                state.setMemoState( 70000 )
                state.setConditionWithSound( 4, true )
                break

            case '30755-05.html':
                if ( state.isMemoState( 70000 )
                        && QuestHelper.hasQuestItem( player, FRAGMENT_OF_ABYSS_JEWEL_3RD ) ) {
                    await QuestHelper.giveSingleItem( player, PORTAL_STONE, 1 )
                    await state.exitQuest( true, true )

                    break
                }

                return

            case '30498-02.html':
            case '30678-01a.html':
            case '30753-01a.html':
            case '30753-03.htm':
            case '30753-04.htm':
            case '30753-06a.html':
                break
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== GABRIELLE ) {
                    break
                }

                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '30753-01.htm' )
                }

                return this.getPath( '30753-02.htm' )

            case QuestStateValues.STARTED:
                const memoState = state.getMemoState()

                switch ( data.characterNpcId ) {
                    case GABRIELLE:
                        if ( memoState >= 20000 && memoState < 30000 ) {
                            return this.getPath( '30753-06.html' )
                        }

                        if ( memoState === 30000 ) {
                            return this.getPath( '30753-08.html' )
                        }

                        if ( memoState >= 40000 && memoState < 50000 ) {
                            return this.getPath( '30753-10.html' )
                        }

                        if ( memoState === 50000 ) {
                            await QuestHelper.takeMultipleItems( player, -1, FEATHER_OF_GABRIELLE, MARK_OF_WATCHMAN )
                            await QuestHelper.giveSingleItem( player, HERALD_OF_SLAYER, 1 )

                            state.setMemoState( 60000 )
                            state.setConditionWithSound( 3, true )

                            return this.getPath( '30753-11.html' )
                        }

                        if ( memoState === 60000 ) {
                            return this.getPath( '30753-12.html' )
                        }

                        if ( memoState === 70000 ) {
                            return this.getPath( '30753-13.html' )
                        }

                        break

                    case WAREHOUSE_CHIEF_MOKE:
                        if ( memoState === 40000 || memoState === 40001 ) {
                            if ( QuestHelper.hasQuestItems( player, FRAGMENT_OF_ABYSS_JEWEL_1ST, MARA_FANG ) ) {
                                await QuestHelper.takeMultipleItems( player, -1, FRAGMENT_OF_ABYSS_JEWEL_1ST, MARA_FANG )
                                await QuestHelper.giveSingleItem( player, MARK_OF_WATCHMAN, 1 )

                                state.setMemoState( memoState === 40001 ? 50000 : 40010 )

                                return this.getPath( '30498-03.html' )
                            }

                            return this.getPath( '30498-01.html' )
                        }

                        if ( memoState === 40010 ) {
                            return this.getPath( '30498-04.html' )
                        }

                        if ( memoState >= 50000 ) {
                            return this.getPath( '30498-05.html' )
                        }

                        break

                    case BLACKSMITH_HELTON:
                        if ( memoState === 40000 || memoState === 40010 ) {
                            if ( QuestHelper.hasQuestItems( player, FRAGMENT_OF_ABYSS_JEWEL_2ND, MUSFEL_FANG ) ) {
                                await QuestHelper.takeMultipleItems( player, -1, FRAGMENT_OF_ABYSS_JEWEL_2ND, MUSFEL_FANG )
                                await QuestHelper.giveSingleItem( player, MARK_OF_WATCHMAN, 1 )

                                state.setMemoState( memoState === 40010 ? 50000 : 40001 )

                                return this.getPath( '30678-02.html' )
                            }

                            return this.getPath( '30678-01.html' )
                        }

                        if ( memoState === 40001 ) {
                            return this.getPath( '30678-03.html' )
                        }

                        if ( memoState >= 50000 ) {
                            return this.getPath( '30678-04.html' )
                        }

                        break

                    case PREFECT_CHAKIRIS:
                        switch ( memoState ) {
                            case 21101:
                            case 21000:
                            case 21100:
                            case 21001:
                            case 20101:
                            case 20100:
                            case 20001:
                            case 20000:
                                if ( QuestHelper.hasQuestItems( player, KRANROT_SKIN, HAMRUT_LEG ) ) {
                                    await QuestHelper.takeMultipleItems( player, -1, KRANROT_SKIN, HAMRUT_LEG )
                                    await QuestHelper.giveSingleItem( player, MARK_OF_WATCHMAN, 1 )

                                    let updatedMemoState = memoState + 10
                                    state.setMemoState( updatedMemoState === 21111 ? 30000 : updatedMemoState )

                                    return this.getPath( '30705-02.html' )
                                }

                                return this.getPath( '30705-01.html' )

                            case 21110:
                            case 21011:
                            case 21010:
                            case 20111:
                            case 20110:
                            case 20011:
                            case 20010:
                                return this.getPath( '30705-03.html' )
                        }

                        if ( memoState >= 30000 ) {
                            return this.getPath( '30705-04.html' )
                        }

                        break

                    case MAGISTER_KAIENA:
                        switch ( memoState ) {
                            case 20111:
                            case 20110:
                            case 20101:
                            case 20100:
                            case 20010:
                            case 20011:
                            case 20001:
                            case 20000:
                                if ( QuestHelper.hasQuestItems( player, MARSH_STALKER_HORN, MARSH_DRAKE_TALONS ) ) {
                                    await QuestHelper.takeMultipleItems( player, -1, MARSH_STALKER_HORN, MARSH_DRAKE_TALONS )
                                    await QuestHelper.giveSingleItem( player, MARK_OF_WATCHMAN, 1 )

                                    let updatedMemoState = memoState + 1000
                                    state.setMemoState( updatedMemoState === 21111 ? 30000 : updatedMemoState )

                                    return this.getPath( '30720-02.html' )
                                }

                                return this.getPath( '30720-01.html' )

                            case 21110:
                            case 21101:
                            case 21100:
                            case 21011:
                            case 21010:
                            case 21001:
                            case 21000:
                                return this.getPath( '30720-03.html' )
                        }

                        if ( memoState >= 30000 ) {
                            return this.getPath( '30720-04.html' )
                        }

                        break

                    case ANTHARAS_WATCHMAN_GILMORE:
                        if ( memoState < 60000 ) {
                            return this.getPath( '30754-01.html' )
                        }

                        if ( memoState === 60000 ) {
                            return this.getPath( '30754-02.html' )
                        }

                        if ( memoState === 70000 ) {
                            if ( QuestHelper.hasQuestItems( player, FRAGMENT_OF_ABYSS_JEWEL_3RD ) ) {
                                return this.getPath( '30754-05.html' )
                            }

                            return this.getPath( '30754-04.html' )
                        }

                        break

                    case ANTHARAS_WATCHMAN_THEODRIC:
                        if ( memoState < 60000 ) {
                            return this.getPath( '30755-01.html' )
                        }

                        if ( memoState === 60000 ) {
                            return this.getPath( '30755-02.html' )
                        }

                        if ( memoState === 70000 ) {
                            if ( !QuestHelper.hasQuestItems( player, FRAGMENT_OF_ABYSS_JEWEL_3RD ) ) {
                                return this.getPath( '30755-03.html' )
                            }

                            return this.getPath( '30755-04.html' )
                        }

                        break

                    case MASTER_KENDRA:
                        switch ( memoState ) {
                            case 21110:
                            case 21100:
                            case 21010:
                            case 21000:
                            case 20110:
                            case 20100:
                            case 20010:
                            case 20000:
                                if ( !QuestHelper.hasQuestItems( player, TOTEM_OF_LAND_DRAGON ) ) {
                                    return this.getPath( '30851-01.html' )
                                }

                                await QuestHelper.takeSingleItem( player, TOTEM_OF_LAND_DRAGON, -1 )
                                await QuestHelper.giveSingleItem( player, MARK_OF_WATCHMAN, 1 )

                                let updatedMemoState = memoState + 1
                                state.setMemoState( updatedMemoState === 21111 ? 30000 : updatedMemoState )

                                return this.getPath( '30851-02.html' )

                            case 21101:
                            case 21011:
                            case 21001:
                            case 20111:
                            case 20101:
                            case 20011:
                            case 20001:
                                return this.getPath( '30851-03.html' )
                        }

                        if ( memoState >= 30000 ) {
                            return this.getPath( '30851-04.html' )
                        }

                        break

                    case HIGH_PRIEST_ORVEN:
                        switch ( memoState ) {
                            case 21011:
                            case 21010:
                            case 21001:
                            case 21000:
                            case 20011:
                            case 20010:
                            case 20001:
                            case 20000:
                                if ( !QuestHelper.hasQuestItems( player, REMAINS_OF_SACRAFICE ) ) {
                                    return this.getPath( '30857-01.html' )
                                }

                                await QuestHelper.takeSingleItem( player, REMAINS_OF_SACRAFICE, -1 )
                                await QuestHelper.giveSingleItem( player, MARK_OF_WATCHMAN, 1 )

                                let updatedMemoState = memoState + 100
                                state.setMemoState( updatedMemoState === 21111 ? 30000 : updatedMemoState )

                                return this.getPath( '30857-02.html' )

                            case 21110:
                            case 21101:
                            case 21100:
                            case 20111:
                            case 20110:
                            case 20101:
                            case 20100:
                                return this.getPath( '30857-03.html' )
                        }

                        if ( memoState >= 30000 ) {
                            return this.getPath( '30857-04.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async onJewelAttacked( player: L2PcInstance, npc: L2Npc, givenItemId: number, amountSpawned: number, spawnedNpcId: number ): Promise<void> {
        if ( npc.getCurrentHp() < npc.getMaxHp() * 0.8
                && !NpcVariablesManager.get( npc.getObjectId(), this.getName() ) ) {

            _.times( amountSpawned, () => {
                QuestHelper.addAttackDesire( QuestHelper.addSpawnAtLocation( spawnedNpcId, npc, true, 180000 ), player )
            } )

            NpcVariablesManager.set( npc.getObjectId(), this.getName(), true )
            this.startQuestTimer( eventNames.despawnOne, 900000, 0, 0 )
        }

        if ( ( npc.getCurrentHp() < ( npc.getMaxHp() * 0.4 ) )
                && !QuestHelper.hasQuestItems( player, givenItemId ) ) {
            await QuestHelper.giveSingleItem( player, givenItemId, 1 )
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

            this.startQuestTimer( eventNames.despawnTwo, 240000, 0, 0 )
        }
    }
}