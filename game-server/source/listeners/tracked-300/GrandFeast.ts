import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const RANSPO = 30594
const JONAS_SALAD_RECIPE = 1455
const JONAS_SAUCE_RECIPE = 1456
const JONAS_STEAK_RECIPE = 1457
const THEME_OF_THE_FEAST = 4421
const OLD_WINE_15_YEAR = 5956
const OLD_WINE_30_YEAR = 5957
const OLD_WINE_60_YEAR = 5958
const RITRONS_DESSERT_RECIPE = 5959
const CORAL_EARRING = 846
const RED_CRESCENT_EARRING = 847
const ENCHANTED_EARRING = 848
const ENCHANTED_RING = 879
const RING_OF_DEVOTION = 890
const BLUE_DIAMOND_NECKLACE = 909
const NECKLACE_OF_DEVOTION = 910
const minimumLevel = 20

type QuestReward = [ number, number, number ] // itemId, amount, adena
const questStateRewards: { [ memoState: number ]: QuestReward } = {
    11: [ RED_CRESCENT_EARRING, 1, 5700 ],
    12: [ CORAL_EARRING, 2, 1200 ],
    13: [ ENCHANTED_RING, 1, 8100 ],
    21: [ CORAL_EARRING, 2, 0 ],
    22: [ ENCHANTED_RING, 1, 6900 ],
    23: [ NECKLACE_OF_DEVOTION, 1, 0 ],
    31: [ BLUE_DIAMOND_NECKLACE, 1, 25400 ],
    32: [ RING_OF_DEVOTION, 2, 8500 ],
    33: [ ENCHANTED_EARRING, 1, 2200 ],
}

export class GrandFeast extends ListenerLogic {
    constructor() {
        super( 'Q00378_GrandFeast', 'listeners/tracked-300/GrandFeast.ts' )
        this.questId = 378
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00378_GrandFeast'
    }

    getQuestStartIds(): Array<number> {
        return [ RANSPO ]
    }

    getTalkIds(): Array<number> {
        return [ RANSPO ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30594-02.htm':
                state.startQuest()
                state.setMemoStateEx( 1, 0 )
                break

            case '30594-05.html':
                if ( QuestHelper.hasQuestItem( player, OLD_WINE_15_YEAR ) ) {
                    await QuestHelper.takeSingleItem( player, OLD_WINE_15_YEAR, 1 )

                    state.setMemoStateEx( 1, state.getMemoStateEx( 1 ) + 10 )
                    state.setConditionWithSound( 2, true )

                    break
                }

                return this.getPath( '30594-08.html' )

            case '30594-06.html':
                if ( QuestHelper.hasQuestItem( player, OLD_WINE_30_YEAR ) ) {
                    await QuestHelper.takeSingleItem( player, OLD_WINE_30_YEAR, 1 )

                    state.setMemoStateEx( 1, state.getMemoStateEx( 1 ) + 20 )
                    state.setConditionWithSound( 2, true )

                    break
                }

                return this.getPath( '30594-08.html' )

            case '30594-07.html':
                if ( QuestHelper.hasQuestItem( player, OLD_WINE_60_YEAR ) ) {
                    await QuestHelper.takeSingleItem( player, OLD_WINE_60_YEAR, 1 )

                    state.setMemoStateEx( 1, state.getMemoStateEx( 1 ) + 30 )
                    state.setConditionWithSound( 2, true )

                    break
                }

                return this.getPath( '30594-08.html' )

            case '30594-09.html':
            case '30594-18.html':
                break

            case '30594-12.html':
                if ( QuestHelper.hasQuestItem( player, THEME_OF_THE_FEAST ) ) {
                    await QuestHelper.takeSingleItem( player, THEME_OF_THE_FEAST, 1 )

                    state.setConditionWithSound( 3, true )
                    break
                }

                return this.getPath( '30594-08.html' )

            case '30594-14.html':
                if ( QuestHelper.hasQuestItem( player, JONAS_SALAD_RECIPE ) ) {
                    await QuestHelper.takeSingleItem( player, JONAS_SALAD_RECIPE, 1 )

                    state.setMemoStateEx( 1, state.getMemoStateEx( 1 ) + 1 )
                    state.setConditionWithSound( 4, true )

                    break
                }

                return this.getPath( '30594-17.html' )

            case '30594-15.html':
                if ( QuestHelper.hasQuestItem( player, JONAS_SAUCE_RECIPE ) ) {
                    await QuestHelper.takeSingleItem( player, JONAS_SAUCE_RECIPE, 1 )

                    state.setMemoStateEx( 1, state.getMemoStateEx( 1 ) + 2 )
                    state.setConditionWithSound( 4, true )

                    break
                }

                return this.getPath( '30594-17.html' )

            case '30594-16.html':
                if ( QuestHelper.hasQuestItem( player, JONAS_STEAK_RECIPE ) ) {
                    await QuestHelper.takeSingleItem( player, JONAS_STEAK_RECIPE, 1 )

                    state.setMemoStateEx( 1, state.getMemoStateEx( 1 ) + 3 )
                    state.setConditionWithSound( 4, true )

                    break
                }

                return this.getPath( '30594-17.html' )


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30594-01.htm' : '30594-03.html' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '30594-04.html' )

                    case 2:
                        return this.getPath( QuestHelper.hasQuestItem( player, THEME_OF_THE_FEAST ) ? '30594-11.html' : '30594-10.html' )

                    case 3:
                        return this.getPath( '30594-13.html' )

                    case 4:
                        if ( QuestHelper.hasQuestItem( player, RITRONS_DESSERT_RECIPE ) ) {
                            await QuestHelper.takeSingleItem( player, RITRONS_DESSERT_RECIPE, 1 )

                            if ( questStateRewards[ state.getMemoStateEx( 1 ) ] ) {
                                let [ itemId, amount, adena ]: QuestReward = questStateRewards[ state.getMemoStateEx( 1 ) ]

                                await QuestHelper.rewardSingleItem( player, itemId, amount )

                                if ( adena > 0 ) {
                                    await QuestHelper.giveAdena( player, adena, true )
                                }
                            }

                            await state.exitQuest( true, true )
                            return this.getPath( '30594-20.html' )
                        }

                        return this.getPath( '30594-19.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}