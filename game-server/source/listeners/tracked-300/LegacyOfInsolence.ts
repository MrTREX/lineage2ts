import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const TRADER_HOLLY = 30839
const WAREHOUSE_KEEPER_WALDERAL = 30844
const MAGISTER_DESMOND = 30855
const ANTIQUE_DEALER_PATRIN = 30929
const CLAUDIA_ATHEBALDT = 31001
const ANCIENT_RED_PAPYRUS = 5966
const ANCIENT_BLUE_PAPYRUS = 5967
const ANCIENT_BLACK_PAPYRUS = 5968
const ANCIENT_WHITE_PAPYRUS = 5969
const REVELATION_OF_THE_SEALS_CHAPTER_OF_AVARICE = 5972
const REVELATION_OF_THE_SEALS_CHAPTER_OF_GNOSIS = 5973
const REVELATION_OF_THE_SEALS_CHAPTER_OF_STRIFE = 5974
const REVELATION_OF_THE_SEALS_CHAPTER_OF_VENGEANCE = 5975
const REVELATION_OF_THE_SEALS_CHAPTER_OF_AWEKENING = 5976
const REVELATION_OF_THE_SEALS_CHAPTER_OF_CALAMITY = 5977
const REVELATION_OF_THE_SEALS_CHAPTER_OF_DESCENT = 5978
const ANCIENT_EPIC_CHAPTER_1 = 5979
const ANCIENT_EPIC_CHAPTER_2 = 5980
const ANCIENT_EPIC_CHAPTER_3 = 5981
const ANCIENT_EPIC_CHAPTER_4 = 5982
const ANCIENT_EPIC_CHAPTER_5 = 5983
const IMPERIAL_GENEALOGY_1 = 5984
const IMPERIAL_GENEALOGY_2 = 5985
const IMPERIAL_GENEALOGY_3 = 5986
const IMPERIAL_GENEALOGY_4 = 5987
const IMPERIAL_GENEALOGY_5 = 5988
const BLUEPRINT_TOWER_OF_INSOLENCE_1ST_FLOOR = 5989
const BLUEPRINT_TOWER_OF_INSOLENCE_2ND_FLOOR = 5990
const BLUEPRINT_TOWER_OF_INSOLENCE_3RD_FLOOR = 5991
const BLUEPRINT_TOWER_OF_INSOLENCE_4TH_FLOOR = 5992
const BLUEPRINT_TOWER_OF_INSOLENCE_5TH_FLOOR = 5993
const BLUEPRINT_TOWER_OF_INSOLENCE_6TH_FLOOR = 5994
const BLUEPRINT_TOWER_OF_INSOLENCE_7TH_FLOOR = 5995
const BLUEPRINT_TOWER_OF_INSOLENCE_8TH_FLOOR = 5996
const BLUEPRINT_TOWER_OF_INSOLENCE_9TH_FLOOR = 5997
const BLUEPRINT_TOWER_OF_INSOLENCE_10TH_FLOOR = 5998
const BLUEPRINT_TOWER_OF_INSOLENCE_11TH_FLOOR = 5999
const BLUEPRINT_TOWER_OF_INSOLENCE_12TH_FLOOR = 6000
const BLUEPRINT_TOWER_OF_INSOLENCE_13TH_FLOOR = 6001
const RECIPE_SEALED_DARK_CRYSTAL_BOOTS_60 = 5368
const RECIPE_SEALED_TALLUM_BOOTS_60 = 5370
const RECIPE_SEALED_BOOTS_OF_NIGHTMARE_60 = 5380
const RECIPE_SEALED_MAJESTIC_BOOTS_60 = 5382
const RECIPE_SEALED_DARK_CRYSTAL_GLOVES_60 = 5392
const RECIPE_SEALED_TALLUM_GLOVES_60 = 5394
const RECIPE_SEALED_GAUNTLETS_OF_NIGHTMARE_60 = 5404
const RECIPE_SEALED_MAJESTIC_GAUNTLETS_60 = 5406
const RECIPE_SEALED_DARK_CRYSTAL_HELMET_60 = 5426
const RECIPE_SEALED_TALLUM_HELMET_60 = 5428
const RECIPE_SEALED_HELM_OF_NIGHTMARE_60 = 5430
const RECIPE_SEALED_MAJESTIC_CIRCLET_60 = 5432
const SEALED_DARK_CRYSTAL_BOOTS_LINING = 5496
const SEALED_TALLUM_BOOTS_LINING = 5497
const SEALED_BOOTS_OF_NIGHTMARE_LINING = 5502
const SEALED_MAJESTIC_BOOTS_LINING = 5503
const SEALED_DARK_CRYSTAL_GLOVES_DESIGN = 5508
const SEALED_TALLUM_GLOVES_DESIGN = 5509
const SEALED_GAUNTLETS_OF_NIGHTMARE_DESIGN = 5514
const SEALED_MAJESTIC_GAUNTLETS_DESIGN = 5515
const SEALED_DARK_CRYSTAL_HELMET_DESIGN = 5525
const SEALED_TALLUM_HELM_DESIGN = 5526
const SEALED_HELM_OF_NIGHTMARE_DESIGN = 5527
const SEALED_MAJESTIC_CIRCLET_DESIGN = 5528
const HALLATES_INSPECTOR = 20825
const minimumLevel = 59

type MonsterReward = [ number, number, number ] // itemId, chance, amount
const monsterRewards: { [ npcId: number ]: MonsterReward } = {
    20817: [ ANCIENT_RED_PAPYRUS, 0.302, 1 ],
    20821: [ ANCIENT_RED_PAPYRUS, 0.410, 1 ],
    [ HALLATES_INSPECTOR ]: [ ANCIENT_RED_PAPYRUS, 0.001, 447 ],
    20829: [ ANCIENT_BLUE_PAPYRUS, 0.451, 1 ],
    21062: [ ANCIENT_WHITE_PAPYRUS, 0.290, 1 ],
    21069: [ ANCIENT_BLACK_PAPYRUS, 0.280, 1 ],
}

export class LegacyOfInsolence extends ListenerLogic {
    constructor() {
        super( 'Q00372_LegacyOfInsolence', 'listeners/tracked-300/LegacyOfInsolence.ts' )
        this.questId = 372
        this.questItemIds = [
            BLUEPRINT_TOWER_OF_INSOLENCE_1ST_FLOOR,
            BLUEPRINT_TOWER_OF_INSOLENCE_2ND_FLOOR,
            BLUEPRINT_TOWER_OF_INSOLENCE_3RD_FLOOR,
            BLUEPRINT_TOWER_OF_INSOLENCE_4TH_FLOOR,
            BLUEPRINT_TOWER_OF_INSOLENCE_5TH_FLOOR,
            BLUEPRINT_TOWER_OF_INSOLENCE_6TH_FLOOR,
            BLUEPRINT_TOWER_OF_INSOLENCE_7TH_FLOOR,
            BLUEPRINT_TOWER_OF_INSOLENCE_8TH_FLOOR,
            BLUEPRINT_TOWER_OF_INSOLENCE_9TH_FLOOR,
            BLUEPRINT_TOWER_OF_INSOLENCE_10TH_FLOOR,
            BLUEPRINT_TOWER_OF_INSOLENCE_11TH_FLOOR,
            BLUEPRINT_TOWER_OF_INSOLENCE_12TH_FLOOR,
            BLUEPRINT_TOWER_OF_INSOLENCE_13TH_FLOOR,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewards ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00372_LegacyOfInsolence'
    }

    getQuestStartIds(): Array<number> {
        return [ WAREHOUSE_KEEPER_WALDERAL ]
    }

    getTalkIds(): Array<number> {
        return [
            WAREHOUSE_KEEPER_WALDERAL,
            TRADER_HOLLY,
            MAGISTER_DESMOND,
            ANTIQUE_DEALER_PATRIN,
            CLAUDIA_ATHEBALDT,
        ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let [ itemId, chance, amount ]: MonsterReward = monsterRewards[ data.npcId ]
        if ( Math.random() > QuestHelper.getAdjustedChance( itemId, chance, data.isChampion ) ) {
            return
        }

        let playerChances = data.npcId === HALLATES_INSPECTOR ? 3 : 1
        let player: L2PcInstance = this.getRandomPartyMemberPerCondition( L2World.getPlayer( data.playerId ), -1, playerChances, L2World.getObjectById( data.targetId ) )

        if ( !player ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, itemId, amount, data.isChampion )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30844-04.htm':
                state.startQuest()
                break

            case '30844-07.html':
                if ( this.hasAllItems( player ) ) {
                    break
                }

                return this.getPath( '30844-06.html' )

            case '30844-09.html':
                await state.exitQuest( true, true )
                break

            case '30844-07a.html':
                if ( this.hasAllItems( player ) ) {
                    await this.takeAllItems( player )
                    await this.rewardDarkCrystalItems( player )

                    break
                }

                return this.getPath( '30844-07e.html' )

            case '30844-07b.html':
                if ( this.hasAllItems( player ) ) {
                    await this.takeAllItems( player )
                    await this.rewardTallumItems( player )

                    break
                }

                return this.getPath( '30844-07e.html' )

            case '30844-07c.html':
                if ( this.hasAllItems( player ) ) {
                    await this.takeAllItems( player )
                    await this.rewardNightmareItems( player )

                    break
                }

                return this.getPath( '30844-07e.html' )

            case '30844-07d.html':
                if ( this.hasAllItems( player ) ) {
                    await this.takeAllItems( player )
                    await this.rewardMajesticItems( player )

                    break
                }

                return this.getPath( '30844-07e.html' )

            case '30844-05b.html':
                state.setConditionWithSound( 2 )
                break

            case '30844-03.htm':
            case '30844-05.html':
            case '30844-05a.html':
            case '30844-08.html':
            case '30844-10.html':
            case '30844-11.html':
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== WAREHOUSE_KEEPER_WALDERAL ) {
                    break
                }

                return this.getPath( player.getLevel() < minimumLevel ? '30844-01.htm' : '30844-02.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case WAREHOUSE_KEEPER_WALDERAL:
                        return this.getPath( '30844-05.html' )

                    case TRADER_HOLLY:
                        if ( QuestHelper.hasQuestItems( player,
                                IMPERIAL_GENEALOGY_1,
                                IMPERIAL_GENEALOGY_2,
                                IMPERIAL_GENEALOGY_3,
                                IMPERIAL_GENEALOGY_4,
                                IMPERIAL_GENEALOGY_5 ) ) {
                            await QuestHelper.takeMultipleItems( player,
                                    1,
                                    IMPERIAL_GENEALOGY_1,
                                    IMPERIAL_GENEALOGY_2,
                                    IMPERIAL_GENEALOGY_3,
                                    IMPERIAL_GENEALOGY_4,
                                    IMPERIAL_GENEALOGY_5 )

                            await this.rewardItemsFromTraderHolly( player )
                            return this.getPath( '30839-02.html' )
                        }

                        return this.getPath( '30839-01.html' )

                    case MAGISTER_DESMOND:
                        if ( QuestHelper.hasQuestItems( player,
                                REVELATION_OF_THE_SEALS_CHAPTER_OF_AVARICE,
                                REVELATION_OF_THE_SEALS_CHAPTER_OF_GNOSIS,
                                REVELATION_OF_THE_SEALS_CHAPTER_OF_STRIFE,
                                REVELATION_OF_THE_SEALS_CHAPTER_OF_VENGEANCE,
                                REVELATION_OF_THE_SEALS_CHAPTER_OF_AWEKENING,
                                REVELATION_OF_THE_SEALS_CHAPTER_OF_CALAMITY,
                                REVELATION_OF_THE_SEALS_CHAPTER_OF_DESCENT ) ) {
                            await QuestHelper.takeMultipleItems( player,
                                    1,
                                    REVELATION_OF_THE_SEALS_CHAPTER_OF_AVARICE,
                                    REVELATION_OF_THE_SEALS_CHAPTER_OF_GNOSIS,
                                    REVELATION_OF_THE_SEALS_CHAPTER_OF_STRIFE,
                                    REVELATION_OF_THE_SEALS_CHAPTER_OF_VENGEANCE,
                                    REVELATION_OF_THE_SEALS_CHAPTER_OF_AWEKENING,
                                    REVELATION_OF_THE_SEALS_CHAPTER_OF_CALAMITY,
                                    REVELATION_OF_THE_SEALS_CHAPTER_OF_DESCENT )

                            await this.rewardItemsFromMagisterDesmond( player )
                            return this.getPath( '30855-02.html' )
                        }

                        return this.getPath( '30855-01.html' )

                    case ANTIQUE_DEALER_PATRIN:
                        if ( QuestHelper.hasQuestItems( player,
                                ANCIENT_EPIC_CHAPTER_1,
                                ANCIENT_EPIC_CHAPTER_2,
                                ANCIENT_EPIC_CHAPTER_3,
                                ANCIENT_EPIC_CHAPTER_4,
                                ANCIENT_EPIC_CHAPTER_5 ) ) {
                            await QuestHelper.takeMultipleItems( player,
                                    1,
                                    ANCIENT_EPIC_CHAPTER_1,
                                    ANCIENT_EPIC_CHAPTER_2,
                                    ANCIENT_EPIC_CHAPTER_3,
                                    ANCIENT_EPIC_CHAPTER_4,
                                    ANCIENT_EPIC_CHAPTER_5 )

                            await this.rewardItemsFromDealerPatrin( player )
                            return this.getPath( '30929-02.html' )
                        }

                        return this.getPath( '30929-02.html' )

                    case CLAUDIA_ATHEBALDT:
                        if ( QuestHelper.hasQuestItems( player,
                                REVELATION_OF_THE_SEALS_CHAPTER_OF_AVARICE,
                                REVELATION_OF_THE_SEALS_CHAPTER_OF_GNOSIS,
                                REVELATION_OF_THE_SEALS_CHAPTER_OF_STRIFE,
                                REVELATION_OF_THE_SEALS_CHAPTER_OF_VENGEANCE,
                                REVELATION_OF_THE_SEALS_CHAPTER_OF_AWEKENING,
                                REVELATION_OF_THE_SEALS_CHAPTER_OF_CALAMITY,
                                REVELATION_OF_THE_SEALS_CHAPTER_OF_DESCENT ) ) {
                            await QuestHelper.takeMultipleItems( player,
                                    1,
                                    REVELATION_OF_THE_SEALS_CHAPTER_OF_AVARICE,
                                    REVELATION_OF_THE_SEALS_CHAPTER_OF_GNOSIS,
                                    REVELATION_OF_THE_SEALS_CHAPTER_OF_STRIFE,
                                    REVELATION_OF_THE_SEALS_CHAPTER_OF_VENGEANCE,
                                    REVELATION_OF_THE_SEALS_CHAPTER_OF_AWEKENING,
                                    REVELATION_OF_THE_SEALS_CHAPTER_OF_CALAMITY,
                                    REVELATION_OF_THE_SEALS_CHAPTER_OF_DESCENT )

                            await this.rewardItemsFromClaudia( player )
                            return this.getPath( '31001-02.html' )
                        }

                        return this.getPath( '31001-01.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    hasAllItems( player: L2PcInstance ): boolean {
        return QuestHelper.hasQuestItems( player,
                BLUEPRINT_TOWER_OF_INSOLENCE_1ST_FLOOR,
                BLUEPRINT_TOWER_OF_INSOLENCE_2ND_FLOOR,
                BLUEPRINT_TOWER_OF_INSOLENCE_3RD_FLOOR,
                BLUEPRINT_TOWER_OF_INSOLENCE_4TH_FLOOR,
                BLUEPRINT_TOWER_OF_INSOLENCE_5TH_FLOOR,
                BLUEPRINT_TOWER_OF_INSOLENCE_6TH_FLOOR,
                BLUEPRINT_TOWER_OF_INSOLENCE_7TH_FLOOR,
                BLUEPRINT_TOWER_OF_INSOLENCE_8TH_FLOOR,
                BLUEPRINT_TOWER_OF_INSOLENCE_9TH_FLOOR,
                BLUEPRINT_TOWER_OF_INSOLENCE_10TH_FLOOR,
                BLUEPRINT_TOWER_OF_INSOLENCE_11TH_FLOOR,
                BLUEPRINT_TOWER_OF_INSOLENCE_12TH_FLOOR,
                BLUEPRINT_TOWER_OF_INSOLENCE_13TH_FLOOR )
    }

    rewardDarkCrystalItems( player: L2PcInstance ): Promise<void> {
        let chance = _.random( 100 )
        if ( chance < 10 ) {
            return QuestHelper.rewardSingleItem( player, SEALED_DARK_CRYSTAL_BOOTS_LINING, 1 )
        }

        if ( chance < 20 ) {
            return QuestHelper.rewardSingleItem( player, SEALED_DARK_CRYSTAL_GLOVES_DESIGN, 1 )
        }

        if ( chance < 30 ) {
            return QuestHelper.rewardSingleItem( player, SEALED_DARK_CRYSTAL_HELMET_DESIGN, 1 )
        }

        if ( chance < 40 ) {
            return QuestHelper.rewardMultipleItems( player, 1, SEALED_DARK_CRYSTAL_BOOTS_LINING, SEALED_DARK_CRYSTAL_GLOVES_DESIGN, SEALED_DARK_CRYSTAL_HELMET_DESIGN )
        }

        if ( chance < 51 ) {
            return QuestHelper.rewardSingleItem( player, RECIPE_SEALED_DARK_CRYSTAL_BOOTS_60, 1 )
        }

        if ( chance < 62 ) {
            return QuestHelper.rewardSingleItem( player, RECIPE_SEALED_DARK_CRYSTAL_GLOVES_60, 1 )
        }

        if ( chance < 79 ) {
            return QuestHelper.rewardSingleItem( player, RECIPE_SEALED_DARK_CRYSTAL_HELMET_60, 1 )
        }

        return QuestHelper.rewardMultipleItems( player, 1, RECIPE_SEALED_DARK_CRYSTAL_BOOTS_60, RECIPE_SEALED_DARK_CRYSTAL_GLOVES_60, RECIPE_SEALED_DARK_CRYSTAL_HELMET_60 )
    }

    rewardItemsFromClaudia( player: L2PcInstance ): Promise<void> {
        let chance = _.random( 100 )

        if ( chance < 31 ) {
            return QuestHelper.rewardSingleItem( player, SEALED_BOOTS_OF_NIGHTMARE_LINING, 1 )
        }

        if ( chance < 62 ) {
            return QuestHelper.rewardSingleItem( player, SEALED_GAUNTLETS_OF_NIGHTMARE_DESIGN, 1 )
        }

        if ( chance < 75 ) {
            return QuestHelper.rewardSingleItem( player, SEALED_HELM_OF_NIGHTMARE_DESIGN, 1 )
        }

        if ( chance < 83 ) {
            return QuestHelper.rewardMultipleItems( player,
                    1,
                    SEALED_BOOTS_OF_NIGHTMARE_LINING,
                    SEALED_GAUNTLETS_OF_NIGHTMARE_DESIGN,
                    SEALED_HELM_OF_NIGHTMARE_DESIGN )
        }

        return QuestHelper.giveAdena( player, 4000, true )
    }

    rewardItemsFromDealerPatrin( player: L2PcInstance ): Promise<void> {
        let chance = _.random( 100 )

        if ( chance < 30 ) {
            return QuestHelper.rewardSingleItem( player, SEALED_TALLUM_BOOTS_LINING, 1 )
        }

        if ( chance < 60 ) {
            return QuestHelper.rewardSingleItem( player, SEALED_TALLUM_GLOVES_DESIGN, 1 )
        }

        if ( chance < 80 ) {
            return QuestHelper.rewardSingleItem( player, SEALED_TALLUM_HELM_DESIGN, 1 )
        }

        if ( chance < 90 ) {
            return QuestHelper.rewardMultipleItems( player,
                    1,
                    SEALED_TALLUM_BOOTS_LINING,
                    SEALED_TALLUM_GLOVES_DESIGN,
                    SEALED_TALLUM_HELM_DESIGN )
        }

        return QuestHelper.giveAdena( player, 4000, true )
    }

    rewardItemsFromMagisterDesmond( player: L2PcInstance ): Promise<void> {
        let chance = _.random( 100 )
        if ( chance < 31 ) {
            return QuestHelper.rewardSingleItem( player, SEALED_MAJESTIC_BOOTS_LINING, 1 )
        }

        if ( chance < 62 ) {
            return QuestHelper.rewardSingleItem( player, SEALED_MAJESTIC_GAUNTLETS_DESIGN, 1 )
        }

        if ( chance < 75 ) {
            return QuestHelper.rewardSingleItem( player, SEALED_MAJESTIC_CIRCLET_DESIGN, 1 )
        }

        if ( chance < 83 ) {
            return QuestHelper.rewardMultipleItems( player,
                    1,
                    SEALED_MAJESTIC_BOOTS_LINING,
                    SEALED_MAJESTIC_GAUNTLETS_DESIGN,
                    SEALED_MAJESTIC_CIRCLET_DESIGN )
        }

        return QuestHelper.giveAdena( player, 4000, true )
    }

    rewardItemsFromTraderHolly( player: L2PcInstance ): Promise<void> {
        let chance = _.random( 100 )

        if ( chance < 30 ) {
            return QuestHelper.rewardSingleItem( player, SEALED_DARK_CRYSTAL_BOOTS_LINING, 1 )
        }

        if ( chance < 60 ) {
            return QuestHelper.rewardSingleItem( player, SEALED_DARK_CRYSTAL_GLOVES_DESIGN, 1 )
        }

        if ( chance < 80 ) {
            return QuestHelper.rewardSingleItem( player, SEALED_DARK_CRYSTAL_HELMET_DESIGN, 1 )
        }

        if ( chance < 90 ) {
            return QuestHelper.rewardMultipleItems( player,
                    1,
                    SEALED_DARK_CRYSTAL_BOOTS_LINING,
                    SEALED_DARK_CRYSTAL_GLOVES_DESIGN,
                    SEALED_DARK_CRYSTAL_HELMET_DESIGN )
        }

        return QuestHelper.giveAdena( player, 4000, true )
    }

    rewardMajesticItems( player: L2PcInstance ): Promise<void> {
        let chance = _.random( 100 )

        if ( chance < 17 ) {
            return QuestHelper.rewardSingleItem( player, SEALED_MAJESTIC_BOOTS_LINING, 1 )
        }

        if ( chance < 34 ) {
            return QuestHelper.rewardSingleItem( player, SEALED_MAJESTIC_GAUNTLETS_DESIGN, 1 )
        }

        if ( chance < 49 ) {
            return QuestHelper.rewardSingleItem( player, SEALED_MAJESTIC_CIRCLET_DESIGN, 1 )
        }

        if ( chance < 58 ) {
            return QuestHelper.rewardMultipleItems( player, 1, SEALED_MAJESTIC_BOOTS_LINING, SEALED_MAJESTIC_GAUNTLETS_DESIGN, SEALED_MAJESTIC_CIRCLET_DESIGN )
        }

        if ( chance < 70 ) {
            return QuestHelper.rewardSingleItem( player, RECIPE_SEALED_MAJESTIC_BOOTS_60, 1 )
        }

        if ( chance < 82 ) {
            return QuestHelper.rewardSingleItem( player, RECIPE_SEALED_MAJESTIC_GAUNTLETS_60, 1 )
        }

        if ( chance < 92 ) {
            return QuestHelper.rewardSingleItem( player, RECIPE_SEALED_MAJESTIC_CIRCLET_60, 1 )
        }

        return QuestHelper.rewardMultipleItems( player, 1, RECIPE_SEALED_MAJESTIC_BOOTS_60, RECIPE_SEALED_MAJESTIC_GAUNTLETS_60, RECIPE_SEALED_MAJESTIC_CIRCLET_60 )
    }

    rewardNightmareItems( player: L2PcInstance ): Promise<void> {
        let chance = _.random( 100 )

        if ( chance < 17 ) {
            return QuestHelper.rewardSingleItem( player, SEALED_BOOTS_OF_NIGHTMARE_LINING, 1 )
        }

        if ( chance < 34 ) {
            return QuestHelper.rewardSingleItem( player, SEALED_GAUNTLETS_OF_NIGHTMARE_DESIGN, 1 )
        }

        if ( chance < 49 ) {
            return QuestHelper.rewardSingleItem( player, SEALED_HELM_OF_NIGHTMARE_DESIGN, 1 )
        }

        if ( chance < 58 ) {
            return QuestHelper.rewardMultipleItems( player, 1, SEALED_BOOTS_OF_NIGHTMARE_LINING, SEALED_GAUNTLETS_OF_NIGHTMARE_DESIGN, SEALED_HELM_OF_NIGHTMARE_DESIGN )
        }

        if ( chance < 70 ) {
            return QuestHelper.rewardSingleItem( player, RECIPE_SEALED_BOOTS_OF_NIGHTMARE_60, 1 )
        }

        if ( chance < 82 ) {
            return QuestHelper.rewardSingleItem( player, RECIPE_SEALED_GAUNTLETS_OF_NIGHTMARE_60, 1 )
        }

        if ( chance < 92 ) {
            return QuestHelper.rewardSingleItem( player, RECIPE_SEALED_HELM_OF_NIGHTMARE_60, 1 )
        }

        return QuestHelper.rewardMultipleItems( player, 1, RECIPE_SEALED_BOOTS_OF_NIGHTMARE_60, RECIPE_SEALED_GAUNTLETS_OF_NIGHTMARE_60, RECIPE_SEALED_HELM_OF_NIGHTMARE_60 )
    }

    rewardTallumItems( player: L2PcInstance ): Promise<void> {
        let chance = _.random( 100 )

        if ( chance < 10 ) {
            return QuestHelper.rewardSingleItem( player, SEALED_TALLUM_BOOTS_LINING, 1 )
        }

        if ( chance < 20 ) {
            return QuestHelper.rewardSingleItem( player, SEALED_TALLUM_GLOVES_DESIGN, 1 )
        }

        if ( chance < 30 ) {
            return QuestHelper.rewardSingleItem( player, SEALED_TALLUM_HELM_DESIGN, 1 )
        }

        if ( chance < 40 ) {
            return QuestHelper.rewardMultipleItems( player, 1, SEALED_TALLUM_BOOTS_LINING, SEALED_TALLUM_GLOVES_DESIGN, SEALED_TALLUM_HELM_DESIGN )
        }

        if ( chance < 51 ) {
            return QuestHelper.rewardSingleItem( player, RECIPE_SEALED_TALLUM_BOOTS_60, 1 )
        }

        if ( chance < 62 ) {
            return QuestHelper.rewardSingleItem( player, RECIPE_SEALED_TALLUM_GLOVES_60, 1 )
        }

        if ( chance < 79 ) {
            return QuestHelper.rewardSingleItem( player, RECIPE_SEALED_TALLUM_HELMET_60, 1 )
        }

        return QuestHelper.rewardMultipleItems( player, 1, RECIPE_SEALED_TALLUM_BOOTS_60, RECIPE_SEALED_TALLUM_GLOVES_60, RECIPE_SEALED_TALLUM_HELMET_60 )
    }

    takeAllItems( player: L2PcInstance ): Promise<void> {
        return QuestHelper.takeMultipleItems( player,
                -1,
                BLUEPRINT_TOWER_OF_INSOLENCE_1ST_FLOOR,
                BLUEPRINT_TOWER_OF_INSOLENCE_2ND_FLOOR,
                BLUEPRINT_TOWER_OF_INSOLENCE_3RD_FLOOR,
                BLUEPRINT_TOWER_OF_INSOLENCE_4TH_FLOOR,
                BLUEPRINT_TOWER_OF_INSOLENCE_5TH_FLOOR,
                BLUEPRINT_TOWER_OF_INSOLENCE_6TH_FLOOR,
                BLUEPRINT_TOWER_OF_INSOLENCE_7TH_FLOOR,
                BLUEPRINT_TOWER_OF_INSOLENCE_8TH_FLOOR,
                BLUEPRINT_TOWER_OF_INSOLENCE_9TH_FLOOR,
                BLUEPRINT_TOWER_OF_INSOLENCE_10TH_FLOOR,
                BLUEPRINT_TOWER_OF_INSOLENCE_11TH_FLOOR,
                BLUEPRINT_TOWER_OF_INSOLENCE_12TH_FLOOR,
                BLUEPRINT_TOWER_OF_INSOLENCE_13TH_FLOOR )
    }
}