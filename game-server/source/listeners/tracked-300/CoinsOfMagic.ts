import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import _ from 'lodash'

const PANO = 30078
const COLLOB = 30092
const RAPIN = 30165
const HAGGER = 30183
const STAN = 30200
const WAREHOUSE_KEEPER_SORINT = 30232
const RESEARCHER_LORAIN = 30673
const BLACKSMITH_DUNING = 30688
const MAGISTER_PAGE = 30696
const UNION_PRESIDENT_BERNARD = 30702
const HEAD_BLACKSMITH_FERRIS = 30847

const Q_BLOOD_MEDUSA = 3472
const Q_BLOOD_WEREWOLF = 3473
const Q_BLOOD_BASILISK = 3474
const Q_BLOOD_DREVANUL = 3475
const Q_BLOOD_SUCCUBUS = 3476
const Q_BLOOD_DRAGON = 3477
const Q_BERETHS_BLOOD_DRAGON = 3478
const Q_MANAKS_BLOOD_WEREWOLF = 3479
const Q_NIAS_BLOOD_MEDUSA = 3480
const Q_GOLD_DRAGON = 3481
const Q_GOLD_WYVERN = 3482
const Q_GOLD_KNIGHT = 3483
const Q_GOLD_GIANT = 3484
const Q_GOLD_DRAKE = 3485
const Q_GOLD_WYRM = 3486
const Q_BERETHS_GOLD_DRAGON = 3487
const Q_MANAKS_GOLD_GIANT = 3488
const Q_NIAS_GOLD_WYVERN = 3489
const Q_SILVER_UNICORN = 3490
const Q_SILVER_FAIRY = 3491
const Q_SILVER_DRYAD = 3492
const Q_SILVER_DRAGON = 3493
const Q_SILVER_GOLEM = 3494
const Q_SILVER_UNDINE = 3495
const Q_BERETHS_SILVER_DRAGON = 3496
const Q_MANAKS_SILVER_DRYAD = 3497
const Q_NIAS_SILVER_FAIRY = 3498
const Q_COIN_DIAGRAM = 3811
const Q_KALDIS_GOLD_DRAGON = 3812
const Q_CC_MEMBERSHIP_1 = 3813
const Q_CC_MEMBERSHIP_2 = 3814
const Q_CC_MEMBERSHIP_3 = 3815

const HEADLESS_KNIGHT = 20146
const OEL_MAHUM = 20161
const SHACKLE = 20235
const ROYAL_CAVE_SERVANT = 20240
const MALRUK_SUCCUBUS_TUREN = 20245
const ROYAL_CAVE_SERVANT_HOLD = 20276
const SHACKLE_HOLD = 20279
const HEADLESS_KNIGHT_HOLD = 20280
const H_MALRUK_SUCCUBUS_TUREN = 20284
const BYFOOT = 20568
const BYFOOT_SIGEL = 20569
const TARLK_BUGBEAR_BOSS = 20572
const OEL_MAHUM_WARRIOR = 20575
const OEL_MAHUM_WITCH_DOCTOR = 20576
const TIMAK_ORC = 20583
const TIMAK_ORC_ARCHER = 20584
const TIMAK_ORC_SOLDIER = 20585
const TIMAK_ORC_SHAMAN = 20587
const LAKIN = 20604
const HARIT_LIZARDMAN_SHAMAN = 20644
const HARIT_LIZARDM_MATRIARCH = 20645
const HATAR_HANISHEE = 20663
const DOOM_KNIGHT = 20674
const PUNISHMENT_OF_UNDEAD = 20678
const VANOR_SILENOS_SHAMAN = 20685
const HUNGRY_CORPSE = 20954
const NIHIL_INVADER = 20957
const DARK_GUARD = 20959
const BLOODY_GHOST = 20960
const FLOAT_OF_GRAVE = 21003
const DOOM_SERVANT = 21006
const DOOM_ARCHER = 21008
const KUKABURO = 21274
const KUKABURO_A = 21275
const KUKABURO_B = 21276
const ANTELOPE = 21278
const ANTELOPE_A = 21279
const ANTELOPE_B = 21280
const BANDERSNATCH = 21282
const BANDERSNATCH_A = 21283
const BANDERSNATCH_B = 21284
const BUFFALO = 21286
const BUFFALO_A = 21287
const BUFFALO_B = 21288
const BRILLIANT_CLAW = 21521
const BRILLIANT_CLAW_1 = 21522
const BRILLIANT_WISDOM = 21526
const BRILLIANT_VENGEANCE = 21531
const BRILLIANT_VENGEANCE_1 = 21658
const BRILLIANT_ANGUISH = 21539
const BRILLIANT_ANGUISH_1 = 21540

const DEMON_STAFF = 206
const DARK_SCREAMER = 233
const WIDOW_MAKER = 303
const SWORD_OF_LIMIT = 132
const DEMONS_BOOTS = 2435
const DEMONS_HOSE = 472
const DEMONS_GLOVES = 2459
const FULL_PLATE_HELMET = 2414
const MOONSTONE_EARING = 852
const NASSENS_EARING = 855
const RING_OF_BINDING = 886
const NECKLACE_OF_PROTECTION = 916

const minimumLevel = 40
const variableNames = {
    weightPoint: 'wp',
    one: 'o',
    two: 't',
    three: 'r',
    flag: 'f',
}

export class CoinsOfMagic extends ListenerLogic {
    constructor() {
        super( 'Q00336_CoinsOfMagic', 'listeners/tracked-300/CoinsOfMagic.ts' )
        this.questId = 336
        this.questItemIds = [
            Q_COIN_DIAGRAM,
            Q_KALDIS_GOLD_DRAGON,
            Q_CC_MEMBERSHIP_1,
            Q_CC_MEMBERSHIP_2,
            Q_CC_MEMBERSHIP_3,
        ]
    }

    async firstStepLogic(
            player: L2PcInstance,
            state: QuestState,
            npcId: number,
            itemId: number,
            amount: number,
            weightValue: number ): Promise<string> {

        if ( QuestHelper.getQuestItemsCount( player, itemId ) < amount ) {
            return this.getPath( `${ npcId }-10.html` )
        }

        state.setVariable( variableNames.one, _.random( 1, 3 ) + _.random( 1, 3 ) * 4 + _.random( 1, 3 ) * 16 )
        state.setVariable( variableNames.flag, 1 )
        state.setVariable( variableNames.weightPoint, weightValue )

        await QuestHelper.takeSingleItem( player, itemId, amount )
        return this.getPath( `${ npcId }-11.html` )
    }

    getAttackableKillIds(): Array<number> {
        return [
            HEADLESS_KNIGHT,
            OEL_MAHUM,
            SHACKLE,
            ROYAL_CAVE_SERVANT,
            MALRUK_SUCCUBUS_TUREN,
            ROYAL_CAVE_SERVANT_HOLD,
            SHACKLE_HOLD,
            HEADLESS_KNIGHT_HOLD,
            H_MALRUK_SUCCUBUS_TUREN,
            BYFOOT,
            BYFOOT_SIGEL,
            TARLK_BUGBEAR_BOSS,
            OEL_MAHUM_WARRIOR,
            OEL_MAHUM_WITCH_DOCTOR,
            TIMAK_ORC,
            TIMAK_ORC_ARCHER,
            TIMAK_ORC_SOLDIER,
            TIMAK_ORC_SHAMAN,
            LAKIN,
            HARIT_LIZARDMAN_SHAMAN,
            HARIT_LIZARDM_MATRIARCH,
            HATAR_HANISHEE,
            DOOM_KNIGHT,
            PUNISHMENT_OF_UNDEAD,
            VANOR_SILENOS_SHAMAN,
            HUNGRY_CORPSE,
            NIHIL_INVADER,
            DARK_GUARD,
            BLOODY_GHOST,
            FLOAT_OF_GRAVE,
            DOOM_SERVANT,
            DOOM_ARCHER,
            KUKABURO,
            KUKABURO_A,
            KUKABURO_B,
            ANTELOPE,
            ANTELOPE_A,
            ANTELOPE_B,
            BANDERSNATCH,
            BANDERSNATCH_A,
            BANDERSNATCH_B,
            BUFFALO,
            BUFFALO_A,
            BUFFALO_B,
            BRILLIANT_CLAW,
            BRILLIANT_CLAW_1,
            BRILLIANT_WISDOM,
            BRILLIANT_VENGEANCE,
            BRILLIANT_VENGEANCE_1,
            BRILLIANT_ANGUISH,
            BRILLIANT_ANGUISH_1,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00336_CoinsOfMagic'
    }

    getQuestStartIds(): Array<number> {
        return [ WAREHOUSE_KEEPER_SORINT ]
    }

    getTalkIds(): Array<number> {
        return [
            PANO,
            COLLOB,
            RAPIN,
            HAGGER,
            STAN,
            RESEARCHER_LORAIN,
            BLACKSMITH_DUNING,
            MAGISTER_PAGE,
            UNION_PRESIDENT_BERNARD,
            HEAD_BLACKSMITH_FERRIS,
        ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        switch ( data.npcId ) {
            case HARIT_LIZARDMAN_SHAMAN:
            case HARIT_LIZARDM_MATRIARCH:
                let state: QuestState = this.getRandomPlayerState( L2World.getPlayer( data.playerId ), L2World.getObjectById( data.targetId ) as L2Npc )
                if ( !state || _.random( 1000 ) >= QuestHelper.getAdjustedChance( Q_KALDIS_GOLD_DRAGON, 63, data.isChampion ) ) {
                    return
                }

                await QuestHelper.rewardSingleQuestItem( state.getPlayer(), Q_KALDIS_GOLD_DRAGON, 1, data.isChampion )

                state.setConditionWithSound( 3 )
                state.showQuestionMark( 336 )

                return
        }

        let player: L2PcInstance = this.getRandomPlayerForMemo( L2World.getPlayer( data.playerId ), L2World.getObjectById( data.targetId ) as L2Npc )
        if ( !player ) {
            return
        }

        switch ( data.npcId ) {
            case SHACKLE:
            case SHACKLE_HOLD:
                if ( _.random( 1000 ) < QuestHelper.getAdjustedChance( Q_GOLD_WYVERN, 70, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, Q_GOLD_WYVERN, 1, data.isChampion )
                }

                return

            case HEADLESS_KNIGHT:
            case TIMAK_ORC:
                if ( _.random( 1000 ) < QuestHelper.getAdjustedChance( Q_GOLD_WYVERN, 80, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, Q_GOLD_WYVERN, 1, data.isChampion )
                }

                return

            case HEADLESS_KNIGHT_HOLD:
                if ( _.random( 1000 ) < QuestHelper.getAdjustedChance( Q_GOLD_WYVERN, 85, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, Q_GOLD_WYVERN, 1, data.isChampion )
                }

                return

            case ROYAL_CAVE_SERVANT:
            case MALRUK_SUCCUBUS_TUREN:
            case ROYAL_CAVE_SERVANT_HOLD:
            case KUKABURO_B:
            case ANTELOPE:
            case ANTELOPE_A:
            case ANTELOPE_B:
            case H_MALRUK_SUCCUBUS_TUREN:
                if ( _.random( 1000 ) < QuestHelper.getAdjustedChance( Q_GOLD_WYVERN, 100, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, Q_GOLD_WYVERN, 1, data.isChampion )
                }

                return

            case BUFFALO:
            case BUFFALO_A:
            case BUFFALO_B:
            case KUKABURO:
            case KUKABURO_A:
                if ( _.random( 1000 ) < QuestHelper.getAdjustedChance( Q_GOLD_WYVERN, 110, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, Q_GOLD_WYVERN, 1, data.isChampion )
                }

                return

            case DOOM_SERVANT:
                if ( _.random( 1000 ) < QuestHelper.getAdjustedChance( Q_GOLD_WYVERN, 140, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, Q_GOLD_WYVERN, 1, data.isChampion )
                }

                return

            case DOOM_KNIGHT:
                if ( _.random( 1000 ) < QuestHelper.getAdjustedChance( Q_GOLD_WYVERN, 210, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, Q_GOLD_WYVERN, 1, data.isChampion )
                }

                return

            case VANOR_SILENOS_SHAMAN:
                if ( _.random( 1000 ) < QuestHelper.getAdjustedChance( Q_GOLD_WYVERN, 70, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, Q_SILVER_UNICORN, 1, data.isChampion )
                }

                return

            case BLOODY_GHOST:
            case TARLK_BUGBEAR_BOSS:
            case OEL_MAHUM:
                if ( _.random( 1000 ) < QuestHelper.getAdjustedChance( Q_GOLD_WYVERN, 80, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, Q_SILVER_UNICORN, 1, data.isChampion )
                }

                return

            case OEL_MAHUM_WARRIOR:
                if ( _.random( 1000 ) < QuestHelper.getAdjustedChance( Q_SILVER_UNICORN, 90, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, Q_SILVER_UNICORN, 1, data.isChampion )
                }

                return

            case HUNGRY_CORPSE:
                if ( _.random( 1000 ) < QuestHelper.getAdjustedChance( Q_SILVER_UNICORN, 100, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, Q_SILVER_UNICORN, 1, data.isChampion )
                }

                return

            case BYFOOT:
                if ( _.random( 1000 ) < QuestHelper.getAdjustedChance( Q_SILVER_UNICORN, 110, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, Q_SILVER_UNICORN, 1, data.isChampion )
                }

                return

            case BYFOOT_SIGEL:
                if ( _.random( 1000 ) < QuestHelper.getAdjustedChance( Q_SILVER_UNICORN, 120, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, Q_SILVER_UNICORN, 1, data.isChampion )
                }

                return

            case DARK_GUARD:
            case BRILLIANT_CLAW:
            case BRILLIANT_CLAW_1:
                if ( _.random( 1000 ) < QuestHelper.getAdjustedChance( Q_SILVER_UNICORN, 150, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, Q_SILVER_UNICORN, 1, data.isChampion )
                }

                return

            case OEL_MAHUM_WITCH_DOCTOR:
                if ( _.random( 1000 ) < QuestHelper.getAdjustedChance( Q_SILVER_UNICORN, 200, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, Q_SILVER_UNICORN, 1, data.isChampion )
                }

                return

            case BRILLIANT_ANGUISH:
            case BRILLIANT_ANGUISH_1:
                if ( _.random( 1000 ) < QuestHelper.getAdjustedChance( Q_SILVER_UNICORN, 210, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, Q_SILVER_UNICORN, 1, data.isChampion )
                }

                return

            case LAKIN:
                if ( _.random( 1000 ) < QuestHelper.getAdjustedChance( Q_BLOOD_MEDUSA, 60, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, Q_BLOOD_MEDUSA, 1, data.isChampion )
                }

                return

            case HATAR_HANISHEE:
                if ( _.random( 1000 ) < QuestHelper.getAdjustedChance( Q_BLOOD_MEDUSA, 70, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, Q_BLOOD_MEDUSA, 1, data.isChampion )
                }

                return

            case PUNISHMENT_OF_UNDEAD:
                if ( _.random( 1000 ) < QuestHelper.getAdjustedChance( Q_BLOOD_MEDUSA, 80, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, Q_BLOOD_MEDUSA, 1, data.isChampion )
                }

                return

            case FLOAT_OF_GRAVE:
            case BANDERSNATCH_A:
            case BANDERSNATCH_B:
                if ( _.random( 1000 ) < QuestHelper.getAdjustedChance( Q_BLOOD_MEDUSA, 90, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, Q_BLOOD_MEDUSA, 1, data.isChampion )
                }

                return

            case BANDERSNATCH:
                if ( _.random( 1000 ) < QuestHelper.getAdjustedChance( Q_BLOOD_MEDUSA, 100, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, Q_BLOOD_MEDUSA, 1, data.isChampion )
                }

                return

            case NIHIL_INVADER:
                if ( _.random( 1000 ) < QuestHelper.getAdjustedChance( Q_BLOOD_MEDUSA, 110, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, Q_BLOOD_MEDUSA, 1, data.isChampion )
                }

                return

            case TIMAK_ORC_SHAMAN:
                if ( _.random( 1000 ) < QuestHelper.getAdjustedChance( Q_BLOOD_MEDUSA, 130, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, Q_BLOOD_MEDUSA, 1, data.isChampion )
                }

                return

            case TIMAK_ORC_ARCHER:
            case TIMAK_ORC_SOLDIER:
                if ( _.random( 1000 ) < QuestHelper.getAdjustedChance( Q_BLOOD_MEDUSA, 140, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, Q_BLOOD_MEDUSA, 1, data.isChampion )
                }

                return

            case DOOM_ARCHER:
            case BRILLIANT_WISDOM:
            case BRILLIANT_VENGEANCE:
            case BRILLIANT_VENGEANCE_1:
                if ( _.random( 1000 ) < QuestHelper.getAdjustedChance( Q_BLOOD_MEDUSA, 160, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, Q_BLOOD_MEDUSA, 1, data.isChampion )
                }

                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        if ( data.eventName === 'QUEST_ACCEPTED' ) {
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ACCEPT )
            if ( !QuestHelper.hasQuestItem( player, Q_COIN_DIAGRAM ) ) {
                await QuestHelper.giveSingleItem( player, Q_COIN_DIAGRAM, 1 )
            }

            state.setMemoState( 1 )
            state.startQuest()
            state.showQuestionMark( 336 )

            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

            return this.getPath( '30232-05.htm' )
        }

        if ( data.eventName.endsWith( '.htm' ) ) {
            return this.getPath( data.eventName )
        }

        switch ( data.characterNpcId ) {
            case PANO:
            case COLLOB:
            case RAPIN:
            case HAGGER:
            case STAN:
            case RESEARCHER_LORAIN:
            case BLACKSMITH_DUNING:
            case MAGISTER_PAGE:
            case HEAD_BLACKSMITH_FERRIS:
                switch ( data.eventName ) {
                    case '1':
                        state.setVariable( variableNames.two, 11 )
                        return this.getPath( `${ data.characterNpcId }-02.html` )

                    case '2':
                        state.setVariable( variableNames.two, 21 )
                        return this.getPath( `${ data.characterNpcId }-03.html` )

                    case '3':
                        state.setVariable( variableNames.two, 31 )
                        return this.getPath( `${ data.characterNpcId }-04.html` )

                    case '4':
                        state.setVariable( variableNames.two, 42 )
                        return this.getPath( `${ data.characterNpcId }-05.html` )

                    case '5':
                        return this.getPath( `${ data.characterNpcId }-06.html` )

                    case '9':
                        return this.getPath( `${ data.characterNpcId }-53.html` )

                    case '13':
                        if ( state.getVariable( variableNames.flag ) === 1 ) {
                            state.setVariable( variableNames.flag, 16 )

                            return this.getPath( `${ data.characterNpcId }-14.html` )
                        }

                        break

                    case '14':
                        if ( state.getVariable( variableNames.flag ) === 1 ) {
                            state.setVariable( variableNames.flag, 32 )
                            return this.getPath( `${ data.characterNpcId }-15.html` )
                        }
                        break

                    case '15':
                        if ( state.getVariable( variableNames.flag ) === 1 ) {
                            state.setVariable( variableNames.flag, 48 )
                            return this.getPath( `${ data.characterNpcId }-16.html` )
                        }

                        break

                    case '16':
                        state.setVariable( variableNames.flag, state.getVariable( variableNames.flag ) + 4 )
                        return this.getPath( `${ data.characterNpcId }-17.html` )

                    case '17':
                        state.setVariable( variableNames.flag, state.getVariable( variableNames.flag ) + 8 )
                        return this.getPath( `${ data.characterNpcId }-18.html` )

                    case '18':
                        state.setVariable( variableNames.flag, state.getVariable( variableNames.flag ) + 12 )
                        return this.getPath( `${ data.characterNpcId }-19.html` )

                    case '22':
                        return this.getPath( `${ data.characterNpcId }-01.html` )
                }

                break
        }

        switch ( data.characterNpcId ) {
            case PANO:
                switch ( data.eventName ) {
                    case '6':
                        return this.processFirstSteps( player, state, PANO, 1, 4, Q_SILVER_DRYAD, Q_SILVER_UNDINE, 1, Q_GOLD_GIANT, Q_SILVER_DRYAD, Q_BLOOD_BASILISK )

                    case '7':
                        return this.processFirstSteps( player, state, PANO, 2, 8, Q_SILVER_DRYAD, Q_SILVER_UNDINE, 1, Q_GOLD_GIANT, Q_SILVER_DRYAD, Q_BLOOD_BASILISK )

                    case '8':
                        return this.processFirstSteps( player, state, PANO, 3, 9, Q_SILVER_DRYAD, Q_SILVER_UNDINE, 1, Q_GOLD_GIANT, Q_SILVER_DRYAD, Q_BLOOD_BASILISK )

                    case '10':
                        return this.processSecondSteps( player, state, PANO, 1, Q_SILVER_DRYAD, Q_SILVER_UNDINE, Q_BERETHS_SILVER_DRAGON, Q_GOLD_GIANT, Q_GOLD_WYRM, Q_GOLD_DRAGON, Q_SILVER_DRYAD, Q_SILVER_UNDINE, Q_SILVER_DRAGON, Q_BLOOD_BASILISK, Q_BLOOD_SUCCUBUS, Q_BLOOD_DRAGON )

                    case '11':
                        return this.processSecondSteps( player, state, PANO, 5, Q_SILVER_DRYAD, Q_SILVER_UNDINE, Q_BERETHS_SILVER_DRAGON, Q_GOLD_GIANT, Q_GOLD_WYRM, Q_GOLD_DRAGON, Q_SILVER_DRYAD, Q_SILVER_UNDINE, Q_SILVER_DRAGON, Q_BLOOD_BASILISK, Q_BLOOD_SUCCUBUS, Q_BLOOD_DRAGON )

                    case '12':
                        return this.processSecondSteps( player, state, PANO, 10, Q_SILVER_DRYAD, Q_SILVER_UNDINE, Q_BERETHS_SILVER_DRAGON, Q_GOLD_GIANT, Q_GOLD_WYRM, Q_GOLD_DRAGON, Q_SILVER_DRYAD, Q_SILVER_UNDINE, Q_SILVER_DRAGON, Q_BLOOD_BASILISK, Q_BLOOD_SUCCUBUS, Q_BLOOD_DRAGON )

                    case '19':
                        return this.processThirdSteps( player, state, PANO, 1, Q_BERETHS_SILVER_DRAGON, Q_GOLD_DRAGON, Q_SILVER_DRAGON, Q_SILVER_DRAGON )

                    case '20':
                        return this.processThirdSteps( player, state, PANO, 2, Q_BERETHS_SILVER_DRAGON, Q_GOLD_DRAGON, Q_SILVER_DRAGON, Q_SILVER_DRAGON )

                    case '21':
                        return this.processThirdSteps( player, state, PANO, 3, Q_BERETHS_SILVER_DRAGON, Q_GOLD_DRAGON, Q_SILVER_DRAGON, Q_SILVER_DRAGON )
                }

                break

            case COLLOB:
                switch ( data.eventName ) {
                    case '6':
                        return this.processFirstSteps( player, state, COLLOB, 1, 4, Q_GOLD_WYRM, Q_GOLD_GIANT, 1, Q_GOLD_WYRM, Q_SILVER_UNDINE, Q_BLOOD_SUCCUBUS )

                    case '7':
                        return this.processFirstSteps( player, state, COLLOB, 2, 8, Q_GOLD_WYRM, Q_GOLD_GIANT, 1, Q_GOLD_WYRM, Q_SILVER_UNDINE, Q_BLOOD_SUCCUBUS )

                    case '8':
                        return this.processFirstSteps( player, state, COLLOB, 3, 9, Q_GOLD_WYRM, Q_GOLD_GIANT, 1, Q_GOLD_WYRM, Q_SILVER_UNDINE, Q_BLOOD_SUCCUBUS )

                    case '10':
                        return this.processSecondSteps( player, state, COLLOB, 1, Q_GOLD_GIANT, Q_GOLD_WYRM, Q_BERETHS_GOLD_DRAGON, Q_GOLD_GIANT, Q_GOLD_WYRM, Q_GOLD_DRAGON, Q_SILVER_DRYAD, Q_SILVER_UNDINE, Q_SILVER_DRAGON, Q_BLOOD_BASILISK, Q_BLOOD_SUCCUBUS, Q_BLOOD_DRAGON )

                    case '11':
                        return this.processSecondSteps( player, state, COLLOB, 5, Q_GOLD_GIANT, Q_GOLD_WYRM, Q_BERETHS_GOLD_DRAGON, Q_GOLD_GIANT, Q_GOLD_WYRM, Q_GOLD_DRAGON, Q_SILVER_DRYAD, Q_SILVER_UNDINE, Q_SILVER_DRAGON, Q_BLOOD_BASILISK, Q_BLOOD_SUCCUBUS, Q_BLOOD_DRAGON )

                    case '12':
                        return this.processSecondSteps( player, state, COLLOB, 10, Q_GOLD_GIANT, Q_GOLD_WYRM, Q_BERETHS_GOLD_DRAGON, Q_GOLD_GIANT, Q_GOLD_WYRM, Q_GOLD_DRAGON, Q_SILVER_DRYAD, Q_SILVER_UNDINE, Q_SILVER_DRAGON, Q_BLOOD_BASILISK, Q_BLOOD_SUCCUBUS, Q_BLOOD_DRAGON )

                    case '19':
                        return this.processThirdSteps( player, state, COLLOB, 1, Q_BERETHS_GOLD_DRAGON, Q_GOLD_DRAGON, Q_SILVER_DRAGON, Q_BLOOD_DRAGON )

                    case '20':
                        return this.processThirdSteps( player, state, COLLOB, 2, Q_BERETHS_GOLD_DRAGON, Q_GOLD_DRAGON, Q_SILVER_DRAGON, Q_BLOOD_DRAGON )

                    case '21':
                        return this.processThirdSteps( player, state, COLLOB, 3, Q_BERETHS_GOLD_DRAGON, Q_GOLD_DRAGON, Q_SILVER_DRAGON, Q_BLOOD_DRAGON )
                }

                break

            case RAPIN:
                switch ( data.eventName ) {
                    case '6':
                        return this.processFirstSteps( player, state, RAPIN, 1, 3, Q_BLOOD_WEREWOLF, Q_BLOOD_DREVANUL, 1, Q_SILVER_GOLEM, Q_SILVER_FAIRY, Q_GOLD_DRAKE )

                    case '7':
                        return this.processFirstSteps( player, state, RAPIN, 2, 7, Q_BLOOD_WEREWOLF, Q_BLOOD_DREVANUL, 1, Q_SILVER_GOLEM, Q_SILVER_FAIRY, Q_GOLD_DRAKE )

                    case '8':
                        return this.processFirstSteps( player, state, RAPIN, 3, 9, Q_BLOOD_WEREWOLF, Q_BLOOD_DREVANUL, 1, Q_SILVER_GOLEM, Q_SILVER_FAIRY, Q_GOLD_DRAKE )

                    case '10':
                        return this.processSecondSteps( player, state, RAPIN, 1, Q_BLOOD_WEREWOLF, Q_BLOOD_DREVANUL, Q_MANAKS_BLOOD_WEREWOLF, Q_SILVER_GOLEM, Q_SILVER_FAIRY, Q_SILVER_UNDINE, Q_SILVER_GOLEM, Q_SILVER_FAIRY, Q_SILVER_DRYAD, Q_GOLD_DRAKE, Q_GOLD_KNIGHT, Q_GOLD_WYRM )

                    case '11':
                        return this.processSecondSteps( player, state, RAPIN, 5, Q_BLOOD_WEREWOLF, Q_BLOOD_DREVANUL, Q_MANAKS_BLOOD_WEREWOLF, Q_SILVER_GOLEM, Q_SILVER_FAIRY, Q_SILVER_UNDINE, Q_SILVER_GOLEM, Q_SILVER_FAIRY, Q_SILVER_DRYAD, Q_GOLD_DRAKE, Q_GOLD_KNIGHT, Q_GOLD_WYRM )

                    case '12':
                        return this.processSecondSteps( player, state, RAPIN, 10, Q_BLOOD_WEREWOLF, Q_BLOOD_DREVANUL, Q_MANAKS_BLOOD_WEREWOLF, Q_SILVER_GOLEM, Q_SILVER_FAIRY, Q_SILVER_UNDINE, Q_SILVER_GOLEM, Q_SILVER_FAIRY, Q_SILVER_DRYAD, Q_GOLD_DRAKE, Q_GOLD_KNIGHT, Q_GOLD_WYRM )

                    case '19':
                        return this.processThirdSteps( player, state, RAPIN, 1, Q_MANAKS_BLOOD_WEREWOLF, Q_SILVER_UNDINE, Q_SILVER_DRYAD, Q_GOLD_WYRM )

                    case '20':
                        return this.processThirdSteps( player, state, RAPIN, 2, Q_MANAKS_BLOOD_WEREWOLF, Q_SILVER_UNDINE, Q_SILVER_DRYAD, Q_GOLD_WYRM )

                    case '21':
                        return this.processThirdSteps( player, state, RAPIN, 3, Q_MANAKS_BLOOD_WEREWOLF, Q_SILVER_UNDINE, Q_SILVER_DRYAD, Q_GOLD_WYRM )
                }
                break

            case HAGGER:
                switch ( data.eventName ) {
                    case '6':
                        return this.processFirstSteps( player, state, HAGGER, 1, 4, Q_SILVER_UNICORN, 0, 2, Q_BLOOD_MEDUSA, Q_SILVER_UNICORN, Q_GOLD_WYVERN )

                    case '7':
                        return this.processFirstSteps( player, state, HAGGER, 2, 8, Q_SILVER_UNICORN, 0, 2, Q_BLOOD_MEDUSA, Q_SILVER_UNICORN, Q_GOLD_WYVERN )

                    case '8':
                        return this.processFirstSteps( player, state, HAGGER, 3, 9, Q_SILVER_UNICORN, 0, 2, Q_BLOOD_MEDUSA, Q_SILVER_UNICORN, Q_GOLD_WYVERN )

                    case '10':
                        return this.processSecondSingleStep( player, state, HAGGER, 1, Q_SILVER_UNICORN, 2, Q_NIAS_SILVER_FAIRY, Q_BLOOD_MEDUSA, Q_BLOOD_WEREWOLF, Q_SILVER_UNICORN, Q_SILVER_GOLEM, Q_GOLD_WYVERN, Q_GOLD_DRAKE )

                    case '11':
                        return this.processSecondSingleStep( player, state, HAGGER, 5, Q_SILVER_UNICORN, 2, Q_NIAS_SILVER_FAIRY, Q_BLOOD_MEDUSA, Q_BLOOD_WEREWOLF, Q_SILVER_UNICORN, Q_SILVER_GOLEM, Q_GOLD_WYVERN, Q_GOLD_DRAKE )

                    case '12':
                        return this.processSecondSingleStep( player, state, HAGGER, 10, Q_SILVER_UNICORN, 2, Q_NIAS_SILVER_FAIRY, Q_BLOOD_MEDUSA, Q_BLOOD_WEREWOLF, Q_SILVER_UNICORN, Q_SILVER_GOLEM, Q_GOLD_WYVERN, Q_GOLD_DRAKE )

                    case '19':
                        return this.processThirdSteps( player, state, HAGGER, 1, Q_NIAS_SILVER_FAIRY, Q_BLOOD_WEREWOLF, Q_SILVER_GOLEM, Q_GOLD_DRAKE )

                    case '20':
                        return this.processThirdSteps( player, state, HAGGER, 2, Q_NIAS_SILVER_FAIRY, Q_BLOOD_WEREWOLF, Q_SILVER_GOLEM, Q_GOLD_DRAKE )

                    case '21':
                        return this.processThirdSteps( player, state, HAGGER, 3, Q_NIAS_SILVER_FAIRY, Q_BLOOD_WEREWOLF, Q_SILVER_GOLEM, Q_GOLD_DRAKE )
                }

                break

            case STAN:
                switch ( data.eventName ) {
                    case '6':
                        return this.processFirstSteps( player, state, STAN, 1, 3, Q_SILVER_FAIRY, Q_SILVER_GOLEM, 1, Q_SILVER_FAIRY, Q_BLOOD_WEREWOLF, Q_GOLD_KNIGHT )

                    case '7':
                        return this.processFirstSteps( player, state, STAN, 2, 7, Q_SILVER_FAIRY, Q_SILVER_GOLEM, 1, Q_SILVER_FAIRY, Q_BLOOD_WEREWOLF, Q_GOLD_KNIGHT )

                    case '8':
                        return this.processFirstSteps( player, state, STAN, 3, 9, Q_SILVER_FAIRY, Q_SILVER_GOLEM, 1, Q_SILVER_FAIRY, Q_BLOOD_WEREWOLF, Q_GOLD_KNIGHT )

                    case '10':
                        return this.processSecondSteps( player, state, STAN, 1, Q_SILVER_FAIRY, Q_SILVER_GOLEM, Q_MANAKS_SILVER_DRYAD, Q_SILVER_FAIRY, Q_SILVER_GOLEM, Q_SILVER_DRYAD, Q_BLOOD_WEREWOLF, Q_BLOOD_DREVANUL, Q_BLOOD_BASILISK, Q_GOLD_KNIGHT, Q_GOLD_DRAKE, Q_GOLD_GIANT )

                    case '11':
                        return this.processSecondSteps( player, state, STAN, 5, Q_SILVER_FAIRY, Q_SILVER_GOLEM, Q_MANAKS_SILVER_DRYAD, Q_SILVER_FAIRY, Q_SILVER_GOLEM, Q_SILVER_DRYAD, Q_BLOOD_WEREWOLF, Q_BLOOD_DREVANUL, Q_BLOOD_BASILISK, Q_GOLD_KNIGHT, Q_GOLD_DRAKE, Q_GOLD_GIANT )

                    case '12':
                        return this.processSecondSteps( player, state, STAN, 10, Q_SILVER_FAIRY, Q_SILVER_GOLEM, Q_MANAKS_SILVER_DRYAD, Q_SILVER_FAIRY, Q_SILVER_GOLEM, Q_SILVER_DRYAD, Q_BLOOD_WEREWOLF, Q_BLOOD_DREVANUL, Q_BLOOD_BASILISK, Q_GOLD_KNIGHT, Q_GOLD_DRAKE, Q_GOLD_GIANT )

                    case '19':
                        return this.processThirdSteps( player, state, STAN, 1, Q_MANAKS_SILVER_DRYAD, Q_SILVER_DRYAD, Q_BLOOD_BASILISK, Q_GOLD_GIANT )

                    case '20':
                        return this.processThirdSteps( player, state, STAN, 2, Q_MANAKS_SILVER_DRYAD, Q_SILVER_DRYAD, Q_BLOOD_BASILISK, Q_GOLD_GIANT )

                    case '21':
                        return this.processThirdSteps( player, state, STAN, 3, Q_MANAKS_SILVER_DRYAD, Q_SILVER_DRYAD, Q_BLOOD_BASILISK, Q_GOLD_GIANT )
                }

                break

            case RESEARCHER_LORAIN:
                switch ( data.eventName ) {
                    case '6':
                        return this.processFirstSteps( player, state, RESEARCHER_LORAIN, 1, 4, Q_GOLD_WYVERN, 0, 2, Q_BLOOD_MEDUSA, Q_SILVER_UNICORN, Q_GOLD_WYVERN )

                    case '7':
                        return this.processFirstSteps( player, state, RESEARCHER_LORAIN, 2, 8, Q_GOLD_WYVERN, 0, 2, Q_BLOOD_MEDUSA, Q_SILVER_UNICORN, Q_GOLD_WYVERN )

                    case '8':
                        return this.processFirstSteps( player, state, RESEARCHER_LORAIN, 3, 9, Q_GOLD_WYVERN, 0, 2, Q_BLOOD_MEDUSA, Q_SILVER_UNICORN, Q_GOLD_WYVERN )

                    case '10':
                        return this.processSecondSingleStep( player, state, RESEARCHER_LORAIN, 1, Q_GOLD_WYVERN, 2, Q_NIAS_GOLD_WYVERN, Q_BLOOD_MEDUSA, Q_BLOOD_DREVANUL, Q_SILVER_UNICORN, Q_SILVER_GOLEM, Q_GOLD_WYVERN, Q_GOLD_KNIGHT )

                    case '11':
                        return this.processSecondSingleStep( player, state, RESEARCHER_LORAIN, 5, Q_GOLD_WYVERN, 2, Q_NIAS_GOLD_WYVERN, Q_BLOOD_MEDUSA, Q_BLOOD_DREVANUL, Q_SILVER_UNICORN, Q_SILVER_GOLEM, Q_GOLD_WYVERN, Q_GOLD_KNIGHT )

                    case '12':
                        return this.processSecondSingleStep( player, state, RESEARCHER_LORAIN, 10, Q_GOLD_WYVERN, 2, Q_NIAS_GOLD_WYVERN, Q_BLOOD_MEDUSA, Q_BLOOD_DREVANUL, Q_SILVER_UNICORN, Q_SILVER_GOLEM, Q_GOLD_WYVERN, Q_GOLD_KNIGHT )

                    case '19':
                        return this.processThirdSteps( player, state, RESEARCHER_LORAIN, 1, Q_NIAS_GOLD_WYVERN, Q_BLOOD_DREVANUL, Q_SILVER_GOLEM, Q_GOLD_KNIGHT )

                    case '20':
                        return this.processThirdSteps( player, state, RESEARCHER_LORAIN, 2, Q_NIAS_GOLD_WYVERN, Q_BLOOD_DREVANUL, Q_SILVER_GOLEM, Q_GOLD_KNIGHT )

                    case '21':
                        return this.processThirdSteps( player, state, RESEARCHER_LORAIN, 3, Q_NIAS_GOLD_WYVERN, Q_BLOOD_DREVANUL, Q_SILVER_GOLEM, Q_GOLD_KNIGHT )
                }

                break

            case BLACKSMITH_DUNING:
                switch ( data.eventName ) {
                    case '6':
                        return this.processFirstSteps( player, state, BLACKSMITH_DUNING, 1, 3, Q_GOLD_DRAKE, Q_GOLD_KNIGHT, 1, Q_SILVER_GOLEM, Q_BLOOD_DREVANUL, Q_GOLD_DRAKE )

                    case '7':
                        return this.processFirstSteps( player, state, BLACKSMITH_DUNING, 2, 7, Q_GOLD_DRAKE, Q_GOLD_KNIGHT, 1, Q_SILVER_GOLEM, Q_BLOOD_DREVANUL, Q_GOLD_DRAKE )

                    case '8':
                        return this.processFirstSteps( player, state, BLACKSMITH_DUNING, 3, 9, Q_GOLD_DRAKE, Q_GOLD_KNIGHT, 1, Q_SILVER_GOLEM, Q_BLOOD_DREVANUL, Q_GOLD_DRAKE )

                    case '10':
                        return this.processSecondSteps( player, state, BLACKSMITH_DUNING, 1, Q_GOLD_KNIGHT, Q_GOLD_DRAKE, Q_MANAKS_GOLD_GIANT, Q_SILVER_GOLEM, Q_SILVER_FAIRY, Q_SILVER_UNDINE, Q_BLOOD_DREVANUL, Q_BLOOD_WEREWOLF, Q_BLOOD_SUCCUBUS, Q_GOLD_DRAKE, Q_GOLD_KNIGHT, Q_GOLD_GIANT )

                    case '11':
                        return this.processSecondSteps( player, state, BLACKSMITH_DUNING, 5, Q_GOLD_KNIGHT, Q_GOLD_DRAKE, Q_MANAKS_GOLD_GIANT, Q_SILVER_GOLEM, Q_SILVER_FAIRY, Q_SILVER_UNDINE, Q_BLOOD_DREVANUL, Q_BLOOD_WEREWOLF, Q_BLOOD_SUCCUBUS, Q_GOLD_DRAKE, Q_GOLD_KNIGHT, Q_GOLD_GIANT )

                    case '12':
                        return this.processSecondSteps( player, state, BLACKSMITH_DUNING, 10, Q_GOLD_KNIGHT, Q_GOLD_DRAKE, Q_MANAKS_GOLD_GIANT, Q_SILVER_GOLEM, Q_SILVER_FAIRY, Q_SILVER_UNDINE, Q_BLOOD_DREVANUL, Q_BLOOD_WEREWOLF, Q_BLOOD_SUCCUBUS, Q_GOLD_DRAKE, Q_GOLD_KNIGHT, Q_GOLD_GIANT )

                    case '19':
                        return this.processThirdSteps( player, state, BLACKSMITH_DUNING, 1, Q_MANAKS_GOLD_GIANT, Q_SILVER_UNDINE, Q_BLOOD_SUCCUBUS, Q_GOLD_GIANT )

                    case '20':
                        return this.processThirdSteps( player, state, BLACKSMITH_DUNING, 2, Q_MANAKS_GOLD_GIANT, Q_SILVER_UNDINE, Q_BLOOD_SUCCUBUS, Q_GOLD_GIANT )

                    case '21':
                        return this.processThirdSteps( player, state, BLACKSMITH_DUNING, 3, Q_MANAKS_GOLD_GIANT, Q_SILVER_UNDINE, Q_BLOOD_SUCCUBUS, Q_GOLD_GIANT )
                }

                break

            case MAGISTER_PAGE:
                switch ( data.eventName ) {
                    case '6':
                        return this.processFirstSteps( player, state, MAGISTER_PAGE, 1, 4, Q_BLOOD_MEDUSA, 0, 2, Q_BLOOD_MEDUSA, Q_SILVER_UNICORN, Q_GOLD_WYVERN )

                    case '7':
                        return this.processFirstSteps( player, state, MAGISTER_PAGE, 2, 8, Q_BLOOD_MEDUSA, 0, 2, Q_BLOOD_MEDUSA, Q_SILVER_UNICORN, Q_GOLD_WYVERN )

                    case '8':
                        return this.processFirstSteps( player, state, MAGISTER_PAGE, 3, 9, Q_BLOOD_MEDUSA, 0, 2, Q_BLOOD_MEDUSA, Q_SILVER_UNICORN, Q_GOLD_WYVERN )

                    case '10':
                        return this.processSecondSingleStep( player, state, MAGISTER_PAGE, 1, Q_BLOOD_MEDUSA, 2, Q_NIAS_BLOOD_MEDUSA, Q_BLOOD_MEDUSA, Q_BLOOD_WEREWOLF, Q_SILVER_UNICORN, Q_SILVER_FAIRY, Q_GOLD_WYVERN, Q_GOLD_KNIGHT )

                    case '11':
                        return this.processSecondSingleStep( player, state, MAGISTER_PAGE, 5, Q_BLOOD_MEDUSA, 2, Q_NIAS_BLOOD_MEDUSA, Q_BLOOD_MEDUSA, Q_BLOOD_WEREWOLF, Q_SILVER_UNICORN, Q_SILVER_FAIRY, Q_GOLD_WYVERN, Q_GOLD_KNIGHT )

                    case '12':
                        return this.processSecondSingleStep( player, state, MAGISTER_PAGE, 10, Q_BLOOD_MEDUSA, 2, Q_NIAS_BLOOD_MEDUSA, Q_BLOOD_MEDUSA, Q_BLOOD_WEREWOLF, Q_SILVER_UNICORN, Q_SILVER_FAIRY, Q_GOLD_WYVERN, Q_GOLD_KNIGHT )

                    case '19':
                        return this.processThirdSteps( player, state, MAGISTER_PAGE, 1, Q_NIAS_BLOOD_MEDUSA, Q_BLOOD_WEREWOLF, Q_SILVER_FAIRY, Q_GOLD_KNIGHT )

                    case '20':
                        return this.processThirdSteps( player, state, MAGISTER_PAGE, 2, Q_NIAS_BLOOD_MEDUSA, Q_BLOOD_WEREWOLF, Q_SILVER_FAIRY, Q_GOLD_KNIGHT )

                    case '21':
                        return this.processThirdSteps( player, state, MAGISTER_PAGE, 3, Q_NIAS_BLOOD_MEDUSA, Q_BLOOD_WEREWOLF, Q_SILVER_FAIRY, Q_GOLD_KNIGHT )
                }

                break

            case HEAD_BLACKSMITH_FERRIS:
                switch ( data.eventName ) {
                    case '6':
                        return this.processFirstSteps( player, state, HEAD_BLACKSMITH_FERRIS, 1, 4, Q_BLOOD_BASILISK, Q_BLOOD_SUCCUBUS, 1, Q_GOLD_GIANT, Q_SILVER_DRYAD, Q_BLOOD_BASILISK )

                    case '7':
                        return this.processFirstSteps( player, state, HEAD_BLACKSMITH_FERRIS, 2, 8, Q_BLOOD_BASILISK, Q_BLOOD_SUCCUBUS, 1, Q_GOLD_GIANT, Q_SILVER_DRYAD, Q_BLOOD_BASILISK )

                    case '8':
                        return this.processFirstSteps( player, state, HEAD_BLACKSMITH_FERRIS, 3, 9, Q_BLOOD_BASILISK, Q_BLOOD_SUCCUBUS, 1, Q_GOLD_GIANT, Q_SILVER_DRYAD, Q_BLOOD_BASILISK )

                    case '10':
                        return this.processSecondSteps( player, state, HEAD_BLACKSMITH_FERRIS, 1, Q_BLOOD_BASILISK, Q_BLOOD_SUCCUBUS, Q_BERETHS_BLOOD_DRAGON, Q_GOLD_GIANT, Q_GOLD_WYRM, Q_GOLD_DRAGON, Q_SILVER_DRYAD, Q_SILVER_UNDINE, Q_SILVER_DRAGON, Q_BLOOD_BASILISK, Q_BLOOD_SUCCUBUS, Q_BLOOD_DRAGON )

                    case '11':
                        return this.processSecondSteps( player, state, HEAD_BLACKSMITH_FERRIS, 5, Q_BLOOD_BASILISK, Q_BLOOD_SUCCUBUS, Q_BERETHS_BLOOD_DRAGON, Q_GOLD_GIANT, Q_GOLD_WYRM, Q_GOLD_DRAGON, Q_SILVER_DRYAD, Q_SILVER_UNDINE, Q_SILVER_DRAGON, Q_BLOOD_BASILISK, Q_BLOOD_SUCCUBUS, Q_BLOOD_DRAGON )

                    case '12':
                        return this.processSecondSteps( player, state, HEAD_BLACKSMITH_FERRIS, 10, Q_BLOOD_BASILISK, Q_BLOOD_SUCCUBUS, Q_BERETHS_BLOOD_DRAGON, Q_GOLD_GIANT, Q_GOLD_WYRM, Q_GOLD_DRAGON, Q_SILVER_DRYAD, Q_SILVER_UNDINE, Q_SILVER_DRAGON, Q_BLOOD_BASILISK, Q_BLOOD_SUCCUBUS, Q_BLOOD_DRAGON )

                    case '19':
                        return this.processThirdSteps( player, state, HEAD_BLACKSMITH_FERRIS, 1, Q_BERETHS_BLOOD_DRAGON, Q_GOLD_DRAGON, Q_SILVER_DRAGON, Q_BLOOD_DRAGON )

                    case '20':
                        return this.processThirdSteps( player, state, HEAD_BLACKSMITH_FERRIS, 2, Q_BERETHS_BLOOD_DRAGON, Q_GOLD_DRAGON, Q_SILVER_DRAGON, Q_BLOOD_DRAGON )

                    case '21':
                        return this.processThirdSteps( player, state, HEAD_BLACKSMITH_FERRIS, 3, Q_BERETHS_BLOOD_DRAGON, Q_GOLD_DRAGON, Q_SILVER_DRAGON, Q_BLOOD_DRAGON )
                }

                break

            case UNION_PRESIDENT_BERNARD:
                switch ( data.eventName ) {
                    case '1':
                        return this.getPath( '30702-02.html' )

                    case '2':
                        state.setMemoState( 2 )
                        state.setConditionWithSound( 2, true )
                        state.showQuestionMark( 336 )

                        return this.getPath( '30702-03.html' )

                    case '3':
                        state.setMemoState( 2 )
                        state.setConditionWithSound( 2, true )
                        state.showQuestionMark( 336 )

                        return this.getPath( '30702-04.html' )

                    case '4':
                        state.setConditionWithSound( 7, true )
                        state.showQuestionMark( 336 )

                        return this.getPath( '30702-06.html' )

                }

                break

            case WAREHOUSE_KEEPER_SORINT:
                switch ( data.eventName ) {
                    case '1':
                        return this.getPath( '30232-03.html' )

                    case '2':
                        return this.getPath( '30232-04.html' )

                    case '3':
                        return this.getPath( '30232-08.html' )

                    case '4':
                        return this.getPath( '30232-09.html' )

                    case '5':
                        if ( QuestHelper.hasQuestItem( player, Q_CC_MEMBERSHIP_3 ) ) {
                            if ( QuestHelper.hasQuestItems( player, Q_BLOOD_DREVANUL, Q_BLOOD_WEREWOLF, Q_GOLD_KNIGHT, Q_GOLD_DRAKE, Q_SILVER_FAIRY, Q_SILVER_GOLEM ) ) {
                                state.setConditionWithSound( 9, true )
                                state.showQuestionMark( 336 )

                                await QuestHelper.takeMultipleItems( player, -1, Q_CC_MEMBERSHIP_3, Q_BLOOD_DREVANUL, Q_BLOOD_WEREWOLF, Q_GOLD_KNIGHT, Q_GOLD_DRAKE, Q_SILVER_FAIRY, Q_SILVER_GOLEM )
                                await QuestHelper.giveSingleItem( player, Q_CC_MEMBERSHIP_2, 1 )

                                return this.getPath( '30232-16.html' )
                            }

                            state.setConditionWithSound( 8, true )
                            state.showQuestionMark( 336 )

                            return this.getPath( '30232-13.html' )
                        }
                        if ( QuestHelper.hasQuestItem( player, Q_CC_MEMBERSHIP_2 ) ) {
                            if ( QuestHelper.hasQuestItems( player, Q_BLOOD_BASILISK, Q_BLOOD_SUCCUBUS, Q_GOLD_GIANT, Q_GOLD_WYRM, Q_SILVER_UNDINE, Q_SILVER_DRYAD ) ) {
                                state.setConditionWithSound( 11, true )
                                state.showQuestionMark( 336 )

                                await QuestHelper.takeMultipleItems( player, -1, Q_CC_MEMBERSHIP_2, Q_BLOOD_BASILISK, Q_BLOOD_SUCCUBUS, Q_GOLD_GIANT, Q_GOLD_WYRM, Q_SILVER_UNDINE, Q_SILVER_DRYAD )
                                await QuestHelper.giveSingleItem( player, Q_CC_MEMBERSHIP_1, 1 )

                                return this.getPath( '30232-17.html' )
                            }

                            state.setConditionWithSound( 10, true )
                            state.showQuestionMark( 336 )

                            return this.getPath( '30232-14.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, Q_CC_MEMBERSHIP_1 ) ) {
                            return this.getPath( '30232-15.html' )
                        }

                        break

                    case '6':
                        return this.getPath( '30232-18.html' )

                    case '7':
                        return this.getPath( '30232-19.html' )

                    case '8':
                        return this.getPath( '30232-20.html' )

                    case '9':
                        return this.getPath( '30232-21.html' )

                    case '10':
                        state.setConditionWithSound( 6, true )
                        state.showQuestionMark( 336 )

                        return this.getPath( '30232-22.html' )

                    case '11':
                        state.setConditionWithSound( 5, true )
                        state.showQuestionMark( 336 )

                        return this.getPath( '30232-23.html' )

                    case '20':
                        if ( QuestHelper.hasQuestItem( player, Q_BERETHS_BLOOD_DRAGON )
                                && QuestHelper.hasQuestItem( player, Q_SILVER_DRAGON )
                                && QuestHelper.getQuestItemsCount( player, Q_GOLD_WYRM ) >= 13 ) {

                            await QuestHelper.takeMultipleItems( player, 1, Q_BERETHS_BLOOD_DRAGON, Q_SILVER_DRAGON )
                            await QuestHelper.takeSingleItem( player, Q_GOLD_WYRM, 13 )
                            await QuestHelper.rewardSingleItem( player, DEMON_STAFF, 1 )

                            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                            return this.getPath( '30232-24a.html' )
                        }

                        return this.getPath( '30232-24.html' )

                    case '21':
                        if ( QuestHelper.hasQuestItem( player, Q_BERETHS_GOLD_DRAGON )
                                && QuestHelper.hasQuestItem( player, Q_BLOOD_DRAGON )
                                && QuestHelper.hasQuestItem( player, Q_SILVER_DRYAD )
                                && QuestHelper.hasQuestItem( player, Q_GOLD_GIANT ) ) {

                            await QuestHelper.takeMultipleItems( player, 1, Q_BERETHS_GOLD_DRAGON, Q_BLOOD_DRAGON, Q_SILVER_DRYAD, Q_GOLD_GIANT )
                            await QuestHelper.rewardSingleItem( player, DARK_SCREAMER, 1 )

                            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                            return this.getPath( '30232-24b.html' )
                        }

                        return this.getPath( '30232-24.html' )

                    case '22':
                        if ( QuestHelper.hasQuestItem( player, Q_BERETHS_SILVER_DRAGON )
                                && QuestHelper.hasQuestItem( player, Q_GOLD_DRAGON )
                                && QuestHelper.hasQuestItem( player, Q_BLOOD_SUCCUBUS )
                                && ( QuestHelper.getQuestItemsCount( player, Q_BLOOD_BASILISK ) >= 2 ) ) {

                            await QuestHelper.takeMultipleItems( player, 2, Q_BERETHS_SILVER_DRAGON, Q_GOLD_DRAGON, Q_BLOOD_SUCCUBUS, Q_BLOOD_BASILISK )
                            await QuestHelper.rewardSingleItem( player, WIDOW_MAKER, 1 )

                            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                            return this.getPath( '30232-24c.html' )
                        }

                        return this.getPath( '30232-24.html' )

                    case '23':
                        if ( QuestHelper.hasQuestItem( player, Q_GOLD_DRAGON )
                                && QuestHelper.hasQuestItem( player, Q_SILVER_DRAGON )
                                && QuestHelper.hasQuestItem( player, Q_BLOOD_DRAGON )
                                && QuestHelper.hasQuestItem( player, Q_SILVER_UNDINE ) ) {

                            await QuestHelper.takeMultipleItems( player, 1, Q_GOLD_DRAGON, Q_SILVER_DRAGON, Q_BLOOD_DRAGON, Q_SILVER_UNDINE )
                            await QuestHelper.rewardSingleItem( player, SWORD_OF_LIMIT, 1 )

                            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                            return this.getPath( '30232-24d.html' )
                        }

                        return this.getPath( '30232-24.html' )

                    case '24':
                        if ( QuestHelper.hasQuestItem( player, Q_MANAKS_GOLD_GIANT ) ) {
                            await QuestHelper.takeSingleItem( player, Q_MANAKS_GOLD_GIANT, 1 )
                            await QuestHelper.rewardSingleItem( player, DEMONS_BOOTS, 1 )

                            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                            return this.getPath( '30232-24e.html' )
                        }

                        return this.getPath( '30232-24.html' )

                    case '25':
                        if ( QuestHelper.hasQuestItem( player, Q_MANAKS_SILVER_DRYAD ) && QuestHelper.hasQuestItem( player, Q_SILVER_DRYAD ) ) {
                            await QuestHelper.takeMultipleItems( player, 1, Q_MANAKS_SILVER_DRYAD, Q_SILVER_DRYAD )
                            await QuestHelper.rewardSingleItem( player, DEMONS_HOSE, 1 )

                            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                            return this.getPath( '30232-24f.html' )
                        }

                        return this.getPath( '30232-24.html' )

                    case '26':
                        if ( QuestHelper.hasQuestItem( player, Q_MANAKS_GOLD_GIANT ) ) {
                            await QuestHelper.takeSingleItem( player, Q_MANAKS_GOLD_GIANT, 1 )
                            await QuestHelper.rewardSingleItem( player, DEMONS_GLOVES, 1 )

                            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                            return this.getPath( '30232-24g.html' )
                        }

                        return this.getPath( '30232-24.html' )

                    case '27':
                        if ( QuestHelper.hasQuestItem( player, Q_MANAKS_BLOOD_WEREWOLF ) && QuestHelper.hasQuestItem( player, Q_GOLD_GIANT ) && QuestHelper.hasQuestItem( player, Q_GOLD_WYRM ) ) {
                            await QuestHelper.takeMultipleItems( player, 1, Q_MANAKS_BLOOD_WEREWOLF, Q_GOLD_GIANT, Q_GOLD_WYRM )
                            await QuestHelper.rewardSingleItem( player, FULL_PLATE_HELMET, 1 )

                            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                            return this.getPath( '30232-24h.html' )
                        }

                        return this.getPath( '30232-24.html' )

                    case '28':
                        if ( ( QuestHelper.getQuestItemsCount( player, Q_NIAS_BLOOD_MEDUSA ) >= 2 ) && ( QuestHelper.getQuestItemsCount( player, Q_GOLD_DRAKE ) >= 2 ) && ( QuestHelper.getQuestItemsCount( player, Q_BLOOD_DREVANUL ) >= 2 ) && ( QuestHelper.getQuestItemsCount( player, Q_GOLD_KNIGHT ) >= 3 ) ) {
                            await QuestHelper.takeMultipleItems( player, 2, Q_NIAS_BLOOD_MEDUSA, Q_GOLD_DRAKE, Q_BLOOD_DREVANUL )
                            await QuestHelper.takeSingleItem( player, Q_GOLD_KNIGHT, 3 )
                            await QuestHelper.rewardSingleItem( player, MOONSTONE_EARING, 1 )

                            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                            return this.getPath( '30232-24i.html' )
                        }

                        return this.getPath( '30232-24.html' )

                    case '29':
                        if ( QuestHelper.getQuestItemsCount( player, Q_NIAS_BLOOD_MEDUSA ) >= 7
                                && QuestHelper.getQuestItemsCount( player, Q_GOLD_KNIGHT ) >= 5
                                && QuestHelper.getQuestItemsCount( player, Q_BLOOD_DREVANUL ) >= 5
                                && QuestHelper.getQuestItemsCount( player, Q_SILVER_GOLEM ) >= 5 ) {

                            await QuestHelper.takeSingleItem( player, Q_NIAS_BLOOD_MEDUSA, 7 )
                            await QuestHelper.takeMultipleItems( player, 5, Q_GOLD_KNIGHT, Q_BLOOD_DREVANUL, Q_SILVER_GOLEM )
                            await QuestHelper.rewardSingleItem( player, NASSENS_EARING, 1 )

                            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                            return this.getPath( '30232-24j.html' )
                        }

                        return this.getPath( '30232-24.html' )

                    case '30':
                        if ( QuestHelper.getQuestItemsCount( player, Q_NIAS_GOLD_WYVERN ) >= 5
                                && QuestHelper.getQuestItemsCount( player, Q_SILVER_GOLEM ) >= 4
                                && QuestHelper.getQuestItemsCount( player, Q_GOLD_DRAKE ) >= 4
                                && QuestHelper.getQuestItemsCount( player, Q_BLOOD_DREVANUL ) >= 4 ) {

                            await QuestHelper.takeSingleItem( player, Q_NIAS_GOLD_WYVERN, 5 )
                            await QuestHelper.takeMultipleItems( player, 4, Q_SILVER_GOLEM, Q_GOLD_DRAKE, Q_BLOOD_DREVANUL )
                            await QuestHelper.rewardSingleItem( player, RING_OF_BINDING, 1 )

                            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                            return this.getPath( '30232-24k.html' )
                        }

                        return this.getPath( '30232-24.html' )

                    case '31':
                        if ( QuestHelper.getQuestItemsCount( player, Q_NIAS_SILVER_FAIRY ) >= 5
                                && QuestHelper.getQuestItemsCount( player, Q_SILVER_FAIRY ) >= 3
                                && QuestHelper.getQuestItemsCount( player, Q_GOLD_KNIGHT ) >= 3
                                && QuestHelper.getQuestItemsCount( player, Q_BLOOD_DREVANUL ) >= 3 ) {

                            await QuestHelper.takeSingleItem( player, Q_NIAS_SILVER_FAIRY, 5 )
                            await QuestHelper.takeMultipleItems( player, 3, Q_SILVER_FAIRY, Q_GOLD_KNIGHT, Q_BLOOD_DREVANUL )
                            await QuestHelper.rewardSingleItem( player, NECKLACE_OF_PROTECTION, 1 )

                            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                            return this.getPath( '30232-24l.html' )
                        }

                        return this.getPath( '30232-24.html' )

                    case '100':
                        await QuestHelper.takeMultipleItems( player, -1, Q_CC_MEMBERSHIP_1, Q_CC_MEMBERSHIP_2, Q_CC_MEMBERSHIP_3 )
                        await state.exitQuest( true, true )

                        return this.getPath( '30232-18a.html' )
                }

                return
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case PANO:
            case COLLOB:
            case HEAD_BLACKSMITH_FERRIS:
                if ( QuestHelper.hasQuestItem( player, Q_CC_MEMBERSHIP_1 ) ) {
                    this.resetVariables( state )
                    return this.getPath( `${ data.characterNpcId }-01.html` )
                }

                if ( QuestHelper.hasAtLeastOneQuestItem( player, Q_CC_MEMBERSHIP_2, Q_CC_MEMBERSHIP_3 ) ) {
                    return this.getPath( `${ data.characterNpcId }-54.html` )
                }

                break

            case RAPIN:
            case STAN:
            case BLACKSMITH_DUNING:
                if ( QuestHelper.hasAtLeastOneQuestItem( player, Q_CC_MEMBERSHIP_1, Q_CC_MEMBERSHIP_2 ) ) {
                    this.resetVariables( state )
                    return this.getPath( `${ data.characterNpcId }-01.html` )
                }

                if ( QuestHelper.hasQuestItem( player, Q_CC_MEMBERSHIP_3 ) ) {
                    return this.getPath( `${ data.characterNpcId }-54.html` )
                }

                break

            case HAGGER:
            case MAGISTER_PAGE:
            case RESEARCHER_LORAIN:
                if ( QuestHelper.hasAtLeastOneQuestItem( player, Q_CC_MEMBERSHIP_1, Q_CC_MEMBERSHIP_2, Q_CC_MEMBERSHIP_3 ) ) {
                    this.resetVariables( state )
                    return this.getPath( `${ data.characterNpcId }-01.html` )
                }

                break

            case UNION_PRESIDENT_BERNARD:
                switch ( state.getMemoState() ) {
                    case 0:
                        break

                    case 1:
                        if ( QuestHelper.hasQuestItem( player, Q_COIN_DIAGRAM ) ) {
                            return this.getPath( '30702-01.html' )
                        }

                        break

                    case 2:
                        return this.getPath( '30702-02a.html' )

                    default:
                        return this.getPath( '30702-05.html' )
                }

                break

            case WAREHOUSE_KEEPER_SORINT:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        if ( player.getLevel() < minimumLevel ) {
                            return this.getPath( '30232-01.htm' )
                        }

                        return this.getPath( '30232-02.htm' )

                    case QuestStateValues.STARTED:
                        switch ( state.getMemoState() ) {
                            case 1:
                            case 2:
                                if ( !QuestHelper.hasQuestItem( player, Q_KALDIS_GOLD_DRAGON ) ) {
                                    return this.getPath( '30232-06.html' )
                                }

                                await QuestHelper.giveSingleItem( player, Q_CC_MEMBERSHIP_3, 1 )
                                await QuestHelper.takeMultipleItems( player, -1, Q_COIN_DIAGRAM, Q_KALDIS_GOLD_DRAGON )

                                state.setMemoState( 3 )
                                state.setConditionWithSound( 4, true )
                                state.showQuestionMark( 336 )

                                return this.getPath( '30232-07.html' )

                            case 3:
                                if ( QuestHelper.hasQuestItem( player, Q_CC_MEMBERSHIP_3 ) ) {
                                    return this.getPath( '30232-10.html' )
                                }
                                if ( QuestHelper.hasQuestItem( player, Q_CC_MEMBERSHIP_2 ) ) {
                                    return this.getPath( '30232-11.html' )
                                }

                                if ( QuestHelper.hasQuestItem( player, Q_CC_MEMBERSHIP_1 ) ) {
                                    return this.getPath( '30232-12.html' )
                                }

                                break
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getRandomPlayerForMemo( player: L2PcInstance, npc: L2Npc ): L2PcInstance {
        let questName = this.getName()
        let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), questName, false )
        let usePlayerStates: boolean = state && state.isStarted() && state.getMemoState() === 3 && !QuestHelper.hasQuestItem( player, Q_KALDIS_GOLD_DRAGON )
        let possiblePlayers: Array<L2PcInstance> = usePlayerStates ? [ player, player ] : []

        if ( player.getParty() ) {
            _.each( player.getParty().getMembers(), ( playerId: number ) => {
                let playerState: QuestState = QuestStateCache.getQuestState( playerId, questName, false )

                if ( playerState
                        && playerState.isStarted()
                        && playerState.getMemoState() === 3 ) {
                    let otherPlayer = L2World.getPlayer( playerId )

                    if ( !QuestHelper.hasQuestItem( player, Q_KALDIS_GOLD_DRAGON )
                            && GeneralHelper.checkIfInRange( 1500, npc, otherPlayer, true ) ) {
                        possiblePlayers.push( player )
                    }
                }
            } )
        }

        return _.sample( possiblePlayers )
    }

    getRandomPlayerState( player: L2PcInstance, npc: L2Npc ): QuestState {
        let questName = this.getName()
        let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), questName, false )
        let usePlayerStates: boolean = state && state.isStarted() && state.getMemoState() === 3 && !QuestHelper.hasQuestItem( player, Q_KALDIS_GOLD_DRAGON )
        let possibleStates: Array<QuestState> = usePlayerStates ? [ state, state ] : []

        if ( player.getParty() ) {
            _.each( player.getParty().getMembers(), ( playerId: number ) => {
                let playerState: QuestState = QuestStateCache.getQuestState( playerId, questName, false )

                if ( playerState
                        && playerState.isStarted()
                        && playerState.getMemoState() === 3 ) {
                    let otherPlayer = L2World.getPlayer( playerId )

                    if ( !QuestHelper.hasQuestItem( player, Q_KALDIS_GOLD_DRAGON )
                            && GeneralHelper.checkIfInRange( 1500, npc, otherPlayer, true ) ) {
                        possibleStates.push( playerState )
                    }
                }
            } )
        }

        return _.sample( possibleStates )
    }

    async processFirstSteps(
            player: L2PcInstance,
            state: QuestState,
            npcId: number,
            weightValue: number,
            baseAmount: number,
            firstItemOne: number,
            firstItemTwo: number,
            multiplier: number,
            secondItem: number,
            thirdItem: number,
            fourthItem: number ) {

        switch ( state.getVariable( variableNames.one ) ) {
            case 42:
                const finalAmount = multiplier * baseAmount
                if ( QuestHelper.getQuestItemsCount( player, firstItemOne ) >= finalAmount
                        && ( firstItemTwo === 0 || QuestHelper.getQuestItemsCount( player, firstItemTwo ) >= baseAmount ) ) {

                    state.setVariable( variableNames.one, _.random( 1, 3 ) + _.random( 1, 3 ) * 4 + _.random( 1, 3 ) * 16 )
                    state.setVariable( variableNames.flag, 1 )
                    state.setVariable( variableNames.weightPoint, weightValue )

                    await QuestHelper.takeSingleItem( player, firstItemOne, finalAmount )
                    if ( firstItemTwo !== 0 ) {
                        await QuestHelper.takeSingleItem( player, firstItemTwo, baseAmount )
                    }

                    return this.getPath( `${ npcId }-11.html` )
                }

                break

            case 31:
                return this.firstStepLogic( player, state, npcId, secondItem, baseAmount, weightValue )

            case 21:
                return this.firstStepLogic( player, state, npcId, thirdItem, baseAmount, weightValue )

            case 11:
                return this.firstStepLogic( player, state, npcId, fourthItem, baseAmount, weightValue )
        }

        return this.getPath( `${ npcId }-10.html` )
    }

    async processSecondSingleStep(
            player: L2PcInstance,
            state: QuestState,
            npcId: number,
            multiplier: number,
            firstItem: number,
            quantityMultiplier: number,
            firstReward: number,
            secondItem: number,
            secondReward: number,
            thirdItem: number,
            thirdReward: number,
            fourthItem: number,
            fourthReward: number ): Promise<string> {

        switch ( state.getVariable( variableNames.two ) ) {
            case 42:
                return this.secondSingleStepLogic( player, npcId, firstItem, 10 * multiplier * quantityMultiplier, firstReward, multiplier )

            case 31:
                return this.secondSingleStepLogic( player, npcId, secondItem, 5 * multiplier, secondReward, multiplier )

            case 21:
                return this.secondSingleStepLogic( player, npcId, thirdItem, 5 * multiplier, thirdReward, multiplier )

            case 11:
                return this.secondSingleStepLogic( player, npcId, fourthItem, 5 * multiplier, fourthReward, multiplier )
        }

        return this.getPath( `${ npcId }-10.html` )
    }

    async processSecondSteps(
            player: L2PcInstance,
            state: QuestState,
            npcId: number,
            multiplier: number,
            firstItemOne: number,
            firstItemTwo: number,
            firstReward: number,
            secondItemOne: number,
            secondItemTwo: number,
            rewardTwo: number,
            thirdItemOne: number,
            thirdItemTwo: number,
            rewardThree: number,
            itemFourOne: number,
            itemFourTwo: number,
            rewardFour: number ): Promise<string> {

        switch ( state.getVariable( variableNames.two ) ) {
            case 42:
                return this.secondStepLogic( player, multiplier, 10, firstItemOne, firstItemTwo, firstReward, npcId )

            case 31:
                return this.secondStepLogic( player, multiplier, 5, secondItemOne, secondItemTwo, rewardTwo, npcId )

            case 21:
                return this.secondStepLogic( player, multiplier, 5, thirdItemOne, thirdItemTwo, rewardThree, npcId )

            case 11:
                return this.secondStepLogic( player, multiplier, 5, itemFourOne, itemFourTwo, rewardFour, npcId )
        }
    }

    async processThirdSteps(
            player: L2PcInstance,
            state: QuestState,
            npcId: number,
            flagValue: number,
            firstItem: number,
            secondItem: number,
            thirdItem: number,
            fourthItem: number ): Promise<string> {

        state.setVariable( variableNames.three, 0 )
        state.setVariable( variableNames.flag, state.getVariable( variableNames.flag ) + flagValue )

        if ( state.getVariable( variableNames.one ) === state.getVariable( variableNames.flag )
                && state.getVariable( variableNames.weightPoint ) >= 0 ) {
            state.setVariable( variableNames.weightPoint, 0 )

            switch ( state.getVariable( variableNames.two ) ) {
                case 42:
                    await QuestHelper.giveSingleItem( player, firstItem, 1 )
                    break

                case 31:
                    await QuestHelper.giveSingleItem( player, secondItem, 1 )
                    break

                case 21:
                    await QuestHelper.giveSingleItem( player, thirdItem, 1 )
                    break

                case 11:
                    await QuestHelper.giveSingleItem( player, fourthItem, 1 )
                    break

            }

            state.setVariable( variableNames.one, 0 )
            return this.getPath( `${ npcId }-20.html` )
        }

        if ( state.getVariable( variableNames.weightPoint ) === 0 ) {
            state.setVariable( variableNames.one, 0 )

            switch ( state.getVariable( variableNames.one ) ) {
                case 21:
                    return this.getPath( `${ npcId }-23.html` )

                case 25:
                    return this.getPath( `${ npcId }-24.html` )

                case 37:
                    return this.getPath( `${ npcId }-25.html` )

                case 41:
                    return this.getPath( `${ npcId }-26.html` )

                case 61:
                    return this.getPath( `${ npcId }-27.html` )

                case 29:
                    return this.getPath( `${ npcId }-28.html` )

                case 45:
                    return this.getPath( `${ npcId }-29.html` )

                case 53:
                    return this.getPath( `${ npcId }-30.html` )

                case 57:
                    return this.getPath( `${ npcId }-31.html` )

                case 22:
                    return this.getPath( `${ npcId }-32.html` )

                case 26:
                    return this.getPath( `${ npcId }-33.html` )

                case 38:
                    return this.getPath( `${ npcId }-34.html` )

                case 42:
                    return this.getPath( `${ npcId }-35.html` )

                case 62:
                    return this.getPath( `${ npcId }-36.html` )

                case 30:
                    return this.getPath( `${ npcId }-37.html` )

                case 46:
                    return this.getPath( `${ npcId }-38.html` )

                case 54:
                    return this.getPath( `${ npcId }-39.html` )

                case 58:
                    return this.getPath( `${ npcId }-40.html` )

                case 23:
                    return this.getPath( `${ npcId }-41.html` )

                case 27:
                    return this.getPath( `${ npcId }-42.html` )

                case 39:
                    return this.getPath( `${ npcId }-43.html` )

                case 43:
                    return this.getPath( `${ npcId }-44.html` )

                case 63:
                    return this.getPath( `${ npcId }-45.html` )

                case 31:
                    return this.getPath( `${ npcId }-46.html` )

                case 47:
                    return this.getPath( `${ npcId }-47.html` )

                case 55:
                    return this.getPath( `${ npcId }-48.html` )

                case 59:
                    return this.getPath( `${ npcId }-49.html` )

            }

            return
        }

        let one = state.getVariable( variableNames.one ) % 4
        let two = Math.floor( state.getVariable( variableNames.one ) / 4 )
        let three = Math.floor( two / 4 )
        two = two % 4

        let four = state.getVariable( variableNames.flag ) % 4
        let five = Math.floor( state.getVariable( variableNames.flag ) / 4 )
        let six = Math.floor( five / 4 )
        five = five % 4

        if ( one === four ) {
            state.setVariable( variableNames.three, state.getVariable( variableNames.three ) + 1 )
        }

        if ( two === five ) {
            state.setVariable( variableNames.three, state.getVariable( variableNames.three ) + 1 )
        }

        if ( three === six ) {
            state.setVariable( variableNames.three, state.getVariable( variableNames.three ) + 1 )
        }

        state.setVariable( variableNames.flag, 1 )
        state.setVariable( variableNames.weightPoint, state.getVariable( variableNames.weightPoint ) - 1 )

        switch ( state.getVariable( variableNames.three ) ) {
            case 0:
                return this.getPath( `${ npcId }-52.html` )

            case 1:
                return this.getPath( `${ npcId }-50.html` )

            case 2:
                return this.getPath( `${ npcId }-51.html` )
        }
    }

    resetVariables( state: QuestState ): void {
        state.unsetVariables( ..._.keys( variableNames ) )
    }

    async secondSingleStepLogic( player: L2PcInstance, npcId: number, itemId: number, amount: number, rewardId: number, rewardAmount: number ): Promise<string> {
        if ( QuestHelper.getQuestItemsCount( player, itemId ) >= amount ) {
            await QuestHelper.takeSingleItem( player, itemId, amount )
            await QuestHelper.giveSingleItem( player, rewardId, rewardAmount )

            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

            return this.getPath( `${ npcId }-07.html` )
        }

        return this.getPath( `${ npcId }-10.html` )
    }

    async secondStepLogic(
            player: L2PcInstance,
            multiplier: number,
            baseAmount: number,
            itemOne: number,
            itemTwo: number,
            givenItemId: number,
            npcId: number ): Promise<string> {

        const finalAmount = baseAmount * multiplier
        if ( QuestHelper.getQuestItemsCount( player, itemOne ) >= finalAmount
                && QuestHelper.getQuestItemsCount( player, itemTwo ) >= finalAmount ) {

            await QuestHelper.takeMultipleItems( player, finalAmount, itemOne, itemTwo )
            await QuestHelper.giveSingleItem( player, givenItemId, multiplier )

            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
            return this.getPath( `${ npcId }-07.html` )
        }

        return this.getPath( `${ npcId }-10.html` )
    }
}