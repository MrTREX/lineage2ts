import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'

import _ from 'lodash'

const KATERINA = 30004
const FLAME_SHARD = 1020
const ICE_SHARD = 1021
const minimumLevel = 17
const UNDINE_NOBLE = 20115

type MonsterReward = [ number, number ] // itemId, chance
const monsterRewardChances: { [ npcId: number ]: MonsterReward } = {
    20109: [ FLAME_SHARD, 925 ], // Salamander
    20110: [ ICE_SHARD, 900 ], // Undine
    20112: [ FLAME_SHARD, 900 ], // Salamander Elder
    20113: [ ICE_SHARD, 925 ], // Undine Elder
    20114: [ FLAME_SHARD, 925 ], // Salamander Noble
    [ UNDINE_NOBLE ]: [ ICE_SHARD, 950 ], // Undine Noble
}

export class CrystalOfFireAndIce extends ListenerLogic {
    constructor() {
        super( 'Q00306_CrystalOfFireAndIce', 'listeners/tracked-300/CrystalOfFireAndIce.ts' )
        this.questId = 306
        this.questItemIds = [
            FLAME_SHARD,
            ICE_SHARD,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00306_CrystalOfFireAndIce'
    }

    getQuestStartIds(): Array<number> {
        return [ KATERINA ]
    }

    getTalkIds(): Array<number> {
        return [ KATERINA ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( data.npcId === UNDINE_NOBLE ) {
            let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

            if ( !state || !state.isStarted() ) {
                return
            }

            let player = L2World.getPlayer( data.playerId )

            await this.rewardQuestItems( player, npc, data )
            return
        }

        let state = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), -1, 3, npc )

        if ( !state ) {
            return
        }

        await this.rewardQuestItems( state.getPlayer(), npc, data )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30004-04.htm':
                state.startQuest()
                break

            case '30004-08.html':
                await state.exitQuest( true, true )
                break

            case '30004-09.html':
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30004-03.htm' : '30004-02.htm' )

            case QuestStateValues.STARTED:
                let flame = QuestHelper.getQuestItemsCount( player, FLAME_SHARD )
                let ice = QuestHelper.getQuestItemsCount( player, ICE_SHARD )
                let total = flame + ice

                if ( total === 0 ) {
                    return this.getPath( '30004-05.html' )
                }

                let amount = ( flame * 40 ) + ( ice * 40 ) + ( total >= 10 ? 5000 : 0 )
                await QuestHelper.giveAdena( player, amount, true )
                await QuestHelper.takeMultipleItems( player, -1, ...this.questItemIds )

                return this.getPath( '30004-07.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async rewardQuestItems( player: L2PcInstance, npc: L2Npc, data: AttackableKillEvent ): Promise<void> {
        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        let [ itemId, chance ] = monsterRewardChances[ data.npcId ]
        if ( _.random( 1000 ) > QuestHelper.getAdjustedChance( itemId, chance, data.isChampion ) ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, itemId, 1, data.isChampion )
        return
    }
}