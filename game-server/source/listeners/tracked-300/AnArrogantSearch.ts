import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import {
    AttackableAttackedEvent,
    AttackableKillEvent,
    NpcGeneralEvent,
    NpcSpawnEvent,
    NpcTalkEvent,
} from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestHelper } from '../helpers/QuestHelper'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { ClassIdValues } from '../../gameService/models/base/ClassId'

import _ from 'lodash'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const HARNE = 30144
const MARTIEN = 30645
const SIR_GUSTAV_ATHEBALDT = 30760
const HARDIN = 30832
const HANELLIN = 30864
const IASON_HEINE = 30969
const HOLY_ARK_OF_SECRECY_1 = 30977
const HOLY_ARK_OF_SECRECY_2 = 30978
const HOLY_ARK_OF_SECRECY_3 = 30979
const ARK_GUARDIANS_CORPSE = 30980
const CLAUDIA_ATHEBALDT = 31001
const GREATER_HEALING_POTION = 1061
const ANTIDOTE = 1831
const TITANS_POWERSTONE = 4287
const HANELLINS_1ST_LETTER = 4288
const HANELLINS_2ND_LETTER = 4289
const HANELLINS_3RD_LETTER = 4290
const FIRST_KEY_OF_ARK = 4291
const SECOND_KEY_OF_ARK = 4292
const THIRD_KEY_OF_ARK = 4293
const WHITE_FABRIC_1 = 4294
const BLOODED_FABRIC = 4295
const BOOK_OF_SAINT = 4397
const BLOOD_OF_SAINT = 4398
const BOUGH_OF_SAINT = 4399
const WHITE_FABRIC_2 = 4400
const SHELL_OF_MONSTERS = 14857
const minimumLevel = 60
const MIN_HP_PERCENTAGE = 0.3
const ANIMAL_BONE = 1872
const ORIHARUKON_ORE = 1874
const COKES = 1879
const COARSE_BONE_POWDER = 1881
const VARNISH_OF_PURITY = 1887
const SYNTHETIC_COKES = 1888
const ENRIA = 4042
const GREAT_SWORD_BLADE = 4104
const HEAVY_WAR_AXE_HEAD = 4105
const SPRITES_STAFF_HEAD = 4106
const KESHANBERK_BLADE = 4107
const SWORD_OF_VALHALLA_BLADE = 4108
const KRIS_EDGE = 4109
const HELL_KNIFE_EDGE = 4110
const ARTHRO_NAIL_BLADE = 4111
const DARK_ELVEN_LONGBOW_SHAFT = 4112
const GREAT_AXE_HEAD = 4113
const SWORD_OF_DAMASCUS_BLADE = 4114
const LANCE_BLADE = 4115
const ART_OF_BATTLE_AXE_BLADE = 4117
const EVIL_SPIRIT_HEAD = 4118
const DEMONS_DAGGER_EDGE = 4119
const BELLION_CESTUS_EDGE = 4120
const BOW_OF_PERIL_SHAFT = 4121
const ARK_GUARDIAN_ELBEROTH = 27182
const ARK_GUARDIAN_SHADOWFANG = 27183
const ANGEL_KILLER = 27184
const YINTZU = 20647
const PALIOTE = 20648
const PLATINUM_TRIBE_SHAMAN = 20828
const PLATINUM_TRIBE_OVERLORD = 20829
const GUARDIAN_ANGEL = 20830
const SEAL_ANGEL_1 = 20831
const SEAL_ANGEL_2 = 20860

const eventNames = {
    despawn: 'd',
}

export class AnArrogantSearch extends ListenerLogic {
    constructor() {
        super( 'Q00348_AnArrogantSearch', 'listeners/tracked-300/AnArrogantSearch.ts' )
        this.questId = 348
        this.questItemIds = [
            SHELL_OF_MONSTERS,
            TITANS_POWERSTONE,
            HANELLINS_1ST_LETTER,
            HANELLINS_2ND_LETTER,
            HANELLINS_3RD_LETTER,
            FIRST_KEY_OF_ARK,
            SECOND_KEY_OF_ARK,
            THIRD_KEY_OF_ARK,
            WHITE_FABRIC_1,
            BOOK_OF_SAINT,
            BLOOD_OF_SAINT,
            BOUGH_OF_SAINT,
            WHITE_FABRIC_2,
        ]
    }

    getAttackableAttackIds(): Array<number> {
        return [
            ARK_GUARDIAN_ELBEROTH,
            ARK_GUARDIAN_SHADOWFANG,
            ANGEL_KILLER,
            PLATINUM_TRIBE_SHAMAN,
            PLATINUM_TRIBE_OVERLORD,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            ARK_GUARDIAN_ELBEROTH,
            ARK_GUARDIAN_SHADOWFANG,
            YINTZU,
            PALIOTE,
            PLATINUM_TRIBE_SHAMAN,
            PLATINUM_TRIBE_OVERLORD,
            GUARDIAN_ANGEL,
            SEAL_ANGEL_1,
            SEAL_ANGEL_2,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00348_AnArrogantSearch'
    }

    getQuestStartIds(): Array<number> {
        return [ HANELLIN ]
    }

    getSpawnIds(): Array<number> {
        return [
            ARK_GUARDIAN_ELBEROTH,
            ARK_GUARDIAN_SHADOWFANG,
            ANGEL_KILLER,
        ]
    }

    getTalkIds(): Array<number> {
        return [
            HANELLIN,
            IASON_HEINE,
            HOLY_ARK_OF_SECRECY_1,
            HOLY_ARK_OF_SECRECY_2,
            HOLY_ARK_OF_SECRECY_3,
            ARK_GUARDIANS_CORPSE,
            CLAUDIA_ATHEBALDT,
            HARNE,
            MARTIEN,
            SIR_GUSTAV_ATHEBALDT,
            HARDIN,
        ]
    }

    async onAttackableAttackedEvent( data: AttackableAttackedEvent ): Promise<void> {
        switch ( data.targetNpcId ) {
            case ARK_GUARDIAN_ELBEROTH:
                return this.onGuardianAttacked( data, NpcStringIds.SORRY_ABOUT_THIS_BUT_I_MUST_KILL_YOU_NOW )

            case ARK_GUARDIAN_SHADOWFANG:
                return this.onGuardianAttacked( data, NpcStringIds.I_SHALL_DRENCH_THIS_MOUNTAIN_WITH_YOUR_BLOOD )

            case ANGEL_KILLER:
                return this.onAngelAttacked( data )

            case PLATINUM_TRIBE_SHAMAN:
                return this.onPlatinumTribeAttacked( data, 60 )

            case PLATINUM_TRIBE_OVERLORD:
                return this.onPlatinumTribeAttacked( data, 70 )
        }
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let npc = L2World.getObjectById( data.targetId ) as L2Npc
        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), -1, 3, npc )
        if ( !state ) {
            return
        }

        let player = state.getPlayer()

        switch ( data.npcId ) {
            case ARK_GUARDIAN_ELBEROTH:
                if ( ( state.getMemoStateEx( 0 ) < 8 )
                        && Math.floor( ( state.getMemoStateEx( 1 ) % 1000 ) / 100 ) === 1
                        && !QuestHelper.hasAtLeastOneQuestItem( player, SECOND_KEY_OF_ARK, BOOK_OF_SAINT ) ) {

                    state.setMemoStateEx( 1, state.getMemoStateEx( 1 ) + 100 )

                    if ( ( state.getMemoStateEx( 1 ) % 10 ) !== 0 ) {
                        state.setConditionWithSound( 11 )
                    }

                    BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.YOU_FOOLS_WILL_GET_WHATS_COMING_TO_YOU )
                    await QuestHelper.giveSingleItem( player, SECOND_KEY_OF_ARK, 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case ARK_GUARDIAN_SHADOWFANG:
                if ( state.getMemoStateEx( 0 ) < 8
                        && Math.floor( ( state.getMemoStateEx( 1 ) % 10000 ) / 1000 ) === 1
                        && !QuestHelper.hasAtLeastOneQuestItem( player, THIRD_KEY_OF_ARK, BOUGH_OF_SAINT ) ) {

                    state.setMemoStateEx( 1, state.getMemoStateEx( 1 ) + 1000 )
                    if ( ( state.getMemoStateEx( 1 ) % 10 ) !== 0 ) {
                        state.setConditionWithSound( 15 )
                    }

                    BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.YOU_GUYS_WOULDNT_KNOW_THE_SEVEN_SEALS_ARE_ARRRGH )
                    await QuestHelper.giveSingleItem( player, THIRD_KEY_OF_ARK, 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case YINTZU:
            case PALIOTE:
                if ( state.isMemoState( 1 ) && !QuestHelper.hasQuestItem( player, SHELL_OF_MONSTERS ) ) {
                    await QuestHelper.giveSingleItem( player, SHELL_OF_MONSTERS, 1 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case PLATINUM_TRIBE_SHAMAN:
                await this.onPlatinumTribeKilled( player, state, 600 )
                return

            case PLATINUM_TRIBE_OVERLORD:
                await this.onPlatinumTribeKilled( player, state, 700 )
                return

            case GUARDIAN_ANGEL:
            case SEAL_ANGEL_1:
            case SEAL_ANGEL_2:
                if ( state.getMemoStateEx( 0 ) === 17 && QuestHelper.hasQuestItem( player, WHITE_FABRIC_1 ) ) {
                    let value = state.getMemoStateEx( 1 ) + _.random( 100 ) + 100

                    if ( value * 2 > 750 ) {
                        await QuestHelper.giveSingleItem( player, BLOODED_FABRIC, 1 )
                        await QuestHelper.takeSingleItem( player, WHITE_FABRIC_1, 1 )

                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                        state.setMemoStateEx( 1, 0 )

                        return
                    }

                    state.setMemoStateEx( 1, value )
                }

                return

        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        if ( data.eventName === eventNames.despawn ) {
            await ( L2World.getObjectById( data.characterId ) as L2Npc ).deleteMe()
            return
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30864-02.htm':
            case '30864-03.htm':
            case '30864-10.html':
            case '30864-11.html':
            case '30864-12.html':
            case '30864-25.html':
            case '31001-02.html':
            case '30144-02.html':
            case '30645-02.html':
                break

            case '30864-04.htm':
                state.startQuest()
                state.setMemoState( 1 )
                state.setConditionWithSound( 2 )
                break

            case '30864-08.html':
                let memoState = state.getMemoState()
                if ( ( memoState === 1 && QuestHelper.hasAtLeastOneQuestItem( player, TITANS_POWERSTONE, SHELL_OF_MONSTERS ) )
                        || ( memoState === 2 ) ) {
                    state.setMemoStateEx( 0, 4 )
                    state.setMemoStateEx( 1, 0 )
                    state.setMemoState( 4 )
                    state.setConditionWithSound( 4 )

                    break
                }

                return

            case '30864-09.html':
                if ( state.isMemoState( 4 ) && state.getMemoStateEx( 1 ) === 0 ) {
                    await QuestHelper.giveMultipleItems( player, 1, HANELLINS_1ST_LETTER, HANELLINS_2ND_LETTER, HANELLINS_3RD_LETTER )

                    state.setMemoState( 5 )
                    state.setConditionWithSound( 5 )

                    break
                }

                return

            case '30864-26.html':
                if ( state.isMemoState( 10 ) && QuestHelper.getQuestItemsCount( player, WHITE_FABRIC_1 ) === 1 ) {
                    state.setMemoState( 11 )

                    break
                }

                return

            case '30864-27.html':
                if ( state.isMemoState( 11 )
                        && state.getMemoStateEx( 1 ) > 0
                        && QuestHelper.getQuestItemsCount( player, WHITE_FABRIC_1 ) === 1 ) {
                    switch ( state.getMemoStateEx( 1 ) ) {
                        case 1:
                            await QuestHelper.giveAdena( player, 43000, true )
                            break

                        case 2:
                            await QuestHelper.giveAdena( player, 4000, true )
                            break

                        case 3:
                            await QuestHelper.giveAdena( player, 13000, true )
                            break

                    }

                    state.setMemoStateEx( 0, 12 )
                    state.setMemoStateEx( 1, 100 )
                    state.setConditionWithSound( 24 )

                    break
                }

                return this.getPath( '30864-28.html' )

            case '30864-29.html':
                if ( state.isMemoState( 11 )
                        && state.getMemoStateEx( 1 ) === 0
                        && QuestHelper.getQuestItemsCount( player, WHITE_FABRIC_1 ) === 1 ) {
                    await QuestHelper.giveAdena( player, 49000, true )

                    state.setMemoState( 12 )
                    state.setMemoStateEx( 0, 12 )
                    state.setMemoStateEx( 1, 20000 )
                    state.setConditionWithSound( 24 )

                    break
                }

                return

            case '30864-30.html':
                if ( state.isMemoState( 11 )
                        && state.getMemoStateEx( 1 ) === 0
                        && QuestHelper.getQuestItemsCount( player, WHITE_FABRIC_1 ) === 1 ) {
                    state.setMemoState( 13 )
                    state.setMemoStateEx( 0, 13 )
                    state.setMemoStateEx( 1, 20000 )
                    state.setConditionWithSound( 25 )

                    break
                }

                return

            case '30864-43.html':
                if ( state.isMemoState( 15 ) ) {
                    state.setMemoState( 16 )
                    break
                }

                return

            case '30864-44.html':
                if ( state.isMemoState( 15 ) || state.isMemoState( 16 ) ) {
                    let amount = QuestHelper.hasQuestItem( player, BLOODED_FABRIC ) ? 9 : 10
                    await QuestHelper.giveSingleItem( player, WHITE_FABRIC_1, amount )
                }

                state.setMemoState( 17 )
                state.setMemoStateEx( 0, 17 )
                state.setMemoStateEx( 1, 0 )
                state.setConditionWithSound( 26 )

                break

            case '30864-47.html':
                if ( state.isMemoState( 17 )
                        && QuestHelper.getQuestItemsCount( player, BLOODED_FABRIC ) >= 10
                        && !QuestHelper.hasQuestItems( player, WHITE_FABRIC_1 ) ) {

                    state.setMemoState( 18 )
                    state.setMemoStateEx( 0, 18 )
                    state.setMemoStateEx( 1, 0 )
                    state.setConditionWithSound( 27 )

                    break
                }

                return

            case '30864-50.html':
                if ( state.isMemoState( 19 ) ) {
                    await QuestHelper.giveSingleItem( player, WHITE_FABRIC_1, 10 )

                    state.setMemoState( 17 )
                    state.setMemoStateEx( 0, 17 )
                    state.setMemoStateEx( 1, 0 )
                    state.setConditionWithSound( 29 )

                    break
                }

                return

            case '30864-51.html':
                await state.exitQuest( true, true )
                break


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onSpawnEvent( data: NpcSpawnEvent ): Promise<void> {
        this.startQuestTimer( eventNames.despawn, 600000, data.characterId, 0 )

        let npc = L2World.getObjectById( data.characterId ) as L2Npc

        switch ( data.npcId ) {
            case ARK_GUARDIAN_ELBEROTH:
                return BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.THAT_DOESNT_BELONG_TO_YOU_DONT_TOUCH_IT )

            case ARK_GUARDIAN_SHADOWFANG:
                return BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.GET_OUT_OF_MY_SIGHT_YOU_INFIDELS )

            case ANGEL_KILLER:
                return BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.I_HAVE_THE_KEY_WHY_DONT_YOU_COME_AND_TAKE_IT )
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30864-01.htm' : '30864-05.html' )

            case QuestStateValues.STARTED:
                let memoState = state.getMemoState()

                switch ( data.characterNpcId ) {
                    case HANELLIN:
                        switch ( memoState ) {
                            case 1:
                            case 2:
                                let hasPowerstone = QuestHelper.hasQuestItem( player, TITANS_POWERSTONE )
                                let hasShell = QuestHelper.hasQuestItem( player, SHELL_OF_MONSTERS )
                                if ( memoState === 1 && !hasPowerstone && !hasShell ) {
                                    return this.getPath( '30864-06.html' )
                                }

                                if ( ( memoState === 1 && hasPowerstone )
                                        || memoState === 2
                                        || hasShell ) {
                                    await QuestHelper.takeMultipleItems( player, 1, SHELL_OF_MONSTERS, TITANS_POWERSTONE )

                                    state.setMemoState( 2 )
                                    return this.getPath( '30864-07.html' )
                                }

                                break

                            case 4:
                                switch ( state.getMemoStateEx( 1 ) ) {
                                    case 0:
                                        state.setMemoState( 5 )
                                        state.setConditionWithSound( 5, true )

                                        await QuestHelper.giveMultipleItems( player, 1, HANELLINS_1ST_LETTER, HANELLINS_2ND_LETTER, HANELLINS_3RD_LETTER )
                                        return this.getPath( '30864-09.html' )

                                    case 1:
                                        state.setMemoState( 5 )
                                        state.setConditionWithSound( 6, true )

                                        await QuestHelper.giveSingleItem( player, HANELLINS_1ST_LETTER, 1 )
                                        return this.getPath( '30864-13.html' )

                                    case 2:
                                        state.setMemoState( 5 )
                                        state.setConditionWithSound( 7, true )

                                        await QuestHelper.giveSingleItem( player, HANELLINS_2ND_LETTER, 1 )
                                        return this.getPath( '30864-14.html' )

                                    case 3:
                                        state.setMemoState( 5 )
                                        state.setConditionWithSound( 8, true )

                                        await QuestHelper.giveSingleItem( player, HANELLINS_3RD_LETTER, 1 )
                                        return this.getPath( '30864-15.html' )
                                }

                                break

                            case 5:
                                if ( ( state.getMemoStateEx( 1 ) % 10 ) === 0 ) {
                                    return this.getPath( '30864-16.html' )
                                }

                                switch ( state.getMemoStateEx( 1 ) ) {
                                    case 1:
                                        return this.getPath( '30864-17.html' )

                                    case 2:
                                        return this.getPath( '30864-18.html' )

                                    case 3:
                                        return this.getPath( '30864-19.html' )
                                }

                                if ( QuestHelper.hasQuestItems( player, BOOK_OF_SAINT, BLOOD_OF_SAINT, BOUGH_OF_SAINT ) ) {
                                    await QuestHelper.takeMultipleItems( player, 1, BOOK_OF_SAINT, BLOOD_OF_SAINT, BOUGH_OF_SAINT )

                                    state.setMemoState( 9 )
                                    state.setConditionWithSound( 22, true )

                                    return this.getPath( '30864-21.html' )
                                }

                                break

                            case 6:
                            case 7:
                                return this.getPath( '30864-20.html' )

                            case 8:
                                if ( QuestHelper.hasQuestItems( player, BOOK_OF_SAINT, BLOOD_OF_SAINT, BOUGH_OF_SAINT ) ) {
                                    await QuestHelper.takeMultipleItems( player, 1, BOOK_OF_SAINT, BLOOD_OF_SAINT, BOUGH_OF_SAINT )

                                    state.setMemoState( 9 )
                                    state.setConditionWithSound( 22, true )

                                    return this.getPath( '30864-21.html' )
                                }

                                switch ( state.getMemoStateEx( 1 ) ) {
                                    case 0:
                                        return this.getPath( '30864-22.html' )

                                    case 1:
                                        if ( QuestHelper.hasQuestItem( player, BLOOD_OF_SAINT ) && !QuestHelper.hasAtLeastOneQuestItem( player, BOOK_OF_SAINT, BOUGH_OF_SAINT ) ) {
                                            return this.getPath( '30864-33.html' )
                                        }

                                        if ( !QuestHelper.hasQuestItems( player, BLOOD_OF_SAINT, WHITE_FABRIC_2 ) ) {
                                            return this.getPath( '30864-36.html' )
                                        }

                                        break

                                    case 2:
                                        if ( QuestHelper.hasQuestItem( player, BOOK_OF_SAINT ) && !QuestHelper.hasAtLeastOneQuestItem( player, BLOOD_OF_SAINT, BOUGH_OF_SAINT ) ) {
                                            return this.getPath( '30864-34.html' )
                                        }

                                        if ( !QuestHelper.hasQuestItems( player, BOOK_OF_SAINT, WHITE_FABRIC_2 ) ) {
                                            return this.getPath( '30864-37.html' )
                                        }

                                        break

                                    case 3:
                                        if ( QuestHelper.hasQuestItem( player, BOUGH_OF_SAINT ) && !QuestHelper.hasAtLeastOneQuestItem( player, BLOOD_OF_SAINT, BOOK_OF_SAINT ) ) {
                                            return this.getPath( '30864-35.html' )
                                        }

                                        if ( !QuestHelper.hasQuestItems( player, BOUGH_OF_SAINT, WHITE_FABRIC_2 ) ) {
                                            return this.getPath( '30864-38.html' )
                                        }

                                        break
                                }

                                if ( state.getMemoStateEx( 1 ) > 0 ) {
                                    let whiteFabricAmount = QuestHelper.getQuestItemsCount( player, WHITE_FABRIC_2 )
                                    if ( whiteFabricAmount > 1 ) {
                                        return this.getPath( '30864-40.html' )
                                    }

                                    if ( whiteFabricAmount === 1 ) {
                                        await QuestHelper.giveSingleItem( player, WHITE_FABRIC_1, 1 )
                                        await QuestHelper.takeSingleItem( player, WHITE_FABRIC_2, 1 )

                                        state.setMemoState( 10 )

                                        return this.getPath( '30864-41.html' )
                                    }
                                }

                                break

                            case 9:
                                let antidoteCount = QuestHelper.getQuestItemsCount( player, ANTIDOTE )
                                let hasHealingPotion = QuestHelper.hasQuestItems( player, GREATER_HEALING_POTION )
                                if ( antidoteCount < 5 || !hasHealingPotion ) {
                                    return this.getPath( '30864-23.html' )
                                }

                                if ( antidoteCount >= 5 && hasHealingPotion ) {
                                    if ( state.getMemoStateEx( 1 ) === 0 ) {
                                        await QuestHelper.giveSingleItem( player, WHITE_FABRIC_1, 1 )
                                        await QuestHelper.takeSingleItem( player, ANTIDOTE, 5 )
                                        await QuestHelper.takeSingleItem( player, GREATER_HEALING_POTION, 1 )

                                        state.setMemoState( 10 )

                                        return this.getPath( '30864-24.html' )
                                    }

                                    await QuestHelper.giveSingleItem( player, WHITE_FABRIC_2, 3 )
                                    await QuestHelper.takeSingleItem( player, ANTIDOTE, 5 )
                                    await QuestHelper.takeSingleItem( player, GREATER_HEALING_POTION, 1 )

                                    state.setMemoState( 10 )
                                    state.setConditionWithSound( 23, true )

                                    return this.getPath( '30864-39.html' )
                                }

                                break

                            case 10:
                                if ( QuestHelper.getQuestItemsCount( player, WHITE_FABRIC_1 ) === 1 ) {
                                    return this.getPath( '30864-25.html' )
                                }

                                if ( state.getMemoStateEx( 1 ) > 0 ) {
                                    let whiteFabricAmount = QuestHelper.getQuestItemsCount( player, WHITE_FABRIC_2 )
                                    if ( whiteFabricAmount > 1 ) {
                                        return this.getPath( '30864-40.html' )
                                    }

                                    if ( whiteFabricAmount === 1 && !QuestHelper.hasQuestItems( player, BOOK_OF_SAINT, BLOOD_OF_SAINT, BOUGH_OF_SAINT ) ) {
                                        await QuestHelper.giveSingleItem( player, WHITE_FABRIC_1, 1 )
                                        await QuestHelper.takeSingleItem( player, WHITE_FABRIC_2, 1 )

                                        state.setMemoState( 10 )
                                        return this.getPath( '30864-41.html' )
                                    }
                                }

                                break

                            case 11:
                                if ( QuestHelper.getQuestItemsCount( player, WHITE_FABRIC_1 ) !== 1 ) {
                                    break
                                }

                                if ( ( state.getMemoStateEx( 1 ) > 0 ) ) {
                                    switch ( state.getMemoStateEx( 1 ) ) {
                                        case 1:
                                            await QuestHelper.giveAdena( player, 43000, true )
                                            break

                                        case 2:
                                            await QuestHelper.giveAdena( player, 4000, true )
                                            break

                                        case 3:
                                            await QuestHelper.giveAdena( player, 13000, true )
                                            break
                                    }

                                    state.setMemoStateEx( 0, 12 )
                                    state.setMemoStateEx( 1, 100 )
                                    state.setConditionWithSound( 24, true )

                                    return this.getPath( '30864-27.html' )
                                }

                                return this.getPath( '30864-28.html' )

                            case 12:
                                if ( QuestHelper.getQuestItemsCount( player, WHITE_FABRIC_1 ) === 1 ) {
                                    return this.getPath( '30864-31.html' )
                                }

                                break

                            case 13:
                                if ( QuestHelper.getQuestItemsCount( player, WHITE_FABRIC_1 ) === 1 ) {
                                    return this.getPath( '30864-32.html' )
                                }

                                break

                            case 14:
                                await this.rewardPlayer( player )
                                state.setMemoState( 15 )

                                return this.getPath( '30864-42.html' )

                            case 15:
                                return this.getPath( '30864-43.html' )

                            case 16:
                                let amountToReward = QuestHelper.hasQuestItem( player, BLOODED_FABRIC ) ? 9 : 10
                                await QuestHelper.giveSingleItem( player, WHITE_FABRIC_1, amountToReward )

                                state.setMemoState( 17 )
                                state.setMemoStateEx( 0, 17 )
                                state.setMemoStateEx( 1, 0 )
                                state.setConditionWithSound( 26, true )

                                return this.getPath( '30864-44.html' )

                            case 17:
                                if ( QuestHelper.hasQuestItem( player, WHITE_FABRIC_1 ) ) {
                                    return this.getPath( '30864-45.html' )
                                }

                                let bloodedFabricCount = QuestHelper.getQuestItemsCount( player, BLOODED_FABRIC )
                                if ( bloodedFabricCount >= 10 ) {
                                    return this.getPath( '30864-46.html' )
                                }

                                let adenaAmount = ( bloodedFabricCount * 1000 ) + 4000
                                await QuestHelper.giveAdena( player, adenaAmount, true )
                                await QuestHelper.takeSingleItem( player, BLOODED_FABRIC, -1 )
                                await state.exitQuest( true, true )

                                return this.getPath( '30864-48.html' )

                            case 18:
                                let memoStateEx = state.getMemoStateEx( 1 )
                                if ( ( memoStateEx % 10 ) < 7 ) {
                                    let i1 = 0
                                    let i2 = 0
                                    let i0 = memoStateEx % 10
                                    if ( i0 >= 4 ) {
                                        i1 = i1 + 6
                                        i0 = i0 - 4
                                        i2 = i2 + 1
                                    }

                                    if ( i0 >= 2 ) {
                                        i0 = i0 - 2
                                        i1 = i1 + 1
                                        i2 = i2 + 1
                                    }

                                    if ( i0 >= 1 ) {
                                        i1 = i1 + 3
                                        i2 = i2 + 1
                                        i0 = i0 - 1
                                    }

                                    if ( i0 === 0 ) {
                                        let bloodedFabricCount = QuestHelper.getQuestItemsCount( player, BLOODED_FABRIC )
                                        if ( ( bloodedFabricCount + i1 ) >= 10 ) {
                                            return this.getPath( '30864-52.html' )
                                        }

                                        if ( i2 === 2 ) {
                                            await QuestHelper.giveAdena( player, 24000, true )
                                        } else if ( i2 === 1 ) {
                                            await QuestHelper.giveAdena( player, 12000, true )
                                        }

                                        await state.exitQuest( true, true )

                                        return this.getPath( '30864-53.html' )
                                    }
                                }

                                if ( ( memoStateEx % 10 ) === 7 ) {
                                    state.setConditionWithSound( 28, true )
                                    state.setMemoState( 19 )

                                    await this.rewardPlayer( player )
                                    return this.getPath( '30864-54.html' )
                                }

                                break

                            case 19:
                                return this.getPath( '30864-49.html' )
                        }

                        break

                    case IASON_HEINE:
                        if ( state.getMemoStateEx( 0 ) !== 18 ) {
                            break
                        }

                        if ( ( state.getMemoStateEx( 1 ) % 8 ) < 4 ) {
                            if ( QuestHelper.getQuestItemsCount( player, BLOODED_FABRIC ) >= 6 ) {
                                await QuestHelper.takeSingleItem( player, BLOODED_FABRIC, 6 )
                                state.setMemoStateEx( 1, state.getMemoStateEx( 1 ) + 4 )
                                return this.getPath( '30969-01.html' )
                            }

                            return this.getPath( '30969-02.html' )
                        }

                        return this.getPath( '30969-03.html' )

                    case HOLY_ARK_OF_SECRECY_1:
                        if ( QuestHelper.hasQuestItem( player, FIRST_KEY_OF_ARK ) ) {
                            await QuestHelper.giveSingleItem( player, BLOOD_OF_SAINT, 1 )
                            await QuestHelper.takeSingleItem( player, FIRST_KEY_OF_ARK, 1 )

                            state.clearRadar()
                            if ( ( state.getMemoStateEx( 1 ) % 10 ) === 0 ) {
                                if ( QuestHelper.hasQuestItems( player, BOOK_OF_SAINT, BOUGH_OF_SAINT ) ) {
                                    state.setConditionWithSound( 21, true )
                                }
                            } else {
                                state.setConditionWithSound( 20, true )
                            }

                            state.setMemoStateEx( 1, state.getMemoStateEx( 1 ) - 20 )
                            if ( Math.floor( ( ( state.getMemoStateEx( 1 ) - 20 ) % 100 ) / 10 ) === 0 ) {
                                state.setMemoStateEx( 0, state.getMemoStateEx( 0 ) + 1 )
                            }

                            if ( ( ( state.getMemoStateEx( 1 ) - 20 ) % 10 ) === 1 ) {
                                state.setMemoStateEx( 0, 8 )
                            }

                            return this.getPath( '30977-01.html' )
                        }

                        if ( state.getMemoState() <= 8
                                && Math.floor( ( state.getMemoStateEx( 1 ) % 100 ) / 10 ) === 0
                                && QuestHelper.hasQuestItem( player, BLOOD_OF_SAINT ) ) {
                            return this.getPath( '30977-02.html' )
                        }

                        if ( state.getMemoState() < 8
                                && Math.floor( ( state.getMemoStateEx( 1 ) % 100 ) / 10 ) === 1
                                && !QuestHelper.hasQuestItem( player, BLOOD_OF_SAINT ) ) {
                            return this.getPath( '30977-03.html' )
                        }

                        break

                    case HOLY_ARK_OF_SECRECY_2:
                        if ( QuestHelper.hasQuestItem( player, SECOND_KEY_OF_ARK ) ) {
                            await QuestHelper.giveSingleItem( player, BOOK_OF_SAINT, 1 )
                            await QuestHelper.takeSingleItem( player, SECOND_KEY_OF_ARK, 1 )

                            state.clearRadar()
                            if ( ( state.getMemoStateEx( 1 ) % 10 ) === 0 ) {
                                if ( QuestHelper.hasQuestItems( player, BLOOD_OF_SAINT, BOUGH_OF_SAINT ) ) {
                                    state.setConditionWithSound( 21, true )
                                }
                            } else {
                                state.setConditionWithSound( 12, true )
                            }

                            state.setMemoStateEx( 1, state.getMemoStateEx( 1 ) - 200 )
                            if ( Math.floor( ( ( state.getMemoStateEx( 1 ) - 200 ) % 1000 ) / 100 ) === 0 ) {
                                state.setMemoStateEx( 0, state.getMemoStateEx( 0 ) + 1 )
                            }

                            if ( ( ( state.getMemoStateEx( 1 ) - 200 ) % 10 ) === 2 ) {
                                state.setMemoStateEx( 0, 8 )
                            }

                            return this.getPath( '30978-01.html' )
                        }

                        if ( ( state.getMemoState() < 8 ) && Math.floor( ( state.getMemoStateEx( 1 ) % 1000 ) / 100 ) === 1 ) {
                            if ( ( state.getMemoStateEx( 1 ) % 10 ) !== 0 ) {
                                state.setConditionWithSound( 10, true )
                            }

                            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                            QuestHelper.addGenericSpawn( null, ARK_GUARDIAN_ELBEROTH, player.getX(), player.getY(), player.getZ(), 0, false, 0, false ) // ark_guardian_elberoth
                            return this.getPath( '30978-02.html' )
                        }

                        if ( state.getMemoState() <= 8
                                && Math.floor( ( state.getMemoStateEx( 1 ) % 1000 ) / 100 ) === 0
                                && QuestHelper.hasQuestItem( player, BOOK_OF_SAINT ) ) {
                            return this.getPath( '30978-03.html' )
                        }

                        break

                    case HOLY_ARK_OF_SECRECY_3:
                        if ( QuestHelper.hasQuestItem( player, THIRD_KEY_OF_ARK ) ) {
                            await QuestHelper.giveSingleItem( player, BOUGH_OF_SAINT, 1 )
                            await QuestHelper.takeSingleItem( player, THIRD_KEY_OF_ARK, 1 )

                            state.clearRadar()
                            if ( ( state.getMemoStateEx( 1 ) % 10 ) === 0 ) {
                                if ( QuestHelper.hasQuestItems( player, BLOOD_OF_SAINT, BOOK_OF_SAINT ) ) {
                                    state.setConditionWithSound( 21, true )
                                }
                            } else {
                                state.setConditionWithSound( 16, true )
                            }

                            state.setMemoStateEx( 1, state.getMemoStateEx( 1 ) - 2000 )
                            if ( Math.floor( ( ( state.getMemoStateEx( 1 ) - 2000 ) % 10000 ) / 1000 ) === 0 ) {
                                state.setMemoStateEx( 0, state.getMemoStateEx( 0 ) + 1 )
                            }

                            if ( ( ( state.getMemoStateEx( 1 ) - 2000 ) % 10 ) === 3 ) {
                                state.setMemoStateEx( 0, 8 )
                            }

                            return this.getPath( '30979-01.html' )
                        }

                        if ( ( state.getMemoState() < 8 )
                                && Math.floor( ( state.getMemoStateEx( 1 ) % 10000 ) / 1000 ) === 1 ) {
                            if ( ( state.getMemoStateEx( 1 ) % 10 ) !== 0 ) {
                                state.setConditionWithSound( 14, true )
                            }

                            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                            QuestHelper.addGenericSpawn( null, ARK_GUARDIAN_SHADOWFANG, player.getX(), player.getY(), player.getZ(), 0, false, 0, false ) // ark_guardian_shadowfang

                            return this.getPath( '30979-02.html' )

                        }

                        if ( state.getMemoState() <= 8
                                && ( ( ( state.getMemoStateEx( 1 ) % 10000 ) / 1000 ) === 0 )
                                && QuestHelper.hasQuestItem( player, BOUGH_OF_SAINT ) ) {
                            return this.getPath( '30979-03.html' )
                        }
                        break

                    case ARK_GUARDIANS_CORPSE:
                        if ( QuestHelper.hasAtLeastOneQuestItem( player, FIRST_KEY_OF_ARK, BLOOD_OF_SAINT ) ) {
                            return this.getPath( '30980-01.html' )
                        }

                        if ( state.getMemoState() >= 8 ) {
                            break
                        }

                        switch ( Math.floor( ( state.getMemoStateEx( 1 ) % 100 ) / 10 ) ) {
                            case 1:
                                state.clearRadar()
                                if ( ( state.getMemoStateEx( 1 ) % 10 ) !== 0 ) {
                                    state.setConditionWithSound( 18, true )
                                }

                                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                                QuestHelper.addGenericSpawn( null, ANGEL_KILLER, player.getX(), player.getY(), player.getZ(), 0, false, 0, false )
                                return this.getPath( '30980-02.html' )

                            case 2:
                                await QuestHelper.giveSingleItem( player, FIRST_KEY_OF_ARK, 1 )

                                state.addRadar( -418, 44174, -3568 )
                                return this.getPath( '30980-03.html' )
                        }

                        break

                    case CLAUDIA_ATHEBALDT:
                        if ( QuestHelper.hasQuestItem( player, HANELLINS_2ND_LETTER ) ) {
                            await QuestHelper.takeSingleItem( player, HANELLINS_2ND_LETTER, 1 )

                            let value = state.getMemoStateEx( 1 ) + 100
                            if ( ( value % 10 ) === 0 ) {
                                state.addRadar( 181472, 7158, -2725 )
                            } else {
                                state.setConditionWithSound( 9, true )
                            }

                            state.setMemoStateEx( 1, value )
                            return this.getPath( '31001-01.html' )
                        }

                        if ( state.getMemoState() < 8
                                && Math.floor( ( state.getMemoStateEx( 1 ) % 1000 ) / 100 ) === 1
                                && !QuestHelper.hasQuestItem( player, SECOND_KEY_OF_ARK ) ) {

                            if ( ( state.getMemoStateEx( 1 ) % 10 ) === 0 ) {
                                state.addRadar( 181472, 7158, -2725 )
                            }

                            return this.getPath( '31001-03.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, SECOND_KEY_OF_ARK ) ) {
                            if ( ( state.getMemoStateEx( 1 ) % 10 ) === 0 ) {
                                state.addRadar( 181472, 7158, -2725 )
                            }

                            return this.getPath( '31001-04.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, BOOK_OF_SAINT ) ) {
                            return this.getPath( '31001-05.html' )
                        }
                        break

                    case HARNE:
                        if ( QuestHelper.hasQuestItem( player, HANELLINS_1ST_LETTER ) ) {
                            await QuestHelper.takeSingleItem( player, HANELLINS_1ST_LETTER, 1 )

                            let value = state.getMemoStateEx( 1 ) + 10
                            if ( ( value % 10 ) === 0 ) {
                                state.addRadar( 2908, 44128, -2712 )
                            } else {
                                state.setConditionWithSound( 17, true )
                            }

                            state.setMemoStateEx( 1, value )
                            return this.getPath( '30144-01.html' )
                        }

                        if ( state.getMemoState() < 8
                                && Math.floor( ( state.getMemoStateEx( 1 ) % 100 ) / 10 ) === 1
                                && !QuestHelper.hasQuestItem( player, FIRST_KEY_OF_ARK ) ) {

                            if ( ( state.getMemoStateEx( 1 ) % 10 ) === 0 ) {
                                state.addRadar( 2908, 44128, -2712 )
                            }

                            return this.getPath( '30144-03.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, FIRST_KEY_OF_ARK ) ) {
                            if ( ( state.getMemoStateEx( 1 ) % 10 ) === 0 ) {
                                state.addRadar( 2908, 44128, -2712 )
                            }

                            return this.getPath( '30144-04.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, BLOOD_OF_SAINT ) ) {
                            return this.getPath( '30144-05.html' )
                        }

                        break

                    case MARTIEN:
                        if ( QuestHelper.hasQuestItem( player, HANELLINS_3RD_LETTER ) ) {
                            await QuestHelper.takeSingleItem( player, HANELLINS_3RD_LETTER, 1 )

                            let value = state.getMemoStateEx( 1 ) + 1000
                            if ( ( value % 10 ) === 0 ) {
                                state.addRadar( 50693, 158674, 376 )
                            } else {
                                state.setConditionWithSound( 13, true )
                            }

                            state.setMemoStateEx( 1, value )
                            return this.getPath( '30645-01.html' )
                        }

                        if ( state.getMemoState() < 8
                                && Math.floor( ( state.getMemoStateEx( 1 ) % 10000 ) / 1000 ) === 1
                                && !QuestHelper.hasQuestItem( player, THIRD_KEY_OF_ARK ) ) {

                            if ( ( state.getMemoStateEx( 1 ) % 10 ) === 0 ) {
                                state.addRadar( 50693, 158674, 376 )
                            }

                            return this.getPath( '30645-03.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, THIRD_KEY_OF_ARK ) ) {
                            if ( ( state.getMemoStateEx( 1 ) % 10 ) === 0 ) {
                                state.addRadar( 50693, 158674, 376 )
                            }

                            return this.getPath( '30645-04.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, BOUGH_OF_SAINT ) ) {
                            return this.getPath( '30645-05.html' )
                        }

                        break

                    case SIR_GUSTAV_ATHEBALDT:
                        if ( state.getMemoStateEx( 0 ) !== 18 ) {
                            break
                        }

                        if ( ( state.getMemoStateEx( 1 ) % 2 ) === 0 ) {
                            if ( QuestHelper.getQuestItemsCount( player, BLOODED_FABRIC ) >= 3 ) {
                                await QuestHelper.takeSingleItem( player, BLOODED_FABRIC, 3 )

                                state.setMemoStateEx( 1, state.getMemoStateEx( 1 ) + 1 )
                                return this.getPath( '30760-01.html' )
                            }

                            return this.getPath( '30760-02.html' )
                        }

                        if ( ( state.getMemoStateEx( 1 ) % 2 ) === 1 ) {
                            return this.getPath( '30760-03.html' )
                        }

                        break

                    case HARDIN:
                        if ( state.getMemoStateEx( 0 ) !== 18 ) {
                            break
                        }

                        if ( ( state.getMemoStateEx( 1 ) % 4 ) < 2 ) {
                            let bloodyFabricAmount = QuestHelper.getQuestItemsCount( player, BLOODED_FABRIC )
                            if ( bloodyFabricAmount >= 1 ) {
                                await QuestHelper.takeSingleItem( player, BLOODED_FABRIC, 1 )

                                state.setMemoStateEx( 1, state.getMemoStateEx( 1 ) + 2 )
                                return this.getPath( '30832-01.html' )
                            }

                            if ( bloodyFabricAmount < 3 ) {
                                return this.getPath( '30832-02.html' )
                            }
                        }

                        return this.getPath( '30832-03.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async onAngelAttacked( data: AttackableAttackedEvent ): Promise<void> {
        let state: QuestState = QuestStateCache.getQuestState( data.attackerPlayerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.attackerPlayerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !QuestHelper.hasAtLeastOneQuestItem( player, FIRST_KEY_OF_ARK, BLOOD_OF_SAINT ) ) {

            if ( state.getMemoStateEx( 0 ) >= 8 ) {
                return
            }

            switch ( Math.floor( ( state.getMemoStateEx( 1 ) % 100 ) / 10 ) ) {
                case 1:
                    if ( npc.getCurrentHp() < npc.getMaxHp() * MIN_HP_PERCENTAGE ) {

                        state.setMemoStateEx( 1, state.getMemoStateEx( 1 ) + 10 )

                        if ( ( state.getMemoStateEx( 1 ) % 10 ) === 0 ) {
                            state.clearRadar()
                            state.addRadar( -2908, 44128, -2712 )
                        } else {
                            state.setConditionWithSound( 19, true )
                        }

                        BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.HA_THAT_WAS_FUN_IF_YOU_WISH_TO_FIND_THE_KEY_SEARCH_THE_CORPSE )
                        await npc.deleteMe()
                    }

                    return

                case 2:
                    BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.WE_DONT_HAVE_ANY_FURTHER_BUSINESS_TO_DISCUSS_HAVE_YOU_SEARCHED_THE_CORPSE_FOR_THE_KEY )
                    await npc.deleteMe()

                    return
            }

            return
        }

        BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.WE_DONT_HAVE_ANY_FURTHER_BUSINESS_TO_DISCUSS_HAVE_YOU_SEARCHED_THE_CORPSE_FOR_THE_KEY )
        await npc.deleteMe()
    }

    onGuardianAttacked( data: AttackableAttackedEvent, npcStringId: number ): void {
        if ( !NpcVariablesManager.get( data.targetId, this.getName() ) ) {
            NpcVariablesManager.set( data.targetId, this.getName(), true )
            BroadcastHelper.broadcastNpcSayStringId( L2World.getObjectById( data.targetId ) as L2Npc, NpcSayType.NpcAll, npcStringId )
        }
    }

    async onPlatinumTribeAttacked( data: AttackableAttackedEvent, increment: number ) {
        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.attackerPlayerId ), -1, 3, L2World.getObjectById( data.targetId ) as L2Npc )
        if ( !state ) {
            return
        }

        if ( !QuestHelper.hasQuestItem( state.getPlayer(), WHITE_FABRIC_1 ) ) {
            return
        }

        let player = state.getPlayer()
        switch ( state.getMemoStateEx( 0 ) ) {
            case 12:
                state.setMemoStateEx( 1, state.getMemoStateEx( 1 ) + increment )
                if ( ( state.getMemoStateEx( 1 ) + increment ) > 80000 ) {
                    await QuestHelper.rewardSingleQuestItem( player, BLOODED_FABRIC, 1, data.isChampion )
                    await QuestHelper.takeSingleItem( player, WHITE_FABRIC_1, 1 )
                    await state.exitQuest( true, true )
                }

                return

            case 13:
                state.setMemoStateEx( 1, state.getMemoStateEx( 1 ) + increment )

                if ( ( state.getMemoStateEx( 1 ) + increment ) > 100000 ) {
                    await QuestHelper.rewardSingleQuestItem( player, BLOODED_FABRIC, 1, data.isChampion )
                    await QuestHelper.takeSingleItem( player, WHITE_FABRIC_1, 1 )

                    state.setMemoState( 14 )
                    state.setMemoStateEx( 0, 14 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                }
        }
    }

    async onPlatinumTribeKilled( player: L2PcInstance, state: QuestState, increment: number ): Promise<void> {
        if ( !QuestHelper.hasQuestItem( player, WHITE_FABRIC_1 ) ) {
            return
        }

        switch ( state.getMemoStateEx( 0 ) ) {
            case 12:

                if ( ( state.getMemoStateEx( 1 ) + 2 * increment ) > 80000 ) {
                    await QuestHelper.giveSingleItem( player, BLOODED_FABRIC, 1 )
                    await QuestHelper.takeSingleItem( player, WHITE_FABRIC_1, 1 )
                    await state.exitQuest( true, true )

                    return
                }

                state.setMemoStateEx( 1, state.getMemoStateEx( 1 ) + increment )

                return

            case 13:
                if ( ( state.getMemoStateEx( 1 ) + 2 * increment ) > 100000 ) {
                    await QuestHelper.giveSingleItem( player, BLOODED_FABRIC, 1 )
                    await QuestHelper.takeSingleItem( player, WHITE_FABRIC_1, 1 )

                    state.setMemoState( 14 )
                    state.setMemoStateEx( 0, 14 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                }

                state.setMemoStateEx( 1, state.getMemoStateEx( 1 ) + increment )

                return
        }
    }

    async rewardPlayer( player: L2PcInstance ): Promise<void> {
        switch ( player.getClassId() ) {
            case ClassIdValues.treasureHunter.id:
            case ClassIdValues.plainsWalker.id:
            case ClassIdValues.abyssWalker.id:
            case ClassIdValues.adventurer.id:
            case ClassIdValues.windRider.id:
            case ClassIdValues.ghostHunter.id:
            case ClassIdValues.maleSoulbreaker.id:
            case ClassIdValues.femaleSoulbreaker.id:
            case ClassIdValues.maleSoulhound.id:
            case ClassIdValues.femaleSoulhound.id:
            case ClassIdValues.inspector.id:
            case ClassIdValues.judicator.id:
                if ( player.getLevel() < 69 ) {
                    await QuestHelper.rewardSingleItem( player, KRIS_EDGE, 1 )
                    await QuestHelper.rewardSingleItem( player, SYNTHETIC_COKES, 2 )

                    return
                }

                await QuestHelper.rewardSingleItem( player, DEMONS_DAGGER_EDGE, 1 )
                await QuestHelper.rewardSingleItem( player, COKES, 2 )

                return

            case ClassIdValues.tyrant.id:
            case ClassIdValues.grandKhavatari.id:
                if ( player.getLevel() < 69 ) {
                    await QuestHelper.rewardMultipleItems( player, 1, ARTHRO_NAIL_BLADE, COKES )
                    await QuestHelper.rewardSingleItem( player, SYNTHETIC_COKES, 2 )

                    return
                }

                await QuestHelper.rewardSingleItem( player, BELLION_CESTUS_EDGE, 1 )
                await QuestHelper.rewardSingleItem( player, ORIHARUKON_ORE, 2 )

                return

            case ClassIdValues.paladin.id:
            case ClassIdValues.darkAvenger.id:
            case ClassIdValues.prophet.id:
            case ClassIdValues.templeKnight.id:
            case ClassIdValues.swordSinger.id:
            case ClassIdValues.shillienKnight.id:
            case ClassIdValues.bladedancer.id:
            case ClassIdValues.shillenElder.id:
            case ClassIdValues.phoenixKnight.id:
            case ClassIdValues.hellKnight.id:
            case ClassIdValues.hierophant.id:
            case ClassIdValues.evaTemplar.id:
            case ClassIdValues.swordMuse.id:
            case ClassIdValues.shillienTemplar.id:
            case ClassIdValues.spectralDancer.id:
            case ClassIdValues.shillienSaint.id:
                if ( player.getLevel() < 69 ) {
                    await QuestHelper.rewardSingleItem( player, KESHANBERK_BLADE, 1 )
                    await QuestHelper.rewardSingleItem( player, SYNTHETIC_COKES, 2 )

                    return
                }

                await QuestHelper.rewardSingleItem( player, SWORD_OF_DAMASCUS_BLADE, 1 )
                await QuestHelper.rewardSingleItem( player, ORIHARUKON_ORE, 2 )

                return

            case ClassIdValues.hawkeye.id:
            case ClassIdValues.silverRanger.id:
            case ClassIdValues.phantomRanger.id:
            case ClassIdValues.sagittarius.id:
            case ClassIdValues.moonlightSentinel.id:
            case ClassIdValues.ghostSentinel.id:
            case ClassIdValues.arbalester.id:
            case ClassIdValues.trickster.id:
                if ( player.getLevel() < 69 ) {
                    await QuestHelper.rewardSingleItem( player, DARK_ELVEN_LONGBOW_SHAFT, 1 )
                    await QuestHelper.rewardSingleItem( player, SYNTHETIC_COKES, 2 )

                    return
                }

                await QuestHelper.rewardSingleItem( player, BOW_OF_PERIL_SHAFT, 1 )
                await QuestHelper.rewardSingleItem( player, COARSE_BONE_POWDER, 9 )

                return

            case ClassIdValues.gladiator.id:
            case ClassIdValues.bishop.id:
            case ClassIdValues.elder.id:
            case ClassIdValues.duelist.id:
            case ClassIdValues.cardinal.id:
            case ClassIdValues.evaSaint.id:
                if ( player.getLevel() < 69 ) {
                    await QuestHelper.rewardMultipleItems( player, 1, HEAVY_WAR_AXE_HEAD, COKES )
                    await QuestHelper.rewardSingleItem( player, SYNTHETIC_COKES, 2 )

                    return
                }

                await QuestHelper.rewardSingleItem( player, ART_OF_BATTLE_AXE_BLADE, 1 )
                await QuestHelper.rewardSingleItem( player, ORIHARUKON_ORE, 2 )

                return

            case ClassIdValues.warlord.id:
            case ClassIdValues.bountyHunter.id:
            case ClassIdValues.warsmith.id:
            case ClassIdValues.dreadnought.id:
            case ClassIdValues.fortuneSeeker.id:
            case ClassIdValues.maestro.id:
                if ( player.getLevel() < 63 ) {
                    await QuestHelper.rewardMultipleItems( player, 1, GREAT_AXE_HEAD, ENRIA, COKES )

                    return
                }

                await QuestHelper.rewardSingleItem( player, LANCE_BLADE, 1 )
                await QuestHelper.rewardSingleItem( player, ORIHARUKON_ORE, 2 )

                return

            case ClassIdValues.sorceror.id:
            case ClassIdValues.spellsinger.id:
            case ClassIdValues.overlord.id:
            case ClassIdValues.archmage.id:
            case ClassIdValues.mysticMuse.id:
            case ClassIdValues.dominator.id:
                if ( player.getLevel() < 63 ) {
                    await QuestHelper.rewardMultipleItems( player, 1, SPRITES_STAFF_HEAD, COARSE_BONE_POWDER )
                    await QuestHelper.rewardSingleItem( player, ORIHARUKON_ORE, 4 )

                    return
                }

                await QuestHelper.rewardSingleItem( player, EVIL_SPIRIT_HEAD, 1 )
                await QuestHelper.rewardSingleItem( player, ANIMAL_BONE, 5 )

                return

            case ClassIdValues.necromancer.id:
            case ClassIdValues.spellhowler.id:
            case ClassIdValues.soultaker.id:
            case ClassIdValues.stormScreamer.id:
                await QuestHelper.rewardSingleItem( player, HELL_KNIFE_EDGE, 1 )
                await QuestHelper.rewardMultipleItems( player, 2, SYNTHETIC_COKES, ANIMAL_BONE )

                return

            case ClassIdValues.destroyer.id:
            case ClassIdValues.titan.id:
            case ClassIdValues.berserker.id:
            case ClassIdValues.doombringer.id:
                await QuestHelper.rewardSingleItem( player, GREAT_SWORD_BLADE, 1 )
                await QuestHelper.rewardMultipleItems( player, 2, VARNISH_OF_PURITY, SYNTHETIC_COKES )

                return

            case ClassIdValues.elementalSummoner.id:
            case ClassIdValues.phantomSummoner.id:
            case ClassIdValues.elementalMaster.id:
            case ClassIdValues.spectralMaster.id:
                await QuestHelper.rewardMultipleItems( player, 1, SWORD_OF_DAMASCUS_BLADE, ENRIA )

                return

            case ClassIdValues.warcryer.id:
            case ClassIdValues.doomcryer.id:
                await QuestHelper.rewardMultipleItems( player, 1, SWORD_OF_VALHALLA_BLADE, ORIHARUKON_ORE, VARNISH_OF_PURITY )

                return

            case ClassIdValues.warlock.id:
            case ClassIdValues.arcanaLord.id:
                await QuestHelper.rewardMultipleItems( player, 1, ART_OF_BATTLE_AXE_BLADE, ENRIA )

                return

            default:
                await QuestHelper.giveAdena( player, 49000, true )

                return
        }
    }
}