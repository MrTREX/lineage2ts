import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'

import _ from 'lodash'

const KAIEN = 30623
const GARVARENTZ = 30704
const GILMORE = 30754
const RODEMAI = 30756
const ORVEN = 30857
const ARTICLES = 4269

const OLD_KEY = 4270
const OLD_HILT = 4271
const TOTEM_NECKLACE = 4272
const CRUCIFIX = 4273
const ORIHARUKON_ORE = 1874
const VARNISH_OF_PURITY = 1887
const SCROLL_EWC = 951
const RAID_SWORD = 133
const COKES = 1879
const RING_OF_AGES = 885
const LEATHER = 1882
const COARSE_BONE_POWDER = 1881
const HEAVY_DOOM_HAMMER = 191
const STONE_OF_PURITY = 1875
const SCROLL_EAC = 952
const DRAKE_LEATHER_BOOTS = 2437

const minimumLevel = 48

const monsterRewardChances = {
    20236: 0.58, // Cave Servant
    20238: 0.75, // Cave Servant Warrior
    20237: 0.78, // Cave Servant Archer
    20239: 0.79, // Cave Servant Captain
    20240: 0.85, // Royal Cave Servant
    20272: 0.58, // Cave Servant
    20273: 0.78, // Cave Servant Archer
    20274: 0.75, // Cave Servant Warrior
    20275: 0.79, // Cave Servant Captain
    20276: 0.85, // Royal Cave Servant
}

export class A1000YearsTheEndOfLamentation extends ListenerLogic {
    constructor() {
        super( 'Q00344_A1000YearsTheEndOfLamentation', 'listeners/tracked-300/A1000YearsTheEndOfLamentation.ts' )
        this.questId = 344
        this.questItemIds = [
            ARTICLES,
            OLD_KEY,
            OLD_HILT,
            TOTEM_NECKLACE,
            CRUCIFIX,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getQuestStartIds(): Array<number> {
        return [ GILMORE ]
    }

    getTalkIds(): Array<number> {
        return [
            KAIEN,
            GARVARENTZ,
            GILMORE,
            RODEMAI,
            ORVEN,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00344_1000YearsTheEndOfLamentation'
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), 1, 3, L2World.getObjectById( data.targetId ) as L2Npc )
        if ( !state ) {
            return
        }

        if ( Math.random() > QuestHelper.getAdjustedChance( ARTICLES, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        let player = state.getPlayer()
        await QuestHelper.rewardSingleQuestItem( player, ARTICLES, 1, data.isChampion )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30754-03.htm':
            case '30754-16.html':
                break

            case '30754-04.htm':
                state.startQuest()
                break

            case '30754-08.html':
                if ( !state.isCondition( 1 ) ) {
                    return
                }

                let player = L2World.getPlayer( data.playerId )
                let count = QuestHelper.getQuestItemsCount( player, ARTICLES )
                if ( count === 0 ) {
                    return this.getPath( '30754-07.html' )
                }

                await QuestHelper.takeSingleItem( player, ARTICLES, -1 )
                if ( _.random( 1000 ) >= count ) {
                    let adenaAmount = count * 60
                    await QuestHelper.giveAdena( player, adenaAmount, true )

                    break
                }

                state.setConditionWithSound( 2, true )
                switch ( _.random( 3 ) ) {
                    case 0:
                        state.setMemoState( 1 )
                        await QuestHelper.giveSingleItem( player, OLD_HILT )
                        break

                    case 1:
                        state.setMemoState( 2 )
                        await QuestHelper.giveSingleItem( player, OLD_KEY )
                        break

                    case 2:
                        state.setMemoState( 3 )
                        await QuestHelper.giveSingleItem( player, TOTEM_NECKLACE )
                        break

                    case 3:
                        state.setMemoState( 4 )
                        await QuestHelper.giveSingleItem( player, CRUCIFIX )
                        break
                }

                return this.getPath( '30754-09.html' )

            case '30754-17.html':
                if ( state.isCondition( 1 ) ) {
                    await state.exitQuest( true, true )
                    break
                }

                return

            case 'relic_info':
                switch ( state.getMemoState() ) {
                    case 1:
                        return this.getPath( '30754-10.html' )

                    case 2:
                        return this.getPath( '30754-11.html' )

                    case 3:
                        return this.getPath( '30754-12.html' )

                    case 4:
                        return this.getPath( '30754-13.html' )
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case GILMORE:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() >= minimumLevel ? '30754-02.htm' : '30754-01.htm' )

                    case QuestStateValues.STARTED:
                        if ( state.isCondition( 1 ) ) {
                            return this.getPath( QuestHelper.hasQuestItem( player, ARTICLES ) ? '30754-06.html' : '30754-05.html' )
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, OLD_KEY, OLD_HILT, TOTEM_NECKLACE, CRUCIFIX ) ) {
                            return this.getPath( '30754-14.html' )
                        }

                        state.setConditionWithSound( 1 )
                        return this.getPath( '30754-15.html' )

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case KAIEN:
                if ( state.getMemoState() !== 1 ) {
                    break
                }

                if ( QuestHelper.hasQuestItem( player, OLD_HILT ) ) {
                    await QuestHelper.takeSingleItem( player, OLD_HILT, -1 )
                    await this.rewardKaienItems( player )

                    state.setConditionWithSound( 1 )
                    return this.getPath( '30623-01.html' )
                }

                return this.getPath( '30623-02.html' )

            case RODEMAI:
                if ( state.getMemoState() !== 2 ) {
                    break
                }

                if ( QuestHelper.hasQuestItem( player, OLD_KEY ) ) {
                    await QuestHelper.takeSingleItem( player, OLD_HILT, -1 )
                    await this.rewardRodemaiItems( player )

                    state.setConditionWithSound( 1 )
                    return this.getPath( '30756-01.html' )
                }

                return this.getPath( '30756-02.html' )

            case GARVARENTZ:
                if ( state.getMemoState() !== 3 ) {
                    break
                }

                if ( QuestHelper.hasQuestItem( player, TOTEM_NECKLACE ) ) {
                    await QuestHelper.takeSingleItem( player, TOTEM_NECKLACE, -1 )
                    await this.rewardGarvarentzItems( player )

                    state.setConditionWithSound( 1 )
                    return this.getPath( '30704-01.html' )
                }

                return this.getPath( '30704-02.html' )

            case ORVEN:
                if ( state.getMemoState() !== 4 ) {
                    break
                }

                if ( QuestHelper.hasQuestItem( player, CRUCIFIX ) ) {
                    await QuestHelper.takeSingleItem( player, CRUCIFIX, -1 )
                    await this.rewardOrvenItems( player )

                    state.setConditionWithSound( 1 )
                    return this.getPath( '30857-01.html' )
                }

                return this.getPath( '30857-02.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async rewardGarvarentzItems( player: L2PcInstance ): Promise<void> {
        let chance = Math.random()
        if ( chance <= 0.47 ) {
            return QuestHelper.rewardSingleItem( player, LEATHER, 70 )
        }

        if ( chance <= 0.97 ) {
            return QuestHelper.rewardSingleItem( player, COARSE_BONE_POWDER, 50 )
        }

        return QuestHelper.rewardSingleItem( player, HEAVY_DOOM_HAMMER, 1 )
    }

    async rewardKaienItems( player: L2PcInstance ): Promise<void> {
        let chance = Math.random()
        if ( chance <= 0.52 ) {
            return QuestHelper.rewardSingleItem( player, ORIHARUKON_ORE, 25 )
        }

        if ( chance <= 0.76 ) {
            return QuestHelper.rewardSingleItem( player, VARNISH_OF_PURITY, 10 )
        }

        if ( chance <= 0.98 ) {
            return QuestHelper.rewardSingleItem( player, SCROLL_EWC, 1 )
        }

        return QuestHelper.rewardSingleItem( player, RAID_SWORD, 1 )
    }

    async rewardOrvenItems( player: L2PcInstance ): Promise<void> {
        let chance = Math.random()
        if ( chance <= 49 ) {
            return QuestHelper.rewardSingleItem( player, STONE_OF_PURITY, 19 )
        }

        if ( chance <= 69 ) {
            return QuestHelper.rewardSingleItem( player, SCROLL_EAC )
        }

        return QuestHelper.rewardSingleItem( player, DRAKE_LEATHER_BOOTS, 5 )
    }

    async rewardRodemaiItems( player: L2PcInstance ): Promise<void> {
        let chance = Math.random()
        if ( chance <= 0.39 ) {
            return QuestHelper.rewardSingleItem( player, COKES, 55 )
        }

        if ( chance <= 0.89 ) {
            return QuestHelper.rewardSingleItem( player, SCROLL_EWC, 1 )
        }

        return QuestHelper.rewardSingleItem( player, RING_OF_AGES, 1 )
    }
}