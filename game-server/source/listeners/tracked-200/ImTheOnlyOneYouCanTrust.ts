import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

const KINTAIJIN = 32640
const STAKATO_FANG = 14879
const minimumLevel = 81

export class ImTheOnlyOneYouCanTrust extends ListenerLogic {
    constructor() {
        super( 'Q00240_ImTheOnlyOneYouCanTrust', 'listeners/tracked-200/ImTheOnlyOneYouCanTrust.ts' )
        this.questId = 240
        this.questItemIds = [ STAKATO_FANG ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            22617,
            22618,
            22619,
            22620,
            22621,
            22622,
            22623,
            22624,
            22625,
            22626,
            22627,
            22628,
            22629,
            22630,
            22631,
            22632,
            22633,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00240_ImTheOnlyOneYouCanTrust'
    }

    getQuestStartIds(): Array<number> {
        return [ KINTAIJIN ]
    }

    getTalkIds(): Array<number> {
        return [ KINTAIJIN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player: L2PcInstance = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 1 )
        if ( !player ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, STAKATO_FANG, 1, data.isChampion )
        if ( QuestHelper.getQuestItemsCount( player, STAKATO_FANG ) >= 25 ) {
            let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
            state.setConditionWithSound( 2, true )

            return
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || data.eventName !== '32640-3.htm' ) {
            return
        }

        state.startQuest()
        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '32640-1.htm' : '32640-0.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( QuestHelper.hasQuestItem( player, STAKATO_FANG ) ? '32640-9.html' : '32640-8.html' )

                    case 2:
                        if ( QuestHelper.getQuestItemsCount( player, STAKATO_FANG ) < 25 ) {
                            break
                        }

                        await QuestHelper.giveAdena( player, 147200, true )
                        await QuestHelper.takeSingleItem( player, STAKATO_FANG, -1 )
                        await QuestHelper.addExpAndSp( player, 589542, 36800 )
                        await state.exitQuest( false, true )

                        return this.getPath( '32640-10.html' )
                }

                break

            case QuestStateValues.COMPLETED:
                return this.getPath( '32640-11.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}