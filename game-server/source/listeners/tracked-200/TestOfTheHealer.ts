import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestHelper } from '../helpers/QuestHelper'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'
import { QuestVariables } from '../helpers/QuestVariables'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { CategoryType } from '../../gameService/enums/CategoryType'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import _ from 'lodash'

const MASTER_SORIUS = 30327
const ALLANA = 30424
const PERRIN = 30428
const PRIEST_BANDELLOS = 30473
const FATHER_GUPU = 30658
const ORPHAN_GIRL = 30659
const WINDY_SHAORING = 30660
const MYSTERIOUS_DARK_ELF = 30661
const PIPER_LONGBOW = 30662
const SLEIN_SHINING_BLADE = 30663
const CAIN_FLYING_KNIFE = 30664
const SAINT_KRISTINA = 30665
const DAURIN_HAMMERCRUSH = 30674

const ADENA = 57
const REPORT_OF_PERRIN = 2810
const CRISTINAS_LETTER = 2811
const PICTURE_OF_WINDY = 2812
const GOLDEN_STATUE = 2813
const WINDYS_PEBBLES = 2814
const ORDER_OF_SORIUS = 2815
const SECRET_LETTER1 = 2816
const SECRET_LETTER2 = 2817
const SECRET_LETTER3 = 2818
const SECRET_LETTER4 = 2819

const MARK_OF_HEALER = 2820
const DIMENSIONAL_DIAMOND = 7562

const LERO_LIZARDMAN_AGENT = 27122
const LERO_LIZARDMAN_LEADER = 27123
const LERO_LIZARDMAN_ASSASSIN = 27124
const LERO_LIZARDMAN_SNIPER = 27125
const LERO_LIZARDMAN_WIZARD = 27126
const LERO_LIZARDMAN_LORD = 27127
const TATOMA = 27134

const minimumLevel = 39

const diamondRewardMap = {
    [ ClassIdValues.cleric.id ]: 60,
    [ ClassIdValues.knight.id ]: 104,
    [ ClassIdValues.oracle.id ]: 45,
}

const defaultDiamondReward = 72

export class TestOfTheHealer extends ListenerLogic {
    constructor() {
        super( 'Q00226_TestOfTheHealer', 'listeners/tracked-200/TestOfTheHealer.ts' )
        this.questId = 226
        this.questItemIds = [
            REPORT_OF_PERRIN,
            CRISTINAS_LETTER,
            PICTURE_OF_WINDY,
            GOLDEN_STATUE,
            WINDYS_PEBBLES,
            ORDER_OF_SORIUS,
            SECRET_LETTER1,
            SECRET_LETTER2,
            SECRET_LETTER3,
            SECRET_LETTER4,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            LERO_LIZARDMAN_AGENT,
            LERO_LIZARDMAN_LEADER,
            LERO_LIZARDMAN_ASSASSIN,
            LERO_LIZARDMAN_SNIPER,
            LERO_LIZARDMAN_WIZARD,
            LERO_LIZARDMAN_LORD,
            TATOMA,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00226_TestOfTheHealer'
    }

    getQuestStartIds(): Array<number> {
        return [ PRIEST_BANDELLOS ]
    }

    getTalkIds(): Array<number> {
        return [
            PRIEST_BANDELLOS,
            MASTER_SORIUS,
            ALLANA,
            PERRIN,
            FATHER_GUPU,
            ORPHAN_GIRL,
            WINDY_SHAORING,
            MYSTERIOUS_DARK_ELF,
            PIPER_LONGBOW,
            SLEIN_SHINING_BLADE,
            CAIN_FLYING_KNIFE,
            SAINT_KRISTINA,
            DAURIN_HAMMERCRUSH,
        ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case LERO_LIZARDMAN_LEADER:
                if ( state.isMemoState( 6 ) && !QuestHelper.hasQuestItem( player, SECRET_LETTER1 ) ) {
                    await QuestHelper.giveSingleItem( player, SECRET_LETTER1, 1 )
                    state.setConditionWithSound( 12, true )
                }

                return

            case LERO_LIZARDMAN_ASSASSIN:
                if ( state.isMemoState( 8 )
                        && QuestHelper.hasQuestItem( player, SECRET_LETTER1 )
                        && !QuestHelper.hasQuestItem( player, SECRET_LETTER2 ) ) {
                    await QuestHelper.giveSingleItem( player, SECRET_LETTER2, 1 )
                    state.setConditionWithSound( 15, true )
                }

                return

            case LERO_LIZARDMAN_SNIPER:
                if ( state.isMemoState( 8 )
                        && QuestHelper.hasQuestItem( player, SECRET_LETTER1 )
                        && !QuestHelper.hasQuestItem( player, SECRET_LETTER3 ) ) {
                    await QuestHelper.giveSingleItem( player, SECRET_LETTER3, 1 )
                    state.setConditionWithSound( 17, true )
                }

                return

            case LERO_LIZARDMAN_LORD:
                if ( state.isMemoState( 8 )
                        && QuestHelper.hasQuestItem( player, SECRET_LETTER1 )
                        && !QuestHelper.hasQuestItem( player, SECRET_LETTER4 ) ) {
                    await QuestHelper.giveSingleItem( player, SECRET_LETTER4, 1 )
                    state.setConditionWithSound( 19, true )
                }

                return

            case TATOMA:
                if ( state.isMemoState( 1 ) ) {
                    state.setMemoState( 2 )
                    state.setConditionWithSound( 3, true )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                }

                return

        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.characterId ) as L2Npc

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( state.isCreated() ) {
                    state.startQuest()
                    state.setMemoState( 1 )

                    await QuestHelper.giveSingleItem( player, REPORT_OF_PERRIN, 1 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                    if ( !PlayerVariablesManager.get( data.playerId, QuestVariables.secondClassDiamondReward ) ) {
                        PlayerVariablesManager.set( player.getObjectId(), QuestVariables.secondClassDiamondReward, true )

                        let amount: number = _.get( diamondRewardMap, player.getClassId(), defaultDiamondReward )
                        await QuestHelper.rewardSingleItem( player, DIMENSIONAL_DIAMOND, amount )

                        return this.getPath( '30473-04a.htm' )
                    }

                    return this.getPath( '30473-04.htm' )
                }

                return

            case '30473-08.html':
                if ( state.isMemoState( 10 ) && QuestHelper.hasQuestItem( player, GOLDEN_STATUE ) ) {
                    break
                }
                return

            case '30473-09.html':
                if ( state.isMemoState( 10 ) && QuestHelper.hasQuestItem( player, GOLDEN_STATUE ) ) {
                    await QuestHelper.giveAdena( player, 233490, true )
                    await QuestHelper.giveSingleItem( player, MARK_OF_HEALER, 1 )
                    await QuestHelper.addExpAndSp( player, 738283, 50662 )
                    await state.exitQuest( false, true )

                    player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )
                    break
                }

                return

            case '30428-02.html':
                if ( state.isMemoState( 1 ) && QuestHelper.hasQuestItem( player, REPORT_OF_PERRIN ) ) {
                    state.setConditionWithSound( 2, true )

                    if ( npc.getSummonedNpcCount() < 1 ) {
                        QuestHelper.addAttackDesire( QuestHelper.addSummonedSpawnAtLocation( npc, TATOMA, npc, true, 200000 ), player )
                    }
                }

                break

            case '30658-02.html':
                if ( state.isMemoState( 4 ) && !QuestHelper.hasAtLeastOneQuestItem( player, PICTURE_OF_WINDY, WINDYS_PEBBLES, GOLDEN_STATUE ) ) {
                    if ( player.getAdena() >= 100000 ) {
                        await QuestHelper.takeSingleItem( player, ADENA, 100000 )
                        await QuestHelper.giveSingleItem( player, PICTURE_OF_WINDY, 1 )

                        state.setConditionWithSound( 7, true )
                        break
                    }

                    return this.getPath( '30658-05.html' )
                }

                return

            case '30658-03.html':
                if ( state.isMemoState( 4 ) && !QuestHelper.hasAtLeastOneQuestItem( player, PICTURE_OF_WINDY, WINDYS_PEBBLES, GOLDEN_STATUE ) ) {
                    state.setMemoState( 5 )
                    break
                }

                return

            case '30658-07.html':
                break

            case '30660-02.html':
                if ( QuestHelper.hasQuestItem( player, PICTURE_OF_WINDY ) ) {
                    break
                }

                return

            case '30660-03.html':
                if ( QuestHelper.hasQuestItem( player, PICTURE_OF_WINDY ) ) {
                    await QuestHelper.takeSingleItem( player, PICTURE_OF_WINDY, 1 )
                    await QuestHelper.giveSingleItem( player, WINDYS_PEBBLES, 1 )
                    await npc.deleteMe()

                    state.setConditionWithSound( 8, true )
                    break
                }

                return

            case '30665-02.html':
                if ( QuestHelper.hasQuestItems( player, SECRET_LETTER1, SECRET_LETTER2, SECRET_LETTER3, SECRET_LETTER4 ) ) {
                    await QuestHelper.giveSingleItem( player, CRISTINAS_LETTER, 1 )
                    await QuestHelper.takeMultipleItems( player, -1, SECRET_LETTER1, SECRET_LETTER2, SECRET_LETTER3, SECRET_LETTER4 )

                    state.setMemoState( 9 )
                    state.setConditionWithSound( 22, true )

                    break
                }

                return

            case '30674-02.html':
                if ( state.isMemoState( 6 ) ) {
                    await QuestHelper.takeSingleItem( player, ORDER_OF_SORIUS, 1 )
                    state.setConditionWithSound( 11 )

                    QuestHelper.addSummonedSpawnAtLocation( npc, LERO_LIZARDMAN_AGENT, npc, true, 200000 )
                    QuestHelper.addSummonedSpawnAtLocation( npc, LERO_LIZARDMAN_AGENT, npc, true, 200000 )
                    QuestHelper.addSummonedSpawnAtLocation( npc, LERO_LIZARDMAN_LEADER, npc, true, 200000 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_BEFORE_BATTLE )
                    break
                }

                return


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === PRIEST_BANDELLOS ) {
                    if ( !player.isInCategory( CategoryType.WHITE_MAGIC_GROUP ) ) {
                        return this.getPath( '30473-02.html' )
                    }

                    if ( player.getLevel() < minimumLevel ) {
                        return this.getPath( '30473-01.html' )
                    }

                    return this.getPath( '30473-03.htm' )
                }

                break

            case QuestStateValues.STARTED:
                let memoState = state.getMemoState()

                switch ( data.characterNpcId ) {
                    case PRIEST_BANDELLOS:
                        if ( ( memoState >= 1 ) && ( memoState < 10 ) ) {
                            return this.getPath( '30473-05.html' )
                        }

                        if ( memoState === 10 ) {
                            if ( QuestHelper.hasQuestItem( player, GOLDEN_STATUE ) ) {
                                return this.getPath( '30473-07.html' )
                            }

                            await QuestHelper.giveAdena( player, 266980, true )
                            await QuestHelper.giveSingleItem( player, MARK_OF_HEALER, 1 )
                            await QuestHelper.addExpAndSp( player, 1476566, 101324 )
                            await state.exitQuest( false, true )

                            player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )
                            return this.getPath( '30473-06.html' )
                        }

                        break

                    case MASTER_SORIUS:
                        if ( memoState === 5 ) {
                            await QuestHelper.giveSingleItem( player, ORDER_OF_SORIUS, 1 )

                            state.setMemoState( 6 )
                            state.setConditionWithSound( 10, true )
                            return this.getPath( '30327-01.html' )
                        }

                        if ( memoState >= 6 && memoState < 9 ) {
                            return this.getPath( '30327-02.html' )
                        }

                        if ( memoState === 9 ) {
                            if ( QuestHelper.hasQuestItem( player, CRISTINAS_LETTER ) ) {
                                await QuestHelper.takeSingleItem( player, CRISTINAS_LETTER, 1 )

                                state.setMemoState( 10 )
                                state.setConditionWithSound( 23, true )
                                return this.getPath( '30327-03.html' )
                            }

                            break
                        }

                        if ( memoState >= 10 ) {
                            return this.getPath( '30327-04.html' )
                        }

                        break

                    case ALLANA:
                        if ( memoState === 3 ) {
                            state.setMemoState( 4 )
                            state.setConditionWithSound( 5, true )

                            return this.getPath( '30424-01.html' )
                        }

                        if ( memoState === 4 ) {
                            state.setMemoState( 4 )
                            return this.getPath( '30424-02.html' )
                        }

                        break

                    case PERRIN:
                        switch ( memoState ) {
                            case 1:
                                if ( QuestHelper.hasQuestItem( player, REPORT_OF_PERRIN ) ) {
                                    return this.getPath( '30428-01.html' )
                                }

                                break

                            case 2:
                                await QuestHelper.takeSingleItem( player, REPORT_OF_PERRIN, 1 )

                                state.setMemoState( 3 )
                                state.setConditionWithSound( 4, true )
                                return this.getPath( '30428-03.html' )

                            case 3:
                                return this.getPath( '30428-04.html' )
                        }

                        break

                    case FATHER_GUPU:
                        if ( memoState === 4 ) {
                            if ( !QuestHelper.hasAtLeastOneQuestItem( player, PICTURE_OF_WINDY, WINDYS_PEBBLES, GOLDEN_STATUE ) ) {
                                state.setConditionWithSound( 6, true )
                                return this.getPath( '30658-01.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, PICTURE_OF_WINDY ) ) {
                                return this.getPath( '30658-04.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, WINDYS_PEBBLES ) ) {
                                await QuestHelper.giveSingleItem( player, GOLDEN_STATUE, 1 )
                                await QuestHelper.takeSingleItem( player, WINDYS_PEBBLES, 1 )

                                state.setMemoState( 5 )
                                return this.getPath( '30658-06.html' )
                            }

                            break
                        }

                        if ( memoState === 5 ) {
                            state.setConditionWithSound( 9, true )
                            return this.getPath( '30658-07.html' )
                        }

                        break

                    case ORPHAN_GIRL:
                        switch ( _.random( 4 ) ) {
                            case 0:
                                return this.getPath( '30659-01.html' )

                            case 1:
                                return this.getPath( '30659-02.html' )

                            case 2:
                                return this.getPath( '30659-03.html' )

                            case 3:
                                return this.getPath( '30659-04.html' )

                            case 4:
                                return this.getPath( '30659-05.html' )
                        }

                        break

                    case WINDY_SHAORING:
                        if ( QuestHelper.hasQuestItem( player, PICTURE_OF_WINDY ) ) {
                            return this.getPath( '30660-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, WINDYS_PEBBLES ) ) {
                            return this.getPath( '30660-04.html' )
                        }

                        break

                    case MYSTERIOUS_DARK_ELF:
                        if ( memoState !== 8 ) {
                            break
                        }

                        let hasLetterOne: boolean = QuestHelper.hasQuestItem( player, SECRET_LETTER1 )
                        if ( !hasLetterOne ) {
                            break
                        }

                        let hasLetterTwo: boolean = QuestHelper.hasQuestItem( player, SECRET_LETTER2 )
                        if ( !hasLetterTwo ) {
                            await this.tryToSpawnMobs( player, data )

                            state.setConditionWithSound( 14 )
                            return this.getPath( '30661-01.html' )
                        }

                        let hasLetterThree = QuestHelper.hasQuestItem( player, SECRET_LETTER3 )
                        if ( !hasLetterThree ) {
                            await this.tryToSpawnMobs( player, data )

                            state.setConditionWithSound( 16 )
                            return this.getPath( '30661-02.html' )
                        }

                        let hasLetterFour = QuestHelper.hasQuestItem( player, SECRET_LETTER4 )
                        if ( !hasLetterFour ) {
                            await this.tryToSpawnMobs( player, data )

                            state.setConditionWithSound( 18 )
                            return this.getPath( '30661-03.html' )
                        }

                        state.setConditionWithSound( 20, true )
                        return this.getPath( '30661-04.html' )

                    case PIPER_LONGBOW:
                        if ( memoState !== 8 ) {
                            break
                        }

                        if ( QuestHelper.hasQuestItem( player, SECRET_LETTER1 ) && !QuestHelper.hasQuestItem( player, SECRET_LETTER2 ) ) {
                            return this.getPath( '30662-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, SECRET_LETTER2 ) && !QuestHelper.hasQuestItems( player, SECRET_LETTER3, SECRET_LETTER4 ) ) {
                            return this.getPath( '30662-02.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, SECRET_LETTER2, SECRET_LETTER3, SECRET_LETTER4 ) ) {
                            state.setConditionWithSound( 21, true )
                            return this.getPath( '30662-03.html' )
                        }

                        break

                    case SLEIN_SHINING_BLADE:
                        if ( memoState !== 8 ) {
                            break
                        }

                        if ( QuestHelper.hasQuestItem( player, SECRET_LETTER1 ) && !QuestHelper.hasQuestItem( player, SECRET_LETTER2 ) ) {
                            return this.getPath( '30663-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, SECRET_LETTER2 ) && !QuestHelper.hasQuestItems( player, SECRET_LETTER3, SECRET_LETTER4 ) ) {
                            return this.getPath( '30663-02.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, SECRET_LETTER2, SECRET_LETTER3, SECRET_LETTER4 ) ) {
                            state.setConditionWithSound( 21, true )
                            return this.getPath( '30663-03.html' )
                        }

                        break

                    case CAIN_FLYING_KNIFE:
                        if ( memoState !== 8 ) {
                            break
                        }

                        if ( QuestHelper.hasQuestItem( player, SECRET_LETTER1 ) && !QuestHelper.hasQuestItem( player, SECRET_LETTER4 ) ) {
                            return this.getPath( '30664-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, SECRET_LETTER2 ) && !QuestHelper.hasQuestItems( player, SECRET_LETTER3, SECRET_LETTER4 ) ) {
                            return this.getPath( '30664-02.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, SECRET_LETTER2, SECRET_LETTER3, SECRET_LETTER4 ) ) {
                            state.setConditionWithSound( 21, true )
                            return this.getPath( '30664-03.html' )
                        }

                        break

                    case SAINT_KRISTINA:
                        let hasAllItems: boolean = QuestHelper.hasQuestItems( player, SECRET_LETTER1, SECRET_LETTER2, SECRET_LETTER3, SECRET_LETTER4 )
                        if ( hasAllItems ) {
                            return this.getPath( '30665-01.html' )
                        }

                        if ( memoState < 9 ) {
                            if ( !hasAllItems ) {
                                return this.getPath( '30665-03.html' )
                            }

                            break
                        }

                        return this.getPath( '30665-04.html' )

                    case DAURIN_HAMMERCRUSH:
                        if ( memoState === 6 ) {
                            if ( QuestHelper.hasQuestItem( player, ORDER_OF_SORIUS ) ) {
                                return this.getPath( '30674-01.html' )
                            }

                            if ( !QuestHelper.hasAtLeastOneQuestItem( player, SECRET_LETTER1, ORDER_OF_SORIUS ) ) {
                                let npc = L2World.getObjectById( data.characterId ) as L2Npc
                                if ( npc.getSummonedNpcCount() < 4 ) {
                                    QuestHelper.addSummonedSpawnAtLocation( npc, LERO_LIZARDMAN_AGENT, npc, true, 200000 )
                                    QuestHelper.addSummonedSpawnAtLocation( npc, LERO_LIZARDMAN_LEADER, npc, true, 200000 )
                                }

                                return this.getPath( '30674-02a.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, SECRET_LETTER1 ) ) {
                                state.setMemoState( 8 )
                                state.setConditionWithSound( 13, true )

                                return this.getPath( '30674-03.html' )
                            }

                            break
                        }

                        if ( memoState >= 8 ) {
                            return this.getPath( '30674-04.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === PRIEST_BANDELLOS ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async tryToSpawnMobs( player: L2PcInstance, data: NpcTalkEvent ): Promise<void> {
        let npc = L2World.getObjectById( data.characterId ) as L2Npc

        if ( npc.getSummonedNpcCount() < 36 ) {
            QuestHelper.addSummonedSpawnAtLocation( npc, LERO_LIZARDMAN_ASSASSIN, npc, true, 200000 )
            QuestHelper.addSummonedSpawnAtLocation( npc, LERO_LIZARDMAN_ASSASSIN, npc, true, 200000 )
            QuestHelper.addSummonedSpawnAtLocation( npc, LERO_LIZARDMAN_ASSASSIN, npc, true, 200000 )

            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_BEFORE_BATTLE )
        }

        await npc.deleteMe()
    }
}