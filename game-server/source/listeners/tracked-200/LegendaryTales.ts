import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import _ from 'lodash'
import aigle from 'aigle'

const GILMORE = 30754
const LARGE_DRAGON_SKULL = 17249
const minimumLevel = 80
const mobIdMap = {
    EMERALD_HORN: 25718,
    DUST_RIDER: 25719,
    BLEEDING_FLY: 25720,
    BLACK_DAGGER: 25721,
    SHADOW_SUMMONER: 25722,
    SPIKE_SLASHER: 25723,
    MUSCLE_BOMBER: 25724,
}

const variableNames = {
    defeatedMobs: 'df',
}

function hasDefeatedMonster( state: QuestState, npcId: number ): boolean {
    let npcIds = _.defaults( state.getVariable( variableNames.defeatedMobs ), [] )
    return npcIds.includes( npcId )
}

export class LegendaryTales extends ListenerLogic {
    constructor() {
        super( 'Q00254_LegendaryTales', 'listeners/tracked-200/LegendaryTales.ts' )
        this.questId = 254
        this.questItemIds = [
            LARGE_DRAGON_SKULL,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return _.values( mobIdMap )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00254_LegendaryTales'
    }

    getQuestStartIds(): Array<number> {
        return [ GILMORE ]
    }

    getTalkIds(): Array<number> {
        return [ GILMORE ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let playerIds = player.getParty() ? player.getParty().getMembers() : [ data.playerId ]

        let questName = this.getName()
        await aigle.resolve( playerIds ).each( async ( playerId: number ) => {
            let state: QuestState = QuestStateCache.getQuestState( playerId, questName, false )

            if ( !state || !state.isCondition( 1 ) || hasDefeatedMonster( state, data.npcId ) ) {
                return
            }

            let otherPlayer = L2World.getPlayer( playerId )
            await QuestHelper.giveSingleItem( otherPlayer, LARGE_DRAGON_SKULL, 1 )
            if ( QuestHelper.getQuestItemsCount( otherPlayer, LARGE_DRAGON_SKULL ) >= 7 ) {
                state.setConditionWithSound( 2, true )
                return
            }

            otherPlayer.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        } )

        return
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30754-05.html':
                state.startQuest()
                break

            case '30754-02.html':
            case '30754-03.html':
            case '30754-04.htm':
            case '30754-08.html':
            case '30754-15.html':
            case '30754-20.html':
            case '30754-21.html':
                break

            case '25718': // Emerald Horn
                return this.getPath( hasDefeatedMonster( state, mobIdMap.EMERALD_HORN ) ? '30754-22.html' : '30754-16.html' )

            case '25719': // Dust Rider
                return this.getPath( hasDefeatedMonster( state, mobIdMap.DUST_RIDER ) ? '30754-23.html' : '30754-17.html' )

            case '25720': // Bleeding Fly
                return this.getPath( hasDefeatedMonster( state, mobIdMap.BLEEDING_FLY ) ? '30754-24.html' : '30754-18.html' )

            case '25721': // Black Dagger Wing
                return this.getPath( hasDefeatedMonster( state, mobIdMap.BLACK_DAGGER ) ? '30754-25.html' : '30754-19.html' )

            case '25722': // Shadow Summoner
                return this.getPath( hasDefeatedMonster( state, mobIdMap.SHADOW_SUMMONER ) ? '30754-26.html' : '30754-16.html' )

            case '25723': // Spike Slasher
                return this.getPath( hasDefeatedMonster( state, mobIdMap.SPIKE_SLASHER ) ? '30754-27.html' : '30754-17.html' )

            case '25724': // Muscle Bomber
                return this.getPath( hasDefeatedMonster( state, mobIdMap.MUSCLE_BOMBER ) ? '30754-28.html' : '30754-18.html' )

            case '13467': // Vesper Thrower
            case '13466': // Vesper Singer
            case '13465': // Vesper Caster
            case '13464': // Vesper Retributer
            case '13463': // Vesper Avenger
            case '13457': // Vesper Cutter
            case '13458': // Vesper Slasher
            case '13459': // Vesper Buster
            case '13460': // Vesper Sharper
            case '13461': // Vesper Fighter
            case '13462': // Vesper Stormer
                let player = L2World.getPlayer( data.playerId )
                if ( state.isCondition( 2 ) && QuestHelper.getQuestItemsCount( player, LARGE_DRAGON_SKULL ) >= 7 ) {
                    await QuestHelper.rewardSingleItem( player, _.parseInt( data.eventName ), 1 )
                    await state.exitQuest( false, true )

                    return this.getPath( '30754-09.html' )
                }

                break
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() < minimumLevel ? '30754-00.htm' : '30754-01.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( QuestHelper.hasQuestItem( player, LARGE_DRAGON_SKULL ) ? '30754-14.htm' : '30754-06.html' )

                    case 2:
                        return this.getPath( QuestHelper.getQuestItemsCount( player, LARGE_DRAGON_SKULL ) < 7 ? '30754-12.htm' : '30754-07.html' )
                }

                break

            case QuestStateValues.COMPLETED:
                return this.getPath( '30754-29.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}