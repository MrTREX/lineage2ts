import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'
const HELVETICA = 32641

const BRAZIER_OF_PURITY = 18806
const EVIL_SPIRITS = 22658
const GUARDIAN_SPIRITS = 22659

const certificateOfSupport = 14865
const BROKEN_PIECE_OF_MAGIC_FORCE = 14867
const GUARDIAN_SPIRIT_FRAGMENT = 14868

const BROKEN_PIECE_OF_MAGIC_FORCE_NEEDED = 10
const GUARDIAN_SPIRIT_FRAGMENT_NEEDED = 20
const CHANCE_FOR_FRAGMENT = 80
const minimumLevel = 82

export class SuccessFailureOfBusiness extends ListenerLogic {
    constructor() {
        super( 'Q00238_SuccessFailureOfBusiness', 'listeners/tracked-200/SuccessFailureOfBusiness.ts' )
        this.questId = 238
        this.questItemIds = [ BROKEN_PIECE_OF_MAGIC_FORCE, GUARDIAN_SPIRIT_FRAGMENT ]
    }

    getAttackableKillIds(): Array<number> {
        return [ BRAZIER_OF_PURITY, EVIL_SPIRITS, GUARDIAN_SPIRITS ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00238_SuccessFailureOfBusiness'
    }

    getQuestStartIds(): Array<number> {
        return [ HELVETICA ]
    }

    getTalkIds(): Array<number> {
        return [ HELVETICA ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( data.npcId === BRAZIER_OF_PURITY ) {
            await this.tryToProgressQuest( data, BROKEN_PIECE_OF_MAGIC_FORCE, BROKEN_PIECE_OF_MAGIC_FORCE_NEEDED, 1, 2 )
            return
        }

        if ( _.random( 100 ) >= QuestHelper.getAdjustedChance( GUARDIAN_SPIRIT_FRAGMENT, CHANCE_FOR_FRAGMENT, data.isChampion ) ) {
            return
        }

        await this.tryToProgressQuest( data, GUARDIAN_SPIRIT_FRAGMENT, GUARDIAN_SPIRIT_FRAGMENT_NEEDED, 3, 4 )
        return
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '32461-02.htm':
                break

            case '32461-03.html':
                state.startQuest()
                break

            case '32461-06.html':
                if ( state.isCondition( 2 ) ) {
                    state.setConditionWithSound( 3, true )
                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( !player.hasQuestCompleted( 'Q00237_WindsOfChange' ) || player.getLevel() < minimumLevel ) {
                    return this.getPath( '32461-00.html' )
                }

                if ( !QuestHelper.hasQuestItem( player, certificateOfSupport ) ) {
                    return this.getPath( '32461-10.html' )
                }

                return this.getPath( '32461-01.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '32461-04.html' )

                    case 2:
                        if ( QuestHelper.getQuestItemsCount( player, BROKEN_PIECE_OF_MAGIC_FORCE ) >= BROKEN_PIECE_OF_MAGIC_FORCE_NEEDED ) {
                            await QuestHelper.takeSingleItem( player, BROKEN_PIECE_OF_MAGIC_FORCE, -1 )
                            return this.getPath( '32461-05.html' )
                        }

                        break

                    case 3:
                        return this.getPath( '32461-07.html' )

                    case 4:
                        if ( QuestHelper.getQuestItemsCount( player, GUARDIAN_SPIRIT_FRAGMENT ) >= GUARDIAN_SPIRIT_FRAGMENT_NEEDED ) {
                            await QuestHelper.giveAdena( player, 283346, true )
                            await QuestHelper.takeSingleItem( player, certificateOfSupport, 1 )
                            await QuestHelper.addExpAndSp( player, 1319736, 103553 )
                            await state.exitQuest( false, true )

                            return this.getPath( '32461-08.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                return this.getPath( '32461-09.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async tryToProgressQuest( data: AttackableKillEvent, itemId: number, maximumCount: number, previousCondition: number, nextCondition: number ): Promise<void> {
        let player: L2PcInstance = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), previousCondition )
        if ( !player ) {
            return
        }

        if ( QuestHelper.getQuestItemsCount( player, itemId ) < maximumCount ) {
            await QuestHelper.rewardSingleQuestItem( player, itemId, 1, data.isChampion )
        }

        if ( QuestHelper.getQuestItemsCount( player, itemId ) >= maximumCount ) {
            let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), this.getName(), false )
            state.setConditionWithSound( nextCondition, true )

            return
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }
}