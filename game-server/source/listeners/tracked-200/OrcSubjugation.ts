import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'

import _ from 'lodash'

const KAYLEEN = 30346
const ORC_AMULET = 1116
const ORC_NECKLACE = 1117
const minimumLevel = 8
const monsterRewardMap = {
    20385: ORC_AMULET, // Balor Orc Archer
    20386: ORC_NECKLACE, // Balor Orc Fighter
    20387: ORC_NECKLACE, // Balor Orc Fighter Leader
    20388: ORC_NECKLACE, // Balor Orc Lieutenant
}

export class OrcSubjugation extends ListenerLogic {
    constructor() {
        super( 'Q00263_OrcSubjugation', 'listeners/tracked-200/OrcSubjugation.ts' )
        this.questId = 263
        this.questItemIds = [ ORC_AMULET, ORC_NECKLACE ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardMap ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00263_OrcSubjugation'
    }

    getQuestStartIds(): Array<number> {
        return [ KAYLEEN ]
    }

    getTalkIds(): Array<number> {
        return [ KAYLEEN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || _.random( 10 ) <= QuestHelper.getAdjustedChance( monsterRewardMap[ data.npcId ], 4, data.isChampion ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        await QuestHelper.rewardSingleQuestItem( player, monsterRewardMap[ data.npcId ], 1, data.isChampion )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30346-04.htm':
                state.startQuest()
                break

            case '30346-07.html':
                await state.exitQuest( true, true )
                break

            case '30346-08.html':
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getRace() !== Race.DARK_ELF ) {
                    return this.getPath( '30346-01.htm' )
                }

                return this.getPath( player.getLevel() >= minimumLevel ? '30346-03.htm' : '30346-02.htm' )

            case QuestStateValues.STARTED:
                let amulets: number = QuestHelper.getQuestItemsCount( player, ORC_AMULET )
                let necklaces: number = QuestHelper.getQuestItemsCount( player, ORC_NECKLACE )
                if ( ( amulets + necklaces ) === 0 ) {
                    return this.getPath( '30346-05.html' )
                }

                let amount: number = ( amulets * 20 ) + ( necklaces * 30 ) + ( ( amulets + necklaces ) >= 10 ? 1100 : 0 )

                await QuestHelper.giveAdena( player, amount, true )
                await QuestHelper.takeMultipleItems( player, -1, ...this.questItemIds )

                return this.getPath( '30346-06.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}