import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'
import { QuestVariables } from '../helpers/QuestVariables'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'
import { CategoryType } from '../../gameService/enums/CategoryType'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'

const MAGISTER_ROA = 30114
const WAREHOUSE_KEEPER_NORMAN = 30210
const TETRARCH_THIFIELL = 30358
const ARKENIA = 30419
const MASTER_IXIA = 30463
const MAGISTER_KAIRA = 30476
const ALDERS_SPIRIT = 30613
const BROTHER_METHEUS = 30614
const BLOODY_PIXY = 31845
const BLIGHT_TREANT = 31850

const KAIRAS_LETTER = 3173
const METHEUSS_FUNERAL_JAR = 3174
const KASANDRAS_REMAINS = 3175
const HERBALISM_TEXTBOOK = 3176
const IXIAS_LIST = 3177
const MEDUSAS_ICHOR = 3178
const MARSH_SPIDER_FLUIDS = 3179
const DEAD_SEEKER_DUNG = 3180
const TYRANTS_BLOOD = 3181
const NIGHTSHADE_ROOT = 3182
const BELLADONNA = 3183
const ALDERS_SKULL1 = 3184
const ALDERS_SKULL2 = 3185
const ALDERS_RECEIPT = 3186
const REVELATIONS_MANUSCRIPT = 3187
const KAIRAS_RECOMMENDATION = 3189
const KAIRAS_INSTRUCTIONS = 3188
const PALUS_CHARM = 3190
const THIFIELLS_LETTER = 3191
const ARKENIAS_NOTE = 3192
const PIXY_GARNET = 3193
const GRANDISS_SKULL = 3194
const KARUL_BUGBEAR_SKULL = 3195
const BREKA_OVERLORD_SKULL = 3196
const LETO_OVERLORD_SKULL = 3197
const RED_FAIRY_DUST = 3198
const TIMIRIRAN_SEED = 3199
const BLACK_WILLOW_LEAF = 3200
const BLIGHT_TREANT_SAP = 3201
const ARKENIAS_LETTER = 3202

const MARK_OF_FATE = 3172
const DIMENSIONAL_DIAMOND = 7562

const HANGMAN_TREE = 20144
const MARSH_STAKATO = 20157
const MEDUSA = 20158
const TYRANT = 20192
const TYRANT_KINGPIN = 20193
const DEAD_SEEKER = 20202
const MARSH_STAKATO_WORKER = 20230
const MARSH_STAKATO_SOLDIER = 20232
const MARSH_SPIDER = 20233
const MARSH_STAKATO_DRONE = 20234
const BREKA_ORC_OVERLORD = 20270
const GRANDIS = 20554
const LETO_LIZARDMAN_OVERLORD = 20582
const KARUL_BUGBEAR = 20600

const BLACK_WILLOW_LURKER = 27079
const minimumLevel = 37

export class TestimonyOfFate extends ListenerLogic {
    constructor() {
        super( 'Q00219_TestimonyOfFate', 'listeners/tracked-200/TestimonyOfFate.ts' )
        this.questId = 219
        this.questItemIds = [
            KAIRAS_LETTER,
            METHEUSS_FUNERAL_JAR,
            KASANDRAS_REMAINS,
            HERBALISM_TEXTBOOK,
            IXIAS_LIST,
            MEDUSAS_ICHOR,
            MARSH_SPIDER_FLUIDS,
            DEAD_SEEKER_DUNG,
            TYRANTS_BLOOD,
            NIGHTSHADE_ROOT,
            BELLADONNA,
            ALDERS_SKULL1,
            ALDERS_SKULL2,
            ALDERS_RECEIPT,
            REVELATIONS_MANUSCRIPT,
            KAIRAS_RECOMMENDATION,
            KAIRAS_INSTRUCTIONS,
            PALUS_CHARM,
            THIFIELLS_LETTER,
            ARKENIAS_NOTE,
            PIXY_GARNET,
            GRANDISS_SKULL,
            KARUL_BUGBEAR_SKULL,
            BREKA_OVERLORD_SKULL,
            LETO_OVERLORD_SKULL,
            RED_FAIRY_DUST,
            TIMIRIRAN_SEED,
            BLACK_WILLOW_LEAF,
            BLIGHT_TREANT_SAP,
            ARKENIAS_LETTER,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            HANGMAN_TREE,
            MARSH_STAKATO,
            MEDUSA,
            TYRANT,
            TYRANT_KINGPIN,
            DEAD_SEEKER,
            MARSH_STAKATO_WORKER,
            MARSH_STAKATO_SOLDIER,
            MARSH_SPIDER,
            MARSH_STAKATO_DRONE,
            BREKA_ORC_OVERLORD,
            GRANDIS,
            LETO_LIZARDMAN_OVERLORD,
            KARUL_BUGBEAR,
            BLACK_WILLOW_LURKER,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ MAGISTER_KAIRA ]
    }

    getTalkIds(): Array<number> {
        return [
            MAGISTER_KAIRA,
            MAGISTER_ROA,
            WAREHOUSE_KEEPER_NORMAN,
            TETRARCH_THIFIELL,
            ARKENIA,
            MASTER_IXIA,
            ALDERS_SPIRIT,
            BROTHER_METHEUS,
            BLOODY_PIXY,
            BLIGHT_TREANT,
        ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId )

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case HANGMAN_TREE:
                if ( QuestHelper.hasQuestItem( player, METHEUSS_FUNERAL_JAR ) && !QuestHelper.hasQuestItem( player, KASANDRAS_REMAINS ) ) {
                    await QuestHelper.takeSingleItem( player, METHEUSS_FUNERAL_JAR, 1 )
                    await QuestHelper.giveSingleItem( player, KASANDRAS_REMAINS, 1 )

                    state.setConditionWithSound( 3, true )
                }

                return

            case MARSH_STAKATO:
            case MARSH_STAKATO_WORKER:
            case MARSH_STAKATO_SOLDIER:
            case MARSH_STAKATO_DRONE:
                if ( QuestHelper.hasQuestItem( player, IXIAS_LIST ) && QuestHelper.getQuestItemsCount( player, NIGHTSHADE_ROOT ) < 10 ) {
                    await QuestHelper.rewardSingleItem( player, NIGHTSHADE_ROOT, 1 )

                    this.tryToProgressQuest( player, state )
                }

                return

            case MEDUSA:
                if ( QuestHelper.hasQuestItem( player, IXIAS_LIST ) && QuestHelper.getQuestItemsCount( player, MEDUSAS_ICHOR ) < 10 ) {
                    await QuestHelper.rewardSingleItem( player, MEDUSAS_ICHOR, 1 )
                    this.tryToProgressQuest( player, state )
                }

                return

            case TYRANT:
            case TYRANT_KINGPIN:
                if ( QuestHelper.hasQuestItem( player, IXIAS_LIST ) && QuestHelper.getQuestItemsCount( player, TYRANTS_BLOOD ) < 10 ) {
                    await QuestHelper.rewardSingleItem( player, TYRANTS_BLOOD, 1 )
                    this.tryToProgressQuest( player, state )
                }

                return

            case DEAD_SEEKER:
                if ( QuestHelper.hasQuestItem( player, IXIAS_LIST ) && QuestHelper.getQuestItemsCount( player, DEAD_SEEKER_DUNG ) < 10 ) {
                    await QuestHelper.rewardSingleItem( player, DEAD_SEEKER_DUNG, 1 )
                    this.tryToProgressQuest( player, state )
                }

                return

            case MARSH_SPIDER:
                if ( QuestHelper.hasQuestItem( player, IXIAS_LIST ) && QuestHelper.getQuestItemsCount( player, MARSH_SPIDER_FLUIDS ) < 10 ) {
                    await QuestHelper.rewardSingleItem( player, MARSH_SPIDER_FLUIDS, 1 )
                    this.tryToProgressQuest( player, state )
                }

                return

            case BREKA_ORC_OVERLORD:
                if ( QuestHelper.hasQuestItems( player, PALUS_CHARM, ARKENIAS_NOTE, PIXY_GARNET )
                        && !QuestHelper.hasQuestItems( player, RED_FAIRY_DUST, BREKA_OVERLORD_SKULL ) ) {

                    await QuestHelper.giveSingleItem( player, BREKA_OVERLORD_SKULL, 1 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                }

                return

            case GRANDIS:
                if ( QuestHelper.hasQuestItems( player, PALUS_CHARM, ARKENIAS_NOTE, PIXY_GARNET )
                        && !QuestHelper.hasQuestItems( player, RED_FAIRY_DUST, GRANDISS_SKULL ) ) {

                    await QuestHelper.giveSingleItem( player, GRANDISS_SKULL, 1 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                }

                return

            case LETO_LIZARDMAN_OVERLORD:
                if ( QuestHelper.hasQuestItems( player, PALUS_CHARM, ARKENIAS_NOTE, PIXY_GARNET )
                        && !QuestHelper.hasQuestItems( player, RED_FAIRY_DUST, LETO_OVERLORD_SKULL ) ) {
                    await QuestHelper.giveSingleItem( player, LETO_OVERLORD_SKULL, 1 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                }

                return

            case KARUL_BUGBEAR:
                if ( QuestHelper.hasQuestItems( player, PALUS_CHARM, ARKENIAS_NOTE, PIXY_GARNET )
                        && !QuestHelper.hasQuestItems( player, RED_FAIRY_DUST, KARUL_BUGBEAR_SKULL ) ) {
                    await QuestHelper.giveSingleItem( player, KARUL_BUGBEAR_SKULL, 1 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                }

                return

            case BLACK_WILLOW_LURKER:
                if ( QuestHelper.hasQuestItems( player, PALUS_CHARM, ARKENIAS_NOTE, TIMIRIRAN_SEED )
                        && !QuestHelper.hasQuestItems( player, BLIGHT_TREANT_SAP, BLACK_WILLOW_LEAF ) ) {
                    await QuestHelper.giveSingleItem( player, BLACK_WILLOW_LEAF, 1 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                }

                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( state.isCreated() ) {
                    state.startQuest()
                    await QuestHelper.giveSingleItem( player, KAIRAS_LETTER, 1 )


                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                    if ( !PlayerVariablesManager.get( data.playerId, QuestVariables.secondClassDiamondReward ) ) {
                        PlayerVariablesManager.set( player.getObjectId(), QuestVariables.secondClassDiamondReward, true )

                        await QuestHelper.rewardSingleItem( player, DIMENSIONAL_DIAMOND, 98 )

                        return this.getPath( '30476-05a.htm' )
                    }

                    return this.getPath( '30476-05.htm' )
                }

                return

            case '30476-04.htm':
            case '30476-13.html':
            case '30476-14.html':
            case '30114-02.html':
            case '30114-03.html':
            case '30463-02a.html':
                break

            case '30476-12.html':
                if ( QuestHelper.hasQuestItem( player, REVELATIONS_MANUSCRIPT ) ) {
                    await QuestHelper.takeSingleItem( player, REVELATIONS_MANUSCRIPT, 1 )
                    await QuestHelper.giveSingleItem( player, KAIRAS_RECOMMENDATION, 1 )

                    state.setConditionWithSound( 15, true )
                    break
                }

                return

            case '30114-04.html':
                if ( QuestHelper.hasQuestItem( player, ALDERS_SKULL2 ) ) {
                    await QuestHelper.takeSingleItem( player, ALDERS_SKULL2, 1 )
                    await QuestHelper.giveSingleItem( player, ALDERS_RECEIPT, 1 )

                    state.setConditionWithSound( 12, true )
                    break
                }

                return

            case '30419-02.html':
                if ( QuestHelper.hasQuestItem( player, THIFIELLS_LETTER ) ) {
                    await QuestHelper.takeSingleItem( player, THIFIELLS_LETTER, 1 )
                    await QuestHelper.giveSingleItem( player, ARKENIAS_NOTE, 1 )

                    state.setConditionWithSound( 17, true )
                    break
                }

                return

            case '30419-05.html':
                if ( QuestHelper.hasQuestItems( player, ARKENIAS_NOTE, RED_FAIRY_DUST, BLIGHT_TREANT_SAP ) ) {
                    await QuestHelper.takeMultipleItems( player, 1, ARKENIAS_NOTE, RED_FAIRY_DUST, BLIGHT_TREANT_SAP )
                    await QuestHelper.giveSingleItem( player, ARKENIAS_LETTER, 1 )

                    state.setConditionWithSound( 18, true )
                    break
                }

                return

            case '31845-02.html':
                await QuestHelper.giveSingleItem( player, PIXY_GARNET, 1 )
                break

            case '31850-02.html':
                await QuestHelper.giveSingleItem( player, TIMIRIRAN_SEED, 1 )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === MAGISTER_KAIRA ) {
                    if ( player.getRace() !== Race.DARK_ELF ) {
                        return this.getPath( '30476-01.html' )
                    }

                    if ( player.getLevel() < minimumLevel ) {
                        return this.getPath( '30476-02.html' )
                    }

                    if ( player.isInCategory( CategoryType.DELF_2ND_GROUP ) ) {
                        return this.getPath( '30476-03.htm' )
                    }

                    return this.getPath( '30476-01a.html' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case MAGISTER_KAIRA:
                        if ( QuestHelper.hasQuestItem( player, KAIRAS_LETTER ) ) {
                            return this.getPath( '30476-06.html' )
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, METHEUSS_FUNERAL_JAR, KASANDRAS_REMAINS ) ) {
                            return this.getPath( '30476-07.html' )
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, HERBALISM_TEXTBOOK, IXIAS_LIST ) ) {
                            state.setConditionWithSound( 5, true )
                            return this.getPath( '30476-08.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, ALDERS_SKULL1 ) ) {
                            await QuestHelper.takeSingleItem( player, ALDERS_SKULL1, 1 )
                            await QuestHelper.giveSingleItem( player, ALDERS_SKULL2, 1 )
                            QuestHelper.addGenericSpawn( null, ALDERS_SPIRIT, 78977, 149036, -3597, 0, false, 200000, false )

                            state.setConditionWithSound( 10, true )
                            return this.getPath( '30476-09.html' )
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, ALDERS_SKULL2, ALDERS_RECEIPT ) ) {
                            state.setConditionWithSound( 11, true )
                            return this.getPath( '30476-10.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, REVELATIONS_MANUSCRIPT ) ) {
                            return this.getPath( '30476-11.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, KAIRAS_INSTRUCTIONS ) ) {
                            await QuestHelper.giveSingleItem( player, KAIRAS_RECOMMENDATION, 1 )
                            await QuestHelper.takeSingleItem( player, KAIRAS_INSTRUCTIONS, 1 )

                            state.setConditionWithSound( 15, true )
                            return this.getPath( '30476-15.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, KAIRAS_RECOMMENDATION ) ) {
                            return this.getPath( '30476-16.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, PALUS_CHARM ) ) {
                            return this.getPath( '30476-17.html' )
                        }

                        break

                    case BROTHER_METHEUS:
                        if ( QuestHelper.hasQuestItem( player, KAIRAS_LETTER ) ) {
                            await QuestHelper.takeSingleItem( player, KAIRAS_LETTER, 1 )
                            await QuestHelper.giveSingleItem( player, METHEUSS_FUNERAL_JAR, 1 )

                            state.setConditionWithSound( 2, true )
                            return this.getPath( '30614-01.html' )
                        }

                        let hasRemains = QuestHelper.hasQuestItem( player, KASANDRAS_REMAINS )
                        if ( QuestHelper.hasQuestItem( player, METHEUSS_FUNERAL_JAR ) && !hasRemains ) {
                            return this.getPath( '30614-02.html' )
                        }

                        if ( hasRemains && !QuestHelper.hasQuestItem( player, METHEUSS_FUNERAL_JAR ) ) {
                            await QuestHelper.takeSingleItem( player, KASANDRAS_REMAINS, 1 )
                            await QuestHelper.giveSingleItem( player, HERBALISM_TEXTBOOK, 1 )

                            state.setConditionWithSound( 4, true )
                            return this.getPath( '30614-03.html' )
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, HERBALISM_TEXTBOOK, IXIAS_LIST ) ) {
                            state.setConditionWithSound( 5, true )
                            return this.getPath( '30614-04.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, BELLADONNA ) ) {
                            await QuestHelper.takeSingleItem( player, BELLADONNA, 1 )
                            await QuestHelper.giveSingleItem( player, ALDERS_SKULL1, 1 )

                            state.setConditionWithSound( 9, true )
                            return this.getPath( '30614-05.html' )
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, ALDERS_SKULL1, ALDERS_SKULL2, ALDERS_RECEIPT, REVELATIONS_MANUSCRIPT, KAIRAS_INSTRUCTIONS, KAIRAS_RECOMMENDATION ) ) {
                            return this.getPath( '30614-06.html' )
                        }

                        break

                    case MASTER_IXIA:
                        if ( QuestHelper.hasQuestItem( player, HERBALISM_TEXTBOOK ) ) {
                            await QuestHelper.takeSingleItem( player, HERBALISM_TEXTBOOK, 1 )
                            await QuestHelper.giveSingleItem( player, IXIAS_LIST, 1 )

                            state.setConditionWithSound( 6, true )
                            return this.getPath( '30463-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, IXIAS_LIST ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, MEDUSAS_ICHOR ) >= 10
                                    && QuestHelper.getQuestItemsCount( player, MARSH_SPIDER_FLUIDS ) >= 10
                                    && QuestHelper.getQuestItemsCount( player, DEAD_SEEKER_DUNG ) >= 10
                                    && QuestHelper.getQuestItemsCount( player, TYRANTS_BLOOD ) >= 10
                                    && QuestHelper.getQuestItemsCount( player, NIGHTSHADE_ROOT ) >= 10 ) {

                                await QuestHelper.takeMultipleItems( player, -1, IXIAS_LIST, MEDUSAS_ICHOR, MARSH_SPIDER_FLUIDS, DEAD_SEEKER_DUNG, TYRANTS_BLOOD, NIGHTSHADE_ROOT )
                                await QuestHelper.giveSingleItem( player, BELLADONNA, 1 )

                                state.setConditionWithSound( 8, true )
                                return this.getPath( '30463-03.html' )
                            }

                            return this.getPath( '30463-02.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, BELLADONNA ) ) {
                            return this.getPath( '30463-04.html' )
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, ALDERS_SKULL1, ALDERS_SKULL2, ALDERS_RECEIPT, REVELATIONS_MANUSCRIPT, KAIRAS_INSTRUCTIONS, KAIRAS_RECOMMENDATION ) ) {
                            return this.getPath( '30463-05.html' )
                        }

                        break

                    case MAGISTER_ROA:
                        if ( QuestHelper.hasQuestItem( player, ALDERS_SKULL2 ) ) {
                            return this.getPath( '30114-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, ALDERS_RECEIPT ) ) {
                            return this.getPath( '30114-05.html' )
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, REVELATIONS_MANUSCRIPT, KAIRAS_INSTRUCTIONS, KAIRAS_RECOMMENDATION ) ) {
                            return this.getPath( '30114-06.html' )
                        }

                        break

                    case WAREHOUSE_KEEPER_NORMAN:
                        if ( QuestHelper.hasQuestItem( player, ALDERS_RECEIPT ) ) {
                            await QuestHelper.takeSingleItem( player, ALDERS_RECEIPT, 1 )
                            await QuestHelper.giveSingleItem( player, REVELATIONS_MANUSCRIPT, 1 )
                            state.setConditionWithSound( 13, true )
                            return this.getPath( '30210-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, REVELATIONS_MANUSCRIPT ) ) {
                            return this.getPath( '30210-02.html' )
                        }

                        break

                    case TETRARCH_THIFIELL:
                        if ( QuestHelper.hasQuestItem( player, KAIRAS_RECOMMENDATION ) ) {
                            await QuestHelper.takeSingleItem( player, KAIRAS_RECOMMENDATION, 1 )
                            await QuestHelper.giveMultipleItems( player, 1, PALUS_CHARM, THIFIELLS_LETTER )

                            state.setConditionWithSound( 16, true )
                            return this.getPath( '30358-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, PALUS_CHARM ) ) {
                            if ( QuestHelper.hasQuestItem( player, THIFIELLS_LETTER ) ) {
                                return this.getPath( '30358-02.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, ARKENIAS_NOTE ) ) {
                                return this.getPath( '30358-03.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, ARKENIAS_LETTER ) ) {
                                await QuestHelper.giveAdena( player, 247708, true )
                                await QuestHelper.giveSingleItem( player, MARK_OF_FATE, 1 )
                                await QuestHelper.addExpAndSp( player, 1365470, 91124 )
                                await state.exitQuest( false, true )

                                player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )
                                return this.getPath( '30358-04.html' )
                            }
                        }

                        break

                    case ARKENIA:
                        if ( !QuestHelper.hasQuestItem( player, PALUS_CHARM ) ) {
                            break
                        }

                        if ( QuestHelper.hasQuestItem( player, THIFIELLS_LETTER ) ) {
                            return this.getPath( '30419-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, ARKENIAS_NOTE )
                                && !QuestHelper.hasQuestItems( player, RED_FAIRY_DUST, BLIGHT_TREANT_SAP ) ) {
                            return this.getPath( '30419-03.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, ARKENIAS_NOTE, RED_FAIRY_DUST, BLIGHT_TREANT_SAP ) ) {
                            return this.getPath( '30419-04.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, ARKENIAS_LETTER ) ) {
                            return this.getPath( '30419-06.html' )
                        }

                        break

                    case ALDERS_SPIRIT:
                        if ( QuestHelper.hasAtLeastOneQuestItem( player, ALDERS_SKULL1, ALDERS_SKULL2 ) ) {
                            return this.getPath( '30613-01.html' )
                        }

                        break

                    case BLOODY_PIXY:
                        if ( !QuestHelper.hasQuestItems( player, PALUS_CHARM, ARKENIAS_NOTE ) ) {
                            break
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, RED_FAIRY_DUST, PIXY_GARNET ) ) {
                            return this.getPath( '31845-01.html' )
                        }

                        if ( !QuestHelper.hasQuestItem( player, RED_FAIRY_DUST )
                                && QuestHelper.hasQuestItem( player, PIXY_GARNET )
                                && !QuestHelper.hasAtLeastOneQuestItem( player, GRANDISS_SKULL, KARUL_BUGBEAR_SKULL, BREKA_OVERLORD_SKULL, LETO_OVERLORD_SKULL ) ) {
                            return this.getPath( '31845-03.html' )
                        }

                        if ( !QuestHelper.hasQuestItem( player, RED_FAIRY_DUST )
                                && QuestHelper.hasQuestItems( player, PIXY_GARNET, GRANDISS_SKULL, KARUL_BUGBEAR_SKULL, BREKA_OVERLORD_SKULL, LETO_OVERLORD_SKULL ) ) {
                            await QuestHelper.takeMultipleItems( player, 1, PIXY_GARNET, GRANDISS_SKULL, KARUL_BUGBEAR_SKULL, BREKA_OVERLORD_SKULL, LETO_OVERLORD_SKULL )
                            await QuestHelper.giveSingleItem( player, RED_FAIRY_DUST, 1 )

                            return this.getPath( '31845-04.html' )
                        }

                        if ( !QuestHelper.hasQuestItem( player, PIXY_GARNET )
                                && QuestHelper.hasQuestItems( player, PALUS_CHARM, ARKENIAS_NOTE, RED_FAIRY_DUST ) ) {
                            return this.getPath( '31845-05.html' )
                        }

                        break

                    case BLIGHT_TREANT:
                        if ( !QuestHelper.hasQuestItems( player, PALUS_CHARM, ARKENIAS_NOTE ) ) {
                            break
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, BLIGHT_TREANT_SAP, TIMIRIRAN_SEED ) ) {
                            return this.getPath( '31850-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, TIMIRIRAN_SEED )
                                && !QuestHelper.hasAtLeastOneQuestItem( player, BLIGHT_TREANT_SAP, BLACK_WILLOW_LEAF ) ) {
                            return this.getPath( '31850-03.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, TIMIRIRAN_SEED, BLACK_WILLOW_LEAF )
                                && !QuestHelper.hasQuestItem( player, BLIGHT_TREANT_SAP ) ) {
                            await QuestHelper.takeMultipleItems( player, 1, TIMIRIRAN_SEED, BLACK_WILLOW_LEAF )
                            await QuestHelper.giveSingleItem( player, BLIGHT_TREANT_SAP, 1 )

                            return this.getPath( '31850-04.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, BLIGHT_TREANT_SAP ) && !QuestHelper.hasQuestItem( player, TIMIRIRAN_SEED ) ) {
                            return this.getPath( '31850-05.html' )
                        }

                        break

                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === MAGISTER_KAIRA ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    tryToProgressQuest( player: L2PcInstance, state: QuestState ): void {
        if ( QuestHelper.getQuestItemsCount( player, NIGHTSHADE_ROOT ) >= 10
                && QuestHelper.getQuestItemsCount( player, MEDUSAS_ICHOR ) >= 10
                && QuestHelper.getQuestItemsCount( player, MARSH_SPIDER_FLUIDS ) >= 10
                && QuestHelper.getQuestItemsCount( player, DEAD_SEEKER_DUNG ) >= 10
                && QuestHelper.getQuestItemsCount( player, TYRANTS_BLOOD ) >= 10 ) {
            state.setConditionWithSound( 7 )

            return player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00219_TestimonyOfFate'
    }
}