import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const VOLLODOS = 30137
const SPORE_SAC = 707
const minimumLevel = 8
const REQUIRED_ITEM_COUNT = 10

const mobChanceMap = {
    20007: 3, // Green Fungus
    20400: 4, // Blood Fungus
}

export class TradeWithTheIvoryTower extends ListenerLogic {
    constructor() {
        super( 'Q00262_TradeWithTheIvoryTower', 'listeners/tracked-200/TradeWithTheIvoryTower.ts' )
        this.questId = 262
        this.questItemIds = [ SPORE_SAC ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( mobChanceMap ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00262_TradeWithTheIvoryTower'
    }

    getQuestStartIds(): Array<number> {
        return [ VOLLODOS ]
    }

    getTalkIds(): Array<number> {
        return [ VOLLODOS ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player: L2PcInstance = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 1 )
        if ( !player ) {
            return
        }

        if ( _.random( 10 ) < QuestHelper.getAdjustedChance( SPORE_SAC, mobChanceMap[ data.npcId ], data.isChampion ) ) {
            await QuestHelper.rewardSingleQuestItem( player, SPORE_SAC, 1, data.isChampion )

            if ( QuestHelper.getQuestItemsCount( player, SPORE_SAC ) >= REQUIRED_ITEM_COUNT ) {
                let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), this.getName(), false )
                state.setConditionWithSound( 2, true )

                return
            }

            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || data.eventName !== '30137-03.htm' ) {
            return
        }

        state.startQuest()
        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30137-02.htm' : '30137-01.htm' )

            case QuestStateValues.STARTED: {
                switch ( state.getCondition() ) {
                    case 1:
                        if ( QuestHelper.getQuestItemsCount( player, SPORE_SAC ) < REQUIRED_ITEM_COUNT ) {
                            return this.getPath( '30137-04.html' )
                        }

                        break

                    case 2:
                        if ( QuestHelper.getQuestItemsCount( player, SPORE_SAC ) >= REQUIRED_ITEM_COUNT ) {
                            await QuestHelper.giveAdena( player, 3000, true )
                            await state.exitQuest( true, true )

                            return this.getPath( '30137-05.html' )
                        }

                        return this.getPath( '30137-04.html' )
                }

                break
            }
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}