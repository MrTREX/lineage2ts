import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { NewbieRewardsHelper } from '../helpers/NewbieRewardsHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'

const PERWAN = 32133
const CRIMSON_SPIDER_CLAW = 9747
const CRIMSON_SPIDER = 22244
const CLAW_PRICE = 45
const BONUS = 2187
const minimumLevel = 15

export class TheFewTheProudTheBrave extends ListenerLogic {
    constructor() {
        super( 'Q00283_TheFewTheProudTheBrave', 'listeners/tracked-200/TheFewTheProudTheBrave.ts' )
        this.questId = 283
        this.questItemIds = [ CRIMSON_SPIDER_CLAW ]
    }

    getAttackableKillIds(): Array<number> {
        return [ CRIMSON_SPIDER ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00283_TheFewTheProudTheBrave'
    }

    getQuestStartIds(): Array<number> {
        return [ PERWAN ]
    }

    getTalkIds(): Array<number> {
        return [ PERWAN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), -1, 3, L2World.getObjectById( data.targetId ) as L2Npc )
        if ( !state ) {
            return
        }

        if ( Math.random() < QuestHelper.getAdjustedChance( CRIMSON_SPIDER_CLAW, 0.6, data.isChampion ) ) {
            let player = state.getPlayer()
            await QuestHelper.rewardSingleQuestItem( player, CRIMSON_SPIDER_CLAW, 1, data.isChampion )
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32133-03.htm':
                state.startQuest()
                break

            case '32133-06.html':
                break

            case '32133-08.html':
                let claws = QuestHelper.getQuestItemsCount( player, CRIMSON_SPIDER_CLAW )
                if ( claws === 0 ) {
                    return this.getPath( '32133-07.html' )
                }

                let amount = ( claws * CLAW_PRICE ) + ( ( claws >= 10 ) ? BONUS : 0 )

                await QuestHelper.giveAdena( player, amount, true )
                await QuestHelper.takeSingleItem( player, CRIMSON_SPIDER_CLAW, -1 )
                await NewbieRewardsHelper.giveNewbieReward( player )

                break

            case '32133-09.html':
                await state.exitQuest( true, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '32133-01.htm' : '32133-02.htm' )

            case QuestStateValues.STARTED:
                return this.getPath( QuestHelper.hasQuestItem( player, CRIMSON_SPIDER_CLAW ) ? '32133-04.html' : '32133-05.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}