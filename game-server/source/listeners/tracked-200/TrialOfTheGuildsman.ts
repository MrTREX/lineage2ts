import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { QuestHelper } from '../helpers/QuestHelper'
import { ItemTypes } from '../../gameService/values/InventoryValues'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'
import { QuestVariables } from '../helpers/QuestVariables'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { L2Object } from '../../gameService/models/L2Object'
import _ from 'lodash'

const WAREHOUSE_KEEPER_VALKON = 30103
const WAREHOUSE_KEEPER_NORMAN = 30210
const BLACKSMITH_ALTRAN = 30283
const BLACKSMITH_PINTER = 30298
const BLACKSMITH_DUNING = 30688

const RECIPE_JOURNEYMAN_RING = 3024
const RECIPE_AMBER_BEAD = 3025
const VALKONS_RECOMMENDATION = 3120
const MANDRAGORA_BERRY = 3121
const ALLTRANS_INSTRUCTIONS = 3122
const ALLTRANS_1ST_RECOMMENDATION = 3123
const ALLTRANS_2ND_RECOMMENDATION = 3124
const NORMANS_INSTRUCTIONS = 3125
const NORMANS_RECEIPT = 3126
const DUNINGS_INSTRUCTIONS = 3127
const DUNINGS_KEY = 3128
const NORMANS_LIST = 3129
const GRAY_BONE_POWDER = 3130
const GRANITE_WHETSTONE = 3131
const RED_PIGMENT = 3132
const BRAIDED_YARN = 3133
const JOURNEYMAN_GEM = 3134
const PINTERS_INSTRUCTIONS = 3135
const AMBER_BEAD = 3136
const AMBER_LUMP = 3137
const JOURNEYMAN_DECO_BEADS = 3138
const JOURNEYMAN_RING = 3139

const MARK_OF_GUILDSMAN = 3119
const DIMENSIONAL_DIAMOND = 7562

const ANT = 20079
const ANT_CAPTAIN = 20080
const ANT_OVERSEER = 20081
const GRANITE_GOLEM = 20083
const MANDRAGORA_SPROUT1 = 20154
const MANDRAGORA_SAPLONG = 20155
const MANDRAGORA_BLOSSOM = 20156
const SILENOS = 20168
const STRAIN = 20200
const GHOUL = 20201
const DEAD_SEEKER = 20202
const MANDRAGORA_SPROUT2 = 20223
const BREKA_ORC = 20267
const BREKA_ORC_ARCHER = 20268
const BREKA_ORC_SHAMAN = 20269
const BREKA_ORC_OVERLORD = 20270
const BREKA_ORC_WARRIOR = 20271

const minimumLevel = 35

export class TrialOfTheGuildsman extends ListenerLogic {
    constructor() {
        super( 'Q00216_TrialOfTheGuildsman', 'listeners/tracked-200/TrialOfTheGuildsman.ts' )
        this.questId = 216
        this.questItemIds = [
            RECIPE_JOURNEYMAN_RING,
            RECIPE_AMBER_BEAD,
            VALKONS_RECOMMENDATION,
            MANDRAGORA_BERRY,
            ALLTRANS_INSTRUCTIONS,
            ALLTRANS_1ST_RECOMMENDATION,
            ALLTRANS_2ND_RECOMMENDATION,
            NORMANS_INSTRUCTIONS,
            NORMANS_RECEIPT,
            DUNINGS_INSTRUCTIONS,
            DUNINGS_KEY,
            NORMANS_LIST,
            GRAY_BONE_POWDER,
            GRANITE_WHETSTONE,
            RED_PIGMENT,
            BRAIDED_YARN,
            JOURNEYMAN_GEM,
            PINTERS_INSTRUCTIONS,
            AMBER_BEAD,
            AMBER_LUMP,
            JOURNEYMAN_DECO_BEADS,
            JOURNEYMAN_RING,
        ]
    }

    checkPartyMember( state: QuestState, target: L2Object ): boolean {
        let player = state.getPlayer()

        switch ( target.getId() ) {
            case ANT:
            case ANT_CAPTAIN:
            case ANT_OVERSEER:
                return QuestHelper.hasQuestItems( player, ALLTRANS_INSTRUCTIONS, PINTERS_INSTRUCTIONS )
                        && QuestHelper.getQuestItemsCount( player, AMBER_BEAD ) < 70

            case GRANITE_GOLEM:
                return QuestHelper.hasQuestItems( player, ALLTRANS_INSTRUCTIONS, NORMANS_LIST )
                        && QuestHelper.getQuestItemsCount( player, GRANITE_WHETSTONE ) < 70

            case SILENOS:
                return QuestHelper.hasQuestItems( player, ALLTRANS_INSTRUCTIONS, NORMANS_LIST )
                        && QuestHelper.getQuestItemsCount( player, BRAIDED_YARN ) < 70

            case STRAIN:
            case GHOUL:
                return QuestHelper.hasQuestItems( player, ALLTRANS_INSTRUCTIONS, NORMANS_LIST )
                        && QuestHelper.getQuestItemsCount( player, GRAY_BONE_POWDER ) < 70

            case DEAD_SEEKER:
                return QuestHelper.hasQuestItems( player, ALLTRANS_INSTRUCTIONS, NORMANS_LIST )
                        && QuestHelper.getQuestItemsCount( player, RED_PIGMENT ) < 70

            case BREKA_ORC:
            case BREKA_ORC_ARCHER:
            case BREKA_ORC_SHAMAN:
            case BREKA_ORC_OVERLORD:
            case BREKA_ORC_WARRIOR:
                return QuestHelper.hasQuestItems( player, ALLTRANS_INSTRUCTIONS, NORMANS_INSTRUCTIONS, DUNINGS_INSTRUCTIONS )
                        && QuestHelper.getQuestItemsCount( player, DUNINGS_KEY ) < 30
        }

        return false
    }

    getAttackableKillIds(): Array<number> {
        return [
            ANT,
            ANT_CAPTAIN,
            ANT_OVERSEER,
            GRANITE_GOLEM,
            MANDRAGORA_SPROUT1,
            MANDRAGORA_SAPLONG,
            MANDRAGORA_BLOSSOM,
            SILENOS,
            STRAIN,
            GHOUL,
            DEAD_SEEKER,
            MANDRAGORA_SPROUT2,
            BREKA_ORC,
            BREKA_ORC_ARCHER,
            BREKA_ORC_SHAMAN,
            BREKA_ORC_OVERLORD,
            BREKA_ORC_WARRIOR,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00216_TrialOfTheGuildsman'
    }

    getQuestStartIds(): Array<number> {
        return [ WAREHOUSE_KEEPER_VALKON ]
    }

    getTalkIds(): Array<number> {
        return [ WAREHOUSE_KEEPER_VALKON, WAREHOUSE_KEEPER_NORMAN, BLACKSMITH_ALTRAN, BLACKSMITH_PINTER, BLACKSMITH_DUNING ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        switch ( data.npcId ) {
            case ANT:
            case ANT_CAPTAIN:
            case ANT_OVERSEER:
                await this.processAntKilled( data, npc )
                return

            case GRANITE_GOLEM:
                await this.processGolemKilled( data, npc )
                return

            case MANDRAGORA_SPROUT1:
            case MANDRAGORA_SAPLONG:
            case MANDRAGORA_BLOSSOM:
            case MANDRAGORA_SPROUT2:
                await this.processMandragoraKilled( data, npc )
                return

            case SILENOS:
                await this.processSilenosKilled( data, npc )
                return

            case STRAIN:
            case GHOUL:
                await this.processGhoulKilled( data, npc )
                return

            case DEAD_SEEKER:
                await this.processSeekerKilled( data, npc )
                return

            case BREKA_ORC:
            case BREKA_ORC_ARCHER:
            case BREKA_ORC_SHAMAN:
            case BREKA_ORC_OVERLORD:
            case BREKA_ORC_WARRIOR:
                await this.processOrcKilled( data, npc )
                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( player.getAdena() >= 2000 ) {
                    state.startQuest()

                    await QuestHelper.takeSingleItem( player, ItemTypes.Adena, 2000 )

                    if ( !QuestHelper.hasQuestItem( player, VALKONS_RECOMMENDATION ) ) {
                        await QuestHelper.giveSingleItem( player, VALKONS_RECOMMENDATION, 1 )
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                    if ( !PlayerVariablesManager.get( data.playerId, QuestVariables.secondClassDiamondReward ) ) {
                        PlayerVariablesManager.set( data.playerId, QuestVariables.secondClassDiamondReward, true )

                        await QuestHelper.rewardSingleItem( player, DIMENSIONAL_DIAMOND, 85 )

                        return this.getPath( '30103-06d.htm' )
                    }

                    return this.getPath( '30103-06.htm' )
                }

                return this.getPath( '30103-05b.htm' )

            case '30103-04.htm':
            case '30103-05.htm':
            case '30103-05a.html':
            case '30103-06a.html':
            case '30103-06b.html':
            case '30103-06c.html':
            case '30103-07a.html':
            case '30103-07b.html':
            case '30103-07c.html':
            case '30210-02.html':
            case '30210-03.html':
            case '30210-08.html':
            case '30210-09.html':
            case '30210-11a.html':
            case '30283-03a.html':
            case '30283-03b.html':
            case '30283-04.html':
            case '30298-03.html':
            case '30298-05a.html':
                break

            case '30103-09a.html':
                if ( QuestHelper.hasQuestItem( player, ALLTRANS_INSTRUCTIONS ) && ( QuestHelper.getQuestItemsCount( player, JOURNEYMAN_RING ) >= 7 ) ) {
                    await QuestHelper.giveAdena( player, 187606, true )
                    await QuestHelper.giveSingleItem( player, MARK_OF_GUILDSMAN, 1 )
                    await QuestHelper.addExpAndSp( player, 1029478, 66768 )
                    await state.exitQuest( false, true )

                    player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )
                    break
                }

                return

            case '30103-09b.html':
                if ( QuestHelper.hasQuestItem( player, ALLTRANS_INSTRUCTIONS ) && ( QuestHelper.getQuestItemsCount( player, JOURNEYMAN_RING ) >= 7 ) ) {
                    await QuestHelper.giveAdena( player, 93803, true )
                    await QuestHelper.giveSingleItem( player, MARK_OF_GUILDSMAN, 1 )
                    await QuestHelper.addExpAndSp( player, 514739, 33384 )
                    await state.exitQuest( false, true )

                    player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )
                    break
                }

                return

            case '30210-04.html':
                if ( QuestHelper.hasQuestItem( player, ALLTRANS_1ST_RECOMMENDATION ) ) {
                    await QuestHelper.takeSingleItem( player, ALLTRANS_1ST_RECOMMENDATION, 1 )
                    await QuestHelper.giveMultipleItems( player, 1, NORMANS_INSTRUCTIONS, NORMANS_RECEIPT )

                    break
                }

                return

            case '30210-10.html':
                if ( QuestHelper.hasQuestItem( player, NORMANS_INSTRUCTIONS ) ) {
                    await QuestHelper.takeSingleItem( player, NORMANS_INSTRUCTIONS, 1 )
                    await QuestHelper.takeSingleItem( player, DUNINGS_KEY, -1 )
                    await QuestHelper.giveSingleItem( player, NORMANS_LIST, 1 )

                    break
                }

                return

            case '30283-03.html':
                if ( QuestHelper.hasQuestItems( player, VALKONS_RECOMMENDATION, MANDRAGORA_BERRY ) ) {
                    await QuestHelper.takeMultipleItems( player, 1, VALKONS_RECOMMENDATION, MANDRAGORA_BERRY )
                    await QuestHelper.giveMultipleItems( player, 1, RECIPE_JOURNEYMAN_RING, ALLTRANS_INSTRUCTIONS, ALLTRANS_1ST_RECOMMENDATION, ALLTRANS_2ND_RECOMMENDATION )

                    state.setConditionWithSound( 5, true )
                    break
                }

                return

            case '30298-04.html':
                if ( player.getClassId() === ClassIdValues.scavenger.id ) {
                    if ( QuestHelper.hasQuestItem( player, ALLTRANS_2ND_RECOMMENDATION ) ) {
                        await QuestHelper.takeSingleItem( player, ALLTRANS_2ND_RECOMMENDATION, 1 )
                        await QuestHelper.giveSingleItem( player, PINTERS_INSTRUCTIONS, 1 )
                    }

                    break
                }

                if ( QuestHelper.hasQuestItem( player, ALLTRANS_2ND_RECOMMENDATION ) ) {
                    await QuestHelper.takeSingleItem( player, ALLTRANS_2ND_RECOMMENDATION, 1 )
                    await QuestHelper.giveMultipleItems( player, 1, RECIPE_AMBER_BEAD, PINTERS_INSTRUCTIONS )

                    return this.getPath( '30298-05.html' )
                }

                return

            case '30688-02.html':
                if ( QuestHelper.hasQuestItem( player, NORMANS_RECEIPT ) ) {
                    await QuestHelper.takeSingleItem( player, NORMANS_RECEIPT, 1 )
                    await QuestHelper.giveSingleItem( player, DUNINGS_INSTRUCTIONS, 1 )

                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === WAREHOUSE_KEEPER_VALKON ) {
                    if ( [ ClassIdValues.artisan.id, ClassIdValues.scavenger.id ].includes( player.getClassId() ) ) {
                        if ( player.getLevel() < minimumLevel ) {
                            return this.getPath( '30103-02.html' )
                        }

                        return this.getPath( '30103-03.htm' )
                    }

                    return this.getPath( '30103-01.html' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case WAREHOUSE_KEEPER_VALKON:
                        if ( QuestHelper.hasQuestItem( player, VALKONS_RECOMMENDATION ) ) {
                            state.setConditionWithSound( 3, true )

                            return this.getPath( '30103-07.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, ALLTRANS_INSTRUCTIONS ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, JOURNEYMAN_RING ) < 7 ) {
                                return this.getPath( '30103-08.html' )
                            }

                            return this.getPath( '30103-09.html' )
                        }

                        break

                    case WAREHOUSE_KEEPER_NORMAN:
                        if ( QuestHelper.hasQuestItem( player, ALLTRANS_INSTRUCTIONS ) ) {
                            if ( QuestHelper.hasQuestItem( player, ALLTRANS_1ST_RECOMMENDATION ) ) {
                                return this.getPath( '30210-01.html' )
                            }

                            if ( QuestHelper.hasQuestItems( player, NORMANS_INSTRUCTIONS, NORMANS_RECEIPT ) ) {
                                return this.getPath( '30210-05.html' )
                            }

                            if ( QuestHelper.hasQuestItems( player, NORMANS_INSTRUCTIONS, DUNINGS_INSTRUCTIONS ) ) {
                                return this.getPath( '30210-06.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, NORMANS_INSTRUCTIONS )
                                    && QuestHelper.getQuestItemsCount( player, DUNINGS_KEY ) >= 30 ) {
                                return this.getPath( '30210-07.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, NORMANS_LIST ) ) {
                                if ( ( QuestHelper.getQuestItemsCount( player, GRAY_BONE_POWDER ) >= 70 )
                                        && QuestHelper.getQuestItemsCount( player, GRANITE_WHETSTONE ) >= 70
                                        && QuestHelper.getQuestItemsCount( player, RED_PIGMENT ) >= 70
                                        && QuestHelper.getQuestItemsCount( player, BRAIDED_YARN ) >= 70 ) {
                                    await QuestHelper.takeSingleItem( player, NORMANS_LIST, 1 )
                                    await QuestHelper.takeMultipleItems( player, -1, GRAY_BONE_POWDER, GRANITE_WHETSTONE, RED_PIGMENT, BRAIDED_YARN )
                                    await QuestHelper.rewardSingleItem( player, JOURNEYMAN_GEM, 7 )

                                    if ( QuestHelper.getQuestItemsCount( player, JOURNEYMAN_DECO_BEADS ) >= 7 ) {
                                        state.setConditionWithSound( 6, true )
                                    }

                                    return this.getPath( '30210-12.html' )
                                }

                                return this.getPath( '30210-11.html' )
                            }

                            if ( !QuestHelper.hasAtLeastOneQuestItem( player, NORMANS_INSTRUCTIONS, NORMANS_LIST )
                                    && QuestHelper.hasAtLeastOneQuestItem( player, JOURNEYMAN_GEM, JOURNEYMAN_RING ) ) {
                                return this.getPath( '30210-13.html' )
                            }
                        }

                        break

                    case BLACKSMITH_ALTRAN:
                        if ( QuestHelper.hasQuestItem( player, VALKONS_RECOMMENDATION ) ) {
                            if ( !QuestHelper.hasQuestItem( player, MANDRAGORA_BERRY ) ) {
                                state.setConditionWithSound( 2, true )

                                return this.getPath( '30283-01.html' )
                            }

                            return this.getPath( '30283-02.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, ALLTRANS_INSTRUCTIONS ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, JOURNEYMAN_RING ) < 7 ) {
                                return this.getPath( '30283-04.html' )
                            }

                            return this.getPath( '30283-05.html' )
                        }

                        break

                    case BLACKSMITH_PINTER:
                        if ( QuestHelper.hasQuestItem( player, ALLTRANS_INSTRUCTIONS ) ) {
                            if ( QuestHelper.hasQuestItem( player, ALLTRANS_2ND_RECOMMENDATION ) ) {
                                return this.getPath( '30298-02.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, PINTERS_INSTRUCTIONS ) ) {
                                if ( QuestHelper.getQuestItemsCount( player, AMBER_BEAD ) < 70 ) {
                                    return this.getPath( '30298-06.html' )
                                }

                                await QuestHelper.takeMultipleItems( player, 1, RECIPE_AMBER_BEAD, PINTERS_INSTRUCTIONS )
                                await QuestHelper.takeMultipleItems( player, -1, AMBER_BEAD, AMBER_LUMP )
                                await QuestHelper.rewardSingleItem( player, JOURNEYMAN_DECO_BEADS, 7 )

                                if ( QuestHelper.getQuestItemsCount( player, JOURNEYMAN_GEM ) >= 7 ) {
                                    state.setConditionWithSound( 6, true )
                                }

                                return this.getPath( '30298-07.html' )
                            }

                            if ( !QuestHelper.hasQuestItem( player, PINTERS_INSTRUCTIONS )
                                    && QuestHelper.hasAtLeastOneQuestItem( player, JOURNEYMAN_DECO_BEADS, JOURNEYMAN_RING ) ) {
                                return this.getPath( '30298-08.html' )
                            }
                        }

                        break

                    case BLACKSMITH_DUNING:
                        if ( QuestHelper.hasQuestItems( player, ALLTRANS_INSTRUCTIONS, NORMANS_INSTRUCTIONS ) ) {
                            if ( QuestHelper.hasQuestItem( player, NORMANS_RECEIPT ) && !QuestHelper.hasQuestItem( player, DUNINGS_INSTRUCTIONS ) ) {
                                return this.getPath( '30688-01.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, DUNINGS_INSTRUCTIONS )
                                    && !QuestHelper.hasQuestItem( player, NORMANS_RECEIPT )
                                    && QuestHelper.getQuestItemsCount( player, DUNINGS_KEY ) < 30 ) {
                                return this.getPath( '30688-03.html' )
                            }

                            if ( ( QuestHelper.getQuestItemsCount( player, DUNINGS_KEY ) >= 30 ) && !QuestHelper.hasQuestItem( player, DUNINGS_INSTRUCTIONS ) ) {
                                return this.getPath( '30688-04.html' )
                            }

                            break
                        }

                        if ( QuestHelper.hasQuestItem( player, ALLTRANS_INSTRUCTIONS ) && !QuestHelper.hasAtLeastOneQuestItem( player, NORMANS_INSTRUCTIONS, DUNINGS_INSTRUCTIONS ) ) {
                            return this.getPath( '30688-05.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === WAREHOUSE_KEEPER_VALKON ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async processAntKilled( data: AttackableKillEvent, npc: L2Npc ): Promise<void> {
        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), -1, 2, npc )
        if ( !state ) {
            return
        }

        let count = 0
        let player = state.getPlayer()

        if ( player.getClassId() === ClassIdValues.scavenger.id && npc.isSweepActive() ) {
            count += 5
        }

        if ( _.random( 1 ) === 0 && player.getClassId() === ClassIdValues.artisan.id ) {
            await QuestHelper.rewardSingleQuestItem( player, AMBER_LUMP, 1, data.isChampion )
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
        }

        if ( ( QuestHelper.getQuestItemsCount( player, AMBER_BEAD ) + count ) < 70 ) {
            count += 5
        }

        if ( count > 0 ) {
            await QuestHelper.rewardUpToLimit( player, AMBER_BEAD, count, 70, data.isChampion )
        }
    }

    async processGhoulKilled( data: AttackableKillEvent, npc: L2Npc ): Promise<void> {
        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), -1, 2, npc )
        if ( !state ) {
            return
        }

        let player = state.getPlayer()

        await QuestHelper.rewardSingleQuestItem( player, GRAY_BONE_POWDER, 5, data.isChampion )
        if ( QuestHelper.getQuestItemsCount( player, GRAY_BONE_POWDER ) >= 70 ) {
            return player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
        }

        return player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async processGolemKilled( data: AttackableKillEvent, npc: L2Npc ): Promise<void> {
        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), -1, 2, npc )
        if ( !state ) {
            return
        }

        let player = state.getPlayer()

        await QuestHelper.rewardSingleQuestItem( player, GRANITE_WHETSTONE, 7, data.isChampion )
        if ( await QuestHelper.getQuestItemsCount( player, GRANITE_WHETSTONE ) >= 70 ) {
            return player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async processMandragoraKilled( data: AttackableKillEvent, npc: L2Npc ): Promise<void> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        if ( GeneralHelper.checkIfInRange( 1500, npc, player, true ) && QuestHelper.hasQuestItems( player, VALKONS_RECOMMENDATION ) && !QuestHelper.hasQuestItems( player, MANDRAGORA_BERRY ) ) {
            await QuestHelper.giveSingleItem( player, MANDRAGORA_BERRY, 1 )
            state.setConditionWithSound( 4, true )
        }
    }

    async processOrcKilled( data: AttackableKillEvent, npc: L2Npc ): Promise<void> {
        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), -1, 2, npc )

        if ( !state ) {
            return
        }

        let player = state.getPlayer()

        await QuestHelper.rewardSingleQuestItem( player, DUNINGS_KEY, 1, data.isChampion )
        if ( QuestHelper.getQuestItemsCount( player, DUNINGS_KEY ) >= 29 ) {

            await QuestHelper.takeSingleItem( player, DUNINGS_INSTRUCTIONS, 1 )
            return player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
        }

        return player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async processSeekerKilled( data: AttackableKillEvent, npc: L2Npc ): Promise<void> {
        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), -1, 2, npc )

        if ( !state ) {
            return
        }

        let player = state.getPlayer()

        await QuestHelper.rewardSingleQuestItem( player, RED_PIGMENT, 7, data.isChampion )
        if ( QuestHelper.getQuestItemsCount( player, RED_PIGMENT ) >= 70 ) {
            return player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
        }

        return player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async processSilenosKilled( data: AttackableKillEvent, npc: L2Npc ): Promise<void> {
        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), -1, 2, npc )
        if ( !state ) {
            return
        }

        let player = state.getPlayer()
        await QuestHelper.rewardSingleQuestItem( player, BRAIDED_YARN, 10, data.isChampion )

        if ( QuestHelper.getQuestItemsCount( player, BRAIDED_YARN ) >= 70 ) {
            return player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
        }

        return player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }
}