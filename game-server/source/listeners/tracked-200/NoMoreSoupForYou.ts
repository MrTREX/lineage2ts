import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const STAN = 30200
const SOUP = 15712
const amountPerKill = 5

type QuestReward = [ number, number ] // itemId, amount
const weaponRewards: Array<QuestReward> = [
    [ 10377, 1 ],
    [ 10401, 1 ],
    [ 10401, 2 ],
    [ 10401, 3 ],
    [ 10401, 4 ],
    [ 10401, 5 ],
    [ 10401, 6 ],
]

const armorRewards: Array<QuestReward> = [
    [ 15812, 1 ],
    [ 15813, 1 ],
    [ 15814, 1 ],
    [ 15791, 1 ],
    [ 15787, 1 ],
    [ 15784, 1 ],
    [ 15781, 1 ],
    [ 15778, 1 ],
    [ 15775, 1 ],
    [ 15774, 5 ],
    [ 15773, 5 ],
    [ 15772, 5 ],
    [ 15693, 5 ],
    [ 15657, 5 ],
    [ 15654, 5 ],
    [ 15651, 5 ],
    [ 15648, 5 ],
    [ 15645, 5 ],
]

const minimumLevel = 82

export class NoMoreSoupForYou extends ListenerLogic {
    constructor() {
        super( 'Q00289_NoMoreSoupForYou', 'listeners/tracked-200/NoMoreSoupForYou.ts' )
        this.questId = 289
    }

    getAttackableKillIds(): Array<number> {
        return [
            18908,
            22779,
            22786,
            22787,
            22788,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00289_NoMoreSoupForYou'
    }

    getQuestStartIds(): Array<number> {
        return [ STAN ]
    }

    getTalkIds(): Array<number> {
        return [ STAN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        await QuestHelper.rewardSingleQuestItem( player, SOUP, amountPerKill, data.isChampion )

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30200-03.htm':
                state.startQuest()
                break

            case '30200-05.htm':
                if ( QuestHelper.getQuestItemsCount( player, SOUP ) >= 500 ) {
                    let [ itemId, amount ] = _.sample( weaponRewards )
                    await QuestHelper.rewardSingleItem( player, itemId, amount )
                    await QuestHelper.takeSingleItem( player, SOUP, 500 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                    return this.getPath( '30200-04.htm' )
                }

                return this.getPath( '30200-07.htm' )

            case '30200-06.htm':
                if ( QuestHelper.getQuestItemsCount( player, SOUP ) >= 100 ) {
                    let [ itemId, amount ] = _.sample( armorRewards )
                    await QuestHelper.rewardSingleItem( player, itemId, amount )
                    await QuestHelper.takeSingleItem( player, SOUP, 100 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                    return this.getPath( '30200-04.htm' )
                }

                return this.getPath( '30200-07.htm' )
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                let canProceed: boolean = player.hasQuestCompleted( 'Q00252_ItSmellsDelicious' ) && player.getLevel() >= minimumLevel
                return this.getPath( canProceed ? '30200-01.htm' : '30200-00.htm' )

            case QuestStateValues.STARTED:
                return this.getPath( QuestHelper.getQuestItemsCount( player, SOUP ) >= 100 ? '30200-04.htm' : '30200-03.htm' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}