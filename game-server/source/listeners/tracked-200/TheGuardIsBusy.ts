import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { NewbieRewardsHelper } from '../helpers/NewbieRewardsHelper'

import _ from 'lodash'
import aigle from 'aigle'

const GILBERT = 30039
const GLUDIO_LORDS_MARK = 1084
const ORC_AMULET = 752
const ORC_NECKLACE = 1085
const WEREWOLF_FANG = 1086
const minimumLevel = 6

type MobChanceProperties = [ number, number, number, number ] // dice limit, chance to reach, itemId, amount to give

const mobProperties: { [ key: number ]: Array<MobChanceProperties> } = {
    20006: [ [ 10, 2, ORC_AMULET, 2 ], [ 10, 10, ORC_AMULET, 1 ] ], // Orc Archer
    20093: [ [ 100, 85, ORC_NECKLACE, 1 ] ], // Orc Fighter
    20096: [ [ 100, 95, ORC_NECKLACE, 1 ] ], // Orc Fighter Sub Leader
    20098: [ [ 100, 100, ORC_NECKLACE, 1 ] ],// Orc Fighter Leader
    20130: [ [ 10, 7, ORC_AMULET, 1 ] ], // Orc
    20131: [ [ 10, 9, ORC_AMULET, 1 ] ], // Orc Grunt
    20132: [ [ 10, 7, WEREWOLF_FANG, 1 ] ], // Werewolf
    20342: [ [ 0, 1, WEREWOLF_FANG, 1 ] ], // Werewolf Chieftain
    20343: [ [ 100, 85, WEREWOLF_FANG, 1 ] ], // Werewolf Hunter
}

export class TheGuardIsBusy extends ListenerLogic {
    constructor() {
        super( 'Q00257_TheGuardIsBusy', 'listeners/tracked-200/TheGuardIsBusy.ts' )
        this.questId = 257
        this.questItemIds = [
            ORC_AMULET,
            GLUDIO_LORDS_MARK,
            ORC_NECKLACE,
            WEREWOLF_FANG,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( mobProperties ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00257_TheGuardIsBusy'
    }

    getQuestStartIds(): Array<number> {
        return [ GILBERT ]
    }

    getTalkIds(): Array<number> {
        return [ GILBERT ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        await aigle.resolve( mobProperties[ data.npcId ] ).eachSeries( async ( properties: MobChanceProperties ) => {
            let [ diceLimit, chance, itemId, amount ] = properties
            if ( _.random( diceLimit ) >= QuestHelper.getAdjustedChance( itemId, chance, data.isChampion ) ) {
                return
            }

            await QuestHelper.rewardSingleQuestItem( player, itemId, amount, data.isChampion )
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

            return false
        } )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30039-03.htm':
                state.startQuest()
                await QuestHelper.giveSingleItem( L2World.getPlayer( data.playerId ), GLUDIO_LORDS_MARK, 1 )
                break

            case '30039-05.html':
                await state.exitQuest( true, true )
                break

            case '30039-06.html':
                break
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30039-02.htm' : '30039-01.html' )

            case QuestStateValues.STARTED:
                if ( !QuestHelper.hasAtLeastOneQuestItem( player, ORC_AMULET, ORC_NECKLACE, WEREWOLF_FANG ) ) {
                    return this.getPath( '30039-04.html' )
                }

                let amuletAmount = QuestHelper.getQuestItemsCount( player, ORC_AMULET )
                let otherItemsAmount = QuestHelper.getItemsSumCount( player, ORC_NECKLACE, WEREWOLF_FANG )
                let totalAmount = ( amuletAmount * 10 ) + ( otherItemsAmount * 20 ) + ( ( amuletAmount + otherItemsAmount ) >= 10 ? 1000 : 0 )

                await QuestHelper.giveAdena( player, totalAmount, true )
                await QuestHelper.takeMultipleItems( player, -1, ORC_AMULET, ORC_NECKLACE, WEREWOLF_FANG )
                await NewbieRewardsHelper.giveNewbieReward( player )

                return this.getPath( '30039-07.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}