import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { CategoryType } from '../../gameService/enums/CategoryType'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'
import { QuestVariables } from '../helpers/QuestVariables'
import _ from 'lodash'
import { PlayerRadarCache } from '../../gameService/cache/PlayerRadarCache'
import { SpawnMakerCache } from '../../gameService/cache/SpawnMakerCache'
import { createItemDefinition } from '../../gameService/interface/ItemDefinition'

const FILAUR = 30535
const KASH = 30644
const MARTIAN = 30645
const RALDO = 30646
const CHEST_OF_SHYSLASSYS = 30647
const MARKETEER_OF_MAMMON = 31092

const LETTER_OF_KASH = 2628
const WATCHERS_EYE1 = 2629
const WATCHERS_EYE2 = 2630
const SCROLL_OF_SHYSLASSYS = 2631
const BROKEN_KEY = 2632

const SHYSLASSYS = 27110
const GORR = 27112
const BARAHAM = 27113
const QUEEN_OF_SUCCUBUS = 27114

const ELVEN_NECKLACE_BEADS = 1904
const WHITE_TUNIC_PATTERN = 1936
const IRON_BOOTS_DESIGN = 1940
const MANTICOR_SKIN_GAITERS_PATTERN = 1943
const GAUNTLET_OF_REPOSE_PATTERN = 1946
const MITHRIL_SCALE_GAITERS_MATERIAL = 2918
const BRIGAMDINE_GAUNTLET_PATTERN = 2927
const TOME_OF_BLOOD_PAGE = 2030
const MARK_OF_CHALLENGER = 2627
const DIMENSIONAL_DIAMONDS = createItemDefinition( 7562, 61 )

const minimumLevel = 35

export class TrialOfTheChallenger extends ListenerLogic {
    constructor() {
        super( 'Q00211_TrialOfTheChallenger', 'listeners/tracked-200/TrialOfTheChallenger.ts' )
        this.questId = 211
        this.questItemIds = [
            LETTER_OF_KASH,
            WATCHERS_EYE1,
            WATCHERS_EYE2,
            SCROLL_OF_SHYSLASSYS,
            BROKEN_KEY,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [ SHYSLASSYS, GORR, BARAHAM, QUEEN_OF_SUCCUBUS ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00211_TrialOfTheChallenger'
    }

    getQuestStartIds(): Array<number> {
        return [ KASH ]
    }

    getTalkIds(): Array<number> {
        return [ FILAUR, KASH, MARTIAN, RALDO, CHEST_OF_SHYSLASSYS, MARKETEER_OF_MAMMON ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId )

        if ( !GeneralHelper.checkIfInRange( 1500, npc, player, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case SHYSLASSYS:
                if ( state.isCondition( 1 ) ) {
                    if ( _.size( SpawnMakerCache.getAllSpawns( data.npcId ) ) < 10 ) {
                        QuestHelper.addSpawnAtLocation( CHEST_OF_SHYSLASSYS, npc, false, 200000 )
                    }

                    await QuestHelper.rewardMultipleItems( player, 1, SCROLL_OF_SHYSLASSYS, BROKEN_KEY )
                    state.setConditionWithSound( 2, true )
                }

                break

            case GORR:
                if ( state.isCondition( 4 ) ) {
                    await QuestHelper.rewardSingleItem( player, WATCHERS_EYE1, 1 )
                    state.setConditionWithSound( 5, true )
                }

                break

            case BARAHAM:
                if ( state.isCondition( 6 ) ) {
                    if ( _.size( SpawnMakerCache.getAllSpawns( data.npcId ) ) < 10 ) {
                        QuestHelper.addSpawnAtLocation( RALDO, npc, false, 100000 )
                    }

                    await QuestHelper.rewardSingleItem( player, WATCHERS_EYE2, 1 )
                    state.setConditionWithSound( 7, true )
                }

                break

            case QUEEN_OF_SUCCUBUS:
                if ( state.isCondition( 9 ) ) {
                    if ( _.size( SpawnMakerCache.getAllSpawns( data.npcId ) ) < 10 ) {
                        QuestHelper.addSpawnAtLocation( RALDO, npc, false, 100000 )
                    }

                    state.setConditionWithSound( 10, true )
                }

                break
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30644-04.htm':
                break

            case '30645-07.html':
            case '30645-08.html':
            case '30646-02.html':
            case '30646-03.html':
                if ( state.isStarted() ) {
                    break
                }

                return

            case '30644-06.htm':
                if ( state.isCreated() ) {
                    state.startQuest()

                    if ( !PlayerVariablesManager.get( data.playerId, QuestVariables.secondClassDiamondReward ) ) {
                        await QuestHelper.giveSingleItem( player, DIMENSIONAL_DIAMONDS.id, DIMENSIONAL_DIAMONDS.count )
                        PlayerVariablesManager.set( data.playerId, QuestVariables.secondClassDiamondReward, true )

                        break
                    }

                    return this.getPath( '30644-05.htm' )
                }

                return

            case '30647-02.html':
                let canGiveRewards: boolean = state.isCondition( 2 ) && QuestHelper.hasQuestItem( player, BROKEN_KEY )
                if ( !canGiveRewards ) {
                    return this.getPath( '30647-04.html' )
                }

                await QuestHelper.takeSingleItem( player, BROKEN_KEY, -1 )
                if ( _.random( 10 ) < 2 ) {
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_JACKPOT )

                    let random = _.random( 100 )
                    if ( random > 90 ) {
                        await QuestHelper.rewardMultipleItems( player, 1, MITHRIL_SCALE_GAITERS_MATERIAL, BRIGAMDINE_GAUNTLET_PATTERN, MANTICOR_SKIN_GAITERS_PATTERN, GAUNTLET_OF_REPOSE_PATTERN, IRON_BOOTS_DESIGN )
                    } else if ( random > 70 ) {
                        await QuestHelper.rewardMultipleItems( player, 1, TOME_OF_BLOOD_PAGE, ELVEN_NECKLACE_BEADS )
                    } else if ( random > 40 ) {
                        await QuestHelper.rewardSingleItem( player, WHITE_TUNIC_PATTERN, 1 )
                    } else {
                        await QuestHelper.rewardSingleItem( player, IRON_BOOTS_DESIGN, 1 )
                    }

                    return this.getPath( '30647-03.html' )
                }

                await QuestHelper.giveAdena( player, _.random( 1000 ) + 1, true )
                break

            case '30645-02.html':
                if ( state.isCondition( 3 ) && QuestHelper.hasQuestItem( player, LETTER_OF_KASH ) ) {
                    state.setConditionWithSound( 4, true )

                    break
                }

                return

            case '30646-04.html':
            case '30646-05.html':
                if ( state.isCondition( 7 ) && QuestHelper.hasQuestItem( player, WATCHERS_EYE2 ) ) {
                    await QuestHelper.takeSingleItem( player, WATCHERS_EYE2, -1 )
                    state.setConditionWithSound( 8, true )

                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case KASH:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        if ( !player.isInCategory( CategoryType.WARRIOR_GROUP ) ) {
                            return this.getPath( '30644-02.html' )
                        }

                        if ( player.getLevel() < minimumLevel ) {
                            return this.getPath( '30644-01.html' )
                        }

                        return this.getPath( '30644-03.htm' )

                    case QuestStateValues.STARTED:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '30644-07.html' )

                            case 2:
                                if ( QuestHelper.hasQuestItem( player, SCROLL_OF_SHYSLASSYS ) ) {
                                    await QuestHelper.takeSingleItem( player, SCROLL_OF_SHYSLASSYS, -1 )
                                    await QuestHelper.giveSingleItem( player, LETTER_OF_KASH, 1 )

                                    state.setConditionWithSound( 3, true )
                                    return this.getPath( '30644-08.html' )
                                }

                                break

                            case 3:
                                if ( QuestHelper.hasQuestItem( player, LETTER_OF_KASH ) ) {
                                    return this.getPath( '30644-09.html' )
                                }

                                break

                            case 8:
                            case 9:
                            case 10:
                                return this.getPath( '30644-10.html' )
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case MARTIAN:
                switch ( state.getCondition() ) {
                    case 3:
                        if ( QuestHelper.hasQuestItem( player, LETTER_OF_KASH ) ) {
                            return this.getPath( '30645-01.html' )
                        }

                        break

                    case 4:
                        return this.getPath( '30645-03.html' )

                    case 5:
                        if ( QuestHelper.hasQuestItems( player, WATCHERS_EYE1 ) ) {
                            await QuestHelper.takeSingleItem( player, WATCHERS_EYE1, -1 )
                            state.setConditionWithSound( 6, true )

                            return this.getPath( '30645-04.html' )
                        }

                        break

                    case 6:
                        return this.getPath( '30645-05.html' )

                    case 7:
                        return this.getPath( '30645-06.html' )

                    case 8:
                    case 9:
                        return this.getPath( '30645-09.html' )
                }

                break

            case CHEST_OF_SHYSLASSYS:
                if ( state.isStarted() ) {
                    return this.getPath( '30647-01.html' )
                }

                break

            case RALDO:
                switch ( state.getCondition() ) {
                    case 7:
                        if ( QuestHelper.hasQuestItem( player, WATCHERS_EYE2 ) ) {
                            return this.getPath( '30646-01.html' )
                        }

                        break

                    case 8:
                        return this.getPath( '30646-06.html' )

                    case 10:
                        await QuestHelper.addExpAndSp( player, 1067606, 69242 )
                        await QuestHelper.giveAdena( player, 194556, true )
                        await QuestHelper.giveSingleItem( player, MARK_OF_CHALLENGER, 1 )

                        player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )
                        await state.exitQuest( false, true )

                        return this.getPath( '30646-07.html' )
                }

                break

            case FILAUR:
                switch ( state.getCondition() ) {
                    case 8:
                        state.setConditionWithSound( 9, true )
                        return this.getPath( '30535-01.html' )

                    case 9:
                        PlayerRadarCache.getRadar( data.playerId ).addNpcMarker( 151589, -174823, -1776 )
                        return this.getPath( '30535-02.html' )

                    case 10:
                        return this.getPath( '30535-03.html' )
                }
                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}