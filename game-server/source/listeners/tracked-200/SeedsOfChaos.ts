import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import {
    AttackableKillEvent,
    NpcGeneralEvent,
    NpcSpawnEvent,
    NpcTalkEvent,
} from '../../gameService/models/events/EventType'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { Race } from '../../gameService/enums/Race'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import _ from 'lodash'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const KURSTIN = 31387
const MYSTERIOU_WIZARD = 31522
const HIERARCH_KEKROPUS = 32138
const VICE_HIERARCH_MAO = 32190
const KATENAR = 32235
const HARKILGAMED = 32236
const RODENPICULA = 32237
const ROCK = 32238
const MOTHER_NORNIL = 32239
const KATENAR_A = 32332
const KATENAR_B = 32333
const HARKILGAMED_A = 32334

const STAR_OF_DESTINY = 5011
const SHINING_MEDALLION = 9743
const BLOOD_JEWEL = 9744
const BLACK_ECHO_CRYSTAL = 9745

const SCROLL_ENCHANT_WEAPON_A_GRADE = 729

const NEEDLE_STAKATO_DRONE = 21516
const SHOUT_OF_SPLENDOR = 21532
const ALLIANCE_OF_SPLENDOR = 21533
const ALLIANCE_OF_SPLENDOR_1 = 21534
const SIGNET_OF_SPLENDOR = 21535
const CROWN_OF_SPLENDOR = 21536
const FANG_OF_SPLENDOR = 21537
const FANG_OF_SPLENDOR_1 = 21538
const WAILINGOF_SPLENDOR = 21539
const WAILINGOF_SPLENDOR_1 = 21540
const VAMPIRE_WIZARD = 21588
const VAMPIRE_WIZARD_A = 21589

const minimumLevel = 75
const variableNames = {
    playerId: 'pid',
    summonOwnerId: 'sid',
    isSpawned: 'isd',
}

const eventNames = {
    KATENAR_120: 'k120',
    KATENAR_A_120: 'ka120',
    KATENAR_B_120: 'kb120',
    HARKILGAMED_120: 'h120',
    HARKILGAMED_A_120: 'ha120',
}

export class SeedsOfChaos extends ListenerLogic {
    constructor() {
        super( 'Q00236_SeedsOfChaos', 'listeners/tracked-200/SeedsOfChaos.ts' )
        this.questId = 236
        this.questItemIds = [
            SHINING_MEDALLION,
            BLOOD_JEWEL,
            BLACK_ECHO_CRYSTAL,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            NEEDLE_STAKATO_DRONE,
            SHOUT_OF_SPLENDOR,
            ALLIANCE_OF_SPLENDOR,
            ALLIANCE_OF_SPLENDOR_1,
            SIGNET_OF_SPLENDOR,
            CROWN_OF_SPLENDOR,
            FANG_OF_SPLENDOR,
            FANG_OF_SPLENDOR_1,
            WAILINGOF_SPLENDOR,
            WAILINGOF_SPLENDOR_1,
            VAMPIRE_WIZARD,
            VAMPIRE_WIZARD_A,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00236_SeedsOfChaos'
    }

    getQuestStartIds(): Array<number> {
        return [ HIERARCH_KEKROPUS ]
    }

    getSpawnIds(): Array<number> {
        return [ KATENAR, HARKILGAMED, KATENAR_A, KATENAR_B, HARKILGAMED_A ]
    }

    getTalkIds(): Array<number> {
        return [
            HIERARCH_KEKROPUS,
            KURSTIN,
            MYSTERIOU_WIZARD,
            VICE_HIERARCH_MAO,
            KATENAR,
            HARKILGAMED,
            RODENPICULA,
            ROCK,
            MOTHER_NORNIL,
            KATENAR_A,
            KATENAR_B,
            HARKILGAMED_A,
        ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId )

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case NEEDLE_STAKATO_DRONE:
                if ( state.isMemoState( 2 )
                        && !QuestHelper.hasQuestItem( player, BLACK_ECHO_CRYSTAL )
                        && _.random( 100 ) < QuestHelper.getAdjustedChance( BLACK_ECHO_CRYSTAL, 20, data.isChampion ) ) {
                    await QuestHelper.giveSingleItem( player, BLACK_ECHO_CRYSTAL, 1 )

                    state.setConditionWithSound( 3, true )
                }

                return

            case SHOUT_OF_SPLENDOR:
            case ALLIANCE_OF_SPLENDOR:
            case ALLIANCE_OF_SPLENDOR_1:
            case SIGNET_OF_SPLENDOR:
            case CROWN_OF_SPLENDOR:
            case FANG_OF_SPLENDOR:
            case FANG_OF_SPLENDOR_1:
            case WAILINGOF_SPLENDOR:
            case WAILINGOF_SPLENDOR_1:
                if ( state.isMemoState( 21 )
                        && QuestHelper.getQuestItemsCount( player, SHINING_MEDALLION ) < 62
                        && _.random( 100 ) < QuestHelper.getAdjustedChance( SHINING_MEDALLION, 70, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, SHINING_MEDALLION, 1, data.isChampion )

                    if ( QuestHelper.getQuestItemsCount( player, SHINING_MEDALLION ) >= 62 ) {
                        state.setMemoState( 22 )
                        state.setConditionWithSound( 13, true )

                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case VAMPIRE_WIZARD:
            case VAMPIRE_WIZARD_A:
                if ( state.isMemoState( 7 )
                        && !QuestHelper.hasQuestItem( player, BLOOD_JEWEL )
                        && _.random( 100 ) < QuestHelper.getAdjustedChance( BLOOD_JEWEL, 8, data.isChampion ) ) {
                    await QuestHelper.giveSingleItem( player, BLOOD_JEWEL, 1 )

                    state.setConditionWithSound( 9, true )
                }

                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        switch ( data.eventName ) {
            case eventNames.KATENAR_120:
            case eventNames.KATENAR_A_120:
            case eventNames.KATENAR_B_120:
                await this.onNamedNpcEvent( data, NpcStringIds.HMM_WHERE_DID_MY_FRIEND_GO )
                return

            case eventNames.HARKILGAMED_120:
            case eventNames.HARKILGAMED_A_120:
                await this.onNamedNpcEvent( data, NpcStringIds.GRAAAH_WERE_BEING_ATTACKED )
                return
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32138-04.htm':
                if ( state.isCreated() ) {
                    state.setMemoState( 1 )
                    state.startQuest()

                    break
                }

                return

            case '32138-03.htm':
                if ( player.getLevel() < minimumLevel
                        || player.getRace() !== Race.KAMAEL
                        || !QuestHelper.hasQuestItem( player, STAR_OF_DESTINY ) ) {
                    return
                }

                break

            case '32138-12.html':
                if ( state.isMemoState( 30 ) ) {
                    state.setMemoState( 40 )
                    state.setConditionWithSound( 15, true )

                    break
                }

                return

            case '31387-03.html':
                if ( state.isMemoState( 11 ) ) {
                    await QuestHelper.takeSingleItem( player, BLOOD_JEWEL, -1 )

                    state.setMemoState( 12 )
                    break
                }

                return

            case '31387-05a.html':
                if ( state.isMemoState( 12 ) ) {
                    if ( player.hasQuestCompleted( 'Q00025_HidingBehindTheTruth' ) ) {
                        break
                    }

                    return this.getPath( '31387-05b.html' )
                }

                return

            case '31387-10.html':
                if ( state.isMemoState( 12 ) ) {
                    state.setMemoState( 20 )
                    state.setMemoStateEx( 1, 1 )
                    state.setConditionWithSound( 11, true )

                    break
                }

                if ( state.isMemoState( 20 ) && state.getMemoStateEx( 1 ) === 1 ) {
                    break
                }

                return

            case '31522-04a.html':
                if ( state.isMemoState( 1 ) ) {
                    break
                }

                return

            case '31522-05a.html':
                if ( state.isMemoState( 1 ) ) {
                    state.setMemoState( 2 )
                    state.setConditionWithSound( 2, true )

                    break
                }

                return

            case '31522-05b.html':
                if ( state.isMemoState( 1 ) ) {
                    state.setMemoState( 3 )
                    state.setMemoStateEx( 1, 0 )
                    state.setConditionWithSound( 6, true )

                    break
                }

                return

            case '31522-09a.html':
                if ( state.isMemoState( 2 ) && QuestHelper.hasQuestItem( player, BLACK_ECHO_CRYSTAL ) ) {
                    await QuestHelper.takeSingleItem( player, BLACK_ECHO_CRYSTAL, -1 )

                    state.setMemoState( 6 )
                    state.setConditionWithSound( 4, true )

                    break
                }

                return

            case '31522-12a.html':
                if ( !state.isMemoState( 6 ) ) {
                    return
                }

                return this.tryToSpawnNpc( data, KATENAR, player.getX() + 10, player.getY() + 10, player.getZ(), 10, '31522-13a.html', '31522-14a.html' )

            case '31522-09b.html':
                if ( !state.isMemoState( 3 ) || state.getMemoStateEx( 1 ) !== 2 ) {
                    return
                }

                return this.tryToSpawnNpc( data, KATENAR_A, player.getX() + 10, player.getY() + 10, player.getZ(), 10, '31522-10b.html', '31522-11b.html' )

            case '31522-14b.html':
                if ( !state.isMemoState( 7 ) || !QuestHelper.hasQuestItem( player, BLOOD_JEWEL ) ) {
                    return
                }

                return this.tryToSpawnNpc( data, KATENAR_B, player.getX() + 10, player.getY() + 10, player.getZ(), 10, '31522-15b.html', '31522-15bz.html' )

            case '32235-09a.html':
                if ( state.isMemoState( 6 ) ) {
                    state.setMemoState( 20 )
                    state.setMemoStateEx( 1, 0 )
                    state.setConditionWithSound( 5, true )

                    break
                }

                return

            case '32236-07.html':
                if ( state.isMemoState( 20 ) ) {
                    if ( NpcVariablesManager.get( data.characterId, variableNames.playerId ) as number > 0 ) {
                        state.setMemoState( 21 )
                        state.setMemoStateEx( 1, 0 )
                        state.setConditionWithSound( 12, true )
                    }

                    break
                }

                return

            case '32237-10.html':
                if ( state.isMemoState( 40 ) ) {
                    break
                }

                return

            case '32237-11.html':
                if ( state.isMemoState( 40 ) ) {
                    state.setMemoState( 42 )
                    state.setConditionWithSound( 17, true )

                    break
                }

                return

            case '32237-13.html':
                if ( state.isMemoState( 43 ) ) {
                    state.setMemoState( 44 )
                    state.setConditionWithSound( 19, true )

                    break
                }

                return

            case '32237-17.html':
                if ( state.isMemoState( 45 ) ) {
                    await QuestHelper.rewardSingleItem( player, SCROLL_ENCHANT_WEAPON_A_GRADE, 1 )
                    await QuestHelper.takeSingleItem( player, STAR_OF_DESTINY, 1 )
                    await state.exitQuest( false, true )

                    break
                }

                return

            case '32238-02.html':
                if ( !state.isMemoState( 20 ) ) {
                    return
                }

                return this.tryToSpawnNpc( data, HARKILGAMED, 71722, -78853, -4464, 0, '32238-03.html', '32238-04z.html' )

            case '32238-06.html':
                if ( !state.isMemoState( 22 ) ) {
                    return
                }

                return this.tryToSpawnNpc( data, HARKILGAMED_A, 71722, -78853, -4464, 0, '32238-07.html', '32238-08.html' )

            case '32239-04.html':
                if ( state.isMemoState( 42 ) ) {
                    state.setMemoState( 43 )
                    state.setConditionWithSound( 18, true )

                    break
                }

                return

            case '32239-08.html':
                if ( state.isMemoState( 44 ) ) {
                    state.setMemoState( 45 )
                    state.setConditionWithSound( 20, true )

                    break
                }

                return

            case '32332-05b.html':
                if ( state.isMemoState( 3 ) && ( state.getMemoStateEx( 1 ) === 2 ) ) {
                    if ( NpcVariablesManager.get( data.characterId, variableNames.playerId ) as number > 0 ) {
                        state.setMemoState( 7 )
                        state.setMemoStateEx( 1, 0 )
                        state.setConditionWithSound( 8, true )
                    }

                    break
                }

                return

            case '32334-17.html':
                if ( state.isMemoState( 22 ) ) {
                    if ( NpcVariablesManager.get( data.characterId, variableNames.playerId ) as number > 0 ) {
                        await QuestHelper.takeSingleItem( player, SHINING_MEDALLION, -1 )

                        state.setMemoState( 30 )
                        state.setConditionWithSound( 14, true )
                    }

                    break
                }

                return

            case 'KEITNAR_DESPAWN':
                if ( state.isMemoState( 20 ) && state.getMemoStateEx( 1 ) === 0 ) {
                    await this.tryToDespawnNpc( data, NpcStringIds.BEST_OF_LUCK_WITH_YOUR_FUTURE_ENDEAVOURS )
                }

                return


            case 'HARKILGAMED_DESPAWN':
                if ( state.isMemoState( 21 ) ) {
                    await this.tryToDespawnNpc( data, NpcStringIds.IN_THAT_CASE_I_WISH_YOU_GOOD_LUCK )
                }

                return

            case 'KEITNAR_A_DESPAWN':
                await this.tryToDespawnNpc( data, NpcStringIds.BEST_OF_LUCK_WITH_YOUR_FUTURE_ENDEAVOURS )
                return

            case 'KEITNAR_B_DESPAWN':
                if ( state.isMemoState( 11 ) && QuestHelper.hasQuestItem( player, BLOOD_JEWEL ) ) {
                    await this.tryToDespawnNpc( data, NpcStringIds.BEST_OF_LUCK_WITH_YOUR_FUTURE_ENDEAVOURS )
                }

                return

            case 'HARKILGAMED_A_DESPAWN':
                if ( state.isMemoState( 30 ) ) {
                    await this.tryToDespawnNpc( data, NpcStringIds.SAFE_TRAVELS )
                }

                return

            case '31387-02.html':
            case '31387-04.html':
            case '31387-06.html':
            case '31387-07.html':
            case '31387-08.html':
            case '31522-02.html':
            case '31522-03.html':
            case '31522-04b.html':
            case '31522-08a.html':
            case '31522-11a.html':
            case '32138-02.html':
            case '32138-07.html':
            case '32138-08.html':
            case '32138-09.html':
            case '32138-10.html':
            case '32138-11.html':
            case '32235-02a.html':
            case '32235-03a.html':
            case '32235-04a.html':
            case '32235-05a.html':
            case '32235-06a.html':
            case '32235-08a.html':
            case '32236-03.html':
            case '32236-04.html':
            case '32236-05.html':
            case '32236-06.html':
            case '32237-02.html':
            case '32237-03.html':
            case '32237-04.html':
            case '32237-05.html':
            case '32237-06.html':
            case '32237-07.html':
            case '32237-08.html':
            case '32237-16.html':
            case '32237-18.html':
            case '32239-03.html':
            case '32239-07.html':
            case '32332-02b.html':
            case '32332-03b.html':
            case '32332-04b.html':
            case '32334-10.html':
            case '32334-11.html':
            case '32334-12.html':
            case '32334-13.html':
            case '32334-14.html':
            case '32334-15.html':
            case '32334-16.html':
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onSpawnEvent( data: NpcSpawnEvent ): Promise<void> {
        switch ( data.npcId ) {
            case KATENAR:
                await this.prepareTimer( eventNames.KATENAR_120, data, NpcStringIds.S1_FINALLY_WE_MEET )
                return

            case HARKILGAMED:
                this.startQuestTimer( eventNames.HARKILGAMED_120, 120000, data.characterId, 0 )
                BroadcastHelper.broadcastNpcSayStringId( L2World.getObjectById( data.characterId ) as L2Npc, NpcSayType.NpcAll, NpcStringIds.HMM_IS_SOMEONE_APPROACHING )
                return

            case KATENAR_A:
                await this.prepareTimer( eventNames.KATENAR_A_120, data, NpcStringIds.S1_DID_YOU_WAIT_FOR_LONG )
                return

            case KATENAR_B:
                await this.prepareTimer( eventNames.KATENAR_B_120, data, NpcStringIds.DID_YOU_BRING_WHAT_I_ASKED_S1 )
                return

            case HARKILGAMED_A:
                await this.prepareTimer( eventNames.HARKILGAMED_A_120, data, NpcStringIds.S1_HAS_EVERYTHING_BEEN_FOUND )
                return
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== HIERARCH_KEKROPUS ) {
                    break
                }

                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '32138-01z.html' )
                }

                if ( player.getRace() !== Race.KAMAEL ) {
                    return this.getPath( '32138-01y.html' )
                }

                if ( QuestHelper.hasQuestItems( player, STAR_OF_DESTINY ) ) {
                    return this.getPath( '32138-01.htm' )
                }

                return this.getPath( '32138-01x.html' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case HIERARCH_KEKROPUS:
                        switch ( state.getMemoState() ) {
                            case 1:
                                return this.getPath( '32138-05.html' )

                            case 30:
                                return this.getPath( '32138-06.html' )

                            case 40:
                                return this.getPath( '32138-13.html' )
                        }

                        break

                    case KURSTIN:
                        switch ( state.getMemoState() ) {
                            case 11:
                                return this.getPath( '31387-01.html' )

                            case 12:
                                return this.getPath( '31387-04.html' )

                            case 20:
                                if ( state.getMemoStateEx( 1 ) === 1 ) {
                                    return this.getPath( '31387-11.html' )
                                }

                                break
                        }

                        break

                    case MYSTERIOU_WIZARD:
                        switch ( state.getMemoState() ) {
                            case 1:
                                return this.getPath( '31522-01.html' )

                            case 2:
                                if ( !QuestHelper.hasQuestItems( player, BLACK_ECHO_CRYSTAL ) ) {
                                    return this.getPath( '31522-06a.html' )
                                }

                                return this.getPath( '31522-07a.html' )

                            case 6:
                                return this.getPath( '31522-10a.html' )

                            case 20:
                                if ( state.getMemoStateEx( 1 ) === 0 ) {
                                    return this.getPath( '31522-15a.html' )
                                }

                                break

                            case 3:
                                if ( state.getMemoStateEx( 1 ) === 0 ) {
                                    state.setMemoStateEx( 1, 1 )

                                    return this.getPath( '31522-06b.html' )
                                }

                                if ( state.getMemoStateEx( 1 ) === 1 ) {
                                    state.setMemoStateEx( 1, 2 )
                                    state.setConditionWithSound( 7, true )

                                    return this.getPath( '31522-07b.html' )
                                }

                                if ( state.getMemoStateEx( 1 ) === 2 ) {
                                    return this.getPath( '31522-08b.html' )
                                }

                                break

                            case 7:
                                if ( !QuestHelper.hasQuestItems( player, BLOOD_JEWEL ) ) {
                                    return this.getPath( '31522-12b.html' )
                                }

                                return this.getPath( '31522-13b.html' )

                            case 11:
                                return this.getPath( '31522-16b.html' )
                        }

                        break

                    case VICE_HIERARCH_MAO:
                        if ( state.getMemoState() >= 40 && state.getMemoState() <= 45 ) {
                            return this.getPath( '32190-01.html' )
                        }
                        break

                    case KATENAR:
                        switch ( state.getMemoState() ) {
                            case 6:
                                NpcVariablesManager.remove( NpcVariablesManager.get( data.characterId, variableNames.summonOwnerId ) as number, variableNames.isSpawned )

                                if ( data.playerId === NpcVariablesManager.get( data.characterId, variableNames.playerId ) ) {
                                    return this.getPath( '32235-01a.html' )
                                }

                                return this.getPath( '32235-01z.html' )

                            case 20:
                                if ( state.getMemoStateEx( 1 ) === 0 ) {
                                    return this.getPath( '32235-09z.html' )
                                }

                                break
                        }

                        break

                    case HARKILGAMED:
                        switch ( state.getMemoState() ) {
                            case 20:
                                NpcVariablesManager.remove( NpcVariablesManager.get( data.characterId, variableNames.summonOwnerId ) as number, variableNames.isSpawned )
                                if ( data.playerId === NpcVariablesManager.get( data.characterId, variableNames.playerId ) ) {
                                    return this.getPath( '32236-01.html' )
                                }

                                return this.getPath( '32236-02.html' )

                            case 21:
                                return this.getPath( '32236-07z.html' )

                            case 22:
                                return this.getPath( '32236-08z.html' )
                        }

                        break

                    case RODENPICULA:
                        switch ( state.getMemoState() ) {
                            case 40:
                                return this.getPath( '32237-01.html' )

                            case 42:
                                return this.getPath( '32237-11a.html' )

                            case 43:
                                return this.getPath( '32237-12.html' )

                            case 44:
                                return this.getPath( '32237-14.html' )

                            case 45:
                                return this.getPath( '32237-15.html' )
                        }

                        break

                    case ROCK:
                        switch ( state.getMemoState() ) {
                            case 20:
                                return this.getPath( '32238-01.html' )

                            case 21:
                                return this.getPath( '32238-04.html' )

                            case 22:
                                return this.getPath( '32238-05.html' )

                            case 30:
                                return this.getPath( '32238-09.html' )
                        }

                        break

                    case MOTHER_NORNIL:
                        switch ( state.getMemoState() ) {
                            case 40:
                                return this.getPath( '32239-01.html' )

                            case 42:
                                return this.getPath( '32239-02.html' )

                            case 43:
                                return this.getPath( '32239-05.html' )

                            case 44:
                                return this.getPath( '32239-06.html' )

                            case 45:
                                return this.getPath( '32239-09.html' )
                        }

                        break

                    case KATENAR_A:
                        switch ( state.getMemoState() ) {
                            case 3:
                                if ( state.getMemoStateEx( 1 ) !== 2 ) {
                                    break
                                }

                                NpcVariablesManager.remove( NpcVariablesManager.get( data.characterId, variableNames.summonOwnerId ) as number, variableNames.isSpawned )
                                if ( data.playerId === NpcVariablesManager.get( data.characterId, variableNames.playerId ) ) {
                                    return this.getPath( '32332-01b.html' )
                                }

                                return this.getPath( '32332-01z.html' )

                            case 7:
                                if ( !QuestHelper.hasQuestItems( player, BLOOD_JEWEL ) ) {
                                    return this.getPath( '32332-05z.html' )
                                }

                                break
                        }

                        break

                    case KATENAR_B:
                        switch ( state.getMemoState() ) {
                            case 7:
                                if ( !QuestHelper.hasQuestItems( player, BLOOD_JEWEL ) ) {
                                    break
                                }

                                state.setMemoState( 11 )
                                state.setConditionWithSound( 10, true )

                                NpcVariablesManager.remove( NpcVariablesManager.get( data.characterId, variableNames.summonOwnerId ) as number, variableNames.isSpawned )
                                if ( data.playerId === NpcVariablesManager.get( data.characterId, variableNames.playerId ) ) {
                                    return this.getPath( '32333-06bz.html' )
                                }

                                return this.getPath( '32333-06b.html' )

                            case 11:
                                return this.getPath( '32333-06b.html' )
                        }

                        break

                    case HARKILGAMED_A:
                        switch ( state.getMemoState() ) {
                            case 22:
                                NpcVariablesManager.remove( NpcVariablesManager.get( data.characterId, variableNames.summonOwnerId ) as number, variableNames.isSpawned )
                                if ( data.playerId === NpcVariablesManager.get( data.characterId, variableNames.playerId ) ) {
                                    return this.getPath( '32334-08.html' )
                                }

                                return this.getPath( '32334-09.html' )

                            case 30:
                                return this.getPath( '32334-18.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === HIERARCH_KEKROPUS ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async onNamedNpcEvent( data: NpcGeneralEvent, stringId: number ): Promise<void> {
        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        let spawnedObjectId = NpcVariablesManager.get( data.characterId, variableNames.summonOwnerId ) as number
        if ( spawnedObjectId > 0 ) {
            if ( NpcVariablesManager.get( spawnedObjectId, variableNames.isSpawned ) ) {
                NpcVariablesManager.remove( spawnedObjectId, variableNames.isSpawned )
                BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, stringId )
            }
        }

        await npc.deleteMe()
    }

    async prepareTimer( name: string, data: NpcSpawnEvent, stringId: number ): Promise<void> {
        this.startQuestTimer( name, 120000, data.characterId, 0 )

        let player = L2World.getPlayer( NpcVariablesManager.get( data.characterId, variableNames.playerId ) as number )
        if ( player ) {
            let npc = L2World.getObjectById( data.characterId ) as L2Npc
            BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, stringId, player.getAppearance().getVisibleName() )
        }
    }

    async tryToDespawnNpc( data: NpcGeneralEvent, stringId: number ): Promise<void> {
        if ( data.playerId !== NpcVariablesManager.get( data.characterId, variableNames.playerId ) ) {
            return
        }

        NpcVariablesManager.remove( NpcVariablesManager.get( data.characterId, variableNames.summonOwnerId ) as number, variableNames.isSpawned )

        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, stringId )
        await npc.deleteMe()
    }

    tryToSpawnNpc( data: NpcGeneralEvent, npcId: number, x: number, y: number, z: number, heading: number, samePlayerPath: string, failurePath: string ): string {
        if ( !NpcVariablesManager.get( data.characterId, variableNames.isSpawned ) ) {
            NpcVariablesManager.set( data.characterId, variableNames.isSpawned, true )
            NpcVariablesManager.set( data.characterId, variableNames.playerId, data.playerId )

            let katenar: L2Npc = QuestHelper.addGenericSpawn( null, npcId, x, y, z, heading, false, 0 )

            NpcVariablesManager.set( katenar.getObjectId(), variableNames.summonOwnerId, data.characterId )
            NpcVariablesManager.set( katenar.getObjectId(), variableNames.playerId, data.playerId )

            return this.getPath( data.eventName )
        }

        if ( NpcVariablesManager.get( data.characterId, variableNames.playerId ) === data.playerId ) {
            return this.getPath( '31522-13a.html' )
        }

        return this.getPath( '31522-14a.html' )
    }
}