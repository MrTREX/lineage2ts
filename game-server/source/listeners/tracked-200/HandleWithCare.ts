import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestHelper } from '../helpers/QuestHelper'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const ANKUMI = 32741
const SEER_UGOROS = 18863
const HIGH_GRADE_LIZARD_SCALE = 15497
const MIDDLE_GRADE_LIZARD_SCALE = 15498
const SCROLL_ENCHANT_WEAPON_S_GRADE = 959
const SCROLL_ENCHANT_ARMOR_S_GRADE = 960
const HOLY_CRYSTAL = 9557

type QuestReward = [ number, number ] // itemId, amount
const questRewards: Array<QuestReward> = [
    [ SCROLL_ENCHANT_WEAPON_S_GRADE, 1 ],
    [ SCROLL_ENCHANT_ARMOR_S_GRADE, 1 ],
    [ SCROLL_ENCHANT_ARMOR_S_GRADE, 2 ],
    [ SCROLL_ENCHANT_ARMOR_S_GRADE, 3 ],
    [ HOLY_CRYSTAL, 1 ],
    [ HOLY_CRYSTAL, 2 ],
]

const minimumLevel = 82

export class HandleWithCare extends ListenerLogic {
    constructor() {
        super( 'Q00288_HandleWithCare', 'listeners/tracked-200/HandleWithCare.ts' )
        this.questId = 288
        this.questItemIds = [
            HIGH_GRADE_LIZARD_SCALE,
            MIDDLE_GRADE_LIZARD_SCALE,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [ SEER_UGOROS ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00288_HandleWithCare'
    }

    getQuestStartIds(): Array<number> {
        return [ ANKUMI ]
    }

    getTalkIds(): Array<number> {
        return [ ANKUMI ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || !state.isCondition( 1 ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId )

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        if ( !QuestHelper.hasQuestItem( player, MIDDLE_GRADE_LIZARD_SCALE ) ) {
            await QuestHelper.giveSingleItem( player, MIDDLE_GRADE_LIZARD_SCALE, 1 )

            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
            state.setConditionWithSound( 2, true )
            return
        }

        if ( !QuestHelper.hasQuestItem( player, HIGH_GRADE_LIZARD_SCALE ) ) {
            await QuestHelper.giveSingleItem( player, HIGH_GRADE_LIZARD_SCALE, 1 )

            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
            state.setConditionWithSound( 3, true )
            return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32741-03.htm':
                if ( player.getLevel() < minimumLevel ) {
                    return
                }

                break

            case '32741-04.html':
                if ( player.getLevel() < minimumLevel ) {
                    return
                }

                state.startQuest()
                break

            case '32741-08.html':
                if ( !state.isCondition( 1 ) ) {
                    return
                }

                let reward: QuestReward = await this.processReward( player )
                if ( reward ) {
                    let [ itemId, amount ] = reward
                    await QuestHelper.rewardSingleItem( player, itemId, amount )
                }

                await state.exitQuest( true, true )
                break


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() < minimumLevel ? '32741-01.html' : '32741-02.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '32741-05.html' )

                    case 2:
                        if ( QuestHelper.hasQuestItem( player, MIDDLE_GRADE_LIZARD_SCALE ) ) {
                            return this.getPath( '32741-06.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, HIGH_GRADE_LIZARD_SCALE ) ) {
                            return this.getPath( '32741-07.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async processReward( player: L2PcInstance ): Promise<QuestReward> {
        if ( QuestHelper.hasQuestItem( player, HIGH_GRADE_LIZARD_SCALE ) ) {
            await QuestHelper.takeSingleItem( player, HIGH_GRADE_LIZARD_SCALE, 1 )

            let [ itemId, amount ] = questRewards[ 4 ]
            await QuestHelper.rewardSingleItem( player, itemId, amount )

            let chance = _.random( 10 )
            if ( chance === 0 ) {
                return questRewards[ 0 ]
            }

            if ( chance < 5 ) {
                return questRewards[ 1 ]
            }

            if ( chance < 8 ) {
                return questRewards[ 2 ]
            }

            return questRewards[ 3 ]
        }

        if ( QuestHelper.hasQuestItem( player, MIDDLE_GRADE_LIZARD_SCALE ) ) {
            await QuestHelper.takeSingleItem( player, MIDDLE_GRADE_LIZARD_SCALE, 1 )
            let chance = _.random( 10 )
            if ( chance === 0 ) {
                return questRewards[ 0 ]
            }

            if ( chance < 4 ) {
                return questRewards[ 1 ]
            }

            if ( chance < 6 ) {
                return questRewards[ 2 ]
            }

            if ( chance < 7 ) {
                return questRewards[ 3 ]
            }

            if ( chance < 9 ) {
                return questRewards[ 4 ]
            }

            return questRewards[ 5 ]
        }
    }
}