import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'

import _ from 'lodash'

const RUKAIN = 30577
const KASHA_WOLF_FANG = 1473
const KASHA_WOLF = 20475
const HEALING_POTION = 1061
const NECKLACE_OF_COURAGE = 1506
const NECKLACE_OF_VALOR = 1507
const minimumLevel = 4

export class ProofOfValor extends ListenerLogic {
    constructor() {
        super( 'Q00271_ProofOfValor', 'listeners/tracked-200/ProofOfValor.ts' )
        this.questId = 271
        this.questItemIds = [ KASHA_WOLF_FANG ]
    }

    getAttackableKillIds(): Array<number> {
        return [ KASHA_WOLF ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00271_ProofOfValor'
    }

    getQuestStartIds(): Array<number> {
        return [ RUKAIN ]
    }

    getTalkIds(): Array<number> {
        return [ RUKAIN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isCondition( 1 ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let currentAmount: number = QuestHelper.getQuestItemsCount( player, KASHA_WOLF_FANG )
        let totalAmount: number = ( _.random( 100 ) < QuestHelper.getAdjustedChance( KASHA_WOLF_FANG, 25, data.isChampion )
                && currentAmount < 49 ) ? 2 : 1

        await QuestHelper.rewardSingleQuestItem( player, KASHA_WOLF_FANG, totalAmount, data.isChampion )

        if ( QuestHelper.getQuestItemsCount( player, KASHA_WOLF_FANG ) >= 50 ) {
            state.setConditionWithSound( 2, true )
            return
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || data.eventName !== '30577-04.htm' ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        state.startQuest()

        if ( QuestHelper.hasAtLeastOneQuestItem( player, NECKLACE_OF_VALOR, NECKLACE_OF_COURAGE ) ) {
            return this.getPath( '30577-08.html' )
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getRace() !== Race.ORC ) {
                    return this.getPath( '30577-01.htm' )
                }

                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '30577-02.htm' )
                }

                return this.getPath( QuestHelper.hasAtLeastOneQuestItem( player, NECKLACE_OF_VALOR, NECKLACE_OF_COURAGE ) ? '30577-07.htm' : '30577-03.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '30577-05.html' )

                    case 2:
                        if ( QuestHelper.getQuestItemsCount( player, KASHA_WOLF_FANG ) < 50 ) {
                            return this.getPath( '30577-05.html' )
                        }

                        if ( _.random( 100 ) <= 13 ) {
                            await QuestHelper.rewardSingleItem( player, NECKLACE_OF_VALOR, 1 )
                            await QuestHelper.rewardSingleItem( player, HEALING_POTION, 10 )
                        } else {
                            await QuestHelper.rewardSingleItem( player, NECKLACE_OF_COURAGE, 1 )
                        }

                        await QuestHelper.takeSingleItem( player, KASHA_WOLF_FANG, -1 )
                        await state.exitQuest( true, true )

                        return this.getPath( '30577-06.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}