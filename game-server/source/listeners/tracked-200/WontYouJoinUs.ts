import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'
const ATHENIA = 32643

const WASTE_LANDFILL_MACHINE = 18805
const SUPPRESSOR = 22656
const EXTERMINATOR = 22657

const SUPPORT_CERTIFICATE = 14866
const DESTROYED_MACHINE_PIECE = 14869
const ENCHANTED_GOLEM_FRAGMENT = 14870

const ENCHANTED_GOLEM_FRAGMENT_NEEDED = 20
const DESTROYED_MACHINE_PIECE_NEEDED = 10
const CHANCE_FOR_FRAGMENT = 80
const minimumLevel = 82

export class WontYouJoinUs extends ListenerLogic {
    constructor() {
        super( 'Q00239_WontYouJoinUs', 'listeners/tracked-200/WontYouJoinUs.ts' )
        this.questId = 239
        this.questItemIds = [ DESTROYED_MACHINE_PIECE, ENCHANTED_GOLEM_FRAGMENT ]
    }

    getAttackableKillIds(): Array<number> {
        return [ WASTE_LANDFILL_MACHINE, SUPPRESSOR, EXTERMINATOR ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00239_WontYouJoinUs'
    }

    getQuestStartIds(): Array<number> {
        return [ ATHENIA ]
    }

    getTalkIds(): Array<number> {
        return [ ATHENIA ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( data.npcId === WASTE_LANDFILL_MACHINE ) {
            await this.tryToProgressQuest( data, DESTROYED_MACHINE_PIECE, DESTROYED_MACHINE_PIECE_NEEDED, 1, 2 )
            return
        }

        if ( _.random( 100 ) >= QuestHelper.getAdjustedChance( ENCHANTED_GOLEM_FRAGMENT, CHANCE_FOR_FRAGMENT, data.isChampion ) ) {
            return
        }

        await this.tryToProgressQuest( data, ENCHANTED_GOLEM_FRAGMENT, ENCHANTED_GOLEM_FRAGMENT_NEEDED, 3, 4 )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '32643-02.htm':
                break

            case '32643-03.html':
                state.startQuest()
                break

            case '32643-07.html':
                if ( state.isCondition( 2 ) ) {
                    state.setConditionWithSound( 3, true )
                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.COMPLETED:
                return this.getPath( '32643-11.html' )

            case QuestStateValues.CREATED:
                if ( !player.hasQuestCompleted( 'Q00237_WindsOfChange' ) || player.getLevel() < minimumLevel ) {
                    return this.getPath( '32643-00.html' )
                }

                if ( !QuestHelper.hasQuestItem( player, SUPPORT_CERTIFICATE ) ) {
                    return this.getPath( '32643-12.html' )
                }

                return this.getPath( '32643-01.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( QuestHelper.hasQuestItem( player, DESTROYED_MACHINE_PIECE ) ? '32643-05.html' : '32643-04.html' )

                    case 2:
                        if ( QuestHelper.getQuestItemsCount( player, DESTROYED_MACHINE_PIECE ) >= DESTROYED_MACHINE_PIECE_NEEDED ) {
                            await QuestHelper.takeSingleItem( player, DESTROYED_MACHINE_PIECE, -1 )

                            return this.getPath( '32643-06.html' )
                        }

                        break

                    case 3:
                        return this.getPath( QuestHelper.hasQuestItem( player, ENCHANTED_GOLEM_FRAGMENT ) ? '32643-08.html' : '32643-09.html' )

                    case 4:
                        if ( QuestHelper.getQuestItemsCount( player, ENCHANTED_GOLEM_FRAGMENT ) >= ENCHANTED_GOLEM_FRAGMENT_NEEDED ) {
                            await QuestHelper.giveAdena( player, 283346, true )
                            await QuestHelper.takeSingleItem( player, SUPPORT_CERTIFICATE, 1 )
                            await QuestHelper.addExpAndSp( player, 1319736, 103553 )
                            await state.exitQuest( false, true )

                            return this.getPath( '32643-10.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async tryToProgressQuest( data: AttackableKillEvent, itemId: number, maximumCount: number, previousCondition: number, nextCondition: number ): Promise<void> {
        let player: L2PcInstance = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), previousCondition )
        if ( !player ) {
            return
        }

        if ( QuestHelper.getQuestItemsCount( player, itemId ) < maximumCount ) {
            await QuestHelper.rewardSingleQuestItem( player, itemId, 1, data.isChampion )
        }

        if ( QuestHelper.getQuestItemsCount( player, itemId ) >= maximumCount ) {
            let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), this.getName(), false )
            state.setConditionWithSound( nextCondition, true )

            return
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }
}