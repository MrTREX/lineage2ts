import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'

import _ from 'lodash'

const TUNATUN = 31537
const SEL_MAHUM_MANE = 15531
const SEL_MAHUM_MANE_COUNT = 300
const minimumLevel = 82

export class HomeSecurity extends ListenerLogic {
    constructor() {
        super( 'Q00278_HomeSecurity', 'listeners/tracked-200/HomeSecurity.ts' )
        this.questId = 278
        this.questItemIds = [
            SEL_MAHUM_MANE,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            18905,
            18906,
            18907,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00278_HomeSecurity'
    }

    getQuestStartIds(): Array<number> {
        return [ TUNATUN ]
    }

    getTalkIds(): Array<number> {
        return [ TUNATUN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.npcId ) {
            case 18905: // Farm Ravager (Crazy)
                let maximumNumber : number = _.random( 1000 ) < QuestHelper.getAdjustedChance( SEL_MAHUM_MANE, 486, data.isChampion ) ? 7 : 6
                await QuestHelper.rewardSingleQuestItem( player, SEL_MAHUM_MANE, _.random( 1, maximumNumber ), data.isChampion )

                await this.tryToProgressQuest( player, state )
                return

            case 18906: // Farm Bandit
            case 18907: // Beast Devourer
                if ( 0.85 >= QuestHelper.getAdjustedChance( SEL_MAHUM_MANE, Math.random(), data.isChampion ) ) {
                    return
                }

                await QuestHelper.rewardSingleQuestItem( player, SEL_MAHUM_MANE, 1, data.isChampion )
                await this.tryToProgressQuest( player, state )
                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31537-02.htm':
                break

            case '31537-04.htm':
                state.startQuest()
                break

            case '31537-07.html':
                let [ itemId, amount ] = this.getRewardParameters()

                await QuestHelper.rewardSingleItem( player, itemId, amount )
                await state.exitQuest( true, true )

                return this.getPath( '31537-07.html' )

        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '31537-03.html' )
                }

                return this.getPath( '31537-01.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '31537-06.html' )

                    case 2:
                        if ( QuestHelper.getQuestItemsCount( player, SEL_MAHUM_MANE ) >= SEL_MAHUM_MANE_COUNT ) {
                            return this.getPath( '31537-05.html' )
                        }

                        return this.getPath( '31537-06.html' )
                }
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getRewardParameters(): [ number, number ] {
        let chanceValue = _.random( 100 )

        if ( chanceValue < 10 ) {
            return [ 960, 1 ]
        }

        if ( chanceValue < 19 ) {
            return [ 960, 2 ]
        }

        if ( chanceValue < 27 ) {
            return [ 960, 3 ]
        }

        if ( chanceValue < 34 ) {
            return [ 960, 4 ]
        }

        if ( chanceValue < 40 ) {
            return [ 960, 5 ]
        }

        if ( chanceValue < 45 ) {
            return [ 960, 6 ]
        }

        if ( chanceValue < 49 ) {
            return [ 960, 7 ]
        }

        if ( chanceValue < 52 ) {
            return [ 960, 8 ]
        }

        if ( chanceValue < 54 ) {
            return [ 960, 9 ]
        }

        if ( chanceValue < 55 ) {
            return [ 960, 10 ]
        }

        if ( chanceValue < 75 ) {
            return [ 9553, 1 ]
        }

        if ( chanceValue < 90 ) {
            return [ 9553, 2 ]
        }

        return [ 959, 1 ]
    }

    async tryToProgressQuest( player: L2PcInstance, state : QuestState ) : Promise<void> {
        if ( QuestHelper.getQuestItemsCount( player, SEL_MAHUM_MANE ) < SEL_MAHUM_MANE_COUNT ) {
            return player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        }

        state.setConditionWithSound( 2, true )
    }
}