import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { ExShowScreenMessage } from '../../gameService/packets/send/ExShowScreenMessage'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'
import { QuestVariables } from '../helpers/QuestVariables'
import { PacketHelper } from '../../gameService/packets/PacketVariables'

const ALSHUPES = 30222
const SPIDER_LEG = 1087
const minimumLevel = 15
const MAX_LEG_COUNT = 8
const message: Buffer = PacketHelper.preservePacket( ExShowScreenMessage.fromNpcMessageId( NpcStringIds.LAST_DUTY_COMPLETE_N_GO_FIND_THE_NEWBIE_GUIDE, 2, 5000, null ) )

export class CollectorsDream extends ListenerLogic {
    constructor() {
        super( 'Q00261_CollectorsDream', 'listeners/tracked-200/CollectorsDream.ts' )
        this.questId = 261
        this.questItemIds = [ SPIDER_LEG ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            20308, // Hook Spider
            20460, // Crimson Spider
            20466, // Pincer Spider
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00261_CollectorsDream'
    }

    getQuestStartIds(): Array<number> {
        return [ ALSHUPES ]
    }

    getTalkIds(): Array<number> {
        return [ ALSHUPES ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isCondition( 1 ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        await QuestHelper.rewardAndProgressState( player, state, SPIDER_LEG, 1, MAX_LEG_COUNT, data.isChampion, 2 )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || data.eventName !== '30222-03.htm' ) {
            return
        }

        state.startQuest()
        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30222-02.htm' : '30222-01.htm' )

            case QuestStateValues.STARTED: {
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '30222-04.html' )
                    case 2:
                        if ( QuestHelper.getQuestItemsCount( player, SPIDER_LEG ) < MAX_LEG_COUNT ) {
                            return this.getPath( '30222-04.html' )
                        }

                        await this.progressRewards( player )
                        await QuestHelper.giveAdena( player, 1000, true )
                        await QuestHelper.addExpAndSp( player, 2000, 0 )
                        await state.exitQuest( true, true )

                        return this.getPath( '30222-05.html' )
                }
                break
            }
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async progressRewards( player: L2PcInstance ) {
        let mission: number = PlayerVariablesManager.get( player.getObjectId(), QuestVariables.guideMission ) as number
        if ( !mission ) {
            PlayerVariablesManager.set( player.getObjectId(), QuestVariables.guideMission, 100000 )
            player.sendCopyData( message )
            return
        }

        if ( Math.floor( ( mission % 100000000 ) / 10000000 ) !== 1 ) {
            PlayerVariablesManager.set( player.getObjectId(), QuestVariables.guideMission, mission + 10000000 )
            player.sendCopyData( message )
        }
    }
}