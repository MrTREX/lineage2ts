import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'

import _ from 'lodash'

const KEEF = 30534
const BAT_FANG = 1491
const RING_OF_RACCOON = 1508
const minimumLevel = 10

const monsterRewardChances: { [ npcId: number ]: Array<number> } = {
    20370: [ 6, 3, 1, -1 ],
    20480: [ 5, 2, -1 ],
}

export class CovertBusiness extends ListenerLogic {
    constructor() {
        super( 'Q00294_CovertBusiness', 'listeners/tracked-200/CovertBusiness.ts' )
        this.questId = 294
        this.questItemIds = [ BAT_FANG ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00294_CovertBusiness'
    }

    getQuestStartIds(): Array<number> {
        return [ KEEF ]
    }

    getTalkIds(): Array<number> {
        return [ KEEF ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isCondition( 1 ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        let chance = QuestHelper.getAdjustedChance( BAT_FANG, _.random( 10 ), data.isChampion )
        let index = _.findIndex( monsterRewardChances[ data.npcId ], ( monsterChance: number ): boolean => {
            return chance > monsterChance
        } )

        await QuestHelper.rewardAndProgressState( player, state, BAT_FANG, index + 1, 100, data.isChampion, 2 )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || data.eventName !== '30534-03.htm' ) {
            return
        }

        state.startQuest()
        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getRace() !== Race.DWARF ) {
                    return this.getPath( '30534-00.htm' )
                }

                return this.getPath( player.getLevel() >= minimumLevel ? '30534-02.htm' : '30534-01.htm' )

            case QuestStateValues.STARTED:
                if ( !state.isCondition( 2 ) ) {
                    return this.getPath( '30534-04.html' )
                }

                await QuestHelper.addExpAndSp( player, 0, 600 )

                if ( QuestHelper.hasQuestItem( player, RING_OF_RACCOON ) ) {
                    await QuestHelper.giveAdena( player, 2400, true )
                    await state.exitQuest( true, true )

                    return this.getPath( '30534-06.html' )
                }

                await QuestHelper.rewardSingleItem( player, RING_OF_RACCOON, 1 )
                await state.exitQuest( true, true )

                return this.getPath( '30534-05.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}