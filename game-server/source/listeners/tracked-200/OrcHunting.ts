import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'
import { NewbieRewardsHelper } from '../helpers/NewbieRewardsHelper'

import _ from 'lodash'

const RAYEN = 30221
const ORC_AMULET = 1114
const ORC_NECKLACE = 1115
const rewardMap = {
    20468: ORC_AMULET, // Kaboo Orc
    20469: ORC_AMULET, // Kaboo Orc Archer
    20470: ORC_AMULET, // Kaboo Orc Grunt
    20471: ORC_NECKLACE, // Kaboo Orc Fighter
    20472: ORC_NECKLACE, // Kaboo Orc Fighter Leader
    20473: ORC_NECKLACE, // Kaboo Orc Fighter Lieutenant
}
const minimumLevel = 6

export class OrcHunting extends ListenerLogic {
    constructor() {
        super( 'Q00260_OrcHunting', 'listeners/tracked-200/OrcHunting.ts' )
        this.questId = 260
        this.questItemIds = [ ORC_AMULET, ORC_NECKLACE ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( rewardMap ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00260_OrcHunting'
    }

    getQuestStartIds(): Array<number> {
        return [ RAYEN ]
    }

    getTalkIds(): Array<number> {
        return [ RAYEN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isStarted() || _.random( 10 ) <= 4 ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        await QuestHelper.rewardSingleQuestItem( player, rewardMap[ data.npcId ], 1, data.isChampion )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30221-04.htm':
                state.startQuest()
                break

            case '30221-07.html':
                await state.exitQuest( true, true )
                break

            case '30221-08.html':
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getRace() !== Race.ELF ) {
                    return this.getPath( '30221-01.html' )
                }

                return this.getPath( player.getLevel() >= minimumLevel ? '30221-03.htm' : '30221-02.html' )

            case QuestStateValues.STARTED:
                let amulets = QuestHelper.getQuestItemsCount( player, ORC_AMULET )
                let necklaces = QuestHelper.getQuestItemsCount( player, ORC_NECKLACE )
                if ( ( amulets + necklaces ) === 0 ) {
                    return this.getPath( '30221-05.html' )
                }

                const amount = ( amulets * 12 ) + ( necklaces * 30 ) + ( ( amulets + necklaces ) >= 10 ? 1000 : 0 )
                await QuestHelper.giveAdena( player, amount, true )
                await QuestHelper.takeMultipleItems( player, -1, ...this.questItemIds )
                await NewbieRewardsHelper.giveNewbieReward( player )

                return this.getPath( '30221-06.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}