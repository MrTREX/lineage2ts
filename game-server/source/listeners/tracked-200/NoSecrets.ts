import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const PINAPS = 30201
const DIARY = 15508
const TABLE = 15509

const mobGroupOne = [
    22783,
    22785,
    22780,
    22782,
    22784,
]

const mobGroupTwo = [
    22775,
    22776,
    22778,
]

const minimumLevel = 81

export class NoSecrets extends ListenerLogic {
    constructor() {
        super( 'Q00251_NoSecrets', 'listeners/tracked-200/NoSecrets.ts' )
        this.questId = 251
        this.questItemIds = [ DIARY, TABLE ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            ...mobGroupOne,
            ...mobGroupTwo,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00251_NoSecrets'
    }

    getQuestStartIds(): Array<number> {
        return [ PINAPS ]
    }

    getTalkIds(): Array<number> {
        return [ PINAPS ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || !state.isStarted() || !state.isCondition( 2 ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        if ( _.random( 100 ) < QuestHelper.getAdjustedChance( DIARY, 10, data.isChampion )
                && mobGroupOne.includes( data.npcId )
                && QuestHelper.getQuestItemsCount( player, DIARY ) < 10 ) {
            await this.tryToProgressQuest( player, state, DIARY, data.isChampion )
            return
        }

        if ( _.random( 100 ) < QuestHelper.getAdjustedChance( TABLE, 5, data.isChampion )
                && mobGroupTwo.includes( data.npcId )
                && QuestHelper.getQuestItemsCount( player, TABLE ) < 5 ) {
            await this.tryToProgressQuest( player, state, TABLE, data.isChampion )
            return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        if ( data.eventName === '30201-03.htm' ) {
            state.startQuest()
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() > minimumLevel ? '30201-01.htm' : '30201-00.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '30201-05.htm' )

                    case 2:
                        if ( QuestHelper.getQuestItemsCount( player, DIARY ) >= 10
                                && QuestHelper.getQuestItemsCount( player, TABLE ) >= 5 ) {

                            await QuestHelper.giveAdena( player, 313355, true )
                            await QuestHelper.addExpAndSp( player, 56787, 160578 )
                            await state.exitQuest( false, true )

                            return this.getPath( '30201-04.htm' )
                        }
                }

                break

            case QuestStateValues.COMPLETED:
                return this.getPath( '30201-06.htm' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async tryToProgressQuest( player: L2PcInstance, state: QuestState, itemId: number, isFromChampion: boolean ): Promise<void> {
        await QuestHelper.rewardSingleQuestItem( player, itemId, 1, isFromChampion )

        if ( QuestHelper.getQuestItemsCount( player, DIARY ) >= 10 && QuestHelper.getQuestItemsCount( player, TABLE ) >= 5 ) {
            state.setConditionWithSound( 2, true )
            return
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }
}