import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const ARIN = 30536
const MAGICAL_WEAVER = 20153
const FLOATING_STONE = 1492
const RING_OF_FIREFLY = 1509
const minimumLevel = 11

export class DreamingOfTheSkies extends ListenerLogic {
    constructor() {
        super( 'Q00295_DreamingOfTheSkies', 'listeners/tracked-200/DreamingOfTheSkies.ts' )
        this.questId = 295
        this.questItemIds = [
            FLOATING_STONE,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [ MAGICAL_WEAVER ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00295_DreamingOfTheSkies'
    }

    getQuestStartIds(): Array<number> {
        return [ ARIN ]
    }

    getTalkIds(): Array<number> {
        return [ ARIN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isCondition( 1 ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        let amount = QuestHelper.getAdjustedChance( FLOATING_STONE, _.random( 100 ), data.isChampion ) > 25 ? 1 : 2
        await QuestHelper.rewardAndProgressState( player, state, FLOATING_STONE, amount, 50, data.isChampion, 2 )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || data.eventName !== '30536-03.htm' ) {
            return
        }

        state.startQuest()
        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30536-02.htm' : '30536-01.htm' )

            case QuestStateValues.STARTED:
                if ( !state.isCondition( 2 ) ) {
                    return this.getPath( '30536-04.html' )
                }

                await QuestHelper.takeSingleItem( player, FLOATING_STONE, -1 )

                if ( QuestHelper.hasQuestItems( player, RING_OF_FIREFLY ) ) {
                    await QuestHelper.giveAdena( player, 2400, true )
                    await state.exitQuest( true, true )

                    return this.getPath( '30536-06.html' )
                }

                await QuestHelper.rewardSingleItem( player, RING_OF_FIREFLY, 1 )
                await state.exitQuest( true, true )

                return this.getPath( '30536-05.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}