import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableAttackedEvent, AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { PlayerRadarCache } from '../../gameService/cache/PlayerRadarCache'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'
import { QuestVariables } from '../helpers/QuestVariables'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'
import { CategoryType } from '../../gameService/enums/CategoryType'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const PREFECT_KASMAN = 30501
const PREFECT_VOKIAN = 30514
const SEER_MANAKIA = 30515
const FLAME_LORD_KAKAI = 30565
const SEER_TANAPI = 30571
const BREKA_CHIEF_VOLTAR = 30615
const ENKU_CHIEF_KEPRA = 30616
const TUREK_CHIEF_BURAI = 30617
const LEUNT_CHIEF_HARAK = 30618
const VUKU_CHIEF_DRIKO = 30619
const GANDI_CHIEF_CHIANTA = 30642

const VOKIANS_ORDER = 3204
const MANASHEN_SHARD = 3205
const TYRANT_TALON = 3206
const GUARDIAN_BASILISK_FANG = 3207
const VOKIANS_ORDER2 = 3208
const NECKLACE_OF_AUTHORITY = 3209
const CHIANTA_1ST_ORDER = 3210
const SCEPTER_OF_BREKA = 3211
const SCEPTER_OF_ENKU = 3212
const SCEPTER_OF_VUKU = 3213
const SCEPTER_OF_TUREK = 3214
const SCEPTER_OF_TUNATH = 3215
const CHIANTA_2ND_ORDER = 3216
const CHIANTA_3RD_ORDER = 3217
const TAMLIN_ORC_SKULL = 3218
const TIMAK_ORC_HEAD = 3219
const SCEPTER_BOX = 3220
const PASHIKAS_HEAD = 3221
const VULTUS_HEAD = 3222
const GLOVE_OF_VOLTAR = 3223
const ENKU_OVERLORD_HEAD = 3224
const GLOVE_OF_KEPRA = 3225
const MAKUM_BUGBEAR_HEAD = 3226
const GLOVE_OF_BURAI = 3227
const MANAKIA_1ST_LETTER = 3228
const MANAKIA_2ND_LETTER = 3229
const KASMANS_1ST_LETTER = 3230
const KASMANS_2ND_LETTER = 3231
const KASMANS_3RD_LETTER = 3232
const DRIKOS_CONTRACT = 3233
const STAKATO_DRONE_HUSK = 3234
const TANAPIS_ORDER = 3235
const SCEPTER_OF_TANTOS = 3236
const RITUAL_BOX = 3237

const MARK_OF_GLORY = 3203
const DIMENSIONAL_DIAMOND = 7562

const TYRANT = 20192
const TYRANT_KINGPIN = 20193
const MARSH_STAKATO_DRONE = 20234
const GUARDIAN_BASILISK = 20550
const MANASHEN_GARGOYLE = 20563
const TIMAK_ORC = 20583
const TIMAK_ORC_ARCHER = 20584
const TIMAK_ORC_SOLDIER = 20585
const TIMAK_ORC_WARRIOR = 20586
const TIMAK_ORC_SHAMAN = 20587
const TIMAK_ORC_OVERLORD = 20588
const TAMLIN_ORC = 20601
const TAMLIN_ORC_ARCHER = 20602
const RAGNA_ORC_OVERLORD = 20778
const RAGNA_ORC_SEER = 20779

const PASHIKA_SON_OF_VOLTAR = 27080
const VULTUS_SON_OF_VOLTAR = 27081
const ENKU_ORC_OVERLORD = 27082
const MAKUM_BUGBEAR_THUG = 27083
const REVENANT_OF_TANTOS_CHIEF = 27086

const minimumLevel = 37
const variableNames = {
    lastAttacker: 'a',
}

export class TestimonyOfGlory extends ListenerLogic {
    constructor() {
        super( 'Q00220_TestimonyOfGlory', 'listeners/tracked-200/TestimonyOfGlory.ts' )
        this.questId = 220
        this.questItemIds = [
            VOKIANS_ORDER,
            MANASHEN_SHARD,
            TYRANT_TALON,
            GUARDIAN_BASILISK_FANG,
            VOKIANS_ORDER2,
            NECKLACE_OF_AUTHORITY,
            CHIANTA_1ST_ORDER,
            SCEPTER_OF_BREKA,
            SCEPTER_OF_ENKU,
            SCEPTER_OF_VUKU,
            SCEPTER_OF_TUREK,
            SCEPTER_OF_TUNATH,
            CHIANTA_2ND_ORDER,
            CHIANTA_3RD_ORDER,
            TAMLIN_ORC_SKULL,
            TIMAK_ORC_HEAD,
            SCEPTER_BOX,
            PASHIKAS_HEAD,
            VULTUS_HEAD,
            GLOVE_OF_VOLTAR,
            ENKU_OVERLORD_HEAD,
            GLOVE_OF_KEPRA,
            MAKUM_BUGBEAR_HEAD,
            GLOVE_OF_BURAI,
            MANAKIA_1ST_LETTER,
            MANAKIA_2ND_LETTER,
            KASMANS_1ST_LETTER,
            KASMANS_2ND_LETTER,
            KASMANS_3RD_LETTER,
            DRIKOS_CONTRACT,
            STAKATO_DRONE_HUSK,
            TANAPIS_ORDER,
            SCEPTER_OF_TANTOS,
            RITUAL_BOX,
        ]
    }

    getAttackableAttackIds(): Array<number> {
        return [
            RAGNA_ORC_OVERLORD,
            RAGNA_ORC_SEER,
            REVENANT_OF_TANTOS_CHIEF,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            TYRANT,
            TYRANT_KINGPIN,
            MARSH_STAKATO_DRONE,
            GUARDIAN_BASILISK,
            MANASHEN_GARGOYLE,
            TIMAK_ORC,
            TIMAK_ORC_ARCHER,
            TIMAK_ORC_SOLDIER,
            TIMAK_ORC_WARRIOR,
            TIMAK_ORC_SHAMAN,
            TIMAK_ORC_OVERLORD,
            TAMLIN_ORC,
            TAMLIN_ORC_ARCHER,
            RAGNA_ORC_OVERLORD,
            RAGNA_ORC_SEER,
            PASHIKA_SON_OF_VOLTAR,
            VULTUS_SON_OF_VOLTAR,
            ENKU_ORC_OVERLORD,
            MAKUM_BUGBEAR_THUG,
            REVENANT_OF_TANTOS_CHIEF,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00220_TestimonyOfGlory'
    }

    getQuestStartIds(): Array<number> {
        return [ PREFECT_VOKIAN ]
    }

    getTalkIds(): Array<number> {
        return [
            PREFECT_VOKIAN,
            PREFECT_KASMAN,
            SEER_MANAKIA,
            FLAME_LORD_KAKAI,
            SEER_TANAPI,
            BREKA_CHIEF_VOLTAR,
            ENKU_CHIEF_KEPRA,
            TUREK_CHIEF_BURAI,
            LEUNT_CHIEF_HARAK,
            VUKU_CHIEF_DRIKO,
            GANDI_CHIEF_CHIANTA,
        ]
    }

    async onAttackableAttackedEvent( data: AttackableAttackedEvent ): Promise<void> {
        let state: QuestState = QuestStateCache.getQuestState( data.attackerPlayerId, this.getName() )

        if ( !state || !state.isStarted() ) {
            return
        }

        let value = NpcVariablesManager.get( data.targetId, this.getName() )

        switch ( data.targetNpcId ) {
            case RAGNA_ORC_OVERLORD:
            case RAGNA_ORC_SEER:
                if ( !value ) {
                    NpcVariablesManager.set( data.targetId, variableNames.lastAttacker, data.attackerPlayerId )

                    let player = L2World.getPlayer( data.attackerPlayerId )
                    if ( !QuestHelper.hasQuestItems( player, SCEPTER_OF_TANTOS ) ) {
                        let npc = L2World.getObjectById( data.targetId ) as L2Npc

                        BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.IS_IT_A_LACKEY_OF_KAKAI )
                        NpcVariablesManager.set( data.targetId, this.getName(), 1 )
                    }

                    return
                }

                if ( value === 1 ) {
                    NpcVariablesManager.set( data.targetId, this.getName(), 2 )
                }

                return

            case REVENANT_OF_TANTOS_CHIEF:
                if ( !value ) {
                    NpcVariablesManager.set( data.targetId, variableNames.lastAttacker, data.attackerPlayerId )

                    let player = L2World.getPlayer( data.attackerPlayerId )
                    if ( !QuestHelper.hasQuestItems( player, SCEPTER_OF_TANTOS ) ) {
                        let npc = L2World.getObjectById( data.targetId ) as L2Npc

                        BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.HOW_REGRETFUL_UNJUST_DISHONOR )
                        NpcVariablesManager.set( data.targetId, this.getName(), 1 )
                    }

                    return
                }

                if ( value === 1 ) {
                    let player = L2World.getPlayer( data.attackerPlayerId )
                    let npc = L2World.getObjectById( data.targetId ) as L2Npc

                    if ( !QuestHelper.hasQuestItems( player, SCEPTER_OF_TANTOS ) && ( npc.getCurrentHp() < ( npc.getMaxHp() / 3 ) ) ) {
                        BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.INDIGNANT_AND_UNFAIR_DEATH )
                        NpcVariablesManager.set( data.targetId, this.getName(), 2 )
                    }
                }

                return
        }
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case TYRANT:
            case TYRANT_KINGPIN:
                if ( QuestHelper.hasQuestItem( player, VOKIANS_ORDER ) && QuestHelper.getQuestItemsCount( player, TYRANT_TALON ) < 10 ) {
                    await QuestHelper.rewardSingleQuestItem( player, TYRANT_TALON, 1, data.isChampion )
                    this.tryToProgressQuest( player, state )
                }

                return

            case MARSH_STAKATO_DRONE:
                if ( !QuestHelper.hasQuestItem( player, SCEPTER_OF_VUKU )
                        && QuestHelper.hasQuestItems( player, NECKLACE_OF_AUTHORITY, CHIANTA_1ST_ORDER, DRIKOS_CONTRACT )
                        && QuestHelper.getQuestItemsCount( player, STAKATO_DRONE_HUSK ) < 30 ) {
                    await QuestHelper.rewardSingleQuestItem( player, STAKATO_DRONE_HUSK, 1, data.isChampion )

                    if ( QuestHelper.getQuestItemsCount( player, TYRANT_TALON ) >= 30 ) {
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case GUARDIAN_BASILISK:
                if ( QuestHelper.hasQuestItem( player, VOKIANS_ORDER )
                        && QuestHelper.getQuestItemsCount( player, GUARDIAN_BASILISK_FANG ) < 10 ) {
                    await QuestHelper.rewardSingleQuestItem( player, GUARDIAN_BASILISK_FANG, 1, data.isChampion )
                    this.tryToProgressQuest( player, state )
                }

                return

            case MANASHEN_GARGOYLE:
                if ( QuestHelper.hasQuestItem( player, VOKIANS_ORDER ) && QuestHelper.getQuestItemsCount( player, MANASHEN_SHARD ) < 10 ) {
                    await QuestHelper.rewardSingleQuestItem( player, MANASHEN_SHARD, 1, data.isChampion )
                    this.tryToProgressQuest( player, state )
                }

                return

            case TIMAK_ORC:
            case TIMAK_ORC_ARCHER:
            case TIMAK_ORC_SOLDIER:
            case TIMAK_ORC_WARRIOR:
            case TIMAK_ORC_SHAMAN:
            case TIMAK_ORC_OVERLORD:
                if ( QuestHelper.hasQuestItems( player, NECKLACE_OF_AUTHORITY, CHIANTA_3RD_ORDER )
                        && QuestHelper.getQuestItemsCount( player, TIMAK_ORC_HEAD ) < 20 ) {
                    await QuestHelper.rewardSingleQuestItem( player, TIMAK_ORC_HEAD, 1, data.isChampion )

                    if ( QuestHelper.getQuestItemsCount( player, MANASHEN_SHARD ) >= 20 ) {
                        if ( QuestHelper.getQuestItemsCount( player, TAMLIN_ORC_SKULL ) >= 20 ) {
                            state.setConditionWithSound( 7 )
                        }

                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case TAMLIN_ORC:
            case TAMLIN_ORC_ARCHER:
                if ( QuestHelper.hasQuestItems( player, NECKLACE_OF_AUTHORITY, CHIANTA_3RD_ORDER )
                        && QuestHelper.getQuestItemsCount( player, TAMLIN_ORC_SKULL ) < 20 ) {
                    await QuestHelper.rewardSingleQuestItem( player, TAMLIN_ORC_SKULL, 1, data.isChampion )

                    if ( QuestHelper.getQuestItemsCount( player, TAMLIN_ORC_SKULL ) >= 20 ) {
                        if ( QuestHelper.getQuestItemsCount( player, TIMAK_ORC_HEAD ) >= 20 ) {
                            state.setConditionWithSound( 7 )
                        }

                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case RAGNA_ORC_OVERLORD:
            case RAGNA_ORC_SEER:
                if ( QuestHelper.hasQuestItem( player, TANAPIS_ORDER ) && !QuestHelper.hasQuestItem( player, SCEPTER_OF_TANTOS ) ) {
                    await QuestHelper.addSpawnAtLocation( REVENANT_OF_TANTOS_CHIEF, npc, true, 200000 )
                    BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.TOO_LATE )
                }

                return

            case PASHIKA_SON_OF_VOLTAR:
                if ( QuestHelper.hasQuestItems( player, NECKLACE_OF_AUTHORITY, CHIANTA_1ST_ORDER, GLOVE_OF_VOLTAR )
                        && !QuestHelper.hasQuestItem( player, PASHIKAS_HEAD ) ) {
                    await QuestHelper.giveSingleItem( player, PASHIKAS_HEAD, 1 )
                    if ( QuestHelper.hasQuestItem( player, VULTUS_HEAD ) ) {
                        await QuestHelper.takeSingleItem( player, GLOVE_OF_VOLTAR, 1 )

                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case VULTUS_SON_OF_VOLTAR:
                if ( QuestHelper.hasQuestItems( player, NECKLACE_OF_AUTHORITY, CHIANTA_1ST_ORDER, GLOVE_OF_VOLTAR )
                        && !QuestHelper.hasQuestItem( player, VULTUS_HEAD ) ) {
                    await QuestHelper.giveSingleItem( player, VULTUS_HEAD, 1 )

                    if ( QuestHelper.hasQuestItem( player, PASHIKAS_HEAD ) ) {
                        await QuestHelper.takeSingleItem( player, GLOVE_OF_VOLTAR, 1 )
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case ENKU_ORC_OVERLORD:
                if ( QuestHelper.hasQuestItems( player, NECKLACE_OF_AUTHORITY, CHIANTA_1ST_ORDER, GLOVE_OF_KEPRA )
                        && QuestHelper.getQuestItemsCount( player, ENKU_OVERLORD_HEAD ) < 4 ) {
                    await QuestHelper.rewardSingleQuestItem( player, ENKU_OVERLORD_HEAD, 1, data.isChampion )

                    if ( QuestHelper.getQuestItemsCount( player, ENKU_OVERLORD_HEAD ) >= 4 ) {
                        await QuestHelper.takeSingleItem( player, GLOVE_OF_KEPRA, 1 )
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case MAKUM_BUGBEAR_THUG:
                if ( QuestHelper.hasQuestItems( player, NECKLACE_OF_AUTHORITY, CHIANTA_1ST_ORDER, GLOVE_OF_BURAI )
                        && ( QuestHelper.getQuestItemsCount( player, MAKUM_BUGBEAR_HEAD ) < 2 ) ) {
                    await QuestHelper.giveSingleItem( player, MAKUM_BUGBEAR_HEAD, 1 )

                    if ( QuestHelper.hasQuestItem( player, MAKUM_BUGBEAR_HEAD ) ) {
                        await QuestHelper.takeSingleItem( player, GLOVE_OF_BURAI, 1 )

                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case REVENANT_OF_TANTOS_CHIEF:
                if ( QuestHelper.hasQuestItem( player, TANAPIS_ORDER ) && !QuestHelper.hasQuestItem( player, SCEPTER_OF_TANTOS ) ) {
                    await QuestHelper.giveSingleItem( player, SCEPTER_OF_TANTOS, 1 )

                    BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.ILL_GET_REVENGE_SOMEDAY )
                    state.setConditionWithSound( 10, true )
                }

                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( state.isCreated() ) {
                    state.startQuest()
                    await QuestHelper.giveSingleItem( player, VOKIANS_ORDER, 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                    if ( !PlayerVariablesManager.get( data.playerId, QuestVariables.secondClassDiamondReward ) ) {
                        PlayerVariablesManager.set( player.getObjectId(), QuestVariables.secondClassDiamondReward, true )

                        await QuestHelper.rewardSingleItem( player, DIMENSIONAL_DIAMOND, 109 )

                        return this.getPath( '30514-05a.htm' )
                    }

                    return this.getPath( '30514-05.htm' )
                }

                return

            case '30514-04.htm':
            case '30514-07.html':
            case '30571-02.html':
            case '30615-03.html':
            case '30616-03.html':
            case '30642-02.html':
            case '30642-06.html':
            case '30642-08.html':
                break

            case '30501-02.html':
                if ( QuestHelper.hasQuestItem( player, SCEPTER_OF_VUKU ) ) {
                    break
                }

                if ( !QuestHelper.hasAtLeastOneQuestItem( player, SCEPTER_OF_VUKU, KASMANS_1ST_LETTER ) ) {
                    await QuestHelper.giveSingleItem( player, KASMANS_1ST_LETTER, 1 )
                    PlayerRadarCache.getRadar( data.playerId ).addMarker( -2150, 124443, -3724 )

                    return this.getPath( '30501-03.html' )
                }

                if ( !QuestHelper.hasQuestItem( player, SCEPTER_OF_VUKU ) && QuestHelper.hasAtLeastOneQuestItem( player, KASMANS_1ST_LETTER, DRIKOS_CONTRACT ) ) {
                    PlayerRadarCache.getRadar( data.playerId ).addMarker( -2150, 124443, -3724 )

                    return this.getPath( '30501-04.html' )
                }

                return

            case '30501-05.html':
                if ( QuestHelper.hasQuestItem( player, SCEPTER_OF_TUREK ) ) {
                    break
                }

                if ( !QuestHelper.hasAtLeastOneQuestItem( player, SCEPTER_OF_TUREK, KASMANS_2ND_LETTER ) ) {
                    await QuestHelper.giveSingleItem( player, KASMANS_2ND_LETTER, 1 )
                    PlayerRadarCache.getRadar( data.playerId ).addMarker( -94294, 110818, -3563 )

                    return this.getPath( '30501-06.html' )
                }

                if ( !QuestHelper.hasQuestItem( player, SCEPTER_OF_TUREK ) && QuestHelper.hasQuestItem( player, KASMANS_2ND_LETTER ) ) {
                    PlayerRadarCache.getRadar( data.playerId ).addMarker( -94294, 110818, -3563 )

                    return this.getPath( '30501-07.html' )
                }

                return

            case '30501-08.html':
                if ( QuestHelper.hasQuestItem( player, SCEPTER_OF_TUNATH ) ) {
                    break
                }

                if ( !QuestHelper.hasAtLeastOneQuestItem( player, SCEPTER_OF_TUNATH, KASMANS_3RD_LETTER ) ) {
                    await QuestHelper.giveSingleItem( player, KASMANS_3RD_LETTER, 1 )
                    PlayerRadarCache.getRadar( data.playerId ).addMarker( -55217, 200628, -3724 )
                    return this.getPath( '30501-09.html' )
                }

                if ( !QuestHelper.hasQuestItem( player, SCEPTER_OF_TUNATH ) && QuestHelper.hasQuestItem( player, KASMANS_3RD_LETTER ) ) {
                    PlayerRadarCache.getRadar( data.playerId ).addMarker( -55217, 200628, -3724 )
                    return this.getPath( '30501-10.html' )
                }

                return

            case '30515-04.html':
                if ( !QuestHelper.hasQuestItem( player, SCEPTER_OF_BREKA ) && QuestHelper.hasQuestItem( player, MANAKIA_1ST_LETTER ) ) {
                    PlayerRadarCache.getRadar( data.playerId ).addMarker( 80100, 119991, -2264 )
                    break
                }

                if ( QuestHelper.hasQuestItem( player, SCEPTER_OF_BREKA ) ) {
                    return this.getPath( '30515-02.html' )
                }

                if ( !QuestHelper.hasAtLeastOneQuestItem( player, SCEPTER_OF_BREKA, MANAKIA_1ST_LETTER ) ) {
                    await QuestHelper.giveSingleItem( player, MANAKIA_1ST_LETTER, 1 )
                    PlayerRadarCache.getRadar( data.playerId ).addMarker( 80100, 119991, -2264 )

                    return this.getPath( '30515-03.html' )
                }

                return

            case '30515-05.html':
                if ( QuestHelper.hasQuestItem( player, SCEPTER_OF_ENKU ) ) {
                    break
                }

                if ( !QuestHelper.hasAtLeastOneQuestItem( player, SCEPTER_OF_ENKU, MANAKIA_2ND_LETTER ) ) {
                    await QuestHelper.giveSingleItem( player, MANAKIA_2ND_LETTER, 1 )
                    PlayerRadarCache.getRadar( data.playerId ).addMarker( 12805, 189249, -3616 )

                    return this.getPath( '30515-06.html' )
                }

                if ( !QuestHelper.hasQuestItem( player, SCEPTER_OF_ENKU ) && QuestHelper.hasQuestItem( player, MANAKIA_2ND_LETTER ) ) {
                    PlayerRadarCache.getRadar( data.playerId ).addMarker( 12805, 189249, -3616 )

                    return this.getPath( '30515-07.html' )
                }

                return

            case '30571-03.html':
                if ( QuestHelper.hasQuestItem( player, SCEPTER_BOX ) ) {
                    await QuestHelper.takeSingleItem( player, SCEPTER_BOX, 1 )
                    await QuestHelper.giveSingleItem( player, TANAPIS_ORDER, 1 )

                    state.setConditionWithSound( 9, true )
                    break
                }

                return

            case '30615-04.html':
                if ( QuestHelper.hasQuestItem( player, MANAKIA_1ST_LETTER ) ) {
                    await QuestHelper.giveSingleItem( player, GLOVE_OF_VOLTAR, 1 )
                    await QuestHelper.takeSingleItem( player, MANAKIA_1ST_LETTER, 1 )

                    let npc = L2World.getObjectById( data.characterId ) as L2Npc

                    QuestHelper.addAttackDesire( QuestHelper.addSummonedSpawnAtLocation( npc, VULTUS_SON_OF_VOLTAR, npc, true, 200000 ), player )
                    QuestHelper.addAttackDesire( QuestHelper.addSummonedSpawnAtLocation( npc, PASHIKA_SON_OF_VOLTAR, npc, true, 200000 ), player )

                    break
                }

                return

            case '30616-04.html':
                if ( QuestHelper.hasQuestItem( player, MANAKIA_2ND_LETTER ) ) {
                    await QuestHelper.giveSingleItem( player, GLOVE_OF_KEPRA, 1 )
                    await QuestHelper.takeSingleItem( player, MANAKIA_2ND_LETTER, 1 )

                    let npc = L2World.getObjectById( data.characterId ) as L2Npc

                    QuestHelper.addAttackDesire( QuestHelper.addSummonedSpawnAtLocation( npc, ENKU_ORC_OVERLORD, npc, true, 200000 ), player )
                    QuestHelper.addAttackDesire( QuestHelper.addSummonedSpawnAtLocation( npc, ENKU_ORC_OVERLORD, npc, true, 200000 ), player )
                    QuestHelper.addAttackDesire( QuestHelper.addSummonedSpawnAtLocation( npc, ENKU_ORC_OVERLORD, npc, true, 200000 ), player )
                    QuestHelper.addAttackDesire( QuestHelper.addSummonedSpawnAtLocation( npc, ENKU_ORC_OVERLORD, npc, true, 200000 ), player )

                    break
                }

                return

            case '30617-03.html':
                if ( QuestHelper.hasQuestItem( player, KASMANS_2ND_LETTER ) ) {
                    await QuestHelper.giveSingleItem( player, GLOVE_OF_BURAI, 1 )
                    await QuestHelper.takeSingleItem( player, KASMANS_2ND_LETTER, 1 )

                    let npc = L2World.getObjectById( data.characterId ) as L2Npc

                    QuestHelper.addAttackDesire( QuestHelper.addSummonedSpawnAtLocation( npc, MAKUM_BUGBEAR_THUG, npc, true, 200000 ), player )
                    QuestHelper.addAttackDesire( QuestHelper.addSummonedSpawnAtLocation( npc, MAKUM_BUGBEAR_THUG, npc, true, 200000 ), player )

                    break
                }

                return

            case '30618-03.html':
                if ( QuestHelper.hasQuestItem( player, KASMANS_3RD_LETTER ) ) {
                    await QuestHelper.giveSingleItem( player, SCEPTER_OF_TUNATH, 1 )
                    await QuestHelper.takeSingleItem( player, KASMANS_3RD_LETTER, 1 )

                    if ( QuestHelper.hasQuestItems( player, SCEPTER_OF_TUREK, SCEPTER_OF_ENKU, SCEPTER_OF_BREKA, SCEPTER_OF_VUKU ) ) {
                        state.setConditionWithSound( 5, true )
                    }

                    break
                }

                return

            case '30619-03.html':
                if ( QuestHelper.hasQuestItem( player, KASMANS_1ST_LETTER ) ) {
                    await QuestHelper.giveSingleItem( player, DRIKOS_CONTRACT, 1 )
                    await QuestHelper.takeSingleItem( player, KASMANS_1ST_LETTER, 1 )

                    break
                }

                return

            case '30642-03.html':
                if ( QuestHelper.hasQuestItem( player, VOKIANS_ORDER2 ) ) {
                    await QuestHelper.takeSingleItem( player, VOKIANS_ORDER2, 1 )
                    await QuestHelper.giveSingleItem( player, CHIANTA_1ST_ORDER, 1 )

                    state.setConditionWithSound( 4, true )
                    break
                }

                return

            case '30642-07.html':
                if ( QuestHelper.hasQuestItems( player, CHIANTA_1ST_ORDER, SCEPTER_OF_BREKA, SCEPTER_OF_VUKU, SCEPTER_OF_TUREK, SCEPTER_OF_TUNATH, SCEPTER_OF_ENKU ) ) {
                    await QuestHelper.takeMultipleItems( player, 1,
                            CHIANTA_1ST_ORDER,
                            SCEPTER_OF_BREKA,
                            SCEPTER_OF_ENKU,
                            SCEPTER_OF_VUKU,
                            SCEPTER_OF_TUREK,
                            SCEPTER_OF_TUNATH,
                            MANAKIA_1ST_LETTER,
                            MANAKIA_2ND_LETTER,
                            KASMANS_1ST_LETTER )
                    await QuestHelper.giveSingleItem( player, CHIANTA_3RD_ORDER, 1 )

                    state.setConditionWithSound( 6, true )
                    break
                }

                return


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === PREFECT_VOKIAN ) {
                    if ( player.getRace() !== Race.ORC ) {
                        return this.getPath( '30514-01.html' )
                    }

                    if ( player.getLevel() < minimumLevel ) {
                        return this.getPath( '30514-02.html' )
                    }

                    if ( player.isInCategory( CategoryType.ORC_2ND_GROUP ) ) {
                        return this.getPath( '30514-03.htm' )
                    }

                    return this.getPath( '30514-01a.html' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case PREFECT_VOKIAN:
                        if ( QuestHelper.hasQuestItem( player, VOKIANS_ORDER ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, MANASHEN_SHARD ) >= 10
                                    && QuestHelper.getQuestItemsCount( player, TYRANT_TALON ) >= 10
                                    && QuestHelper.getQuestItemsCount( player, GUARDIAN_BASILISK_FANG ) >= 10 ) {

                                await QuestHelper.takeMultipleItems( player, -1, VOKIANS_ORDER, MANASHEN_SHARD, TYRANT_TALON, GUARDIAN_BASILISK_FANG )
                                await QuestHelper.giveMultipleItems( player, 1, VOKIANS_ORDER2, NECKLACE_OF_AUTHORITY )

                                state.setConditionWithSound( 3, true )
                                return this.getPath( '30514-08.html' )
                            }

                            return this.getPath( '30514-06.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, VOKIANS_ORDER2, NECKLACE_OF_AUTHORITY ) ) {
                            return this.getPath( '30514-09.html' )
                        }

                        if ( !QuestHelper.hasQuestItem( player, NECKLACE_OF_AUTHORITY )
                                && QuestHelper.hasAtLeastOneQuestItem( player, VOKIANS_ORDER2, SCEPTER_BOX ) ) {
                            return this.getPath( '30514-10.html' )
                        }

                        break

                    case PREFECT_KASMAN:
                        if ( QuestHelper.hasQuestItems( player, NECKLACE_OF_AUTHORITY, CHIANTA_1ST_ORDER ) ) {
                            return this.getPath( '30501-01.html' )
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, CHIANTA_2ND_ORDER, CHIANTA_3RD_ORDER, SCEPTER_BOX ) ) {
                            return this.getPath( '30501-11.html' )
                        }

                        break

                    case SEER_MANAKIA:
                        if ( QuestHelper.hasQuestItems( player, NECKLACE_OF_AUTHORITY, CHIANTA_1ST_ORDER ) ) {
                            return this.getPath( '30515-01.html' )
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, CHIANTA_2ND_ORDER, CHIANTA_3RD_ORDER, SCEPTER_BOX ) ) {
                            return this.getPath( '30515-08.html' )
                        }

                        break

                    case FLAME_LORD_KAKAI:
                        if ( !QuestHelper.hasQuestItem( player, RITUAL_BOX )
                                && QuestHelper.hasAtLeastOneQuestItem( player, SCEPTER_BOX, TANAPIS_ORDER ) ) {
                            return this.getPath( '30565-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, RITUAL_BOX ) ) {
                            await QuestHelper.giveAdena( player, 262720, true )
                            await QuestHelper.giveSingleItem( player, MARK_OF_GLORY, 1 )
                            await QuestHelper.addExpAndSp( player, 1448226, 96648 )
                            await state.exitQuest( false, true )

                            player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )
                            return this.getPath( '30565-02.html' )
                        }

                        break

                    case SEER_TANAPI:
                        if ( QuestHelper.hasQuestItem( player, SCEPTER_BOX ) ) {
                            return this.getPath( '30571-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, TANAPIS_ORDER ) ) {
                            if ( !QuestHelper.hasQuestItem( player, SCEPTER_OF_TANTOS ) ) {
                                return this.getPath( '30571-04.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, 1, TANAPIS_ORDER, SCEPTER_OF_TANTOS )
                            await QuestHelper.giveSingleItem( player, RITUAL_BOX, 1 )

                            state.setConditionWithSound( 11, true )
                            return this.getPath( '30571-05.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, RITUAL_BOX ) ) {
                            return this.getPath( '30571-06.html' )
                        }

                        break

                    case BREKA_CHIEF_VOLTAR:
                        if ( QuestHelper.hasQuestItems( player, NECKLACE_OF_AUTHORITY, CHIANTA_1ST_ORDER ) ) {
                            if ( !QuestHelper.hasAtLeastOneQuestItem( player, SCEPTER_OF_BREKA, MANAKIA_1ST_LETTER, GLOVE_OF_VOLTAR, PASHIKAS_HEAD, VULTUS_HEAD ) ) {
                                return this.getPath( '30615-01.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, MANAKIA_1ST_LETTER ) ) {
                                PlayerRadarCache.getRadar( data.playerId ).removeMarker( 80100, 119991, -2264 )
                                return this.getPath( '30615-02.html' )
                            }

                            if ( !QuestHelper.hasQuestItem( player, SCEPTER_OF_BREKA ) && QuestHelper.hasQuestItem( player, GLOVE_OF_VOLTAR ) && ( ( QuestHelper.getQuestItemsCount( player, PASHIKAS_HEAD ) + QuestHelper.getQuestItemsCount( player, VULTUS_HEAD ) ) < 2 ) ) {
                                let npc = L2World.getObjectById( data.characterId ) as L2Npc
                                if ( npc.getSummonedNpcCount() < 2 ) {
                                    QuestHelper.addAttackDesire( QuestHelper.addSummonedSpawnAtLocation( npc, PASHIKA_SON_OF_VOLTAR, npc, true, 200000 ), player )
                                    QuestHelper.addAttackDesire( QuestHelper.addSummonedSpawnAtLocation( npc, VULTUS_SON_OF_VOLTAR, npc, true, 200000 ), player )
                                }

                                return this.getPath( '30615-05.html' )
                            }

                            if ( QuestHelper.hasQuestItems( player, PASHIKAS_HEAD, VULTUS_HEAD ) ) {
                                await QuestHelper.giveSingleItem( player, SCEPTER_OF_BREKA, 1 )
                                await QuestHelper.takeMultipleItems( player, 1, PASHIKAS_HEAD, VULTUS_HEAD )

                                if ( QuestHelper.hasQuestItems( player, SCEPTER_OF_ENKU, SCEPTER_OF_VUKU, SCEPTER_OF_TUREK, SCEPTER_OF_TUNATH ) ) {
                                    state.setConditionWithSound( 5, true )
                                }

                                return this.getPath( '30615-06.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, SCEPTER_OF_BREKA ) ) {
                                return this.getPath( '30615-07.html' )
                            }

                            break
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, CHIANTA_2ND_ORDER, CHIANTA_3RD_ORDER, SCEPTER_BOX ) ) {
                            return this.getPath( '30615-08.html' )
                        }

                        break

                    case ENKU_CHIEF_KEPRA:
                        if ( QuestHelper.hasQuestItems( player, NECKLACE_OF_AUTHORITY, CHIANTA_1ST_ORDER ) ) {
                            if ( !QuestHelper.hasAtLeastOneQuestItem( player, SCEPTER_OF_ENKU, MANAKIA_2ND_LETTER, GLOVE_OF_KEPRA ) && ( ( QuestHelper.getQuestItemsCount( player, ENKU_OVERLORD_HEAD ) ) < 4 ) ) {
                                return this.getPath( '30616-01.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, MANAKIA_2ND_LETTER ) ) {
                                PlayerRadarCache.getRadar( data.playerId ).removeMarker( 12805, 189249, -3616 )
                                return this.getPath( '30616-02.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, GLOVE_OF_KEPRA ) && ( ( QuestHelper.getQuestItemsCount( player, ENKU_OVERLORD_HEAD ) ) < 4 ) ) {
                                let npc = L2World.getObjectById( data.characterId ) as L2Npc
                                if ( npc.getSummonedNpcCount() < 5 ) {
                                    QuestHelper.addAttackDesire( QuestHelper.addSummonedSpawnAtLocation( npc, ENKU_ORC_OVERLORD, npc, true, 200000 ), player )
                                }

                                return this.getPath( '30616-05.html' )
                            }

                            if ( QuestHelper.getQuestItemsCount( player, ENKU_OVERLORD_HEAD ) >= 4 ) {
                                await QuestHelper.giveSingleItem( player, SCEPTER_OF_ENKU, 1 )
                                await QuestHelper.takeSingleItem( player, ENKU_OVERLORD_HEAD, -1 )

                                if ( QuestHelper.hasQuestItems( player, SCEPTER_OF_BREKA, SCEPTER_OF_VUKU, SCEPTER_OF_TUREK, SCEPTER_OF_TUNATH ) ) {
                                    state.setConditionWithSound( 5, true )
                                }

                                return this.getPath( '30616-06.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, SCEPTER_OF_ENKU ) ) {
                                return this.getPath( '30616-07.html' )
                            }

                            break
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, CHIANTA_2ND_ORDER, CHIANTA_3RD_ORDER, SCEPTER_BOX ) ) {
                            return this.getPath( '30616-08.html' )
                        }

                        break

                    case TUREK_CHIEF_BURAI:
                        if ( QuestHelper.hasQuestItems( player, NECKLACE_OF_AUTHORITY, CHIANTA_1ST_ORDER ) ) {
                            if ( !QuestHelper.hasAtLeastOneQuestItem( player, SCEPTER_OF_TUREK, KASMANS_2ND_LETTER, GLOVE_OF_BURAI, MAKUM_BUGBEAR_HEAD ) ) {
                                return this.getPath( '30617-01.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, KASMANS_2ND_LETTER ) ) {
                                PlayerRadarCache.getRadar( data.playerId ).removeMarker( -94294, 110818, -3563 )
                                return this.getPath( '30617-02.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, GLOVE_OF_BURAI ) ) {
                                let npc = L2World.getObjectById( data.characterId ) as L2Npc
                                if ( npc.getSummonedNpcCount() < 3 ) {
                                    QuestHelper.addAttackDesire( QuestHelper.addSummonedSpawnAtLocation( npc, MAKUM_BUGBEAR_THUG, npc, true, 200000 ), player )
                                    QuestHelper.addAttackDesire( QuestHelper.addSummonedSpawnAtLocation( npc, MAKUM_BUGBEAR_THUG, npc, true, 200000 ), player )
                                }

                                return this.getPath( '30617-04.html' )
                            }

                            if ( QuestHelper.getQuestItemsCount( player, MAKUM_BUGBEAR_HEAD ) >= 2 ) {
                                await QuestHelper.giveSingleItem( player, SCEPTER_OF_TUREK, 1 )
                                await QuestHelper.takeSingleItem( player, MAKUM_BUGBEAR_HEAD, -1 )

                                if ( QuestHelper.hasQuestItems( player, SCEPTER_OF_ENKU, SCEPTER_OF_BREKA, SCEPTER_OF_VUKU, SCEPTER_OF_TUNATH ) ) {
                                    state.setConditionWithSound( 5, true )
                                }

                                return this.getPath( '30617-05.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, SCEPTER_OF_TUREK ) ) {
                                return this.getPath( '30617-06.html' )
                            }

                            break
                        }

                        if ( QuestHelper.hasQuestItem( player, NECKLACE_OF_AUTHORITY ) && QuestHelper.hasAtLeastOneQuestItem( player, CHIANTA_2ND_ORDER, CHIANTA_3RD_ORDER, SCEPTER_BOX ) ) {
                            return this.getPath( '30617-07.html' )
                        }

                        break

                    case LEUNT_CHIEF_HARAK:
                        if ( QuestHelper.hasQuestItems( player, NECKLACE_OF_AUTHORITY, CHIANTA_1ST_ORDER ) ) {
                            if ( !QuestHelper.hasAtLeastOneQuestItem( player, SCEPTER_OF_TUNATH, KASMANS_3RD_LETTER ) ) {
                                return this.getPath( '30618-01.html' )
                            }

                            if ( !QuestHelper.hasQuestItem( player, SCEPTER_OF_TUNATH )
                                    && QuestHelper.hasQuestItem( player, KASMANS_3RD_LETTER ) ) {
                                PlayerRadarCache.getRadar( data.playerId ).removeMarker( -55217, 200628, -3724 )

                                return this.getPath( '30618-02.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, SCEPTER_OF_TUNATH ) ) {
                                return this.getPath( '30618-04.html' )
                            }

                            break
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, CHIANTA_2ND_ORDER, CHIANTA_3RD_ORDER, SCEPTER_BOX ) ) {
                            return this.getPath( '30618-05.html' )
                        }

                        break

                    case VUKU_CHIEF_DRIKO:
                        if ( QuestHelper.hasQuestItems( player, NECKLACE_OF_AUTHORITY, CHIANTA_1ST_ORDER ) ) {
                            if ( !QuestHelper.hasAtLeastOneQuestItem( player, SCEPTER_OF_VUKU, KASMANS_1ST_LETTER, DRIKOS_CONTRACT ) ) {
                                return this.getPath( '30619-01.html' )
                            }

                            if ( !QuestHelper.hasQuestItem( player, SCEPTER_OF_VUKU ) && QuestHelper.hasQuestItem( player, KASMANS_1ST_LETTER ) ) {
                                PlayerRadarCache.getRadar( data.playerId ).removeMarker( -2150, 124443, -3724 )
                                return this.getPath( '30619-02.html' )
                            }

                            if ( !QuestHelper.hasQuestItem( player, SCEPTER_OF_VUKU ) && QuestHelper.hasQuestItem( player, DRIKOS_CONTRACT ) ) {
                                if ( QuestHelper.getQuestItemsCount( player, STAKATO_DRONE_HUSK ) < 30 ) {
                                    return this.getPath( '30619-04.html' )
                                }

                                await QuestHelper.giveSingleItem( player, SCEPTER_OF_VUKU, 1 )
                                await QuestHelper.takeMultipleItems( player, -1, DRIKOS_CONTRACT, STAKATO_DRONE_HUSK )

                                if ( QuestHelper.hasQuestItems( player, SCEPTER_OF_TUREK, SCEPTER_OF_ENKU, SCEPTER_OF_BREKA, SCEPTER_OF_TUNATH ) ) {
                                    state.setConditionWithSound( 5, true )
                                }

                                return this.getPath( '30619-05.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, SCEPTER_OF_VUKU ) ) {
                                return this.getPath( '30619-06.html' )
                            }

                            break
                        }

                        if ( QuestHelper.hasQuestItem( player, NECKLACE_OF_AUTHORITY )
                                && QuestHelper.hasAtLeastOneQuestItem( player, CHIANTA_2ND_ORDER, CHIANTA_3RD_ORDER, SCEPTER_BOX ) ) {
                            return this.getPath( '30619-07.html' )
                        }

                        break

                    case GANDI_CHIEF_CHIANTA:
                        if ( QuestHelper.hasQuestItems( player, NECKLACE_OF_AUTHORITY, VOKIANS_ORDER2 ) ) {
                            return this.getPath( '30642-01.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, NECKLACE_OF_AUTHORITY, CHIANTA_1ST_ORDER ) ) {
                            let totalItems = QuestHelper.getQuestItemsCount( player, SCEPTER_OF_BREKA ) +
                                    QuestHelper.getQuestItemsCount( player, SCEPTER_OF_VUKU ) +
                                    QuestHelper.getQuestItemsCount( player, SCEPTER_OF_TUREK ) +
                                    QuestHelper.getQuestItemsCount( player, SCEPTER_OF_TUNATH ) +
                                    QuestHelper.getQuestItemsCount( player, SCEPTER_OF_ENKU )

                            if ( totalItems < 5 ) {
                                return this.getPath( '30642-04.html' )
                            }

                            if ( QuestHelper.hasQuestItems( player, SCEPTER_OF_BREKA, SCEPTER_OF_VUKU, SCEPTER_OF_TUREK, SCEPTER_OF_TUNATH, SCEPTER_OF_ENKU ) ) {
                                return this.getPath( '30642-05.html' )
                            }

                            break
                        }

                        if ( QuestHelper.hasQuestItems( player, NECKLACE_OF_AUTHORITY, CHIANTA_2ND_ORDER ) ) {
                            await QuestHelper.giveSingleItem( player, CHIANTA_3RD_ORDER, 1 )
                            await QuestHelper.takeSingleItem( player, CHIANTA_2ND_ORDER, 1 )

                            return this.getPath( '30642-09.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, NECKLACE_OF_AUTHORITY, CHIANTA_3RD_ORDER ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, TAMLIN_ORC_SKULL ) >= 20
                                    && QuestHelper.getQuestItemsCount( player, TIMAK_ORC_HEAD ) >= 20 ) {

                                await QuestHelper.takeMultipleItems( player, -1, NECKLACE_OF_AUTHORITY, CHIANTA_3RD_ORDER, TAMLIN_ORC_SKULL, TIMAK_ORC_HEAD )
                                await QuestHelper.giveSingleItem( player, SCEPTER_BOX, 1 )

                                state.setConditionWithSound( 8, true )
                                return this.getPath( '30642-11.html' )
                            }

                            return this.getPath( '30642-10.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, SCEPTER_BOX ) ) {
                            return this.getPath( '30642-12.html' )
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, TANAPIS_ORDER, RITUAL_BOX ) ) {
                            return this.getPath( '30642-13.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === PREFECT_VOKIAN ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    tryToProgressQuest( player: L2PcInstance, state: QuestState ): void {
        if ( QuestHelper.getQuestItemsCount( player, TYRANT_TALON ) >= 10 ) {
            if ( QuestHelper.getQuestItemsCount( player, MANASHEN_SHARD ) >= 10
                    && QuestHelper.getQuestItemsCount( player, GUARDIAN_BASILISK_FANG ) >= 10 ) {
                state.setConditionWithSound( 2 )
            }

            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
            return
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }
}