import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const WIRPHY = 30540
const WHINSTONE_GOLEM = 20521
const STARSTONE = 1573
const GATEKEEPER_TOKEN = 1659
const minimumLevel = 15
const STARSTONE_COUT = 20

export class GatekeepersFavor extends ListenerLogic {
    constructor() {
        super( 'Q00297_GatekeepersFavor', 'listeners/tracked-200/GatekeepersFavor.ts' )
        this.questId = 297
        this.questItemIds = [ STARSTONE ]
    }

    getAttackableKillIds(): Array<number> {
        return [ WHINSTONE_GOLEM ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00297_GatekeepersFavor'
    }

    getQuestStartIds(): Array<number> {
        return [ WIRPHY ]
    }

    getTalkIds(): Array<number> {
        return [ WIRPHY ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isCondition( 1 ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        await QuestHelper.rewardAndProgressState( player, state, STARSTONE, 1, STARSTONE_COUT, data.isChampion, 2 )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || data.eventName !== '30536-03.htm' ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        if ( player.getLevel() < minimumLevel ) {
            return this.getPath( '30540-01.htm' )
        }

        state.startQuest()
        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( '30540-02.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '30540-04.html' )

                    case 2:
                        if ( QuestHelper.getQuestItemsCount( player, STARSTONE ) < STARSTONE_COUT ) {
                            return this.getPath( '30540-04.html' )
                        }

                        await QuestHelper.rewardSingleItem( player, GATEKEEPER_TOKEN, 2 )
                        await state.exitQuest( true, true )

                        return this.getPath( '30540-05.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}