import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'

import _ from 'lodash'

const MARYSE_REDBONNET = 30553
const BLACK_WOLF_PELT = 1482
const requiredAmountOfPelts = 40
const BLACK_WOLF = 20317

const SCROLL_OF_ESCAPE = 736
const GRANDMAS_PEARL = 1502
const GRANDMAS_MIRROR = 1503
const GRANDMAS_NECKLACE = 1504
const GRANDMAS_HAIRPIN = 1505
const minimumLevel = 4

export class RevengeOfTheRedbonnet extends ListenerLogic {
    constructor() {
        super( 'Q00291_RevengeOfTheRedbonnet', 'listeners/tracked-200/RevengeOfTheRedbonnet.ts' )
        this.questId = 291
        this.questItemIds = [
            BLACK_WOLF_PELT,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [ BLACK_WOLF ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00291_RevengeOfTheRedbonnet'
    }

    getQuestStartIds(): Array<number> {
        return [ MARYSE_REDBONNET ]
    }

    getTalkIds(): Array<number> {
        return [ MARYSE_REDBONNET ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isCondition( 1 ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId )

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, BLACK_WOLF_PELT, 1, data.isChampion )
        if ( QuestHelper.getQuestItemsCount( player, BLACK_WOLF_PELT ) >= requiredAmountOfPelts ) {
            state.setConditionWithSound( 2, true )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || data.eventName !== '30553-03.htm' ) {
            return
        }

        state.startQuest()
        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30553-02.htm' : '30553-01.htm' )

            case QuestStateValues.STARTED:
                if ( !state.isCondition( 2 ) || !QuestHelper.hasQuestItem( player, BLACK_WOLF_PELT ) ) {
                    return this.getPath( '30553-04.html' )
                }

                await QuestHelper.takeSingleItem( player, BLACK_WOLF_PELT, -1 )
                await this.rewardItems( player )

                await state.exitQuest( true, true )
                return this.getPath( '30553-05.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async rewardItems( player: L2PcInstance ): Promise<void> {
        let chance = _.random( 100 )
        if ( chance <= 2 ) {
            return QuestHelper.rewardSingleItem( player, GRANDMAS_PEARL, 1 )
        }

        if ( chance <= 20 ) {
            return QuestHelper.rewardSingleItem( player, GRANDMAS_MIRROR, 1 )
        }

        if ( chance <= 45 ) {
            return QuestHelper.rewardSingleItem( player, GRANDMAS_NECKLACE, 1 )
        }

        await QuestHelper.rewardSingleItem( player, GRANDMAS_HAIRPIN, 1 )
        return QuestHelper.rewardSingleItem( player, SCROLL_OF_ESCAPE, 1 )
    }
}