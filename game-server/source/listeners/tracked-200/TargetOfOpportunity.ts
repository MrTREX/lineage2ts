import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const JERIAN = 32302
const minimumLevel = 82

const rewardItemIds = [
    15515,
    15516,
]

const monsterRewards = {
    22373: 15517,
    22374: 15518,
    22375: 15519,
    22376: 15520,
}

export class TargetOfOpportunity extends ListenerLogic {
    constructor() {
        super( 'Q00279_TargetOfOpportunity', 'listeners/tracked-200/TargetOfOpportunity.ts' )
        this.questId = 279
        this.questItemIds = _.values( monsterRewards )
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewards ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00279_TargetOfOpportunity'
    }

    getQuestStartIds(): Array<number> {
        return [ JERIAN ]
    }

    getTalkIds(): Array<number> {
        return [ JERIAN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 1 )
        if ( !player || QuestHelper.hasQuestItem( player, monsterRewards[ data.npcId ] ) ) {
            return
        }

        if ( _.random( 1000 ) >= QuestHelper.getAdjustedChance( monsterRewards[ data.npcId ], 311, data.isChampion ) ) {
            return
        }

        await QuestHelper.giveSingleItem( player, monsterRewards[ data.npcId ], 1 )
        if ( QuestHelper.hasQuestItems( player, ...this.questItemIds ) ) {
            let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), this.getName(), false )
            state.setConditionWithSound( 2, true )
            return
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32302-05.html':
                state.startQuest()
                break

            case '32302-08.html':
                if ( QuestHelper.hasQuestItems( player, ...this.questItemIds ) ) {
                    await QuestHelper.rewardMultipleItems( player, 1, ...rewardItemIds )
                    await state.exitQuest( true, true )
                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '32302-01.htm' : '32302-02.html' )

            case QuestStateValues.STARTED:
                return this.getPath( QuestHelper.hasQuestItems( player, ...this.questItemIds ) ? '32302-07.html' : '32302-06.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}