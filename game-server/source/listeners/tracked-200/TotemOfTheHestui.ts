import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { NewbieRewardsHelper } from '../helpers/NewbieRewardsHelper'
import { Race } from '../../gameService/enums/Race'

import _ from 'lodash'

const TANAPI = 30571
const KASHA_PARASITE = 1480
const KASHA_CRYSTAL = 1481
const KASHA_BEAR = 20479
const KASHA_BEAR_TOTEM = 27044
const minimumLevel = 15

type ChanceRange = [ number, number ] // minimum, maximum
const chanceRanges: Array<ChanceRange> = [
    [ 79, 100 ],
    [ 69, 20 ],
    [ 59, 15 ],
    [ 49, 10 ],
    [ 39, 2 ],
]

export class TotemOfTheHestui extends ListenerLogic {
    constructor() {
        super( 'Q00276_TotemOfTheHestui', 'listeners/tracked-200/TotemOfTheHestui.ts' )
        this.questId = 276
        this.questItemIds = [ KASHA_PARASITE, KASHA_CRYSTAL ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            KASHA_BEAR,
            KASHA_BEAR_TOTEM,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00276_TotemOfTheHestui'
    }

    getQuestStartIds(): Array<number> {
        return [ TANAPI ]
    }

    getTalkIds(): Array<number> {
        return [ TANAPI ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isStarted() || !state.isCondition( 1 ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case KASHA_BEAR:
                let itemCount = QuestHelper.getQuestItemsCount( player, KASHA_PARASITE )
                let chance = _.random( 100 )

                let shouldSpawnTotem: boolean = _.some( chanceRanges, ( range: ChanceRange ): boolean => {
                    return itemCount > range[ 0 ] && chance < QuestHelper.getAdjustedChance( KASHA_PARASITE, range[ 1 ], data.isChampion )
                } )

                if ( shouldSpawnTotem ) {
                    QuestHelper.addSpawnAtLocation( KASHA_BEAR_TOTEM, player )
                    await QuestHelper.takeSingleItem( player, KASHA_PARASITE, -1 )
                    return
                }

                await QuestHelper.rewardSingleQuestItem( player, KASHA_PARASITE, 1, data.isChampion )

                return

            case KASHA_BEAR_TOTEM:
                await QuestHelper.giveSingleItem( player, KASHA_CRYSTAL, 1 )
                state.setConditionWithSound( 2 )

                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || data.eventName !== '30571-03.htm' ) {
            return
        }

        state.startQuest()
        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getRace() !== Race.ORC ) {
                    return this.getPath( '30571-00.htm' )
                }

                return this.getPath( player.getLevel() >= minimumLevel ? '30571-02.htm' : '30571-01.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '30571-04.html' )

                    case 2:
                        if ( !QuestHelper.hasQuestItems( player, KASHA_CRYSTAL ) ) {
                            return this.getPath( '30571-04.html' )
                        }

                        await NewbieRewardsHelper.giveNewbieReward( player )
                        await QuestHelper.rewardMultipleItems( player, 1, 29, 1500 )
                        await state.exitQuest( true, true )

                        return this.getPath( '30571-05.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}