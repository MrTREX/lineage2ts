import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'
import aigle from 'aigle'
import { createItemDefinition, ItemDefinition } from '../../gameService/interface/ItemDefinition'

const PAINT = 30136
const WOLF_CLAW = 1367
const WOLF_CLAW_COUNT = 50
const minimumLevel = 3

type MonsterReward = [ number, number ] // chance, amount
const monsterRewardsMap: { [ npcId: number ]: Array<MonsterReward> } = {
    20003: [ [ 2, 25 ], [ 8, 50 ] ],
    20456: [ [ 1, 80 ], [ 2, 100 ] ],
}

type QuestReward = [ number, ...Array<ItemDefinition> ]
const questRewards: Array<QuestReward> = [
    [ 1, createItemDefinition( 4633, 1 ) ],
    [ 2, createItemDefinition( 57, 2000 ) ],
    [ 5, createItemDefinition( 5140, 1 ) ],
    [ 8, createItemDefinition( 735, 1 ), createItemDefinition( 57, 50 ) ],
    [ 11, createItemDefinition( 737, 1 ) ],
    [ 14, createItemDefinition( 734, 1 ) ],
    [ 17, createItemDefinition( 35, 1 ), createItemDefinition( 57, 50 ) ],
]

export class KeenClaws extends ListenerLogic {
    constructor() {
        super( 'Q00264_KeenClaws', 'listeners/tracked-200/KeenClaws.ts' )
        this.questId = 264
        this.questItemIds = [ WOLF_CLAW ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardsMap ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00264_KeenClaws'
    }

    getQuestStartIds(): Array<number> {
        return [ PAINT ]
    }

    getTalkIds(): Array<number> {
        return [ PAINT ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isCondition( 1 ) ) {
            return
        }

        let chanceValue = _.random( 100 )

        let reward: MonsterReward = _.find( monsterRewardsMap[ data.npcId ], ( item: MonsterReward ) => {
            return chanceValue < QuestHelper.getAdjustedChance( WOLF_CLAW, item[ 0 ], data.isChampion )
        } )

        if ( !reward ) {
            return
        }

        let [ , amount ] = reward
        let player = L2World.getPlayer( data.playerId )
        await QuestHelper.rewardSingleQuestItem( player, WOLF_CLAW, amount, data.isChampion )

        if ( QuestHelper.getQuestItemsCount( player, WOLF_CLAW ) >= WOLF_CLAW_COUNT ) {
            state.setConditionWithSound( 2, true )
            return
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || data.eventName !== '30136-03.htm' ) {
            return
        }

        state.startQuest()
        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30136-02.htm' : '30136-01.htm' )

            case QuestStateValues.STARTED: {
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '30136-04.html' )

                    case 2:
                        if ( QuestHelper.getQuestItemsCount( player, WOLF_CLAW ) < WOLF_CLAW_COUNT ) {
                            return this.getPath( '30136-04.html' )
                        }

                        let finalChance = _.random( 17 )
                        let reward: QuestReward = _.find( questRewards, ( value: QuestReward ): boolean => {
                            return finalChance < value[ 0 ]
                        } )

                        if ( !reward ) {
                            return
                        }

                        await aigle.resolve( reward.slice( 1 ) ).each( ( item: ItemDefinition ) : Promise<void> => {
                            return QuestHelper.rewardSingleItem( player, item.id, item.count )
                        } )

                        await state.exitQuest( true, true )
                        return this.getPath( '30136-05.html' )
                }

                break
            }
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}