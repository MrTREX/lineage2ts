import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'
import { QuestVariables } from '../helpers/QuestVariables'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import _ from 'lodash'

const PRIESTESS_VIVYAN = 30030
const TRADER_SARIEN = 30436
const SEER_RACOY = 30507
const SEER_SOMAK = 30510
const SEER_MANAKIA = 30515
const SHADOW_ORIM = 30630
const ANCESTOR_MARTANKUS = 30649
const SEER_PEKIRON = 30682

const VENDETTA_TOTEM = 2880
const TAMLIN_ORC_HEAD = 2881
const WARSPIRIT_TOTEM = 2882
const ORIMS_CONTRACT = 2883
const PORTAS_EYE = 2884
const EXCUROS_SCALE = 2885
const MORDEOS_TALON = 2886
const BRAKIS_REMAINS1 = 2887
const PEKIRONS_TOTEM = 2888
const TONARS_SKULL = 2889
const TONARS_RIB_BONE = 2890
const TONARS_SPINE = 2891
const TONARS_ARM_BONE = 2892
const TONARS_THIGH_BONE = 2893
const TONARS_REMAINS1 = 2894
const MANAKIAS_TOTEM = 2895
const HERMODTS_SKULL = 2896
const HERMODTS_RIB_BONE = 2897
const HERMODTS_SPINE = 2898
const HERMODTS_ARM_BONE = 2899
const HERMODTS_THIGH_BONE = 2900
const HERMODTS_REMAINS1 = 2901
const RACOYS_TOTEM = 2902
const VIVIANTES_LETTER = 2903
const INSECT_DIAGRAM_BOOK = 2904
const KIRUNAS_SKULL = 2905
const KIRUNAS_RIB_BONE = 2906
const KIRUNAS_SPINE = 2907
const KIRUNAS_ARM_BONE = 2908
const KIRUNAS_THIGH_BONE = 2909
const KIRUNAS_REMAINS1 = 2910
const BRAKIS_REMAINS2 = 2911
const TONARS_REMAINS2 = 2912
const HERMODTS_REMAINS2 = 2913
const KIRUNAS_REMAINS2 = 2914

const MARK_OF_WARSPIRIT = 2879
const DIMENSIONAL_DIAMOND = 7562

const NOBLE_ANT = 20089
const NOBLE_ANT_LEADER = 20090
const MEDUSA = 20158
const PORTA = 20213
const EXCURO = 20214
const MORDERO = 20215
const LETO_LIZARDMAN_SHAMAN = 20581
const LETO_LIZARDMAN_OVERLORD = 20582
const TAMLIN_ORC = 20601
const TAMLIN_ORC_ARCHER = 20602

const STENOA_GORGON_QUEEN = 27108
const minimumLevel = 39

export class TestOfTheWarSpirit extends ListenerLogic {
    constructor() {
        super( 'Q00233_TestOfTheWarSpirit', 'listeners/tracked-200/TestOfTheWarSpirit.ts' )
        this.questId = 233
        this.questItemIds = [
            VENDETTA_TOTEM,
            TAMLIN_ORC_HEAD,
            WARSPIRIT_TOTEM,
            ORIMS_CONTRACT,
            PORTAS_EYE,
            EXCUROS_SCALE,
            MORDEOS_TALON,
            BRAKIS_REMAINS1,
            PEKIRONS_TOTEM,
            TONARS_SKULL,
            TONARS_RIB_BONE,
            TONARS_SPINE,
            TONARS_ARM_BONE,
            TONARS_THIGH_BONE,
            TONARS_REMAINS1,
            MANAKIAS_TOTEM,
            HERMODTS_SKULL,
            HERMODTS_RIB_BONE,
            HERMODTS_SPINE,
            HERMODTS_ARM_BONE,
            HERMODTS_THIGH_BONE,
            HERMODTS_REMAINS1,
            RACOYS_TOTEM,
            VIVIANTES_LETTER,
            INSECT_DIAGRAM_BOOK,
            KIRUNAS_SKULL,
            KIRUNAS_RIB_BONE,
            KIRUNAS_SPINE,
            KIRUNAS_ARM_BONE,
            KIRUNAS_THIGH_BONE,
            KIRUNAS_REMAINS1,
            BRAKIS_REMAINS2,
            TONARS_REMAINS2,
            HERMODTS_REMAINS2,
            KIRUNAS_REMAINS2,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            NOBLE_ANT,
            NOBLE_ANT_LEADER,
            MEDUSA,
            PORTA,
            EXCURO,
            MORDERO,
            LETO_LIZARDMAN_SHAMAN,
            LETO_LIZARDMAN_OVERLORD,
            TAMLIN_ORC,
            TAMLIN_ORC_ARCHER,
            STENOA_GORGON_QUEEN,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00233_TestOfTheWarSpirit'
    }

    getQuestStartIds(): Array<number> {
        return [ SEER_SOMAK ]
    }

    getTalkIds(): Array<number> {
        return [
            SEER_SOMAK,
            PRIESTESS_VIVYAN,
            TRADER_SARIEN,
            SEER_RACOY,
            SEER_MANAKIA,
            SHADOW_ORIM,
            ANCESTOR_MARTANKUS,
            SEER_PEKIRON,
        ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case NOBLE_ANT:
            case NOBLE_ANT_LEADER:
                if ( !QuestHelper.hasQuestItems( player, RACOYS_TOTEM, INSECT_DIAGRAM_BOOK ) ) {
                    return
                }

                let chance = _.random( 100 )
                if ( chance > 65 ) {
                    if ( !QuestHelper.hasQuestItem( player, KIRUNAS_THIGH_BONE ) ) {
                        await QuestHelper.giveSingleItem( player, KIRUNAS_THIGH_BONE, 1 )

                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                        return
                    }

                    if ( !QuestHelper.hasQuestItem( player, KIRUNAS_ARM_BONE ) ) {
                        await QuestHelper.giveSingleItem( player, KIRUNAS_ARM_BONE, 1 )

                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                        return
                    }

                    return
                }

                if ( chance > 30 ) {
                    if ( !QuestHelper.hasQuestItem( player, KIRUNAS_SPINE ) ) {
                        await QuestHelper.giveSingleItem( player, KIRUNAS_SPINE, 1 )

                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                        return
                    }

                    if ( !QuestHelper.hasQuestItem( player, KIRUNAS_RIB_BONE ) ) {
                        await QuestHelper.giveSingleItem( player, KIRUNAS_RIB_BONE, 1 )

                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                        return
                    }

                    return
                }

                if ( chance > 0 && !QuestHelper.hasQuestItem( player, KIRUNAS_SKULL ) ) {
                    await QuestHelper.giveSingleItem( player, KIRUNAS_SKULL, 1 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                }

                return

            case MEDUSA:
                if ( !QuestHelper.hasQuestItem( player, MANAKIAS_TOTEM ) ) {
                    return
                }

                if ( !QuestHelper.hasQuestItem( player, HERMODTS_RIB_BONE ) ) {
                    await QuestHelper.giveSingleItem( player, HERMODTS_RIB_BONE, 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                    return
                }

                if ( !QuestHelper.hasQuestItem( player, HERMODTS_SPINE ) ) {
                    await QuestHelper.giveSingleItem( player, HERMODTS_SPINE, 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                    return
                }

                if ( !QuestHelper.hasQuestItem( player, HERMODTS_ARM_BONE ) ) {
                    await QuestHelper.giveSingleItem( player, HERMODTS_ARM_BONE, 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                    return
                }

                if ( !QuestHelper.hasQuestItem( player, HERMODTS_THIGH_BONE ) ) {
                    await QuestHelper.giveSingleItem( player, HERMODTS_THIGH_BONE, 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                    return
                }

                return

            case PORTA:
                if ( QuestHelper.hasQuestItem( player, ORIMS_CONTRACT ) ) {
                    await QuestHelper.rewardUpToLimit( player, PORTAS_EYE, 2, 10, data.isChampion )
                }

                return

            case EXCURO:
                if ( QuestHelper.hasQuestItem( player, ORIMS_CONTRACT ) ) {
                    await QuestHelper.rewardUpToLimit( player, EXCUROS_SCALE, 5, 10, data.isChampion )
                }

                return

            case MORDERO:
                if ( QuestHelper.hasQuestItem( player, ORIMS_CONTRACT ) ) {
                    await QuestHelper.rewardUpToLimit( player, MORDEOS_TALON, 5, 10, data.isChampion )
                }

                return

            case LETO_LIZARDMAN_SHAMAN:
            case LETO_LIZARDMAN_OVERLORD:
                if ( !QuestHelper.hasQuestItem( player, PEKIRONS_TOTEM ) ) {
                    return
                }

                if ( !QuestHelper.hasQuestItem( player, TONARS_SKULL ) ) {
                    await QuestHelper.giveSingleItem( player, TONARS_SKULL, 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                    return
                }

                if ( !QuestHelper.hasQuestItem( player, TONARS_RIB_BONE ) ) {
                    await QuestHelper.giveSingleItem( player, TONARS_RIB_BONE, 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                    return
                }

                if ( !QuestHelper.hasQuestItem( player, TONARS_SPINE ) ) {
                    await QuestHelper.giveSingleItem( player, TONARS_SPINE, 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                    return
                }

                if ( !QuestHelper.hasQuestItem( player, TONARS_ARM_BONE ) ) {
                    await QuestHelper.giveSingleItem( player, TONARS_ARM_BONE, 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                    return
                }

                if ( !QuestHelper.hasQuestItem( player, TONARS_THIGH_BONE ) ) {
                    await QuestHelper.giveSingleItem( player, TONARS_THIGH_BONE, 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                    return
                }

                return

            case TAMLIN_ORC:
            case TAMLIN_ORC_ARCHER:
                if ( QuestHelper.hasQuestItem( player, VENDETTA_TOTEM ) ) {
                    await QuestHelper.rewardAndProgressState( player, state, TAMLIN_ORC_HEAD, 1, 13, data.isChampion, 4 )
                }

                return

            case STENOA_GORGON_QUEEN:
                if ( QuestHelper.hasQuestItem( player, MANAKIAS_TOTEM ) && !QuestHelper.hasQuestItem( player, HERMODTS_SKULL ) ) {
                    await QuestHelper.giveSingleItem( player, HERMODTS_SKULL, 1 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                }

                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( state.isCreated() ) {
                    state.startQuest()

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                    if ( !PlayerVariablesManager.get( data.playerId, QuestVariables.secondClassDiamondReward ) ) {
                        PlayerVariablesManager.set( player.getObjectId(), QuestVariables.secondClassDiamondReward, true )

                        await QuestHelper.rewardSingleItem( player, DIMENSIONAL_DIAMOND, 92 )

                        return this.getPath( '30510-05e.htm' )
                    }

                    return this.getPath( '30510-05.htm' )
                }

                return

            case '30510-05a.html':
            case '30510-05b.html':
            case '30510-05c.html':
            case '30510-05d.html':
            case '30510-05.html':
            case '30030-02.html':
            case '30030-03.html':
            case '30630-02.html':
            case '30630-03.html':
            case '30649-02.html':
                break

            case '30030-04.html':
                await QuestHelper.giveSingleItem( player, VIVIANTES_LETTER, 1 )
                break

            case '30507-02.html':
                await QuestHelper.giveSingleItem( player, RACOYS_TOTEM, 1 )
                break

            case '30515-02.html':
                await QuestHelper.giveSingleItem( player, MANAKIAS_TOTEM, 1 )
                break

            case '30630-04.html':
                await QuestHelper.giveSingleItem( player, ORIMS_CONTRACT, 1 )
                break

            case '30649-03.html':
                if ( QuestHelper.hasQuestItem( player, TONARS_REMAINS2 ) ) {
                    await QuestHelper.giveAdena( player, 161806, true )
                    await QuestHelper.giveSingleItem( player, MARK_OF_WARSPIRIT, 1 )
                    await QuestHelper.addExpAndSp( player, 894888, 61408 )
                    await state.exitQuest( false, true )

                    player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )
                    break
                }

                return

            case '30682-02.html':
                await QuestHelper.giveSingleItem( player, PEKIRONS_TOTEM, 1 )
                break


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== SEER_SOMAK ) {
                    break
                }

                if ( player.getRace() !== Race.ORC ) {
                    return this.getPath( '30510-01.html' )
                }

                if ( player.getClassId() !== ClassIdValues.orcShaman.id ) {
                    return this.getPath( '30510-02.html' )
                }

                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '30510-03.html' )
                }

                return this.getPath( '30510-04.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case SEER_SOMAK:
                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, VENDETTA_TOTEM, WARSPIRIT_TOTEM ) ) {
                            if ( QuestHelper.hasQuestItems( player, BRAKIS_REMAINS1, HERMODTS_REMAINS1, KIRUNAS_REMAINS1, TONARS_REMAINS1 ) ) {
                                await QuestHelper.giveSingleItem( player, VENDETTA_TOTEM, 1 )
                                await QuestHelper.takeMultipleItems( player, 1, BRAKIS_REMAINS1, TONARS_REMAINS1, HERMODTS_REMAINS1, KIRUNAS_REMAINS1 )

                                state.setConditionWithSound( 3 )
                                return this.getPath( '30510-07.html' )
                            }

                            return this.getPath( '30510-06.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, VENDETTA_TOTEM ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, TAMLIN_ORC_HEAD ) < 13 ) {
                                return this.getPath( '30510-08.html' )
                            }

                            await QuestHelper.takeSingleItem( player, VENDETTA_TOTEM, 1 )
                            await QuestHelper.giveMultipleItems( player, 1, WARSPIRIT_TOTEM, BRAKIS_REMAINS2, TONARS_REMAINS2, HERMODTS_REMAINS2, KIRUNAS_REMAINS2 )

                            state.setConditionWithSound( 5 )
                            return this.getPath( '30510-09.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, WARSPIRIT_TOTEM ) ) {
                            return this.getPath( '30510-10.html' )
                        }

                        break

                    case PRIESTESS_VIVYAN:
                        if ( QuestHelper.hasQuestItem( player, RACOYS_TOTEM )
                                && !QuestHelper.hasAtLeastOneQuestItem( player, VIVIANTES_LETTER, INSECT_DIAGRAM_BOOK ) ) {
                            return this.getPath( '30030-01.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, RACOYS_TOTEM, VIVIANTES_LETTER )
                                && !QuestHelper.hasQuestItem( player, INSECT_DIAGRAM_BOOK ) ) {
                            return this.getPath( '30030-05.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, RACOYS_TOTEM, INSECT_DIAGRAM_BOOK )
                                && !QuestHelper.hasQuestItem( player, VIVIANTES_LETTER ) ) {
                            return this.getPath( '30030-06.html' )
                        }

                        if ( !QuestHelper.hasQuestItem( player, RACOYS_TOTEM )
                                && QuestHelper.hasAtLeastOneQuestItem( player, KIRUNAS_REMAINS1, KIRUNAS_REMAINS2, VENDETTA_TOTEM ) ) {
                            return this.getPath( '30030-07.html' )
                        }

                        break

                    case TRADER_SARIEN:
                        if ( QuestHelper.hasQuestItems( player, RACOYS_TOTEM, VIVIANTES_LETTER )
                                && !QuestHelper.hasQuestItem( player, INSECT_DIAGRAM_BOOK ) ) {
                            await QuestHelper.takeSingleItem( player, VIVIANTES_LETTER, 1 )
                            await QuestHelper.giveSingleItem( player, INSECT_DIAGRAM_BOOK, 1 )

                            return this.getPath( '30436-01.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, RACOYS_TOTEM, INSECT_DIAGRAM_BOOK )
                                && !QuestHelper.hasQuestItem( player, VIVIANTES_LETTER ) ) {
                            return this.getPath( '30436-02.html' )
                        }

                        if ( !QuestHelper.hasQuestItem( player, RACOYS_TOTEM )
                                && QuestHelper.hasAtLeastOneQuestItem( player, KIRUNAS_REMAINS1, KIRUNAS_REMAINS2, VENDETTA_TOTEM ) ) {
                            return this.getPath( '30436-03.html' )
                        }

                        break

                    case SEER_RACOY:
                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, RACOYS_TOTEM, KIRUNAS_REMAINS1, KIRUNAS_REMAINS2, VENDETTA_TOTEM ) ) {
                            return this.getPath( '30507-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, RACOYS_TOTEM )
                                && !QuestHelper.hasAtLeastOneQuestItem( player, VIVIANTES_LETTER, INSECT_DIAGRAM_BOOK ) ) {
                            return this.getPath( '30507-03.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, RACOYS_TOTEM, VIVIANTES_LETTER )
                                && !QuestHelper.hasQuestItem( player, INSECT_DIAGRAM_BOOK ) ) {
                            return this.getPath( '30507-04.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, RACOYS_TOTEM, INSECT_DIAGRAM_BOOK )
                                && !QuestHelper.hasQuestItem( player, VIVIANTES_LETTER ) ) {
                            if ( QuestHelper.hasQuestItems( player, KIRUNAS_SKULL, KIRUNAS_RIB_BONE, KIRUNAS_SPINE, KIRUNAS_ARM_BONE, KIRUNAS_THIGH_BONE ) ) {
                                await QuestHelper.takeMultipleItems( player, 1, RACOYS_TOTEM, INSECT_DIAGRAM_BOOK, KIRUNAS_SKULL, KIRUNAS_RIB_BONE, KIRUNAS_SPINE, KIRUNAS_ARM_BONE, KIRUNAS_THIGH_BONE )
                                await QuestHelper.giveSingleItem( player, KIRUNAS_REMAINS1, 1 )

                                if ( QuestHelper.hasQuestItems( player, BRAKIS_REMAINS1, HERMODTS_REMAINS1, TONARS_REMAINS1 ) ) {
                                    state.setConditionWithSound( 2 )
                                }

                                return this.getPath( '30507-06.html' )
                            }

                            return this.getPath( '30507-05.html' )
                        }

                        if ( !QuestHelper.hasQuestItem( player, RACOYS_TOTEM )
                                && QuestHelper.hasAtLeastOneQuestItem( player, KIRUNAS_REMAINS1, KIRUNAS_REMAINS2, VENDETTA_TOTEM ) ) {
                            return this.getPath( '30507-07.html' )
                        }

                        break

                    case SEER_MANAKIA:
                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, MANAKIAS_TOTEM, HERMODTS_REMAINS2, VENDETTA_TOTEM, HERMODTS_REMAINS1 ) ) {
                            return this.getPath( '30515-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, MANAKIAS_TOTEM ) ) {
                            if ( QuestHelper.hasQuestItems( player, HERMODTS_SKULL, HERMODTS_RIB_BONE, HERMODTS_SPINE, HERMODTS_ARM_BONE, HERMODTS_THIGH_BONE ) ) {
                                await QuestHelper.takeMultipleItems( player, 1, MANAKIAS_TOTEM, HERMODTS_SKULL, HERMODTS_RIB_BONE, HERMODTS_SPINE, HERMODTS_ARM_BONE, HERMODTS_THIGH_BONE )
                                await QuestHelper.giveSingleItem( player, HERMODTS_REMAINS1, 1 )

                                if ( QuestHelper.hasQuestItems( player, BRAKIS_REMAINS1, KIRUNAS_REMAINS1, TONARS_REMAINS1 ) ) {
                                    state.setConditionWithSound( 2 )
                                }

                                return this.getPath( '30515-04.html' )
                            }

                            return this.getPath( '30515-03.html' )
                        }

                        if ( !QuestHelper.hasQuestItem( player, MANAKIAS_TOTEM )
                                && QuestHelper.hasAtLeastOneQuestItem( player, HERMODTS_REMAINS1, HERMODTS_REMAINS2, VENDETTA_TOTEM ) ) {
                            return this.getPath( '30515-05.html' )
                        }

                        break

                    case SHADOW_ORIM:
                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, ORIMS_CONTRACT, BRAKIS_REMAINS1, BRAKIS_REMAINS2, VENDETTA_TOTEM ) ) {
                            return this.getPath( '30630-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, ORIMS_CONTRACT ) ) {
                            let totalCount = QuestHelper.getItemsSumCount( player, PORTAS_EYE, EXCUROS_SCALE, MORDEOS_TALON )
                            if ( totalCount < 30 ) {
                                return this.getPath( '30630-05.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, -1, ORIMS_CONTRACT, PORTAS_EYE, EXCUROS_SCALE, MORDEOS_TALON )
                            await QuestHelper.giveSingleItem( player, BRAKIS_REMAINS1, 1 )

                            if ( QuestHelper.hasQuestItems( player, HERMODTS_REMAINS1, KIRUNAS_REMAINS1, TONARS_REMAINS1 ) ) {
                                state.setConditionWithSound( 2 )
                            }

                            return this.getPath( '30630-06.html' )
                        }

                        if ( !QuestHelper.hasQuestItem( player, ORIMS_CONTRACT ) && QuestHelper.hasAtLeastOneQuestItem( player, BRAKIS_REMAINS1, BRAKIS_REMAINS2, VENDETTA_TOTEM ) ) {
                            return this.getPath( '30630-07.html' )
                        }

                        break

                    case ANCESTOR_MARTANKUS:
                        if ( QuestHelper.hasQuestItem( player, WARSPIRIT_TOTEM ) ) {
                            return this.getPath( '30649-01.html' )
                        }

                        break

                    case SEER_PEKIRON:
                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, PEKIRONS_TOTEM, TONARS_REMAINS1, TONARS_REMAINS2, VENDETTA_TOTEM ) ) {
                            return this.getPath( '30682-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, PEKIRONS_TOTEM ) ) {
                            if ( QuestHelper.hasQuestItems( player, TONARS_SKULL, TONARS_RIB_BONE, TONARS_SPINE, TONARS_ARM_BONE, TONARS_THIGH_BONE ) ) {
                                await QuestHelper.takeMultipleItems( player, 1, PEKIRONS_TOTEM, TONARS_SKULL, TONARS_RIB_BONE, TONARS_SPINE, TONARS_ARM_BONE, TONARS_THIGH_BONE )
                                await QuestHelper.giveSingleItem( player, TONARS_REMAINS1, 1 )

                                if ( QuestHelper.hasQuestItems( player, BRAKIS_REMAINS1, HERMODTS_REMAINS1, KIRUNAS_REMAINS1 ) ) {
                                    state.setConditionWithSound( 2 )
                                }

                                return this.getPath( '30682-04.html' )
                            }

                            return this.getPath( '30682-03.html' )
                        }

                        if ( !QuestHelper.hasQuestItem( player, PEKIRONS_TOTEM )
                                && QuestHelper.hasAtLeastOneQuestItem( player, TONARS_REMAINS1, TONARS_REMAINS2, VENDETTA_TOTEM ) ) {
                            return this.getPath( '30682-05.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === SEER_SOMAK ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}