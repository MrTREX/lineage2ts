import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'

import _ from 'lodash'

const TREANT_BREMEC = 31853
const GOBLIN_CLUB = 1335
const GOBLIN_RAIDER = 20325
const SILVERY_LEAF = 1340
const minimumLevel = 4

export class WrathOfVerdure extends ListenerLogic {
    constructor() {
        super( 'Q00267_WrathOfVerdure', 'listeners/tracked-200/WrathOfVerdure.ts' )
        this.questId = 267
        this.questItemIds = [
            GOBLIN_CLUB,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [ GOBLIN_RAIDER ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00267_WrathOfVerdure'
    }

    getQuestStartIds(): Array<number> {
        return [ TREANT_BREMEC ]
    }

    getTalkIds(): Array<number> {
        return [ TREANT_BREMEC ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || Math.random() > QuestHelper.getAdjustedChance( GOBLIN_CLUB, 0.5, data.isChampion ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        await QuestHelper.rewardSingleQuestItem( player, GOBLIN_CLUB, 1, data.isChampion )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '31853-04.htm':
                state.startQuest()
                break

            case '31853-07.html':
                await state.exitQuest( true, true )
                break

            case '31853-08.html':
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getRace() !== Race.ELF ) {
                    return this.getPath( '31853-01.htm' )
                }

                return this.getPath( player.getLevel() >= minimumLevel ? '31853-03.htm' : '31853-02.htm' )

            case QuestStateValues.STARTED:
                let count = QuestHelper.getQuestItemsCount( player, GOBLIN_CLUB )
                if ( count === 0 ) {
                    return this.getPath( '31853-05.html' )
                }

                await QuestHelper.rewardSingleItem( player, SILVERY_LEAF, count )
                await QuestHelper.takeSingleItem( player, GOBLIN_CLUB, -1 )

                if ( count >= 10 ) {
                    await QuestHelper.giveAdena( player, 600, true )
                }

                return this.getPath( '31853-06.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}