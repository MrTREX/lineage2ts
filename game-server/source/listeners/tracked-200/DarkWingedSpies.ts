import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import {
    AttackableKillEvent,
    NpcGeneralEvent,
    NpcSeePlayerEvent,
    NpcTalkEvent,
} from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2Attackable } from '../../gameService/models/actor/L2Attackable'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'
import { AIEffectHelper } from '../../gameService/aicontroller/helpers/AIEffectHelper'

import _ from 'lodash'

const NERUGA_CHIEF_TANTUS = 30567
const DARKWING_BAT_FANG = 1478
const VARANGKAS_PARASITE = 1479
const DARKWING_BAT = 20316
const VARANGKAS_TRACKER = 27043
const minimumLevel = 11
const itemRate = 60
const MAX_BAT_FANG_COUNT = 70

export class DarkWingedSpies extends ListenerLogic {
    constructor() {
        super( 'Q00275_DarkWingedSpies', 'listeners/tracked-200/DarkWingedSpies.ts' )
        this.questId = 275
        this.questItemIds = [
            DARKWING_BAT_FANG,
            VARANGKAS_PARASITE,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [ DARKWING_BAT, VARANGKAS_TRACKER ]
    }

    getNpcSeePlayerIds(): Array<number> {
        return [ VARANGKAS_TRACKER ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00275_DarkWingedSpies'
    }

    getQuestStartIds(): Array<number> {
        return [ NERUGA_CHIEF_TANTUS ]
    }

    getTalkIds(): Array<number> {
        return [ NERUGA_CHIEF_TANTUS ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || !state.isCondition( 1 ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        let count = QuestHelper.getQuestItemsCount( player, DARKWING_BAT_FANG )

        switch ( data.npcId ) {
            case DARKWING_BAT:
                if ( await QuestHelper.rewardUpToLimit( player, DARKWING_BAT_FANG, 1, MAX_BAT_FANG_COUNT, data.isChampion ) ) {
                    state.setConditionWithSound( 2 )
                    return
                }

                if ( count > 10 && count < 66 && _.random( 100 ) < QuestHelper.getAdjustedChance( VARANGKAS_PARASITE, 10, data.isChampion ) ) {
                    QuestHelper.addSpawnAtLocation( VARANGKAS_TRACKER, player )
                    await QuestHelper.giveSingleItem( player, VARANGKAS_PARASITE, 1 )
                }

                return

            case VARANGKAS_TRACKER:
                if ( count < 66 && QuestHelper.hasQuestItem( player, VARANGKAS_PARASITE ) ) {
                    if ( await QuestHelper.rewardUpToLimit( player, DARKWING_BAT_FANG, 5, MAX_BAT_FANG_COUNT, data.isChampion ) ) {
                        state.setConditionWithSound( 2 )
                    }

                    await QuestHelper.takeSingleItem( player, VARANGKAS_PARASITE, -1 )
                }

                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || data.eventName !== '30567-03.htm' ) {
            return
        }

        state.startQuest()
        return this.getPath( data.eventName )
    }

    async onNpcSeePlayerEvent( data: NpcSeePlayerEvent ): Promise<void> {
        let npc = L2World.getObjectById( data.npcObjectId ) as L2Attackable
        npc.setRunning()

        return AIEffectHelper.notifyAttackedWithTargetId( npc, data.playerId, 0, 1 )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getRace() !== Race.ORC ) {
                    return this.getPath( '30567-00.htm' )
                }

                return this.getPath( player.getLevel() >= minimumLevel ? '30567-02.htm' : '30567-01.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '30567-04.html' )

                    case 2:
                        let amount = QuestHelper.getQuestItemsCount( player, DARKWING_BAT_FANG )
                        if ( amount < MAX_BAT_FANG_COUNT ) {
                            return this.getPath( '30567-04.html' )
                        }

                        await QuestHelper.giveAdena( player, amount * itemRate, true )
                        await state.exitQuest( true, true )

                        return this.getPath( '30567-05.html' )
                }
                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}