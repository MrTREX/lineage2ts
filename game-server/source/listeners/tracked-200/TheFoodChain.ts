import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const BIXON = 32175
const GREY_KELTIR_TOOTH = 9809
const BLACK_WOLF_TOOTH = 9810
const minimumLevel = 3
const teethCountForItem = 25

const rewardItemIds = [
    28,
    35,
    41,
    48,
    116,
]

type MonsterDrop = [ number, number, number ] // chance value, itemId, item amount
const monsterDropChances: { [ npcId: number ]: Array<MonsterDrop> } = {
    22229: [ [ 1000, GREY_KELTIR_TOOTH, 1 ] ],
    22230: [ [ 500, GREY_KELTIR_TOOTH, 1 ], [ 1000, GREY_KELTIR_TOOTH, 2 ] ],
    22231: [ [ 1000, GREY_KELTIR_TOOTH, 2 ] ],
    22232: [ [ 1000, BLACK_WOLF_TOOTH, 3 ] ],
    22233: [ [ 500, BLACK_WOLF_TOOTH, 3 ], [ 1000, BLACK_WOLF_TOOTH, 4 ] ],
}

export class TheFoodChain extends ListenerLogic {
    constructor() {
        super( 'Q00280_TheFoodChain', 'listeners/tracked-200/TargetOfOpportunity.ts' )
        this.questId = 280
        this.questItemIds = [ GREY_KELTIR_TOOTH, BLACK_WOLF_TOOTH ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterDropChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00280_TheFoodChain'
    }

    getQuestStartIds(): Array<number> {
        return [ BIXON ]
    }

    getTalkIds(): Array<number> {
        return [ BIXON ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId )

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        let chanceValue = _.random( 1000 )
        let drop: MonsterDrop = _.find( monsterDropChances[ data.npcId ], ( item: MonsterDrop ): boolean => {
            let [ chance, itemId ] = item
            return chanceValue < QuestHelper.getAdjustedChance( itemId, chance, data.isChampion )
        } )

        if ( !drop ) {
            return
        }

        let [ , itemId, amount ] = drop
        await QuestHelper.rewardSingleQuestItem( player, itemId, amount, data.isChampion )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32175-03.htm':
                state.startQuest()
                break

            case '32175-06.html':
                let totalAmount = QuestHelper.getItemsSumCount( player, GREY_KELTIR_TOOTH, BLACK_WOLF_TOOTH )
                if ( totalAmount === 0 ) {
                    return this.getPath( '32175-07.html' )
                }

                await QuestHelper.giveAdena( player, 2 * totalAmount, true )
                await QuestHelper.takeMultipleItems( player, -1, GREY_KELTIR_TOOTH, BLACK_WOLF_TOOTH )

                break

            case '32175-08.html':
                break

            case '32175-09.html':
                await state.exitQuest( true, true )
                break

            case '32175-11.html':
                let greyTeeth = QuestHelper.getQuestItemsCount( player, GREY_KELTIR_TOOTH )
                let blackTeeth = QuestHelper.getQuestItemsCount( player, BLACK_WOLF_TOOTH )

                if ( ( greyTeeth + blackTeeth ) < teethCountForItem ) {
                    return this.getPath( '32175-10.html' )
                }

                if ( greyTeeth >= teethCountForItem ) {
                    await QuestHelper.takeSingleItem( player, GREY_KELTIR_TOOTH, teethCountForItem )
                } else {
                    await QuestHelper.takeSingleItem( player, GREY_KELTIR_TOOTH, greyTeeth )
                    await QuestHelper.takeSingleItem( player, BLACK_WOLF_TOOTH, teethCountForItem - greyTeeth )
                }

                await QuestHelper.rewardSingleItem( player, _.sample( rewardItemIds ), 1 )
                break

        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '32175-01.htm' : '32175-02.htm' )

            case QuestStateValues.STARTED:
                if ( QuestHelper.hasAtLeastOneQuestItem( player, ...this.questItemIds ) ) {
                    return this.getPath( '32175-05.html' )
                }

                return this.getPath( '32175-04.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}