import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const FAKE_GREYMORE = 32757
const SEEKER_SOLINA = 22790
const SAVIOR_SOLINA = 22791
const ASCETIC_SOLINA = 22793
const DIVINITY_JUDGE = 22794
const DIVINITY_MANAGER = 22795
const DIVINITY_SUPERVISOR = 22796
const DIVINITY_WORSHIPPER = 22797
const DIVINITY_PROTECTOR = 22798
const DIVINITY_FIGHTER = 22799
const DIVINITY_MAGUS = 22800
const TATTERED_MONK_CLOTHES = 15526
const minimumLevel = 82

type RewardData = [ number, boolean ] // chance limit, guarantee at least one item
const monsterRewardChance: { [ npcId: number ]: RewardData } = {
    [ SEEKER_SOLINA ]: [ 57, false ],
    [ SAVIOR_SOLINA ]: [ 55, false ],
    [ ASCETIC_SOLINA ]: [ 59, false ],
    [ DIVINITY_JUDGE ]: [ 698, false ],
    [ DIVINITY_MANAGER ]: [ 735, false ],
    [ DIVINITY_SUPERVISOR ]: [ 903, false ],
    [ DIVINITY_WORSHIPPER ]: [ 811, false ],
    [ DIVINITY_PROTECTOR ]: [ 884, true ],
    [ DIVINITY_FIGHTER ]: [ 893, true ],
    [ DIVINITY_MAGUS ]: [ 953, true ],
}

export class TheOneWhoEndsSilence extends ListenerLogic {
    constructor() {
        super( 'Q00270_TheOneWhoEndsSilence', 'listeners/tracked-200/TheOneWhoEndsSilence.ts' )
        this.questId = 270
        this.questItemIds = [
            TATTERED_MONK_CLOTHES,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            SEEKER_SOLINA,
            SAVIOR_SOLINA,
            ASCETIC_SOLINA,
            DIVINITY_JUDGE,
            DIVINITY_MANAGER,
            DIVINITY_SUPERVISOR,
            DIVINITY_WORSHIPPER,
            DIVINITY_PROTECTOR,
            DIVINITY_FIGHTER,
            DIVINITY_MAGUS,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00270_TheOneWhoEndsSilence'
    }

    getQuestStartIds(): Array<number> {
        return [ FAKE_GREYMORE ]
    }

    getTalkIds(): Array<number> {
        return [ FAKE_GREYMORE ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 1 )
        if ( !player ) {
            return
        }

        let [ chance, isItemGuaranteed ] = monsterRewardChance[ data.npcId ]
        await this.rewardItem( player, chance, L2World.getObjectById( data.targetId ) as L2Npc, isItemGuaranteed, data.isChampion )

        return
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let itemCount = QuestHelper.getQuestItemsCount( player, TATTERED_MONK_CLOTHES )

        switch ( data.eventName ) {
            case '32757-02.htm':
                if ( player.hasQuestCompleted( 'Q10288_SecretMission' ) && player.getLevel() >= minimumLevel ) {
                    break
                }

                return

            case '32757-04.html':
                if ( player.hasQuestCompleted( 'Q10288_SecretMission' ) && player.getLevel() >= minimumLevel ) {
                    state.startQuest()
                    break
                }

                return

            case '32757-08.html':
                if ( state.isCondition( 1 ) ) {

                    if ( itemCount === 0 ) {
                        return this.getPath( '32757-06.html' )
                    }

                    if ( itemCount < 100 ) {
                        return this.getPath( '32757-07.html' )
                    }

                    break
                }

                return

            case 'rags100':
                if ( itemCount >= 100 ) {
                    let itemId = _.random( 10 ) < 5 ? ( _.random( 1000 ) < 438 ? 10373 : 10397 ) + _.random( 8 ) : this.getScrollReward( 1 )

                    await QuestHelper.rewardSingleItem( player, itemId, 1 )
                    await QuestHelper.takeSingleItem( player, TATTERED_MONK_CLOTHES, 100 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                    return this.getPath( '32757-09.html' )
                }

                return this.getPath( '32757-10.html' )

            case 'rags200':
                if ( itemCount >= 200 ) {
                    let itemId = _.random( 1000 ) < 549 ? 10373 : 10397
                    await QuestHelper.rewardMultipleItems( player, 1, itemId + _.random( 8 ), this.getScrollReward( 2 ) )
                    await QuestHelper.takeSingleItem( player, TATTERED_MONK_CLOTHES, 200 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                    return this.getPath( '32757-09.html' )
                }

                return this.getPath( '32757-10.html' )

            case 'rags300':
                if ( itemCount >= 300 ) {
                    await QuestHelper.rewardMultipleItems( player, 1, 10373 + _.random( 8 ), 10397 + _.random( 8 ), this.getScrollReward( 3 ) )
                    await QuestHelper.takeSingleItem( player, TATTERED_MONK_CLOTHES, 300 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                    return this.getPath( '32757-09.html' )
                }

                return this.getPath( '32757-10.html' )

            case 'rags400':
                if ( itemCount >= 400 ) {
                    let itemId = _.random( 10 ) < 5 ? ( _.random( 1000 ) < 438 ? 10373 : 10397 ) + _.random( 8 ) : this.getScrollReward( 1 )

                    await QuestHelper.rewardMultipleItems( player, 1, 10373 + _.random( 8 ), 10397 + _.random( 8 ), itemId, this.getScrollReward( 3 ) )
                    await QuestHelper.takeSingleItem( player, TATTERED_MONK_CLOTHES, 400 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                    return this.getPath( '32757-09.html' )
                }

                return this.getPath( '32757-10.html' )

            case 'rags500':
                if ( itemCount >= 500 ) {
                    let itemId = _.random( 1000 ) < 549 ? 10373 : 10397
                    await QuestHelper.rewardMultipleItems( player, 1, 10373 + _.random( 8 ), 10397 + _.random( 8 ), itemId + _.random( 8 ), this.getScrollReward( 3 ), this.getScrollReward( 2 ) )
                    await QuestHelper.takeSingleItem( player, TATTERED_MONK_CLOTHES, 500 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                    return this.getPath( '32757-09.html' )
                }

                return this.getPath( '32757-10.html' )

            case 'exit1':
                if ( state.isCondition( 1 ) ) {
                    if ( itemCount >= 1 ) {
                        return this.getPath( '32757-12.html' )
                    }

                    await state.exitQuest( true, true )
                    return this.getPath( '32757-13.html' )
                }

                return

            case 'exit2':
                if ( state.isCondition( 1 ) ) {
                    await state.exitQuest( true, true )
                    return this.getPath( '32757-13.html' )
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                let shouldProceed: boolean = player.getLevel() >= minimumLevel && player.hasQuestCompleted( 'Q10288_SecretMission' )
                return this.getPath( shouldProceed ? '32757-01.htm' : '32757-03.html' )

            case QuestStateValues.STARTED:
                if ( state.isCondition( 1 ) ) {
                    return this.getPath( '32757-05.html' )
                }
                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getScrollReward( type: number ): number {
        let chance

        switch ( type ) {
            case 1:
                chance = _.random( 100 )
                if ( chance < 1 ) {
                    return 5593
                }

                if ( chance < 28 ) {
                    return 5594
                }

                if ( chance < 61 ) {
                    return 5595
                }

                return 9898

            case 2:
                chance = _.random( 100 )
                if ( chance < 20 ) {
                    return 5593
                }

                if ( chance < 40 ) {
                    return 5594
                }

                if ( chance < 70 ) {
                    return 5595
                }

                return 9898

            case 3:
                chance = _.random( 1000 )
                if ( chance < 242 ) {
                    return 5593
                }

                if ( chance < 486 ) {
                    return 5594
                }

                if ( chance < 742 ) {
                    return 5595
                }

                return 9898
        }

        return 5593
    }

    async rewardItem( player: L2PcInstance, chance: number, npc: L2Npc, isItemGuaranteed: boolean, isFromChampion: boolean ): Promise<void> {
        if ( !GeneralHelper.checkIfInRange( 1500, npc, player, false ) ) {
            return
        }

        let additionalAmount = isItemGuaranteed ? 1 : 0
        let totalAmount = ( _.random( 1000 ) < QuestHelper.getAdjustedChance( TATTERED_MONK_CLOTHES, chance, isFromChampion ) ? 1 : 0 ) + additionalAmount
        if ( totalAmount > 0 ) {
            await QuestHelper.rewardSingleQuestItem( player, TATTERED_MONK_CLOTHES, totalAmount, isFromChampion )
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        }
    }
}