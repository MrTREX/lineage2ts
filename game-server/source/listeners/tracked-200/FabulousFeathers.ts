import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const ERINU = 32164
const feather = 9746
const featherCount = 80
const minimumLevel = 17

const monsterRewardChances = {
    22251: 0.748, // Shady Muertos Captain
    22253: 0.772, // Shady Muertos Warrior
    22254: 0.772, // Shady Muertos Archer
    22255: 0.796, // Shady Muertos Commander
    22256: 0.952, // Shady Muertos Wizard
}

export class FabulousFeathers extends ListenerLogic {
    constructor() {
        super( 'Q00286_FabulousFeathers', 'listeners/tracked-200/FabulousFeathers.ts' )
        this.questId = 286
        this.questItemIds = [ feather ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00286_FabulousFeathers'
    }

    getQuestStartIds(): Array<number> {
        return [ ERINU ]
    }

    getTalkIds(): Array<number> {
        return [ ERINU ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32164-03.htm':
                state.startQuest()
                break

            case '32164-06.html':
                if ( !state.isCondition( 2 ) || QuestHelper.getQuestItemsCount( player, feather ) < featherCount ) {
                    return this.getPath( '32164-07.html' )
                }

                await QuestHelper.takeSingleItem( player, feather, -1 )
                await QuestHelper.giveAdena( player, 4160, true )
                await state.exitQuest( true, true )

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '32164-01.htm' : '32164-02.htm' )

            case QuestStateValues.STARTED:
                let canProceed: boolean = state.isCondition( 2 ) && QuestHelper.getQuestItemsCount( player, feather ) >= featherCount
                return this.getPath( canProceed ? '32164-04.html' : '32164-05.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}