import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import _ from 'lodash'

const SALLY = 32743
const mobRewardMap: { [ key: number ]: number } = {
    18864: 15493,
    18865: 15494,
    18868: 15495,
}

const questItems = _.values( mobRewardMap )
const minimumLevel = 82

export class WatchWhatYouEat extends ListenerLogic {
    constructor() {
        super( 'Q00250_WatchWhatYouEat', 'listeners/tracked-200/WatchWhatYouEat.ts' )
        this.questId = 250
        this.questItemIds = questItems
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( mobRewardMap ).map( value => _.parseInt( value ) )
    }

    getApproachedForTalkIds(): Array<number> {
        return [ SALLY ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00250_WatchWhatYouEat'
    }

    getQuestStartIds(): Array<number> {
        return [ SALLY ]
    }

    getTalkIds(): Array<number> {
        return [ SALLY ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let reward = mobRewardMap[ data.npcId ]

        if ( !QuestHelper.hasQuestItem( player, reward ) ) {
            await QuestHelper.giveSingleItem( player, reward, 1 )
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        }

        if ( QuestHelper.hasQuestItems( player, ...questItems ) ) {
            state.setConditionWithSound( 2, true )
        }
    }

    async onApproachedForTalk(): Promise<string> {
        return this.getPath( '32743-20.html' )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '32743-03.htm':
                state.startQuest()
                break

            case '32743-end.htm':
                let player = L2World.getPlayer( data.playerId )
                await QuestHelper.giveAdena( player, 135661, true )
                await QuestHelper.addExpAndSp( player, 698334, 76369 )
                await state.exitQuest( false, true )
                break

            case '32743-22.html':
                if ( state.isCompleted() ) {
                    return this.getPath( '32743-23.html' )
                }

                break
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '32743-01.htm' : '32743-00.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '32743-04.htm' )

                    case 2:
                        if ( QuestHelper.hasQuestItems( player, ...questItems ) ) {
                            await QuestHelper.takeMultipleItems( player, 1, ...questItems )
                            return this.getPath( '32743-05.htm' )
                        }

                        return this.getPath( '32743-06.htm' )
                }

                break

            case QuestStateValues.COMPLETED:
                return this.getPath( '32743-done.htm' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}