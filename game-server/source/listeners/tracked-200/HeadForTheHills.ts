import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { NewbieRewardsHelper } from '../helpers/NewbieRewardsHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const CLAWS = 9796
const MERCELA = 32173
const minimumLevel = 6

const questRewards = [
    115, // Earring of Wisdom
    876, // Ring of Anguish
    907, // Necklace of Anguish
    22, // Leather Shirt
    428, // Feriotic Tunic
    1100, // Cotton Tunic
    29, // Leather Pants
    463, // Feriotic Stockings
    1103, // Cotton Stockings
    736, // Scroll of Escape
]

const monsterRewardChances = {
    22234: 39, // Green Goblin
    22235: 45, // Mountain Werewolf
    22236: 65, // Muertos Archer
    22237: 72, // Mountain Fungus
    22238: 92, // Mountain Werewolf Chief
    22239: 99, // Muertos Guard
}

export class HeadForTheHills extends ListenerLogic {
    constructor() {
        super( 'Q00281_HeadForTheHills', 'listeners/tracked-200/HeadForTheHills.ts' )
        this.questId = 281
        this.questItemIds = [ CLAWS ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00281_HeadForTheHills'
    }

    getQuestStartIds(): Array<number> {
        return [ MERCELA ]
    }

    getTalkIds(): Array<number> {
        return [ MERCELA ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || !state.isStarted() ) {
            return
        }

        if ( _.random( 100 ) < QuestHelper.getAdjustedChance( CLAWS, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            let player = L2World.getPlayer( data.playerId )
            await QuestHelper.rewardSingleQuestItem( player, CLAWS, 1, data.isChampion )

            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32173-03.htm':
                state.startQuest()
                break

            case '32173-06.html':
                let claws = QuestHelper.getQuestItemsCount( player, CLAWS )

                if ( claws === 0 ) {
                    return this.getPath( '32173-07.html' )
                }

                let amount = ( claws * 23 ) + ( claws >= 10 ? 400 : 0 )

                await QuestHelper.giveAdena( player, amount, true )
                await QuestHelper.takeSingleItem( player, CLAWS, -1 )
                await NewbieRewardsHelper.giveNewbieReward( player )

                break

            case '32173-08.html':
                break

            case '32173-09.html':
                await state.exitQuest( true, true )
                break

            case '32173-11.html':
                if ( QuestHelper.getQuestItemsCount( player, CLAWS ) < 50 ) {
                    return this.getPath( '32173-10.html' )
                }

                let itemId: number = _.random( 100 ) <= 36 ? _.sample( questRewards ) : _.last( questRewards )

                await QuestHelper.rewardSingleItem( player, itemId, 1 )
                await QuestHelper.takeSingleItem( player, CLAWS, 50 )
                await NewbieRewardsHelper.giveNewbieReward( player )

                break


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '32173-01.htm' : '32173-02.htm' )

            case QuestStateValues.STARTED:
                return this.getPath( QuestHelper.hasQuestItems( player, CLAWS ) ? '32173-05.html' : '32173-04.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}