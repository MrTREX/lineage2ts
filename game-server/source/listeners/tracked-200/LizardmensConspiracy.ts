import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const GUARD_PRAGA = 30333
const MAGISTER_ROHMER = 30344
const PATROLS_REPORT = 7182
const SHINING_GEM = 7183
const SHINING_RED_GEM = 7184
const minimumLevel = 25

type MonsterReward = [ number, number ] // itemId, chance
const monsterRewards: { [ npcId: number ]: MonsterReward } = {
    20922: [ SHINING_GEM, 0.49 ],
    20923: [ SHINING_GEM, 0.70 ],
    20924: [ SHINING_GEM, 0.75 ],
    20926: [ SHINING_RED_GEM, 0.54 ],
    20927: [ SHINING_RED_GEM, 0.54 ],
}

export class LizardmensConspiracy extends ListenerLogic {
    constructor() {
        super( 'Q00298_LizardmensConspiracy', 'listeners/tracked-200/LizardmensConspiracy.ts' )
        this.questId = 298
        this.questItemIds = [
            PATROLS_REPORT,
            SHINING_GEM,
            SHINING_RED_GEM,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewards ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00298_LizardmensConspiracy'
    }

    getQuestStartIds(): Array<number> {
        return [ GUARD_PRAGA ]
    }

    getTalkIds(): Array<number> {
        return [ GUARD_PRAGA, MAGISTER_ROHMER ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), 2, 3, L2World.getObjectById( data.targetId ) as L2Npc )

        if ( !state ) {
            return
        }

        let [ itemId, chance ] = monsterRewards[ data.npcId ]
        if ( QuestHelper.getAdjustedChance( itemId, Math.random(), data.isChampion ) < chance ) {
            return
        }

        let player = state.getPlayer()
        await QuestHelper.rewardSingleQuestItem( player, itemId, 1, data.isChampion )

        if ( QuestHelper.getQuestItemsCount( player, SHINING_GEM ) >= 50 && QuestHelper.getQuestItemsCount( player, SHINING_RED_GEM ) >= 50 ) {
            state.setConditionWithSound( 3, true )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30333-03.htm':
                state.startQuest()
                await QuestHelper.giveSingleItem( player, PATROLS_REPORT, 1 )
                break

            case '30344-04.html':
                if ( !state.isCondition( 1 ) || !QuestHelper.hasQuestItem( player, PATROLS_REPORT ) ) {
                    return
                }

                await QuestHelper.takeSingleItem( player, PATROLS_REPORT, -1 )
                state.setConditionWithSound( 2, true )
                break

            case '30344-06.html':
                if ( !state.isCondition( 3 ) ) {
                    return this.getPath( '30344-07.html' )
                }

                await QuestHelper.addExpAndSp( player, 0, 42000 )
                await state.exitQuest( true, true )

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== GUARD_PRAGA ) {
                    break
                }

                return this.getPath( player.getLevel() >= minimumLevel ? '30333-01.htm' : '30333-02.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case GUARD_PRAGA:
                        if ( QuestHelper.hasQuestItems( player, PATROLS_REPORT ) ) {
                            return this.getPath( '30333-04.html' )
                        }

                        break

                    case MAGISTER_ROHMER:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '30344-01.html' )

                            case 2:
                                return this.getPath( '30344-02.html' )

                            case 3:
                                return this.getPath( '30344-03.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}