import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const PINAPS = 30201
const ENCHANT_WEAPON_S = 959
const ENCHANT_ARMOR_S = 960
const FIRE_CRYSTAL = 9552
const SEL_MAHUM_ID_TAG = 15714
const minimumLevel = 82

const monsterRewardChances = {
    22775: 932, // Sel Mahum Drill Sergeant
    22776: 397, // Sel Mahum Training Officer
    22777: 932, // Sel Mahum Drill Sergeant
    22778: 932, // Sel Mahum Drill Sergeant
    22780: 363, // Sel Mahum Recruit
    22781: 483, // Sel Mahum Soldier
    22782: 363, // Sel Mahum Recruit
    22783: 352, // Sel Mahum Soldier
    22784: 363, // Sel Mahum Recruit
    22785: 169, // Sel Mahum Soldier
}

export class ThreatRemoval extends ListenerLogic {
    constructor() {
        super( 'Q00290_ThreatRemoval', 'listeners/tracked-200/ThreatRemoval.ts' )
        this.questId = 290
        this.questItemIds = [
            SEL_MAHUM_ID_TAG,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00290_ThreatRemoval'
    }

    getQuestStartIds(): Array<number> {
        return [ PINAPS ]
    }

    getTalkIds(): Array<number> {
        return [ PINAPS ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player: L2PcInstance = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 1 )
        if ( !player ) {
            return
        }

        if ( _.random( 1000 ) < QuestHelper.getAdjustedChance( SEL_MAHUM_ID_TAG, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            await QuestHelper.rewardSingleQuestItem( player, SEL_MAHUM_ID_TAG, 1, data.isChampion )
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30201-02.html':
                state.startQuest()
                break

            case '30201-06.html':
                if ( !state.isCondition( 1 ) ) {
                    return
                }

                await QuestHelper.takeSingleItem( player, SEL_MAHUM_ID_TAG, 400 )
                switch ( _.random( 10 ) ) {
                    case 0:
                        await QuestHelper.rewardSingleItem( player, ENCHANT_WEAPON_S, 1 )
                        break

                    case 1:
                    case 2:
                    case 3:
                        await QuestHelper.rewardSingleItem( player, ENCHANT_ARMOR_S, 1 )
                        break

                    case 4:
                    case 5:
                        await QuestHelper.rewardSingleItem( player, ENCHANT_ARMOR_S, 2 )
                        break

                    case 6:
                        await QuestHelper.rewardSingleItem( player, ENCHANT_ARMOR_S, 3 )
                        break

                    case 7:
                    case 8:
                        await QuestHelper.rewardSingleItem( player, FIRE_CRYSTAL, 1 )
                        break

                    case 9:
                    case 10:
                        await QuestHelper.rewardSingleItem( player, FIRE_CRYSTAL, 2 )
                        break
                }

                break

            case '30201-07.html':
                if ( !state.isCondition( 1 ) ) {
                    return
                }

                break

            case 'exit':
                if ( !state.isCondition( 1 ) ) {
                    return
                }

                if ( QuestHelper.hasQuestItem( player, SEL_MAHUM_ID_TAG ) ) {
                    return this.getPath( '30201-08.html' )
                }

                await state.exitQuest( true, true )
                return this.getPath( '30201-09.html' )

            case '30201-10.html':
                if ( !state.isCondition( 1 ) ) {
                    return
                }

                await state.exitQuest( true, true )
                break


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                let canProceed: boolean = player.getLevel() >= minimumLevel && player.hasQuestCompleted( 'Q00251_NoSecrets' )
                return this.getPath( canProceed ? '30201-01.htm' : '30201-03.html' )

            case QuestStateValues.STARTED:
                return this.getPath( QuestHelper.getQuestItemsCount( player, SEL_MAHUM_ID_TAG ) < 400 ? '30201-04.html' : '30201-05.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}