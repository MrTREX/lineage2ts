import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const LECTOR = 30001
const WOLF_PELT = 702

type RewardChance = [ number, number ]
const rewardChances : Array<RewardChance> = [
    [ 390, 1 ], // Cotton Shirt
    [ 29, 6 ], // Leather Pants
    [ 22, 9 ], // Leather Shirt
    [ 1119, 13 ], // Short Leather Gloves
    [ 426, 16 ], // Tunic
]

const minimumLevel = 3
const WOLF_PELT_COUNT = 40

export class BringWolfPelts extends ListenerLogic {
    constructor() {
        super( 'Q00258_BringWolfPelts', 'listeners/tracked-200/BringWolfPelts.ts' )
        this.questId = 258
        this.questItemIds = [ WOLF_PELT ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            20120, // Wolf
            20442, // Elder Wolf
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00258_BringWolfPelts'
    }

    getQuestStartIds(): Array<number> {
        return [ LECTOR ]
    }

    getTalkIds(): Array<number> {
        return [ LECTOR ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isCondition( 1 ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        await QuestHelper.rewardSingleQuestItem( player, WOLF_PELT, 1, data.isChampion )

        if ( QuestHelper.getQuestItemsCount( player, WOLF_PELT ) >= WOLF_PELT_COUNT ) {
            state.setConditionWithSound( 2, true )
            return
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || data.eventName !== '30001-03.html' ) {
            return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30001-02.htm' : '30001-01.html' )

            case QuestStateValues.STARTED: {
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '30001-04.html' )

                    case 2:
                        if ( QuestHelper.getQuestItemsCount( player, WOLF_PELT ) < WOLF_PELT_COUNT ) {
                            return this.getPath( '30001-04.html' )
                        }

                        let chance = _.random( 15 )
                        let reward: RewardChance = rewardChances.find( ( reward: [ number, number ] ) => {
                            return chance < reward[ 1 ]
                        } )

                        if ( reward ) {
                            await QuestHelper.rewardSingleItem( player, reward[ 0 ], 1 )
                        }

                        await state.exitQuest( true, true )
                        return this.getPath( '30001-05.html' )
                }

                break
            }
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}