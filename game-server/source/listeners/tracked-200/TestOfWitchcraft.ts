import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import {
    AttackableAttackedEvent,
    AttackableKillEvent,
    NpcGeneralEvent,
    NpcTalkEvent,
} from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'
import { QuestVariables } from '../helpers/QuestVariables'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { L2ItemInstance } from '../../gameService/models/items/instance/L2ItemInstance'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const GROCER_LARA = 30063
const TRADER_ALEXANDRIA = 30098
const MAGISTER_IKER = 30110
const PRIEST_VADIN = 30188
const TRADER_NESTLE = 30314
const SIR_KLAUS_VASPER = 30417
const LEOPOLD = 30435
const MAGISTER_KAIRA = 30476
const SHADOW_ORIM = 30630
const WARDEN_RODERIK = 30631
const WARDEN_ENDRIGO = 30632
const FISHER_EVERT = 30633

const SWORD_OF_BINDING = 3029
const ORIMS_DIAGRAM = 3308
const ALEXANDRIAS_BOOK = 3309
const IKERS_LIST = 3310
const DIRE_WYRM_FANG = 3311
const LETO_LIZARDMAN_CHARM = 3312
const ENCHANTED_STONE_GOLEM_HEARTSTONE = 3313
const LARAS_MEMO = 3314
const NESTLES_MEMO = 3315
const LEOPOLDS_JOURNAL = 3316
const AKLANTOTH_1ST_GEM = 3317
const AKLANTOTH_2ND_GEM = 3318
const AKLANTOTH_3RD_GEM = 3319
const AKLANTOTH_4TH_GEM = 3320
const AKLANTOTH_5TH_GEM = 3321
const AKLANTOTH_6TH_GEM = 3322
const BRIMSTONE_1ST = 3323
const ORIMS_INSTRUCTIONS = 3324
const ORIMS_1ST_LETTER = 3325
const ORIMS_2ND_LETTER = 3326
const SIR_VASPERS_LETTER = 3327
const VADINS_CRUCIFIX = 3328
const TAMLIN_ORC_AMULET = 3329
const VADINS_SANCTIONS = 3330
const IKERS_AMULET = 3331
const SOULTRAP_CRYSTAL = 3332
const PURGATORY_KEY = 3333
const ZERUEL_BIND_CRYSTAL = 3334
const BRIMSTONE_2ND = 3335

const MARK_OF_WITCHCRAFT = 3307
const DIMENSIONAL_DIAMOND = 7562

const DIRE_WYRM = 20557
const ENCHANTED_STONE_GOLEM = 20565
const LETO_LIZARDMAN = 20577
const LETO_LIZARDMAN_ARCHER = 20578
const LETO_LIZARDMAN_SOLDIER = 20579
const LETO_LIZARDMAN_WARRIOR = 20580
const LETO_LIZARDMAN_SHAMAN = 20581
const LETO_LIZARDMAN_OVERLORD = 20582
const TAMLIN_ORC = 20601
const TAMLIN_ORC_ARCHER = 20602

const NAMELESS_REVENANT = 27099
const SKELETAL_MERCENARY = 27100
const DREVANUL_PRINCE_ZERUEL = 27101

const minimumLevel = 39
const DREVANUL_PRINCE_ZERUEL_SPAWN = new Location( 13395, 169807, -3708 )


export class TestOfWitchcraft extends ListenerLogic {
    constructor() {
        super( 'Q00229_TestOfWitchcraft', 'listeners/tracked-200/TestOfWitchcraft.ts' )
        this.questId = 229
        this.questItemIds = [
            SWORD_OF_BINDING,
            ORIMS_DIAGRAM,
            ALEXANDRIAS_BOOK,
            IKERS_LIST,
            DIRE_WYRM_FANG,
            LETO_LIZARDMAN_CHARM,
            ENCHANTED_STONE_GOLEM_HEARTSTONE,
            LARAS_MEMO,
            NESTLES_MEMO,
            LEOPOLDS_JOURNAL,
            AKLANTOTH_1ST_GEM,
            AKLANTOTH_2ND_GEM,
            AKLANTOTH_3RD_GEM,
            AKLANTOTH_4TH_GEM,
            AKLANTOTH_5TH_GEM,
            AKLANTOTH_6TH_GEM,
            BRIMSTONE_1ST,
            ORIMS_INSTRUCTIONS,
            ORIMS_1ST_LETTER,
            ORIMS_2ND_LETTER,
            SIR_VASPERS_LETTER,
            VADINS_CRUCIFIX,
            TAMLIN_ORC_AMULET,
            VADINS_SANCTIONS,
            IKERS_AMULET,
            SOULTRAP_CRYSTAL,
            PURGATORY_KEY,
            ZERUEL_BIND_CRYSTAL,
            BRIMSTONE_2ND,
        ]
    }

    getAttackableAttackIds(): Array<number> {
        return [
            NAMELESS_REVENANT,
            SKELETAL_MERCENARY,
            DREVANUL_PRINCE_ZERUEL,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            DIRE_WYRM,
            ENCHANTED_STONE_GOLEM,
            LETO_LIZARDMAN,
            LETO_LIZARDMAN_ARCHER,
            LETO_LIZARDMAN_SOLDIER,
            LETO_LIZARDMAN_WARRIOR,
            LETO_LIZARDMAN_SHAMAN,
            LETO_LIZARDMAN_OVERLORD,
            TAMLIN_ORC,
            TAMLIN_ORC_ARCHER,
            NAMELESS_REVENANT,
            SKELETAL_MERCENARY,
            DREVANUL_PRINCE_ZERUEL,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00229_TestOfWitchcraft'
    }

    getQuestStartIds(): Array<number> {
        return [ SHADOW_ORIM ]
    }

    getTalkIds(): Array<number> {
        return [
            SHADOW_ORIM,
            GROCER_LARA,
            TRADER_ALEXANDRIA,
            MAGISTER_IKER,
            PRIEST_VADIN,
            TRADER_NESTLE,
            SIR_KLAUS_VASPER,
            LEOPOLD,
            MAGISTER_KAIRA,
            WARDEN_RODERIK,
            WARDEN_ENDRIGO,
            FISHER_EVERT,
        ]
    }

    async onAttackableAttackedEvent( data: AttackableAttackedEvent ): Promise<void> {
        let state: QuestState = QuestStateCache.getQuestState( data.attackerPlayerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.attackerPlayerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc
        let value: boolean = !NpcVariablesManager.get( data.targetId, this.getName() )

        switch ( data.targetNpcId ) {
            case NAMELESS_REVENANT:
                if ( value
                        && QuestHelper.hasQuestItems( player, ALEXANDRIAS_BOOK, LARAS_MEMO )
                        && !QuestHelper.hasQuestItem( player, AKLANTOTH_3RD_GEM ) ) {
                    NpcVariablesManager.set( data.targetId, this.getName(), true )
                    BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.I_ABSOLUTELY_CANNOT_GIVE_IT_TO_YOU_IT_IS_MY_PRECIOUS_JEWEL )
                }

                return

            case SKELETAL_MERCENARY:
                if ( value
                        && QuestHelper.hasQuestItem( player, LEOPOLDS_JOURNAL )
                        && !QuestHelper.hasQuestItems( player, AKLANTOTH_4TH_GEM, AKLANTOTH_5TH_GEM, AKLANTOTH_6TH_GEM ) ) {
                    NpcVariablesManager.set( data.targetId, this.getName(), true )
                    BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.I_ABSOLUTELY_CANNOT_GIVE_IT_TO_YOU_IT_IS_MY_PRECIOUS_JEWEL )
                }

                return

            case DREVANUL_PRINCE_ZERUEL:
                if ( QuestHelper.hasQuestItem( player, BRIMSTONE_1ST ) ) {
                    BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.ILL_TAKE_YOUR_LIVES_LATER )
                    await npc.deleteMe()

                    state.setConditionWithSound( 5, true )
                    return
                }

                if ( QuestHelper.hasQuestItems( player, ORIMS_INSTRUCTIONS, BRIMSTONE_2ND, SOULTRAP_CRYSTAL )
                        && value ) {
                    let weapon: L2ItemInstance = player.getActiveWeaponInstance()
                    let outcome: boolean = weapon && weapon.getId() === SWORD_OF_BINDING
                    if ( outcome ) {
                        NpcVariablesManager.set( data.targetId, this.getName(), true )
                        BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.THAT_SWORD_IS_REALLY )
                    }
                }

                return

        }
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( npc.getId() ) {
            case DIRE_WYRM:
                if ( QuestHelper.hasQuestItems( player, ALEXANDRIAS_BOOK, IKERS_LIST ) ) {
                    await this.rewardItem( player, DIRE_WYRM_FANG, 20, data.isChampion )
                }

                return

            case ENCHANTED_STONE_GOLEM:
                if ( QuestHelper.hasQuestItems( player, ALEXANDRIAS_BOOK, IKERS_LIST ) ) {
                    await this.rewardItem( player, ENCHANTED_STONE_GOLEM_HEARTSTONE, 20, data.isChampion )
                }

                return

            case LETO_LIZARDMAN:
            case LETO_LIZARDMAN_ARCHER:
            case LETO_LIZARDMAN_SOLDIER:
            case LETO_LIZARDMAN_WARRIOR:
            case LETO_LIZARDMAN_SHAMAN:
            case LETO_LIZARDMAN_OVERLORD:
                if ( QuestHelper.hasQuestItems( player, ALEXANDRIAS_BOOK, IKERS_LIST ) ) {
                    await this.rewardItem( player, LETO_LIZARDMAN_CHARM, 20, data.isChampion )
                }

                return

            case TAMLIN_ORC:
            case TAMLIN_ORC_ARCHER:
                if ( QuestHelper.hasQuestItem( player, VADINS_CRUCIFIX )
                        && Math.random() < QuestHelper.getAdjustedChance( TAMLIN_ORC_AMULET, 0.5, data.isChampion ) ) {
                    await this.rewardItem( player, TAMLIN_ORC_AMULET, 20, data.isChampion )
                }

                return

            case NAMELESS_REVENANT:
                if ( QuestHelper.hasQuestItems( player, ALEXANDRIAS_BOOK, LARAS_MEMO )
                        && !QuestHelper.hasQuestItem( player, AKLANTOTH_3RD_GEM ) ) {
                    await QuestHelper.takeSingleItem( player, LARAS_MEMO, 1 )
                    await QuestHelper.giveSingleItem( player, AKLANTOTH_3RD_GEM, 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

                    if ( QuestHelper.hasQuestItems( player, AKLANTOTH_1ST_GEM, AKLANTOTH_2ND_GEM, AKLANTOTH_4TH_GEM, AKLANTOTH_5TH_GEM, AKLANTOTH_6TH_GEM ) ) {
                        state.setConditionWithSound( 3 )
                    }
                }

                return

            case SKELETAL_MERCENARY:
                let canProceedWithMercenary = QuestHelper.hasQuestItem( player, LEOPOLDS_JOURNAL ) && !QuestHelper.hasQuestItems( player, AKLANTOTH_4TH_GEM, AKLANTOTH_5TH_GEM, AKLANTOTH_6TH_GEM )
                if ( !canProceedWithMercenary ) {
                    return
                }

                if ( !QuestHelper.hasQuestItem( player, AKLANTOTH_4TH_GEM ) ) {
                    await QuestHelper.giveSingleItem( player, AKLANTOTH_4TH_GEM, 1 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

                    return
                }

                if ( !QuestHelper.hasQuestItem( player, AKLANTOTH_5TH_GEM ) ) {
                    await QuestHelper.giveSingleItem( player, AKLANTOTH_5TH_GEM, 1 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

                    return
                }

                if ( !QuestHelper.hasQuestItem( player, AKLANTOTH_6TH_GEM ) ) {
                    await QuestHelper.takeSingleItem( player, LEOPOLDS_JOURNAL, 1 )
                    await QuestHelper.giveSingleItem( player, AKLANTOTH_6TH_GEM, 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

                    if ( QuestHelper.hasQuestItems( player, AKLANTOTH_1ST_GEM, AKLANTOTH_2ND_GEM, AKLANTOTH_3RD_GEM ) ) {
                        state.setConditionWithSound( 3 )
                    }
                }

                return

            case DREVANUL_PRINCE_ZERUEL:
                if ( QuestHelper.hasQuestItems( player, ORIMS_INSTRUCTIONS, BRIMSTONE_2ND, SWORD_OF_BINDING, SOULTRAP_CRYSTAL )
                        && npc.getKillingBlowWeapon() === SWORD_OF_BINDING ) {
                    BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.NO_I_HAVENT_COMPLETELY_FINISHED_THE_COMMAND_FOR_DESTRUCTION_AND_SLAUGHTER_YET )
                    await QuestHelper.takeMultipleItems( player, 1, SOULTRAP_CRYSTAL, BRIMSTONE_2ND )
                    await QuestHelper.rewardMultipleItems( player, 1, PURGATORY_KEY, ZERUEL_BIND_CRYSTAL )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                    state.setConditionWithSound( 10 )
                }

                return

        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.characterId ) as L2Npc

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( state.isCreated() ) {
                    state.startQuest()

                    await QuestHelper.giveSingleItem( player, ORIMS_DIAGRAM, 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                    if ( !PlayerVariablesManager.get( data.playerId, QuestVariables.secondClassDiamondReward ) ) {
                        PlayerVariablesManager.set( player.getObjectId(), QuestVariables.secondClassDiamondReward, true )

                        let amount: number = player.getClassId() === ClassIdValues.wizard.id ? 122 : 104
                        await QuestHelper.rewardSingleItem( player, DIMENSIONAL_DIAMOND, amount )

                        return this.getPath( '30630-08a.htm' )
                    }

                    return this.getPath( '30630-08.htm' )
                }

                return

            case '30630-04.htm':
            case '30630-06.htm':
            case '30630-07.htm':
            case '30630-12.htm':
            case '30630-13.htm':
            case '30630-20.htm':
            case '30630-21.htm':
            case '30098-02.htm':
            case '30110-02.htm':
            case '30417-02.htm':
                break

            case '30630-14.htm':
                if ( QuestHelper.hasQuestItem( player, ALEXANDRIAS_BOOK ) ) {
                    await QuestHelper.takeMultipleItems( player, 1, ALEXANDRIAS_BOOK, AKLANTOTH_1ST_GEM, AKLANTOTH_2ND_GEM, AKLANTOTH_3RD_GEM, AKLANTOTH_4TH_GEM, AKLANTOTH_5TH_GEM, AKLANTOTH_6TH_GEM )
                    await QuestHelper.giveSingleItem( player, BRIMSTONE_1ST, 1 )

                    state.setConditionWithSound( 4, true )
                    QuestHelper.addSpawnAtLocation( DREVANUL_PRINCE_ZERUEL, npc, true, 0 )
                    break
                }

                return

            case '30630-16.htm':
                if ( QuestHelper.hasQuestItem( player, BRIMSTONE_1ST ) ) {
                    await QuestHelper.takeSingleItem( player, BRIMSTONE_1ST, 1 )
                    await QuestHelper.giveMultipleItems( player, 1, ORIMS_INSTRUCTIONS, ORIMS_1ST_LETTER, ORIMS_2ND_LETTER )

                    state.setConditionWithSound( 6, true )
                    break
                }

                return

            case '30630-22.htm':
                if ( QuestHelper.hasQuestItem( player, ZERUEL_BIND_CRYSTAL ) ) {
                    await QuestHelper.giveAdena( player, 372154, true )
                    await QuestHelper.giveSingleItem( player, MARK_OF_WITCHCRAFT, 1 )
                    await QuestHelper.addExpAndSp( player, 2058244, 141240 )
                    await state.exitQuest( false, true )

                    player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )
                    break
                }

                return

            case '30063-02.htm':
                await QuestHelper.giveSingleItem( player, LARAS_MEMO, 1 )
                break

            case '30098-03.htm':
                if ( QuestHelper.hasQuestItem( player, ORIMS_DIAGRAM ) ) {
                    await QuestHelper.takeSingleItem( player, ORIMS_DIAGRAM, 1 )
                    await QuestHelper.giveSingleItem( player, ALEXANDRIAS_BOOK, 1 )

                    state.setConditionWithSound( 2, true )
                    break
                }

                return

            case '30110-03.htm':
                await QuestHelper.giveSingleItem( player, IKERS_LIST, 1 )
                break

            case '30110-08.htm':
                await QuestHelper.takeSingleItem( player, ORIMS_2ND_LETTER, 1 )
                await QuestHelper.giveSingleItem( player, IKERS_AMULET, 1 )
                await QuestHelper.giveSingleItem( player, SOULTRAP_CRYSTAL, 1 )

                if ( QuestHelper.hasQuestItem( player, SWORD_OF_BINDING ) ) {
                    state.setConditionWithSound( 7, true )
                }

                break

            case '30314-02.htm':
                await QuestHelper.giveSingleItem( player, NESTLES_MEMO, 1 )
                break

            case '30417-03.htm':
                if ( QuestHelper.hasQuestItem( player, ORIMS_1ST_LETTER ) ) {
                    await QuestHelper.takeSingleItem( player, ORIMS_1ST_LETTER, 1 )
                    await QuestHelper.giveSingleItem( player, SIR_VASPERS_LETTER, 1 )

                    break
                }

                return

            case '30435-02.htm':
                if ( QuestHelper.hasQuestItem( player, NESTLES_MEMO ) ) {
                    await QuestHelper.takeSingleItem( player, NESTLES_MEMO, 1 )
                    await QuestHelper.giveSingleItem( player, LEOPOLDS_JOURNAL, 1 )

                    break
                }

                return

            case '30476-02.htm':
                await QuestHelper.giveSingleItem( player, AKLANTOTH_2ND_GEM, 1 )
                if ( QuestHelper.hasQuestItems( player,
                        AKLANTOTH_1ST_GEM,
                        AKLANTOTH_3RD_GEM,
                        AKLANTOTH_4TH_GEM,
                        AKLANTOTH_5TH_GEM,
                        AKLANTOTH_6TH_GEM ) ) {
                    state.setConditionWithSound( 3, true )
                }

                break

            case '30633-02.htm':
                await QuestHelper.giveSingleItem( player, BRIMSTONE_2ND, 1 )
                state.setConditionWithSound( 9, true )

                if ( npc.getSummonedNpcCount() < 1 ) {
                    QuestHelper.addSummonedSpawnAtLocation( npc, DREVANUL_PRINCE_ZERUEL, DREVANUL_PRINCE_ZERUEL_SPAWN, false, 0 )
                }

                break


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== SHADOW_ORIM ) {
                    break
                }

                if ( ![ ClassIdValues.wizard.id, ClassIdValues.knight.id, ClassIdValues.palusKnight.id ].includes( player.getClassId() ) ) {
                    return this.getPath( '30630-01.htm' )
                }

                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '30630-02.htm' )
                }

                if ( player.getClassId() === ClassIdValues.wizard.id ) {
                    return this.getPath( '30630-03.htm' )
                }

                return this.getPath( '30630-05.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case SHADOW_ORIM:
                        if ( QuestHelper.hasQuestItem( player, ORIMS_DIAGRAM ) ) {
                            return this.getPath( '30630-09.htm' )
                        }

                        if ( QuestHelper.hasQuestItem( player, ALEXANDRIAS_BOOK ) ) {
                            if ( QuestHelper.hasQuestItems( player, AKLANTOTH_1ST_GEM, AKLANTOTH_2ND_GEM, AKLANTOTH_3RD_GEM, AKLANTOTH_4TH_GEM, AKLANTOTH_5TH_GEM, AKLANTOTH_6TH_GEM ) ) {
                                return this.getPath( '30630-11.htm' )
                            }

                            return this.getPath( '30630-10.htm' )
                        }

                        if ( QuestHelper.hasQuestItem( player, BRIMSTONE_1ST ) ) {
                            return this.getPath( '30630-15.htm' )
                        }

                        if ( QuestHelper.hasQuestItem( player, ORIMS_INSTRUCTIONS ) && !QuestHelper.hasAtLeastOneQuestItem( player, SWORD_OF_BINDING, SOULTRAP_CRYSTAL ) ) {
                            return this.getPath( '30630-17.htm' )
                        }

                        if ( QuestHelper.hasQuestItems( player, SWORD_OF_BINDING, SOULTRAP_CRYSTAL ) ) {
                            state.setConditionWithSound( 8, true )
                            return this.getPath( '30630-18.htm' )
                        }

                        if ( QuestHelper.hasQuestItems( player, SWORD_OF_BINDING, ZERUEL_BIND_CRYSTAL ) ) {
                            return this.getPath( '30630-19.htm' )
                        }

                        break

                    case GROCER_LARA:
                        if ( QuestHelper.hasQuestItem( player, ALEXANDRIAS_BOOK ) ) {
                            if ( !QuestHelper.hasAtLeastOneQuestItem( player, LARAS_MEMO, AKLANTOTH_3RD_GEM ) ) {
                                return this.getPath( '30063-01.htm' )
                            }

                            if ( !QuestHelper.hasQuestItem( player, AKLANTOTH_3RD_GEM ) && QuestHelper.hasQuestItem( player, LARAS_MEMO ) ) {
                                return this.getPath( '30063-03.htm' )
                            }

                            if ( !QuestHelper.hasQuestItem( player, LARAS_MEMO ) && QuestHelper.hasQuestItem( player, AKLANTOTH_3RD_GEM ) ) {
                                return this.getPath( '30063-04.htm' )
                            }

                            break
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, BRIMSTONE_1ST, ORIMS_INSTRUCTIONS ) ) {
                            return this.getPath( '30063-05.htm' )
                        }

                        break

                    case TRADER_ALEXANDRIA:
                        if ( QuestHelper.hasQuestItem( player, ORIMS_DIAGRAM ) ) {
                            return this.getPath( '30098-01.htm' )
                        }

                        if ( QuestHelper.hasQuestItem( player, ALEXANDRIAS_BOOK ) ) {
                            return this.getPath( '30098-04.htm' )
                        }

                        if ( QuestHelper.hasQuestItems( player, ORIMS_INSTRUCTIONS, BRIMSTONE_1ST ) ) {
                            return this.getPath( '30098-05.htm' )
                        }

                        break

                    case MAGISTER_IKER:
                        if ( QuestHelper.hasQuestItem( player, ALEXANDRIAS_BOOK ) ) {
                            if ( !QuestHelper.hasAtLeastOneQuestItem( player, IKERS_LIST, AKLANTOTH_1ST_GEM ) ) {
                                return this.getPath( '30110-01.htm' )
                            }

                            if ( QuestHelper.hasQuestItem( player, IKERS_LIST ) ) {
                                if ( QuestHelper.getQuestItemsCount( player, DIRE_WYRM_FANG ) >= 20
                                        && QuestHelper.getQuestItemsCount( player, LETO_LIZARDMAN_CHARM ) >= 20
                                        && QuestHelper.getQuestItemsCount( player, ENCHANTED_STONE_GOLEM_HEARTSTONE ) >= 20 ) {
                                    await QuestHelper.takeMultipleItems( player, -1, IKERS_LIST, DIRE_WYRM_FANG, LETO_LIZARDMAN_CHARM, ENCHANTED_STONE_GOLEM_HEARTSTONE )
                                    await QuestHelper.giveSingleItem( player, AKLANTOTH_1ST_GEM, 1 )

                                    if ( QuestHelper.hasQuestItems( player, AKLANTOTH_2ND_GEM, AKLANTOTH_3RD_GEM, AKLANTOTH_4TH_GEM, AKLANTOTH_5TH_GEM, AKLANTOTH_6TH_GEM ) ) {
                                        state.setConditionWithSound( 3, true )
                                    }

                                    return this.getPath( '30110-05.htm' )
                                }

                                return this.getPath( '30110-04.htm' )
                            }

                            if ( !QuestHelper.hasQuestItem( player, IKERS_LIST ) && QuestHelper.hasQuestItem( player, AKLANTOTH_1ST_GEM ) ) {
                                return this.getPath( '30110-06.htm' )
                            }

                            break
                        }

                        if ( QuestHelper.hasQuestItem( player, ORIMS_INSTRUCTIONS ) ) {
                            if ( !QuestHelper.hasAtLeastOneQuestItem( player, SOULTRAP_CRYSTAL, ZERUEL_BIND_CRYSTAL ) ) {
                                return this.getPath( '30110-07.htm' )
                            }

                            if ( !QuestHelper.hasQuestItem( player, ZERUEL_BIND_CRYSTAL )
                                    && QuestHelper.hasQuestItem( player, SOULTRAP_CRYSTAL ) ) {
                                return this.getPath( '30110-09.htm' )
                            }

                            if ( !QuestHelper.hasQuestItem( player, SOULTRAP_CRYSTAL )
                                    && QuestHelper.hasQuestItem( player, ZERUEL_BIND_CRYSTAL ) ) {
                                return this.getPath( '30110-10.htm' )
                            }
                        }

                        break

                    case PRIEST_VADIN:
                        if ( QuestHelper.hasQuestItems( player, ORIMS_INSTRUCTIONS, SIR_VASPERS_LETTER ) ) {
                            await QuestHelper.takeSingleItem( player, SIR_VASPERS_LETTER, 1 )
                            await QuestHelper.giveSingleItem( player, VADINS_CRUCIFIX, 1 )

                            return this.getPath( '30188-01.htm' )
                        }

                        if ( QuestHelper.hasQuestItem( player, VADINS_CRUCIFIX ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, TAMLIN_ORC_AMULET ) < 20 ) {
                                return this.getPath( '30188-02.htm' )
                            }

                            await QuestHelper.takeMultipleItems( player, -1, VADINS_CRUCIFIX, TAMLIN_ORC_AMULET )
                            await QuestHelper.giveSingleItem( player, VADINS_SANCTIONS, 1 )

                            return this.getPath( '30188-03.htm' )
                        }

                        if ( QuestHelper.hasQuestItem( player, ORIMS_INSTRUCTIONS ) ) {
                            if ( QuestHelper.hasQuestItem( player, VADINS_SANCTIONS ) ) {
                                return this.getPath( '30188-04.htm' )
                            }

                            if ( QuestHelper.hasQuestItem( player, SWORD_OF_BINDING ) ) {
                                return this.getPath( '30188-05.htm' )
                            }
                        }

                        break

                    case TRADER_NESTLE:
                        if ( !QuestHelper.hasQuestItem( player, ALEXANDRIAS_BOOK ) ) {
                            break
                        }

                        let hasItems: boolean = QuestHelper.hasAtLeastOneQuestItem( player, LEOPOLDS_JOURNAL, AKLANTOTH_4TH_GEM, AKLANTOTH_5TH_GEM, AKLANTOTH_6TH_GEM )
                        let hasMemo: boolean = QuestHelper.hasQuestItem( player, NESTLES_MEMO )
                        if ( !hasItems || !hasMemo ) {
                            return this.getPath( '30314-01.htm' )
                        }

                        if ( hasMemo && !QuestHelper.hasQuestItem( player, LEOPOLDS_JOURNAL ) ) {
                            return this.getPath( '30314-03.htm' )
                        }

                        if ( !hasMemo && hasItems ) {
                            return this.getPath( '30314-04.htm' )
                        }

                        break

                    case SIR_KLAUS_VASPER:
                        if ( !QuestHelper.hasQuestItem( player, ORIMS_INSTRUCTIONS ) ) {
                            break
                        }

                        if ( QuestHelper.hasQuestItem( player, ORIMS_1ST_LETTER ) ) {
                            return this.getPath( '30417-01.htm' )
                        }

                        if ( QuestHelper.hasQuestItem( player, SIR_VASPERS_LETTER ) ) {
                            return this.getPath( '30417-04.htm' )
                        }

                        if ( QuestHelper.hasQuestItem( player, VADINS_SANCTIONS ) ) {
                            await QuestHelper.giveSingleItem( player, SWORD_OF_BINDING, 1 )
                            await QuestHelper.takeSingleItem( player, VADINS_SANCTIONS, 1 )

                            if ( QuestHelper.hasQuestItem( player, SOULTRAP_CRYSTAL ) ) {
                                state.setConditionWithSound( 7, true )
                            }

                            return this.getPath( '30417-05.htm' )
                        }

                        if ( QuestHelper.hasQuestItem( player, SWORD_OF_BINDING ) ) {
                            return this.getPath( '30417-06.htm' )
                        }

                        break

                    case LEOPOLD:
                        if ( QuestHelper.hasQuestItem( player, ALEXANDRIAS_BOOK ) ) {
                            let hasMemo: boolean = QuestHelper.hasQuestItem( player, NESTLES_MEMO )
                            let hasJournal: boolean = QuestHelper.hasQuestItem( player, LEOPOLDS_JOURNAL )

                            if ( hasMemo && !hasJournal ) {
                                return this.getPath( '30435-01.htm' )
                            }

                            if ( hasJournal && !hasMemo ) {
                                return this.getPath( '30435-03.htm' )
                            }

                            if ( QuestHelper.hasQuestItems( player, AKLANTOTH_4TH_GEM, AKLANTOTH_5TH_GEM, AKLANTOTH_6TH_GEM ) ) {
                                return this.getPath( '30435-04.htm' )
                            }

                            break
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, BRIMSTONE_1ST, ORIMS_INSTRUCTIONS ) ) {
                            return this.getPath( '30435-05.htm' )
                        }

                        break

                    case MAGISTER_KAIRA:
                        if ( QuestHelper.hasQuestItem( player, ALEXANDRIAS_BOOK ) ) {
                            if ( !QuestHelper.hasQuestItem( player, AKLANTOTH_2ND_GEM ) ) {
                                return this.getPath( '30476-01.htm' )
                            }

                            return this.getPath( '30476-03.htm' )
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, BRIMSTONE_1ST, ORIMS_INSTRUCTIONS ) ) {
                            return this.getPath( '30476-04.htm' )
                        }

                        break

                    case WARDEN_RODERIK:
                        if ( QuestHelper.hasQuestItem( player, ALEXANDRIAS_BOOK )
                                && QuestHelper.hasAtLeastOneQuestItem( player, LARAS_MEMO, AKLANTOTH_3RD_GEM ) ) {
                            return this.getPath( '30631-01.htm' )
                        }

                        break

                    case WARDEN_ENDRIGO:
                        if ( QuestHelper.hasQuestItem( player, ALEXANDRIAS_BOOK )
                                && QuestHelper.hasAtLeastOneQuestItem( player, LARAS_MEMO, AKLANTOTH_3RD_GEM ) ) {
                            return this.getPath( '30632-01.htm' )
                        }

                        break

                    case FISHER_EVERT:
                        if ( !QuestHelper.hasQuestItem( player, ORIMS_INSTRUCTIONS ) ) {
                            break
                        }

                        if ( QuestHelper.hasQuestItems( player, SOULTRAP_CRYSTAL, SWORD_OF_BINDING )
                                && !QuestHelper.hasQuestItem( player, BRIMSTONE_2ND ) ) {
                            return this.getPath( '30633-01.htm' )
                        }

                        if ( QuestHelper.hasQuestItems( player, SOULTRAP_CRYSTAL, BRIMSTONE_2ND )
                                && !QuestHelper.hasQuestItem( player, ZERUEL_BIND_CRYSTAL ) ) {

                            let npc = L2World.getObjectById( data.characterId ) as L2Npc
                            if ( npc.getSummonedNpcCount() < 1 ) {
                                QuestHelper.addSummonedSpawnAtLocation( npc, DREVANUL_PRINCE_ZERUEL, DREVANUL_PRINCE_ZERUEL_SPAWN, false, 0 )
                            }

                            return this.getPath( '30633-02.htm' )
                        }

                        if ( QuestHelper.hasQuestItem( player, ZERUEL_BIND_CRYSTAL )
                                && !QuestHelper.hasAtLeastOneQuestItem( player, SOULTRAP_CRYSTAL, BRIMSTONE_2ND ) ) {
                            return this.getPath( '30633-03.htm' )
                        }

                        break

                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === SHADOW_ORIM ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }
        return QuestHelper.getNoQuestMessagePath()
    }

    async rewardItem( player: L2PcInstance, itemId: number, maximumCount: number, isFromChampion: boolean ): Promise<void> {
        if ( QuestHelper.getQuestItemsCount( player, itemId ) >= maximumCount ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, itemId, 1, isFromChampion )

        if ( QuestHelper.getQuestItemsCount( player, itemId ) >= maximumCount ) {
            return player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
        }

        return player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }
}