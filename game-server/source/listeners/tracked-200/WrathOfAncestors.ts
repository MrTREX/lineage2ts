import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'

const LIVINA = 30572
const GRAVE_ROBBERS_HEAD = 1474
const minimumLevel = 5

export class WrathOfAncestors extends ListenerLogic {
    constructor() {
        super( 'Q00272_WrathOfAncestors', 'listeners/tracked-200/WrathOfAncestors.ts' )
        this.questId = 272
        this.questItemIds = [
            GRAVE_ROBBERS_HEAD,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            20319, // Goblin Grave Robber
            20320, // Goblin Tomb Raider Leader
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00272_WrathOfAncestors'
    }

    getQuestStartIds(): Array<number> {
        return [ LIVINA ]
    }

    getTalkIds(): Array<number> {
        return [ LIVINA ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isCondition( 1 ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        await QuestHelper.rewardSingleQuestItem( player, GRAVE_ROBBERS_HEAD, 1, data.isChampion )

        if ( QuestHelper.getQuestItemsCount( player, GRAVE_ROBBERS_HEAD ) >= 50 ) {
            state.setConditionWithSound( 2, true )
            return
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || data.eventName !== '30572-04.htm' ) {
            return
        }

        state.startQuest()

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getRace() !== Race.ORC ) {
                    return this.getPath( '30572-01.htm' )
                }

                return this.getPath( player.getLevel() >= minimumLevel ? '30572-03.htm' : '30572-02.htm' )

            case QuestStateValues.STARTED: {
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '30572-05.html' )

                    case 2:
                        await QuestHelper.giveAdena( player, 1500, true )
                        await state.exitQuest( true, true )
                        return this.getPath( '30572-06.html' )
                }

                break
            }
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}