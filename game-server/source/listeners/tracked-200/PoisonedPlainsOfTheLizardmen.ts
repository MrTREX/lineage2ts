import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const MOUEN = 30196
const JOHNNY = 32744
const minimumLevel = 82

export class PoisonedPlainsOfTheLizardmen extends ListenerLogic {
    constructor() {
        super( 'Q00249_PoisonedPlainsOfTheLizardmen', 'listeners/tracked-200/PoisonedPlainsOfTheLizardmen.ts' )
        this.questId = 249
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00249_PoisonedPlainsOfTheLizardmen'
    }

    getQuestStartIds(): Array<number> {
        return [ MOUEN ]
    }

    getTalkIds(): Array<number> {
        return [ MOUEN, JOHNNY ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30196-03.htm':
                state.startQuest()
                break

            case '32744-03.htm':
                let player = L2World.getPlayer( data.playerId )
                await QuestHelper.giveAdena( player, 83056, true )
                await QuestHelper.addExpAndSp( player, 477496, 58743 )
                await state.exitQuest( false, true )
                break
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case MOUEN:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() >= minimumLevel ? '30196-01.htm' : '30196-00.htm' )

                    case QuestStateValues.STARTED:
                        if ( state.isCondition( 1 ) ) {
                            return this.getPath( '30196-04.htm' )
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return this.getPath( '30196-05.htm' )
                }

                break

            case JOHNNY:
                if ( state.isCondition( 1 ) ) {
                    return this.getPath( '32744-01.htm' )
                }

                if ( state.isCompleted() ) {
                    return this.getPath( '32744-04.htm' )
                }

                break
        }
        return QuestHelper.getNoQuestMessagePath()
    }
}