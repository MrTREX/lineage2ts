import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'
import { QuestVariables } from '../helpers/QuestVariables'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'
import { teleportCharacterToGeometryCoordinates } from '../../gameService/helpers/TeleportHelper'
import { GeometryId } from '../../gameService/enums/GeometryId'

const IRON_GATES_LOCKIRIN = 30531
const GOLDEN_WHEELS_SPIRON = 30532
const SILVER_SCALES_BALANKI = 30533
const BRONZE_KEYS_KEEF = 30534
const GRAY_PILLAR_MEMBER_FILAUR = 30535
const BLACK_ANVILS_ARIN = 30536
const MASTER_TOMA = 30556
const CHIEF_CROTO = 30671
const JAILER_DUBABAH = 30672
const RESEARCHER_LORAIN = 30673

const RECOMMENDATION_OF_BALANKI = 2864
const RECOMMENDATION_OF_FILAUR = 2865
const RECOMMENDATION_OF_ARIN = 2866
const LETTER_OF_SOLDER_DERACHMENT = 2868
const PAINT_OF_KAMURU = 2869
const NECKLACE_OF_KAMUTU = 2870
const PAINT_OF_TELEPORT_DEVICE = 2871
const TELEPORT_DEVICE = 2872
const ARCHITECTURE_OF_CRUMA = 2873
const REPORT_OF_CRUMA = 2874
const INGREDIENTS_OF_ANTIDOTE = 2875
const STINGER_WASP_NEEDLE = 2876
const MARSH_SPIDERS_WEB = 2877
const BLOOD_OF_LEECH = 2878
const BROKEN_TELEPORT_DEVICE = 2916

const MARK_OF_MAESTRO = 2867
const DIMENSIONAL_DIAMOND = 7562

const KING_BUGBEAR = 20150
const GIANT_MIST_LEECH = 20225
const STINGER_WASP = 20229
const MARSH_SPIDER = 20233

const EVIL_EYE_LORD = 27133
const minimumLevel = 39

const eventNames = {
    spawnBugbear: 's',
}

export class TestOfTheMaestro extends ListenerLogic {
    constructor() {
        super( 'Q00231_TestOfTheMaestro', 'listeners/tracked-200/TestOfTheMaestro.ts' )
        this.questId = 231
        this.questItemIds = [
            RECOMMENDATION_OF_BALANKI,
            RECOMMENDATION_OF_FILAUR,
            RECOMMENDATION_OF_ARIN,
            LETTER_OF_SOLDER_DERACHMENT,
            PAINT_OF_KAMURU,
            NECKLACE_OF_KAMUTU,
            PAINT_OF_TELEPORT_DEVICE,
            TELEPORT_DEVICE,
            ARCHITECTURE_OF_CRUMA,
            REPORT_OF_CRUMA,
            INGREDIENTS_OF_ANTIDOTE,
            STINGER_WASP_NEEDLE,
            MARSH_SPIDERS_WEB,
            BLOOD_OF_LEECH,
            BROKEN_TELEPORT_DEVICE,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            KING_BUGBEAR,
            GIANT_MIST_LEECH,
            STINGER_WASP,
            MARSH_SPIDER,
            EVIL_EYE_LORD,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00231_TestOfTheMaestro'
    }

    getQuestStartIds(): Array<number> {
        return [ IRON_GATES_LOCKIRIN ]
    }

    getTalkIds(): Array<number> {
        return [
            IRON_GATES_LOCKIRIN,
            GOLDEN_WHEELS_SPIRON,
            SILVER_SCALES_BALANKI,
            BRONZE_KEYS_KEEF,
            GRAY_PILLAR_MEMBER_FILAUR,
            BLACK_ANVILS_ARIN,
            MASTER_TOMA,
            CHIEF_CROTO,
            JAILER_DUBABAH,
            RESEARCHER_LORAIN,
        ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case GIANT_MIST_LEECH:
                if ( state.isMemoState( 4 )
                        && QuestHelper.hasQuestItem( player, INGREDIENTS_OF_ANTIDOTE )
                        && QuestHelper.getQuestItemsCount( player, BLOOD_OF_LEECH ) < 10 ) {
                    await this.rewardItems( player, BLOOD_OF_LEECH, data.isChampion )
                }
                return

            case STINGER_WASP:
                if ( state.isMemoState( 4 )
                        && QuestHelper.hasQuestItem( player, INGREDIENTS_OF_ANTIDOTE )
                        && QuestHelper.getQuestItemsCount( player, STINGER_WASP_NEEDLE ) < 10 ) {
                    await this.rewardItems( player, STINGER_WASP_NEEDLE, data.isChampion )
                }

                return

            case MARSH_SPIDER:
                if ( state.isMemoState( 4 )
                        && QuestHelper.hasQuestItem( player, INGREDIENTS_OF_ANTIDOTE )
                        && QuestHelper.getQuestItemsCount( player, MARSH_SPIDERS_WEB ) < 10 ) {
                    await this.rewardItems( player, MARSH_SPIDERS_WEB, data.isChampion )
                }

                return

            case EVIL_EYE_LORD:
                if ( state.isMemoState( 2 )
                        && QuestHelper.hasQuestItem( player, PAINT_OF_KAMURU )
                        && !QuestHelper.hasQuestItem( player, NECKLACE_OF_KAMUTU ) ) {
                    await QuestHelper.giveSingleItem( player, NECKLACE_OF_KAMUTU, 1 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                }

                return

        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( state.isCreated() ) {
                    state.startQuest()
                    state.setMemoState( 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                    if ( !PlayerVariablesManager.get( data.playerId, QuestVariables.secondClassDiamondReward ) ) {
                        PlayerVariablesManager.set( player.getObjectId(), QuestVariables.secondClassDiamondReward, true )

                        await QuestHelper.rewardSingleItem( player, DIMENSIONAL_DIAMOND, 23 )

                        return this.getPath( '30531-04a.htm' )
                    }

                    return this.getPath( '30531-04.htm' )
                }

                return

            case '30533-02.html':
                state.setMemoState( 2 )
                break

            case '30556-02.html':
            case '30556-03.html':
            case '30556-04.html':
                break

            case '30556-05.html':
                if ( !QuestHelper.hasQuestItem( player, PAINT_OF_TELEPORT_DEVICE ) ) {
                    return
                }

                await QuestHelper.giveSingleItem( player, BROKEN_TELEPORT_DEVICE, 1 )
                await QuestHelper.takeSingleItem( player, PAINT_OF_TELEPORT_DEVICE, 1 )
                await teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, 140352, -194133, -3146 )

                this.startQuestTimer( 'SPAWN_KING_BUGBEAR', 5000, data.characterId, data.playerId )
                break

            case '30671-02.html':
                await QuestHelper.giveSingleItem( player, PAINT_OF_KAMURU, 1 )
                break

            case '30673-04.html':
                if ( QuestHelper.hasQuestItem( player, INGREDIENTS_OF_ANTIDOTE )
                        && QuestHelper.getQuestItemsCount( player, STINGER_WASP_NEEDLE ) >= 10
                        && QuestHelper.getQuestItemsCount( player, MARSH_SPIDERS_WEB ) >= 10
                        && QuestHelper.getQuestItemsCount( player, BLOOD_OF_LEECH ) >= 10 ) {
                    await QuestHelper.giveSingleItem( player, REPORT_OF_CRUMA, 1 )
                    await QuestHelper.takeMultipleItems( player, -1, STINGER_WASP_NEEDLE, MARSH_SPIDERS_WEB, BLOOD_OF_LEECH, INGREDIENTS_OF_ANTIDOTE )
                    break
                }

                return

            case eventNames.spawnBugbear:
                QuestHelper.addAttackDesire( QuestHelper.addGenericSpawn( null, KING_BUGBEAR, 140395, -194147, -3146, 0, false, 200000, false ), player )
                QuestHelper.addAttackDesire( QuestHelper.addGenericSpawn( null, KING_BUGBEAR, 140395, -194147, -3146, 0, false, 200000, false ), player )
                QuestHelper.addAttackDesire( QuestHelper.addGenericSpawn( null, KING_BUGBEAR, 140395, -194147, -3146, 0, false, 200000, false ), player )
                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== IRON_GATES_LOCKIRIN ) {
                    break
                }

                if ( player.getClassId() !== ClassIdValues.artisan.id ) {
                    return this.getPath( '30531-02.html' )
                }

                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '30531-01.html' )
                }

                return this.getPath( '30531-03.htm' )

            case QuestStateValues.STARTED:
                let memoState = state.getMemoState()

                switch ( data.characterNpcId ) {
                    case IRON_GATES_LOCKIRIN:
                        let hasLockirinItems = QuestHelper.hasQuestItems( player, RECOMMENDATION_OF_BALANKI, RECOMMENDATION_OF_FILAUR, RECOMMENDATION_OF_ARIN )
                        if ( memoState >= 1 && !hasLockirinItems ) {
                            return this.getPath( '30531-05.html' )
                        }

                        if ( hasLockirinItems ) {
                            await QuestHelper.giveAdena( player, 372154, true )
                            await QuestHelper.giveSingleItem( player, MARK_OF_MAESTRO, 1 )
                            await QuestHelper.addExpAndSp( player, 2085244, 141240 )
                            await state.exitQuest( false, true )

                            player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )
                            return this.getPath( '30531-06.html' )
                        }

                        break

                    case GOLDEN_WHEELS_SPIRON:
                        return this.getPath( '30532-01.html' )

                    case SILVER_SCALES_BALANKI:
                        if ( memoState === 1 && !QuestHelper.hasQuestItem( player, RECOMMENDATION_OF_BALANKI ) ) {
                            return this.getPath( '30533-01.html' )
                        }

                        if ( memoState === 2 ) {
                            if ( !QuestHelper.hasQuestItem( player, LETTER_OF_SOLDER_DERACHMENT ) ) {
                                return this.getPath( '30533-03.html' )
                            }

                            await QuestHelper.giveSingleItem( player, RECOMMENDATION_OF_BALANKI, 1 )
                            await QuestHelper.takeSingleItem( player, LETTER_OF_SOLDER_DERACHMENT, 1 )

                            if ( QuestHelper.hasQuestItems( player, RECOMMENDATION_OF_ARIN, RECOMMENDATION_OF_FILAUR ) ) {
                                state.setConditionWithSound( 2, true )
                            }

                            state.setMemoState( 1 )
                            return this.getPath( '30533-04.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, RECOMMENDATION_OF_BALANKI ) ) {
                            return this.getPath( '30533-05.html' )
                        }

                        break

                    case BRONZE_KEYS_KEEF:
                        return this.getPath( '30534-01.html' )

                    case GRAY_PILLAR_MEMBER_FILAUR:
                        if ( memoState === 1 && !QuestHelper.hasQuestItem( player, RECOMMENDATION_OF_FILAUR ) ) {
                            await QuestHelper.giveSingleItem( player, ARCHITECTURE_OF_CRUMA, 1 )

                            state.setMemoState( 4 )
                            return this.getPath( '30535-01.html' )
                        }

                        if ( memoState === 4 ) {
                            if ( QuestHelper.hasQuestItem( player, ARCHITECTURE_OF_CRUMA )
                                    && !QuestHelper.hasQuestItem( player, REPORT_OF_CRUMA ) ) {
                                return this.getPath( '30535-02.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, REPORT_OF_CRUMA )
                                    && !QuestHelper.hasQuestItem( player, ARCHITECTURE_OF_CRUMA ) ) {
                                await QuestHelper.giveSingleItem( player, RECOMMENDATION_OF_FILAUR, 1 )
                                await QuestHelper.takeSingleItem( player, REPORT_OF_CRUMA, 1 )

                                if ( QuestHelper.hasQuestItems( player, RECOMMENDATION_OF_BALANKI, RECOMMENDATION_OF_ARIN ) ) {
                                    state.setConditionWithSound( 2, true )
                                }

                                state.setMemoState( 1 )
                                return this.getPath( '30535-03.html' )
                            }
                        }

                        if ( QuestHelper.hasQuestItem( player, RECOMMENDATION_OF_FILAUR ) ) {
                            return this.getPath( '30535-04.html' )
                        }

                        break

                    case BLACK_ANVILS_ARIN:
                        if ( memoState === 1 && !QuestHelper.hasQuestItem( player, RECOMMENDATION_OF_ARIN ) ) {
                            await QuestHelper.giveSingleItem( player, PAINT_OF_TELEPORT_DEVICE, 1 )

                            state.setMemoState( 3 )
                            return this.getPath( '30536-01.html' )
                        }

                        if ( memoState === 3 ) {
                            if ( QuestHelper.hasQuestItem( player, PAINT_OF_TELEPORT_DEVICE )
                                    && !QuestHelper.hasQuestItem( player, TELEPORT_DEVICE ) ) {
                                return this.getPath( '30536-02.html' )
                            }

                            if ( QuestHelper.getQuestItemsCount( player, TELEPORT_DEVICE ) >= 5 ) {
                                await QuestHelper.giveSingleItem( player, RECOMMENDATION_OF_ARIN, 1 )
                                await QuestHelper.takeSingleItem( player, TELEPORT_DEVICE, -1 )

                                if ( QuestHelper.hasQuestItems( player, RECOMMENDATION_OF_BALANKI, RECOMMENDATION_OF_FILAUR ) ) {
                                    state.setConditionWithSound( 2, true )
                                }

                                state.setMemoState( 1 )
                                return this.getPath( '30536-03.html' )
                            }
                        }

                        if ( QuestHelper.hasQuestItem( player, RECOMMENDATION_OF_ARIN ) ) {
                            return this.getPath( '30536-04.html' )
                        }

                        break

                    case MASTER_TOMA:
                        if ( memoState !== 3 ) {
                            break
                        }

                        if ( QuestHelper.hasQuestItem( player, PAINT_OF_TELEPORT_DEVICE ) ) {
                            return this.getPath( '30556-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, BROKEN_TELEPORT_DEVICE ) ) {
                            await QuestHelper.giveSingleItem( player, TELEPORT_DEVICE, 5 )
                            await QuestHelper.takeSingleItem( player, BROKEN_TELEPORT_DEVICE, 1 )

                            return this.getPath( '30556-06.html' )
                        }

                        if ( QuestHelper.getQuestItemsCount( player, TELEPORT_DEVICE ) === 5 ) {
                            return this.getPath( '30556-07.html' )
                        }

                        break

                    case CHIEF_CROTO:
                        if ( memoState == 2
                                && !QuestHelper.hasAtLeastOneQuestItem( player, PAINT_OF_KAMURU, NECKLACE_OF_KAMUTU, LETTER_OF_SOLDER_DERACHMENT ) ) {
                            return this.getPath( '30671-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, PAINT_OF_KAMURU )
                                && !QuestHelper.hasQuestItem( player, NECKLACE_OF_KAMUTU ) ) {
                            return this.getPath( '30671-03.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, NECKLACE_OF_KAMUTU ) ) {
                            await QuestHelper.giveSingleItem( player, LETTER_OF_SOLDER_DERACHMENT, 1 )
                            await QuestHelper.takeMultipleItems( player, 1, NECKLACE_OF_KAMUTU, PAINT_OF_KAMURU )

                            return this.getPath( '30671-04.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, LETTER_OF_SOLDER_DERACHMENT ) ) {
                            return this.getPath( '30671-05.html' )
                        }

                        break

                    case JAILER_DUBABAH:
                        if ( QuestHelper.hasQuestItem( player, PAINT_OF_KAMURU ) ) {
                            return this.getPath( '30672-01.html' )
                        }

                        break

                    case RESEARCHER_LORAIN:
                        if ( memoState !== 4 ) {
                            break
                        }

                        if ( QuestHelper.hasQuestItem( player, ARCHITECTURE_OF_CRUMA )
                                && !QuestHelper.hasAtLeastOneQuestItem( player, INGREDIENTS_OF_ANTIDOTE, REPORT_OF_CRUMA ) ) {
                            await QuestHelper.giveSingleItem( player, INGREDIENTS_OF_ANTIDOTE, 1 )
                            await QuestHelper.takeSingleItem( player, ARCHITECTURE_OF_CRUMA, 1 )

                            return this.getPath( '30673-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, INGREDIENTS_OF_ANTIDOTE )
                                && !QuestHelper.hasQuestItem( player, REPORT_OF_CRUMA ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, STINGER_WASP_NEEDLE ) >= 10
                                    && QuestHelper.getQuestItemsCount( player, MARSH_SPIDERS_WEB ) >= 10
                                    && QuestHelper.getQuestItemsCount( player, BLOOD_OF_LEECH ) >= 10 ) {
                                return this.getPath( '30673-03.html' )
                            }

                            return this.getPath( '30673-02.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, REPORT_OF_CRUMA ) ) {
                            return this.getPath( '30673-05.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === IRON_GATES_LOCKIRIN ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async rewardItems( player: L2PcInstance, itemId: number, isFromChampion: boolean ): Promise<void> {
        await QuestHelper.rewardSingleQuestItem( player, itemId, 1, isFromChampion )
        if ( QuestHelper.getQuestItemsCount( player, itemId ) >= 10 ) {
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
            return
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }
}