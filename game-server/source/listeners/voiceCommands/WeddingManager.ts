import {
    EventTerminationResult,
    EventType,
    NpcGeneralEvent,
    PlayerAnswerDialogEvent,
    PlayerLoggedOutEvent,
    PlayerLoginEvent,
    VoiceCommandEvent,
} from '../../gameService/models/events/EventType'
import {
    PersistedConfigurationLogic,
    PersistedConfigurationLogicType
} from '../../gameService/models/listener/PersistedConfigurationLogic'
import { DataManager } from '../../data/manager'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2ItemInstance } from '../../gameService/models/items/instance/L2ItemInstance'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'
import { L2World } from '../../gameService/L2World'
import { NpcHtmlMessagePath } from '../../gameService/packets/send/NpcHtmlMessage'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { MagicSkillUseWithCharacters } from '../../gameService/packets/send/MagicSkillUse'
import { SkillCache } from '../../gameService/cache/SkillCache'
import { ConfirmDialog, SystemMessageHelper } from '../../gameService/packets/send/SystemMessage'
import { ItemTypes } from '../../gameService/values/InventoryValues'
import { MailManager } from '../../gameService/instancemanager/MailManager'
import { MailMessage } from '../../gameService/models/mail/MailMessage'
import { MailAttachments } from '../../gameService/models/mail/MailAttachments'
import { L2Event } from '../../gameService/models/entity/L2Event'
import { SiegeManager } from '../../gameService/instancemanager/SiegeManager'
import { TvTEvent } from '../../gameService/models/entity/TvTEvent'
import { ActionFailed } from '../../gameService/packets/send/ActionFailed'
import { SevenSigns } from '../../gameService/directives/SevenSigns'
import { SevenSignsSide } from '../../gameService/values/SevenSignsValues'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { SetupGauge, SetupGaugeColors } from '../../gameService/packets/send/SetupGauge'
import { ListenerDescription } from '../../gameService/models/ListenerLogic'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import { AbstractVariablesMap } from '../../gameService/variables/AbstractVariablesManager'
import { ConfigManager } from '../../config/ConfigManager'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { PacketDispatcher } from '../../gameService/PacketDispatcher'
import { PacketHelper } from '../../gameService/packets/PacketVariables'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { AreaType } from '../../gameService/models/areas/AreaType'
import { PlayerFriendsCache } from '../../gameService/cache/PlayerFriendsCache'

const npcIds: Array<number> = [
    50007,
]

const formalWearItemId = 6408

interface WeddingConfiguration extends PersistedConfigurationLogicType {
    isEnabled: boolean
    weddingPrice: number
    enableTeleport: boolean
    teleportPrice: number
    teleportLimit: number
    teleportDuration: number
    allowSameSex: boolean
    requireFormalWear: boolean
    dissolutionPercentage: number
}

const playerConfigurationName = 'WeddingParameters'

const enum WeddingStatus {
    None,
    RequestingEngagement,
    Engaged,
    RequestingWedding,
    WaitingAcceptance,
    RequestedAcceptance
}

interface PlayerWeddingParameters extends AbstractVariablesMap {
    partnerId: number
    isMarried: boolean
    lastActionDate: number
    status: WeddingStatus
    lastTeleportDate: number
}

function updateParameters( playerId: number, partnerId: number, playerParameters: PlayerWeddingParameters, partnerParameters: PlayerWeddingParameters ): void {
    PlayerVariablesManager.set( playerId, playerConfigurationName, playerParameters )
    PlayerVariablesManager.set( partnerId, playerConfigurationName, partnerParameters )
}

function generatePacket( value: string ): Buffer {
    return PacketHelper.preservePacket( SystemMessageHelper.sendString( value ) )
}

const successDialogResult: EventTerminationResult = {
    message: 'Request to Engage has been >ACCEPTED<',
    priority: 0,
    terminate: true,
}

const failureDialogResult: EventTerminationResult = {
    message: 'Request to Engage has been >DENIED<',
    priority: 0,
    terminate: true,
}

const ignoreDialogResult: EventTerminationResult = {
    message: undefined,
    priority: 0,
    terminate: false,
}

const notMarriedMessage: Buffer = generatePacket( 'You don\'t appear to be married!' )
const waitForRequestMessage: Buffer = generatePacket( 'Please wait for current engagement request.' )
const alreadyEngagedMessage: Buffer = generatePacket( 'You are already engaged.' )
const partnerOnlineMessage: Buffer = generatePacket( 'Your dear partner has logged in.' )
const teleportWaitMessage: Buffer = generatePacket( 'You need to rest a while before making another teleport jump!' )

export class WeddingManager extends PersistedConfigurationLogic<WeddingConfiguration> {
    constructor() {
        super( 'WeddingManager', 'listeners/addons/WeddingManager.ts' )
    }

    getDefaultConfiguration(): WeddingConfiguration {
        return {
            allowSameSex: false,
            dissolutionPercentage: 20,
            enableTeleport: true,
            isEnabled: true,
            requireFormalWear: true,
            teleportLimit: 60000,
            teleportDuration: 3000,
            teleportPrice: 50000,
            weddingPrice: 250000000,
        }
    }

    getApproachedForTalkIds(): Array<number> {
        return npcIds
    }

    getPathPrefix(): string {
        return 'data/datapack/custom/events/Wedding'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onApproachedForTalk(): Promise<string> {
        return DataManager.getHtmlData().getItem( this.getPath( 'Start.html' ) )
                          .replace( '%fee%', this.configuration.weddingPrice.toString() )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let playerParameters: PlayerWeddingParameters = PlayerVariablesManager.get( data.playerId, playerConfigurationName ) as PlayerWeddingParameters
        if ( !playerParameters || !playerParameters.partnerId ) {
            return this.getPath( 'NoPartner.html' )
        }

        let partnerParameters: PlayerWeddingParameters = PlayerVariablesManager.get( playerParameters.partnerId, playerConfigurationName ) as PlayerWeddingParameters
        if ( !partnerParameters || playerParameters.partnerId !== partnerParameters.partnerId ) {
            PlayerVariablesManager.remove( data.playerId, playerConfigurationName )
            return this.getPath( 'NoPartner.html' )
        }

        if ( playerParameters.isMarried ) {
            return this.getPath( 'Already.html' )
        }

        if ( playerParameters.status === WeddingStatus.WaitingAcceptance ) {
            return this.getPath( 'WaitForPartner.html' )
        }

        let partner = L2World.getPlayer( playerParameters.partnerId )
        let player = L2World.getPlayer( data.playerId )
        if ( !partner || !partner.isOnline() || !player ) {
            return this.getPath( 'NotFound.html' )
        }

        let needsNormalWear = !this.isWearingFormalWear( player ) || !this.isWearingFormalWear( partner )
        if ( playerParameters.status === WeddingStatus.Engaged ) {
            if ( needsNormalWear ) {
                let path = this.getPath( 'NoFormal.html' )
                this.sendResponseToPartner( partner, DataManager.getHtmlData().getItem( path ), path )

                return path
            }

            playerParameters.status = WeddingStatus.RequestingWedding
            partnerParameters.status = WeddingStatus.RequestingWedding

            updateParameters( player.getObjectId(), partner.getObjectId(), playerParameters, partnerParameters )

            return DataManager.getHtmlData().getItem( this.getPath( 'Ask.html' ) )
                              .replace( '%player%', partner.getName() )
        }

        if ( playerParameters.status === WeddingStatus.RequestedAcceptance ) {
            if ( needsNormalWear ) {
                let path = this.getPath( 'NoFormal.html' )
                this.sendResponseToPartner( partner, DataManager.getHtmlData().getItem( path ), path )

                return path
            }

            return DataManager.getHtmlData().getItem( this.getPath( 'Ask.html' ) )
                              .replace( '%player%', partner.getName() )
        }

        if ( data.eventName === 'ask' ) {
            if ( needsNormalWear ) {
                let path = this.getPath( 'NoFormal.html' )
                this.sendResponseToPartner( partner, DataManager.getHtmlData().getItem( path ), path )

                return path
            }

            playerParameters.status = WeddingStatus.WaitingAcceptance
            partnerParameters.status = WeddingStatus.RequestedAcceptance

            updateParameters( player.getObjectId(), partner.getObjectId(), playerParameters, partnerParameters )

            let path = this.getPath( 'Ask.html' )
            let html = DataManager.getHtmlData().getItem( path )
                                  .replace( '%player%', player.getName() )

            this.sendResponseToPartner( partner, html, path )

            return DataManager.getHtmlData().getItem( this.getPath( 'Requested.html' ) )
                              .replace( '%player%', partner.getName() )
        }

        if ( data.eventName === 'accept' ) {
            if ( needsNormalWear ) {
                let path = this.getPath( 'NoFormal.html' )
                this.sendResponseToPartner( partner, DataManager.getHtmlData().getItem( path ), path )

                return path
            }

            if ( player.getAdena() < this.configuration.weddingPrice || partner.getAdena() < this.configuration.weddingPrice ) {
                let path = this.getPath( 'Adena.html' )
                let html = DataManager.getHtmlData().getItem( path )
                                      .replace( '%fee%', this.configuration.weddingPrice.toString() )

                this.sendResponseToPartner( partner, html, path )
                return html
            }

            await player.reduceAdena( this.configuration.weddingPrice, true, this.name )
            await partner.reduceAdena( this.configuration.weddingPrice, true, this.name )

            playerParameters.isMarried = true
            playerParameters.lastActionDate = Date.now()
            playerParameters.status = WeddingStatus.None

            partnerParameters.isMarried = true
            partnerParameters.lastActionDate = playerParameters.lastActionDate
            partnerParameters.status = WeddingStatus.None

            updateParameters( player.getObjectId(), partner.getObjectId(), playerParameters, partnerParameters )

            BroadcastHelper.dataToSelfInRange( player, MagicSkillUseWithCharacters( player, player, 2230, 1, 1, 0 ) )
            BroadcastHelper.dataToSelfInRange( partner, MagicSkillUseWithCharacters( partner, partner, 2230, 1, 1, 0 ) )

            let fireworksSkill = SkillCache.getSkill( 2025, 1 )
            if ( fireworksSkill ) {
                await player.doCast( fireworksSkill )
                await partner.doCast( fireworksSkill )
            }

            let message = `Congratulations to ${ player.getName() } and ${ partner.getName() } ! They have been married.`
            BroadcastHelper.broadcastTextToAllPlayers( 'Wedding Announcer', message )

            let acceptPath = this.getPath( 'Accepted.html' )
            this.sendResponseToPartner( partner, DataManager.getHtmlData().getItem( acceptPath ), acceptPath )
            return acceptPath
        }

        if ( data.eventName === 'decline' ) {
            PlayerVariablesManager.remove( player.getObjectId(), playerConfigurationName )
            PlayerVariablesManager.remove( partner.getObjectId(), playerConfigurationName )

            player.sendMessage( 'You declined your partner\'s marriage request.' )
            partner.sendMessage( 'Your partner declined your marriage request.' )

            let declinePath = this.getPath( 'Declined.html' )
            this.sendResponseToPartner( partner, DataManager.getHtmlData().getItem( declinePath ), declinePath )
            return declinePath
        }
    }

    isWearingFormalWear( player: L2PcInstance ): boolean {
        if ( this.configuration.requireFormalWear ) {
            let item: L2ItemInstance = player.getChestArmorInstance()
            return item && item.getId() === formalWearItemId
        }

        return true
    }

    sendResponseToPartner( player: L2PcInstance, html: string, path: string ): void {
        player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId() ) )
    }

    getCustomListeners(): Array<ListenerDescription> {
        if ( !this.configuration.isEnabled ) {
            return []
        }

        return [
            {
                registerType: ListenerRegisterType.General,
                eventType: EventType.PlayerLogin,
                method: this.onEnterWorldEvent.bind( this ),
                ids: null,
            },
            {
                registerType: ListenerRegisterType.General,
                eventType: EventType.PlayerLoggedOut,
                method: this.onPlayerExit.bind( this ),
                ids: null,
            },
            {
                registerType: ListenerRegisterType.General,
                eventType: EventType.PlayerAnswerDialog,
                method: this.onPlayerAnswerDialog.bind( this ),
                ids: null,
            },
            {
                registerType: ListenerRegisterType.General,
                eventType: EventType.VoiceCommand,
                method: this.onVoiceCommandStart.bind( this ),
                ids: null,
            },
        ]
    }

    async onEnterWorldEvent( data: PlayerLoginEvent ): Promise<void> {
        let playerParameters: PlayerWeddingParameters = PlayerVariablesManager.get( data.playerId, playerConfigurationName ) as PlayerWeddingParameters
        if ( playerParameters && playerParameters.partnerId > 0 ) {
            await PlayerVariablesManager.restoreMe( playerParameters.partnerId )

            if ( L2World.getPlayer( playerParameters.partnerId ) ) {
                PacketDispatcher.sendCopyData( playerParameters.partnerId, partnerOnlineMessage )
            }
        }
    }

    async onPlayerExit( data: PlayerLoggedOutEvent ): Promise<void> {
        let playerParameters: PlayerWeddingParameters = PlayerVariablesManager.get( data.playerId, playerConfigurationName ) as PlayerWeddingParameters
        if ( playerParameters && playerParameters.partnerId > 0 ) {
            if ( L2World.getPlayer( playerParameters.partnerId ) ) {
                PlayerVariablesManager.cancelDeletion( data.playerId )
                return
            }

            let partnerParameters: PlayerWeddingParameters = PlayerVariablesManager.get( playerParameters.partnerId, playerConfigurationName ) as PlayerWeddingParameters
            if ( partnerParameters ) {
                PlayerVariablesManager.scheduleDeletion( playerParameters.partnerId )
            }
        }
    }

    async onPlayerAnswerDialog( data: PlayerAnswerDialogEvent ): Promise<EventTerminationResult> {
        if ( data.messageId === SystemMessageIds.S1 ) {
            let playerParameters: PlayerWeddingParameters = PlayerVariablesManager.get( data.playerId, playerConfigurationName ) as PlayerWeddingParameters
            if ( !playerParameters || playerParameters.status !== WeddingStatus.RequestingEngagement ) {
                return ignoreDialogResult
            }

            if ( this.configuration.isEnabled ) {

                let partnerParameters: PlayerWeddingParameters = PlayerVariablesManager.get( playerParameters.partnerId, playerConfigurationName ) as PlayerWeddingParameters
                if ( partnerParameters
                        && partnerParameters.status === playerParameters.status
                        && partnerParameters.partnerId === data.playerId ) {
                    if ( data.answer === 1 ) {

                        let currentDate = Date.now()
                        playerParameters.status = WeddingStatus.Engaged
                        playerParameters.lastActionDate = currentDate

                        partnerParameters.status = WeddingStatus.Engaged
                        partnerParameters.lastActionDate = currentDate

                        updateParameters( data.playerId, playerParameters.partnerId, playerParameters, partnerParameters )

                        return successDialogResult
                    }

                    return failureDialogResult
                }
            }

            return ignoreDialogResult
        }
    }

    async onVoiceCommandStart( data: VoiceCommandEvent ): Promise<void> {
        switch ( data.command ) {
            case 'engage':
                return this.onEngage( data.playerId )

            case 'divorce':
                return this.onDivorce( data.playerId )

            case 'gotolove':
                return this.onTeleport( data.playerId )
        }
    }

    async onDivorce( playerId: number ): Promise<void> {
        let playerParameters: PlayerWeddingParameters = PlayerVariablesManager.get( playerId, playerConfigurationName ) as PlayerWeddingParameters
        if ( !playerParameters || !playerParameters.isMarried ) {
            PacketDispatcher.sendCopyData( playerId, notMarriedMessage )
            return
        }

        let partnerParameters: PlayerWeddingParameters = PlayerVariablesManager.get( playerParameters.partnerId, playerConfigurationName ) as PlayerWeddingParameters
        if ( !partnerParameters || !partnerParameters.isMarried || playerParameters.partnerId !== partnerParameters.partnerId ) {
            PlayerVariablesManager.remove( playerId, playerConfigurationName )
            PacketDispatcher.sendCopyData( playerId, notMarriedMessage )
            return
        }

        let player = L2World.getPlayer( playerId )
        if ( !player ) {
            return
        }

        await player.initializeWarehouse()
        let totalAdena = player.getAdena()
        let warehouseAdena = player.getWarehouse().getItemByItemId( ItemTypes.Adena )
        if ( warehouseAdena ) {
            totalAdena += warehouseAdena.getCount()
        }

        let requestedAdena = Math.floor( this.configuration.dissolutionPercentage * ( totalAdena / 100 ) )
        let differenceCost = requestedAdena - player.getAdena()

        if ( differenceCost > 0 ) {
            await player.reduceAdena( player.getAdena(), true, 'WeddingManager' )
            await player.getWarehouse().destroyItemByItemId( ItemTypes.Adena, differenceCost, 0, 'WeddingManager' )
        } else {
            await player.reduceAdena( requestedAdena, true, 'WeddingManager' )
        }

        player.sendMessage( 'You are now divorced.' )
        PlayerVariablesManager.remove( player.getObjectId(), playerConfigurationName )

        // TODO : what to do if we cannot send attachments to offline account?
        let partner = L2World.getPlayer( playerParameters.partnerId )
        if ( !partner && ConfigManager.general.allowAttachments() ) {
            let message = MailMessage.fromValues( player.getObjectId(), playerParameters.partnerId, 'Divorce Payment', 'Divorce payment is included with this message.', 0 )
            let attachments: MailAttachments = message.createAttachments()
            if ( !attachments ) {
                return
            }

            await attachments.addItem( ItemTypes.Adena, requestedAdena, 0, 'WeddingManager:divorce' )
            return MailManager.sendMessage( message )
        }

        await partner.getInventory().addAdena( requestedAdena, 0, 'WeddingManager:divorce' )
    }

    async onEngage( playerId: number ): Promise<void> {
        let playerParameters: PlayerWeddingParameters = PlayerVariablesManager.get( playerId, playerConfigurationName ) as PlayerWeddingParameters
        let currentTime = Date.now()
        if ( playerParameters ) {
            if ( playerParameters.status === WeddingStatus.RequestingEngagement && ( playerParameters.lastActionDate + 15000 ) > currentTime ) {
                PacketDispatcher.sendCopyData( playerId, waitForRequestMessage )
                return
            }

            if ( playerParameters.status === WeddingStatus.Engaged ) {
                PacketDispatcher.sendCopyData( playerId, alreadyEngagedMessage )
                return
            }
        }

        let player = L2World.getPlayer( playerId )
        if ( !player ) {
            return
        }

        let target = player.getTarget() as L2PcInstance
        if ( !target || !target.isPlayer() || target.getObjectId() === player.getObjectId() ) {
            player.sendMessage( 'Target another player to start engagement.' )
            return
        }

        let playerFriendsIds = PlayerFriendsCache.getFriends( player.getObjectId() )
        if ( !playerFriendsIds.has( target.getObjectId() ) ) {
            player.sendMessage( 'The player you want to ask is not on your friends list, you must first be on each others friends list before you choose to engage.' )
            return
        }

        let partnerParameters: PlayerWeddingParameters = PlayerVariablesManager.get( target.getObjectId(), playerConfigurationName ) as PlayerWeddingParameters
        if ( partnerParameters ) {
            if ( partnerParameters.isMarried ) {
                player.sendMessage( 'Targeted player is already married.' )
                return
            }

            if ( partnerParameters.partnerId > 0 ) {
                player.sendMessage( 'Player already engaged with someone else.' )
                return
            }
        }

        if ( !this.configuration.allowSameSex && player.getAppearance().getSex() === target.getAppearance().getSex() ) {
            player.sendMessage( 'Same sex marriage is not allowed on this server!' )
            return
        }

        playerParameters = {
            lastTeleportDate: 0,
            isMarried: false,
            partnerId: target.getObjectId(),
            status: WeddingStatus.RequestingEngagement,
            lastActionDate: currentTime,
        }

        partnerParameters = {
            lastTeleportDate: 0,
            isMarried: false,
            partnerId: player.getObjectId(),
            status: WeddingStatus.RequestingEngagement,
            lastActionDate: currentTime,
        }

        updateParameters( player.getObjectId(), target.getObjectId(), playerParameters, partnerParameters )

        let text = `${ player.getName() } is asking to engage you. Do you want to start a new relationship?`
        let packet : Buffer = ConfirmDialog.fromText( text ).addTime( 15000 ).getBuffer()

        target.sendOwnedData( packet )
    }

    async onTeleport( playerId: number ): Promise<void> {
        let playerParameters: PlayerWeddingParameters = PlayerVariablesManager.get( playerId, playerConfigurationName ) as PlayerWeddingParameters
        if ( !playerParameters || !playerParameters.isMarried ) {
            PacketDispatcher.sendCopyData( playerId, notMarriedMessage )
            return
        }

        let partnerParameters: PlayerWeddingParameters = PlayerVariablesManager.get( playerParameters.partnerId, playerConfigurationName ) as PlayerWeddingParameters
        if ( !partnerParameters || !partnerParameters.isMarried || playerParameters.partnerId !== partnerParameters.partnerId ) {
            PlayerVariablesManager.remove( playerId, playerConfigurationName )
            PacketDispatcher.sendCopyData( playerId, notMarriedMessage )
            return
        }

        let currentDate = Date.now()
        if ( ( playerParameters.lastTeleportDate + this.configuration.teleportLimit ) > currentDate ) {
            PacketDispatcher.sendCopyData( playerId, teleportWaitMessage )
            return
        }

        let player = L2World.getPlayer( playerId )
        if ( !player ) {
            return
        }

        if ( player.isUnderAttack() ) {
            player.sendMessage( 'Teleporting is not allowed when under attack. Please try again later.' )
            return
        }

        if ( player.getAdena() < this.configuration.teleportPrice ) {
            player.sendMessage( `Teleporting costs ${ GeneralHelper.getAdenaFormat( this.configuration.teleportPrice ) } Adena` )
            return
        }

        if ( player.isInDuel() ) {
            player.sendMessage( 'You are in a duel!' )
            return
        }

        if ( player.inObserverMode() ) {
            player.sendMessage( 'You are in the observation.' )
            return
        }

        if ( player.isCombatFlagEquipped() ) {
            player.sendMessage( 'While you are holding a Combat Flag or Territory Ward you can\'t go to your love!' )
            return
        }

        if ( player.isCursedWeaponEquipped() ) {
            player.sendMessage( 'While you are holding a Cursed Weapon you can\'t go to your love!' )
            return
        }

        if ( player.isJailed() ) {
            player.sendMessage( 'You are in Jail!' )
            return
        }

        if ( player.isInOlympiadMode() ) {
            player.sendMessage( 'You are in the Olympiad now.' )
            return
        }

        let ownerState = QuestStateCache.getQuestState( player.getObjectId(), 'Q00505_BloodOffering' )
        if ( ownerState && ownerState.isStarted() ) {
            player.sendMessage( 'You are in a festival.' )
            return
        }

        if ( player.isInParty() && player.getParty().isInDimensionalRift() ) {
            player.sendMessage( 'You are in the dimensional rift.' )
            return
        }

        if ( player.isInArea( AreaType.NoSummonPlayer ) ) {
            player.sendMessage( 'You are in area which blocks summoning.' )
            return
        }

        if ( player.isInArea( AreaType.Boss ) ) {
            player.sendMessage( 'You are inside a Boss Zone.' )
            return
        }

        if ( L2Event.isParticipant( player.getObjectId() ) ) {
            player.sendMessage( 'You are in an event.' )
            return
        }

        let siege = SiegeManager.getSiegeFromObject( player )
        if ( siege && siege.isInProgress() ) {
            player.sendMessage( 'You are in a siege, you cannot go to your partner.' )
            return
        }

        if ( !TvTEvent.onEscapeUse( player.getObjectId() ) ) {
            player.sendOwnedData( ActionFailed() )
            return
        }

        let partner: L2PcInstance = L2World.getPlayer( playerParameters.partnerId )
        if ( !partner || !partner.isOnline() ) {
            player.sendMessage( 'Your partner is not online.' )
            return
        }

        if ( player.getInstanceId() !== partner.getInstanceId() ) {
            player.sendMessage( 'Your partner is in another World!' )
            return
        }

        if ( partner.isUnderAttack() ) {
            player.sendMessage( 'Teleporting is not allowed when partner is under attack. Please try again later.' )
            return
        }

        if ( partner.isJailed() ) {
            player.sendMessage( 'Your partner is in Jail.' )
            return
        }

        if ( partner.isCursedWeaponEquipped() ) {
            player.sendMessage( 'Your partner is holding a Cursed Weapon and you can\'t go to your love!' )
            return
        }

        if ( partner.isInOlympiadMode() ) {
            player.sendMessage( 'Your partner is in the Olympiad now.' )
            return
        }

        if ( partner.isInDuel() ) {
            player.sendMessage( 'Your partner is in a duel.' )
            return
        }

        let partnerState = QuestStateCache.getQuestState( partner.getObjectId(), 'Q00505_BloodOffering' )
        if ( partnerState && partnerState.isStarted() ) {
            player.sendMessage( 'Your partner is in a festival.' )
            return
        }

        if ( partner.isInParty() && partner.getParty().isInDimensionalRift() ) {
            player.sendMessage( 'Your partner is in dimensional rift.' )
            return
        }

        if ( partner.inObserverMode() ) {
            player.sendMessage( 'Your partner is in the observation.' )
            return
        }

        if ( partner.isInArea( AreaType.NoSummonPlayer ) ) {
            player.sendMessage( 'Your partner is in area which blocks summoning.' )
            return
        }

        if ( partner.isInArea( AreaType.Boss ) ) {
            player.sendMessage( 'Your partner is inside a Boss Zone.' )
            return
        }

        if ( L2Event.isParticipant( partner.getObjectId() ) ) {
            player.sendMessage( 'Your partner is in an event.' )
            return
        }

        let partnerSiege = SiegeManager.getSiegeFromObject( partner )
        if ( partnerSiege && partnerSiege.isInProgress() ) {
            player.sendMessage( 'Your partner is in a siege, you cannot go to your partner.' )
            return
        }

        if ( partner.isIn7sDungeon() && !player.isIn7sDungeon() ) {
            let playerSide = SevenSigns.getPlayerSide( player.getObjectId() )

            if ( SevenSigns.isSealValidationPeriod() && playerSide !== SevenSigns.getWinnerSide() ) {
                player.sendMessage( 'Your Partner is in a Seven Signs Dungeon and you are not in the winner Cabal!' )
                return
            }

            if ( playerSide === SevenSignsSide.None ) {
                player.sendMessage( 'Your Partner is in a Seven Signs Dungeon and you are not registered!' )
                return
            }
        }

        if ( !TvTEvent.onEscapeUse( partner.getObjectId() ) ) {
            player.sendMessage( 'Your partner is in an event.' )
            return
        }

        await player.reduceAdena( this.configuration.teleportPrice, true, 'WeddingManager:onTeleport' )
        BroadcastHelper.dataInLocation( player, MagicSkillUseWithCharacters( player, player, 1050, 1, this.configuration.teleportDuration, 0 ), 900 )
        player.sendOwnedData( SetupGauge( SetupGaugeColors.Blue, this.configuration.teleportDuration, this.configuration.teleportDuration ) )

        setTimeout( this.teleportToPartner, Math.max( 100, this.configuration.teleportDuration ), player.getObjectId(), partner.getObjectId() )
    }

    teleportToPartner( playerId: number, partnerId: number ) {
        let player = L2World.getPlayer( playerId )
        let partner = L2World.getPlayer( partnerId )
        if ( !player || player.isDead() || !partner ) {
            return
        }

        let currentSiege = SiegeManager.getSiegeFromObject( partner )
        if ( currentSiege && currentSiege.isInProgress() ) {
            player.sendMessage( 'Your partner is in a siege, you cannot go to your partner.' )
            return false
        }

        player.setIsIn7sDungeon( partner.isIn7sDungeon() )

        let playerParameters: PlayerWeddingParameters = PlayerVariablesManager.get( playerId, playerConfigurationName ) as PlayerWeddingParameters
        playerParameters.lastTeleportDate = Date.now()

        return player.teleportToLocation( partner, true )
    }
}