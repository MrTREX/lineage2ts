import {
    PersistedConfigurationLogic,
    PersistedConfigurationLogicType,
} from '../../gameService/models/listener/PersistedConfigurationLogic'
import { ItemTypes } from '../../gameService/values/InventoryValues'
import { NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { DataManager } from '../../data/manager'
import _ from 'lodash'

const npcIds: Array<number> = [
    31042,
    31043,
]

type CrystalValues = [ number, string, string, string ]
const crystalMap: { [ key: number ]: CrystalValues } = {
    4410: [ 4411, '01', '02', '03' ],
    4409: [ 4412, '04', '05', '06' ],
    4408: [ 4413, '07', '08', '09' ],
    4420: [ 4414, '10', '11', '12' ],
    4421: [ 4415, '13', '14', '15' ],
    4419: [ 4417, '16', '05', '06' ],
    4418: [ 4416, '17', '05', '06' ],
}

interface EchoCrystalsConfiguration extends PersistedConfigurationLogicType {
    itemId: number
    itemAmount: number
}

export class EchoCrystals extends PersistedConfigurationLogic<EchoCrystalsConfiguration> {
    constructor() {
        super( 'EchoCrystals', 'listeners/addons/EchoCrystals.ts' )
    }

    getDefaultConfiguration(): EchoCrystalsConfiguration {
        return {
            itemAmount: 200,
            itemId: ItemTypes.Adena,
        }
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onTalkEvent(): Promise<string> {
        return this.getPath( 'main.htm' )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let reward: CrystalValues = crystalMap[ data.eventName ]

        if ( !reward ) {
            return
        }

        let [ crystalId, successCode, noAdenaCode, noScoreCode ] = reward
        let itemId = _.parseInt( data.eventName )
        let player = L2World.getPlayer( data.playerId )

        if ( !QuestHelper.hasQuestItems( player, itemId ) ) {
            return this.getPath( `${ data.characterNpcId }-${ noScoreCode }.htm` )
        }

        if ( player.getAdena() < 200 ) {
            return this.getPath( `${ data.characterNpcId }-${ noAdenaCode }.htm` )
        }

        await QuestHelper.takeSingleItem( player, this.configuration.itemId, this.configuration.itemAmount )
        await QuestHelper.giveSingleItem( player, crystalId, 1 )

        return this.getPath( `${ data.characterNpcId }-${ successCode }.htm` )
    }

    getPath( name: string ): string {
        return DataManager.getHtmlData().getItem( `overrides/html/addons/echoCrystals/${name}` )
                .replace( '%amount%', this.configuration.itemAmount.toString() )
                .replace( '%name%', DataManager.getItems().getTemplate( this.configuration.itemId ).getName() )
    }
}