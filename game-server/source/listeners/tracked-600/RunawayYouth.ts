import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestStateValues } from '../../gameService/models/quest/State'

const BATIDAE = 31989
const IVAN = 32014
const SOE = 736
const minimumLevel = 26

export class RunawayYouth extends ListenerLogic {
    constructor() {
        super( 'Q00651_RunawayYouth', 'listeners/tracked-600/RunawayYouth.ts' )
        this.questId = 651
    }

    getQuestStartIds(): Array<number> {
        return [ IVAN ]
    }

    getTalkIds(): Array<number> {
        return [
            BATIDAE,
            IVAN,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00651_RunawayYouth'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32014-03.html':
                break

            case '32014-04.htm':
                if ( !QuestHelper.hasQuestItem( player, SOE ) ) {
                    return '32014-05.htm'
                }

                state.startQuest()
                await QuestHelper.takeSingleItem( player, SOE, 1 )

                let npc = L2World.getObjectById( data.characterId ) as L2Npc
                await npc.deleteMe()

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== IVAN ) {
                    break
                }

                return this.getPath( player.getLevel() >= minimumLevel ? '32014-01.htm' : '32014-01a.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case IVAN:
                        return this.getPath( '32014-02.html' )

                    case BATIDAE:
                        await QuestHelper.giveAdena( player, 2883, true )
                        await state.exitQuest( true, true )

                        return this.getPath( '31989-01.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}