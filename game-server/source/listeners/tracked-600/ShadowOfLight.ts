import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const EYE_OF_ARGOS = 31683
const EYE_OF_DARKNESS = 7189
const minimumLevel = 68

type PlayerReward = [ number, number, number, number ] // itemId, adena amount, exp amount, sp amount
const playerRewards: Array<PlayerReward> = [
    [
        6699,
        40000,
        120000,
        20000,
    ],
    [
        6698,
        60000,
        110000,
        15000,
    ],
    [
        6700,
        40000,
        150000,
        10000,
    ],
    [
        0,
        100000,
        140000,
        11250,
    ],
]

export class ShadowOfLight extends ListenerLogic {
    constructor() {
        super( 'Q00602_ShadowOfLight', 'listeners/tracked-500/ShadowOfLight.ts' )
        this.questId = 602
        this.questItemIds = [ EYE_OF_DARKNESS ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            21299,
            21304,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ EYE_OF_ARGOS ]
    }

    getTalkIds(): Array<number> {
        return [ EYE_OF_ARGOS ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00602_ShadowOfLight'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '31683-02.htm':
                state.startQuest()
                break

            case '31683-05.html':
                let player = L2World.getPlayer( data.playerId )
                if ( QuestHelper.getQuestItemsCount( player, EYE_OF_DARKNESS ) < 100 ) {
                    return this.getPath( '31683-06.html' )
                }

                let [ itemId, adenaAmount, exp, sp ] = _.sample( playerRewards )
                if ( itemId !== 0 ) {
                    await QuestHelper.rewardSingleItem( player, itemId, 3 )
                }

                await QuestHelper.giveAdena( player, adenaAmount, true )
                await QuestHelper.addExpAndSp( player, exp, sp )
                await state.exitQuest( true, true )

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( Math.random() > QuestHelper.getAdjustedChance( EYE_OF_DARKNESS, data.npcId === 21299 ? 0.56 : 0.8, data.isChampion ) ) {
            return
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )
        if ( !state || !state.isCondition( 1 ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        await QuestHelper.rewardAndProgressState( player, state, EYE_OF_DARKNESS, 1, 100, data.isChampion, 2 )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                let player = L2World.getPlayer( data.playerId )
                return this.getPath( player.getLevel() >= minimumLevel ? '31683-01.htm' : '31683-00.htm' )

            case QuestStateValues.STARTED:
                return this.getPath( state.isCondition( 1 ) ? '31683-03.html' : '31683-04.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}