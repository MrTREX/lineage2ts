import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestStateValues } from '../../gameService/models/quest/State'

const TANTAN = 32012
const SARA = 30180
const SOULSHOT_C = 1464
const ENCHANT_ARMOR_D = 956
const minimumLevel = 46

export class AnAgedExAdventurer extends ListenerLogic {
    constructor() {
        super( 'Q00652_AnAgedExAdventurer', 'listeners/tracked-600/AnAgedExAdventurer.ts' )
        this.questId = 652
    }

    getQuestStartIds(): Array<number> {
        return [ TANTAN ]
    }

    getTalkIds(): Array<number> {
        return [
            TANTAN,
            SARA,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00652_AnAgedExAdventurer'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '32012-04.htm':
                let player = L2World.getPlayer( data.playerId )
                if ( QuestHelper.getQuestItemsCount( player, SOULSHOT_C ) < 100 ) {
                    return '32012-05.htm'
                }

                state.startQuest()
                await QuestHelper.takeSingleItem( player, SOULSHOT_C, 100 )

                let npc = L2World.getObjectById( data.characterId ) as L2Npc
                await npc.deleteMe()

                break

            case '32012-03.html':
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== TANTAN ) {
                    break
                }

                return this.getPath( player.getLevel() >= minimumLevel ? '32012-01.htm' : '32012-01a.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case TANTAN:
                        return this.getPath( '32012-02.html' )

                    case SARA:
                        if ( Math.random() <= 0.4 ) {
                            await QuestHelper.rewardSingleItem( player, ENCHANT_ARMOR_D, 1 )
                            await QuestHelper.giveAdena( player, 5026, true )

                            return this.getPath( '30180-01.html' )
                        }

                        await QuestHelper.giveAdena( player, 10000, true )
                        await state.exitQuest( true, true )

                        return this.getPath( '30180-02.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}