import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const RAILMAN_OBI = 32052
const THIEF_GUILD_MARK = 8099
const minimumLevel = 30

const monsterRewardChances: { [ npcId: number ]: number } = {
    22017: 0.529, // Bandit Sweeper
    22018: 0.452, // Bandit Hound
    22019: 0.606, // Bandit Watchman
    22021: 0.615, // Bandit Undertaker
    22022: 0.721, // Bandit Assassin
    22023: 0.827, // Bandit Warrior
    22024: 0.779, // Bandit Inspector
    22026: 1, // Bandit Captain
}

const requiredAmount = 200

export class ALooterAndARailroadMan extends ListenerLogic {
    constructor() {
        super( 'Q00649_ALooterAndARailroadMan', 'listeners/tracked-600/ALooterAndARailroadMan.ts' )
        this.questId = 649
        this.questItemIds = [ THIEF_GUILD_MARK ]
    }

    getQuestStartIds(): Array<number> {
        return [ RAILMAN_OBI ]
    }

    getTalkIds(): Array<number> {
        return [ RAILMAN_OBI ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32052-03.htm':
                state.startQuest()
                break

            case '32052-06.html':
                if ( state.isCondition( 2 ) && QuestHelper.hasQuestItem( player, THIEF_GUILD_MARK ) ) {
                    await QuestHelper.giveAdena( player, 21698, true )
                    await state.exitQuest( true, true )

                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( Math.random() > QuestHelper.getAdjustedChance( THIEF_GUILD_MARK, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )
        if ( !state || !state.isCondition( 1 ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        await QuestHelper.rewardAndProgressState( player, state, THIEF_GUILD_MARK, 1, requiredAmount, data.isChampion, 2 )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '32052-01.htm' : '32052-02.htm' )

            case QuestStateValues.STARTED:
                return this.getPath( QuestHelper.getQuestItemsCount( player, THIEF_GUILD_MARK ) >= requiredAmount ? '32052-04.html' : '32052-05.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00649_ALooterAndARailroadMan'
    }
}