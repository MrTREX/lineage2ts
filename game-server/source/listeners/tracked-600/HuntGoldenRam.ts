import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const KAHMAN = 31554
const GOLDEN_RAM_BADGE_RECRUIT = 7246
const GOLDEN_RAM_BADGE_SOLDIER = 7247
const SPLINTER_STAKATO_CHITIN = 7248
const NEEDLE_STAKATO_CHITIN = 7249
const requiredAmount = 100
const minimumLevel = 66

type MonsterRewardData = [ number, number, number ] // itemId, chance, condition value
const monsterRewards: { [ npcId: number ]: MonsterRewardData } = {
    21508: [ SPLINTER_STAKATO_CHITIN, 0.500, 1 ], // splinter_stakato
    21509: [ SPLINTER_STAKATO_CHITIN, 0.430, 1 ], // splinter_stakato_worker
    21510: [ SPLINTER_STAKATO_CHITIN, 0.521, 1 ], // splinter_stakato_soldier
    21511: [ SPLINTER_STAKATO_CHITIN, 0.575, 1 ], // splinter_stakato_drone
    21512: [ SPLINTER_STAKATO_CHITIN, 0.746, 1 ], // splinter_stakato_drone_a
    21513: [ NEEDLE_STAKATO_CHITIN, 0.500, 2 ], // needle_stakato
    21514: [ NEEDLE_STAKATO_CHITIN, 0.430, 2 ], // needle_stakato_worker
    21515: [ NEEDLE_STAKATO_CHITIN, 0.520, 2 ], // needle_stakato_soldier
    21516: [ NEEDLE_STAKATO_CHITIN, 0.531, 2 ], // needle_stakato_drone
    21517: [ NEEDLE_STAKATO_CHITIN, 0.744, 2 ], // needle_stakato_drone_a
}

export class HuntGoldenRam extends ListenerLogic {
    constructor() {
        super( 'Q00628_HuntGoldenRam', 'listeners/tracked-600/HuntGoldenRam.ts' )
        this.questId = 628
        this.questItemIds = [
            SPLINTER_STAKATO_CHITIN,
            NEEDLE_STAKATO_CHITIN,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ KAHMAN ]
    }

    getTalkIds(): Array<number> {
        return [ KAHMAN ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewards ).map( value => _.parseInt( value ) )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'accept':
                if ( state.isCreated() && player.getLevel() >= minimumLevel ) {
                    state.startQuest()

                    if ( QuestHelper.hasQuestItem( player, GOLDEN_RAM_BADGE_SOLDIER ) ) {
                        state.setConditionWithSound( 3 )

                        return this.getPath( '31554-05.htm' )
                    }

                    if ( QuestHelper.hasQuestItem( player, GOLDEN_RAM_BADGE_RECRUIT ) ) {
                        state.setConditionWithSound( 2 )

                        return this.getPath( '31554-04.htm' )
                    }

                    return this.getPath( '31554-03.htm' )
                }

                return

            case '31554-08.html':
                if ( QuestHelper.getQuestItemsCount( player, SPLINTER_STAKATO_CHITIN ) >= requiredAmount ) {
                    await QuestHelper.takeSingleItem( player, SPLINTER_STAKATO_CHITIN, -1 )
                    await QuestHelper.giveSingleItem( player, GOLDEN_RAM_BADGE_RECRUIT, 1 )

                    state.setConditionWithSound( 2, true )
                    break
                }

                return

            case '31554-12.html':
            case '31554-13.html':
                break

            case '31554-14.html':
                await state.exitQuest( true, true )
                break


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00628_HuntGoldenRam'
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let [ itemId, chanceValue, conditionValue ]: MonsterRewardData = monsterRewards[ data.npcId ]
        if ( Math.random() > QuestHelper.getAdjustedChance( itemId, chanceValue, data.isChampion ) ) {
            return
        }

        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), -1, 1, L2World.getObjectById( data.targetId ) )
        if ( !state || state.isCondition( 3 ) || conditionValue > state.getCondition() ) {
            return
        }

        await QuestHelper.rewardUpToLimit( state.getPlayer(), itemId, 1, requiredAmount, data.isChampion )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '31554-01.htm' : '31554-02.htm' )

            case QuestStateValues.STARTED:
                let splinterAmount: number = QuestHelper.getQuestItemsCount( player, SPLINTER_STAKATO_CHITIN )
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( splinterAmount >= requiredAmount ? '31554-07.html' : '31554-06.html' )

                    case 2:
                        if ( QuestHelper.hasQuestItem( player, GOLDEN_RAM_BADGE_RECRUIT ) ) {
                            let needleAmount: number = QuestHelper.getQuestItemsCount( player, NEEDLE_STAKATO_CHITIN )

                            if ( splinterAmount >= requiredAmount && needleAmount >= requiredAmount ) {
                                await QuestHelper.takeMultipleItems( player, -1, GOLDEN_RAM_BADGE_RECRUIT, SPLINTER_STAKATO_CHITIN, NEEDLE_STAKATO_CHITIN )
                                await QuestHelper.giveSingleItem( player, GOLDEN_RAM_BADGE_SOLDIER, 1 )

                                state.setConditionWithSound( 3, true )
                                return this.getPath( '31554-10.html' )
                            }

                            return this.getPath( '31554-09.html' )
                        }

                        state.setConditionWithSound( 1 )
                        return this.getPath( splinterAmount >= requiredAmount ? '31554-07.html' : '31554-06.html' )

                    case 3:
                        if ( QuestHelper.hasQuestItem( player, GOLDEN_RAM_BADGE_SOLDIER ) ) {
                            return this.getPath( '31554-11.html' )
                        }

                        state.setConditionWithSound( 1 )
                        return this.getPath( splinterAmount >= requiredAmount ? '31554-07.html' : '31554-06.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}