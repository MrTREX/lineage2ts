import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import _ from 'lodash'
import aigle from 'aigle'

const ALEX = 30291
const MARIA = 30608
const SCROLL_ENCHANT_WEAPON_D_GRADE = 955
const SCROLL_ENCHANT_ARMOR_D_GRADE = 956
const WATCHING_EYES = 8074
const ROUGHLY_HEWN_ROCK_GOLEM_SHARD = 8075
const DELU_LIZARDMANS_SCALE = 8076
const minimumLevel = 30
const adenaRewardOne = 13000
const adenaRewardTwo = 1000
const adenaRewardThree = 20000
const adenaRewardFour = 2000
const adenaRewardFive = 45000
const adenaRewardSix = 5000
const DELU_LIZARDMAN_COMMANDER = 21107 // Delu Lizardman Commander

type MonsterData = [ number, number ] // itemId, chance
const monsterRewards: { [ npcId: number ]: MonsterData } = {
    21102: [ WATCHING_EYES, 0.500 ], // Watchman of the Plains
    21106: [ WATCHING_EYES, 0.630 ], // Cursed Seer
    21103: [ ROUGHLY_HEWN_ROCK_GOLEM_SHARD, 0.520 ], // Roughly Hewn Rock Golem
    20781: [ DELU_LIZARDMANS_SCALE, 0.650 ], // Delu Lizardman Shaman
    21104: [ DELU_LIZARDMANS_SCALE, 0.650 ], // Delu Lizardman Supplier
    21105: [ DELU_LIZARDMANS_SCALE, 0.750 ], // Delu Lizardman Special Agent
    [ DELU_LIZARDMAN_COMMANDER ]: [ DELU_LIZARDMANS_SCALE, 0.33 ],
}

const tradableItems: Array<number> = [ WATCHING_EYES, ROUGHLY_HEWN_ROCK_GOLEM_SHARD, DELU_LIZARDMANS_SCALE ]

export class AidingTheFloranVillage extends ListenerLogic {
    constructor() {
        super( 'Q00660_AidingTheFloranVillage', 'listeners/tracked-600/AidingTheFloranVillage.ts' )
        this.questId = 660
        this.questItemIds = [
            WATCHING_EYES,
            ROUGHLY_HEWN_ROCK_GOLEM_SHARD,
            DELU_LIZARDMANS_SCALE,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ MARIA, ALEX ]
    }

    getTalkIds(): Array<number> {
        return [ MARIA, ALEX ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewards ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00660_AidingTheFloranVillage'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30608-06.htm':
                if ( player.getLevel() >= minimumLevel ) {
                    state.startQuest()
                    break
                }

                return this.getPath( '30608-06a.htm' )

            case '30608-02.htm':
            case '30608-03.html':
            case '30291-07.html':
            case '30291-09.html':
            case '30291-10.html':
            case '30291-14.html':
            case '30291-18.html':
                break

            case '30291-03.htm':
                if ( player.getLevel() >= minimumLevel ) {
                    if ( state.isCreated() ) {
                        state.setState( QuestStateValues.STARTED )
                        state.setConditionWithSound( 2 )

                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ACCEPT )
                    }

                    break
                }

                return this.getPath( '30291-02.htm' )

            case '30291-06.html':
                let itemCount: number = QuestHelper.getItemsSumCount( player, WATCHING_EYES, ROUGHLY_HEWN_ROCK_GOLEM_SHARD, DELU_LIZARDMANS_SCALE )
                if ( itemCount > 0 ) {
                    let adenaAmount = itemCount * 100
                    await QuestHelper.takeMultipleItems( player, -1, WATCHING_EYES, ROUGHLY_HEWN_ROCK_GOLEM_SHARD, DELU_LIZARDMANS_SCALE )
                    await QuestHelper.giveAdena( player, adenaAmount, true )

                    break
                }

                return this.getPath( '30291-08.html' )

            case '30291-08a.html':
                await state.exitQuest( true, true )
                break

            case '30291-12.html':
                return this.onDonate100( player, data.eventName )

            case '30291-16.html':
                return this.onDonate200( player, data.eventName )

            case '30291-20.html':
                return this.onDonate500( player, data.eventName )

            case '30291-22.html':
                let totalAmount: number = QuestHelper.getItemsSumCount( player, WATCHING_EYES, ROUGHLY_HEWN_ROCK_GOLEM_SHARD, DELU_LIZARDMANS_SCALE )
                if ( totalAmount === 0 ) {
                    return this.getPath( '30291-23.html' )
                }

                await QuestHelper.giveAdena( player, totalAmount * 100, true )
                await state.exitQuest( true, true )

                break
        }

        return this.getPath( data.eventName )
    }

    async onDonate100( player: L2PcInstance, defaultPath: string ): Promise<string> {
        let eyeAmount = QuestHelper.getQuestItemsCount( player, WATCHING_EYES )
        let shardAmount = QuestHelper.getQuestItemsCount( player, ROUGHLY_HEWN_ROCK_GOLEM_SHARD )
        let scaleAmount = QuestHelper.getQuestItemsCount( player, DELU_LIZARDMANS_SCALE )
        let total: number = eyeAmount + shardAmount + scaleAmount

        if ( total < 100 ) {
            return this.getPath( '30291-11.html' )
        }

        await this.takePlayerItems( player, 100, eyeAmount, shardAmount, scaleAmount )

        if ( Math.random() > 0.5 ) {
            await QuestHelper.rewardSingleItem( player, SCROLL_ENCHANT_ARMOR_D_GRADE, 1 )
            await QuestHelper.giveAdena( player, adenaRewardOne, true )

            return this.getPath( defaultPath )
        }

        await QuestHelper.giveAdena( player, adenaRewardTwo, true )
        return this.getPath( '30291-13.html' )
    }

    async takePlayerItems( player: L2PcInstance, requiredAmount: number, ...itemCounts: Array<number> ): Promise<void> {
        await aigle.resolve( tradableItems ).eachSeries( async ( itemId: number, index: number ) => {
            let currentCount: number = itemCounts[ index ]
            if ( currentCount < requiredAmount ) {
                await QuestHelper.takeSingleItem( player, itemId, currentCount )
                requiredAmount -= currentCount

                return
            }

            await QuestHelper.takeSingleItem( player, itemId, requiredAmount )
            return false
        } )
    }

    async onDonate200( player: L2PcInstance, defaultPath: string ): Promise<string> {
        let eyeAmount = QuestHelper.getQuestItemsCount( player, WATCHING_EYES )
        let shardAmount = QuestHelper.getQuestItemsCount( player, ROUGHLY_HEWN_ROCK_GOLEM_SHARD )
        let scaleAmount = QuestHelper.getQuestItemsCount( player, DELU_LIZARDMANS_SCALE )
        let total: number = eyeAmount + shardAmount + scaleAmount

        if ( total < 200 ) {
            return this.getPath( '30291-15.html' )
        }

        await this.takePlayerItems( player, 200, eyeAmount, shardAmount, scaleAmount )

        if ( Math.random() > 0.5 ) {
            if ( Math.random() > 0.5 ) {
                await QuestHelper.rewardSingleItem( player, SCROLL_ENCHANT_ARMOR_D_GRADE, 1 )
                await QuestHelper.giveAdena( player, adenaRewardThree, true )

                return this.getPath( defaultPath )
            }

            await QuestHelper.rewardSingleItem( player, SCROLL_ENCHANT_WEAPON_D_GRADE, 1 )
            return this.getPath( defaultPath )
        }

        await QuestHelper.giveAdena( player, adenaRewardFour, true )
        return this.getPath( '30291-17.html' )
    }

    async onDonate500( player: L2PcInstance, defaultPath: string ): Promise<string> {
        let eyeAmount = QuestHelper.getQuestItemsCount( player, WATCHING_EYES )
        let shardAmount = QuestHelper.getQuestItemsCount( player, ROUGHLY_HEWN_ROCK_GOLEM_SHARD )
        let scaleAmount = QuestHelper.getQuestItemsCount( player, DELU_LIZARDMANS_SCALE )
        let total: number = eyeAmount + shardAmount + scaleAmount

        if ( total < 500 ) {
            return this.getPath( '30291-19.html' )
        }

        await this.takePlayerItems( player, 500, eyeAmount, shardAmount, scaleAmount )

        if ( Math.random() > 0.5 ) {
            await QuestHelper.rewardSingleItem( player, SCROLL_ENCHANT_ARMOR_D_GRADE, 1 )
            await QuestHelper.giveAdena( player, adenaRewardFive, true )

            return this.getPath( defaultPath )
        }

        await QuestHelper.giveAdena( player, adenaRewardSix, true )
        return this.getPath( '30291-21.html' )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let [ itemId, chance ]: MonsterData = monsterRewards[ data.npcId ]
        let isChanceBased: boolean = data.npcId !== DELU_LIZARDMAN_COMMANDER
        let isChanceFailure: boolean = Math.random() > QuestHelper.getAdjustedChance( itemId, chance, data.isChampion )

        if ( isChanceBased && isChanceFailure ) {
            return
        }

        let player: L2PcInstance = this.getRandomPartyMemberPerCondition( L2World.getPlayer( data.playerId ), 2, 2, L2World.getObjectById( data.targetId ) )
        if ( !player ) {
            return
        }

        let amount: number = isChanceFailure || isChanceBased ? 1 : 2
        await QuestHelper.rewardSingleQuestItem( player, itemId, amount, data.isChampion )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                switch ( data.characterNpcId ) {
                    case MARIA:
                        return this.getPath( player.getLevel() >= minimumLevel ? '30608-01.htm' : '30608-04.html' )

                    case ALEX:
                        return this.getPath( player.getLevel() >= minimumLevel ? '30291-01.htm' : '30291-02.htm' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case MARIA:
                        return this.getPath( '30608-05.html' )

                    case ALEX:
                        switch ( state.getCondition() ) {
                            case 1:
                                state.setConditionWithSound( 2, true )
                                return this.getPath( '30291-04.html' )

                            case 2:
                                return this.getPath( '30291-05.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}