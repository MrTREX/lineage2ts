import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const MYSTERIOUS_NECROMANCER = 31518
const ENFEUX = 31519
const SEAL_OF_LIGHT = 7170
const BEAD_OF_OBEDIENCE = 7171
const GEM_OF_SAINTS = 7172
const minimumLevel = 60
const BEAD_OF_OBEDIENCE_COUNT_REQUIRED = 300
const ASOFE = 4043
const THONS = 4044
const ENRIA = 4042
const MOLD_HARDENER = 4041

const monsterRewardChances = {
    21520: 0.661, // Eye of Splendor
    21523: 0.668, // Flash of Splendor
    21524: 0.714, // Blade of Splendor
    21525: 0.714, // Blade of Splendor
    21526: 0.796, // Wisdom of Splendor
    21529: 0.659, // Soul of Splendor
    21530: 0.704, // Victory of Splendor
    21531: 0.791, // Punishment of Splendor
    21532: 0.820, // Shout of Splendor
    21535: 0.827, // Signet of Splendor
    21536: 0.798, // Crown of Splendor
    21539: 0.875, // Wailing of Splendor
    21540: 0.875, // Wailing of Splendor
    21658: 0.791, // Punishment of Splendor
}

export class HeartInSearchOfPower extends ListenerLogic {
    constructor() {
        super( 'Q00627_HeartInSearchOfPower', 'listeners/tracked-600/HeartInSearchOfPower.ts' )
        this.questId = 627
        this.questItemIds = [
            SEAL_OF_LIGHT,
            BEAD_OF_OBEDIENCE,
            GEM_OF_SAINTS,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ MYSTERIOUS_NECROMANCER ]
    }

    getTalkIds(): Array<number> {
        return [ MYSTERIOUS_NECROMANCER, ENFEUX ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00627_HeartInSearchOfPower'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31518-02.htm':
                state.startQuest()
                break

            case '31518-06.html':
                if ( QuestHelper.getQuestItemsCount( player, BEAD_OF_OBEDIENCE ) < BEAD_OF_OBEDIENCE_COUNT_REQUIRED ) {
                    return this.getPath( '31518-05.html' )
                }

                await QuestHelper.takeSingleItem( player, BEAD_OF_OBEDIENCE, -1 )
                await QuestHelper.giveSingleItem( player, SEAL_OF_LIGHT, 1 )

                state.setConditionWithSound( 3 )
                break

            case 'Adena':
            case 'Asofes':
            case 'Thons':
            case 'Enrias':
            case 'Mold_Hardener':
                if ( !QuestHelper.hasQuestItem( player, GEM_OF_SAINTS ) ) {
                    return this.getPath( '31518-11.html' )
                }

                switch ( data.eventName ) {
                    case 'Adena':
                        await QuestHelper.giveAdena( player, 100000, true )
                        break

                    case 'Asofes':
                        await QuestHelper.rewardSingleItem( player, ASOFE, 13 )
                        await QuestHelper.giveAdena( player, 6400, true )
                        break

                    case 'Thons':
                        await QuestHelper.rewardSingleItem( player, THONS, 13 )
                        await QuestHelper.giveAdena( player, 6400, true )
                        break

                    case 'Enrias':
                        await QuestHelper.rewardSingleItem( player, ENRIA, 6 )
                        await QuestHelper.giveAdena( player, 13600, true )
                        break

                    case 'Mold_Hardener':
                        await QuestHelper.rewardSingleItem( player, MOLD_HARDENER, 3 )
                        await QuestHelper.giveAdena( player, 17200, true )
                        break
                }

                await state.exitQuest( true )
                return this.getPath( '31518-10.html' )

            case '31519-02.html':
                if ( QuestHelper.hasQuestItem( player, SEAL_OF_LIGHT ) && state.isCondition( 3 ) ) {
                    await QuestHelper.takeSingleItem( player, SEAL_OF_LIGHT, -1 )
                    await QuestHelper.giveSingleItem( player, GEM_OF_SAINTS, 1 )

                    state.setConditionWithSound( 4 )
                    break
                }

                return

            case '31518-09.html':
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( Math.random() > QuestHelper.getAdjustedChance( BEAD_OF_OBEDIENCE, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        let state: QuestState = this.getRandomPartyMemberStateForCondition( L2World.getPlayer( data.playerId ), 1 )
        if ( !state ) {
            return
        }

        let player = state.getPlayer()
        await QuestHelper.rewardAndProgressState( player, state, BEAD_OF_OBEDIENCE, 1, BEAD_OF_OBEDIENCE_COUNT_REQUIRED, data.isChampion, 2 )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === MYSTERIOUS_NECROMANCER ) {
                    return this.getPath( player.getLevel() >= minimumLevel ? '31518-01.htm' : '31518-00.htm' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case MYSTERIOUS_NECROMANCER:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '31518-03.html' )

                            case 2:
                                return this.getPath( '31518-04.html' )

                            case 3:
                                return this.getPath( '31518-07.html' )

                            case 4:
                                return this.getPath( '31518-08.html' )

                        }

                        break

                    case ENFEUX:
                        switch ( state.getCondition() ) {
                            case 3:
                                return this.getPath( '31519-01.html' )

                            case 4:
                                return this.getPath( '31519-03.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}