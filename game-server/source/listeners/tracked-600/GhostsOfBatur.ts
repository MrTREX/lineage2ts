import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const KARUDA = 32017
const CONTAMINATED_MOREK_WARRIOR = 22703
const CONTAMINATED_BATUR_WARRIOR = 22704
const CONTAMINATED_BATUR_COMMANDER = 22705
const CURSED_GRAVE_GOODS = 8089 // Old item
const CURSED_BURIAL_ITEMS = 14861 // New item
const minimumLevel = 80
const requiredAmount = 500

const monsterRewardChances : {[ npcId: number ] : number } = {
    22703: 0.516,
    22704: 0.664,
    22705: 0.686,
}

export class GhostsOfBatur extends ListenerLogic {
    constructor() {
        super( 'Q00645_GhostsOfBatur', 'listeners/tracked-600/GhostsOfBatur.ts' )
        this.questId = 645
        this.questItemIds = [ CURSED_GRAVE_GOODS ]
    }

    getQuestStartIds(): Array<number> {
        return [ KARUDA ]
    }

    getTalkIds(): Array<number> {
        return [ KARUDA ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            CONTAMINATED_MOREK_WARRIOR,
            CONTAMINATED_BATUR_WARRIOR,
            CONTAMINATED_BATUR_COMMANDER,
        ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '32017-03.htm':
                state.startQuest()
                break

            case '32017-06.html':
            case '32017-08.html':
                break

            case '32017-09.html':
                await state.exitQuest( true, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( Math.random() > QuestHelper.getAdjustedChance( CURSED_BURIAL_ITEMS, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        let state : QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), 1, 1, L2World.getObjectById( data.targetId ) )
        if ( !state ) {
            return
        }

        await QuestHelper.rewardAndProgressState( state.getPlayer(), state, CURSED_BURIAL_ITEMS, 1, requiredAmount, data.isChampion, 2 )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '32017-01.htm' : '32017-02.html' )

            case QuestStateValues.STARTED:
                let amount = QuestHelper.getQuestItemsCount( player, CURSED_GRAVE_GOODS )
                if ( amount > 0 && amount < 180 ) {
                    let adenaAmount : number = 56000 + ( amount * 64 )
                    await QuestHelper.giveAdena( player, adenaAmount, false )
                    await QuestHelper.addExpAndSp( player, 138000, 7997 )
                    await state.exitQuest( true, true )

                    return this.getPath( '32017-07.html' )
                }

                return this.getPath( QuestHelper.hasQuestItem( player, CURSED_BURIAL_ITEMS ) ? '32017-04.html' : '32017-05.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00645_GhostsOfBatur'
    }
}