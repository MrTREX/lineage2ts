import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const JUDE = 32356
const EVIL_WEAPON = 10327
const bigAmount = 200
const smallAmount = 5

const rewardIds: Array<Array<number>> = [
    [
        10373,
        10374,
        10375,
        10376,
        10377,
        10378,
        10379,
        10380,
        10381,
    ],
    [
        10397,
        10398,
        10399,
        10400,
        10401,
        10402,
        10403,
        10404,
        10405,
    ],
]

const monsterRewardChances: { [ npcId: number ]: number } = {
    22398: 0.173,
    22399: 0.246,
}

const minimumLevel = 78

export class JudesRequest extends ListenerLogic {
    constructor() {
        super( 'Q00690_JudesRequest', 'listeners/tracked-600/JudesRequest.ts' )
        this.questId = 690
        this.questItemIds = [ EVIL_WEAPON ]
    }

    getQuestStartIds(): Array<number> {
        return [ JUDE ]
    }

    getTalkIds(): Array<number> {
        return [ JUDE ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00690_JudesRequest'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32356-03.htm':
                state.startQuest()
                break

            case '32356-07.htm':
                if ( QuestHelper.getQuestItemsCount( player, EVIL_WEAPON ) >= bigAmount ) {
                    await QuestHelper.takeSingleItem( player, EVIL_WEAPON, bigAmount )
                    await QuestHelper.rewardSingleItem( player, _.sample( _.sample( rewardIds ) ), 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                    return this.getPath( '32356-07.htm' )
                }

                return this.getPath( '32356-07a.htm' )

            case '32356-08.htm':
                await state.exitQuest( true, true )
                break

            case '32356-09.htm':
                if ( QuestHelper.getQuestItemsCount( player, EVIL_WEAPON ) >= smallAmount ) {
                    await QuestHelper.takeSingleItem( player, EVIL_WEAPON, smallAmount )
                    await QuestHelper.rewardSingleItem( player, _.sample( _.sample( rewardIds ) ), 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                    return this.getPath( '32356-09.htm' )
                }

                return this.getPath( '32356-09a.htm' )

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( Math.random() > QuestHelper.getAdjustedChance( EVIL_WEAPON, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        let player: L2PcInstance = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 1 )
        if ( !player ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, EVIL_WEAPON, 1, data.isChampion )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getLevel() >= minimumLevel ) {
                    return this.getPath( '32356-01.htm' )
                }

                return this.getPath( '32356-02.htm' )

            case QuestStateValues.STARTED:
                let amount: number = QuestHelper.getQuestItemsCount( player, EVIL_WEAPON )
                if ( amount >= bigAmount ) {
                    return this.getPath( '32356-04.htm' )
                }

                if ( amount < smallAmount ) {
                    return this.getPath( '32356-05a.htm' )
                }

                return this.getPath( '32356-05.htm' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}