import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { SkillHolder } from '../../gameService/models/holders/SkillHolder'
import { AttackableAttackedEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const WAHKAN = 31371
const ASEFA = 31372
const UDANS_BOX = 31561
const UDANS_EYE = 31684
const KEY = 1661
const STOLEN_GREEN_TOTEM = 7237
const WISDOM_STONE = 7081
const GREEN_TOTEM = 7238
const KETRA_MARKS = [
    7211, // Mark of Ketra's Alliance - Level 1
    7212, // Mark of Ketra's Alliance - Level 2
    7213, // Mark of Ketra's Alliance - Level 3
    7214, // Mark of Ketra's Alliance - Level 4
    7215, // Mark of Ketra's Alliance - Level 5
]

const GOW = new SkillHolder( 4547 ) // Gaze of Watcher
const DISPEL_GOW = new SkillHolder( 4548 ) // Quest - Dispel Watcher Gaze
const MIN_LEVEL = 74

const variableNames = {
    isSpawned: 'is',
}

const eventNames = {
    despawnEyeNpc: 'den',
}

export class MagicalPowerOfWaterPartOne extends ListenerLogic {
    constructor() {
        super( 'Q00609_MagicalPowerOfWaterPart1', 'listeners/tracked-600/MagicalPowerOfWaterPartOne.ts' )
        this.questId = 609
        this.questItemIds = [ STOLEN_GREEN_TOTEM ]
    }

    getAttackableAttackIds(): Array<number> {
        return [
            21350, // Varka Silenos Recruit
            21351, // Varka Silenos Footman
            21353, // Varka Silenos Scout
            21354, // Varka Silenos Hunter
            21355, // Varka Silenos Shaman
            21357, // Varka Silenos Priest
            21358, // Varka Silenos Warrior
            21360, // Varka Silenos Medium
            21361, // Varka Silenos Magus
            21362, // Varka Silenos Officer
            21364, // Varka Silenos Seer
            21365, // Varka Silenos Great Magus
            21366, // Varka Silenos General
            21368, // Varka Silenos Great Seer
            21369, // Varka's Commander
            21370, // Varka's Elite Guard
            21371, // Varka's Head Magus
            21372, // Varka's Head Guard
            21373, // Varka's Prophet
            21374, // Prophet's Guard
            21375, // Disciple of Prophet
        ]
    }

    getTalkIds(): Array<number> {
        return [ ASEFA, WAHKAN, UDANS_BOX ]
    }

    getQuestStartIds(): Array<number> {
        return [ WAHKAN ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00609_MagicalPowerOfWaterPart1'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31371-02.html':
                state.startQuest()
                break

            case 'open_box':
                if ( !QuestHelper.hasQuestItem( player, KEY ) ) {
                    return this.getPath( '31561-02.html' )
                }

                if ( state.isCondition( 2 ) ) {
                    await QuestHelper.takeSingleItem( player, KEY, 1 )

                    if ( state.getVariable( variableNames.isSpawned ) ) {
                        return this.getPath( '31561-04.html' )
                    }

                    await QuestHelper.giveSingleItem( player, STOLEN_GREEN_TOTEM, 1 )
                    state.setConditionWithSound( 3, true )

                    return this.getPath( '31561-03.html' )
                }

                return

            case eventNames.despawnEyeNpc:
                let npc = L2World.getObjectById( data.characterId ) as L2Npc
                BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.UDAN_HAS_ALREADY_SEEN_YOUR_FACE )
                await npc.deleteMe()

                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableAttackedEvent( data: AttackableAttackedEvent ): Promise<void> {
        let state: QuestState = QuestStateCache.getQuestState( data.attackerPlayerId, this.getName(), false )

        if ( !state || !state.isCondition( 2 ) || !state.getVariable( variableNames.isSpawned ) ) {
            return
        }

        let npc = L2World.getObjectById( data.targetId ) as L2Npc
        npc.setTarget( L2World.getPlayer( data.attackerPlayerId ) )
        await npc.doCastWithHolder( GOW )

        let eyeNpc = QuestHelper.addSpawnAtLocation( UDANS_EYE, npc )
        state.setVariable( variableNames.isSpawned, true )

        this.startQuestTimer( eventNames.despawnEyeNpc, 10000, data.targetId, data.attackerPlayerId )
        return BroadcastHelper.broadcastNpcSayStringId( eyeNpc, NpcSayType.NpcAll, NpcStringIds.YOU_CANT_AVOID_THE_EYES_OF_UDAN )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === WAHKAN ) {
                    if ( player.getLevel() < MIN_LEVEL ) {
                        return this.getPath( '31371-00b.html' )
                    }

                    return this.getPath( QuestHelper.hasAtLeastOneQuestItem( player, ...KETRA_MARKS ) ? '31371-01.htm' : '31371-00a.html' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case WAHKAN:
                        if ( state.isCondition( 1 ) ) {
                            return this.getPath( '31371-03.html' )
                        }

                        break

                    case ASEFA:
                        switch ( state.getCondition() ) {
                            case 1:
                                state.setConditionWithSound( 2, true )
                                return this.getPath( '31372-01.html' )

                            case 2:
                                if ( state.getVariable( variableNames.isSpawned ) ) {
                                    state.unsetVariable( variableNames.isSpawned )

                                    let npc = L2World.getObjectById( data.characterId ) as L2Npc
                                    npc.setTarget( player )
                                    await npc.doCastWithHolder( DISPEL_GOW )

                                    return this.getPath( '31372-03.html' )
                                }

                                return this.getPath( '31372-02.html' )

                            case 3:
                                await QuestHelper.rewardMultipleItems( player, 1, GREEN_TOTEM, WISDOM_STONE )
                                await state.exitQuest( true, true )

                                return this.getPath( '31372-04.html' )
                        }

                        break

                    case UDANS_BOX:
                        if ( state.isCondition( 2 ) ) {
                            return this.getPath( '31561-01.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}