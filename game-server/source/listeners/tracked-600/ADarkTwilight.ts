import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const HIERARCH = 31517
const BLOOD_OF_SAINT = 7169
const minimumLevel = 60
const ITEMS_COUNT_REQUIRED = 300
const ADENA_COUNT = 100000
const XP_COUNT = 162773
const SP_COUNT = 12500

const monsterRewardChances: { [ npcId: number ]: number } = {
    21520: 0.641, // Eye of Splendor
    21523: 0.648, // Flash of Splendor
    21524: 0.692, // Blade of Splendor
    21525: 0.710, // Blade of Splendor
    21526: 0.772, // Wisdom of Splendor
    21529: 0.639, // Soul of Splendor
    21530: 0.683, // Victory of Splendor
    21531: 0.767, // Punishment of Splendor
    21532: 0.795, // Shout of Splendor
    21535: 0.802, // Signet of Splendor
    21536: 0.774, // Crown of Splendor
    21539: 0.848, // Wailing of Splendor
    21540: 0.880, // Wailing of Splendor
    21658: 0.790, // Punishment of Splendor
}

export class ADarkTwilight extends ListenerLogic {
    constructor() {
        super( 'Q00626_ADarkTwilight', 'listeners/tracked-600/ADarkTwilight.ts' )
        this.questId = 626
        this.questItemIds = [ BLOOD_OF_SAINT ]
    }

    getQuestStartIds(): Array<number> {
        return [ HIERARCH ]
    }

    getTalkIds(): Array<number> {
        return [ HIERARCH ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00626_ADarkTwilight'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31517-05.html':
                break

            case '31517-02.htm':
                state.startQuest()
                break

            case 'Exp':
                if ( QuestHelper.getQuestItemsCount( player, BLOOD_OF_SAINT ) < ITEMS_COUNT_REQUIRED ) {
                    return this.getPath( '31517-06.html' )
                }

                await QuestHelper.addExpAndSp( player, XP_COUNT, SP_COUNT )
                await state.exitQuest( true, true )

                return this.getPath( '31517-07.html' )

            case 'Adena':
                if ( QuestHelper.getQuestItemsCount( player, BLOOD_OF_SAINT ) < ITEMS_COUNT_REQUIRED ) {
                    return this.getPath( '31517-06.html' )
                }

                await QuestHelper.giveAdena( player, ADENA_COUNT, true )
                await state.exitQuest( true, true )

                return this.getPath( '31517-07.html' )

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( Math.random() > QuestHelper.getAdjustedChance( BLOOD_OF_SAINT, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        let state: QuestState = this.getRandomPartyMemberStateForCondition( L2World.getPlayer( data.playerId ), 1 )
        if ( !state ) {
            return
        }

        let player = state.getPlayer()
        await QuestHelper.rewardAndProgressState( player, state, BLOOD_OF_SAINT, 1, ITEMS_COUNT_REQUIRED, data.isChampion, 2 )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '31517-01.htm' : '31517-00.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '31517-03.html' )

                    case 2:
                        return this.getPath( '31517-04.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}