import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const MINA = 31388
const RIB_BONE_OF_A_BLACK_MAGUS = 7544
const ZOMBIES_LIVER = 7545
const minimumLevel = 65
const requiredAmount = 200

type MonsterReward = [ number, number ] // itemId, chance
const monsterRewards: { [ npcId: number ]: MonsterReward } = {
    21553: [ ZOMBIES_LIVER, 0.417 ], // Trampled Man
    21554: [ ZOMBIES_LIVER, 0.417 ], // Trampled Man
    21557: [ RIB_BONE_OF_A_BLACK_MAGUS, 0.394 ], // Bone Snatcher
    21558: [ RIB_BONE_OF_A_BLACK_MAGUS, 0.394 ], // Bone Snatcher
    21559: [ RIB_BONE_OF_A_BLACK_MAGUS, 0.436 ], // Bone Maker
    21560: [ RIB_BONE_OF_A_BLACK_MAGUS, 0.430 ], // Bone Shaper
    21561: [ ZOMBIES_LIVER, 0.538 ], // Sacrificed Man
    21563: [ RIB_BONE_OF_A_BLACK_MAGUS, 0.436 ], // Bone Collector
    21564: [ RIB_BONE_OF_A_BLACK_MAGUS, 0.414 ], // Skull Collector
    21565: [ RIB_BONE_OF_A_BLACK_MAGUS, 0.420 ], // Bone Animator
    21566: [ RIB_BONE_OF_A_BLACK_MAGUS, 0.460 ], // Skull Animator
    21567: [ RIB_BONE_OF_A_BLACK_MAGUS, 0.549 ], // Bone Slayer
    21570: [ ZOMBIES_LIVER, 0.508 ], // Ghost of Betrayer
    21572: [ RIB_BONE_OF_A_BLACK_MAGUS, 0.465 ], // Bone Sweeper
    21574: [ RIB_BONE_OF_A_BLACK_MAGUS, 0.586 ], // Bone Grinder
    21575: [ RIB_BONE_OF_A_BLACK_MAGUS, 0.329 ], // Bone Grinder
    21578: [ ZOMBIES_LIVER, 0.649 ], // Behemoth Zombie
    21580: [ RIB_BONE_OF_A_BLACK_MAGUS, 0.462 ], // Bone Caster
    21581: [ RIB_BONE_OF_A_BLACK_MAGUS, 0.505 ], // Bone Puppeteer
    21583: [ RIB_BONE_OF_A_BLACK_MAGUS, 0.475 ], // Bone Scavenger
    21584: [ RIB_BONE_OF_A_BLACK_MAGUS, 0.475 ], // Bone Scavenger
    21596: [ RIB_BONE_OF_A_BLACK_MAGUS, 0.543 ], // Requiem Lord
    21597: [ ZOMBIES_LIVER, 0.510 ], // Requiem Behemoth
    21598: [ ZOMBIES_LIVER, 0.572 ], // Requiem Behemoth
    21599: [ RIB_BONE_OF_A_BLACK_MAGUS, 0.580 ], // Requiem Priest
    21600: [ ZOMBIES_LIVER, 0.561 ], // Requiem Behemoth
    21601: [ RIB_BONE_OF_A_BLACK_MAGUS, 0.677 ], // Requiem Behemoth
}

export class InTheForgottenVillage extends ListenerLogic {
    constructor() {
        super( 'Q00633_InTheForgottenVillage', 'listeners/tracked-600/InTheForgottenVillage.ts' )
        this.questId = 633
        this.questItemIds = [
            RIB_BONE_OF_A_BLACK_MAGUS,
            ZOMBIES_LIVER,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ MINA ]
    }

    getTalkIds(): Array<number> {
        return [ MINA ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewards ).map( value => _.parseInt( value ) )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31388-03.htm':
                state.startQuest()
                break

            case '31388-04.html':
            case '31388-05.html':
            case '31388-06.html':
                break

            case '31388-07.html':
                if ( !state.isCondition( 2 ) ) {
                    return
                }

                if ( QuestHelper.getQuestItemsCount( player, RIB_BONE_OF_A_BLACK_MAGUS ) >= requiredAmount ) {
                    await QuestHelper.takeSingleItem( player, RIB_BONE_OF_A_BLACK_MAGUS, -1 )
                    await QuestHelper.giveAdena( player, 25000, true )
                    await QuestHelper.addExpAndSp( player, 305235, 0 )

                    state.setConditionWithSound( 1, true )
                    break
                }

                return this.getPath( '31388-08.html' )

            case '31388-09.html':
                await state.exitQuest( true, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let [ itemId, chance ]: MonsterReward = monsterRewards[ data.npcId ]
        if ( Math.random() > QuestHelper.getAdjustedChance( itemId, chance, data.isChampion ) ) {
            return
        }

        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), -1, 3, L2World.getObjectById( data.targetId ) )
        if ( !state ) {
            return
        }

        let player = state.getPlayer()
        if ( itemId === RIB_BONE_OF_A_BLACK_MAGUS ) {
            if ( !state.isCondition( 1 ) ) {
                return
            }

            await QuestHelper.rewardAndProgressState( player, state, itemId, 1, requiredAmount, data.isChampion, 2 )
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, itemId, 1, data.isChampion )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '31388-01.htm' : '31388-02.htm' )

            case QuestStateValues.STARTED:
                return this.getPath( QuestHelper.getQuestItemsCount( player, RIB_BONE_OF_A_BLACK_MAGUS ) >= requiredAmount ? '31388-04.html' : '31388-05.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00633_InTheForgottenVillage'
    }
}