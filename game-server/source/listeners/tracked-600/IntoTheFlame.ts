import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const KLEIN = 31540
const HILDA = 31271
const vacualiteOreItemId = 7265
const VACUALITE = 7266
const VACUALITE_FLOATING_STONE = 7267
const minimumLevel = 60
const requireOreAmount = 50

const monsterRewardChances: { [ npcId: number ]: number } = {
    21274: 0.630,
    21276: 0.630,
    21282: 0.670,
    21283: 0.670,
    21284: 0.670,
    21290: 0.710,
    21291: 0.710,
    21292: 0.710,
}

export class IntoTheFlame extends ListenerLogic {
    constructor() {
        super( 'Q00618_IntoTheFlame', 'listeners/tracked-600/IntoTheFlame.ts' )
        this.questId = 618
        this.questItemIds = [ vacualiteOreItemId, VACUALITE ]
    }

    getQuestStartIds(): Array<number> {
        return [ KLEIN ]
    }

    getTalkIds(): Array<number> {
        return [ HILDA, KLEIN ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31540-03.htm':
                state.startQuest()
                break

            case '31540-05.html':
                if ( !QuestHelper.hasQuestItem( player, VACUALITE ) ) {
                    return this.getPath( '31540-03.htm' )
                }

                await QuestHelper.rewardSingleItem( player, VACUALITE_FLOATING_STONE, 1 )
                await state.exitQuest( true, true )

                break

            case '31271-02.html':
                if ( state.isCondition( 1 ) ) {
                    state.setConditionWithSound( 2, true )

                    break
                }

                return

            case '31271-05.html':
                if ( QuestHelper.getQuestItemsCount( player, vacualiteOreItemId ) >= requireOreAmount
                        && state.isCondition( 3 ) ) {
                    await QuestHelper.takeSingleItem( player, vacualiteOreItemId, -1 )
                    await QuestHelper.giveSingleItem( player, VACUALITE, 1 )

                    state.setConditionWithSound( 4, true )
                    break
                }

                return this.getPath( '31271-03.html' )

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00618_IntoTheFlame'
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( Math.random() > QuestHelper.getAdjustedChance( vacualiteOreItemId, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        let state: QuestState = this.getRandomPartyMemberStateForCondition( L2World.getPlayer( data.playerId ), 2 )
        if ( !state ) {
            return
        }

        let player = state.getPlayer()
        await QuestHelper.rewardAndProgressState( player, state, vacualiteOreItemId, 1, requireOreAmount, data.isChampion, 3 )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== KLEIN ) {
                    break
                }

                return this.getPath( player.getLevel() < minimumLevel ? '31540-01.html' : '31540-02.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case KLEIN:
                        return this.getPath( state.isCondition( 4 ) ? '31540-04.html' : '31540-03.htm' )

                    case HILDA:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '31271-01.html' )

                            case 2:
                                return this.getPath( '31271-03.html' )

                            case 3:
                                return this.getPath( '31271-04.html' )

                            case 4:
                                return this.getPath( '31271-06.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}