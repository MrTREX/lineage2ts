import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import _ from 'lodash'
import { createItemDefinition } from '../../gameService/interface/ItemDefinition'

const GHOST_OF_WIGOTH_1 = 31452
const NAMELESS_SPIRIT = 31453
const GHOST_OF_WIGOTH_2 = 31454
const GHOST_CHAMBERLAIN_OF_ELMOREDEN_1 = 31919
const CONQUERORS_SEPULCHER_MANAGER = 31921
const EMPERORS_SEPULCHER_MANAGER = 31922
const GREAT_SAGES_SEPULCHER_MANAGER = 31923
const JUDGES_SEPULCHER_MANAGER = 31924
const BROKEN_RELIC_PART = 7254
const SEALED_BOX = 7255
const GOBLET_OF_ALECTIA = 7256
const GOBLET_OF_TISHAS = 7257
const GOBLET_OF_MEKARA = 7258
const GOBLET_OF_MORIGUL = 7259
const CHAPEL_KEY = 7260
const USED_GRAVE_PASS = 7261
const ANTIQUE_BROOCH = 7262
const minimumLevel = 74
const ENTER_LOC = new Location( 170000, -88250, -2912, 0 )
const EXIT_LOC = new Location( 169584, -91008, -2912, 0 )
const CORD = createItemDefinition( 1884, 42 )
const METALLIC_FIBER = createItemDefinition( 1895, 36 )
const MITHRIL_ORE = createItemDefinition( 1876, 4 )
const COARSE_BONE_POWDER = createItemDefinition( 1881, 6 )
const METALLIC_THREAD = createItemDefinition( 5549, 8 )
const ORIHARUKON_ORE = createItemDefinition( 1874, 1 )
const COMPOUND_BRAID = createItemDefinition( 1889, 1 )
const ADAMANTITE_NUGGET = createItemDefinition( 1877, 1 )
const CRAFTED_LEATHER = createItemDefinition( 1894, 1 )
const ASOFE = createItemDefinition( 4043, 1 )
const SYNTETHIC_COKES = createItemDefinition( 1888, 1 )
const MOLD_LUBRICANT = createItemDefinition( 4040, 1 )
const MITHRIL_ALLOY = createItemDefinition( 1890, 1 )
const DURABLE_METAL_PLATE = createItemDefinition( 5550, 1 )
const ORIHARUKON = createItemDefinition( 1893, 1 )
const MAESTRO_ANVIL_LOCK = createItemDefinition( 4046, 1 )
const MAESTRO_MOLD = createItemDefinition( 4048, 1 )
const BRAIDED_HEMP = createItemDefinition( 1878, 8 )
const LEATHER = createItemDefinition( 1882, 24 )
const COKES = createItemDefinition( 1879, 4 )
const STEEL = createItemDefinition( 1880, 6 )
const HIGH_GRADE_SUEDE = createItemDefinition( 1885, 6 )
const STONE_OF_PURITY = createItemDefinition( 1875, 1 )
const STEEL_MOLD = createItemDefinition( 1883, 1 )
const METAL_HARDENER = createItemDefinition( 5220, 1 )
const MOLD_GLUE = createItemDefinition( 4039, 1 )
const THONS = createItemDefinition( 4044, 1 )
const VARNISH_OF_PURITY = createItemDefinition( 1887, 1 )
const ENRIA = createItemDefinition( 4042, 1 )
const SILVER_MOLD = createItemDefinition( 1886, 1 )
const MOLD_HARDENER = createItemDefinition( 4041, 1 )
const BLACKSMITHS_FRAMES = createItemDefinition( 1892, 1 )
const ARTISANS_FRAMES = createItemDefinition( 1891, 1 )
const CRAFTSMAN_MOLD = createItemDefinition( 4047, 1 )
const ENCHANT_ARMOR_A_GRADE = createItemDefinition( 730, 1 )
const ENCHANT_ARMOR_B_GRADE = createItemDefinition( 948, 1 )
const ENCHANT_ARMOR_S_GRADE = createItemDefinition( 960, 1 )
const ENCHANT_WEAPON_A_GRADE = createItemDefinition( 729, 1 )
const ENCHANT_WEAPON_B_GRADE = createItemDefinition( 947, 1 )
const ENCHANT_WEAPON_S_GRADE = createItemDefinition( 959, 1 )
const SEALED_TATEOSSIAN_EARRING_PART = createItemDefinition( 6698, 1 )
const SEALED_TATEOSSIAN_RING_GEM = createItemDefinition( 6699, 1 )
const SEALED_TATEOSSIAN_NECKLACE_CHAIN = createItemDefinition( 6700, 1 )
const SEALED_IMPERIAL_CRUSADER_BREASTPLATE_PART = createItemDefinition( 6701, 1 )
const SEALED_IMPERIAL_CRUSADER_GAITERS_PATTERN = createItemDefinition( 6702, 1 )
const SEALED_IMPERIAL_CRUSADER_GAUNTLETS_DESIGN = createItemDefinition( 6703, 1 )
const SEALED_IMPERIAL_CRUSADER_BOOTS_DESIGN = createItemDefinition( 6704, 1 )
const SEALED_IMPERIAL_CRUSADER_SHIELD_PART = createItemDefinition( 6705, 1 )
const SEALED_IMPERIAL_CRUSADER_HELMET_PATTERN = createItemDefinition( 6706, 1 )
const SEALED_DRACONIC_LEATHER_ARMOR_PART = createItemDefinition( 6707, 1 )
const SEALED_DRACONIC_LEATHER_GLOVES_FABRIC = createItemDefinition( 6708, 1 )
const SEALED_DRACONIC_LEATHER_BOOTS_DESIGN = createItemDefinition( 6709, 1 )
const SEALED_DRACONIC_LEATHER_HELMET_PATTERN = createItemDefinition( 6710, 1 )
const SEALED_MAJOR_ARCANA_ROBE_PART = createItemDefinition( 6711, 1 )
const SEALED_MAJOR_ARCANA_GLOVES_FABRIC = createItemDefinition( 6712, 1 )
const SEALED_MAJOR_ARCANA_BOOTS_DESIGN = createItemDefinition( 6713, 1 )
const SEALED_MAJOR_ARCANA_CIRCLET_PATTERN = createItemDefinition( 6714, 1 )
const FORGOTTEN_BLADE_EDGE = createItemDefinition( 6688, 1 )
const BASALT_BATTLEHAMMER_HEAD = createItemDefinition( 6689, 1 )
const IMPERIAL_STAFF_HEAD = createItemDefinition( 6690, 1 )
const ANGEL_SLAYER_BLADE = createItemDefinition( 6691, 1 )
const DRACONIC_BOW_SHAFT = createItemDefinition( 7579, 1 )
const DRAGON_HUNTER_AXE_BLADE = createItemDefinition( 6693, 1 )
const SAINT_SPEAR_BLADE = createItemDefinition( 6694, 1 )
const DEMON_SPLINTER_BLADE = createItemDefinition( 6695, 1 )
const HEAVENS_DIVIDER_EDGE = createItemDefinition( 6696, 1 )
const ARCANA_MACE_HEAD = createItemDefinition( 6697, 1 )
const HALISHA_ALECTIA = 25339
const HALISHA_TISHAS = 25342
const HALISHA_MEKARA = 25346
const HALISHA_MORIGUL = 25349

const mobGroupOne : Record<number, number> = {
    18141: 0.9,
    18142: 0.9,
    18143: 0.9,
    18144: 0.9,
    18145: 0.76,
    18146: 0.78,
    18147: 0.73,
    18148: 0.85,
    18149: 0.75,
    18230: 0.58,
}

const mobGroupTwo : Record<number, number> = {
    18120: 0.51,
    18121: 0.44,
    18122: 0.10,
    18123: 0.51,
    18124: 0.44,
    18125: 0.10,
    18126: 0.51,
    18127: 0.44,
    18128: 0.10,
    18129: 0.51,
    18130: 0.44,
    18131: 0.10,
    18132: 0.54,
    18133: 0.42,
    18134: 0.7,
    18135: 0.42,
    18136: 0.42,
    18137: 0.6,
    18138: 0.41,
    18139: 0.39,
    18140: 0.41,
    18166: 0.8,
    18167: 0.7,
    18168: 0.10,
    18169: 0.6,
    18170: 0.7,
    18171: 0.11,
    18172: 0.6,
    18173: 0.17,
    18174: 0.45,
    18175: 0.10,
    18176: 0.17,
    18177: 0.45,
    18178: 0.10,
    18179: 0.17,
    18180: 0.45,
    18181: 0.10,
    18182: 0.17,
    18183: 0.45,
    18184: 0.10,
    18185: 0.46,
    18186: 0.47,
    18187: 0.42,
    18188: 0.7,
    18189: 0.42,
    18190: 0.42,
    18191: 0.6,
    18192: 0.41,
    18193: 0.39,
    18194: 0.41,
    18195: 0.8,
    18220: 0.47,
    18221: 0.51,
    18222: 0.43,
    18223: 0.7,
    18224: 0.44,
    18225: 0.43,
    18226: 0.6,
    18227: 0.82,
    18229: 0.41,
}

const mobGroupThree = {
    18212: 0.50,
    18213: 0.50,
    18214: 0.50,
    18215: 0.50,
    18216: 0.50,
    18217: 0.50,
    18218: 0.50,
    18219: 0.50,
}

const variableNames = {
    value: 'vl',
}

export class FourGoblets extends ListenerLogic {
    constructor() {
        super( 'Q00620_FourGoblets', 'listeners/tracked-600/FourGoblets.ts' )
        this.questId = 620
        this.questItemIds = [
            SEALED_BOX,
            GOBLET_OF_ALECTIA,
            GOBLET_OF_TISHAS,
            GOBLET_OF_MEKARA,
            GOBLET_OF_MORIGUL,
            USED_GRAVE_PASS,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ NAMELESS_SPIRIT ]
    }

    getTalkIds(): Array<number> {
        return [
            NAMELESS_SPIRIT,
            GHOST_OF_WIGOTH_1,
            GHOST_OF_WIGOTH_2,
            GHOST_CHAMBERLAIN_OF_ELMOREDEN_1,
            CONQUERORS_SEPULCHER_MANAGER,
            EMPERORS_SEPULCHER_MANAGER,
            GREAT_SAGES_SEPULCHER_MANAGER,
            JUDGES_SEPULCHER_MANAGER,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            ..._.keys( mobGroupOne ).map( value => _.parseInt( value ) ),
            ..._.keys( mobGroupTwo ).map( value => _.parseInt( value ) ),
            ..._.keys( mobGroupThree ).map( value => _.parseInt( value ) ),
            HALISHA_ALECTIA,
            HALISHA_TISHAS,
            HALISHA_MEKARA,
            HALISHA_MORIGUL,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00620_FourGoblets'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31453-02.htm':
            case '31453-03.htm':
            case '31453-04.htm':
            case '31453-05.htm':
            case '31453-06.htm':
            case '31453-07.htm':
            case '31453-08.htm':
            case '31453-09.htm':
            case '31453-10.htm':
            case '31453-11.htm':
            case '31453-16.html':
            case '31453-17.html':
            case '31453-18.html':
            case '31453-19.html':
            case '31453-20.html':
            case '31453-21.html':
            case '31453-22.html':
            case '31453-23.html':
            case '31453-24.html':
            case '31453-25.html':
            case '31453-27.html':
            case '31452-04.html':
                break

            case '31453-12.htm':
                state.startQuest()
                state.setMemoState( 0 )
                if ( QuestHelper.hasQuestItem( player, ANTIQUE_BROOCH ) ) {
                    state.setConditionWithSound( 2 )
                }

                break

            case '31453-15.html':
                await QuestHelper.takeSingleItem( player, CHAPEL_KEY, -1 )
                await state.exitQuest( true, true )

                break

            case '31453-28.html':
                if ( QuestHelper.hasQuestItems( player, GOBLET_OF_ALECTIA, GOBLET_OF_TISHAS, GOBLET_OF_MEKARA, GOBLET_OF_MORIGUL ) ) {
                    await QuestHelper.takeMultipleItems( player, 1, GOBLET_OF_ALECTIA, GOBLET_OF_TISHAS, GOBLET_OF_MEKARA, GOBLET_OF_MORIGUL )
                    await QuestHelper.giveSingleItem( player, ANTIQUE_BROOCH, 1 )

                    state.setConditionWithSound( 2, true )
                    break
                }

                return

            case '31454-02.html':
                await player.teleportToLocation( ENTER_LOC, true )
                break

            case '31454-04.html':
                if ( state.getVariable( variableNames.value ) && QuestHelper.getQuestItemsCount( player, BROKEN_RELIC_PART ) >= 1000 ) {
                    break
                }

                return

            case '6881':
            case '6883':
            case '6885':
            case '6887':
            case '6891':
            case '6893':
            case '6895':
            case '6897':
            case '6899':
            case '7580':
                if ( state.getVariable( variableNames.value ) && QuestHelper.getQuestItemsCount( player, BROKEN_RELIC_PART ) >= 1000 ) {

                    await QuestHelper.takeSingleItem( player, BROKEN_RELIC_PART, 1000 )
                    await QuestHelper.giveSingleItem( player, _.parseInt( data.eventName ), 1 )

                    return this.getPath( '31454-05.html' )
                }

                return

            case '31454-07.html':
                if ( state.getVariable( variableNames.value ) && QuestHelper.hasQuestItem( player, SEALED_BOX ) ) {
                    if ( Math.random() < 0.98 ) {
                        return this.getPath( await this.rewardPlayer( player ) ? data.eventName : '31454-08.html' )
                    }

                    await QuestHelper.takeSingleItem( player, SEALED_BOX, 1 )
                    return this.getPath( '31454-09.html' )
                }

                return

            case 'EXIT':
                await QuestHelper.takeSingleItem( player, CHAPEL_KEY, -1 )
                await player.teleportToLocation( EXIT_LOC, true )
                return

            case '31919-02.html':
                if ( QuestHelper.hasQuestItems( player, SEALED_BOX ) ) {
                    if ( Math.random() < 0.5 ) {
                        return this.getPath( await this.rewardPlayer( player ) ? data.eventName : '31919-03.html' )
                    }

                    await QuestHelper.takeSingleItem( player, SEALED_BOX, 1 )
                    return this.getPath( '31919-04.html' )
                }

                return this.getPath( '31919-05.html' )

            case 'ENTER':
                // TODO : implement FourSepulchersManager.tryEntry(npc, player);
                return QuestHelper.getFeatureNotImplementedMessagePath()

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async rewardPlayer( player: L2PcInstance ): Promise<boolean> {
        await QuestHelper.takeSingleItem( player, SEALED_BOX, 1 )
        let hasReceivedReward: boolean = false

        switch ( _.random( 4 ) ) {
            case 0:
                hasReceivedReward = true
                await QuestHelper.giveAdena( player, 10000, true )
                break

            case 1:
                if ( _.random( 1000 ) < 848 ) {
                    hasReceivedReward = true

                    let chance: number = _.random( 1000 )
                    if ( chance < 43 ) {
                        await QuestHelper.rewardSingleItem( player, CORD.id, CORD.count )
                    } else if ( chance < 66 ) {
                        await QuestHelper.rewardSingleItem( player, METALLIC_FIBER.id, METALLIC_FIBER.count )
                    } else if ( chance < 184 ) {
                        await QuestHelper.rewardSingleItem( player, MITHRIL_ORE.id, MITHRIL_ORE.count )
                    } else if ( chance < 250 ) {
                        await QuestHelper.rewardSingleItem( player, COARSE_BONE_POWDER.id, COARSE_BONE_POWDER.count )
                    } else if ( chance < 287 ) {
                        await QuestHelper.rewardSingleItem( player, METALLIC_THREAD.id, METALLIC_THREAD.count )
                    } else if ( chance < 484 ) {
                        await QuestHelper.rewardSingleItem( player, ORIHARUKON_ORE.id, ORIHARUKON_ORE.count )
                    } else if ( chance < 681 ) {
                        await QuestHelper.rewardSingleItem( player, COMPOUND_BRAID.id, COMPOUND_BRAID.count )
                    } else if ( chance < 799 ) {
                        await QuestHelper.rewardSingleItem( player, ADAMANTITE_NUGGET.id, ADAMANTITE_NUGGET.count )
                    } else if ( chance < 902 ) {
                        await QuestHelper.rewardSingleItem( player, CRAFTED_LEATHER.id, CRAFTED_LEATHER.count )
                    } else {
                        await QuestHelper.rewardSingleItem( player, ASOFE.id, ASOFE.count )
                    }
                }

                if ( _.random( 1000 ) < 323 ) {
                    hasReceivedReward = true

                    let chance = _.random( 1000 )
                    if ( chance < 335 ) {
                        await QuestHelper.rewardSingleItem( player, SYNTETHIC_COKES.id, SYNTETHIC_COKES.count )
                    } else if ( chance < 556 ) {
                        await QuestHelper.rewardSingleItem( player, MOLD_LUBRICANT.id, MOLD_LUBRICANT.count )
                    } else if ( chance < 725 ) {
                        await QuestHelper.rewardSingleItem( player, MITHRIL_ALLOY.id, MITHRIL_ALLOY.count )
                    } else if ( chance < 872 ) {
                        await QuestHelper.rewardSingleItem( player, DURABLE_METAL_PLATE.id, DURABLE_METAL_PLATE.count )
                    } else if ( chance < 962 ) {
                        await QuestHelper.rewardSingleItem( player, ORIHARUKON.id, ORIHARUKON.count )
                    } else if ( chance < 986 ) {
                        await QuestHelper.rewardSingleItem( player, MAESTRO_ANVIL_LOCK.id, MAESTRO_ANVIL_LOCK.count )
                    } else {
                        await QuestHelper.rewardSingleItem( player, MAESTRO_MOLD.id, MAESTRO_MOLD.count )
                    }
                }
                break

            case 2:
                if ( _.random( 1000 ) < 847 ) {
                    hasReceivedReward = true

                    let chance = _.random( 1000 )
                    if ( chance < 148 ) {
                        await QuestHelper.rewardSingleItem( player, BRAIDED_HEMP.id, BRAIDED_HEMP.count )
                    } else if ( chance < 175 ) {
                        await QuestHelper.rewardSingleItem( player, LEATHER.id, LEATHER.count )
                    } else if ( chance < 273 ) {
                        await QuestHelper.rewardSingleItem( player, COKES.id, COKES.count )
                    } else if ( chance < 322 ) {
                        await QuestHelper.rewardSingleItem( player, STEEL.id, STEEL.count )
                    } else if ( chance < 357 ) {
                        await QuestHelper.rewardSingleItem( player, HIGH_GRADE_SUEDE.id, HIGH_GRADE_SUEDE.count )
                    } else if ( chance < 554 ) {
                        await QuestHelper.rewardSingleItem( player, STONE_OF_PURITY.id, STONE_OF_PURITY.count )
                    } else if ( chance < 685 ) {
                        await QuestHelper.rewardSingleItem( player, STEEL_MOLD.id, STEEL_MOLD.count )
                    } else if ( chance < 803 ) {
                        await QuestHelper.rewardSingleItem( player, METAL_HARDENER.id, METAL_HARDENER.count )
                    } else if ( chance < 901 ) {
                        await QuestHelper.rewardSingleItem( player, MOLD_GLUE.id, MOLD_GLUE.count )
                    } else {
                        await QuestHelper.rewardSingleItem( player, THONS.id, THONS.count )
                    }
                }

                if ( _.random( 1000 ) < 251 ) {
                    hasReceivedReward = true

                    let chance = _.random( 1000 )
                    if ( chance < 350 ) {
                        await QuestHelper.rewardSingleItem( player, VARNISH_OF_PURITY.id, VARNISH_OF_PURITY.count )
                    } else if ( chance < 587 ) {
                        await QuestHelper.rewardSingleItem( player, ENRIA.id, ENRIA.count )
                    } else if ( chance < 798 ) {
                        await QuestHelper.rewardSingleItem( player, SILVER_MOLD.id, SILVER_MOLD.count )
                    } else if ( chance < 922 ) {
                        await QuestHelper.rewardSingleItem( player, MOLD_HARDENER.id, MOLD_HARDENER.count )
                    } else if ( chance < 966 ) {
                        await QuestHelper.rewardSingleItem( player, BLACKSMITHS_FRAMES.id, BLACKSMITHS_FRAMES.count )
                    } else if ( chance < 996 ) {
                        await QuestHelper.rewardSingleItem( player, ARTISANS_FRAMES.id, ARTISANS_FRAMES.count )
                    } else {
                        await QuestHelper.rewardSingleItem( player, CRAFTSMAN_MOLD.id, CRAFTSMAN_MOLD.count )
                    }
                }

                break

            case 3:
                if ( _.random( 1000 ) < 31 ) {
                    hasReceivedReward = true

                    let chance = _.random( 1000 )
                    if ( chance < 223 ) {
                        await QuestHelper.rewardSingleItem( player, ENCHANT_ARMOR_A_GRADE.id, ENCHANT_ARMOR_A_GRADE.count )
                    } else if ( chance < 893 ) {
                        await QuestHelper.rewardSingleItem( player, ENCHANT_ARMOR_B_GRADE.id, ENCHANT_ARMOR_B_GRADE.count )
                    } else {
                        await QuestHelper.rewardSingleItem( player, ENCHANT_ARMOR_S_GRADE.id, ENCHANT_ARMOR_S_GRADE.count )
                    }
                }

                if ( _.random( 1000 ) < 5 ) {
                    hasReceivedReward = true

                    let chance = _.random( 1000 )
                    if ( chance < 202 ) {
                        await QuestHelper.rewardSingleItem( player, ENCHANT_WEAPON_A_GRADE.id, ENCHANT_WEAPON_A_GRADE.count )
                    } else if ( chance < 928 ) {
                        await QuestHelper.rewardSingleItem( player, ENCHANT_WEAPON_B_GRADE.id, ENCHANT_WEAPON_B_GRADE.count )
                    } else {
                        await QuestHelper.rewardSingleItem( player, ENCHANT_WEAPON_S_GRADE.id, ENCHANT_WEAPON_S_GRADE.count )
                    }
                }
                break

            case 4:
                if ( _.random( 1000 ) < 329 ) {
                    hasReceivedReward = true

                    let chance = _.random( 1000 )
                    if ( chance < 88 ) {
                        await QuestHelper.rewardSingleItem( player, SEALED_TATEOSSIAN_EARRING_PART.id, SEALED_TATEOSSIAN_EARRING_PART.count )
                    } else if ( chance < 185 ) {
                        await QuestHelper.rewardSingleItem( player, SEALED_TATEOSSIAN_RING_GEM.id, SEALED_TATEOSSIAN_RING_GEM.count )
                    } else if ( chance < 238 ) {
                        await QuestHelper.rewardSingleItem( player, SEALED_TATEOSSIAN_NECKLACE_CHAIN.id, SEALED_TATEOSSIAN_NECKLACE_CHAIN.count )
                    } else if ( chance < 262 ) {
                        await QuestHelper.rewardSingleItem( player, SEALED_IMPERIAL_CRUSADER_BREASTPLATE_PART.id, SEALED_IMPERIAL_CRUSADER_BREASTPLATE_PART.count )
                    } else if ( chance < 292 ) {
                        await QuestHelper.rewardSingleItem( player, SEALED_IMPERIAL_CRUSADER_GAITERS_PATTERN.id, SEALED_IMPERIAL_CRUSADER_GAITERS_PATTERN.count )
                    } else if ( chance < 356 ) {
                        await QuestHelper.rewardSingleItem( player, SEALED_IMPERIAL_CRUSADER_GAUNTLETS_DESIGN.id, SEALED_IMPERIAL_CRUSADER_GAUNTLETS_DESIGN.count )
                    } else if ( chance < 420 ) {
                        await QuestHelper.rewardSingleItem( player, SEALED_IMPERIAL_CRUSADER_BOOTS_DESIGN.id, SEALED_IMPERIAL_CRUSADER_BOOTS_DESIGN.count )
                    } else if ( chance < 482 ) {
                        await QuestHelper.rewardSingleItem( player, SEALED_IMPERIAL_CRUSADER_SHIELD_PART.id, SEALED_IMPERIAL_CRUSADER_SHIELD_PART.count )
                    } else if ( chance < 554 ) {
                        await QuestHelper.rewardSingleItem( player, SEALED_IMPERIAL_CRUSADER_HELMET_PATTERN.id, SEALED_IMPERIAL_CRUSADER_HELMET_PATTERN.count )
                    } else if ( chance < 576 ) {
                        await QuestHelper.rewardSingleItem( player, SEALED_DRACONIC_LEATHER_ARMOR_PART.id, SEALED_DRACONIC_LEATHER_ARMOR_PART.count )
                    } else if ( chance < 640 ) {
                        await QuestHelper.rewardSingleItem( player, SEALED_DRACONIC_LEATHER_GLOVES_FABRIC.id, SEALED_DRACONIC_LEATHER_GLOVES_FABRIC.count )
                    } else if ( chance < 704 ) {
                        await QuestHelper.rewardSingleItem( player, SEALED_DRACONIC_LEATHER_BOOTS_DESIGN.id, SEALED_DRACONIC_LEATHER_BOOTS_DESIGN.count )
                    } else if ( chance < 777 ) {
                        await QuestHelper.rewardSingleItem( player, SEALED_DRACONIC_LEATHER_HELMET_PATTERN.id, SEALED_DRACONIC_LEATHER_HELMET_PATTERN.count )
                    } else if ( chance < 799 ) {
                        await QuestHelper.rewardSingleItem( player, SEALED_MAJOR_ARCANA_ROBE_PART.id, SEALED_MAJOR_ARCANA_ROBE_PART.count )
                    } else if ( chance < 863 ) {
                        await QuestHelper.rewardSingleItem( player, SEALED_MAJOR_ARCANA_GLOVES_FABRIC.id, SEALED_MAJOR_ARCANA_GLOVES_FABRIC.count )
                    } else if ( chance < 927 ) {
                        await QuestHelper.rewardSingleItem( player, SEALED_MAJOR_ARCANA_BOOTS_DESIGN.id, SEALED_MAJOR_ARCANA_BOOTS_DESIGN.count )
                    } else {
                        await QuestHelper.rewardSingleItem( player, SEALED_MAJOR_ARCANA_CIRCLET_PATTERN.id, SEALED_MAJOR_ARCANA_CIRCLET_PATTERN.count )
                    }
                }

                if ( _.random( 1000 ) < 54 ) {
                    hasReceivedReward = true

                    let chance = _.random( 1000 )
                    if ( chance < 100 ) {
                        await QuestHelper.rewardSingleItem( player, FORGOTTEN_BLADE_EDGE.id, FORGOTTEN_BLADE_EDGE.count )
                    } else if ( chance < 198 ) {
                        await QuestHelper.rewardSingleItem( player, BASALT_BATTLEHAMMER_HEAD.id, BASALT_BATTLEHAMMER_HEAD.count )
                    } else if ( chance < 298 ) {
                        await QuestHelper.rewardSingleItem( player, IMPERIAL_STAFF_HEAD.id, IMPERIAL_STAFF_HEAD.count )
                    } else if ( chance < 398 ) {
                        await QuestHelper.rewardSingleItem( player, ANGEL_SLAYER_BLADE.id, ANGEL_SLAYER_BLADE.count )
                    } else if ( chance < 499 ) {
                        await QuestHelper.rewardSingleItem( player, DRACONIC_BOW_SHAFT.id, DRACONIC_BOW_SHAFT.count )
                    } else if ( chance < 601 ) {
                        await QuestHelper.rewardSingleItem( player, DRAGON_HUNTER_AXE_BLADE.id, DRAGON_HUNTER_AXE_BLADE.count )
                    } else if ( chance < 703 ) {
                        await QuestHelper.rewardSingleItem( player, SAINT_SPEAR_BLADE.id, SAINT_SPEAR_BLADE.count )
                    } else if ( chance < 801 ) {
                        await QuestHelper.rewardSingleItem( player, DEMON_SPLINTER_BLADE.id, DEMON_SPLINTER_BLADE.count )
                    } else if ( chance < 902 ) {
                        await QuestHelper.rewardSingleItem( player, HEAVENS_DIVIDER_EDGE.id, HEAVENS_DIVIDER_EDGE.count )
                    } else {
                        await QuestHelper.rewardSingleItem( player, ARCANA_MACE_HEAD.id, ARCANA_MACE_HEAD.count )
                    }
                }

                break
        }

        return hasReceivedReward
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        switch ( data.npcId ) {
            case HALISHA_ALECTIA:
            case HALISHA_TISHAS:
            case HALISHA_MEKARA:
            case HALISHA_MORIGUL:
                await this.runPlayerPartyActions( data )
                return
        }

        let player: L2PcInstance = this.getRandomPartyMemberPerCondition( L2World.getPlayer( data.playerId ), -1, 3, L2World.getObjectById( data.targetId ) )
        if ( !player ) {
            return
        }


        if ( _.has( mobGroupOne, data.npcId ) ) {
            if ( Math.random() > QuestHelper.getAdjustedChance( SEALED_BOX, mobGroupOne[ data.npcId ], data.isChampion ) ) {
                return
            }

            await QuestHelper.rewardSingleQuestItem( player, SEALED_BOX, 1, data.isChampion )
            return
        }

        if ( _.has( mobGroupTwo, data.npcId ) ) {
            let amount: number = Math.random() < QuestHelper.getAdjustedChance( SEALED_BOX, mobGroupTwo[ data.npcId ], data.isChampion ) ? 2 : 1
            await QuestHelper.rewardSingleQuestItem( player, SEALED_BOX, amount, data.isChampion )
            return
        }

        let amount: number = Math.random() < QuestHelper.getAdjustedChance( SEALED_BOX, mobGroupTwo[ data.npcId ], data.isChampion ) ? 5 : 4
        await QuestHelper.rewardSingleQuestItem( player, SEALED_BOX, amount, data.isChampion )
    }

    runPlayerPartyActions( data: AttackableKillEvent ): Promise<void> {
        return QuestHelper.runStateActionForParty( data.playerId, data.targetId, this.getName(), async ( state: QuestState, player: L2PcInstance, npc: L2Npc ): Promise<void> => {
            if ( !state ) {
                return
            }

            if ( QuestHelper.hasQuestItem( player, ANTIQUE_BROOCH ) ) {
                return
            }

            if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
                return
            }

            state.setVariable( variableNames.value, 2 )

            switch ( npc.getId() ) {
                case HALISHA_ALECTIA:
                    if ( !QuestHelper.hasQuestItem( player, GOBLET_OF_ALECTIA ) ) {
                        await QuestHelper.giveSingleItem( player, GOBLET_OF_ALECTIA, 1 )
                    }

                    return

                case HALISHA_TISHAS:
                    if ( !QuestHelper.hasQuestItem( player, GOBLET_OF_TISHAS ) ) {
                        await QuestHelper.giveSingleItem( player, GOBLET_OF_TISHAS, 1 )
                    }

                    return

                case HALISHA_MEKARA:
                    if ( !QuestHelper.hasQuestItem( player, GOBLET_OF_MEKARA ) ) {
                        await QuestHelper.giveSingleItem( player, GOBLET_OF_MEKARA, 1 )
                    }

                    return

                case HALISHA_MORIGUL:
                    if ( !QuestHelper.hasQuestItem( player, GOBLET_OF_MORIGUL ) ) {
                        await QuestHelper.giveSingleItem( player, GOBLET_OF_MORIGUL, 1 )
                    }

                    return
            }
        } )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '31453-01.htm' : '31453-13.html' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case NAMELESS_SPIRIT:
                        if ( !QuestHelper.hasQuestItem( player, ANTIQUE_BROOCH ) ) {
                            if ( QuestHelper.hasQuestItems( player, GOBLET_OF_ALECTIA, GOBLET_OF_TISHAS, GOBLET_OF_MEKARA, GOBLET_OF_MORIGUL ) ) {
                                return this.getPath( '31453-26.html' )
                            }

                            return this.getPath( '31453-14.html' )
                        }

                        return this.getPath( '31453-29.html' )

                    case GHOST_OF_WIGOTH_1:
                        if ( !QuestHelper.hasQuestItem( player, ANTIQUE_BROOCH ) ) {
                            if ( QuestHelper.hasQuestItems( player, GOBLET_OF_ALECTIA, GOBLET_OF_TISHAS, GOBLET_OF_MEKARA, GOBLET_OF_MORIGUL ) ) {
                                return this.getPath( '31452-01.html' )
                            }

                            if ( QuestHelper.getItemsSumCount( player, GOBLET_OF_ALECTIA, GOBLET_OF_TISHAS, GOBLET_OF_MEKARA, GOBLET_OF_MORIGUL ) < 3 ) {
                                return this.getPath( '31452-02.html' )
                            }

                            return this.getPath( '31452-03.html' )
                        }

                        return this.getPath( '31452-05.html' )

                    case GHOST_OF_WIGOTH_2:
                        let value: number = state.getVariable( variableNames.value )
                        let brokenRelicPartCount: number = QuestHelper.getQuestItemsCount( player, BROKEN_RELIC_PART )
                        if ( value === 2 ) {
                            state.setVariable( variableNames.value, 3 )

                            if ( QuestHelper.hasQuestItems( player, GOBLET_OF_ALECTIA, GOBLET_OF_TISHAS, GOBLET_OF_MEKARA, GOBLET_OF_MORIGUL ) ) {
                                if ( !QuestHelper.hasQuestItem( player, SEALED_BOX ) ) {
                                    return this.getPath( brokenRelicPartCount < 1000 ? '31454-01.html' : '31454-03.html' )
                                }

                                return this.getPath( brokenRelicPartCount < 1000 ? '31454-06.html' : '31454-10.html' )
                            }

                            if ( !QuestHelper.hasQuestItem( player, SEALED_BOX ) ) {
                                return this.getPath( brokenRelicPartCount < 1000 ? '31454-11.html' : '31454-12.html' )
                            }

                            return this.getPath( brokenRelicPartCount < 1000 ? '31454-13.html' : '31454-14.html' )
                        }

                        if ( value === 3 ) {
                            if ( !QuestHelper.hasQuestItem( player, SEALED_BOX ) ) {
                                return this.getPath( brokenRelicPartCount < 1000 ? '31454-15.html' : '31454-12.html' )
                            }

                            return this.getPath( brokenRelicPartCount < 1000 ? '31454-13.html' : '31454-14.html' )
                        }

                        break

                    case GHOST_CHAMBERLAIN_OF_ELMOREDEN_1:
                        return this.getPath( '31919-01.html' )

                    case CONQUERORS_SEPULCHER_MANAGER:
                        return this.getPath( '31921-01.html' )

                    case EMPERORS_SEPULCHER_MANAGER:
                        return this.getPath( '31922-01.html' )

                    case GREAT_SAGES_SEPULCHER_MANAGER:
                        return this.getPath( '31923-01.html' )

                    case JUDGES_SEPULCHER_MANAGER:
                        return this.getPath( '31924-01.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}