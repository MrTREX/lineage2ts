import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2Npc } from '../../gameService/models/actor/L2Npc'

const ASHAS = 31377
const TAYR = 25302
const TAYR_HEAD = 7241
const WISDOM_FEATHER = 7230
const VARKA_ALLIANCE_FOUR = 7224
const minimumLevel = 75

export class SlayTheEnemyCommanderVarka extends ListenerLogic {
    constructor() {
        super( 'Q00614_SlayTheEnemyCommanderVarka', 'listeners/tracked-600/SlayTheEnemyCommanderVarka.ts' )
        this.questId = 614
        this.questItemIds = [ TAYR_HEAD ]
    }

    getQuestStartIds(): Array<number> {
        return [ ASHAS ]
    }

    getTalkIds(): Array<number> {
        return [ ASHAS ]
    }

    getAttackableKillIds(): Array<number> {
        return [ TAYR ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00614_SlayTheEnemyCommanderVarka'
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        await QuestHelper.runStateActionForParty( data.playerId, data.targetId, this.getName(), async ( state: QuestState, player: L2PcInstance, npc: L2Npc ): Promise<void> => {
            if ( !state || !state.isCondition( 1 ) ) {
                return
            }

            if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
                return
            }

            await QuestHelper.giveSingleItem( player, TAYR_HEAD, 1 )
            state.setConditionWithSound( 2, true )
        } )

        return
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31377-04.htm':
                state.startQuest()
                break

            case '31377-07.html':
                if ( QuestHelper.hasQuestItem( player, TAYR_HEAD ) && state.isCondition( 2 ) ) {
                    await QuestHelper.rewardSingleItem( player, WISDOM_FEATHER, 1 )
                    await QuestHelper.addExpAndSp( player, 10000, 0 )
                    await state.exitQuest( true, true )

                    break
                }

                return QuestHelper.getNoQuestMessagePath()

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '31377-03.htm' )
                }

                return this.getPath( QuestHelper.hasQuestItem( player, VARKA_ALLIANCE_FOUR ) ? '31377-01.htm' : '31377-02.htm' )

            case QuestStateValues.STARTED:
                return this.getPath( state.isCondition( 2 ) && QuestHelper.hasQuestItem( player, TAYR_HEAD ) ? '31377-05.html' : '31377-06.html' )

            case QuestStateValues.COMPLETED:
                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}