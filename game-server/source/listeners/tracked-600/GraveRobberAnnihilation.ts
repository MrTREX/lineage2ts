import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const KARUDA = 32017
const ORC_GOODS = 8088
const minimumLevel = 20
const requiredAmount = 120

const monsterRewardChances = {
    22003: 0.714, // Grave Robber Scout
    22004: 0.841, // Grave Robber Lookout
    22005: 0.778, // Grave Robber Ranger
    22006: 0.746, // Grave Robber Guard
    22008: 0.810, // Grave Robber Fighter
}

type PlayerReward = [ number, number ] // itemId, amount
const playerRewards: { [ name: string ]: PlayerReward } = {
    varnish: [ 1865, 30 ], // Varnish
    animalskin: [ 1867, 40 ], // Animal Skin
    animalbone: [ 1872, 40 ], // Animal Bone
    charcoal: [ 1871, 30 ], // Charcoal
    coal: [ 1870, 30 ], // Coal
    ironore: [ 1869, 30 ], // Iron Ore
}

export class GraveRobberAnnihilation extends ListenerLogic {
    constructor() {
        super( 'Q00644_GraveRobberAnnihilation', 'listeners/tracked-600/GraveRobberAnnihilation.ts' )
        this.questId = 644
        this.questItemIds = [ ORC_GOODS ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getQuestStartIds(): Array<number> {
        return [ KARUDA ]
    }

    getTalkIds(): Array<number> {
        return [ KARUDA ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00644_GraveRobberAnnihilation'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32017-03.htm':
                state.startQuest()
                break

            case '32017-06.html':
                if ( state.isCondition( 2 ) && QuestHelper.getQuestItemsCount( player, ORC_GOODS ) >= requiredAmount ) {
                    break
                }

                return

            case 'varnish':
            case 'animalskin':
            case 'animalbone':
            case 'charcoal':
            case 'coal':
            case 'ironore':
                if ( state.isCondition( 2 ) ) {
                    let [ itemId, amount ]: PlayerReward = playerRewards[ data.eventName ]
                    await QuestHelper.rewardSingleItem( player, itemId, amount )
                    await state.exitQuest( true, true )

                    return this.getPath( '32017-07.html' )
                }

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), 1, 3, L2World.getObjectById( data.targetId ) )
        if ( !state ) {
            return
        }

        await QuestHelper.rewardAndProgressState( state.getPlayer(), state, ORC_GOODS, 1, requiredAmount, data.isChampion, 2 )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '32017-01.htm' : '32017-02.htm' )

            case QuestStateValues.STARTED:
                if ( state.isCondition( 2 ) && QuestHelper.getQuestItemsCount( player, ORC_GOODS ) >= requiredAmount ) {
                    return this.getPath( '32017-04.html' )
                }

                return this.getPath( '32017-05.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}