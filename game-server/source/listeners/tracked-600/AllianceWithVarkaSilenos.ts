import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestHelper } from '../helpers/QuestHelper'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const NARAN_ASHANUK = 31378
const KETRA_BADGE_SOLDIER = 7226
const KETRA_BADGE_OFFICER = 7227
const KETRA_BADGE_CAPTAIN = 7228
const VALOR_FEATHER = 7229
const WISDOM_FEATHER = 7230
const minimumLevel = 74

type MonsterDropData = [ number, number, number ] // chance, condition value, itemId
const monsterDropData: { [ npcId: number ]: MonsterDropData } = {
    21324: [ 0.500, 1, KETRA_BADGE_SOLDIER ], // Ketra Orc Footman
    21325: [ 0.500, 1, KETRA_BADGE_SOLDIER ], // Ketra's War Hound
    21327: [ 0.509, 1, KETRA_BADGE_SOLDIER ], // Ketra Orc Raider
    21328: [ 0.521, 1, KETRA_BADGE_SOLDIER ], // Ketra Orc Scout
    21329: [ 0.519, 1, KETRA_BADGE_SOLDIER ], // Ketra Orc Shaman
    21331: [ 0.500, 2, KETRA_BADGE_OFFICER ], // Ketra Orc Warrior
    21332: [ 0.500, 2, KETRA_BADGE_OFFICER ], // Ketra Orc Lieutenant
    21334: [ 0.509, 2, KETRA_BADGE_OFFICER ], // Ketra Orc Medium
    21335: [ 0.518, 2, KETRA_BADGE_OFFICER ], // Ketra Orc Elite Soldier
    21336: [ 0.518, 2, KETRA_BADGE_OFFICER ], // Ketra Orc White Captain
    21338: [ 0.527, 2, KETRA_BADGE_OFFICER ], // Ketra Orc Seer
    21339: [ 0.500, 2, KETRA_BADGE_CAPTAIN ], // Ketra Orc General
    21340: [ 0.500, 2, KETRA_BADGE_CAPTAIN ], // Ketra Orc Battalion Commander
    21342: [ 0.508, 2, KETRA_BADGE_CAPTAIN ], // Ketra Orc Grand Seer
    21343: [ 0.628, 2, KETRA_BADGE_OFFICER ], // Ketra Commander
    21344: [ 0.604, 2, KETRA_BADGE_OFFICER ], // Ketra Elite Guard
    21345: [ 0.627, 2, KETRA_BADGE_CAPTAIN ], // Ketra's Head Shaman
    21346: [ 0.604, 2, KETRA_BADGE_CAPTAIN ], // Ketra's Head Guard
    21347: [ 0.649, 2, KETRA_BADGE_CAPTAIN ], // Ketra Prophet
    21348: [ 0.626, 2, KETRA_BADGE_CAPTAIN ], // Prophet's Guard
    21349: [ 0.626, 2, KETRA_BADGE_CAPTAIN ], // Prophet's Aide
}

const ketraMarks: Array<number> = [
    7211, // Mark of Ketra's Alliance - Level 1
    7212, // Mark of Ketra's Alliance - Level 2
    7213, // Mark of Ketra's Alliance - Level 3
    7214, // Mark of Ketra's Alliance - Level 4
    7215, // Mark of Ketra's Alliance - Level 5
]
const varkaMarks: Array<number> = [
    7221, // Mark of Varka's Alliance - Level 1
    7222, // Mark of Varka's Alliance - Level 2
    7223, // Mark of Varka's Alliance - Level 3
    7224, // Mark of Varka's Alliance - Level 4
    7225, // Mark of Varka's Alliance - Level 5
]

const soldierBadgeCounts: Array<number> = [
    100, // cond 1
    200, // cond 2
    300, // cond 3
    300, // cond 4
    400, // cond 5
]

const officerBadgeCounts: Array<number> = [
    0, // cond 1
    100, // cond 2
    200, // cond 3
    300, // cond 4
    400, // cond 5
]

const captainBadgeCounts: Array<number> = [
    0, // cond 1
    0, // cond 2
    100, // cond 3
    200, // cond 4
    200, // cond 5
]

const itemCountMapping: { [ itemId: number ]: Array<number> } = {
    [ KETRA_BADGE_SOLDIER ]: soldierBadgeCounts,
    [ KETRA_BADGE_OFFICER ]: officerBadgeCounts,
    [ KETRA_BADGE_CAPTAIN ]: captainBadgeCounts,
}

export class AllianceWithVarkaSilenos extends ListenerLogic {
    constructor() {
        super( 'Q00611_AllianceWithVarkaSilenos', 'listeners/tracked-600/AllianceWithVarkaSilenos.ts' )
        this.questId = 611
        this.questItemIds = [
            KETRA_BADGE_CAPTAIN,
            KETRA_BADGE_OFFICER,
            KETRA_BADGE_SOLDIER,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ NARAN_ASHANUK ]
    }

    getTalkIds(): Array<number> {
        return [ NARAN_ASHANUK ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterDropData ).map( value => _.parseInt( value ) )
    }

    canReceiveItem( player: L2PcInstance, state: QuestState, itemId: number ): boolean {
        let itemAmounts: Array<number> = itemCountMapping[ itemId ]
        if ( !itemAmounts ) {
            return false
        }

        return QuestHelper.getQuestItemsCount( player, itemId ) < itemAmounts[ state.getCondition() - 1 ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00611_AllianceWithVarkaSilenos'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31378-12a.html':
            case '31378-12b.html':
            case '31378-25.html':
                break

            case '31378-04.htm':
                if ( QuestHelper.hasAtLeastOneQuestItem( player, ...ketraMarks ) ) {
                    return this.getPath( '31378-03.htm' )
                }

                state.startQuest()

                let index: number = _.findIndex( varkaMarks, ( itemId: number ): boolean => {
                    return QuestHelper.hasQuestItem( player, itemId )
                } )

                if ( index !== -1 ) {
                    state.setConditionWithSound( index + 2 )
                    return this.getPath( `31378-0${ index + 5 }.htm` )
                }

                state.setConditionWithSound( 1 )
                break

            case '31378-12.html':
                if ( QuestHelper.getQuestItemsCount( player, KETRA_BADGE_SOLDIER ) < soldierBadgeCounts[ 0 ] ) {
                    return QuestHelper.getNoQuestMessagePath()
                }

                await QuestHelper.takeSingleItem( player, KETRA_BADGE_SOLDIER, -1 )
                await QuestHelper.giveSingleItem( player, varkaMarks[ 0 ], 1 )

                state.setConditionWithSound( 2, true )
                break

            case '31378-15.html':
                if ( QuestHelper.getQuestItemsCount( player, KETRA_BADGE_SOLDIER ) < soldierBadgeCounts[ 1 ]
                        || QuestHelper.getQuestItemsCount( player, KETRA_BADGE_OFFICER ) < officerBadgeCounts[ 1 ] ) {
                    return QuestHelper.getNoQuestMessagePath()
                }

                await QuestHelper.takeMultipleItems( player, -1, KETRA_BADGE_SOLDIER, KETRA_BADGE_OFFICER, varkaMarks[ 0 ] )
                await QuestHelper.giveSingleItem( player, varkaMarks[ 1 ], 1 )

                state.setConditionWithSound( 3, true )
                break

            case '31378-18.html':
                if ( QuestHelper.getQuestItemsCount( player, KETRA_BADGE_SOLDIER ) < soldierBadgeCounts[ 2 ]
                        || QuestHelper.getQuestItemsCount( player, KETRA_BADGE_OFFICER ) < officerBadgeCounts[ 2 ]
                        || QuestHelper.getQuestItemsCount( player, KETRA_BADGE_CAPTAIN ) < captainBadgeCounts[ 2 ] ) {
                    return QuestHelper.getNoQuestMessagePath()
                }

                await QuestHelper.takeMultipleItems( player, -1, KETRA_BADGE_SOLDIER, KETRA_BADGE_OFFICER, KETRA_BADGE_CAPTAIN, varkaMarks[ 1 ] )
                await QuestHelper.giveSingleItem( player, varkaMarks[ 2 ], 1 )

                state.setConditionWithSound( 4, true )
                break

            case '31378-21.html':
                if ( !QuestHelper.hasQuestItem( player, VALOR_FEATHER )
                        || QuestHelper.getQuestItemsCount( player, KETRA_BADGE_SOLDIER ) < soldierBadgeCounts[ 3 ]
                        || QuestHelper.getQuestItemsCount( player, KETRA_BADGE_OFFICER ) < officerBadgeCounts[ 3 ]
                        || QuestHelper.getQuestItemsCount( player, KETRA_BADGE_CAPTAIN ) < captainBadgeCounts[ 3 ] ) {
                    return QuestHelper.getNoQuestMessagePath()
                }

                await QuestHelper.takeMultipleItems( player, -1, KETRA_BADGE_SOLDIER, KETRA_BADGE_OFFICER, KETRA_BADGE_CAPTAIN, VALOR_FEATHER, varkaMarks[ 2 ] )
                await QuestHelper.giveSingleItem( player, varkaMarks[ 3 ], 1 )

                state.setConditionWithSound( 5, true )
                break

            case '31378-26.html':
                await QuestHelper.takeMultipleItems( player, -1, VALOR_FEATHER, WISDOM_FEATHER, ...varkaMarks )
                await state.exitQuest( true, true )

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let [ chance, minimumConditionRequired, itemId ] = monsterDropData[ data.npcId ]
        if ( Math.random() > QuestHelper.getAdjustedChance( itemId, chance, data.isChampion ) ) {
            return
        }

        let player: L2PcInstance = this.getRandomPartyMemberPerStateType( L2World.getPlayer( data.playerId ), QuestStateValues.STARTED )

        if ( !player ) {
            return
        }

        let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), this.getName(), false )

        if ( state.getCondition() < minimumConditionRequired || !this.canReceiveItem( player, state, itemId ) ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, itemId, 1, data.isChampion )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '31378-01.htm' : '31378-02.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( this.hasMatchingBadges( player, 0 ) ? '31378-11.html' : '31378-10.html' )

                    case 2:
                        return this.getPath( QuestHelper.hasQuestItem( player, varkaMarks[ 0 ] ) && this.hasMatchingBadges( player, 1 ) ? '31378-14.html' : '31378-13.html' )

                    case 3:
                        return this.getPath( QuestHelper.hasQuestItem( player, varkaMarks[ 1 ] ) && this.hasMatchingBadges( player, 2 ) ? '31378-17.html' : '31378-16.html' )

                    case 4:
                        return this.getPath( QuestHelper.hasQuestItems( player, varkaMarks[ 2 ], VALOR_FEATHER ) && this.hasMatchingBadges( player, 3 ) ? '31378-20.html' : '31378-19.html' )

                    case 5:
                        if ( !QuestHelper.hasQuestItem( player, varkaMarks[ 3 ] )
                                || !QuestHelper.hasQuestItem( player, WISDOM_FEATHER )
                                || QuestHelper.getQuestItemsCount( player, KETRA_BADGE_SOLDIER ) < soldierBadgeCounts[ 4 ]
                                || QuestHelper.getQuestItemsCount( player, KETRA_BADGE_OFFICER ) < officerBadgeCounts[ 4 ]
                                || QuestHelper.getQuestItemsCount( player, KETRA_BADGE_CAPTAIN ) < captainBadgeCounts[ 4 ] ) {

                            return this.getPath( '31378-22.html' )
                        }

                        await QuestHelper.takeMultipleItems( player, -1, KETRA_BADGE_SOLDIER, KETRA_BADGE_OFFICER, KETRA_BADGE_CAPTAIN, WISDOM_FEATHER, varkaMarks[ 3 ] )
                        await QuestHelper.giveSingleItem( player, varkaMarks[ 4 ], 1 )

                        state.setConditionWithSound( 6, true )
                        return this.getPath( '31378-23.html' )

                    case 6:
                        if ( QuestHelper.hasQuestItem( player, varkaMarks[ 4 ] ) ) {
                            return this.getPath( '31378-24.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    hasMatchingBadges( player: L2PcInstance, index: number ): boolean {
        switch ( index ) {
            case 0:
                return QuestHelper.getQuestItemsCount( player, KETRA_BADGE_SOLDIER ) >= soldierBadgeCounts[ index ]

            case 1:
                return QuestHelper.getQuestItemsCount( player, KETRA_BADGE_SOLDIER ) >= soldierBadgeCounts[ index ]
                        && QuestHelper.getQuestItemsCount( player, KETRA_BADGE_OFFICER ) >= officerBadgeCounts[ index ]

            case 2:
            case 3:
            case 4:
                return QuestHelper.getQuestItemsCount( player, KETRA_BADGE_SOLDIER ) >= soldierBadgeCounts[ index ]
                        && QuestHelper.getQuestItemsCount( player, KETRA_BADGE_OFFICER ) >= officerBadgeCounts[ index ]
                        && QuestHelper.getQuestItemsCount( player, KETRA_BADGE_CAPTAIN ) >= captainBadgeCounts[ index ]

        }

        return false
    }
}