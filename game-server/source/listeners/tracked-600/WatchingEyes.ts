import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const EYE_OF_ARGOS = 31683
const PROOF_OF_AVENGER = 7188
const minimumLevel = 71

const monsterRewardChances: { [ npcId: number ]: number } = {
    21308: 0.79,
    21309: 0.82,
    21306: 0.85,
    21310: 0.68,
    21311: 0.63,
}

type PlayerReward = [ number, number ] // itemId, adena amount
const playerRewards: Array<PlayerReward> = [
    [ 6699, 90000 ],
    [ 6698, 80000 ],
    [ 6700, 40000 ],
    [ 0, 230000 ],
]

export class WatchingEyes extends ListenerLogic {
    constructor() {
        super( 'Q00601_WatchingEyes', 'listeners/tracked-500/WatchingEyes.ts' )
        this.questId = 601
        this.questItemIds = [ PROOF_OF_AVENGER ]
    }

    getQuestStartIds(): Array<number> {
        return [ EYE_OF_ARGOS ]
    }

    getTalkIds(): Array<number> {
        return [ EYE_OF_ARGOS ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00601_WatchingEyes'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '31683-02.htm':
                state.startQuest()
                break

            case '31683-05.html':
                let player = L2World.getPlayer( data.playerId )
                if ( QuestHelper.getQuestItemsCount( player, PROOF_OF_AVENGER ) < 100 ) {
                    return this.getPath( '31683-06.html' )
                }

                let [ itemId, adenaAmount ] = _.sample( playerRewards )
                if ( itemId !== 0 ) {
                    await QuestHelper.rewardSingleItem( player, itemId, 5 )
                    await QuestHelper.addExpAndSp( player, 120000, 10000 )
                }

                await QuestHelper.giveAdena( player, adenaAmount, true )
                await state.exitQuest( true, true )

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( Math.random() > QuestHelper.getAdjustedChance( PROOF_OF_AVENGER, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )
        if ( !state || !state.isCondition( 1 ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        await QuestHelper.rewardAndProgressState( player, state, PROOF_OF_AVENGER, 1, 100, data.isChampion, 2 )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                let player = L2World.getPlayer( data.playerId )
                return this.getPath( player.getLevel() >= minimumLevel ? '31683-01.htm' : '31683-00.htm' )

            case QuestStateValues.STARTED:
                return this.getPath( state.isCondition( 1 ) ? '31683-03.html' : '31683-04.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}