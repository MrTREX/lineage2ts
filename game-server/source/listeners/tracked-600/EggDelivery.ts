import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const JEREMY = 31521
const PULIN = 31543
const NAFF = 31544
const CROCUS = 31545
const KUBER = 31546
const BOELIN = 31547
const VALENTINE = 31584
const BOILED_EGG = 7195
const EGG_PRICE = 7196
const minimumLevel = 68
const QUICK_STEP_POTION = 734
const SEALED_RING_OF_AURAKYRA = 6849
const SEALED_SANDDRAGONS_EARING = 6847
const SEALED_DRAGON_NECKLACE = 6851

const npcIds: Array<number> = [
    NAFF,
    CROCUS,
    KUBER,
    BOELIN,
]

export class EggDelivery extends ListenerLogic {
    constructor() {
        super( 'Q00621_EggDelivery', 'listeners/tracked-600/EggDelivery.ts' )
        this.questId = 621
        this.questItemIds = [
            BOILED_EGG,
            EGG_PRICE,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ JEREMY ]
    }

    getTalkIds(): Array<number> {
        return [
            ...npcIds,
            JEREMY,
            PULIN,
            VALENTINE,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00621_EggDelivery'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31521-03.htm':
                if ( state.isCreated() ) {
                    state.startQuest()

                    await QuestHelper.giveSingleItem( player, BOILED_EGG, 5 )
                    break
                }

                return

            case '31521-06.html':
                if ( state.isCondition( 6 ) ) {
                    if ( QuestHelper.getQuestItemsCount( player, EGG_PRICE ) >= 5 ) {
                        state.setConditionWithSound( 7, true )
                        await QuestHelper.takeSingleItem( player, EGG_PRICE, -1 )
                        break
                    }

                    return this.getPath( '31521-07.html' )
                }

                return

            case '31543-02.html':
                if ( state.isCondition( 1 ) ) {
                    if ( QuestHelper.hasQuestItem( player, BOILED_EGG ) ) {
                        await QuestHelper.takeSingleItem( player, BOILED_EGG, 1 )
                        await QuestHelper.giveSingleItem( player, EGG_PRICE, 1 )

                        state.setConditionWithSound( 2, true )
                        break
                    }

                    return this.getPath( '31543-03.html' )
                }

                return

            case '31544-02.html':
            case '31545-02.html':
            case '31546-02.html':
            case '31547-02.html':
                if ( npcIds.includes( data.characterNpcId ) && state.isCondition( npcIds.indexOf( data.characterNpcId ) + 2 ) ) {
                    if ( QuestHelper.hasQuestItem( player, BOILED_EGG ) ) {
                        await QuestHelper.takeSingleItem( player, BOILED_EGG, 1 )
                        await QuestHelper.giveSingleItem( player, EGG_PRICE, 1 )

                        state.setConditionWithSound( state.getCondition() + 1, true )
                        break
                    }

                    return this.getPath( `${ data.characterNpcId }-03.html` )
                }

                return

            case '31584-02.html':
                if ( state.isCondition( 7 ) ) {
                    let chance: number = Math.random()
                    if ( chance < 0.8 ) {
                        await QuestHelper.rewardSingleItem( player, QUICK_STEP_POTION, 1 )
                        await QuestHelper.giveAdena( player, 18800, true )
                    } else if ( chance < 0.88 ) {
                        await QuestHelper.rewardSingleItem( player, SEALED_RING_OF_AURAKYRA, 1 )
                    } else if ( chance < 0.96 ) {
                        await QuestHelper.rewardSingleItem( player, SEALED_SANDDRAGONS_EARING, 1 )
                    } else if ( chance < 0.1 ) {
                        await QuestHelper.rewardSingleItem( player, SEALED_DRAGON_NECKLACE, 1 )
                    }

                    await state.exitQuest( true, true )
                    break
                }

                return


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case JEREMY:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() >= minimumLevel ? '31521-01.htm' : '31521-02.htm' )

                    case QuestStateValues.STARTED:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '31521-04.html' )

                            case 6:
                                if ( QuestHelper.hasQuestItem( player, EGG_PRICE ) ) {
                                    return this.getPath( '31521-05.html' )
                                }

                                break

                            case 7:
                                if ( !QuestHelper.hasQuestItem( player, BOILED_EGG ) ) {
                                    return this.getPath( '31521-08.html' )
                                }

                                break
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case PULIN:
                if ( !state.isStarted() ) {
                    break
                }

                switch ( state.getCondition() ) {
                    case 1:
                        if ( QuestHelper.getQuestItemsCount( player, BOILED_EGG ) >= 5 ) {
                            return this.getPath( '31543-01.html' )
                        }

                        break

                    case 2:
                        return this.getPath( '31543-04.html' )
                }

                break

            case NAFF:
            case CROCUS:
            case KUBER:
            case BOELIN:
                if ( !state.isStarted() ) {
                    break
                }

                let conditionValue: number = npcIds.indexOf( data.characterNpcId ) + 2
                if ( state.isCondition( conditionValue ) && QuestHelper.hasQuestItem( player, EGG_PRICE ) ) { // 2,3,4,5
                    return this.getPath( `${ data.characterNpcId }-01.html` )
                } else if ( state.isCondition( conditionValue + 1 ) ) { // 3,4,5,6
                    return this.getPath( `${ data.characterNpcId }-04.html` )
                }

                break

            case VALENTINE:
                if ( state.isStarted() && state.isCondition( 7 ) ) {
                    return this.getPath( '31584-01.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}