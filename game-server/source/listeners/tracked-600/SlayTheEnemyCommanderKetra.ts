import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'

const KADUN = 31370
const MOS = 25312
const MOS_HEAD = 7236
const WISDOM_TOTEM = 7220
const KETRA_ALLIANCE_FOUR = 7214
const MIN_LEVEL = 75

export class SlayTheEnemyCommanderKetra extends ListenerLogic {
    constructor() {
        super( 'Q00608_SlayTheEnemyCommanderKetra', 'listeners/tracked-600/SlayTheEnemyCommanderKetra.ts' )
        this.questId = 608
        this.questItemIds = [
            MOS_HEAD,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ KADUN ]
    }

    getTalkIds(): Array<number> {
        return [ KADUN ]
    }

    getAttackableKillIds(): Array<number> {
        return [ MOS ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        await QuestHelper.runStateActionForParty( data.playerId, data.targetId, this.getName(), async ( state: QuestState, player: L2PcInstance, npc: L2Npc ): Promise<void> => {
            if ( !state || !state.isCondition( 1 ) ) {
                return
            }

            if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
                return
            }

            await QuestHelper.giveSingleItem( player, MOS_HEAD, 1 )
            state.setConditionWithSound( 2, true )
        } )

        return
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '31370-04.htm':
                state.startQuest()
                break

            case '31370-07.html':
                let player = L2World.getPlayer( data.playerId )

                if ( QuestHelper.hasQuestItem( player, MOS_HEAD ) && state.isCondition( 2 ) ) {
                    await QuestHelper.rewardSingleItem( player, WISDOM_TOTEM, 1 )
                    await QuestHelper.addExpAndSp( player, 10000, 0 )
                    await state.exitQuest( true, true )

                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getLevel() < MIN_LEVEL ) {
                    return this.getPath( '31370-03.htm' )
                }

                return this.getPath( QuestHelper.hasQuestItem( player, KETRA_ALLIANCE_FOUR ) ? '31370-01.htm' : '31370-02.htm' )

            case QuestStateValues.STARTED:
                return this.getPath( state.isCondition( 2 ) && QuestHelper.hasQuestItem( player, MOS_HEAD ) ? '31370-05.html' : '31370-06.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00608_SlayTheEnemyCommanderKetra'
    }
}