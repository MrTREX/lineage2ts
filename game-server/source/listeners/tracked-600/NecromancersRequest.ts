import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const MYSTERIOUS_WIZARD = 31522
const VAMPIRES_HEART = 7542
const ZOMBIES_BRAIN = 7543
const minimumLevel = 63
const requiredAmount = 200
const ADENA_REWARD = 120000

type MonsterData = [ number, number ] // chance, itemId
const monsterRewards: { [ npcId: number ]: MonsterData } = {
    21547: [ 0.565, ZOMBIES_BRAIN ], // Corrupted Knight
    21548: [ 0.484, ZOMBIES_BRAIN ], // Resurrected Knight
    21549: [ 0.585, ZOMBIES_BRAIN ], // Corrupted Guard
    21550: [ 0.597, ZOMBIES_BRAIN ], // Corrupted Guard
    21551: [ 0.673, ZOMBIES_BRAIN ], // Resurrected Guard
    21552: [ 0.637, ZOMBIES_BRAIN ], // Resurrected Guard
    21555: [ 0.575, ZOMBIES_BRAIN ], // Slaughter Executioner
    21556: [ 0.560, ZOMBIES_BRAIN ], // Slaughter Executioner
    21562: [ 0.631, ZOMBIES_BRAIN ], // Guillotine's Ghost
    21571: [ 0.758, ZOMBIES_BRAIN ], // Ghost of Rebellion Soldier
    21576: [ 0.647, ZOMBIES_BRAIN ], // Ghost of Guillotine
    21577: [ 0.625, ZOMBIES_BRAIN ], // Ghost of Guillotine
    21579: [ 0.766, ZOMBIES_BRAIN ], // Ghost of Rebellion Leader
    21568: [ 0.452, VAMPIRES_HEART ], // Devil Bat
    21569: [ 0.484, VAMPIRES_HEART ], // Devil Bat
    21573: [ 0.499, VAMPIRES_HEART ], // Atrox
    21582: [ 0.522, VAMPIRES_HEART ], // Vampire Soldier
    21585: [ 0.413, VAMPIRES_HEART ], // Vampire Magician
    21586: [ 0.496, VAMPIRES_HEART ], // Vampire Adept
    21587: [ 0.519, VAMPIRES_HEART ], // Vampire Warrior
    21588: [ 0.428, VAMPIRES_HEART ], // Vampire Wizard
    21589: [ 0.439, VAMPIRES_HEART ], // Vampire Wizard
    21590: [ 0.428, VAMPIRES_HEART ], // Vampire Magister
    21591: [ 0.502, VAMPIRES_HEART ], // Vampire Magister
    21592: [ 0.370, VAMPIRES_HEART ], // Vampire Magister
    21593: [ 0.592, VAMPIRES_HEART ], // Vampire Warlord
    21594: [ 0.554, VAMPIRES_HEART ], // Vampire Warlord
    21595: [ 0.392, VAMPIRES_HEART ], // Vampire Warlord
}

export class NecromancersRequest extends ListenerLogic {
    constructor() {
        super( 'Q00632_NecromancersRequest', 'listeners/tracked-600/NecromancersRequest.ts' )
        this.questId = 632
        this.questItemIds = [
            VAMPIRES_HEART,
            ZOMBIES_BRAIN,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ MYSTERIOUS_WIZARD ]
    }

    getTalkIds(): Array<number> {
        return [ MYSTERIOUS_WIZARD ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewards ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00632_NecromancersRequest'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31522-104.htm':
                if ( player.getLevel() >= minimumLevel ) {
                    state.startQuest()
                    state.setMemoState( 1 )

                    break
                }

                return

            case '31522-201.html':
                break

            case '31522-202.html':
                if ( QuestHelper.getQuestItemsCount( player, VAMPIRES_HEART ) >= requiredAmount ) {
                    await QuestHelper.takeSingleItem( player, VAMPIRES_HEART, -1 )
                    await QuestHelper.giveAdena( player, ADENA_REWARD, true )

                    state.setMemoState( 1 )
                    break
                }

                return this.getPath( '31522-203.html' )

            case '31522-204.html':
                await state.exitQuest( true, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let [ chance, itemId ] = monsterRewards[ data.npcId ]
        if ( Math.random() > QuestHelper.getAdjustedChance( itemId, chance, data.isChampion ) ) {
            return
        }

        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), -1, 3, L2World.getObjectById( data.targetId ) )
        if ( !state ) {
            return
        }

        let player = state.getPlayer()
        await QuestHelper.rewardSingleQuestItem( player, itemId, 1, data.isChampion )

        if ( itemId === VAMPIRES_HEART && QuestHelper.getQuestItemsCount( player, VAMPIRES_HEART ) >= requiredAmount ) {
            state.setConditionWithSound( 2 )
            state.setMemoState( 2 )
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '31522-101.htm' : '31522-103.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getMemoState() ) {
                    case 1:
                        return this.getPath( '31522-106.html' )

                    case 2:
                        if ( QuestHelper.getQuestItemsCount( player, VAMPIRES_HEART ) >= requiredAmount ) {
                            return this.getPath( '31522-105.html' )
                        }

                        return this.getPath( '31522-203.html' )
                }
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}