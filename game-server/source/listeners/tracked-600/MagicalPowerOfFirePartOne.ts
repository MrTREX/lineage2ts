import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { SkillHolder } from '../../gameService/models/holders/SkillHolder'
import { AttackableAttackedEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const NARAN = 31378
const UDAN = 31379
const ASEFA_BOX = 31559
const ASEFA_EYE = 31684
const KEY = 1661
const STOLEN_RED_TOTEM = 7242
const WISDOM_STONE = 7081
const RED_TOTEM = 7243
const varkaMarks: Array<number> = [
    7221, // Mark of Varka's Alliance - Level 1
    7222, // Mark of Varka's Alliance - Level 2
    7223, // Mark of Varka's Alliance - Level 3
    7224, // Mark of Varka's Alliance - Level 4
    7225, // Mark of Varka's Alliance - Level 5
]

const GOW = new SkillHolder( 4547 ) // Gaze of Watcher
const DISPEL_GOW = new SkillHolder( 4548 ) // Quest - Dispel Watcher Gaze
const minimumLevel = 74

const variableNames = {
    isSpawned: 'is',
}

const eventNames = {
    despawnEye: 'de',
}

export class MagicalPowerOfFirePartOne extends ListenerLogic {
    constructor() {
        super( 'Q00615_MagicalPowerOfFirePart1', 'listeners/tracked-600/MagicalPowerOfFirePartOne.ts' )
        this.questId = 615
        this.questItemIds = [
            STOLEN_RED_TOTEM,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ NARAN ]
    }

    getTalkIds(): Array<number> {
        return [ UDAN, NARAN, ASEFA_BOX ]
    }

    getAttackableAttackIds(): Array<number> {
        return [
            21324, // Ketra Orc Footman
            21325, // Ketra's War Hound
            21327, // Ketra Orc Raider
            21328, // Ketra Orc Scout
            21329, // Ketra Orc Shaman
            21331, // Ketra Orc Warrior
            21332, // Ketra Orc Lieutenant
            21334, // Ketra Orc Medium
            21335, // Ketra Orc Elite Soldier
            21336, // Ketra Orc White Captain
            21338, // Ketra Orc Seer
            21339, // Ketra Orc General
            21340, // Ketra Orc Battalion Commander
            21342, // Ketra Orc Grand Seer
            21343, // Ketra Commander
            21344, // Ketra Elite Guard
            21345, // Ketra's Head Shaman
            21346, // Ketra's Head Guard
            21347, // Ketra Prophet
            21348, // Prophet's Guard
            21349, // Prophet's Aide
        ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '31378-02.html':
                state.startQuest()
                break

            case 'open_box':
                let player = L2World.getPlayer( data.playerId )
                if ( !QuestHelper.hasQuestItem( player, KEY ) ) {
                    return this.getPath( '31559-02.html' )
                }

                if ( state.isCondition( 2 ) ) {
                    await QuestHelper.takeSingleItem( player, KEY, 1 )

                    if ( state.getVariable( variableNames.isSpawned ) ) {
                        return this.getPath( '31559-04.html' )
                    }

                    await QuestHelper.giveSingleItem( player, STOLEN_RED_TOTEM, 1 )
                    state.setConditionWithSound( 3, true )

                    return this.getPath( '31559-03.html' )
                }

                return

            case eventNames.despawnEye:
                let npc = L2World.getObjectById( data.characterId ) as L2Npc

                BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.ASEFA_HAS_ALREADY_SEEN_YOUR_FACE )
                await npc.deleteMe()

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableAttackedEvent( data: AttackableAttackedEvent ): Promise<void> {
        let state: QuestState = QuestStateCache.getQuestState( data.attackerPlayerId, this.getName(), false )

        if ( !state || !state.isCondition( 1 ) || !state.getVariable( variableNames.isSpawned ) ) {
            return
        }

        let npc = L2World.getObjectById( data.targetId ) as L2Npc
        npc.setTarget( L2World.getPlayer( data.attackerPlayerId ) )
        await npc.doCastWithHolder( GOW )

        let eye: L2Npc = QuestHelper.addSpawnAtLocation( ASEFA_EYE, npc )
        this.startQuestTimer( eventNames.despawnEye, 10000, eye.getObjectId(), data.attackerPlayerId )
        return BroadcastHelper.broadcastNpcSayStringId( eye, NpcSayType.NpcAll, NpcStringIds.YOU_CANT_AVOID_THE_EYES_OF_ASEFA )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== NARAN ) {
                    break
                }

                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '31378-00b.html' )
                }

                return this.getPath( QuestHelper.hasAtLeastOneQuestItem( player, ...varkaMarks ) ? '31378-01.htm' : '31378-00a.html' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case UDAN:
                        switch ( state.getCondition() ) {
                            case 1:
                                state.setConditionWithSound( 2, true )
                                return this.getPath( '31379-01.html' )

                            case 2:
                                if ( state.getVariable( variableNames.isSpawned ) ) {
                                    state.unsetVariable( variableNames.isSpawned )

                                    let npc = L2World.getObjectById( data.characterId ) as L2Npc
                                    npc.setTarget( player )
                                    await npc.doCastWithHolder( DISPEL_GOW )

                                    return this.getPath( '31379-03.html' )
                                }

                                return this.getPath( '31379-02.html' )

                            case 3:
                                await QuestHelper.rewardMultipleItems( player, 1, RED_TOTEM, WISDOM_STONE )
                                await state.exitQuest( true, true )

                                return this.getPath( '31379-04.html' )
                        }

                        break

                    case ASEFA_BOX:
                        if ( state.isCondition( 2 ) ) {
                            return this.getPath( '31559-01.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00615_MagicalPowerOfFirePart1'
    }
}