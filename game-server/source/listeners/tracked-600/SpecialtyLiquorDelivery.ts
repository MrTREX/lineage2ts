import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const JEREMY = 31521
const PULIN = 31543
const NAFF = 31544
const CROCUS = 31545
const KUBER = 31546
const BOELIN = 31547
const LIETTA = 31267
const SPECIAL_DRINK = 7197
const SPECIAL_DRINK_PRICE = 7198
const QUICK_STEP_POTION = 734
const SEALED_RING_OF_AURAKYRA = 6849
const SEALED_SANDDRAGONS_EARING = 6847
const SEALED_DRAGON_NECKLACE = 6851
const minimumLevel = 68

const npcIds: Array<number> = [
    KUBER,
    CROCUS,
    NAFF,
    PULIN,
]

export class SpecialtyLiquorDelivery extends ListenerLogic {
    constructor() {
        super( 'Q00622_SpecialtyLiquorDelivery', 'listeners/tracked-600/SpecialtyLiquorDelivery.ts' )
        this.questId = 622
        this.questItemIds = [
            SPECIAL_DRINK,
            SPECIAL_DRINK_PRICE,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ JEREMY ]
    }

    getTalkIds(): Array<number> {
        return [
            ...npcIds,
            JEREMY,
            BOELIN,
            LIETTA,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00622_SpecialtyLiquorDelivery'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31521-03.htm':
                if ( state.isCreated() ) {
                    state.startQuest()

                    await QuestHelper.giveSingleItem( player, SPECIAL_DRINK, 5 )
                    break
                }

                return

            case '31521-06.html':
                if ( state.isCondition( 6 ) ) {
                    if ( QuestHelper.getQuestItemsCount( player, SPECIAL_DRINK_PRICE ) >= 5 ) {
                        await QuestHelper.takeSingleItem( player, SPECIAL_DRINK_PRICE, -1 )

                        state.setConditionWithSound( 7, true )
                        break
                    }

                    return this.getPath( '31521-07.html' )
                }

                return

            case '31547-02.html':
                if ( state.isCondition( 1 ) ) {
                    if ( QuestHelper.hasQuestItem( player, SPECIAL_DRINK ) ) {
                        await QuestHelper.takeSingleItem( player, SPECIAL_DRINK, 1 )
                        await QuestHelper.giveSingleItem( player, SPECIAL_DRINK_PRICE, 1 )

                        state.setConditionWithSound( 2, true )
                        break
                    }

                    return this.getPath( '31547-03.html' )
                }

                return

            case '31543-02.html':
            case '31544-02.html':
            case '31545-02.html':
            case '31546-02.html':
                if ( npcIds.includes( data.characterNpcId ) && state.isCondition( npcIds.indexOf( data.characterNpcId ) + 2 ) ) {
                    if ( QuestHelper.hasQuestItem( player, SPECIAL_DRINK ) ) {
                        await QuestHelper.takeSingleItem( player, SPECIAL_DRINK, 1 )
                        await QuestHelper.giveSingleItem( player, SPECIAL_DRINK_PRICE, 1 )

                        state.setConditionWithSound( state.getCondition() + 1, true )
                        break
                    }

                    return this.getPath( `${ data.characterNpcId }-03.html` )
                }

                return

            case '31267-02.html':
                if ( state.isCondition( 7 ) ) {
                    let chance: number = Math.random()
                    if ( chance < 0.800 ) {
                        await QuestHelper.rewardSingleItem( player, QUICK_STEP_POTION, 1 )
                        await QuestHelper.giveAdena( player, 18800, true )
                    } else if ( chance < 0.880 ) {
                        await QuestHelper.rewardSingleItem( player, SEALED_RING_OF_AURAKYRA, 1 )
                    } else if ( chance < 0.960 ) {
                        await QuestHelper.rewardSingleItem( player, SEALED_SANDDRAGONS_EARING, 1 )
                    } else {
                        await QuestHelper.rewardSingleItem( player, SEALED_DRAGON_NECKLACE, 1 )
                    }

                    await state.exitQuest( true, true )
                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case JEREMY:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() >= minimumLevel ? '31521-01.htm' : '31521-02.htm' )

                    case QuestStateValues.STARTED:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '31521-04.html' )

                            case 6:
                                if ( QuestHelper.hasQuestItem( player, SPECIAL_DRINK_PRICE ) ) {
                                    return this.getPath( '31521-05.html' )
                                }

                                break

                            case 7:
                                if ( !QuestHelper.hasQuestItem( player, SPECIAL_DRINK ) ) {
                                    return this.getPath( '31521-08.html' )
                                }

                                break
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case BOELIN:
                if ( !state.isStarted() ) {
                    break
                }

                switch ( state.getCondition() ) {
                    case 1:
                        if ( QuestHelper.getQuestItemsCount( player, SPECIAL_DRINK ) >= 5 ) {
                            return this.getPath( '31547-01.html' )
                        }

                        break

                    case 2:
                        return this.getPath( '31547-04.html' )
                }

                break

            case KUBER:
            case CROCUS:
            case NAFF:
            case PULIN:
                if ( !state.isStarted() ) {
                    break
                }

                let conditionValue: number = npcIds.indexOf( data.characterNpcId ) + 2
                if ( state.isCondition( conditionValue ) && QuestHelper.hasQuestItem( player, SPECIAL_DRINK_PRICE ) ) {
                    return this.getPath( `${ data.characterNpcId }-01.html` )
                } else if ( state.isCondition( conditionValue + 1 ) ) {
                    return this.getPath( `${ data.characterNpcId }-04.html` )
                }

                break

            case LIETTA:
                if ( state.isStarted() && state.isCondition( 7 ) ) {
                    return this.getPath( '31267-01.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}