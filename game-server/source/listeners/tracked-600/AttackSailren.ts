import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const SHILENS_STONE_STATUE = 32109
const GAZKH_FRAGMENT = 8782
const GAZKH = 8784
const requiredAmount = 30
const minimumLevel = 77

export class AttackSailren extends ListenerLogic {
    constructor() {
        super( 'Q00641_AttackSailren', 'listeners/tracked-600/AttackSailren.ts' )
        this.questId = 641
        this.questItemIds = [ GAZKH_FRAGMENT ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            22196, // Velociraptor
            22197, // Velociraptor
            22198, // Velociraptor
            22218, // Velociraptor
            22223, // Velociraptor
            22199, // Pterosaur
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ SHILENS_STONE_STATUE ]
    }

    getTalkIds(): Array<number> {
        return [ SHILENS_STONE_STATUE ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32109-1.html':
                state.startQuest()
                break

            case '32109-2a.html':
                if ( QuestHelper.getQuestItemsCount( player, GAZKH_FRAGMENT ) >= requiredAmount ) {
                    await QuestHelper.giveSingleItem( player, GAZKH, 1 )
                    await state.exitQuest( true, true )
                }

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = this.getRandomPartyMemberStateForCondition( L2World.getPlayer( data.playerId ), 1 )
        if ( !state ) {
            return
        }

        await QuestHelper.rewardAndProgressState( state.getPlayer(), state, GAZKH_FRAGMENT, 1, requiredAmount, data.isChampion, 2 )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '32109-0.htm' )
                }

                return this.getPath( player.hasQuestCompleted( 'Q00126_TheNameOfEvil2' ) ? '32109-0a.htm' : '32109-0b.htm' )

            case QuestStateValues.STARTED:
                return this.getPath( state.isCondition( 1 ) ? '32109-1a.html' : '32109-2.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00641_AttackSailren'
    }
}