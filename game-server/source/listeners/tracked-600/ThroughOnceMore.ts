import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const FLAURON = 32010
const VISITOR_MARK = 8064
const FADED_MARK = 8065
const NECRO_HEART = 8066
const MARK = 8067
const minimumLevel = 72
const requiredAmount = 10
const roomKey = 8273

export class ThroughOnceMore extends ListenerLogic {
    constructor() {
        super( 'Q00637_ThroughOnceMore', 'listeners/tracked-600/ThroughOnceMore.ts' )
        this.questId = 637
        this.questItemIds = [
            NECRO_HEART,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ FLAURON ]
    }

    getTalkIds(): Array<number> {
        return [ FLAURON ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            21565,
            21566,
            21567,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00637_ThroughOnceMore'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '32010-03.htm':
                state.startQuest()
                break

            case '32010-10.htm':
                await state.exitQuest( true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let amount : number = Math.random() < QuestHelper.getAdjustedChance( NECRO_HEART, 0.90, data.isChampion ) ? 2 : 1
        await QuestHelper.rewardAndProgressState( player, state, NECRO_HEART, amount, requiredAmount, data.isChampion, 2 )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getLevel() > minimumLevel ) {
                    if ( QuestHelper.hasQuestItem( player, FADED_MARK ) ) {
                        return this.getPath( '32010-02.htm' )
                    }

                    if ( QuestHelper.hasQuestItem( player, VISITOR_MARK ) ) {
                        await state.exitQuest( true )
                        return this.getPath( '32010-01a.htm' )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK ) ) {
                        await state.exitQuest( true )
                        return this.getPath( '32010-0.htm' )
                    }
                }

                await state.exitQuest( true )
                return this.getPath( '32010-01.htm' )

            case QuestStateValues.STARTED:
                if ( state.isCondition( 2 ) && QuestHelper.getQuestItemsCount( player, NECRO_HEART ) >= requiredAmount ) {
                    await QuestHelper.takeSingleItem( player, NECRO_HEART, 10 )
                    await QuestHelper.takeSingleItem( player, FADED_MARK, 1 )
                    await QuestHelper.giveSingleItem( player, MARK, 1 )
                    await QuestHelper.rewardSingleItem( player, roomKey, 10 )
                    await state.exitQuest( true, true )

                    return this.getPath( '32010-05.htm' )
                }

                return this.getPath( '32010-04.htm' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}