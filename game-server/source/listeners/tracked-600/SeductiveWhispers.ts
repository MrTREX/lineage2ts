import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { DataManager } from '../../data/manager'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'
import { createItemDefinition, ItemDefinition } from '../../gameService/interface/ItemDefinition'

const WILBERT = 30846
const minimumLevel = 50
const SPIRIT_BEAD = 8766
const SCROLL_ENCHANT_WEAPON_A_GRADE = createItemDefinition( 729, 1 )
const SCROLL_ENCHANT_ARMOR_A_GRADE = createItemDefinition( 730, 2 )
const SCROLL_ENCHANT_WEAPON_B_GRADE = createItemDefinition( 947, 2 )
const SCROLL_ENCHANT_ARMOR_B_GRADE = createItemDefinition( 948, 2 )
const SCROLL_ENCHANT_WEAPON_C_GRADE = createItemDefinition( 951, 1 )
const SCROLL_ENCHANT_WEAPON_D_GRADE = createItemDefinition( 955, 1 )
const RECIPE_GREAT_SWORD_60 = createItemDefinition( 4963, 1 )
const RECIPE_HEAVY_WAR_AXE_60 = createItemDefinition( 4964, 1 )
const RECIPE_SPRITES_STAFF_60 = createItemDefinition( 4965, 1 )
const RECIPE_KESHANBERK_60 = createItemDefinition( 4966, 1 )
const RECIPE_SWORD_OF_VALHALLA_60 = createItemDefinition( 4967, 1 )
const RECIPE_KRIS_60 = createItemDefinition( 4968, 1 )
const RECIPE_HELL_KNIFE_60 = createItemDefinition( 4969, 1 )
const RECIPE_ARTHRO_NAIL_60 = createItemDefinition( 4970, 1 )
const RECIPE_DARK_ELVEN_LONG_BOW_60 = createItemDefinition( 4971, 1 )
const RECIPE_GREAT_AXE_60 = createItemDefinition( 4972, 1 )
const RECIPE_SWORD_OF_DAMASCUS_60 = createItemDefinition( 5000, 1 )
const RECIPE_LANCE_60 = createItemDefinition( 5001, 1 )
const RECIPE_DEADMANS_GLORY_60 = createItemDefinition( 5002, 1 )
const RECIPE_ART_OF_BATTLE_AXE_60 = createItemDefinition( 5003, 1 )
const RECIPE_TAFF_OF_EVIL_SPIRITS_60 = createItemDefinition( 5004, 1 )
const RECIPE_DEMONS_DAGGER_60 = createItemDefinition( 5005, 1 )
const RECIPE_BELLION_CESTUS_60 = createItemDefinition( 5006, 1 )
const RECIPE_BOW_OF_PERIL_60 = createItemDefinition( 5007, 1 )
const GREAT_SWORD_BLADE = createItemDefinition( 4104, 12 )
const GREAT_AXE_HEAD = createItemDefinition( 4113, 12 )
const DARK_ELVEN_LONGBOW_SHAFT = createItemDefinition( 4112, 12 )
const SWORD_OF_VALHALLA_BLADE = createItemDefinition( 4108, 12 )
const ARTHRO_NAIL_BLADE = createItemDefinition( 4111, 12 )
const SPRITES_STAFF_HEAD = createItemDefinition( 4104, 12 )
const KRIS_EDGE = createItemDefinition( 4109, 12 )
const KESHANBERK_BLADE = createItemDefinition( 4107, 12 )
const HEAVY_WAR_AXE_HEAD = createItemDefinition( 4105, 12 )
const HELL_KNIFE_EDGE = createItemDefinition( 4110, 12 )
const SWORD_OF_DAMASCUS_BLADE = createItemDefinition( 4114, 13 )
const LANCE_BLADE = createItemDefinition( 4115, 13 )
const BELLION_CESTUS_EDGE = createItemDefinition( 4120, 13 )
const EVIL_SPIRIT_HEAD = createItemDefinition( 4118, 13 )
const DEADMANS_GLORY_STONE = createItemDefinition( 4116, 13 )
const ART_OF_BATTLE_AXE_BLADE = createItemDefinition( 4117, 13 )
const DEMONS_DAGGER_EDGE = createItemDefinition( 4119, 13 )
const BOW_OF_PERIL_SHAFT = createItemDefinition( 4121, 13 )
const SPITEFUL_SOUL_LEADER = 20974

const monsterRewardChances: { [ npcId: number ]: number } = {
    20674: 0.807,
    20678: 0.372,
    20954: 0.460,
    20955: 0.537,
    20956: 0.540,
    20957: 0.565,
    20958: 0.425,
    20959: 0.682,
    20960: 0.372,
    20961: 0.547,
    20962: 0.522,
    20963: 0.498,
    20975: 0.975,
    20976: 0.825,
    20996: 0.385,
    20997: 0.342,
    20998: 0.377,
    20999: 0.450,
    21000: 0.395,
    21001: 0.535,
    21002: 0.472,
    21006: 0.502,
    21007: 0.540,
    21008: 0.692,
    21009: 0.740,
    21010: 0.595,
    [ SPITEFUL_SOUL_LEADER ]: 1,
}

const variableNames = {
    cardValue: 'cv',
}

export class SeductiveWhispers extends ListenerLogic {
    constructor() {
        super( 'Q00663_SeductiveWhispers', 'listeners/tracked-600/SeductiveWhispers.ts' )
        this.questId = 663
    }

    getQuestStartIds(): Array<number> {
        return [ WILBERT ]
    }

    getTalkIds(): Array<number> {
        return [ WILBERT ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00663_SeductiveWhispers'
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30846-01a.htm':
                if ( player.getLevel() >= minimumLevel ) {
                    break
                }

                return

            case '30846-03.htm':
                if ( player.getLevel() >= minimumLevel ) {
                    state.startQuest()
                    state.setMemoState( 1 )

                    break
                }

                return

            case '30846-06.html':
            case '30846-07.html':
            case '30846-08.html':
                break

            case '30846-09.html':
                if ( ( state.getMemoState() % 10 ) <= 4 ) {
                    let value = Math.floor( state.getMemoState() / 10 )
                    if ( value < 1 ) {
                        if ( QuestHelper.getQuestItemsCount( player, SPIRIT_BEAD ) >= 50 ) {
                            await QuestHelper.takeSingleItem( player, SPIRIT_BEAD, 50 )

                            state.setMemoState( 5 )
                            state.setVariable( variableNames.cardValue, 0 )

                            break
                        }

                        return this.getPath( '30846-10.html' )
                    }

                    state.setMemoState( ( value * 10 ) + 5 )
                    state.setVariable( variableNames.cardValue, 0 )

                    return this.getPath( '30846-09a.html' )
                }

                return

            case '30846-14.html':
                if ( ( state.getMemoState() % 10 ) === 5 && Math.floor( state.getMemoState() / 1000 ) === 0 ) {
                    let cardOne: number = Math.max( state.getVariable( variableNames.cardValue ), 0 )
                    let valueOne: number = cardOne % 10
                    let valueTwo: number = Math.floor( ( cardOne - valueOne ) / 10 )
                    let chanceOne: number = _.random( 1, 2 )
                    let chanceTwo: number = _.random( 1, 5 )
                    let winCount: number = Math.floor( state.getMemoState() / 10 ) + 1
                    let cardTwo: number = ( chanceOne * 10 ) + chanceTwo

                    if ( chanceOne === valueTwo ) {
                        let sum = chanceTwo + valueOne
                        if ( ( sum % 5 ) === 0 && sum !== 10 ) {
                            if ( ( ( state.getMemoState() % 100 ) / 10 ) >= 7 ) {
                                await QuestHelper.giveAdena( player, 2384000, true )
                                await QuestHelper.rewardSingleItem( player, SCROLL_ENCHANT_WEAPON_A_GRADE.id, SCROLL_ENCHANT_WEAPON_A_GRADE.count )
                                await QuestHelper.rewardSingleItem( player, SCROLL_ENCHANT_ARMOR_A_GRADE.id, SCROLL_ENCHANT_ARMOR_A_GRADE.count )

                                state.setMemoState( 4 )

                                return this.formatReply( player, '30846-14.html', cardOne, cardTwo, winCount, -1 )
                            }

                            state.setMemoState( Math.floor( ( state.getMemoState() / 10 ) * 10 ) + 7 )
                            return this.formatReply( player, '30846-13.html', cardOne, cardTwo, winCount, -1 )
                        }

                        state.setMemoState( Math.floor( ( state.getMemoState() / 10 ) * 10 ) + 6 )
                        state.setVariable( variableNames.cardValue, cardTwo )

                        return this.formatReply( player, '30846-12.html', cardOne, cardTwo, winCount, -1 )
                    }

                    if ( chanceTwo === 5 || valueOne === 5 ) {
                        if ( ( ( state.getMemoState() % 100 ) / 10 ) >= 7 ) {
                            await QuestHelper.giveAdena( player, 2384000, true )
                            await QuestHelper.rewardSingleItem( player, SCROLL_ENCHANT_WEAPON_A_GRADE.id, SCROLL_ENCHANT_WEAPON_A_GRADE.count )
                            await QuestHelper.rewardSingleItem( player, SCROLL_ENCHANT_ARMOR_A_GRADE.id, SCROLL_ENCHANT_ARMOR_A_GRADE.count )

                            state.setMemoState( 4 )

                            return this.formatReply( player, '30846-14.html', cardOne, cardTwo, winCount, -1 )
                        }

                        state.setMemoState( Math.floor( ( state.getMemoState() / 10 ) * 10 ) + 7 )
                        return this.formatReply( player, '30846-13.html', cardOne, cardTwo, winCount, -1 )
                    }

                    state.setMemoState( Math.floor( ( state.getMemoState() / 10 ) * 10 ) + 6 )
                    state.setVariable( variableNames.cardValue, cardTwo )

                    return this.formatReply( player, '30846-12.html', cardOne, cardTwo, winCount, -1 )
                }

                return

            case '30846-19.html':
                if ( ( state.getMemoState() % 10 ) === 6 && Math.floor( state.getMemoState() / 1000 ) === 0 ) {
                    let cardOne = Math.max( state.getVariable( variableNames.cardValue ), 0 )
                    let valueOne = cardOne % 10
                    let valueTwo = Math.floor( ( cardOne - valueOne ) / 10 )
                    let chanceOne = _.random( 1, 2 )
                    let chanceTwo = _.random( 1, 5 )
                    let cardTwo = ( chanceOne * 10 ) + chanceTwo

                    if ( chanceOne === valueTwo ) {
                        let sum = chanceTwo + valueOne
                        if ( ( sum % 5 ) === 0 && sum !== 10 ) {
                            state.setMemoState( 1 )
                            state.setVariable( variableNames.cardValue, 0 )

                            return this.formatReply( player, '30846-19.html', cardOne, cardTwo, -1, -1 )
                        }

                        state.setMemoState( Math.floor( ( state.getMemoState() / 10 ) * 10 ) + 5 )
                        state.setVariable( variableNames.cardValue, cardTwo )

                        return this.formatReply( player, '30846-18.html', cardOne, cardTwo, -1, -1 )
                    }

                    if ( chanceTwo === 5 || valueOne === 5 ) {
                        state.setMemoState( 1 )
                        return this.formatReply( player, '30846-19.html', cardOne, cardTwo, -1, -1 )
                    }

                    state.setMemoState( Math.floor( ( state.getMemoState() / 10 ) * 10 ) + 5 )
                    state.setVariable( variableNames.cardValue, cardTwo )

                    return this.formatReply( player, '30846-18.html', cardOne, cardTwo, -1, -1 )
                }

                return

            case '30846-20.html':
                if ( ( state.getMemoState() % 10 ) === 7 && Math.floor( state.getMemoState() / 1000 ) === 0 ) {
                    state.setMemoState( Math.floor( ( ( state.getMemoState() / 10 ) + 1 ) * 10 ) + 4 )
                    state.setVariable( variableNames.cardValue, 0 )

                    break
                }

                return

            case '30846-21.html':
                if ( ( state.getMemoState() % 10 ) === 7 && Math.floor( state.getMemoState() / 1000 ) === 0 ) {
                    await this.rewardPlayer( player, Math.floor( state.getMemoState() / 10 ) )

                    state.setMemoState( 1 )
                    state.setVariable( variableNames.cardValue, 0 )

                    break
                }
                return

            case '30846-21a.html':
                if ( state.isMemoState( 1 ) ) {
                    break
                }

                return

            case '30846-22.html':
                if ( ( state.getMemoState() % 10 ) === 1 ) {
                    if ( QuestHelper.hasQuestItem( player, SPIRIT_BEAD ) ) {
                        await QuestHelper.takeSingleItem( player, SPIRIT_BEAD, 1 )

                        state.setMemoState( 1005 )
                        break
                    }

                    return this.getPath( '30846-22a.html' )
                }

                return

            case '30846-25.html':
                if ( state.isMemoState( 1005 ) ) {
                    let cardOne: number = Math.max( state.getVariable( variableNames.cardValue ), 0 )
                    let cardOneModulus: number = cardOne % 10
                    let value: number = Math.floor( ( cardOne - cardOneModulus ) / 10 )

                    let chanceOne: number = _.random( 1, 2 )
                    let chanceTwo: number = _.random( 1, 5 )
                    let cardTwo: number = ( chanceOne * 10 ) + chanceTwo

                    if ( chanceOne === value ) {
                        let valueTwo: number = chanceTwo + cardOneModulus
                        if ( ( valueTwo % 5 ) === 0 && valueTwo !== 10 ) {
                            state.setMemoState( 1 )
                            state.setVariable( variableNames.cardValue, 0 )

                            await QuestHelper.giveAdena( player, 800, true )

                            return this.formatReply( player, '30846-25.html', cardOne, cardTwo, -1, cardOneModulus )
                        }

                        state.setMemoState( 1006 )
                        state.setVariable( variableNames.cardValue, cardTwo )

                        return this.formatReply( player, '30846-24.html', cardOne, cardTwo, -1, -1 )
                    }

                    if ( chanceTwo === 5 || cardOneModulus === 5 ) {
                        state.setMemoState( 1 )
                        state.setVariable( variableNames.cardValue, 0 )

                        await QuestHelper.giveAdena( player, 800, true )

                        return this.formatReply( player, '30846-25.html', cardOne, cardTwo, -1, -1 )
                    }

                    state.setMemoState( 1006 )
                    state.setVariable( variableNames.cardValue, cardTwo )

                    return this.formatReply( player, '30846-24.html', cardOne, cardTwo, -1, -1 )
                }

                return

            case '30846-29.html':
                if ( state.isMemoState( 1006 ) ) {
                    let cardOne: number = Math.max( state.getVariable( variableNames.cardValue ), 0 )
                    let valueOne: number = cardOne % 10
                    let valueTwo: number = Math.floor( ( cardOne - valueOne ) / 10 )
                    let chanceOne: number = _.random( 1, 2 )
                    let chanceTwo: number = _.random( 1, 5 )
                    let cardTwo: number = ( chanceOne * 10 ) + chanceTwo

                    if ( chanceOne === valueTwo ) {
                        let valueThree = chanceTwo + valueOne
                        if ( ( valueThree % 5 ) === 0 && valueThree !== 10 ) {
                            state.setMemoState( 1 )
                            state.setVariable( variableNames.cardValue, 0 )

                            return this.formatReply( player, '30846-29.html', cardOne, cardTwo, 0, -1 )
                        }

                        state.setMemoState( 1005 )
                        state.setVariable( variableNames.cardValue, cardTwo )

                        return this.formatReply( player, '30846-28.html', cardOne, cardTwo, 0, -1 )
                    }

                    if ( chanceTwo === 5 || valueOne === 5 ) {
                        state.setMemoState( 1 )
                        state.setVariable( variableNames.cardValue, 0 )

                        return this.formatReply( player, '30846-29.html', cardOne, cardTwo, 0, -1 )
                    }

                    state.setMemoState( 1005 )
                    state.setVariable( variableNames.cardValue, cardTwo )

                    return this.formatReply( player, '30846-28.html', cardOne, cardTwo, 0, -1 )
                }

                return

            case '30846-30.html':
                await state.exitQuest( true )
                break

            case '30846-31.html':
            case '30846-32.html':
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    rewardItem( player: L2PcInstance, item: ItemDefinition ): Promise<void> {
        return QuestHelper.rewardSingleItem( player, item.id, item.count )
    }

    async rewardPlayer( player: L2PcInstance, value: number ): Promise<void> {
        switch ( value ) {
            case 0:
                return QuestHelper.giveAdena( player, 40000, true )

            case 1:
                return QuestHelper.giveAdena( player, 80000, true )

            case 2:
                await QuestHelper.giveAdena( player, 110000, true )
                return this.rewardItem( player, SCROLL_ENCHANT_WEAPON_D_GRADE )

            case 3:
                await QuestHelper.giveAdena( player, 199000, true )
                return this.rewardItem( player, SCROLL_ENCHANT_WEAPON_C_GRADE )

            case 4:
                await QuestHelper.giveAdena( player, 388000, true )

                switch ( _.random( 1, 18 ) ) {
                    case 1:
                        return this.rewardItem( player, RECIPE_GREAT_SWORD_60 )
                    case 2:
                        return this.rewardItem( player, RECIPE_HEAVY_WAR_AXE_60 )
                    case 3:
                        return this.rewardItem( player, RECIPE_SPRITES_STAFF_60 )
                    case 4:
                        return this.rewardItem( player, RECIPE_KESHANBERK_60 )
                    case 5:
                        return this.rewardItem( player, RECIPE_SWORD_OF_VALHALLA_60 )
                    case 6:
                        return this.rewardItem( player, RECIPE_KRIS_60 )
                    case 7:
                        return this.rewardItem( player, RECIPE_HELL_KNIFE_60 )
                    case 8:
                        return this.rewardItem( player, RECIPE_ARTHRO_NAIL_60 )
                    case 9:
                        return this.rewardItem( player, RECIPE_DARK_ELVEN_LONG_BOW_60 )
                    case 10:
                        return this.rewardItem( player, RECIPE_GREAT_AXE_60 )
                    case 11:
                        return this.rewardItem( player, RECIPE_SWORD_OF_DAMASCUS_60 )
                    case 12:
                        return this.rewardItem( player, RECIPE_LANCE_60 )
                    case 13:
                        return this.rewardItem( player, RECIPE_DEADMANS_GLORY_60 )
                    case 14:
                        return this.rewardItem( player, RECIPE_ART_OF_BATTLE_AXE_60 )
                    case 15:
                        return this.rewardItem( player, RECIPE_TAFF_OF_EVIL_SPIRITS_60 )
                    case 16:
                        return this.rewardItem( player, RECIPE_DEMONS_DAGGER_60 )
                    case 17:
                        return this.rewardItem( player, RECIPE_BELLION_CESTUS_60 )
                    case 18:
                        return this.rewardItem( player, RECIPE_BOW_OF_PERIL_60 )
                }

                return

            case 5:
                await QuestHelper.giveAdena( player, 675000, true )
                switch ( _.random( 1, 18 ) ) {
                    case 1:
                        return this.rewardItem( player, GREAT_SWORD_BLADE )
                    case 2:
                        return this.rewardItem( player, GREAT_AXE_HEAD )
                    case 3:
                        return this.rewardItem( player, DARK_ELVEN_LONGBOW_SHAFT )
                    case 4:
                        return this.rewardItem( player, SWORD_OF_VALHALLA_BLADE )
                    case 5:
                        return this.rewardItem( player, ARTHRO_NAIL_BLADE )
                    case 6:
                        return this.rewardItem( player, SPRITES_STAFF_HEAD )
                    case 7:
                        return this.rewardItem( player, KRIS_EDGE )
                    case 8:
                        return this.rewardItem( player, KESHANBERK_BLADE )
                    case 9:
                        return this.rewardItem( player, HEAVY_WAR_AXE_HEAD )
                    case 10:
                        return this.rewardItem( player, HELL_KNIFE_EDGE )
                    case 11:
                        return this.rewardItem( player, SWORD_OF_DAMASCUS_BLADE )
                    case 12:
                        return this.rewardItem( player, LANCE_BLADE )
                    case 13:
                        return this.rewardItem( player, BELLION_CESTUS_EDGE )
                    case 14:
                        return this.rewardItem( player, EVIL_SPIRIT_HEAD )
                    case 15:
                        return this.rewardItem( player, DEADMANS_GLORY_STONE )
                    case 16:
                        return this.rewardItem( player, ART_OF_BATTLE_AXE_BLADE )
                    case 17:
                        return this.rewardItem( player, DEMONS_DAGGER_EDGE )
                    case 18:
                        return this.rewardItem( player, BOW_OF_PERIL_SHAFT )
                }

                return

            case 6:
                await QuestHelper.giveAdena( player, 1284000, true )
                await this.rewardItem( player, SCROLL_ENCHANT_WEAPON_B_GRADE )

                return this.rewardItem( player, SCROLL_ENCHANT_ARMOR_B_GRADE )
        }
    }

    formatReply( player: L2PcInstance, path: string, cardOne: number, cardTwo: number, winCount: number, cardValue: number ): string {
        let html: string = DataManager.getHtmlData().getItem( this.getPath( path ) )
                .replace( '<?card1pic?>', cardOne.toString() )
                .replace( '<?card2pic?>', cardTwo.toString() )
                .replace( '<?name?>', player.getName() )

        if ( winCount >= 0 ) {
            html = html.replace( '<?wincount?>', winCount.toString() )
        }

        if ( cardValue >= 0 ) {
            html = html.replace( '<?card1?>', cardValue.toString() )
        }

        return html
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let isSoulLeader: boolean = data.npcId === SPITEFUL_SOUL_LEADER
        let isFailedChance: boolean = Math.random() > QuestHelper.getAdjustedChance( SPIRIT_BEAD, monsterRewardChances[ data.npcId ], data.isChampion )
        if ( !isSoulLeader && isFailedChance ) {
            return
        }

        let player: L2PcInstance = this.getRandomPartyMemberPerCondition( L2World.getPlayer( data.playerId ), -1, 2, L2World.getObjectById( data.targetId ) )
        if ( !player ) {
            return
        }

        let amount: number = !isSoulLeader || isFailedChance ? 1 : 2
        await QuestHelper.rewardSingleQuestItem( player, SPIRIT_BEAD, amount, data.isChampion )
    }

    checkPartyMemberConditions( state: QuestState ): boolean {
        return state.isStarted() && state.getMemoState() >= 1 && state.getMemoState() <= 4
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() < minimumLevel ? '30846-02.html' : '30846-01.htm' )

            case QuestStateValues.STARTED:
                if ( state.getMemoState() >= 1 && state.getMemoState() < 4 ) {
                    if ( QuestHelper.hasQuestItem( player, SPIRIT_BEAD ) ) {
                        return this.getPath( '30846-05.html' )
                    }

                    return this.getPath( '30846-04.html' )
                }

                if ( Math.floor( state.getMemoState() / 1000 ) === 0 ) {
                    switch ( state.getMemoState() % 10 ) {
                        case 4:
                            return this.getPath( '30846-05a.html' )

                        case 5:
                            return this.getPath( '30846-11.html' )

                        case 6:
                            return this.getPath( '30846-15.html' )

                        case 7:
                            if ( ( ( state.getMemoState() % 100 ) / 10 ) >= 7 ) {
                                state.setMemoState( 1 )

                                await QuestHelper.giveAdena( player, 2384000, true )
                                await this.rewardItem( player, SCROLL_ENCHANT_WEAPON_A_GRADE )
                                await this.rewardItem( player, SCROLL_ENCHANT_ARMOR_A_GRADE )

                                return this.getPath( '30846-17.html' )
                            }

                            let winCount = Math.floor( state.getMemoState() / 10 ) + 1
                            return this.formatReply( player, '30846-16.html', 0, 0, winCount, 0 )
                    }

                    break
                }

                if ( state.isMemoState( 1005 ) ) {
                    return this.getPath( '30846-23.html' )
                }

                if ( state.isMemoState( 1006 ) ) {
                    return this.getPath( '30846-26.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}