import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const DIMENSIONAL_FRAGMENT = 7079
const minimumLevel = 20

const talkableNpcIds: Array<number> = [
    31494,
    31495,
    31496,
    31497,
    31498,
    31499,
    31500,
    31501,
    31502,
    31503,
    31504,
    31505,
    31506,
    31507,
]

export class InSearchOfFragmentsOfDimension extends ListenerLogic {
    constructor() {
        super( 'Q00634_InSearchOfFragmentsOfDimension', 'listeners/tracked-600/InSearchOfFragmentsOfDimension.ts' )
        this.questId = 634
    }

    getQuestStartIds(): Array<number> {
        return talkableNpcIds
    }

    getTalkIds(): Array<number> {
        return talkableNpcIds
    }

    getAttackableKillIds(): Array<number> {
        return [
            21208, // Hallowed Watchman
            21209, // Hallowed Seer
            21210, // Vault Guardian
            21211, // Vault Seer
            21212, // Hallowed Sentinel
            21213, // Hallowed Monk
            21214, // Vault Sentinel
            21215, // Vault Monk
            21216, // Overlord of the Holy Lands
            21217, // Hallowed Priest
            21218, // Vault Overlord
            21219, // Vault Priest
            21220, // Sepulcher Archon
            21221, // Sepulcher Inquisitor
            21222, // Sepulcher Archon
            21223, // Sepulcher Inquisitor
            21224, // Sepulcher Guardian
            21225, // Sepulcher Sage
            21226, // Sepulcher Guardian
            21227, // Sepulcher Sage
            21228, // Sepulcher Guard
            21229, // Sepulcher Preacher
            21230, // Sepulcher Guard
            21231, // Sepulcher Preacher
            21232, // Barrow Guardian
            21233, // Barrow Seer
            21234, // Grave Guardian
            21235, // Grave Seer
            21236, // Barrow Sentinel
            21237, // Barrow Monk
            21238, // Grave Sentinel
            21239, // Grave Monk
            21240, // Barrow Overlord
            21241, // Barrow Priest
            21242, // Grave Overlord
            21243, // Grave Priest
            21244, // Crypt Archon
            21245, // Crypt Inquisitor
            21246, // Tomb Archon
            21247, // Tomb Inquisitor
            21248, // Crypt Guardian
            21249, // Crypt Sage
            21250, // Tomb Guardian
            21251, // Tomb Sage
            21252, // Crypt Guard
            21253, // Crypt Preacher
            21254, // Tomb Guard
            21255, // Tomb Preacher
            21256, // Underground Werewolf
        ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '31494-02.htm':
                state.startQuest()
                break

            case '31494-05.html':
            case '31494-06.html':
                break

            case '31494-07.html':
                await state.exitQuest( true, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( Math.random() > QuestHelper.getAdjustedChance( DIMENSIONAL_FRAGMENT, 0.1, data.isChampion ) ) {
            return
        }

        let npc = L2World.getObjectById( data.targetId ) as L2Npc
        let player = this.getRandomPartyMemberPerCondition( L2World.getPlayer( data.playerId ), -1, 3, npc )
        if ( !player ) {
            return
        }

        let amount: number = Math.floor( ( 0.15 * npc.getLevel() ) + 1.6 )
        await QuestHelper.rewardSingleQuestItem( player, DIMENSIONAL_FRAGMENT, amount, data.isChampion )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '31494-01.htm' : '31494-03.htm' )

            case QuestStateValues.STARTED:
                return this.getPath( '31494-04.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00634_InSearchOfFragmentsOfDimension'
    }
}