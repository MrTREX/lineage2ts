import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const ASHAS = 31377
const SEED = 7187
const MOLAR = 7234
const minimumLevel = 74
const MOLAR_COUNT = 100

const monsterRewardChances: { [ npcId: number ]: number } = {
    21324: 0.500, // Ketra Orc Footman
    21327: 0.510, // Ketra Orc Raider
    21328: 0.522, // Ketra Orc Scout
    21329: 0.519, // Ketra Orc Shaman
    21331: 0.529, // Ketra Orc Warrior
    21332: 0.529, // Ketra Orc Lieutenant
    21334: 0.539, // Ketra Orc Medium
    21336: 0.548, // Ketra Orc White Captain
    21338: 0.558, // Ketra Orc Seer
    21339: 0.568, // Ketra Orc General
    21340: 0.568, // Ketra Orc Battalion Commander
    21342: 0.578, // Ketra Orc Grand Seer
    21343: 0.664, // Ketra Commander
    21345: 0.713, // Ketra's Head Shaman
    21347: 0.738, // Ketra Prophet
}

export class BattleAgainstKetraOrcs extends ListenerLogic {
    constructor() {
        super( 'Q00612_BattleAgainstKetraOrcs', 'listeners/tracked-600/BattleAgainstKetraOrcs.ts' )
        this.questId = 612
        this.questItemIds = [ MOLAR ]
    }

    getQuestStartIds(): Array<number> {
        return [ ASHAS ]
    }

    getTalkIds(): Array<number> {
        return [ ASHAS ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31377-03.htm':
                state.startQuest()
                break

            case '31377-06.html':
                break

            case '31377-07.html':
                if ( QuestHelper.getQuestItemsCount( player, MOLAR ) < MOLAR_COUNT ) {
                    return this.getPath( '31377-08.html' )
                }

                await QuestHelper.takeSingleItem( player, MOLAR, MOLAR_COUNT )
                await QuestHelper.rewardSingleItem( player, SEED, 20 )

                break

            case '31377-09.html':
                await state.exitQuest( true, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( Math.random() > QuestHelper.getAdjustedChance( MOLAR, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        let player: L2PcInstance = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 1 )
        if ( !player ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, MOLAR, 1, data.isChampion )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '31377-01.htm' : '31377-02.htm' )

            case QuestStateValues.STARTED:
                return this.getPath( QuestHelper.hasQuestItem( player, MOLAR ) ? '31377-04.html' : '31377-05.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00612_BattleAgainstKetraOrcs'
    }
}