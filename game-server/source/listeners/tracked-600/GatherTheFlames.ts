import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const HILDA = 31271
const VULCAN = 31539
const ROONEY = 32049
const TORCH = 7264
const playerRewardItemIds: Array<number> = [
    6881,
    6883,
    6885,
    6887,
    6891,
    6893,
    6895,
    6897,
    6899,
    7580,
]

const monsterRewardChances: { [ npcId: number ]: number } = {
    22634: 0.639,
    22635: 0.611,
    22636: 0.649,
    22637: 0.639,
    22638: 0.639,
    22639: 0.645,
    22640: 0.559,
    22641: 0.588,
    22642: 0.537,
    22643: 0.618,
    22644: 0.633,
    22645: 0.550,
    22646: 0.593,
    22647: 0.688,
    22648: 0.632,
    22649: 0.685,
}

const minimumLevel = 74

export class GatherTheFlames extends ListenerLogic {
    constructor() {
        super( 'Q00617_GatherTheFlames', 'listeners/tracked-600/GatherTheFlames.ts' )
        this.questId = 617
        this.questItemIds = [ TORCH ]
    }

    getQuestStartIds(): Array<number> {
        return [ HILDA, VULCAN ]
    }

    getTalkIds(): Array<number> {
        return [ ROONEY, HILDA, VULCAN ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31539-03.htm':
            case '31271-03.htm':
                state.startQuest()
                break

            case '32049-02.html':
            case '31539-04.html':
            case '31539-06.html':
                break

            case '31539-07.html':
                if ( QuestHelper.getQuestItemsCount( player, TORCH ) < 1000 || !state.isStarted() ) {
                    return QuestHelper.getNoQuestMessagePath()
                }

                await QuestHelper.takeSingleItem( player, TORCH, 1000 )
                await QuestHelper.rewardSingleItem( player, _.sample( playerRewardItemIds ), 1 )
                break

            case '31539-08.html':
                await state.exitQuest( true, true )
                break

            case '6883':
            case '6885':
            case '7580':
            case '6891':
            case '6893':
            case '6895':
            case '6897':
            case '6899':
            case '6887':
            case '6881':
                if ( QuestHelper.getQuestItemsCount( player, TORCH ) < 1200 || !state.isStarted() ) {
                    return QuestHelper.getNoQuestMessagePath()
                }

                await QuestHelper.takeSingleItem( player, TORCH, 1200 )
                await QuestHelper.rewardSingleItem( player, _.parseInt( data.eventName ), 1 )

                if ( [ '6887', '6881' ].includes( data.eventName ) ) {
                    return this.getPath( '32049-03.html' )
                }

                return this.getPath( '32049-04.html' )

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player: L2PcInstance = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 1 )
        if ( !player ) {
            return
        }

        let amount: number = Math.random() < QuestHelper.getAdjustedChance( TORCH, monsterRewardChances[ data.npcId ], data.isChampion ) ? 2 : 1
        await QuestHelper.rewardSingleQuestItem( player, TORCH, amount, data.isChampion )

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                switch ( data.characterNpcId ) {
                    case VULCAN:
                        return this.getPath( player.getLevel() >= minimumLevel ? '31539-01.htm' : '31539-02.htm' )

                    case HILDA:
                        return this.getPath( player.getLevel() >= minimumLevel ? '31271-01.htm' : '31271-02.htm' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case ROONEY:
                        return this.getPath( QuestHelper.getQuestItemsCount( player, TORCH ) >= 1200 ? '32049-02.html' : '32049-01.html' )

                    case VULCAN:
                        return this.getPath( QuestHelper.getQuestItemsCount( player, TORCH ) >= 1000 ? '31539-04.html' : '31539-05.html' )

                    case HILDA:
                        return this.getPath( '31271-04.html' )
                }

                break

            case QuestStateValues.COMPLETED:
                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00617_GatherTheFlames'
    }
}