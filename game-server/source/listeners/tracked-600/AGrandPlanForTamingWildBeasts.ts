import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { L2Clan } from '../../gameService/models/L2Clan'
import { DataManager } from '../../data/manager'
import { ClanHallSiegeManager } from '../../gameService/instancemanager/ClanHallSiegeManager'
import { ClanHallSiegeEngineValues } from '../../gameService/models/entity/clanhall/ClanHallSiegeEngine'
import { SiegableHall } from '../../gameService/models/entity/clanhall/SiegableHall'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { QuestHelper } from '../helpers/QuestHelper'
import moment from 'moment'

const MESSENGER = 35627
const CRYSTAL_OF_PURITY = 8084
const TRAINER_LICENSE = 8293
const REQUIRED_CRYSTAL_COUNT = 10
const clanLevel = 4
const minutesTillSiegeBegins = 3600
const initialMessagePath = 'data/datapack/conquerablehalls/flagwar/WildBeastReserve/messenger_initial.htm'

export class AGrandPlanForTamingWildBeasts extends ListenerLogic {
    constructor() {
        super( 'Q00655_AGrandPlanForTamingWildBeasts', 'listeners/tracked-600/AGrandPlanForTamingWildBeasts.ts' )
        this.questId = 655
        this.questItemIds = [ CRYSTAL_OF_PURITY, TRAINER_LICENSE ]
    }

    getQuestStartIds(): Array<number> {
        return [ MESSENGER ]
    }

    getTalkIds(): Array<number> {
        return [ MESSENGER ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '35627-06.html':
                let player = L2World.getPlayer( data.playerId )
                let clan: L2Clan = player.getClan()
                if ( clan
                        && clan.getLevel() >= clanLevel
                        && clan.getFortId() === 0 //
                        && player.isClanLeader()
                        && this.getMinutesToSiege() < minutesTillSiegeBegins ) {
                    state.startQuest()
                    break
                }

                return

            case '35627-06a.html':
                break

            case '35627-11.html':
                if ( this.getMinutesToSiege() < minutesTillSiegeBegins ) {
                    return initialMessagePath
                }

                return DataManager.getHtmlData().getItem( this.getPath( data.eventName ) )
                        .replace( '%next_siege%', this.getSiegeFormattedDate() )

        }

        return this.getPath( data.eventName )
    }

    getMinutesToSiege(): number {
        let hall: SiegableHall = this.getClanHall()
        if ( hall ) {
            return Math.floor( ( hall.getNextSiegeTime() - Date.now() ) / 3600 )
        }
    }

    getClanHall(): SiegableHall {
        return ClanHallSiegeManager.getSiegableHall( ClanHallSiegeEngineValues.BEAST_FARM )
    }

    getSiegeFormattedDate(): string {
        let hall: SiegableHall = this.getClanHall()
        if ( hall ) {
            return moment( hall.getNextSiegeTime() ).format( 'yyyy-MM-dd HH:mm:ss' )
        }

        return 'Clan Hall is unavailable'
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00655_AGrandPlanForTamingWildBeasts'
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                let clan: L2Clan = player.getClan()
                if ( !clan ) {
                    break
                }

                if ( this.getMinutesToSiege() < minutesTillSiegeBegins ) {
                    if ( player.isClanLeader() ) {
                        if ( clan.getFortId() === 0 ) {
                            if ( clan.getLevel() >= clanLevel ) {
                                return this.getPath( '35627-01.html' )
                            }

                            return this.getPath( '35627-03.html' )
                        }

                        return this.getPath( '35627-04.html' )
                    }

                    if ( clan.getFortId() === ClanHallSiegeEngineValues.BEAST_FARM ) {
                        return initialMessagePath
                    }

                    return this.getPath( '35627-05.html' )
                }

                return DataManager.getHtmlData().getItem( this.getPath( '35627-02.html' ) )
                        .replace( '%next_siege%', this.getSiegeFormattedDate() )

            case QuestStateValues.STARTED:
                if ( this.getMinutesToSiege() > minutesTillSiegeBegins ) {
                    await state.exitQuest( true, true )

                    return this.getPath( '35627-07.html' )
                }

                if ( QuestHelper.hasQuestItem( player, TRAINER_LICENSE ) ) {
                    return this.getPath( '35627-09.html' )
                }

                if ( QuestHelper.getQuestItemsCount( player, CRYSTAL_OF_PURITY ) < REQUIRED_CRYSTAL_COUNT ) {
                    return this.getPath( '35627-08.html' )
                }

                await QuestHelper.takeSingleItem( player, CRYSTAL_OF_PURITY, -1 )
                await QuestHelper.giveSingleItem( player, TRAINER_LICENSE, 1 )

                state.setConditionWithSound( 3, true )

                return this.getPath( '35627-10.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}