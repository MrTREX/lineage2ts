import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const LEKON = 32557
const VALDSTONE = 25623
const VULTURES_GOLDEN_FEATHER = 13871
const minimumLevel = 75
const VULTURES_GOLDEN_FEATHER_ADENA = 1500
const BONUS = 8335
const bonusLimit = 10

const monsterRewardChances: { [ npcId: number ]: number } = {
    22614: 0.840, // Vulture Rider lvl 1
    22615: 0.857, // Vulture Rider lvl 2
    25633: 0.719, // Vulture Rider lvl 3
    25623: 1,
}

export class GuardianOfTheSkies extends ListenerLogic {
    constructor() {
        super( 'Q00699_GuardianOfTheSkies', 'listeners/tracked-600/GuardianOfTheSkies.ts' )
        this.questId = 699
        this.questItemIds = [ VULTURES_GOLDEN_FEATHER ]
    }

    getQuestStartIds(): Array<number> {
        return [ LEKON ]
    }

    getTalkIds(): Array<number> {
        return [ LEKON ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '32557-03.htm':
            case '32557-08.html':
                break

            case '32557-04.htm':
                state.startQuest()
                break

            case '32557-09.html':
                await state.exitQuest( true, true )
                break


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( Math.random() > QuestHelper.getAdjustedChance( VULTURES_GOLDEN_FEATHER, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        let amount = data.npcId !== VALDSTONE ? 1 : _.random( 10 ) + this.getValdStoneModifier()
        await QuestHelper.rewardSingleQuestItem( player, VULTURES_GOLDEN_FEATHER, amount, data.isChampion )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    getValdStoneModifier(): number {
        let chance = Math.random()

        if ( chance < 0.215 ) {
            return 90
        }

        if ( chance < 0.446 ) {
            return 80
        }

        if ( chance < 0.715 ) {
            return 70
        }

        return 60
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                let isNotQualified = !player.hasQuestCompleted( 'Q10273_GoodDayToFly' ) || player.getLevel() < minimumLevel
                return this.getPath( isNotQualified ? '32557-02.htm' : '32557-01.htm' )

            case QuestStateValues.STARTED:
                let amount = QuestHelper.getQuestItemsCount( player, VULTURES_GOLDEN_FEATHER )
                if ( amount > 0 ) {
                    let adenaAmount = ( amount * VULTURES_GOLDEN_FEATHER_ADENA ) + ( amount > bonusLimit ? BONUS : 0 )
                    await QuestHelper.takeSingleItem( player, VULTURES_GOLDEN_FEATHER, -1 )
                    await QuestHelper.giveAdena( player, adenaAmount, true )

                    return this.getPath( amount > bonusLimit ? '32557-07.html' : '32557-06.html' )
                }

                return this.getPath( '32557-05.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00699_GuardianOfTheSkies'
    }
}