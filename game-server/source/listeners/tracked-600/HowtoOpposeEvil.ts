import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { ItemTypes } from '../../gameService/values/InventoryValues'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const DILIOS = 32549
const KIRKLAN = 32550
const LEKONS_CERTIFICATE = 13857
const minimumLevel = 75

type MonsterData = [ number, number ] // itemId, chance
const monsterRewards: { [ npcId: number ]: MonsterData } = {

    // Seed of Infinity
    22509: [ 13863, 500 ],
    22510: [ 13863, 500 ],
    22511: [ 13863, 500 ],
    22512: [ 13863, 500 ],
    22513: [ 13863, 500 ],
    22514: [ 13863, 500 ],
    22515: [ 13863, 500 ],

    // Seed of Destruction
    22537: [ 13865, 250 ],
    22538: [ 13865, 250 ],
    22539: [ 13865, 250 ],
    22540: [ 13865, 250 ],
    22541: [ 13865, 250 ],
    22542: [ 13865, 250 ],
    22543: [ 13865, 250 ],
    22544: [ 13865, 250 ],
    22546: [ 13865, 250 ],
    22547: [ 13865, 250 ],
    22548: [ 13865, 250 ],
    22549: [ 13865, 250 ],
    22550: [ 13865, 250 ],
    22551: [ 13865, 250 ],
    22552: [ 13865, 250 ],
    22593: [ 13865, 250 ],
    22596: [ 13865, 250 ],
    22597: [ 13865, 250 ],

    // Seed of Annihilation
    22746: [ 15536, 125 ],
    22747: [ 15536, 125 ],
    22748: [ 15536, 125 ],
    22749: [ 15536, 125 ],
    22750: [ 15536, 125 ],
    22751: [ 15536, 125 ],
    22752: [ 15536, 125 ],
    22753: [ 15536, 125 ],
    22754: [ 15536, 125 ],
    22755: [ 15536, 125 ],
    22756: [ 15536, 125 ],
    22757: [ 15536, 125 ],
    22758: [ 15536, 125 ],
    22759: [ 15536, 125 ],
    22760: [ 15536, 125 ],
    22761: [ 15536, 125 ],
    22762: [ 15536, 125 ],
    22763: [ 15536, 125 ],
    22764: [ 15536, 125 ],
    22765: [ 15536, 125 ],
}

const rewardTypes = {
    spritStoneFragment: 15486,
    dragonkinCharm: 13841,
    freedSoul: 13796,
}

export class HowtoOpposeEvil extends ListenerLogic {
    constructor() {
        super( 'Q00692_HowtoOpposeEvil', 'listeners/tracked-600/HowtoOpposeEvil.ts' )
        this.questId = 692
        this.questItemIds = [
            13863,
            13864,
            13865,
            13866,
            13867,
            15535,
            15536,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ DILIOS ]
    }

    getTalkIds(): Array<number> {
        return [ DILIOS, KIRKLAN ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewards ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00692_HowtoOpposeEvil'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32549-03.htm':
                state.startQuest()
                break

            case '32550-04.htm':
                state.setConditionWithSound( 3 )
                break

            case '32550-07.htm':
                if ( await this.checkAndReward( player, 13863, 5, rewardTypes.freedSoul, 1 ) ) {
                    return this.getPath( '32550-08.htm' )
                }

                break

            case '32550-09.htm':
                if ( !await this.checkAndReward( player, 13798, 1, ItemTypes.Adena, 5000 ) ) {
                    return this.getPath( '32550-10.htm' )
                }

                break

            case '32550-12.htm':
                if ( await this.checkAndReward( player, 13865, 5, rewardTypes.dragonkinCharm, 1 ) ) {
                    return this.getPath( '32550-13.htm' )
                }

                break

            case '32550-14.htm':
                if ( await this.checkAndReward( player, 13867, 1, ItemTypes.Adena, 5000 ) ) {
                    return this.getPath( '32550-15.htm' )
                }

                break

            case '32550-17.htm':
                if ( await this.checkAndReward( player, 15536, 5, rewardTypes.spritStoneFragment, 1 ) ) {
                    return this.getPath( '32550-18.htm' )
                }

                break

            case '32550-19.htm':
                if ( await this.checkAndReward( player, 15535, 1, ItemTypes.Adena, 5000 ) ) {
                    return this.getPath( '32550-20.htm' )
                }

                break

            default:
                return

        }
        return this.getPath( data.eventName )
    }

    async checkAndReward( player: L2PcInstance, requiredItemId: number, requiredAmount: number, rewardItemId: number, rewardAmount: number ): Promise<boolean> {
        let amountAvailable = QuestHelper.getQuestItemsCount( player, requiredItemId )
        if ( amountAvailable < requiredAmount ) {
            return true
        }

        await QuestHelper.takeSingleItem( player, requiredItemId, requiredAmount )
        await QuestHelper.rewardSingleItem( player, rewardItemId, rewardAmount )

        return false
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let [ itemId, chance ]: MonsterData = monsterRewards[ data.npcId ]
        if ( Math.random() > QuestHelper.getAdjustedChance( itemId, chance, data.isChampion ) ) {
            return
        }

        let player: L2PcInstance = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 3 )
        if ( !player ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, itemId, 1, data.isChampion )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '32549-01.htm' : '32549-00.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case DILIOS:
                        if ( state.isCondition( 1 ) && QuestHelper.hasQuestItem( player, LEKONS_CERTIFICATE ) ) {
                            await QuestHelper.takeSingleItem( player, LEKONS_CERTIFICATE, -1 )
                            state.setConditionWithSound( 2 )

                            return this.getPath( '32549-04.htm' )
                        }

                        if ( state.isCondition( 2 ) ) {
                            return this.getPath( '32549-05.htm' )
                        }

                        break

                    case KIRKLAN:
                        if ( state.isCondition( 2 ) ) {
                            return this.getPath( '32550-01.htm' )
                        }

                        if ( state.isCondition( 3 ) ) {
                            if ( QuestHelper.hasAtLeastOneQuestItem( player, ...this.questItemIds ) ) {
                                return this.getPath( '32550-05.htm' )
                            }

                            return this.getPath( '32550-04.htm' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}