import { ListenerLogic } from '../../gameService/models/ListenerLogic'

export class IntoTheDimensionalRift extends ListenerLogic {
    constructor() {
        super( 'Q00635_IntoTheDimensionalRift', 'listeners/tracked-600/IntoTheDimensionalRift.ts' )
        this.questId = 635
    }

    getPathPrefix(): string {
        return 'data/datapack/'
    }
}