import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'
import { createItemDefinition, ItemDefinition } from '../../gameService/interface/ItemDefinition'

const JEREMY = 31521
const THERMAL_BUFFALO = 21315
const THERMAL_FLAVA = 21316
const THERMAL_ANTELOPE = 21318
const LEAF_OF_FLAVA = createItemDefinition( 7199, 100 )
const BUFFALO_MEAT = createItemDefinition( 7200, 100 )
const HORN_OF_ANTELOPE = createItemDefinition( 7201, 100 )
const RING_OF_AURAKYRA = createItemDefinition( 6849, 1 )
const SEALED_SANDDRAGONS_EARING = createItemDefinition( 6847, 1 )
const DRAGON_NECKLACE = createItemDefinition( 6851, 1 )
const minimumLevel = 71

const monsterRewards: { [ npcId: number ]: ItemDefinition } = {
    [ THERMAL_BUFFALO ]: BUFFALO_MEAT,
    [ THERMAL_FLAVA ]: LEAF_OF_FLAVA,
    [ THERMAL_ANTELOPE ]: HORN_OF_ANTELOPE,
}

export class TheFinestFood extends ListenerLogic {
    constructor() {
        super( 'Q00623_TheFinestFood', 'listeners/tracked-600/TheFinestFood.ts' )
        this.questId = 623
        this.questItemIds = [
            LEAF_OF_FLAVA.id,
            BUFFALO_MEAT.id,
            HORN_OF_ANTELOPE.id,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ JEREMY ]
    }

    getTalkIds(): Array<number> {
        return [ JEREMY ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewards ).map( value => _.parseInt( value ) )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '31521-03.htm':
                if ( state.isCreated() ) {
                    state.startQuest()
                    break
                }

                return

            case '31521-06.html':
                if ( !state.isCondition( 2 ) ) {
                    return
                }

                let player = L2World.getPlayer( data.playerId )
                if ( QuestHelper.hasEachItem( player, [ LEAF_OF_FLAVA, BUFFALO_MEAT, HORN_OF_ANTELOPE ] ) ) {
                    await this.rewardPlayer( player )
                    await state.exitQuest( true, true )
                    break
                }

                return this.getPath( '31521-07.html' )


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async rewardPlayer( player: L2PcInstance ): Promise<void> {
        let random = Math.random()
        if ( random < 0.120 ) {
            await QuestHelper.giveAdena( player, 25000, true )
            await QuestHelper.rewardSingleItem( player, RING_OF_AURAKYRA.id, RING_OF_AURAKYRA.count )
        }

        if ( random < 0.240 ) {
            await QuestHelper.giveAdena( player, 65000, true )
            await QuestHelper.rewardSingleItem( player, SEALED_SANDDRAGONS_EARING.id, SEALED_SANDDRAGONS_EARING.count )

            return
        }

        if ( random < 0.340 ) {
            await QuestHelper.giveAdena( player, 25000, true )
            await QuestHelper.rewardSingleItem( player, DRAGON_NECKLACE.id, DRAGON_NECKLACE.count )

            return
        }

        if ( random < 0.940 ) {
            await QuestHelper.giveAdena( player, 73000, true )
            await QuestHelper.addExpAndSp( player, 230000, 18200 )

            return
        }
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), 1, 3, L2World.getObjectById( data.targetId ) )
        if ( !state ) {
            return
        }

        let player = state.getPlayer()
        let item: ItemDefinition = monsterRewards[ data.npcId ]
        await QuestHelper.rewardUpToLimit( player, item.id, 1, item.count, data.isChampion )

        if ( QuestHelper.hasEachItem( player, [ BUFFALO_MEAT, HORN_OF_ANTELOPE, LEAF_OF_FLAVA ] ) ) {
            state.setConditionWithSound( 2, true )
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '31521-01.htm' : '31521-02.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '31521-04.html' )

                    case 2:
                        return this.getPath( '31521-05.html' )
                }

                break

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00623_TheFinestFood'
    }
}