import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestStateValues } from '../../gameService/models/quest/State'

const KLEIN = 31540
const VALAKAS = 29028
const EMPTY_CRYSTAL = 21906
const FILLED_CRYSTAL_VALAKAS_ENERGY = 21908
const JEWEL_OF_VALAKAS = 21896
const VACUALITE_FLOATING_STONE = 7267
const minimumLevel = 83

export class JewelOfValakas extends ListenerLogic {
    constructor() {
        super( 'Q10505_JewelOfValakas', 'listeners/tracked-10500/JewelOfValakas.ts' )
        this.questId = 10505
        this.questItemIds = [ EMPTY_CRYSTAL, FILLED_CRYSTAL_VALAKAS_ENERGY ]
    }

    getQuestStartIds(): Array<number> {
        return [ KLEIN ]
    }

    getTalkIds(): Array<number> {
        return [ KLEIN ]
    }

    getAttackableKillIds(): Array<number> {
        return [ VALAKAS ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q10505_JewelOfValakas'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        if ( player.getLevel() < minimumLevel || !QuestHelper.hasQuestItem( player, VACUALITE_FLOATING_STONE ) ) {
            return
        }

        switch ( data.eventName ) {
            case '31540-05.htm':
            case '31540-06.htm':
                break

            case '31540-07.html':
                state.startQuest()
                await QuestHelper.giveSingleItem( player, EMPTY_CRYSTAL, 1 )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        await QuestHelper.runStateActionForCommandChannel( data.playerId, data.targetId, this.getName(), async ( state: QuestState, player: L2PcInstance ): Promise<void> => {
            if ( !state.isCondition( 1 ) ) {
                return
            }

            await QuestHelper.takeSingleItem( player, EMPTY_CRYSTAL, -1 )
            await QuestHelper.giveSingleItem( player, FILLED_CRYSTAL_VALAKAS_ENERGY, 1 )

            state.setConditionWithSound( 2, true )
        } )

        return
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '31540-02.html' )
                }

                if ( !QuestHelper.hasQuestItem( player, VACUALITE_FLOATING_STONE ) ) {
                    return this.getPath( '31540-04.html' )
                }

                return this.getPath( '31540-01.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        if ( QuestHelper.hasQuestItem( player, EMPTY_CRYSTAL ) ) {
                            return this.getPath( '31540-08.html' )
                        }

                        await QuestHelper.giveSingleItem( player, EMPTY_CRYSTAL, 1 )
                        return this.getPath( '31540-09.html' )

                    case 2:
                        await QuestHelper.giveSingleItem( player, JEWEL_OF_VALAKAS, 1 )
                        await state.exitQuest( false, true )

                        return this.getPath( '31540-10.html' )
                }

                break

            case QuestStateValues.COMPLETED:
                return this.getPath( '31540-03.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}