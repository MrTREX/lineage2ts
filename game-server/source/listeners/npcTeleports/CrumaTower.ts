import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import { NpcTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import _ from 'lodash'

const MOZELLA = 30483
const locations: Array<Location> = [
    new Location( 17776, 113968, -11671 ),
    new Location( 17680, 113968, -11671 ),
]

const playerLevel = 55

export class CrumaTower extends ListenerLogic {
    constructor() {
        super( 'CrumaTower', 'listeners/npcTeleports/CrumaTower.ts' )
    }

    getApproachedForTalkIds(): Array<number> {
        return [ MOZELLA ]
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/Teleports/CrumaTower'
    }

    getQuestStartIds(): Array<number> {
        return [ MOZELLA ]
    }

    getTalkIds(): Array<number> {
        return [ MOZELLA ]
    }

    async onTalkEvent( data : NpcTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )

        if ( player.getLevel() <= playerLevel ) {
            await player.teleportToLocation( _.sample( locations ) )
            return
        }

        return this.getPath( '30483-1.html' )
    }
}