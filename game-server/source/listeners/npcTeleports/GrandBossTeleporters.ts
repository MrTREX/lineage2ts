import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestHelper } from '../helpers/QuestHelper'
import { ListenerManager } from '../../gameService/instancemanager/ListenerManager'
import { GrandBossManager } from '../../gameService/cache/GrandBossManager'
import { L2GrandBossInstance } from '../../gameService/models/actor/instance/L2GrandBossInstance'
import { ConfigManager } from '../../config/ConfigManager'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import _ from 'lodash'
import { DoorManager } from '../../gameService/cache/DoorManager'
import { AreaCache } from '../../gameService/cache/AreaCache'
import { AreaType } from '../../gameService/models/areas/AreaType'
import { GeometryId } from '../../gameService/enums/GeometryId'
import { teleportCharacterToGeometryCoordinates } from '../../gameService/helpers/TeleportHelper'

const npcIds: Array<number> = [
    31384, // Gatekeeper of Fire Dragon : Opening some doors
    31385, // Heart of Volcano : Teleport into Lair of Valakas
    31540, // Watcher of Valakas Klein : Teleport into Hall of Flames
    31686, // Gatekeeper of Fire Dragon : Opens doors to Heart of Volcano
    31687, // Gatekeeper of Fire Dragon : Opens doors to Heart of Volcano
    31759, // Teleportation Cubic : Teleport out of Lair of Valakas
]

const VACUALITE_FLOATING_STONE = 7267
const ENTER_HALL_OF_FLAMES = new Location( 183813, -115157, -3303 )
const ValakasTeleport = new Location( 204328, -111874, 70 )
const outsideTeleport = new Location( 150037, -57720, -2976 )

const variableNames = {
    allowEnter: 'a'
}

export class GrandBossTeleporters extends ListenerLogic {
    playerCount: number = 0

    constructor() {
        super( 'GrandBossTeleporters', 'listeners/npcTeleports/GrandBossTeleporters.ts' )
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/Teleports/GrandBossTeleporters'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onNpcEvent( data : NpcGeneralEvent ): Promise<string> {

        let player = L2World.getPlayer( data.playerId )

        if ( QuestHelper.hasQuestItems( player, VACUALITE_FLOATING_STONE ) ) {
            let state: QuestState = this.getQuestState( data.playerId, false )

            if ( state ) {
                state.setVariable( variableNames.allowEnter, true )
                await player.teleportToLocation( ENTER_HALL_OF_FLAMES )
                return
            }
        }

        return this.getPath( '31540-06.htm' )
    }

    async onTalkEvent( data : NpcTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let state: QuestState = this.getQuestState( data.playerId, true )

        switch ( data.characterNpcId ) {
            case 31385:
                let prerequisiteQuest = this.getPrerequisiteQuest()
                if ( prerequisiteQuest ) {
                    let status = GrandBossManager.getBossStatus( 29028 )

                    if ( [ 0, 1 ].includes( status ) ) {
                        if ( this.playerCount >= 200 ) {
                            return this.getPath( '31385-03.htm' )
                        }

                        if ( state.getVariable( variableNames.allowEnter ) ) {
                            state.unsetVariable( variableNames.allowEnter )
                            let area = AreaCache.getAreaWithCoordinates( 212852, -114842, -1632, AreaType.Boss )

                            if ( area ) {
                                // TODO : record player entry time outside of area
                                //area.allowPlayerEntry( player, 30 )
                            }

                            await teleportCharacterToGeometryCoordinates( GeometryId.ValakasTeleport, player, ValakasTeleport.getX(), ValakasTeleport.getY(), ValakasTeleport.getZ() )

                            this.playerCount++

                            if ( status === 0 ) {
                                let valakas: L2GrandBossInstance = GrandBossManager.getBoss( 29028 )
                                if ( valakas ) {
                                    prerequisiteQuest.startQuestTimer( 'beginning', ConfigManager.grandBoss.getValakasWaitTime(), valakas.getObjectId() )
                                    await GrandBossManager.setBossStatus( 29028, 1 )
                                }
                            }

                            return
                        }

                        return this.getPath( '31385-04.htm' )
                    }

                    if ( status === 2 ) {
                        return this.getPath( '31385-02.htm' )
                    }

                    return this.getPath( '31385-01.htm' )
                }

                return this.getPath( '31385-01.htm' )

            case 31384:
                DoorManager.getDoor( 24210004 )?.openMe()
                return

            case 31686:
                DoorManager.getDoor( 24210006 )?.openMe()
                return

            case 31687:
                DoorManager.getDoor( 24210005 )?.openMe()
                break

            case 31540:
                if ( this.playerCount < 50 ) {
                    return this.getPath( '31540-01.htm' )
                }

                if ( this.playerCount < 100 ) {
                    return this.getPath( '31540-02.htm' )
                }

                if ( this.playerCount < 150 ) {
                    return this.getPath( '31540-03.htm' )
                }

                if ( this.playerCount < 200 ) {
                    return this.getPath( '31540-04.htm' )
                }

                return this.getPath( '31540-05.htm' )

            case 31759:
                await teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, outsideTeleport.getX(), outsideTeleport.getY(), outsideTeleport.getZ() )
                break
        }

        return
    }

    getPrerequisiteQuest(): ListenerLogic {
        return ListenerManager.getListenerByName( 'Valakas' )
    }
}