import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { NpcApproachedForTalkEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { L2DoorInstance } from '../../gameService/models/actor/instance/L2DoorInstance'
import { DoorManager } from '../../gameService/cache/DoorManager'

const TRIOLS_MIRROR_1 = 32039
const TRIOLS_MIRROR_2 = 32040

const locationMap = {
    [ TRIOLS_MIRROR_1 ]: new Location( -12766, -35840, -10856 ),
    [ TRIOLS_MIRROR_2 ]: new Location( 36640, -51218, 718 ),
}

const npcIds: Array<number> = [
    32034, 32035, 32036, 32037, 32039, 32040,
]

const VISITORS_MARK = 8064
const FADED_VISITORS_MARK = 8065
const PAGANS_MARK = 8067

export class PaganTeleporters extends ListenerLogic {
    constructor() {
        super( 'PaganTeleporters', 'listeners/npcTeleports/PaganTeleporters.ts' )
    }

    getApproachedForTalkIds(): Array<number> {
        return [ TRIOLS_MIRROR_1, TRIOLS_MIRROR_2 ]
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/Teleports/PaganTeleporters'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onApproachedForTalk( data : NpcApproachedForTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )

        let location: Location = locationMap[ data.characterNpcId ]
        if ( location ) {
            await player.teleportToLocation( location )
            return
        }
    }

    async onNpcEvent( data : NpcGeneralEvent ): Promise<string> {

        let event = data.eventName
        switch ( event ) {
            case 'Close_Door1':
                this.closeDoor( 19160001 )
                return

            case 'Close_Door2': {
                this.closeDoor( 19160010 )
                this.closeDoor( 19160011 )
                return
            }
        }
    }

    async onTalkEvent( data : NpcTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let npc : L2Npc = L2World.getObjectById( data.characterId ) as L2Npc

        switch ( npc.getId() ) {
            case 32034:
                if ( !QuestHelper.hasAtLeastOneQuestItem( player, VISITORS_MARK, FADED_VISITORS_MARK, PAGANS_MARK ) ) {
                    return this.getPath( 'noItem.htm' )
                }

                this.openDoor( 19160001 )
                this.startQuestTimer( 'Close_Door1', 10000 )

                return this.getPath( 'FadedMark.htm' )

            case 32035:
                this.openDoor( 19160001 )
                this.startQuestTimer( 'Close_Door1', 10000 )

                return this.getPath( 'FadedMark.htm' )

            case 32036:
                if ( !QuestHelper.hasQuestItems( player, PAGANS_MARK ) ) {
                    return this.getPath( 'noMark.htm' )
                }

                this.startQuestTimer( 'Close_Door2', 10000 )
                this.openDoor( 19160010 )
                this.openDoor( 19160011 )

                return this.getPath( 'openDoor.htm' )

            case 32037: {
                this.openDoor( 19160010 )
                this.openDoor( 19160011 )
                this.startQuestTimer( 'Close_Door2', 10000 )
                return this.getPath( 'FadedMark.htm' )
            }
        }

        return super.onTalkEvent( data )
    }

    private closeDoor( doorId: number ): void {
        let door: L2DoorInstance = DoorManager.getDoor( doorId )
        if ( door && door.isOpen() ) {
            door.closeMe()
        }
    }

    private openDoor( doorId: number ): void {
        let door: L2DoorInstance = DoorManager.getDoor( doorId )
        if ( door && !door.isOpen() ) {
            door.openMe()
        }
    }
}