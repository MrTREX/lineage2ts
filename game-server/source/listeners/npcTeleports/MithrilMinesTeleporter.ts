import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2NpcValues } from '../../gameService/values/L2NpcValues'
import { NpcApproachedForTalkEvent, NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import _ from 'lodash'

const npcId: number = 32652
const locations: Array<Location> = [
    new Location( 171946, -173352, 3440 ),
    new Location( 175499, -181586, -904 ),
    new Location( 173462, -174011, 3480 ),
    new Location( 179299, -182831, -224 ),
    new Location( 178591, -184615, -360 ),
    new Location( 175499, -181586, -904 ),
]

export class MithrilMinesTeleporter extends ListenerLogic {
    constructor() {
        super( 'MithrilMinesTeleporter', 'listeners/npcTeleports/MithrilMinesTeleporter.ts' )
    }

    getApproachedForTalkIds(): Array<number> {
        return [ npcId ]
    }

    getQuestStartIds(): Array<number> {
        return [ npcId ]
    }

    getTalkIds(): Array<number> {
        return [ npcId ]
    }

    async onApproachedForTalk( data : NpcApproachedForTalkEvent ): Promise<string> {
        let npc : L2Npc = L2World.getObjectById( data.characterId ) as L2Npc

        if ( npc.isInsideRadiusCoordinates( 173147, -173762, 0, L2NpcValues.interactionDistance, false ) ) {
            return this.getPath( '32652-01.htm' )
        }

        if ( npc.isInsideRadiusCoordinates( 181941, -174614, 0, L2NpcValues.interactionDistance, false ) ) {
            return this.getPath( '32652-02.htm' )
        }

        if ( npc.isInsideRadiusCoordinates( 179560, -182956, 0, L2NpcValues.interactionDistance, false ) ) {
            return this.getPath( '32652-03.htm' )
        }

        return super.onApproachedForTalk( data )
    }

    async onNpcEvent( data : NpcGeneralEvent ): Promise<string> {

        let player = L2World.getPlayer( data.playerId )

        let index = _.parseInt( data.eventName ) - 1
        if ( locations[ index ] ) {
            await player.teleportToLocation( locations[ index ], false )
            return
        }

        return
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/Teleports/MithrilMinesTeleporter'
    }
}