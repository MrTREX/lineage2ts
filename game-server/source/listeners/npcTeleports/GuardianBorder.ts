import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import {
    CharacterEnterZoneEvent,
    EventType,
    NpcApproachedForTalkEvent,
    NpcGeneralEvent
} from '../../gameService/models/events/EventType'
import { DataManager } from '../../data/manager'
import { DimensionalRiftManager } from '../../gameService/instancemanager/DimensionalRiftManager'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { PlayerGroupCache } from '../../gameService/cache/PlayerGroupCache'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import _, { DebouncedFunc } from 'lodash'
import { AreaCache } from '../../gameService/cache/AreaCache'

const npcIds : Array<number> = [
    // Dungeon 0
    31865, 31866, 31867, 31868, 31869, 31870, 31871, 31872, 31873,
    // Dungeon 1
    31874, 31875, 31876, 31877, 31878, 31879, 31880, 31881, 31882,
    // Dungeon 2
    31883, 31884, 31885, 31886, 31887, 31888, 31889, 31890, 31891,
    // Dungeon 3
    31892, 31893, 31894, 31895, 31896, 31897, 31898, 31899, 31900,
    // Dungeon 4
    31901, 31902, 31903, 31904, 31905, 31906, 31907, 31908, 31909,
    // Dungeon 5
    31910, 31911, 31912, 31913, 31914, 31915, 31916, 31917, 31918
]

const areaNames : Array<string> = [
    'guardian_border_31',
    'guardian_border_32',
    'guardian_border_33',
    'guardian_border_34',
    'guardian_border_35',
    'guardian_border_36',
    'guardian_border_37',
    'guardian_border_38',
    'guardian_border_39',
    'guardian_border_41',
    'guardian_border_42',
    'guardian_border_43',
    'guardian_border_44',
    'guardian_border_45',
    'guardian_border_46',
    'guardian_border_47',
    'guardian_border_48',
    'guardian_border_49',
    'guardian_border_51',
    'guardian_border_52',
    'guardian_border_53',
    'guardian_border_54',
    'guardian_border_55',
    'guardian_border_56',
    'guardian_border_57',
    'guardian_border_58',
    'guardian_border_59',
    'guardian_border_61',
    'guardian_border_62',
    'guardian_border_63',
    'guardian_border_64',
    'guardian_border_65',
    'guardian_border_66',
    'guardian_border_67',
    'guardian_border_68',
    'guardian_border_69',
    'guardian_border_71',
    'guardian_border_72',
    'guardian_border_73',
    'guardian_border_74',
    'guardian_border_75',
    'guardian_border_76',
    'guardian_border_77',
    'guardian_border_78',
    'guardian_border_79',
    'guardian_border_81',
    'guardian_border_82',
    'guardian_border_83',
    'guardian_border_84',
    'guardian_border_85',
    'guardian_border_86',
    'guardian_border_87',
    'guardian_border_88',
    'guardian_border_89'
]

export class GuardianBorder extends ListenerLogic {

    pendingZoneIds: Set<number> = new Set<number>()
    debounceZoneCheck: DebouncedFunc<() => void>
    areaIds: Array<number>

    constructor() {
        super( 'GuardianBorder', 'listeners/npcTeleports/GuardianBorder.ts' )
        this.debounceZoneCheck = _.debounce( this.runZoneCheck.bind( this ), 10000, {
            trailing: true,
            maxWait: 15000,
        } )
    }

    /*
        Due to territory areas need to be created, we do not know area numeric ids for trigger events.
        Hence, once areas are created these can then be used for area look-ups.
     */
    async load(): Promise<void> {
        this.areaIds = areaNames.map( ( name: string ) => AreaCache.getAreaByName( name ).id )
    }

    getApproachedForTalkIds(): Array<number> {
        return npcIds
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/Teleports/GuardianBorder'
    }

    async onApproachedForTalk( data: NpcApproachedForTalkEvent ): Promise<string> {
        let template = DataManager.getNpcData().getTemplate( data.characterNpcId )
        if ( !template ) {
            return
        }

        let index = template.getParameters()[ 'RoomIndex' ]
        return this.getPath( `tel_dungeon_npc_hi${index}.htm` )
    }

    setQuestState( playerId: number ) : void {
        let state = QuestStateCache.getQuestState( playerId, 'Q00635_IntoTheDimensionalRift', true )
        state.setMemoStateEx( 1, -1 )
    }

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.AreaId,
                eventType: EventType.CharacterEnterArea,
                method: this.onZoneEntered.bind( this ),
                ids: this.areaIds
            }
        ]
    }

    onZoneEntered( data: CharacterEnterZoneEvent ) : void {
        this.pendingZoneIds.add( data.areaId )
        this.debounceZoneCheck()
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        if ( data.eventName.endsWith( '.htm' ) ) {
            return this.getPath( data.eventName )
        }

        let [ type, choice ] = data.eventName.split( ';' )

        if ( type !== '-1000' ) {
            return
        }

        let party = PlayerGroupCache.getParty( data.playerId )
        let player = L2World.getPlayer( data.playerId )

        if ( party && !party.isLeader( player ) ) {
            return this.getPath( 'tel_dungeon_npc_notleader.htm' )
        }

        switch ( choice ) {
            case '1':
                if ( !party ) {
                    await DimensionalRiftManager.teleportToWaitingRoom( player )
                    this.setQuestState( data.playerId )

                    return
                }

                party.getDimensionalRift().manualExitRift( player, L2World.getObjectById( data.characterId ) as L2Npc )
                party.getMembers().forEach( ( playerId: number ) => this.setQuestState( playerId ) )

                return

            case '2':
                if ( !party ) {
                    return this.getPath( 'tel_dungeon_npc_notleader.htm' )
                }

                let state = QuestStateCache.getQuestState( data.playerId, 'Q00635_IntoTheDimensionalRift', true )
                if ( state.getMemoStateEx( 1 ) !== 1 ) {
                    return this.getPath( 'tel_dungeon_npc_nomorechance.htm' )
                }

                state.setMemoStateEx( 1, 0 )
                party.getDimensionalRift().manualTeleport( player, L2World.getObjectById( data.characterId ) as L2Npc )

                return
        }
    }

    runZoneCheck() : Promise<void> {
        let ids = Array.from( this.pendingZoneIds )
        this.pendingZoneIds.clear()

        let playerIds : Array<number> = ids.flatMap( ( areaId: number ) => {
            let area = AreaCache.getAreaById( areaId )
            if ( !area ) {
                return []
            }

            let areaPlayers = L2World.getPlayerIdsByBox( area.getSpatialIndex() )
            return areaPlayers.filter( playerId => !PlayerGroupCache.getParty( playerId ) )
        } )

        playerIds.forEach( ( playerId: number ) => this.setQuestState( playerId ) )

        return DimensionalRiftManager.teleportAllToWaitingRoom( playerIds.map( playerId => L2World.getPlayer( playerId ) ) )
    }
}