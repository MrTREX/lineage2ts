import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import { QuestHelper } from '../helpers/QuestHelper'
import { NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import _ from 'lodash'

const npcIds: Array<number> = [
    32864,
    32865,
    32866,
    32867,
    32868,
    32869,
    32870,
    32891,
]

const WILL_OF_ANTHARAS = 17266
const SEALED_BLOOD_CRYSTAL = 17267
const ANTHARAS_BLOOD_CRYSTAL = 17268

const minimumLevel = 80
const locations: Array<Location> = [
    new Location( 117046, 76798, -2696 ), // Hunter's Village
    new Location( 99218, 110283, -3696 ), // The Center of Dragon Valley
    new Location( 116992, 113716, -3056 ), // Deep inside Dragon Valley(North)
    new Location( 113203, 121063, -3712 ), // Deep inside Dragon Valley (South)
    new Location( 146129, 111232, -3568 ), // Antharas' Lair - Magic Force Field Bridge
    new Location( 148447, 110582, -3944 ), // Deep inside Antharas' Lair
    new Location( 73122, 118351, -3714 ), // Entrance to Dragon Valley
    new Location( 131116, 114333, -3704 ), // Entrance of Antharas' Lair
]

export class SeparatedSoul extends ListenerLogic {
    constructor() {
        super( 'SeparatedSoul', 'listeners/npcTeleports/SeparatedSoul.ts' )
    }

    getApproachedForTalkIds(): Array<number> {
        return npcIds
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/Teleports/SeparatedSoul'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onNpcEvent( data : NpcGeneralEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let value = _.parseInt( data.eventName )

        if ( _.inRange( value, 1, 9 ) ) {
            if ( player.getLevel() >= minimumLevel ) {
                let index = value - 1
                await player.teleportToLocation( _.nth( locations, index ), false )
                return
            }

            return this.getPath( 'no-level.htm' )
        }

        switch ( value ) {
            case 23241:
                if ( QuestHelper.hasQuestItems( player, WILL_OF_ANTHARAS, SEALED_BLOOD_CRYSTAL ) ) {
                    await QuestHelper.takeMultipleItems( player, 1, WILL_OF_ANTHARAS, SEALED_BLOOD_CRYSTAL )
                    await QuestHelper.giveSingleItem( player, ANTHARAS_BLOOD_CRYSTAL, 1 )
                    return
                }

                return this.getPath( 'no-items.htm' )

            case 23242:
                return this.getPath( 'separatedsoul.htm' )

        }

    }
}