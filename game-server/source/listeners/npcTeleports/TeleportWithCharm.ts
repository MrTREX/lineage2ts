import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import { QuestHelper } from '../helpers/QuestHelper'
import { NpcTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'

const WHIRPY = 30540
const TAMIL = 30576

const ORC_GATEKEEPER_CHARM = 1658
const DWARF_GATEKEEPER_TOKEN = 1659

const ORC_TELEPORT = new Location( -80826, 149775, -3043 )
const DWARF_TELEPORT = new Location( -80826, 149775, -3043 )

export class TeleportWithCharm extends ListenerLogic {
    constructor() {
        super( 'TeleportWithCharm', 'listeners/npcTeleports/TeleportWithCharm.ts' )
    }

    getQuestStartIds(): Array<number> {
        return [ WHIRPY, TAMIL ]
    }

    getTalkIds(): Array<number> {
        return [ WHIRPY, TAMIL ]
    }

    async onTalkEvent( data : NpcTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case WHIRPY:
                if ( QuestHelper.hasQuestItems( player, DWARF_GATEKEEPER_TOKEN ) ) {
                    await QuestHelper.takeSingleItem( player, DWARF_GATEKEEPER_TOKEN )
                    await player.teleportToLocation( DWARF_TELEPORT )
                    return
                }

                return this.getPath( '30540-01.htm' )

            case TAMIL:
                if ( QuestHelper.hasQuestItems( player, ORC_GATEKEEPER_CHARM ) ) {
                    await QuestHelper.takeSingleItem( player, ORC_GATEKEEPER_CHARM )
                    await player.teleportToLocation( ORC_TELEPORT )
                    return
                }

                return this.getPath( '30576-01.htm' )
        }
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/Teleports/TeleportWithCharm'
    }
}