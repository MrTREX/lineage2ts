import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { NpcTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'
import _ from 'lodash'
import { AreaCache } from '../../gameService/cache/AreaCache'
import { AreaType } from '../../gameService/models/areas/AreaType'
import { TownArea } from '../../gameService/models/areas/type/Town'

const mainNpcId : number = 32484 // Pathfinder Worker
const npcIds: Array<number> = [
    mainNpcId,
    32658, // Guardian of Eastern Seal
    32659, // Guardian of Western Seal
    32660, // Guardian of Southern Seal
    32661, // Guardian of Northern Seal
    32662, // Guardian of Great Seal
    32663, // Guardian of Tower of Seal
]

const hallLocations: Array<Location> = [
    new Location( -114597, -152501, -6750 ),
    new Location( -114589, -154162, -6750 ),
]

const returnLocations = {
    0: new Location( 43835, -47749, -792 ), // Undefined origin, return to Rune
    7: new Location( -14023, 123677, -3112 ), // Gludio
    8: new Location( 18101, 145936, -3088 ), // Dion
    10: new Location( 80905, 56361, -1552 ), // Oren
    14: new Location( 42772, -48062, -792 ), // Rune
    15: new Location( 108469, 221690, -3592 ), // Heine
    17: new Location( 85991, -142234, -1336 ), // Schuttgart
}

export class DelusionTeleport extends ListenerLogic {
    constructor() {
        super( 'DelusionTeleport', 'listeners/npcTeleports/DelusionTeleport.ts' )
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onTalkEvent( data : NpcTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let npc : L2Npc = L2World.getObjectById( data.characterId ) as L2Npc

        if ( npc.getId() === mainNpcId ) {
            let area = AreaCache.getArea( npc, AreaType.Town ) as TownArea
            let townId = area ? area.properties.townId : 0

            PlayerVariablesManager.set( data.playerId, this.name, townId )
            await player.teleportToLocation( _.sample( hallLocations ), false )
            return
        }

        let townId = _.defaultTo( PlayerVariablesManager.get( data.playerId, this.name ), 0 ) as number

        await player.teleportToLocation( returnLocations[ townId ], true )
        PlayerVariablesManager.remove( data.playerId, this.name )
    }
}