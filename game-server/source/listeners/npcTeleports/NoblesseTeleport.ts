import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestHelper } from '../helpers/QuestHelper'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'

const npcIds: Array<number> = [
    30006,
    30059,
    30080,
    30134,
    30146,
    30177,
    30233,
    30256,
    30320,
    30540,
    30576,
    30836,
    30848,
    30878,
    30899,
    31275,
    31320,
    31964,
    32163,
]

const OLYMPIAD_TOKEN = 13722

export class NoblesseTeleport extends ListenerLogic {
    constructor() {
        super( 'NoblesseTeleport', 'listeners/npcTeleports/NoblesseTeleport.ts' )
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/Teleports/NoblesseTeleport'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        if ( 'teleportWithToken' === data.eventName ) {
            let npc = L2World.getObjectById( data.characterId ) as L2Npc
            let player = L2World.getPlayer( data.playerId )

            if ( QuestHelper.hasQuestItems( player, OLYMPIAD_TOKEN ) ) {
                npc.showChatWindow( player, 3 )
                return
            }

            return this.getPath( 'noble-nopass.htm' )
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let path : string = player.isNoble() ? 'nobleteleporter.htm' : 'nobleteleporter-no.htm'

        return this.getPath( path )
    }
}