import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestHelper } from '../helpers/QuestHelper'
import { NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'

const GHOST_CHAMBERLAIN_OF_ELMOREDEN_1 = 31919
const GHOST_CHAMBERLAIN_OF_ELMOREDEN_2 = 31920
const USED_GRAVE_PASS = 7261
const ANTIQUE_BROOCH = 7262

const FOUR_SEPULCHERS_LOC = new Location( 178127, -84435, -7215 )
const IMPERIAL_TOMB_LOC = new Location( 186699, -75915, -2826 )

export class GhostChamberlainOfElmoreden extends ListenerLogic {
    constructor() {
        super( 'GhostChamberlainOfElmoreden', 'listeners/npcTeleports/GhostChamberlainOfElmoreden.ts' )
    }

    getApproachedForTalkIds(): Array<number> {
        return [ GHOST_CHAMBERLAIN_OF_ELMOREDEN_1, GHOST_CHAMBERLAIN_OF_ELMOREDEN_2 ]
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/Teleports/GhostChamberlainOfElmoreden'
    }

    getQuestStartIds(): Array<number> {
        return [ GHOST_CHAMBERLAIN_OF_ELMOREDEN_1, GHOST_CHAMBERLAIN_OF_ELMOREDEN_2 ]
    }

    getTalkIds(): Array<number> {
        return [ GHOST_CHAMBERLAIN_OF_ELMOREDEN_1, GHOST_CHAMBERLAIN_OF_ELMOREDEN_2 ]
    }

    async onNpcEvent( data : NpcGeneralEvent ): Promise<string> {

        let event = data.eventName
        let player = L2World.getPlayer( data.playerId )

        if ( event === 'FOUR_SEPULCHERS' ) {
            if ( QuestHelper.hasQuestItems( player, USED_GRAVE_PASS ) ) {
                await QuestHelper.takeSingleItem( player, USED_GRAVE_PASS, 1 )
                await player.teleportToLocation( FOUR_SEPULCHERS_LOC )
                return
            }

            if ( QuestHelper.hasQuestItems( player, ANTIQUE_BROOCH ) ) {
                await player.teleportToLocation( FOUR_SEPULCHERS_LOC )
                return
            }

            return this.getPath( `${ data.characterNpcId }-01.html` )
        }

        if ( event === 'IMPERIAL_TOMB' ) {
            if ( QuestHelper.hasQuestItems( player, USED_GRAVE_PASS ) ) {
                await QuestHelper.takeSingleItem( player, USED_GRAVE_PASS, 1 )
                await player.teleportToLocation( IMPERIAL_TOMB_LOC )
                return
            }

            if ( QuestHelper.hasQuestItems( player, ANTIQUE_BROOCH ) ) {
                await player.teleportToLocation( IMPERIAL_TOMB_LOC )
                return
            }

            return this.getPath( `${ data.characterNpcId }-01.html` )
        }
    }
}