import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import {
    EventType,
    NpcGeneralEvent,
    NpcTalkEvent,
    OlympiadMatchResultEvent,
} from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestType } from '../../gameService/enums/QuestType'
import { QuestStateValues } from '../../gameService/models/quest/State'
import aigle from 'aigle'

const MANAGER = 31688
const CERT_3 = 17238
const CERT_5 = 17239
const CERT_10 = 17240
const OLY_CHEST = 17169
const MEDAL_OF_GLORY = 21874
const minimumLevel = 75

const variableNames = {
    value: 'vl'
}

export class OlympiadStarter extends ListenerLogic {
    constructor() {
        super( 'Q00551_OlympiadStarter', 'listeners/tracked-500/OlympiadStarter.ts' )
        this.questId = 551
        this.questItemIds = [
            CERT_3,
            CERT_5,
            CERT_10,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ MANAGER ]
    }

    getTalkIds(): Array<number> {
        return [ MANAGER ]
    }

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.General,
                eventType: EventType.OlympiadMatchResult,
                method: this.processOlympiadMatchResult.bind( this ),
                ids: null,
            },
        ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31688-03.html':
                state.startQuest()
                state.setMemoState( 1 )

                break

            case '31688-04.html':
                let certThreeAmount: number = QuestHelper.getQuestItemsCount( player, CERT_3 )
                let certFiveAmount: number = QuestHelper.getQuestItemsCount( player, CERT_5 )

                if ( certThreeAmount > 0 || certFiveAmount > 0 ) {
                    if ( certThreeAmount > 0 ) {
                        await QuestHelper.takeSingleItem( player, CERT_3, -1 )
                        await QuestHelper.rewardSingleItem( player, OLY_CHEST, 1 )
                    }

                    if ( certFiveAmount > 0 ) {
                        await QuestHelper.takeSingleItem( player, CERT_5, -1 )
                        await QuestHelper.rewardSingleItem( player, OLY_CHEST, 1 )
                        await QuestHelper.rewardSingleItem( player, MEDAL_OF_GLORY, 3 )
                    }

                    await state.exitQuestWithType( QuestType.DAILY, true )
                }

                break
        }

        return this.getPath( data.eventName )
    }

    async processOlympiadMatchResult( data: OlympiadMatchResultEvent ): Promise<void> {
        await aigle.resolve( data.playerIds ).each( ( id : number ) => this.processEachOlympiadPlayer( data, id ) )
    }

    async processEachOlympiadPlayer( data: OlympiadMatchResultEvent, playerId: number ): Promise<void> {
        let state: QuestState = QuestStateCache.getQuestState( playerId, this.getName(), false )

        if ( !state || !state.isStarted() || !state.isMemoState( 1 ) ) {
            return
        }

        let value : number = state.getVariable( variableNames.value ) || 0
        let player = L2World.getPlayer( playerId )

        if ( value < 9 ) {
            if ( value === 2 ) {
                await QuestHelper.giveSingleItem( player, CERT_3, 1 )
            } else if ( value === 4 ) {
                await QuestHelper.giveSingleItem( player, CERT_5, 1 )
            }

            state.setVariable( variableNames.value, value + 1 )
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

            return
        }

        if ( value !== 9 ) {
            return
        }

        state.setVariable( variableNames.value, value + 1 )
        state.setMemoState( 2 )
        state.setConditionWithSound( 2, true )

        await QuestHelper.giveSingleItem( player, CERT_10, 1 )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )

        if ( player.getLevel() < minimumLevel || !player.isNoble() ) {
            return this.getPath( '31688-00.htm' )
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( '31688-01.htm' )

            case QuestStateValues.STARTED:
                if ( state.isMemoState( 1 ) ) {
                    return this.getPath( QuestHelper.getItemsSumCount( player, CERT_3, CERT_5, CERT_10 ) > 0 ? '31688-07.html' : '31688-06.html' )
                }

                if ( state.isMemoState( 2 ) ) {
                    await QuestHelper.rewardSingleItem( player, OLY_CHEST, 4 )
                    await QuestHelper.rewardSingleItem( player, MEDAL_OF_GLORY, 5 )
                    await state.exitQuestWithType( QuestType.DAILY, true )

                    return this.getPath( '31688-04.html' )
                }

                break

            case QuestStateValues.COMPLETED:
                if ( state.isNowAvailable() ) {
                    state.setState( QuestStateValues.CREATED )

                    return this.getPath( '31688-01.htm' )
                }

                return this.getPath( '31688-05.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00551_OlympiadStarter'
    }
}