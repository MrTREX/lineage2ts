import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import {
    EventType,
    NpcGeneralEvent,
    NpcTalkEvent,
    OlympiadMatchResultEvent,
} from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { CompetitionType } from '../../gameService/models/olympiad/CompetitionType'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestType } from '../../gameService/enums/QuestType'
import { QuestStateValues } from '../../gameService/models/quest/State'
import aigle from 'aigle'

const MANAGER = 31688
const TEAM_EVENT_CERTIFICATE = 17241
const CLASS_FREE_BATTLE_CERTIFICATE = 17242
const CLASS_BATTLE_CERTIFICATE = 17243
const OLY_CHEST = 17169
const minimumLevel = 75

const typeItemMapping: { [ type: number ]: number } = {
    [ CompetitionType.CLASSED ]: CLASS_BATTLE_CERTIFICATE,
    [ CompetitionType.NON_CLASSED ]: CLASS_FREE_BATTLE_CERTIFICATE,
    [ CompetitionType.TEAMS ]: TEAM_EVENT_CERTIFICATE,
}

export class OlympiadVeteran extends ListenerLogic {
    constructor() {
        super( 'Q00552_OlympiadVeteran', 'listeners/tracked-500/OlympiadVeteran.ts' )
        this.questId = 552
        this.questItemIds = [
            TEAM_EVENT_CERTIFICATE,
            CLASS_FREE_BATTLE_CERTIFICATE,
            CLASS_BATTLE_CERTIFICATE,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ MANAGER ]
    }

    getTalkIds(): Array<number> {
        return [ MANAGER ]
    }

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.General,
                eventType: EventType.OlympiadMatchResult,
                method: this.processOlympiadMatchResult.bind( this ),
                ids: null,
            },
        ]
    }

    async processOlympiadMatchResult( data: OlympiadMatchResultEvent ): Promise<void> {
        await aigle.resolve( data.playerIds ).each( ( id : number ) => this.processEachOlympiadPlayer( data, id ) )
    }

    async processEachOlympiadPlayer( data: OlympiadMatchResultEvent, playerId: number ): Promise<void> {
        let state: QuestState = QuestStateCache.getQuestState( playerId, this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        let itemId: number = typeItemMapping[ data.type ]
        let variableName = CompetitionType[ data.type ]

        if ( !variableName || !itemId ) {
            return
        }

        let matches = state.getVariable( variableName ) || 0
        state.setVariable( variableName, matches + 1 )

        let player = L2World.getPlayer( playerId )
        let isWinner = playerId === data.winnerPlayerId

        if ( matches === 5
                && ( !isWinner || !QuestHelper.hasQuestItem( player, itemId ) ) ) {
            await QuestHelper.giveSingleItem( player, itemId, 1 )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '31688-03.html':
                state.startQuest()
                break

            case '31688-04.html':
                let player = L2World.getPlayer( data.playerId )
                let amount: number = QuestHelper.getItemsSumCount( player, TEAM_EVENT_CERTIFICATE, CLASS_FREE_BATTLE_CERTIFICATE, CLASS_BATTLE_CERTIFICATE )

                if ( amount > 0 ) {
                    await QuestHelper.rewardSingleItem( player, OLY_CHEST, amount )
                    await state.exitQuestWithType( QuestType.DAILY, true )

                    break
                }

                return QuestHelper.getNoQuestMessagePath()
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )

        if ( player.getLevel() < minimumLevel || !player.isNoble() ) {
            return this.getPath( '31688-00.htm' )
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( '31688-01.htm' )

            case QuestStateValues.STARTED:
                let amount: number = QuestHelper.getItemsSumCount( player, TEAM_EVENT_CERTIFICATE, CLASS_FREE_BATTLE_CERTIFICATE, CLASS_BATTLE_CERTIFICATE )

                if ( amount === 3 ) {
                    await QuestHelper.rewardSingleItem( player, OLY_CHEST, 4 )
                    await state.exitQuestWithType( QuestType.DAILY, true )

                    return this.getPath( '31688-04.html' )
                }

                return this.getPath( `31688-s${ amount }.html` )

            case QuestStateValues.COMPLETED:
                if ( state.isNowAvailable() ) {
                    state.setState( QuestStateValues.CREATED )

                    return this.getPath( '31688-01.htm' )
                }

                return this.getPath( '31688-05.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00552_OlympiadVeteran'
    }
}