import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestHelper } from '../helpers/QuestHelper'
import { PlayerRadarCache } from '../../gameService/cache/PlayerRadarCache'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { L2Clan } from '../../gameService/models/L2Clan'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { PledgeShowInfoUpdate } from '../../gameService/packets/send/PledgeShowInfoUpdate'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'

const SIR_ERIC_RODEMAI = 30868

type RaidRewardData = [ number, number, number ] // npcId, itemId, clanReputation amount
const raidRewards : { [ raidId : number ] : RaidRewardData } = {
    1: [ 25252, 8277, 560 ], // Palibati Queen Themis
    2: [ 25478, 14883, 584 ], // Shilen's Priest Hisilrome
    3: [ 25255, 8280, 602 ], // Gargoyle Lord Tiphon
    4: [ 25245, 8281, 784 ], // Last Lesser Giant Glaki
    5: [ 25051, 8282, 558 ], // Rahha
    6: [ 25524, 8494, 768 ], // Flamestone Giant
}

const variableNames = {
    raidId: 'ri',
}

export class AClansReputation extends ListenerLogic {
    constructor() {
        super( 'Q00508_AClansReputation', 'listeners/tracked-500/AClansReputation.ts' )
        this.questId = 508
    }

    getAttackableKillIds(): Array<number> {
        return [
            25252,
            25478,
            25255,
            25245,
            25051,
            25524,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ SIR_ERIC_RODEMAI ]
    }

    getTalkIds(): Array<number> {
        return [ SIR_ERIC_RODEMAI ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return QuestHelper.getNoQuestMessagePath()
        }

        switch ( data.eventName ) {
            case '30868-0.html':
                state.startQuest()
                break

            case '30868-1.html':
                state.setVariable( variableNames.raidId, 1 )
                PlayerRadarCache.getRadar( data.playerId ).addNpcMarker( 192376, 22087, -3608 )
                break

            case '30868-2.html':
                state.setVariable( variableNames.raidId, 2 )
                PlayerRadarCache.getRadar( data.playerId ).addNpcMarker( 168288, 28368, -3632 )
                break

            case '30868-3.html':
                state.setVariable( variableNames.raidId, 3 )
                PlayerRadarCache.getRadar( data.playerId ).addNpcMarker( 170048, -24896, -3440 )
                break

            case '30868-4.html':
                state.setVariable( variableNames.raidId, 4 )
                PlayerRadarCache.getRadar( data.playerId ).addNpcMarker( 188809, 47780, -5968 )
                break

            case '30868-5.html':
                state.setVariable( variableNames.raidId, 5 )
                PlayerRadarCache.getRadar( data.playerId ).addNpcMarker( 117760, -9072, -3264 )
                break

            case '30868-6.html':
                state.setVariable( variableNames.raidId, 6 )
                PlayerRadarCache.getRadar( data.playerId ).addNpcMarker( 144600, -5500, -4100 )
                break

            case '30868-7.html':
                await state.exitQuest( true, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )

        let clan: L2Clan = player.getClan()
        if ( !clan ) {
            return
        }

        let state: QuestState = QuestStateCache.getQuestState( clan.getLeaderId(), this.getName(), false )
        if ( !state || !state.isStarted() ) {
            return
        }

        let npc = L2World.getObjectById( data.targetId ) as L2Npc
        let leader: L2PcInstance = clan.getLeader().getPlayerInstance()

        if ( !GeneralHelper.checkIfInRange( 1500, leader, npc, true ) ) {
            return
        }

        let raidData: RaidRewardData = raidRewards[ state.getVariable( variableNames.raidId ) ]
        if ( !raidData ) {
            return
        }

        let [ npcId, itemId ] = raidData

        if ( npcId !== data.npcId || QuestHelper.hasQuestItem( leader, itemId ) ) {
            return
        }

        await QuestHelper.giveSingleItem( leader, itemId, 1 )
        leader.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )
        let clan: L2Clan = player.getClan()

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                let isNotValid: boolean = !clan || !player.isClanLeader() || clan.getLevel() < 5
                return this.getPath( isNotValid ? '30868-0a.htm' : '30868-0b.htm' )

            case QuestStateValues.STARTED:
                if ( !clan || !player.isClanLeader() ) {
                    await state.exitQuest( true )

                    return this.getPath( '30868-8.html' )
                }

                let raidId = state.getVariable( variableNames.raidId )
                let raidData: RaidRewardData = raidRewards[ raidId ]
                if ( !raidData ) {
                    return this.getPath( '30868-0.html' )
                }

                let [ , itemId, points ] = raidData

                if ( !QuestHelper.hasQuestItem( player, itemId ) ) {
                    return this.getPath( `30868-${ raidId }a.html` )
                }

                await QuestHelper.takeSingleItem( player, itemId, -1 )
                await clan.addReputationScore( points, true )

                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_FANFARE_1 )

                let message: Buffer = new SystemMessageBuilder( SystemMessageIds.CLAN_QUEST_COMPLETED_AND_S1_POINTS_GAINED )
                        .addNumber( points )
                        .getBuffer()
                player.sendOwnedData( message )
                clan.broadcastDataToOnlineMembers( PledgeShowInfoUpdate( clan ) )

                return this.getPath( `30868-${ raidId }b.html` )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00508_AClansReputation'
    }
}