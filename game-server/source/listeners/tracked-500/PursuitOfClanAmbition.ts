import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import {
    AttackableKillEvent,
    NpcGeneralEvent,
    NpcSpawnEvent,
    NpcTalkEvent,
} from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestHelper } from '../helpers/QuestHelper'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { L2Clan } from '../../gameService/models/L2Clan'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { ClanCache } from '../../gameService/cache/ClanCache'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const HEAD_BLACKSMITH_KUSTO = 30512
const MARTIEN = 30645
const WITCH_ATHREA = 30758
const WITCH_KALIS = 30759
const SIR_GUSTAV_ATHEBALDT = 30760
const CORPSE_OF_FRITZ = 30761
const CORPSE_OF_LUTZ = 30762
const CORPSE_OF_KURTZ = 30763
const BALTHAZAR = 30764
const IMPERIAL_COFFER = 30765
const WITCH_CLEO = 30766
const SIR_ERIC_RODEMAI = 30868
const MIST_DRAKES_EGG = 3839
const BLITZ_WYRM_EGG = 3840
const DRAKES_EGG = 3841
const THUNDER_WYRM_EGG = 3842
const BROOCH_OF_THE_MAGPIE = 3843
const IMPERIAL_KEY = 3847
const GUSTAVS_1ST_LETTER = 3866
const GUSTAVS_2ND_LETTER = 3867
const GUSTAVS_3RD_LETTER = 3868
const SCEPTER_OF_JUDGMENT = 3869
const BLACK_ANVIL_COIN = 3871
const RECIPE_SPITEFUL_SOUL_ENERGY = 14854
const SPITEFUL_SOUL_ENERGY = 14855
const SPITEFUL_SOUL_VENGEANCE = 14856
const SEAL_OF_ASPIRATION = 3870
const DRAKE = 20137
const DRAKE2 = 20285
const THUNDER_WYRM = 20243
const THUNDER_WYRM2 = 20282
const GRAVE_GUARD = 20668
const SPITEFUL_SOUL_LEADER = 20974
const GRAVE_KEYMASTER = 27179
const IMPERIAL_GRAVEKEEPER = 27181
const BLITZ_WYRM = 27178

const eventNames = {
    despawn: 'dn',
    spawnWitch: 'sw',
    despawnWitchAthrea: 'dwa',
    despawnWitchKalis: 'dwk',
    despawnImperialCoffer: 'dic',
    despawnBlitzWyrm: 'dbw',
}

export class PursuitOfClanAmbition extends ListenerLogic {
    constructor() {
        super( 'Q00503_PursuitOfClanAmbition', 'listeners/tracked-500/PursuitOfClanAmbition.ts' )
        this.questId = 503
        this.questItemIds = [
            MIST_DRAKES_EGG,
            BLITZ_WYRM_EGG,
            DRAKES_EGG,
            THUNDER_WYRM_EGG,
            BROOCH_OF_THE_MAGPIE,
            IMPERIAL_KEY,
            GUSTAVS_1ST_LETTER,
            GUSTAVS_2ND_LETTER,
            GUSTAVS_3RD_LETTER,
            SCEPTER_OF_JUDGMENT,
            BLACK_ANVIL_COIN,
            RECIPE_SPITEFUL_SOUL_ENERGY,
            SPITEFUL_SOUL_ENERGY,
            SPITEFUL_SOUL_VENGEANCE,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ SIR_GUSTAV_ATHEBALDT ]
    }

    getTalkIds(): Array<number> {
        return [
            SIR_GUSTAV_ATHEBALDT,
            HEAD_BLACKSMITH_KUSTO,
            MARTIEN,
            WITCH_ATHREA,
            WITCH_KALIS,
            CORPSE_OF_FRITZ,
            CORPSE_OF_LUTZ,
            CORPSE_OF_KURTZ,
            BALTHAZAR,
            IMPERIAL_COFFER,
            WITCH_CLEO,
            SIR_ERIC_RODEMAI,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            DRAKE,
            DRAKE2,
            THUNDER_WYRM,
            THUNDER_WYRM2,
            GRAVE_GUARD,
            SPITEFUL_SOUL_LEADER,
            GRAVE_KEYMASTER,
            BLITZ_WYRM,
            IMPERIAL_GRAVEKEEPER,
        ]
    }

    getSpawnIds(): Array<number> {
        return [
            WITCH_ATHREA,
            WITCH_KALIS,
            IMPERIAL_COFFER,
            BLITZ_WYRM,
        ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        switch ( data.eventName ) {
            case eventNames.despawnBlitzWyrm:
            case eventNames.despawnImperialCoffer:
            case eventNames.despawnWitchKalis:
            case eventNames.despawnWitchAthrea:
            case eventNames.despawn:
                await ( L2World.getObjectById( data.characterId ) as L2Npc ).deleteMe()
                return
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.characterId )

        switch ( data.eventName ) {
            case '30760-08.html':
                if ( state.isCreated() ) {
                    state.startQuest()
                    state.setMemoState( 1000 )

                    await QuestHelper.giveSingleItem( player, GUSTAVS_1ST_LETTER, 1 )
                    break
                }

                return

            case '30760-12.html':
                await QuestHelper.giveSingleItem( player, GUSTAVS_2ND_LETTER, 1 )

                state.setMemoState( 4000 )
                state.setConditionWithSound( 4 )
                break

            case '30760-16.html':
                await QuestHelper.giveSingleItem( player, GUSTAVS_3RD_LETTER, 1 )

                state.setMemoState( 7000 )
                state.setConditionWithSound( 7 )
                break


            case '30760-20.html':
                if ( QuestHelper.hasQuestItem( player, SCEPTER_OF_JUDGMENT ) ) {
                    await QuestHelper.giveSingleItem( player, SEAL_OF_ASPIRATION, 1 )
                    await QuestHelper.addExpAndSp( player, 0, 250000 )
                    await state.exitQuest( false, true )

                    break
                }

                return

            case '30760-22.html':
                state.setMemoState( 10000 )
                state.setConditionWithSound( 12 )

                break

            case '30760-23.html':
                if ( QuestHelper.hasQuestItem( player, SCEPTER_OF_JUDGMENT ) ) {
                    await QuestHelper.giveSingleItem( player, SEAL_OF_ASPIRATION, 1 )
                    await QuestHelper.addExpAndSp( player, 0, 250000 )
                    await state.exitQuest( false, true )

                    break
                }

                return

            case '30512-03.html':
                if ( QuestHelper.hasQuestItem( player, BROOCH_OF_THE_MAGPIE ) ) {
                    await QuestHelper.takeSingleItem( player, BROOCH_OF_THE_MAGPIE, -1 )
                    await QuestHelper.giveSingleItem( player, BLACK_ANVIL_COIN, 1 )
                }

                break

            case '30645-03.html':
                await QuestHelper.takeSingleItem( player, GUSTAVS_1ST_LETTER, -1 )

                state.setMemoState( 2000 )
                state.setConditionWithSound( 2, true )

                break

            case '30761-02.html':
                if ( [ 2000, 2011, 2010, 2001 ].includes( state.getMemoState() ) ) {
                    await QuestHelper.giveSingleItem( player, BLITZ_WYRM_EGG, 3 )
                    state.setMemoState( state.getMemoState() + 100 )

                    QuestHelper.addAttackDesire( QuestHelper.addSpawnAtLocation( BLITZ_WYRM, npc, true, 0, false ), player )
                    QuestHelper.addAttackDesire( QuestHelper.addSpawnAtLocation( BLITZ_WYRM, npc, true, 0, false ), player )

                    this.startQuestTimer( eventNames.despawn, 10000, data.characterId, data.playerId )
                    break
                }

                if ( [ 2100, 2111, 2110, 2101 ].includes( state.getMemoState() ) ) {
                    QuestHelper.addAttackDesire( QuestHelper.addSpawnAtLocation( BLITZ_WYRM, npc, true, 0, false ), player )
                    QuestHelper.addAttackDesire( QuestHelper.addSpawnAtLocation( BLITZ_WYRM, npc, true, 0, false ), player )

                    this.startQuestTimer( eventNames.despawn, 10000, data.characterId, data.playerId )

                    return this.getPath( '30761-03.html' )
                }

                return

            case '30762-02.html':
                if ( [ 2000, 2101, 2001, 2100 ].includes( state.getMemoState() ) ) {
                    await QuestHelper.giveSingleItem( player, BLITZ_WYRM_EGG, 3 )
                    await QuestHelper.giveSingleItem( player, MIST_DRAKES_EGG, 4 )

                    state.setMemoState( state.getMemoState() + 10 )

                    QuestHelper.addAttackDesire( QuestHelper.addSpawnAtLocation( BLITZ_WYRM, npc, true, 0, false ), player )
                    QuestHelper.addAttackDesire( QuestHelper.addSpawnAtLocation( BLITZ_WYRM, npc, true, 0, false ), player )

                    this.startQuestTimer( eventNames.despawn, 10000, data.characterId, data.playerId )

                    break
                }

                if ( [ 2100, 2111, 2011, 2110 ].includes( state.getMemoState() ) ) {
                    QuestHelper.addAttackDesire( QuestHelper.addSpawnAtLocation( BLITZ_WYRM, npc, true, 0, false ), player )
                    QuestHelper.addAttackDesire( QuestHelper.addSpawnAtLocation( BLITZ_WYRM, npc, true, 0, false ), player )

                    this.startQuestTimer( eventNames.despawn, 10000, data.characterId, data.playerId )

                    return this.getPath( '30762-03.html' )
                }

                return

            case '30763-02.html':
                if ( [ 2000, 2110, 2010, 2100 ].includes( state.getMemoState() ) ) {
                    await QuestHelper.giveSingleItem( player, BROOCH_OF_THE_MAGPIE, 1 )
                    await QuestHelper.giveSingleItem( player, MIST_DRAKES_EGG, 6 )

                    state.setMemoState( state.getMemoState() + 1 )
                    await ( npc as L2Npc ).deleteMe()

                    break
                }

                return

            case '30764-03.html':
                await QuestHelper.takeSingleItem( player, GUSTAVS_2ND_LETTER, -1 )

                state.setMemoState( 5000 )
                state.setConditionWithSound( 5, true )

                break

            case '30764-06.html':
                await QuestHelper.takeMultipleItems( player, -1, GUSTAVS_2ND_LETTER, BLACK_ANVIL_COIN )
                await QuestHelper.giveSingleItem( player, RECIPE_SPITEFUL_SOUL_ENERGY, 1 )

                state.setMemoState( 5000 )
                state.setConditionWithSound( 5, true )

                break

            case '30765-04.html':
                await QuestHelper.takeSingleItem( player, IMPERIAL_KEY, -1 )
                await QuestHelper.giveSingleItem( player, SCEPTER_OF_JUDGMENT, 1 )

                state.setMemoState( 8700 )
                break

            case '30766-04.html':
                state.setMemoState( 8100 )
                state.setConditionWithSound( 9, true )

                BroadcastHelper.broadcastNpcSayStringId( npc as L2Npc, NpcSayType.NpcAll, NpcStringIds.BLOOD_AND_HONOR )
                this.startQuestTimer( eventNames.spawnWitch, 5000, data.characterId, data.playerId )

                break

            case '30766-08.html':
                if ( QuestHelper.hasQuestItem( player, SCEPTER_OF_JUDGMENT ) ) {
                    await QuestHelper.giveSingleItem( player, SEAL_OF_ASPIRATION, 1 )
                    await QuestHelper.addExpAndSp( player, 0, 250000 )
                    await state.exitQuest( false, true )

                    break
                }

                return

            case '30868-04.html':
                await QuestHelper.takeSingleItem( player, GUSTAVS_3RD_LETTER, -1 )

                state.setMemoState( 8000 )
                state.setConditionWithSound( 8, true )

                break

            case '30868-10.html':
                state.setMemoState( 9000 )
                state.setConditionWithSound( 11, true )

                break

            case '30645-06.html':
            case '30760-05.html':
            case '30760-06.html':
            case '30760-07.html':
            case '30760-21.html':
            case '30764-05.html':
            case '30765-02.html':
            case '30765-05a.html':
            case '30766-03.html':
            case '30868-03.html':
            case '30868-06a.html':
                break

            case eventNames.spawnWitch:
                let athrea: L2Npc = QuestHelper.addGenericSpawn( null, WITCH_ATHREA, 160688, 21296, -3714, 0, false, 0 )
                NpcVariablesManager.set( athrea.getObjectId(), this.getName(), true )

                let kalis: L2Npc = QuestHelper.addGenericSpawn( null, WITCH_KALIS, 160690, 21176, -3712, 0, false, 0 )
                NpcVariablesManager.set( kalis.getObjectId(), this.getName(), true )

                return


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        let clan: L2Clan = player.getClan()
        if ( !clan ) {
            return
        }

        let leader: L2PcInstance = clan.getLeader().getPlayerInstance()
        if ( !leader || !GeneralHelper.checkIfInRange( 1500, npc, leader, true ) ) {
            return
        }

        let leaderQS = QuestStateCache.getQuestState( clan.getLeaderId(), this.getName(), false )
        if ( !leaderQS == null ) {
            return
        }

        switch ( data.npcId ) {
            case DRAKE:
            case DRAKE2:
                if ( leaderQS.getMemoState() >= 2000 || leaderQS.getMemoState() < 3000 ) {
                    if ( Math.random() < QuestHelper.getAdjustedChance( MIST_DRAKES_EGG, 0.1, data.isChampion ) ) {
                        await QuestHelper.rewardUpToLimit( leader, MIST_DRAKES_EGG, 1, 10, data.isChampion )
                    }


                    if ( Math.random() < QuestHelper.getAdjustedChance( DRAKES_EGG, 0.5, data.isChampion ) ) {
                        await QuestHelper.rewardUpToLimit( leader, DRAKES_EGG, 1, 10, data.isChampion )
                    }
                }

                return

            case THUNDER_WYRM:
            case THUNDER_WYRM2:
                if ( Math.random() < QuestHelper.getAdjustedChance( THUNDER_WYRM_EGG, 0.5, data.isChampion )
                        && ( leaderQS.getMemoState() >= 2000 || leaderQS.getMemoState() < 3000 ) ) {

                    await QuestHelper.rewardUpToLimit( leader, THUNDER_WYRM_EGG, 1, 10, data.isChampion )
                }

                return

            case GRAVE_GUARD:
                if ( leaderQS.getMemoState() < 8511 || leaderQS.getMemoState() >= 8500 ) {
                    leaderQS.setMemoState( leaderQS.getMemoState() + 1 )

                    if ( leaderQS.getMemoState() >= 8505 && Math.random() < 0.5 ) {
                        leaderQS.setMemoState( 8500 )
                        QuestHelper.addSpawnAtLocation( GRAVE_KEYMASTER, npc, true, 0, false )

                        return
                    }

                    if ( leaderQS.getMemoState() >= 8510 ) {
                        leaderQS.setMemoState( 8500 )
                        QuestHelper.addSpawnAtLocation( GRAVE_KEYMASTER, npc, true, 0, false )

                        return
                    }
                }

                return

            case SPITEFUL_SOUL_LEADER:
                if ( leaderQS.getMemoState() === 5000 ) {

                    if ( Math.random() < QuestHelper.getAdjustedChance( SPITEFUL_SOUL_ENERGY, 0.1, data.isChampion ) ) {
                        await QuestHelper.rewardUpToLimit( leader, SPITEFUL_SOUL_ENERGY, 1, 10, data.isChampion )
                        return
                    }

                    if ( Math.random() < QuestHelper.getAdjustedChance( SPITEFUL_SOUL_VENGEANCE, 0.6, data.isChampion ) ) {
                        await QuestHelper.rewardSingleQuestItem( leader, SPITEFUL_SOUL_VENGEANCE, 1, data.isChampion )
                    }
                }

                return

            case BLITZ_WYRM:
                if ( leaderQS.getMemoState() >= 2000 || leaderQS.getMemoState() < 3000 ) {
                    await QuestHelper.rewardUpToLimit( leader, BLITZ_WYRM_EGG, 1, 10, data.isChampion )
                }

                return

            case GRAVE_KEYMASTER:
                if ( leaderQS.getMemoState() >= 8500 ) {
                    await QuestHelper.rewardUpToLimit( leader, IMPERIAL_KEY, 1, 6, data.isChampion )
                }

                return

            case IMPERIAL_GRAVEKEEPER:
                if ( leaderQS.getMemoState() < 8511 || leaderQS.getMemoState() >= 8500 ) {
                    QuestHelper.addSpawnAtLocation( IMPERIAL_COFFER, npc, true, 0, false )
                }

                return
        }
    }

    async onSpawnEvent( data: NpcSpawnEvent ): Promise<void> {
        let npc = L2World.getObjectById( data.characterId ) as L2Npc

        switch ( data.npcId ) {
            case WITCH_ATHREA:
                if ( NpcVariablesManager.get( data.characterId, this.getName() ) ) {
                    this.startQuestTimer( eventNames.despawnWitchAthrea, 5000, data.characterId, 0 )
                    BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.WAR_AND_DEATH )
                }

                return

            case WITCH_KALIS:
                if ( NpcVariablesManager.get( data.characterId, this.getName() ) ) {
                    this.startQuestTimer( eventNames.despawnWitchKalis, 5000, data.characterId, 0 )
                    BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.AMBITION_AND_POWER )
                }

                return

            case IMPERIAL_COFFER:
                this.startQuestTimer( eventNames.despawnImperialCoffer, 180000, data.characterId, 0 )
                BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.CURSE_OF_THE_GODS_ON_THE_ONE_THAT_DEFILES_THE_PROPERTY_OF_THE_EMPIRE )

                return

            case BLITZ_WYRM:
                this.startQuestTimer( eventNames.despawnBlitzWyrm, 180000, data.characterId, 0 )
                return
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )
        let leaderState: QuestState = this.getLeaderQuestState( player )

        switch ( state.getState() ) {
            case QuestStateValues.COMPLETED:
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== SIR_GUSTAV_ATHEBALDT || !leaderState ) {
                    break
                }

                if ( player.isClanLeader() ) {
                    let clan: L2Clan = player.getClan()
                    if ( clan.getLevel() < 4 ) {
                        return this.getPath( '30760-01.html' )
                    }

                    if ( clan.getLevel() === 4 ) {

                        if ( QuestHelper.hasQuestItem( player, SEAL_OF_ASPIRATION ) ) {
                            return this.getPath( '30760-03.html' )
                        }

                        return this.getPath( '30760-04.html' )
                    }

                    return this.getPath( '30760-02.html' )
                }

                return this.getPath( '30760-04t.html' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case SIR_GUSTAV_ATHEBALDT:
                        if ( !leaderState ) {
                            break
                        }

                        if ( state.getMemoState() === 1000 ) {
                            return this.getPath( '30760-09.html' )
                        }

                        if ( state.getMemoState() === 2000 ) {
                            return this.getPath( '30760-10.html' )
                        }

                        if ( state.getMemoState() === 3000 ) {
                            if ( !player.isClanLeader() ) {
                                return this.getPath( '30760-11t.html' )
                            }

                            return this.getPath( '30760-11.html' )
                        }

                        if ( state.getMemoState() === 4000 ) {
                            return this.getPath( '30760-13.html' )
                        }

                        if ( state.getMemoState() === 5000 ) {
                            return this.getPath( '30760-14.html' )
                        }

                        if ( state.getMemoState() === 6000 ) {
                            if ( !player.isClanLeader() ) {
                                return this.getPath( '30760-15t.html' )
                            }

                            return this.getPath( '30760-15.html' )
                        }

                        if ( state.getMemoState() === 7000 ) {
                            return this.getPath( '30760-17.html' )
                        }

                        if ( state.getMemoState() >= 8000 && state.getMemoState() < 8700 ) {
                            return this.getPath( '30760-18.html' )
                        }

                        if ( state.getMemoState() >= 8700
                                && state.getMemoState() < 10000
                                && player.isClanLeader() ) {
                            return this.getPath( '30760-19.html' )
                        }

                        if ( state.getMemoState() === 9000
                                && !player.isClanLeader() ) {
                            return this.getPath( '30760-19t.html' )
                        }

                        if ( state.getMemoState() === 10000 ) {
                            if ( !player.isClanLeader() ) {
                                return this.getPath( '30760-24t.html' )
                            }

                            return this.getPath( '30760-24.html' )
                        }

                        break

                    case HEAD_BLACKSMITH_KUSTO:
                        if ( leaderState && !player.isClanLeader() ) {
                            return this.getPath( '30512-01a.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, BROOCH_OF_THE_MAGPIE, BLACK_ANVIL_COIN ) ) {
                            return this.getPath( '30512-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, BROOCH_OF_THE_MAGPIE ) ) {
                            return this.getPath( '30512-02.html' )
                        }

                        if ( leaderState
                                && QuestHelper.hasQuestItem( player, BLACK_ANVIL_COIN )
                                && !QuestHelper.hasQuestItem( player, BROOCH_OF_THE_MAGPIE ) ) {
                            return this.getPath( '30512-04.html' )
                        }

                        break

                    case MARTIEN:
                        if ( !leaderState ) {
                            break
                        }

                        if ( state.getMemoState() === 1000 ) {
                            if ( !player.isClanLeader() ) {
                                return this.getPath( '30645-01.html' )
                            }
                                return this.getPath( '30645-02.html' )

                        }

                        if ( state.getMemoState() >= 2000 && state.getMemoState() < 3000 ) {
                            if ( QuestHelper.getQuestItemsCount( player, MIST_DRAKES_EGG ) < 10
                                    || QuestHelper.getQuestItemsCount( player, BLITZ_WYRM_EGG ) < 10
                                    || QuestHelper.getQuestItemsCount( player, THUNDER_WYRM_EGG ) < 10
                                    || QuestHelper.getQuestItemsCount( player, DRAKES_EGG ) < 10 ) {
                                return this.getPath( '30645-04.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, -1, MIST_DRAKES_EGG, BLITZ_WYRM_EGG, DRAKES_EGG, THUNDER_WYRM_EGG )

                            state.setMemoState( 3000 )
                            state.setConditionWithSound( 3, true )

                            return this.getPath( '30645-05.html' )
                        }

                        if ( state.getMemoState() === 3000 ) {
                            return this.getPath( '30645-07.html' )
                        }

                        if ( state.getMemoState() > 3000 ) {
                            return this.getPath( '30645-08.html' )
                        }

                        break

                    case WITCH_ATHREA:
                        if ( leaderState ) {
                            return this.getPath( '30758-01.html' )
                        }

                        break

                    case WITCH_KALIS:
                        if ( leaderState ) {
                            return this.getPath( '30759-01.html' )
                        }

                        break

                    case CORPSE_OF_FRITZ:
                        if ( state.getMemoState() >= 2000 && state.getMemoState() < 3000 ) {
                            return this.getPath( '30761-01.html' )
                        }

                        break

                    case CORPSE_OF_LUTZ:
                        if ( state.getMemoState() >= 2000 && state.getMemoState() < 3000 ) {
                            return this.getPath( '30762-01.html' )
                        }

                        break

                    case CORPSE_OF_KURTZ:
                        if ( [ 2000, 2110, 2010, 2100 ].includes( state.getMemoState() ) ) {
                            return this.getPath( '30763-01.html' )
                        }

                        if ( [ 2001, 2111, 2011, 2101 ].includes( state.getMemoState() ) ) {
                            return this.getPath( '30763-03.html' )
                        }

                        break

                    case BALTHAZAR:
                        if ( !leaderState ) {
                            break
                        }

                        if ( state.getMemoState() === 4000 ) {
                            if ( !player.isClanLeader() ) {
                                return this.getPath( '30764-01.html' )
                            }

                            if ( !QuestHelper.hasQuestItem( player, BLACK_ANVIL_COIN ) && player.isClanLeader() ) {
                                return this.getPath( '30764-02.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, BLACK_ANVIL_COIN ) ) {
                                return this.getPath( '30764-04.html' )
                            }

                            break
                        }

                        if ( state.getMemoState() === 5000 ) {
                            if ( QuestHelper.getQuestItemsCount( player, SPITEFUL_SOUL_ENERGY ) < 10 ) {
                                return this.getPath( '30764-07a.html' )
                            }

                            await QuestHelper.takeSingleItem( player, SPITEFUL_SOUL_ENERGY, -1 )

                            state.setMemoState( 6000 )
                            state.setConditionWithSound( 6, true )

                            return this.getPath( '30764-08a.html' )
                        }

                        if ( state.getMemoState() >= 6000 ) {
                            return this.getPath( '30764-09.html' )
                        }

                        break

                    case IMPERIAL_COFFER:
                        if ( !leaderState ) {
                            break
                        }

                        if ( state.getMemoState() >= 8500 && state.getMemoState() < 8700 ) {
                            if ( QuestHelper.getQuestItemsCount( player, IMPERIAL_KEY ) >= 6 ) {
                                if ( !player.isClanLeader() ) {
                                    return this.getPath( '30765-01.html' )
                                }

                                return this.getPath( '30765-03.html' )
                            }

                            break
                        }

                        if ( state.getMemoState() >= 8700 ) {
                            return this.getPath( '30765-05.html' )
                        }

                        break

                    case WITCH_CLEO:
                        if ( !leaderState ) {
                            break
                        }

                        if ( !player.isClanLeader() ) {
                            return this.getPath( '30766-01.html' )
                        }

                        if ( state.getMemoState() === 8000 ) {
                            return this.getPath( '30766-02.html' )
                        }

                        if ( state.getMemoState() === 8100 ) {
                            return this.getPath( '30766-05.html' )
                        }

                        if ( state.getMemoState() > 8100 && state.getMemoState() < 10000 ) {
                            return this.getPath( '30766-06.html' )
                        }

                        if ( state.getMemoState() === 10000 && player.isClanLeader() ) {
                            return this.getPath( '30766-07.html' )
                        }

                        break

                    case SIR_ERIC_RODEMAI:
                        if ( !leaderState ) {
                            break
                        }

                        if ( state.getMemoState() === 7000 ) {
                            if ( !player.isClanLeader() ) {
                                return this.getPath( '30868-01.html' )
                            }

                            return this.getPath( '30868-02.html' )
                        }

                        if ( state.getMemoState() === 8000 ) {
                            return this.getPath( '30868-05.html' )
                        }

                        if ( state.getMemoState() === 8100 ) {
                            if ( player.isClanLeader() ) {
                                state.setMemoState( 8500 )
                                state.setConditionWithSound( 10, true )

                                return this.getPath( '30868-06.html' )
                            }

                            return this.getPath( '30868-07.html' )
                        }

                        if ( state.getMemoState() >= 8500 && state.getMemoState() < 8511 ) {
                            return this.getPath( '30868-08.html' )
                        }

                        if ( state.getMemoState() === 8700 ) {
                            return this.getPath( '30868-09.html' )
                        }

                        if ( state.getMemoState() >= 9000 ) {
                            return this.getPath( '30868-11.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getLeaderQuestState( player: L2PcInstance ): QuestState {
        let clan: L2Clan = ClanCache.getClan( player.getClanId() )

        if ( !clan ) {
            return
        }

        return QuestStateCache.getQuestState( clan.getLeaderId(), this.getName(), false )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00503_PursuitOfClanAmbition'
    }
}