import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import {
    AttackableKillEvent,
    NpcApproachedForTalkEvent,
    NpcGeneralEvent,
    NpcTalkEvent,
} from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2Party } from '../../gameService/models/L2Party'
import { PlayerGroupCache } from '../../gameService/cache/PlayerGroupCache'
import { L2CommandChannel } from '../../gameService/models/L2CommandChannel'
import { L2Attackable } from '../../gameService/models/actor/L2Attackable'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { QuestType } from '../../gameService/enums/QuestType'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { L2Item } from '../../gameService/models/items/L2Item'
import { DataManager } from '../../data/manager'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { AggroCache, AggroData } from '../../gameService/cache/AggroCache'
import { L2Object } from '../../gameService/models/L2Object'
import _ from 'lodash'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const separatedSoulNpcIds = [
    32864,
    32865,
    32866,
    32867,
    32868,
    32869,
    32870,
    32891,
]

const DRAKE_LORD_CORPSE = 32884
const BEHEMOTH_LEADER_CORPSE = 32885
const DRAGON_BEAST_CORPSE = 32886
const DRAKE_LORD_ESSENCE = 17251
const BEHEMOTH_LEADER_ESSENCE = 17252
const DRAGON_BEAST_ESSENCE = 17253
const minimumPlayerAmount = 18
const minimumLevel = 80

const monsterCorpses: { [ npcId: number ]: number } = {
    25725: DRAKE_LORD_CORPSE,
    25726: BEHEMOTH_LEADER_CORPSE,
    25727: DRAGON_BEAST_CORPSE,
}

const monsterEssences: { [ npcId: number ]: number } = {
    [ DRAKE_LORD_CORPSE ]: DRAKE_LORD_ESSENCE,
    [ BEHEMOTH_LEADER_CORPSE ]: BEHEMOTH_LEADER_ESSENCE,
    [ DRAGON_BEAST_CORPSE ]: DRAGON_BEAST_ESSENCE,
}

const WEAPONS = [
    15558, // Periel Sword
    15559, // Skull Edge
    15560, // Vigwik Axe
    15561, // Devilish Maul
    15562, // Feather Eye Blade
    15563, // Octo Claw
    15564, // Doubletop Spear
    15565, // Rising Star
    15566, // Black Visage
    15567, // Veniplant Sword
    15568, // Skull Carnium Bow
    15569, // Gemtail Rapier
    15570, // Finale Blade
    15571, // Dominion Crossbow
]

const ARMOR = [
    15743, // Sealed Vorpal Helmet
    15746, // Sealed Vorpal Breastplate
    15749, // Sealed Vorpal Gaiters
    15752, // Sealed Vorpal Gauntlets
    15755, // Sealed Vorpal Boots
    15758, // Sealed Vorpal Shield
    15744, // Sealed Vorpal Leather Helmet
    15747, // Sealed Vorpal Leather Breastplate
    15750, // Sealed Vorpal Leather Leggings
    15753, // Sealed Vorpal Leather Gloves
    15756, // Sealed Vorpal Leather Boots
    15745, // Sealed Vorpal Circlet
    15748, // Sealed Vorpal Tunic
    15751, // Sealed Vorpal Stockings
    15754, // Sealed Vorpal Gloves
    15757, // Sealed Vorpal Shoes
    15759, // Sealed Vorpal Sigil
]

const ACCESSORIES = [
    15763, // Sealed Vorpal Ring
    15764, // Sealed Vorpal Earring
    15765, // Sealed Vorpal Necklace
]

const ATTRIBUTE_CRYSTALS = [
    9552, // Fire Crystal
    9553, // Water Crystal
    9554, // Earth Crystal
    9555, // Wind Crystal
    9556, // Dark Crystal
    9557, // Holy Crystal
]

const BLESSED_SCROLL_ENCHANT_WEAPON_S = 6577
const BLESSED_SCROLL_ENCHANT_ARMOR_S = 6578
const SCROLL_ENCHANT_WEAPON_S = 959
const GEMSTONE_S = 2134

const eventNames = {
    unspawn: 'us',
}

export class DontKnowDontCare extends ListenerLogic {
    allowedPlayers: { [ npcId: number ]: Set<number> } = {}

    constructor() {
        super( 'Q00456_DontKnowDontCare', 'listeners/tracked-400/DontKnowDontCare.ts' )
        this.questId = 456
        this.questItemIds = [
            DRAKE_LORD_ESSENCE,
            BEHEMOTH_LEADER_ESSENCE,
            DRAGON_BEAST_ESSENCE,
        ]
    }

    getQuestStartIds(): Array<number> {
        return separatedSoulNpcIds
    }

    getTalkIds(): Array<number> {
        return [
            ...separatedSoulNpcIds,
            DRAKE_LORD_CORPSE,
            BEHEMOTH_LEADER_CORPSE,
            DRAGON_BEAST_CORPSE,
        ]
    }

    getApproachedForTalkIds(): Array<number> {
        return [
            DRAKE_LORD_CORPSE,
            BEHEMOTH_LEADER_CORPSE,
            DRAGON_BEAST_CORPSE,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterCorpses ).map( value => _.parseInt( value ) )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        switch ( data.eventName ) {
            case '32864-04.htm':
            case '32864-05.htm':
            case '32864-06.htm':
                if ( state && state.isCreated() ) {
                    break
                }

                return

            case '32864-07.htm':
                if ( state && state.isCreated() ) {
                    state.startQuest()
                    break
                }

                return

            case eventNames.unspawn:
                delete this.allowedPlayers[ data.characterId ]
                await ( L2World.getObjectById( data.characterId ) as L2Npc ).deleteMe()
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let party: L2Party = PlayerGroupCache.getParty( data.playerId )
        if ( !party || !party.isInCommandChannel() ) {
            return
        }

        let channel: L2CommandChannel = party.getCommandChannel()

        if ( channel.getMemberCount() < minimumPlayerAmount ) {
            return
        }

        let npc = L2World.getObjectById( data.targetId ) as L2Attackable
        let playerSet = new Set<number>()

        _.each( AggroCache.getOwnedData( data.targetId ), ( data: AggroData ) => {
            let attacker: L2Object = L2World.getObjectById( data.attackerId )
            if ( !attacker && !attacker.isPlayer() ) {
                return
            }

            let attackerParty: L2Party = PlayerGroupCache.getParty( attacker.getObjectId() )

            if ( attackerParty
                    && attackerParty.getCommandChannel() === channel
                    && GeneralHelper.checkIfInRange( 1500, npc, attacker, true ) ) {
                playerSet.add( attacker.getObjectId() )
            }
        } )

        if ( playerSet.size > 0 ) {
            let spawned: L2Npc = QuestHelper.addSpawnAtLocation( monsterCorpses[ data.npcId ], npc, true, 0 )
            this.allowedPlayers[ spawned.getObjectId() ] = playerSet
            this.startQuestTimer( eventNames.unspawn, 300000, data.targetId, 0 )
        }
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00456_DontKnowDontCare'
    }

    async onApproachedForTalk( data: NpcApproachedForTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state
                || !state.isCondition( 1 )
                || !this.allowedPlayers[ data.characterId ]
                || this.allowedPlayers[ data.characterId ].has( data.playerId ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let itemId: number = monsterEssences[ data.characterNpcId ]
        if ( QuestHelper.hasQuestItem( player, itemId ) ) {
            return this.getPath( `${ data.characterNpcId }-03.html` )
        }

        await QuestHelper.giveSingleItem( player, itemId, 1 )

        if ( QuestHelper.hasQuestItems( player, ...this.questItemIds ) ) {
            state.setConditionWithSound( 2, true )
        } else {
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        }

        return this.getPath( `${ data.characterNpcId }-01.html` )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.COMPLETED:
                if ( !state.isNowAvailable() ) {
                    return this.getPath( '32864-02.html' )
                }

                state.setState( QuestStateValues.CREATED )

            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '32864-01.htm' : '32864-03.html' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( QuestHelper.hasAtLeastOneQuestItem( player, ...this.questItemIds ) ? '32864-09.html' : '32864-08.html' )
                    case 2:
                        if ( QuestHelper.hasQuestItems( player, ...this.questItemIds ) ) {
                            await this.rewardPlayer( player, L2World.getObjectById( data.characterId ) as L2Npc )
                            await state.exitQuestWithType( QuestType.DAILY, true )

                            return this.getPath( '32864-10.html' )
                        }

                        break
                }
                break


        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async rewardPlayer( player: L2PcInstance, npc: L2Npc ): Promise<void> {
        let chance = _.random( 10000 )
        let itemId: number
        let amount: number = 1

        if ( chance < 170 ) {
            itemId = _.sample( ARMOR )
        } else if ( chance < 200 ) {
            itemId = _.sample( ACCESSORIES )
        } else if ( chance < 270 ) {
            itemId = _.sample( WEAPONS )
        } else if ( chance < 325 ) {
            itemId = BLESSED_SCROLL_ENCHANT_WEAPON_S
        } else if ( chance < 425 ) {
            itemId = BLESSED_SCROLL_ENCHANT_ARMOR_S
        } else if ( chance < 925 ) {
            itemId = _.sample( ATTRIBUTE_CRYSTALS )
        } else if ( chance < 1100 ) {
            itemId = SCROLL_ENCHANT_WEAPON_S
        } else {
            itemId = GEMSTONE_S
            amount = 3
        }

        await QuestHelper.rewardSingleItem( player, itemId, amount )

        let item: L2Item = DataManager.getItems().getTemplate( itemId )
        return BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.S1_RECEIVED_A_S2_ITEM_AS_A_REWARD_FROM_THE_SEPARATED_SOUL, player.getName(), item.getName() )
    }
}