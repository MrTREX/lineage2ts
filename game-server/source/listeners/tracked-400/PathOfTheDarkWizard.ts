import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'

const CHARKEREN = 30415
const ANNIKA = 30418
const ARKENIA = 30419
const VARIKA = 30421
const SEEDS_OF_ANGER = 1253
const SEEDS_OF_DESPAIR = 1254
const SEEDS_OF_HORROR = 1255
const SEEDS_OF_LUNACY = 1256
const FAMILYS_REMAINS = 1257
const KNEE_BONE = 1259
const HEART_OF_LUNACY = 1260
const LUCKY_KEY = 1277
const CANDLE = 1278
const HUB_SCENT = 1279
const JEWEL_OF_DARKNESS = 1261
const MARSH_ZOMBIE = 20015
const MISERY_SKELETON = 20022
const SKELETON_SCOUT = 20045
const SKELETON_HUNTER = 20517
const SKELETON_HUNTER_ARCHER = 20518
const minimumLevel = 18

export class PathOfTheDarkWizard extends ListenerLogic {
    constructor() {
        super( 'Q00412_PathOfTheDarkWizard', 'listeners/tracked-400/PathOfTheDarkWizard.ts' )
        this.questId = 412
        this.questItemIds = [
            SEEDS_OF_ANGER,
            SEEDS_OF_DESPAIR,
            SEEDS_OF_HORROR,
            SEEDS_OF_LUNACY,
            FAMILYS_REMAINS,
            KNEE_BONE,
            HEART_OF_LUNACY,
            LUCKY_KEY,
            CANDLE,
            HUB_SCENT,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00412_PathOfTheDarkWizard'
    }

    getQuestStartIds(): Array<number> {
        return [ VARIKA ]
    }

    getTalkIds(): Array<number> {
        return [ VARIKA, CHARKEREN, ANNIKA, ARKENIA ]
    }

    getAttackableKillIds(): Array<number> {
        return [ MARSH_ZOMBIE, MISERY_SKELETON, SKELETON_SCOUT, SKELETON_HUNTER, SKELETON_HUNTER_ARCHER ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( player.getClassId() === ClassIdValues.darkMage.id ) {
                    if ( player.getLevel() >= minimumLevel ) {
                        if ( QuestHelper.hasQuestItem( player, JEWEL_OF_DARKNESS ) ) {
                            return this.getPath( '30421-04.htm' )
                        }

                        state.startQuest()
                        await QuestHelper.giveSingleItem( player, SEEDS_OF_DESPAIR, 1 )

                        return this.getPath( '30421-05.htm' )
                    }

                    return this.getPath( '30421-02.htm' )
                }

                if ( player.getClassId() === ClassIdValues.darkWizard.id ) {
                    return this.getPath( '30421-02a.htm' )
                }

                return this.getPath( '30421-03.htm' )

            case '30421-06.html':
                if ( QuestHelper.hasQuestItem( player, SEEDS_OF_ANGER ) ) {
                    break
                }

                return this.getPath( '30421-07.html' )

            case '30421-09.html':
                if ( QuestHelper.hasQuestItem( player, SEEDS_OF_HORROR ) ) {
                    break
                }

                return this.getPath( '30421-10.html' )

            case '30421-11.html':
                if ( QuestHelper.hasQuestItem( player, SEEDS_OF_LUNACY ) ) {
                    break
                }

                if ( !QuestHelper.hasQuestItem( player, SEEDS_OF_LUNACY )
                        && QuestHelper.hasQuestItem( player, SEEDS_OF_DESPAIR ) ) {
                    return this.getPath( '30421-12.html' )
                }

                return

            case '30421-08.html':
            case '30415-02.html':
                break

            case '30415-03.html':
                await QuestHelper.giveSingleItem( player, LUCKY_KEY, 1 )
                break

            case '30418-02.html':
                await QuestHelper.giveSingleItem( player, CANDLE, 1 )
                break


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( npc.getId() ) {
            case MARSH_ZOMBIE:
                if ( Math.random() < QuestHelper.getAdjustedChance( FAMILYS_REMAINS, 0.5, data.isChampion )
                        && QuestHelper.hasQuestItem( player, LUCKY_KEY )
                        && QuestHelper.getQuestItemsCount( player, FAMILYS_REMAINS ) < 3 ) {
                    await QuestHelper.rewardSingleQuestItem( player, FAMILYS_REMAINS, 1, data.isChampion )
                    this.playSound( player, FAMILYS_REMAINS, 3 )
                }

                return

            case MISERY_SKELETON:
            case SKELETON_HUNTER:
            case SKELETON_HUNTER_ARCHER:
                if ( Math.random() < QuestHelper.getAdjustedChance( KNEE_BONE, 0.5, data.isChampion )
                        && QuestHelper.hasQuestItem( player, CANDLE )
                        && QuestHelper.getQuestItemsCount( player, KNEE_BONE ) < 2 ) {
                    await QuestHelper.rewardSingleQuestItem( player, KNEE_BONE, 1, data.isChampion )
                    this.playSound( player, KNEE_BONE, 2 )
                }

                return

            case SKELETON_SCOUT:
                if ( Math.random() < QuestHelper.getAdjustedChance( HEART_OF_LUNACY, 0.5, data.isChampion )
                        && QuestHelper.hasQuestItem( player, HUB_SCENT )
                        && QuestHelper.getQuestItemsCount( player, HEART_OF_LUNACY ) < 3 ) {
                    await QuestHelper.rewardSingleQuestItem( player, HEART_OF_LUNACY, 1, data.isChampion )
                    this.playSound( player, HEART_OF_LUNACY, 3 )
                }

                return

        }
    }

    playSound( player: L2PcInstance, itemId: number, amount: number ): void {
        if ( QuestHelper.getQuestItemsCount( player, itemId ) >= amount ) {
            return player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === VARIKA ) {
                    if ( !QuestHelper.hasQuestItem( player, JEWEL_OF_DARKNESS ) ) {
                        return this.getPath( '30421-01.htm' )
                    }

                    return this.getPath( '30421-04.htm' )
                }

                break

            case QuestStateValues.STARTED:
                let [ hasDespair, hasHorror, hasLunacy, hasAnger ]: Array<boolean> = QuestHelper.hasEachQuestItem( player, SEEDS_OF_DESPAIR, SEEDS_OF_HORROR, SEEDS_OF_LUNACY, SEEDS_OF_ANGER )

                switch ( data.characterNpcId ) {
                    case VARIKA:
                        if ( !hasDespair ) {
                            break
                        }

                        if ( hasHorror && hasLunacy && hasAnger ) {
                            await QuestHelper.giveAdena( player, 163800, true )
                            await QuestHelper.giveSingleItem( player, JEWEL_OF_DARKNESS, 1 )

                            if ( player.getLevel() >= 20 ) {
                                await QuestHelper.addExpAndSp( player, 320534, 28630 )
                            } else if ( player.getLevel() === 19 ) {
                                await QuestHelper.addExpAndSp( player, 456128, 28630 )
                            } else {
                                await QuestHelper.addExpAndSp( player, 591724, 35328 )
                            }

                            await state.exitQuest( false, true )
                            player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )

                            return this.getPath( '30421-13.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, FAMILYS_REMAINS, LUCKY_KEY, CANDLE, HUB_SCENT, KNEE_BONE, HEART_OF_LUNACY ) ) {
                            return this.getPath( '30421-14.html' )
                        }

                        if ( !hasAnger ) {
                            return this.getPath( '30421-08.html' )
                        }

                        if ( !hasHorror ) {
                            return this.getPath( '30421-15.html' )
                        }

                        if ( !hasLunacy ) {
                            return this.getPath( '30421-12.html' )
                        }

                        break

                    case CHARKEREN:
                        if ( !hasAnger && hasDespair ) {
                            let hasKey: boolean = QuestHelper.hasQuestItem( player, LUCKY_KEY )
                            let amountRemains: number = QuestHelper.getQuestItemsCount( player, FAMILYS_REMAINS )

                            if ( !hasKey && amountRemains === 0 ) {
                                return this.getPath( '30415-01.html' )
                            }

                            if ( hasKey && amountRemains < 3 ) {
                                return this.getPath( '30415-04.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, -1, FAMILYS_REMAINS, LUCKY_KEY )
                            await QuestHelper.giveSingleItem( player, SEEDS_OF_ANGER, 1 )

                            return this.getPath( '30415-05.html' )
                        }

                        return this.getPath( '30415-06.html' )

                    case ANNIKA:
                        if ( !hasHorror && hasDespair ) {
                            let hasCandle: boolean = QuestHelper.hasQuestItem( player, CANDLE )
                            let amountBone: number = QuestHelper.getQuestItemsCount( player, KNEE_BONE )

                            if ( !hasCandle && amountBone === 0 ) {
                                return this.getPath( '30418-01.html' )
                            }

                            if ( hasCandle && amountBone < 2 ) {
                                return this.getPath( '30418-03.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, -1, KNEE_BONE, CANDLE )
                            await QuestHelper.giveSingleItem( player, SEEDS_OF_HORROR, 1 )

                            return this.getPath( '30418-04.html' )
                        }

                        break

                    case ARKENIA:
                        if ( hasLunacy ) {
                            break
                        }

                        let hasScent: boolean = QuestHelper.hasQuestItem( player, HUB_SCENT )
                        let amountLunacy: number = QuestHelper.getQuestItemsCount( player, HEART_OF_LUNACY )

                        if ( !hasScent && amountLunacy === 0 ) {
                            await QuestHelper.giveSingleItem( player, HUB_SCENT, 1 )
                            return this.getPath( '30419-01.html' )
                        }

                        if ( hasScent && amountLunacy < 3 ) {
                            return this.getPath( '30419-02.html' )
                        }

                        await QuestHelper.giveSingleItem( player, SEEDS_OF_LUNACY, 1 )
                        await QuestHelper.takeMultipleItems( player, -1, HEART_OF_LUNACY, HUB_SCENT )

                        return this.getPath( '30419-03.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}