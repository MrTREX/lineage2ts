import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { SkillHolder } from '../../gameService/models/holders/SkillHolder'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import {
    AttackableAttackedEvent,
    AttackableKillEvent,
    NpcGeneralEvent,
    NpcTalkEvent,
} from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2ItemInstance } from '../../gameService/models/items/instance/L2ItemInstance'
import { L2Summon } from '../../gameService/models/actor/L2Summon'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import aigle from 'aigle'
import _ from 'lodash'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const CRONOS = 30610
const MIMYU = 30747
const DRAGONFLUTE_OF_WIND = 3500
const DRAGONFLUTE_OF_STAR = 3501
const DRAGONFLUTE_OF_TWILIGHT = 3502
const FAIRY_LEAF = 4325
const TREE_OF_WIND = 27185
const TREE_OF_STAR = 27186
const TREE_OF_TWILIGHT = 27187
const TREE_OF_ABYSS = 27188
const SOUL_OF_TREE_GUARDIAN = 27189
const CURSE_OF_MIMYU = new SkillHolder( 4167 )
const DRYAD_ROOT = new SkillHolder( 1201, 33 )
const VICIOUS_POISON = new SkillHolder( 4243 )
const DRAGON_BUGLE_OF_WIND = 4422
const DRAGON_BUGLE_OF_STAR = 4423
const DRAGON_BUGLE_OF_TWILIGHT = 4424
const minimumLevel = 45
const minimumHatchlingLevel = 55

type MonsterParameters = [ number, number, number, number ] // stringId, memoState, memoValue, hitCount
const monsterData: { [ npcId: number ]: MonsterParameters } = {
    [ TREE_OF_WIND ]: [ NpcStringIds.HEY_YOUVE_ALREADY_DRUNK_THE_ESSENCE_OF_WIND, 2, 1, 270 ],
    [ TREE_OF_STAR ]: [ NpcStringIds.HEY_YOUVE_ALREADY_DRUNK_THE_ESSENCE_OF_A_STAR, 4, 2, 400 ],
    [ TREE_OF_TWILIGHT ]: [ NpcStringIds.HEY_YOUVE_ALREADY_DRUNK_THE_ESSENCE_OF_DUSK, 8, 4, 150 ],
    [ TREE_OF_ABYSS ]: [ NpcStringIds.HEY_YOUVE_ALREADY_DRUNK_THE_ESSENCE_OF_THE_ABYSS, 16, 8, 270 ],
}

const eventNames = {
    despawn: 'despawn',
}

const variableNames = {
    fluteObjectId: 'foi',
    hits: 'hits',
}

const npcIds = _.keys( monsterData ).map( value => _.parseInt( value ) )

export class LittleWingsBigAdventure extends ListenerLogic {
    constructor() {
        super( 'Q00421_LittleWingsBigAdventure', 'listeners/tracked-400/LittleWingsBigAdventure.ts' )
        this.questId = 421
        this.questItemIds = [ FAIRY_LEAF ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00421_LittleWingsBigAdventure'
    }

    getQuestStartIds(): Array<number> {
        return [ CRONOS ]
    }

    getTalkIds(): Array<number> {
        return [ CRONOS, MIMYU ]
    }

    getAttackableKillIds(): Array<number> {
        return npcIds
    }

    getAttackableAttackIds(): Array<number> {
        return npcIds
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        if ( data.eventName === eventNames.despawn ) {
            let npc = L2World.getObjectById( data.characterId ) as L2Npc
            if ( npc ) {
                await npc.deleteMe()
            }

            return
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30610-05.htm':
                if ( state.isCreated() ) {
                    if ( QuestHelper.getItemsSumCount( player, DRAGONFLUTE_OF_WIND, DRAGONFLUTE_OF_STAR, DRAGONFLUTE_OF_TWILIGHT ) === 1 ) {
                        let flute: L2ItemInstance = this.getFlute( player )

                        if ( flute.getEnchantLevel() < minimumHatchlingLevel ) {
                            return this.getPath( '30610-06.html' )
                        }

                        state.startQuest()
                        state.setMemoState( 100 )
                        state.setVariable( variableNames.fluteObjectId, flute.getObjectId() )

                        break
                    }

                    return this.getPath( '30610-06.html' )
                }

                break

            case '30747-04.html':
                let summon: L2Summon = player.getSummon()

                if ( !summon ) {
                    return this.getPath( '30747-02.html' )
                }

                if ( summon.getControlObjectId() !== state.getVariable( variableNames.fluteObjectId ) ) {
                    return this.getPath( '30747-03.html' )
                }

                break

            case '30747-05.html':
                let playerSummon: L2Summon = player.getSummon()

                if ( !playerSummon ) {
                    return this.getPath( '30747-06.html' )
                }

                if ( playerSummon.getControlObjectId() !== state.getVariable( variableNames.fluteObjectId ) ) {
                    return this.getPath( '30747-06.html' )
                }

                await QuestHelper.giveSingleItem( player, FAIRY_LEAF, 4 )
                state.setConditionWithSound( 2, true )
                state.setMemoState( 0 )

                break

            case '30747-07.html':
            case '30747-08.html':
            case '30747-09.html':
            case '30747-10.html':
                break


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case CRONOS:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        let fluteCount: number = QuestHelper.getItemsSumCount( player, DRAGONFLUTE_OF_WIND, DRAGONFLUTE_OF_STAR, DRAGONFLUTE_OF_TWILIGHT )
                        if ( fluteCount === 0 ) {
                            break // this quest does not show up if no flute in inventory
                        }

                        if ( player.getLevel() < minimumLevel ) {
                            return this.getPath( '30610-01.htm' )
                        }

                        if ( fluteCount > 1 ) {
                            return this.getPath( '30610-02.htm' )
                        }

                        if ( this.getFlute( player ).getEnchantLevel() < minimumHatchlingLevel ) {
                            return this.getPath( '30610-03.html' )
                        }

                        return this.getPath( '30610-04.htm' )

                    case QuestStateValues.STARTED:
                        return this.getPath( '30610-07.html' )

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case MIMYU:
                switch ( state.getMemoState() ) {
                    case 100:
                        state.setMemoState( 200 )
                        return this.getPath( '30747-01.html' )

                    case 200:
                        let summon: L2Summon = player.getSummon()

                        if ( !summon ) {
                            return this.getPath( '30747-02.html' )
                        }

                        if ( summon.getControlObjectId() !== state.getVariable( variableNames.fluteObjectId ) ) {
                            return this.getPath( '30747-03.html' )
                        }

                        return this.getPath( '30747-04.html' )

                    case 0:
                        return this.getPath( '30747-07.html' )

                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                    case 10:
                    case 11:
                    case 12:
                    case 13:
                    case 14:
                        if ( QuestHelper.hasQuestItems( player, FAIRY_LEAF ) ) {
                            return this.getPath( '30747-11.html' )
                        }

                        break

                    case 15:
                        if ( !QuestHelper.hasQuestItems( player, FAIRY_LEAF ) ) {
                            let summon: L2Summon = player.getSummon()

                            if ( !summon ) {
                                return this.getPath( '30747-12.html' )
                            }

                            if ( summon.getControlObjectId() === state.getVariable( variableNames.fluteObjectId ) ) {
                                state.setMemoState( 16 )
                                return this.getPath( '30747-13.html' )
                            }

                            return this.getPath( '30747-14.html' )
                        }

                        break

                    case 16:
                        if ( QuestHelper.hasQuestItems( player, FAIRY_LEAF ) ) {
                            break
                        }

                        if ( player.hasSummon() ) {
                            return this.getPath( '30747-15.html' )
                        }

                        let fluteCount: number = QuestHelper.getItemsSumCount( player, DRAGONFLUTE_OF_WIND, DRAGONFLUTE_OF_STAR, DRAGONFLUTE_OF_TWILIGHT )

                        if ( fluteCount > 1 ) {
                            return this.getPath( '30747-17.html' )
                        }

                        if ( fluteCount === 1 ) {
                            let flute: L2ItemInstance = this.getFlute( player )

                            if ( flute.getObjectId() === state.getVariable( variableNames.fluteObjectId ) ) {
                                // TODO what if the hatchling has items in his inventory?
                                // Should they be transfered to the strider or given to the player?
                                switch ( flute.getId() ) {
                                    case DRAGONFLUTE_OF_WIND:
                                        await QuestHelper.takeSingleItem( player, DRAGONFLUTE_OF_WIND, 1 )
                                        await QuestHelper.giveSingleItem( player, DRAGON_BUGLE_OF_WIND, 1 )

                                        break

                                    case DRAGONFLUTE_OF_STAR:
                                        await QuestHelper.takeSingleItem( player, DRAGONFLUTE_OF_STAR, -1 )
                                        await QuestHelper.giveSingleItem( player, DRAGON_BUGLE_OF_STAR, 1 )

                                        break

                                    case DRAGONFLUTE_OF_TWILIGHT:
                                        await QuestHelper.takeSingleItem( player, DRAGONFLUTE_OF_TWILIGHT, -1 )
                                        await QuestHelper.giveSingleItem( player, DRAGON_BUGLE_OF_TWILIGHT, 1 )

                                        break
                                }

                                await state.exitQuest( true, true )
                                return this.getPath( '30747-16.html' )
                            }

                            let npc = L2World.getObjectById( data.characterId ) as L2Npc
                            npc.setTarget( player )
                            await npc.doCastWithHolder( CURSE_OF_MIMYU )

                            return this.getPath( '30747-18.html' )
                        }
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getFlute( player: L2PcInstance ): L2ItemInstance {
        let item: L2ItemInstance

        _.each( [ DRAGONFLUTE_OF_WIND, DRAGONFLUTE_OF_STAR, DRAGONFLUTE_OF_TWILIGHT ], ( itemId: number ) => {
            item = player.getInventory().getItemByItemId( itemId )

            return !item
        } )

        return item
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        let quest = this
        await aigle.resolve( 20 ).times( async ( index: number ) => {
            let mob: L2Npc = QuestHelper.addSpawnAtLocation( SOUL_OF_TREE_GUARDIAN, npc )

            if ( index === 0 ) {
                npc.setTarget( player )
                await npc.doCastWithHolder( VICIOUS_POISON )
            }

            QuestHelper.addAttackDesire( mob, player )
            quest.startQuestTimer( eventNames.despawn, 300000, mob.getObjectId(), 0, false )
        } )
    }

    async onAttackableAttackedEvent( data: AttackableAttackedEvent ): Promise<void> {
        let state: QuestState = QuestStateCache.getQuestState( data.attackerPlayerId, this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        let npc = L2World.getObjectById( data.targetId ) as L2Npc
        let player = L2World.getPlayer( data.attackerPlayerId )

        if ( state.isCondition( 2 ) ) {
            let summon = L2World.getObjectById( data.attackerId ) as L2Summon

            if ( !summon.isSummon() ) {
                if ( Math.random() < 0.3 ) {
                    npc.setTarget( player )
                    await npc.doCastWithHolder( VICIOUS_POISON )
                }

                return
            }

            let [ stringId, memoState, memoValue, totalHitsRequired ] = monsterData[ data.targetNpcId ]
            if ( ( state.getMemoState() % memoState ) < memoValue ) {
                if ( summon.getControlObjectId() !== state.getVariable( variableNames.fluteObjectId ) ) {
                    return
                }

                let hitsCounter = state.getVariable( variableNames.hits ) || 0

                hitsCounter++
                state.setVariable( variableNames.hits, hitsCounter )

                if ( hitsCounter < totalHitsRequired ) {
                    if ( data.targetNpcId === TREE_OF_ABYSS && Math.random() < 0.02 ) {
                        npc.setTarget( summon )
                        await npc.doCastWithHolder( DRYAD_ROOT )
                    }

                    return
                }

                if ( Math.random() < 0.02 && QuestHelper.hasQuestItem( player, FAIRY_LEAF ) ) {
                    BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.GIVE_ME_A_FAIRY_LEAF )
                    await QuestHelper.takeSingleItem( player, FAIRY_LEAF, 1 )

                    state.setMemoState( state.getMemoState() + memoValue )
                    state.unsetVariable( variableNames.hits )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                    if ( state.getMemoState() === 15 ) {
                        state.setConditionWithSound( 3 )
                    }
                }

                return
            }

            switch ( _.random( 2 ) ) {
                case 0:
                    BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.WHY_DO_YOU_BOTHER_ME_AGAIN )
                    break

                case 1:
                    BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, stringId )
                    break

                case 2:
                    BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.LEAVE_NOW_BEFORE_YOU_INCUR_THE_WRATH_OF_THE_GUARDIAN_GHOST )
                    break
            }

            return
        }

        if ( npc.getCurrentHp() < ( npc.getMaxHp() * 0.67 ) && Math.random() < 0.3 ) {
            npc.setTarget( player )
            await npc.doCastWithHolder( VICIOUS_POISON )
        }
    }
}