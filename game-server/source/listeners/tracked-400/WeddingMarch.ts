import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const KANTABILON = 31042
const SILVER_CRYSTAL = 7540
const WEDDING_ECHO_CRYSTAL = 7062
const minimumLevel = 38
const CRYSTAL_COUNT = 50

export class WeddingMarch extends ListenerLogic {
    constructor() {
        super( 'Q00431_WeddingMarch', 'listeners/tracked-400/WeddingMarch.ts' )
        this.questId = 431
        this.questItemIds = [ SILVER_CRYSTAL ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            20786, // Lienrik
            20787, // Lienrik Lad
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ KANTABILON ]
    }

    getTalkIds(): Array<number> {
        return [ KANTABILON ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00431_WeddingMarch'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31042-02.htm':
                state.startQuest()
                break

            case '31042-06.html':
                if ( QuestHelper.getQuestItemsCount( player, SILVER_CRYSTAL ) < CRYSTAL_COUNT ) {
                    return this.getPath( '31042-05.html' )
                }

                await QuestHelper.rewardSingleItem( player, WEDDING_ECHO_CRYSTAL, 25 )
                await state.exitQuest( true, true )

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( Math.random() > QuestHelper.getAdjustedChance( SILVER_CRYSTAL, 0.5, data.isChampion ) ) {
            return
        }

        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), 1, 1, L2World.getObjectById( data.targetId ) )
        if ( !state ) {
            return
        }

        await QuestHelper.rewardAndProgressState( state.getPlayer(), state, SILVER_CRYSTAL, 1, CRYSTAL_COUNT, data.isChampion, 2 )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '31042-01.htm' : '31042-00.htm' )

            case QuestStateValues.STARTED:
                return this.getPath( state.isCondition( 1 ) ? '31042-03.html' : '31042-04.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}