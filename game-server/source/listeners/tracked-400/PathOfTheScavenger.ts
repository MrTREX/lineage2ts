import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import {
    AttackableAttackedEvent,
    AttackableKillEvent,
    NpcGeneralEvent,
    NpcTalkEvent,
} from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { L2Attackable } from '../../gameService/models/actor/L2Attackable'
import { QuestStateValues } from '../../gameService/models/quest/State'
import _ from 'lodash'

const WAREHOUSE_KEEPER_RAUT = 30316
const TRADER_SHARI = 30517
const TRADER_MION = 30519
const COLLECTOR_PIPI = 30524
const HEAD_BLACKSMITH_BRONK = 30525
const PRIEST_OF_THE_EARTH_ZIMENF = 30538
const MASTER_TOMA = 30556
const TORAI = 30557
const WAREHOUSE_CHIEF_YASENI = 31958
const PIPPIS_LETTER_OF_RECOMMENDATION = 1643
const ROUTS_TELEPORT_SCROLL = 1644
const SUCCUBUS_UNDIES = 1645
const MIONS_LETTER = 1646
const BRONKS_INGOT = 1647
const SHARIS_AXE = 1648
const ZIMENFS_POTION = 1649
const BRONKS_PAY = 1650
const SHARIS_PAY = 1651
const ZIMENFS_PAY = 1652
const BEAR_PICTURE = 1653
const TARANTULA_PICTURE = 1654
const HONEY_JAR = 1655
const BEAD = 1656
const BEAD_PARCEL = 1657
const BEAD_PARCEL2 = 8543
const RING_OF_RAVEN = 1642
const HUNTER_TARANTULA = 20403
const PLUNDER_TARANTULA = 20508
const HUNTER_BEAR = 20777
const HONEY_BEAR = 27058
const SPOIL = 254
const minimumLevel = 18

const variableNames = {
    initialAttacker: '417.ia',
    flagValue: 'fa',
}

export class PathOfTheScavenger extends ListenerLogic {
    constructor() {
        super( 'Q00417_PathOfTheScavenger', 'listeners/tracked-400/PathOfTheScavenger.ts' )
        this.questId = 417
        this.questItemIds = [
            PIPPIS_LETTER_OF_RECOMMENDATION,
            ROUTS_TELEPORT_SCROLL,
            SUCCUBUS_UNDIES,
            MIONS_LETTER,
            BRONKS_INGOT,
            SHARIS_AXE,
            ZIMENFS_POTION,
            BRONKS_PAY,
            SHARIS_PAY,
            ZIMENFS_PAY,
            BEAR_PICTURE,
            TARANTULA_PICTURE,
            HONEY_JAR,
            BEAD,
            BEAD_PARCEL,
            BEAD_PARCEL2,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00417_PathOfTheScavenger'
    }

    getQuestStartIds(): Array<number> {
        return [ COLLECTOR_PIPI ]
    }

    getTalkIds(): Array<number> {
        return [
            COLLECTOR_PIPI,
            WAREHOUSE_KEEPER_RAUT,
            TRADER_MION,
            TRADER_SHARI,
            HEAD_BLACKSMITH_BRONK,
            PRIEST_OF_THE_EARTH_ZIMENF,
            MASTER_TOMA,
            TORAI,
            WAREHOUSE_CHIEF_YASENI,
        ]
    }

    getAttackableAttackIds(): Array<number> {
        return [
            HUNTER_TARANTULA,
            PLUNDER_TARANTULA,
            HUNTER_BEAR,
            HONEY_BEAR,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            HUNTER_TARANTULA,
            PLUNDER_TARANTULA,
            HUNTER_BEAR,
            HONEY_BEAR,
        ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( player.getClassId() === ClassIdValues.dwarvenFighter.id ) {
                    if ( player.getLevel() >= minimumLevel ) {
                        if ( QuestHelper.hasQuestItem( player, RING_OF_RAVEN ) ) {
                            return this.getPath( '30524-04.htm' )
                        }

                        state.startQuest()
                        state.setMemoStateEx( 1, 0 )

                        await QuestHelper.giveSingleItem( player, PIPPIS_LETTER_OF_RECOMMENDATION, 1 )
                        return this.getPath( '30524-05.htm' )
                    }

                    return this.getPath( '30524-02.htm' )
                }

                if ( player.getClassId() === ClassIdValues.scavenger.id ) {
                    return this.getPath( '30524-02a.htm' )
                }

                return this.getPath( '30524-08.htm' )

            case '30524-03.html':
            case '30557-02.html':
            case '30519-06.html':
                break

            case 'reply_1':
                if ( QuestHelper.hasQuestItem( player, PIPPIS_LETTER_OF_RECOMMENDATION ) ) {
                    await QuestHelper.takeSingleItem( player, PIPPIS_LETTER_OF_RECOMMENDATION, 1 )
                    switch ( _.random( 2 ) ) {
                        case 0:
                            await QuestHelper.giveSingleItem( player, ZIMENFS_POTION, 1 )
                            return this.getPath( '30519-02.html' )

                        case 1:
                            await QuestHelper.giveSingleItem( player, SHARIS_AXE, 1 )
                            return this.getPath( '30519-03.html' )

                        case 2:
                            await QuestHelper.giveSingleItem( player, BRONKS_INGOT, 1 )
                            return this.getPath( '30519-04.html' )
                    }
                }

                return

            case '30519-07.html':
                state.setMemoStateEx( 1, state.getMemoStateEx( 1 ) + 1 )
                break

            case 'reply_2':
                switch ( _.random( 1 ) ) {
                    case 0:
                        return this.getPath( '30519-06.html' )

                    case 1:
                        return this.getPath( '30519-11.html' )
                }

                return

            case 'reply_3':
                if ( ( state.getMemoStateEx( 1 ) % 10 ) < 2 ) {
                    state.setMemoStateEx( 1, state.getMemoStateEx( 1 ) + 1 )
                    return this.getPath( '30519-07.html' )
                }

                if ( ( ( state.getMemoStateEx( 1 ) % 10 ) === 2 ) && state.isMemoState( 0 ) ) {
                    return this.getPath( '30519-07.html' )
                }

                if ( ( ( state.getMemoStateEx( 1 ) % 10 ) === 2 ) && state.isMemoState( 1 ) ) {
                    state.setMemoStateEx( 1, state.getMemoStateEx( 1 ) + 1 )
                    return this.getPath( '30519-09.html' )
                }

                if ( ( state.getMemoStateEx( 1 ) % 10 ) >= 3 && state.isMemoState( 1 ) ) {
                    await QuestHelper.giveSingleItem( player, MIONS_LETTER, 1 )
                    await QuestHelper.takeMultipleItems( player, 1, SHARIS_AXE, ZIMENFS_POTION, BRONKS_INGOT )

                    state.setConditionWithSound( 4, true )
                    return this.getPath( '30519-10.html' )
                }
                return

            case 'reply_4':
                await QuestHelper.takeMultipleItems( player, 1, ZIMENFS_PAY, SHARIS_PAY, BRONKS_PAY )

                switch ( _.random( 2 ) ) {
                    case 0:
                        await QuestHelper.giveSingleItem( player, ZIMENFS_POTION, 1 )
                        return this.getPath( '30519-02.html' )

                    case 1:
                        await QuestHelper.giveSingleItem( player, SHARIS_AXE, 1 )
                        return this.getPath( '30519-03.html' )

                    case 2:
                        await QuestHelper.giveSingleItem( player, BRONKS_INGOT, 1 )
                        return this.getPath( '30519-04.html' )

                }

                return

            case '30556-05b.html':
                if ( QuestHelper.hasQuestItem( player, TARANTULA_PICTURE )
                        && QuestHelper.getQuestItemsCount( player, BEAD ) >= 20 ) {
                    await QuestHelper.takeMultipleItems( player, -1, TARANTULA_PICTURE, BEAD )
                    await QuestHelper.giveSingleItem( player, BEAD_PARCEL, 1 )

                    state.setConditionWithSound( 9, true )
                    break
                }

                return

            case '30556-06b.html':
                if ( QuestHelper.hasQuestItem( player, TARANTULA_PICTURE ) && QuestHelper.getQuestItemsCount( player, BEAD ) >= 20 ) {
                    await QuestHelper.takeMultipleItems( player, -1, TARANTULA_PICTURE, BEAD )
                    await QuestHelper.giveSingleItem( player, BEAD_PARCEL2, 1 )

                    state.setMemoState( 2 )
                    state.setConditionWithSound( 12, true )

                    break
                }

                return

            case '30316-02.html':
                if ( QuestHelper.hasQuestItem( player, BEAD_PARCEL ) ) {
                    await QuestHelper.takeSingleItem( player, BEAD_PARCEL, 1 )
                    await QuestHelper.giveSingleItem( player, ROUTS_TELEPORT_SCROLL, 1 )

                    state.setConditionWithSound( 10, true )
                    break
                }

                return

            case '30316-03.html':
                if ( QuestHelper.hasQuestItem( player, BEAD_PARCEL ) ) {
                    await QuestHelper.takeSingleItem( player, BEAD_PARCEL, 1 )
                    await QuestHelper.giveSingleItem( player, ROUTS_TELEPORT_SCROLL, 1 )

                    state.setConditionWithSound( 10, true )
                    break
                }

                return

            case '30557-03.html':
                if ( QuestHelper.hasQuestItem( player, ROUTS_TELEPORT_SCROLL ) ) {
                    await QuestHelper.takeSingleItem( player, ROUTS_TELEPORT_SCROLL, 1 )
                    await QuestHelper.giveSingleItem( player, SUCCUBUS_UNDIES, 1 )

                    state.setConditionWithSound( 11, true )
                    await ( L2World.getObjectById( data.characterId ) as L2Npc ).deleteMe()
                    break
                }

                return

            case '31958-02.html':
                if ( state.isMemoState( 2 )
                        && QuestHelper.hasQuestItem( player, BEAD_PARCEL2 ) ) {
                    await QuestHelper.giveAdena( player, 163800, true )
                    await QuestHelper.giveSingleItem( player, RING_OF_RAVEN, 1 )

                    if ( player.getLevel() >= 20 ) {
                        await QuestHelper.addExpAndSp( player, 320534, 35412 )
                    } else if ( player.getLevel() === 19 ) {
                        await QuestHelper.addExpAndSp( player, 456128, 42110 )
                    } else {
                        await QuestHelper.addExpAndSp( player, 591724, 48808 )
                    }

                    await state.exitQuest( false, true )
                    player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )

                    break
                }

                return


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableAttackedEvent( data: AttackableAttackedEvent ): Promise<void> {
        let state: QuestState = QuestStateCache.getQuestState( data.attackerPlayerId, this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        switch ( data.targetNpcId ) {
            case HUNTER_BEAR:
                switch ( NpcVariablesManager.get( data.targetId, this.getName() ) ) {
                    case 2:
                        return

                    case 1:
                        if ( NpcVariablesManager.get( data.targetId, variableNames.initialAttacker ) !== data.attackerPlayerId ) {
                            NpcVariablesManager.set( data.targetId, this.getName(), 2 )
                        }

                        return


                    default:
                        NpcVariablesManager.set( data.targetId, this.getName(), 1 )
                        NpcVariablesManager.set( data.targetId, variableNames.initialAttacker, data.attackerPlayerId )
                        break
                }

                return

            case HUNTER_TARANTULA:
            case PLUNDER_TARANTULA:
            case HONEY_BEAR:
                if ( !NpcVariablesManager.get( data.targetId, this.getName() ) ) {
                    NpcVariablesManager.set( data.targetId, this.getName(), 1 )
                    NpcVariablesManager.set( data.targetId, variableNames.initialAttacker, data.attackerPlayerId )
                }

                // TODO: rework, see last line, should be skill parameter and not last skill casted.
                let player = L2World.getPlayer( data.attackerPlayerId )
                if ( player.getLastSkillCast() && player.getLastSkillCast().getId() === SPOIL ) {
                    NpcVariablesManager.set( data.targetId, this.getName(), 2 )
                    player.setLastSkillCast( null ) // Reset last skill cast.
                }

                return
        }
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Attackable

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case HUNTER_BEAR:
                if ( NpcVariablesManager.get( data.targetId, this.getName() ) === 1
                        && data.playerId === NpcVariablesManager.get( data.targetId, variableNames.initialAttacker )
                        && QuestHelper.hasQuestItem( player, BEAR_PICTURE )
                        && QuestHelper.getQuestItemsCount( player, HONEY_JAR ) < 5 ) {
                    let value: number = state.getVariable( variableNames.flagValue ) as number || 0

                    if ( value > 0 && _.random( 5 ) <= value ) {
                        QuestHelper.addSpawnAtLocation( HONEY_BEAR, npc, true, 0, true )
                        state.setVariable( variableNames.flagValue, 0 )
                    } else {
                        state.setVariable( variableNames.flagValue, value + 1 )
                    }
                }

                return

            case HONEY_BEAR:
                if ( NpcVariablesManager.get( data.targetId, this.getName() ) === 2
                        && data.playerId === NpcVariablesManager.get( data.targetId, variableNames.initialAttacker )
                        && npc.isSpoiled()
                        && QuestHelper.hasQuestItem( player, BEAR_PICTURE ) ) {
                    await QuestHelper.rewardAndProgressState( player, state, HONEY_JAR, 1, 5, data.isChampion, 6 )
                }

                return

            case HUNTER_TARANTULA:
            case PLUNDER_TARANTULA:
                if ( NpcVariablesManager.get( data.targetId, this.getName() ) === 2
                        && data.playerId === NpcVariablesManager.get( data.targetId, variableNames.initialAttacker )
                        && npc.isSpoiled()
                        && QuestHelper.hasQuestItem( player, TARANTULA_PICTURE ) ) {
                    await QuestHelper.rewardAndProgressState( player, state, BEAD, 1, 20, data.isChampion, 8 )
                }

                return
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === COLLECTOR_PIPI ) {
                    return this.getPath( '30524-01.htm' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case COLLECTOR_PIPI:
                        if ( QuestHelper.hasQuestItem( player, PIPPIS_LETTER_OF_RECOMMENDATION ) ) {
                            return this.getPath( '30524-06.html' )
                        }

                        return this.getPath( '30524-07.html' )

                    case TRADER_MION:
                        if ( QuestHelper.hasQuestItem( player, PIPPIS_LETTER_OF_RECOMMENDATION ) ) {
                            state.setConditionWithSound( 2, true )

                            return this.getPath( '30519-01.html' )
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, SHARIS_AXE, BRONKS_INGOT, ZIMENFS_POTION ) ) {
                            if ( state.getMemoStateEx( 1 ) % 10 === 0 ) {
                                return this.getPath( '30519-05.html' )
                            }

                            return this.getPath( '30519-08.html' )
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, SHARIS_PAY, BRONKS_PAY, ZIMENFS_PAY ) ) {
                            if ( state.getMemoStateEx( 1 ) < 50 ) {
                                return this.getPath( '30519-12.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, 1, SHARIS_PAY, ZIMENFS_PAY, BRONKS_PAY )
                            await QuestHelper.giveSingleItem( player, MIONS_LETTER, 1 )

                            state.setConditionWithSound( 4, true )
                            return this.getPath( '30519-15.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, MIONS_LETTER ) ) {
                            return this.getPath( '30519-13.html' )
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, BEAR_PICTURE, TARANTULA_PICTURE, BEAD_PARCEL, ROUTS_TELEPORT_SCROLL, SUCCUBUS_UNDIES ) ) {
                            return this.getPath( '30519-14.html' )
                        }

                        break

                    case TRADER_SHARI:
                        if ( QuestHelper.hasQuestItem( player, SHARIS_AXE ) ) {
                            await QuestHelper.takeSingleItem( player, SHARIS_AXE, 1 )
                            await QuestHelper.giveSingleItem( player, SHARIS_PAY, 1 )

                            state.setMemoStateEx( 1, state.getMemoStateEx( 1 ) + 10 )

                            if ( state.getMemoStateEx( 1 ) < 20 ) {
                                return this.getPath( '30517-01.html' )
                            }

                            state.setMemoState( 1 )
                            state.setConditionWithSound( 3, true )

                            return this.getPath( '30517-02.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, SHARIS_PAY ) ) {
                            return this.getPath( '30517-03.html' )
                        }

                        break

                    case HEAD_BLACKSMITH_BRONK:
                        if ( QuestHelper.hasQuestItem( player, BRONKS_INGOT ) ) {
                            await QuestHelper.takeSingleItem( player, BRONKS_INGOT, 1 )
                            await QuestHelper.giveSingleItem( player, BRONKS_PAY, 1 )

                            state.setMemoStateEx( 1, state.getMemoStateEx( 1 ) + 10 )

                            if ( state.getMemoStateEx( 1 ) < 20 ) {
                                return this.getPath( '30525-01.html' )
                            }

                            state.setMemoState( 1 )
                            state.setConditionWithSound( 3, true )

                            return this.getPath( '30525-02.html' )

                        }

                        if ( QuestHelper.hasQuestItem( player, BRONKS_PAY ) ) {
                            return this.getPath( '30525-03.html' )
                        }

                        break

                    case PRIEST_OF_THE_EARTH_ZIMENF:
                        if ( QuestHelper.hasQuestItem( player, ZIMENFS_POTION ) ) {
                            await QuestHelper.takeSingleItem( player, ZIMENFS_POTION, 1 )
                            await QuestHelper.giveSingleItem( player, ZIMENFS_PAY, 1 )

                            state.setMemoStateEx( 1, state.getMemoStateEx( 1 ) + 10 )

                            if ( state.getMemoStateEx( 1 ) < 20 ) {
                                return this.getPath( '30538-01.html' )
                            }

                            state.setMemoState( 1 )
                            state.setConditionWithSound( 3, true )

                            return this.getPath( '30538-02.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, ZIMENFS_PAY ) ) {
                            return this.getPath( '30538-03.html' )
                        }

                        break

                    case MASTER_TOMA:
                        if ( QuestHelper.hasQuestItem( player, MIONS_LETTER ) ) {
                            await QuestHelper.takeSingleItem( player, MIONS_LETTER, 1 )
                            await QuestHelper.giveSingleItem( player, BEAR_PICTURE, 1 )

                            state.setConditionWithSound( 5, true )
                            state.setVariable( variableNames.flagValue, 0 )

                            return this.getPath( '30556-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, BEAR_PICTURE ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, HONEY_JAR ) < 5 ) {
                                return this.getPath( '30556-02.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, -1, BEAR_PICTURE, HONEY_JAR )
                            await QuestHelper.giveSingleItem( player, TARANTULA_PICTURE, 1 )

                            state.setConditionWithSound( 7, true )
                            return this.getPath( '30556-03.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, TARANTULA_PICTURE ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, BEAD ) < 20 ) {
                                return this.getPath( '30556-04.html' )
                            }

                            return this.getPath( '30556-05a.html' )
                        }

                        let hasParcelOne = QuestHelper.hasQuestItem( player, BEAD_PARCEL )
                        let hasParcelTwo = QuestHelper.hasQuestItem( player, BEAD_PARCEL2 )

                        if ( hasParcelOne && !hasParcelTwo ) {
                            return this.getPath( '30556-06a.html' )
                        }

                        if ( hasParcelTwo
                                && !hasParcelOne
                                && state.isMemoState( 2 ) ) {
                            return this.getPath( '30556-06c.html' )
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, ROUTS_TELEPORT_SCROLL, SUCCUBUS_UNDIES ) ) {
                            return this.getPath( '30556-07.html' )
                        }
                        break

                    case WAREHOUSE_KEEPER_RAUT:
                        if ( QuestHelper.hasQuestItem( player, BEAD_PARCEL ) ) {
                            return this.getPath( '30316-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, ROUTS_TELEPORT_SCROLL ) ) {
                            return this.getPath( '30316-04.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, SUCCUBUS_UNDIES ) ) {
                            await QuestHelper.giveAdena( player, 81900, true )
                            await QuestHelper.giveSingleItem( player, RING_OF_RAVEN, 1 )

                            if ( player.getLevel() >= 20 ) {
                                await QuestHelper.addExpAndSp( player, 160267, 17706 )
                            } else if ( player.getLevel() === 19 ) {
                                await QuestHelper.addExpAndSp( player, 228064, 21055 )
                            } else {
                                await QuestHelper.addExpAndSp( player, 295862, 24404 )
                            }

                            await state.exitQuest( false, true )
                            player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )

                            return this.getPath( '30316-05.html' )
                        }

                        break

                    case TORAI:
                        if ( QuestHelper.hasQuestItem( player, ROUTS_TELEPORT_SCROLL ) ) {
                            return this.getPath( '30557-01.html' )
                        }

                        break

                    case WAREHOUSE_CHIEF_YASENI:
                        if ( QuestHelper.hasQuestItem( player, BEAD_PARCEL2 )
                                && !QuestHelper.hasQuestItem( player, BEAD_PARCEL )
                                && state.isMemoState( 2 ) ) {
                            return this.getPath( '31958-01.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}