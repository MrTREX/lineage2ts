import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2ItemInstance } from '../../gameService/models/items/instance/L2ItemInstance'
import { L2Summon } from '../../gameService/models/actor/L2Summon'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const BLACKSMITH_PUSHKIN = 30300
const PIOTUR = 30597
const ELDER_CASIAN = 30612
const KATARI = 30668
const MAGISTER_JOAN = 30718
const BLACK_JUDGE = 30981
const RATMAN_SCAVENGERS_SKULL = 4326
const TUREK_WAR_HOUNDS_TAIL = 4327
const TYRANT_KINGPINS_HEART = 4328
const TRISALIM_TARANTULAS_VENOM_SAC = 4329
const PENITENTS_MANACLES1 = 4330
const MANUAL_OF_MANACLES = 4331
const PENITENTS_MANACLES = 4425
const MANACLES_OF_PENITENT = 4426
const SILVER_NUGGET = 1873
const ADAMANTITE_NUGGET = 1877
const COKES = 1879
const STEEL = 1880
const BLACKSMITHS_FRAME = 1892
const SCAVENGER_WERERAT = 20039
const TYRANT_KINGPIN = 20193
const TUREK_WAR_HOUND = 20494
const TRISALIM_TARANTULA = 20561

const variableNames = {
    value: 'vl'
}

export class RepentYourSins extends ListenerLogic {
    constructor() {
        super( 'Q00422_RepentYourSins', 'listeners/tracked-400/RepentYourSins.ts' )
        this.questId = 422
        this.questItemIds = [
            RATMAN_SCAVENGERS_SKULL,
            TUREK_WAR_HOUNDS_TAIL,
            TYRANT_KINGPINS_HEART,
            TRISALIM_TARANTULAS_VENOM_SAC,
            PENITENTS_MANACLES1,
            MANUAL_OF_MANACLES,
            PENITENTS_MANACLES,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ BLACK_JUDGE ]
    }

    getTalkIds(): Array<number> {
        return [
            BLACK_JUDGE,
            BLACKSMITH_PUSHKIN,
            PIOTUR,
            ELDER_CASIAN,
            KATARI,
            MAGISTER_JOAN,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00422_RepentYourSins'
    }

    getAttackableKillIds(): Array<number> {
        return [
            SCAVENGER_WERERAT,
            TYRANT_KINGPIN,
            TUREK_WAR_HOUND,
            TRISALIM_TARANTULA,
        ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT':
                state.startQuest()
                if ( player.getLevel() > 20 && player.getLevel() < 31 ) {

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ACCEPT )
                    state.setMemoState( 2 )
                    state.setConditionWithSound( 3 )

                    return this.getPath( '30981-04.htm' )
                }

                if ( player.getLevel() < 21 ) {
                    state.setMemoState( 1 )
                    state.setVariable( variableNames.value, 0 )
                    state.setConditionWithSound( 2 )

                    return this.getPath( '30981-03.htm' )
                }

                if ( player.getLevel() > 30 && player.getLevel() < 41 ) {
                    state.setMemoState( 3 )
                    state.setConditionWithSound( 4 )

                    return this.getPath( '30981-05.htm' )
                }

                state.setMemoState( 4 )
                state.setConditionWithSound( 5 )
                return this.getPath( '30981-06.htm' )

            case '30981-11.html':
                if ( state.getMemoState() >= 9
                        && state.getMemoState() <= 12
                        && QuestHelper.hasAtLeastOneQuestItem( player, MANACLES_OF_PENITENT, PENITENTS_MANACLES1 ) ) {
                    await QuestHelper.takeMultipleItems( player, 1, MANACLES_OF_PENITENT, PENITENTS_MANACLES1 )
                    await QuestHelper.giveSingleItem( player, PENITENTS_MANACLES, 1 )

                    state.setVariable( variableNames.value, player.getLevel() )
                    state.setConditionWithSound( 16 )

                    break
                }

                return

            case '30981-14.html':
            case '30981-17.html':
                if ( state.getMemoState() >= 9 && state.getMemoState() <= 12 ) {
                    break
                }

                return

            case '30981-15t.html':
                let petItem: L2ItemInstance = player.getInventory().getItemByItemId( PENITENTS_MANACLES )
                let petLevel = !petItem ? 0 : petItem.getEnchantLevel()
                if ( state.getMemoState() >= 9
                        && state.getMemoState() <= 12
                        && petLevel > state.getVariable( variableNames.value ) ) {
                    let summon: L2Summon = player.getSummon()
                    if ( summon ) {
                        break
                    }

                    let value: number
                    if ( player.getLevel() > state.getVariable( variableNames.value ) ) {
                        value = petLevel - state.getVariable( variableNames.value ) - ( player.getLevel() - state.getVariable( variableNames.value ) )
                    } else {
                        value = petLevel - state.getVariable( variableNames.value )
                    }

                    if ( petItem ) {
                        await QuestHelper.takeSingleItem( player, PENITENTS_MANACLES, -1 )
                    }

                    await QuestHelper.giveSingleItem( player, MANACLES_OF_PENITENT, 1 )

                    let chanceValue = _.random( Math.max( value, 1 ) - 1 ) + 1
                    if ( player.getPkKills() <= chanceValue ) {

                        player.setPkKills( 0 )
                        await state.exitQuest( true, true )

                        return this.getPath( '30981-15.html' )
                    }

                    player.setPkKills( player.getPkKills() - chanceValue )
                    state.setVariable( variableNames.value, 0 )

                    return this.getPath( '30981-16.html' )
                }

                break

            case '30981-18.html':
                if ( state.getMemoState() >= 9 && state.getMemoState() <= 12 ) {
                    await state.exitQuest( true, true )
                    break
                }

                return


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case SCAVENGER_WERERAT:
                if ( state.isMemoState( 5 ) ) {
                    await QuestHelper.rewardUpToLimit( player, RATMAN_SCAVENGERS_SKULL, 1, 10, data.isChampion )
                }

                return

            case TYRANT_KINGPIN:
                if ( state.isMemoState( 7 ) && !QuestHelper.hasQuestItem( player, TYRANT_KINGPINS_HEART ) ) {
                    await QuestHelper.giveSingleItem( player, TYRANT_KINGPINS_HEART, 1 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                }

                return

            case TUREK_WAR_HOUND:
                if ( state.isMemoState( 6 ) ) {
                    await QuestHelper.rewardUpToLimit( player, TUREK_WAR_HOUNDS_TAIL, 1, 10, data.isChampion )
                }

                return

            case TRISALIM_TARANTULA:
                if ( state.isMemoState( 8 ) ) {
                    await QuestHelper.rewardUpToLimit( player, TRISALIM_TARANTULAS_VENOM_SAC, 1, 3, data.isChampion )
                }

                return
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === BLACK_JUDGE ) {
                    if ( player.getPkKills() === 0 ) {
                        return this.getPath( '30981-01.htm' )
                    }

                    return this.getPath( '30981-02.htm' )
                }

                break

            case QuestStateValues.STARTED:
                let memoState = state.getMemoState()
                switch ( data.characterNpcId ) {
                    case BLACK_JUDGE:
                        if ( memoState === 1000 ) {
                            await QuestHelper.takeSingleItem( player, PENITENTS_MANACLES, 1 )
                            break
                        }

                        if ( memoState < 9 ) {
                            return this.getPath( '30981-07.html' )
                        }

                        if ( memoState <= 12 ) {
                            if ( !QuestHelper.hasAtLeastOneQuestItem( player, MANUAL_OF_MANACLES, MANACLES_OF_PENITENT, PENITENTS_MANACLES1, PENITENTS_MANACLES ) ) {
                                await QuestHelper.giveSingleItem( player, MANUAL_OF_MANACLES, 1 )
                                state.setConditionWithSound( 14, true )

                                return this.getPath( '30981-08.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, MANUAL_OF_MANACLES )
                                    && !QuestHelper.hasAtLeastOneQuestItem( player, MANACLES_OF_PENITENT, PENITENTS_MANACLES1, PENITENTS_MANACLES ) ) {
                                return this.getPath( '30981-09.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, PENITENTS_MANACLES1 )
                                    && !QuestHelper.hasAtLeastOneQuestItem( player, MANUAL_OF_MANACLES, MANACLES_OF_PENITENT, PENITENTS_MANACLES ) ) {
                                return this.getPath( '30981-10.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, PENITENTS_MANACLES ) ) {
                                let petItem: L2ItemInstance = player.getInventory().getItemByItemId( PENITENTS_MANACLES )
                                let petLevel = !petItem ? 0 : petItem.getEnchantLevel()

                                if ( petLevel < ( state.getVariable( variableNames.value ) + 1 ) ) {
                                    return this.getPath( '30981-12.html' )
                                }

                                return this.getPath( '30981-13.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, MANACLES_OF_PENITENT )
                                    && !QuestHelper.hasQuestItem( player, PENITENTS_MANACLES ) ) {
                                return this.getPath( '30981-16t.html' )
                            }
                        }

                        break

                    case BLACKSMITH_PUSHKIN:
                        if ( memoState >= 9 && memoState <= 12 ) {
                            if ( !QuestHelper.hasAtLeastOneQuestItem( player, PENITENTS_MANACLES1, PENITENTS_MANACLES, MANACLES_OF_PENITENT )
                                    && QuestHelper.hasQuestItem( player, MANUAL_OF_MANACLES ) ) {
                                if ( QuestHelper.hasQuestItem( player, BLACKSMITHS_FRAME )
                                        && QuestHelper.getQuestItemsCount( player, STEEL ) >= 5
                                        && QuestHelper.getQuestItemsCount( player, ADAMANTITE_NUGGET ) >= 2
                                        && QuestHelper.getQuestItemsCount( player, SILVER_NUGGET ) >= 10
                                        && QuestHelper.getQuestItemsCount( player, COKES ) >= 10 ) {
                                    await QuestHelper.takeMultipleItems( player, 10, SILVER_NUGGET, COKES )
                                    await QuestHelper.takeSingleItem( player, ADAMANTITE_NUGGET, 2 )
                                    await QuestHelper.takeSingleItem( player, STEEL, 5 )
                                    await QuestHelper.takeMultipleItems( player, 1, BLACKSMITHS_FRAME, MANUAL_OF_MANACLES )
                                    await QuestHelper.giveSingleItem( player, PENITENTS_MANACLES1, 1 )

                                    state.setConditionWithSound( 15, true )
                                    return this.getPath( '30300-01.html' )
                                }

                                return this.getPath( '30300-02.html' )
                            }

                            return this.getPath( '30300-03.html' )
                        }
                        break

                    case PIOTUR:
                        switch ( memoState ) {
                            case 2:
                                state.setMemoState( 6 )
                                state.setConditionWithSound( 7, true )

                                return this.getPath( '30597-01.html' )

                            case 6:
                                if ( QuestHelper.getQuestItemsCount( player, TUREK_WAR_HOUNDS_TAIL ) < 10 ) {
                                    return this.getPath( '30597-02.html' )
                                }

                                await QuestHelper.takeSingleItem( player, TUREK_WAR_HOUNDS_TAIL, -1 )

                                state.setMemoState( 10 )
                                state.setConditionWithSound( 11, true )

                                return this.getPath( '30597-03.html' )

                            case 10:
                                return this.getPath( '30597-04.html' )
                        }

                        break

                    case ELDER_CASIAN:
                        switch ( memoState ) {
                            case 3:
                                state.setMemoState( 7 )
                                state.setConditionWithSound( 8, true )

                                return this.getPath( '30612-01.html' )

                            case 7:
                                if ( !QuestHelper.hasQuestItem( player, TYRANT_KINGPINS_HEART ) ) {
                                    return this.getPath( '30612-02.html' )
                                }

                                await QuestHelper.takeSingleItem( player, TYRANT_KINGPINS_HEART, -1 )

                                state.setMemoState( 11 )
                                state.setConditionWithSound( 12, true )

                                return this.getPath( '30612-03.html' )

                            case 11:
                                return this.getPath( '30612-04.html' )
                        }

                        break

                    case KATARI:
                        switch ( memoState ) {
                            case 1:
                                state.setMemoState( 5 )
                                state.setConditionWithSound( 6, true )

                                return this.getPath( '30668-01.html' )

                            case 5:
                                if ( QuestHelper.getQuestItemsCount( player, RATMAN_SCAVENGERS_SKULL ) < 10 ) {
                                    return this.getPath( '30668-02.html' )
                                }

                                await QuestHelper.takeSingleItem( player, RATMAN_SCAVENGERS_SKULL, -1 )

                                state.setMemoState( 9 )
                                state.setConditionWithSound( 10, true )

                                return this.getPath( '30668-03.html' )

                            case 9:
                                return this.getPath( '30668-04.html' )
                        }

                        break

                    case MAGISTER_JOAN:
                        switch ( memoState ) {
                            case 4:
                                state.setMemoState( 8 )
                                state.setConditionWithSound( 9, true )

                                return this.getPath( '30718-01.html' )

                            case 8:
                                if ( QuestHelper.getQuestItemsCount( player, TRISALIM_TARANTULAS_VENOM_SAC ) < 3 ) {
                                    return this.getPath( '30718-02.html' )
                                }

                                await QuestHelper.takeSingleItem( player, TRISALIM_TARANTULAS_VENOM_SAC, -1 )

                                state.setMemoState( 12 )
                                state.setConditionWithSound( 13, true )

                                return this.getPath( '30718-03.html' )

                            case 12:
                                return this.getPath( '30718-04.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}