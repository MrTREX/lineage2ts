import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { QuestHelper } from '../helpers/QuestHelper'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'

const PARINA = 30391
const EARTH_SNAKE = 30409
const WASTELAND_LIZARDMAN = 30410
const FLAME_SALAMANDER = 30411
const WIND_SYLPH = 30412
const WATER_UNDINE = 30413
const MAP_OF_LUSTER = 1280
const KEY_OF_FLAME = 1281
const FLAME_EARING = 1282
const BROKEN_BRONZE_MIRROR = 1283
const WIND_FEATHER = 1284
const WIND_BANGLE = 1285
const RAMAS_DIARY = 1286
const SPARKLE_PEBBLE = 1287
const WATER_NECKLACE = 1288
const RUSTY_COIN = 1289
const RED_SOIL = 1290
const EARTH_RING = 1291
const BEAD_OF_SEASON = 1292
const RED_BEAR = 20021
const RATMAN_WARRIOR = 20359
const WATER_SEER = 27030
const minimumLevel = 18

export class PathOfTheHumanWizard extends ListenerLogic {
    constructor() {
        super( 'Q00404_PathOfTheHumanWizard', 'listeners/tracked-400/PathOfTheHumanWizard.ts' )
        this.questId = 404
        this.questItemIds = [
            MAP_OF_LUSTER,
            KEY_OF_FLAME,
            FLAME_EARING,
            BROKEN_BRONZE_MIRROR,
            WIND_FEATHER,
            WIND_BANGLE,
            RAMAS_DIARY,
            SPARKLE_PEBBLE,
            WATER_NECKLACE,
            RUSTY_COIN,
            RED_SOIL,
            EARTH_RING,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ PARINA ]
    }

    getTalkIds(): Array<number> {
        return [
            PARINA,
            EARTH_SNAKE,
            WASTELAND_LIZARDMAN,
            FLAME_SALAMANDER,
            WIND_SYLPH,
            WATER_UNDINE,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            RED_BEAR,
            RATMAN_WARRIOR,
            WATER_SEER,
        ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( player.getClassId() === ClassIdValues.mage.id ) {
                    if ( player.getLevel() >= minimumLevel ) {
                        if ( QuestHelper.hasQuestItem( player, BEAD_OF_SEASON ) ) {
                            return this.getPath( '30391-03.htm' )
                        }

                        state.startQuest()
                        return this.getPath( '30391-07.htm' )
                    }

                    return this.getPath( '30391-02.htm' )
                }

                if ( player.getClassId() === ClassIdValues.wizard.id ) {
                    return this.getPath( '30391-02a.htm' )
                }

                return this.getPath( '30391-01.htm' )

            case '30410-02.html':
                break

            case '30410-03.html':
                await QuestHelper.giveSingleItem( player, WIND_FEATHER, 1 )
                state.setConditionWithSound( 6, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00404_PathOfTheHumanWizard'
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isStarted() || NpcVariablesManager.get( data.targetId, this.getName() ) !== 1 ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case RED_BEAR:
                if ( Math.random() < QuestHelper.getAdjustedChance( RED_SOIL, 0.2, data.isChampion )
                        && QuestHelper.hasQuestItem( player, RUSTY_COIN )
                        && !QuestHelper.hasQuestItem( player, RED_SOIL ) ) {
                    await QuestHelper.giveSingleItem( player, RED_SOIL, 1 )
                    state.setConditionWithSound( 12, true )
                }

                return

            case RATMAN_WARRIOR:
                if ( Math.random() < QuestHelper.getAdjustedChance( KEY_OF_FLAME, 0.8, data.isChampion )
                        && QuestHelper.hasQuestItem( player, MAP_OF_LUSTER )
                        && !QuestHelper.hasQuestItem( player, KEY_OF_FLAME ) ) {
                    await QuestHelper.giveSingleItem( player, KEY_OF_FLAME, 1 )
                    state.setConditionWithSound( 3, true )
                }

                return

            case WATER_SEER:
                if ( Math.random() < QuestHelper.getAdjustedChance( SPARKLE_PEBBLE, 0.8, data.isChampion )
                        && QuestHelper.hasQuestItem( player, RAMAS_DIARY )
                        && QuestHelper.getQuestItemsCount( player, SPARKLE_PEBBLE ) < 2 ) {
                    await QuestHelper.rewardAndProgressState( player, state, SPARKLE_PEBBLE, 1, 2, data.isChampion, 9 )
                }

                return
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === PARINA ) {
                    return this.getPath( '30391-04.htm' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case PARINA:
                        if ( !QuestHelper.hasQuestItems( player, FLAME_EARING, WIND_BANGLE, WATER_NECKLACE, EARTH_RING ) ) {
                            return this.getPath( '30391-05.html' )
                        }

                        await QuestHelper.giveAdena( player, 163800, true )
                        await QuestHelper.takeMultipleItems( player, -1, FLAME_EARING, WIND_BANGLE, WATER_NECKLACE, EARTH_RING )

                        if ( !QuestHelper.hasQuestItem( player, BEAD_OF_SEASON ) ) {
                            await QuestHelper.giveSingleItem( player, BEAD_OF_SEASON, 1 )
                        }

                        if ( player.getLevel() >= 20 ) {
                            await QuestHelper.addExpAndSp( player, 320534, 23152 )
                        } else if ( player.getLevel() === 19 ) {
                            await QuestHelper.addExpAndSp( player, 456128, 29850 )
                        } else {
                            await QuestHelper.addExpAndSp( player, 591724, 36548 )
                        }

                        await state.exitQuest( false, true )
                        player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )

                        return this.getPath( '30391-06.html' )

                    case EARTH_SNAKE:
                        if ( QuestHelper.hasQuestItem( player, RUSTY_COIN ) ) {
                            if ( !QuestHelper.hasQuestItem( player, RED_SOIL ) ) {
                                return this.getPath( '30409-02.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, -1, RUSTY_COIN, RED_SOIL )

                            if ( !QuestHelper.hasQuestItem( player, EARTH_RING ) ) {
                                await QuestHelper.giveSingleItem( player, EARTH_RING, 1 )
                            }

                            state.setConditionWithSound( 13, true )
                            return this.getPath( '30409-03.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, EARTH_RING ) ) {
                            return this.getPath( '30409-04.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, WATER_NECKLACE ) ) {
                            await QuestHelper.giveSingleItem( player, RUSTY_COIN, 1 )

                            state.setConditionWithSound( 11, true )
                            return this.getPath( '30409-01.html' )
                        }

                        break

                    case WASTELAND_LIZARDMAN:
                        if ( QuestHelper.hasQuestItem( player, BROKEN_BRONZE_MIRROR ) ) {
                            if ( !QuestHelper.hasQuestItem( player, WIND_FEATHER ) ) {
                                return this.getPath( '30410-01.html' )
                            }

                            return this.getPath( '30410-04.html' )
                        }

                        break

                    case FLAME_SALAMANDER:
                        if ( QuestHelper.hasQuestItem( player, MAP_OF_LUSTER ) ) {
                            if ( !QuestHelper.hasQuestItem( player, KEY_OF_FLAME ) ) {
                                return this.getPath( '30411-02.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, -1, MAP_OF_LUSTER, KEY_OF_FLAME )

                            if ( !QuestHelper.hasQuestItem( player, FLAME_EARING ) ) {
                                await QuestHelper.giveSingleItem( player, FLAME_EARING, 1 )
                            }

                            state.setConditionWithSound( 4, true )
                            return this.getPath( '30411-03.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, FLAME_EARING ) ) {
                            return this.getPath( '30411-04.html' )
                        }

                        await QuestHelper.giveSingleItem( player, MAP_OF_LUSTER, 1 )
                        state.setConditionWithSound( 2, true )

                        return this.getPath( '30411-01.html' )

                    case WIND_SYLPH:
                        if ( QuestHelper.hasQuestItem( player, BROKEN_BRONZE_MIRROR ) ) {
                            if ( !QuestHelper.hasQuestItem( player, WIND_FEATHER ) ) {
                                return this.getPath( '30412-02.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, -1, BROKEN_BRONZE_MIRROR, WIND_FEATHER )

                            if ( !QuestHelper.hasQuestItem( player, WIND_BANGLE ) ) {
                                await QuestHelper.giveSingleItem( player, WIND_BANGLE, 1 )
                            }

                            state.setConditionWithSound( 7, true )
                            return this.getPath( '30412-03.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, WIND_BANGLE ) ) {
                            return this.getPath( '30412-04.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, FLAME_EARING ) ) {
                            await QuestHelper.giveSingleItem( player, BROKEN_BRONZE_MIRROR, 1 )

                            state.setConditionWithSound( 5, true )
                            return this.getPath( '30412-01.html' )
                        }

                        break

                    case WATER_UNDINE:
                        if ( QuestHelper.hasQuestItem( player, RAMAS_DIARY ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, SPARKLE_PEBBLE ) < 2 ) {
                                return this.getPath( '30413-02.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, -1, RAMAS_DIARY, SPARKLE_PEBBLE )

                            if ( !QuestHelper.hasQuestItem( player, WATER_NECKLACE ) ) {
                                await QuestHelper.giveSingleItem( player, WATER_NECKLACE, 1 )
                            }

                            state.setConditionWithSound( 10, true )
                            return this.getPath( '30413-03.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, WATER_NECKLACE ) ) {
                            return this.getPath( '30413-04.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, WIND_BANGLE ) ) {
                            await QuestHelper.giveSingleItem( player, RAMAS_DIARY, 1 )

                            state.setConditionWithSound( 8, true )
                            return this.getPath( '30413-01.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}