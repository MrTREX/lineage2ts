import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { DataManager } from '../../data/manager'
import { QuestType } from '../../gameService/enums/QuestType'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { L2Attackable } from '../../gameService/models/actor/L2Attackable'
import { PacketDispatcher } from '../../gameService/PacketDispatcher'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { ExQuestNpcLogList } from '../../gameService/packets/send/builder/ExQuestNpcLogList'

import _ from 'lodash'

const KELLEYIA = 32768
const KOOKABURRAS = [
    18878,
    18879,
]

const COUGARS = [
    18885,
    18886,
]

const BUFFALOS = [
    18892,
    18893,
]

const GRENDELS = [
    18899,
    18900,
]

const ICARUS_WEAPON_RECIPES = [
    10373,
    10374,
    10375,
    10376,
    10377,
    10378,
    10379,
    10380,
    10381,
]

const ICARUS_WEAPON_PIECES = [
    10397,
    10398,
    10399,
    10400,
    10401,
    10402,
    10403,
    10404,
    10405,
]

const variableNames = {
    overhits: 'oh',
    overhitsCritical: 'ohc',
    overhitsConsecutive: 'ohcon',
}

const minimumLevel = 81

export class PerfectForm extends ListenerLogic {
    constructor() {
        super( 'Q00458_PerfectForm', 'listeners/tracked-400/PerfectForm.ts' )
        this.questId = 458
    }

    getQuestStartIds(): Array<number> {
        return [ KELLEYIA ]
    }

    getTalkIds(): Array<number> {
        return [ KELLEYIA ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            ...KOOKABURRAS,
            ...COUGARS,
            ...BUFFALOS,
            ...GRENDELS,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00458_PerfectForm'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '32768-10.htm':
                state.startQuest()
                break

            case 'results1':
                if ( state.isCondition( 2 ) ) {
                    let overhitsTotal = state.getVariable( variableNames.overhits ) || 0
                    if ( overhitsTotal >= 35 ) {
                        return this.populateOverhitValues( '32768-14a.html', overhitsTotal )
                    }

                    if ( overhitsTotal >= 10 ) {
                        return this.populateOverhitValues( '32768-14b.html', overhitsTotal )
                    }

                    return this.populateOverhitValues( '32768-14c.html', overhitsTotal )
                }

                return QuestHelper.getNoQuestMessagePath()

            case 'results2':
                if ( state.isCondition( 2 ) ) {
                    let overhitsCritical: number = state.getVariable( variableNames.overhitsCritical ) || 0
                    if ( overhitsCritical >= 30 ) {
                        return this.populateOverhitValues( '32768-15a.html', overhitsCritical )
                    }

                    if ( overhitsCritical >= 5 ) {
                        return this.populateOverhitValues( '32768-15b.html', overhitsCritical )
                    }

                    return this.populateOverhitValues( '32768-15c.html', overhitsCritical )
                }

                return QuestHelper.getNoQuestMessagePath()

            case 'results3':
                if ( state.isCondition( 2 ) ) {
                    let overhitsConsecutive = state.getVariable( variableNames.overhitsConsecutive ) || 0
                    if ( overhitsConsecutive >= 20 ) {
                        return this.populateOverhitValues( '32768-16a.html', overhitsConsecutive )
                    }

                    if ( overhitsConsecutive >= 7 ) {
                        return this.populateOverhitValues( '32768-16b.html', overhitsConsecutive )
                    }

                    return this.populateOverhitValues( '32768-16c.html', overhitsConsecutive )
                }

                return QuestHelper.getNoQuestMessagePath()

            case '32768-17.html':
                if ( state.isCondition( 2 ) ) {
                    let player = L2World.getPlayer( data.playerId )
                    let overhitsConsecutive = state.getVariable( variableNames.overhitsConsecutive ) || 0

                    if ( overhitsConsecutive >= 20 ) {
                        await QuestHelper.rewardSingleItem( player, _.sample( ICARUS_WEAPON_RECIPES ), 1 )
                    } else if ( overhitsConsecutive >= 7 ) {
                        await QuestHelper.rewardSingleItem( player, _.sample( ICARUS_WEAPON_PIECES ), 5 )
                    } else {
                        await QuestHelper.rewardSingleItem( player, _.sample( ICARUS_WEAPON_PIECES ), 2 )

                        await QuestHelper.rewardSingleItem( player, 15482, 10 ) // Golden Spice Crate
                        await QuestHelper.rewardSingleItem( player, 15483, 10 ) // Crystal Spice Crate
                    }

                    await state.exitQuestWithType( QuestType.DAILY, true )
                    break
                }

                return QuestHelper.getNoQuestMessagePath()

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    populateOverhitValues( path: string, value: number ): string {
        return DataManager.getHtmlData().getItem( this.getPath( path ) )
                .replace( '<?number?>', value.toString() )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.COMPLETED:
                if ( !state.isNowAvailable() ) {
                    return this.getPath( '32768-18.htm' )
                }

                state.setState( QuestStateValues.CREATED )

            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() > minimumLevel ? '32768-01.htm' : '32768-00.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        let hasSome: boolean = _.some( [ '18879', '18886', '18893', '18900' ], ( id: string ) => {
                            return !!state.getVariable( id )
                        } )

                        if ( hasSome ) {
                            return this.getPath( '32768-12.html' )
                        }

                        return this.getPath( '32768-11.html' )

                    case 2:
                        return this.getPath( '32768-13.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isCondition( 1 ) ) {
            return
        }

        let npcId: number = [ KOOKABURRAS[ 0 ], COUGARS[ 0 ], BUFFALOS[ 0 ], GRENDELS[ 0 ] ].includes( data.npcId ) ? data.npcId + 1 : data.npcId

        let variableName = npcId.toString()
        let currentValue = state.getVariable( variableName ) || 0
        if ( currentValue < 10 ) {
            state.setVariable( variableName, currentValue + 1 )

            let mob: L2Attackable = L2World.getObjectById( data.targetId ) as L2Attackable
            if ( mob.isOverhit() ) {
                state.incrementVariable( variableNames.overhits, 1 )
                let maxHp = mob.getMaxHp()
                let overhitPercentage = ( maxHp + mob.getOverhitDamage() ) / maxHp
                if ( overhitPercentage >= 1.2 ) {
                    state.incrementVariable( variableNames.overhitsCritical, 1 )
                }

                state.incrementVariable( variableNames.overhitsConsecutive, 1 )
            } else {
                if ( state.getVariable( variableNames.overhitsConsecutive ) > 0 ) {
                    state.setVariable( variableNames.overhitsConsecutive, 0 )
                }
            }

            let shouldSetCondition: boolean = _.every( [ '18879', '18886', '18893', '18900' ], ( id: string ) => {
                return state.getVariable( id ) >= 10
            } )

            if ( shouldSetCondition ) {
                state.setConditionWithSound( 2, true )
            } else {
                PacketDispatcher.sendCopyData( data.playerId, SoundPacket.ITEMSOUND_QUEST_ITEMGET )
            }

            let log: ExQuestNpcLogList = new ExQuestNpcLogList( this.getId() )
            log.addRecord( 18879, state.getVariable( '18879' ) )
            log.addRecord( 18886, state.getVariable( '18886' ) )
            log.addRecord( 18893, state.getVariable( '18893' ) )
            log.addRecord( 18900, state.getVariable( '18900' ) )

            PacketDispatcher.sendOwnedData( data.playerId, log.getBuffer() )
        }
    }
}