import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import {
    AttackableKillEvent,
    EventType,
    ItemTalkingEvent,
    NpcGeneralEvent,
    NpcTalkEvent,
} from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestType } from '../../gameService/enums/QuestType'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { L2Npc } from '../../gameService/models/actor/L2Npc'

import _ from 'lodash'

type NpcRewardValues = [ number, number, number, number ] // npcId, exp, sp, adena
const npcRewards: Array<NpcRewardValues> = [
    [ 32596, 0, 0, 0 ],
    [ 30657, 15449, 17696, 42910 ],
    [ 30839, 189377, 21692, 52599 ],
    [ 30899, 249180, 28542, 69210 ],
    [ 31350, 249180, 28542, 69210 ],
    [ 30539, 19408, 47062, 169442 ],
    [ 30297, 24146, 58551, 210806 ],
    [ 31960, 15449, 17696, 42910 ],
    [ 31588, 15449, 17696, 42910 ],
]

const STRONGBOX = 15537
const BOOK = 15538
const BOOK2 = 15539
const minimumLevel = 82

const monsterDropChances: { [ npcId: number ]: number } = {
    22799: 0.009,
    22794: 0.006,
    22800: 0.010,
    22796: 0.009,
    22798: 0.009,
    22795: 0.008,
    22797: 0.007,
    22789: 0.005,
    22791: 0.004,
    22790: 0.005,
    22792: 0.004,
    22793: 0.005,
}

const variableNames = {
    chosenNpcId: 'cni',
}

export class Oath extends ListenerLogic {
    constructor() {
        super( 'Q00464_Oath', 'listeners/tracked-400/Oath.ts' )
        this.questId = 464
        this.questItemIds = [
            BOOK,
            BOOK2,
        ]
    }

    getTalkIds(): Array<number> {
        return npcRewards.map( ( values: NpcRewardValues ) => values[ 0 ] )
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterDropChances ).map( value => _.parseInt( value ) )
    }

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.ItemId,
                eventType: EventType.ItemTalking,
                method: this.onItemTalking.bind( this ),
                ids: [ STRONGBOX ],
            },
        ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32596-04.html':
                if ( !QuestHelper.hasQuestItem( player, BOOK ) ) {
                    return QuestHelper.getNoQuestMessagePath()
                }

                let conditionValue: number = _.random( 2, 8 )
                state.setVariable( variableNames.chosenNpcId, npcRewards[ conditionValue - 1 ][ 0 ] )
                state.setConditionWithSound( conditionValue, true )

                await QuestHelper.takeSingleItem( player, BOOK, 1 )
                await QuestHelper.giveSingleItem( player, BOOK2, 1 )

                switch ( conditionValue ) {
                    case 2:
                        return this.getPath( '32596-04.html' )

                    case 3:
                        return this.getPath( '32596-04a.html' )

                    case 4:
                        return this.getPath( '32596-04b.html' )

                    case 5:
                        return this.getPath( '32596-04c.html' )

                    case 6:
                        return this.getPath( '32596-04d.html' )

                    case 7:
                        return this.getPath( '32596-04e.html' )

                    case 8:
                        return this.getPath( '32596-04f.html' )

                    case 9:
                        return this.getPath( '32596-04g.html' )
                }

                break

            case 'end_quest':
                if ( !QuestHelper.hasQuestItem( player, BOOK2 ) ) {
                    return QuestHelper.getNoQuestMessagePath()
                }

                let index: number = state.getCondition() - 1
                let [ , exp, sp, adena ]: NpcRewardValues = npcRewards[ index ]

                await QuestHelper.addExpAndSp( player, exp, sp )
                await QuestHelper.giveAdena( player, adena, true )
                await state.exitQuestWithType( QuestType.DAILY, true )

                return this.getPath( `${ data.characterNpcId }-02.html` )

            case '32596-02.html':
            case '32596-03.html':
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00464_Oath'
    }

    async onItemTalking( data: ItemTalkingEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )

        switch ( state.getState() ) {
            case QuestStateValues.COMPLETED:
                if ( !state.isNowAvailable() ) {
                    return this.getPath( 'strongbox-03.html' )
                }

                state.setState( QuestStateValues.CREATED )

            case QuestStateValues.CREATED:
                let player = L2World.getPlayer( data.playerId )

                if ( player.getLevel() >= minimumLevel ) {
                    state.startQuest()

                    await QuestHelper.takeSingleItem( player, STRONGBOX, -1 )
                    await QuestHelper.giveSingleItem( player, BOOK, 1 )

                    return this.getPath( 'strongbox-01.htm' )
                }

                return this.getPath( 'strongbox-00.htm' )

            case QuestStateValues.STARTED:
                return this.getPath( 'strongbox-02.html' )
        }
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( Math.random() > QuestHelper.getAdjustedChance( STRONGBOX, monsterDropChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        let npc = L2World.getObjectById( data.targetId ) as L2Npc
        npc.dropSingleItem( STRONGBOX, 1, data.playerId )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )

        if ( !state.isStarted() ) {
            return QuestHelper.getNoQuestMessagePath()
        }

        if ( data.characterNpcId === npcRewards[ 0 ][ 0 ] ) {
            switch ( state.getCondition() ) {
                case 1:
                    return this.getPath( '32596-01.html' )

                case 2:
                    return this.getPath( '32596-05.html' )

                case 3:
                    return this.getPath( '32596-05a.html' )

                case 4:
                    return this.getPath( '32596-05b.html' )

                case 5:
                    return this.getPath( '32596-05c.html' )

                case 6:
                    return this.getPath( '32596-05d.html' )

                case 7:
                    return this.getPath( '32596-05e.html' )

                case 8:
                    return this.getPath( '32596-05f.html' )

                case 9:
                    return this.getPath( '32596-05g.html' )
            }

            return QuestHelper.getNoQuestMessagePath()
        }

        if ( state.getCondition() > 1 && state.getVariable( variableNames.chosenNpcId ) === data.characterNpcId ) {
            return this.getPath( `${ data.characterNpcId }-01.html` )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}