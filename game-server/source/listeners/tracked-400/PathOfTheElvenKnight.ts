import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'
import _ from 'lodash'

const BLACKSMITH_KLUTO = 30317
const MASTER_SORIUS = 30327
const SORIUS_LETTER = 1202
const KLUTO_BOX = 1203
const TOPAZ_PIECE = 1205
const EMERALD_PIECE = 1206
const KLUTO_MEMO = 1276
const ELVEN_KNIGHT_BROOCH = 1204
const minimumLevel = 18
const OL_MAHUM_NOVICE = 20782

const monsterChances: { [ npcId: number ]: number } = {
    20035: 0.7, // Tracker Skeleton
    20042: 0.7, // Tracker Skeleton Leader
    20045: 0.7, // Skeleton Scout
    20051: 0.7, // Skeleton Bowman
    20054: 0.7, // Ruin Spartoi
    20060: 0.7, // Salamander Noble
    [ OL_MAHUM_NOVICE ]: 0.5, // Ol Mahum Novice
}

export class PathOfTheElvenKnight extends ListenerLogic {
    constructor() {
        super( 'Q00406_PathOfTheElvenKnight', 'listeners/tracked-400/PathOfTheElvenKnight.ts' )
        this.questId = 406
        this.questItemIds = [
            SORIUS_LETTER,
            KLUTO_BOX,
            TOPAZ_PIECE,
            EMERALD_PIECE,
            KLUTO_MEMO,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ MASTER_SORIUS ]
    }

    getTalkIds(): Array<number> {
        return [ MASTER_SORIUS, BLACKSMITH_KLUTO ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00406_PathOfTheElvenKnight'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( player.getClassId() !== ClassIdValues.elvenFighter.id ) {
                    if ( player.getClassId() === ClassIdValues.elvenKnight.id ) {
                        return this.getPath( '30327-02a.htm' )
                    }

                    return this.getPath( '30327-02.htm' )
                }

                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '30327-03.htm' )
                }

                if ( QuestHelper.hasQuestItem( player, ELVEN_KNIGHT_BROOCH ) ) {
                    return this.getPath( '30327-04.htm' )
                }

                return this.getPath( '30327-05.htm' )

            case '30327-06.htm':
                state.startQuest()
                break

            case '30317-02.html':
                await QuestHelper.takeSingleItem( player, SORIUS_LETTER, -1 )

                if ( !QuestHelper.hasQuestItem( player, KLUTO_MEMO ) ) {
                    await QuestHelper.giveSingleItem( player, KLUTO_MEMO, 1 )
                }

                state.setConditionWithSound( 4, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let itemId: number = data.npcId === OL_MAHUM_NOVICE ? EMERALD_PIECE : TOPAZ_PIECE
        if ( Math.random() > QuestHelper.getAdjustedChance( itemId, monsterChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        if ( data.npcId === OL_MAHUM_NOVICE ) {
            if ( QuestHelper.hasQuestItem( player, KLUTO_MEMO ) ) {
                await QuestHelper.rewardAndProgressState( player, state, itemId, 1, 20, data.isChampion, 5 )
            }

            return
        }

        if ( QuestHelper.hasQuestItem( player, KLUTO_BOX ) ) {
            await QuestHelper.rewardAndProgressState( player, state, itemId, 1, 20, data.isChampion, 2 )
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === MASTER_SORIUS ) {
                    return this.getPath( '30327-01.htm' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case MASTER_SORIUS:
                        if ( !QuestHelper.hasQuestItem( player, KLUTO_BOX ) ) {
                            let topazAmount: number = QuestHelper.getQuestItemsCount( player, TOPAZ_PIECE )
                            if ( topazAmount === 0 ) {
                                return this.getPath( '30327-07.html' )
                            }

                            if ( topazAmount < 20 ) {
                                return this.getPath( '30327-08.html' )
                            }

                            if ( !QuestHelper.hasAtLeastOneQuestItem( player, KLUTO_MEMO, SORIUS_LETTER ) ) {
                                // TODO: implement me
                            }

                            if ( QuestHelper.hasAtLeastOneQuestItem( player, SORIUS_LETTER, KLUTO_MEMO ) ) {
                                return this.getPath( '30327-11.html' )
                            }

                            if ( !QuestHelper.hasQuestItem( player, SORIUS_LETTER ) ) {
                                await QuestHelper.giveSingleItem( player, SORIUS_LETTER, 1 )
                            }

                            state.setConditionWithSound( 3, true )
                            return this.getPath( '30327-09.html' )
                        }

                        await QuestHelper.giveAdena( player, 163800, true )
                        if ( !QuestHelper.hasQuestItem( player, ELVEN_KNIGHT_BROOCH ) ) {
                            await QuestHelper.giveSingleItem( player, ELVEN_KNIGHT_BROOCH, 1 )
                        }

                        if ( player.getLevel() >= 20 ) {
                            await QuestHelper.addExpAndSp( player, 320534, 23152 )
                        } else if ( player.getLevel() === 19 ) {
                            await QuestHelper.addExpAndSp( player, 456128, 29850 )
                        } else {
                            await QuestHelper.addExpAndSp( player, 591724, 33328 )
                        }

                        await state.exitQuest( false, true )
                        player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )

                        return this.getPath( '30327-10.html' )

                    case BLACKSMITH_KLUTO:
                        if ( QuestHelper.hasQuestItem( player, KLUTO_BOX ) ) {
                            return this.getPath( '30317-06.html' )
                        }

                        if ( QuestHelper.getQuestItemsCount( player, TOPAZ_PIECE ) < 20 ) {
                            break
                        }

                        if ( QuestHelper.hasQuestItem( player, SORIUS_LETTER ) ) {
                            return this.getPath( '30317-01.html' )
                        }

                        if ( !QuestHelper.hasQuestItem( player, KLUTO_MEMO ) ) {
                            break
                        }

                        let emeraldAmount = QuestHelper.getQuestItemsCount( player, EMERALD_PIECE )
                        if ( emeraldAmount === 0 ) {
                            return this.getPath( '30317-03.html' )
                        }

                        if ( emeraldAmount < 20 ) {
                            return this.getPath( '30317-04.html' )
                        }

                        if ( !QuestHelper.hasQuestItem( player, KLUTO_BOX ) ) {
                            await QuestHelper.giveSingleItem( player, KLUTO_BOX, 1 )
                        }

                        await QuestHelper.takeMultipleItems( player, -1, TOPAZ_PIECE, EMERALD_PIECE, KLUTO_MEMO )

                        state.setConditionWithSound( 6, true )
                        return this.getPath( '30317-05.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}