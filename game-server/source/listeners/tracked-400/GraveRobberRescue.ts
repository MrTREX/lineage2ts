import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestType } from '../../gameService/enums/QuestType'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { L2Attackable } from '../../gameService/models/actor/L2Attackable'
import { ExShowScreenMessage } from '../../gameService/packets/send/ExShowScreenMessage'
import { AIEffectHelper } from '../../gameService/aicontroller/helpers/AIEffectHelper'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const KANEMIKA = 32650
const WARRIOR = 32651
const WARRIOR_MON = 22741
const EVIDENCE_OF_MIGRATION = 14876
const minimumLevel = 80

const eventNames = {
    despawn: 'ds',
}

export class GraveRobberRescue extends ListenerLogic {
    constructor() {
        super( 'Q00450_GraveRobberRescue', 'listeners/tracked-400/GraveRobberRescue.ts' )
        this.questId = 450
        this.questItemIds = [ EVIDENCE_OF_MIGRATION ]
    }

    getQuestStartIds(): Array<number> {
        return [ KANEMIKA ]
    }

    getTalkIds(): Array<number> {
        return [ KANEMIKA, WARRIOR ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00450_GraveRobberRescue'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '32650-04.htm':
            case '32650-05.htm':
            case '32650-06.html':
                break

            case '32650-07.htm':
                state.startQuest()
                break

            case eventNames.despawn:
                let npc = L2World.getObjectById( data.characterId ) as L2Npc
                npc.setIsBusy( false )
                await npc.deleteMe()
                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        if ( data.characterNpcId === KANEMIKA ) {
            switch ( state.getState() ) {
                case QuestStateValues.COMPLETED:
                    if ( !state.isNowAvailable() ) {
                        return this.getPath( '32650-03.html' )
                    }

                    state.setState( QuestStateValues.CREATED )
                case QuestStateValues.CREATED:
                    return this.getPath( player.getLevel() >= minimumLevel ? '32650-01.htm' : '32650-02.htm' )

                case QuestStateValues.STARTED:
                    if ( state.isCondition( 1 ) ) {
                        return this.getPath( !QuestHelper.hasQuestItem( player, EVIDENCE_OF_MIGRATION ) ? '32650-08.html' : '32650-09.html' )
                    }

                    await QuestHelper.giveAdena( player, 65000, true )
                    await state.exitQuestWithType( QuestType.DAILY, true )

                    return this.getPath( '32650-10.html' )
            }

            return QuestHelper.getNoQuestMessagePath()
        }

        if ( state.isCondition( 1 ) ) {
            let npc = L2World.getObjectById( data.characterId ) as L2Npc
            if ( npc.isBusy() ) {
                return
            }

            if ( Math.random() < 0.66 ) {
                await QuestHelper.rewardSingleItem( player, EVIDENCE_OF_MIGRATION, 1 )
                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

                AIEffectHelper.notifyMoveWithCoordinates( npc, npc.getX() + 100, npc.getY() + 100, npc.getZ(), 0, npc.getInstanceId() )
                npc.setIsBusy( true )

                this.startQuestTimer( eventNames.despawn, 3000, data.characterId, data.playerId )

                if ( QuestHelper.getQuestItemsCount( player, EVIDENCE_OF_MIGRATION ) >= 10 ) {
                    state.setConditionWithSound( 2, true )
                }

                return this.getPath( '32651-01.html' )
            }

            BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, Math.random() < 0.50 ? NpcStringIds.GRUNT_OH : NpcStringIds.GRUNT_WHATS_WRONG_WITH_ME )
            await npc.deleteMe()

            let monster: L2Attackable = QuestHelper.addGenericSpawn( null, WARRIOR_MON, npc.getX(), npc.getY(), npc.getZ(), npc.getHeading(), true, 600000 ) as L2Attackable
            monster.setRunning()

            AIEffectHelper.notifyAttacked( monster, player, 999 )
            player.sendOwnedData( ExShowScreenMessage.fromNpcMessageId( NpcStringIds.THE_GRAVE_ROBBER_WARRIOR_HAS_BEEN_FILLED_WITH_DARK_ENERGY_AND_IS_ATTACKING_YOU, 5, 5000, null ) )

            return
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}