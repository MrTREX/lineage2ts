import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Attackable } from '../../gameService/models/actor/L2Attackable'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'

import _ from 'lodash'

const npcIds: Array<number> = [
    31562, // Klufe
    31563, // Perelin
    31564, // Mishini
    31565, // Ogord
    31566, // Ropfi
    31567, // Bleaker
    31568, // Pamfus
    31569, // Cyano
    31570, // Lanosco
    31571, // Hufs
    31572, // O'Fulle
    31573, // Monakan
    31574, // Willie
    31575, // Litulon
    31576, // Berix
    31577, // Linnaeus
    31578, // Hilgendorf
    31579, // Klaus
    31696, // Platis
    31697, // Eindarkner
    31989, // Batidae
    32007, // Galba
    32348, // Burang
]

/*
    Npc Id mapping to values [ chance, quantity ]
    Chance value is from 1 to 1000
    Quantity is amount of Sweet Fluid dropped from a monster.
 */
type MonsterReward = [ number, number ]
const dropMap: { [ npcId: number ]: MonsterReward } = {
    20005: [ 45, 1 ], // Imp Elder
    20013: [ 100, 1 ], // Dryad
    20016: [ 100, 1 ], // Stone Golem
    20017: [ 115, 1 ], // Vuku Orc Fighter
    20030: [ 105, 1 ], // Langk Lizardman
    20132: [ 70, 1 ], // Werewolf
    20038: [ 135, 1 ], // Venomous Spider
    20044: [ 125, 1 ], // Lirein Elder
    20046: [ 100, 1 ], // Stink Zombie
    20047: [ 100, 1 ], // Sukar Wererat Leader
    20050: [ 140, 1 ], // Arachnid Predator
    20058: [ 140, 1 ], // Ol Mahum Guard
    20063: [ 160, 1 ], // Ol Mahum Shooter
    20066: [ 170, 1 ], // Ol Mahum Captain
    20070: [ 180, 1 ], // Lesser Basilisk
    20074: [ 195, 1 ], // Androscorpio
    20077: [ 205, 1 ], // Androscorpio Hunter
    20078: [ 205, 1 ], // Whispering Wind
    20079: [ 205, 1 ], // Ant
    20080: [ 220, 1 ], // Ant Captain
    20081: [ 370, 1 ], // Ant Overseer
    20083: [ 245, 1 ], // Granite Golem
    20084: [ 255, 1 ], // Ant Patrol
    20085: [ 265, 1 ], // Puncher
    20087: [ 565, 1 ], // Ant Soldier
    20088: [ 605, 1 ], // Ant Warrior Captain
    20089: [ 250, 1 ], // Noble Ant
    20100: [ 85, 1 ], // Skeleton Archer
    20103: [ 110, 1 ], // Giant Spider
    20105: [ 110, 1 ], // Dark Horror
    20115: [ 190, 1 ], // Undine Noble
    20120: [ 20, 1 ], // Wolf
    20131: [ 45, 1 ], // Orc Grunt
    20135: [ 360, 1 ], // Alligator
    20157: [ 235, 1 ], // Marsh Stakato
    20162: [ 195, 1 ], // Ogre
    20176: [ 280, 1 ], // Wyrm
    20211: [ 170, 1 ], // Ol Mahum Captain
    20225: [ 160, 1 ], // Giant Mist Leech
    20227: [ 180, 1 ], // Horror Mist Ripper
    20230: [ 260, 1 ], // Marsh Stakato Worker
    20232: [ 245, 1 ], // Marsh Stakato Soldier
    20234: [ 290, 1 ], // Marsh Stakato Drone
    20241: [ 700, 1 ], // Hunter Gargoyle
    20267: [ 215, 1 ], // Breka Orc
    20268: [ 295, 1 ], // Breka Orc Archer
    20269: [ 255, 1 ], // Breka Orc Shaman
    20270: [ 365, 1 ], // Breka Orc Overlord
    20271: [ 295, 1 ], // Breka Orc Warrior
    20286: [ 700, 1 ], // Hunter Gargoyle
    20308: [ 110, 1 ], // Hook Spider
    20312: [ 45, 1 ], // Rakeclaw Imp Hunter
    20317: [ 20, 1 ], // Black Wolf
    20324: [ 85, 1 ], // Goblin Brigand Lieutenant
    20333: [ 100, 1 ], // Greystone Golem
    20341: [ 100, 1 ], // Undead Slave
    20346: [ 85, 1 ], // Darkstone Golem
    20349: [ 850, 1 ], // Cave Bat
    20356: [ 165, 1 ], // Langk Lizardman Leader
    20357: [ 140, 1 ], // Langk Lizardman Lieutenant
    20363: [ 70, 1 ], // Maraku Werewolf
    20368: [ 85, 1 ], // Grave Keeper
    20371: [ 100, 1 ], // Mist Terror
    20386: [ 85, 1 ], // Balor Orc Fighter
    20389: [ 90, 1 ], // Boogle Ratman
    20403: [ 110, 1 ], // Hunter Tarantula
    20404: [ 95, 1 ], // Silent Horror
    20433: [ 100, 1 ], // Festering Bat
    20436: [ 140, 1 ], // Ol Mahum Supplier
    20448: [ 45, 1 ], // Utuku Orc Grunt
    20456: [ 20, 1 ], // Ashen Wolf
    20463: [ 85, 1 ], // Dungeon Skeleton Archer
    20470: [ 45, 1 ], // Kaboo Orc Grunt
    20471: [ 85, 1 ], // Kaboo Orc Fighter
    20475: [ 20, 1 ], // Kasha Wolf
    20478: [ 110, 1 ], // Kasha Blade Spider
    20487: [ 90, 1 ], // Kuruka Ratman
    20511: [ 100, 1 ], // Pitchstone Golem
    20525: [ 20, 1 ], // Gray Wolf
    20528: [ 100, 1 ], // Goblin Lord
    20536: [ 15, 1 ], // Elder Brown Keltir
    20537: [ 15, 1 ], // Elder Red Keltir
    20538: [ 15, 1 ], // Elder Prairie Keltir
    20539: [ 15, 1 ], // Elder Longtail Keltir
    20544: [ 15, 1 ], // Elder Keltir
    20550: [ 300, 1 ], // Guardian Basilisk
    20551: [ 300, 1 ], // Road Scavenger
    20552: [ 650, 1 ], // Fettered Soul
    20553: [ 335, 1 ], // Windsus
    20554: [ 390, 1 ], // Grandis
    20555: [ 350, 1 ], // Giant Fungus
    20557: [ 390, 1 ], // Dire Wyrm
    20559: [ 420, 1 ], // Rotting Golem
    20560: [ 440, 1 ], // Trisalim Spider
    20562: [ 485, 1 ], // Spore Zombie
    20573: [ 545, 1 ], // Tarlk Basilisk
    20575: [ 645, 1 ], // Oel Mahum Warrior
    20630: [ 350, 1 ], // Taik Orc
    20632: [ 475, 1 ], // Taik Orc Warrior
    20634: [ 960, 1 ], // Taik Orc Captain
    20636: [ 495, 1 ], // Forest of Mirrors Ghost
    20638: [ 540, 1 ], // Forest of Mirrors Ghost
    20641: [ 680, 1 ], // Harit Lizardman Grunt
    20643: [ 660, 1 ], // Harit Lizardman Warrior
    20644: [ 645, 1 ], // Harit Lizardman Shaman
    20659: [ 440, 1 ], // Grave Wanderer
    20661: [ 575, 1 ], // Hatar Ratman Thief
    20663: [ 525, 1 ], // Hatar Hanishee
    20665: [ 680, 1 ], // Taik Orc Supply
    20667: [ 730, 1 ], // Farcran
    20766: [ 210, 1 ], // Scout of the Plains
    20781: [ 270, 1 ], // Delu Lizardman Shaman
    20783: [ 140, 1 ], // Dread Wolf
    20784: [ 155, 1 ], // Tasaba Lizardman
    20786: [ 170, 1 ], // Lienrik
    20788: [ 325, 1 ], // Rakul
    20790: [ 390, 1 ], // Dailaon
    20792: [ 620, 1 ], // Farhite
    20794: [ 635, 1 ], // Blade Stakato
    20796: [ 640, 1 ], // Blade Stakato Warrior
    20798: [ 850, 1 ], // Water Giant
    20800: [ 740, 1 ], // Eva's Seeker
    20802: [ 900, 1 ], // Theeder Mage
    20804: [ 775, 1 ], // Crokian Lad
    20806: [ 805, 1 ], // Crokian Lad Warrior
    20833: [ 455, 1 ], // Zaken's Archer
    20834: [ 680, 1 ], // Mardian
    20836: [ 785, 1 ], // Pirate Zombie
    20837: [ 835, 1 ], // Tainted Ogre
    20839: [ 430, 1 ], // Unpleasant Humming
    20841: [ 460, 1 ], // Fiend Archer
    20845: [ 605, 1 ], // Pirate Zombie Captain
    20847: [ 570, 1 ], // Veil Master
    20849: [ 585, 1 ], // Light Worm
    20936: [ 290, 1 ], // Tanor Silenos
    20937: [ 315, 1 ], // Tanor Silenos Grunt
    20939: [ 385, 1 ], // Tanor Silenos Warrior
    20940: [ 500, 1 ], // Tanor Silenos Shaman
    20941: [ 460, 1 ], // Tanor Silenos Chieftain
    20943: [ 345, 1 ], // Nightmare Keeper
    20944: [ 335, 1 ], // Nightmare Lord
    21100: [ 125, 1 ], // Langk Lizardman Sentinel
    21101: [ 155, 1 ], // Langk Lizardman Shaman
    21103: [ 215, 1 ], // Roughly Hewn Rock Golem
    21105: [ 310, 1 ], // Delu Lizardman Special Agent
    21107: [ 600, 1 ], // Delu Lizardman Commander
    21117: [ 120, 1 ], // Kasha Imp
    21023: [ 170, 1 ], // Sobbing Wind
    21024: [ 175, 1 ], // Babbling Wind
    21025: [ 185, 1 ], // Giggling Wind
    21026: [ 200, 1 ], // Singing Wind
    21034: [ 195, 1 ], // Ogre
    21125: [ 12, 1 ], // Northern Trimden
    21520: [ 880, 1 ], // Eye of Splendor
    21526: [ 970, 1 ], // Wisdom of Splendor
    21536: [ 985, 1 ], // Crown of Splendor
    21602: [ 555, 1 ], // Zaken's Pikeman
    21603: [ 750, 1 ], // Zaken's Pikeman
    21605: [ 620, 1 ], // Zaken's Archer
    21606: [ 875, 1 ], // Zaken's Archer
    21611: [ 590, 1 ], // Unpleasant Humming
    21612: [ 835, 1 ], // Unpleasant Humming
    21617: [ 615, 1 ], // Fiend Archer
    21618: [ 875, 1 ], // Fiend Archer
    21635: [ 775, 1 ], // Veil Master
    21638: [ 165, 1 ], // Dread Wolf
    21639: [ 185, 1 ], // Tasaba Lizardman
    21641: [ 195, 1 ], // Ogre
    21644: [ 170, 1 ], // Lienrik
    22231: [ 10, 1 ], // Dominant Grey Keltir
    22233: [ 20, 1 ], // Dominant Black Wolf
    22234: [ 30, 1 ], // Green Goblin
    22235: [ 35, 1 ], // Mountain Werewolf
    22237: [ 55, 1 ], // Mountain Fungus
    22238: [ 70, 1 ], // Mountain Werewolf Chief
    22241: [ 80, 1 ], // Colossus
    22244: [ 90, 1 ], // Crimson Spider
    22247: [ 90, 1 ], // Grotto Golem
    22250: [ 90, 1 ], // Grotto Leopard
    22252: [ 95, 1 ], // Grotto Grizzly
    20579: [ 420, 2 ], // Leto Lizardman Soldier
    20639: [ 280, 2 ], // Mirror
    20646: [ 145, 2 ], // Halingka
    20648: [ 120, 2 ], // Paliote
    20650: [ 460, 2 ], // Kranrot
    20651: [ 260, 2 ], // Gamlin
    20652: [ 335, 2 ], // Leogul
    20657: [ 630, 2 ], // Lesser Giant Mage
    20658: [ 570, 2 ], // Lesser Giant Elder
    20808: [ 50, 2 ], // Nos Lad
    20809: [ 865, 2 ], // Ghost of the Tower
    20832: [ 700, 2 ], // Zaken's Pikeman
    20979: [ 980, 2 ], // Elmoradan's Maid
    20991: [ 665, 2 ], // Swamp Tribe
    20994: [ 590, 2 ], // Garden Guard Leader
    21261: [ 170, 2 ], // Ol Mahum Transcender
    21263: [ 795, 2 ], // Ol Mahum Transcender
    21508: [ 100, 2 ], // Splinter Stakato
    21510: [ 280, 2 ], // Splinter Stakato Soldier
    21511: [ 995, 2 ], // Splinter Stakato Drone
    21512: [ 995, 2 ], // Splinter Stakato Drone
    21514: [ 185, 2 ], // Needle Stakato Worker
    21516: [ 495, 2 ], // Needle Stakato Drone
    21517: [ 495, 2 ], // Needle Stakato Drone
    21518: [ 255, 2 ], // Frenzy Stakato Soldier
    21636: [ 950, 2 ], // Veil Master
    20655: [ 110, 3 ], // Lesser Giant Shooter
    20656: [ 150, 3 ], // Lesser Giant Scout
    20772: [ 105, 3 ], // Barif's Pet
    20810: [ 50, 3 ], // Hallate's Seer
    20812: [ 490, 3 ], // Archer of Despair
    20814: [ 775, 3 ], // Blader of Despair
    20816: [ 875, 3 ], // Hallate's Royal Guard
    20819: [ 280, 3 ], // Archer of Abyss
    20955: [ 670, 3 ], // Ghostly Warrior
    20978: [ 555, 3 ], // Elmoradan's Archer Escort
    21058: [ 355, 3 ], // Beast Lord
    21060: [ 45, 3 ], // Beast Seer
    21075: [ 110, 3 ], // Slaughter Bathin
    21078: [ 610, 3 ], // Magus Valac
    21081: [ 955, 3 ], // Power Angel Amon
    21264: [ 920, 3 ], // Ol Mahum Transcender
    20815: [ 205, 4 ], // Hound Dog of Hallate
    20822: [ 100, 4 ], // Hallate's Maid
    20824: [ 665, 4 ], // Hallate's Commander
    20825: [ 620, 4 ], // Hallate's Inspector
    20983: [ 205, 4 ], // Binder
    21314: [ 145, 4 ], // Hot Springs Bandersnatchling
    21316: [ 235, 4 ], // Hot Springs Flava
    21318: [ 280, 4 ], // Hot Springs Antelope
    21320: [ 355, 4 ], // Hot Springs Yeti
    21322: [ 430, 4 ], // Hot Springs Bandersnatch
    21376: [ 280, 4 ], // Scarlet Stakato Worker
    21378: [ 375, 4 ], // Scarlet Stakato Noble
    21380: [ 375, 4 ], // Tepra Scarab
    21387: [ 640, 4 ], // Arimanes of Destruction
    21393: [ 935, 4 ], // Magma Drake
    21395: [ 855, 4 ], // Elder Lavasaurus
    21652: [ 375, 4 ], // Scarlet Stakato Noble
    21655: [ 640, 4 ], // Arimanes of Destruction
    21657: [ 935, 4 ], // Magma Drake
    20828: [ 935, 5 ], // Platinum Tribe Shaman
    21061: [ 530, 5 ], // Hallate's Guardian
    21069: [ 825, 5 ], // Platinum Guardian Prefect
    21382: [ 125, 5 ], // Mercenary of Destruction
    21384: [ 400, 5 ], // Necromancer of Destruction
    21390: [ 750, 5 ], // Ashuras of Destruction
    21654: [ 400, 5 ], // Necromancer of Destruction
    21656: [ 750, 5 ], // Ashuras of Destruction
}

const dropMapSpecial: { [ npcId: number ]: MonsterReward } = {
    20829: [ 115, 6 ], // Platinum Tribe Overlord
    20859: [ 890, 8 ], // Guardian Angel
    21066: [ 5, 5 ], // Platinum Guardian Shaman
    21068: [ 565, 11 ], // Guardian Archangel
    21071: [ 400, 12 ], // Seal Archangel
}

const SWEET_FLUID: number = 7586

export class GetFinishingShots extends ListenerLogic {

    constructor() {
        super( 'Q00426_QuestForFishingShot', 'listeners/tracked-400/GetFinishingShots.ts' )
        this.questItemIds = [ SWEET_FLUID ]
        this.questId = 426
    }

    getAttackableKillIds(): Array<number> {
        return [
                ..._.keys( dropMap ),
                ..._.keys( dropMapSpecial )
        ].map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00426_QuestForFishingShot'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let npc: L2Attackable = L2World.getObjectById( data.targetId ) as L2Attackable
        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), -1, 2, npc )
        if ( !state ) {
            return
        }

        let player = state.getPlayer()

        if ( dropMapSpecial[ data.npcId ] ) {
            let [ chance, quantity ] = dropMapSpecial[ data.npcId ]
            let bonus = _.random( 1000 ) <= QuestHelper.getAdjustedChance( SWEET_FLUID, chance, data.isChampion ) ? 1 : 0

            await QuestHelper.rewardSingleQuestItem( player, SWEET_FLUID, quantity + bonus, data.isChampion )
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

            return
        }

        let [ chance, quantity ] = dropMap[ data.npcId ]

        if ( _.random( 1000 ) <= QuestHelper.getAdjustedChance( SWEET_FLUID, chance, data.isChampion ) ) {
            await QuestHelper.rewardSingleQuestItem( player, SWEET_FLUID, quantity, data.isChampion )
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

            return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case 'QUEST_ACEPT':
                state.startQuest()
                return this.getPath( '03.htm' )

            case '1':
                return this.getPath( '06.html' )

            case '2':
                return this.getPath( '07.html' )

            case '3':
                await state.exitQuest( true )
                return this.getPath( '08.html' )
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let state: QuestState = this.getQuestState( data.playerId, true )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( '01.htm' )

            case QuestStateValues.STARTED: {
                if ( !QuestHelper.hasQuestItems( player, SWEET_FLUID ) ) {
                    return this.getPath( '04.html' )
                }

                return this.getPath( '05.html' )
            }
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}