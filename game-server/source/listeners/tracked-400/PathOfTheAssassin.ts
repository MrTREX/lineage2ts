import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'

const TRISKEL = 30416
const GUARD_LEIKAN = 30382
const ARKENIA = 30419
const SHILENS_CALL = 1245
const ARKENIAS_LETTER = 1246
const LEIKANS_NOTE = 1247
const MOONSTONE_BEASTS_MOLAR = 1248
const SHILENS_TEARS = 1250
const ARKENIAS_RECOMMENDATION = 1251
const IRON_HEART = 1252
const MOONSTONE_BEAST = 20369
const CALPICO = 27036
const MIN_LEVEL = 18

export class PathOfTheAssassin extends ListenerLogic {
    constructor() {
        super( 'Q00411_PathOfTheAssassin', 'listeners/tracked-400/PathOfTheAssassin.ts' )
        this.questId = 411
        this.questItemIds = [
            SHILENS_CALL,
            ARKENIAS_LETTER,
            LEIKANS_NOTE,
            MOONSTONE_BEASTS_MOLAR,
            SHILENS_TEARS,
            ARKENIAS_RECOMMENDATION,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ TRISKEL ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00411_PathOfTheAssassin'
    }

    getTalkIds(): Array<number> {
        return [ TRISKEL, GUARD_LEIKAN, ARKENIA ]
    }

    getAttackableKillIds(): Array<number> {
        return [ MOONSTONE_BEAST, CALPICO ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( player.getClassId() === ClassIdValues.darkFighter.id ) {
                    if ( player.getLevel() >= MIN_LEVEL ) {
                        if ( QuestHelper.hasQuestItem( player, IRON_HEART ) ) {
                            return this.getPath( '30416-04.htm' )
                        }

                        state.startQuest()
                        await QuestHelper.giveSingleItem( player, SHILENS_CALL, 1 )
                        return this.getPath( '30416-05.htm' )
                    }

                    return this.getPath( '30416-03.htm' )
                }

                if ( player.getClassId() === ClassIdValues.assassin.id ) {
                    return this.getPath( '30416-02a.htm' )
                }

                return this.getPath( '30416-02.htm' )

            case '30382-02.html':
            case '30382-04.html':
                break

            case '30382-03.html':
                if ( QuestHelper.hasQuestItem( player, ARKENIAS_LETTER ) ) {
                    await QuestHelper.takeSingleItem( player, ARKENIAS_LETTER, 1 )
                    await QuestHelper.giveSingleItem( player, LEIKANS_NOTE, 1 )

                    state.setConditionWithSound( 3, true )
                    break
                }

                return

            case '30419-02.html':
            case '30419-03.html':
            case '30419-04.html':
            case '30419-06.html':
                break

            case '30419-05.html':
                if ( QuestHelper.hasQuestItem( player, SHILENS_CALL ) ) {
                    await QuestHelper.takeSingleItem( player, SHILENS_CALL, 1 )
                    await QuestHelper.giveSingleItem( player, ARKENIAS_LETTER, 1 )

                    state.setConditionWithSound( 2, true )
                    break
                }

                return


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case MOONSTONE_BEAST:
                if ( QuestHelper.hasQuestItem( player, LEIKANS_NOTE )
                        && QuestHelper.getQuestItemsCount( player, MOONSTONE_BEASTS_MOLAR ) < 10 ) {
                    await QuestHelper.rewardAndProgressState( player, state, MOONSTONE_BEASTS_MOLAR, 1, 10, data.isChampion, 4 )
                }

                return

            case CALPICO:
                if ( !QuestHelper.hasQuestItem( player, SHILENS_TEARS ) ) {
                    await QuestHelper.giveSingleItem( player, SHILENS_TEARS, 1 )
                    state.setConditionWithSound( 6, true )
                }

                return

        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === TRISKEL ) {
                    if ( !QuestHelper.hasQuestItem( player, IRON_HEART ) ) {
                        return this.getPath( '30416-01.htm' )
                    }

                    return this.getPath( '30416-04.htm' )
                }

                break

            case QuestStateValues.STARTED:
                let [ hasLetter, hasNote, hasTears, hasRecommendation, hasHeart, hasCall ]: Array<boolean> = QuestHelper.hasEachQuestItem( player, ARKENIAS_LETTER, LEIKANS_NOTE, SHILENS_TEARS, ARKENIAS_RECOMMENDATION, IRON_HEART, SHILENS_CALL )
                switch ( data.characterNpcId ) {
                    case TRISKEL:
                        if ( !hasLetter
                                && !hasNote
                                && !hasTears
                                && !hasHeart
                                && hasRecommendation ) {
                            await QuestHelper.giveAdena( player, 163800, true )
                            await QuestHelper.giveSingleItem( player, IRON_HEART, 1 )

                            if ( player.getLevel() >= 20 ) {
                                await QuestHelper.addExpAndSp( player, 320534, 35830 )
                            } else if ( player.getLevel() === 19 ) {
                                await QuestHelper.addExpAndSp( player, 456128, 35830 )
                            } else {
                                await QuestHelper.addExpAndSp( player, 591724, 42528 )
                            }

                            await state.exitQuest( false, true )
                            player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )

                            return this.getPath( '30416-06.html' )
                        }

                        if ( !hasNote
                                && !hasTears
                                && !hasRecommendation
                                && !hasHeart
                                && !hasCall
                                && hasLetter ) {
                            return this.getPath( '30416-07.html' )
                        }

                        if ( !hasLetter
                                && !hasTears
                                && !hasRecommendation
                                && !hasHeart
                                && !hasCall && hasNote ) {
                            return this.getPath( '30416-08.html' )
                        }

                        if ( !hasLetter
                                && !hasNote
                                && !hasTears
                                && !hasRecommendation
                                && !hasHeart
                                && !hasCall ) {
                            return this.getPath( '30416-09.html' )
                        }

                        if ( !hasLetter
                                && !hasNote
                                && !hasRecommendation
                                && !hasHeart
                                && !hasCall
                                && hasTears ) {
                            return this.getPath( '30416-10.html' )
                        }

                        if ( !hasLetter
                                && !hasNote
                                && !hasTears
                                && !hasRecommendation
                                && !hasHeart
                                && hasCall ) {
                            return this.getPath( '30416-11.html' )
                        }

                        break

                    case GUARD_LEIKAN:
                        let hasMolar: boolean = QuestHelper.hasQuestItem( player, MOONSTONE_BEASTS_MOLAR )

                        if ( !hasNote
                                && !hasTears
                                && !hasRecommendation
                                && !hasHeart
                                && !hasCall
                                && !hasMolar
                                && hasLetter ) {
                            return this.getPath( '30382-01.html' )
                        }

                        if ( !hasLetter
                                && !hasTears
                                && !hasRecommendation
                                && !hasHeart
                                && !hasCall
                                && !hasMolar
                                && hasNote ) {
                            return this.getPath( '30382-05.html' )
                        }

                        if ( !hasLetter
                                && !hasTears
                                && !hasRecommendation
                                && !hasHeart
                                && !hasCall
                                && hasNote ) {
                            if ( QuestHelper.getQuestItemsCount( player, MOONSTONE_BEASTS_MOLAR ) < 10 ) {
                                return this.getPath( '30382-06.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, -1, LEIKANS_NOTE, MOONSTONE_BEASTS_MOLAR )

                            state.setConditionWithSound( 5, true )
                            return this.getPath( '30382-07.html' )
                        }

                        if ( hasTears ) {
                            return this.getPath( '30382-08.html' )
                        }

                        if ( !hasLetter
                                && !hasNote
                                && !hasTears
                                && !hasRecommendation
                                && !hasHeart
                                && !hasCall
                                && !hasMolar ) {
                            return this.getPath( '30382-09.html' )
                        }

                        break

                    case ARKENIA:
                        if ( !hasLetter
                                && !hasNote
                                && !hasTears
                                && !hasRecommendation
                                && !hasHeart
                                && hasCall ) {
                            return this.getPath( '30419-01.html' )
                        }

                        if ( !hasNote
                                && !hasTears
                                && !hasRecommendation
                                && !hasHeart
                                && hasLetter ) {
                            return this.getPath( '30419-07.html' )
                        }

                        if ( !hasLetter
                                && !hasNote
                                && !hasRecommendation
                                && !hasHeart
                                && !hasCall
                                && hasTears ) {
                            await QuestHelper.takeSingleItem( player, SHILENS_TEARS, 1 )
                            await QuestHelper.giveSingleItem( player, ARKENIAS_RECOMMENDATION, 1 )

                            state.setConditionWithSound( 7, true )
                            return this.getPath( '30419-08.html' )
                        }

                        if ( !hasLetter
                                && !hasNote
                                && !hasHeart
                                && !hasCall
                                && hasRecommendation ) {
                            return this.getPath( '30419-09.html' )
                        }

                        if ( !hasLetter
                                && !hasTears
                                && !hasRecommendation
                                && !hasHeart
                                && !hasCall
                                && hasNote ) {
                            return this.getPath( '30419-10.html' )
                        }

                        if ( !hasLetter
                                && !hasNote
                                && !hasTears
                                && !hasRecommendation
                                && !hasHeart
                                && !hasCall ) {
                            return this.getPath( '30419-11.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}