import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { QuestHelper } from '../helpers/QuestHelper'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

const BLACKSMITH_SILVERA = 30527
const BLACKSMITH_PINTER = 30298
const BLACKSMITH_KLUTO = 30317
const IRON_GATES_LOCKIRIN = 30531
const WAREHOUSE_KEEPER_RYDEL = 31956
const MINERAL_TRADER_HITCHI = 31963
const RAILROAD_WORKER_OBI = 32052
const SILVERYS_RING = 1632
const PASS_1ST_CERTIFICATE = 1633
const PASS_2ND_CERTIFICATE = 1634
const BOOGLE_RATMAN_TOOTH = 1636
const BOOGLE_RATMAN_LEADERS_TOOTH = 1637
const KLUTOS_LETTER = 1638
const FOOTPRINT_OF_THIEF = 1639
const STOLEN_SECRET_BOX = 1640
const SECRET_BOX = 1641
const FINAL_PASS_CERTIFICATE = 1635
const VUKU_ORC_FIGHTER = 20017
const BOOGLE_RATMAN = 20389
const BOOGLE_RATMAN_LEADER = 20390
const minimumLevel = 18

export class PathOfTheArtisan extends ListenerLogic {
    constructor() {
        super( 'Q00418_PathOfTheArtisan', 'listeners/tracked-400/PathOfTheArtisan.ts' )
        this.questId = 418
        this.questItemIds = [
            SILVERYS_RING,
            PASS_1ST_CERTIFICATE,
            PASS_2ND_CERTIFICATE,
            BOOGLE_RATMAN_TOOTH,
            BOOGLE_RATMAN_LEADERS_TOOTH,
            KLUTOS_LETTER,
            FOOTPRINT_OF_THIEF,
            STOLEN_SECRET_BOX,
            SECRET_BOX,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ BLACKSMITH_SILVERA ]
    }

    getTalkIds(): Array<number> {
        return [
            BLACKSMITH_SILVERA,
            BLACKSMITH_PINTER,
            BLACKSMITH_KLUTO,
            IRON_GATES_LOCKIRIN,
            WAREHOUSE_KEEPER_RYDEL,
            MINERAL_TRADER_HITCHI,
            RAILROAD_WORKER_OBI,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            VUKU_ORC_FIGHTER,
            BOOGLE_RATMAN,
            BOOGLE_RATMAN_LEADER,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00418_PathOfTheArtisan'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( player.getClassId() === ClassIdValues.dwarvenFighter.id ) {
                    if ( player.getLevel() >= minimumLevel ) {
                        if ( QuestHelper.hasQuestItem( player, FINAL_PASS_CERTIFICATE ) ) {
                            return this.getPath( '30527-04.htm' )
                        }

                        return this.getPath( '30527-05.htm' )
                    }

                    return this.getPath( '30527-03.htm' )
                }

                if ( player.getClassId() === ClassIdValues.artisan.id ) {
                    return this.getPath( '30527-02a.htm' )
                }

                return this.getPath( '30527-02.htm' )

            case '30527-06.htm':
                state.startQuest()
                await QuestHelper.giveSingleItem( player, SILVERYS_RING, 1 )

                break

            case '30527-08b.html':
                await QuestHelper.takeMultipleItems( player, -1, SILVERYS_RING, BOOGLE_RATMAN_TOOTH, BOOGLE_RATMAN_LEADERS_TOOTH )
                await QuestHelper.giveSingleItem( player, PASS_1ST_CERTIFICATE, 1 )

                state.setConditionWithSound( 3, true )
                break

            case '30527-08c.html':
                await QuestHelper.takeMultipleItems( player, -1, SILVERYS_RING, BOOGLE_RATMAN_TOOTH, BOOGLE_RATMAN_LEADERS_TOOTH )

                state.setMemoState( 10 )
                state.setConditionWithSound( 8, true )

                break

            case '30298-02.html':
            case '30317-02.html':
            case '30317-03.html':
            case '30317-05.html':
            case '30317-06.html':
            case '30317-11.html':
            case '30531-02.html':
            case '30531-03.html':
            case '30531-04.html':
            case '31956-02.html':
            case '31956-03.html':
            case '32052-02.html':
            case '32052-03.html':
            case '32052-04.html':
            case '32052-05.html':
            case '32052-06.html':
            case '32052-10.html':
            case '32052-11.html':
            case '32052-12.html':
                break

            case '30298-03.html':
                if ( QuestHelper.hasQuestItem( player, KLUTOS_LETTER ) ) {
                    await QuestHelper.takeSingleItem( player, KLUTOS_LETTER, 1 )
                    await QuestHelper.giveSingleItem( player, FOOTPRINT_OF_THIEF, 1 )

                    state.setConditionWithSound( 5, true )
                    break
                }

                return

            case '30298-06.html':
                if ( QuestHelper.hasQuestItems( player, FOOTPRINT_OF_THIEF, STOLEN_SECRET_BOX ) ) {
                    await QuestHelper.takeMultipleItems( player, -1, FOOTPRINT_OF_THIEF, STOLEN_SECRET_BOX )
                    await QuestHelper.giveMultipleItems( player, 1, SECRET_BOX, PASS_2ND_CERTIFICATE )

                    state.setConditionWithSound( 7, true )
                    break
                }

                return

            case '30317-04.html':
                await QuestHelper.giveSingleItem( player, KLUTOS_LETTER, 1 )
                state.setConditionWithSound( 4, true )

                break

            case '30317-07.html':
                await QuestHelper.giveSingleItem( player, KLUTOS_LETTER, 1 )
                state.setConditionWithSound( 4 )

                break

            case '30317-10.html':
                if ( QuestHelper.hasQuestItems( player, PASS_2ND_CERTIFICATE, SECRET_BOX ) ) {
                    await QuestHelper.giveAdena( player, 163800, true )
                    await QuestHelper.giveSingleItem( player, FINAL_PASS_CERTIFICATE, 1 )

                    if ( player.getLevel() >= 20 ) {
                        await QuestHelper.addExpAndSp( player, 320534, 32452 )
                    } else if ( player.getLevel() === 19 ) {
                        await QuestHelper.addExpAndSp( player, 456128, 30150 )
                    } else {
                        await QuestHelper.addExpAndSp( player, 591724, 36848 )
                    }

                    await state.exitQuest( false, true )
                    player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )

                    break
                }

                return

            case '30317-12.html':
                if ( QuestHelper.hasQuestItems( player, PASS_2ND_CERTIFICATE, SECRET_BOX ) ) {
                    await this.finishQuest( player, state, data )
                    break
                }

                return

            case '30531-05.html':
                if ( state.isMemoState( 101 ) ) {
                    await this.finishQuest( player, state, data )
                    break
                }

                return

            case '31956-04.html':
                if ( state.isMemoState( 201 ) ) {
                    await this.finishQuest( player, state, data )
                    break
                }

                return

            case '31963-02.html':
            case '31963-06.html':
                if ( state.isMemoState( 100 ) ) {
                    break
                }

                return

            case '31963-03.html':
                if ( state.isMemoState( 100 ) ) {
                    state.setMemoState( 101 )
                    state.setConditionWithSound( 10, true )

                    break
                }

                return

            case '31963-05.html':
                if ( state.isMemoState( 100 ) ) {
                    state.setMemoState( 102 )
                    state.setConditionWithSound( 11, true )

                    break
                }

                return

            case '31963-07.html':
                if ( state.isMemoState( 100 ) ) {
                    state.setMemoState( 201 )
                    state.setConditionWithSound( 12, true )

                    break
                }

                return

            case '31963-09.html':
                if ( state.isMemoState( 100 ) ) {
                    state.setMemoState( 202 )
                    break
                }

                return

            case '31963-10.html':
                if ( state.isMemoState( 202 ) ) {
                    await this.finishQuest( player, state, data )
                    break
                }

                return

            case '32052-07.html':
                if ( state.isMemoState( 10 ) ) {
                    state.setMemoState( 100 )
                    state.setConditionWithSound( 9, true )

                    break
                }

                return

            case '32052-13.html':
                if ( state.isMemoState( 102 ) ) {
                    await this.finishQuest( player, state, data )
                    break
                }

                return


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async finishQuest( player: L2PcInstance, state: QuestState, data: NpcGeneralEvent ): Promise<void> {
        await QuestHelper.giveAdena( player, 81900, true )
        await QuestHelper.giveSingleItem( player, FINAL_PASS_CERTIFICATE, 1 )

        if ( player.getLevel() >= 20 ) {
            await QuestHelper.addExpAndSp( player, 160267, 11726 )
        } else if ( player.getLevel() === 19 ) {
            await QuestHelper.addExpAndSp( player, 228064, 15075 )
        } else {
            await QuestHelper.addExpAndSp( player, 295862, 18424 )
        }

        player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )
        return state.exitQuest( false, true )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case VUKU_ORC_FIGHTER:
                if ( Math.random() < QuestHelper.getAdjustedChance( STOLEN_SECRET_BOX, 0.2, data.isChampion )
                        && QuestHelper.hasQuestItem( player, FOOTPRINT_OF_THIEF )
                        && !QuestHelper.hasQuestItem( player, STOLEN_SECRET_BOX ) ) {
                    await QuestHelper.giveSingleItem( player, STOLEN_SECRET_BOX, 1 )
                    state.setConditionWithSound( 6, true )
                }

                return

            case BOOGLE_RATMAN:
                if ( Math.random() < QuestHelper.getAdjustedChance( BOOGLE_RATMAN_TOOTH, 0.7, data.isChampion )
                        && QuestHelper.hasQuestItem( player, SILVERYS_RING )
                        && QuestHelper.getQuestItemsCount( player, BOOGLE_RATMAN_TOOTH ) < 10 ) {
                    await QuestHelper.rewardSingleQuestItem( player, BOOGLE_RATMAN_TOOTH, 1, data.isChampion )

                    if ( QuestHelper.getQuestItemsCount( player, BOOGLE_RATMAN_TOOTH ) >= 10
                            && QuestHelper.getQuestItemsCount( player, BOOGLE_RATMAN_LEADERS_TOOTH ) >= 2 ) {
                        state.setConditionWithSound( 2, true )
                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case BOOGLE_RATMAN_LEADER:
                if ( Math.random() < QuestHelper.getAdjustedChance( BOOGLE_RATMAN_LEADERS_TOOTH, 0.5, data.isChampion )
                        && QuestHelper.hasQuestItem( player, SILVERYS_RING )
                        && QuestHelper.getQuestItemsCount( player, BOOGLE_RATMAN_LEADERS_TOOTH ) < 2 ) {
                    await QuestHelper.rewardSingleQuestItem( player, BOOGLE_RATMAN_LEADERS_TOOTH, 1, data.isChampion )

                    if ( QuestHelper.getQuestItemsCount( player, BOOGLE_RATMAN_TOOTH ) >= 10
                            && QuestHelper.getQuestItemsCount( player, BOOGLE_RATMAN_LEADERS_TOOTH ) >= 2 ) {
                        state.setConditionWithSound( 2, true )
                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === BLACKSMITH_SILVERA ) {
                    return this.getPath( '30527-01.htm' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case BLACKSMITH_SILVERA:
                        if ( QuestHelper.hasQuestItem( player, SILVERYS_RING ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, BOOGLE_RATMAN_TOOTH ) >= 10
                                    && QuestHelper.getQuestItemsCount( player, BOOGLE_RATMAN_LEADERS_TOOTH ) >= 2 ) {
                                return this.getPath( '30527-08a.html' )
                            }

                            return this.getPath( '30527-07.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, PASS_1ST_CERTIFICATE ) ) {
                            return this.getPath( '30527-09.html' )
                        }

                        if ( state.isMemoState( 10 ) ) {
                            return this.getPath( '30527-09a.html' )
                        }

                        break

                    case BLACKSMITH_PINTER:
                        if ( !QuestHelper.hasQuestItem( player, PASS_1ST_CERTIFICATE ) ) {
                            break
                        }

                        if ( QuestHelper.hasQuestItem( player, KLUTOS_LETTER ) ) {
                            return this.getPath( '30298-01.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, FOOTPRINT_OF_THIEF ) ) {
                            if ( QuestHelper.hasQuestItem( player, STOLEN_SECRET_BOX ) ) {
                                return this.getPath( '30298-05.html' )
                            }

                            return this.getPath( '30298-04.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, PASS_2ND_CERTIFICATE, SECRET_BOX ) ) {
                            return this.getPath( '30298-07.html' )
                        }

                        break

                    case BLACKSMITH_KLUTO:
                        if ( QuestHelper.hasQuestItem( player, PASS_1ST_CERTIFICATE )
                                && !QuestHelper.hasAtLeastOneQuestItem( player, FOOTPRINT_OF_THIEF, KLUTOS_LETTER, PASS_2ND_CERTIFICATE, SECRET_BOX ) ) {
                            return this.getPath( '30317-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, PASS_1ST_CERTIFICATE )
                                && QuestHelper.hasAtLeastOneQuestItem( player, KLUTOS_LETTER, FOOTPRINT_OF_THIEF ) ) {
                            return this.getPath( '30317-08.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, PASS_1ST_CERTIFICATE, PASS_2ND_CERTIFICATE, SECRET_BOX ) ) {
                            return this.getPath( '30317-09.html' )
                        }

                        break

                    case IRON_GATES_LOCKIRIN:
                        if ( state.isMemoState( 101 ) ) {
                            return this.getPath( '30531-01.html' )
                        }

                        break

                    case WAREHOUSE_KEEPER_RYDEL:
                        if ( state.isMemoState( 201 ) ) {
                            return this.getPath( '31956-01.html' )
                        }

                        break

                    case MINERAL_TRADER_HITCHI:
                        switch ( state.getMemoState() ) {
                            case 100:
                                return this.getPath( '31963-01.html' )

                            case 101:
                                return this.getPath( '31963-04.html' )

                            case 102:
                                return this.getPath( '31963-06a.html' )

                            case 201:
                                return this.getPath( '31963-08.html' )

                            case 202:
                                return this.getPath( '31963-11.html' )
                        }

                        break

                    case RAILROAD_WORKER_OBI:
                        switch ( state.getMemoState() ) {
                            case 10:
                                return this.getPath( '32052-01.html' )

                            case 100:
                                return this.getPath( '32052-08.html' )

                            case 102:
                                return this.getPath( '32052-09.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}