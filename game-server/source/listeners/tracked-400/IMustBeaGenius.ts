import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { DataManager } from '../../data/manager'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestType } from '../../gameService/enums/QuestType'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import _ from 'lodash'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const GUTENHAGEN = 32069
const CORPSE_LOG = 15510
const COLLECTION = 15511
const minimumLevel = 70

type MonsterReward = [ number, number ] // amount, chance value
const monsterRewards: { [ npcId: number ]: MonsterReward } = {
    22801: [ 5, 0 ],
    22802: [ 5, 0 ],
    22803: [ 5, 0 ],
    22804: [ -2, 1 ],
    22805: [ -2, 1 ],
    22806: [ -2, 1 ],
    22807: [ -1, -1 ],
    22809: [ 2, 2 ],
    22810: [ -3, 3 ],
    22811: [ 3, -1 ],
    22812: [ 1, -1 ],
}

type PlayerReward = [ number, number, string ] // exp, sp, html suffix
const rewards: Array<PlayerReward> = [
    [ 198725, 15892, '08' ],
    [ 278216, 22249, '08' ],
    [ 317961, 25427, '08' ],
    [ 357706, 28606, '09' ],
    [ 397451, 31784, '09' ],
    [ 596176, 47677, '09' ],
    [ 715411, 57212, '10' ],
    [ 794901, 63569, '10' ],
    [ 914137, 73104, '10' ],
    [ 1192352, 95353, '11' ],
]

const variableNames = {
    randomNumber: 'rn',
    chanceValue: 'cv',
    talkValue: 'tv',
}

export class IMustBeaGenius extends ListenerLogic {
    constructor() {
        super( 'Q00463_IMustBeaGenius', 'listeners/tracked-400/IMustBeaGenius.ts' )
        this.questId = 463
        this.questItemIds = [ COLLECTION, CORPSE_LOG ]
    }

    getQuestStartIds(): Array<number> {
        return [ GUTENHAGEN ]
    }

    getTalkIds(): Array<number> {
        return [ GUTENHAGEN ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewards ).map( value => _.parseInt( value ) )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '32069-03.htm':
                state.startQuest()
                let number: number = _.random( 50 ) + 550
                state.setVariable( variableNames.randomNumber, number.toString() )
                state.setVariable( variableNames.chanceValue, _.random( 3 ) )

                return DataManager.getHtmlData().getItem( this.getPath( '32069-03.htm' ) )
                        .replace( '%num%', number.toString() )

            case '32069-05.htm':
                return DataManager.getHtmlData().getItem( this.getPath( '32069-05.htm' ) )
                        .replace( '%num%', state.getVariable( variableNames.randomNumber ).toString() )

            case 'reward':
                if ( state.isCondition( 2 ) ) {
                    let [ exp, sp, htmlSuffix ]: PlayerReward = _.sample( rewards )
                    let player = L2World.getPlayer( data.playerId )

                    await QuestHelper.addExpAndSp( player, exp, sp )
                    await state.exitQuestWithType( QuestType.DAILY, true )

                    return this.getPath( `32069-${ htmlSuffix }.html` )
                }

                return

            case '32069-02.htm':
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isCondition( 1 ) ) {
            return
        }

        let [ amount, chance ]: MonsterReward = monsterRewards[ data.npcId ]
        let player = L2World.getPlayer( data.playerId )
        let shouldSendMessage: boolean = false

        if ( state.getVariable( variableNames.chanceValue ) === chance ) {
            amount = _.random( 99 ) + 1
        }

        if ( amount > 0 ) {
            await QuestHelper.giveSingleItem( player, CORPSE_LOG, amount )
            shouldSendMessage = true
        } else if ( amount < 0 && ( QuestHelper.getQuestItemsCount( player, CORPSE_LOG ) + amount ) > 0 ) {
            await QuestHelper.takeSingleItem( player, CORPSE_LOG, Math.abs( amount ) )
            shouldSendMessage = true
        }

        if ( !shouldSendMessage ) {
            return
        }

        BroadcastHelper.broadcastNpcSayStringId( L2World.getObjectById( data.targetId ) as L2Npc, NpcSayType.NpcAll, NpcStringIds.ATT_ATTACK_S1_RO_ROGUE_S2, player.getName(), amount.toString() )

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        if ( QuestHelper.getQuestItemsCount( player, CORPSE_LOG ) === state.getVariable( variableNames.randomNumber ) ) {
            await QuestHelper.takeSingleItem( player, CORPSE_LOG, -1 )
            await QuestHelper.giveSingleItem( player, COLLECTION, 1 )

            state.setConditionWithSound( 2, true )
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.COMPLETED:
                if ( !state.isNowAvailable() ) {
                    return this.getPath( '32069-07.htm' )
                }

                state.setState( QuestStateValues.CREATED )

            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '32069-01.htm' : '32069-00.htm' )

            case QuestStateValues.STARTED:
                if ( state.isCondition( 1 ) ) {
                    return this.getPath( '32069-04.html' )
                }

                if ( state.getVariable( variableNames.talkValue ) === 1 ) {
                    return this.getPath( '32069-06a.html' )
                }

                await QuestHelper.takeSingleItem( player, COLLECTION, -1 )
                state.setVariable( variableNames.talkValue, 1 )

                return this.getPath( '32069-06.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00463_IMustBeaGenius'
    }
}