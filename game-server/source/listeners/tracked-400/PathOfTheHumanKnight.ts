import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

const HIGH_PRIEST_BIOTIN = 30031
const LEVIAN = 30037
const CAPTAIN_GILBERT = 30039
const HIGH_PRIEST_RAYMOND = 30289
const CAPTAIN_BATHIS = 30332
const CAPTAIN_BEZIQUE = 30379
const SIR_KLAUS_VASPER = 30417
const SIR_ARON_TANFORD = 30653
const SQUIRES_MARK = 1271
const COIN_OF_LORDS1 = 1162
const COIN_OF_LORDS2 = 1163
const COIN_OF_LORDS3 = 1164
const COIN_OF_LORDS4 = 1165
const COIN_OF_LORDS5 = 1166
const COIN_OF_LORDS6 = 1167
const GLUDIO_GUARDS_1ST_BADGE = 1168
const BUGBEAR_NECKLACE = 1169
const EINHASADS_1ST_TEMPLE_BADGE = 1170
const EINHASAD_CRUCIFIX = 1171
const GLUDIO_GUARDS_2ND_BADGE = 1172
const VENOMOUS_SPIDERS_LEG = 1173
const EINHASADS_2ND_TEMPLE_BADGE = 1174
const LIZARDMANS_TOTEM = 1175
const GLUDIO_GUARDS_3RD_BADGE = 1176
const GIANT_SPIDERS_HUSK = 1177
const EINHASADS_3RD_TEMPLE_BADGE = 1178
const SKULL_OF_SILENT_HORROR = 1179
const SWORD_OF_RITUAL = 1161
const LANGK_LIZARDMAN_WARRIOR = 20024
const LANGK_LIZARDMAN_SCOUT = 20027
const LANGK_LIZARDMAN = 20030
const VENOMOUS_SPIDER = 20038
const ARACHNID_TRACKER = 20043
const ARACHNID_PREDATOR = 20050
const GIANT_SPIDER = 20103
const TALON_SPIDER = 20106
const BLADE_SPIDER = 20108
const SILENT_HORROR = 20404
const BUGBEAR_RAIDER = 20775
const UNDEAD_PRIEST = 27024
const minimumLevel = 18

export class PathOfTheHumanKnight extends ListenerLogic {
    constructor() {
        super( 'Q00402_PathOfTheHumanKnight', 'listeners/tracked-400/PathOfTheHumanKnight.ts' )
        this.questId = 402
        this.questItemIds = [
            SQUIRES_MARK,
            COIN_OF_LORDS1,
            COIN_OF_LORDS2,
            COIN_OF_LORDS3,
            COIN_OF_LORDS4,
            COIN_OF_LORDS5,
            COIN_OF_LORDS6,
            GLUDIO_GUARDS_1ST_BADGE,
            BUGBEAR_NECKLACE,
            EINHASADS_1ST_TEMPLE_BADGE,
            EINHASAD_CRUCIFIX,
            GLUDIO_GUARDS_2ND_BADGE,
            VENOMOUS_SPIDERS_LEG,
            EINHASADS_2ND_TEMPLE_BADGE,
            LIZARDMANS_TOTEM,
            GLUDIO_GUARDS_3RD_BADGE,
            GIANT_SPIDERS_HUSK,
            EINHASADS_3RD_TEMPLE_BADGE,
            SKULL_OF_SILENT_HORROR,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            LANGK_LIZARDMAN_WARRIOR,
            LANGK_LIZARDMAN_SCOUT,
            LANGK_LIZARDMAN,
            VENOMOUS_SPIDER,
            ARACHNID_TRACKER,
            ARACHNID_PREDATOR,
            GIANT_SPIDER,
            TALON_SPIDER,
            BLADE_SPIDER,
            SILENT_HORROR,
            BUGBEAR_RAIDER,
            UNDEAD_PRIEST,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ SIR_KLAUS_VASPER ]
    }

    getTalkIds(): Array<number> {
        return [
            SIR_KLAUS_VASPER,
            HIGH_PRIEST_BIOTIN,
            LEVIAN,
            HIGH_PRIEST_RAYMOND,
            CAPTAIN_GILBERT,
            CAPTAIN_BATHIS,
            CAPTAIN_BEZIQUE,
            SIR_ARON_TANFORD,
        ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case LANGK_LIZARDMAN_WARRIOR:
            case LANGK_LIZARDMAN_SCOUT:
            case LANGK_LIZARDMAN:
                if ( Math.random() < QuestHelper.getAdjustedChance( LIZARDMANS_TOTEM, 0.5, data.isChampion )
                        && QuestHelper.hasQuestItem( player, EINHASADS_2ND_TEMPLE_BADGE )
                        && QuestHelper.getQuestItemsCount( player, LIZARDMANS_TOTEM ) < 20 ) {
                    await QuestHelper.rewardSingleQuestItem( player, LIZARDMANS_TOTEM, 1, data.isChampion )
                    this.playSound( player, LIZARDMANS_TOTEM, 20 )
                }
                return

            case VENOMOUS_SPIDER:
            case ARACHNID_TRACKER:
            case ARACHNID_PREDATOR:
                if ( QuestHelper.hasQuestItem( player, GLUDIO_GUARDS_2ND_BADGE )
                        && QuestHelper.getQuestItemsCount( player, VENOMOUS_SPIDERS_LEG ) < 20 ) {
                    await QuestHelper.rewardSingleQuestItem( player, VENOMOUS_SPIDERS_LEG, 1, data.isChampion )
                    this.playSound( player, VENOMOUS_SPIDERS_LEG, 20 )
                }

                return

            case GIANT_SPIDER:
            case TALON_SPIDER:
            case BLADE_SPIDER:
                if ( Math.random() < QuestHelper.getAdjustedChance( GIANT_SPIDERS_HUSK, 0.4, data.isChampion )
                        && QuestHelper.hasQuestItem( player, GLUDIO_GUARDS_3RD_BADGE )
                        && QuestHelper.getQuestItemsCount( player, GIANT_SPIDERS_HUSK ) < 20 ) {
                    await QuestHelper.rewardSingleQuestItem( player, GIANT_SPIDERS_HUSK, 1, data.isChampion )
                    this.playSound( player, GIANT_SPIDERS_HUSK, 20 )
                }

                return

            case SILENT_HORROR:
                if ( Math.random() < QuestHelper.getAdjustedChance( SKULL_OF_SILENT_HORROR, 0.4, data.isChampion )
                        && QuestHelper.hasQuestItem( player, EINHASADS_3RD_TEMPLE_BADGE )
                        && QuestHelper.getQuestItemsCount( player, SKULL_OF_SILENT_HORROR ) < 10 ) {
                    await QuestHelper.rewardSingleQuestItem( player, SKULL_OF_SILENT_HORROR, 1, data.isChampion )
                    this.playSound( player, SKULL_OF_SILENT_HORROR, 10 )
                }

                return

            case BUGBEAR_RAIDER:
                if ( QuestHelper.hasQuestItem( player, GLUDIO_GUARDS_1ST_BADGE )
                        && QuestHelper.getQuestItemsCount( player, BUGBEAR_NECKLACE ) < 10 ) {
                    await QuestHelper.rewardSingleQuestItem( player, BUGBEAR_NECKLACE, 1, data.isChampion )
                    this.playSound( player, BUGBEAR_NECKLACE, 10 )
                }

                return

            case UNDEAD_PRIEST:
                if ( Math.random() < QuestHelper.getAdjustedChance( EINHASAD_CRUCIFIX, 0.5, data.isChampion )
                        && QuestHelper.hasQuestItem( player, EINHASADS_1ST_TEMPLE_BADGE )
                        && QuestHelper.getQuestItemsCount( player, EINHASAD_CRUCIFIX ) < 12 ) {
                    await QuestHelper.rewardSingleQuestItem( player, EINHASAD_CRUCIFIX, 1, data.isChampion )
                    this.playSound( player, EINHASAD_CRUCIFIX, 12 )
                }

                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( player.getClassId() === ClassIdValues.fighter.id ) {
                    if ( player.getLevel() >= minimumLevel ) {
                        if ( QuestHelper.hasQuestItem( player, SWORD_OF_RITUAL ) ) {
                            return this.getPath( '30417-04.htm' )
                        }

                        return this.getPath( '30417-05.htm' )
                    }

                    return this.getPath( '30417-02.htm' )
                }

                if ( player.getClassId() === ClassIdValues.knight.id ) {
                    return this.getPath( '30417-02a.htm' )
                }

                return this.getPath( '30417-03.htm' )

            case '30417-08.htm':
                state.startQuest()
                await QuestHelper.giveSingleItem( player, SQUIRES_MARK, 1 )

                break

            case '30289-02.html':
            case '30417-06.html':
            case '30417-07.htm':
            case '30417-15.html':
                break

            case '30417-13.html':
            case '30417-14.html':
                let coinAmount = this.getCoinCount( player )
                if ( QuestHelper.hasQuestItems( player, SQUIRES_MARK )
                        && coinAmount > 2
                        && coinAmount < 6 ) {
                    await QuestHelper.giveAdena( player, 81900, true )
                    await QuestHelper.giveSingleItem( player, SWORD_OF_RITUAL, 1 )

                    if ( player.getLevel() >= 20 ) {
                        await QuestHelper.addExpAndSp( player, 160267, 11576 )
                    } else if ( player.getLevel() === 19 ) {
                        await QuestHelper.addExpAndSp( player, 228064, 14925 )
                    } else {
                        await QuestHelper.addExpAndSp( player, 295862, 18274 )
                    }

                    await state.exitQuest( false, true )
                    player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )

                    break
                }

                return

            case '30031-02.html':
                await QuestHelper.giveSingleItem( player, EINHASADS_3RD_TEMPLE_BADGE, 1 )
                break

            case '30037-02.html':
                await QuestHelper.giveSingleItem( player, EINHASADS_2ND_TEMPLE_BADGE, 1 )
                break

            case '30289-03.html':
                await QuestHelper.giveSingleItem( player, EINHASADS_1ST_TEMPLE_BADGE, 1 )
                break

            case '30039-02.html':
                await QuestHelper.giveSingleItem( player, GLUDIO_GUARDS_3RD_BADGE, 1 )
                break

            case '30379-02.html':
                await QuestHelper.giveSingleItem( player, GLUDIO_GUARDS_2ND_BADGE, 1 )
                break

            case '30332-02.html':
                await QuestHelper.giveSingleItem( player, GLUDIO_GUARDS_1ST_BADGE, 1 )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === SIR_KLAUS_VASPER ) {
                    return this.getPath( '30417-01.htm' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case SIR_KLAUS_VASPER:
                        if ( QuestHelper.hasQuestItem( player, SQUIRES_MARK ) ) {
                            let coins = this.getCoinCount( player )
                            if ( coins < 3 ) {
                                return this.getPath( '30417-09.html' )
                            }

                            if ( coins === 3 ) {
                                return this.getPath( '30417-10.html' )
                            }

                            if ( coins < 6 ) {
                                return this.getPath( '30417-11.html' )
                            }

                            await QuestHelper.giveAdena( player, 163800, true )
                            await QuestHelper.giveSingleItem( player, SWORD_OF_RITUAL, 1 )

                            if ( player.getLevel() >= 20 ) {
                                await QuestHelper.addExpAndSp( player, 320534, 23152 )
                            } else if ( player.getLevel() === 19 ) {
                                await QuestHelper.addExpAndSp( player, 456128, 29850 )
                            } else {
                                await QuestHelper.addExpAndSp( player, 591724, 36542 )
                            }

                            await state.exitQuest( false, true )
                            player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )

                            return this.getPath( '30417-12.html' )
                        }

                        break

                    case HIGH_PRIEST_BIOTIN:
                        if ( QuestHelper.hasQuestItem( player, SQUIRES_MARK )
                                && !QuestHelper.hasAtLeastOneQuestItem( player, EINHASADS_3RD_TEMPLE_BADGE, COIN_OF_LORDS6 ) ) {
                            return this.getPath( '30031-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, EINHASADS_3RD_TEMPLE_BADGE ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, SKULL_OF_SILENT_HORROR ) < 10 ) {
                                return this.getPath( '30031-03.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, -1, EINHASADS_3RD_TEMPLE_BADGE, SKULL_OF_SILENT_HORROR )
                            await QuestHelper.giveSingleItem( player, COIN_OF_LORDS6, 1 )

                            return this.getPath( '30031-04.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, COIN_OF_LORDS6 ) ) {
                            return this.getPath( '30031-05.html' )
                        }

                        break

                    case LEVIAN:
                        if ( QuestHelper.hasQuestItem( player, SQUIRES_MARK )
                                && !QuestHelper.hasAtLeastOneQuestItem( player, EINHASADS_2ND_TEMPLE_BADGE, COIN_OF_LORDS4 ) ) {
                            return this.getPath( '30037-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, EINHASADS_2ND_TEMPLE_BADGE ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, LIZARDMANS_TOTEM ) < 20 ) {
                                return this.getPath( '30037-03.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, -1, EINHASADS_2ND_TEMPLE_BADGE, LIZARDMANS_TOTEM )
                            await QuestHelper.giveSingleItem( player, COIN_OF_LORDS4, 1 )

                            return this.getPath( '30037-04.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, COIN_OF_LORDS4 ) ) {
                            return this.getPath( '30037-05.html' )
                        }

                        break

                    case HIGH_PRIEST_RAYMOND:
                        if ( QuestHelper.hasQuestItem( player, SQUIRES_MARK )
                                && !QuestHelper.hasAtLeastOneQuestItem( player, EINHASADS_1ST_TEMPLE_BADGE, COIN_OF_LORDS2 ) ) {
                            return this.getPath( '30289-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, EINHASADS_1ST_TEMPLE_BADGE ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, EINHASAD_CRUCIFIX ) < 12 ) {
                                return this.getPath( '30289-04.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, -1, EINHASADS_1ST_TEMPLE_BADGE, EINHASAD_CRUCIFIX )
                            await QuestHelper.giveSingleItem( player, COIN_OF_LORDS2, 1 )

                            return this.getPath( '30289-05.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, COIN_OF_LORDS2 ) ) {
                            return this.getPath( '30289-06.html' )
                        }

                        break

                    case CAPTAIN_GILBERT:
                        if ( QuestHelper.hasQuestItem( player, SQUIRES_MARK )
                                && !QuestHelper.hasAtLeastOneQuestItem( player, GLUDIO_GUARDS_3RD_BADGE, COIN_OF_LORDS5 ) ) {
                            return this.getPath( '30039-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, GLUDIO_GUARDS_3RD_BADGE ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, GIANT_SPIDERS_HUSK ) < 20 ) {
                                return this.getPath( '30039-03.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, -1, GLUDIO_GUARDS_3RD_BADGE, GIANT_SPIDERS_HUSK )
                            await QuestHelper.giveSingleItem( player, COIN_OF_LORDS5, 1 )

                            return this.getPath( '30039-04.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, COIN_OF_LORDS5 ) ) {
                            return this.getPath( '30039-05.html' )
                        }

                        break

                    case CAPTAIN_BEZIQUE:
                        if ( QuestHelper.hasQuestItem( player, SQUIRES_MARK )
                                && !QuestHelper.hasAtLeastOneQuestItem( player, GLUDIO_GUARDS_2ND_BADGE, COIN_OF_LORDS3 ) ) {
                            return this.getPath( '30379-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, GLUDIO_GUARDS_2ND_BADGE ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, VENOMOUS_SPIDERS_LEG ) < 20 ) {
                                return this.getPath( '30379-03.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, -1, GLUDIO_GUARDS_2ND_BADGE, VENOMOUS_SPIDERS_LEG )
                            await QuestHelper.giveSingleItem( player, COIN_OF_LORDS3, 1 )

                            return this.getPath( '30379-04.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, COIN_OF_LORDS3 ) ) {
                            return this.getPath( '30379-05.html' )
                        }

                        break

                    case CAPTAIN_BATHIS:
                        if ( QuestHelper.hasQuestItem( player, SQUIRES_MARK )
                                && !QuestHelper.hasAtLeastOneQuestItem( player, GLUDIO_GUARDS_1ST_BADGE, COIN_OF_LORDS1 ) ) {
                            return this.getPath( '30332-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, GLUDIO_GUARDS_1ST_BADGE ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, BUGBEAR_NECKLACE ) < 10 ) {
                                return this.getPath( '30332-03.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, -1, GLUDIO_GUARDS_1ST_BADGE, BUGBEAR_NECKLACE )
                            await QuestHelper.giveSingleItem( player, COIN_OF_LORDS1, 1 )

                            return this.getPath( '30332-04.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, COIN_OF_LORDS1 ) ) {
                            return this.getPath( '30332-05.html' )
                        }

                        break

                    case SIR_ARON_TANFORD:
                        if ( QuestHelper.hasQuestItem( player, SQUIRES_MARK ) ) {
                            return this.getPath( '30653-01.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00402_PathOfTheHumanKnight'
    }

    getCoinCount( player: L2PcInstance ): number {
        return QuestHelper.getItemsSumCount( player, COIN_OF_LORDS1, COIN_OF_LORDS2, COIN_OF_LORDS3, COIN_OF_LORDS4, COIN_OF_LORDS5, COIN_OF_LORDS6 )
    }

    playSound( player: L2PcInstance, itemId: number, limit: number ): void {
        let packet: Buffer = QuestHelper.hasQuestItemCount( player, itemId, limit ) ? SoundPacket.ITEMSOUND_QUEST_MIDDLE : SoundPacket.ITEMSOUND_QUEST_ITEMGET
        player.sendCopyData( packet )
    }
}