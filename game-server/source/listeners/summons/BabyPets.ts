import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import { EventType, NpcGeneralEvent, PlayerStartsLogoutEvent, PlayerSummonSpawnEvent } from '../../gameService/models/events/EventType'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2PetInstance } from '../../gameService/models/actor/instance/L2PetInstance'
import { SkillCache } from '../../gameService/cache/SkillCache'
import { Skill } from '../../gameService/models/Skill'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { L2World } from '../../gameService/L2World'
import _ from 'lodash'

const npcIds: Array<number> = [
    12780, // Baby Buffalo
    12781, // Baby Kookaburra
    12782, // Baby Cougar
]

const normalHealSkillId = 4717
const greaterHealSkillId = 4718

const enum EventNames {
    CastHeal = 'ch'
}

// TODO : convert pets actions to AI Controller trait, to avoid messing with quest timers
// consider that pets may cast heals/buffs only when attacking, per L2 lore
export class BabyPets extends ListenerLogic {
    constructor() {
        super( 'BabyPets', 'listeners/summons/BabyPets.ts' )
    }

    async castSkill( pet: L2PetInstance, skill: Skill, hpLimit: number ): Promise<void> {
        let owner: L2PcInstance = pet.getOwner()
        if ( !owner.isDead()
                && !pet.isHungry()
                && ( ( ( owner.getCurrentHp() / owner.getMaxHp() ) * 100 ) < hpLimit )
                && pet.canCastSkill( skill ) ) {

            let previousFollowStatus = pet.isFollowingOwner()
            if ( !previousFollowStatus && !pet.isInsideRadius( owner, skill.getCastRange(), true ) ) {
                return
            }

            pet.setTarget( owner )
            await pet.doCast( skill )

            let message = new SystemMessageBuilder( SystemMessageIds.PET_USES_S1 )
                    .addSkillName( skill )
                    .getBuffer()
            pet.sendOwnedData( message )

            if ( previousFollowStatus !== pet.isFollowingOwner() ) {
                pet.setAIFollow( previousFollowStatus )
            }
        }
    }

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.General,
                eventType: EventType.PlayerStartsLogout,
                method: this.stopTimers.bind( this ),
                ids: null,
            },
        ]
    }

    getSummonSpawnIds(): Array<number> {
        return npcIds
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {

        let event = data.eventName
        let player = L2World.getPlayer( data.playerId )

        if ( event === EventNames.CastHeal && player ) {
            let pet: L2PetInstance = player.getSummon() as L2PetInstance

            if ( pet ) {
                if ( _.random( 100 ) <= 25 ) {
                    await this.castSkill( pet, SkillCache.getSkill( normalHealSkillId, this.getHealLevel( pet ) ), 80 )
                }

                if ( _.random( 100 ) <= 75 ) {
                    await this.castSkill( pet, SkillCache.getSkill( greaterHealSkillId, this.getHealLevel( pet ) ), 15 )
                }

                return
            }

            this.stopQuestTimer( EventNames.CastHeal, null, player.getObjectId() )
            return
        }

        return
    }

    async onSummonSpawnEvent( data: PlayerSummonSpawnEvent ): Promise<void> {
        this.startQuestTimer( EventNames.CastHeal, 1000, undefined, data.ownerId, true )
    }

    getHealLevel( pet: L2PetInstance ) {
        let summonLevel = pet.getLevel()
        let value = summonLevel < 70 ? ( summonLevel / 10 ) : ( 7 + ( ( summonLevel - 70 ) / 5 ) )
        return _.clamp( Math.floor( value ), 1, 12 )
    }

    async stopTimers( data: PlayerStartsLogoutEvent ) : Promise<void> {
        this.stopQuestTimer( EventNames.CastHeal, undefined, data.playerId )
    }
}