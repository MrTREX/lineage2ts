import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { L2Character } from '../../gameService/models/actor/L2Character'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { L2ServitorInstance } from '../../gameService/models/actor/instance/L2ServitorInstance'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { CharacterKilledEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'

const npcIds: Array<number> = [
    // Kat the Cat
    14111, 14112, 14113, 14114,
    // Mew the Cat
    14159, 14160, 14161, 14162,
    // Boxer the Unicorn
    14295, 14296, 14297, 14298,
    // Mirage the Unicorn
    14343, 14344, 14345, 14346,
    // Shadow
    14479, 14480, 14481, 14482,
    // Silhouette
    14527, 14528, 14529, 14530,
]

const PAKO_THE_CAT = 27102
const UNICORN_RACER = 27103
const SHADOW_TUREN = 27104
const MIMI_THE_CAT = 27105
const UNICORN_PHANTASM = 27106
const SILHOUETTE_TILFO = 27107

const CRYSTAL_OF_STARTING_1ST = 3360
const CRYSTAL_OF_INPROGRESS_1ST = 3361
const CRYSTAL_OF_DEFEAT_1ST = 3363
const CRYSTAL_OF_STARTING_2ND = 3365
const CRYSTAL_OF_INPROGRESS_2ND = 3366
const CRYSTAL_OF_DEFEAT_2ND = 3368
const CRYSTAL_OF_STARTING_3RD = 3370
const CRYSTAL_OF_INPROGRESS_3RD = 3371
const CRYSTAL_OF_DEFEAT_3RD = 3373
const CRYSTAL_OF_STARTING_4TH = 3375
const CRYSTAL_OF_INPROGRESS_4TH = 3376
const CRYSTAL_OF_DEFEAT_4TH = 3378
const CRYSTAL_OF_STARTING_5TH = 3380
const CRYSTAL_OF_INPROGRESS_5TH = 3381
const CRYSTAL_OF_DEFEAT_5TH = 3383
const CRYSTAL_OF_STARTING_6TH = 3385
const CRYSTAL_OF_INPROGRESS_6TH = 3386
const CRYSTAL_OF_DEFEAT_6TH = 3388

const monsters: { [ npcId: number ]: Array<number> } = {
    [ PAKO_THE_CAT ]: [ CRYSTAL_OF_STARTING_1ST, CRYSTAL_OF_INPROGRESS_1ST, CRYSTAL_OF_DEFEAT_1ST ],
    [ UNICORN_RACER ]: [ CRYSTAL_OF_STARTING_3RD, CRYSTAL_OF_INPROGRESS_3RD, CRYSTAL_OF_DEFEAT_3RD ],
    [ SHADOW_TUREN ]: [ CRYSTAL_OF_STARTING_5TH, CRYSTAL_OF_INPROGRESS_5TH, CRYSTAL_OF_DEFEAT_5TH ],
    [ MIMI_THE_CAT ]: [ CRYSTAL_OF_STARTING_2ND, CRYSTAL_OF_INPROGRESS_2ND, CRYSTAL_OF_DEFEAT_2ND ],
    [ UNICORN_PHANTASM ]: [ CRYSTAL_OF_STARTING_4TH, CRYSTAL_OF_INPROGRESS_4TH, CRYSTAL_OF_DEFEAT_4TH ],
    [ SILHOUETTE_TILFO ]: [ CRYSTAL_OF_STARTING_6TH, CRYSTAL_OF_INPROGRESS_6TH, CRYSTAL_OF_DEFEAT_6TH ],
}

export class Servitors extends ListenerLogic {
    constructor() {
        super( 'Servitors', 'listeners/summons/Servitors.ts' )
    }

    getCreatureKillIds(): Array<number> {
        return npcIds
    }

    async onCreatureKillEvent( data : CharacterKilledEvent ): Promise<void> {
        let attacker = L2World.getObjectById( data.attackerId ) as L2Character
        let target = L2World.getObjectById( data.targetId ) as L2Character

        let shouldProceed: boolean = attacker.isNpc()
                && target.isServitor()
                && GeneralHelper.checkIfInRange( 1500, attacker, target, true )

        if ( !shouldProceed ) {
            return
        }

        let player = ( target as L2ServitorInstance ).getOwner()
        let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), 'Q00230_TestOfTheSummoner' )

        if ( state && QuestHelper.hasQuestItems( player, CRYSTAL_OF_INPROGRESS_3RD ) ) {
            let items: Array<number> = monsters[ attacker.getId() ]

            if ( !items ) {
                return
            }

            let [ starting, progress, defeat ] = items
            await QuestHelper.giveSingleItem( player, defeat, 1 ) // Crystal of Defeat
            await QuestHelper.takeSingleItem( player, progress, -1 ) // Crystal of Inprogress
            await QuestHelper.takeSingleItem( player, starting, -1 ) // Crystal of Starting
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        }
    }
}