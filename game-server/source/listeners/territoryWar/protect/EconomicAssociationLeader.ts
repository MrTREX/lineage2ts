import { TWProtectionLogic } from '../TWProtectionLogic'

export class EconomicAssociationLeader extends TWProtectionLogic {
    protectNpcIds: Array<number> = [
        36513,
        36519,
        36525,
        36531,
        36537,
        36543,
        36549,
        36555,
        36561
    ]

    constructor() {
        super( 'Q00733_ProtectTheEconomicAssociationLeader', 'listeners/territoryWar/protect/EconomicAssociationLeader.ts' )
    }

    getTerritoryId( npcId: number ): number {
        return 81 + Math.floor( ( npcId - 36513 ) / 6 )
    }

    getDescription(): string {
        return 'Protect the Economic Association Leader'
    }
}