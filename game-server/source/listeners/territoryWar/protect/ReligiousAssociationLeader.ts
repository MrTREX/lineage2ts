import { TWProtectionLogic } from '../TWProtectionLogic'

export class ReligiousAssociationLeader extends TWProtectionLogic {
    protectNpcIds: Array<number> = [
        36510,
        36516,
        36522,
        36528,
        36534,
        36540,
        36546,
        36552,
        36558
    ]

    constructor() {
        super( 'Q00732_ProtectTheReligiousAssociationLeader', 'listeners/territoryWar/protect/ReligiousAssociationLeader.ts' )
        this.questId = 732
    }

    getTerritoryId( npcId: number ): number {
        return 81 + Math.floor( ( npcId - 36510 ) / 6 )
    }

    getDescription(): string {
        return 'Protect the Religious Association Leader'
    }
}