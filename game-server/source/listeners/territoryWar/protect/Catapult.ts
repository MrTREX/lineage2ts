import { TWProtectionLogic } from '../TWProtectionLogic'

export class Catapult extends TWProtectionLogic {
    protectNpcIds: Array<number> = [
        36499,
        36500,
        36501,
        36502,
        36503,
        36504,
        36505,
        36506,
        36507
    ]

    constructor() {
        super( 'Q00729_ProtectTheTerritoryCatapult', 'listeners/territoryWar/protect/Catapult.ts' )
        this.questId = 729
    }

    getTerritoryId( npcId: number ): number {
        return npcId - 36418
    }

    getDescription(): string {
        return 'Protect the Territory Catapult!'
    }
}