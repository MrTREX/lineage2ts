import { TWProtectionLogic } from '../TWProtectionLogic'

export class MilitaryAssociationLeader extends TWProtectionLogic {
    protectNpcIds: Array<number> = [
        36508,
        36514,
        36520,
        36526,
        36532,
        36538,
        36544,
        36550,
        36556
    ]

    constructor() {
        super( 'Q00731_ProtectTheMilitaryAssociationLeader', 'listeners/territoryWar/protect/MilitaryAssociationLeader.ts' )
        this.questId = 731
    }

    getTerritoryId( npcId: number ): number {
        return 81 + Math.floor( ( npcId - 36508 ) / 6 )
    }

    getDescription(): string {
        return 'Protect the Military Association Leader'
    }
}