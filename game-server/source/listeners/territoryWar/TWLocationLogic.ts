import { TerritoryWarLogic } from './TerritoryWarLogic'
import { ListenerDescription } from '../../gameService/models/ListenerLogic'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import { AttackableKillEvent, EventType } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { TerritoryWarManager } from '../../gameService/instancemanager/TerritoryWarManager'
import { ExShowScreenMessage } from '../../gameService/packets/send/ExShowScreenMessage'

export abstract class TWLocationLogic extends TerritoryWarLogic {
    abstract catapultId: number
    abstract leaderIds: Set<number>
    abstract guardNpcIds: Array<number>
    abstract territoryId: number

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.General,
                eventType: EventType.TWPlayerLogin,
                method: this.onPlayerLogin.bind( this ),
                ids: null,
            },
            {
                registerType: ListenerRegisterType.NpcId,
                eventType: EventType.AttackableKilled,
                method: this.onAttackableKillEvent.bind( this ),
                ids: [
                    this.catapultId,
                    ...this.leaderIds,
                    ...this.guardNpcIds,
                ],
            },
        ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        if ( !player ) {
            return
        }

        let castleId = player.getSiegeSide() - 80
        if ( data.npcId === this.catapultId ) {
            TerritoryWarManager.territoryCatapultDestroyed( castleId )
            TerritoryWarManager.giveTWPoint( player, this.territoryId, 4 )
            TerritoryWarManager.announceToParticipants( ExShowScreenMessage.fromNpcMessageId( this.getMessageId(), 2, 10000, null ), 135000, 13500 )

            this.processBecomeMercenaryQuest( data.playerId, true )
        }

        if ( this.leaderIds.has( data.npcId ) ) {
            TerritoryWarManager.giveTWPoint( player, this.territoryId, 3 )
        }

        if ( player.getSiegeSide() !== this.territoryId && TerritoryWarManager.getTerritory( castleId ) ) {
            // TODO : use proper data structure instead of array
            TerritoryWarManager.getTerritory( castleId ).getQuestDone()[ 0 ]++
        }
    }

    abstract getMessageId(): number
}