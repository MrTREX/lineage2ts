import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import { EventType, NpcSeeSkillEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { TerritoryWarManager } from '../../gameService/instancemanager/TerritoryWarManager'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { TerritoryWard } from '../../gameService/instancemanager/territory/TerritoryWard'
import { TerritoryNPCSpawn } from '../../gameService/instancemanager/territory/TerritoryNPCSpawn'

export class TerritoryWarTriggers extends ListenerLogic {
    constructor() {
        super( 'TerritoryWarTriggers', 'listeners/territoryWar/TerritoryWarTriggers.ts' )
    }

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.NpcId,
                eventType: EventType.NpcSeeSkill,
                method: this.onNpcSeeSkill.bind( this ),
                ids: [ 36590 ],
                triggers: {
                    targetIds: new Set( [ 845, 847 ] )
                }
            }
        ]
    }

    async onNpcSeeSkill( data: NpcSeeSkillEvent ) : Promise<void> {
        if ( !data.targetIds.includes( data.receiverNpcId ) ) {
            return
        }

        let npc = L2World.getObjectById( data.receiverId ) as L2Npc
        if ( !npc ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        if ( !player || !player.getClan() ) {
            return
        }

        if ( data.skillId === 845 ) {
            if ( TerritoryWarManager.getSiegeFlagForClan( player.getClan() ) !== npc ) {
                return
            }
            await npc.deleteMe()
            return TerritoryWarManager.setSiegeFlagForClan( player.getClan(), null )
        }

        if ( data.skillId === 847 ) {
            let territoryId = player.getSiegeSide()
            if ( TerritoryWarManager.getFlagForTerritory( territoryId ) !== npc ) {
                return
            }

            let ward : TerritoryWard = TerritoryWarManager.getTerritoryWard( territoryId )
            if ( !ward ) {
                return
            }

            let castleId = territoryId - 80
            if ( castleId === ward.getOwnerCastleId() ) {
                let territory = TerritoryWarManager.getTerritory( ward.getOwnerCastleId() )

                if ( !territory ) {
                    return
                }

                territory.getOwnedWard().map( ( spawn: TerritoryNPCSpawn ) => {
                    if ( spawn.getId() !== ward.getTerritoryId() ) {
                        return
                    }

                    // spawn.setNPC( spawn.getNpc().getSpawn().doSpawn() )
                    ward.unSpawnMe()
                    ward.setNpc( spawn.getNpc() )
                } )

                return
            }

            ward.unSpawnMe()
            ward.setNpc( TerritoryWarManager.addTerritoryWard( ward.getTerritoryId(), castleId, ward.getOwnerCastleId(), true ) )
            ward.setOwnerCastleId( castleId )

            // TODO : remove array arguments and use proper object structure
            TerritoryWarManager.getTerritory( castleId ).getQuestDone()[ 1 ]++
        }
    }
}