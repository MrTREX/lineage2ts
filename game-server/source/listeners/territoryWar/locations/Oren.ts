import { TWLocationLogic } from '../TWLocationLogic'
import { NpcStringIds } from '../../../gameService/packets/NpcStringIds'

export class Oren extends TWLocationLogic {
    catapultId: number = 36502
    guardNpcIds: Array<number> = [
        36527,
        36529,
        36530
    ]

    leaderIds: Set<number> = new Set<number>( [
        36526,
        36528,
        36531,
        36594
    ] )

    territoryId: number = 84

    constructor() {
        super( 'Q00720_ForTheSakeOfTheTerritoryOren', 'listeners/territoryWar/locations/Oren.ts' )
        this.questId = 720
    }

    getMessageId(): number {
        return NpcStringIds.THE_CATAPULT_OF_OREN_HAS_BEEN_DESTROYED
    }

    getDescription(): string {
        return 'For the Sake of the Territory - Oren'
    }
}