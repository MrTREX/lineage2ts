import { TWLocationLogic } from '../TWLocationLogic'
import { NpcStringIds } from '../../../gameService/packets/NpcStringIds'

export class Rune extends TWLocationLogic {
    catapultId: number = 36506
    guardNpcIds: Array<number> = [
        36551,
        36553,
        36554
    ]

    leaderIds: Set<number> = new Set<number>( [
        36550,
        36552,
        36555,
        36598
    ] )

    territoryId: number = 88

    constructor() {
        super( 'Q00724_ForTheSakeOfTheTerritoryRune', 'listeners/territoryWar/locations/Rune.ts' )
        this.questId = 724
    }

    getMessageId(): number {
        return NpcStringIds.THE_CATAPULT_OF_RUNE_HAS_BEEN_DESTROYED
    }

    getDescription(): string {
        return 'For the Sake of the Territory - Rune'
    }
}