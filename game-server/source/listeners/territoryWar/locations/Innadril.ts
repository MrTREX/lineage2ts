import { TWLocationLogic } from '../TWLocationLogic'
import { NpcStringIds } from '../../../gameService/packets/NpcStringIds'

export class Innadril extends TWLocationLogic {
    catapultId: number = 36504
    guardNpcIds: Array<number> = [
        36539,
        36541,
        36542
    ]

    leaderIds: Set<number> = new Set<number>( [
        36538,
        36540,
        36543,
        36596
    ] )

    territoryId: number = 86

    constructor() {
        super( 'Q00722_ForTheSakeOfTheTerritoryInnadril', 'listeners/territoryWar/locations/Innadril.ts' )
        this.questId = 722
    }

    getMessageId(): number {
        return NpcStringIds.THE_CATAPULT_OF_INNADRIL_HAS_BEEN_DESTROYED
    }

    getDescription(): string {
        return 'For the Sake of the Territory - Innadril'
    }
}