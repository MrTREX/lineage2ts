import { TerritoryWarLogic, TerritoryWarVariableNames } from './TerritoryWarLogic'
import { ListenerDescription } from '../../gameService/models/ListenerLogic'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import { EventType, TWPlayerDeathEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { TerritoryWarManager } from '../../gameService/instancemanager/TerritoryWarManager'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { ExShowScreenMessage } from '../../gameService/packets/send/ExShowScreenMessage'
import { MsInDays } from '../../gameService/enums/MsFromTime'

export abstract class TWDestructionLogic extends TerritoryWarLogic {
    abstract classIds : Set<number>

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.General,
                eventType: EventType.TWPlayerDeath,
                method: this.onPlayerDie.bind( this ),
                ids: null,
                triggers: {
                    targetIds: this.classIds,
                },
            },
        ]
    }

    async onPlayerDie( data: TWPlayerDeathEvent ): Promise<void> {
        if ( !this.classIds.has( data.targetClassId ) ) {
            return
        }

        let attacker = L2World.getObjectById( data.attackerId )
        if ( !attacker ) {
            return
        }

        let attackerPlayer = attacker.getActingPlayer()
        if ( !attackerPlayer ) {
            return
        }

        TerritoryWarManager.giveTWPoint( attackerPlayer, attackerPlayer.getSiegeSide(), 1 )

        await Promise.all( data.partyPlayerIds.map( ( objectId: number ) => {
            if ( data.attackerId === objectId ) {
                this.processStepsForHonor( data.attackerId )
                this.processBecomeMercenaryQuest( data.attackerId, false )
            }

            let player = L2World.getPlayer( objectId )
            if ( !player ) {
                return
            }

            return this.processQuestFinish( player )
        } ) )
    }

    async processQuestFinish( player: L2PcInstance ): Promise<void> {
        let state = QuestStateCache.getQuestState( player.getObjectId(), this.name, true )

        if ( !state.isCompleted() ) {
            state.incrementVariable( TerritoryWarVariableNames.Kills, 1 )

            let kills = state.getVariable( TerritoryWarVariableNames.Kills )
            let maxKills = state.getVariable( TerritoryWarVariableNames.KillsRequired )

            if ( !state.isStarted() ) {
                state.startQuest( false )
                maxKills = this.getMaxKills()
                state.setVariable( TerritoryWarVariableNames.KillsRequired, maxKills )
            }

            if ( kills >= maxKills ) {
                TerritoryWarManager.giveQuestPoints( player )
                await QuestHelper.addExpAndSp( player, 534000, 51000 )

                state.setVariable( TerritoryWarVariableNames.FinishTime, Date.now() )
                state.unsetVariables( TerritoryWarVariableNames.Kills, TerritoryWarVariableNames.KillsRequired )
                player.sendOwnedData( ExShowScreenMessage.fromNpcMessageId( this.getFinishMessageId(), 2, 10000, null ) )

                return state.exitQuest( true )
            }

            return player.sendOwnedData( ExShowScreenMessage.fromNpcMessageId( this.getContinueMessageId(), 2, 10000, [ maxKills.toString(), kills.toString() ] ) )
        }

        let finishTime = state.getVariable( TerritoryWarVariableNames.FinishTime )
        if ( player.isGM() || ( finishTime > 0 && ( Date.now() - finishTime ) > MsInDays.One ) ) {
            state.startQuest( false )
            let maxKills = this.getMaxKills()

            state.incrementVariable( TerritoryWarVariableNames.Kills, 1 )
            state.setVariable( TerritoryWarVariableNames.KillsRequired, maxKills )
            state.unsetVariable( TerritoryWarVariableNames.FinishTime )

            return player.sendOwnedData( ExShowScreenMessage.fromNpcMessageId( this.getContinueMessageId(), 2, 10000, [ maxKills.toString(), '1' ] ) )
        }
    }

    abstract getFinishMessageId(): number
    abstract getContinueMessageId(): number
    abstract getMaxKills(): number
}