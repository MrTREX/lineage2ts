import { TWDestructionLogic } from '../TWDestructionLogic'
import { NpcStringIds } from '../../../gameService/packets/NpcStringIds'
import _ from 'lodash'

export class DestroyKeyTargets extends TWDestructionLogic {
    classIds: Set<number> = new Set<number>( [
        51,
        57,
        115,
        118,
    ] )

    constructor() {
        super( 'Q00738_DestroyKeyTargets', 'listeners/territoryWar/destroy/DestroyKeyTargets.ts' )
        this.questId = 738
    }

    getContinueMessageId(): number {
        return NpcStringIds.YOU_HAVE_DEFEATED_S2_OF_S1_WARSMITHS_AND_OVERLORDS
    }

    getFinishMessageId(): number {
        return NpcStringIds.YOU_DESTROYED_THE_ENEMYS_PROFESSIONALS
    }

    getMaxKills(): number {
        return _.random( 3, 8 )
    }

    getDescription(): string {
        return 'Destroy Key Targets'
    }
}