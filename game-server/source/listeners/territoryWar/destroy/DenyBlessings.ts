import { TWDestructionLogic } from '../TWDestructionLogic'
import { NpcStringIds } from '../../../gameService/packets/NpcStringIds'
import _ from 'lodash'

export class DenyBlessings extends TWDestructionLogic {
    classIds: Set<number> = new Set<number>( [
        16,
        17,
        30,
        43,
        52,
        97,
        98,
        105,
        112,
        116,
    ] )

    constructor() {
        super( 'Q00737_DenyBlessings', 'listeners/territoryWar/destroy/DenyBlessings.ts' )
        this.questId = 737
    }

    getContinueMessageId(): number {
        return NpcStringIds.YOU_HAVE_DEFEATED_S2_OF_S1_HEALERS_AND_BUFFERS
    }

    getFinishMessageId(): number {
        return NpcStringIds.YOU_WEAKENED_THE_ENEMYS_ATTACK
    }

    getMaxKills(): number {
        return _.random( 3, 8 )
    }

    getDescription(): string {
        return 'Deny Blessings'
    }
}