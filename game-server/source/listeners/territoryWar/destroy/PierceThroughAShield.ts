import { TWDestructionLogic } from '../TWDestructionLogic'
import { NpcStringIds } from '../../../gameService/packets/NpcStringIds'
import _ from 'lodash'

export class PierceThroughAShield extends TWDestructionLogic {
    classIds: Set<number> = new Set<number>( [
        6,
        91,
        5,
        90,
        20,
        99,
        33,
        106
    ] )

    constructor() {
        super( 'Q00734_PierceThroughAShield', 'listeners/territoryWar/destroy/PierceThroughAShield.ts' )
        this.questId = 734
    }

    getContinueMessageId(): number {
        return NpcStringIds.YOU_HAVE_DEFEATED_S2_OF_S1_KNIGHTS
    }

    getFinishMessageId(): number {
        return NpcStringIds.YOU_WEAKENED_THE_ENEMYS_DEFENSE
    }

    getDescription(): string {
        return 'Pierce through a Shield!'
    }

    getMaxKills(): number {
        return _.random( 10, 15 )
    }
}