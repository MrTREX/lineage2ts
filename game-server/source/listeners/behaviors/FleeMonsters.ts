import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AIIntent } from '../../gameService/aicontroller/enums/AIIntent'
import { RandomWalkTrait } from '../../gameService/aicontroller/traits/npc/RandomWalkTrait'
import { AITraitCache } from '../../gameService/cache/AITraitCache'
import { RunAwayTrait } from '../../gameService/aicontroller/traits/npc/RunAwayTrait'
import { TraitSettings } from '../../gameService/aicontroller/interface/IAITrait'

const npcIds : Array<number> = [
    18150, // Victim
    18151, // Victim
    18152, // Victim
    18153, // Victim
    18154, // Victim
    18155, // Victim
    18156, // Victim
    18157, // Victim
    20002, // Rabbit
    20432, // Elpy
    22228, // Grey Elpy
    25604 // Mutated Elpy
]

export class FleeMonsters extends ListenerLogic {
    constructor() {
        super( 'FleeMonsters', 'listeners/behaviors/FleeMonsters.ts' )
    }

    async initialize(): Promise<void> {
        const aiTraits: TraitSettings = {
            ...AITraitCache.createEmptyTraits(),
            [ AIIntent.WAITING ]: RandomWalkTrait,
            [ AIIntent.ATTACK ]: new RunAwayTrait( 500 ),
        }

        AITraitCache.setManyNpcTraits( npcIds, aiTraits )
    }
}