import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableAttackedEvent, EventType, NpcFinishedSkillEvent } from '../../gameService/models/events/EventType'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { L2World } from '../../gameService/L2World'
import { L2Attackable } from '../../gameService/models/actor/L2Attackable'
import { AggroCache } from '../../gameService/cache/AggroCache'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SkillCache } from '../../gameService/cache/SkillCache'
import { Skill } from '../../gameService/models/Skill'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'

const npcIds: Array<number> = [
    20213, // Porta, 40 level
    20221, // Perum, 48 level
]

const minimumDistance = 300
const targetMinimumDistance = 100
const summonSkillId = 4161

export class TeleportIntoRange extends ListenerLogic {
    constructor() {
        super( 'TeleportIntoRange', 'listeners/behaviors/TeleportIntoRange.ts' )
    }

    getAttackableAttackIds(): Array<number> {
        return npcIds
    }

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.NpcId,
                eventType: EventType.NpcSkillFinished,
                method: this.onNpcSkillFinishedEvent.bind( this ),
                ids: npcIds,
                triggers: {
                    targetIds: new Set( [ summonSkillId ] )
                }
            }
        ]
    }

    async onAttackableAttackedEvent( data: AttackableAttackedEvent ): Promise<void> {
        if ( NpcVariablesManager.get( data.targetId, this.getName() ) || Math.random() > 0.5 ) {
            return
        }

        let npc = L2World.getObjectById( data.targetId ) as L2Attackable
        let player = L2World.getPlayer( data.attackerPlayerId )
        let distance = npc.calculateDistance( player, true )

        if ( distance > minimumDistance ) {
            await this.attemptPlayerTeleport( npc, player )
            return
        }

        if ( distance > targetMinimumDistance ) {
            if ( Math.random() < 0.1 ) {
                return this.attemptPlayerTeleport( npc, player )
            }

            let target = AggroCache.getMostHated( npc )
            if ( target && target.getObjectId() === data.attackerPlayerId ) {
                return this.attemptPlayerTeleport( npc, player )
            }
        }
    }

    async attemptPlayerTeleport( npc: L2Attackable, player: L2PcInstance ): Promise<void> {
        let skill: Skill = SkillCache.getSkill( summonSkillId, 1 )

        if ( skill.getStartCostMp() < npc.getCurrentMp()
                && skill.getHpConsume() < npc.getCurrentHp()
                && !npc.isAllSkillsDisabled() ) {
            npc.setTarget( player )

            // TODO : since hp/mp checks are needed, do we subtract those from npc since normal cast does not
            await npc.doCast( skill )

            NpcVariablesManager.set( npc.getObjectId(), this.getName(), true )
        }
    }

    async onNpcSkillFinishedEvent( data: NpcFinishedSkillEvent ): Promise<void> {
        if ( data.skillId !== summonSkillId ) {
            return
        }

        let npc = L2World.getObjectById( data.attackerId ) as L2Attackable

        if ( !npc.isDead() ) {
            let player = L2World.getPlayer( data.targetId )

            await player.teleportToLocationWithOffset( npc, 50 )
            NpcVariablesManager.set( npc.getObjectId(), this.getName(), false )
        }
    }
}