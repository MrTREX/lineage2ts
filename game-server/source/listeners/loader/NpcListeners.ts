import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NewbieGuide } from '../npcs/NewbieGuide'
import { TeleportToRaceTrack } from '../npcTeleports/TeleportToRaceTrack'
import { TeleportToFantasyIsland } from '../npcTeleports/TeleportToFantasyIsland'
import { TeleportToUndergroundColiseum } from '../npcTeleports/TeleportToUndergroundColiseum'
import { TeleportWithCharm } from '../npcTeleports/TeleportWithCharm'
import { TowerOfInsolenceVortex } from '../npcTeleports/TowerOfInsolenceVortex'
import { Survivor } from '../npcTeleports/Survivor'
import { StrongholdsTeleports } from '../npcTeleports/StrongholdsTeleports'
import { Asher } from '../npcTeleports/Asher'
import { CrumaTower } from '../npcTeleports/CrumaTower'
import { DelusionTeleport } from '../npcTeleports/DelusionTeleport'
import { ElrokiTeleporters } from '../npcTeleports/ElrokiTeleporters'
import { GatekeeperSpirit } from '../npcTeleports/GatekeeperSpirit'
import { GhostChamberlainOfElmoreden } from '../npcTeleports/GhostChamberlainOfElmoreden'
import { GrandBossTeleporters } from '../npcTeleports/GrandBossTeleporters'
import { HuntingGroundsTeleport } from '../npcTeleports/HuntingGroundsTeleport'
import { Klemis } from '../npcTeleports/Klemis'
import { MithrilMinesTeleporter } from '../npcTeleports/MithrilMinesTeleporter'
import { NoblesseTeleport } from '../npcTeleports/NoblesseTeleport'
import { OracleTeleport } from '../npcTeleports/OracleTeleport'
import { PaganTeleporters } from '../npcTeleports/PaganTeleporters'
import { SeparatedSoul } from '../npcTeleports/SeparatedSoul'
import { StakatoNestTeleporter } from '../npcTeleports/StakatoNestTeleporter'
import { SteelCitadelTeleport } from '../npcTeleports/SteelCitadelTeleport'
import { WyvernManager } from '../npcs/WyvernManager'
import { WeaverOlf } from '../npcs/WeaverOlf'
import { FirstClassTransferTalk } from '../npcs/FirstClassTransferTalk'
import { VarkaSilenosSupport } from '../npcs/VarkaSilenosSupport'
import { HealerTrainer } from '../npcs/HealerTrainer'
import { TownPets } from '../npcs/TownPets'
import { TerritoryManagers } from '../npcs/TerritoryManagers'
import { SymbolMaker } from '../npcs/SymbolMaker'
import { SupportUnitCaptain } from '../npcs/SupportUnitCaptain'
import { Servitors } from '../summons/Servitors'
import { ImprovedBabyPets } from '../summons/ImprovedBabyPets'
import { BabyPets } from '../summons/BabyPets'
import { GolemTrader } from '../summons/GolemTrader'
import { SubclassCertification } from '../npcs/SubclassCertification'
import { Sirra } from '../npcs/Sirra'
import { Selina } from '../npcs/Selina'
import { Rignos } from '../npcs/Rignos'
import { Rafforty } from '../npcs/Rafforty'
import { PriestOfBlessing } from '../npcs/PriestOfBlessing'
import { NevitsHerald } from '../npcs/NevitsHerald'
import { MonumentOfHeroes } from '../npcs/MonumentOfHeroes'
import { MercenaryCaptain } from '../npcs/MercenaryCaptain'
import { ManorManager } from '../npcs/ManorManager'
import { Kier } from '../npcs/Kier'
import { KetraOrcSupport } from '../npcs/KetraOrcSupport'
import { Katenar } from '../npcs/Katenar'
import { Jinia } from '../npcs/Jinia'
import { FreyasSteward } from '../npcs/FreyasSteward'
import { FortressSiegeManager } from '../npcs/FortressSiegeManager'
import { FortressArcherCaptain } from '../npcs/FortressArcherCaptain'
import { Fisherman } from '../npcs/Fisherman'
import { FameManager } from '../npcs/FameManager'
import { DragonVortex } from '../npcs/DragonVortex'
import { Dorian } from '../npcs/Dorian'
import { ClassMaster } from '../npcs/ClassMaster'
import { ClanTrader } from '../npcs/ClanTrader'
import { CastleWarehouse } from '../npcs/CastleWarehouse'
import { CastleTeleporter } from '../npcs/CastleTeleporter'
import { CastleSiegeManager } from '../npcs/CastleSiegeManager'
import { CastleMercenaryManager } from '../npcs/CastleMercenaryManager'
import { CastleCourtMagician } from '../npcs/CastleCourtMagician'
import { CastleChamberlain } from '../npcs/CastleChamberlain'
import { CastleBlacksmith } from '../npcs/CastleBlacksmith'
import { CastleAmbassador } from '../npcs/CastleAmbassador'
import { BlackMarketeerOfMammon } from '../npcs/BlackMarketeerOfMammon'
import { BlackJudge } from '../npcs/BlackJudge'
import { AvantGarde } from '../npcs/AvantGarde'
import { Asamah } from '../npcs/Asamah'
import { ArenaManager } from '../npcs/ArenaManager'
import { Alexandria } from '../npcs/Alexandria'
import { Alarm } from '../npcs/Alarm'
import { Abercrombie } from '../npcs/Abercrombie'
import { Tunatun } from '../npcs/Tunatun'
import { Toma } from '../npcs/Toma'
import { TreasureChests } from '../npcs/TreasureChests'
import { IsleOfPrayerMobs } from '../npcs/IsleOfPrayerMobs'
import { LuckyPig } from '../npcs/LuckyPig'
import { DimensionalMerchant } from '../npcs/DimensionalMerchant'
import { CorpseOfDeadman } from '../npcs/CorpseOfDeadman'
import { GuardianBorder } from '../npcTeleports/GuardianBorder'
import { RiftWatcher } from '../npcs/RiftWatcher'
import { ScribeLeandro } from '../npcs/routes/ScribeLeandro'
import { PorterRemy } from '../npcs/routes/PorterRemy'
import { TetrarchExecKreed } from '../npcs/routes/TetrarchExecKreed'
import { TetrarchAgentAlhena } from '../npcs/routes/TetrarchAgentAlhena'
import { BardCasiel } from '../npcs/routes/BardCasiel'
import { TradekeeperJaradine } from '../npcs/routes/TradekeeperJaradine'
import { MessengerRogin } from '../npcs/routes/MessengerRogin'
import { PorterTate } from '../npcs/routes/PorterTate'
import { ProphetGludio } from '../npcs/routes/ProphetGludio'
import { ProphetGiran } from '../npcs/routes/ProphetGiran'
import { ProphetAden } from '../npcs/routes/ProphetAden'
import { ProphetRune } from '../npcs/routes/ProphetRune'
import { Tolonis } from '../npcs/Tolonis'
import { SevenSignsFestivalGuide } from '../npcs/SevenSignsFestivalGuide'
import { SevenSignsFestivalWitch } from '../npcs/SevenSignsFestivalWitch'
import { SevenSignsFestivalDrops } from '../npcs/SevenSignsFestivalDrops'
import { SevenSignsFestivalBomb } from '../npcs/SevenSignsFestivalBomb'

export const NpcListeners : Array<ListenerLogic> = [
    new NewbieGuide(),
    new TeleportToRaceTrack(),
    new TeleportToFantasyIsland(),
    new TeleportToUndergroundColiseum(),
    new TeleportWithCharm(),
    new TowerOfInsolenceVortex(),
    new Survivor(),
    new StrongholdsTeleports(),
    new Asher(),
    new CrumaTower(),
    new DelusionTeleport(),
    new ElrokiTeleporters(),
    new GatekeeperSpirit(),
    new GhostChamberlainOfElmoreden(),
    new GrandBossTeleporters(),
    new HuntingGroundsTeleport(),
    new Klemis(),
    new MithrilMinesTeleporter(),
    new NoblesseTeleport(),
    new OracleTeleport(),
    new PaganTeleporters(),
    new SeparatedSoul(),
    new StakatoNestTeleporter(),
    new SteelCitadelTeleport(),
    new WyvernManager(),
    new WeaverOlf(),
    new FirstClassTransferTalk(),
    new VarkaSilenosSupport(),
    new HealerTrainer(),
    new TownPets(),
    new TerritoryManagers(),
    new SymbolMaker(),
    new SupportUnitCaptain(),
    new Servitors(),
    new ImprovedBabyPets(),
    new BabyPets(),
    new GolemTrader(),
    new SubclassCertification(),
    new Sirra(),
    new Selina(),
    new Rignos(),
    new Rafforty(),
    new PriestOfBlessing(),
    new NevitsHerald(),
    new MonumentOfHeroes(),
    new MercenaryCaptain(),
    new ManorManager(),
    new Kier(),
    new KetraOrcSupport(),
    new Katenar(),
    new Jinia(),
    new FreyasSteward(),
    new FortressSiegeManager(),
    new FortressArcherCaptain(),
    new Fisherman(),
    new FameManager(),
    new DragonVortex(),
    new Dorian(),
    new ClassMaster(),
    new ClanTrader(),
    new CastleWarehouse(),
    new CastleTeleporter(),
    new CastleSiegeManager(),
    new CastleMercenaryManager(),
    new CastleCourtMagician(),
    new CastleChamberlain(),
    new CastleBlacksmith(),
    new CastleAmbassador(),
    new BlackMarketeerOfMammon(),
    new BlackJudge(),
    new AvantGarde(),
    new Asamah(),
    new ArenaManager(),
    new Alexandria(),
    new Alarm(),
    new Abercrombie(),
    new Tunatun(),
    new Toma(),
    new TreasureChests(),
    new IsleOfPrayerMobs(),
    new LuckyPig(),
    new DimensionalMerchant(),
    new CorpseOfDeadman(),
    new GuardianBorder(),
    new RiftWatcher(),
    new Tolonis(),

    /*
        Seven Signs festival
     */
    new SevenSignsFestivalGuide(),
    new SevenSignsFestivalWitch(),
    new SevenSignsFestivalDrops(),
    new SevenSignsFestivalBomb(),
    /*
        Npc route listeners.
     */
    new ScribeLeandro(),
    new PorterRemy(),
    new TetrarchExecKreed(),
    new TetrarchAgentAlhena(),
    new BardCasiel(),
    new TradekeeperJaradine(),
    new MessengerRogin(),
    new PorterTate(),
    new ProphetGludio(),
    new ProphetGiran(),
    new ProphetAden(),
    new ProphetRune()
]