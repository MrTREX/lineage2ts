import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import { L2DataApi } from '../../data/interface/l2DataApi'
import { ListenerCache } from '../../gameService/cache/ListenerCache'
import { EventType } from '../../gameService/models/events/EventType'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import { ListenerManager } from '../../gameService/instancemanager/ListenerManager'
import { NpcListeners } from './NpcListeners'
import { VillageMasterQuests } from './VillageMasterQuests'
import { TrackedQuestList } from './TrackedQuestList'
import { AreasList } from './AreasList'
import { EventList } from './EventList'
import { AddonList } from './AddonList'
import { Behaviors } from './Behaviors'
import { Instances } from './Instances'
import { Tasks } from './Tasks'
import { ConfigManager } from '../../config/ConfigManager'
import { VoiceCommands } from './VoiceCommands'
import aigle from 'aigle'
import _ from 'lodash'
import { Bypasses } from './Bypasses'
import { EventListenerMethod } from '../../gameService/models/events/listeners/EventListenerData'
import perfy from 'perfy'
import { TerritoryWarQuests } from './TerritoryWarQuests'

const allListeners: Array<ListenerLogic> = _.flatten( [
    NpcListeners,
    VillageMasterQuests,
    TrackedQuestList,
    AreasList,
    EventList,
    AddonList,
    Behaviors,
    Instances,
    Tasks,
    VoiceCommands,
    Bypasses,
    TerritoryWarQuests
] )

export const MainListenerLoader: L2DataApi = {
    async load(): Promise<Array<string>> {
        const name = 'MainListenerLoader'
        perfy.start( name )
        if ( ConfigManager.diagnostic.isLoadListeners() ) {
            await aigle.resolve( allListeners ).eachLimit( 10, async ( listener: ListenerLogic ) => {
                return listener.load()
            } )

            _.each( allListeners, ( listener: ListenerLogic ) => {
                ListenerManager.register( listener )

                ListenerCache.registerListener( ListenerRegisterType.NpcId, EventType.NpcStartQuest, listener, _.noop as EventListenerMethod, listener.getQuestStartIds() )
                ListenerCache.registerListener( ListenerRegisterType.NpcId, EventType.NpcApproachedForTalk, listener, listener.startApproachedForTalkEvent.bind( listener ), listener.getApproachedForTalkIds() )
                ListenerCache.registerListener( ListenerRegisterType.NpcId, EventType.NpcTalk, listener, listener.startOnTalkEvent.bind( listener ), listener.getTalkIds() )
                ListenerCache.registerListener( ListenerRegisterType.NpcId, EventType.AttackableKilled, listener, listener.onAttackableKillEvent.bind( listener ), listener.getAttackableKillIds() )
                ListenerCache.registerListener( ListenerRegisterType.NpcId, EventType.NpcSpawn, listener, listener.onSpawnEvent.bind( listener ), listener.getSpawnIds() )
                ListenerCache.registerListener( ListenerRegisterType.NpcId, EventType.CharacterKilled, listener, listener.onCreatureKillEvent.bind( listener ), listener.getCreatureKillIds() )
                ListenerCache.registerListener( ListenerRegisterType.NpcId, EventType.PlayerSummonSpawn, listener, listener.onSummonSpawnEvent.bind( listener ), listener.getSummonSpawnIds() )
                ListenerCache.registerListener( ListenerRegisterType.NpcId, EventType.NpcSeePlayer, listener, listener.onNpcSeePlayerEvent.bind( listener ), listener.getNpcSeePlayerIds() )
                ListenerCache.registerListener( ListenerRegisterType.NpcId, EventType.AttackableAttacked, listener, listener.onAttackableAttackedEvent.bind( listener ), listener.getAttackableAttackIds() )

                _.each( listener.getCustomListeners(), ( data: ListenerDescription ) => {
                    ListenerCache.registerListener( data.registerType, data.eventType, listener, data.method, data.ids, data.triggers )
                } )
            } )

            await aigle.resolve( allListeners ).eachLimit( 10, async ( quest: ListenerLogic ) => {
                return quest.initialize()
            } )
        }

        return [
            `ListenerLoader: loaded total ${ _.size( allListeners ) } listeners in ${ perfy.end( name ).time } seconds.`,
            `ListenerLoader: loaded ${ _.size( ListenerManager.questIdMap ) } valid quests.`,
        ]
    },
}