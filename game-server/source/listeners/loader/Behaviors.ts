import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { TreasureChest } from '../behaviors/TreasureChest'
import { NonLethalableNpcs } from '../behaviors/NonLethalableNpcs'
import { FleeMonsters } from '../behaviors/FleeMonsters'
import { RemnantsDie } from '../behaviors/RemnantsDie'
import { SeeThroughSilentMove } from '../behaviors/SeeThroughSilentMove'
import { TeleportIntoRange } from '../behaviors/TeleportIntoRange'
import { RaidBossDispellBuffs } from '../behaviors/RaidBossDispellBuffs'
import { NonTalkingNpcs } from '../behaviors/NonTalkingNpcs'

/*
    TODO : convert logic to individual AITraits
    then either use listeners to assign AITraits or build AITrait loader with assigned traits
 */
export const Behaviors: Array<ListenerLogic> = [
    new TreasureChest(),
    new NonLethalableNpcs(),
    new FleeMonsters(),
    new RemnantsDie(),
    new SeeThroughSilentMove(),
    new TeleportIntoRange(),
    new RaidBossDispellBuffs(),
    new NonTalkingNpcs(),
]