import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { GetFinishingShots } from '../tracked-400/GetFinishingShots'
import { Tutorial } from '../tracked-200/Tutorial'
import { LettersOfLove } from '../tracked/LettersOfLove'
import { WhatWomenWant } from '../tracked/WhatWomenWant'
import { WillTheSealBeBroken } from '../tracked/WillTheSealBeBroken'
import { LongLiveThePaagrioLord } from '../tracked/LongLiveThePaagrioLord'
import { StepIntoTheFuture } from '../tracked/StepIntoTheFuture'
import { ATripBegins } from '../tracked/ATripBegins'
import { AnAdventureBegins } from '../tracked/AnAdventureBegins'
import { MinersFavor } from '../tracked/MinersFavor'
import { IntoTheCityOfHumans } from '../tracked/IntoTheCityOfHumans'
import { IntoTheWorld } from '../tracked/IntoTheWorld'
import { SecretMeetingWithKetraOrcs } from '../tracked/SecretMeetingWithKetraOrcs'
import { SecretMeetingWithVarkaSilenos } from '../tracked/SecretMeetingWithVarkaSilenos'
import { ParcelDelivery } from '../tracked/ParcelDelivery'
import { WhereaboutsOfTheArchaeologist } from '../tracked/WhereaboutsOfTheArchaeologist'
import { SweetWhispers } from '../tracked/SweetWhispers'
import { TheComingDarkness } from '../tracked/TheComingDarkness'
import { LightAndDarkness } from '../tracked/LightAndDarkness'
import { MeetingWithTheGoldenRam } from '../tracked/MeetingWithTheGoldenRam'
import { GoToThePastureland } from '../tracked/GoToThePastureland'
import { BringUpWithLove } from '../tracked/BringUpWithLove'
import { HiddenTruth } from '../tracked/HiddenTruth'
import { TragedyInVonHellmannForest } from '../tracked/TragedyInVonHellmannForest'
import { LidiasHeart } from '../tracked/LidiasHeart'
import { InhabitantsOfTheForestOfTheDead } from '../tracked/InhabitantsOfTheForestOfTheDead'
import { HidingBehindTheTruth } from '../tracked/HidingBehindTheTruth'
import { TiredOfWaiting } from '../tracked/TiredOfWaiting'
import { ChestCaughtWithABaitOfWind } from '../tracked/ChestCaughtWithABaitOfWind'
import { ChestCaughtWithABaitOfIcyAir } from '../tracked/ChestCaughtWithABaitOfIcyAir'
import { ChestCaughtWithABaitOfEarth } from '../tracked/ChestCaughtWithABaitOfEarth'
import { ChestCaughtWithABaitOfFire } from '../tracked/ChestCaughtWithABaitOfFire'
import { SecretBuriedInTheSwamp } from '../tracked/SecretBuriedInTheSwamp'
import { AnObviousLie } from '../tracked/AnObviousLie'
import { MakeAPairOfDressShoes } from '../tracked/MakeAPairOfDressShoes'
import { InSearchOfCloth } from '../tracked/InSearchOfCloth'
import { FindGlitteringJewelry } from '../tracked/FindGlitteringJewelry'
import { MakeASewingKit } from '../tracked/MakeASewingKit'
import { MakeFormalWear } from '../tracked/MakeFormalWear'
import { DragonFangs } from '../tracked/DragonFangs'
import { RedEyedInvaders } from '../tracked/RedEyedInvaders'
import { ASpecialOrder } from '../tracked/ASpecialOrder'
import { HelpTheUncle } from '../tracked/HelpTheUncle'
import { HelpTheSister } from '../tracked/HelpTheSister'
import { HelpTheSon } from '../tracked/HelpTheSon'
import { ToTalkingIsland } from '../tracked/ToTalkingIsland'
import { OnceMoreInTheArmsOfTheMotherTree } from '../tracked/OnceMoreInTheArmsOfTheMotherTree'
import { IntoTheDarkElvenForest } from '../tracked/IntoTheDarkElvenForest'
import { ToTheImmortalPlateau } from '../tracked/ToTheImmortalPlateau'
import { TheRoadHome } from '../tracked/TheRoadHome'
import { LanoscosSpecialBait } from '../tracked/LanoscosSpecialBait'
import { OFullesSpecialBait } from '../tracked/OFullesSpecialBait'
import { WilliesSpecialBait } from '../tracked/WilliesSpecialBait'
import { LinnaeusSpecialBait } from '../tracked/LinnaeusSpecialBait'
import { GoodWorksReward } from '../tracked/GoodWorksReward'
import { LawEnforcement } from '../tracked/LawEnforcement'
import { PathOfTheTrooper } from '../tracked/PathOfTheTrooper'
import { PathOfTheWarder } from '../tracked/PathOfTheWarder'
import { CertifiedBerserker } from '../tracked/CertifiedBerserker'
import { CertifiedSoulBreaker } from '../tracked/CertifiedSoulBreaker'
import { CertifiedArbalester } from '../tracked/CertifiedArbalester'
import { SwordOfSolidarity } from '../tracked-100/SwordOfSolidarity'
import { SeaOfSporesFever } from '../tracked-100/SeaOfSporesFever'
import { SpiritOfCraftsman } from '../tracked-100/SpiritOfCraftsman'
import { SpiritOfMirrors } from '../tracked-100/SpiritOfMirrors'
import { SkirmishWithOrcs } from '../tracked-100/SkirmishWithOrcs'
import { ForgottenTruth } from '../tracked-100/ForgottenTruth'
import { MercilessPunishment } from '../tracked-100/MercilessPunishment'
import { JumbleTumbleDiamondFuss } from '../tracked-100/JumbleTumbleDiamondFuss'
import { InSearchOfTheNest } from '../tracked-100/InSearchOfTheNest'
import { ToThePrimevalIsle } from '../tracked-100/ToThePrimevalIsle'
import { ElrokianHuntersProof } from '../tracked-100/ElrokianHuntersProof'
import { WalkOfFate } from '../tracked-100/WalkOfFate'
import { StatusOfTheBeaconTower } from '../tracked-100/StatusOfTheBeaconTower'
import { ResurrectionOfAnOldManager } from '../tracked-100/ResurrectionOfAnOldManager'
import { TheOtherSideOfTruth } from '../tracked-100/TheOtherSideOfTruth'
import { BeyondTheHillsOfWinter } from '../tracked-100/BeyondTheHillsOfWinter'
import { TheOceanOfDistantStars } from '../tracked-100/TheOceanOfDistantStars'
import { ToLeadAndBeLed } from '../tracked-100/ToLeadAndBeLed'
import { LastImperialPrince } from '../tracked-100/LastImperialPrince'
import { PavelsLastResearch } from '../tracked-100/PavelsLastResearch'
import { PavelTheGiant } from '../tracked-100/PavelTheGiant'
import { OminousNews } from '../tracked-100/OminousNews'
import { TheLeaderAndTheFollower } from '../tracked-100/TheLeaderAndTheFollower'
import { MeetingTheElroki } from '../tracked-100/MeetingTheElroki'
import { TheNameOfEvilPartOne } from '../tracked-100/TheNameOfEvilPartOne'
import { TheNameOfEvilPartTwo } from '../tracked-100/TheNameOfEvilPartTwo'
import { PailakaSongOfIceAndFire } from '../tracked-100/PailakaSongOfIceAndFire'
import { PailakaDevilsLegacy } from '../tracked-100/PailakaDevilsLegacy'
import { PathToHellbound } from '../tracked-100/PathToHellbound'
import { BirdInACage } from '../tracked-100/BirdInACage'
import { MatrasCuriosity } from '../tracked-100/MatrasCuriosity'
import { ThatsBloodyHot } from '../tracked-100/ThatsBloodyHot'
import { TempleMissionary } from '../tracked-100/TempleMissionary'
import { TempleExecutor } from '../tracked-100/TempleExecutor'
import { MoreThanMeetsTheEye } from '../tracked-100/MoreThanMeetsTheEye'
import { TempleChampionPartOne } from '../tracked-100/TempleChampionPartOne'
import { TempleChampionPartTwo } from '../tracked-100/TempleChampionPartTwo'
import { ShadowFoxPartOne } from '../tracked-100/ShadowFoxPartOne'
import { ShadowFoxPartTwo } from '../tracked-100/ShadowFoxPartTwo'
import { ShadowFoxPartThree } from '../tracked-100/ShadowFoxPartThree'
import { FallenAngelRequestOfDawn } from '../tracked-100/FallenAngelRequestOfDawn'
import { FallenAngelRequestOfDusk } from '../tracked-100/FallenAngelRequestOfDusk'
import { PathtoBecominganEliteMercenary } from '../tracked-100/PathtoBecominganEliteMercenary'
import { TheZeroHour } from '../tracked-100/TheZeroHour'
import { PathtoBecominganExaltedMercenary } from '../tracked-100/PathtoBecominganExaltedMercenary'
import { CureForFever } from '../tracked-100/CureForFever'
import { ShardsOfGolem } from '../tracked-100/ShardsOfGolem'
import { DeliverGoods } from '../tracked-100/DeliverGoods'
import { SacrificeToTheSea } from '../tracked-100/SacrificeToTheSea'
import { FindSirWindawood } from '../tracked-100/FindSirWindawood'
import { MillenniumLove } from '../tracked-100/MillenniumLove'
import { RecoverSmuggledGoods } from '../tracked-100/RecoverSmuggledGoods'
import { SeedOfEvil } from '../tracked-100/SeedOfEvil'
import { ProtectTheWaterSource } from '../tracked-100/ProtectTheWaterSource'
import { NerupasRequest } from '../tracked-100/NerupasRequest'
import { FruitOfTheMotherTree } from '../tracked-100/FruitOfTheMotherTree'
import { CurseOfTheUndergroundFortress } from '../tracked-100/CurseOfTheUndergroundFortress'
import { LegacyOfThePoet } from '../tracked-100/LegacyOfThePoet'
import { BloodFiend } from '../tracked-100/BloodFiend'
import { ShilensHunt } from '../tracked-100/ShilensHunt'
import { MassOfDarkness } from '../tracked-100/MassOfDarkness'
import { DwarvenKinship } from '../tracked-100/DwarvenKinship'
import { DeliverSupplies } from '../tracked-100/DeliverSupplies'
import { OffspringOfNightmares } from '../tracked-100/OffspringOfNightmares'
import { DangerousSeduction } from '../tracked-100/DangerousSeduction'
import { ActsOfEvil } from '../tracked-100/ActsOfEvil'
import { NewHorizons } from '../tracked-100/NewHorizons'
import { ToTheIsleOfSouls } from '../tracked-100/ToTheIsleOfSouls'
import { SupplyCheck } from '../tracked-100/SupplyCheck'
import { TheWayOfTheWarrior } from '../tracked-100/TheWayOfTheWarrior'
import { StepsForHonor } from '../tracked-100/StepsForHonor'
import { IconicTrinity } from '../tracked-100/IconicTrinity'
import { IntoTheLargeCavern } from '../tracked-100/IntoTheLargeCavern'
import { NewRecruits } from '../tracked-100/NewRecruits'
import { RelicExploration } from '../tracked-100/RelicExploration'
import { ArtOfPersuasion } from '../tracked-100/ArtOfPersuasion'
import { NikolasCooperation } from '../tracked-100/NikolasCooperation'
import { ContractExecution } from '../tracked-100/ContractExecution'
import { NikolasHeart } from '../tracked-100/NikolasHeart'
import { SealRemoval } from '../tracked-100/SealRemoval'
import { ContractCompletion } from '../tracked-100/ContractCompletion'
import { LostDream } from '../tracked-100/LostDream'
import { VainConclusion } from '../tracked-100/VainConclusion'
import { SevenSignsSeriesOfDoubt } from '../tracked-100/SevenSignsSeriesOfDoubt'
import { SevenSignsDyingMessage } from '../tracked-100/SevenSignsDyingMessage'
import { SevenSignsMammonsContract } from '../tracked-100/SevenSignsMammonsContract'
import { SevenSignsSecretRitualOfThePriests } from '../tracked-100/SevenSignsSecretRitualOfThePriests'
import { SevenSignsSealOfTheEmperor } from '../tracked-100/SevenSignsSealOfTheEmperor'
import { SevenSignsTheSacredBookOfSeal } from '../tracked-100/SevenSignsTheSacredBookOfSeal'
import { SevenSignsEmbryo } from '../tracked-100/SevenSignsEmbryo'
import { TrialOfTheChallenger } from '../tracked-200/TrialOfTheChallenger'
import { TrialOfDuty } from '../tracked-200/TrialOfDuty'
import { TrialOfTheSeeker } from '../tracked-200/TrialOfTheSeeker'
import { TrialOfTheScholar } from '../tracked-200/TrialOfTheScholar'
import { TrialOfThePilgrim } from '../tracked-200/TrialOfThePilgrim'
import { TrialOfTheGuildsman } from '../tracked-200/TrialOfTheGuildsman'
import { TestimonyOfTrust } from '../tracked-200/TestimonyOfTrust'
import { TestimonyOfLife } from '../tracked-200/TestimonyOfLife'
import { TestimonyOfFate } from '../tracked-200/TestimonyOfFate'
import { TestimonyOfGlory } from '../tracked-200/TestimonyOfGlory'
import { TestimonyOfProsperity } from '../tracked-200/TestimonyOfProsperity'
import { TestOfTheDuelist } from '../tracked-200/TestOfTheDuelist'
import { TestOfTheChampion } from '../tracked-200/TestOfTheChampion'
import { TestOfSagittarius } from '../tracked-200/TestOfSagittarius'
import { TestOfTheSearcher } from '../tracked-200/TestOfTheSearcher'
import { TestOfTheHealer } from '../tracked-200/TestOfTheHealer'
import { TestOfTheReformer } from '../tracked-200/TestOfTheReformer'
import { TestOfMagus } from '../tracked-200/TestOfMagus'
import { TestOfWitchcraft } from '../tracked-200/TestOfWitchcraft'
import { TestOfTheSummoner } from '../tracked-200/TestOfTheSummoner'
import { TestOfTheMaestro } from '../tracked-200/TestOfTheMaestro'
import { TestOfTheLord } from '../tracked-200/TestOfTheLord'
import { TestOfTheWarSpirit } from '../tracked-200/TestOfTheWarSpirit'
import { FatesWhisper } from '../tracked-200/FatesWhisper'
import { MimirsElixir } from '../tracked-200/MimirsElixir'
import { SeedsOfChaos } from '../tracked-200/SeedsOfChaos'
import { WindsOfChange } from '../tracked-200/WindsOfChange'
import { SuccessFailureOfBusiness } from '../tracked-200/SuccessFailureOfBusiness'
import { ImTheOnlyOneYouCanTrust } from '../tracked-200/ImTheOnlyOneYouCanTrust'
import { PossessorOfAPreciousSoulPartOne } from '../tracked-200/PossessorOfAPreciousSoulPartOne'
import { PossessorOfAPreciousSoulPartTwo } from '../tracked-200/PossessorOfAPreciousSoulPartTwo'
import { PossessorOfAPreciousSoulPartThree } from '../tracked-200/PossessorOfAPreciousSoulPartThree'
import { PossessorOfAPreciousSoulPartFour } from '../tracked-200/PossessorOfAPreciousSoulPartFour'
import { PoisonedPlainsOfTheLizardmen } from '../tracked-200/PoisonedPlainsOfTheLizardmen'
import { WatchWhatYouEat } from '../tracked-200/WatchWhatYouEat'
import { NoSecrets } from '../tracked-200/NoSecrets'
import { ItSmellsDelicious } from '../tracked-200/ItSmellsDelicious'
import { LegendaryTales } from '../tracked-200/LegendaryTales'
import { TheGuardIsBusy } from '../tracked-200/TheGuardIsBusy'
import { BringWolfPelts } from '../tracked-200/BringWolfPelts'
import { RequestFromTheFarmOwner } from '../tracked-200/RequestFromTheFarmOwner'
import { OrcHunting } from '../tracked-200/OrcHunting'
import { CollectorsDream } from '../tracked-200/CollectorsDream'
import { TradeWithTheIvoryTower } from '../tracked-200/TradeWithTheIvoryTower'
import { OrcSubjugation } from '../tracked-200/OrcSubjugation'
import { KeenClaws } from '../tracked-200/KeenClaws'
import { BondsOfSlavery } from '../tracked-200/BondsOfSlavery'
import { PleasOfPixies } from '../tracked-200/PleasOfPixies'
import { WrathOfVerdure } from '../tracked-200/WrathOfVerdure'
import { TracesOfEvil } from '../tracked-200/TracesOfEvil'
import { InventionAmbition } from '../tracked-200/InventionAmbition'
import { TheOneWhoEndsSilence } from '../tracked-200/TheOneWhoEndsSilence'
import { ProofOfValor } from '../tracked-200/ProofOfValor'
import { WrathOfAncestors } from '../tracked-200/WrathOfAncestors'
import { InvadersOfTheHolyLand } from '../tracked-200/InvadersOfTheHolyLand'
import { SkirmishWithTheWerewolves } from '../tracked-200/SkirmishWithTheWerewolves'
import { DarkWingedSpies } from '../tracked-200/DarkWingedSpies'
import { TotemOfTheHestui } from '../tracked-200/TotemOfTheHestui'
import { GatekeepersOffering } from '../tracked-200/GatekeepersOffering'
import { HomeSecurity } from '../tracked-200/HomeSecurity'
import { TargetOfOpportunity } from '../tracked-200/TargetOfOpportunity'
import { TheFoodChain } from '../tracked-200/TheFoodChain'
import { HeadForTheHills } from '../tracked-200/HeadForTheHills'
import { TheFewTheProudTheBrave } from '../tracked-200/TheFewTheProudTheBrave'
import { MuertosFeather } from '../tracked-200/MuertosFeather'
import { FabulousFeathers } from '../tracked-200/FabulousFeathers'
import { FiguringItOut } from '../tracked-200/FiguringItOut'
import { HandleWithCare } from '../tracked-200/HandleWithCare'
import { NoMoreSoupForYou } from '../tracked-200/NoMoreSoupForYou'
import { ThreatRemoval } from '../tracked-200/ThreatRemoval'
import { RevengeOfTheRedbonnet } from '../tracked-200/RevengeOfTheRedbonnet'
import { BrigandsSweep } from '../tracked-200/BrigandsSweep'
import { TheHiddenVeins } from '../tracked-200/TheHiddenVeins'
import { CovertBusiness } from '../tracked-200/CovertBusiness'
import { DreamingOfTheSkies } from '../tracked-200/DreamingOfTheSkies'
import { TarantulasSpiderSilk } from '../tracked-200/TarantulasSpiderSilk'
import { GatekeepersFavor } from '../tracked-200/GatekeepersFavor'
import { LizardmensConspiracy } from '../tracked-200/LizardmensConspiracy'
import { GatherIngredientsForPie } from '../tracked-200/GatherIngredientsForPie'
import { HuntingLetoLizardman } from '../tracked-300/HuntingLetoLizardman'
import { CollectArrowheads } from '../tracked-300/CollectArrowheads'
import { CrystalOfFireAndIce } from '../tracked-300/CrystalOfFireAndIce'
import { ControlDeviceOfTheGiants } from '../tracked-300/ControlDeviceOfTheGiants'
import { ReedFieldMaintenance } from '../tracked-300/ReedFieldMaintenance'
import { ForAGoodCause } from '../tracked-300/ForAGoodCause'
import { OnlyWhatRemains } from '../tracked-300/OnlyWhatRemains'
import { ExpulsionOfEvilSpirits } from '../tracked-300/ExpulsionOfEvilSpirits'
import { TakeAdvantageOfTheCrisis } from '../tracked-300/TakeAdvantageOfTheCrisis'
import { CollectSpores } from '../tracked-300/CollectSpores'
import { DestroyPlagueCarriers } from '../tracked-300/DestroyPlagueCarriers'
import { CatchTheWind } from '../tracked-300/CatchTheWind'
import { ScentOfDeath } from '../tracked-300/ScentOfDeath'
import { BonesTellTheFuture } from '../tracked-300/BonesTellTheFuture'
import { SweetestVenom } from '../tracked-300/SweetestVenom'
import { GrimCollector } from '../tracked-300/GrimCollector'
import { VanquishRemnants } from '../tracked-300/VanquishRemnants'
import { RecoverTheFarmland } from '../tracked-300/RecoverTheFarmland'
import { SenseForBusiness } from '../tracked-300/SenseForBusiness'
import { CuriosityOfADwarf } from '../tracked-300/CuriosityOfADwarf'
import { AdeptOfTaste } from '../tracked-300/AdeptOfTaste'
import { ArrowOfVengeance } from '../tracked-300/ArrowOfVengeance'
import { HuntOfTheBlackLion } from '../tracked-300/HuntOfTheBlackLion'
import { TheWishingPotion } from '../tracked-300/TheWishingPotion'
import { TheSongOfTheHunter } from '../tracked-300/TheSongOfTheHunter'
import { CoinsOfMagic } from '../tracked-300/CoinsOfMagic'
import { AudienceWithTheLandDragon } from '../tracked-300/AudienceWithTheLandDragon'
import { AlligatorHunter } from '../tracked-300/AlligatorHunter'
import { SubjugationOfLizardmen } from '../tracked-300/SubjugationOfLizardmen'
import { HuntingForWildBeasts } from '../tracked-300/HuntingForWildBeasts'
import { UnderTheShadowOfTheIvoryTower } from '../tracked-300/UnderTheShadowOfTheIvoryTower'
import { A1000YearsTheEndOfLamentation } from '../tracked-300/A1000YearsTheEndOfLamentation'
import { MethodToRaiseTheDead } from '../tracked-300/MethodToRaiseTheDead'
import { GoGetTheCalculator } from '../tracked-300/GoGetTheCalculator'
import { AnArrogantSearch } from '../tracked-300/AnArrogantSearch'
import { EnhanceYourWeapon } from '../tracked-300/EnhanceYourWeapon'
import { BlackSwan } from '../tracked-300/BlackSwan'
import { HelpRoodRaiseANewPet } from '../tracked-300/HelpRoodRaiseANewPet'
import { ConquestOfAlligatorIsland } from '../tracked-300/ConquestOfAlligatorIsland'
import { FamilyHonor } from '../tracked-300/FamilyHonor'
import { DigUpTheSeaOfSpores } from '../tracked-300/DigUpTheSeaOfSpores'
import { WarehouseKeepersAmbition } from '../tracked-300/WarehouseKeepersAmbition'
import { IllegitimateChildOfTheGoddess } from '../tracked-300/IllegitimateChildOfTheGoddess'
import { ForASleeplessDeadman } from '../tracked-300/ForASleeplessDeadman'
import { PlunderTheirSupplies } from '../tracked-300/PlunderTheirSupplies'
import { BardsMandolin } from '../tracked-300/BardsMandolin'
import { SorrowfulSoundOfFlute } from '../tracked-300/SorrowfulSoundOfFlute'
import { JovialAccordion } from '../tracked-300/JovialAccordion'
import { DevilsLegacy } from '../tracked-300/DevilsLegacy'
import { SilverHairedShaman } from '../tracked-300/SilverHairedShaman'
import { ElectrifyingRecharge } from '../tracked-300/ElectrifyingRecharge'
import { TrespassingIntoTheHolyGround } from '../tracked-300/TrespassingIntoTheHolyGround'
import { CollectorOfJewels } from '../tracked-300/CollectorOfJewels'
import { AnElderSowsSeeds } from '../tracked-300/AnElderSowsSeeds'
import { ShrieksOfGhosts } from '../tracked-300/ShrieksOfGhosts'
import { LegacyOfInsolence } from '../tracked-300/LegacyOfInsolence'
import { SupplierOfReagents } from '../tracked-300/SupplierOfReagents'
import { ExplorationOfTheGiantsCavePartOne } from '../tracked-300/ExplorationOfTheGiantsCavePartOne'
import { ExplorationOfTheGiantsCavePartTwo } from '../tracked-300/ExplorationOfTheGiantsCavePartTwo'
import { GrandFeast } from '../tracked-300/GrandFeast'
import { FantasyWine } from '../tracked-300/FantasyWine'
import { BringOutTheFlavorOfIngredients } from '../tracked-300/BringOutTheFlavorOfIngredients'
import { LetsBecomeARoyalMember } from '../tracked-300/LetsBecomeARoyalMember'
import { KailsMagicCoin } from '../tracked-300/KailsMagicCoin'
import { TreasureHunt } from '../tracked-300/TreasureHunt'
import { WarehouseKeepersPastime } from '../tracked-300/WarehouseKeepersPastime'
import { YokeOfThePast } from '../tracked-300/YokeOfThePast'
import { StolenDignity } from '../tracked-300/StolenDignity'
import { PathOfTheWarrior } from '../tracked-400/PathOfTheWarrior'
import { PathOfTheHumanKnight } from '../tracked-400/PathOfTheHumanKnight'
import { PathOfTheRogue } from '../tracked-400/PathOfTheRogue'
import { PathOfTheHumanWizard } from '../tracked-400/PathOfTheHumanWizard'
import { PathOfTheCleric } from '../tracked-400/PathOfTheCleric'
import { PathOfTheElvenKnight } from '../tracked-400/PathOfTheElvenKnight'
import { PathOfTheElvenScout } from '../tracked-400/PathOfTheElvenScout'
import { PathOfTheElvenWizard } from '../tracked-400/PathOfTheElvenWizard'
import { PathOfTheElvenOracle } from '../tracked-400/PathOfTheElvenOracle'
import { PathOfThePalusKnight } from '../tracked-400/PathOfThePalusKnight'
import { PathOfTheAssassin } from '../tracked-400/PathOfTheAssassin'
import { PathOfTheDarkWizard } from '../tracked-400/PathOfTheDarkWizard'
import { PathOfTheShillienOracle } from '../tracked-400/PathOfTheShillienOracle'
import { PathOfTheOrcRaider } from '../tracked-400/PathOfTheOrcRaider'
import { PathOfTheOrcMonk } from '../tracked-400/PathOfTheOrcMonk'
import { PathOfTheOrcShaman } from '../tracked-400/PathOfTheOrcShaman'
import { PathOfTheScavenger } from '../tracked-400/PathOfTheScavenger'
import { PathOfTheArtisan } from '../tracked-400/PathOfTheArtisan'
import { GetAPet } from '../tracked-400/GetAPet'
import { LittleWing } from '../tracked-400/LittleWing'
import { LittleWingsBigAdventure } from '../tracked-400/LittleWingsBigAdventure'
import { RepentYourSins } from '../tracked-400/RepentYourSins'
import { TakeYourBestShot } from '../tracked-400/TakeYourBestShot'
import { WeddingMarch } from '../tracked-400/WeddingMarch'
import { BirthdayPartySong } from '../tracked-400/BirthdayPartySong'
import { GraveRobberRescue } from '../tracked-400/GraveRobberRescue'
import { LuciensAltar } from '../tracked-400/LuciensAltar'
import { FindingtheLostSoldiers } from '../tracked-400/FindingtheLostSoldiers'
import { NotStrongEnoughAlone } from '../tracked-400/NotStrongEnoughAlone'
import { CompletelyLost } from '../tracked-400/CompletelyLost'
import { WingsOfSand } from '../tracked-400/WingsOfSand'
import { DontKnowDontCare } from '../tracked-400/DontKnowDontCare'
import { LostAndFound } from '../tracked-400/LostAndFound'
import { PerfectForm } from '../tracked-400/PerfectForm'
import { RumbleInTheBase } from '../tracked-400/RumbleInTheBase'
import { IMustBeaGenius } from '../tracked-400/IMustBeaGenius'
import { Oath } from '../tracked-400/Oath'
import { ProofOfClanAlliance } from '../tracked-500/ProofOfClanAlliance'
import { PursuitOfClanAmbition } from '../tracked-500/PursuitOfClanAmbition'
import { CompetitionForTheBanditStronghold } from '../tracked-500/CompetitionForTheBanditStronghold'
import { AClansReputation } from '../tracked-500/AClansReputation'
import { AClansFame } from '../tracked-500/AClansFame'
import { AClansPrestige } from '../tracked-500/AClansPrestige'
import { AwlUnderFoot } from '../tracked-500/AwlUnderFoot'
import { BladeUnderFoot } from '../tracked-500/BladeUnderFoot'
import { OlympiadStarter } from '../tracked-500/OlympiadStarter'
import { OlympiadVeteran } from '../tracked-500/OlympiadVeteran'
import { OlympiadUndefeated } from '../tracked-500/OlympiadUndefeated'
import { WatchingEyes } from '../tracked-600/WatchingEyes'
import { ShadowOfLight } from '../tracked-600/ShadowOfLight'
import { DaimonTheWhiteEyedPartOne } from '../tracked-600/DaimonTheWhiteEyedPartOne'
import { DaimonTheWhiteEyedPartTwo } from '../tracked-600/DaimonTheWhiteEyedPartTwo'
import { AllianceWithKetraOrcs } from '../tracked-600/AllianceWithKetraOrcs'
import { BattleAgainstVarkaSilenos } from '../tracked-600/BattleAgainstVarkaSilenos'
import { ProveYourCourageKetra } from '../tracked-600/ProveYourCourageKetra'
import { SlayTheEnemyCommanderKetra } from '../tracked-600/SlayTheEnemyCommanderKetra'
import { MagicalPowerOfWaterPartOne } from '../tracked-600/MagicalPowerOfWaterPartOne'
import { MagicalPowerOfWaterPartTwo } from '../tracked-600/MagicalPowerOfWaterPartTwo'
import { AllianceWithVarkaSilenos } from '../tracked-600/AllianceWithVarkaSilenos'
import { BattleAgainstKetraOrcs } from '../tracked-600/BattleAgainstKetraOrcs'
import { ProveYourCourageVarka } from '../tracked-600/ProveYourCourageVarka'
import { SlayTheEnemyCommanderVarka } from '../tracked-600/SlayTheEnemyCommanderVarka'
import { MagicalPowerOfFirePartOne } from '../tracked-600/MagicalPowerOfFirePartOne'
import { GatherTheFlames } from '../tracked-600/GatherTheFlames'
import { IntoTheFlame } from '../tracked-600/IntoTheFlame'
import { RelicsOfTheOldEmpire } from '../tracked-600/RelicsOfTheOldEmpire'
import { FourGoblets } from '../tracked-600/FourGoblets'
import { EggDelivery } from '../tracked-600/EggDelivery'
import { SpecialtyLiquorDelivery } from '../tracked-600/SpecialtyLiquorDelivery'
import { TheFinestFood } from '../tracked-600/TheFinestFood'
import { TheFinestIngredientsPartOne } from '../tracked-600/TheFinestIngredientsPartOne'
import { TheFinestIngredientsPartTwo } from '../tracked-600/TheFinestIngredientsPartTwo'
import { ADarkTwilight } from '../tracked-600/ADarkTwilight'
import { HeartInSearchOfPower } from '../tracked-600/HeartInSearchOfPower'
import { HuntGoldenRam } from '../tracked-600/HuntGoldenRam'
import { CleanUpTheSwampOfScreams } from '../tracked-600/CleanUpTheSwampOfScreams'
import { DeliciousTopChoiceMeat } from '../tracked-600/DeliciousTopChoiceMeat'
import { NecromancersRequest } from '../tracked-600/NecromancersRequest'
import { InTheForgottenVillage } from '../tracked-600/InTheForgottenVillage'
import { InSearchOfFragmentsOfDimension } from '../tracked-600/InSearchOfFragmentsOfDimension'
import { IntoTheDimensionalRift } from '../tracked-600/IntoTheDimensionalRift'
import { TruthBeyond } from '../tracked-600/TruthBeyond'
import { ThroughOnceMore } from '../tracked-600/ThroughOnceMore'
import { SeekersOfTheHolyGrail } from '../tracked-600/SeekersOfTheHolyGrail'
import { AttackSailren } from '../tracked-600/AttackSailren'
import { APowerfulPrimevalCreature } from '../tracked-600/APowerfulPrimevalCreature'
import { RiseAndFallOfTheElrokiTribe } from '../tracked-600/RiseAndFallOfTheElrokiTribe'
import { GraveRobberAnnihilation } from '../tracked-600/GraveRobberAnnihilation'
import { GhostsOfBatur } from '../tracked-600/GhostsOfBatur'
import { InfluxOfMachines } from '../tracked-600/InfluxOfMachines'
import { AnIceMerchantsDream } from '../tracked-600/AnIceMerchantsDream'
import { ALooterAndARailroadMan } from '../tracked-600/ALooterAndARailroadMan'
import { ABrokenDream } from '../tracked-600/ABrokenDream'
import { RunawayYouth } from '../tracked-600/RunawayYouth'
import { AnAgedExAdventurer } from '../tracked-600/AnAgedExAdventurer'
import { WildMaiden } from '../tracked-600/WildMaiden'
import { JourneyToASettlement } from '../tracked-600/JourneyToASettlement'
import { AGrandPlanForTamingWildBeasts } from '../tracked-600/AGrandPlanForTamingWildBeasts'
import { IdRatherBeCollectingFairyBreath } from '../tracked-600/IdRatherBeCollectingFairyBreath'
import { AidingTheFloranVillage } from '../tracked-600/AidingTheFloranVillage'
import { MakingTheHarvestGroundsSafe } from '../tracked-600/MakingTheHarvestGroundsSafe'
import { AGameOfCards } from '../tracked-600/AGameOfCards'
import { SeductiveWhispers } from '../tracked-600/SeductiveWhispers'
import { DefeatTheElrokianRaiders } from '../tracked-600/DefeatTheElrokianRaiders'
import { JudesRequest } from '../tracked-600/JudesRequest'
import { MatrasSuspiciousRequest } from '../tracked-600/MatrasSuspiciousRequest'
import { HowtoOpposeEvil } from '../tracked-600/HowtoOpposeEvil'
import { DefendTheHallOfSuffering } from '../tracked-600/DefendTheHallOfSuffering'
import { GuardianOfTheSkies } from '../tracked-600/GuardianOfTheSkies'
import { ProofOfExistence } from '../tracked-700/ProofOfExistence'
import { ATrapForRevenge } from '../tracked-700/ATrapForRevenge'
import { PathToBecomingALordGludio } from '../tracked-700/PathToBecomingALordGludio'
import { HowLavasaurusesAreMade } from '../tracked-900/HowLavasaurusesAreMade'
import { ReclaimOurEra } from '../tracked-900/ReclaimOurEra'
import { TheCallOfAntharas } from '../tracked-900/TheCallOfAntharas'
import { DragonTrophyAntharas } from '../tracked-900/DragonTrophyAntharas'
import { RefinedDragonBlood } from '../tracked-900/RefinedDragonBlood'
import { TheCallOfValakas } from '../tracked-900/TheCallOfValakas'
import { DragonTrophyValakas } from '../tracked-900/DragonTrophyValakas'
import { FallenAngelSelect } from '../tracked-900/FallenAngelSelect'
import { JourneyToGracia } from '../tracked-10200/JourneyToGracia'
import { ToTheSeedOfInfinity } from '../tracked-10200/ToTheSeedOfInfinity'
import { ToTheSeedOfDestruction } from '../tracked-10200/ToTheSeedOfDestruction'
import { BirthOfTheSeed } from '../tracked-10200/BirthOfTheSeed'
import { TheEnvelopingDarkness } from '../tracked-10200/TheEnvelopingDarkness'
import { LightFragment } from '../tracked-10200/LightFragment'
import { GoodDayToFly } from '../tracked-10200/GoodDayToFly'
import { CollectingInTheAir } from '../tracked-10200/CollectingInTheAir'
import { ContainingTheAttributePower } from '../tracked-10200/ContainingTheAttributePower'
import { MutatedKaneusGludio } from '../tracked-10200/MutatedKaneusGludio'
import { MutatedKaneusDion } from '../tracked-10200/MutatedKaneusDion'
import { MutatedKaneusHeine } from '../tracked-10200/MutatedKaneusHeine'
import { MutatedKaneusOren } from '../tracked-10200/MutatedKaneusOren'
import { MutatedKaneusSchuttgart } from '../tracked-10200/MutatedKaneusSchuttgart'
import { MutatedKaneusRune } from '../tracked-10200/MutatedKaneusRune'
import { RequestOfIceMerchant } from '../tracked-10200/RequestOfIceMerchant'
import { AcquisitionOfDivineSword } from '../tracked-10200/AcquisitionOfDivineSword'
import { MeetingSirra } from '../tracked-10200/MeetingSirra'
import { ReunionWithSirra } from '../tracked-10200/ReunionWithSirra'
import { StoryOfThoseLeft } from '../tracked-10200/StoryOfThoseLeft'
import { SecretMission } from '../tracked-10200/SecretMission'
import { FadeToBlack } from '../tracked-10200/FadeToBlack'
import { LandDragonConqueror } from '../tracked-10200/LandDragonConqueror'
import { FireDragonDestroyer } from '../tracked-10200/FireDragonDestroyer'
import { SevenSignsGirlOfDoubt } from '../tracked-10200/SevenSignsGirlOfDoubt'
import {
    SevenSignsForbiddenBookOfTheElmoreAdenKingdom,
} from '../tracked-10200/SevenSignsForbiddenBookOfTheElmoreAdenKingdom'
import { SevenSignsToTheMonasteryOfSilence } from '../tracked-10200/SevenSignsToTheMonasteryOfSilence'
import { SevenSignsSolinasTomb } from '../tracked-10200/SevenSignsSolinasTomb'
import { SevenSignsOneWhoSeeksThePowerOfTheSeal } from '../tracked-10200/SevenSignsOneWhoSeeksThePowerOfTheSeal'
import { ZakenEmbroideredSoulCloak } from '../tracked-10500/ZakenEmbroideredSoulCloak'
import { FreyaEmbroideredSoulCloak } from '../tracked-10500/FreyaEmbroideredSoulCloak'
import { FrintezzaEmbroideredSoulCloak } from '../tracked-10500/FrintezzaEmbroideredSoulCloak'
import { JewelOfAntharas } from '../tracked-10500/JewelOfAntharas'
import { JewelOfValakas } from '../tracked-10500/JewelOfValakas'
import { SagaOfTheDoombringer } from '../tracked/SagaOfTheDoombringer'
import { SagaOfTheSoulHound } from '../tracked/SagaOfTheSoulHound'
import { SagaOfTheTrickster } from '../tracked/SagaOfTheTrickster'
import { SagaOfThePhoenixKnight } from '../tracked/SagaOfThePhoenixKnight'
import { SagaOfEvasTemplar } from '../tracked/SagaOfEvasTemplar'
import { SagaOfTheDuelist } from '../tracked/SagaOfTheDuelist'
import { SagaOfTheDreadnought } from '../tracked/SagaOfTheDreadnought'
import { SagaOfTheTitan } from '../tracked/SagaOfTheTitan'
import { SagaOfTheGrandKhavatari } from '../tracked/SagaOfTheGrandKhavatari'
import { SagaOfTheSwordMuse } from '../tracked/SagaOfTheSwordMuse'
import { SagaOfTheDominator } from '../tracked/SagaOfTheDominator'
import { SagaOfTheDoomcryer } from '../tracked/SagaOfTheDoomcryer'
import { SagaOfTheAdventurer } from '../tracked/SagaOfTheAdventurer'
import { SagaOfTheWindRider } from '../tracked/SagaOfTheWindRider'
import { SagaOfTheGhostHunter } from '../tracked/SagaOfTheGhostHunter'
import { SagaOfTheSagittarius } from '../tracked/SagaOfTheSagittarius'
import { SagaOfTheMoonlightSentinel } from '../tracked/SagaOfTheMoonlightSentinel'
import { SagaOfTheGhostSentinel } from '../tracked/SagaOfTheGhostSentinel'
import { SagaOfTheCardinal } from '../tracked/SagaOfTheCardinal'
import { SagaOfTheHierophant } from '../tracked/SagaOfTheHierophant'
import { SagaOfEvasSaint } from '../tracked/SagaOfEvasSaint'
import { SagaOfTheArchmage } from '../tracked/SagaOfTheArchmage'
import { SagaOfTheMysticMuse } from '../tracked/SagaOfTheMysticMuse'
import { SagaOfTheStormScreamer } from '../tracked/SagaOfTheStormScreamer'
import { SagaOfTheArcanaLord } from '../tracked/SagaOfTheArcanaLord'
import { SagaOfTheElementalMaster } from '../tracked/SagaOfTheElementalMaster'
import { SagaOfTheSpectralMaster } from '../tracked/SagaOfTheSpectralMaster'
import { SagaOfTheSoultaker } from '../tracked/SagaOfTheSoultaker'
import { SagaOfTheHellKnight } from '../tracked/SagaOfTheHellKnight'
import { SagaOfTheSpectralDancer } from '../tracked/SagaOfTheSpectralDancer'
import { SagaOfTheShillienTemplar } from '../tracked/SagaOfTheShillienTemplar'
import { SagaOfTheShillienSaint } from '../tracked/SagaOfTheShillienSaint'
import { SagaOfTheFortuneSeeker } from '../tracked/SagaOfTheFortuneSeeker'
import { SagaOfTheMaestro } from '../tracked-100/SagaOfTheMaestro'

export const TrackedQuestList: Array<ListenerLogic> = [
    new GetFinishingShots(),
    new Tutorial(),
    new LettersOfLove(),
    new WhatWomenWant(),
    new WillTheSealBeBroken(),
    new LongLiveThePaagrioLord(),
    new StepIntoTheFuture(),
    new ATripBegins(),
    new AnAdventureBegins(),
    new MinersFavor(),
    new IntoTheCityOfHumans(),
    new IntoTheWorld(),
    new SecretMeetingWithKetraOrcs(),
    new SecretMeetingWithVarkaSilenos(),
    new ParcelDelivery(),
    new WhereaboutsOfTheArchaeologist(),
    new SweetWhispers(),
    new TheComingDarkness(),
    new LightAndDarkness(),
    new MeetingWithTheGoldenRam(),
    new GoToThePastureland(),
    new BringUpWithLove(),
    new HiddenTruth(),
    new TragedyInVonHellmannForest(),
    new LidiasHeart(),
    new InhabitantsOfTheForestOfTheDead(),
    new HidingBehindTheTruth(),
    new TiredOfWaiting(),
    new ChestCaughtWithABaitOfWind(),
    new ChestCaughtWithABaitOfIcyAir(),
    new ChestCaughtWithABaitOfEarth(),
    new ChestCaughtWithABaitOfFire(),
    new SecretBuriedInTheSwamp(),
    new AnObviousLie(),
    new MakeAPairOfDressShoes(),
    new InSearchOfCloth(),
    new FindGlitteringJewelry(),
    new MakeASewingKit(),
    new MakeFormalWear(),
    new DragonFangs(),
    new RedEyedInvaders(),
    new ASpecialOrder(),
    new HelpTheUncle(),
    new HelpTheSister(),
    new HelpTheSon(),
    new ToTalkingIsland(),
    new OnceMoreInTheArmsOfTheMotherTree(),
    new IntoTheDarkElvenForest(),
    new ToTheImmortalPlateau(),
    new TheRoadHome(),
    new LanoscosSpecialBait(),
    new OFullesSpecialBait(),
    new WilliesSpecialBait(),
    new LinnaeusSpecialBait(),
    new GoodWorksReward(),
    new LawEnforcement(),
    new PathOfTheTrooper(),
    new PathOfTheWarder(),
    new CertifiedBerserker(),
    new CertifiedSoulBreaker(),
    new CertifiedArbalester(),
    new SwordOfSolidarity(),
    new SeaOfSporesFever(),
    new SpiritOfCraftsman(),
    new SpiritOfMirrors(),
    new SkirmishWithOrcs(),
    new ForgottenTruth(),
    new MercilessPunishment(),
    new JumbleTumbleDiamondFuss(),
    new InSearchOfTheNest(),
    new ToThePrimevalIsle(),
    new ElrokianHuntersProof(),
    new WalkOfFate(),
    new StatusOfTheBeaconTower(),
    new ResurrectionOfAnOldManager(),
    new TheOtherSideOfTruth(),
    new BeyondTheHillsOfWinter(),
    new TheOceanOfDistantStars(),
    new ToLeadAndBeLed(),
    new LastImperialPrince(),
    new PavelsLastResearch(),
    new PavelTheGiant(),
    new OminousNews(),
    new TheLeaderAndTheFollower(),
    new MeetingTheElroki(),
    new TheNameOfEvilPartOne(),
    new TheNameOfEvilPartTwo(),
    new PailakaSongOfIceAndFire(),
    new PailakaDevilsLegacy(),
    new PathToHellbound(),
    new BirdInACage(),
    new MatrasCuriosity(),
    new ThatsBloodyHot(),
    new TempleMissionary(),
    new TempleExecutor(),
    new MoreThanMeetsTheEye(),
    new TempleChampionPartOne(),
    new TempleChampionPartTwo(),
    new ShadowFoxPartOne(),
    new ShadowFoxPartTwo(),
    new ShadowFoxPartThree(),
    new FallenAngelRequestOfDawn(),
    new FallenAngelRequestOfDusk(),
    new TheZeroHour(),
    new PathtoBecominganEliteMercenary(),
    new PathtoBecominganExaltedMercenary(),
    new CureForFever(),
    new ShardsOfGolem(),
    new DeliverGoods(),
    new SacrificeToTheSea(),
    new FindSirWindawood(),
    new MillenniumLove(),
    new RecoverSmuggledGoods(),
    new SeedOfEvil(),
    new ProtectTheWaterSource(),
    new NerupasRequest(),
    new FruitOfTheMotherTree(),
    new CurseOfTheUndergroundFortress(),
    new LegacyOfThePoet(),
    new BloodFiend(),
    new ShilensHunt(),
    new MassOfDarkness(),
    new DwarvenKinship(),
    new DeliverSupplies(),
    new OffspringOfNightmares(),
    new DangerousSeduction(),
    new ActsOfEvil(),
    new NewHorizons(),
    new ToTheIsleOfSouls(),
    new SupplyCheck(),
    new TheWayOfTheWarrior(),
    new StepsForHonor(),
    new IconicTrinity(),
    new IntoTheLargeCavern(),
    new NewRecruits(),
    new RelicExploration(),
    new ArtOfPersuasion(),
    new NikolasCooperation(),
    new ContractExecution(),
    new NikolasHeart(),
    new SealRemoval(),
    new ContractCompletion(),
    new LostDream(),
    new VainConclusion(),
    new SevenSignsSeriesOfDoubt(),
    new SevenSignsDyingMessage(),
    new SevenSignsMammonsContract(),
    new SevenSignsSecretRitualOfThePriests(),
    new SevenSignsSealOfTheEmperor(),
    new SevenSignsTheSacredBookOfSeal(),
    new SevenSignsEmbryo(),
    new TrialOfTheChallenger(),
    new TrialOfDuty(),
    new TrialOfTheSeeker(),
    new TrialOfTheScholar(),
    new TrialOfThePilgrim(),
    new TrialOfTheGuildsman(),
    new TestimonyOfTrust(),
    new TestimonyOfLife(),
    new TestimonyOfFate(),
    new TestimonyOfGlory(),
    new TestimonyOfProsperity(),
    new TestOfTheDuelist(),
    new TestOfTheChampion(),
    new TestOfSagittarius(),
    new TestOfTheSearcher(),
    new TestOfTheHealer(),
    new TestOfTheReformer(),
    new TestOfMagus(),
    new TestOfWitchcraft(),
    new TestOfTheSummoner(),
    new TestOfTheMaestro(),
    new TestOfTheLord(),
    new TestOfTheWarSpirit(),
    new FatesWhisper(),
    new MimirsElixir(),
    new SeedsOfChaos(),
    new WindsOfChange(),
    new SuccessFailureOfBusiness(),
    new ImTheOnlyOneYouCanTrust(),
    new PossessorOfAPreciousSoulPartOne(),
    new PossessorOfAPreciousSoulPartTwo(),
    new PossessorOfAPreciousSoulPartThree(),
    new PossessorOfAPreciousSoulPartFour(),
    new PoisonedPlainsOfTheLizardmen(),
    new WatchWhatYouEat(),
    new NoSecrets(),
    new ItSmellsDelicious(),
    new LegendaryTales(),
    new TheGuardIsBusy(),
    new BringWolfPelts(),
    new RequestFromTheFarmOwner(),
    new OrcHunting(),
    new CollectorsDream(),
    new TradeWithTheIvoryTower(),
    new OrcSubjugation(),
    new KeenClaws(),
    new BondsOfSlavery(),
    new PleasOfPixies(),
    new WrathOfVerdure(),
    new TracesOfEvil(),
    new InventionAmbition(),
    new TheOneWhoEndsSilence(),
    new ProofOfValor(),
    new WrathOfAncestors(),
    new InvadersOfTheHolyLand(),
    new SkirmishWithTheWerewolves(),
    new DarkWingedSpies(),
    new TotemOfTheHestui(),
    new GatekeepersOffering(),
    new HomeSecurity(),
    new TargetOfOpportunity(),
    new TheFoodChain(),
    new HeadForTheHills(),
    new TheFewTheProudTheBrave(),
    new MuertosFeather(),
    new FabulousFeathers(),
    new FiguringItOut(),
    new HandleWithCare(),
    new NoMoreSoupForYou(),
    new ThreatRemoval(),
    new RevengeOfTheRedbonnet(),
    new BrigandsSweep(),
    new TheHiddenVeins(),
    new CovertBusiness(),
    new DreamingOfTheSkies(),
    new TarantulasSpiderSilk(),
    new GatekeepersFavor(),
    new LizardmensConspiracy(),
    new GatherIngredientsForPie(),
    new HuntingLetoLizardman(),
    new CollectArrowheads(),
    new CrystalOfFireAndIce(),
    new ControlDeviceOfTheGiants(),
    new ReedFieldMaintenance(),
    new ForAGoodCause(),
    new OnlyWhatRemains(),
    new ExpulsionOfEvilSpirits(),
    new TakeAdvantageOfTheCrisis(),
    new CollectSpores(),
    new DestroyPlagueCarriers(),
    new CatchTheWind(),
    new ScentOfDeath(),
    new BonesTellTheFuture(),
    new SweetestVenom(),
    new GrimCollector(),
    new VanquishRemnants(),
    new RecoverTheFarmland(),
    new SenseForBusiness(),
    new CuriosityOfADwarf(),
    new AdeptOfTaste(),
    new ArrowOfVengeance(),
    new HuntOfTheBlackLion(),
    new TheWishingPotion(),
    new TheSongOfTheHunter(),
    new CoinsOfMagic(),
    new AudienceWithTheLandDragon(),
    new AlligatorHunter(),
    new SubjugationOfLizardmen(),
    new HuntingForWildBeasts(),
    new UnderTheShadowOfTheIvoryTower(),
    new A1000YearsTheEndOfLamentation(),
    new MethodToRaiseTheDead(),
    new GoGetTheCalculator(),
    new AnArrogantSearch(),
    new EnhanceYourWeapon(),
    new BlackSwan(),
    new HelpRoodRaiseANewPet(),
    new ConquestOfAlligatorIsland(),
    new FamilyHonor(),
    new DigUpTheSeaOfSpores(),
    new WarehouseKeepersAmbition(),
    new IllegitimateChildOfTheGoddess(),
    new ForASleeplessDeadman(),
    new PlunderTheirSupplies(),
    new BardsMandolin(),
    new SorrowfulSoundOfFlute(),
    new JovialAccordion(),
    new DevilsLegacy(),
    new SilverHairedShaman(),
    new ElectrifyingRecharge(),
    new TrespassingIntoTheHolyGround(),
    new CollectorOfJewels(),
    new AnElderSowsSeeds(),
    new ShrieksOfGhosts(),
    new LegacyOfInsolence(),
    new SupplierOfReagents(),
    new ExplorationOfTheGiantsCavePartOne(),
    new ExplorationOfTheGiantsCavePartTwo(),
    new GrandFeast(),
    new FantasyWine(),
    new BringOutTheFlavorOfIngredients(),
    new LetsBecomeARoyalMember(),
    new KailsMagicCoin(),
    new TreasureHunt(),
    new WarehouseKeepersPastime(),
    new YokeOfThePast(),
    new StolenDignity(),
    new PathOfTheWarrior(),
    new PathOfTheHumanKnight(),
    new PathOfTheRogue(),
    new PathOfTheHumanWizard(),
    new PathOfTheCleric(),
    new PathOfTheElvenKnight(),
    new PathOfTheElvenScout(),
    new PathOfTheElvenWizard(),
    new PathOfTheElvenOracle(),
    new PathOfThePalusKnight(),
    new PathOfTheAssassin(),
    new PathOfTheDarkWizard(),
    new PathOfTheShillienOracle(),
    new PathOfTheOrcRaider(),
    new PathOfTheOrcMonk(),
    new PathOfTheOrcShaman(),
    new PathOfTheScavenger(),
    new PathOfTheArtisan(),
    new GetAPet(),
    new LittleWing(),
    new LittleWingsBigAdventure(),
    new RepentYourSins(),
    new TakeYourBestShot(),
    new WeddingMarch(),
    new BirthdayPartySong(),
    new GraveRobberRescue(),
    new LuciensAltar(),
    new FindingtheLostSoldiers(),
    new NotStrongEnoughAlone(),
    new CompletelyLost(),
    new WingsOfSand(),
    new DontKnowDontCare(),
    new LostAndFound(),
    new PerfectForm(),
    new RumbleInTheBase(),
    new IMustBeaGenius(),
    new Oath(),
    new ProofOfClanAlliance(),
    new PursuitOfClanAmbition(),
    new CompetitionForTheBanditStronghold(),
    new AClansReputation(),
    new AClansFame(),
    new AClansPrestige(),
    new AwlUnderFoot(),
    new BladeUnderFoot(),
    new OlympiadStarter(),
    new OlympiadVeteran(),
    new OlympiadUndefeated(),
    new WatchingEyes(),
    new ShadowOfLight(),
    new DaimonTheWhiteEyedPartOne(),
    new DaimonTheWhiteEyedPartTwo(),
    new AllianceWithKetraOrcs(),
    new BattleAgainstVarkaSilenos(),
    new ProveYourCourageKetra(),
    new SlayTheEnemyCommanderKetra(),
    new MagicalPowerOfWaterPartOne(),
    new MagicalPowerOfWaterPartTwo(),
    new AllianceWithVarkaSilenos(),
    new BattleAgainstKetraOrcs(),
    new ProveYourCourageVarka(),
    new SlayTheEnemyCommanderVarka(),
    new MagicalPowerOfFirePartOne(),
    new GatherTheFlames(),
    new IntoTheFlame(),
    new RelicsOfTheOldEmpire(),
    new FourGoblets(),
    new EggDelivery(),
    new SpecialtyLiquorDelivery(),
    new TheFinestFood(),
    new TheFinestIngredientsPartOne(),
    new TheFinestIngredientsPartTwo(),
    new ADarkTwilight(),
    new HeartInSearchOfPower(),
    new HuntGoldenRam(),
    new CleanUpTheSwampOfScreams(),
    new DeliciousTopChoiceMeat(),
    new NecromancersRequest(),
    new InTheForgottenVillage(),
    new InSearchOfFragmentsOfDimension(),
    new IntoTheDimensionalRift(),
    new TruthBeyond(),
    new ThroughOnceMore(),
    new SeekersOfTheHolyGrail(),
    new AttackSailren(),
    new APowerfulPrimevalCreature(),
    new RiseAndFallOfTheElrokiTribe(),
    new GraveRobberAnnihilation(),
    new GhostsOfBatur(),
    new InfluxOfMachines(),
    new AnIceMerchantsDream(),
    new ALooterAndARailroadMan(),
    new ABrokenDream(),
    new RunawayYouth(),
    new AnAgedExAdventurer(),
    new WildMaiden(),
    new JourneyToASettlement(),
    new AGrandPlanForTamingWildBeasts(),
    new IdRatherBeCollectingFairyBreath(),
    new AidingTheFloranVillage(),
    new MakingTheHarvestGroundsSafe(),
    new AGameOfCards(),
    new SeductiveWhispers(),
    new DefeatTheElrokianRaiders(),
    new JudesRequest(),
    new MatrasSuspiciousRequest(),
    new HowtoOpposeEvil(),
    new DefendTheHallOfSuffering(),
    new GuardianOfTheSkies(),
    new ProofOfExistence(),
    new ATrapForRevenge(),
    new PathToBecomingALordGludio(),
    new HowLavasaurusesAreMade(),
    new ReclaimOurEra(),
    new TheCallOfAntharas(),
    new DragonTrophyAntharas(),
    new RefinedDragonBlood(),
    new TheCallOfValakas(),
    new DragonTrophyValakas(),
    new FallenAngelSelect(),
    new JourneyToGracia(),
    new ToTheSeedOfInfinity(),
    new ToTheSeedOfDestruction(),
    new BirthOfTheSeed(),
    new TheEnvelopingDarkness(),
    new LightFragment(),
    new GoodDayToFly(),
    new CollectingInTheAir(),
    new ContainingTheAttributePower(),
    new MutatedKaneusGludio(),
    new MutatedKaneusDion(),
    new MutatedKaneusHeine(),
    new MutatedKaneusOren(),
    new MutatedKaneusSchuttgart(),
    new MutatedKaneusRune(),
    new RequestOfIceMerchant(),
    new AcquisitionOfDivineSword(),
    new MeetingSirra(),
    new ReunionWithSirra(),
    new StoryOfThoseLeft(),
    new SecretMission(),
    new FadeToBlack(),
    new LandDragonConqueror(),
    new FireDragonDestroyer(),
    new SevenSignsGirlOfDoubt(),
    new SevenSignsForbiddenBookOfTheElmoreAdenKingdom(),
    new SevenSignsToTheMonasteryOfSilence(),
    new SevenSignsSolinasTomb(),
    new SevenSignsOneWhoSeeksThePowerOfTheSeal(),
    new ZakenEmbroideredSoulCloak(),
    new FreyaEmbroideredSoulCloak(),
    new FrintezzaEmbroideredSoulCloak(),
    new JewelOfAntharas(),
    new JewelOfValakas(),
    new SagaOfTheDoombringer(),
    new SagaOfTheSoulHound(),
    new SagaOfTheTrickster(),
    new SagaOfThePhoenixKnight(),
    new SagaOfEvasTemplar(),
    new SagaOfTheSwordMuse(),
    new SagaOfTheDuelist(),
    new SagaOfTheDreadnought(),
    new SagaOfTheTitan(),
    new SagaOfTheGrandKhavatari(),
    new SagaOfTheDominator(),
    new SagaOfTheDoomcryer(),
    new SagaOfTheAdventurer(),
    new SagaOfTheWindRider(),
    new SagaOfTheGhostHunter(),
    new SagaOfTheSagittarius(),
    new SagaOfTheMoonlightSentinel(),
    new SagaOfTheGhostSentinel(),
    new SagaOfTheCardinal(),
    new SagaOfTheHierophant(),
    new SagaOfEvasSaint(),
    new SagaOfTheArchmage(),
    new SagaOfTheMysticMuse(),
    new SagaOfTheStormScreamer(),
    new SagaOfTheArcanaLord(),
    new SagaOfTheElementalMaster(),
    new SagaOfTheSpectralMaster(),
    new SagaOfTheSoultaker(),
    new SagaOfTheHellKnight(),
    new SagaOfTheSpectralDancer(),
    new SagaOfTheShillienTemplar(),
    new SagaOfTheShillienSaint(),
    new SagaOfTheFortuneSeeker(),
    new SagaOfTheMaestro()
]