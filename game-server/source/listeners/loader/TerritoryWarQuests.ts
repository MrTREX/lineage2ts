import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { TerritoryWarTriggers } from '../territoryWar/TerritoryWarTriggers'
import { Aden } from '../territoryWar/locations/Aden'
import { Dion } from '../territoryWar/locations/Dion'
import { Gludio } from '../territoryWar/locations/Gludio'
import { Giran } from '../territoryWar/locations/Giran'
import { Goddard } from '../territoryWar/locations/Goddard'
import { Innadril } from '../territoryWar/locations/Innadril'
import { Oren } from '../territoryWar/locations/Oren'
import { Rune } from '../territoryWar/locations/Rune'
import { Schuttgart } from '../territoryWar/locations/Schuttgart'
import { Catapult } from '../territoryWar/protect/Catapult'
import { SuppliesSafe } from '../territoryWar/protect/SuppliesSafe'
import { EconomicAssociationLeader } from '../territoryWar/protect/EconomicAssociationLeader'
import { MilitaryAssociationLeader } from '../territoryWar/protect/MilitaryAssociationLeader'
import { ReligiousAssociationLeader } from '../territoryWar/protect/ReligiousAssociationLeader'
import { PierceThroughAShield } from '../territoryWar/destroy/PierceThroughAShield'
import { DenyBlessings } from '../territoryWar/destroy/DenyBlessings'
import { DestroyKeyTargets } from '../territoryWar/destroy/DestroyKeyTargets'
import { MakeSpearsDull } from '../territoryWar/destroy/MakeSpearsDull'
import { WeakenTheMagic } from '../territoryWar/destroy/WeakenTheMagic'

export const TerritoryWarQuests: Array<ListenerLogic> = [
    new TerritoryWarTriggers(),
    new Aden(),
    new Dion(),
    new Giran(),
    new Gludio(),
    new Goddard(),
    new Innadril(),
    new Oren(),
    new Rune(),
    new Schuttgart(),
    new Catapult(),
    new EconomicAssociationLeader(),
    new MilitaryAssociationLeader(),
    new ReligiousAssociationLeader(),
    new SuppliesSafe(),
    new DenyBlessings(),
    new DestroyKeyTargets(),
    new MakeSpearsDull(),
    new PierceThroughAShield(),
    new WeakenTheMagic()
]