import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { CharacterBirthday } from '../events/CharacterBirthday'
import { FreyaCelebration } from '../events/FreyaCelebration'
import { GiftOfVitality } from '../events/GiftOfVitality'
import { HeavyMedal } from '../events/HeavyMedal'
import { LoveYourGatekeeper } from '../events/LoveYourGatekeeper'
import { MasterOfEnchanting } from '../events/MasterOfEnchanting'
import { Valentine } from '../events/Valentine'
import { L2Day } from '../events/L2Day'
import { PlayerLevelupRewards } from '../events/PlayerLevelupRewards'

export const EventList: Array<ListenerLogic> = [
    new CharacterBirthday(),
    new FreyaCelebration(),
    new GiftOfVitality(),
    new HeavyMedal(),
    new LoveYourGatekeeper(),
    new MasterOfEnchanting(),
    new Valentine(),
    new L2Day(),
    new PlayerLevelupRewards()
]