import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { WeddingManager } from '../voiceCommands/WeddingManager'

export const VoiceCommands: Array<ListenerLogic> = [
    new WeddingManager(),
]