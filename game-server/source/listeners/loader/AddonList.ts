import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { DelevelManager } from '../addons/DelevelManager'
import { EchoCrystals } from '../addons/EchoCrystals'
import { NewbieNpcFinder } from '../addons/NewbieNpcFinder'
import { DropManager } from '../addons/DropManager'

export const AddonList: Array<ListenerLogic> = [
    new DelevelManager(),
    new EchoCrystals(),
    new NewbieNpcFinder(),
    new DropManager()
]