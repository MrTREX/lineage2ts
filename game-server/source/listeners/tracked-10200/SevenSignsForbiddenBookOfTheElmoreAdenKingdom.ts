import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcApproachedForTalkEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const SOPHIA1 = 32596
const ELCADIA = 32784
const ELCADIA_INSTANCE = 32785
const PILE_OF_BOOKS1 = 32809
const PILE_OF_BOOKS2 = 32810
const PILE_OF_BOOKS3 = 32811
const PILE_OF_BOOKS4 = 32812
const PILE_OF_BOOKS5 = 32813
const SOPHIA2 = 32861
const SOPHIA3 = 32863
const SOLINAS_BIOGRAPHY = 17213
const minimumLevel = 81

export class SevenSignsForbiddenBookOfTheElmoreAdenKingdom extends ListenerLogic {
    constructor() {
        super( 'Q10293_SevenSignsForbiddenBookOfTheElmoreAdenKingdom', 'listeners/tracked-10200/SevenSignsForbiddenBookOfTheElmoreAdenKingdom.ts' )
        this.questId = 10293
        this.questItemIds = [ SOLINAS_BIOGRAPHY ]
    }

    getQuestStartIds(): Array<number> {
        return [ ELCADIA ]
    }

    getApproachedForTalkIds(): Array<number> {
        return [ SOPHIA3 ]
    }

    getTalkIds(): Array<number> {
        return [
            ELCADIA,
            ELCADIA_INSTANCE,
            SOPHIA1,
            SOPHIA2,
            SOPHIA3,
            PILE_OF_BOOKS1,
            PILE_OF_BOOKS2,
            PILE_OF_BOOKS3,
            PILE_OF_BOOKS4,
            PILE_OF_BOOKS5,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q10293_SevenSignsForbiddenBookOfTheElmoreAdenKingdom'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32784-03.htm':
            case '32784-05.html':
            case '32861-13.html':
            case '32863-02.html':
            case '32863-03.html':
                break

            case '32784-04.html':
                state.startQuest()
                break

            case '32784-07.html':
            case '32784-08.html':
                if ( state.isCondition( 8 ) ) {
                    break
                }

                return

            case 'REWARD':
                if ( player.isSubClassActive() ) {
                    return this.getPath( '32784-10.html' )
                }

                await QuestHelper.addExpAndSp( player, 15000000, 1500000 )
                await state.exitQuest( false, true )

                return this.getPath( '32784-09.html' )

            case '32785-02.html':
                if ( state.isCondition( 1 ) ) {
                    break
                }

                return

            case '32785-07.html':
                if ( state.isCondition( 4 ) ) {
                    state.setConditionWithSound( 5, true )
                    break
                }

                return

            case '32596-03.html':
            case '32596-04.html':
                if ( state.getCondition() >= 1 && state.getCondition() < 8 ) {
                    break
                }

                return

            case '32861-02.html':
            case '32861-03.html':
                if ( state.isCondition( 1 ) ) {
                    break
                }

                return

            case '32861-04.html':
                if ( state.isCondition( 1 ) ) {
                    state.setConditionWithSound( 2, true )
                    break
                }

                return

            case '32861-07.html':
                if ( state.isCondition( 3 ) ) {
                    break
                }

                return

            case '32861-08.html':
                if ( state.isCondition( 3 ) ) {
                    state.setConditionWithSound( 4, true )
                    break
                }

                return

            case '32861-11.html':
                if ( state.isCondition( 5 ) ) {
                    state.setConditionWithSound( 6, true )
                    break
                }

                return

            case '32809-02.html':
                if ( state.isCondition( 6 ) ) {
                    state.setConditionWithSound( 7, true )
                    await QuestHelper.giveSingleItem( player, SOLINAS_BIOGRAPHY, 1 )

                    break
                }

                return

            case '32810-02.html':
            case '32811-02.html':
            case '32812-02.html':
            case '32813-02.html':
                if ( state.isCondition( 6 ) ) {
                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onApproachedForTalk( data: NpcApproachedForTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        if ( state.getCondition() >= 1 && state.getCondition() < 8 ) {
            return this.getPath( '32863-01.html' )
        }

        return this.getPath( '32863-04.html' )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== ELCADIA ) {
                    break
                }

                let canProceed: boolean = player.getLevel() >= minimumLevel && player.hasQuestCompleted( 'Q10292_SevenSignsGirlOfDoubt' )
                return this.getPath( canProceed ? '32784-01.htm' : '32784-11.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case ELCADIA:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '32784-06.html' )

                            case 8:
                                return this.getPath( '32784-07.html' )
                        }

                        break

                    case ELCADIA_INSTANCE:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '32785-01.html' )

                            case 2:
                                state.setConditionWithSound( 3, true )
                                return this.getPath( '32785-04.html' )

                            case 3:
                                return this.getPath( '32785-05.html' )

                            case 4:
                                return this.getPath( '32785-06.html' )

                            case 5:
                                return this.getPath( '32785-08.html' )

                            case 6:
                                return this.getPath( '32785-09.html' )

                            case 7:
                                state.setConditionWithSound( 8, true )
                                return this.getPath( '32785-11.html' )

                            case 8:
                                return this.getPath( '32785-12.html' )
                        }

                        break

                    case SOPHIA1:
                        if ( state.getCondition() >= 1 && state.getCondition() < 8 ) {
                            return this.getPath( '32596-01.html' )
                        }

                        return this.getPath( '32596-05.html' )

                    case SOPHIA2:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '32861-01.html' )

                            case 2:
                                return this.getPath( '32861-05.html' )

                            case 3:
                                return this.getPath( '32861-06.html' )

                            case 4:
                                return this.getPath( '32861-09.html' )

                            case 5:
                                return this.getPath( '32861-10.html' )

                            case 6:
                            case 7:
                                return this.getPath( '32861-12.html' )

                            case 8:
                                return this.getPath( '32861-14.html' )
                        }

                        break

                    case PILE_OF_BOOKS1:
                        if ( state.isCondition( 6 ) ) {
                            return this.getPath( '32809-01.html' )
                        }

                        break

                    case PILE_OF_BOOKS2:
                        if ( state.isCondition( 6 ) ) {
                            return this.getPath( '32810-01.html' )
                        }

                        break

                    case PILE_OF_BOOKS3:
                        if ( state.isCondition( 6 ) ) {
                            return this.getPath( '32811-01.html' )
                        }

                        break

                    case PILE_OF_BOOKS4:
                        if ( state.isCondition( 6 ) ) {
                            return this.getPath( '32812-01.html' )
                        }

                        break

                    case PILE_OF_BOOKS5:
                        if ( state.isCondition( 6 ) ) {
                            return this.getPath( '32813-01.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === ELCADIA ) {
                    return this.getPath( '32784-02.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}