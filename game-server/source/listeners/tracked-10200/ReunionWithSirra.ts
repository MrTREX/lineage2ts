import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestHelper } from '../helpers/QuestHelper'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { InstanceWorld } from '../../gameService/models/instancezone/InstanceWorld'
import { InstanceManager } from '../../gameService/instancemanager/InstanceManager'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const RAFFORTY = 32020
const JINIA = 32760
const SIRRA = 32762
const JINIA2 = 32781
const BLACK_FROZEN_CORE = 15470
const minimumLevel = 82
const EXIT_LOC = new Location( 113793, -109342, -845, 0 )
const variableNames = {
    value: 'vl',
}

export class ReunionWithSirra extends ListenerLogic {
    constructor() {
        super( 'Q10286_ReunionWithSirra', 'listeners/tracked-10200/ReunionWithSirra.ts' )
        this.questId = 10286
        this.questItemIds = [ BLACK_FROZEN_CORE ]
    }

    getQuestStartIds(): Array<number> {
        return [ RAFFORTY ]
    }

    getTalkIds(): Array<number> {
        return [ RAFFORTY, JINIA, SIRRA, JINIA2 ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q10286_ReunionWithSirra'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32020-02.htm':
                state.startQuest()
                state.setMemoState( 1 )
                break

            case '32020-03.html':
                if ( state.isMemoState( 1 ) ) {
                    break
                }

                return

            case '32760-02.html':
            case '32760-03.html':
            case '32760-04.html':
                if ( state.isMemoState( 1 ) ) {
                    break
                }

                return

            case '32760-05.html':
                if ( state.isMemoState( 1 ) ) {
                    let sirra: L2Npc = QuestHelper.addGenericSpawn( null, SIRRA, -23905, -8790, -5384, 56238, false, 0, false, L2World.getObjectById( data.characterId ).getInstanceId() )
                    BroadcastHelper.broadcastNpcSayStringId( sirra, NpcSayType.NpcAll, NpcStringIds.YOU_ADVANCED_BRAVELY_BUT_GOT_SUCH_A_TINY_RESULT_HOHOHO )

                    state.setVariable( variableNames.value, 1 )
                    state.setConditionWithSound( 3, true )

                    break
                }

                return

            case '32760-07.html':
                if ( state.isMemoState( 1 ) && state.getVariable( variableNames.value ) === 2 ) {
                    state.setMemoState( 2 )

                    let world: InstanceWorld = InstanceManager.getPlayerWorld( data.playerId )
                    world.removeAllowedId( data.playerId )
                    player.setInstanceId( 0 )

                    break
                }

                return

            case '32760-08.html':
                if ( state.isMemoState( 2 ) ) {
                    state.setConditionWithSound( 5, true )
                    await player.teleportToLocation( EXIT_LOC, true )

                    break
                }

                return

            case '32762-02.html':
            case '32762-03.html':
                if ( state.isMemoState( 1 ) && state.getVariable( variableNames.value ) === 1 ) {
                    break
                }

                return

            case '32762-04.html':
                if ( state.isMemoState( 1 ) && state.getVariable( variableNames.value ) === 1 ) {
                    if ( !QuestHelper.hasQuestItem( player, BLACK_FROZEN_CORE ) ) {
                        await QuestHelper.giveSingleItem( player, BLACK_FROZEN_CORE, 5 )
                    }

                    state.setVariable( variableNames.value, 2 )
                    state.setConditionWithSound( 4, true )

                    break
                }

                return

            case '32781-02.html':
            case '32781-03.html':
                if ( state.isMemoState( 2 ) ) {
                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== RAFFORTY ) {
                    break
                }

                let canProceed: boolean = player.getLevel() >= minimumLevel && player.hasQuestCompleted( 'Q10285_MeetingSirra' )
                return this.getPath( canProceed ? '32020-01.htm' : '32020-04.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case RAFFORTY:
                        if ( state.isMemoState( 1 ) ) {
                            return this.getPath( player.getLevel() >= minimumLevel ? '32020-06.html' : '32020-08.html' )
                        }

                        if ( state.isMemoState( 2 ) ) {
                            return this.getPath( '32020-07.html' )
                        }

                        break

                    case JINIA:
                        if ( !state.isMemoState( 1 ) ) {
                            break
                        }

                        switch ( state.getVariable( variableNames.value ) ) {
                            case 1:
                                return this.getPath( '32760-05.html' )

                            case 2:
                                return this.getPath( '32760-06.html' )

                            default:
                                return this.getPath( '32760-01.html' )
                        }

                        break

                    case SIRRA:
                        if ( !state.isMemoState( 1 ) ) {
                            break
                        }

                        switch ( state.getVariable( variableNames.value ) ) {
                            case 1:
                                return this.getPath( '32762-01.html' )

                            case 2:
                                return this.getPath( '32762-05.html' )
                        }

                        break

                    case JINIA2:
                        await QuestHelper.addExpAndSp( player, 2152200, 181070 )
                        await state.exitQuest( false, true )

                        return this.getPath( '32781-01.html' )
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === RAFFORTY ) {
                    return this.getPath( '32020-05.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}