import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestStateValues } from '../../gameService/models/quest/State'

const GOSTA = 30916
const MINEVIA = 30907
const BLADE_OTIS = 18562
const WEIRD_BUNEI = 18564
const TISSUE_BO = 13834
const TISSUE_WB = 13835
const minimumLevel = 37

export class MutatedKaneusHeine extends ListenerLogic {
    constructor() {
        super( 'Q10278_MutatedKaneusHeine', 'listeners/tracked-10200/MutatedKaneusHeine.ts' )
        this.questId = 10278
        this.questItemIds = [ TISSUE_BO, TISSUE_WB ]
    }

    getQuestStartIds(): Array<number> {
        return [ GOSTA ]
    }

    getTalkIds(): Array<number> {
        return [ GOSTA, MINEVIA ]
    }

    getAttackableKillIds(): Array<number> {
        return [ BLADE_OTIS, WEIRD_BUNEI ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q10278_MutatedKaneusHeine'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30916-03.htm':
                state.startQuest()
                break

            case '30907-03.htm':
                let player = L2World.getPlayer( data.playerId )
                if ( !QuestHelper.hasQuestItems( player, TISSUE_BO, TISSUE_WB ) ) {
                    return
                }

                await QuestHelper.giveAdena( player, 50000, true )
                await state.exitQuest( false, true )

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player: L2PcInstance = this.getRandomPartyMemberPerCondition( L2World.getPlayer( data.playerId ), -1, 1, L2World.getObjectById( data.targetId ) )
        if ( !player ) {
            return
        }

        let itemId: number = data.npcId === BLADE_OTIS ? TISSUE_BO : TISSUE_WB
        await QuestHelper.giveSingleItem( player, itemId, 1 )

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    checkPartyMemberConditions( state: QuestState, conditionValue: number, target: L2Npc ): boolean {
        if ( !state.isStarted() ) {
            return false
        }

        let player = state.getPlayer()
        let itemId: number = target.getId() === BLADE_OTIS ? TISSUE_BO : TISSUE_WB

        return !QuestHelper.hasQuestItem( player, itemId )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== GOSTA ) {
                    break
                }

                return this.getPath( player.getLevel() > minimumLevel ? '30916-01.htm' : '30916-00.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case GOSTA:
                        return this.getPath( QuestHelper.hasQuestItems( player, TISSUE_BO, TISSUE_WB ) ? '30916-05.htm' : '30916-04.htm' )

                    case MINEVIA:
                        return this.getPath( QuestHelper.hasQuestItems( player, TISSUE_BO, TISSUE_WB ) ? '30907-02.htm' : '30907-01.htm' )
                }

                break

            case QuestStateValues.COMPLETED:
                switch ( data.characterNpcId ) {
                    case GOSTA:
                        return this.getPath( '30916-06.htm' )

                    case MINEVIA:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}