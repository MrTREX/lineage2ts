import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestHelper } from '../helpers/QuestHelper'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { InstanceManager } from '../../gameService/instancemanager/InstanceManager'
import { InstanceWorld } from '../../gameService/models/instancezone/InstanceWorld'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const RAFFORTY = 32020
const FREYAS_STEWARD = 32029
const JINIA = 32760
const KEGOR = 32761
const SIRRA = 32762
const JINIA2 = 32781
const minimumLevel = 82
const EXIT_LOC = new Location( 113793, -109342, -845, 0 )
const FREYA_LOC = new Location( 103045, -124361, -2768, 0 )

const variableNames = {
    value: 'vl',
}

export class MeetingSirra extends ListenerLogic {
    constructor() {
        super( 'Q10285_MeetingSirra', 'listeners/tracked-10200/MeetingSirra.ts' )
        this.questId = 10285
    }

    getTalkIds(): Array<number> {
        return [ RAFFORTY, JINIA, KEGOR, SIRRA, JINIA2, FREYAS_STEWARD ]
    }

    getQuestStartIds(): Array<number> {
        return [ RAFFORTY ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32020-02.htm':
                break

            case '32020-03.htm':
                state.startQuest()
                state.setMemoState( 1 )

                break

            case '32760-02.html':
                if ( state.isMemoState( 1 ) && !state.getVariable( variableNames.value ) ) {
                    state.setVariable( variableNames.value, 1 )
                    state.setConditionWithSound( 3, true )

                    break
                }

                return

            case '32760-05.html':
                if ( state.isMemoState( 1 ) && state.getVariable( variableNames.value ) === 2 ) {
                    break
                }

                return

            case '32760-06.html':
                if ( state.isMemoState( 1 ) && state.getVariable( variableNames.value ) === 2 ) {
                    let sirra: L2Npc = QuestHelper.addGenericSpawn( null, SIRRA, -23905, -8790, -5384, 56238, false, 0, false, L2World.getObjectById( data.characterId ).getInstanceId() )
                    BroadcastHelper.broadcastNpcSayStringId( sirra, NpcSayType.NpcAll, NpcStringIds.THERES_NOTHING_YOU_CANT_SAY_I_CANT_LISTEN_TO_YOU_ANYMORE )

                    state.setVariable( variableNames.value, 3 )
                    state.setConditionWithSound( 5, true )

                    break
                }

                return

            case '32760-09.html':
            case '32760-10.html':
            case '32760-11.html':
                if ( state.isMemoState( 1 ) && state.getVariable( variableNames.value ) === 4 ) {
                    break
                }

                return

            case '32760-12.html':
                if ( state.isMemoState( 1 ) && state.getVariable( variableNames.value ) === 4 ) {
                    state.setVariable( variableNames.value, 5 )
                    state.setConditionWithSound( 7, true )

                    break
                }

                return

            case '32760-13.html':
                if ( state.isMemoState( 1 ) && state.getVariable( variableNames.value ) === 5 ) {
                    state.unsetVariable( variableNames.value )
                    state.setMemoState( 2 )

                    let world: InstanceWorld = InstanceManager.getPlayerWorld( data.playerId )
                    world.removeAllowedId( data.playerId )
                    player.setInstanceId( 0 )

                    break
                }

                return

            case '32760-14.html':
                if ( state.isMemoState( 2 ) ) {
                    await player.teleportToLocation( EXIT_LOC, true )
                    break
                }

                return

            case '32761-02.html':
                if ( state.isMemoState( 1 ) && state.getVariable( variableNames.value ) === 1 ) {
                    state.setVariable( variableNames.value, 2 )
                    state.setConditionWithSound( 4, true )

                    break
                }

                return

            case '32762-02.html':
            case '32762-03.html':
            case '32762-04.html':
            case '32762-05.html':
            case '32762-06.html':
            case '32762-07.html':
                if ( state.isMemoState( 1 ) && state.getVariable( variableNames.value ) === 3 ) {
                    break
                }

                return

            case '32762-08.html':
                if ( state.isMemoState( 1 ) && state.getVariable( variableNames.value ) === 3 ) {
                    state.setVariable( variableNames.value, 4 )
                    state.setConditionWithSound( 6, true )

                    await ( L2World.getObjectById( data.characterId ) as L2Npc ).deleteMe()
                    break
                }

                return

            case '32781-02.html':
            case '32781-03.html':
                if ( state.isMemoState( 2 ) ) {
                    break
                }

                return

            case 'TELEPORT':
                if ( player.getLevel() >= minimumLevel ) {
                    await player.teleportToLocation( FREYA_LOC, true )
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q10285_MeetingSirra'
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                let canProceed: boolean = player.getLevel() >= minimumLevel && player.hasQuestCompleted( 'Q10284_AcquisitionOfDivineSword' )
                return this.getPath( canProceed ? '32020-01.htm' : '32020-04.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case RAFFORTY:
                        switch ( state.getMemoState() ) {
                            case 1:
                                return this.getPath( player.getLevel() >= minimumLevel ? '32020-06.html' : '32020-09.html' )

                            case 2:
                                return this.getPath( '32020-07.html' )

                            case 3:
                                await QuestHelper.giveAdena( player, 283425, true )
                                await QuestHelper.addExpAndSp( player, 939075, 83855 )
                                await state.exitQuest( false, true )

                                return this.getPath( '32020-08.html' )
                        }

                        break

                    case JINIA:
                        if ( !state.isMemoState( 1 ) ) {
                            break
                        }

                        switch ( state.getVariable( variableNames.value ) ) {
                            case 1:
                                return this.getPath( '32760-03.html' )

                            case 2:
                                return this.getPath( '32760-04.html' )

                            case 3:
                                return this.getPath( '32760-07.html' )

                            case 4:
                                return this.getPath( '32760-08.html' )

                            case 5:
                                return this.getPath( '32760-15.html' )

                            default:
                                return this.getPath( '32760-01.html' )
                        }

                        break

                    case KEGOR:
                        if ( !state.isMemoState( 1 ) ) {
                            break
                        }

                        switch ( state.getVariable( variableNames.value ) ) {
                            case 1:
                                return this.getPath( '32761-01.html' )

                            case 2:
                                return this.getPath( '32761-03.html' )

                            case 3:
                                return this.getPath( '32761-04.html' )
                        }

                        break

                    case SIRRA:
                        if ( !state.isMemoState( 1 ) ) {
                            break
                        }

                        switch ( state.getVariable( variableNames.value ) ) {
                            case 3:
                                return this.getPath( '32762-01.html' )

                            case 4:
                                return this.getPath( '32762-09.html' )
                        }

                        break

                    case JINIA2:
                        if ( state.isMemoState( 2 ) ) {
                            return this.getPath( '32781-01.html' )
                        }

                        if ( state.isMemoState( 3 ) ) {
                            return this.getPath( '32781-04.html' )
                        }

                        break

                    case FREYAS_STEWARD:
                        if ( state.isMemoState( 2 ) ) {
                            state.setConditionWithSound( 8, true )

                            return this.getPath( '32029-01.html' )
                        }

                        break

                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === RAFFORTY ) {
                    return this.getPath( '32020-05.htm' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}