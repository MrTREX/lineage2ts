import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { SkillHolder } from '../../gameService/models/holders/SkillHolder'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

const LEKON = 32557
const MARK = 13856
const AURA_BIRD_FALCON = new SkillHolder( 5982, 1 )
const AURA_BIRD_OWL = new SkillHolder( 5983, 1 )
const minimumLevel = 75

export class GoodDayToFly extends ListenerLogic {
    constructor() {
        super( 'Q10273_GoodDayToFly', 'listeners/tracked-10200/GoodDayToFly.ts' )
        this.questId = 10273
        this.questItemIds = [
            MARK,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ LEKON ]
    }

    getTalkIds(): Array<number> {
        return [ LEKON ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            22614, // Vulture Rider
            22615 // Vulture Rider
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q10273_GoodDayToFly'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32557-06.htm':
                state.startQuest()
                break

            case '32557-09.html':
                state.setMemoState( 1 )
                await AURA_BIRD_FALCON.getSkill().applyEffects( player, player )
                break

            case '32557-10.html':
                state.setMemoState( 2 )
                await AURA_BIRD_OWL.getSkill().applyEffects( player, player )
                break

            case '32557-13.html':
                switch ( state.getMemoState() ) {
                    case 1:
                        await AURA_BIRD_FALCON.getSkill().applyEffects( player, player )
                        break

                    case 2:
                        await AURA_BIRD_OWL.getSkill().applyEffects( player, player )
                        break
                }

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isStarted() || !state.isCondition( 1 ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        if ( QuestHelper.getQuestItemsCount( player, MARK ) < 5 ) {
            await QuestHelper.rewardSingleQuestItem( player, MARK, 1, data.isChampion )
        }

        if ( QuestHelper.getQuestItemsCount( player, MARK ) >= 5 ) {
            state.setConditionWithSound( 2, true )
            return
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() < minimumLevel ? '32557-00.html' : '32557-01.htm' )

            case QuestStateValues.STARTED:
                if ( QuestHelper.getQuestItemsCount( player, MARK ) >= 5 ) {
                    switch ( state.getMemoState() ) {
                        case 1:
                            await QuestHelper.rewardSingleItem( player, 13553, 1 )
                            break

                        case 2:
                            await QuestHelper.rewardSingleItem( player, 13554, 1 )
                            break
                    }

                    await QuestHelper.rewardSingleItem( player, 13857, 1 )
                    await QuestHelper.addExpAndSp( player, 25160, 2525 )
                    await state.exitQuest( false, true )

                    return this.getPath( '32557-14.html' )
                }

                if ( !state.getMemoState() ) {
                    return this.getPath( '32557-07.html' )
                }

                return this.getPath( '32557-11.html' )

            case QuestStateValues.COMPLETED:
                return this.getPath( '32557-0a.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}