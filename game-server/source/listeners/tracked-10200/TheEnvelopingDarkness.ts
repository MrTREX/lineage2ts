import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const ORBYU = 32560
const EL = 32556
const MEDIBAL_CORPSE = 32528
const MEDIBAL_DOCUMENT = 13852
const minimumLevel = 75

export class TheEnvelopingDarkness extends ListenerLogic {
    constructor() {
        super( 'Q10271_TheEnvelopingDarkness', 'listeners/tracked-10200/TheEnvelopingDarkness.ts' )
        this.questId = 10271
        this.questItemIds = [ MEDIBAL_DOCUMENT ]
    }

    getQuestStartIds(): Array<number> {
        return [ ORBYU ]
    }

    getTalkIds(): Array<number> {
        return [ ORBYU, EL, MEDIBAL_CORPSE ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q10271_TheEnvelopingDarkness'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32560-05.html':
                state.startQuest()
                break

            case '32556-06.html':
                state.setConditionWithSound( 2, true )
                break

            case '32556-09.html':
                if ( QuestHelper.hasQuestItem( player, MEDIBAL_DOCUMENT ) ) {
                    await QuestHelper.takeSingleItem( player, MEDIBAL_DOCUMENT, -1 )
                    state.setConditionWithSound( 4, true )
                }

                break
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== ORBYU ) {
                    break
                }

                let canProceed : boolean = player.getLevel() >= minimumLevel && player.hasQuestCompleted( 'Q10269_ToTheSeedOfDestruction' )
                return this.getPath( canProceed ? '32560-01.htm' : '32560-02.html' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case ORBYU:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '32560-05.html' )

                            case 2:
                                return this.getPath( '32560-06.html' )

                            case 3:
                                return this.getPath( '32560-07.html' )

                            case 4:
                                await QuestHelper.giveAdena( player, 62516, true )
                                await QuestHelper.addExpAndSp( player, 377403, 37867 )
                                await state.exitQuest( false, true )

                                return this.getPath( '32560-08.html' )
                        }

                        break

                    case EL:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '32556-01.html' )

                            case 2:
                                return this.getPath( '32556-07.html' )

                            case 3:
                                return this.getPath( '32556-08.html' )

                            case 4:
                                return this.getPath( '32556-09.html' )
                        }

                        break

                    case MEDIBAL_CORPSE:
                        switch ( state.getCondition() ) {
                            case 2:
                                state.setConditionWithSound( 3, true )
                                await QuestHelper.giveSingleItem( player, MEDIBAL_DOCUMENT, 1 )

                                return this.getPath( '32528-01.html' )

                            case 3:
                            case 4:
                                return this.getPath( '32528-03.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                switch ( data.characterNpcId ) {
                    case ORBYU:
                        return this.getPath( '32560-03.html' )

                    case EL:
                        return this.getPath( '32556-02.html' )

                    case MEDIBAL_CORPSE:
                        return this.getPath( '32528-02.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}