import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { ItemTypes } from '../../gameService/values/InventoryValues'
import { QuestStateValues } from '../../gameService/models/quest/State'

const ORBYU = 32560
const ARTIUS = 32559
const GINBY = 32566
const LELRIKIA = 32567
const LEKON = 32557
const FRAGMENT_POWDER = 13853
const LIGHT_FRAGMENT_POWDER = 13854
const LIGHT_FRAGMENT = 13855
const minimumLevel = 75

const variableNames = {
    isWaiting: 'iw',
}

export class LightFragment extends ListenerLogic {
    constructor() {
        super( 'Q10272_LightFragment', 'listeners/tracked-10200/LightFragment.ts' )
        this.questId = 10272
        this.questItemIds = [ FRAGMENT_POWDER, LIGHT_FRAGMENT_POWDER ]
    }

    getQuestStartIds(): Array<number> {
        return [ ORBYU ]
    }

    getTalkIds(): Array<number> {
        return [
            ORBYU,
            ARTIUS,
            GINBY,
            LELRIKIA,
            LEKON,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            22536, // Royal Guard Captain
            22537, // Dragon Steed Troop Grand Magician
            22538, // Dragon Steed Troop Commander
            22539, // Dragon Steed Troops No 1 Battalion Commander
            22540, // White Dragon Leader
            22541, // Dragon Steed Troop Infantry
            22542, // Dragon Steed Troop Magic Leader
            22543, // Dragon Steed Troop Magician
            22544, // Dragon Steed Troop Magic Soldier
            22547, // Dragon Steed Troop Healer
            22550, // Savage Warrior
            22551, // Priest of Darkness
            22552, // Mutation Drake
            22596, // White Dragon Leader
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q10272_LightFragment'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32560-06.html':
                state.startQuest()
                break

            case '32559-03.html':
                state.setConditionWithSound( 2, true )
                break

            case '32559-07.html':
                state.setConditionWithSound( 3, true )
                break

            case 'pay':
                if ( QuestHelper.getQuestItemsCount( player, ItemTypes.Adena ) >= 10000 ) {
                    await QuestHelper.takeSingleItem( player, ItemTypes.Adena, 10000 )

                    return this.getPath( '32566-05.html' )
                }

                return this.getPath( '32566-04a.html' )

            case '32567-04.html':
                state.setConditionWithSound( 4, true )
                break

            case '32559-12.html':
                state.setConditionWithSound( 5, true )
                break

            case '32557-03.html':
                if ( QuestHelper.getQuestItemsCount( player, LIGHT_FRAGMENT_POWDER ) >= 100 ) {
                    await QuestHelper.takeSingleItem( player, LIGHT_FRAGMENT_POWDER, 100 )
                    state.setVariable( variableNames.isWaiting, true )

                    break
                }

                return this.getPath( '32557-04.html' )
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isStarted() || !state.isCondition( 5 ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        if ( QuestHelper.getQuestItemsCount( player, FRAGMENT_POWDER ) >= 100 ) {
            return
        }

        let amount: number = Math.random() < QuestHelper.getAdjustedAmount( FRAGMENT_POWDER, 0.60, data.isChampion ) ? 2 : 1
        await QuestHelper.rewardSingleQuestItem( player, FRAGMENT_POWDER, amount, data.isChampion )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== ORBYU ) {
                    break
                }

                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '32560-03.html' )
                }

                return this.getPath( player.hasQuestCompleted( 'Q10271_TheEnvelopingDarkness' ) ? '32560-01.htm' : '32560-02.html' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case ORBYU:
                        return this.getPath( '32560-06.html' )

                    case ARTIUS:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '32559-01.html' )

                            case 2:
                                return this.getPath( '32559-04.html' )

                            case 3:
                                return this.getPath( '32559-08.html' )

                            case 4:
                                return this.getPath( '32559-10.html' )

                            case 5:
                                let powderAmount: number = QuestHelper.getQuestItemsCount( player, FRAGMENT_POWDER )
                                if ( powderAmount >= 100 ) {
                                    state.setConditionWithSound( 6, true )

                                    return this.getPath( '32559-15.html' )
                                }

                                return this.getPath( powderAmount > 0 ? '32559-14.html' : '32559-13.html' )

                            case 6:
                                if ( QuestHelper.getQuestItemsCount( player, LIGHT_FRAGMENT_POWDER ) < 100 ) {
                                    return this.getPath( '32559-16.html' )
                                }

                                state.setConditionWithSound( 7, true )
                                return this.getPath( '32559-17.html' )

                            case 8:
                                await QuestHelper.giveAdena( player, 556980, true )
                                await QuestHelper.addExpAndSp( player, 1009016, 91363 )
                                await state.exitQuest( false, true )

                                return this.getPath( '32559-18.html' )
                        }

                        break

                    case GINBY:
                        switch ( state.getCondition() ) {
                            case 1:
                            case 2:
                                return this.getPath( '32566-02.html' )

                            case 3:
                                return this.getPath( '32566-01.html' )

                            case 4:
                                return this.getPath( '32566-09.html' )

                            case 5:
                                return this.getPath( '32566-10.html' )

                            case 6:
                                return this.getPath( '32566-10.html' )
                        }

                        break

                    case LELRIKIA:
                        switch ( state.getCondition() ) {
                            case 3:
                                return this.getPath( '32567-01.html' )

                            case 4:
                                return this.getPath( '32567-05.html' )
                        }

                        break

                    case LEKON:
                        switch ( state.getCondition() ) {
                            case 7:
                                if ( state.getVariable( variableNames.isWaiting ) ) {
                                    state.unsetVariable( variableNames.isWaiting )
                                    state.setConditionWithSound( 8, true )

                                    await QuestHelper.giveSingleItem( player, LIGHT_FRAGMENT, 1 )

                                    return this.getPath( '32557-05.html' )
                                }

                                return this.getPath( '32557-01.html' )

                            case 8:
                                return this.getPath( '32557-06.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                switch ( data.characterNpcId ) {
                    case ORBYU:
                        return this.getPath( '32560-04.html' )

                    case ARTIUS:
                        return this.getPath( '32559-19.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}