import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestStateValues } from '../../gameService/models/quest/State'

const VISHOTSKY = 31981
const ATRAXIA = 31972
const VENOMOUS_STORACE = 18571
const KEL_BILETTE = 18573
const TISSUE_VS = 13838
const TISSUE_KB = 13839
const minimumLevel = 57

export class MutatedKaneusSchuttgart extends ListenerLogic {
    constructor() {
        super( 'Q10280_MutatedKaneusSchuttgart', 'listeners/tracked-10200/MutatedKaneusSchuttgart.ts' )
        this.questId = 10280
        this.questItemIds = [ TISSUE_VS, TISSUE_KB ]
    }

    getQuestStartIds(): Array<number> {
        return [ VISHOTSKY ]
    }

    getTalkIds(): Array<number> {
        return [ VISHOTSKY, ATRAXIA ]
    }

    getAttackableKillIds(): Array<number> {
        return [ VENOMOUS_STORACE, KEL_BILETTE ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q10280_MutatedKaneusSchuttgart'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '31981-03.htm':
                state.startQuest()
                break

            case '31972-03.htm':
                let player = L2World.getPlayer( data.playerId )
                if ( !QuestHelper.hasQuestItems( player, TISSUE_VS, TISSUE_KB ) ) {
                    return
                }

                await QuestHelper.giveAdena( player, 210000, true )
                await state.exitQuest( false, true )

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player: L2PcInstance = this.getRandomPartyMemberPerCondition( L2World.getPlayer( data.playerId ), -1, 1, L2World.getObjectById( data.targetId ) )
        if ( !player ) {
            return
        }

        let itemId: number = data.npcId === VENOMOUS_STORACE ? TISSUE_VS : TISSUE_KB
        await QuestHelper.giveSingleItem( player, itemId, 1 )

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    checkPartyMemberConditions( state: QuestState, conditionValue: number, target: L2Npc ): boolean {
        if ( !state.isStarted() ) {
            return false
        }

        let player = state.getPlayer()
        let itemId: number = target.getId() === VENOMOUS_STORACE ? TISSUE_VS : TISSUE_KB

        return !QuestHelper.hasQuestItem( player, itemId )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== VISHOTSKY ) {
                    break
                }

                return this.getPath( player.getLevel() > minimumLevel ? '31981-01.htm' : '31981-00.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case VISHOTSKY:
                        return this.getPath( QuestHelper.hasQuestItems( player, TISSUE_VS, TISSUE_KB ) ? '31981-05.htm' : '31981-04.htm' )

                    case ATRAXIA:
                        return this.getPath( QuestHelper.hasQuestItems( player, TISSUE_VS, TISSUE_KB ) ? '31972-02.htm' : '31972-01.htm' )
                }

                break

            case QuestStateValues.COMPLETED:
                switch ( data.characterNpcId ) {
                    case VISHOTSKY:
                        return this.getPath( '31981-06.htm' )

                    case ATRAXIA:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}