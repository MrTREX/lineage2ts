import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2Party } from '../../gameService/models/L2Party'
import { PlayerGroupCache } from '../../gameService/cache/PlayerGroupCache'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'
import aigle from 'aigle'

const GREYMORE = 32757
const MARK_OF_SPLENDOR = 15527
const MARK_OF_DARKNESS = 15528
const ANAYS = 25701
const minimumLevel = 82

export class FadeToBlack extends ListenerLogic {
    constructor() {
        super( 'Q10289_FadeToBlack', 'listeners/tracked-10200/FadeToBlack.ts' )
        this.questId = 10289
        this.questItemIds = [ MARK_OF_SPLENDOR, MARK_OF_DARKNESS ]
    }

    getQuestStartIds(): Array<number> {
        return [ GREYMORE ]
    }

    getTalkIds(): Array<number> {
        return [ GREYMORE ]
    }

    getAttackableKillIds(): Array<number> {
        return [ ANAYS ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q10289_FadeToBlack'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        if ( data.eventName.length === 2 ) {
            if ( !state.isCondition( 3 ) || !QuestHelper.hasQuestItem( player, MARK_OF_SPLENDOR ) ) {
                return
            }

            await this.rewardPlayer( data.eventName, player )

            let marksOfDarkness = QuestHelper.getQuestItemsCount( player, MARK_OF_DARKNESS )
            if ( marksOfDarkness > 0 ) {
                await QuestHelper.addExpAndSp( player, 55983 * marksOfDarkness, 136500 * marksOfDarkness )
            }

            await state.exitQuest( false, true )
            return this.getPath( '32757-09.html' )
        }

        switch ( data.eventName ) {
            case '32757-02.htm':
                break

            case '32757-03.htm':
                state.startQuest()
                break

            case '32757-06.html':
                if ( state.isCondition( 2 ) && QuestHelper.hasQuestItem( player, MARK_OF_DARKNESS ) ) {
                    return this.getPath( '32757-07.html' )
                }

                if ( state.isCondition( 3 ) && QuestHelper.hasQuestItem( player, MARK_OF_SPLENDOR ) ) {
                    return this.getPath( '32757-08.html' )
                }

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async rewardPlayer( name: string, player: L2PcInstance ): Promise<void> {
        switch ( name ) {
            case '11':
                await QuestHelper.rewardSingleItem( player, 15775, 1 )
                await QuestHelper.giveAdena( player, 420920, true )
                return

            case '12':
                await QuestHelper.rewardSingleItem( player, 15776, 1 )
                await QuestHelper.giveAdena( player, 420920, true )
                return

            case '13':
                await QuestHelper.rewardSingleItem( player, 15777, 1 )
                await QuestHelper.giveAdena( player, 420920, true )
                return

            case '14':
                await QuestHelper.rewardSingleItem( player, 15778, 1 )
                return

            case '15':
                await QuestHelper.rewardSingleItem( player, 15779, 1 )
                await QuestHelper.giveAdena( player, 168360, true )
                return

            case '16':
                await QuestHelper.rewardSingleItem( player, 15780, 1 )
                await QuestHelper.giveAdena( player, 168360, true )
                return

            case '17':
                await QuestHelper.rewardSingleItem( player, 15781, 1 )
                await QuestHelper.giveAdena( player, 252540, true )
                return

            case '18':
                await QuestHelper.rewardSingleItem( player, 15782, 1 )
                await QuestHelper.giveAdena( player, 357780, true )
                return

            case '19':
                await QuestHelper.rewardSingleItem( player, 15783, 1 )
                await QuestHelper.giveAdena( player, 357780, true )
                return

            case '20':
                await QuestHelper.rewardSingleItem( player, 15784, 1 )
                await QuestHelper.giveAdena( player, 505100, true )
                return

            case '21':
                await QuestHelper.rewardSingleItem( player, 15785, 1 )
                await QuestHelper.giveAdena( player, 505100, true )
                return

            case '22':
                await QuestHelper.rewardSingleItem( player, 15786, 1 )
                await QuestHelper.giveAdena( player, 505100, true )
                return

            case '23':
                await QuestHelper.rewardSingleItem( player, 15787, 1 )
                await QuestHelper.giveAdena( player, 505100, true )
                return

            case '24':
                await QuestHelper.rewardSingleItem( player, 15787, 1 )
                await QuestHelper.giveAdena( player, 505100, true )
                return

            case '25':
                await QuestHelper.rewardSingleItem( player, 15789, 1 )
                await QuestHelper.giveAdena( player, 505100, true )
                return

            case '26':
                await QuestHelper.rewardSingleItem( player, 15790, 1 )
                await QuestHelper.giveAdena( player, 496680, true )
                return

            case '27':
                await QuestHelper.rewardSingleItem( player, 15791, 1 )
                await QuestHelper.giveAdena( player, 496680, true )
                return

            case '28':
                await QuestHelper.rewardSingleItem( player, 15792, 1 )
                await QuestHelper.giveAdena( player, 563860, true )
                return

            case '29':
                await QuestHelper.rewardSingleItem( player, 15793, 1 )
                await QuestHelper.giveAdena( player, 509040, true )
                return

            case '30':
                await QuestHelper.rewardSingleItem( player, 15794, 1 )
                await QuestHelper.giveAdena( player, 454240, true )
                return

        }
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let party: L2Party = PlayerGroupCache.getParty( data.playerId )
        let questName = this.getName()

        let playerIds: Array<number> = _.filter( party ? party.getMembers() : [ data.playerId ], ( playerId: number ): boolean => {
            let state: QuestState = QuestStateCache.getQuestState( playerId, questName, false )

            return state && state.getCondition() < 3
        } )

        let luckyPlayerId: number = _.sample( playerIds )
        await aigle.resolve( playerIds ).each( async ( playerId: number ) => {
            let player = L2World.getPlayer( playerId )
            await QuestHelper.giveSingleItem( player, luckyPlayerId === playerId ? MARK_OF_SPLENDOR : MARK_OF_DARKNESS, 1 )

            let state: QuestState = QuestStateCache.getQuestState( playerId, questName, false )
            state.setConditionWithSound( luckyPlayerId === playerId ? 3 : 2, true )
        } )

        return
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                let companionState: QuestState = QuestStateCache.getQuestState( data.playerId, 'Q10288_SecretMission', false )
                if ( player.getLevel() < minimumLevel
                        || !companionState
                        || !companionState.isCompleted() ) {
                    return this.getPath( '32757-00.htm' )
                }

                return this.getPath( '32757-01.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '32757-04.html' )

                    case 2:
                    case 3:
                        return this.getPath( '32757-05.html' )
                }

                break

            case QuestStateValues.COMPLETED:
                return this.getPath( '32757-10.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}