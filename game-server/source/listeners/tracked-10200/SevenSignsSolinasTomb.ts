import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const ELCADIA = 32787
const ERISS_EVIL_THOUGHTS = 32792
const SOLINAS_EVIL_THOUGHTS = 32793
const SOLINA = 32794
const ERIS = 32795
const ANAIS = 32796
const JUDE_VAN_ETINA = 32797
const TELEPORT_CONTROL_DEVICE_1 = 32837
const POWERFUL_DEVICE_1 = 32838
const POWERFUL_DEVICE_2 = 32839
const POWERFUL_DEVICE_3 = 32840
const POWERFUL_DEVICE_4 = 32841
const TELEPORT_CONTROL_DEVICE_2 = 32842
const TOMB_OF_THE_SAINTESS = 32843
const TELEPORT_CONTROL_DEVICE_3 = 32844
const ALTAR_OF_HALLOWS_1 = 32857
const ALTAR_OF_HALLOWS_2 = 32858
const ALTAR_OF_HALLOWS_3 = 32859
const ALTAR_OF_HALLOWS_4 = 32860
const SCROLL_OF_ABSTINENCE = 17228
const SHIELD_OF_SACRIFICE = 17229
const SWORD_OF_HOLY_SPIRIT = 17230
const STAFF_OF_BLESSING = 17231
const minimumLevel = 81

/*
    TODO : according to https://www.youtube.com/watch?v=g6b3fZyYGQM , Elcadia talks lines depending which zone entered
 */

export class SevenSignsSolinasTomb extends ListenerLogic {
    constructor() {
        super( 'Q10295_SevenSignsSolinasTomb', 'listeners/tracked-10200/SevenSignsSolinasTomb.ts' )
        this.questId = 10295
        this.questItemIds = [
            SCROLL_OF_ABSTINENCE,
            SHIELD_OF_SACRIFICE,
            SWORD_OF_HOLY_SPIRIT,
            STAFF_OF_BLESSING,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ ERISS_EVIL_THOUGHTS ]
    }

    getTalkIds(): Array<number> {
        return [
            ERISS_EVIL_THOUGHTS,
            SOLINAS_EVIL_THOUGHTS,
            SOLINA,
            ERIS,
            ANAIS,
            JUDE_VAN_ETINA,
            TELEPORT_CONTROL_DEVICE_1,
            POWERFUL_DEVICE_1,
            POWERFUL_DEVICE_2,
            POWERFUL_DEVICE_3,
            POWERFUL_DEVICE_4,
            TELEPORT_CONTROL_DEVICE_2,
            TOMB_OF_THE_SAINTESS,
            TELEPORT_CONTROL_DEVICE_3,
            ALTAR_OF_HALLOWS_1,
            ALTAR_OF_HALLOWS_2,
            ALTAR_OF_HALLOWS_3,
            ALTAR_OF_HALLOWS_4,
            ELCADIA,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q10295_SevenSignsSolinasTomb'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32792-02.htm':
            case '32792-04.htm':
            case '32792-05.htm':
            case '32792-06.htm':
            case '32793-06.html':
                break

            case '32792-03.htm':
                state.startQuest()
                state.setMemoState( 1 )
                break

            case '32793-02.html':
            case '32793-03.html':
                if ( state.isMemoState( 3 ) ) {
                    break
                }

                return

            case '32793-04.html':
                if ( state.isMemoState( 3 ) ) {
                    state.setMemoState( 4 )
                    state.setConditionWithSound( 2, true )

                    break
                }

                return

            case '32793-05.html':
            case '32794-02.html':
                if ( state.isMemoState( 4 ) ) {
                    state.setMemoState( 5 )
                    break
                }

                return

            case '32793-07.html':
                if ( state.isMemoState( 5 ) ) {
                    break
                }

                return

            case '32793-08.html':
                if ( state.isMemoState( 5 ) ) {
                    state.setMemoState( 6 )
                    state.setConditionWithSound( 3, true )

                    break
                }

                return

            case '32837-02.html':
                if ( state.getMemoState() > 1 ) {
                    await QuestHelper.takeMultipleItems( player, -1, SCROLL_OF_ABSTINENCE, SHIELD_OF_SACRIFICE, SWORD_OF_HOLY_SPIRIT, STAFF_OF_BLESSING )
                    break
                }

                return

            case '32838-02.html':
                if ( state.isMemoState( 1 ) ) {
                    if ( QuestHelper.hasQuestItem( player, SCROLL_OF_ABSTINENCE ) ) {
                        break
                    }

                    return this.getPath( '32838-03.html' )
                }

                return

            case '32839-02.html':
                if ( state.isMemoState( 1 ) ) {
                    if ( QuestHelper.hasQuestItem( player, SHIELD_OF_SACRIFICE ) ) {
                        break
                    }

                    return this.getPath( '32839-03.html' )
                }

                return

            case '32840-02.html':
                if ( state.isMemoState( 1 ) ) {
                    if ( QuestHelper.hasQuestItem( player, SWORD_OF_HOLY_SPIRIT ) ) {
                        break
                    }

                    return this.getPath( '32840-03.html' )
                }

                return

            case '32841-02.html':
                if ( state.isMemoState( 1 ) ) {
                    if ( QuestHelper.hasQuestItem( player, STAFF_OF_BLESSING ) ) {
                        break
                    }

                    return this.getPath( '32841-03.html' )
                }

                return

            case '32857-02.html':
                if ( state.isMemoState( 1 ) ) {
                    if ( QuestHelper.hasQuestItem( player, STAFF_OF_BLESSING ) ) {
                        break
                    }

                    await QuestHelper.giveSingleItem( player, STAFF_OF_BLESSING, 1 )

                    return this.getPath( '32857-03.html' )
                }

                return

            case '32858-02.html':
                if ( state.isMemoState( 1 ) ) {
                    if ( QuestHelper.hasQuestItem( player, SWORD_OF_HOLY_SPIRIT ) ) {
                        break
                    }

                    await QuestHelper.giveSingleItem( player, SWORD_OF_HOLY_SPIRIT, 1 )

                    return this.getPath( '32858-03.html' )
                }

                return

            case '32859-02.html':
                if ( state.isMemoState( 1 ) ) {
                    if ( QuestHelper.hasQuestItem( player, SCROLL_OF_ABSTINENCE ) ) {
                        break
                    }

                    await QuestHelper.giveSingleItem( player, SCROLL_OF_ABSTINENCE, 1 )

                    return this.getPath( '32859-03.html' )
                }

                return

            case '32860-02.html':
                if ( state.isMemoState( 1 ) ) {
                    if ( QuestHelper.hasQuestItem( player, SHIELD_OF_SACRIFICE ) ) {
                        break
                    }

                    await QuestHelper.giveSingleItem( player, SHIELD_OF_SACRIFICE, 1 )

                    return this.getPath( '32860-03.html' )
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                switch ( data.characterNpcId ) {
                    case ELCADIA:
                        return this.getPath( '32787-01.html' )

                    case ERISS_EVIL_THOUGHTS:
                        if ( player.getLevel() >= minimumLevel
                                && player.hasQuestCompleted( 'Q10294_SevenSignsToTheMonasteryOfSilence' ) ) {
                            return this.getPath( '32792-01.htm' )
                        }

                        break
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case ERISS_EVIL_THOUGHTS:
                        switch ( state.getMemoState() ) {
                            case 1:
                                return this.getPath( '32792-12.html' )

                            case 2:
                                return this.getPath( '32792-08.html' )

                            case 3:
                            case 4:
                            case 5:
                                return this.getPath( '32792-09.html' )

                            case 6:
                                if ( player.isSubClassActive() ) {
                                    return this.getPath( '32792-10.html' )
                                }

                                await QuestHelper.addExpAndSp( player, 125000000, 12500000 )
                                await state.exitQuest( false, true )

                                return this.getPath( '32792-11.html' )
                        }

                        break

                    case SOLINAS_EVIL_THOUGHTS:
                        switch ( state.getMemoState() ) {
                            case 3:
                                return this.getPath( '32793-01.html' )

                            case 4:
                                return this.getPath( '32793-09.html' )

                            case 5:
                                return this.getPath( '32793-10.html' )

                            case 6:
                                return this.getPath( '32793-11.html' )
                        }

                        break

                    case SOLINA:
                        switch ( state.getMemoState() ) {
                            case 4:
                                return this.getPath( '32794-01.html' )

                            case 5:
                                return this.getPath( '32794-03.html' )
                        }

                        break

                    case ERIS:
                        switch ( state.getMemoState() ) {
                            case 4:
                                return this.getPath( '32795-01.html' )

                            case 5:
                                return this.getPath( '32795-02.html' )
                        }

                        break

                    case ANAIS:
                        switch ( state.getMemoState() ) {
                            case 4:
                                return this.getPath( '32796-01.html' )

                            case 5:
                                return this.getPath( '32796-02.html' )
                        }

                        break

                    case JUDE_VAN_ETINA:
                        switch ( state.getMemoState() ) {
                            case 4:
                                return this.getPath( '32797-01.html' )

                            case 5:
                                return this.getPath( '32797-02.html' )
                        }

                        break

                    case TELEPORT_CONTROL_DEVICE_1:
                        if ( state.isMemoState( 1 ) ) {
                            return this.getPath( '32837-03.html' )
                        }

                        await QuestHelper.takeMultipleItems( player, -1, SCROLL_OF_ABSTINENCE, SHIELD_OF_SACRIFICE, SWORD_OF_HOLY_SPIRIT, STAFF_OF_BLESSING )

                        return this.getPath( '32837-01.html' )

                    case POWERFUL_DEVICE_1:
                        if ( state.isMemoState( 1 ) ) {
                            return this.getPath( '32838-01.html' )
                        }

                        break

                    case POWERFUL_DEVICE_2:
                        if ( state.isMemoState( 1 ) ) {
                            return this.getPath( '32839-01.html' )
                        }

                        break

                    case POWERFUL_DEVICE_3:
                        if ( state.isMemoState( 1 ) ) {
                            return this.getPath( '32840-01.html' )
                        }

                        break

                    case POWERFUL_DEVICE_4:
                        if ( state.isMemoState( 1 ) ) {
                            return this.getPath( '32841-01.html' )
                        }

                        break

                    case TELEPORT_CONTROL_DEVICE_2:
                        if ( state.getMemoState() > 2 ) {
                            return this.getPath( '32842-01.html' )
                        }

                        break

                    case TOMB_OF_THE_SAINTESS:
                        if ( state.isMemoState( 2 ) ) {
                            return this.getPath( '32843-01.html' )
                        }

                        if ( state.getMemoState() > 2 ) {
                            return this.getPath( '32843-02.html' )
                        }

                        break

                    case TELEPORT_CONTROL_DEVICE_3:
                        if ( state.getMemoState() > 2 ) {
                            return this.getPath( '32844-01.html' )
                        }

                        break

                    case ALTAR_OF_HALLOWS_1:
                        if ( state.isMemoState( 1 ) ) {
                            return this.getPath( '32857-01.html' )
                        }

                        break

                    case ALTAR_OF_HALLOWS_2:
                        if ( state.isMemoState( 1 ) ) {
                            return this.getPath( '32858-01.html' )
                        }

                        break

                    case ALTAR_OF_HALLOWS_3:
                        if ( state.isMemoState( 1 ) ) {
                            return this.getPath( '32859-01.html' )
                        }

                        break

                    case ALTAR_OF_HALLOWS_4:
                        if ( state.isMemoState( 1 ) ) {
                            return this.getPath( '32860-01.html' )
                        }

                        break

                    case ELCADIA:
                        switch ( state.getMemoState() ) {
                            case 1:
                                return this.getPath( '32787-02.html' )

                            case 2:
                                return this.getPath( '32787-03.html' )

                            case 3:
                                return this.getPath( '32787-04.html' )

                            case 4:
                                return this.getPath( '32787-05.html' )

                            case 5:
                                return this.getPath( '32787-06.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === ERISS_EVIL_THOUGHTS ) {
                    return this.getPath( '32792-07.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}