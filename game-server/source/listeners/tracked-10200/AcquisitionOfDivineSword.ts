import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { InstanceManager } from '../../gameService/instancemanager/InstanceManager'
import { InstanceWorld } from '../../gameService/models/instancezone/InstanceWorld'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { QuestHelper } from '../helpers/QuestHelper'

const RAFFORTY = 32020
const KRUN = 32653
const TARUN = 32654
const JINIA = 32760
const minimumLevel = 82
const COLD_RESISTANCE_POTION = 15514
const exitLocation = new Location( 113793, -109342, -845, 0 )

const variableNames = {
    one: 'one',
    two: 'two',
    three: 'three',
}

export class AcquisitionOfDivineSword extends ListenerLogic {
    constructor() {
        super( 'Q10284_AcquisitionOfDivineSword', 'listeners/tracked-10200/AcquisitionOfDivineSword.ts' )
        this.questId = 10284
        this.questItemIds = [ COLD_RESISTANCE_POTION ]
    }

    getQuestStartIds(): Array<number> {
        return [ RAFFORTY ]
    }

    getTalkIds(): Array<number> {
        return [ RAFFORTY, JINIA, TARUN, KRUN ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q10284_AcquisitionOfDivineSword'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32020-02.html':
                state.startQuest()
                state.setMemoState( 1 )

                break

            case '32020-03.html':
            case '32760-02a.html':
            case '32760-02b.html':
            case '32760-03a.html':
            case '32760-03b.html':
            case '32760-04a.html':
            case '32760-04b.html':
                if ( state.isMemoState( 1 ) ) {
                    break
                }

                return

            case '32760-02c.html':
                if ( state.isMemoState( 1 ) ) {
                    state.setVariable( variableNames.one, true )
                    break
                }

                return

            case 'another_story':
                if ( state.isMemoState( 1 ) ) {
                    let optionOne: boolean = state.getVariable( variableNames.one )
                    let optionTwo: boolean = state.getVariable( variableNames.two )
                    let optionThree: boolean = state.getVariable( variableNames.three )

                    if ( optionOne && !optionTwo && !optionThree ) {
                        return this.getPath( '32760-05a.html' )
                    }

                    if ( !optionOne && optionTwo && !optionThree ) {
                        return this.getPath( '32760-05b.html' )
                    }

                    if ( !optionOne && !optionTwo && optionThree ) {
                        return this.getPath( '32760-05c.html' )
                    }

                    if ( !optionOne && optionTwo && optionThree ) {
                        return this.getPath( '32760-05d.html' )
                    }

                    if ( optionOne && !optionTwo && optionThree ) {
                        return this.getPath( '32760-05e.html' )
                    }

                    if ( optionOne && optionTwo && !optionThree ) {
                        return this.getPath( '32760-05f.html' )
                    }

                    if ( optionOne && optionTwo && optionThree ) {
                        return this.getPath( '32760-05g.html' )
                    }
                }

                return

            case '32760-03c.html':
                if ( state.isMemoState( 1 ) ) {
                    state.setVariable( variableNames.two, true )

                    break
                }

                return

            case '32760-04c.html':
                if ( state.isMemoState( 1 ) ) {
                    state.setVariable( variableNames.three, true )

                    break
                }

                return

            case '32760-06.html':
                if ( state.isMemoState( 1 )
                        && state.getVariable( variableNames.one )
                        && state.getVariable( variableNames.two )
                        && state.getVariable( variableNames.three ) ) {
                    break
                }

                return

            case '32760-07.html':
                if ( state.isMemoState( 1 )
                        && state.getVariable( variableNames.one )
                        && state.getVariable( variableNames.two )
                        && state.getVariable( variableNames.three ) ) {
                    state.unsetVariables( variableNames.one, variableNames.two, variableNames.three )

                    state.setConditionWithSound( 3, true )
                    state.setMemoState( 2 )

                    let world: InstanceWorld = InstanceManager.getPlayerWorld( data.playerId )
                    world.removeAllowedId( data.playerId )

                    player.setInstanceId( 0 )

                    break
                }

                return

            case 'exit_instance':
                if ( state.isMemoState( 2 ) ) {
                    await player.teleportToLocation( exitLocation, true )
                }
                return

            case '32654-02.html':
            case '32654-03.html':
            case '32653-02.html':
            case '32653-03.html':
                if ( state.isMemoState( 2 ) ) {
                    break
                }
                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                let canProceed: boolean = player.getLevel() >= minimumLevel && player.hasQuestCompleted( 'Q10283_RequestOfIceMerchant' )
                return this.getPath( canProceed ? '32020-01.htm' : '32020-04.html' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case RAFFORTY:
                        switch ( state.getMemoState() ) {
                            case 1:
                                return this.getPath( player.getLevel() >= minimumLevel ? '32020-06.html' : '32020-08.html' )

                            case 2:
                                return this.getPath( '32020-07.html' )
                        }

                        break

                    case JINIA:
                        if ( !state.isMemoState( 1 ) ) {
                            break
                        }

                        let valueOne: boolean = state.getVariable( variableNames.one )
                        let valueTwo: boolean = state.getVariable( variableNames.two )
                        let valueThree: boolean = state.getVariable( variableNames.three )

                        if ( !valueOne && !valueTwo && !valueThree ) {
                            return this.getPath( '32760-01.html' )
                        }

                        if ( valueOne && !valueTwo && !valueThree ) {
                            return this.getPath( '32760-01a.html' )
                        }

                        if ( !valueOne && valueTwo && !valueThree ) {
                            return this.getPath( '32760-01b.html' )
                        }

                        if ( !valueOne && !valueTwo && valueThree ) {
                            return this.getPath( '32760-01c.html' )
                        }

                        if ( !valueOne && valueTwo && valueThree ) {
                            return this.getPath( '32760-01d.html' )
                        }

                        if ( valueOne && !valueTwo && valueThree ) {
                            return this.getPath( '32760-01e.html' )
                        }

                        if ( valueOne && valueTwo && !valueThree ) {
                            return this.getPath( '32760-01f.html' )
                        }

                        if ( valueOne && valueTwo && valueThree ) {
                            return this.getPath( '32760-01g.html' )
                        }

                        break

                    case TARUN:
                        switch ( state.getMemoState() ) {
                            case 2:
                                return this.getPath( player.getLevel() >= minimumLevel ? '32654-01.html' : '32654-05.html' )

                            case 3:
                                await QuestHelper.giveAdena( player, 296425, true )
                                await QuestHelper.addExpAndSp( player, 921805, 82230 )
                                await state.exitQuest( false, true )

                                return this.getPath( '32654-04.html' )
                        }

                        break

                    case KRUN:
                        switch ( state.getMemoState() ) {
                            case 2:
                                return this.getPath( player.getLevel() >= minimumLevel ? '32653-01.html' : '32653-05.html' )

                            case 3:
                                await QuestHelper.giveAdena( player, 296425, true )
                                await QuestHelper.addExpAndSp( player, 921805, 82230 )
                                await state.exitQuest( false, true )

                                return this.getPath( '32653-04.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === RAFFORTY ) {
                    return this.getPath( '32020-05.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}