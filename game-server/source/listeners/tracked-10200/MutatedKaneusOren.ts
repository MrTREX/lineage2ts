import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestStateValues } from '../../gameService/models/quest/State'

const MOUEN = 30196
const ROVIA = 30189
const KAIM_ABIGORE = 18566
const KNIGHT_MONTAGNAR = 18568
const TISSUE_KA = 13836
const TISSUE_KM = 13837
const minimumLevel = 47

export class MutatedKaneusOren extends ListenerLogic {
    constructor() {
        super( 'Q10279_MutatedKaneusOren', 'listeners/tracked-10200/MutatedKaneusOren.ts' )
        this.questId = 10279
        this.questItemIds = [ TISSUE_KA, TISSUE_KM ]
    }

    getQuestStartIds(): Array<number> {
        return [ MOUEN ]
    }

    getTalkIds(): Array<number> {
        return [ MOUEN, ROVIA ]
    }

    getAttackableKillIds(): Array<number> {
        return [ KAIM_ABIGORE, KNIGHT_MONTAGNAR ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q10279_MutatedKaneusOren'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30196-03.htm':
                state.startQuest()
                break

            case '30189-03.htm':
                let player = L2World.getPlayer( data.playerId )
                if ( !QuestHelper.hasQuestItems( player, TISSUE_KA, TISSUE_KM ) ) {
                    return
                }

                await QuestHelper.giveAdena( player, 100000, true )
                await state.exitQuest( false, true )

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player: L2PcInstance = this.getRandomPartyMemberPerCondition( L2World.getPlayer( data.playerId ), -1, 1, L2World.getObjectById( data.targetId ) )
        if ( !player ) {
            return
        }

        let itemId: number = data.npcId === KAIM_ABIGORE ? TISSUE_KA : TISSUE_KM
        await QuestHelper.giveSingleItem( player, itemId, 1 )

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    checkPartyMemberConditions( state: QuestState, conditionValue: number, target: L2Npc ): boolean {
        if ( !state.isStarted() ) {
            return false
        }

        let player = state.getPlayer()
        let itemId: number = target.getId() === KAIM_ABIGORE ? TISSUE_KA : TISSUE_KM

        return !QuestHelper.hasQuestItem( player, itemId )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== MOUEN ) {
                    break
                }

                return this.getPath( player.getLevel() > minimumLevel ? '30196-01.htm' : '30196-00.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case MOUEN:
                        return this.getPath( QuestHelper.hasQuestItems( player, TISSUE_KA, TISSUE_KM ) ? '30196-05.htm' : '30196-04.htm' )

                    case ROVIA:
                        return this.getPath( QuestHelper.hasQuestItems( player, TISSUE_KA, TISSUE_KM ) ? '30189-02.htm' : '30189-01.htm' )
                }

                break

            case QuestStateValues.COMPLETED:
                switch ( data.characterNpcId ) {
                    case MOUEN:
                        return this.getPath( '30916-06.htm' )

                    case ROVIA:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}