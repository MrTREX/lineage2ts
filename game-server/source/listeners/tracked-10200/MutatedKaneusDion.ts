import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestStateValues } from '../../gameService/models/quest/State'

const LUKAS = 30071
const MIRIEN = 30461
const CRIMSON_HATU = 18558
const SEER_FLOUROS = 18559
const TISSUE_CH = 13832
const TISSUE_SF = 13833
const minimumLevel = 27

export class MutatedKaneusDion extends ListenerLogic {
    constructor() {
        super( 'Q10277_MutatedKaneusDion', 'listeners/tracked-10200/MutatedKaneusDion.ts' )
        this.questId = 10277
        this.questItemIds = [ TISSUE_CH, TISSUE_SF ]
    }

    getQuestStartIds(): Array<number> {
        return [ LUKAS ]
    }

    getTalkIds(): Array<number> {
        return [ LUKAS, MIRIEN ]
    }

    getAttackableKillIds(): Array<number> {
        return [ CRIMSON_HATU, SEER_FLOUROS ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q10277_MutatedKaneusDion'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30071-03.html':
                state.startQuest()
                break

            case '30461-03.html':
                let player = L2World.getPlayer( data.playerId )
                if ( !QuestHelper.hasQuestItems( player, TISSUE_CH, TISSUE_SF ) ) {
                    return
                }

                await QuestHelper.giveAdena( player, 20000, true )
                await state.exitQuest( false, true )

                break

            default:
                return
        }


        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player: L2PcInstance = this.getRandomPartyMemberPerCondition( L2World.getPlayer( data.playerId ), -1, 1, L2World.getObjectById( data.targetId ) )
        if ( !player ) {
            return
        }

        let itemId: number = data.npcId === CRIMSON_HATU ? TISSUE_CH : TISSUE_SF
        await QuestHelper.giveSingleItem( player, itemId, 1 )

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    checkPartyMemberConditions( state: QuestState, conditionValue: number, target: L2Npc ): boolean {
        if ( !state.isStarted() ) {
            return false
        }

        let player = state.getPlayer()
        let itemId: number = target.getId() === CRIMSON_HATU ? TISSUE_CH : TISSUE_SF

        return !QuestHelper.hasQuestItem( player, itemId )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== LUKAS ) {
                    break
                }

                return this.getPath( player.getLevel() > minimumLevel ? '30071-01.htm' : '30071-00.html' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case LUKAS:
                        return this.getPath( QuestHelper.hasQuestItems( player, TISSUE_CH, TISSUE_SF ) ? '30071-05.html' : '30071-04.html' )

                    case MIRIEN:
                        return this.getPath( QuestHelper.hasQuestItems( player, TISSUE_CH, TISSUE_SF ) ? '30461-02.html' : '30461-01.html' )
                }

                break

            case QuestStateValues.COMPLETED:
                switch ( data.characterNpcId ) {
                    case LUKAS:
                        return this.getPath( '30071-06.html' )

                    case MIRIEN:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}