import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestStateValues } from '../../gameService/models/quest/State'

const MATHIAS = 31340
const KAYAN = 31335
const WHITE_ALLOSCE = 18577
const TISSUE_WA = 13840
const minimumLevel = 67

export class MutatedKaneusRune extends ListenerLogic {
    constructor() {
        super( 'Q10281_MutatedKaneusRune', 'listeners/tracked-10200/MutatedKaneusRune.ts' )
        this.questId = 10281
        this.questItemIds = [ TISSUE_WA ]
    }

    getQuestStartIds(): Array<number> {
        return [ MATHIAS ]
    }

    getTalkIds(): Array<number> {
        return [ MATHIAS, KAYAN ]
    }

    getAttackableKillIds(): Array<number> {
        return [ WHITE_ALLOSCE ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q10281_MutatedKaneusRune'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '31340-03.htm':
                state.startQuest()
                break

            case '31335-03.htm':
                let player = L2World.getPlayer( data.playerId )
                if ( !QuestHelper.hasQuestItem( player, TISSUE_WA ) ) {
                    return
                }

                await QuestHelper.giveAdena( player, 360000, true )
                await state.exitQuest( false, true )

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player: L2PcInstance = this.getRandomPartyMemberPerCondition( L2World.getPlayer( data.playerId ), -1, 1, L2World.getObjectById( data.targetId ) )
        if ( !player ) {
            return
        }

        await QuestHelper.giveSingleItem( player, TISSUE_WA, 1 )

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    checkPartyMemberConditions( state: QuestState, conditionValue: number, target: L2Npc ): boolean {
        if ( !state.isStarted() ) {
            return false
        }

        let player = state.getPlayer()
        return !QuestHelper.hasQuestItem( player, TISSUE_WA )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== MATHIAS ) {
                    break
                }

                return this.getPath( player.getLevel() > minimumLevel ? '31340-01.htm' : '31340-00.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case MATHIAS:
                        return this.getPath( QuestHelper.hasQuestItem( player, TISSUE_WA ) ? '31340-05.htm' : '31340-04.htm' )

                    case KAYAN:
                        return this.getPath( QuestHelper.hasQuestItem( player, TISSUE_WA ) ? '31335-02.htm' : '31335-01.htm' )
                }

                break

            case QuestStateValues.COMPLETED:
                switch ( data.characterNpcId ) {
                    case MATHIAS:
                        return this.getPath( '31340-06.htm' )

                    case KAYAN:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}