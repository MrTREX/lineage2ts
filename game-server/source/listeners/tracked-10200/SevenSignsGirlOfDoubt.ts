import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'

import _ from 'lodash'

const HARDIN = 30832
const WOOD = 32593
const FRANZ = 32597
const ELCADIA = 32784
const ELCADIAS_MARK = 17226
const minimumLevel = 81
const CREATURE_OF_THE_DUSK1 = 27422
const CREATURE_OF_THE_DUSK2 = 27424

const variableNames = {
    killAmount: 'ka',
}

export class SevenSignsGirlOfDoubt extends ListenerLogic {
    constructor() {
        super( 'Q10292_SevenSignsGirlOfDoubt', 'listeners/tracked-10200/SevenSignsGirlOfDoubt.ts' )
        this.questId = 10292
        this.questItemIds = [ ELCADIAS_MARK ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            22801, // Cruel Pincer Golem
            22802, // Cruel Pincer Golem
            22803, // Cruel Pincer Golem
            22804, // Horrifying Jackhammer Golem
            22805, // Horrifying Jackhammer Golem
            22806, // Horrifying Jackhammer Golem
            CREATURE_OF_THE_DUSK1,
            CREATURE_OF_THE_DUSK2,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ WOOD ]
    }

    getTalkIds(): Array<number> {
        return [ WOOD, FRANZ, ELCADIA, HARDIN ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q10292_SevenSignsGirlOfDoubt'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32593-02.htm':
            case '32593-04.htm':
            case '32597-02.html':
            case '32597-04.html':
                break

            case '32593-03.htm':
                state.startQuest()
                break

            case '32597-03.html':
                state.setConditionWithSound( 2, true )
                break

            case '32784-02.html':
                if ( state.isCondition( 2 ) ) {
                    break
                }

                return

            case '32784-03.html':
                if ( state.isCondition( 2 ) ) {
                    state.setConditionWithSound( 3, true )
                    break
                }

                return

            case '32784-05.html':
                if ( state.isCondition( 4 ) ) {
                    break
                }

                return

            case '32784-06.html':
                if ( state.isCondition( 4 ) ) {
                    state.setConditionWithSound( 5, true )

                    break
                }

                return

            case 'SPAWN':
                if ( !NpcVariablesManager.get( data.characterId, this.getName() ) ) {
                    state.setVariable( variableNames.killAmount, 0 )
                    NpcVariablesManager.set( data.characterId, this.getName(), true )

                    QuestHelper.addGenericSpawn( null, CREATURE_OF_THE_DUSK1, 89440, -238016, -9632, _.random( 360 ), false, 0, false, player.getInstanceId() )
                    QuestHelper.addGenericSpawn( null, CREATURE_OF_THE_DUSK2, 89524, -238131, -9632, _.random( 360 ), false, 0, false, player.getInstanceId() )

                    return
                }

                return this.getPath( '32784-07.html' )

            case '32784-11.html':
            case '32784-12.html':
                if ( state.isCondition( 6 ) ) {
                    break
                }

                return

            case '32784-13.html':
                if ( state.isCondition( 6 ) ) {
                    state.setConditionWithSound( 7, true )

                    break
                }

                return

            case '30832-02.html':
                if ( state.isCondition( 7 ) ) {
                    state.setConditionWithSound( 8, true )
                    break
                }

                return

            case '30832-03.html':
                if ( state.isCondition( 8 ) ) {
                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), -1, 3, L2World.getObjectById( data.targetId ) )
        if ( !state ) {
            return
        }

        if ( [ CREATURE_OF_THE_DUSK1, CREATURE_OF_THE_DUSK2 ].includes( data.npcId ) ) {
            state.incrementVariable( variableNames.killAmount, 1 )

            if ( state.getVariable( variableNames.killAmount ) === 2 ) {
                state.setConditionWithSound( 6, true )
            }

            return
        }

        if ( Math.random() > QuestHelper.getAdjustedChance( ELCADIAS_MARK, 0.7, data.isChampion ) ) {
            return
        }

        let player = state.getPlayer()
        await QuestHelper.rewardAndProgressState( player, state, ELCADIAS_MARK, 1, 10, data.isChampion, 4 )
    }

    checkPartyMemberConditions( state: QuestState ): boolean {
        return state.getCondition() < 4
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== WOOD ) {
                    break
                }

                let canProceed: boolean = player.getLevel() >= minimumLevel && player.hasQuestCompleted( 'Q00198_SevenSignsEmbryo' )
                return this.getPath( canProceed ? '32593-01.htm' : '32593-06.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case WOOD:
                        return this.getPath( '32593-07.html' )

                    case FRANZ:
                        let conditionValue = state.getCondition()
                        if ( conditionValue === 1 ) {
                            return this.getPath( '32597-01.html' )
                        }

                        if ( conditionValue >= 2 && conditionValue < 7 ) {
                            return this.getPath( '32597-05.html' )
                        }

                        if ( conditionValue === 7 ) {
                            return this.getPath( '32597-06.html' )
                        }

                        break

                    case ELCADIA:
                        switch ( state.getCondition() ) {
                            case 2:
                                return this.getPath( '32784-01.html' )

                            case 3:
                                if ( QuestHelper.getQuestItemsCount( player, ELCADIAS_MARK ) < 10 ) {
                                    return this.getPath( '32784-03.html' )
                                }

                                await QuestHelper.takeSingleItem( player, ELCADIAS_MARK, -1 )
                                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                                return this.getPath( '32784-04.html' )

                            case 4:
                                return this.getPath( '32784-08.html' )

                            case 5:
                                return this.getPath( '32784-09.html' )

                            case 6:
                                return this.getPath( '32784-10.html' )

                            case 7:
                                return this.getPath( '32784-14.html' )

                            case 8:
                                if ( player.isSubClassActive() ) {
                                    return this.getPath( '32784-15.html' )
                                }

                                await QuestHelper.addExpAndSp( player, 10000000, 1000000 )
                                await state.exitQuest( false, true )

                                return this.getPath( '32784-16.html' )

                        }

                        break

                    case HARDIN:
                        switch ( state.getCondition() ) {
                            case 7:
                                return this.getPath( '30832-01.html' )

                            case 8:
                                return this.getPath( '30832-03.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === WOOD ) {
                    return this.getPath( '32593-05.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}