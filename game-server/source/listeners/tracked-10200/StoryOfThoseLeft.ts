import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { InstanceManager } from '../../gameService/instancemanager/InstanceManager'
import { InstanceWorld } from '../../gameService/models/instancezone/InstanceWorld'

import _ from 'lodash'

const RAFFORTY = 32020
const JINIA = 32760
const KEGOR = 32761
const minimumLevel = 82
const exitLocation = new Location( 113793, -109342, -845, 0 )
const variableNames = {
    one: 'one',
    two: 'two',
}

export class StoryOfThoseLeft extends ListenerLogic {
    constructor() {
        super( 'Q10287_StoryOfThoseLeft', 'listeners/tracked-10200/StoryOfThoseLeft.ts' )
        this.questId = 10287
    }

    getQuestStartIds(): Array<number> {
        return [ RAFFORTY ]
    }

    getTalkIds(): Array<number> {
        return [ RAFFORTY, JINIA, KEGOR ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q10287_StoryOfThoseLeft'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32020-02.htm':
                state.startQuest()
                state.setMemoState( 1 )

                break

            case '32020-08.html': {
                if ( state.isMemoState( 2 ) ) {
                    break
                }
                return
            }
            case '32760-02.html': {
                if ( state.isMemoState( 1 ) ) {
                    break
                }
                return
            }
            case '32760-03.html':
                if ( state.isMemoState( 1 ) ) {
                    state.setVariable( variableNames.one, true )
                    state.setConditionWithSound( 3, true )

                    break
                }

                return

            case '32760-06.html':
                if ( state.isMemoState( 2 ) ) {
                    state.setConditionWithSound( 5, true )
                    await player.teleportToLocation( exitLocation, true )
                    break
                }

                return

            case '32761-02.html':
                if ( state.isMemoState( 1 )
                        && state.getVariable( variableNames.one )
                        && !state.getVariable( variableNames.two ) ) {
                    break
                }

                return

            case '32761-03.html':
                if ( state.isMemoState( 1 )
                        && state.getVariable( variableNames.one )
                        && !state.getVariable( variableNames.two ) ) {
                    state.setVariable( variableNames.two, true )
                    state.setConditionWithSound( 4, true )

                    break
                }

                return

            case '10549':
            case '10550':
            case '10551':
            case '10552':
            case '10553':
            case '14219':
                if ( state.isMemoState( 2 ) ) {
                    await QuestHelper.rewardSingleItem( player, _.parseInt( data.eventName ), 1 )
                    await state.exitQuest( false, true )

                    return this.getPath( '32020-09.html' )
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== RAFFORTY ) {
                    break
                }

                let canProceed: boolean = player.getLevel() >= minimumLevel && player.hasQuestCompleted( 'Q10286_ReunionWithSirra' )
                return this.getPath( canProceed ? '32020-01.htm' : '32020-03.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case RAFFORTY:
                        if ( state.isMemoState( 1 ) ) {
                            return this.getPath( player.getLevel() >= minimumLevel ? '32020-05.html' : '32020-06.html' )
                        }

                        if ( state.isMemoState( 2 ) ) {
                            return this.getPath( '32020-07.html' )
                        }

                        break

                    case JINIA:
                        if ( !state.isMemoState( 1 ) ) {
                            break
                        }

                        let one: boolean = state.getVariable( variableNames.one )
                        let two: boolean = state.getVariable( variableNames.two )
                        if ( !one && !two ) {
                            return this.getPath( '32760-01.html' )
                        }

                        if ( one && !two ) {
                            return this.getPath( '32760-04.html' )
                        }

                        if ( one && two ) {
                            state.setConditionWithSound( 5, true )
                            state.setMemoState( 2 )

                            state.unsetVariables( variableNames.one, variableNames.two )

                            let world: InstanceWorld = InstanceManager.getPlayerWorld( data.playerId )
                            world.removeAllowedId( data.playerId )
                            player.setInstanceId( 0 )

                            return this.getPath( '32760-05.html' )
                        }

                        break

                    case KEGOR:
                        if ( !state.isMemoState( 1 ) ) {
                            break
                        }

                        let valueOne: boolean = state.getVariable( variableNames.one )
                        let valueTwo: boolean = state.getVariable( variableNames.two )

                        if ( valueOne && !valueTwo ) {
                            return this.getPath( '32761-01.html' )
                        }

                        if ( !valueOne && !valueTwo ) {
                            return this.getPath( '32761-04.html' )
                        }

                        if ( valueOne && valueTwo ) {
                            return this.getPath( '32761-05.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === RAFFORTY ) {
                    return this.getPath( '32020-04.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}