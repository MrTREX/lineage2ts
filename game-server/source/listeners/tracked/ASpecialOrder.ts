import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const HELVETIA = 30081
const OFULLE = 31572
const GESTO = 30511

const ORANGE_SWIFT_FISH = 6450
const ORANGE_UGLY_FISH = 6451
const ORANGE_WIDE_FISH = 6452
const GOLDEN_COBOL = 5079
const BUR_COBOL = 5082
const GREAT_COBOL = 5084
const WONDROUS_CUBIC = 10632
const BOX_OF_FISH = 12764
const BOX_OF_SEED = 12765

const minimumLevel = 40

export class ASpecialOrder extends ListenerLogic {
    constructor() {
        super( 'Q00040_ASpecialOrder', 'listeners/tracked/ASpecialOrder.ts' )
        this.questId = 40
        this.questItemIds = [ BOX_OF_FISH, BOX_OF_SEED ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00040_ASpecialOrder'
    }

    getQuestStartIds(): Array<number> {
        return [ HELVETIA ]
    }

    getTalkIds(): Array<number> {
        return [ HELVETIA ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'accept':
                state.startQuest()

                if ( Math.random() > 0.5 ) {
                    state.setConditionWithSound( 2 )
                    return this.getPath( '30081-03.html' )
                }

                state.setConditionWithSound( 5 )
                return this.getPath( '30081-04.html' )

            case '30081-07.html':
                if ( state.isCondition( 4 ) && QuestHelper.hasQuestItem( player, BOX_OF_FISH ) ) {
                    await QuestHelper.rewardSingleItem( player, WONDROUS_CUBIC, 1 )
                    await state.exitQuest( false, true )

                    break
                }

                return

            case '30081-10.html':
                if ( state.isCondition( 7 ) && QuestHelper.hasQuestItem( player, BOX_OF_SEED ) ) {
                    await QuestHelper.rewardSingleItem( player, WONDROUS_CUBIC, 1 )
                    await state.exitQuest( false, true )

                    break
                }

                return

            case '31572-02.html':
            case '30511-02.html':
                break

            case '31572-03.html':
                if ( state.isCondition( 2 ) ) {
                    state.setConditionWithSound( 3, true )

                    break
                }

                return

            case '30511-03.html':
                if ( state.isCondition( 5 ) ) {
                    state.setConditionWithSound( 6, true )

                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case HELVETIA:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() >= minimumLevel ? '30081-01.htm' : '30081-02.htm' )

                    case QuestStateValues.STARTED:
                        switch ( state.getCondition() ) {
                            case 2:
                            case 3:
                                return this.getPath( '30081-05.html' )

                            case 4:
                                if ( QuestHelper.hasQuestItem( player, BOX_OF_FISH ) ) {
                                    return this.getPath( '30081-06.html' )
                                }

                                break

                            case 5:
                            case 6:
                                return this.getPath( '30081-08.html' )

                            case 7:
                                if ( QuestHelper.hasQuestItem( player, BOX_OF_SEED ) ) {
                                    return this.getPath( '30081-09.html' )
                                }

                                break
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case OFULLE:
                switch ( state.getCondition() ) {
                    case 2:
                        return this.getPath( '31572-01.html' )

                    case 3:
                        if ( QuestHelper.getQuestItemsCount( player, ORANGE_SWIFT_FISH ) >= 10
                                && QuestHelper.getQuestItemsCount( player, ORANGE_UGLY_FISH ) >= 10
                                && QuestHelper.getQuestItemsCount( player, ORANGE_WIDE_FISH ) >= 10 ) {
                            state.setConditionWithSound( 4, true )
                            await QuestHelper.rewardSingleItem( player, BOX_OF_FISH, 1 )
                            await QuestHelper.takeMultipleItems( player, 10, ORANGE_SWIFT_FISH, ORANGE_UGLY_FISH, ORANGE_WIDE_FISH )

                            return this.getPath( '31572-05.html' )
                        }

                        return this.getPath( '31572-04.html' )

                    case 4:
                        return this.getPath( '31572-06.html' )
                }

                break

            case GESTO:
                switch ( state.getCondition() ) {
                    case 5:
                        return this.getPath( '30511-01.html' )

                    case 6:
                        if ( QuestHelper.getQuestItemsCount( player, GOLDEN_COBOL ) >= 40
                                && QuestHelper.getQuestItemsCount( player, BUR_COBOL ) >= 40
                                && QuestHelper.getQuestItemsCount( player, GREAT_COBOL ) >= 40 ) {
                            state.setConditionWithSound( 7, true )
                            await QuestHelper.rewardSingleItem( player, BOX_OF_SEED, 1 )
                            await QuestHelper.takeMultipleItems( player, 40, GOLDEN_COBOL, BUR_COBOL, GREAT_COBOL )

                            return this.getPath( '30511-05.html' )
                        }

                        return this.getPath( '30511-04.html' )

                    case 7:
                        return this.getPath( '30511-06.html' )
                }

                break

        }

        return QuestHelper.getNoQuestMessagePath()
    }
}