import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

const WATERS = 30828
const SOPHYA = 30735

const MONSTER_EYE_DESTROYER = 20068
const MONSTER_EYE_GAZER = 20266

const TRIDENT = 291
const MAP_PIECE = 7548
const MAP = 7549
const PET_TICKET = 7583

const minimumLevel = 25

export class HelpTheUncle extends ListenerLogic {
    constructor() {
        super( 'Q00042_HelpTheUncle', 'listeners/tracked/HelpTheUncle.ts' )
        this.questId = 42
        this.questItemIds = [ MAP, MAP_PIECE ]
    }

    getAttackableKillIds(): Array<number> {
        return [ MONSTER_EYE_DESTROYER, MONSTER_EYE_GAZER ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00042_HelpTheUncle'
    }

    getQuestStartIds(): Array<number> {
        return [ WATERS ]
    }

    getTalkIds(): Array<number> {
        return [ WATERS, SOPHYA ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )

        if ( !state || !state.isCondition( 2 ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        await QuestHelper.rewardSingleQuestItem( player, MAP_PIECE, 1, data.isChampion )

        if ( QuestHelper.getQuestItemsCount( player, MAP_PIECE ) >= 30 ) {
            state.setConditionWithSound( 3, true )
            return
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30828-01.htm':
                state.startQuest()
                break

            case '30828-03.html':
                if ( QuestHelper.hasQuestItem( player, TRIDENT ) ) {
                    await QuestHelper.takeSingleItem( player, TRIDENT, 1 )
                    state.setConditionWithSound( 2, true )

                    break
                }

                return this.getPath( '30828-03a.html' )

            case '30828-06.html':
                if ( QuestHelper.getQuestItemsCount( player, MAP_PIECE ) >= 30 ) {
                    await QuestHelper.takeSingleItem( player, MAP_PIECE, -1 )
                    await QuestHelper.giveSingleItem( player, MAP, 1 )
                    state.setConditionWithSound( 4, true )

                    break
                }

                return this.getPath( '30828-06a.html' )

            case '30735-02.html':
                if ( QuestHelper.hasQuestItem( player, MAP ) ) {
                    await QuestHelper.takeSingleItem( player, MAP, -1 )
                    state.setConditionWithSound( 5, true )

                    break
                }

                return this.getPath( '30735-02a.html' )

            case '30828-09.html':
                await QuestHelper.giveSingleItem( player, PET_TICKET, 1 )
                await state.exitQuest( false, true )
                break
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case WATERS:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() >= minimumLevel ? '30828-00.htm' : '30828-00a.html' )

                    case QuestStateValues.STARTED:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( QuestHelper.hasQuestItem( player, TRIDENT ) ? '30828-02.html' : '30828-02a.html' )

                            case 2:
                                return this.getPath( '30828-04.html' )

                            case 3:
                                return this.getPath( '30828-05.html' )

                            case 4:
                                return this.getPath( '30828-07.html' )

                            case 5:
                                return this.getPath( '30828-08.html' )
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case SOPHYA:
                if ( state.isStarted() ) {
                    switch ( state.getCondition() ) {
                        case 4:
                            return this.getPath( '30735-01.html' )

                        case 5:
                            return this.getPath( '30735-03.html' )
                    }
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}