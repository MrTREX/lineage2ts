import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import { AttackableAttackedEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { AIEffectHelper } from '../../gameService/aicontroller/helpers/AIEffectHelper'
import _ from 'lodash'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const HIGH_PRIEST_AGRIPEL = 31348
const PRIEST_BENEDICT = 31349
const MYSTERIOUS_WIZARD = 31522
const TOMBSTONE = 31531
const MAID_OF_LIDIA = 31532
const BROKEN_BOOKSHELF2 = 31533
const BROKEN_BOOKSHELF3 = 31534
const BROKEN_BOOKSHELF4 = 31535
const COFFIN = 31536

const TRIOL_PAWN = 27218

const MAP_FOREST_OF_THE_DEAD = 7063
const CONTRACT = 7066
const LIDAS_DRESS = 7155
const TOTEM_DOLL2 = 7156
const GEMSTONE_KEY = 7157
const TOTEM_DOLL3 = 7158

const NECKLACE_OF_BLESSING = 936
const EARING_OF_BLESSING = 874
const RING_OF_BLESSING = 905

const minimumLevel = 66
const locations: { [ npcId: number ]: Location } = {
    [ BROKEN_BOOKSHELF2 ]: new Location( 47142, -35941, -1623 ),
    [ BROKEN_BOOKSHELF3 ]: new Location( 50055, -47020, -3396 ),
    [ BROKEN_BOOKSHELF4 ]: new Location( 59712, -47568, -2720 ),
}
const coffinLocation = new Location( 60104, -35820, -681 )
const variableNames = {
    questId: 'qd',
}

export class HidingBehindTheTruth extends ListenerLogic {
    constructor() {
        super( 'Q00025_HidingBehindTheTruth', 'listeners/tracked/HidingBehindTheTruth.ts' )
        this.questId = 25
        this.questItemIds = [ GEMSTONE_KEY, CONTRACT, TOTEM_DOLL3, TOTEM_DOLL2, LIDAS_DRESS ]
    }

    getAttackableAttackIds(): Array<number> {
        return [ TRIOL_PAWN ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00025_HidingBehindTheTruth'
    }

    getQuestStartIds(): Array<number> {
        return [ PRIEST_BENEDICT ]
    }

    getTalkIds(): Array<number> {
        return [ HIGH_PRIEST_AGRIPEL, PRIEST_BENEDICT, MYSTERIOUS_WIZARD, TOMBSTONE, MAID_OF_LIDIA, BROKEN_BOOKSHELF2, BROKEN_BOOKSHELF3, BROKEN_BOOKSHELF4, COFFIN ]
    }

    async onAttackableAttackedEvent( data: AttackableAttackedEvent ): Promise<void> {
        let npc: L2Npc = L2World.getObjectById( data.targetId ) as L2Npc
        if ( npc.getCurrentHp() <= ( 0.30 * npc.getMaxHp() ) ) {
            let player = L2World.getPlayer( data.attackerPlayerId )
            let state = QuestStateCache.getQuestState( data.attackerPlayerId, this.name )
            if ( state
                    && state.isMemoState( 8 )
                    && !QuestHelper.hasQuestItem( player, TOTEM_DOLL3 )
                    && data.attackerPlayerId === NpcVariablesManager.get( data.targetId, this.getName() ) ) {
                state.setConditionWithSound( 8, true )

                await QuestHelper.rewardSingleItem( player, TOTEM_DOLL3, 1 )
                BroadcastHelper.broadcastNpcSayStringId( npc,
                        NpcSayType.All,
                        NpcStringIds.YOUVE_ENDED_MY_IMMORTAL_LIFE_YOURE_PROTECTED_BY_THE_FEUDAL_LORD_ARENT_YOU )

                let objectId: number = NpcVariablesManager.get( data.targetId, variableNames.questId ) as number
                if ( objectId > 0 ) {
                    NpcVariablesManager.set( objectId, variableNames.questId, 0 )
                }

                await npc.deleteMe()
            }
        }

        return
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc: L2Npc = L2World.getObjectById( data.characterId ) as L2Npc

        switch ( data.eventName ) {
            case '31349-06.html':
            case '31349-07.html':
            case '31349-08.html':
            case '31349-09.html':
            case '31522-08.html':
            case '31522-09.html':
            case '31522-07.html':
            case '31522-11.html':
            case '31348-04.html':
            case '31348-05.html':
            case '31348-06.html':
            case '31348-11.html':
            case '31348-07.html':
            case '31348-12.html':
            case '31348-14.html':
            case '31532-04.html':
            case '31532-05.html':
            case '31532-06.html':
            case '31532-14.html':
            case '31532-15.html':
            case '31532-16.html':
            case '31532-19.html':
            case '31532-20.html':
                return this.getPath( data.eventName )

            case '31349-03.html':
                if ( state.isCreated()
                        && player.hasQuestCompleted( 'Q00024_InhabitantsOfTheForestOfTheDead' )
                        && player.getLevel() >= minimumLevel ) {
                    state.setMemoState( 1 )
                    state.startQuest()
                    return this.getPath( data.eventName )
                }

                break

            case '31349-05.html':
                if ( state.isMemoState( 1 ) ) {
                    if ( QuestHelper.hasQuestItem( player, TOTEM_DOLL2 ) ) {
                        return this.getPath( '31349-04.html' )
                    }

                    state.setConditionWithSound( 2, true )
                    return this.getPath( data.eventName )
                }

                break

            case '31349-10.html':
                if ( state.isMemoState( 1 ) && QuestHelper.hasQuestItem( player, TOTEM_DOLL2 ) ) {
                    state.setMemoState( 2 )
                    state.setConditionWithSound( 4, true )
                    return this.getPath( data.eventName )
                }

                break

            case '31522-04.html':
                if ( state.isMemoState( 6 ) && QuestHelper.hasQuestItem( player, GEMSTONE_KEY ) ) {
                    state.setMemoState( 7 )
                    state.setMemoStateEx( 1, 20 )
                    state.setConditionWithSound( 6, true )
                    return this.getPath( data.eventName )
                }

                break

            case '31522-10.html':
                if ( state.isMemoState( 16 ) ) {
                    state.setMemoState( 19 )
                    return this.getPath( data.eventName )
                }

                break

            case '31522-13.html':
                if ( state.isMemoState( 19 ) ) {
                    state.setMemoState( 20 )
                    state.setConditionWithSound( 16, true )
                    return this.getPath( data.eventName )
                }

                break

            case '31522-16.html':
                if ( state.isMemoState( 24 ) ) {
                    await QuestHelper.takeSingleItem( player, MAP_FOREST_OF_THE_DEAD, -1 )
                    await QuestHelper.rewardSingleItem( player, EARING_OF_BLESSING, 1 )
                    await QuestHelper.rewardSingleItem( player, NECKLACE_OF_BLESSING, 1 )
                    await QuestHelper.addExpAndSp( player, 572277, 53750 )

                    await state.exitQuest( false, true )
                    return this.getPath( data.eventName )
                }

                break

            case '31348-02.html':
                if ( state.isMemoState( 2 ) ) {
                    await QuestHelper.takeSingleItem( player, TOTEM_DOLL2, -1 )
                    state.setMemoState( 3 )
                    return this.getPath( data.eventName )
                }

                break

            case '31348-08.html':
                if ( state.isMemoState( 3 ) ) {
                    await QuestHelper.takeSingleItem( player, GEMSTONE_KEY, 1 )
                    state.setMemoState( 6 )
                    state.setConditionWithSound( 5, true )
                    return this.getPath( data.eventName )
                }

                break

            case '31348-10.html':
                if ( state.isMemoState( 20 ) && QuestHelper.hasQuestItem( player, TOTEM_DOLL3 ) ) {
                    await QuestHelper.takeSingleItem( player, TOTEM_DOLL3, -1 )
                    state.setMemoState( 21 )
                    return this.getPath( data.eventName )
                }

                break

            case '31348-13.html':
                if ( state.isMemoState( 21 ) ) {
                    state.setMemoState( 22 )
                    return this.getPath( data.eventName )
                }

                break

            case '31348-16.html':
                if ( state.isMemoState( 22 ) ) {
                    state.setMemoState( 23 )
                    state.setConditionWithSound( 17, true )
                    return this.getPath( data.eventName )
                }

                break

            case '31348-17.html':
                if ( state.isMemoState( 22 ) ) {
                    state.setMemoState( 24 )
                    state.setConditionWithSound( 18, true )
                    return this.getPath( data.eventName )
                }

                break

            case '31533-04.html':
                if ( state.getMemoStateEx( data.characterNpcId ) !== 0 ) {
                    return this.getPath( '31533-03.html' )
                }

                if ( _.random( 60 ) > state.getMemoStateEx( 1 ) ) {
                    state.setMemoStateEx( 1, state.getMemoStateEx( 1 ) + 20 )
                    state.setMemoStateEx( data.characterNpcId, 1 )
                    return this.getPath( '31533-03.html' )
                }

                state.setMemoState( 8 )
                player.sendCopyData( SoundPacket.AMDSOUND_HORROR_02 )

                return this.getPath( data.eventName )

            case '31533-05.html':
                if ( state.isMemoState( 8 ) ) {
                    if ( !QuestHelper.hasQuestItem( player, TOTEM_DOLL3 ) ) {
                        let brokenDeskOwner = NpcVariablesManager.get( data.characterId, variableNames.questId )
                        if ( !brokenDeskOwner ) {
                            NpcVariablesManager.set( data.characterId, variableNames.questId, data.playerId )
                            let spawnedNpc: L2Npc = QuestHelper.addSpawnAtLocation( TRIOL_PAWN, locations[ data.characterNpcId ], true, 0 )

                            NpcVariablesManager.set( spawnedNpc.getObjectId(), variableNames.questId, data.characterId )
                            NpcVariablesManager.set( spawnedNpc.getObjectId(), this.getName(), data.playerId )

                            this.startQuestTimer( 'SAY_TRIYOL', 500, spawnedNpc.getObjectId(), data.playerId )
                            this.startQuestTimer( 'DESPAWN_TRIYOL', 120000, spawnedNpc.getObjectId(), data.playerId )
                            await AIEffectHelper.notifyAttackedWithTargetId( spawnedNpc, data.playerId, 1, 100 )

                            state.setConditionWithSound( 7, false )
                            return this.getPath( data.eventName )
                        }

                        if ( brokenDeskOwner === data.playerId ) {
                            return this.getPath( '31533-06.html' )
                        }

                        return this.getPath( '31533-07.html' )
                    }

                    return this.getPath( '31533-08.html' )
                }

                break

            case '31533-09.html':
                if ( state.isMemoState( 8 ) && QuestHelper.hasQuestItems( player, TOTEM_DOLL3, GEMSTONE_KEY ) ) {
                    await QuestHelper.giveSingleItem( player, CONTRACT, 1 )
                    await QuestHelper.takeSingleItem( player, GEMSTONE_KEY, -1 )
                    state.setMemoState( 9 )
                    state.setConditionWithSound( 9, false )
                    return this.getPath( data.eventName )
                }

                break

            case 'SAY_TRIYOL':
                BroadcastHelper.broadcastNpcSayStringId( npc,
                        NpcSayType.All,
                        NpcStringIds.THAT_BOX_WAS_SEALED_BY_MY_MASTER_S1_DONT_TOUCH_IT,
                        player.getName() )
                break

            case 'DESPAWN_TRIYOL':
                let objectId: number = NpcVariablesManager.get( data.characterId, variableNames.questId ) as number
                if ( objectId > 0 ) {
                    NpcVariablesManager.set( objectId, variableNames.questId, 0 )
                }

                await npc.deleteMe()
                break

            case '31532-02.html':
                if ( state.isMemoState( 9 ) && QuestHelper.hasQuestItem( player, CONTRACT ) ) {
                    await QuestHelper.takeSingleItem( player, CONTRACT, -1 )
                    state.setMemoState( 10 )
                    return this.getPath( data.eventName )
                }

                break

            case '31532-07.html':
                if ( state.isMemoState( 10 ) ) {
                    state.setMemoState( 11 )
                    player.sendCopyData( SoundPacket.SKILLSOUND_HORROR_1 )
                    state.setConditionWithSound( 11, false )
                    return this.getPath( data.eventName )
                }

                break

            case '31532-11.html':
                if ( state.isMemoState( 13 ) ) {
                    let memoStateEx = state.getMemoStateEx( 1 )
                    if ( memoStateEx <= 3 ) {
                        state.setMemoStateEx( 1, memoStateEx + 1 )
                        player.sendCopyData( SoundPacket.CHRSOUND_FDELF_CRY )
                        return this.getPath( data.eventName )
                    }

                    state.setMemoState( 14 )
                    return this.getPath( '31532-12.html' )
                }

                break

            case '31532-17.html':
                if ( state.isMemoState( 14 ) ) {
                    state.setMemoState( 15 )
                    return this.getPath( data.eventName )
                }

                break

            case '31532-21.html':
                if ( state.isMemoState( 15 ) ) {
                    state.setMemoState( 16 )
                    state.setConditionWithSound( 15, false )
                    return this.getPath( data.eventName )
                }

                break

            case '31532-25.html':
                if ( state.isMemoState( 23 ) ) {
                    await QuestHelper.takeSingleItem( player, MAP_FOREST_OF_THE_DEAD, -1 )
                    await QuestHelper.rewardSingleItem( player, EARING_OF_BLESSING, 1 )
                    await QuestHelper.rewardSingleItem( player, RING_OF_BLESSING, 2 )
                    await QuestHelper.addExpAndSp( player, 572277, 53750 )

                    await state.exitQuest( false, true )
                    return this.getPath( data.eventName )
                }
                break

            case '31531-02.html':
                if ( state.isMemoState( 11 ) ) {
                    let coffinNpc: L2Npc = QuestHelper.addSpawnAtLocation( COFFIN, coffinLocation, true, 0 )
                    this.startQuestTimer( 'DESPAWN_BOX', 20000, coffinNpc.getObjectId(), data.playerId )

                    state.setConditionWithSound( 12, true )
                    return this.getPath( data.eventName )
                }

                break

            case 'DESPAWN_BOX':
                await npc.deleteMe()
                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === PRIEST_BENEDICT ) {
                    let companionState: QuestState = QuestStateCache.getQuestState( data.playerId, 'Q00024_InhabitantsOfTheForestOfTheDead' )
                    if ( companionState && companionState.isCompleted() && player.getLevel() >= minimumLevel ) {
                        return this.getPath( '31349-01.htm' )
                    }

                    return this.getPath( '31349-02.html' )
                }
                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case PRIEST_BENEDICT:
                        switch ( state.getMemoState() ) {
                            case 1:
                                return this.getPath( '31349-03a.html' )
                            case 2:
                                return this.getPath( '31349-11.html' )
                        }

                        break

                    case MYSTERIOUS_WIZARD:
                        switch ( state.getMemoState() ) {
                            case 1:
                                if ( !QuestHelper.hasQuestItem( player, TOTEM_DOLL2 ) ) {
                                    await QuestHelper.giveSingleItem( player, TOTEM_DOLL2, 1 )
                                    state.setConditionWithSound( 3, true )
                                    return this.getPath( '31522-01.html' )
                                }

                                return this.getPath( '31522-02.html' )

                            case 6:
                                if ( QuestHelper.hasQuestItem( player, GEMSTONE_KEY ) ) {
                                    return this.getPath( '31522-03.html' )
                                }

                                break

                            case 9:
                                if ( QuestHelper.hasQuestItem( player, CONTRACT ) ) {
                                    state.setConditionWithSound( 10, true )
                                    return this.getPath( '31522-06.html' )
                                }

                                break

                            case 16:
                                return this.getPath( '31522-06a.html' )

                            case 19:
                                return this.getPath( '31522-12.html' )

                            case 20:
                                return this.getPath( '31522-14.html' )

                            case 24:
                                return this.getPath( '31522-15.html' )

                            case 23:
                                return this.getPath( '31522-15a.html' )

                            default:
                                if ( ( state.getMemoState() % 100 ) === 7 ) {
                                    return this.getPath( '31522-05.html' )
                                }

                                break
                        }

                        break

                    case HIGH_PRIEST_AGRIPEL:
                        switch ( state.getMemoState() ) {
                            case 2:
                                return this.getPath( '31348-01.html' )

                            case 3:
                                return this.getPath( '31348-03.html' )

                            case 6:
                                return this.getPath( '31348-08a.html' )

                            case 20:
                                if ( QuestHelper.hasQuestItem( player, TOTEM_DOLL3 ) ) {
                                    return this.getPath( '31348-09.html' )
                                }

                                break

                            case 21:
                                return this.getPath( '31348-10a.html' )

                            case 22:
                                return this.getPath( '31348-15.html' )

                            case 23:
                                return this.getPath( '31348-18.html' )

                            case 24:
                                return this.getPath( '31348-19.html' )
                        }

                        break

                    case BROKEN_BOOKSHELF2:
                    case BROKEN_BOOKSHELF3:
                    case BROKEN_BOOKSHELF4:
                        if ( ( state.getMemoState() % 100 ) === 7 ) {
                            return this.getPath( '31533-01.html' )
                        }

                        if ( ( state.getMemoState() % 100 ) >= 9 ) {
                            return this.getPath( '31533-02.html' )
                        }

                        if ( state.isMemoState( 8 ) ) {
                            return this.getPath( '31533-04.html' )
                        }

                        break

                    case MAID_OF_LIDIA:
                        switch ( state.getMemoState() ) {
                            case 9:
                                if ( QuestHelper.hasQuestItem( player, CONTRACT ) ) {
                                    return this.getPath( '31532-01.html' )
                                }

                                break

                            case 10:
                                return this.getPath( '31532-03.html' )

                            case 11:
                                player.sendCopyData( SoundPacket.SKILLSOUND_HORROR_1 )
                                return this.getPath( '31532-08.html' )

                            case 12:
                                if ( QuestHelper.hasQuestItem( player, LIDAS_DRESS ) ) {
                                    await QuestHelper.takeSingleItem( player, LIDAS_DRESS, -1 )
                                    state.setMemoState( 13 )
                                    state.setConditionWithSound( 14, true )
                                    return this.getPath( '31532-09.html' )
                                }

                                break

                            case 13:
                                state.setMemoStateEx( 1, 0 )
                                player.sendCopyData( SoundPacket.CHRSOUND_FDELF_CRY )
                                return this.getPath( '31532-10.html' )

                            case 14:
                                return this.getPath( '31532-13.html' )

                            case 15:
                                return this.getPath( '31532-18.html' )

                            case 16:
                                return this.getPath( '31532-22.html' )

                            case 23:
                                return this.getPath( '31532-23.html' )

                            case 24:
                                return this.getPath( '31532-24.html' )
                        }

                        break

                    case TOMBSTONE:
                        switch ( state.getMemoState() ) {
                            case 11:
                                return this.getPath( '31531-01.html' )
                            case 12:
                                return this.getPath( '31531-03.html' )
                        }

                        break

                    case COFFIN:
                        if ( state.isMemoState( 11 ) ) {
                            await QuestHelper.giveSingleItem( player, LIDAS_DRESS, 1 )
                            this.stopQuestTimer( 'DESPAWN_BOX', data.characterId, data.playerId )

                            this.startQuestTimer( 'DESPAWN_BOX', 3000, data.characterId, data.playerId )
                            state.setMemoState( 12 )
                            state.setConditionWithSound( 13, true )
                            return this.getPath( '31536-01.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === PRIEST_BENEDICT ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}