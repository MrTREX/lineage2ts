import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const ABERCROMBIE = 31555
const FORGOTTEN_MONUMENT_1 = 31661
const FORGOTTEN_MONUMENT_2 = 31662
const FORGOTTEN_MONUMENT_3 = 31663
const FORGOTTEN_MONUMENT_4 = 31664
const CORPSE_OF_DWARF = 31665

const KRORINS_JOURNAL = 7252

const minimumLevel = 66
const monumentNpcIds = new Set<number>( [ FORGOTTEN_MONUMENT_1, FORGOTTEN_MONUMENT_2, FORGOTTEN_MONUMENT_3, FORGOTTEN_MONUMENT_4 ] )

export class SecretBuriedInTheSwamp extends ListenerLogic {
    constructor() {
        super( 'Q00031_SecretBuriedInTheSwamp', 'listeners/tracked/SecretBuriedInTheSwamp.ts' )
        this.questId = 31
        this.questItemIds = [ KRORINS_JOURNAL ]
    }

    getConditionValue( npcId: number ): number {
        return npcId - FORGOTTEN_MONUMENT_1 + 3
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00031_SecretBuriedInTheSwamp'
    }

    getQuestStartIds(): Array<number> {
        return [ ABERCROMBIE ]
    }

    getTalkIds(): Array<number> {
        return [ ABERCROMBIE, CORPSE_OF_DWARF, FORGOTTEN_MONUMENT_1, FORGOTTEN_MONUMENT_2, FORGOTTEN_MONUMENT_3, FORGOTTEN_MONUMENT_4 ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31555-02.html':
                if ( state.isCreated() ) {
                    state.startQuest()

                    return this.getPath( data.eventName )
                }

                return

            case '31665-02.html':
                if ( state.isCondition( 1 ) ) {
                    state.setConditionWithSound( 2, true )
                    await QuestHelper.giveSingleItem( player, KRORINS_JOURNAL, 1 )
                    return this.getPath( data.eventName )
                }

                return

            case '31555-05.html':
                if ( state.isCondition( 2 ) && QuestHelper.hasQuestItem( player, KRORINS_JOURNAL ) ) {
                    await QuestHelper.takeSingleItem( player, KRORINS_JOURNAL, -1 )
                    state.setConditionWithSound( 3, true )
                    return this.getPath( data.eventName )
                }

                return

            case '31661-02.html':
            case '31662-02.html':
            case '31663-02.html':
            case '31664-02.html':
                if ( monumentNpcIds.has( data.characterNpcId ) && state.isCondition( this.getConditionValue( data.characterNpcId ) ) ) {
                    state.setConditionWithSound( state.getCondition() + 1, true )
                    return this.getPath( data.eventName )
                }

                return

            case '31555-08.html':
                if ( state.isCondition( 7 ) ) {
                    await QuestHelper.addExpAndSp( player, 490000, 45880 )
                    await QuestHelper.giveAdena( player, 120000, true )
                    await state.exitQuest( false, true )

                    return this.getPath( data.eventName )
                }

                return
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case ABERCROMBIE:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() >= minimumLevel ? '31555-01.htm' : '31555-03.htm' )

                    case QuestStateValues.STARTED:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '31555-02.html' )

                            case 2:
                                if ( QuestHelper.hasQuestItem( player, KRORINS_JOURNAL ) ) {
                                    return this.getPath( '31555-04.html' )
                                }

                                break

                            case 3:
                                return this.getPath( '31555-06.html' )

                            case 7:
                                return this.getPath( '31555-07.html' )
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getNoQuestMessagePath()
                }

                break

            case CORPSE_OF_DWARF:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '31665-01.html' )

                    case 2:
                        return this.getPath( '31665-03.html' )
                }

                break

            case FORGOTTEN_MONUMENT_1:
            case FORGOTTEN_MONUMENT_2:
            case FORGOTTEN_MONUMENT_3:
            case FORGOTTEN_MONUMENT_4:
                let value: number = this.getConditionValue( data.characterNpcId )
                if ( state.isCondition( value ) ) {
                    return this.getPath( `${ data.characterNpcId }-01.html` )
                }

                if ( state.isCondition( value + 1 ) ) {
                    return this.getPath( `${ data.characterNpcId }-03.html` )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}