import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

const ELLIE = 30091
const FELTON = 30879

const ALLIGATOR = 20135

const SILVER_NUGGET = 1873
const ORIHARUKON = 1893
const THONS = 4044
const JEWEL_BOX = 7077
const ROUGH_JEWEL = 7162

const minimumLevel = 60
const JEWEL_COUNT = 10
const ORIHARUKON_COUNT = 5
const NUGGET_COUNT = 500
const THONS_COUNT = 150

export class FindGlitteringJewelry extends ListenerLogic {
    constructor() {
        super( 'Q00035_FindGlitteringJewelry', 'listeners/tracked/FindGlitteringJewelry.ts' )
        this.questId = 35
        this.questItemIds = [ ROUGH_JEWEL ]
    }

    getAttackableKillIds(): Array<number> {
        return [ ALLIGATOR ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00035_FindGlitteringJewelry'
    }

    getQuestStartIds(): Array<number> {
        return [ ELLIE ]
    }

    getTalkIds(): Array<number> {
        return [ ELLIE, FELTON ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player: L2PcInstance = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 2 )
        if ( !player || QuestHelper.getAdjustedChance( ROUGH_JEWEL, Math.random(), data.isChampion ) < 0.5 ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, ROUGH_JEWEL, 1, data.isChampion )

        if ( QuestHelper.hasQuestItemCount( player, ROUGH_JEWEL, JEWEL_COUNT ) ) {
            let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), this.name )
            state.setConditionWithSound( 3, true )
            return
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30091-03.htm':
                state.startQuest()
                break

            case '30879-02.html':
                state.setConditionWithSound( 2, true )
                break

            case '30091-07.html':
                if ( QuestHelper.getQuestItemsCount( player, ROUGH_JEWEL ) < JEWEL_COUNT ) {
                    return this.getPath( '30091-08.html' )
                }

                await QuestHelper.takeSingleItem( player, ROUGH_JEWEL, -1 )
                state.setConditionWithSound( 4, true )
                break

            case '30091-11.html':
                if ( QuestHelper.hasQuestItemCount( player, ORIHARUKON, ORIHARUKON_COUNT )
                        && QuestHelper.hasQuestItemCount( player, SILVER_NUGGET, NUGGET_COUNT )
                        && QuestHelper.hasQuestItemCount( player, THONS, THONS_COUNT ) ) {

                    await QuestHelper.takeSingleItem( player, ORIHARUKON, ORIHARUKON_COUNT )
                    await QuestHelper.takeSingleItem( player, SILVER_NUGGET, NUGGET_COUNT )
                    await QuestHelper.takeSingleItem( player, THONS, THONS_COUNT )

                    await QuestHelper.giveSingleItem( player, JEWEL_BOX, 1 )
                    await state.exitQuest( false, true )
                    break
                }

                return this.getPath( '30091-12.html' )

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case ELLIE:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() >= minimumLevel ? '30091-01.htm' : '30091-02.html' )

                    case QuestStateValues.STARTED:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '30091-04.html' )

                            case 3:
                                return this.getPath( QuestHelper.hasQuestItemCount( player, ROUGH_JEWEL, JEWEL_COUNT ) ? '30091-06.html' : '30091-05.html' )

                            case 4:
                                if ( QuestHelper.hasQuestItemCount( player, ORIHARUKON, ORIHARUKON_COUNT )
                                        && QuestHelper.hasQuestItemCount( player, SILVER_NUGGET, NUGGET_COUNT )
                                        && QuestHelper.hasQuestItemCount( player, THONS, THONS_COUNT ) ) {
                                    return this.getPath( '30091-09.html' )
                                }

                                return this.getPath( '30091-10.html' )
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getNoQuestMessagePath()
                }

                break

            case FELTON:
                if ( state.isStarted() ) {
                    switch ( state.getCondition() ) {
                        case 1:
                            return this.getPath( '30879-01.html' )

                        case 2:
                            return this.getPath( '30879-03.html' )
                    }
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}