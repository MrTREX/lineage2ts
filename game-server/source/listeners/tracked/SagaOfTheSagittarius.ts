import { SagaLogic, SagaPlaceholders } from '../helpers/SagaLogic'
import { ILocational, Location } from '../../gameService/models/Location'

const npcIds : Array<number> = [
    30702,
    31627,
    31604,
    31640,
    31633,
    31646,
    31647,
    31650,
    31654,
    31655,
    31657,
    31641
]

const itemIds : Array<number> = [
    7080,
    7519,
    7081,
    7497,
    7280,
    7311,
    7342,
    7373,
    7404,
    7435,
    7105,
    0
]

const mobIds : Array<number> = [
    27296,
    27231,
    27305
]

const spawnLocations : Array<ILocational> = [
    new Location( 191046, -40640, -3042 ),
    new Location( 46066, -36396, -1685 ),
    new Location( 46066, -36396, -1685 )
]

export class SagaOfTheSagittarius extends SagaLogic {
    constructor() {
        super( 'Q00082_SagaOfTheSagittarius', 'listeners/tracked-100/SagaOfTheSagittarius.ts' )
        this.questId = 82
    }

    getAllItemIds(): Array<number> {
        return itemIds
    }

    getAllMobIds(): Array<number> {
        return mobIds
    }

    getAllNpcIds(): Array<number> {
        return npcIds
    }

    getAllSpawnLocations(): Array<ILocational> {
        return spawnLocations
    }

    getClassId(): number {
        return 92
    }

    getPreviousClassId(): number {
        return 0x09
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00082_SagaOfTheSagittarius'
    }
}