import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2World } from '../../gameService/L2World'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'

const HIGH_PRIEST_INNOCENTIN = 31328
const TRADER_VIOLET = 31386
const TOMBSTONE = 31523
const GHOST_OF_VON_HELLMANN = 31524
const BROKEN_BOOKSHELF = 31526
const BOX = 31530

const LIDIAS_DIARY = 7064
const SILVER_KEY = 7149
const SILVER_SPEAR = 7150

const MAP_FOREST_OF_THE_DEAD = 7063
const LIDIAS_HAIRPIN = 7148
const minimumLevel = 64
const GHOST_SPAWN = new Location( 51432, -54570, -3136 )

export class LidiasHeart extends ListenerLogic {
    constructor() {
        super( 'Q00023_LidiasHeart', 'listeners/tracked/LidiasHeart.ts' )
        this.questId = 23
        this.questItemIds = [ LIDIAS_DIARY, SILVER_KEY, SILVER_SPEAR ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00023_LidiasHeart'
    }

    getQuestStartIds(): Array<number> {
        return [ HIGH_PRIEST_INNOCENTIN ]
    }

    getSpawnIds(): Array<number> {
        return [ GHOST_OF_VON_HELLMANN ]
    }

    getTalkIds(): Array<number> {
        return [ HIGH_PRIEST_INNOCENTIN, TRADER_VIOLET, TOMBSTONE, GHOST_OF_VON_HELLMANN, BROKEN_BOOKSHELF, BOX ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        if ( data.eventName === 'despawn' ) {
            let objectId: number = NpcVariablesManager.get( data.characterId, 'npcObjectId' ) as number
            if ( objectId > 0 ) {
                NpcVariablesManager.set( objectId, 'spawned', true )
            }

            let npc = L2World.getObjectById( data.characterId ) as L2Npc
            await npc.deleteMe()

            return
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '31328-02.htm' )
                }

                if ( !QuestHelper.hasQuestItems( player, MAP_FOREST_OF_THE_DEAD ) ) {
                    await QuestHelper.giveSingleItem( player, MAP_FOREST_OF_THE_DEAD, 1 )
                }

                await QuestHelper.giveSingleItem( player, SILVER_KEY, 1 )
                state.startQuest()
                state.setMemoState( 1 )

                return this.getPath( '31328-03.htm' )

            case '31328-05.html':
            case '31328-06.html':
            case '31328-10.html':
            case '31328-11.html':
            case '31328-16.html':
            case '31328-17.html':
            case '31328-18.html':
            case '31524-03.html':
            case '31526-04.html':
            case '31526-05.html':
            case '31526-07a.html':
            case '31526-09.html':
                return this.getPath( data.eventName )

            case '31328-07.html':
                if ( state.isMemoState( 1 ) ) {
                    state.setMemoState( 2 )
                    state.setConditionWithSound( 2, true )
                    return this.getPath( data.eventName )
                }

                return

            case '31328-12.html':
                if ( state.isMemoState( 5 ) || state.isMemoState( 6 ) ) {
                    state.setMemoState( 6 )
                    state.setConditionWithSound( 5, false )
                    return this.getPath( data.eventName )
                }

                return

            case '31328-13.html':
                if ( state.isMemoState( 5 ) || state.isMemoState( 6 ) ) {
                    state.setMemoState( 7 )
                    return this.getPath( data.eventName )
                }

                return

            case '31328-19.html':
                player.sendCopyData( SoundPacket.AMBSOUND_MT_CREAK )
                return this.getPath( data.eventName )

            case '31328-20.html':
                if ( state.isMemoState( 7 ) ) {
                    state.setMemoState( 8 )
                    state.setConditionWithSound( 6, false )
                    return this.getPath( data.eventName )
                }

                return

            case '31328-21.html':
                state.setConditionWithSound( 5, false )
                return this.getPath( data.eventName )

            case '31523-02.html':
                if ( state.isMemoState( 8 ) || state.isMemoState( 9 ) ) {
                    player.sendCopyData( SoundPacket.SKILLSOUND_HORROR_02 )

                    if ( !NpcVariablesManager.get( data.characterId, 'spawned' ) ) {
                        NpcVariablesManager.set( data.characterId, 'spawned', true )

                        let npc = L2World.getObjectById( data.characterId ) as L2Npc
                        let ghost: L2Npc = QuestHelper.addSummonedSpawnAtLocation( npc, GHOST_OF_VON_HELLMANN, GHOST_SPAWN, false, 0 )
                        NpcVariablesManager.set( ghost.getObjectId(), 'npcObjectId', data.characterId )
                        return this.getPath( data.eventName )
                    }

                    return this.getPath( '31523-03.html' )
                }

                return

            case '31523-06.html':
                if ( state.isMemoState( 9 ) ) {
                    await QuestHelper.giveSingleItem( player, SILVER_KEY, 1 )
                    state.setMemoState( 10 )
                    state.setConditionWithSound( 8, false )

                    return this.getPath( data.eventName )
                }

                return

            case '31524-02.html':
                player.sendCopyData( SoundPacket.CHRSOUND_MHFIGHTER_CRY )
                return this.getPath( data.eventName )

            case '31524-04.html':
                if ( state.isMemoState( 8 ) ) {
                    await QuestHelper.takeSingleItem( player, LIDIAS_DIARY, 1 )
                    state.setMemoState( 9 )
                    state.setConditionWithSound( 7, false )
                    return this.getPath( data.eventName )
                }

                return

            case '31526-02.html':
                if ( state.isMemoState( 2 ) && QuestHelper.hasQuestItem( player, SILVER_KEY ) ) {
                    await QuestHelper.takeSingleItem( player, SILVER_KEY, -1 )
                    state.setMemoState( 3 )
                    return this.getPath( data.eventName )
                }

                return

            case '31526-06.html':
                if ( !QuestHelper.hasQuestItem( player, LIDIAS_HAIRPIN ) ) {
                    await QuestHelper.giveSingleItem( player, LIDIAS_HAIRPIN, 1 )
                }

                state.setMemoState( state.getMemoState() + 1 )

                if ( QuestHelper.hasQuestItem( player, LIDIAS_DIARY ) ) {
                    state.setConditionWithSound( 4, false )
                }

                return this.getPath( data.eventName )

            case '31526-08.html':
                player.sendCopyData( SoundPacket.ITEMSOUND_ARMOR_LEATHER )
                return this.getPath( data.eventName )

            case '31526-10.html':
                player.sendCopyData( SoundPacket.AMBSOUND_EG_DRON )
                return this.getPath( data.eventName )

            case '31526-11.html':
                await QuestHelper.giveSingleItem( player, LIDIAS_DIARY, 1 )
                state.setMemoState( state.getMemoState() + 1 )

                if ( QuestHelper.hasQuestItem( player, LIDIAS_HAIRPIN ) ) {
                    state.setConditionWithSound( 4, false )
                }

                return this.getPath( data.eventName )

            case '31530-02.html':
                if ( state.isMemoState( 11 ) && QuestHelper.hasQuestItem( player, SILVER_KEY ) ) {
                    await QuestHelper.giveSingleItem( player, SILVER_SPEAR, 1 )
                    await QuestHelper.takeSingleItem( player, SILVER_KEY, -1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_WEAPON_SPEAR )
                    state.setConditionWithSound( 10, false )
                    return this.getPath( data.eventName )
                }

                return
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name, true )

        if ( state.isCreated() ) {
            if ( data.characterNpcId === HIGH_PRIEST_INNOCENTIN ) {
                let companionState = QuestStateCache.getQuestState( data.playerId, 'Q00022_TragedyInVonHellmannForest' )
                if ( companionState && companionState.isCompleted() ) {
                    return this.getPath( '31328-01.htm' )
                }

                return this.getPath( '31328-01a.html' )
            }

            return QuestHelper.getNoQuestMessagePath()
        }

        if ( state.isStarted() ) {
            let player = L2World.getPlayer( data.playerId )

            switch ( data.characterNpcId ) {
                case HIGH_PRIEST_INNOCENTIN:
                    switch ( state.getMemoState() ) {
                        case 1:
                            return this.getPath( '31328-04.html' )
                        case 2:
                            return this.getPath( '31328-08.html' )
                        case 5:
                            return this.getPath( '31328-09.html' )
                        case 6:
                            return this.getPath( '31328-14.html' )
                        case 7:
                            return this.getPath( '31328-15.html' )
                        case 8:
                            state.setConditionWithSound( 6, true )
                            return this.getPath( '31328-22.html' )
                    }

                    break

                case TRADER_VIOLET:
                    switch ( state.getMemoState() ) {
                        case 10:
                            if ( QuestHelper.hasQuestItem( player, SILVER_KEY ) ) {
                                state.setMemoState( 11 )
                                state.setConditionWithSound( 9, true )

                                return this.getPath( '31386-01.html' )
                            }

                            break
                        case 11:
                            if ( !QuestHelper.hasQuestItem( player, SILVER_SPEAR ) ) {
                                return this.getPath( '31386-02.html' )
                            }

                            await QuestHelper.giveAdena( player, 350000, true )
                            await QuestHelper.addExpAndSp( player, 456893, 42112 )
                            await state.exitQuest( false, true )

                            return this.getPath( '31386-03.html' )
                    }

                    break

                case TOMBSTONE:
                    switch ( state.getMemoState() ) {
                        case 8:
                            return this.getPath( '31523-01.html' )
                        case 9:
                            return this.getPath( '31523-04.html' )
                        case 10:
                            return this.getPath( '31523-05.html' )
                    }

                    break

                case GHOST_OF_VON_HELLMANN:
                    switch ( state.getMemoState() ) {
                        case 8:
                            return this.getPath( '31524-01.html' )

                        case 9:
                            if ( !QuestHelper.hasQuestItem( player, SILVER_KEY ) ) {
                                return this.getPath( '31524-05.html' )
                            }

                            break

                        case 10:
                            if ( QuestHelper.hasQuestItem( player, SILVER_KEY ) ) {
                                state.setMemoState( 10 )
                                return this.getPath( '31524-06.html' )
                            }

                            break
                    }

                    break

                case BROKEN_BOOKSHELF:
                    switch ( state.getMemoState() ) {
                        case 2:
                            if ( QuestHelper.hasQuestItem( player, SILVER_KEY ) ) {
                                state.setConditionWithSound( 3, true )
                                return this.getPath( '31526-01.html' )
                            }

                            break

                        case 3:
                            return this.getPath( '31526-03.html' )

                        case 4:
                            if ( QuestHelper.hasQuestItem( player, LIDIAS_HAIRPIN ) ) {
                                return this.getPath( '31526-07.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, LIDIAS_DIARY ) ) {
                                return this.getPath( '31526-12.html' )
                            }

                            break

                        case 5:
                            if ( QuestHelper.hasQuestItems( player, LIDIAS_HAIRPIN, LIDIAS_DIARY ) ) {
                                return this.getPath( '31526-13.html' )
                            }

                            break

                    }

                    break

                case BOX: {
                    if ( state.getMemoState() === 11 ) {
                        if ( QuestHelper.hasQuestItem( player, SILVER_KEY ) ) {
                            return this.getPath( '31530-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, SILVER_SPEAR ) ) {
                            return this.getPath( '31530-03.html' )
                        }
                    }

                    break
                }
            }
        }

        if ( state.isCompleted() ) {
            switch ( data.characterNpcId ) {
                case HIGH_PRIEST_INNOCENTIN:
                    return QuestHelper.getAlreadyCompletedMessagePath()

                case TRADER_VIOLET:
                    let companionState = QuestStateCache.getQuestState( data.playerId, 'Q00024_InhabitantsOfTheForestOfTheDead' )
                    if ( !companionState ) {
                        return this.getPath( '31386-04.html' )
                    }
            }
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}