import { SagaLogic } from '../helpers/SagaLogic'
import { ILocational, Location } from '../../gameService/models/Location'

const npcIds: Array<number> = [
    31582,
    31623,
    31297,
    31297,
    31599,
    31646,
    31647,
    31653,
    31654,
    31655,
    31656,
    31297,
]

const itemIds: Array<number> = [
    7080,
    7532,
    7081,
    7510,
    7293,
    7324,
    7355,
    7386,
    7417,
    7448,
    7086,
    0,
]

const mobIds: Array<number> = [
    27258,
    27244,
    27263,
]

const spawnLocations: Array<ILocational> = [
    new Location( 164650, -74121, -2871 ),
    new Location( 47391, -56929, -2370 ),
    new Location( 47429, -56923, -2383 ),
]

export class SagaOfTheHellKnight extends SagaLogic {
    constructor() {
        super( 'Q00095_SagaOfTheHellKnight', 'listeners/tracked/SagaOfTheHellKnight.ts' )
        this.questId = 95
    }

    getAllItemIds(): Array<number> {
        return itemIds
    }

    getAllMobIds(): Array<number> {
        return mobIds
    }

    getAllNpcIds(): Array<number> {
        return npcIds
    }

    getAllSpawnLocations(): Array<ILocational> {
        return spawnLocations
    }

    getClassId(): number {
        return 91
    }

    getPreviousClassId(): number {
        return 0x06
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00095_SagaOfTheHellKnight'
    }
}