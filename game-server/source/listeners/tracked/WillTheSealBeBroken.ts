import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'

const TALLOTH = 30141

// Monsters
const OMEN_BEAST = 20031
const TAINTED_ZOMBIE = 20041
const STINK_ZOMBIE = 20046
const LESSER_SUCCUBUS = 20048
const LESSER_SUCCUBUS_TUREN = 20052
const LESSER_SUCCUBUS_TILFO = 20057

// Items
const OMEN_BEAST_EYE = 1081
const TAINT_STONE = 1082
const SUCCUBUS_BLOOD = 1083
const ENCHANT = 956

const minimumLevel = 16

export class WillTheSealBeBroken extends ListenerLogic {
    constructor() {
        super( 'Q00003_WillTheSealBeBroken', 'listeners/tracked/WillTheSealBeBroken.ts' )
        this.questItemIds = [ OMEN_BEAST_EYE, TAINT_STONE, SUCCUBUS_BLOOD ]
        this.questId = 3
    }

    getAttackableKillIds(): Array<number> {
        return [ OMEN_BEAST, TAINTED_ZOMBIE, STINK_ZOMBIE, LESSER_SUCCUBUS, LESSER_SUCCUBUS_TILFO, LESSER_SUCCUBUS_TUREN ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00003_WillTheSealBeBroken'
    }

    getQuestStartIds(): Array<number> {
        return [ TALLOTH ]
    }

    getTalkIds(): Array<number> {
        return [ TALLOTH ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player: L2PcInstance = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 1 )
        if ( !player ) {
            return
        }

        let state: QuestState = this.getQuestState( player.getObjectId(), false )
        switch ( data.npcId ) {
            case OMEN_BEAST:
                await this.giveItems( player, state, OMEN_BEAST_EYE )
                break

            case STINK_ZOMBIE:
            case TAINTED_ZOMBIE:
                await this.giveItems( player, state, TAINT_STONE )
                break

            case LESSER_SUCCUBUS:
            case LESSER_SUCCUBUS_TILFO:
            case LESSER_SUCCUBUS_TUREN:
                await this.giveItems( player, state, SUCCUBUS_BLOOD )
                break
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, false )
        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30141-03.htm':
                state.startQuest()
                break

            case '30141-05.html':
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let state: QuestState = this.getQuestState( data.playerId, true )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getRace() !== Race.DARK_ELF ) {
                    return this.getPath( '30141-00.htm' )
                }

                return this.getPath( player.getLevel() >= minimumLevel ? '30141-02.htm' : '30141-01.html' )

            case QuestStateValues.STARTED:
                if ( state.isCondition( 1 ) ) {
                    return this.getPath( '30141-04.html' )
                }

                await QuestHelper.giveSingleItem( player, ENCHANT, 1 )
                await state.exitQuest( false, true )

                return this.getPath( '30141-06.html' )
            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }
    }

    async giveItems( player: L2PcInstance, state: QuestState, itemId: number ): Promise<void> {
        if ( !QuestHelper.hasQuestItems( player, itemId ) ) {
            await QuestHelper.giveSingleItem( player, itemId, 1 )

            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

            if ( QuestHelper.hasQuestItems( player, ...this.questItemIds ) ) {
                state.setConditionWithSound( 2, true )
            }
        }
    }
}