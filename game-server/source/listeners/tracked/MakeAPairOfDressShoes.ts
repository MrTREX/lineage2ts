import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const IAN = 30164
const WOODLEY = 30838
const LEIKAR = 31520

const LEATHER = 1882
const THREAD = 1868
const DRESS_SHOES_BOX = 7113

const minimumLevel = 60
const LEATHER_COUNT = 200
const THREAD_COUNT = 600
const ADENA_COUNT = 500000
const ADENA_COUNT2 = 200000
const ADENA_COUNT3 = 300000

export class MakeAPairOfDressShoes extends ListenerLogic {
    constructor() {
        super( 'Q00033_MakeAPairOfDressShoes', 'listeners/tracked/MakeAPairOfDressShoes.ts' )
        this.questId = 33
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00033_MakeAPairOfDressShoes'
    }

    getQuestStartIds(): Array<number> {
        return [ WOODLEY ]
    }

    getTalkIds(): Array<number> {
        return [ WOODLEY, IAN, LEIKAR ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30838-03.htm':
                state.startQuest()
                break

            case '30838-06.html':
                state.setConditionWithSound( 3, true )
                break

            case '30838-09.html':
                if ( QuestHelper.hasQuestItemCount( player, LEATHER, LEATHER_COUNT )
                        && QuestHelper.hasQuestItemCount( player, THREAD, THREAD_COUNT )
                        && player.getAdena() >= ADENA_COUNT2 ) {

                    await QuestHelper.takeSingleItem( player, LEATHER, LEATHER_COUNT )
                    await QuestHelper.takeSingleItem( player, THREAD, LEATHER_COUNT )
                    await player.reduceAdena( ADENA_COUNT2, true, 'quest:MakeAPairOfDressShoes' )
                    state.setConditionWithSound( 4, true )

                    break
                }

                return this.getPath( '30838-10.html' )

            case '30838-13.html':
                await QuestHelper.giveSingleItem( player, DRESS_SHOES_BOX, 1 )
                await state.exitQuest( false, true )
                break

            case '31520-02.html':
                state.setConditionWithSound( 2, true )
                break

            case '30164-02.html':
                if ( player.getAdena() < ADENA_COUNT3 ) {
                    return this.getPath( '30164-03.html' )
                }

                await player.reduceAdena( ADENA_COUNT3, true, 'quest:MakeAPairOfDressShoes' )
                state.setConditionWithSound( 5, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case WOODLEY:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() >= minimumLevel ? '30838-01.htm' : '30838-02.html' )

                    case QuestStateValues.STARTED:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '30838-04.html' )

                            case 2:
                                return this.getPath( '30838-05.html' )

                            case 3:
                                if ( QuestHelper.hasQuestItemCount( player, LEATHER, LEATHER_COUNT )
                                        && QuestHelper.hasQuestItemCount( player, THREAD, THREAD_COUNT )
                                        && player.getAdena() >= ADENA_COUNT ) {
                                    return this.getPath( '30838-07.html' )
                                }

                                return this.getPath( '30838-08.html' )

                            case 4:
                                return this.getPath( '30838-11.html' )

                            case 5:
                                return this.getPath( '30838-12.html' )
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case LEIKAR:
                if ( state.isStarted() ) {
                    switch ( state.getCondition() ) {
                        case 1:
                            return this.getPath( '31520-01.html' )

                        case 2:
                            return this.getPath( '31520-03.html' )
                    }
                }

                break

            case IAN:
                if ( state.isStarted() ) {
                    switch ( state.getCondition() ) {
                        case 4:
                            return this.getPath( '30164-01.html' )

                        case 5:
                            return this.getPath( '30164-04.html' )
                    }
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}