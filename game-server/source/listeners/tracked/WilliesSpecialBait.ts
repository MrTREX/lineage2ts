import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const WILLIE = 31574
const TARLK_BASILISK = 20573

const TARLK_EYE = 7623
const EARTH_FISHING_LURE = 7612
const minimumLevel = 48

export class WilliesSpecialBait extends ListenerLogic {
    constructor() {
        super( 'Q00052_WilliesSpecialBait', 'listeners/tracked/WilliesSpecialBait.ts' )
        this.questId = 52
        this.questItemIds = [ TARLK_EYE ]
    }

    getAttackableKillIds(): Array<number> {
        return [ TARLK_BASILISK ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00052_WilliesSpecialBait'
    }

    getQuestStartIds(): Array<number> {
        return [ WILLIE ]
    }

    getTalkIds(): Array<number> {
        return [ WILLIE ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player: L2PcInstance = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 1 )

        if ( !player ) {
            return
        }

        if ( QuestHelper.getQuestItemsCount( player, TARLK_EYE ) < 100
                && _.random( 100 ) < QuestHelper.getAdjustedChance( TARLK_EYE, 33, data.isChampion ) ) {
            await QuestHelper.rewardSingleQuestItem( player, TARLK_EYE, 1, data.isChampion )
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        }

        if ( QuestHelper.getQuestItemsCount( player, TARLK_EYE ) >= 100 ) {
            let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), this.name )
            state.setConditionWithSound( 2, true )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )
        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '31574-03.htm':
                state.startQuest()
                break

            case '31574-07.html':
                let player = L2World.getPlayer( data.playerId )
                if ( state.isCondition( 2 ) && QuestHelper.getQuestItemsCount( player, TARLK_EYE ) >= 100 ) {
                    await QuestHelper.rewardSingleItem( player, EARTH_FISHING_LURE, 4 )
                    await state.exitQuest( false, true )

                    return this.getPath( '31574-06.htm' )
                }

                break
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name, true )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                let player = L2World.getPlayer( data.playerId )
                return this.getPath( player.getLevel() >= minimumLevel ? '31574-01.htm' : '31574-02.html' )

            case QuestStateValues.STARTED:
                return this.getPath( state.isCondition( 1 ) ? '31574-05.html' : '31574-04.html' )

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}