import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const OFULLE = 31572
const KIKI = 31442

const treasureBox = 6503
const letter = 7626
const ring = 881
const minimumLevel = 36

export class ChestCaughtWithABaitOfIcyAir extends ListenerLogic {
    constructor() {
        super( 'Q00028_ChestCaughtWithABaitOfIcyAir', 'listeners/tracked/ChestCaughtWithABaitOfIcyAir.ts' )
        this.questId = 28
        this.questItemIds = [ treasureBox, letter ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00028_ChestCaughtWithABaitOfIcyAir'
    }

    getQuestStartIds(): Array<number> {
        return [ OFULLE ]
    }

    getTalkIds(): Array<number> {
        return [ OFULLE, KIKI ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31572-04.htm':
                state.startQuest()
                break

            case '31572-08.htm':
                if ( state.isCondition( 1 ) && QuestHelper.hasQuestItem( player, treasureBox ) ) {
                    await QuestHelper.giveSingleItem( player, letter, 1 )
                    await QuestHelper.takeSingleItem( player, treasureBox, 1 )

                    state.setConditionWithSound( 2, true )
                    return this.getPath( '31572-07.htm' )
                }

                break

            case '31442-03.htm':
                if ( state.isCondition( 2 ) && QuestHelper.hasQuestItem( player, letter ) ) {
                    await QuestHelper.rewardSingleItem( player, ring, 1 )
                    await state.exitQuest( false, true )

                    return this.getPath( '31442-02.htm' )
                }

                break
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()

            case QuestStateValues.CREATED:
                if ( data.characterNpcId === OFULLE ) {
                    if ( ( player.getLevel() >= minimumLevel ) && player.hasQuestCompleted( 'Q00051_OFullesSpecialBait' ) ) {
                        return this.getPath( '31572-01.htm' )
                    }

                    return this.getPath( '31572-02.htm' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case OFULLE:
                        switch ( state.getCondition() ) {
                            case 1:
                                if ( QuestHelper.hasQuestItem( player, treasureBox ) ) {
                                    return this.getPath( '31572-05.htm' )
                                }

                                return this.getPath( '31572-06.htm' )

                            case 2:
                                return this.getPath( '31572-09.htm' )
                        }

                        break

                    case KIKI:
                        if ( state.isCondition( 2 ) ) {
                            return this.getPath( '31442-01.htm' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}