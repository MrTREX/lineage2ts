import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'
import { createItemDefinition } from '../../gameService/interface/ItemDefinition'

const IRIS = 30034
const MAGISTER_ROHMER = 30344
const GUARD_LUIS = 30386

const LIZARDMAN_SENTINEL = 21100
const LIZARDMAN_SHAMAN = 21101
const LIZARDMAN_LEADER = 20356
const LIZARDMAN_SUB_LEADER = 20357

const FEATHER = createItemDefinition( 7173, 100 )
const TOTEM_TOOTH_1ST = 7174
const TOTEM_TOOTH_2ND = createItemDefinition( 7175, 50 )
const LETTER_1ST = 7176
const LETTER_2ND = 7177

const BONE_HELMET = 45
const LEATHER_GAUNTLET = 605
const ASPIS = 627
const BLUE_BUCKSKIN_BOOTS = 1123

const minimumLevel = 19

export class DragonFangs extends ListenerLogic {
    constructor() {
        super( 'Q00038_DragonFangs', 'listeners/tracked/DragonFangs.ts' )
        this.questId = 38
        this.questItemIds = [ FEATHER.id, TOTEM_TOOTH_1ST, TOTEM_TOOTH_2ND.id, LETTER_1ST, LETTER_2ND ]
    }

    getAttackableKillIds(): Array<number> {
        return [ LIZARDMAN_SENTINEL, LIZARDMAN_SHAMAN, LIZARDMAN_LEADER, LIZARDMAN_SUB_LEADER ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00038_DragonFangs'
    }

    getQuestStartIds(): Array<number> {
        return [ GUARD_LUIS ]
    }

    getTalkIds(): Array<number> {
        return [ GUARD_LUIS, IRIS, MAGISTER_ROHMER ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        switch ( data.npcId ) {
            case LIZARDMAN_SUB_LEADER:
            case LIZARDMAN_SENTINEL:
                let featherState: QuestState = this.getRandomPartyMemberState( player, 1, 3, npc )
                if ( featherState ) {
                    await QuestHelper.rewardAndProgressState( featherState.getPlayer(), featherState, FEATHER.id, 1, FEATHER.count, data.isChampion, 2 )
                }

                return

            case LIZARDMAN_LEADER:
            case LIZARDMAN_SHAMAN: {
                let toothState: QuestState = this.getRandomPartyMemberState( player, 6, 3, npc )
                if ( toothState && Math.random() <= QuestHelper.getAdjustedChance( TOTEM_TOOTH_2ND.id, 0.5, data.isChampion ) ) {
                    await QuestHelper.rewardAndProgressState( toothState.getPlayer(), toothState, TOTEM_TOOTH_2ND.id, 1, TOTEM_TOOTH_2ND.count, data.isChampion, 7 )
                }

                return
            }
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30386-03.htm':
                if ( state.isCreated() ) {
                    state.startQuest()
                }

                break

            case '30386-06.html':
                if ( state.isCondition( 2 ) ) {
                    if ( QuestHelper.hasItem( player, FEATHER ) ) {
                        state.setConditionWithSound( 3, true )

                        await QuestHelper.takeSingleItem( player, FEATHER.id, FEATHER.count )
                        await QuestHelper.giveSingleItem( player, TOTEM_TOOTH_1ST, 1 )

                        break
                    }

                    return this.getPath( '30386-07.html' )
                }

                break

            case '30034-02.html':
                if ( state.isCondition( 3 ) ) {
                    if ( QuestHelper.hasQuestItem( player, TOTEM_TOOTH_1ST ) ) {
                        state.setConditionWithSound( 4, true )

                        await QuestHelper.takeSingleItem( player, TOTEM_TOOTH_1ST, 1 )
                        await QuestHelper.giveSingleItem( player, LETTER_1ST, 1 )

                        break
                    }

                    return this.getPath( '30034-03.html' )
                }

                break

            case '30034-06.html':
                if ( state.isCondition( 5 ) ) {
                    if ( QuestHelper.hasQuestItem( player, LETTER_2ND ) ) {
                        state.setConditionWithSound( 6, true )

                        await QuestHelper.takeSingleItem( player, LETTER_2ND, 1 )

                        break
                    }

                    return this.getPath( '30034-07.html' )
                }

                break

            case '30034-10.html': {
                if ( state.isCondition( 7 ) ) {
                    if ( QuestHelper.hasItem( player, TOTEM_TOOTH_2ND ) ) {
                        await QuestHelper.addExpAndSp( player, 435117, 23977 )

                        let chance = _.random( 100 )
                        let itemId: number = 0
                        let adenaAmount: number = 0

                        if ( chance < 25 ) {
                            itemId = BONE_HELMET
                            adenaAmount = 5200
                        } else if ( chance < 50 ) {
                            itemId = ASPIS
                            adenaAmount = 1500
                        } else if ( chance < 75 ) {
                            itemId = BLUE_BUCKSKIN_BOOTS
                            adenaAmount = 3200
                        } else {
                            itemId = LEATHER_GAUNTLET
                            adenaAmount = 3200
                        }

                        await QuestHelper.rewardSingleItem( player, itemId, 1 )
                        await QuestHelper.giveAdena( player, adenaAmount, true )
                        await state.exitQuest( false, true )

                        break
                    }

                    return this.getPath( '30034-11.html' )
                }
                break
            }
            case '30344-02.html':
                if ( state.isCondition( 4 ) ) {
                    if ( QuestHelper.hasQuestItem( player, LETTER_1ST ) ) {
                        state.setConditionWithSound( 5, true )

                        await QuestHelper.takeSingleItem( player, LETTER_1ST, 1 )
                        await QuestHelper.giveSingleItem( player, LETTER_2ND, 1 )

                        break
                    }

                    return this.getPath( '30344-03.html' )
                }

                break
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case IRIS:
                switch ( state.getCondition() ) {
                    case 3:
                        return this.getPath( '30034-01.html' )

                    case 4:
                        return this.getPath( '30034-04.html' )

                    case 5:
                        return this.getPath( '30034-05.html' )

                    case 6:
                        return this.getPath( '30034-09.html' )

                    case 7:
                        if ( QuestHelper.hasItem( player, TOTEM_TOOTH_2ND ) ) {
                            return this.getPath( '30034-08.html' )
                        }

                        break

                }

                break

            case MAGISTER_ROHMER:
                if ( state.isCondition( 4 ) ) {
                    return this.getPath( '30344-01.html' )
                }

                if ( state.isCondition( 5 ) ) {
                    return this.getPath( '30344-04.html' )
                }

                break

            case GUARD_LUIS:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() >= minimumLevel ? '30386-01.htm' : '30386-02.htm' )

                    case QuestStateValues.STARTED:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '30386-05.html' )

                            case 2:
                                if ( QuestHelper.hasItem( player, FEATHER ) ) {
                                    return this.getPath( '30386-04.html' )
                                }

                                break

                            case 3:
                                return this.getPath( '30386-08.html' )
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getNoQuestMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}