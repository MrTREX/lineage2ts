import { SagaLogic } from '../helpers/SagaLogic'
import { ILocational, Location } from '../../gameService/models/Location'

const npcIds: Array<number> = [
    30174,
    31281,
    31614,
    31614,
    31629,
    31646,
    31648,
    31652,
    31654,
    31655,
    31659,
    31614,
]

const itemIds: Array<number> = [
    7080,
    7605,
    7081,
    7507,
    7290,
    7321,
    7352,
    7383,
    7414,
    7445,
    7111,
    0,
]

const mobIds: Array<number> = [
    27314,
    27241,
    27311,
]

const spawnLocations: Array<ILocational> = [
    new Location( 161719, -92823, -1893 ),
    new Location( 124376, 82127, -2796 ),
    new Location( 124355, 82155, -2803 ),
]

export class SagaOfTheElementalMaster extends SagaLogic {
    constructor() {
        super( 'Q00092_SagaOfTheElementalMaster', 'listeners/tracked/SagaOfTheElementalMaster.ts' )
        this.questId = 92
    }

    getAllItemIds(): Array<number> {
        return itemIds
    }

    getAllMobIds(): Array<number> {
        return mobIds
    }

    getAllNpcIds(): Array<number> {
        return npcIds
    }

    getAllSpawnLocations(): Array<ILocational> {
        return spawnLocations
    }

    getClassId(): number {
        return 104
    }

    getPreviousClassId(): number {
        return 0x1c
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00092_SagaOfTheElementalMaster'
    }
}