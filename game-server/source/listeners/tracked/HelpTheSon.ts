import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

const LUNDY = 30827
const DRIKUS = 30505

const MAILLE_GUARD = 20921
const MAILLE_SCOUT = 20920
const MAILLE_LIZARDMAN = 20919

const WORK_HAMMER = 168
const GEMSTONE_FRAGMENT = 7552
const GEMSTONE = 7553
const PET_TICKET = 7585

const minimumLevel = 24

export class HelpTheSon extends ListenerLogic {
    constructor() {
        super( 'Q00044_HelpTheSon', 'listeners/tracked/HelpTheSon.ts' )
        this.questId = 44
        this.questItemIds = [ GEMSTONE, GEMSTONE_FRAGMENT ]
    }

    getAttackableKillIds(): Array<number> {
        return [ MAILLE_GUARD, MAILLE_LIZARDMAN, MAILLE_SCOUT ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00044_HelpTheSon'
    }

    getQuestStartIds(): Array<number> {
        return [ LUNDY ]
    }

    getTalkIds(): Array<number> {
        return [ LUNDY, DRIKUS ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )

        if ( !state || !state.isCondition( 2 ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        await QuestHelper.rewardSingleQuestItem( player, GEMSTONE_FRAGMENT, 1, data.isChampion )

        if ( QuestHelper.getQuestItemsCount( player, GEMSTONE_FRAGMENT ) >= 30 ) {
            state.setConditionWithSound( 3, true )
            return
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30827-01.htm':
                state.startQuest()
                break

            case '30827-03.html':
                if ( QuestHelper.hasQuestItem( player, WORK_HAMMER ) ) {
                    await QuestHelper.takeSingleItem( player, WORK_HAMMER, 1 )
                    state.setConditionWithSound( 2, true )

                    break
                }

                return this.getPath( '30827-03a.html' )

            case '30827-06.html':
                if ( QuestHelper.getQuestItemsCount( player, GEMSTONE_FRAGMENT ) >= 30 ) {
                    await QuestHelper.takeSingleItem( player, GEMSTONE_FRAGMENT, -1 )
                    await QuestHelper.giveSingleItem( player, GEMSTONE, 1 )
                    state.setConditionWithSound( 4, true )

                    break
                }

                return this.getPath( '30827-06a.html' )

            case '30505-02.html':
                if ( QuestHelper.hasQuestItem( player, GEMSTONE ) ) {
                    await QuestHelper.takeSingleItem( player, GEMSTONE, -1 )
                    state.setConditionWithSound( 5, true )

                    break
                }

                return this.getPath( '30505-02a.html' )

            case '30827-09.html':
                await QuestHelper.giveSingleItem( player, PET_TICKET, 1 )
                await state.exitQuest( false, true )
                break
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case LUNDY:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() >= minimumLevel ? '30827-00.htm' : '30827-00a.html' )

                    case QuestStateValues.STARTED:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( QuestHelper.hasQuestItem( player, WORK_HAMMER ) ? '30827-02.html' : '30827-02a.html' )

                            case 2:
                                return this.getPath( '30827-04.html' )

                            case 3:
                                return this.getPath( '30827-05.html' )

                            case 4:
                                return this.getPath( '30827-07.html' )

                            case 5:
                                return this.getPath( '30827-08.html' )
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case DRIKUS:
                if ( state.isStarted() ) {
                    switch ( state.getCondition() ) {
                        case 4:
                            return this.getPath( '30505-01.html' )

                        case 5:
                            return this.getPath( '30505-03.html' )
                    }
                }
                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}