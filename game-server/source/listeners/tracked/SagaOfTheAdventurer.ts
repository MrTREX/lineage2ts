import { SagaLogic, SagaPlaceholders } from '../helpers/SagaLogic'
import { ILocational, Location } from '../../gameService/models/Location'

const npcIds : Array<number> = [
    31603,
    31584,
    31579,
    31615,
    31619,
    31646,
    31647,
    31651,
    31654,
    31655,
    31658,
    31616
]

const itemIds : Array<number> = [
    7080,
    7516,
    7081,
    7494,
    7277,
    7308,
    7339,
    7370,
    7401,
    7432,
    7102,
    0
]

const mobIds : Array<number> = [
    27299,
    27228,
    27302
]

const spawnLocations : Array<ILocational> = [
    new Location( 119518, -28658, -3811 ),
    new Location( 181205, 36676, -4816 ),
    new Location( 181215, 36676, -4812 )
]

export class SagaOfTheAdventurer extends SagaLogic {
    constructor() {
        super( 'Q00079_SagaOfTheAdventurer', 'listeners/tracked-100/SagaOfTheAdventurer.ts' )
        this.questId = 79
    }

    getAllItemIds(): Array<number> {
        return itemIds
    }

    getAllMobIds(): Array<number> {
        return mobIds
    }

    getAllNpcIds(): Array<number> {
        return npcIds
    }

    getAllSpawnLocations(): Array<ILocational> {
        return spawnLocations
    }

    getClassId(): number {
        return 93
    }

    getPreviousClassId(): number {
        return 0x08
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00079_SagaOfTheAdventurer'
    }
}