import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'
import _ from 'lodash'

const MASTER_SHUBAIN = 32194
const MASTER_GWAIN = 32197

const FELIM_LIZARDMAN_HEAD = 9749
const VENOMOUS_SPIDERS_LEG = 9750
const TUMRAN_BUGBEAR_HEART = 9751
const SHUBAINS_RECOMMENDATION = 9752

const GWAINS_RECOMMENDATION = 9753

const FELIM_LIZARDMAN_WARRIOR = 20014
const VENOMOUS_SPIDER = 20038
const TUMRAN_BUGBEAR = 20062

const minimumLevel = 18

export class PathOfTheTrooper extends ListenerLogic {
    constructor() {
        super( 'Q00062_PathOfTheTrooper', 'listeners/tracked/PathOfTheTrooper.ts' )
        this.questId = 62
        this.questItemIds = [ FELIM_LIZARDMAN_HEAD, VENOMOUS_SPIDERS_LEG, TUMRAN_BUGBEAR_HEART, SHUBAINS_RECOMMENDATION ]
    }

    getAttackableKillIds(): Array<number> {
        return [ FELIM_LIZARDMAN_WARRIOR, VENOMOUS_SPIDER, TUMRAN_BUGBEAR ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00062_PathOfTheTrooper'
    }

    getQuestStartIds(): Array<number> {
        return [ MASTER_GWAIN ]
    }

    getTalkIds(): Array<number> {
        return [ MASTER_GWAIN, MASTER_SHUBAIN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, npc, player, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case FELIM_LIZARDMAN_WARRIOR:
                if ( state.isCondition( 2 ) && QuestHelper.getQuestItemsCount( player, FELIM_LIZARDMAN_HEAD ) < 5 ) {
                    await QuestHelper.rewardSingleQuestItem( player, FELIM_LIZARDMAN_HEAD, 1, data.isChampion )

                    if ( QuestHelper.getQuestItemsCount( player, FELIM_LIZARDMAN_HEAD ) >= 5 ) {
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case VENOMOUS_SPIDER:
                if ( state.isCondition( 3 ) && QuestHelper.getQuestItemsCount( player, VENOMOUS_SPIDERS_LEG ) < 10 ) {
                    await QuestHelper.rewardSingleQuestItem( player, VENOMOUS_SPIDERS_LEG, 1, data.isChampion )

                    if ( QuestHelper.getQuestItemsCount( player, VENOMOUS_SPIDERS_LEG ) >= 10 ) {
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case TUMRAN_BUGBEAR:
                if ( state.isCondition( 5 )
                        && !QuestHelper.hasQuestItem( player, TUMRAN_BUGBEAR_HEART )
                        && _.random( 10 ) < QuestHelper.getAdjustedChance( TUMRAN_BUGBEAR_HEART, 5, data.isChampion ) ) {
                    await QuestHelper.giveSingleItem( player, TUMRAN_BUGBEAR_HEART, 1 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                }

                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )
        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( state.isCreated() ) {
                    state.startQuest()
                    state.setMemoState( 1 )

                    return this.getPath( '32197-06.htm' )
                }

                return

            case '32194-02.html':
                if ( state.isCondition( 1 ) ) {
                    state.setMemoState( 2 )
                    state.setConditionWithSound( 2, true )

                    return this.getPath( data.eventName )
                }

                return
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === MASTER_GWAIN ) {
                    if ( player.getRace() === Race.KAMAEL ) {
                        if ( player.getClassId() === ClassIdValues.maleSoldier.id ) {
                            if ( player.getLevel() >= minimumLevel ) {
                                return this.getPath( '32197-01.htm' )
                            }

                            return this.getPath( '32197-02.html' )
                        }

                        return this.getPath( '32197-03.html' )
                    }

                    return this.getPath( '32197-04.html' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case MASTER_GWAIN:
                        switch ( state.getCondition() ) {
                            case 1:
                            case 2:
                            case 3:
                                return this.getPath( '32197-07.html' )

                            case 4:
                                await QuestHelper.takeSingleItem( player, SHUBAINS_RECOMMENDATION, 1 )
                                state.setMemoState( 5 )
                                state.setConditionWithSound( 5, true )
                                return this.getPath( '32197-08.html' )

                            case 5:
                                if ( !QuestHelper.hasQuestItem( player, TUMRAN_BUGBEAR_HEART ) ) {
                                    return this.getPath( '32197-09.html' )
                                }

                                await QuestHelper.giveAdena( player, 163800, true )
                                await QuestHelper.takeSingleItem( player, TUMRAN_BUGBEAR_HEART, 1 )
                                await QuestHelper.giveSingleItem( player, GWAINS_RECOMMENDATION, 1 )

                                let level = player.getLevel()
                                if ( level >= 20 ) {
                                    await QuestHelper.addExpAndSp( player, 320534, 20848 )
                                } else if ( level === 19 ) {
                                    await QuestHelper.addExpAndSp( player, 456128, 27546 )
                                } else {
                                    await QuestHelper.addExpAndSp( player, 591724, 34244 )
                                }

                                await state.exitQuest( false, true )
                                player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )

                                return this.getPath( '32197-10.html' )
                        }

                        break

                    case MASTER_SHUBAIN:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '32194-01.html' )

                            case 2:
                                if ( QuestHelper.getQuestItemsCount( player, FELIM_LIZARDMAN_HEAD ) < 5 ) {
                                    return this.getPath( '32194-03.html' )
                                }

                                await QuestHelper.takeSingleItem( player, FELIM_LIZARDMAN_HEAD, -1 )

                                state.setMemoState( 3 )
                                state.setConditionWithSound( 3, true )

                                return this.getPath( '32194-04.html' )

                            case 3:
                                if ( QuestHelper.getQuestItemsCount( player, VENOMOUS_SPIDERS_LEG ) < 10 ) {
                                    return this.getPath( '32194-05.html' )
                                }

                                await QuestHelper.takeSingleItem( player, VENOMOUS_SPIDERS_LEG, -1 )
                                await QuestHelper.giveSingleItem( player, SHUBAINS_RECOMMENDATION, 1 )

                                state.setMemoState( 4 )
                                state.setConditionWithSound( 4, true )

                                return this.getPath( '32194-06.html' )

                            case 4:
                                return this.getPath( '32194-07.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === MASTER_GWAIN ) {
                    return this.getPath( '32197-05.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}