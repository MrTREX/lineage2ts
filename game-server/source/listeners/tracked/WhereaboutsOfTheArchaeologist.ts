import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const LIESEL = 31263
const GHOST_OF_ADVENTURER = 31538

const LETTER = 7253

export class WhereaboutsOfTheArchaeologist extends ListenerLogic {
    constructor() {
        super( 'Q00014_WhereaboutsOfTheArchaeologist', 'listeners/tracked/WhereaboutsOfTheArchaeologist.ts' )
        this.questId = 14
        this.questItemIds = [ LETTER ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00014_WhereaboutsOfTheArchaeologist'
    }

    getQuestStartIds(): Array<number> {
        return [ LIESEL ]
    }

    getTalkIds(): Array<number> {
        return [ LIESEL, GHOST_OF_ADVENTURER ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let state: QuestState = this.getQuestState( data.playerId, false )
        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '31263-02.html':
                state.startQuest()
                await QuestHelper.giveSingleItem( player, LETTER, 1 )
                break

            case '31538-01.html':
                if ( state.isCondition( 1 ) && QuestHelper.hasQuestItems( player, LETTER ) ) {
                    await QuestHelper.giveAdena( player, 136928, true )
                    await QuestHelper.addExpAndSp( player, 325881, 32524 )
                    await state.exitQuest( false, true )

                    return
                }

                return this.getPath( '31538-02.html' )
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === LIESEL ) {
                    let player = L2World.getPlayer( data.playerId )
                    return this.getPath( player.getLevel() < 74 ? '31263-01.html' : '31263-00.htm' )
                }

                break

            case QuestStateValues.STARTED:
                if ( state.isCondition( 1 ) ) {
                    switch ( data.characterNpcId ) {
                        case LIESEL:
                            return this.getPath( '31263-02.html' )
                        case GHOST_OF_ADVENTURER:
                            return this.getPath( '31538-00.html' )
                    }
                }

                break

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}