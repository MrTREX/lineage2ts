import { SagaLogic, SagaPlaceholders } from '../helpers/SagaLogic'
import { ILocational, Location } from '../../gameService/models/Location'

const npcIds: Array<number> = [
    30852,
    31624,
    31278,
    30852,
    31638,
    31646,
    31648,
    31651,
    31654,
    31655,
    31658,
    31281,
]

const itemIds: Array<number> = [
    7080,
    7535,
    7081,
    7486,
    7269,
    7300,
    7331,
    7362,
    7393,
    7424,
    7094,
    6482,
]

const mobIds: Array<number> = [
    27287,
    27220,
    27279,
]

const spawnLocations: Array<ILocational> = [
    new Location( 119518, -28658, -3811 ),
    new Location( 181215, 36676, -4812 ),
    new Location( 181227, 36703, -4816 ),
]

export class SagaOfEvasTemplar extends SagaLogic {
    constructor() {
        super( 'Q00071_SagaOfEvasTemplar', 'listeners/tracked-100/SagaOfEvasTemplar.ts' )
        this.questId = 71
    }

    getAllItemIds(): Array<number> {
        return itemIds
    }

    getAllMobIds(): Array<number> {
        return mobIds
    }

    getAllNpcIds(): Array<number> {
        return npcIds
    }

    getAllSpawnLocations(): Array<ILocational> {
        return spawnLocations
    }

    getClassId(): number {
        return 99
    }

    getPreviousClassId(): number {
        return 0x14
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00071_SagaOfEvasTemplar'
    }
}