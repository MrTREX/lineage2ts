import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'

const JASMINE = 30134
const ROSELYN = 30355
const HARNE = 30144

const ROSELYNS_NOTE = 7573
const SCROLL_OF_ESCAPE_GIRAN = 7559
const MARK_OF_TRAVELER = 7570

const minimumLevel = 3

export class AnAdventureBegins extends ListenerLogic {
    constructor() {
        super( 'Q00008_AnAdventureBegins', 'listeners/tracked/AnAdventureBegins.ts' )
        this.questId = 8
        this.questItemIds = [ ROSELYNS_NOTE ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00008_AnAdventureBegins'
    }

    getQuestStartIds(): Array<number> {
        return [ JASMINE ]
    }

    getTalkIds(): Array<number> {
        return [ JASMINE, ROSELYN, HARNE ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state ) {
            return null
        }

        let player = L2World.getPlayer( data.playerId )
        switch ( data.eventName ) {
            case '30134-03.htm':
                state.startQuest()
                break

            case '30134-06.html':
                await QuestHelper.rewardSingleItem( player, SCROLL_OF_ESCAPE_GIRAN, 1 )
                await QuestHelper.giveSingleItem( player, MARK_OF_TRAVELER, 1 )
                await state.exitQuest( false, true )
                break

            case '30355-02.html':
                state.setConditionWithSound( 2, true )
                await QuestHelper.giveSingleItem( player, ROSELYNS_NOTE, 1 )
                break

            case '30144-02.html':
                if ( !QuestHelper.hasQuestItems( player, ROSELYNS_NOTE ) ) {
                    return this.getPath( '30144-03.html' )
                }

                await QuestHelper.takeSingleItem( player, ROSELYNS_NOTE, -1 )
                state.setConditionWithSound( 3, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case JASMINE:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( ( player.getRace() === Race.DARK_ELF ) && ( player.getLevel() >= minimumLevel ) ? '30134-02.htm' : '30134-01.html' )

                    case QuestStateValues.STARTED:
                        if ( state.isCondition( 1 ) ) {
                            return this.getPath( '30134-04.html' )
                        }

                        if ( state.isCondition( 3 ) ) {
                            return this.getPath( '30134-05.html' )
                        }
                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case ROSELYN:
                if ( state.isStarted() ) {
                    if ( state.isCondition( 1 ) ) {
                        return this.getPath( '30355-01.html' )
                    }

                    if ( state.isCondition( 2 ) ) {
                        return this.getPath( '30355-03.html' )
                    }
                }

                break

            case HARNE:
                if ( state.isStarted() ) {
                    if ( state.isCondition( 2 ) ) {
                        return this.getPath( '30144-01.html' )
                    }

                    if ( state.isCondition( 3 ) ) {
                        return this.getPath( '30144-04.html' )
                    }
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}