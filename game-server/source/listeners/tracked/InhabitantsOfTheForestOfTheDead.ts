import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { PacketDispatcher } from '../../gameService/PacketDispatcher'

import _ from 'lodash'

const DORIAN = 31389
const MYSTERIOUS_WIZARD = 31522
const TOMBSTONE = 31531
const LIDIA_MAID = 31532

const LIDIA_LETTER = 7065
const LIDIA_HAIRPIN = 7148
const SUSPICIOUS_TOTEM_DOLL = 7151
const FLOWER_BOUQUET = 7152
const SILVER_CROSS_OF_EINHASAD = 7153
const BROKEN_SILVER_CROSS_OF_EINHASAD = 7154
const TOTEM = 7156

const mobs: Array<number> = [ 21557, 21558, 21560, 21563, 21564, 21565, 21566, 21567 ]
const minimumLevel: number = 65
const variableNames = {
    value: 'v'
}

export class InhabitantsOfTheForestOfTheDead extends ListenerLogic {
    constructor() {
        super( 'Q00024_InhabitantsOfTheForestOfTheDead', 'listeners/tracked/InhabitantsOfTheForestOfTheDead.ts' )
        this.questId = 24
        this.questItemIds = [ LIDIA_LETTER, LIDIA_HAIRPIN, SUSPICIOUS_TOTEM_DOLL, FLOWER_BOUQUET, SILVER_CROSS_OF_EINHASAD, BROKEN_SILVER_CROSS_OF_EINHASAD ]
    }

    getAttackableKillIds(): Array<number> {
        return mobs
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00024_InhabitantsOfTheForestOfTheDead'
    }

    getQuestStartIds(): Array<number> {
        return [ DORIAN ]
    }

    getTalkIds(): Array<number> {
        return [ DORIAN, MYSTERIOUS_WIZARD, TOMBSTONE, LIDIA_MAID ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )
        if ( !state
                || !state.isCondition( 9 )
                || _.random( 100 ) >= QuestHelper.getAdjustedChance( SUSPICIOUS_TOTEM_DOLL, 10, data.isChampion ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        await QuestHelper.giveSingleItem( player, SUSPICIOUS_TOTEM_DOLL, 1 )
        state.setConditionWithSound( 10, true )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31389-02.htm':
                if ( ( player.getLevel() >= minimumLevel ) && player.hasQuestCompleted( 'Q00023_LidiasHeart' ) ) {
                    state.startQuest()
                    await QuestHelper.giveSingleItem( player, FLOWER_BOUQUET, 1 )
                    return this.getPath( '31389-03.htm' )
                }

                break

            case '31389-08.html':
                state.setVariable( variableNames.value, 1 )
                break

            case '31389-13.html':
                await QuestHelper.giveSingleItem( player, SILVER_CROSS_OF_EINHASAD, 1 )
                state.setConditionWithSound( 3, true )
                state.unsetVariable( variableNames.value )
                break

            case '31389-18.html':
                player.sendCopyData( SoundPacket.INTERFACESOUND_CHARSTAT_OPEN )
                break

            case '31389-19.html':
                if ( !QuestHelper.hasQuestItem( player, BROKEN_SILVER_CROSS_OF_EINHASAD ) ) {
                    return QuestHelper.getNoQuestMessagePath()
                }

                await QuestHelper.takeSingleItem( player, BROKEN_SILVER_CROSS_OF_EINHASAD, -1 )
                state.setConditionWithSound( 5, true )
                break

            case '31532-04.html':
                await QuestHelper.giveSingleItem( player, LIDIA_LETTER, 1 )
                state.setConditionWithSound( 6, true )
                break

            case '31532-07.html':
                if ( state.isCondition( 8 ) ) {
                    if ( !QuestHelper.hasQuestItems( player, LIDIA_HAIRPIN, LIDIA_LETTER ) ) {
                        return QuestHelper.getNoQuestMessagePath()
                    }

                    await QuestHelper.takeSingleItem( player, LIDIA_HAIRPIN, -1 )
                    await QuestHelper.takeSingleItem( player, LIDIA_LETTER, -1 )
                    state.setVariable( variableNames.value, 1 )

                    return this.getPath( '31532-06.html' )
                }

                if ( state.isCondition( 6 ) ) {
                    state.setConditionWithSound( 7, true )
                }

                break

            case '31532-10.html':
                state.setVariable( variableNames.value, 2 )
                break

            case '31532-14.html':
                state.setVariable( variableNames.value, 3 )
                break

            case '31532-19.html':
                state.unsetVariable( variableNames.value )
                state.setConditionWithSound( 9, true )
                break

            case '31522-03.html':
                if ( !QuestHelper.hasQuestItem( player, SUSPICIOUS_TOTEM_DOLL ) ) {
                    return QuestHelper.getNoQuestMessagePath()
                }

                await QuestHelper.takeSingleItem( player, SUSPICIOUS_TOTEM_DOLL, 1 )
                state.setVariable( variableNames.value, 1 )
                break

            case '31522-08.html':
                state.unsetVariable( variableNames.value )
                state.setConditionWithSound( 11, true )
                break

            case '31522-17.html':
                state.setVariable( variableNames.value, 1 )
                break

            case '31522-21.html':
                await QuestHelper.giveSingleItem( player, TOTEM, 1 )
                await QuestHelper.addExpAndSp( player, 242105, 22529 )
                await state.exitQuest( false, true )
                break

            case '31531-02.html':
                if ( !QuestHelper.hasQuestItem( player, FLOWER_BOUQUET ) ) {
                    return QuestHelper.getNoQuestMessagePath()
                }

                await QuestHelper.takeSingleItem( player, FLOWER_BOUQUET, -1 )
                state.setConditionWithSound( 2, true )
                break
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name, true )
        switch ( data.characterNpcId ) {
            case DORIAN:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( '31389-01.htm' )

                    case QuestStateValues.STARTED:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '31389-04.html' )

                            case 2:
                                if ( !state.getVariable( variableNames.value ) ) {
                                    return this.getPath( '31389-05.html' )
                                }

                                return this.getPath( '31389-09.html' )

                            case 3:
                                return this.getPath( '31389-14.html' )

                            case 4:
                                return this.getPath( '31389-15.html' )

                            case 5:
                                return this.getPath( '31389-20.html' )

                            case 6:
                            case 8:
                                return this.getPath( '31389-22.html' )

                            case 7:
                                let player = L2World.getPlayer( data.playerId )
                                await QuestHelper.giveSingleItem( player, LIDIA_HAIRPIN, 1 )
                                state.setConditionWithSound( 8, true )

                                return this.getPath( '31389-21.html' )
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case MYSTERIOUS_WIZARD:
                switch ( state.getState() ) {
                    case QuestStateValues.STARTED:
                        if ( state.isCondition( 10 ) ) {
                            return this.getPath( !state.getVariable( variableNames.value ) ? '31522-01.html' : '31522-04.html' )
                        }

                        if ( state.isCondition( 11 ) ) {
                            return this.getPath( !state.getVariable( variableNames.value ) ? '31522-09.html' : '31522-18.html' )
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        let companionState: QuestState = QuestStateCache.getQuestState( data.playerId, 'Q00025_HidingBehindTheTruth' )
                        if ( !companionState ) {
                            return this.getPath( '31522-22.html' )
                        }

                        break
                }

                break

            case TOMBSTONE:
                if ( state.isStarted() ) {
                    if ( state.isCondition( 1 ) ) {
                        PacketDispatcher.sendCopyData( data.playerId, SoundPacket.AMDSOUND_WIND_LOOT )
                        return this.getPath( '31531-01.html' )
                    }

                    if ( state.isCondition( 2 ) ) {
                        return this.getPath( '31531-03.html' )
                    }
                }

                break

            case LIDIA_MAID:
                if ( state.isStarted() ) {
                    switch ( state.getCondition() ) {
                        case 5:
                            return this.getPath( '31532-01.html' )

                        case 6:
                            return this.getPath( '31532-05.html' )

                        case 7:
                            return this.getPath( '31532-07a.html' )

                        case 8:
                            switch ( state.getVariable( variableNames.value ) ) {
                                case 1:
                                    return this.getPath( '31532-08.html' )

                                case 2:
                                    return this.getPath( '31532-11.html' )

                                case 3:
                                    return this.getPath( '31532-15.html' )
                            }

                            return this.getPath( '31532-07a.html' )

                        case 9:
                        case 10:
                            return this.getPath( '31532-20.html' )
                    }
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}