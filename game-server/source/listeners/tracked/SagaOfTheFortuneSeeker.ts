import { SagaLogic } from '../helpers/SagaLogic'
import { ILocational, Location } from '../../gameService/models/Location'

const npcIds: Array<number> = [
    31594,
    31623,
    31600,
    31600,
    31601,
    31646,
    31649,
    31650,
    31654,
    31655,
    31657,
    31600,
]

const itemIds: Array<number> = [
    7080,
    7608,
    7081,
    7514,
    7297,
    7328,
    7359,
    7390,
    7421,
    7452,
    7109,
    0,
]

const mobIds: Array<number> = [
    27259,
    27248,
    27309,
]

const spawnLocations: Array<ILocational> = [
    new Location( 191046, -40640, -3042 ),
    new Location( 46066, -36396, -1685 ),
    new Location( 46087, -36372, -1685 ),
]

export class SagaOfTheFortuneSeeker extends SagaLogic {
    constructor() {
        super( 'Q00099_SagaOfTheFortuneSeeker', 'listeners/tracked/SagaOfTheFortuneSeeker.ts' )
        this.questId = 99
    }

    getAllItemIds(): Array<number> {
        return itemIds
    }

    getAllMobIds(): Array<number> {
        return mobIds
    }

    getAllNpcIds(): Array<number> {
        return npcIds
    }

    getAllSpawnLocations(): Array<ILocational> {
        return spawnLocations
    }

    getClassId(): number {
        return 117
    }

    getPreviousClassId(): number {
        return 0x37
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00099_SagaOfTheFortuneSeeker'
    }
}