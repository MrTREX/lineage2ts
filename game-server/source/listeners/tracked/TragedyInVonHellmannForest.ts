import { ListenerLogic, QuestTimer } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import { AttackableAttackedEvent, AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { AIEffectHelper } from '../../gameService/aicontroller/helpers/AIEffectHelper'
import _ from 'lodash'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const INNOCENTIN = 31328
const TIFAREN = 31334
const WELL = 31527
const GHOST_OF_PRIEST = 31528
const GHOST_OF_ADVENTURER = 31529
const mobsToKill = [
    21553, // Trampled Man
    21554, // Trampled Man
    21555, // Slaughter Executioner
    21556, // Slaughter Executioner
    21561, // Sacrificed Man
]
const SOUL_OF_WELL = 27217

const CROSS_OF_EINHASAD = 7141
const LOST_SKULL_OF_ELF = 7142
const LETTER_OF_INNOCENTIN = 7143
const JEWEL_OF_ADVENTURER_1 = 7144
const JEWEL_OF_ADVENTURER_2 = 7145
const SEALED_REPORT_BOX = 7146
const REPORT_BOX = 7147

const minimumLevel = 63
const priestLocation = new Location( 38354, -49777, -1128 )
const soulWellLocation = new Location( 34706, -54590, -2054 )

export class TragedyInVonHellmannForest extends ListenerLogic {
    tifarenOwnerId: number = 0
    soulWellNpcId: number = 0

    constructor() {
        super( 'Q00022_TragedyInVonHellmannForest', 'listeners/tracked/TragedyInVonHellmannForest.ts' )
        this.questId = 22
        this.questItemIds = [ LOST_SKULL_OF_ELF, CROSS_OF_EINHASAD, REPORT_BOX, JEWEL_OF_ADVENTURER_1, JEWEL_OF_ADVENTURER_2, SEALED_REPORT_BOX ]
    }

    getAttackableAttackIds(): Array<number> {
        return [ SOUL_OF_WELL ]
    }

    getAttackableKillIds(): Array<number> {
        return [ ...mobsToKill, SOUL_OF_WELL ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00022_TragedyInVonHellmannForest'
    }

    getQuestStartIds(): Array<number> {
        return [ TIFAREN ]
    }

    getTalkIds(): Array<number> {
        return [ INNOCENTIN, TIFAREN, WELL, GHOST_OF_PRIEST, GHOST_OF_ADVENTURER ]
    }

    async onAttackableAttackedEvent( data: AttackableAttackedEvent ): Promise<void> {
        let state: QuestState = QuestStateCache.getQuestState( data.attackerPlayerId, this.name )
        if ( !state || !state.isCondition( 10 ) ) {
            return
        }

        let player = L2World.getPlayer( data.attackerPlayerId )
        if ( !player || !QuestHelper.hasQuestItem( player, JEWEL_OF_ADVENTURER_1 ) ) {
            return
        }

        if ( state.isMemoState( 10 ) ) {
            state.setMemoState( 11 )
            return
        }

        if ( NpcVariablesManager.get( data.targetId, this.name ) === 1 ) {
            await QuestHelper.takeSingleItem( player, JEWEL_OF_ADVENTURER_1, -1 )
            await QuestHelper.giveSingleItem( player, JEWEL_OF_ADVENTURER_2, 1 )
            state.setConditionWithSound( 11, true )
        }
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isCondition( 4 ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId )

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        if ( npc.getId() === SOUL_OF_WELL ) {
            this.soulWellNpcId = 0
            return
        }

        if ( QuestHelper.hasQuestItem( player, CROSS_OF_EINHASAD )
                && !QuestHelper.hasQuestItem( player, LOST_SKULL_OF_ELF )
                && _.random( 100 ) < QuestHelper.getAdjustedChance( LOST_SKULL_OF_ELF, 10, data.isChampion ) ) {

            await QuestHelper.giveSingleItem( player, LOST_SKULL_OF_ELF, 1 )
            state.setConditionWithSound( 5, true )
            return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31529-02.html':
            case '31529-04.html':
            case '31529-05.html':
            case '31529-06.html':
            case '31529-07.html':
            case '31529-09.html':
            case '31529-13.html':
            case '31529-13a.html':
            case '31528-02.html':
            case '31528-05.html':
            case '31528-06.html':
            case '31528-07.html':
            case '31328-13.html':
            case '31328-06.html':
            case '31328-05.html':
            case '31328-02.html':
            case '31328-07.html':
            case '31328-08.html':
            case '31328-14.html':
            case '31328-15.html':
            case '31328-16.html':
            case '31328-17.html':
            case '31328-18.html':
            case '31334-12.html':
                return this.getPath( data.eventName )

            case '31334-02.htm':
                if ( state.isCreated() ) {
                    let companionState: QuestState = QuestStateCache.getQuestState( data.playerId, 'Q00021_HiddenTruth' )
                    if ( ( player.getLevel() >= minimumLevel ) && companionState && companionState.isCompleted() ) {
                        return this.getPath( data.eventName )
                    }

                    return this.getPath( '31334-03.html' )
                }

                return

            case '31334-04.html':
                if ( state.isCreated() ) {
                    state.startQuest()
                    return this.getPath( data.eventName )
                }

                return

            case '31334-06.html':
                if ( state.isCondition( 3 ) && QuestHelper.hasQuestItem( player, CROSS_OF_EINHASAD ) ) {
                    return this.getPath( data.eventName )
                }

                state.setConditionWithSound( 2, true )
                return this.getPath( '31334-07.html' )

            case '31334-08.html':
                if ( state.isCondition( 3 ) ) {
                    state.setConditionWithSound( 4, true )
                    return this.getPath( data.eventName )
                }

                return

            case '31334-13.html':
                let conditionValue = state.getCondition()
                if ( ( ( 5 <= conditionValue ) && ( conditionValue <= 7 ) ) && QuestHelper.hasQuestItem( player, CROSS_OF_EINHASAD ) ) {
                    if ( this.tifarenOwnerId === 0 ) {
                        this.tifarenOwnerId = player.getObjectId()

                        let ghost: L2Npc = QuestHelper.addSpawnAtLocation( GHOST_OF_PRIEST, priestLocation, true, 0 )
                        NpcVariablesManager.set( ghost.getObjectId(), this.name, player.getObjectId() )

                        this.startQuestTimer( 'despawnGhost', 1000 * 120, ghost.getObjectId() )
                        BroadcastHelper.broadcastNpcSayStringId( ghost,
                                NpcSayType.NpcAll,
                                NpcStringIds.DID_YOU_CALL_ME_S1,
                                player.getName() )

                        if ( ( ( conditionValue === 5 ) || ( conditionValue === 6 ) ) && QuestHelper.hasQuestItem( player, LOST_SKULL_OF_ELF ) ) {
                            await QuestHelper.takeSingleItem( player, LOST_SKULL_OF_ELF, -1 )
                            state.setConditionWithSound( 7, true )
                        }

                        return this.getPath( data.eventName )
                    }

                    state.setConditionWithSound( 6, true )
                    return this.getPath( '31334-14.html' )
                }

                return

            case '31528-04.html':
                if ( NpcVariablesManager.get( data.characterId, this.name ) === player.getObjectId() ) {
                    player.sendCopyData( SoundPacket.AMBSOUND_HORROR_03 )
                    return this.getPath( data.eventName )
                }

                return

            case '31528-08.html':
                let timer: QuestTimer = this.getQuestTimer( 'despawnGhost', data.characterId, data.playerId )
                if ( timer && NpcVariablesManager.get( data.characterId, this.name ) === player.getObjectId() ) {
                    this.removeQuestTimer( timer )

                    NpcVariablesManager.remove( data.characterId, this.name )
                    this.startQuestTimer( 'despawnGhost', 1000 * 3, data.characterId )
                    state.setConditionWithSound( 8, false )
                    return this.getPath( data.eventName )
                }

                return

            case 'despawnGhost':
                this.tifarenOwnerId = 0

                let despawnNpc: L2Npc = L2World.getObjectById( data.characterId ) as L2Npc
                if ( NpcVariablesManager.has( data.characterId, this.name ) ) {
                    BroadcastHelper.broadcastNpcSayStringId( despawnNpc,
                            NpcSayType.NpcAll,
                            NpcStringIds.IM_CONFUSED_MAYBE_ITS_TIME_TO_GO_BACK )
                }

                await despawnNpc.deleteMe()
                return

            case '31328-03.html':
                if ( state.isCondition( 8 ) ) {
                    await QuestHelper.takeSingleItem( player, CROSS_OF_EINHASAD )
                    return this.getPath( data.eventName )
                }

                return

            case '31328-09.html':
                if ( state.isCondition( 8 ) ) {
                    await QuestHelper.giveSingleItem( player, LETTER_OF_INNOCENTIN, 1 )
                    state.setConditionWithSound( 9, true )
                    return this.getPath( data.eventName )
                }

                return

            case '31328-11.html':
                if ( state.isCondition( 14 ) && QuestHelper.hasQuestItem( player, REPORT_BOX ) ) {
                    await QuestHelper.takeSingleItem( player, REPORT_BOX )
                    state.setConditionWithSound( 15, true )
                    return this.getPath( data.eventName )
                }

                return

            case '31328-19.html':
                if ( state.isCondition( 15 ) ) {
                    state.setConditionWithSound( 16, true )
                    return this.getPath( data.eventName )
                }

                return

            case '31527-02.html':
                if ( state.isCondition( 10 ) && this.soulWellNpcId === 0 ) {
                    let npc: L2Npc = QuestHelper.addSpawnAtLocation( SOUL_OF_WELL, soulWellLocation, true, 0 )

                    this.startQuestTimer( 'activateSoulOfWell', 90000, npc.getObjectId() )
                    this.startQuestTimer( 'despawnSoulOfWell', 120000, npc.getObjectId() )

                    await AIEffectHelper.notifyAttackedWithTargetId( npc, data.playerId, 1, 1 )
                    player.sendCopyData( SoundPacket.SKILLSOUND_ANTARAS_FEAR )

                    return this.getPath( data.eventName )
                }

                return this.getPath( '31527-03.html' )

            case 'activateSoulOfWell':
                NpcVariablesManager.set( data.characterId, this.name, 1 )
                return

            case 'despawnSoulOfWell':
                let despawnSoul: L2Npc = L2World.getObjectById( data.characterId ) as L2Npc
                if ( !despawnSoul.isDead() ) {
                    this.soulWellNpcId = 0
                }

                await despawnSoul.deleteMe()
                return

            case '31529-03.html':
                if ( state.isCondition( 9 ) && QuestHelper.hasQuestItem( player, LETTER_OF_INNOCENTIN ) ) {
                    state.setMemoState( 8 )
                    return this.getPath( data.eventName )
                }

                return

            case '31529-08.html':
                if ( state.isMemoState( 8 ) ) {
                    state.setMemoState( 9 )
                    return this.getPath( data.eventName )
                }

                return

            case '31529-11.html':
                if ( state.isMemoState( 9 ) ) {
                    await QuestHelper.giveSingleItem( player, JEWEL_OF_ADVENTURER_1, 1 )
                    state.setConditionWithSound( 10, true )
                    state.setMemoState( 10 )
                    return this.getPath( data.eventName )
                }

                return
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case TIFAREN:
                switch ( state.getCondition() ) {
                    case 0:
                        if ( state.isCreated() ) {
                            return this.getPath( '31334-01.htm' )
                        }

                        if ( state.isCompleted() ) {
                            return QuestHelper.getAlreadyCompletedMessagePath()
                        }

                        break

                    case 1:
                    case 3:
                        return this.getPath( '31334-05.html' )

                    case 4:
                    case 5:
                        if ( QuestHelper.hasQuestItem( player, CROSS_OF_EINHASAD ) ) {
                            if ( !QuestHelper.hasQuestItem( player, LOST_SKULL_OF_ELF ) ) {
                                return this.getPath( '31334-09.html' )
                            }

                            if ( this.tifarenOwnerId === 0 ) {
                                return this.getPath( '31334-10.html' )
                            }

                            return this.getPath( '31334-11.html' )
                        }

                        break

                    case 6:
                    case 7:
                        if ( QuestHelper.hasQuestItem( player, CROSS_OF_EINHASAD ) ) {
                            if ( this.tifarenOwnerId === 0 ) {
                                return this.getPath( '31334-17.html' )
                            }

                            if ( this.tifarenOwnerId === data.playerId ) {
                                return this.getPath( '31334-15.html' )
                            }

                            state.setConditionWithSound( 6, true )
                            return this.getPath( '31334-16.html' )
                        }

                        break

                    case 8:
                        if ( QuestHelper.hasQuestItem( player, CROSS_OF_EINHASAD ) ) {
                            return this.getPath( '31334-18.html' )
                        }

                        break
                }

                break

            case GHOST_OF_PRIEST:
                player.sendCopyData( SoundPacket.AMBSOUND_HORROR_15 )

                if ( NpcVariablesManager.get( data.characterId, this.name ) === data.playerId ) {
                    return this.getPath( '31528-01.html' )
                }

                return this.getPath( '31528-03.html' )

            case INNOCENTIN:
                switch ( state.getCondition() ) {
                    case 2:
                        if ( !QuestHelper.hasQuestItem( player, CROSS_OF_EINHASAD ) ) {
                            await QuestHelper.giveSingleItem( player, CROSS_OF_EINHASAD, 1 )
                            state.setConditionWithSound( 3, true )
                            return this.getPath( '31328-01.html' )
                        }

                        break

                    case 3:
                        if ( QuestHelper.hasQuestItem( player, CROSS_OF_EINHASAD ) ) {
                            return this.getPath( '31328-01b.html' )
                        }

                        break

                    case 8:
                        if ( QuestHelper.hasQuestItem( player, CROSS_OF_EINHASAD ) ) {
                            return this.getPath( '31328-02.html' )
                        }

                        return this.getPath( '31328-04.html' )

                    case 9:
                        return this.getPath( '31328-09a.html' )

                    case 14:
                        if ( QuestHelper.hasQuestItem( player, REPORT_BOX ) ) {
                            return this.getPath( '31328-10.html' )
                        }

                        break

                    case 15:
                        return this.getPath( '31328-12.html' )

                    case 16:
                        await QuestHelper.addExpAndSp( player, 345966, 31578 )
                        await state.exitQuest( false, true )

                        if ( player.getLevel() >= minimumLevel ) {
                            return this.getPath( '31328-20.html' )
                        }

                        return this.getPath( '31328-21.html' )
                }

                break

            case WELL:
                switch ( state.getCondition() ) {
                    case 10:
                        if ( QuestHelper.hasQuestItem( player, JEWEL_OF_ADVENTURER_1 ) ) {
                            player.sendCopyData( SoundPacket.AMBSOUND_HORROR_01 )
                            return this.getPath( '31527-01.html' )
                        }

                        break

                    case 12:
                        if ( QuestHelper.hasQuestItem( player, JEWEL_OF_ADVENTURER_2 ) && !QuestHelper.hasQuestItem( player, SEALED_REPORT_BOX ) ) {
                            await QuestHelper.giveSingleItem( player, SEALED_REPORT_BOX, 1 )
                            state.setConditionWithSound( 13, true )
                            return this.getPath( '31527-04.html' )
                        }

                        break

                    case 13:
                    case 14:
                    case 15:
                    case 16:
                        return this.getPath( '31527-05.html' )
                }

                break

            case GHOST_OF_ADVENTURER: {
                switch ( state.getCondition() ) {
                    case 9:
                        if ( QuestHelper.hasQuestItem( player, LETTER_OF_INNOCENTIN ) ) {

                            switch ( state.getMemoState() ) {
                                case 0:
                                    return this.getPath( '31529-01.html' )
                                case 8:
                                    return this.getPath( '31529-03a.html' )
                                case 9:
                                    return this.getPath( '31529-10.html' )
                            }
                        }

                        break

                    case 10:
                        if ( QuestHelper.hasQuestItem( player, JEWEL_OF_ADVENTURER_1 ) ) {
                            switch ( state.getMemoState() ) {
                                case 10:
                                    return this.getPath( '31529-12.html' )
                                case 11:
                                    return this.getPath( '31529-14.html' )
                            }
                        }

                        break

                    case 11:
                        if ( QuestHelper.hasQuestItem( player, JEWEL_OF_ADVENTURER_2 ) && !QuestHelper.hasQuestItem( player, SEALED_REPORT_BOX ) ) {
                            state.setConditionWithSound( 12, true )
                            return this.getPath( '31529-15.html' )
                        }

                        break

                    case 13:
                        if ( QuestHelper.hasQuestItem( player, JEWEL_OF_ADVENTURER_2 ) && QuestHelper.hasQuestItem( player, SEALED_REPORT_BOX ) ) {
                            await QuestHelper.giveSingleItem( player, REPORT_BOX, 1 )
                            await QuestHelper.takeSingleItem( player, SEALED_REPORT_BOX, -1 )
                            await QuestHelper.takeSingleItem( player, JEWEL_OF_ADVENTURER_2, -1 )

                            state.setConditionWithSound( 14, true )
                            return this.getPath( '31529-16.html' )
                        }

                        break

                    case 14:
                        if ( QuestHelper.hasQuestItems( player, REPORT_BOX ) ) {
                            return this.getPath( '31529-17.html' )
                        }

                        break
                }

                break
            }
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}