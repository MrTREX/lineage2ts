import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

const RADIA = 30088
const RALFORD = 30165
const VARAN = 30294

const mobs = [
    20560, // Trisalim Spider
    20561, // Trisalim Tarantula
]

const SUEDE = 1866
const THREAD = 1868
const MYSTERIOUS_CLOTH = 7076
const SKEIN_OF_YARN = 7161
const SPINNERET = 7528

const minimumLevel = 60
const SPINNERET_COUNT = 10
const SUEDE_COUNT = 3000
const THREAD_COUNT = 5000

export class InSearchOfCloth extends ListenerLogic {
    constructor() {
        super( 'Q00034_InSearchOfCloth', 'listeners/tracked/InSearchOfCloth.ts' )
        this.questId = 34
        this.questItemIds = [ SKEIN_OF_YARN, SPINNERET ]
    }

    getAttackableKillIds(): Array<number> {
        return mobs
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00034_InSearchOfCloth'
    }

    getQuestStartIds(): Array<number> {
        return [ RADIA ]
    }

    getTalkIds(): Array<number> {
        return [ RADIA, RALFORD, VARAN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player: L2PcInstance = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 4 )
        if ( player && QuestHelper.getAdjustedChance( SPINNERET, Math.random(), data.isChampion ) > 0.5 ) {
            let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), this.name )
            await QuestHelper.rewardSingleQuestItem( player, SPINNERET, 1, data.isChampion )

            if ( QuestHelper.hasQuestItemCount( player, SPINNERET, SPINNERET_COUNT ) ) {
                state.setConditionWithSound( 5, true )
                return
            }

            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30088-03.htm':
                state.startQuest()
                break

            case '30294-02.html':
                state.setConditionWithSound( 2, true )
                break

            case '30088-06.html':
                state.setConditionWithSound( 3, true )
                break

            case '30165-02.html':
                state.setConditionWithSound( 4, true )
                break

            case '30165-05.html':
                if ( QuestHelper.getQuestItemsCount( player, SPINNERET ) < SPINNERET_COUNT ) {
                    return QuestHelper.getNoQuestMessagePath()
                }

                await QuestHelper.takeSingleItem( player, SPINNERET, SPINNERET_COUNT )
                await QuestHelper.giveSingleItem( player, SKEIN_OF_YARN, 1 )

                state.setConditionWithSound( 6, true )
                break

            case '30088-10.html':
                if ( QuestHelper.hasQuestItemCount( player, SUEDE, SUEDE_COUNT )
                        && QuestHelper.hasQuestItemCount( player, THREAD, THREAD_COUNT )
                        && QuestHelper.hasQuestItem( player, SKEIN_OF_YARN ) ) {
                    await QuestHelper.takeSingleItem( player, SKEIN_OF_YARN, 1 )
                    await QuestHelper.takeSingleItem( player, SUEDE, SUEDE_COUNT )
                    await QuestHelper.takeSingleItem( player, THREAD, THREAD_COUNT )

                    await QuestHelper.giveSingleItem( player, MYSTERIOUS_CLOTH, 1 )
                    await state.exitQuest( false, true )

                    break
                }

                return this.getPath( '30088-11.html' )

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case RADIA:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() >= minimumLevel ? '30088-01.htm' : '30088-02.html' )

                    case QuestStateValues.STARTED:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '30088-04.html' )

                            case 2:
                                return this.getPath( '30088-05.html' )

                            case 3:
                                return this.getPath( '30088-07.html' )

                            case 6:
                                if ( QuestHelper.hasQuestItemCount( player, SUEDE, SUEDE_COUNT ) && QuestHelper.hasQuestItemCount( player, THREAD, THREAD_COUNT ) ) {
                                    return this.getPath( '30088-08.html' )
                                }

                                return this.getPath( '30088-09.html' )
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getNoQuestMessagePath()
                }

                break

            case VARAN:
                if ( state.isStarted() ) {
                    switch ( state.getCondition() ) {
                        case 1:
                            return this.getPath( '30294-01.html' )
                        case 2:
                            return this.getPath( '30294-03.html' )
                    }
                }

                break

            case RALFORD:
                if ( state.isStarted() ) {
                    switch ( state.getCondition() ) {
                        case 3:
                            return this.getPath( '30165-01.html' )

                        case 4:
                            return this.getPath( '30165-03.html' )

                        case 5:
                            return this.getPath( '30165-04.html' )

                        case 6:
                            return this.getPath( '30165-06.html' )
                    }
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}