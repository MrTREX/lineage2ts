import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcSpawnEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { CategoryType } from '../../gameService/enums/CategoryType'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { Race } from '../../gameService/enums/Race'
import { AIEffectHelper } from '../../gameService/aicontroller/helpers/AIEffectHelper'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const GROCER_HELVERIA = 30081
const BLACK_MARKETEER_OF_MAMMON = 31092
const BLUEPRINT_SELLER_DAEGER = 31435
const MARK = 32487

const BLOODY_CLOTH_FRAGMENT = 10867
const HELVETIAS_ANTIDOTE = 10868

const MARK_OF_CHALLENGER = 2627
const MARK_OF_DUTY = 2633
const MARK_OF_SEEKER = 2673
const MARK_OF_SCHOLAR = 2674
const MARK_OF_PILGRIM = 2721
const MARK_OF_TRUST = 2734
const MARK_OF_DUELIST = 2762
const MARK_OF_SEARCHER = 2809
const MARK_OF_HEALER = 2820
const MARK_OF_REFORMER = 2821
const MARK_OF_MAGUS = 2840
const MARK_OF_MAESTRO = 2867
const MARK_OF_WARSPIRIT = 2879
const MARK_OF_GUILDSMAN = 3119
const MARK_OF_LIFE = 3140
const MARK_OF_FATE = 3172
const MARK_OF_GLORY = 3203
const MARK_OF_PROSPERITY = 3238
const MARK_OF_CHAMPION = 3276
const MARK_OF_SAGITTARIUS = 3293
const MARK_OF_WITCHCRAFT = 3307
const MARK_OF_SUMMONER = 3336
const MARK_OF_LORD = 3390

const PURSUER = 27340

const minimumLevel = 39
const ONE_MILLION = 1000000
const TWO_MILLION = 2000000
const THREE_MILLION = 3000000

export class GoodWorksReward extends ListenerLogic {
    constructor() {
        super( 'Q00060_GoodWorksReward', 'listeners/tracked/GoodWorksReward.ts' )
        this.questId = 60
        this.questItemIds = [ BLOODY_CLOTH_FRAGMENT, HELVETIAS_ANTIDOTE ]
    }

    getAttackableKillIds(): Array<number> {
        return [ PURSUER ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00060_GoodWorksReward'
    }

    getQuestStartIds(): Array<number> {
        return [ BLUEPRINT_SELLER_DAEGER ]
    }

    getSpawnIds(): Array<number> {
        return [ PURSUER ]
    }

    getTalkIds(): Array<number> {
        return [ BLUEPRINT_SELLER_DAEGER, GROCER_HELVERIA, BLACK_MARKETEER_OF_MAMMON, MARK ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, npc, player, true ) ) {
            return
        }

        if ( state.isMemoState( 1 ) ) {
            if ( data.playerId === NpcVariablesManager.get( data.targetId, 'playerId' ) ) {
                state.setMemoState( 2 )
                state.setConditionWithSound( 2, true )

                BroadcastHelper.broadcastNpcSayStringId(
                        npc,
                        NpcSayType.NpcAll,
                        NpcStringIds.YOU_ARE_STRONG_THIS_WAS_A_MISTAKE )
            } else {
                BroadcastHelper.broadcastNpcSayStringId(
                        npc,
                        NpcSayType.NpcAll,
                        NpcStringIds.WHO_ARE_YOU_TO_JOIN_IN_THE_BATTLE_HOW_UPSETTING )
            }
        }

        let objectId: number = NpcVariablesManager.get( data.targetId, 'spawnedNpcObjectId' ) as number
        if ( objectId > 0 && NpcVariablesManager.get( objectId, 'spawned' ) ) {
            NpcVariablesManager.set( objectId, 'spawned', false )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        if ( data.eventName === 'despawn' ) {
            let npc = L2World.getObjectById( data.characterId ) as L2Npc
            BroadcastHelper.broadcastNpcSayStringId(
                    npc,
                    NpcSayType.NpcAll,
                    NpcStringIds.YOU_HAVE_GOOD_LUCK_I_SHALL_RETURN )

            let spawnedNpcId: number = NpcVariablesManager.get( data.characterId, 'spawnedNpcObjectId' ) as number
            NpcVariablesManager.set( spawnedNpcId, 'spawned', false )

            await npc.deleteMe()
            return
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31435-07.htm':
                if ( state.isCreated() ) {
                    state.startQuest()
                    state.setMemoState( 1 )

                    return this.getPath( data.eventName )
                }

                return

            case '31435-02.htm':
                return this.getPath( data.eventName )

            case '31435-10.html':
                if ( state.isMemoState( 3 ) ) {
                    state.setMemoState( 4 )
                    state.setConditionWithSound( 4, true )
                    return this.getPath( data.eventName )
                }

                return

            case '31435-14.html':
                if ( state.isMemoState( 8 ) ) {
                    state.setMemoState( 9 )
                    state.setConditionWithSound( 9, true )
                    return this.getPath( data.eventName )
                }

                return

            case '30081-02.html':
                if ( state.isMemoState( 4 ) ) {
                    return this.getPath( data.eventName )
                }

                return

            case '30081-03.html':
                if ( state.isMemoState( 4 ) ) {
                    await QuestHelper.takeSingleItem( player, BLOODY_CLOTH_FRAGMENT, -1 )
                    state.setMemoState( 5 )
                    state.setConditionWithSound( 5, true )
                    return this.getPath( data.eventName )
                }

                return

            case '30081-05.html':
                let memoState = state.getMemoState()
                if ( ( memoState >= 5 ) && ( memoState <= 6 ) ) {
                    if ( player.getAdena() >= THREE_MILLION ) {
                        await QuestHelper.giveSingleItem( player, HELVETIAS_ANTIDOTE, 1 )
                        await QuestHelper.giveAdena( player, THREE_MILLION )
                        state.setMemoState( 7 )
                        state.setConditionWithSound( 7, true )

                        return this.getPath( data.eventName )
                    }

                    state.setMemoState( 6 )
                    state.setConditionWithSound( 6, true )
                    return this.getPath( '30081-06.html' )
                }

                return

            case '30081-07.html':
                if ( state.isMemoState( 5 ) ) {
                    state.setMemoState( 6 )
                    state.setConditionWithSound( 6, true )
                    return this.getPath( data.eventName )
                }

                return

            case 'REPLY_1':
                if ( state.isMemoState( 10 ) ) {
                    let secondJobQuests: boolean = player.hasAnyQuestsCompleted(
                            'Q00222_TestOfTheDuelist',
                            'Q00223_TestOfTheChampion',
                            'Q00224_TestOfSagittarius',
                            'Q00225_TestOfTheSearcher',
                            'Q00226_TestOfTheHealer',
                            'Q00227_TestOfTheReformer',
                            'Q00228_TestOfMagus',
                            'Q00229_TestOfWitchcraft',
                            'Q00230_TestOfTheSummoner',
                            'Q00231_TestOfTheMaestro',
                            'Q00232_TestOfTheLord',
                            'Q00233_TestOfTheWarSpirit' )

                    let otherJobQuests: boolean = player.hasAnyQuestsCompleted(
                            'Q00217_TestimonyOfTrust',
                            'Q00218_TestimonyOfLife',
                            'Q00219_TestimonyOfFate',
                            'Q00220_TestimonyOfGlory',
                            'Q00221_TestimonyOfProsperity' )

                    if ( player.hasAnyQuestsCompleted(
                            'Q00211_TrialOfTheChallenger',
                            'Q00212_TrialOfDuty',
                            'Q00213_TrialOfTheSeeker',
                            'Q00214_TrialOfTheScholar',
                            'Q00215_TrialOfThePilgrim',
                            'Q00216_TrialOfTheGuildsman' ) ) {

                        if ( otherJobQuests ) {

                            if ( secondJobQuests ) {
                                state.setMemoStateEx( 1, 3 )
                            } else {
                                state.setMemoStateEx( 1, 2 )
                            }

                        } else if ( secondJobQuests ) {
                            state.setMemoStateEx( 1, 2 )
                        } else {
                            state.setMemoStateEx( 1, 1 )
                        }
                    } else if ( otherJobQuests ) {
                        if ( secondJobQuests ) {
                            state.setMemoStateEx( 1, 2 )
                        } else {
                            state.setMemoStateEx( 1, 1 )
                        }
                    } else if ( secondJobQuests ) {
                        state.setMemoStateEx( 1, 1 )
                    }

                    return this.getPath( '31092-02.html' )
                }

                return

            case 'REPLY_2':
                if ( state.isMemoState( 10 ) ) {
                    if ( state.getMemoStateEx( 1 ) >= 3 ) {
                        return this.getPath( '31092-03b.html' )
                    }

                    if ( state.getMemoStateEx( 1 ) >= 1 ) {
                        return this.getPath( '31092-03.html' )
                    }

                    return this.getPath( '31092-03a.html' )
                }

                return

            case 'REPLY_3':
                if ( state.isMemoState( 10 ) ) {
                    if ( state.getMemoStateEx( 1 ) >= 3 ) {
                        await QuestHelper.giveAdena( player, THREE_MILLION )
                        return this.getPath( '31092-04a.html' )
                    }

                    if ( state.getMemoStateEx( 1 ) === 2 ) {
                        await QuestHelper.giveAdena( player, TWO_MILLION )
                        return this.getPath( '31092-04b.html' )
                    }

                    if ( state.getMemoStateEx( 1 ) === 1 ) {
                        await QuestHelper.giveAdena( player, ONE_MILLION )
                        return this.getPath( '31092-04b.html' )
                    }

                    await state.exitQuest( false, true )
                }

                return

            case 'REPLY_4':
                if ( state.isMemoState( 10 ) ) {
                    switch ( player.getClassId() ) {
                        case ClassIdValues.warrior.id:
                            return this.getPath( '31092-05.html' )

                        case ClassIdValues.knight.id:
                            return this.getPath( '31092-06.html' )

                        case ClassIdValues.rogue.id:
                            return this.getPath( '31092-07.html' )

                        case ClassIdValues.wizard.id:
                            return this.getPath( '31092-08.html' )

                        case ClassIdValues.cleric.id:
                            return this.getPath( '31092-09.html' )

                        case ClassIdValues.elvenKnight.id:
                            return this.getPath( '31092-10.html' )

                        case ClassIdValues.elvenScout.id:
                            return this.getPath( '31092-11.html' )

                        case ClassIdValues.elvenWizard.id:
                            return this.getPath( '31092-12.html' )

                        case ClassIdValues.oracle.id:
                            return this.getPath( '31092-13.html' )

                        case ClassIdValues.palusKnight.id:
                            return this.getPath( '31092-14.html' )

                        case ClassIdValues.assassin.id:
                            return this.getPath( '31092-15.html' )

                        case ClassIdValues.darkWizard.id:
                            return this.getPath( '31092-16.html' )

                        case ClassIdValues.shillienOracle.id:
                            return this.getPath( '31092-17.html' )

                        case ClassIdValues.orcRaider.id:
                            return this.getPath( '31092-18.html' )

                        case ClassIdValues.orcMonk.id:
                            return this.getPath( '31092-19.html' )

                        case ClassIdValues.orcShaman.id:
                            return this.getPath( '31092-20.html' )

                        case ClassIdValues.scavenger.id:
                            return this.getPath( '31092-21.html' )

                        case ClassIdValues.artisan.id:
                            return this.getPath( '31092-22.html' )
                    }

                    await state.exitQuest( false, true )
                }

                return

            case 'REPLY_5':
                if ( player.isInCategory( CategoryType.SECOND_CLASS_GROUP ) ) {

                    switch ( player.getClassId() ) {
                        case ClassIdValues.warrior.id:
                            return this.getPath( '31092-05a.html' )

                        case ClassIdValues.knight.id:
                            return this.getPath( '31092-06a.html' )

                        case ClassIdValues.rogue.id:
                            return this.getPath( '31092-07a.html' )

                        case ClassIdValues.wizard.id:
                            return this.getPath( '31092-08a.html' )

                        case ClassIdValues.cleric.id:
                            return this.getPath( '31092-09a.html' )

                        case ClassIdValues.elvenKnight.id:
                            return this.getPath( '31092-10a.html' )

                        case ClassIdValues.elvenScout.id:
                            return this.getPath( '31092-11a.html' )

                        case ClassIdValues.elvenWizard.id:
                            return this.getPath( '31092-12a.html' )

                        case ClassIdValues.oracle.id:
                            return this.getPath( '31092-13a.html' )

                        case ClassIdValues.palusKnight.id:
                            return this.getPath( '31092-14a.html' )

                        case ClassIdValues.assassin.id:
                            return this.getPath( '31092-15a.html' )

                        case ClassIdValues.darkWizard.id:
                            return this.getPath( '31092-16a.html' )

                        case ClassIdValues.shillienOracle.id:
                            return this.getPath( '31092-17a.html' )

                        case ClassIdValues.orcRaider.id:
                            return this.getPath( '31092-18a.html' )

                        case ClassIdValues.orcMonk.id:
                            return this.getPath( '31092-19a.html' )

                        case ClassIdValues.orcShaman.id:
                            return this.getPath( '31092-20a.html' )

                        case ClassIdValues.scavenger.id:
                            return this.getPath( '31092-21a.html' )

                        case ClassIdValues.artisan.id:
                            return this.getPath( '31092-22a.html' )
                    }
                }

                return

            case 'REPLY_6':
                if ( player.getClassId() === ClassIdValues.warrior.id ) {
                    if ( !QuestHelper.hasQuestItem( player, MARK_OF_CHALLENGER ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_CHALLENGER, 1 )
                    }

                    if ( !QuestHelper.hasQuestItem( player, MARK_OF_TRUST ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_TRUST, 1 )
                    }

                    if ( !QuestHelper.hasQuestItem( player, MARK_OF_DUELIST ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_DUELIST, 1 )
                    }

                    return this.getPath( '31092-25.html' )
                }

                return

            case 'REPLY_7':
                if ( player.getClassId() === ClassIdValues.warrior.id ) {
                    if ( !QuestHelper.hasQuestItem( player, MARK_OF_CHALLENGER ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_CHALLENGER, 1 )
                    }

                    if ( !QuestHelper.hasQuestItem( player, MARK_OF_TRUST ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_TRUST, 1 )
                    }

                    if ( !QuestHelper.hasQuestItem( player, MARK_OF_CHAMPION ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_CHAMPION, 1 )
                    }

                    return this.getPath( '31092-25.html' )
                }

                return

            case 'REPLY_8':
                if ( player.getClassId() === ClassIdValues.knight.id ) {
                    if ( !QuestHelper.hasQuestItem( player, MARK_OF_DUTY ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_DUTY, 1 )
                    }

                    if ( !QuestHelper.hasQuestItem( player, MARK_OF_TRUST ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_TRUST, 1 )
                    }

                    if ( !QuestHelper.hasQuestItem( player, MARK_OF_HEALER ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_HEALER, 1 )
                    }

                    return this.getPath( '31092-25.html' )
                }

                return

            case 'REPLY_9':
                if ( player.getClassId() === ClassIdValues.knight.id ) {
                    if ( !QuestHelper.hasQuestItem( player, MARK_OF_DUTY ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_DUTY, 1 )
                    }

                    if ( !QuestHelper.hasQuestItem( player, MARK_OF_TRUST ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_TRUST, 1 )
                    }

                    if ( !QuestHelper.hasQuestItem( player, MARK_OF_WITCHCRAFT ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_WITCHCRAFT, 1 )
                    }

                    return this.getPath( '31092-25.html' )
                }

                return

            case 'REPLY_10':
                if ( player.getClassId() === ClassIdValues.rogue.id ) {
                    if ( !QuestHelper.hasQuestItem( player, MARK_OF_SEEKER ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_SEEKER, 1 )
                    }

                    if ( !QuestHelper.hasQuestItem( player, MARK_OF_TRUST ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_TRUST, 1 )
                    }

                    if ( !QuestHelper.hasQuestItem( player, MARK_OF_SEARCHER ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_SEARCHER, 1 )
                    }

                    return this.getPath( '31092-25.html' )
                }

                return

            case 'REPLY_11':
                if ( player.getClassId() === ClassIdValues.rogue.id ) {
                    if ( QuestHelper.hasQuestItem( player, MARK_OF_SEEKER ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_SEEKER, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_TRUST ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_TRUST, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_SAGITTARIUS ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_SAGITTARIUS, 1 )
                    }

                    return this.getPath( '31092-25.html' )
                }

                return
            case 'REPLY_12':
                if ( player.getClassId() === ClassIdValues.wizard.id ) {
                    if ( QuestHelper.hasQuestItem( player, MARK_OF_SCHOLAR ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_SCHOLAR, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_TRUST ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_TRUST, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_MAGUS ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_MAGUS, 1 )
                    }

                    return this.getPath( '31092-25.html' )
                }

                return

            case 'REPLY_13':
                if ( player.getClassId() === ClassIdValues.wizard.id ) {
                    if ( QuestHelper.hasQuestItem( player, MARK_OF_SCHOLAR ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_SCHOLAR, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_TRUST ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_TRUST, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_WITCHCRAFT ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_WITCHCRAFT, 1 )
                    }

                    return this.getPath( '31092-25.html' )
                }

                return

            case 'REPLY_14':
                if ( player.getClassId() === ClassIdValues.wizard.id ) {
                    if ( QuestHelper.hasQuestItem( player, MARK_OF_SCHOLAR ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_SCHOLAR, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_TRUST ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_TRUST, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_SUMMONER ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_SUMMONER, 1 )
                    }

                    return this.getPath( '31092-25.html' )
                }

                return

            case 'REPLY_15':
                if ( player.getClassId() === ClassIdValues.cleric.id ) {
                    if ( QuestHelper.hasQuestItem( player, MARK_OF_PILGRIM ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_PILGRIM, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_TRUST ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_TRUST, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_HEALER ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_HEALER, 1 )
                    }

                    return this.getPath( '31092-25.html' )
                }

                return

            case 'REPLY_16':
                if ( player.getClassId() === ClassIdValues.cleric.id ) {
                    if ( QuestHelper.hasQuestItem( player, MARK_OF_PILGRIM ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_PILGRIM, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_TRUST ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_TRUST, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_REFORMER ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_REFORMER, 1 )
                    }

                    return this.getPath( '31092-25.html' )
                }

                return

            case 'REPLY_17':
                if ( player.getClassId() === ClassIdValues.elvenKnight.id ) {
                    if ( QuestHelper.hasQuestItem( player, MARK_OF_DUTY ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_DUTY, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_LIFE ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_LIFE, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_HEALER ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_HEALER, 1 )
                    }

                    return this.getPath( '31092-25.html' )
                }

                return

            case 'REPLY_18':
                if ( player.getClassId() === ClassIdValues.elvenKnight.id ) {
                    if ( QuestHelper.hasQuestItem( player, MARK_OF_CHALLENGER ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_CHALLENGER, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_LIFE ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_LIFE, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_DUELIST ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_DUELIST, 1 )
                    }

                    return this.getPath( '31092-25.html' )
                }

                return

            case 'REPLY_19':
                if ( player.getClassId() === ClassIdValues.elvenScout.id ) {
                    if ( QuestHelper.hasQuestItem( player, MARK_OF_SEEKER ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_SEEKER, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_LIFE ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_LIFE, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_SEARCHER ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_SEARCHER, 1 )
                    }

                    return this.getPath( '31092-25.html' )
                }

                return

            case 'REPLY_20':
                if ( player.getClassId() === ClassIdValues.elvenScout.id ) {
                    if ( QuestHelper.hasQuestItem( player, MARK_OF_SEEKER ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_SEEKER, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_LIFE ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_LIFE, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_SAGITTARIUS ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_SAGITTARIUS, 1 )
                    }

                    return this.getPath( '31092-25.html' )
                }

                return

            case 'REPLY_21':
                if ( player.getClassId() === ClassIdValues.elvenWizard.id ) {
                    if ( QuestHelper.hasQuestItem( player, MARK_OF_SCHOLAR ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_SCHOLAR, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_LIFE ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_LIFE, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_MAGUS ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_MAGUS, 1 )
                    }

                    return this.getPath( '31092-25.html' )
                }

                return

            case 'REPLY_22':
                if ( player.getClassId() === ClassIdValues.elvenWizard.id ) {
                    if ( QuestHelper.hasQuestItem( player, MARK_OF_SCHOLAR ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_SCHOLAR, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_LIFE ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_LIFE, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_SUMMONER ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_SUMMONER, 1 )
                    }

                    return this.getPath( '31092-25.html' )
                }

                return

            case 'REPLY_23':
                if ( player.getClassId() === ClassIdValues.oracle.id ) {
                    if ( QuestHelper.hasQuestItem( player, MARK_OF_PILGRIM ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_PILGRIM, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_LIFE ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_LIFE, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_HEALER ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_HEALER, 1 )
                    }

                    return this.getPath( '31092-25.html' )
                }

                return

            case 'REPLY_24':
                if ( player.getClassId() === ClassIdValues.palusKnight.id ) {
                    if ( QuestHelper.hasQuestItem( player, MARK_OF_DUTY ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_DUTY, 1 )
                    }
                    if ( QuestHelper.hasQuestItem( player, MARK_OF_FATE ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_FATE, 1 )
                    }
                    if ( QuestHelper.hasQuestItem( player, MARK_OF_WITCHCRAFT ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_WITCHCRAFT, 1 )
                    }
                    return this.getPath( '31092-25.html' )
                }

                return

            case 'REPLY_25':
                if ( player.getClassId() === ClassIdValues.palusKnight.id ) {
                    if ( QuestHelper.hasQuestItem( player, MARK_OF_CHALLENGER ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_CHALLENGER, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_FATE ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_FATE, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_DUELIST ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_DUELIST, 1 )
                    }

                    return this.getPath( '31092-25.html' )
                }

                return

            case 'REPLY_26':
                if ( player.getClassId() === ClassIdValues.assassin.id ) {
                    if ( QuestHelper.hasQuestItem( player, MARK_OF_SEEKER ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_SEEKER, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_FATE ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_FATE, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_SEARCHER ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_SEARCHER, 1 )
                    }

                    return this.getPath( '31092-25.html' )
                }

                return

            case 'REPLY_27':
                if ( player.getClassId() === ClassIdValues.assassin.id ) {
                    if ( QuestHelper.hasQuestItem( player, MARK_OF_SEEKER ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_SEEKER, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_FATE ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_FATE, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_SAGITTARIUS ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_SAGITTARIUS, 1 )
                    }

                    return this.getPath( '31092-25.html' )
                }

                return

            case 'REPLY_28':
                if ( player.getClassId() === ClassIdValues.darkWizard.id ) {
                    if ( QuestHelper.hasQuestItem( player, MARK_OF_SCHOLAR ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_SCHOLAR, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_FATE ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_FATE, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_MAGUS ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_MAGUS, 1 )
                    }

                    return this.getPath( '31092-25.html' )
                }

                return

            case 'REPLY_29':
                if ( player.getClassId() === ClassIdValues.darkWizard.id ) {
                    if ( QuestHelper.hasQuestItem( player, MARK_OF_SCHOLAR ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_SCHOLAR, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_FATE ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_FATE, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_SUMMONER ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_SUMMONER, 1 )
                    }

                    return this.getPath( '31092-25.html' )
                }

                return

            case 'REPLY_30':
                if ( player.getClassId() === ClassIdValues.shillienOracle.id ) {
                    if ( QuestHelper.hasQuestItem( player, MARK_OF_PILGRIM ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_PILGRIM, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_FATE ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_FATE, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_REFORMER ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_REFORMER, 1 )
                    }

                    return this.getPath( '31092-25.html' )
                }

                return

            case 'REPLY_31':
                if ( player.getClassId() === ClassIdValues.orcRaider.id ) {
                    if ( QuestHelper.hasQuestItem( player, MARK_OF_CHALLENGER ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_CHALLENGER, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_GLORY ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_GLORY, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_CHAMPION ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_CHAMPION, 1 )
                    }

                    return this.getPath( '31092-25.html' )
                }

                return

            case 'REPLY_32':
                if ( player.getClassId() === ClassIdValues.orcMonk.id ) {
                    if ( QuestHelper.hasQuestItem( player, MARK_OF_CHALLENGER ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_CHALLENGER, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_GLORY ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_GLORY, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_DUELIST ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_DUELIST, 1 )
                    }

                    return this.getPath( '31092-25.html' )
                }

                return

            case 'REPLY_33':
                if ( player.getClassId() === ClassIdValues.orcShaman.id ) {
                    if ( QuestHelper.hasQuestItem( player, MARK_OF_PILGRIM ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_PILGRIM, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_GLORY ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_GLORY, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_LORD ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_LORD, 1 )
                    }

                    return this.getPath( '31092-25.html' )
                }

                return

            case 'REPLY_34':
                if ( player.getClassId() === ClassIdValues.orcShaman.id ) {
                    if ( QuestHelper.hasQuestItem( player, MARK_OF_PILGRIM ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_PILGRIM, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_GLORY ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_GLORY, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_WARSPIRIT ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_WARSPIRIT, 1 )
                    }

                    return this.getPath( '31092-25.html' )
                }

                return

            case 'REPLY_35':
                if ( player.getClassId() === ClassIdValues.scavenger.id ) {
                    if ( QuestHelper.hasQuestItem( player, MARK_OF_GUILDSMAN ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_GUILDSMAN, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_PROSPERITY ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_PROSPERITY, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_SEARCHER ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_SEARCHER, 1 )
                    }

                    return this.getPath( '31092-25.html' )
                }

                return

            case 'REPLY_36':
                if ( player.getClassId() === ClassIdValues.artisan.id ) {
                    if ( QuestHelper.hasQuestItem( player, MARK_OF_GUILDSMAN ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_GUILDSMAN, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_PROSPERITY ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_PROSPERITY, 1 )
                    }

                    if ( QuestHelper.hasQuestItem( player, MARK_OF_MAESTRO ) ) {
                        await QuestHelper.giveSingleItem( player, MARK_OF_MAESTRO, 1 )
                    }

                    return this.getPath( '31092-25.html' )
                }

                return

            case '32487-04.html':
                if ( state.isMemoState( 1 ) ) {
                    if ( !NpcVariablesManager.get( data.characterId, 'spawned' ) ) {
                        NpcVariablesManager.set( data.characterId, 'spawned', true )
                        NpcVariablesManager.set( data.characterId, 'playerId', data.playerId )

                        let npc: L2Npc = QuestHelper.addGenericSpawn( null, PURSUER, player.getX() + 50, player.getY() + 50, player.getZ(), 0, false, 0, false, 0 )

                        NpcVariablesManager.set( npc.getObjectId(), 'playerId', data.playerId )
                        NpcVariablesManager.set( npc.getObjectId(), 'spawnedNpcObjectId', data.characterId )
                        NpcVariablesManager.set( npc.getObjectId(), 'firstPlayerId', data.playerId )

                        await AIEffectHelper.notifyAttackedWithTargetId( npc, data.playerId, 1, 1 )

                        return this.getPath( data.eventName )
                    }

                    return this.getPath( '32487-05.html' )
                }

                return

            case '32487-10.html':
                if ( state.isMemoState( 7 ) ) {
                    await QuestHelper.takeSingleItem( player, HELVETIAS_ANTIDOTE, 1 )
                    state.setMemoState( 8 )
                    state.setConditionWithSound( 8, true )

                    if ( NpcVariablesManager.get( data.characterId, 'spawned' ) ) {
                        NpcVariablesManager.set( data.characterId, 'spawned', false )
                    }

                    return this.getPath( data.eventName )
                }

                return
        }
    }

    async onSpawnEvent( data: NpcSpawnEvent ): Promise<void> {
        this.startQuestTimer( 'despawn', 60000, data.characterId, null )

        let playerId: number = NpcVariablesManager.get( data.characterId, 'firstPlayerId' ) as number
        let player = L2World.getPlayer( playerId )
        if ( player ) {
            let npc = L2World.getObjectById( data.characterId ) as L2Npc
            BroadcastHelper.broadcastNpcSayStringId(
                    npc,
                    NpcSayType.NpcAll,
                    NpcStringIds.S1_I_MUST_KILL_YOU_BLAME_YOUR_OWN_CURIOSITY,
                    player.getAppearance().getVisibleName() )
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name, true )
        let player = L2World.getPlayer( data.playerId )

        if ( state.isCreated() ) {
            if ( data.characterNpcId === BLUEPRINT_SELLER_DAEGER ) {
                if ( player.getRace() !== Race.KAMAEL ) {
                    if ( player.isInCategory( CategoryType.SECOND_CLASS_GROUP ) ) {
                        return this.getPath( player.getLevel() >= minimumLevel ? '31435-01.htm' : '31435-03.htm' )
                    }

                    return this.getPath( '31435-04.htm' )
                }

                return this.getPath( '31435-06.htm' )
            }

            return
        }

        if ( state.isStarted() ) {
            let memoState = state.getMemoState()

            switch ( data.characterNpcId ) {
                case BLUEPRINT_SELLER_DAEGER:
                    if ( memoState <= 2 ) {
                        return this.getPath( '31435-08.html' )
                    }

                    if ( memoState === 3 ) {
                        return this.getPath( '31435-09.html' )
                    }

                    if ( memoState === 4 ) {
                        return this.getPath( '31435-11.html' )
                    }

                    if ( memoState > 4 && memoState < 8 ) {
                        return this.getPath( '31435-12.html' )
                    }

                    if ( memoState === 8 ) {
                        return this.getPath( '31435-13.html' )
                    }

                    if ( memoState === 9 ) {
                        state.setMemoState( 10 )
                        state.setConditionWithSound( 10, true )
                        return this.getPath( '31435-15.html' )
                    }

                    if ( memoState === 10 ) {
                        return this.getPath( '31435-16.html' )
                    }

                    return

                case GROCER_HELVERIA:
                    if ( memoState === 4 ) {
                        return this.getPath( '30081-01.html' )
                    }

                    if ( memoState === 5 ) {
                        return this.getPath( '30081-04.html' )
                    }

                    if ( memoState === 6 ) {
                        return this.getPath( '30081-08.html' )
                    }

                    if ( memoState === 7 ) {
                        if ( !QuestHelper.hasQuestItem( player, HELVETIAS_ANTIDOTE ) ) {
                            await QuestHelper.giveSingleItem( player, HELVETIAS_ANTIDOTE, 1 )
                            return this.getPath( '30081-09.html' )
                        }

                        return this.getPath( '30081-10.html' )
                    }

                    return

                case BLACK_MARKETEER_OF_MAMMON:
                    if ( memoState === 10 ) {
                        if ( player.isInCategory( CategoryType.SECOND_CLASS_GROUP ) ) {
                            state.setMemoStateEx( 1, 0 )
                            return this.getPath( '31092-01.html' )
                        }

                        await QuestHelper.giveAdena( player, THREE_MILLION )
                        await state.exitQuest( false, true )

                        return this.getPath( '31092-01a.html' )
                    }

                    return

                case MARK:
                    if ( memoState === 1 ) {
                        if ( !NpcVariablesManager.get( data.characterId, 'spawned' ) ) {
                            return this.getPath( '32487-01.html' )
                        }

                        if ( NpcVariablesManager.get( data.characterId, 'playerId' ) === data.playerId ) {
                            return this.getPath( '32487-03.html' )
                        }

                        return this.getPath( '32487-02.html' )
                    }

                    if ( memoState === 2 ) {
                        await QuestHelper.giveSingleItem( player, BLOODY_CLOTH_FRAGMENT, 1 )

                        state.setMemoState( 3 )
                        state.setConditionWithSound( 3, true )

                        return this.getPath( '32487-06.html' )
                    }

                    if ( memoState >= 3 && memoState < 7 ) {
                        return this.getPath( '32487-07.html' )
                    }

                    if ( memoState === 7 ) {
                        return this.getPath( '32487-09.html' )
                    }

                    return
            }

            return
        }

        if ( state.isCompleted() ) {
            switch ( data.characterNpcId ) {
                case BLUEPRINT_SELLER_DAEGER:
                    return QuestHelper.getAlreadyCompletedMessagePath()

                case BLACK_MARKETEER_OF_MAMMON:
                    if ( player.isInCategory( CategoryType.SECOND_CLASS_GROUP ) ) {
                        return this.getPath( '31092-23.html' )
                    }

                    return this.getPath( '31092-24.html' )
            }

            return
        }
    }
}