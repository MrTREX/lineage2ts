import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'

const REED = 30520
const BALANKI = 30533
const GERALD = 30650

const VERY_EXPENSIVE_NECKLACE = 7574
const SCROLL_OF_ESCAPE_GIRAN = 7559
const MARK_OF_TRAVELER = 7570

const minimumLevel = 3

export class IntoTheWorld extends ListenerLogic {
    constructor() {
        super( 'Q00010_IntoTheWorld', 'listeners/tracked/IntoTheWorld.ts' )
        this.questId = 10
        this.questItemIds = [ VERY_EXPENSIVE_NECKLACE ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00010_IntoTheWorld'
    }

    getQuestStartIds(): Array<number> {
        return [ BALANKI ]
    }

    getTalkIds(): Array<number> {
        return [ BALANKI, REED, GERALD ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state ) {
            return null
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30533-03.htm':
                state.startQuest()
                break

            case '30533-06.html':
                await QuestHelper.giveSingleItem( player, MARK_OF_TRAVELER, 1 )
                await QuestHelper.rewardSingleItem( player, SCROLL_OF_ESCAPE_GIRAN, 1 )
                await state.exitQuest( false, true )
                break

            case '30520-02.html':
                await QuestHelper.giveSingleItem( player, VERY_EXPENSIVE_NECKLACE, 1 )
                state.setConditionWithSound( 2, true )
                break

            case '30520-05.html':
                state.setConditionWithSound( 4, true )
                break

            case '30650-02.html':
                if ( !QuestHelper.hasQuestItems( player, VERY_EXPENSIVE_NECKLACE ) ) {
                    return '30650-03.html'
                }

                await QuestHelper.giveSingleItem( player, VERY_EXPENSIVE_NECKLACE, -1 )
                state.setConditionWithSound( 3, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case BALANKI:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        if ( player.getLevel() >= minimumLevel && player.getRace() === Race.DWARF ) {
                            return this.getPath( '30533-01.htm' )
                        }

                        return this.getPath( '30533-02.html' )

                    case QuestStateValues.STARTED:
                        if ( state.isCondition( 1 ) ) {
                            return this.getPath( '30533-04.html' )
                        }

                        if ( state.isCondition( 4 ) ) {
                            return this.getPath( '30533-05.html' )
                        }
                        break
                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case REED:
                if ( state.isStarted() ) {
                    switch ( state.getCondition() ) {
                        case 1:
                            return this.getPath( '30520-01.html' )

                        case 2:
                            return this.getPath( '30520-03.html' )

                        case 3:
                            return this.getPath( '30520-04.html' )

                        case 4:
                            return this.getPath( '30520-06.html' )
                    }
                }

                break

            case GERALD:
                if ( state.isStarted() ) {
                    if ( state.isCondition( 2 ) ) {
                        return this.getPath( '30650-01.html' )
                    }

                    if ( state.isCondition( 3 ) ) {
                        return this.getPath( '30650-04.html' )
                    }
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}