import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { ExShowScreenMessage } from '../../gameService/packets/send/ExShowScreenMessage'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'

const BOLTER = 30554
const SHARI = 30517
const GARITA = 30518
const REED = 30520
const BRUNON = 30526

const BOLTERS_LIST = 1547
const MINING_BOOTS = 1548
const MINERS_PICK = 1549
const BOOMBOOM_POWDER = 1550
const REDSTONE_BEER = 1551
const BOLTERS_SMELLY_SOCKS = 1552
const NECKLACE = 906

const minimumLevel = 2

export class MinersFavor extends ListenerLogic {
    constructor() {
        super( 'Q00005_MinersFavor', 'listeners/tracked/MinersFavor.ts' )
        this.questItemIds = [ BOLTERS_LIST, MINING_BOOTS, MINERS_PICK, BOOMBOOM_POWDER, REDSTONE_BEER, BOLTERS_SMELLY_SOCKS ]
        this.questId = 5
    }

    checkCondition( player: L2PcInstance, state: QuestState ) {
        if ( QuestHelper.hasQuestItems( player, BOLTERS_LIST, MINING_BOOTS, MINERS_PICK, BOOMBOOM_POWDER, REDSTONE_BEER ) ) {
            state.setConditionWithSound( 2, true )
        }
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00005_MinersFavor'
    }

    getQuestStartIds(): Array<number> {
        return [ BOLTER ]
    }

    getTalkIds(): Array<number> {
        return [ BOLTER, SHARI, GARITA, REED, BRUNON ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30554-03.htm':
                state.startQuest()
                await QuestHelper.giveSingleItem( player, BOLTERS_LIST, 1 )
                await QuestHelper.giveSingleItem( player, BOLTERS_SMELLY_SOCKS, 1 )
                break

            case '30526-02.html':
                if ( !QuestHelper.hasQuestItems( player, BOLTERS_SMELLY_SOCKS ) ) {
                    return this.getPath( '30526-04.html' )
                }

                await QuestHelper.takeSingleItem( player, BOLTERS_SMELLY_SOCKS, -1 )
                await QuestHelper.giveSingleItem( player, MINERS_PICK, 1 )
                this.checkCondition( player, state )
                break

            case '30554-05.html':
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case BOLTER:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() >= minimumLevel ? '30554-02.htm' : '30554-01.html' )

                    case QuestStateValues.STARTED:
                        if ( state.isCondition( 1 ) ) {
                            return this.getPath( '30554-04.html' )
                        }

                        await QuestHelper.giveAdena( player, 2466, true )
                        await QuestHelper.addExpAndSp( player, 5672, 446 )
                        await QuestHelper.rewardSingleItem( player, NECKLACE, 1 )
                        await state.exitQuest( false, true )

                        player.sendOwnedData( ExShowScreenMessage.fromNpcMessageId( NpcStringIds.DELIVERY_DUTY_COMPLETE_N_GO_FIND_THE_NEWBIE_GUIDE, 2, 5000, null ) )

                        return this.getPath( '30554-06.html' )

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }
                break

            case BRUNON:
                if ( state.isStarted() ) {
                    return this.getPath( QuestHelper.hasQuestItems( player, MINERS_PICK ) ? '30526-03.html' : '30526-01.html' )
                }
                break

            case REED:
                return this.processItem( player, state, data.characterNpcId, REDSTONE_BEER )

            case SHARI:
                return this.processItem( player, state, data.characterNpcId, BOOMBOOM_POWDER )

            case GARITA:
                return this.processItem( player, state, data.characterNpcId, MINING_BOOTS )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async processItem( player: L2PcInstance, state: QuestState, npcId: number, itemId: number ): Promise<string> {
        if ( !state.isStarted() ) {
            return QuestHelper.getNoQuestMessagePath()
        }

        if ( QuestHelper.hasQuestItems( player, itemId ) ) {
            return this.getPath( `${ npcId }-02.html` )
        }

        await QuestHelper.giveSingleItem( player, itemId, 1 )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        this.checkCondition( player, state )

        return this.getPath( `${ npcId }-01.html` )
    }
}