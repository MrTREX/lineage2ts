import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'

const ROXXY = 30006
const BAULRO = 30033
const SIR_COLLIN = 30311

const BAULRO_LETTER = 7571
const SCROLL_OF_ESCAPE_GIRAN = 7559
const MARK_OF_TRAVELER = 7570

const minimumLevel = 3

export class StepIntoTheFuture extends ListenerLogic {
    constructor() {
        super( 'Q00006_StepIntoTheFuture', 'listeners/tracked/StepIntoTheFuture.ts' )
        this.questItemIds = [ BAULRO_LETTER ]
        this.questId = 6
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00006_StepIntoTheFuture'
    }

    getQuestStartIds(): Array<number> {
        return [ ROXXY ]
    }

    getTalkIds(): Array<number> {
        return [ ROXXY, BAULRO, SIR_COLLIN ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state ) {
            return null
        }

        let player = L2World.getPlayer( data.playerId )
        switch ( data.eventName ) {
            case '30006-03.htm':
                state.startQuest()
                break

            case '30006-06.html':
                await QuestHelper.rewardSingleItem( player, SCROLL_OF_ESCAPE_GIRAN, 1 )
                await QuestHelper.giveSingleItem( player, MARK_OF_TRAVELER, 1 )
                await state.exitQuest( false, true )
                break

            case '30033-02.html':
                state.setConditionWithSound( 2, true )
                await QuestHelper.giveSingleItem( player, BAULRO_LETTER, 1 )
                break

            case '30311-02.html':
                if ( !QuestHelper.hasQuestItems( player, BAULRO_LETTER ) ) {
                    return this.getPath( '30311-03.html' )
                }

                await QuestHelper.takeSingleItem( player, BAULRO_LETTER, -1 )
                state.setConditionWithSound( 3, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case ROXXY:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( ( player.getRace() === Race.HUMAN && player.getLevel() >= minimumLevel ) ? '30006-02.htm' : '30006-01.html' )

                    case QuestStateValues.STARTED:
                        if ( state.isCondition( 1 ) ) {
                            return this.getPath( '30006-04.html' )
                        }

                        if ( state.isCondition( 3 ) ) {
                            return this.getPath( '30006-05.html' )
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()

                }

                break

            case BAULRO:
                if ( state.isStarted() ) {
                    if ( state.isCondition( 1 ) ) {
                        return this.getPath( '30033-01.html' )
                    }

                    if ( state.isCondition( 2 ) ) {
                        return this.getPath( '30033-03.html' )
                    }
                }

                break

            case SIR_COLLIN:
                if ( state.isStarted() ) {
                    if ( state.isCondition( 2 ) ) {
                        return this.getPath( '30311-01.html' )
                    }

                    if ( state.isCondition( 3 ) ) {
                        return this.getPath( '30311-04.html' )
                    }
                }
                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}