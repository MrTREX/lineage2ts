import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { createItemDefinition, ItemDefinition } from '../../gameService/interface/ItemDefinition'

const CAPTAIN_BATHIA = 30332
const GUARD_BABENCO = 30334

const MALE_LIZARDMAN = 20919
const MALE_LIZARDMAN_SCOUT = 20920
const MALE_LIZARDMAN_GUARD = 20921
const GIANT_ARANE = 20925

const LIZ_NECKLACE_A = createItemDefinition( 7178, 100 )
const LIZ_NECKLACE_B = createItemDefinition( 7179, 100 )
const LIZ_PERFUME = createItemDefinition( 7180, 30 )
const LIZ_GEM = createItemDefinition( 7181, 30 )

const GREEN_HIGH_LURE = createItemDefinition( 6521, 60 )
const BABYDUCK_ROD = createItemDefinition( 6529, 1 )
const FISHING_SHOT_NONE = createItemDefinition( 6535, 500 )

const minimumLevel = 20

export class RedEyedInvaders extends ListenerLogic {
    constructor() {
        super( 'Q00039_RedEyedInvaders', 'listeners/tracked/RedEyedInvaders.ts' )
        this.questId = 39
        this.questItemIds = [ LIZ_NECKLACE_A.id, LIZ_NECKLACE_B.id, LIZ_PERFUME.id, LIZ_GEM.id ]
    }

    getAttackableKillIds(): Array<number> {
        return [ MALE_LIZARDMAN_GUARD, MALE_LIZARDMAN_SCOUT, MALE_LIZARDMAN, GIANT_ARANE ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00039_RedEyedInvaders'
    }

    getQuestStartIds(): Array<number> {
        return [ GUARD_BABENCO ]
    }

    getTalkIds(): Array<number> {
        return [ GUARD_BABENCO, CAPTAIN_BATHIA ]
    }

    private async awardItem( state : QuestState, data: AttackableKillEvent, questItem: ItemDefinition, requiredItem: ItemDefinition, nextCondition: number ) : Promise<void> {
        let partyMember = state.getPlayer()

        if ( !partyMember ) {
            return
        }

        if ( await QuestHelper.rewardUpToLimit( partyMember,questItem.id, 1, questItem.count, data.isChampion )
                && QuestHelper.hasItem( partyMember, requiredItem ) ) {
            state.setConditionWithSound( nextCondition, true )
            state.showQuestionMark( this.questId )
        }
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        let state: QuestState

        switch ( data.npcId ) {
            case MALE_LIZARDMAN:
                if ( Math.random() < QuestHelper.getAdjustedChance( LIZ_NECKLACE_A.id, 0.5, data.isChampion ) ) {
                    return
                }

                state = this.getRandomPartyMemberState( player, 2, 3, npc )
                if ( state ) {
                    await this.awardItem( state, data, LIZ_NECKLACE_A, LIZ_NECKLACE_B, 3 )
                }

                return

            case MALE_LIZARDMAN_SCOUT:
                if ( Math.random() > QuestHelper.getAdjustedChance( LIZ_NECKLACE_A.id, 0.5, data.isChampion ) ) {
                    state = this.getRandomPartyMemberState( player, 2, 3, npc )

                    if ( state ) {
                        await this.awardItem( state, data, LIZ_NECKLACE_A, LIZ_NECKLACE_B, 3 )
                        return
                    }
                }

                if ( Math.random() > QuestHelper.getAdjustedChance( LIZ_PERFUME.id, 0.25, data.isChampion ) ) {
                    state = this.getRandomPartyMemberState( player, 4, 3, npc )

                    if ( state ) {
                        await this.awardItem( state, data, LIZ_PERFUME, LIZ_GEM, 5 )
                        return
                    }
                }

                return

            case MALE_LIZARDMAN_GUARD:
                if ( Math.random() > QuestHelper.getAdjustedChance( LIZ_NECKLACE_B.id, 0.5, data.isChampion ) ) {
                    state = this.getRandomPartyMemberState( player, 2, 3, npc )

                    if ( state ) {
                        await this.awardItem( state, data, LIZ_NECKLACE_B, LIZ_NECKLACE_A, 3 )
                        return
                    }
                }

                if ( Math.random() > QuestHelper.getAdjustedChance( LIZ_PERFUME.id, 0.3, data.isChampion ) ) {
                    state = this.getRandomPartyMemberState( player, 4, 3, npc )
                    if ( state ) {
                        await this.awardItem( state, data, LIZ_PERFUME, LIZ_GEM, 5 )
                    }
                }

                return

            case GIANT_ARANE:
                if ( Math.random() < QuestHelper.getAdjustedChance( LIZ_GEM.id, 0.5, data.isChampion ) ) {
                    return
                }

                state = this.getRandomPartyMemberState( player, 4, 3, npc )
                if ( state ) {
                    await this.awardItem( state, data, LIZ_GEM, LIZ_PERFUME, 5 )
                }

                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30334-03.htm':
                if ( state.isCreated() ) {
                    state.startQuest()
                    break
                }

                return

            case '30332-02.html':
                if ( state.isCondition( 1 ) ) {
                    state.setConditionWithSound( 2, true )

                    break
                }

                return

            case '30332-05.html':
                if ( state.isCondition( 3 ) ) {
                    if ( QuestHelper.hasEachItem( player, [ LIZ_NECKLACE_A, LIZ_NECKLACE_B ] ) ) {
                        state.setConditionWithSound( 4, true )
                        await QuestHelper.removeAllItems( player, [ LIZ_NECKLACE_A, LIZ_NECKLACE_B ], true )

                        break
                    }

                    return this.getPath( '30332-06.html' )
                }

                return

            case '30332-09.html':
                if ( state.isCondition( 5 ) ) {
                    if ( QuestHelper.hasEachItem( player, [ LIZ_PERFUME, LIZ_GEM ] ) ) {
                        await QuestHelper.removeAllItems( player, [ LIZ_PERFUME, LIZ_GEM ], true )
                        await QuestHelper.rewardSingleItem( player, GREEN_HIGH_LURE.id, GREEN_HIGH_LURE.count )
                        await QuestHelper.rewardSingleItem( player, BABYDUCK_ROD.id, BABYDUCK_ROD.count )
                        await QuestHelper.rewardSingleItem( player, FISHING_SHOT_NONE.id, FISHING_SHOT_NONE.count )
                        await QuestHelper.addExpAndSp( player, 62366, 2783 )

                        await state.exitQuest( false, true )
                    }

                    return this.getPath( '30332-10.html' )
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name, true )

        switch ( data.characterNpcId ) {
            case CAPTAIN_BATHIA:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '30332-01.html' )

                    case 2:
                        return this.getPath( '30332-03.html' )

                    case 3:
                        return this.getPath( '30332-04.html' )

                    case 4:
                        return this.getPath( '30332-07.html' )

                    case 5:
                        return this.getPath( '30332-08.html' )
                }

                break

            case GUARD_BABENCO:
                if ( state.isCreated() ) {
                    let player = L2World.getPlayer( data.playerId )
                    return this.getPath( player.getLevel() >= minimumLevel ? '30334-01.htm' : '30334-02.htm' )
                }

                if ( state.isStarted() && state.isCondition( 1 ) ) {
                    return this.getPath( '30334-04.html' )
                }

                if ( state.isCompleted() ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

        }

        return QuestHelper.getNoQuestMessagePath()
    }
}