import { SagaLogic, SagaPlaceholders } from '../helpers/SagaLogic'
import { ILocational, Location } from '../../gameService/models/Location'

const npcIds: Array<number> = [
    30850,
    31624,
    31298,
    31276,
    31595,
    31646,
    31648,
    31650,
    31654,
    31655,
    31657,
    31522,
]

const itemIds: Array<number> = [
    7080,
    7538,
    7081,
    7489,
    7272,
    7303,
    7334,
    7365,
    7396,
    7427,
    7097,
    6480,
]

const mobIds: Array<number> = [
    27290,
    27223,
    27282,
]

const spawnLocations: Array<ILocational> = [
    new Location( 191046, -40640, -3042 ),
    new Location( 46087, -36372, -1685 ),
    new Location( 46066, -36396, -1685 ),
]

export class SagaOfTheDreadnought extends SagaLogic {
    constructor() {
        super( 'Q00074_SagaOfTheDreadnought', 'listeners/tracked-100/SagaOfTheDreadnought.ts' )
        this.questId = 74
    }

    getAllItemIds(): Array<number> {
        return itemIds
    }

    getAllMobIds(): Array<number> {
        return mobIds
    }

    getAllNpcIds(): Array<number> {
        return npcIds
    }

    getAllSpawnLocations(): Array<ILocational> {
        return spawnLocations
    }

    getClassId(): number {
        return 89
    }

    getPreviousClassId(): number {
        return 0x03
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00074_SagaOfTheDreadnought'
    }
}