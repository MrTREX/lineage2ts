import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const LANOSCO = 31570
const SHALING = 31434

const treasureBox = 6500
const blueprint = 7625
const blackPearlRing = 880

const minimumLevel = 27

export class ChestCaughtWithABaitOfWind extends ListenerLogic {
    constructor() {
        super( 'Q00027_ChestCaughtWithABaitOfWind', 'listeners/tracked/ChestCaughtWithABaitOfWind.ts' )
        this.questId = 27
        this.questItemIds = [ blueprint, treasureBox ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00027_ChestCaughtWithABaitOfWind'
    }

    getQuestStartIds(): Array<number> {
        return [ LANOSCO ]
    }

    getTalkIds(): Array<number> {
        return [ LANOSCO, SHALING ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31570-03.htm':
                state.startQuest()
                break

            case '31570-05.htm':
                if ( state.isCondition( 1 ) && QuestHelper.hasQuestItem( player, treasureBox ) ) {
                    state.setConditionWithSound( 2, true )

                    await QuestHelper.giveSingleItem( player, blueprint, 1 )
                    await QuestHelper.takeSingleItem( player, treasureBox, 1 )

                    return this.getPath( '31570-06.htm' )
                }

                break

            case '31434-02.htm':
                if ( state.isCondition( 2 ) && QuestHelper.hasQuestItem( player, blueprint ) ) {
                    await QuestHelper.rewardSingleItem( player, blackPearlRing, 1 )
                    await state.exitQuest( false, true )

                    return this.getPath( '31434-01.htm' )
                }

                break
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()

            case QuestStateValues.CREATED:
                if ( data.characterNpcId === LANOSCO ) {
                    if ( ( player.getLevel() >= minimumLevel ) && player.hasQuestCompleted( 'Q00050_LanoscosSpecialBait' ) ) {
                        return this.getPath( '31570-01.htm' )
                    }

                    return this.getPath( '31570-02.htm' )
                }
                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case LANOSCO:
                        if ( state.isCondition( 1 ) ) {
                            if ( QuestHelper.hasQuestItem( player, treasureBox ) ) {
                                return this.getPath( '31570-04.htm' )
                            }

                            return this.getPath( '31570-05.htm' )
                        }

                        return this.getPath( '31570-07.htm' )

                    case SHALING:
                        if ( state.isCondition( 2 ) ) {
                            return this.getPath( '31434-00.htm' )
                        }

                        break
                }
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}