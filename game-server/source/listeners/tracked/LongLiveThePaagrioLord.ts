import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { L2World } from '../../gameService/L2World'
import { Race } from '../../gameService/enums/Race'
import { QuestHelper } from '../helpers/QuestHelper'
import { ExShowScreenMessage } from '../../gameService/packets/send/ExShowScreenMessage'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'

const KUNAI = 30559
const USKA = 30560
const GROOKIN = 30562
const VARKEES = 30566
const NAKUSIN = 30578
const HESTUI = 30585
const URUTU = 30587

const CLUB = 4
const HONEY_KHANDAR = 1541
const BEAR_FUR_CLOAK = 1542
const BLOODY_AXE = 1543
const ANCESTOR_SKULL = 1544
const SPIDER_DUST = 1545
const DEEP_SEA_ORB = 1546

const mininumLevel = 2

export class LongLiveThePaagrioLord extends ListenerLogic {
    constructor() {
        super( 'Q00004_LongLiveThePaagrioLord', 'listeners/tracked/LongLiveThePaagrioLord.ts' )
        this.questItemIds = [ HONEY_KHANDAR, BEAR_FUR_CLOAK, BLOODY_AXE, ANCESTOR_SKULL, SPIDER_DUST, DEEP_SEA_ORB ]
        this.questId = 4
    }

    getQuestStartIds(): Array<number> {
        return [ NAKUSIN ]
    }

    getTalkIds(): Array<number> {
        return [ NAKUSIN, VARKEES, URUTU, HESTUI, KUNAI, USKA, GROOKIN ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00004_LongLiveThePaagrioLord'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state : QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30578-03.htm':
                state.startQuest()
                break

            case '30578-05.html':
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state : QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case NAKUSIN:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        if ( player.getRace() !== Race.ORC ) {
                            return this.getPath( '30578-00.htm' )
                        }

                        return this.getPath( player.getLevel() >= mininumLevel ? '30578-02.htm' : '30578-01.htm' )

                    case QuestStateValues.STARTED:
                        if ( state.isCondition( 1 ) ) {
                            return this.getPath( '30578-04.html' )
                        }

                        await QuestHelper.giveSingleItem( player, CLUB, 1 )

                        player.sendOwnedData( ExShowScreenMessage.fromNpcMessageId( NpcStringIds.DELIVERY_DUTY_COMPLETE_N_GO_FIND_THE_NEWBIE_GUIDE, 2, 5000, null ) )
                        await QuestHelper.addExpAndSp( player, 4254, 335 )
                        await QuestHelper.giveAdena( player, 1850, true )

                        await state.exitQuest( false, true )

                        return this.getPath( '30578-06.html' )

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                return QuestHelper.getNoQuestMessagePath()

            case VARKEES:
                return this.giveItem( player, state, data.characterNpcId, HONEY_KHANDAR )

            case URUTU:
                return this.giveItem( player, state, data.characterNpcId, DEEP_SEA_ORB )

            case HESTUI:
                return this.giveItem( player, state, data.characterNpcId, BEAR_FUR_CLOAK )

            case KUNAI:
                return this.giveItem( player, state, data.characterNpcId, SPIDER_DUST )

            case USKA:
                return this.giveItem( player, state, data.characterNpcId, ANCESTOR_SKULL )

            case GROOKIN:
                return this.giveItem( player, state, data.characterNpcId, BLOODY_AXE )
        }
    }

    async giveItem( player: L2PcInstance, state: QuestState, npcId: number, itemId: number ) : Promise<string> {
        if ( !state.isStarted() ) {
            return QuestHelper.getNoQuestMessagePath()
        }

        if ( QuestHelper.hasQuestItems( player, itemId ) ) {
            return this.getPath( `${npcId}-02.html` )
        }

        await QuestHelper.giveSingleItem( player, itemId, 1 )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

        if ( QuestHelper.hasQuestItems( player, ...this.questItemIds ) ) {
            state.setConditionWithSound( 2, true )
        }

        return this.getPath( `${npcId}-01.html` )
    }
}