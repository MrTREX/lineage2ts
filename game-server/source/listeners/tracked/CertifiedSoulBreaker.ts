import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import { AttackableKillEvent, NpcGeneralEvent, NpcSpawnEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'
import { QuestHelper } from '../helpers/QuestHelper'
import { CategoryType } from '../../gameService/enums/CategoryType'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'
import { DataManager } from '../../data/manager'
import { QuestVariables } from '../helpers/QuestVariables'
import { AIEffectHelper } from '../../gameService/aicontroller/helpers/AIEffectHelper'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const CAPTAIN_LUCAS = 30071
const JACOB = 30073
const GUARD_HARLAN = 30074
const GUARD_XABER = 30075
const GUARD_LIAM = 30076
const GUARD_VESA = 30123
const GUARD_ZEROME = 30124
const WHARF_MANAGER_FELTON = 30879
const KEKROPUS = 32138
const VICE_HIERARCH_CASCA = 32139
const GRAND_MASTER_HOLST = 32199
const GRAND_MASTER_VITUS = 32213
const GRAND_MASTER_MELDINA = 32214
const KATENAR = 32242
const CARGO_BOX = 32243
const SUSPICIOUS_MAN = 32244

const SEALED_DOCUMENT = 9803
const WYRM_HEART = 9804
const KEKROPUS_RECOMMENDATION = 9805

const DIMENSIONAL_DIAMOND = 7562
const SOUL_BREAKER_CERTIFICATE = 9806

const WYRM = 20176
const GUARDIAN_ANGEL = 27332

const minimumLevel = 39

const suspiciousSpawnLocation = new Location( 16489, 146249, -3112 )
const movementLocation = new Location( 16490, 145839, -3080 )

export class CertifiedSoulBreaker extends ListenerLogic {
    constructor() {
        super( 'Q00065_CertifiedSoulBreaker', 'listeners/tracked/CertifiedSoulBreaker.ts' )
        this.questId = 65
        this.questItemIds = [ SEALED_DOCUMENT, WYRM_HEART, KEKROPUS_RECOMMENDATION ]
    }

    getAttackableKillIds(): Array<number> {
        return [ WYRM, GUARDIAN_ANGEL ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00065_CertifiedSoulBreaker'
    }

    getQuestStartIds(): Array<number> {
        return [ GRAND_MASTER_VITUS ]
    }

    getSpawnIds(): Array<number> {
        return [ GUARDIAN_ANGEL, SUSPICIOUS_MAN ]
    }

    getTalkIds(): Array<number> {
        return [
            GRAND_MASTER_VITUS,
            CAPTAIN_LUCAS,
            JACOB,
            GUARD_HARLAN,
            GUARD_XABER,
            GUARD_LIAM,
            GUARD_VESA,
            GUARD_ZEROME,
            WHARF_MANAGER_FELTON,
            KEKROPUS,
            VICE_HIERARCH_CASCA,
            GRAND_MASTER_HOLST,
            GRAND_MASTER_MELDINA,
            KATENAR,
            CARGO_BOX,
            SUSPICIOUS_MAN,
        ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, npc, player, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case WYRM:
                if ( state.isMemoState( 22 ) ) {
                    if ( Math.random() > QuestHelper.getAdjustedChance( WYRM_HEART, 0.20, data.isChampion ) ) {
                        return
                    }

                    if ( await QuestHelper.rewardAndProgressState( player, state, WYRM_HEART, 1, 10, data.isChampion, 16 ) ) {
                        state.setMemoState( 23 )
                        return
                    }
                }

                return

            case GUARDIAN_ANGEL:
                let playerId = NpcVariablesManager.get( data.targetId, 'playerId' ) as number

                if ( playerId === data.playerId && state.isMemoState( 12 ) ) {
                    let katenar: L2Npc = QuestHelper.addGenericSpawn( null, KATENAR, player.getX() + 20, player.getY() + 20, player.getZ(), 0, false, 0, false, player.getInstanceId() )

                    NpcVariablesManager.set( katenar.getObjectId(), 'playerId', data.playerId )
                    NpcVariablesManager.set( katenar.getObjectId(), 'npcObjectId', data.targetId )

                    state.setMemoState( 13 )

                    BroadcastHelper.broadcastNpcSayStringId(
                            npc,
                            NpcSayType.NpcAll,
                            NpcStringIds.GRR_IVE_BEEN_HIT )

                    return
                }

                let npcObjectId = NpcVariablesManager.get( data.targetId, 'npcObjectId' ) as number
                if ( npcObjectId > 0 && NpcVariablesManager.get( npcObjectId, 'SPAWNED' ) ) {
                    NpcVariablesManager.set( npcObjectId, 'SPAWNED', false )
                }

                BroadcastHelper.broadcastNpcSayStringId(
                        npc,
                        NpcSayType.NpcAll,
                        NpcStringIds.GRR_WHO_ARE_YOU_AND_WHY_HAVE_YOU_STOPPED_ME )

                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let currentNpc = L2World.getObjectById( data.characterId ) as L2Npc

        switch ( data.eventName ) {
            case 'DESPAWN_70':
                let npcObjectId = NpcVariablesManager.get( data.characterId, 'npcObjectId' ) as number
                if ( npcObjectId > 0 && NpcVariablesManager.get( data.characterNpcId, 'isSpawned' ) ) {
                    NpcVariablesManager.set( data.characterNpcId, 'isSpawned', false )

                    let storedPlayer = L2World.getPlayer( NpcVariablesManager.get( data.characterNpcId, 'playerId' ) as number )
                    if ( storedPlayer ) {
                        BroadcastHelper.broadcastNpcSayStringId(
                                currentNpc,
                                NpcSayType.NpcAll,
                                NpcStringIds.S1_I_WILL_BE_BACK_SOON_STAY_THERE_AND_DONT_YOU_DARE_WANDER_OFF,
                                storedPlayer.getAppearance().getVisibleName(),
                        )
                    }
                }

                if ( currentNpc ) {
                    await currentNpc.deleteMe()
                }

                return

            case 'DESPAWN_5':
                if ( currentNpc ) {
                    await currentNpc.deleteMe()
                }

                return
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( state.isCreated() ) {
                    state.startQuest()
                    state.setMemoState( 1 )

                    if ( !PlayerVariablesManager.get( data.playerId, QuestVariables.secondClassDiamondReward ) ) {
                        PlayerVariablesManager.set( data.playerId, QuestVariables.secondClassDiamondReward, true )
                        await QuestHelper.rewardSingleItem( player, DIMENSIONAL_DIAMOND, 47 )

                        return this.getPath( '32213-05.htm' )
                    }

                    return this.getPath( '32213-06.htm' )
                }

                return

            case '32213-04.htm':
                if ( player.getLevel() >= minimumLevel && player.isInCategory( CategoryType.KAMAEL_SECOND_CLASS_GROUP ) ) {
                    break
                }

                return

            case '30071-02.html':
                if ( state.isMemoState( 7 ) ) {
                    state.setMemoState( 8 )
                    state.setConditionWithSound( 8, true )

                    break
                }

                return

            case '30879-02.html':
                if ( state.isMemoState( 11 ) ) {
                    break
                }

                return

            case '30879-03.html':
                if ( state.isMemoState( 11 ) ) {
                    state.setMemoState( 12 )
                    state.setConditionWithSound( 12, true )

                    break
                }

                return

            case '32138-02.html':
            case '32138-03.html':
                if ( state.isMemoState( 1 ) ) {
                    break
                }

                return

            case '32138-04.html':
                if ( state.isMemoState( 1 ) ) {
                    state.setMemoState( 2 )
                    state.setConditionWithSound( 2, true )

                    break
                }

                return

            case '32138-07.html':
                if ( state.isMemoState( 21 ) ) {
                    state.setMemoState( 22 )
                    state.setConditionWithSound( 15, true )

                    break
                }

                return

            case '32138-10.html':
            case '32138-11.html':
                if ( state.isMemoState( 23 ) ) {
                    break
                }

                return

            case '32138-12.html':
                if ( state.isMemoState( 23 ) ) {
                    await QuestHelper.takeSingleItem( player, WYRM_HEART, -1 )
                    await QuestHelper.giveSingleItem( player, KEKROPUS_RECOMMENDATION, 1 )

                    state.setMemoState( 24 )
                    state.setConditionWithSound( 17, true )

                    break
                }

                return

            case '32139-02.html':
                if ( state.isMemoState( 2 ) ) {
                    state.setMemoState( 3 )
                    state.setConditionWithSound( 3, true )

                    break
                }

                return

            case '32139-04.html':
                if ( state.isMemoState( 3 ) ) {
                    state.setMemoState( 4 )
                    state.setConditionWithSound( 4, true )

                    break
                }

                return

            case '32139-07.html':
                if ( state.isMemoState( 14 ) ) {
                    break
                }

                return

            case '32139-08.html':
                if ( state.isMemoState( 14 ) ) {
                    await QuestHelper.takeSingleItem( player, SEALED_DOCUMENT, -1 )

                    state.setMemoState( 21 )
                    state.setConditionWithSound( 14, true )

                    return
                }

                return

            case '32199-02.html':
                if ( state.isMemoState( 4 ) ) {
                    state.setMemoState( 5 )
                    state.setConditionWithSound( 5, true )


                    QuestHelper.addSummonedSpawnAtLocation( currentNpc, SUSPICIOUS_MAN, suspiciousSpawnLocation, false, 0 )

                    break
                }

                return

            case '32214-02.html':
                if ( state.isMemoState( 10 ) ) {
                    state.setMemoState( 11 )
                    state.setConditionWithSound( 11, true )

                    break
                }

                return
        }

        return this.getPath( data.eventName )
    }

    async onSpawnEvent( data: NpcSpawnEvent ): Promise<void> {
        let npc = L2World.getObjectById( data.characterId ) as L2Npc

        switch ( data.npcId ) {
            case SUSPICIOUS_MAN:
                this.startQuestTimer( 'DESPAWN_5', 5000, data.characterId, null )
                BroadcastHelper.broadcastNpcSayStringId(
                        npc,
                        NpcSayType.NpcAll,
                        NpcStringIds.DRATS_HOW_COULD_I_BE_SO_WRONG )

                AIEffectHelper.notifyMove( npc, movementLocation )

                return

            case GUARDIAN_ANGEL:
                this.startQuestTimer( 'DESPAWN_70', 70000, data.characterId, null )
                let player = L2World.getPlayer( NpcVariablesManager.get( data.characterId, 'playerId' ) as number )
                if ( player ) {
                    BroadcastHelper.broadcastNpcSayStringId(
                            npc,
                            NpcSayType.NpcAll,
                            NpcStringIds.S1_STEP_BACK_FROM_THE_CONFOUNDED_BOX_I_WILL_TAKE_IT_MYSELF,
                            player.getAppearance().getVisibleName(),
                    )
                }

                return
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === GRAND_MASTER_VITUS ) {
                    if ( player.getRace() === Race.KAMAEL ) {
                        if ( ( player.getLevel() >= minimumLevel ) && player.isInCategory( CategoryType.KAMAEL_SECOND_CLASS_GROUP ) ) {
                            return this.getPath( '32213-01.htm' )
                        }

                        return this.getPath( '32213-03.html' )
                    }

                    return this.getPath( '32213-02.html' )
                }

                break

            case QuestStateValues.STARTED:
                let memoState = state.getMemoState()
                let memoStateEx = state.getMemoStateEx( 1 )

                switch ( data.characterNpcId ) {
                    case GRAND_MASTER_VITUS:
                        if ( memoState === 1 ) {
                            return this.getPath( '32213-07.html' )
                        }

                        if ( memoState > 1 && memoState < 24 ) {
                            return this.getPath( '32213-08.html' )
                        }

                        if ( memoState === 24 ) {
                            await QuestHelper.giveAdena( player, 71194, true )
                            await QuestHelper.giveSingleItem( player, SOUL_BREAKER_CERTIFICATE, 1 )
                            await QuestHelper.addExpAndSp( player, 393750, 27020 )
                            await state.exitQuest( false, true )

                            player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )

                            return this.getPath( '32213-10.html' )
                        }

                        break

                    case CAPTAIN_LUCAS:
                        if ( memoState === 7 ) {
                            return this.getPath( '30071-01.html' )
                        }

                        if ( memoState === 8 ) {
                            return this.getPath( '30071-03.html' )
                        }

                        break

                    case JACOB:
                        if ( memoState === 6 ) {
                            if ( memoStateEx === 0 ) {
                                state.setMemoStateEx( 1, 10 )
                                return this.getPath( '30073-01.html' )
                            }

                            if ( memoStateEx === 10 ) {
                                return this.getPath( '30073-01a.html' )
                            }

                            if ( memoStateEx === 1 ) {
                                state.setMemoState( 7 )
                                state.setMemoStateEx( 1, 0 )

                                state.setConditionWithSound( 7, true )

                                return this.getPath( '30073-02.html' )
                            }
                        }

                        if ( memoState === 7 ) {
                            return this.getPath( '30073-03.html' )
                        }

                        break

                    case GUARD_HARLAN:
                        if ( memoState === 6 ) {
                            if ( memoStateEx === 0 ) {
                                state.setMemoStateEx( 1, 1 )

                                return this.getPath( '30074-01.html' )
                            }

                            if ( memoStateEx === 1 ) {
                                return this.getPath( '30074-01a.html' )
                            }

                            if ( memoStateEx === 10 ) {
                                state.setMemoState( 7 )
                                state.setMemoStateEx( 1, 0 )
                                state.setConditionWithSound( 7, true )

                                return this.getPath( '30074-02.html' )
                            }
                        }

                        if ( memoState === 7 ) {
                            return this.getPath( '30074-03.html' )
                        }

                        break

                    case GUARD_XABER:
                        if ( memoState === 8 ) {
                            if ( memoStateEx === 0 ) {
                                state.setMemoStateEx( 1, 1 )
                                return this.getPath( '30075-01.html' )
                            }

                            if ( memoStateEx === 1 ) {
                                return this.getPath( '30075-01a.html' )
                            }

                            if ( memoStateEx === 10 ) {
                                state.setMemoState( 9 )
                                state.setMemoStateEx( 1, 0 )
                                state.setConditionWithSound( 9, true )

                                return this.getPath( '30075-02.html' )
                            }
                        }

                        if ( memoState === 9 ) {
                            return this.getPath( '30075-03.html' )
                        }

                        break

                    case GUARD_LIAM:
                        if ( memoState === 8 ) {
                            if ( memoStateEx === 0 ) {
                                state.setMemoStateEx( 1, 10 )
                                return this.getPath( '30076-01.html' )
                            }

                            if ( memoStateEx === 10 ) {
                                return this.getPath( '30076-01a.html' )
                            }

                            if ( memoStateEx === 1 ) {
                                state.setMemoState( 9 )
                                state.setMemoStateEx( 1, 0 )
                                state.setConditionWithSound( 9, true )
                                return this.getPath( '30076-02.html' )
                            }
                        }

                        if ( memoState === 9 ) {
                            return this.getPath( '30076-03.html' )
                        }

                        break

                    case GUARD_VESA:
                        if ( memoState === 9 ) {
                            if ( memoStateEx === 0 ) {
                                state.setMemoStateEx( 1, 10 )
                                return this.getPath( '30123-01.html' )
                            }

                            if ( memoStateEx === 10 ) {
                                return this.getPath( '30123-01.html' )
                            }

                            if ( memoStateEx === 1 ) {
                                state.setMemoState( 10 )
                                state.setMemoStateEx( 1, 0 )
                                state.setConditionWithSound( 10, true )
                                return this.getPath( '30123-02.html' )
                            }
                        }

                        if ( memoState === 10 ) {
                            return this.getPath( '30123-03.html' )
                        }

                        break

                    case GUARD_ZEROME:
                        if ( memoState === 9 ) {
                            if ( memoStateEx === 0 ) {
                                state.setMemoStateEx( 1, 1 )
                                return this.getPath( '30124-01.html' )
                            }

                            if ( memoStateEx === 1 ) {
                                return this.getPath( '30124-01.html' )
                            }

                            if ( memoStateEx === 10 ) {
                                state.setMemoState( 10 )
                                state.setMemoStateEx( 1, 0 )
                                state.setConditionWithSound( 10, true )
                                return this.getPath( '30124-02.html' )
                            }
                        }

                        if ( memoState === 10 ) {
                            return this.getPath( '30124-03.html' )
                        }

                        break

                    case WHARF_MANAGER_FELTON:
                        if ( memoState === 11 ) {
                            return this.getPath( '30879-01.html' )
                        }

                        if ( memoState === 12 ) {
                            return this.getPath( '30879-04.html' )
                        }

                        break

                    case KEKROPUS:
                        if ( memoState === 1 ) {
                            return DataManager.getHtmlData().getItem( '32138-01.html' )
                                    .replace( '%name1%', player.getName() )
                        }

                        if ( memoState === 2 ) {
                            return this.getPath( '32138-05.html' )
                        }

                        if ( memoState === 21 ) {
                            return this.getPath( '32138-06.html' )
                        }

                        if ( memoState === 22 ) {
                            return this.getPath( '32138-08.html' )
                        }

                        if ( memoState === 23 ) {
                            return this.getPath( '32138-09.html' )
                        }

                        if ( memoState === 24 ) {
                            return this.getPath( '32138-13.html' )
                        }

                        break

                    case VICE_HIERARCH_CASCA:
                        if ( memoState === 2 ) {
                            return this.getPath( '32139-01.html' )
                        }

                        if ( memoState === 3 ) {
                            return this.getPath( '32139-03.html' )
                        }

                        if ( memoState === 4 ) {
                            return this.getPath( '32139-05.html' )
                        }

                        if ( memoState === 14 ) {
                            return this.getPath( '32139-06.html' )
                        }

                        if ( memoState === 21 ) {
                            return this.getPath( '32139-09.html' )
                        }

                        break

                    case GRAND_MASTER_HOLST:
                        if ( memoState === 4 ) {
                            return this.getPath( '32199-01.html' )
                        }

                        if ( memoState === 5 ) {
                            state.setMemoState( 6 )
                            state.setMemoStateEx( 1, 0 ) // Custom line
                            state.setConditionWithSound( 6, true )
                            return this.getPath( '32199-03.html' )
                        }

                        if ( memoState === 6 ) {
                            return this.getPath( '32199-04.html' )
                        }

                        break

                    case GRAND_MASTER_MELDINA:
                        if ( memoState === 10 ) {
                            return this.getPath( '32214-01.html' )
                        }

                        if ( memoState === 11 ) {
                            return this.getPath( '32214-03.html' )
                        }

                        break

                    case CARGO_BOX:
                        if ( memoState === 12 ) {
                            if ( !NpcVariablesManager.get( data.characterId, 'SPAWNED' ) ) {
                                NpcVariablesManager.set( data.characterId, 'SPAWNED', true )
                                NpcVariablesManager.set( data.characterId, 'playerId', data.playerId )

                                let angel: L2Npc = QuestHelper.addGenericSpawn( null, GUARDIAN_ANGEL, 36110, 191921, -3712, 0, true, 0, false, player.getInstanceId() )

                                NpcVariablesManager.set( angel.getObjectId(), 'playerId', data.playerId )
                                NpcVariablesManager.set( angel.getObjectId(), 'npcObjectId', data.characterId )

                                await AIEffectHelper.notifyAttackedWithTargetId( angel, data.playerId, 1, 1 )

                                return this.getPath( '32243-01.html' )
                            }

                            if ( NpcVariablesManager.get( data.characterId, 'playerId' ) === data.playerId ) {
                                return this.getPath( '32243-03.html' )
                            }

                            return this.getPath( '32243-02.html' )
                        }

                        if ( memoState === 13 ) {
                            if ( !NpcVariablesManager.get( data.characterId, 'SPAWNED' ) ) {
                                NpcVariablesManager.set( data.characterId, 'SPAWNED', true )
                                NpcVariablesManager.set( data.characterId, 'playerId', data.playerId )

                                let katenar: L2Npc = QuestHelper.addGenericSpawn( null, KATENAR, 36110, 191921, -3712, 0, false, 0, false, player.getInstanceId() )

                                NpcVariablesManager.set( katenar.getObjectId(), 'playerId', data.playerId )
                                NpcVariablesManager.set( katenar.getObjectId(), 'npcObjectId', data.characterId )

                                return this.getPath( '32243-06.html' )
                            }

                            if ( NpcVariablesManager.get( data.characterId, 'playerId' ) === data.playerId ) {
                                return this.getPath( '32243-04.html' )
                            }

                            return this.getPath( '32243-05.html' )
                        }

                        if ( memoState === 14 ) {
                            return this.getPath( '32243-07.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === GRAND_MASTER_VITUS ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}