import { SagaLogic, SagaPlaceholders } from '../helpers/SagaLogic'
import { ILocational, Location } from '../../gameService/models/Location'

const npcIds : Array<number> = [
    30702,
    31587,
    31604,
    31640,
    31635,
    31646,
    31649,
    31652,
    31654,
    31655,
    31659,
    31641
]

const itemIds : Array<number> = [
    7080,
    7521,
    7081,
    7499,
    7282,
    7313,
    7344,
    7375,
    7406,
    7437,
    7107,
    0
]

const mobIds : Array<number> = [
    27298,
    27233,
    27307
]

const spawnLocations : Array<ILocational> = [
    new Location( 161719, -92823, -1893 ),
    new Location( 124376, 82127, -2796 ),
    new Location( 124376, 82127, -2796 )
]

export class SagaOfTheGhostSentinel extends SagaLogic {
    constructor() {
        super( 'Q00084_SagaOfTheGhostSentinel', 'listeners/tracked/SagaOfTheGhostSentinel.ts' )
        this.questId = 84
    }

    getAllItemIds(): Array<number> {
        return itemIds
    }

    getAllMobIds(): Array<number> {
        return mobIds
    }

    getAllNpcIds(): Array<number> {
        return npcIds
    }

    getAllSpawnLocations(): Array<ILocational> {
        return spawnLocations
    }

    getClassId(): number {
        return 109
    }

    getPreviousClassId(): number {
        return 0x25
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00084_SagaOfTheGhostSentinel'
    }
}