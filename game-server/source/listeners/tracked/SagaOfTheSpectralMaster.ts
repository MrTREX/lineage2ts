import { SagaLogic } from '../helpers/SagaLogic'
import { ILocational, Location } from '../../gameService/models/Location'

const npcIds: Array<number> = [
    30175,
    31287,
    31613,
    30175,
    31632,
    31646,
    31649,
    31653,
    31654,
    31655,
    31656,
    31613,
]

const itemIds: Array<number> = [
    7080,
    7606,
    7081,
    7508,
    7291,
    7322,
    7353,
    7384,
    7415,
    7446,
    7112,
    0,
]

const mobIds: Array<number> = [
    27315,
    27242,
    27312,
]

const spawnLocations: Array<ILocational> = [
    new Location( 164650, -74121, -2871 ),
    new Location( 47429, -56923, -2383 ),
    new Location( 47391, -56929, -2370 ),
]

export class SagaOfTheSpectralMaster extends SagaLogic {
    constructor() {
        super( 'Q00093_SagaOfTheSpectralMaster', 'listeners/tracked/SagaOfTheSpectralMaster.ts' )
        this.questId = 93
    }

    getAllItemIds(): Array<number> {
        return itemIds
    }

    getAllMobIds(): Array<number> {
        return mobIds
    }

    getAllNpcIds(): Array<number> {
        return npcIds
    }

    getAllSpawnLocations(): Array<ILocational> {
        return spawnLocations
    }

    getClassId(): number {
        return 111
    }

    getPreviousClassId(): number {
        return 0x29
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00093_SagaOfTheSpectralMaster'
    }
}