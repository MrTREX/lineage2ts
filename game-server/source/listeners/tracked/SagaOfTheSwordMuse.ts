import { SagaLogic, SagaPlaceholders } from '../helpers/SagaLogic'
import { ILocational, Location } from '../../gameService/models/Location'

const npcIds: Array<number> = [
    30853,
    31624,
    31583,
    31537,
    31618,
    31646,
    31649,
    31652,
    31654,
    31655,
    31659,
    31281,
]

const itemIds: Array<number> = [
    7080,
    7536,
    7081,
    7487,
    7270,
    7301,
    7332,
    7363,
    7394,
    7425,
    7095,
    6482,
]

const mobIds: Array<number> = [
    27288,
    27221,
    27280,
]

const spawnLocations: Array<ILocational> = [
    new Location( 161719, -92823, -1893 ),
    new Location( 124355, 82155, -2803 ),
    new Location( 124376, 82127, -2796 ),
]

const textLines: Array<string> = [
    `${ SagaPlaceholders.playerName }! Pursued to here! However, I jumped out of the Banshouren boundaries! You look at the giant as the sign of power!`,
    '... Oh ... good! So it was ... let\'s begin!',
    'I do not have the patience ..! I have been a giant force ...! Cough chatter ah ah ah!',
    `Paying homage to those who disrupt the orderly will be ${ SagaPlaceholders.playerName }'s death!`,
    'Now, my soul freed from the shackles of the millennium, Halixia, to the back side I come ...',
    'Why do you interfere others\' battles?',
    'This is a waste of time.. Say goodbye...!',
    '...That is the enemy',
    `...Goodness! ${ SagaPlaceholders.playerName } you are still looking?`,
    `${ SagaPlaceholders.playerName } ... Not just to whom the victory. Only personnel involved in the fighting are eligible to share in the victory.`,
    `Your sword is not an ornament. Don't you think, ${ SagaPlaceholders.playerName }?`,
    'Goodness! I no longer sense a battle there now.',
    'let...',
    'Only engaged in the battle to bar their choice. Perhaps you should regret.',
    'The human nation was foolish to try and fight a giant\'s strength.',
    'Must...Retreat... Too...Strong.',
    `${ SagaPlaceholders.playerName }. Defeat...by...retaining...and...Mo...Hacker`,
    '....! Fight...Defeat...It...Fight...Defeat...It...',
]

export class SagaOfTheSwordMuse extends SagaLogic {
    constructor() {
        super( 'Q00072_SagaOfTheSwordMuse', 'listeners/tracked-100/SagaOfTheSwordMuse.ts' )
        this.questId = 72
    }

    getAllItemIds(): Array<number> {
        return itemIds
    }

    getAllMobIds(): Array<number> {
        return mobIds
    }

    getAllNpcIds(): Array<number> {
        return npcIds
    }

    getAllSpawnLocations(): Array<ILocational> {
        return spawnLocations
    }

    getClassId(): number {
        return 100
    }

    getPreviousClassId(): number {
        return 0x15
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00072_SagaOfTheSwordMuse'
    }
}