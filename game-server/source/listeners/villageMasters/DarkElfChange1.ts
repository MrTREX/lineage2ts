import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { CategoryType } from '../../gameService/enums/CategoryType'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { QuestHelper } from '../helpers/QuestHelper'
import { Race } from '../../gameService/enums/Race'

import _ from 'lodash'

const npcIds: Array<number> = [
    30290, // Xenos
    30297, // Tobias
    30462, // Tronix
    32096, // Helminter
]

const GAZE_OF_ABYSS = 1244
const IRON_HEART = 1252
const DARK_JEWEL = 1261
const ORB_OF_ABYSS = 1270
// Rewards
const SHADOW_ITEM_EXCHANGE_COUPON_D_GRADE = 8869
// Classes
const PALUS_KNIGHT = 32
const ASSASSIN = 35
const DARK_WIZARD = 39
const SHILLIEN_ORACLE = 42

const minimumLevel = 20

export class DarkElfChange1 extends ListenerLogic {
    constructor() {
        super( 'DarkElfChange1', 'quests/villageMasters/DarkElfChange1.ts' )
    }

    getPathPrefix(): string {
        return 'data/datapack/village_master/DarkElfChange1'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let { eventName } = data

        switch ( eventName ) {
            case '30290-01.htm':
            case '30290-02.htm':
            case '30290-03.htm':
            case '30290-04.htm':
            case '30290-05.htm':
            case '30290-06.htm':
            case '30290-07.htm':
            case '30290-08.htm':
            case '30290-09.htm':
            case '30290-10.htm':
            case '30290-11.htm':
            case '30290-12.htm':
            case '30290-13.htm':
            case '30290-14.htm':
            case '30297-01.htm':
            case '30297-02.htm':
            case '30297-03.htm':
            case '30297-04.htm':
            case '30297-05.htm':
            case '30297-06.htm':
            case '30297-07.htm':
            case '30297-08.htm':
            case '30297-09.htm':
            case '30297-10.htm':
            case '30297-11.htm':
            case '30297-12.htm':
            case '30297-13.htm':
            case '30297-14.htm':
            case '30462-01.htm':
            case '30462-02.htm':
            case '30462-03.htm':
            case '30462-04.htm':
            case '30462-05.htm':
            case '30462-06.htm':
            case '30462-07.htm':
            case '30462-08.htm':
            case '30462-09.htm':
            case '30462-10.htm':
            case '30462-11.htm':
            case '30462-12.htm':
            case '30462-13.htm':
            case '30462-14.htm':
            case '32096-01.htm':
            case '32096-02.htm':
            case '32096-03.htm':
            case '32096-04.htm':
            case '32096-05.htm':
            case '32096-06.htm':
            case '32096-07.htm':
            case '32096-08.htm':
            case '32096-09.htm':
            case '32096-10.htm':
            case '32096-11.htm':
            case '32096-12.htm':
            case '32096-13.htm':
            case '32096-14.htm':
                return this.getPath( eventName )

            case '32':
            case '35':
            case '39':
            case '42':
                return this.getRequestClassChange( data )
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )

        if ( player.getRace() === Race.DARK_ELF ) {
            if ( player.isInCategory( CategoryType.FIGHTER_GROUP ) ) {
                return this.getPath( `${ data.characterNpcId }-01.htm` )
            }

            if ( ( player.isInCategory( CategoryType.MAGE_GROUP ) ) ) {
                return this.getPath( `${ data.characterNpcId }-08.htm` )
            }

            return
        }

        return this.getPath( `${ data.characterNpcId }-33.htm` )
    }

    async getRequestClassChange( data: NpcGeneralEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let classId: number = _.parseInt( data.eventName )

        if ( player.isInCategory( CategoryType.SECOND_CLASS_GROUP ) ) {
            return this.getPath( `${ data.characterNpcId }-15.htm` )
        }

        if ( player.isInCategory( CategoryType.THIRD_CLASS_GROUP ) ) {
            return this.getPath( `${ data.characterNpcId }-16.htm` )
        }
        if ( player.isInCategory( CategoryType.FOURTH_CLASS_GROUP ) ) {
            return this.getPath( '30290-34.htm' )
        }

        if ( classId === PALUS_KNIGHT && player.getClassId() === ClassIdValues.darkFighter.id ) {
            if ( player.getLevel() < minimumLevel ) {
                if ( QuestHelper.hasQuestItems( player, GAZE_OF_ABYSS ) ) {
                    return this.getPath( `${ data.characterNpcId }-17.htm` )
                }
                return this.getPath( `${ data.characterNpcId }-18.htm` )
            }

            if ( QuestHelper.hasQuestItems( player, GAZE_OF_ABYSS ) ) {
                await QuestHelper.takeSingleItem( player, GAZE_OF_ABYSS, -1 )

                await player.setClassId( PALUS_KNIGHT )
                player.setBaseClass( PALUS_KNIGHT )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, SHADOW_ITEM_EXCHANGE_COUPON_D_GRADE, 15 )
                return this.getPath( `${ data.characterNpcId }-19.htm` )
            }

            return this.getPath( `${ data.characterNpcId }-20.htm` )
        }

        if ( classId === ASSASSIN && player.getClassId() === ClassIdValues.darkFighter.id ) {
            if ( player.getLevel() < minimumLevel ) {
                if ( QuestHelper.hasQuestItems( player, IRON_HEART ) ) {
                    return this.getPath( `${ data.characterNpcId }-21.htm` )
                }

                return this.getPath( `${ data.characterNpcId }-22.htm` )
            }

            if ( QuestHelper.hasQuestItems( player, IRON_HEART ) ) {
                await QuestHelper.takeSingleItem( player, IRON_HEART, -1 )

                await player.setClassId( ASSASSIN )
                player.setBaseClass( ASSASSIN )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, SHADOW_ITEM_EXCHANGE_COUPON_D_GRADE, 15 )
                return this.getPath( `${ data.characterNpcId }-23.htm` )
            }

            return this.getPath( `${ data.characterNpcId }-24.htm` )
        }

        if ( classId === DARK_WIZARD && player.getClassId() === ClassIdValues.darkMage.id ) {
            if ( player.getLevel() < minimumLevel ) {
                if ( QuestHelper.hasQuestItems( player, DARK_JEWEL ) ) {
                    return this.getPath( `${ data.characterNpcId }-25.htm` )
                }

                return this.getPath( `${ data.characterNpcId }-26.htm` )
            }

            if ( QuestHelper.hasQuestItems( player, DARK_JEWEL ) ) {
                await QuestHelper.takeSingleItem( player, DARK_JEWEL, -1 )

                await player.setClassId( DARK_WIZARD )
                player.setBaseClass( DARK_WIZARD )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, SHADOW_ITEM_EXCHANGE_COUPON_D_GRADE, 15 )
                return this.getPath( `${ data.characterNpcId }-27.htm` )
            }

            return this.getPath( `${ data.characterNpcId }-28.htm` )
        }

        if ( classId === SHILLIEN_ORACLE && player.getClassId() == ClassIdValues.darkMage.id ) {
            if ( player.getLevel() < minimumLevel ) {
                if ( QuestHelper.hasQuestItems( player, ORB_OF_ABYSS ) ) {
                    return this.getPath( `${ data.characterNpcId }-29.htm` )
                }

                return this.getPath( `${ data.characterNpcId }-30.htm` )
            }

            if ( QuestHelper.hasQuestItems( player, ORB_OF_ABYSS ) ) {
                await QuestHelper.takeSingleItem( player, ORB_OF_ABYSS, -1 )

                await player.setClassId( SHILLIEN_ORACLE )
                player.setBaseClass( SHILLIEN_ORACLE )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, SHADOW_ITEM_EXCHANGE_COUPON_D_GRADE, 15 )
                return this.getPath( `${ data.characterNpcId }-31.htm` )
            }

            return this.getPath( `${ data.characterNpcId }-32.htm` )
        }
    }
}