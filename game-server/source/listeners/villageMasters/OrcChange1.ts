import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { CategoryType } from '../../gameService/enums/CategoryType'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { QuestHelper } from '../helpers/QuestHelper'
import { Race } from '../../gameService/enums/Race'

import _ from 'lodash'

const npcIds: Array<number> = [
    30500, // Osborn
    30505, // Drikus
    30508, // Castor
    32097, // Finker
]

const MARK_OF_RAIDER = 1592
const KHAVATARI_TOTEM = 1615
const MASK_OF_MEDIUM = 1631

const rewardItemId = 8869
const rewardAmount = 15
const minimumLevel = 20

const RAIDER = ClassIdValues.orcRaider.id
const MONK = ClassIdValues.orcMonk.id
const SHAMAN = ClassIdValues.orcShaman.id

export class OrcChange1 extends ListenerLogic {

    constructor() {
        super( 'OrcChange1', 'quests/villageMasters/OrcChange1.ts' )
    }

    getPathPrefix(): string {
        return 'data/datapack/village_master/OrcChange1'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        switch ( data.eventName ) {
            case '45':
            case '47':
            case '50':
                return this.getRequestedClassChange( data )
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )

        if ( player.getRace() === Race.ORC ) {
            if ( player.isInCategory( CategoryType.FIGHTER_GROUP ) ) {
                return this.getPath( `${ data.characterNpcId }-01.htm` )
            }

            if ( player.isInCategory( CategoryType.MAGE_GROUP ) ) {
                return this.getPath( `${ data.characterNpcId }-06.htm` )
            }

            return
        }

        return this.getPath( `${ data.characterNpcId }-23.htm` )
    }

    async getRequestedClassChange( data: NpcGeneralEvent ) {
        let player = L2World.getPlayer( data.playerId )
        let classId: number = _.parseInt( data.eventName )

        if ( player.isInCategory( CategoryType.SECOND_CLASS_GROUP ) ) {
            return this.getPath( `${ data.characterNpcId }-09.htm` )
        }

        if ( player.isInCategory( CategoryType.THIRD_CLASS_GROUP ) ) {
            return this.getPath( `${ data.characterNpcId }-10.htm` )
        }

        if ( player.isInCategory( CategoryType.FOURTH_CLASS_GROUP ) ) {
            return this.getPath( '30500-24.htm' )
        }

        if ( classId === RAIDER && player.getClassId() === ClassIdValues.orcFighter.id ) {
            let hasRequiredItems: boolean = QuestHelper.hasQuestItems( player, MARK_OF_RAIDER )
            if ( player.getLevel() < minimumLevel ) {
                if ( hasRequiredItems ) {
                    return this.getPath( `${ data.characterNpcId }-11.htm` )
                }

                return this.getPath( `${ data.characterNpcId }-12.htm` )
            }

            if ( hasRequiredItems ) {
                await QuestHelper.takeSingleItem( player, MARK_OF_RAIDER, -1 )

                await player.setClassId( RAIDER )
                player.setBaseClass( RAIDER )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                return this.getPath( `${ data.characterNpcId }-14.htm` )
            }

            return this.getPath( `${ data.characterNpcId }-13.htm` )
        }

        if ( classId === MONK && player.getClassId() === ClassIdValues.orcFighter.id ) {
            let hasRequiredItems: boolean = QuestHelper.hasQuestItems( player, KHAVATARI_TOTEM )
            if ( player.getLevel() < minimumLevel ) {
                if ( hasRequiredItems ) {
                    return this.getPath( `${ data.characterNpcId }-15.htm` )
                }

                return this.getPath( `${ data.characterNpcId }-16.htm` )
            }

            if ( hasRequiredItems ) {
                await QuestHelper.takeSingleItem( player, KHAVATARI_TOTEM, -1 )

                await player.setClassId( MONK )
                player.setBaseClass( MONK )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                return this.getPath( `${ data.characterNpcId }-18.htm` )
            }

            return this.getPath( `${ data.characterNpcId }-17.htm` )
        }

        if ( classId === SHAMAN && player.getClassId() === ClassIdValues.orcMage.id ) {
            let hasRequiredItems: boolean = QuestHelper.hasQuestItems( player, MASK_OF_MEDIUM )
            if ( player.getLevel() < minimumLevel ) {
                if ( hasRequiredItems ) {
                    return this.getPath( `${ data.characterNpcId }-19.htm` )
                }

                return this.getPath( `${ data.characterNpcId }-20.htm` )
            }

            if ( hasRequiredItems ) {
                await QuestHelper.takeSingleItem( player, MASK_OF_MEDIUM, -1 )

                await player.setClassId( SHAMAN )
                player.setBaseClass( SHAMAN )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                return this.getPath( `${ data.characterNpcId }-22.htm` )
            }

            return this.getPath( `${ data.characterNpcId }-21.htm` )
        }
    }
}