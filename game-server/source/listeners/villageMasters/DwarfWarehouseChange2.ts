import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { CategoryType } from '../../gameService/enums/CategoryType'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'

import _ from 'lodash'

const npcIds: Array<number> = [
    30511, // Gesto
    30676, // Croop
    30685, // Baxt
    30845, // Klump
    30894, // Natools
    31269, // Mona
    31314, // Donal
    31958, // Yasheni
]

const MARK_OF_SEARCHER = 2809
const MARK_OF_GUILDSMAN = 3119
const MARK_OF_PROSPERITY = 3238

const BOUNTY_HUNTER = ClassIdValues.bountyHunter.id
const minimumLevel = 40

const rewardItemId = 8870
const rewardAmount = 15

export class DwarfWarehouseChange2 extends ListenerLogic {

    constructor() {
        super( 'DwarfWarehouseChange2', 'quests/villageMasters/DwarfWarehouseChange2.ts' )
    }

    getPathPrefix(): string {
        return 'data/datapack/village_master/DwarfWarehouseChange2'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        switch ( data.eventName ) {
            case '30511-03.htm':
            case '30511-04.htm':
            case '30511-05.htm':
                return this.getPath( data.eventName )

            case '55':
                return this.getRequestedClassChange( data )
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )

        if ( player.isInCategory( CategoryType.FOURTH_CLASS_GROUP ) && player.isInCategory( CategoryType.BOUNTY_HUNTER_GROUP ) ) {
            return this.getPath( '30511-01.htm' )
        }

        if ( player.isInCategory( CategoryType.BOUNTY_HUNTER_GROUP ) ) {

            if ( [ ClassIdValues.scavenger.id, ClassIdValues.bountyHunter.id ].includes( player.getClassId() ) ) {
                return this.getPath( '30511-02.htm' )
            }

            return this.getPath( '30511-06.htm' )
        }

        return this.getPath( '30511-07.htm' )
    }

    async getRequestedClassChange( data: NpcGeneralEvent ): Promise<string> {
        const player = L2World.getPlayer( data.playerId )
        const classId: number = _.parseInt( data.eventName )

        if ( player.isInCategory( CategoryType.THIRD_CLASS_GROUP ) ) {
            return this.getPath( '30511-08.htm' )
        }

        if ( classId === BOUNTY_HUNTER && player.getClassId() === ClassIdValues.scavenger.id ) {
            let hasRequiredItems: boolean = QuestHelper.hasQuestItems( player, MARK_OF_GUILDSMAN, MARK_OF_PROSPERITY, MARK_OF_SEARCHER )
            if ( player.getLevel() < minimumLevel ) {
                if ( hasRequiredItems ) {
                    return this.getPath( '30511-09.htm' )
                }

                return this.getPath( '30511-10.htm' )
            }

            if ( hasRequiredItems ) {
                await QuestHelper.takeMultipleItems( player, -1, MARK_OF_GUILDSMAN, MARK_OF_PROSPERITY, MARK_OF_SEARCHER )

                await player.setClassId( BOUNTY_HUNTER )
                player.setBaseClass( BOUNTY_HUNTER )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                return this.getPath( '30511-11.htm' )
            }

            return this.getPath( '30511-12.htm' )
        }
    }
}