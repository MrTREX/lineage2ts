import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { DataManager } from '../../data/manager'
import { CategoryType } from '../../gameService/enums/CategoryType'
import { L2World } from '../../gameService/L2World'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestHelper } from '../helpers/QuestHelper'
import { Race } from '../../gameService/enums/Race'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'

import _ from 'lodash'

const maleNpcIds: Array<number> = [
    32146, // Valpor
    32205, // Aetonic
    32209, // Ferdinand
    32213, // Vitus
    32217, // Barta
    32221, // Brome
    32225, // Taine
    32229, // Hagel
    32233, // Zoldart
]

const femaleNpcIds: Array<number> = [
    32145, // Maynard
    32206, // Pieche
    32210, // Eddy
    32214, // Meldina
    32218, // Miya
    32222, // Liane
    32226, // Raula
    32230, // Ceci
    32234, // Nizer
]

const ORKURUS_RECOMMENDATION = 9760
const KAMAEL_INQUISITOR_MARK = 9782
const SOUL_BREAKER_CERTIFICATE = 9806

const rewardItemId = 8870
const rewardAmount = 15
const minimumLevel = 40

const BERSERKER = ClassIdValues.berserker.id
const MALE_SOULBREAKER = ClassIdValues.maleSoulbreaker.id
const FEMALE_SOULBREAKER = ClassIdValues.femaleSoulbreaker.id
const ARBALESTER = ClassIdValues.arbalester.id

export class KamaelChange2 extends ListenerLogic {

    constructor() {
        super( 'KamaelChange2', 'quests/villageMasters/KamaelChange2.ts' )
    }

    getPathPrefix(): string {
        return 'data/datapack/village_master/KamaelChange2'
    }

    getQuestStartIds(): Array<number> {
        return _.flatten( [ maleNpcIds, femaleNpcIds ] )
    }

    getTalkIds(): Array<number> {
        return _.flatten( [ maleNpcIds, femaleNpcIds ] )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        switch ( data.eventName ) {
            case '127':
            case '128':
            case '129':
            case '130':
                return this.getRequestedClassChange( data )
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )

        if ( player.getRace() !== Race.KAMAEL ) {
            return this.getPath( '32145-01.htm' )
        }

        if ( player.isInCategory( CategoryType.KAMAEL_FIRST_CLASS_GROUP ) ) {
            if ( player.getClassId() === ClassIdValues.maleSoldier.id ) {
                return this.getPath( '32145-02.htm' )
            }

            if ( player.getClassId() === ClassIdValues.femaleSoldier.id ) {
                return this.getPath( '32145-03.htm' )
            }

            return
        }

        let isMaleNpc = maleNpcIds.includes( data.characterNpcId )
        if ( player.isInCategory( CategoryType.KAMAEL_SECOND_CLASS_GROUP ) ) {
            if ( isMaleNpc ) {
                if ( player.isInCategory( CategoryType.KAMAEL_FEMALE_MAIN_OCCUPATION ) ) {
                    return this.getPath( '32145-04.htm' )
                }

                if ( player.getClassId() === ClassIdValues.trooper.id ) {
                    return this.getPath( '32145-05.htm' )
                }

                if ( player.getClassId() === ClassIdValues.warder.id ) {
                    return this.getPath( '32145-02.htm' )
                }

                return
            }

            if ( player.isInCategory( CategoryType.KAMAEL_MALE_MAIN_OCCUPATION ) ) {
                return this.getPath( '32145-10.htm' )
            }

            if ( player.getClassId() === ClassIdValues.trooper.id ) {
                return this.getPath( '32145-03.htm' )
            }

            if ( player.getClassId() === ClassIdValues.warder.id ) {
                return this.getPath( '32145-11.htm' )
            }

            return
        }

        if ( player.isInCategory( CategoryType.KAMAEL_THIRD_CLASS_GROUP ) ) {
            if ( isMaleNpc ) {
                if ( player.isInCategory( CategoryType.KAMAEL_MALE_MAIN_OCCUPATION ) ) {
                    return this.getPath( '32145-16.htm' )
                }

                return this.getPath( '32145-04.htm' )
            }

            if ( player.isInCategory( CategoryType.KAMAEL_FEMALE_MAIN_OCCUPATION ) ) {
                return this.getPath( '32145-17.htm' )
            }

            return this.getPath( '32145-10.htm' )
        }

        if ( player.isInCategory( CategoryType.KAMAEL_FOURTH_CLASS_GROUP ) ) {
            if ( isMaleNpc ) {
                if ( player.isInCategory( CategoryType.KAMAEL_MALE_MAIN_OCCUPATION ) ) {
                    return this.getPath( '32145-18.htm' )
                }

                return this.getPath( '32145-04.htm' )
            }

            if ( player.isInCategory( CategoryType.KAMAEL_FEMALE_MAIN_OCCUPATION ) ) {
                return this.getPath( '32145-19.htm' )
            }

            return this.getPath( '32145-10.htm' )
        }
    }

    async getRequestedClassChange( data: NpcGeneralEvent ): Promise<string> {
        let classId: number = _.parseInt( data.eventName )

        if ( !DataManager.getCategoryData().isInCategory( CategoryType.KAMAEL_THIRD_CLASS_GROUP, classId ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let isMaleNpc = maleNpcIds.includes( data.characterNpcId )

        if ( player.isInCategory( CategoryType.KAMAEL_FIRST_CLASS_GROUP ) ) {
            if ( isMaleNpc ) {
                return this.getPath( '32145-02.htm' )
            }

            return this.getPath( '32145-03.htm' )
        }

        if ( player.isInCategory( CategoryType.KAMAEL_THIRD_CLASS_GROUP ) ) {
            if ( isMaleNpc ) {
                return this.getPath( '32145-16.htm' )
            }

            return this.getPath( '32145-17.htm' )
        }

        if ( player.isInCategory( CategoryType.KAMAEL_FOURTH_CLASS_GROUP ) ) {
            if ( isMaleNpc ) {
                return this.getPath( '32145-18.htm' )
            }

            return this.getPath( '32145-19.htm' )
        }

        if ( player.getClassId() === ClassIdValues.trooper.id ) {
            if ( isMaleNpc ) {
                if ( classId === BERSERKER ) {
                    let state: QuestState = QuestStateCache.getQuestState( data.playerId, 'Q00064_CertifiedBerserker' )

                    if ( player.getLevel() < minimumLevel ) {
                        if ( state && state.isCompleted() ) {
                            return this.getPath( '32145-20.htm' )
                        }

                        return this.getPath( '32145-21.htm' )
                    }

                    if ( !state || !state.isCompleted() ) {
                        return this.getPath( '32145-22.htm' )
                    }

                    await QuestHelper.takeSingleItem( player, ORKURUS_RECOMMENDATION, -1 )

                    await player.setClassId( BERSERKER )
                    player.setBaseClass( BERSERKER )
                    player.broadcastUserInfo()

                    await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                    return this.getPath( '32145-23.htm' )
                }

                if ( classId === MALE_SOULBREAKER ) {
                    let state: QuestState = QuestStateCache.getQuestState( data.playerId, 'Q00065_CertifiedSoulBreaker' )
                    if ( player.getLevel() < minimumLevel ) {
                        if ( state && state.isCompleted() ) {
                            return this.getPath( '32145-24.htm' )
                        }

                        return this.getPath( '32145-25.htm' )
                    }

                    if ( !state || !state.isCompleted() ) {
                        return this.getPath( '32145-26.htm' )
                    }

                    await QuestHelper.takeSingleItem( player, SOUL_BREAKER_CERTIFICATE, -1 )

                    await player.setClassId( MALE_SOULBREAKER )
                    player.setBaseClass( MALE_SOULBREAKER )
                    player.broadcastUserInfo()

                    await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                    return this.getPath( '32145-27.htm' )
                }
            }

            return this.getPath( '32145-10.htm' )
        }

        if ( player.getClassId() === ClassIdValues.warder.id ) {
            if ( isMaleNpc ) {
                return this.getPath( '32145-04.htm' )
            }

            if ( classId === FEMALE_SOULBREAKER ) {
                let state: QuestState = QuestStateCache.getQuestState( data.playerId, 'Q00065_CertifiedSoulBreaker' )
                if ( player.getLevel() < minimumLevel ) {
                    if ( state && state.isCompleted() ) {
                        return this.getPath( '32145-28.htm' )
                    }

                    return this.getPath( '32145-29.htm' )
                }

                if ( !state || !state.isCompleted() ) {
                    return this.getPath( '32145-30.htm' )
                }

                await QuestHelper.takeSingleItem( player, SOUL_BREAKER_CERTIFICATE, -1 )

                await player.setClassId( FEMALE_SOULBREAKER )
                player.setBaseClass( FEMALE_SOULBREAKER )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                return this.getPath( '32145-31.htm' )
            }

            if ( classId === ARBALESTER ) {
                let state: QuestState = QuestStateCache.getQuestState( data.playerId, 'Q00066_CertifiedArbalester' )
                if ( player.getLevel() < minimumLevel ) {
                    if ( state && state.isCompleted() ) {
                        return this.getPath( '32145-32.htm' )
                    }

                    return this.getPath( '32145-33.htm' )
                }

                if ( !state || !state.isCompleted() ) {
                    return this.getPath( '32145-34.htm' )
                }

                await QuestHelper.takeSingleItem( player, KAMAEL_INQUISITOR_MARK, -1 )

                await player.setClassId( ARBALESTER )
                player.setBaseClass( ARBALESTER )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                return this.getPath( '32145-35.htm' )
            }
        }
    }
}