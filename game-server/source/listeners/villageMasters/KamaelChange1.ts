import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { CategoryType } from '../../gameService/enums/CategoryType'
import { DataManager } from '../../data/manager'
import { L2World } from '../../gameService/L2World'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestHelper } from '../helpers/QuestHelper'
import { Race } from '../../gameService/enums/Race'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'

import _ from 'lodash'

const npcIds: Array<number> = [
    32191, // Hanarin
    32193, // Yeniche
    32196, // Gershwin
    32199, // Holst
    32202, // Khadava
]

const GWAINS_RECOMMENDATION = 9753
const STEELRAZOR_EVALUATION = 9772

const rewardItemId = 8869
const rewardAmount = 15
const minimumLevel = 20

const TROOPER = ClassIdValues.trooper.id
const WARDER = ClassIdValues.warder.id

export class KamaelChange1 extends ListenerLogic {

    constructor() {
        super( 'KamaelChange1', 'quests/villageMasters/KamaelChange1.ts' )
    }

    getPathPrefix(): string {
        return 'data/datapack/village_master/KamaelChange1'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        switch ( data.eventName ) {
            case '125':
            case '126':
                return this.getRequestedClassChange( data )
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )

        if ( player.getRace() !== Race.KAMAEL ) {
            return this.getPath( '32191-01.htm' )
        }

        if ( player.isInCategory( CategoryType.KAMAEL_FIRST_CLASS_GROUP ) ) {
            if ( player.getClassId() === ClassIdValues.maleSoldier.id ) {
                return this.getPath( '32191-02.htm' )
            }

            if ( player.getClassId() === ClassIdValues.femaleSoldier.id ) {
                return this.getPath( '32191-06.htm' )
            }

            return
        }

        if ( player.isInCategory( CategoryType.KAMAEL_SECOND_CLASS_GROUP ) ) {
            return this.getPath( '32191-10.htm' )
        }

        if ( player.isInCategory( CategoryType.KAMAEL_THIRD_CLASS_GROUP ) ) {
            return this.getPath( '32191-11.htm' )
        }

        return this.getPath( '32191-12.htm' )
    }

    async getRequestedClassChange( data: NpcGeneralEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let classId: number = _.parseInt( data.eventName )

        if ( !DataManager.getCategoryData().isInCategory( CategoryType.KAMAEL_SECOND_CLASS_GROUP, classId ) ) {
            return
        }

        if ( player.isInCategory( CategoryType.KAMAEL_SECOND_CLASS_GROUP ) ) {
            return this.getPath( '32191-10.htm' )
        }

        if ( player.isInCategory( CategoryType.KAMAEL_THIRD_CLASS_GROUP ) ) {
            return this.getPath( '32191-11.htm' )
        }

        if ( player.isInCategory( CategoryType.KAMAEL_FOURTH_CLASS_GROUP ) ) {
            return this.getPath( '32191-12.htm' )
        }

        if ( classId === TROOPER && player.getClassId() === ClassIdValues.maleSoldier.id ) {
            let state: QuestState = QuestStateCache.getQuestState( data.playerId, 'Q00062_PathOfTheTrooper' )
            if ( player.getLevel() < minimumLevel ) {
                if ( state && state.isCompleted() ) {
                    return this.getPath( '32191-13.htm' )
                }

                return this.getPath( '32191-14.htm' )
            }

            if ( !state || !state.isCompleted() ) {
                return this.getPath( '32191-15.htm' )
            }

            await QuestHelper.takeSingleItem( player, GWAINS_RECOMMENDATION, -1 )

            await player.setClassId( TROOPER )
            player.setBaseClass( TROOPER )
            player.broadcastUserInfo()

            await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
            return this.getPath( '32191-16.htm' )
        }

        if ( classId === WARDER && player.getClassId() === ClassIdValues.femaleSoldier.id ) {
            let state: QuestState = QuestStateCache.getQuestState( data.playerId, 'Q00063_PathOfTheWarder' )
            if ( player.getLevel() < minimumLevel ) {
                if ( state && state.isCompleted() ) {
                    return this.getPath( '32191-17.htm' )
                }

                return this.getPath( '32191-18.htm' )
            }

            if ( !state || !state.isCompleted() ) {
                return this.getPath( '32191-19.htm' )
            }

            await QuestHelper.takeSingleItem( player, STEELRAZOR_EVALUATION, -1 )

            await player.setClassId( WARDER )
            player.setBaseClass( WARDER )
            player.broadcastUserInfo()

            await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
            return this.getPath( '32191-20.htm' )
        }
    }
}