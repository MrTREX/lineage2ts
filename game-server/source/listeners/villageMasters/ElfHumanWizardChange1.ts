import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { CategoryType } from '../../gameService/enums/CategoryType'
import { QuestHelper } from '../helpers/QuestHelper'
import { Race } from '../../gameService/enums/Race'

import _ from 'lodash'

const npcIds: Array<number> = [
    30037, // Levian
    30070, // Sylvain
    30289, // Raymond
    32095, // Marie
    32098, // Celes
]

const MARK_OF_FAITH = 1201
const ETERNITY_DIAMOND = 1230
const LEAF_OF_ORACLE = 1235
const BEAD_OF_SEASON = 1292

const WIZARD = ClassIdValues.wizard.id
const CLERIC = ClassIdValues.cleric.id
const ELVEN_WIZARD = ClassIdValues.elvenWizard.id
const ORACLE = ClassIdValues.oracle.id

const minimumLevel = 20
const rewardItemId = 8869
const rewardAmount = 15

export class ElfHumanWizardChange1 extends ListenerLogic {

    constructor() {
        super( 'ElfHumanWizardChange1', 'quests/villageMasters/ElfHumanWizardChange1.ts' )
    }

    getPathPrefix(): string {
        return 'data/datapack/village_master/ElfHumanWizardChange1'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        switch ( data.eventName ) {
            case '11':
            case '15':
            case '26':
            case '29':
                return this.getRequestedClassChange( data )
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )

        if ( player.isInCategory( CategoryType.MAGE_GROUP ) && [ Race.HUMAN, Race.ELF ].includes( player.getRace() ) ) {
            if ( player.getRace() === Race.HUMAN ) {
                return this.getPath( `${ data.characterNpcId }-01.htm` )
            }

            return this.getPath( `${ data.characterNpcId }-08.htm` )
        }

        return this.getPath( `${ data.characterNpcId }-15.htm` )
    }

    async getRequestedClassChange( data: NpcGeneralEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let classId: number = _.parseInt( data.eventName )

        if ( player.isInCategory( CategoryType.SECOND_CLASS_GROUP ) ) {
            return this.getPath( `${ data.characterNpcId }-16.htm` )
        }

        if ( player.isInCategory( CategoryType.THIRD_CLASS_GROUP ) ) {
            return this.getPath( `${ data.characterNpcId }-17.htm` )
        }

        if ( player.isInCategory( CategoryType.FOURTH_CLASS_GROUP ) ) {
            return this.getPath( '30037-34.htm' )
        }

        if ( classId === WIZARD && player.getClassId() === ClassIdValues.mage.id ) {
            let hasRequiredItems: boolean = QuestHelper.hasQuestItems( player, BEAD_OF_SEASON )
            if ( player.getLevel() < minimumLevel ) {
                if ( hasRequiredItems ) {
                    return this.getPath( `${ data.characterNpcId }-18.htm` )
                }

                return this.getPath( `${ data.characterNpcId }-19.htm` )
            }

            if ( hasRequiredItems ) {
                await QuestHelper.takeSingleItem( player, BEAD_OF_SEASON, -1 )

                await player.setClassId( WIZARD )
                player.setBaseClass( WIZARD )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                return this.getPath( `${ data.characterNpcId }-20.htm` )
            }

            return this.getPath( `${ data.characterNpcId }-21.htm` )
        }

        if ( classId === CLERIC && player.getClassId() === ClassIdValues.mage.id ) {
            let hasRequiredItems: boolean = QuestHelper.hasQuestItems( player, MARK_OF_FAITH )
            if ( player.getLevel() < minimumLevel ) {
                if ( hasRequiredItems ) {
                    return this.getPath( `${ data.characterNpcId }-22.htm` )
                }

                return this.getPath( `${ data.characterNpcId }-23.htm` )
            }

            if ( hasRequiredItems ) {
                await QuestHelper.takeSingleItem( player, MARK_OF_FAITH, -1 )

                await player.setClassId( CLERIC )
                player.setBaseClass( CLERIC )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                return this.getPath( `${ data.characterNpcId }-24.htm` )
            }

            return this.getPath( `${ data.characterNpcId }-25.htm` )
        }

        if ( classId === ELVEN_WIZARD && player.getClassId() === ClassIdValues.elvenMage.id ) {
            let hasRequiredItems: boolean = QuestHelper.hasQuestItems( player, ETERNITY_DIAMOND )
            if ( player.getLevel() < minimumLevel ) {
                if ( hasRequiredItems ) {
                    return this.getPath( `${ data.characterNpcId }-26.htm` )
                }

                return this.getPath( `${ data.characterNpcId }-27.htm` )
            }

            if ( hasRequiredItems ) {
                await QuestHelper.takeSingleItem( player, ETERNITY_DIAMOND, -1 )

                await player.setClassId( ELVEN_WIZARD )
                player.setBaseClass( ELVEN_WIZARD )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                return this.getPath( `${ data.characterNpcId }-28.htm` )
            }

            return this.getPath( `${ data.characterNpcId }-29.htm` )
        }

        if ( classId === ORACLE && player.getClassId() === ClassIdValues.elvenMage.id ) {
            let hasRequiredItems: boolean = QuestHelper.hasQuestItems( player, LEAF_OF_ORACLE )
            if ( player.getLevel() < minimumLevel ) {
                if ( hasRequiredItems ) {
                    return this.getPath( `${ data.characterNpcId }-30.htm` )
                }

                return this.getPath( `${ data.characterNpcId }-31.htm` )
            }

            if ( hasRequiredItems ) {
                await QuestHelper.takeSingleItem( player, LEAF_OF_ORACLE, -1 )

                await player.setClassId( ORACLE )
                player.setBaseClass( ORACLE )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                return this.getPath( `${ data.characterNpcId }-32.htm` )
            }
            return this.getPath( `${ data.characterNpcId }-33.htm` )
        }
    }
}