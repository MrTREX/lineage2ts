import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { CategoryType } from '../../gameService/enums/CategoryType'
import { QuestHelper } from '../helpers/QuestHelper'
import { Race } from '../../gameService/enums/Race'

import _ from 'lodash'

const npcIds: Array<number> = [
    30066, // Pabris
    30288, // Rains
    30373, // Ramos
    32094, // Schule
]

const MEDALLION_OF_WARRIOR = 1145
const SWORD_OF_RITUAL = 1161
const BEZIQUES_RECOMMENDATION = 1190
const ELVEN_KNIGHT_BROOCH = 1204
const REISAS_RECOMMENDATION = 1217

const WARRIOR = ClassIdValues.warrior.id
const KNIGHT = ClassIdValues.knight.id
const ROGUE = ClassIdValues.rogue.id
const ELVEN_KNIGHT = ClassIdValues.elvenKnight.id
const ELVEN_SCOUT = ClassIdValues.elvenScout.id

const minimumLevel = 20
const rewardItemId = 8869
const rewardAmount = 15

export class ElfHumanFighterChange1 extends ListenerLogic {

    constructor() {
        super( 'ElfHumanFighterChange1', 'quests/villageMasters/ElfHumanFighterChange1.ts' )
    }

    getPathPrefix(): string {
        return 'data/datapack/village_master/ElfHumanFighterChange1'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        switch ( data.eventName ) {
            case '1':
            case '4':
            case '7':
            case '19':
            case '22':
                return this.getRequestedClassChange( data )
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )

        if ( player.isInCategory( CategoryType.FIGHTER_GROUP ) && [ Race.HUMAN, Race.ELF ].includes( player.getRace() ) ) {
            if ( player.getRace() === Race.HUMAN ) {
                return this.getPath( `${ data.characterNpcId }-01.htm` )
            }

            return this.getPath( `${ data.characterNpcId }-11.htm` )
        }

        return this.getPath( `${ data.characterNpcId }-18.htm` )
    }

    async getRequestedClassChange( data: NpcGeneralEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let classId: number = _.parseInt( data.eventName )

        if ( player.isInCategory( CategoryType.SECOND_CLASS_GROUP ) ) {
            return this.getPath( `${ data.characterNpcId }-19.htm` )
        }

        if ( player.isInCategory( CategoryType.THIRD_CLASS_GROUP ) ) {
            return this.getPath( `${ data.characterNpcId }-20.htm` )
        }

        if ( player.isInCategory( CategoryType.FOURTH_CLASS_GROUP ) ) {
            return this.getPath( '30066-41.htm' )
        }

        if ( classId === WARRIOR && player.getClassId() === ClassIdValues.fighter.id ) {
            let hasRequiredItems: boolean = QuestHelper.hasQuestItems( player, MEDALLION_OF_WARRIOR )
            if ( player.getLevel() < minimumLevel ) {
                if ( hasRequiredItems ) {
                    return this.getPath( `${ data.characterNpcId }-21.htm` )
                }

                return this.getPath( `${ data.characterNpcId }-22.htm` )
            }

            if ( hasRequiredItems ) {
                await QuestHelper.takeSingleItem( player, MEDALLION_OF_WARRIOR, -1 )

                await player.setClassId( WARRIOR )
                player.setBaseClass( WARRIOR )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                return this.getPath( `${ data.characterNpcId }-23.htm` )
            }

            return this.getPath( `${ data.characterNpcId }-24.htm` )
        }

        if ( classId === KNIGHT && player.getClassId() === ClassIdValues.fighter.id ) {
            let hasRequiredItems: boolean = QuestHelper.hasQuestItems( player, SWORD_OF_RITUAL )
            if ( player.getLevel() < minimumLevel ) {
                if ( hasRequiredItems ) {
                    return this.getPath( `${ data.characterNpcId }-25.htm` )
                }

                return this.getPath( `${ data.characterNpcId }-25.htm` )
            }

            if ( hasRequiredItems ) {
                await QuestHelper.takeSingleItem( player, SWORD_OF_RITUAL, -1 )

                await player.setClassId( KNIGHT )
                player.setBaseClass( KNIGHT )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                return this.getPath( `${ data.characterNpcId }-27.htm` )
            }

            return this.getPath( `${ data.characterNpcId }-28.htm` )
        }

        if ( classId === ROGUE && player.getClassId() === ClassIdValues.fighter.id ) {
            let hasRequiredItems: boolean = QuestHelper.hasQuestItems( player, BEZIQUES_RECOMMENDATION )
            if ( player.getLevel() < minimumLevel ) {
                if ( hasRequiredItems ) {
                    return this.getPath( `${ data.characterNpcId }-29.htm` )
                }

                return this.getPath( `${ data.characterNpcId }-30.htm` )
            }

            if ( hasRequiredItems ) {
                await QuestHelper.takeSingleItem( player, BEZIQUES_RECOMMENDATION, -1 )

                await player.setClassId( ROGUE )
                player.setBaseClass( ROGUE )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                return this.getPath( `${ data.characterNpcId }-31.htm` )
            }

            return this.getPath( `${ data.characterNpcId }-32.htm` )
        }

        if ( classId === ELVEN_KNIGHT && player.getClassId() === ClassIdValues.elvenFighter.id ) {
            let hasRequiredItems: boolean = QuestHelper.hasQuestItems( player, ELVEN_KNIGHT_BROOCH )
            if ( player.getLevel() < minimumLevel ) {
                if ( hasRequiredItems ) {
                    return this.getPath( `${ data.characterNpcId }-33.htm` )
                }

                return this.getPath( `${ data.characterNpcId }-34.htm` )
            }

            if ( hasRequiredItems ) {
                await QuestHelper.takeSingleItem( player, ELVEN_KNIGHT_BROOCH, -1 )

                await player.setClassId( ELVEN_KNIGHT )
                player.setBaseClass( ELVEN_KNIGHT )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                return this.getPath( `${ data.characterNpcId }-35.htm` )
            }

            return this.getPath( `${ data.characterNpcId }-36.htm` )
        }

        if ( classId === ELVEN_SCOUT && player.getClassId() === ClassIdValues.elvenFighter.id ) {
            if ( player.getLevel() < minimumLevel ) {
                if ( QuestHelper.hasQuestItems( player, REISAS_RECOMMENDATION ) ) {
                    return this.getPath( `${ data.characterNpcId }-37.htm` )
                }

                return this.getPath( `${ data.characterNpcId }-38.htm` )
            }

            if ( QuestHelper.hasQuestItems( player, REISAS_RECOMMENDATION ) ) {
                await QuestHelper.takeSingleItem( player, REISAS_RECOMMENDATION, -1 )

                await player.setClassId( ELVEN_SCOUT )
                player.setBaseClass( ELVEN_SCOUT )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                return this.getPath( `${ data.characterNpcId }-39.htm` )
            }

            return this.getPath( `${ data.characterNpcId }-40.htm` )
        }
    }
}