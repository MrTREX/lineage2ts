import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { CategoryType } from '../../gameService/enums/CategoryType'
import { QuestHelper } from '../helpers/QuestHelper'

import _ from 'lodash'

const npcIds: Array<number> = [
    30109, // Hannavalt
    30187, // Klaus
    30689, // Siria
    30849, // Sedrick
    30900, // Marcus
    31276, // Bernhard
    31321, // Siegmund
    31965, // Hector
]

const MARK_OF_CHALLENGER = 2627
const MARK_OF_DUTY = 2633
const MARK_OF_SEEKER = 2673
const MARK_OF_TRUST = 2734
const MARK_OF_DUELIST = 2762
const MARK_OF_SEARCHER = 2809
const MARK_OF_HEALER = 2820
const MARK_OF_LIFE = 3140
const MARK_OF_CHAMPION = 3276
const MARK_OF_SAGITTARIUS = 3293
const MARK_OF_WITCHCRAFT = 3307

const GLADIATOR = ClassIdValues.gladiator.id
const WARLORD = ClassIdValues.warlord.id
const PALADIN = ClassIdValues.paladin.id
const DARK_AVENGER = ClassIdValues.darkAvenger.id
const TREASURE_HUNTER = ClassIdValues.treasureHunter.id
const HAWKEYE = ClassIdValues.hawkeye.id
const TEMPLE_KNIGHT = ClassIdValues.templeKnight.id
const SWORDSINGER = ClassIdValues.swordSinger.id
const PLAINS_WALKER = ClassIdValues.plainsWalker.id
const SILVER_RANGER = ClassIdValues.silverRanger.id

const minimumLevel = 40
const rewardItemId = 8870
const rewardAmount = 15

export class ElfHumanFighterChange2 extends ListenerLogic {

    constructor() {
        super( 'ElfHumanFighterChange2', 'quests/villageMasters/ElfHumanFighterChange2.ts' )
    }

    getPathPrefix(): string {
        return 'data/datapack/village_master/ElfHumanFighterChange2'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        switch ( data.eventName ) {
            case '2':
            case '3':
            case '5':
            case '6':
            case '8':
            case '9':
            case '20':
            case '21':
            case '23':
            case '24':
                return this.getRequestedClassChange( data )
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )

        if ( player.isInCategory( CategoryType.FIGHTER_GROUP )
                && player.isInCategory( CategoryType.FOURTH_CLASS_GROUP )
                && ( player.isInCategory( CategoryType.HUMAN_FALL_CLASS ) || player.isInCategory( CategoryType.ELF_FALL_CLASS ) ) ) {
            return this.getPath( '30109-01.htm' )
        }

        if ( player.isInCategory( CategoryType.FIGHTER_GROUP )
                && ( player.isInCategory( CategoryType.HUMAN_FALL_CLASS ) || player.isInCategory( CategoryType.ELF_FALL_CLASS ) ) ) {

            if ( [ ClassIdValues.warrior.id, ClassIdValues.gladiator.id, ClassIdValues.warlord.id ].includes( player.getClassId() ) ) {
                return this.getPath( '30109-02.htm' )
            }

            if ( [ ClassIdValues.knight.id, ClassIdValues.paladin.id, ClassIdValues.darkAvenger.id ].includes( player.getClassId() ) ) {
                return this.getPath( '30109-09.htm' )
            }

            if ( [ ClassIdValues.rogue.id, ClassIdValues.treasureHunter.id, ClassIdValues.hawkeye.id ].includes( player.getClassId() ) ) {
                return this.getPath( '30109-16.htm' )
            }

            if ( [ ClassIdValues.elvenKnight.id, ClassIdValues.templeKnight.id, ClassIdValues.swordSinger.id ].includes( player.getClassId() ) ) {
                return this.getPath( '30109-23.htm' )
            }

            if ( [ ClassIdValues.elvenScout.id, ClassIdValues.plainsWalker.id, ClassIdValues.silverRanger.id ].includes( player.getClassId() ) ) {
                return this.getPath( '30109-30.htm' )
            }

            return this.getPath( '30109-37.htm' )
        }

        return this.getPath( '30109-38.htm' )
    }

    async getRequestedClassChange( data: NpcGeneralEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let classId: number = _.parseInt( data.eventName )

        if ( player.isInCategory( CategoryType.THIRD_CLASS_GROUP ) ) {
            return this.getPath( '30109-39.htm' )
        }

        if ( classId === GLADIATOR && player.getClassId() === ClassIdValues.warrior.id ) {
            let hasRequiredItems: boolean = QuestHelper.hasQuestItems( player, MARK_OF_CHALLENGER, MARK_OF_TRUST, MARK_OF_DUELIST )
            if ( player.getLevel() < minimumLevel ) {
                if ( hasRequiredItems ) {
                    return this.getPath( '30109-40.htm' )
                }

                return this.getPath( '30109-41.htm' )
            }

            if ( hasRequiredItems ) {
                await QuestHelper.takeMultipleItems( player, -1, MARK_OF_CHALLENGER, MARK_OF_TRUST, MARK_OF_DUELIST )

                await player.setClassId( GLADIATOR )
                player.setBaseClass( GLADIATOR )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                return this.getPath( '30109-42.htm' )
            }

            return this.getPath( '30109-43.htm' )
        }

        if ( classId === WARLORD && player.getClassId() === ClassIdValues.warrior.id ) {
            let hasRequiredItems: boolean = QuestHelper.hasQuestItems( player, MARK_OF_CHALLENGER, MARK_OF_TRUST, MARK_OF_CHAMPION )
            if ( player.getLevel() < minimumLevel ) {
                if ( hasRequiredItems ) {
                    return this.getPath( '30109-44.htm' )
                }

                return this.getPath( '30109-45.htm' )
            }

            if ( hasRequiredItems ) {
                await QuestHelper.takeMultipleItems( player, -1, MARK_OF_CHALLENGER, MARK_OF_TRUST, MARK_OF_CHAMPION )

                await player.setClassId( WARLORD )
                player.setBaseClass( WARLORD )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                return this.getPath( '30109-46.htm' )
            }

            return this.getPath( '30109-47.htm' )
        }

        if ( classId === PALADIN && player.getClassId() === ClassIdValues.knight.id ) {
            let hasRequiredItems: boolean = QuestHelper.hasQuestItems( player, MARK_OF_DUTY, MARK_OF_TRUST, MARK_OF_HEALER )
            if ( player.getLevel() < minimumLevel ) {
                if ( hasRequiredItems ) {
                    return this.getPath( '30109-48.htm' )
                }

                return this.getPath( '30109-49.htm' )
            }

            if ( hasRequiredItems ) {
                await QuestHelper.takeMultipleItems( player, -1, MARK_OF_DUTY, MARK_OF_TRUST, MARK_OF_HEALER )

                await player.setClassId( PALADIN )
                player.setBaseClass( PALADIN )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                return this.getPath( '30109-50.htm' )
            }

            return this.getPath( '30109-51.htm' )
        }

        if ( classId === DARK_AVENGER && player.getClassId() === ClassIdValues.knight.id ) {
            let hasRequiredItems: boolean = QuestHelper.hasQuestItems( player, MARK_OF_DUTY, MARK_OF_TRUST, MARK_OF_WITCHCRAFT )
            if ( player.getLevel() < minimumLevel ) {
                if ( hasRequiredItems ) {
                    return this.getPath( '30109-52.htm' )
                }

                return this.getPath( '30109-53.htm' )
            }

            if ( hasRequiredItems ) {
                await QuestHelper.takeMultipleItems( player, -1, MARK_OF_DUTY, MARK_OF_TRUST, MARK_OF_WITCHCRAFT )

                await player.setClassId( DARK_AVENGER )
                player.setBaseClass( DARK_AVENGER )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                return this.getPath( '30109-54.htm' )
            }

            return this.getPath( '30109-55.htm' )
        }

        if ( classId === TREASURE_HUNTER && player.getClassId() === ClassIdValues.rogue.id ) {
            let hasRequiredItems: boolean = QuestHelper.hasQuestItems( player, MARK_OF_SEEKER, MARK_OF_TRUST, MARK_OF_SEARCHER )
            if ( player.getLevel() < minimumLevel ) {
                if ( hasRequiredItems ) {
                    return this.getPath( '30109-56.htm' )
                }

                return this.getPath( '30109-57.htm' )
            }

            if ( hasRequiredItems ) {
                await QuestHelper.takeMultipleItems( player, -1, MARK_OF_SEEKER, MARK_OF_TRUST, MARK_OF_SEARCHER )

                await player.setClassId( TREASURE_HUNTER )
                player.setBaseClass( TREASURE_HUNTER )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                return this.getPath( '30109-58.htm' )
            }

            return this.getPath( '30109-59.htm' )
        }

        if ( classId === HAWKEYE && player.getClassId() === ClassIdValues.rogue.id ) {
            if ( player.getLevel() < minimumLevel ) {
                if ( QuestHelper.hasQuestItems( player, MARK_OF_SEEKER, MARK_OF_TRUST, MARK_OF_SAGITTARIUS ) ) {
                    return this.getPath( '30109-60.htm' )
                }

                return this.getPath( '30109-61.htm' )
            }

            if ( QuestHelper.hasQuestItems( player, MARK_OF_SEEKER, MARK_OF_TRUST, MARK_OF_SAGITTARIUS ) ) {
                await QuestHelper.takeMultipleItems( player, -1, MARK_OF_SEEKER, MARK_OF_TRUST, MARK_OF_SAGITTARIUS )

                await player.setClassId( HAWKEYE )
                player.setBaseClass( HAWKEYE )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                return this.getPath( '30109-62.htm' )
            }

            return this.getPath( '30109-63.htm' )
        }

        if ( classId === TEMPLE_KNIGHT && player.getClassId() === ClassIdValues.elvenKnight.id ) {
            let hasRequiredItems: boolean = QuestHelper.hasQuestItems( player, MARK_OF_DUTY, MARK_OF_LIFE, MARK_OF_HEALER )
            if ( player.getLevel() < minimumLevel ) {
                if ( hasRequiredItems ) {
                    return this.getPath( '30109-64.htm' )
                }

                return this.getPath( '30109-65.htm' )
            }

            if ( hasRequiredItems ) {
                await QuestHelper.takeMultipleItems( player, -1, MARK_OF_DUTY, MARK_OF_LIFE, MARK_OF_HEALER )

                await player.setClassId( TEMPLE_KNIGHT )
                player.setBaseClass( TEMPLE_KNIGHT )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                return this.getPath( '30109-66.htm' )
            }

            return this.getPath( '30109-67.htm' )
        }

        if ( classId === SWORDSINGER && player.getClassId() === ClassIdValues.elvenKnight.id ) {
            let hasRequiredItems: boolean = QuestHelper.hasQuestItems( player, MARK_OF_CHALLENGER, MARK_OF_LIFE, MARK_OF_DUELIST )
            if ( player.getLevel() < minimumLevel ) {
                if ( hasRequiredItems ) {
                    return this.getPath( '30109-68.htm' )
                }

                return this.getPath( '30109-69.htm' )
            }

            if ( hasRequiredItems ) {
                await QuestHelper.takeMultipleItems( player, -1, MARK_OF_CHALLENGER, MARK_OF_LIFE, MARK_OF_DUELIST )

                await player.setClassId( SWORDSINGER )
                player.setBaseClass( SWORDSINGER )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                return this.getPath( '30109-70.htm' )
            }

            return this.getPath( '30109-71.htm' )
        }

        if ( classId === PLAINS_WALKER && player.getClassId() === ClassIdValues.elvenScout.id ) {
            if ( player.getLevel() < minimumLevel ) {
                if ( QuestHelper.hasQuestItems( player, MARK_OF_SEEKER, MARK_OF_LIFE, MARK_OF_SEARCHER ) ) {
                    return this.getPath( '30109-72.htm' )
                }

                return this.getPath( '30109-73.htm' )
            }

            if ( QuestHelper.hasQuestItems( player, MARK_OF_SEEKER, MARK_OF_LIFE, MARK_OF_SEARCHER ) ) {
                await QuestHelper.takeMultipleItems( player, -1, MARK_OF_SEEKER, MARK_OF_LIFE, MARK_OF_SEARCHER )

                await player.setClassId( PLAINS_WALKER )
                player.setBaseClass( PLAINS_WALKER )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                return this.getPath( '30109-74.htm' )
            }

            return this.getPath( '30109-75.htm' )
        }

        if ( classId === SILVER_RANGER && player.getClassId() === ClassIdValues.elvenScout.id ) {
            if ( player.getLevel() < minimumLevel ) {
                if ( QuestHelper.hasQuestItems( player, MARK_OF_SEEKER, MARK_OF_LIFE, MARK_OF_SAGITTARIUS ) ) {
                    return this.getPath( '30109-76.htm' )
                }

                return this.getPath( '30109-77.htm' )
            }

            if ( QuestHelper.hasQuestItems( player, MARK_OF_SEEKER, MARK_OF_LIFE, MARK_OF_SAGITTARIUS ) ) {
                await QuestHelper.takeMultipleItems( player, -1, MARK_OF_SEEKER, MARK_OF_LIFE, MARK_OF_SAGITTARIUS )

                await player.setClassId( SILVER_RANGER )
                player.setBaseClass( SILVER_RANGER )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                return this.getPath( '30109-78.htm' )
            }

            return this.getPath( '30109-79.htm' )
        }
    }
}