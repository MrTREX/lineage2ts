import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { CategoryType } from '../../gameService/enums/CategoryType'
import { QuestHelper } from '../helpers/QuestHelper'

import _ from 'lodash'

const npcIds: Array<number> = [
    30120, // Maximilian
    30191, // Hollint
    30857, // Orven
    30905, // Squillari
    31279, // Gregory
    31328, // Innocentin
    31968, // Baryl
]

const MARK_OF_PILGRIM = 2721
const MARK_OF_TRUST = 2734
const MARK_OF_HEALER = 2820
const MARK_OF_REFORMER = 2821
const MARK_OF_LIFE = 3140

const BISHOP = ClassIdValues.bishop.id
const PROPHET = ClassIdValues.prophet.id
const ELDER = ClassIdValues.elder.id

const minimumLevel = 40
const rewardItemId = 8870
const rewardAmount = 15

export class ElfHumanClericChange2 extends ListenerLogic {

    constructor() {
        super( 'ElfHumanClericChange2', 'quests/villageMasters/ElfHumanClericChange2.ts' )
    }

    getPathPrefix(): string {
        return 'data/datapack/village_master/ElfHumanClericChange2'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        switch ( data.eventName ) {
            case '30120-02.htm':
            case '30120-03.htm':
            case '30120-04.htm':
            case '30120-05.htm':
            case '30120-06.htm':
            case '30120-07.htm':
            case '30120-08.htm':
            case '30120-10.htm':
            case '30120-11.htm':
            case '30120-12.htm':
                return this.getPath( data.eventName )

            case '16':
            case '17':
            case '30':
                return this.getRequestedClassChange( data )
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )

        if ( player.isInCategory( CategoryType.CLERIC_GROUP )
                && player.isInCategory( CategoryType.FOURTH_CLASS_GROUP )
                && ( player.isInCategory( CategoryType.HUMAN_CALL_CLASS ) || player.isInCategory( CategoryType.ELF_CALL_CLASS ) ) ) {
            return this.getPath( '30120-01.htm' )
        }

        if ( player.isInCategory( CategoryType.CLERIC_GROUP )
                && ( player.isInCategory( CategoryType.HUMAN_CALL_CLASS ) || player.isInCategory( CategoryType.ELF_CALL_CLASS ) ) ) {
            if ( [ ClassIdValues.cleric.id, ClassIdValues.bishop.id, ClassIdValues.prophet.id ].includes( player.getClassId() ) ) {
                return this.getPath( '30120-02.htm' )
            }

            if ( [ ClassIdValues.oracle.id, ClassIdValues.elder.id ].includes( player.getClassId() ) ) {
                return this.getPath( '30120-09.htm' )
            }

            return this.getPath( '30120-13.htm' )
        }

        return this.getPath( '30120-14.htm' )
    }

    async getRequestedClassChange( data: NpcGeneralEvent ): Promise<string> {
        const player = L2World.getPlayer( data.playerId )
        const classId: number = _.parseInt( data.eventName )

        if ( player.isInCategory( CategoryType.THIRD_CLASS_GROUP ) ) {
            return this.getPath( '30120-15.htm' )
        }

        if ( classId === BISHOP && player.getClassId() === ClassIdValues.cleric.id ) {
            let hasRequiredItems: boolean = QuestHelper.hasQuestItems( player, MARK_OF_PILGRIM, MARK_OF_TRUST, MARK_OF_HEALER )
            if ( player.getLevel() < minimumLevel ) {
                if ( hasRequiredItems ) {
                    return this.getPath( '30120-16.htm' )
                }

                return this.getPath( '30120-17.htm' )
            }

            if ( hasRequiredItems ) {
                await QuestHelper.takeMultipleItems( player, -1, MARK_OF_PILGRIM, MARK_OF_TRUST, MARK_OF_HEALER )

                await player.setClassId( BISHOP )
                player.setBaseClass( BISHOP )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                return this.getPath( '30120-18.htm' )
            }

            return this.getPath( '30120-19.htm' )
        }

        if ( classId === PROPHET && player.getClassId() === ClassIdValues.cleric.id ) {
            if ( player.getLevel() < minimumLevel ) {
                if ( QuestHelper.hasQuestItems( player, MARK_OF_PILGRIM, MARK_OF_TRUST, MARK_OF_REFORMER ) ) {
                    return this.getPath( '30120-20.htm' )
                }

                return this.getPath( '30120-21.htm' )
            }

            if ( QuestHelper.hasQuestItems( player, MARK_OF_PILGRIM, MARK_OF_TRUST, MARK_OF_REFORMER ) ) {
                await QuestHelper.takeMultipleItems( player, -1, MARK_OF_PILGRIM, MARK_OF_TRUST, MARK_OF_REFORMER )

                await player.setClassId( PROPHET )
                player.setBaseClass( PROPHET )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                return this.getPath( '30120-22.htm' )
            }

            return this.getPath( '30120-23.htm' )
        }

        if ( classId === ELDER && player.getClassId() === ClassIdValues.oracle.id ) {
            if ( player.getLevel() < minimumLevel ) {
                if ( QuestHelper.hasQuestItems( player, MARK_OF_PILGRIM, MARK_OF_LIFE, MARK_OF_HEALER ) ) {
                    return this.getPath( '30120-24.htm' )
                }

                return this.getPath( '30120-25.htm' )
            }

            if ( QuestHelper.hasQuestItems( player, MARK_OF_PILGRIM, MARK_OF_LIFE, MARK_OF_HEALER ) ) {
                await QuestHelper.takeMultipleItems( player, -1, MARK_OF_PILGRIM, MARK_OF_LIFE, MARK_OF_HEALER )

                await player.setClassId( ELDER )
                player.setBaseClass( ELDER )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                return this.getPath( '30120-26.htm' )
            }

            return this.getPath( '30120-27.htm' )
        }
    }
}