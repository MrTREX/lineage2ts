import { SkillHolder } from '../../../gameService/models/holders/SkillHolder'

export const GOLDEN_SPICE = 15474
export const CRYSTAL_SPICE = 15475
export const SKILL_GOLDEN_SPICE = 9049
export const SKILL_CRYSTAL_SPICE = 9050
export const SKILL_BLESSED_GOLDEN_SPICE = 9051
export const SKILL_BLESSED_CRYSTAL_SPICE = 9052
export const SKILL_SGRADE_GOLDEN_SPICE = 9053
export const SKILL_SGRADE_CRYSTAL_SPICE = 9054

export interface BeastFarmGrowingMonster {
    chance: number
    growthLevel: number
    tameNpcId: number
    skillMap: {
        [ skillId: number ]: number
    }
}

export const BestFarmGrowthData: { [ npcId: number ]: BeastFarmGrowingMonster } = {
    18873: {
        chance: 100,
        growthLevel: 0,
        tameNpcId: 18869,
        skillMap: {
            [ SKILL_GOLDEN_SPICE ]: 18874,
            [ SKILL_CRYSTAL_SPICE ]: 18875,
            [ SKILL_BLESSED_GOLDEN_SPICE ]: 18869,
            [ SKILL_BLESSED_CRYSTAL_SPICE ]: 18869,
            [ SKILL_SGRADE_GOLDEN_SPICE ]: 18878,
            [ SKILL_SGRADE_CRYSTAL_SPICE ]: 18879,
        },
    },

    18874: {
        chance: 40,
        growthLevel: 1,
        tameNpcId: 18869,
        skillMap: {
            [ SKILL_GOLDEN_SPICE ]: 18876,
        },
    },

    18875: {
        chance: 40,
        growthLevel: 1,
        tameNpcId: 18869,
        skillMap: {
            [ SKILL_CRYSTAL_SPICE ]: 18877,
        },
    },

    18876: {
        chance: 25,
        growthLevel: 2,
        tameNpcId: 18869,
        skillMap: {
            [ SKILL_GOLDEN_SPICE ]: 18878,
        },
    },

    18877: {
        chance: 25,
        growthLevel: 2,
        tameNpcId: 18869,
        skillMap: {
            [ SKILL_CRYSTAL_SPICE ]: 18879,
        },
    },

    18880: {
        chance: 100,
        growthLevel: 0,
        tameNpcId: 18870,
        skillMap: {
            [ SKILL_GOLDEN_SPICE ]: 18881,
            [ SKILL_CRYSTAL_SPICE ]: 18882,
            [ SKILL_BLESSED_GOLDEN_SPICE ]: 18870,
            [ SKILL_BLESSED_CRYSTAL_SPICE ]: 18870,
            [ SKILL_SGRADE_GOLDEN_SPICE ]: 18885,
            [ SKILL_SGRADE_CRYSTAL_SPICE ]: 18886,
        },
    },

    18881: {
        chance: 40,
        growthLevel: 1,
        tameNpcId: 18870,
        skillMap: {
            [ SKILL_GOLDEN_SPICE ]: 18883,
        },
    },

    18882: {
        chance: 40,
        growthLevel: 1,
        tameNpcId: 18870,
        skillMap: {
            [ SKILL_CRYSTAL_SPICE ]: 18884,
        },
    },

    18883: {
        chance: 25,
        growthLevel: 2,
        tameNpcId: 18870,
        skillMap: {
            [ SKILL_GOLDEN_SPICE ]: 18885,
        },
    },

    18884: {
        chance: 25,
        growthLevel: 2,
        tameNpcId: 18870,
        skillMap: {
            [ SKILL_CRYSTAL_SPICE ]: 18886,
        },
    },

    18887: {
        chance: 100,
        growthLevel: 0,
        tameNpcId: 18871,
        skillMap: {
            [ SKILL_GOLDEN_SPICE ]: 18888,
            [ SKILL_CRYSTAL_SPICE ]: 18889,
            [ SKILL_BLESSED_GOLDEN_SPICE ]: 18871,
            [ SKILL_BLESSED_CRYSTAL_SPICE ]: 18871,
            [ SKILL_SGRADE_GOLDEN_SPICE ]: 18892,
            [ SKILL_SGRADE_CRYSTAL_SPICE ]: 18893,
        },
    },

    18888: {
        chance: 40,
        growthLevel: 1,
        tameNpcId: 18871,
        skillMap: {
            [ SKILL_GOLDEN_SPICE ]: 18890,
        },
    },

    18889: {
        chance: 40,
        growthLevel: 1,
        tameNpcId: 18871,
        skillMap: {
            [ SKILL_CRYSTAL_SPICE ]: 18891,
        },
    },

    18890: {
        chance: 25,
        growthLevel: 2,
        tameNpcId: 18871,
        skillMap: {
            [ SKILL_GOLDEN_SPICE ]: 18892,
        },
    },

    18891: {
        chance: 25,
        growthLevel: 2,
        tameNpcId: 18871,
        skillMap: {
            [ SKILL_CRYSTAL_SPICE ]: 18893,
        },
    },

    18894: {
        chance: 100,
        growthLevel: 0,
        tameNpcId: 18872,
        skillMap: {
            [ SKILL_GOLDEN_SPICE ]: 18895,
            [ SKILL_CRYSTAL_SPICE ]: 18896,
            [ SKILL_BLESSED_GOLDEN_SPICE ]: 18872,
            [ SKILL_BLESSED_CRYSTAL_SPICE ]: 18872,
            [ SKILL_SGRADE_GOLDEN_SPICE ]: 18899,
            [ SKILL_SGRADE_CRYSTAL_SPICE ]: 18900,
        },
    },

    18895: {
        chance: 40,
        growthLevel: 1,
        tameNpcId: 18872,
        skillMap: {
            [ SKILL_GOLDEN_SPICE ]: 18897
        }
    },

    18896: {
        chance: 40,
        growthLevel: 1,
        tameNpcId: 18872,
        skillMap: {
            [ SKILL_CRYSTAL_SPICE ]: 18898
        }
    },

    18897: {
        chance: 25,
        growthLevel: 2,
        tameNpcId: 18872,
        skillMap: {
            [ SKILL_GOLDEN_SPICE ]: 18899
        }
    },

    18898: {
        chance: 25,
        growthLevel: 2,
        tameNpcId: 18872,
        skillMap: {
            [ SKILL_CRYSTAL_SPICE ]: 18900
        }
    }
}

export interface BeastFarmTamedBeast {
    template: string
    skills: Array<SkillHolder>
}

export const BeastFarmTamedBeasts : Array<BeastFarmTamedBeast> = [
    {
        template: '%name% of Focus',
        skills: [ new SkillHolder( 6432, 1 ), new SkillHolder( 6668, 1 ) ]
    },
    {
        template: '%name% of Guiding',
        skills: [ new SkillHolder( 6433, 1 ), new SkillHolder( 6670, 1 ) ]
    },
    {
        template: '%name% of Swifth',
        skills: [ new SkillHolder( 6434, 1 ), new SkillHolder( 6667, 1 ) ]
    },
    {
        template: 'Berserker %name%',
        skills: [ new SkillHolder( 6671, 1 ) ]
    },
    {
        template: '%name% of Protection',
        skills: [ new SkillHolder( 6669, 1 ), new SkillHolder( 6672, 1 ) ]
    },
    {
        template: '%name% of Vigor',
        skills: [ new SkillHolder( 6431, 1 ), new SkillHolder( 6666, 1 ) ]
    }
]