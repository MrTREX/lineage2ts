import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableAttackedEvent, AttackableKillEvent, NpcSpawnEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2Attackable } from '../../gameService/models/actor/L2Attackable'
import { SkillHolder } from '../../gameService/models/holders/SkillHolder'
import { ClassId, ClassIdValues } from '../../gameService/models/base/ClassId'
import { ILocational } from '../../gameService/models/Location'
import { L2Party } from '../../gameService/models/L2Party'
import { PlayerGroupCache } from '../../gameService/cache/PlayerGroupCache'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { Skill } from '../../gameService/models/Skill'
import { QuestHelper } from '../helpers/QuestHelper'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { L2Playable } from '../../gameService/models/actor/L2Playable'
import { AIEffectHelper } from '../../gameService/aicontroller/helpers/AIEffectHelper'
import _ from 'lodash'

const DRAKOS_ASSASSIN = 22823
const GREATER_HERB_OF_MANA = 8604
const SUPERIOR_HERB_OF_MANA = 8605
const MORALE_BOOST1 = new SkillHolder( 6885 )
const MORALE_BOOST2 = new SkillHolder( 6885, 2 )
const MORALE_BOOST3 = new SkillHolder( 6885, 3 )
const requiredDistance = 1500
const requiredMemberCount = 3
const minimumLevel = 80
const requiredClassLevel = 3

const boostSkillPoints: { [ classId: number ]: number } = {
    [ ClassIdValues.adventurer.id ]: 0.2,
    [ ClassIdValues.arcanaLord.id ]: 1.5,
    [ ClassIdValues.archmage.id ]: 0.3,
    [ ClassIdValues.cardinal.id ]: -0.6,
    [ ClassIdValues.dominator.id ]: 0.2,
    [ ClassIdValues.doombringer.id ]: 0.2,
    [ ClassIdValues.doomcryer.id ]: 0.1,
    [ ClassIdValues.dreadnought.id ]: 0.7,
    [ ClassIdValues.duelist.id ]: 0.2,
    [ ClassIdValues.elementalMaster.id ]: 1.4,
    [ ClassIdValues.evaSaint.id ]: -0.6,
    [ ClassIdValues.evaTemplar.id ]: 0.8,
    [ ClassIdValues.femaleSoulhound.id ]: 0.4,
    [ ClassIdValues.fortuneSeeker.id ]: 0.9,
    [ ClassIdValues.ghostHunter.id ]: 0.2,
    [ ClassIdValues.ghostSentinel.id ]: 0.2,
    [ ClassIdValues.grandKhavatari.id ]: 0.2,
    [ ClassIdValues.hellKnight.id ]: 0.6,
    [ ClassIdValues.hierophant.id ]: 0.0,
    [ ClassIdValues.judicator.id ]: 0.1,
    [ ClassIdValues.moonlightSentinel.id ]: 0.2,
    [ ClassIdValues.maestro.id ]: 0.7,
    [ ClassIdValues.maleSoulhound.id ]: 0.4,
    [ ClassIdValues.mysticMuse.id ]: 0.3,
    [ ClassIdValues.phoenixKnight.id ]: 0.6,
    [ ClassIdValues.sagittarius.id ]: 0.2,
    [ ClassIdValues.shillienSaint.id ]: -0.6,
    [ ClassIdValues.shillienTemplar.id ]: 0.8,
    [ ClassIdValues.soultaker.id ]: 0.3,
    [ ClassIdValues.spectralDancer.id ]: 0.4,
    [ ClassIdValues.spectralMaster.id ]: 1.4,
    [ ClassIdValues.stormScreamer.id ]: 0.3,
    [ ClassIdValues.swordMuse.id ]: 0.4,
    [ ClassIdValues.titan.id ]: 0.3,
    [ ClassIdValues.trickster.id ]: 0.5,
    [ ClassIdValues.windRider.id ]: 0.2,
}

const dropOnKillItems: Array<number> = [ GREATER_HERB_OF_MANA, SUPERIOR_HERB_OF_MANA ]

export class DragonValley extends ListenerLogic {
    constructor() {
        super( 'DragonValley', 'listeners/areas/DragonValley.ts' )
    }

    getAttackableAttackIds(): Array<number> {
        return [
            22824, // Drakos Guardian
            22862, // Drakos Hunter
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            22822, // Drakos Warrior
            22823, // Drakos Assassin
            22824, // Drakos Guardian
            22825, // Giant Scorpion Bones
            22826, // Scorpion Bones
            22827, // Batwing Drake
            22828, // Parasitic Leech
            22829, // Emerald Drake
            22830, // Gem Dragon
            22831, // Dragon Tracker of the Valley
            22832, // Dragon Scout of the Valley
            22833, // Sand Drake Tracker
            22834, // Dust Dragon Tracker
            22860, // Hungry Parasitic Leech
            22861, // Hard Scorpion Bones
            22862, // Drakos Hunter
        ]
    }

    getSpawnIds(): Array<number> {
        return [
            22826, // Scorpion Bones
            22823, // Drakos Assassin
            22828, // Parasitic Leech
        ]
    }

    async onSpawnEvent( data: NpcSpawnEvent ): Promise<void> {
        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        npc.setShowSummonAnimation( true )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let npc = L2World.getObjectById( data.targetId ) as L2Attackable
        if ( !npc.isSpoiled() ) {
            return
        }

        let itemId: number = _.sample( dropOnKillItems )
        npc.dropSingleItem( itemId, 1, data.playerId )
        await this.applyBoostSkill( data.playerId, npc )
    }

    async applyBoostSkill( playerId: number, npc: ILocational ): Promise<void> {
        let party: L2Party = PlayerGroupCache.getParty( playerId )
        if ( !party || party.getMemberCount() < requiredMemberCount ) {
            return
        }

        let applicablePlayers: Array<L2PcInstance> = []
        let points: number = party.getMembers().reduce( ( total: number, partyPlayerId: number ): number => {
            let player = L2World.getPlayer( partyPlayerId )
            if ( player.getLevel() >= minimumLevel
                    && ClassId.getLevelByClassId( player.getClassId() ) >= requiredClassLevel
                    && player.calculateDistance( npc, true ) < requiredDistance ) {
                applicablePlayers.push( player )
                return total + boostSkillPoints[ player.getClassId() ]
            }

            return total
        }, 0 )

        if ( points < 1 ) {
            return
        }

        let skill: Skill = this.getBoostSkill( Math.floor( points ) )
        await Promise.all( applicablePlayers.map( ( player: L2PcInstance ) => {
            return skill.applyEffects( player, player )
        } ) )
    }

    getBoostSkill( points: number ): Skill {
        switch ( points ) {
            case 1:
                return MORALE_BOOST1.getSkill()

            case 2:
                return MORALE_BOOST2.getSkill()
        }

        return MORALE_BOOST3.getSkill()
    }

    async onAttackableAttackedEvent( data: AttackableAttackedEvent ): Promise<void> {
        if ( Math.random() > 0.05 || NpcVariablesManager.get( data.targetId, this.getName() ) ) {
            return
        }

        let npc = L2World.getObjectById( data.targetId ) as L2Npc
        if ( npc.getCurrentHp() < ( npc.getMaxHp() / 2 ) ) {
            NpcVariablesManager.set( data.targetId, this.getName(), true )
            let attacker = L2World.getObjectById( data.attackerId ) as L2Playable

            _.times( _.random( 3, 5 ), () => {
                let mob: L2Npc = QuestHelper.addSpawnAtLocation( DRAKOS_ASSASSIN, npc, true, 0, true )
                AIEffectHelper.notifyAttacked( mob, attacker )
            } )
        }
    }
}