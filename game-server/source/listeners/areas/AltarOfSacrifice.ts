import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import _ from 'lodash'
import { DataManager } from '../../data/manager'
import { L2ManualSpawn } from '../../gameService/models/spawns/type/L2ManualSpawn'

type LocationData = [ Location ] |
        [ Location, number ] |
        [ Location, number, number ] |
        [ Location, number, number, number ]
const locationData: Array<LocationData> = [
    // TalkingIsland
    [ new Location( -92481, 244812, -3505 ) ],

    // Elven
    [ new Location( 40241, 53974, -3262 ) ],

    // DarkElven
    [ new Location( 1851, 21697, -3305 ), 25750 ],

    // Dwarven
    [ new Location( 130133, -180968, -3271 ), 25800, 25782 ],

    // Orc
    [ new Location( -45329, -118327, -166 ), 25779 ],

    // Kamael
    [ new Location( -104031, 45059, -1417 ) ],

    // Oren
    [ new Location( 80188, 47037, -3109 ), 25767, 25770 ],

    // Gludin
    [ new Location( -86620, 151536, -3018 ), 25735, 25738, 25741 ],

    // Gludio
    [ new Location( -14152, 120674, -2935 ), 25744, 25747 ],

    // Dion
    [ new Location( 16715, 148320, -3210 ), 25753, 25754, 25757 ],

    // Heine
    [ new Location( 120123, 219164, -3319 ), 25773, 25776 ],

    // Giran
    [ new Location( 80712, 142538, -3487 ), 25760, 25763, 25766 ],

    // Aden
    [ new Location( 152720, 24714, -2083 ), 25793, 25794, 25797 ],

    // Rune
    [ new Location( 28010, -49175, -1278 ) ],

    // Goddard
    [ new Location( 152274, -57706, -3383 ), 25787, 25790 ],

    // Schutgart
    [ new Location( 82066, -139418, -2220 ), 25784 ],

    // Primeval
    [ new Location( 10998, -24068, -3603 ) ],

    // Dragon Valley
    [ new Location( 69592, 118694, -3417 ) ],
]

const minRadius = 250
const maxRadius = 500
const changeDelay = GeneralHelper.hoursToMillis( 4 )
const initialDelay = GeneralHelper.minutesToMillis( 3 )

const eventNames = {
    spawn: 'sp',
    despawn: 'dsp',
}

function createSpawnEvent( index: number ): string {
    return `${ eventNames.spawn }:${ index }`
}

function createDespawnEvent( index: number ): string {
    return `${ eventNames.despawn }:${ index }`
}

export class AltarOfSacrifice extends ListenerLogic {
    allAltars: Array<AltarData>

    constructor() {
        super( 'AltarOfSacrifice', 'listeners/areas/AltarOfSacrifice.ts' )
    }

    async despawnBoss( altarIndex: number ): Promise<boolean> {
        let data: AltarData = _.nth( this.allAltars, altarIndex )

        if ( data.spawnedBossId === 0 ) {
            return true
        }

        let npc: L2Npc = L2World.getObjectById( data.spawnedBossId ) as L2Npc
        if ( npc && npc.isInCombat() ) {
            return false
        }

        await npc.deleteMe()
        data.spawnedBossId = 0

        return true
    }

    async initialize(): Promise<void> {
        let quest = this
        this.allAltars = _.map( locationData, ( locationItem: LocationData, index: number ): AltarData => {
            let [ location, ...bossIds ] = locationItem

            if ( !_.isEmpty( bossIds ) ) {
                quest.startQuestTimer( createSpawnEvent( index ), _.random( initialDelay ) + 10000, 0, 0 )
            }

            return {
                bossIds,
                location,
                spawnedBossId: 0,
            }
        } )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let [ operation, indexValue ] = _.split( data.eventName, ':' )
        let index: number = _.parseInt( indexValue )

        switch ( operation ) {
            case eventNames.spawn:
                if ( this.spawnBoss( index ) ) {
                    this.startQuestTimer( createDespawnEvent( index ), changeDelay, 0, 0 )
                    return
                }

                this.startQuestTimer( data.eventName, 10000, 0, 0 )
                return

            case eventNames.despawn:
                if ( await this.despawnBoss( index ) ) {
                    this.startQuestTimer( createSpawnEvent( index ), changeDelay, 0, 0 )
                    return
                }

                this.startQuestTimer( data.eventName, 10000, 0, 0 )
                return
        }
    }

    spawnBoss( altarIndex: number ): boolean {
        let data: AltarData = _.nth( this.allAltars, altarIndex )

        if ( _.isEmpty( data.bossIds ) || data.spawnedBossId > 0 ) {
            return true
        }

        let radius = _.random( minRadius, maxRadius )
        let angleRadians = Math.random() * 2 * Math.PI
        let spawnX = Math.floor( radius * Math.cos( angleRadians ) ) + data.location.getX()
        let spawnY = Math.floor( radius * Math.sin( angleRadians ) ) + data.location.getY()
        let npcId = _.sample( data.bossIds )
        let template = DataManager.getNpcData().getTemplate( npcId )

        let spawn: L2ManualSpawn = L2ManualSpawn.fromParameters( template, 1, spawnX, spawnY, data.location.getZ(), _.random( 65536 ), 0 )

        spawn.startSpawn()

        data.spawnedBossId = spawn.getNpcObjectIds()[ 0 ]
    }
}

interface AltarData {
    bossIds: Array<number>
    location: Location
    spawnedBossId: number
}