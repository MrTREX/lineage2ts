import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, EventType, NpcSeeSkillEvent } from '../../gameService/models/events/EventType'
import { BeastFarmTamedBeast, BeastFarmTamedBeasts, BestFarmGrowthData } from './data/BeastFarmData'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { SocialAction } from '../../gameService/packets/send/SocialAction'
import { L2Attackable } from '../../gameService/models/actor/L2Attackable'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2TamedBeastInstance } from '../../gameService/models/actor/instance/L2TamedBeastInstance'
import { NpcInfo } from '../../gameService/packets/send/NpcInfo'
import { SkillHolder } from '../../gameService/models/holders/SkillHolder'
import { QuestHelper } from '../helpers/QuestHelper'
import { AIEffectHelper } from '../../gameService/aicontroller/helpers/AIEffectHelper'
import _ from 'lodash'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'

const GOLDEN_SPICE = 15474
const CRYSTAL_SPICE = 15475
const SKILL_GOLDEN_SPICE = 9049
const SKILL_CRYSTAL_SPICE = 9050
const SKILL_BLESSED_GOLDEN_SPICE = 9051
const SKILL_BLESSED_CRYSTAL_SPICE = 9052
const SKILL_SGRADE_GOLDEN_SPICE = 9053
const SKILL_SGRADE_CRYSTAL_SPICE = 9054
const validSkills: Set<number> = new Set<number>( [
    SKILL_GOLDEN_SPICE,
    SKILL_CRYSTAL_SPICE,
    SKILL_BLESSED_GOLDEN_SPICE,
    SKILL_BLESSED_CRYSTAL_SPICE,
    SKILL_SGRADE_GOLDEN_SPICE,
    SKILL_SGRADE_CRYSTAL_SPICE,
] )

const tamedBeastIds = [
    18869,
    18870,
    18871,
    18872,
]
const TAME_CHANCE = 20
const SPECIAL_SPICE_CHANCES = [
    33,
    75,
]

// all mobs that can eat...
const feedableBeastNpcIds = [
    // Kookaburras
    18873,
    18874,
    18875,
    18876,
    18877,
    18878,
    18879,
    // Cougars
    18880,
    18881,
    18882,
    18883,
    18884,
    18885,
    18886,
    // Buffalos
    18887,
    18888,
    18889,
    18890,
    18891,
    18892,
    18893,
    // Grendels
    18894,
    18895,
    18896,
    18897,
    18898,
    18899,
    18900,
]

export class BeastFarm extends ListenerLogic {
    playerFeedData : { [ objectId: number ] : number } = {}

    constructor() {
        super( 'BeastFarm', 'listeners/areas/BeastFarm.ts' )
    }

    getAttackableKillIds(): Array<number> {
        return feedableBeastNpcIds
    }

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.NpcId,
                eventType: EventType.NpcSeeSkill,
                method: this.onNpcSeeSkill.bind( this ),
                ids: feedableBeastNpcIds,
                triggers: {
                    targetIds: new Set( [ SKILL_GOLDEN_SPICE, SKILL_BLESSED_GOLDEN_SPICE, SKILL_CRYSTAL_SPICE, SKILL_BLESSED_CRYSTAL_SPICE ] )
                }
            }
        ]
    }

    async onNpcSeeSkill( data: NpcSeeSkillEvent ): Promise<void> {
        if ( !data.targetIds.includes( data.receiverId ) || data.playerId === 0 ) {
            return
        }


        if ( !feedableBeastNpcIds.includes( data.receiverNpcId ) || !validSkills.has( data.skillId ) ) {
            return
        }

        let growthLevel = _.get( BestFarmGrowthData, [ data.receiverNpcId, 'growthLevel' ], 3 )

        // prevent exploit which allows 2 players to simultaneously raise the same 0-growth beast
        // If the mob is at 0th level (when it still listens to all feeders) lock it to the first feeder!
        if ( growthLevel === 0 && this.playerFeedData[ data.receiverId ] ) {
            return
        }

        if ( growthLevel > 0 && this.playerFeedData[ data.receiverId ] !== data.playerId ) {
            // check if this is the same player as the one who raised it from growth 0.
            // if no, then do not allow a chance to raise the pet (food gets consumed but has no effect).
            return
        }

        this.playerFeedData[ data.receiverId ] = data.playerId

        let npc : L2Npc = L2World.getObjectById( data.receiverId ) as L2Npc
        BroadcastHelper.dataInLocation( npc, SocialAction( data.receiverId, 2 ), npc.getBroadcastRadius() )

        let itemId = 0
        if ( data.skillId === SKILL_GOLDEN_SPICE || data.skillId === SKILL_BLESSED_GOLDEN_SPICE ) {
            itemId = GOLDEN_SPICE
        } else if ( data.skillId === SKILL_CRYSTAL_SPICE || data.skillId === SKILL_BLESSED_CRYSTAL_SPICE ) {
            itemId = CRYSTAL_SPICE
        }

        let player = L2World.getPlayer( data.playerId )
        if ( !player ) {
            return
        }

        if ( BestFarmGrowthData[ data.receiverNpcId ] ) {
            let nextNpcId : number = this.getNextNpcId( data.receiverNpcId, data.skillId )
            if ( nextNpcId < 0 ) {
                if ( growthLevel === 0 ) {
                    _.unset( this.playerFeedData, data.receiverId )
                    AIEffectHelper.notifyAttacked( npc, player )
                }

                return
            }

            return this.spawnNextBeast( npc, player, nextNpcId, itemId )
        }

        player.sendMessage( 'The beast spit out the feed instead of eating it.' )
        npc.dropSingleItem( itemId, 1, data.playerId )
    }

    getNextNpcId( npcId: number, skillId: number ) : number {
        let data = BestFarmGrowthData[ npcId ]
        if ( !data.skillMap[ skillId ] ) {
            return -1
        }

        if ( [ SKILL_BLESSED_GOLDEN_SPICE, SKILL_BLESSED_CRYSTAL_SPICE, SKILL_SGRADE_GOLDEN_SPICE, SKILL_SGRADE_CRYSTAL_SPICE ].includes( skillId ) ) {
            if ( _.random( 100 ) < SPECIAL_SPICE_CHANCES[ 0 ] ) {
                if ( _.random( 100 ) < SPECIAL_SPICE_CHANCES[ 1 ] ) {
                    return data.skillMap[ skillId ]
                }

                if ( skillId === SKILL_BLESSED_GOLDEN_SPICE || skillId === SKILL_SGRADE_GOLDEN_SPICE ) {
                    return data.skillMap[ SKILL_GOLDEN_SPICE ]
                }

                return data.skillMap[ SKILL_CRYSTAL_SPICE ]
            }

            return -1
        }

        if ( data.growthLevel === 2 && _.random( 100 ) < TAME_CHANCE ) {
            return data.tameNpcId
        }

        if ( _.random( 100 ) < data.chance ) {
            return data.skillMap[ skillId ]
        }

        return -1
    }

    async spawnNextBeast( npc: L2Npc, player: L2PcInstance, nextNpcId: number, food: number ) : Promise<void> {

        if ( this.playerFeedData[ npc.getObjectId() ] === player.getObjectId() ) {
            _.unset( this.playerFeedData, npc.getObjectId() )
        }

        await npc.deleteMe()

        // if this is finally a trained mob, then despawn any other trained mobs that the
        // player might have and initialize the Tamed Beast.
        if ( tamedBeastIds.includes( nextNpcId ) ) {
            let nextNpc = new L2TamedBeastInstance( nextNpcId, player.getObjectId(), food, npc.getX(), npc.getY(), npc.getZ(), true )

            let data : BeastFarmTamedBeast = _.sample( BeastFarmTamedBeasts )
            nextNpc.setName( data.template.replace( '%name%', this.getNpcName( nextNpcId ) ) )

            BroadcastHelper.dataBasedOnVisibility( nextNpc, NpcInfo( nextNpc.getObjectId(), player.getObjectId() ) )
            nextNpc.setRunning()

            nextNpc.beastSkills = _.map( data.skills, ( holder : SkillHolder ) => {
                return holder.getSkill()
            } )

            return this.checkJewelOfInnocence( player )
        }

        // if not trained, the newly spawned mob will automatically be agro against its feeder
        // (what happened to "never bite the hand that feeds you" anyway?!)
        let nextNpc : L2Attackable = QuestHelper.addSpawnAtLocation( nextNpcId, npc ) as L2Attackable
        this.playerFeedData[ nextNpc.getObjectId() ] = player.getObjectId()

        nextNpc.setRunning()
        player.setTarget( nextNpc )

        return AIEffectHelper.notifyAttacked( nextNpc, player, 99999 )
    }

    getNpcName( npcId: number ) : string {
        switch ( npcId ) {
            case 18869:
                return 'Alpine Kookaburra'

            case 18870:
                return 'Alpine Cougar'

            case 18871:
                return 'Alpine Buffalo'

            case 18872:
                return 'Alpine Grendel'
        }

        return 'Not Defined'
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        _.unset( this.playerFeedData, data.targetId )
        return
    }

    async checkJewelOfInnocence( player: L2PcInstance ): Promise<void> {
        if ( _.random( 100 ) > 5 || QuestHelper.hasQuestItems( player, 15533 ) ) {
            return
        }

        let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), 'Q00020_BringUpWithLove' )
        if ( state && state.isCondition( 1 ) ) {
            await QuestHelper.giveSingleItem( player, 15533, 1 )
            state.setConditionWithSound( 2, true )
        }
    }
}