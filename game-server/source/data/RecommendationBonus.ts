import { L2PcInstance } from '../gameService/models/actor/instance/L2PcInstance'

const bonus = [
    [
        25,
        50,
        50,
        50,
        50,
        50,
        50,
        50,
        50,
        50
    ],
    [
        16,
        33,
        50,
        50,
        50,
        50,
        50,
        50,
        50,
        50
    ],
    [
        12,
        25,
        37,
        50,
        50,
        50,
        50,
        50,
        50,
        50
    ],
    [
        10,
        20,
        30,
        40,
        50,
        50,
        50,
        50,
        50,
        50
    ],
    [
        8,
        16,
        25,
        33,
        41,
        50,
        50,
        50,
        50,
        50
    ],
    [
        7,
        14,
        21,
        28,
        35,
        42,
        50,
        50,
        50,
        50
    ],
    [
        6,
        12,
        18,
        25,
        31,
        37,
        43,
        50,
        50,
        50
    ],
    [
        5,
        11,
        16,
        22,
        27,
        33,
        38,
        44,
        50,
        50
    ],
    [
        5,
        10,
        15,
        20,
        25,
        30,
        35,
        40,
        45,
        50
    ]
]

export const RecommendationBonus = {

    getBonus( player: L2PcInstance ) : number {
        if ( player && player.isOnline() && player.getRecommendationsHave() !== 0 && player.getRecommendationsBonusTime() > 0 ) {
            let level = player.getLevel() / 10
            let exp = ( Math.min( 100, player.getRecommendationsHave() ) - 1 ) / 10

            return bonus[ Math.floor( level ) ][ Math.floor( exp ) ]
        }

        return 0
    },

    getMultiplier( activeCharacter: L2PcInstance ) {
        let multiplier = 1.0
        let bonus = RecommendationBonus.getBonus( activeCharacter )
        if ( bonus > 0 ) {
            multiplier += ( bonus / 100 )
        }

        return multiplier
    }
}