import Database from 'better-sqlite3'
import pkgDir from 'pkg-dir'
import { ConfigManager } from '../../config/ConfigManager'
import { ServerLog } from '../../logger/Logger'
import chalk from 'chalk'

const rootDirectory = pkgDir.sync( __dirname )
const database = new Database( `${rootDirectory}/${ ConfigManager.data.getDataOptions() }`, {
    readonly: true,
    fileMustExist: true
} )

export const SQLiteDataEngine = {
    getMany( query: string ): Array<unknown> {
        try {
            return database.prepare( query ).all()
        } catch ( error ) {
            ServerLog.error( `Query ${chalk.red( query )} has error ${chalk.red( error.message )}` )
        }

        return null
    },

    getManyWithParameters( query: string, ...parameters : Array<unknown> ): Array<unknown> {
        try {
            return database.prepare( query ).all( ...parameters )
        } catch ( error ) {
            ServerLog.error( `Query ${chalk.red( query )} has error ${chalk.red( error.message )}` )
        }

        return null
    },

    shutdown() : void {
        if ( database.inTransaction ) {
            throw new Error( 'Detected ongoing transaction. Unable to shutdown data engine.' )
        }

        database.close()
    }
}