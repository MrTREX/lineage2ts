import Database from 'better-sqlite3'
import pkgDir from 'pkg-dir'
import { ConfigManager } from '../../config/ConfigManager'
import { ServerLog } from '../../logger/Logger'
import chalk from 'chalk'

const rootDirectory = pkgDir.sync( __dirname )
let database

export const SQLiteGeoDataEngine = {
    getOne( query: string, ...parameters: Array<string | number> ): unknown {
        try {
            return database.prepare( query ).get( parameters )
        } catch ( error ) {
            ServerLog.error( `Query ${chalk.red( query )} has error ${chalk.red( error.message )}` )
        }

        return null
    },

    shutdown() {
        if ( !database ) {
            return
        }

        database.close()
        database = null
    },

    start() : void {
        if ( database ) {
            return
        }

        database = new Database( `${rootDirectory}/${ ConfigManager.data.getGeoOptions() }`, {
            readonly: true,
            fileMustExist: true
        } )
    },
}