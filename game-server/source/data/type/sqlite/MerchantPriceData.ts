import { L2MerchantPriceDataApi } from '../../interface/MerchantPriceDataApi'
import { MerchantPriceConfig } from '../../../gameService/models/MerchantPriceConfig'
import { SQLiteDataEngine } from '../../engines/sqlite'
import logSymbols from 'log-symbols'
import _ from 'lodash'

const query = 'select * from merchant_price_data'

let byTownId : Record<number, MerchantPriceConfig>
let byCastleId : Record<number, MerchantPriceConfig>
let defaultConfig : MerchantPriceConfig

export const SQLiteMerchantPriceData : L2MerchantPriceDataApi = {
    getConfigByCastleId( id: number ): MerchantPriceConfig {
        return byCastleId[ id ]
    },

    getConfigByTownId( townId: number ): MerchantPriceConfig {
        return byTownId[ townId ]
    },

    getAllConfigs(): Array<MerchantPriceConfig> {
        return _.values( byTownId )
    },

    getDefaultConfig() : MerchantPriceConfig {
        return defaultConfig
    },

    async load(): Promise<Array<string>> {
        byTownId = {}
        byCastleId = {}

        let databaseItems: Array<any> = SQLiteDataEngine.getMany( query )

        _.each( databaseItems, ( databaseItem: any ) : void => {
            let config = new MerchantPriceConfig()

            config.name = databaseItem[ 'name' ]
            config.baseTax = databaseItem[ 'baseTax' ]
            config.castleId = databaseItem[ 'castleId' ]
            config.townId = databaseItem[ 'townId' ]

            if ( config.castleId === 0 && config.townId === 0 ) {
                defaultConfig = config
            }

            byTownId[ config.townId ] = config

            if ( config.townId === 0 && config.castleId > 0 ) {
                byCastleId[ config.castleId ] = config
            }
        } )

        return [
           `MerchantPriceData : loaded ${_.size( byTownId )} town references.`,
           `MerchantPriceData : loaded ${_.size( byCastleId )} castle references.`,
        ]
    }
}