import { L2EnchantItemGroupsDataApi } from '../../interface/EnchantItemGroupsDataApi'
import { EnchantItemGroup } from '../../../gameService/models/items/enchant/EnchantItemGroup'
import { L2Item } from '../../../gameService/models/items/L2Item'
import { EnchantScrollGroup } from '../../../gameService/models/items/enchant/EnchantScrollGroup'
import { EnchantRateItem } from '../../../gameService/models/items/enchant/EnchantRateItem'
import { SQLiteDataEngine } from '../../engines/sqlite'
import logSymbols from 'log-symbols'
import _ from 'lodash'
import { ItemTableSlots } from '../../../gameService/enums/L2ItemSlots'

const rateQuery = 'select * from enchant_rate_groups'
const scrollQuery = 'select * from enchant_item_scroll_groups'

let rateGroups : { [ key: string] : EnchantItemGroup }
let scrollGroups : { [ key: number ]: EnchantScrollGroup }

export const SQLiteEnchantItemGroupsData: L2EnchantItemGroupsDataApi = {
    getItemGroup( name: string ): EnchantItemGroup {
        return rateGroups[ name ]
    },

    getItemGroupByItem( item: L2Item, scrollGroup: number ) : EnchantItemGroup {
        let group : EnchantScrollGroup = scrollGroups[ scrollGroup ]
        let rateGroup : EnchantRateItem = group.getRateGroup( item )
        return rateGroup ? rateGroups[ rateGroup.name ] : null
    },

    getScrollGroup( id: number ): EnchantScrollGroup {
        return scrollGroups[ id ]
    },

    async load(): Promise<Array<string>> {
        rateGroups = {}
        scrollGroups = {}

        let rateItems: Array<any> = SQLiteDataEngine.getMany( rateQuery )
        _.each( rateItems, Helper.createRateGroup )

        let scrollItems: Array<any> = SQLiteDataEngine.getMany( scrollQuery )
        Helper.createScrollGroup( scrollItems )

        return [
           `EnchantItemGroups loaded ${_.size( rateGroups )} rate groups.`,
           `EnchantItemGroups loaded ${_.size( scrollGroups )} scroll groups.`,
        ]
    }
}

const Helper = {
    createRateGroup( databaseItem: any ) {
        let { groupName, enchantLevel, chance } = databaseItem

        if ( !rateGroups[ groupName ] ) {
            rateGroups[ groupName ] = new EnchantItemGroup()
            rateGroups[ groupName ].name = groupName
        }

        let min :number
        let max : number

        if ( enchantLevel.includes( '-' ) ) {
            [ min, max ] = _.map( _.split( enchantLevel, '-' ), value => _.parseInt( value ) )
        } else {
            min = _.parseInt( enchantLevel )
            max = min
        }

        rateGroups[ groupName ].chances.push( {
            min,
            max,
            chance
        } )
    },

    createScrollGroup( databaseItems: Array<any> ) {
        let defaultGroup = new EnchantScrollGroup()

       defaultGroup.rates = _.map( databaseItems, ( databaseItem: any ) => {
            let { rateGroup, slot, isMagicWeapon } = databaseItem

            let item = new EnchantRateItem()
            item.name = rateGroup
            item.slot |= _.defaultTo( ItemTableSlots[ slot as string ], 0 )
            item.isMagicWeapon = isMagicWeapon === 'true'

            return item
        } )

        scrollGroups[ 0 ] = defaultGroup
    }
}