import { UICategories, UIDataApi, UIKeys } from '../../interface/UIDataApi'
import { ActionKey } from '../../../gameService/models/ActionKey'
import { SQLiteDataEngine } from '../../engines/sqlite'
import logSymbols from 'log-symbols'
import _ from 'lodash'

const query = 'select * from ui_data'

let storedKeys: UIKeys
let storedCategories: UICategories

export const SQLiteUIData: UIDataApi = {
    getCategories(): { [ key: number ]: Array<number> } {
        return storedCategories
    },

    getKeys(): { [ key: number ]: Array<ActionKey> } {
        return storedKeys
    },

    async load() {
        storedKeys = {}
        storedCategories = {}

        let databaseItems: Array<any> = SQLiteDataEngine.getMany( query )

        _.each( databaseItems, Helper.createCategory )
        return [
            `UIData loaded ${ _.size( storedKeys ) } keys and ${ _.size( storedCategories ) } categories.`,
        ]
    },
}

const Helper = {
    createCategory( databaseItem: any ) {
        storedCategories[ databaseItem.categoryId ] = _.split( databaseItem.commands, ',' ).map( value => _.parseInt( value ) )

        if ( databaseItem[ 'keys_json' ] ) {
            let keys = JSON.parse( databaseItem[ 'keys_json' ] )

            storedKeys[ databaseItem.categoryId ] = _.castArray( keys ).map( ( key: any ): ActionKey => {
                let commandId = _.get( key, 'cmd', 0 )
                let keyValue = _.get( key, 'key', 0 )
                let toggleKeyOne = _.get( key, 'toggleKey1', 0 )
                let toggleKeyTwo = _.get( key, 'toggleKey2', 0 )
                let visibleStatus = _.get( key, 'showType', 1 )

                return {
                    categoryId: databaseItem.categoryId,
                    commandId,
                    key: keyValue,
                    toggleKeyOne,
                    toggleKeyTwo,
                    visibleStatus,
                }
            } )
        }
    },
}