import { L2XPLostDataApi } from '../../interface/XPLostDataApi'
import { SQLiteDataEngine } from '../../engines/sqlite'
import { ConfigManager } from '../../../config/ConfigManager'
import logSymbols from 'log-symbols'
import _ from 'lodash'

const query = 'select * from xp_lost'

let levels : { [ key: number ]: number }

export const SQLiteXPLostData : L2XPLostDataApi = {
    getXpPercent( level: number ): number {
        return levels[ _.clamp( level, 0, _.size( levels ) - 1 ) ]
    },

    async load(): Promise<Array<string>> {
        levels = {}
        let missingLevels = 0

        let databaseItems: Array<any> = SQLiteDataEngine.getMany( query )

        _.each( databaseItems, ( databaseItem: any ) => {
            let { level, value } = databaseItem
            levels[ level ] = value
        } )

        let lastLevel = _.defaultTo( _.max( _.map( databaseItems, 'level' ) ), 0 )

        if ( lastLevel < ConfigManager.character.getMaxPlayerLevel() ) {
            missingLevels = ConfigManager.character.getMaxPlayerLevel() - lastLevel

            _.times( missingLevels, ( index: number ) => {
                levels[ index + lastLevel + 1 ] = 1
            } )
        }

        return [
           `XPLostData loaded ${_.size( levels )} levels with ${missingLevels} missing levels.`
        ]
    }
}