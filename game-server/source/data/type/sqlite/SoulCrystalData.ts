import { L2SoulCrystalAbsorbType, L2SoulCrystalDataApi, L2SoulCrystalDataItem, L2SoulCrystalDataNpc } from '../../interface/SouldCrystalDataApi'
import { SQLiteDataEngine } from '../../engines/sqlite'
import _ from 'lodash'
import logSymbols from 'log-symbols'

const itemsQuery = 'select * from levelup_crystal_items'
const npcQuery = 'select * from levelup_crystal_npcs'

let items: { [ itemId: number ]: L2SoulCrystalDataItem }
let npcs: { [ npcId: number ]: Array<L2SoulCrystalDataNpc> }

export const SQLiteSoulCrystalData: L2SoulCrystalDataApi = {
    getItems(): { [ itemId: number ]: L2SoulCrystalDataItem } {
        return items
    },

    getNpcs(): { [ npcId: number ]: Array<L2SoulCrystalDataNpc> } {
        return npcs
    },

    async load(): Promise<Array<string>> {
        items = {}
        npcs = {}

        let itemRecords: Array<any> = SQLiteDataEngine.getMany( itemsQuery )
        _.each( itemRecords, Helper.createItem )

        let npcRecords: Array<any> = SQLiteDataEngine.getMany( npcQuery )
        _.each( npcRecords, Helper.createNpcDefinition )

        return [
            `SoulCrystalData : loaded ${ _.size( items ) } crystal items`,
            `SoulCrystalData : loaded ${ _.size( npcs ) } npc definitions from ${ _.size( npcRecords ) } records`,
        ]
    },
}

const Helper = {
    createItem( databaseItem: any ): void {
        let item: L2SoulCrystalDataItem = _.pick( databaseItem, [ 'itemId', 'nextCrystalItemId', 'level' ] )
        items[ item.itemId ] = item
    },

    createNpcDefinition( databaseItem: any ): void {
        if ( !npcs[ databaseItem.npcId ] ) {
            npcs[ databaseItem.npcId ] = []
        }

        let parsedValues: Array<any> = JSON.parse( databaseItem[ 'chances_json' ] )

        _.each( parsedValues, ( value: any ) => {
            let definition = _.pick( value, [ 'chance', 'levels', 'requiresSkill' ] ) as L2SoulCrystalDataNpc

            definition.npcId = databaseItem.npcId
            definition.type = L2SoulCrystalAbsorbType[ value.type as string ]

            npcs[ databaseItem.npcId ].push( definition )
        } )
    },
}