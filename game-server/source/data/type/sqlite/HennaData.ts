import { L2HennaDataApi } from '../../interface/HennaDataApi'
import { L2Henna } from '../../../gameService/models/items/L2Henna'
import { SQLiteDataEngine } from '../../engines/sqlite'
import logSymbols from 'log-symbols'
import _ from 'lodash'

const query = 'select * from henna_data'

let allHennas : { [ key: number ]: L2Henna }
let hennaByClassId : Record<number, Array<L2Henna>>

export const SQLiteHennaData : L2HennaDataApi = {
    get( symbolId: number ): L2Henna {
        return allHennas[ symbolId ]
    },

    getAll( classId: number ): Array<L2Henna> {
        return hennaByClassId[ classId ] ?? []
    },

    async load(): Promise<Array<string>> {
        allHennas = {}
        hennaByClassId = {}

        let databaseItems: Array<any> = SQLiteDataEngine.getMany( query )

        _.each( databaseItems, Helper.createHenna )

        return [
           `HennaData loaded ${_.size( allHennas )} items.`
        ]
    }
}

const Helper = {
    createHenna( databaseItem: any ) {
        let henna = new L2Henna()

        henna.dyeId = databaseItem[ 'id' ]
        henna.dyeName = databaseItem[ 'name' ]
        henna.dyeItemId = databaseItem[ 'itemId' ]

        if ( databaseItem[ 'stats_json' ] ) {
            let stats = JSON.parse( databaseItem[ 'stats_json' ] )

            _.assign( henna, stats )
        }

        if ( databaseItem[ 'wear_json' ] ) {
            let wearData = JSON.parse( databaseItem[ 'wear_json' ] )

            henna.wearFee = wearData.fee
            henna.wearCount = wearData.count
        }

        if ( databaseItem[ 'cancel_json' ] ) {
            let cancelData = JSON.parse( databaseItem[ 'cancel_json' ] )

            henna.cancelFee = cancelData.fee
            henna.cancelCount = cancelData.count
        }

        if ( databaseItem[ 'classId_json' ] ) {
            henna.wearClassIds = new Set<number>( JSON.parse( databaseItem[ 'classId_json' ] ) )
        }

        henna.wearClassIds.forEach( ( hennaId: number ) => {
            let classHennas = hennaByClassId[ hennaId ]
            if ( !classHennas ) {
                classHennas = []
                hennaByClassId[ hennaId ] = classHennas
            }

            classHennas.push( henna )
        } )

        allHennas[ henna.getDyeId() ] = henna
    }
}