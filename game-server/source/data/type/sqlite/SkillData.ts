import { SkillDataApi } from '../../interface/SkillDataApi'
import { Skill } from '../../../gameService/models/Skill'
import { SQLiteDataEngine } from '../../engines/sqlite'
import { L2EnchantSkillLearn } from '../../../gameService/models/L2EnchantSkillLearn'
import { EnchantSkillHolder, L2EnchantSkillGroup } from '../../../gameService/models/L2EnchantSkillGroup'
import { SkillCreator } from './helpers/SkillCreator'
import _ from 'lodash'
import logSymbols from 'log-symbols'
import { getFunctionTemplateAmount } from './helpers/FunctionHelper'

let skillsAtMaxLevel: { [ key: number ]: number }
let allHashedSkills: { [ key: number ]: Skill }
let enchantableSkillIds: Set<number>

let skillGroups: { [ key: number ]: L2EnchantSkillGroup }
let skillTrees: { [ key: number ]: L2EnchantSkillLearn }

const querySkillData = 'select * from skill_data'
const querySkillGroups = 'select * from enchant_skill_groups'

export const SQLiteSkillData: SkillDataApi = {
    getIdsByPartialName( name: string ): Array<number> {
        const nameQuery = 'select id from skill_data where name LIKE ? order by id'
        const databaseItems : Array<unknown> = SQLiteDataEngine.getManyWithParameters( nameQuery, `%${name}%` )

        if ( !databaseItems ) {
            return []
        }

        return databaseItems.map( ( item: any ) => item.id )
    },

    getAllHashedSkills(): { [ hashCode: number ]: Skill } {
        return allHashedSkills
    },

    getEnchantableSkillIds(): Set<number> {
        return enchantableSkillIds
    },

    getSkillsAtMaxLevel(): { [ key: number ]: number } {
        return skillsAtMaxLevel
    },

    getHashCode( id: number, level: number ): number {
        return ( id * 1021 ) + level
    },

    async load() {
        allHashedSkills = {}
        skillsAtMaxLevel = {}
        enchantableSkillIds = new Set<number>()
        skillGroups = {}
        skillTrees = {}

        let skillGroupItems: Array<any> = SQLiteDataEngine.getMany( querySkillGroups )

        _.each( skillGroupItems, ( databaseItem: any ) => {
            let { groupId, level, adena, exp, sp } = databaseItem
            let rates = JSON.parse( databaseItem[ 'rates_json' ] )
            let holder = new EnchantSkillHolder()

            _.assign( holder, { level, adena, exp, sp, rates } )

            if ( !skillGroups[ groupId ] ) {
                skillGroups[ groupId ] = new L2EnchantSkillGroup( groupId )
            }

            skillGroups[ groupId ].skills.push( holder )
        } )

        let skillDataItems: Array<any> = SQLiteDataEngine.getMany( querySkillData )

        _.each( skillDataItems, ( databaseItem: any ) => {
            SkillHelper.createSkill( databaseItem )
        } )

        return [
            `EnchantSkillGroups loaded ${ _.size( skillGroups ) } groups using ${ _.size( skillGroupItems ) } records.`,
            `EnchantSkillGroups updated ${ _.size( skillTrees ) } skill trees.`,
            `SkillData loaded ${ _.size( skillsAtMaxLevel ) } individual skills.`,
            `SkillData loaded ${ _.size( enchantableSkillIds ) } enchantable skills.`,
            `SkillData loaded ${ _.size( allHashedSkills ) } available skills and ${ getFunctionTemplateAmount() } existing function templates`,
        ]
    },

    getSkillEnchantmentBySkillId( id: number ): L2EnchantSkillLearn {
        return skillTrees[ id ]
    },

    getEnchantSkillGroupById( id: number ): L2EnchantSkillGroup {
        return skillGroups[ id ]
    }
}

const SkillHelper = {
    createSkill( databaseItem: any ): void {
        let enchantLevels = SkillHelper.getEnchantLevels( databaseItem )
        let skillLevel = databaseItem[ 'lastLevel' ]
        let skillId = databaseItem[ 'id' ]

        let skillFactory = new SkillCreator( databaseItem )

        _.times( skillLevel, ( index: number ) => {
            let skill = skillFactory.createSingleSkill( index )
            allHashedSkills[ SQLiteSkillData.getHashCode( skill.id, skill.level ) ] = skill
        } )

        skillsAtMaxLevel[ skillId ] = skillLevel

        enchantLevels.forEach( ( value: number, index: number ) => {
            _.times( value, ( valueCount: number ) => {
                let level = ( index + 1 ) * 100 + valueCount
                let skill = skillFactory.createSingleSkill( level )

                allHashedSkills[ SQLiteSkillData.getHashCode( skill.id, skill.level ) ] = skill

                if ( level > 99 ) {
                    enchantableSkillIds.add( skill.id )
                }
            } )
        } )
    },

    getEnchantLevels( databaseItem: any ): Array<number> {
        let skillId = databaseItem[ 'id' ]
        let skillLevel = databaseItem[ 'lastLevel' ]

        return [ 1,2,3,4,5,6,7,8 ].reduce( ( levels: Array<number>, index: number ) => {
            let groupValue: number = databaseItem[ `enchantGroup${ index }` ]
            let outcome = _.isUndefined( groupValue ) ? 0 : SkillHelper.addRouteForSkill( skillId, skillLevel, index, groupValue )

            levels.push( outcome )
            return levels
        }, [] )
    },

    addRouteForSkill( id: number, level: number, route: number, group: number ): number {
        let skill = skillTrees[ id ]
        if ( !skill ) {
            skillTrees[ id ] = new L2EnchantSkillLearn( id, level )
        }

        if ( skillGroups[ group ] ) {
            skillTrees[ id ].addEnchantRoute( route, group )
            return skillGroups[ group ].getEnchantGroupDetails().length
        }

        return 0
    },
}