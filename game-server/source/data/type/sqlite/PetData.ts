import { PetDataApi, PetDataMap } from '../../interface/PetDataApi'
import { L2PetData } from '../../../gameService/models/L2PetData'
import { L2PetLevelData } from '../../../gameService/models/L2PetLevelData'
import { SQLiteDataEngine } from '../../engines/sqlite'
import _ from 'lodash'
import logSymbols from 'log-symbols'
import { PetSkillItem } from '../../../gameService/models/skills/SkillItem'

let npcIdData: PetDataMap
let itemIdData: PetDataMap

const query = 'select * from pet_data'

export const SQLitePetData: PetDataApi = {
    getPetItems(): PetDataMap {
        return itemIdData
    },

    getPetDataByNpcId( id: number ): L2PetData {
        return npcIdData[ id ]
    },

    getPetDataByItemId( itemId: number ): L2PetData {
        return itemIdData[ itemId ]
    },

    getPetLevelData( npcId: number, level: number ): L2PetLevelData {
        let data = npcIdData[ npcId ]
        if ( data ) {
            return data.getPetLevelData( level )
        }
    },

    getPetMinLevel( npcId: number ): number {
        let data = npcIdData[ npcId ]
        if ( data ) {
            return data.minimumLevel
        }

        return 0
    },

    async load() {
        npcIdData = {}
        itemIdData = {}

        let databaseItems: Array<any> = SQLiteDataEngine.getMany( query )

        _.each( databaseItems, PetHelper.createPet )

        return [
            `PetData loaded ${ _.size( npcIdData ) } pets from ${ _.size( databaseItems ) } records.`,
        ]
    },
}

const PetHelper = {
    createPet( databaseItem: any ): void {
        let { id, itemId, minimumLevel } = databaseItem
        let pet = new L2PetData( id, itemId )

        PetHelper.addSetAttributes( pet, databaseItem )
        PetHelper.addSkills( pet, databaseItem )
        PetHelper.addLevels( pet, databaseItem )
        pet.minimumLevel = minimumLevel

        npcIdData[ id ] = pet

        /**
         * Certain pets such as Wyvern do not have control itemId (itemId = 0), hence
         * no need to cache such data.
         */
        if ( itemId ) {
            itemIdData[ itemId ] = pet
        }
    },

    addSetAttributes( pet: L2PetData, databaseItem: any ) {
        if ( !databaseItem[ 'set_json' ] ) {
            return
        }

        let setAttributes = JSON.parse( databaseItem[ 'set_json' ] )

        pet.food = _.has( setAttributes, 'food' ) ? setAttributes.food : []

        if ( _.has( setAttributes, 'hungry_limit' ) ) {
            pet.hungryLimit = setAttributes[ 'hungry_limit' ]
        }

        /*
            L2J has 'sync_level' property handling, however data does not have such property
         */
    },

    addSkills( pet: L2PetData, databaseItem: any ) {
        if ( !databaseItem[ 'skills_json' ] ) {
            return
        }

        let skills = JSON.parse( databaseItem[ 'skills_json' ] )

        pet.skills = _.castArray( skills ).map( ( skillData: any ) : PetSkillItem => {
            return {
                id: skillData.skillId,
                level: skillData.skillLvl,
                minLevel: skillData.minLvl
            }
        } )
    },

    addLevels( pet: L2PetData, databaseItem: any ) {
        if ( !databaseItem[ 'levels_json' ] ) {
            return
        }

        let levels = JSON.parse( databaseItem[ 'levels_json' ] )
        let highestLevel = 0

        _.castArray( levels ).forEach( ( levelData: any ) => {
            let { level } = levelData
            pet.levelStats[ level ] = PetHelper.createPetLevelData( levelData )

            if ( highestLevel < level ) {
                highestLevel = level
            }
        } )

        pet.highestLevel = highestLevel
    },

    createPetLevelData( levelData: any ): L2PetLevelData {
        let pet = new L2PetLevelData()

        pet.ownerExpRatio = Math.min( 1, _.get( levelData, 'get_exp_type', 100 ) / 100 )
        pet.petMaxExp = _.get( levelData, 'exp', 0 )
        pet.maxHP = _.get( levelData, 'org_hp', 1 )
        pet.maxMP = _.get( levelData, 'org_mp', 1 )

        pet.powerAttack = _.get( levelData, 'org_pattack', 1 )
        pet.powerDefence = _.get( levelData, 'org_pdefend', 1 )
        pet.magicAttack = _.get( levelData, 'org_mattack', 1 )
        pet.magicDefence = _.get( levelData, 'org_mdefend', 1 )

        pet.maxFeed = _.get( levelData, 'max_meal', 1 )
        pet.feedBattle = _.get( levelData, 'consume_meal_in_battle', 1 )
        pet.feedNormal = _.get( levelData, 'consume_meal_in_normal', 1 )
        pet.regenerateHP = _.get( levelData, 'org_hp_regen', 1 )

        pet.regenerateMP = _.get( levelData, 'org_mp_regen', 1 )
        pet.useSoulshot = _.get( levelData, 'soulshot_count', 1 )
        pet.useSpiritshot = _.get( levelData, 'spiritshot_count', 1 )

        let speedData = _.get( levelData, 'speed_on_ride', {} )
        pet.walkSpeedOnRide = _.get( speedData, 'walk', 0 )
        pet.runSpeedOnRide = _.get( speedData, 'run', 0 )
        pet.slowSwimSpeedOnRide = _.get( speedData, 'slowSwim', 0 )
        pet.fastSwimSpeedOnRide = _.get( speedData, 'fastSwim', 0 )
        pet.slowFlySpeedOnRide = _.get( speedData, 'slowFly', 0 )
        pet.fastFlySpeedOnRide = _.get( speedData, 'fastFly', 0 )

        return pet
    },
}