import { PlayerTemplateData } from '../../interface/PlayerTemplateData'
import { ClassId, IClassId } from '../../../gameService/models/base/ClassId'
import { L2PcTemplate } from '../../../gameService/models/actor/templates/L2PcTemplate'
import { SQLiteDataEngine } from '../../engines/sqlite'
import { WeaponType } from '../../../gameService/models/items/type/WeaponType'
import { MoveType } from '../../../gameService/models/stats/MoveType'
import { InventorySlot } from '../../../gameService/values/InventoryValues'
import _ from 'lodash'

let playerTemplates: { [key: number]: L2PcTemplate }

const query = 'select * from player_templates'

export const SQLitePlayerTemplateData: PlayerTemplateData = {
    async load() {
        playerTemplates = {}

        let databaseItems: Array<any> = SQLiteDataEngine.getMany( query )

        _.each( databaseItems, TemplateHelper.createPlayerTemplate )
        return [
            `PlayerTemplates loaded ${ _.size( playerTemplates ) } classes from ${ _.size( databaseItems ) } records.`
        ]
    },

    getTemplateByClassId: ( classId: IClassId ): L2PcTemplate => {
        return playerTemplates[ classId.id ]
    },

    getTemplateById: ( classId: number ): L2PcTemplate => {
        return playerTemplates[ classId ]
    }
}

const TemplateHelper = {
    createPlayerTemplate( databaseItem: any ): void {
        let { classId } = databaseItem
        let template = new L2PcTemplate()

        TemplateHelper.setL2CharacterTemplateFields( template, databaseItem )
        TemplateHelper.setL2PcTemplateFields( template, databaseItem )
        TemplateHelper.addLevelData( template, databaseItem )

        playerTemplates[ classId ] = template
    },

    setL2CharacterTemplateFields( template: L2PcTemplate, databaseItem: any ): void {

        template.baseSTR = _.get( databaseItem, 'str', 0 )
        template.baseCON = _.get( databaseItem, 'con', 0 )
        template.baseDEX = _.get( databaseItem, 'dex', 0 )
        template.baseINT = _.get( databaseItem, 'int', 0 )
        template.baseWIT = _.get( databaseItem, 'wit', 0 )
        template.baseMEN = _.get( databaseItem, 'men', 0 )

        template.baseHpMax = 0
        template.baseCpMax = 0
        template.baseMpMax = 0
        template.baseHpReg = 0
        template.baseMpReg = 0

        template.basePAtk = _.get( databaseItem, 'powerAttack', 0 )
        template.baseMAtk = _.get( databaseItem, 'magicAttack', 0 )
        template.basePDef = TemplateHelper.getPowerDefence( databaseItem )
        template.baseMDef = TemplateHelper.getMagicDefence( databaseItem )

        template.basePAtkSpd = _.get( databaseItem, 'attackSpeed', 300 )
        template.baseMAtkSpd = 333
        template.baseShldDef = 0
        template.baseAttackRange = _.get( databaseItem, 'attackRange', 40 )
        template.randomDamage = _.get( databaseItem, 'randomDamage', 0 )

        template.baseAttackType = _.defaultTo( WeaponType[ _.toUpper( _.get( databaseItem, 'attackType' ) ) ], WeaponType.FIST )
        template.baseShldRate = 0
        template.baseCritRate = _.get( databaseItem, 'critRate', 4 )
        template.baseMCritRate = 0

        template.baseBreath = _.get( databaseItem, 'breath', 100 )
        template.baseFire = 0
        template.baseWind = 0
        template.baseWater = 0
        template.baseEarth = 0
        template.baseHoly = 0
        template.baseDark = 0

        template.baseFireRes = 0
        template.baseWindRes = 0
        template.baseWaterRes = 0
        template.baseEarthRes = 0
        template.baseHolyRes = 0
        template.baseDarkRes = 0
        template.baseElementRes = 0

        template.moveType[ MoveType.RUN ] = _.get( databaseItem, 'moveSpeedRun', 120 )
        template.moveType[ MoveType.WALK ] = _.get( databaseItem, 'moveSpeedWalk', 50 )
        template.moveType[ MoveType.FAST_SWIM ] = _.get( databaseItem, 'moveSpeedFastswim', template.getBaseMoveSpeed( MoveType.RUN ) )
        template.moveType[ MoveType.SLOW_SWIM ] = _.get( databaseItem, 'moveSpeedSlowswim', template.getBaseMoveSpeed( MoveType.WALK ) )
        template.moveType[ MoveType.FAST_FLY ] = template.getBaseMoveSpeed( MoveType.RUN )
        template.moveType[ MoveType.SLOW_FLY ] = template.getBaseMoveSpeed( MoveType.WALK )

        template.fCollisionRadius = _.get( databaseItem, 'collisionMaleRadius', 0 )
        template.fCollisionHeight = _.get( databaseItem, 'collisionMaleHeight', 0 )
        template.collisionRadius = Math.floor( template.fCollisionRadius )
        template.collisionHeight = Math.floor( template.fCollisionHeight )

        template.fCollisionRadiusFemale = _.get( databaseItem, 'collisionFemaleRadius', 0 )
        template.fCollisionHeightFemale = _.get( databaseItem, 'collisionFemaleHeight', 0 )
    },

    setL2PcTemplateFields( template: L2PcTemplate, databaseItem: any ): void {
        template.classId = _.get( databaseItem, 'classId', 0 )
        template.race = ClassId.getClassIdByIdentifier( template.classId ).race

        template.baseSlotDef[ InventorySlot.Chest ] = _.get( databaseItem, 'powerDefenceChest', 0 )
        template.baseSlotDef[ InventorySlot.Legs ] = _.get( databaseItem, 'powerDefenceLegs', 0 )
        template.baseSlotDef[ InventorySlot.Head ] = _.get( databaseItem, 'powerDefenceHead', 0 )
        template.baseSlotDef[ InventorySlot.Feet ] = _.get( databaseItem, 'powerDefenceFeet', 0 )
        template.baseSlotDef[ InventorySlot.Gloves ] = _.get( databaseItem, 'powerDefenceGloves', 0 )
        template.baseSlotDef[ InventorySlot.Under ] = _.get( databaseItem, 'powerDefenceUnderwear', 0 )
        template.baseSlotDef[ InventorySlot.Cloak ] = _.get( databaseItem, 'powerDefenceCloak', 0 )
        template.baseSlotDef[ InventorySlot.RightEar ] = _.get( databaseItem, 'magicDefenceRightEar', 0 )
        template.baseSlotDef[ InventorySlot.LeftEar ] = _.get( databaseItem, 'magicDefenceLeftEear', 0 )
        template.baseSlotDef[ InventorySlot.RightFinger ] = _.get( databaseItem, 'magicDefenceRightFinger', 0 )
        template.baseSlotDef[ InventorySlot.LeftFinger ] = _.get( databaseItem, 'magicDefenceLeftFinger', 0 )
        template.baseSlotDef[ InventorySlot.Neck ] = _.get( databaseItem, 'magicDefenceNeck', 0 )

        template.baseSafeFallHeight = _.get( databaseItem, 'safeFall', 333 )
    },

    addLevelData( template: L2PcTemplate, databaseItem: any ) {
        if ( !databaseItem[ 'levelData_json' ] ) {
            return
        }

        let levelData = JSON.parse( databaseItem[ 'levelData_json' ] )
        levelData = _.sortBy( levelData, [ 'level' ] )

        template.baseHp = _.map( levelData, 'hp' )
        template.baseMp = _.map( levelData, 'mp' )
        template.baseCp = _.map( levelData, 'cp' )

        template.baseHpRegeneration = _.map( levelData, 'hpRegeneration' )
        template.baseMpRegeneration = _.map( levelData, 'mpRegeneration' )
        template.baseCpRegeneration = _.map( levelData, 'cpRegeneration' )
    },


    getPowerDefence( databaseItem: any ): number {
        let properties = [
            'powerDefenceChest',
            'powerDefenceLegs',
            'powerDefenceHead',
            'powerDefenceFeet',
            'powerDefenceGloves',
            'powerDefenceUnderwear',
            'powerDefenceCloak',
        ]

        return _.reduce( properties, ( total: number, property: string ) => {
            return total + _.get( databaseItem, property, 0 )
        }, 0 )
    },

    getMagicDefence( databaseItem: any ): number {
        let properties = [
            'magicDefenceRightEar',
            'magicDefenceLeftEar',
            'magicDefenceRightFinger',
            'magicDefenceLeftFinger',
            'magicDefenceNeck',
        ]

        return _.reduce( properties, ( total: number, property: string ) => {
            return total + _.get( databaseItem, property, 0 )
        }, 0 )
    },
}