import { OptionDataApi } from '../../interface/OptionDataApi'
import { Options } from '../../../gameService/models/options/Options'
import { SQLiteDataEngine } from '../../engines/sqlite'
import { FunctionTemplate } from '../../../gameService/models/stats/functions/FunctionTemplate'
import { getExistingSkill } from '../../../gameService/models/holders/SkillHolder'
import { OptionsSkillHolder } from '../../../gameService/models/options/OptionsSkillHolder'
import { OptionsSkillType } from '../../../gameService/enums/OptionsSkillType'
import logSymbols from 'log-symbols'
import _ from 'lodash'
import { createOrGetFunctionTemplate, getFunctionTemplateAmount } from './helpers/FunctionHelper'
import { StatFunctionName } from '../../../gameService/enums/StatFunction'

let allOptions: { [key: number]: Options }

const query = 'select * from option_data'

export const SQLiteOptionsData: OptionDataApi = {
    getOptions( id: number ): Options {
        return allOptions[ id ]
    },

    async load() {
        allOptions = {}

        let databaseItems: Array<any> = SQLiteDataEngine.getMany( query )

        _.each( databaseItems, OptionsHelper.createOptions )

        return [
           `OptionsData : loaded ${_.size( allOptions )} item options and ${ getFunctionTemplateAmount() } existing function templates`
        ]
    }
}

const OptionsHelper = {
    createOptions( databaseItem: any ): void {
        let { id } = databaseItem

        let options = new Options( id )

        _.each( [
            OptionsHelper.addFunctions,
            OptionsHelper.addActiveSkill,
            OptionsHelper.addPassiveSkill,
            OptionsHelper.addAttackSkill,
            OptionsHelper.addMagicSkill,
            OptionsHelper.addCriticalSkill
        ], ( method: Function ) => {
            method( options, databaseItem )
        } )

        allOptions[ id ] = options
    },

    addFunctions( options: Options, databaseItem: any ): void {
        if ( !databaseItem[ 'for_json' ] ) {
            return
        }

        let modifiers = JSON.parse( databaseItem[ 'for_json' ] )

        _.each( modifiers, ( stats: any, functionName: StatFunctionName ) => {
            _.each( _.castArray( stats ), ( statItem: any ) => {
                let { stat, val } = statItem
                options.functions.push( createOrGetFunctionTemplate( functionName, stat, val ).generateFunction( options ) )
            } )
        } )
    },

    addActiveSkill( options: Options, databaseItem: any ): void {
        if ( databaseItem[ 'active_skill_json' ] ) {
            let { id, level } = JSON.parse( databaseItem[ 'active_skill_json' ] )

            options.activeSkill = getExistingSkill( id, level )
        }
    },

    addPassiveSkill( options: Options, databaseItem: any ): void {
        if ( databaseItem[ 'passive_skill_json' ] ) {
            let { id, level } = JSON.parse( databaseItem[ 'passive_skill_json' ] )

            options.passiveSkill = getExistingSkill( id, level )
        }
    },

    addAttackSkill( options: Options, databaseItem: any ): void {
        if ( databaseItem[ 'attack_skill_json' ] ) {
            let { id, level, chance } = JSON.parse( databaseItem[ 'attack_skill_json' ] )

            if ( options.activationSkill ) {
                throw new Error( `Activation skill already assigned to option id=${options.id}` )
            }

            options.activationSkill = new OptionsSkillHolder( id, level, chance, OptionsSkillType.Attack )
        }
    },

    addMagicSkill( options: Options, databaseItem: any ): void {
        if ( databaseItem[ 'magic_skill_json' ] ) {
            let { id, level, chance } = JSON.parse( databaseItem[ 'magic_skill_json' ] )

            if ( options.activationSkill ) {
                throw new Error( `Activation skill already assigned to option id=${options.id}` )
            }

            options.activationSkill = new OptionsSkillHolder( id, level, chance, OptionsSkillType.Magic )
        }
    },

    addCriticalSkill( options: Options, databaseItem: any ): void {
        if ( databaseItem[ 'critical_skill_json' ] ) {
            let { id, level, chance } = JSON.parse( databaseItem[ 'critical_skill_json' ] )

            if ( options.activationSkill ) {
                throw new Error( `Activation skill already assigned to option id=${options.id}` )
            }

            options.activationSkill = new OptionsSkillHolder( id, level, chance, OptionsSkillType.Critical )
        }
    },
}