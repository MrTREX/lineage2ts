import { L2RecipeDataInput, L2RecipeDataItem, L2RecipeDataOutput, RecipeDataApi } from '../../interface/RecipeDataApi'
import { SQLiteDataEngine } from '../../engines/sqlite'
import _ from 'lodash'

let allRecipes: { [key: number]: L2RecipeDataItem }
let recipesByItemId : { [key: number]: L2RecipeDataItem }

const query = 'select * from recipes'

export const SQLiteRecipes : RecipeDataApi = {
    getRecipeByItemId( itemId: number ): L2RecipeDataItem {
        return recipesByItemId[ itemId ]
    },

    getRecipeList( recipeId: number ): L2RecipeDataItem {
        return allRecipes[ recipeId ]
    },

    async load(): Promise<Array<string>> {
        allRecipes = {}
        recipesByItemId = {}

        SQLiteDataEngine.getMany( query ).forEach( createRecipe )

        return [
            `Recipes : created ${ _.size( allRecipes ) } recipes`
        ]
    }
}

function createInput( value: Object ) : L2RecipeDataInput {
    return {
        id: value[ 'id' ],
        amount: value[ 'amount' ]
    }
}

function createOutput( value: Object ) : L2RecipeDataOutput {
    return {
        id: value[ 'id' ],
        amount: value[ 'amount' ],
        chance: value[ 'chance' ]
    }
}

function createRecipe( databaseItem: any ) : void {
    let inputs = JSON.parse( databaseItem.inputs_json ) as Array<unknown>
    let outputs = JSON.parse( databaseItem.outputs_json ) as Array<unknown>
    let npcInputs = databaseItem.npc_json ? JSON.parse( databaseItem.npc_json ) as Array<unknown> : undefined

    let recipe : L2RecipeDataItem = {
        consumeMp: databaseItem.consumeMp,
        id: databaseItem.id,
        inputs: inputs.map( value => createInput( value ) ),
        isCommon: databaseItem.isCommon === 1,
        level: databaseItem.level,
        npcInputs: npcInputs?.map( value => createInput( value ) ),
        outputs: outputs.map( value => createOutput( value ) ),
        successRate: !databaseItem.successRate ? 1 : databaseItem.successRate
    }

    allRecipes[ recipe.id ] = recipe

    if ( databaseItem.itemId ) {
        recipesByItemId[ databaseItem.itemId ] = recipe
    }
}