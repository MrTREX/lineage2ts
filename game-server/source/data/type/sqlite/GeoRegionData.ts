import { L2GeoRegionDataApi, L2GeoRegionDataStats } from '../../interface/GeoRegionDataApi'
import { SQLiteGeoDataEngine } from '../../engines/sqlite-geo'

export const SQLiteGeoRegionData : L2GeoRegionDataApi = {
    deInitialize(): void {
        return SQLiteGeoDataEngine.shutdown()
    },

    initialize(): L2GeoRegionDataStats {
        SQLiteGeoDataEngine.start()

        const query = 'select count(*) as regionSize from geoindex'
        let databaseItem = SQLiteGeoDataEngine.getOne( query )
        if ( !databaseItem ) {
            return {
                regions: 0
            }
        }

        return {
            regions: databaseItem[ 'regionSize' ]
        }
    },

    getRegionData( regionX: number, regionY: number ): Buffer {
        const query = 'select data from geoindex where regionX = ? and regionY = ?'
        let databaseItem = SQLiteGeoDataEngine.getOne( query, regionX, regionY )

        if ( !databaseItem ) {
            return
        }

        return ( databaseItem as Object )[ 'data' ]
    },

    shutdown() {
        SQLiteGeoDataEngine.shutdown()
    }
}