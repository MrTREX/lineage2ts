import { ExperienceDataApi } from '../../interface/ExperienceDataApi'
import { SQLiteDataEngine } from '../../engines/sqlite'
import logSymbols from 'log-symbols'
import _ from 'lodash'

let data : { [ key: number ]: number } = {}
const query = 'select * from experience_data'

export const SQLiteExperienceData : ExperienceDataApi = {
    getExpForLevel: ( level: number ): number => {
        return data[ level ]
    },

    async load () {
        data = {}
        let databaseItems: Array<any> = SQLiteDataEngine.getMany( query )

        _.each( databaseItems, ( databaseItem : any ) => {
            let { level, exp } = databaseItem
            data[ level ] = exp
        } )

        return [
            `ExperienceData loaded ${_.size( data )} levels.`
        ]
    },

    getPercentFromCurrentLevel: ( currentExp: number, currentLevel: number ) : number => {
        let currentLevelXp = data[ currentLevel ]
        let nextLevelXp = data[ currentLevel + 1 ]

        if ( _.isUndefined( nextLevelXp ) ) {
            return 0
        }

        return ( currentExp - currentLevelXp ) / ( nextLevelXp - currentLevelXp )
    }
}