import { L2GeometryDataApi, L2GeometryDataItem } from '../../interface/GeometryDataApi'
import logSymbols from 'log-symbols'
import { SQLiteDataEngine } from '../../engines/sqlite'
import { DeserializationFormat, RoaringBitmap32 } from 'roaring'
import _ from 'lodash'
import { GeometryId } from '../../../gameService/enums/GeometryId'

let allGeometryItems: { [ id in GeometryId]? : L2GeometryDataItem }
const query = 'select * from geometry'

export const SQLiteGeometryData: L2GeometryDataApi = {
    getItem( id: string ): L2GeometryDataItem {
        return allGeometryItems[ id ]
    },

    async load(): Promise<Array<string>> {
        allGeometryItems = {}

        let items = SQLiteDataEngine.getMany( query )
        let messages : Array<string> = []

        _.each( items, ( databaseItem: any ) => {
            let { id, data, type, size, parameters_json } = databaseItem
            let parameters = _.attempt( JSON.parse, parameters_json )
            let pointData = new RoaringBitmap32().deserialize( data, DeserializationFormat.croaring )

            pointData.shrinkToFit()

            delete parameters.type
            delete parameters.size

            let item : L2GeometryDataItem = {
                id,
                data: pointData,
                parameters: _.isError( parameters ) ? {} : parameters,
                type,
                size
            }

            allGeometryItems[ id ] = item

            if ( item.data.size !== item.size ) {
                messages.push( `${logSymbols.error} Geometry: incorrect point size for id = ${id}` )
            }
        } )

        messages.push( `Geometry: loaded ${ _.size( allGeometryItems ) } point sets` )

        return messages
    },
}