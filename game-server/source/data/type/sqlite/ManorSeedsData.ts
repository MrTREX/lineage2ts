import { L2ManorSeedsDataApi } from '../../interface/ManorSeedsDataApi'
import { L2Seed } from '../../../gameService/models/L2Seed'
import { SQLiteDataEngine } from '../../engines/sqlite'
import _ from 'lodash'

export const SQLiteManorSeedsData : L2ManorSeedsDataApi = {
    async getAll(): Promise<Array<L2Seed>> {
        const query = 'select * from manor_seeds'
        let databaseItems = SQLiteDataEngine.getMany( query )
        return _.map( databaseItems, ( databaseItem: any ) : L2Seed => {
            let seed = new L2Seed()

            seed.cropId = databaseItem[ 'cropId' ]
            seed.seedId = databaseItem[ 'seedId' ]
            seed.level = databaseItem[ 'level' ]
            seed.matureId = databaseItem[ 'matureId' ]

            seed.reward1 = databaseItem[ 'rewardOne' ]
            seed.reward2 = databaseItem[ 'rewardTwo' ]
            seed.isAlternativeValue = databaseItem[ 'alternative' ] === 'true'
            seed.cropLimit = databaseItem[ 'cropLimit' ]

            seed.seedLimit = databaseItem[ 'seedLimit' ]
            seed.castleId = databaseItem[ 'castleId' ]

            return seed
        } )
    }
}