import { L2ServitorSkillItem, L2ServitorSkillsDataApi } from '../../interface/ServitorSkillsDataApi'
import { SQLiteDataEngine } from '../../engines/sqlite'

export const SQLiteServitorSkills : L2ServitorSkillsDataApi = {
    getAll(): Array<L2ServitorSkillItem> {
        const query = 'select * from servitor_skills'
        return SQLiteDataEngine.getMany( query ).map( ( databaseItem: any ) : L2ServitorSkillItem => {
            return {
                npcId: databaseItem.npcId,
                skillId: databaseItem.skillId,
                skillLevel: databaseItem.skillLevel
            }
        } )
    }
}