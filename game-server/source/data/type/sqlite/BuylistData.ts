import { L2BuylistDataApi, L2BuylistDataItem, L2BuylistDataProduct } from '../../interface/BuylistDataApi'
import { SQLiteDataEngine } from '../../engines/sqlite'

export const SQLiteBuylistData: L2BuylistDataApi = {
    async getAll(): Promise<Array<L2BuylistDataItem>> {
        let databaseItems: Array<unknown> = SQLiteDataEngine.getMany( 'select * from buylists_data' )

        return databaseItems.map( ( databaseItem : Object ) : L2BuylistDataItem => {
            let items = JSON.parse( databaseItem[ 'items_json' ] ) as Array<Object>

            return {
                id: databaseItem[ 'listId' ],
                npcId: databaseItem[ 'npcId' ],
                products: items.map( ( item : Object ) : L2BuylistDataProduct => {
                    return {
                        amount: item[ 'count' ] ?? -1,
                        itemId: item[ 'id' ],
                        price: item[ 'price' ] ?? -1,
                        restockDelay: item[ 'restock_delay' ] ?? -1
                    }
                } )
            }
        } )
    }
}