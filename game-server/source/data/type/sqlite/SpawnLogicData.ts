import { L2SpawnLogicDataApi, L2SpawnLogicItem } from '../../interface/SpawnLogicDataApi'
import { SQLiteDataEngine } from '../../engines/sqlite'
import { L2SpawnDefaults } from '../../../gameService/enums/spawn/L2SpawnDefaults'

const defaultAiParameters = {}

export const SQLiteSpawnLogicData : L2SpawnLogicDataApi = {
    getAll(): Record<string, L2SpawnLogicItem> {
        const queryEx: string = 'select * from spawn_logic_ex'
        let databaseItemsEx = SQLiteDataEngine.getMany( queryEx )

        let allItems : Record<string, L2SpawnLogicItem> = databaseItemsEx.reduce( ( allItems: Record<string, L2SpawnLogicItem>, databaseItem: unknown ) : Record<string, L2SpawnLogicItem> => {
            let logic = createSpawnLogicItemEx( databaseItem )

            allItems[ logic.makerId ] = logic

            return allItems
        }, {} ) as Record<string, L2SpawnLogicItem>

        const query: string = 'select * from spawn_logic'
        let databaseItems = SQLiteDataEngine.getMany( query )

        return databaseItems.reduce( ( allItems: Record<string, L2SpawnLogicItem>, databaseItem: Object ) : Record<string, L2SpawnLogicItem> => {
            let logic = createSpawnLogicItem( databaseItem )

            allItems[ logic.makerId ] = logic

            return allItems
        }, allItems ) as Record<string, L2SpawnLogicItem>
    }
}

function createSpawnLogicItemEx( databaseItem: Object ) : L2SpawnLogicItem {
    let spawnTerritoryIds = JSON.parse( databaseItem[ 'territories_json' ] )
    let aiParameters = databaseItem[ 'ai_json' ] ? JSON.parse( databaseItem[ 'ai_json' ] ) : defaultAiParameters
    let avoidTerritoryIds = databaseItem[ 'avoidTerritories_json' ] ? JSON.parse( databaseItem[ 'avoidTerritories_json' ] ) : null

    return {
        aiParameters,
        avoidTerritoryIds,
        eventType: databaseItem[ 'eventType' ],
        isFlying: databaseItem[ 'flying' ] === 1,
        logicName: databaseItem[ 'logic' ],
        makerId: databaseItem[ 'makerId' ],
        maxAmount: databaseItem[ 'maxAmount' ],
        spawnTerritoryIds
    }
}

/*
    Two types of maker definitions possible, since they use slightly different format.
    Each type also works differently and may not be compatible with each other. However,
    due to somewhat compatible structure and the way makers need to be loaded, we can
    hand-wave challenge of making entirely different process of using and loading such makers.
    We simply provide more default empty values to ensure that any default maker can still
    work with data.
 */
function createSpawnLogicItem( databaseItem: Object ) : L2SpawnLogicItem {
    let spawnTerritoryIds = JSON.parse( databaseItem[ 'territories_json' ] )
    let aiParameters = databaseItem[ 'parameters_json' ] ? JSON.parse( databaseItem[ 'parameters_json' ] ) : defaultAiParameters

    return {
        aiParameters,
        avoidTerritoryIds: null,
        eventType: aiParameters[ 'eventName' ],
        isFlying: false,
        logicName: L2SpawnDefaults.SpawnLogic,
        makerId: databaseItem[ 'makerId' ],
        maxAmount: databaseItem[ 'maxAmount' ],
        spawnTerritoryIds
    }
}