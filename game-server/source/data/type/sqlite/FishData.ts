import { FishDataApi, FishDataItem } from '../../interface/FishDataApi'
import { SQLiteDataEngine } from '../../engines/sqlite'
import _ from 'lodash'
import { FishGroup } from '../../../gameService/enums/FishGroup'
import logSymbols from 'log-symbols'

// level, group, grade
let data : Record<number, Record<number, Record<number, FishDataItem>>> = {}
const query = 'select * from fish_data'

export const SQLiteFishData : FishDataApi = {
    getFish( level: number, group: FishGroup, grade: number ): FishDataItem {
        return _.get( data, [ level, group, grade ] )
    },

    async load(): Promise<Array<string>> {
        data = {}

        let databaseItems: Array<any> = SQLiteDataEngine.getMany( query )
        databaseItems.forEach( FishHelper.createFish )
        return [
           `FishData : processed ${_.size( databaseItems )} items, with ${_.size( data )} levels`
        ]
    }
}

const FishHelper = {
    createFish( databaseItem: any ) {
        let fish : FishDataItem = {
            biteRate: databaseItem.biteRate,
            cheatingProbability: databaseItem.cheatingProbability,
            combatDuration: databaseItem.combatDuration,
            grade: databaseItem.grade,
            groupId: databaseItem.groupId,
            gutsCheckProbability: databaseItem.gutsCheckProbability,
            gutsCheckTime: databaseItem.gutsCheckTime,
            hp: databaseItem.hp,
            hpRegeneration: databaseItem.hpRegeneration,
            id: databaseItem.id,
            itemId: databaseItem.itemId,
            itemName: databaseItem.itemName,
            lengthRate: databaseItem.lengthRate,
            level: databaseItem.level,
            maxLength: databaseItem.maxLength,
            startCombatTime: databaseItem.startCombatTime
        }

        let path = [ fish.level, fish.groupId, fish.grade ]
        if ( _.has( data, path ) ) {
            console.error( logSymbols.error, 'Fish data is already defined for', fish )
        }

        _.setWith( data, path, fish, Object )
    }
}