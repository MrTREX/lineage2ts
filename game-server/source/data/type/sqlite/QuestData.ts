import { L2QuestDataApi } from '../../interface/QuestDataApi'
import { SQLiteDataEngine } from '../../engines/sqlite'
import _ from 'lodash'

const query = 'select * from quest_data'

let questNames: { [ id: number ]: string }

export const SQLiteQuestData: L2QuestDataApi = {
    getQuestName( id: number ): string {
        return questNames[ id ]
    },

    async load(): Promise<Array<string>> {
        questNames = {}

        let databaseItems: Array<any> = SQLiteDataEngine.getMany( query )

        _.each( databaseItems, Helper.addQuestData )

        return [
            `QuestData : loaded ${ _.size( questNames ) } quest names.`,
        ]
    },
}

const Helper = {
    addQuestData( databaseItem: any ): void {
        let { questId, name } = databaseItem
        questNames[ questId ] = name
    },
}