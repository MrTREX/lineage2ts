import { HtmlDataApi, L2HtmlActions, L2HtmlDataSize } from '../../interface/HtmlDataApi'
import { SQLiteDataEngine } from '../../engines/sqlite'
import _ from 'lodash'
import logSymbols from 'log-symbols'

const allHtmlStrings: Map<string, string> = new Map<string, string>()
const allHtmlActions: Map<string, L2HtmlActions> = new Map<string, L2HtmlActions>()

const query = 'select * from html_cache'

export const SQLiteHtmlData: HtmlDataApi = {
    reLoad(): Promise<void> {
        return this.load()
    },

    getActions( path: string ): L2HtmlActions {
        return allHtmlActions.get( path )
    },

    hasItem( path: string ): boolean {
        return allHtmlStrings.has( path )
    },

    getItem( path: string ): string {
        if ( !allHtmlStrings.has( path ) ) {
            return getMissingHtml( path )
        }

        return allHtmlStrings.get( path ).slice()
    },

    async load() {
        allHtmlStrings.clear()
        allHtmlActions.clear()

        let databaseItems: Array<any> = SQLiteDataEngine.getMany( query )

        databaseItems.forEach( Helper.processHtmlItem )

        return [
            `HtmlData : cached ${ _.size( allHtmlStrings ) } individual paths`,
            `HtmlData : cached ${ _.size( allHtmlActions ) } known bypass actions`,
        ]
    },

    getSize(): L2HtmlDataSize {
        return {
            entries: allHtmlStrings.size,
            actions: allHtmlActions.size
        }
    }
}

function getMissingHtml( path: string ) {
    return `<html><body>My Text is missing for path:<br>${ path }</body></html>`
}

const Helper = {
    processHtmlItem( databaseItem: any ) {
        allHtmlStrings.set( databaseItem.name, databaseItem.contents )

        if ( databaseItem[ 'actions_json' ] ) {
            allHtmlActions.set( databaseItem.name, JSON.parse( databaseItem[ 'actions_json' ] ) )
        }
    },
}