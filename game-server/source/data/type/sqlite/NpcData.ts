import { L2NpcDataTemplateOrdering, NpcDataApi } from '../../interface/NpcDataApi'
import { AISkillData, L2NpcTemplate } from '../../../gameService/models/actor/templates/L2NpcTemplate'
import { SQLiteDataEngine } from '../../engines/sqlite'
import { MoveType } from '../../../gameService/models/stats/MoveType'
import { WeaponType } from '../../../gameService/models/items/type/WeaponType'
import { AIType } from '../../../gameService/enums/AIType'
import { Race } from '../../../gameService/enums/Race'
import { ConfigManager } from '../../../config/ConfigManager'
import { IDropItem } from '../../../gameService/models/drops/IDropItem'
import { DropListScopeMap } from '../../../gameService/models/drops/DropListScope'
import { Skill } from '../../../gameService/models/Skill'
import { L2EffectType } from '../../../gameService/enums/effects/L2EffectType'
import { SkillCache } from '../../../gameService/cache/SkillCache'
import { DataManager } from '../../manager'
import { L2CharacterSkillMap } from '../../../gameService/models/actor/templates/L2CharacterTemplate'
import { AbstractFunction } from '../../../gameService/models/stats/functions/AbstractFunction'
import _ from 'lodash'
import logSymbols from 'log-symbols'
import {
    AISkillFocus,
    AISkillUsage,
    AISkillUsageBehavior,
    AISkillUsageType,
    AIUsageProbabilities,
} from '../../../gameService/enums/AISkillUsage'
import { GeometryId, getGeometryByType, getMoveGeometry } from '../../../gameService/enums/GeometryId'
import { NpcTemplateType } from '../../../gameService/enums/NpcTemplateType'
import { DropListScope } from '../../../gameService/enums/drops/DropListScope'
import { L2NpcScriptedMinion } from '../../../gameService/models/actor/templates/L2NpcMinion'

let allNpcs: { [ npcId: number ]: L2NpcTemplate }
let allClans: { [ key: string ]: number }
let allClansNames: { [ key: number ]: string }
let npcTypes: Record<number, Array<L2NpcTemplate>>
let idByName: Record<string, number>

const queryNpcs = 'select * from npc_data'

let currentMessageLog: Array<string> = []

export const SQLiteNpcData: NpcDataApi = {
    getIdByName( name: string ): number {
        return idByName[ name ]
    },

    getTemplateIdsByName( name: string, ordering: L2NpcDataTemplateOrdering = L2NpcDataTemplateOrdering.Id ): Array<number> {
        const query = `select id from npc_data where name LIKE ? order by ${ordering}`
        const databaseItems : Array<unknown> = SQLiteDataEngine.getManyWithParameters( query, `%${name}%` )

        if ( !databaseItems ) {
            return []
        }

        return databaseItems.map( ( item: Object ) => item[ 'id' ] )
    },

    getTemplateIdsByLevel( levelMin: number, levelMax: number, ordering: L2NpcDataTemplateOrdering = L2NpcDataTemplateOrdering.Id ): Array<number> {
        const query = `select id from npc_data where level >= ? and level <= ? order by ${ordering}`
        const databaseItems : Array<unknown> = SQLiteDataEngine.getManyWithParameters( query, levelMin, levelMax )

        if ( !databaseItems ) {
            return []
        }

        return databaseItems.map( ( item: any ) => item.id )
    },

    getTemplatesByType( type: NpcTemplateType ): Array<L2NpcTemplate> {
        return npcTypes[ type ]
    },

    getTemplates(): { [ p: number ]: L2NpcTemplate } {
        return allNpcs
    },

    getClanId( name: string ): number {
        let id = allClans[ name.toUpperCase() ]
        return _.isNumber( id ) ? id : -1
    },

    getClanName( id: number ): string {
        return allClansNames[ id ]
    },

    getTemplate( id: number ): L2NpcTemplate {
        return allNpcs[ id ]
    },

    async load() {
        allNpcs = {}
        allClans = {}
        npcTypes = {}
        currentMessageLog = []
        allClansNames = {}
        idByName = {}

        let startTime = Date.now()

        let databaseItems: Array<any> = SQLiteDataEngine.getMany( queryNpcs )

        _.each( databaseItems, NpcHelper.createNpc )

        allClansNames = _.invert( allClans )

        npcTypes = _.mapValues( npcTypes, ( templates: Array<L2NpcTemplate> ) : Array<L2NpcTemplate> => {
            return templates.sort( ( first: L2NpcTemplate, second: L2NpcTemplate ) : number => {
                return first.level - second.level
            } )
        } )

        currentMessageLog.push( `NpcData : loaded ${ _.size( allClans ) } clans.` )
        currentMessageLog.push( `NpcData : loaded ${ _.size( allNpcs ) } templates in ${ Date.now() - startTime } ms` )

        return currentMessageLog
    }

}

const NpcHelper = {
    createNpc( databaseItem: any ) {
        let template: L2NpcTemplate = NpcHelper.createTemplate( databaseItem )

        allNpcs[ template.getId() ] = template

        if ( !npcTypes[ template.getType() ] ) {
            npcTypes[ template.getType() ] = []
        }

        npcTypes[ template.getType() ].push( template )
        idByName[ databaseItem.referenceId ] = template.getId()
    },

    createTemplate( databaseItem: any ): L2NpcTemplate {
        let template = new L2NpcTemplate()

        NpcHelper.assignL2CharacterTemplateFields( template, databaseItem )
        NpcHelper.assignL2NpcTemplateFields( template, databaseItem )
        NpcHelper.assignSkills( template, databaseItem )
        NpcHelper.assignParameters( template, databaseItem )

        if ( databaseItem[ 'droplists_json' ] ) {
            template.dropLists = NpcHelper.getDroplists( databaseItem )
        }

        let teachInfo: Array<number> = DataManager.getSkillLearnData().getClassIds( template.getId() )
        if ( teachInfo ) {
            template.teachableClassIds = teachInfo
        }

        template.canGreetPlayerValue = NpcHelper.canGreetPlayer( template )

        return template
    },

    assignL2CharacterTemplateFields( template: L2NpcTemplate, databaseItem: any ) {
        if ( databaseItem[ 'stats_json' ] ) {
            let stats = JSON.parse( databaseItem[ 'stats_json' ] )

            template.baseSTR = _.get( stats, 'str', 0 )
            template.baseCON = _.get( stats, 'con', 0 )
            template.baseDEX = _.get( stats, 'dex', 0 )
            template.baseINT = _.get( stats, 'int', 0 )
            template.baseWIT = _.get( stats, 'wit', 0 )
            template.baseMEN = _.get( stats, 'men', 0 )

            template.baseHpMax = _.get( stats, 'vitals.hp', 0 )
            template.baseCpMax = _.get( stats, 'vitals.cp', 0 )
            template.baseMpMax = _.get( stats, 'vitals.mp', 0 )
            template.baseHpReg = _.get( stats, 'vitals.hpRegen', 0 )
            template.baseMpReg = _.get( stats, 'vitals.mpRegen', 0 )

            template.basePAtk = _.get( stats, 'attack.physical', 0 )
            template.baseMAtk = _.get( stats, 'attack.magical', 0 )
            template.basePDef = _.get( stats, 'defence.physical', 0 )
            template.baseMDef = _.get( stats, 'defence.magical', 0 )

            template.basePAtkSpd = _.get( stats, 'attack.attackSpeed', 300 )
            template.baseMAtkSpd = _.get( stats, 'attack.attackMSpeed', 333 )
            template.baseShldDef = _.get( stats, 'defence.shield', 0 )
            template.baseAttackRange = _.get( stats, 'attack.range', 40 )
            template.randomDamage = _.get( stats, 'attack.random', 0 )
            template.attackReuseDelay = _.get( stats, 'attack.reuseDelay', 0 )

            template.baseAttackType = _.defaultTo( WeaponType[ _.toUpper( _.get( stats, 'attack.type' ) ) ], WeaponType.FIST )
            template.baseShldRate = _.get( stats, 'defence.shieldRate', 0 )
            template.baseCritRate = _.get( stats, 'attack.critical', 4 )
            template.baseMCritRate = _.get( stats, 'attack.mcritical', 4 )

            if ( _.isArray( _.get( stats, 'attribute.attack' ) ) ) {
                throw new Error( `NpcHelper.assignL2CharacterTemplateFields : More than one attack detected for NPC with id = ${ databaseItem[ 'id' ] }` )
            }

            let { type, value } = _.get( stats, 'attribute.attack', {} )
            type = _.toUpper( type )

            template.baseBreath = type === 'BREATH' ? value : 100
            template.baseFire = type === 'FIRE' ? value : 0
            template.baseWind = type === 'WIND' ? value : 0
            template.baseWater = type === 'WATER' ? value : 0
            template.baseEarth = type === 'EARTH' ? value : 0
            template.baseHoly = type === 'HOLY' ? value : 0
            template.baseDark = type === 'DARK' ? value : 0

            template.baseFireRes = _.get( stats, 'attribute.defence.fire', 0 )
            template.baseWindRes = _.get( stats, 'attribute.defence.wind', 0 )
            template.baseWaterRes = _.get( stats, 'attribute.defence.water', 0 )
            template.baseEarthRes = _.get( stats, 'attribute.defence.earth', 0 )
            template.baseHolyRes = _.get( stats, 'attribute.defence.holy', 0 )
            template.baseDarkRes = _.get( stats, 'attribute.defence.dark', 0 )
            template.baseElementRes = _.get( stats, 'attribute.defence.default', 0 )

            template.moveType[ MoveType.RUN ] = _.get( stats, 'speed.run.ground', 120 )
            template.moveType[ MoveType.WALK ] = _.get( stats, 'speed.walk.ground', 50 )
            template.moveType[ MoveType.FAST_SWIM ] = _.get( stats, 'speed.run.swim', template.getBaseMoveSpeed( MoveType.RUN ) )
            template.moveType[ MoveType.SLOW_SWIM ] = _.get( stats, 'speed.walk.swim', template.getBaseMoveSpeed( MoveType.WALK ) )
            template.moveType[ MoveType.FAST_FLY ] = _.get( stats, 'speed.run.fly', template.getBaseMoveSpeed( MoveType.RUN ) )
            template.moveType[ MoveType.SLOW_FLY ] = _.get( stats, 'speed.walk.fly', template.getBaseMoveSpeed( MoveType.WALK ) )
        }

        let collisionValues = databaseItem[ 'collision_json' ] ? JSON.parse( databaseItem[ 'collision_json' ] ) : null

        template.fCollisionHeight = _.get( collisionValues, 'height.normal', 0 )
        template.fCollisionRadius = _.get( collisionValues, 'radius.normal', 0 )
        template.collisionRadius = Math.floor( template.fCollisionRadius )
        template.collisionHeight = Math.floor( template.fCollisionHeight )
    },

    assignL2NpcTemplateFields( template: L2NpcTemplate, databaseItem: any ) {
        template.id = databaseItem[ 'id' ]
        template.displayId = databaseItem[ 'displayId' ] ? databaseItem[ 'displayId' ] : template.id
        template.level = _.get( databaseItem, 'level', 70 )
        template.type = _.defaultTo( NpcTemplateType[ databaseItem.type as string ], NpcTemplateType.L2Npc )

        template.name = _.defaultTo( databaseItem[ 'name' ], 'Unknown Monster' )
        template.usingServerSideName = databaseItem[ 'usingServerSideName' ] === 'true'
        template.title = _.get( databaseItem, 'title', '' )
        template.usingServerSideTitle = databaseItem[ 'usingServerSideTitle' ] === 'true'
        template.race = _.defaultTo( Race[ _.get( databaseItem, 'race' ) as string ], Race.NONE )

        let equipment = databaseItem[ 'equipment_json' ] ? JSON.parse( databaseItem[ 'equipment_json' ] ) : null
        template.chestId = _.get( equipment, 'chest', 0 )
        template.rhandId = _.get( equipment, 'rhand', 0 )
        template.lhandId = _.get( equipment, 'lhand', 0 )
        template.weaponEnchant = _.get( equipment, 'weaponEnchant', 0 )

        let acquire = databaseItem[ 'acquire_json' ] ? JSON.parse( databaseItem[ 'acquire_json' ] ) : null
        template.expRate = _.get( acquire, 'expRate', 0 )
        template.sp = _.get( acquire, 'sp', 0 )
        template.raidPoints = _.get( acquire, 'raidPoints', 0 )

        let status = databaseItem[ 'status_json' ] ? JSON.parse( databaseItem[ 'status_json' ] ) : null
        template.unique = _.get( status, 'unique', false ) === true
        template.attackable = _.get( status, 'attackable', true ) === true
        template.targetable = _.get( status, 'targetable', true ) === true
        template.undying = _.get( status, 'undying', true ) === true
        template.showName = _.get( status, 'showName', true ) === true
        template.flying = _.get( status, 'flying', false ) === true
        template.canMoveValue = _.get( status, 'canMove', true ) === true
        template.noSleepMode = _.get( status, 'noSleepMode', false ) === true
        template.passableDoor = _.get( status, 'passableDoor', false ) === true
        template.hasSummoner = _.get( status, 'hasSummoner', false ) === true
        template.canBeSownValue = _.get( status, 'canBeSown', false ) === true

        template.corpseTime = _.defaultTo( databaseItem[ 'corpsetime' ], ConfigManager.npc.getDefaultCorpseTime() )

        let aiStats = databaseItem[ 'ai_json' ] ? JSON.parse( databaseItem[ 'ai_json' ] ) : null
        template.aiType = _.defaultTo( AIType[ _.get( aiStats, 'type' ) as string ], AIType.FIGHTER )
        template.aggroRange = _.get( aiStats, 'aggroRange', 0 )
        template.clanHelpRange = _.get( aiStats, 'clanHelpRange', 0 )

        template.isChaosValue = _.get( aiStats, 'isChaos', false ) === true
        template.isAggressive = _.get( aiStats, 'isAggressive', true ) === true

        /*
            TODO : figure out to use value
            - stored in "skill" property of only 55 npcs (meaning it is hardly worthwhile to code such functionality)
            - determine source of this data
            - use or remove (consider that AI chances may need to be increased if such value is used)
         */
        template.maxSkillChance = _.get( aiStats, 'maxChance', 15 )

        let clanNames = _.get( aiStats, 'clanList.clan' )
        if ( clanNames ) {
            clanNames = _.castArray( clanNames )

            template.clans = _.map( clanNames, NpcHelper.addClan )
        }

        let ignoreIds = _.get( aiStats, 'clanList.ignoreNpcId' )
        if ( ignoreIds ) {
            template.ignoreClanNpcIds = _.castArray( ignoreIds )
        }

        let shotStats = databaseItem[ 'shots_json' ] ? JSON.parse( databaseItem[ 'shots_json' ] ) : null
        template.soulShot = _.get( shotStats, 'soul', 0 )
        template.spiritShot = _.get( shotStats, 'spirit', 0 )
        template.soulShotChance = _.get( shotStats, 'shotChance', 0 )
        template.spiritShotChance = _.get( shotStats, 'spiritChance', 0 )

        let collision = databaseItem[ 'collision_json' ] ? JSON.parse( databaseItem[ 'collision_json' ] ) : null
        template.collisionRadiusGrown = _.get( collision, 'radius.grown', 0 )
        template.collisionHeightGrown = _.get( collision, 'height.grown', 0 )
        template.aggroInterval = Math.max( Math.floor( ( template.aggroRange / 250 ) * 1000 ), 1000 )

        let levelSquared = template.getLevel() * template.getLevel()
        template.expReward = levelSquared * template.expRate
        template.vitalityPointsPerHp = levelSquared > 0 && template.expReward > 0 ? template.getBaseHpMax() * ( ( levelSquared * 9 ) / ( 100 * template.expReward ) ) : 0

        template.dropGeometryId = this.getDropGeometryId( template )
        template.moveGeometryId = getMoveGeometry( ( template.collisionRadius + template.collisionHeight ) * 2.5 )
        template.infoId = template.displayId + 1000000
    },

    // TODO : convert to build allClans using query first, then requiring no additions at runtime
    addClan( name: string ): number {
        let key = _.toUpper( name )
        let existingId = allClans[ key ]
        if ( !_.isUndefined( existingId ) ) {
            return existingId
        }

        let id = _.size( allClans )
        allClans[ key ] = id
        return id
    },

    getDroplists( databaseItem: any ): { [ key: number ]: Array<IDropItem> } {
        let droplists = JSON.parse( databaseItem[ 'droplists_json' ] )
        let itemMap = {}

        _.each( droplists, ( value: any, key: string ) => {
            let scope = DropListScope[ _.toUpper( key ) as string ]
            if ( !_.isUndefined( scope ) ) {
                itemMap[ scope ] = NpcHelper.getScopeItems( scope, value )
            }
        } )

        return itemMap
    },

    getScopeItems( scope: DropListScope, value: any ): Array<IDropItem> {
        let items: Array<IDropItem> = []

        let [ makeItem, makeGroup ] = DropListScopeMap[ scope ]

        if ( _.has( value, 'group' ) ) {
            let { group } = value

            _.castArray( group ).forEach( ( singleGroup: any ) => {
                let { item, chance } = singleGroup

                let groupItem = makeGroup( chance )
                _.castArray( item ).forEach( ( singleItem: any ) => {
                    let { id, min, max, chance } = singleItem
                    groupItem.items.push( makeItem( id, min, max, chance ) )
                } )

                items.push( groupItem )
            } )
        }

        if ( _.has( value, 'item' ) ) {
            let { item } = value

            _.castArray( item ).forEach( ( singleItem: any ) => {
                let { id, min, max, chance } = singleItem
                items.push( makeItem( id, min, max, chance ) )
            } )
        }

        return items
    },

    assignSkills( template: L2NpcTemplate, databaseItem: any ): void {
        if ( !databaseItem[ 'skilllist_json' ] ) {
            return
        }

        let skillMap = JSON.parse( databaseItem[ 'skilllist_json' ] )
        let templateSkillMap: L2CharacterSkillMap = {}
        let skills: Array<Skill> = _.reduce( skillMap, ( allSkills: Array<Skill>, level: number, id: string ) => {
            let skill = SkillCache.getSkill( _.parseInt( id ), level )

            if ( skill ) {
                templateSkillMap[ skill.getId() ] = skill
                allSkills.push( skill )
            }

            return allSkills
        }, [] )

        template.skills = skills
        template.skillMap = templateSkillMap
        template.passiveSkills = skills.filter( ( skill: Skill ): boolean => skill.isPassive() )

        if ( skills.length === 0 ) {
            return
        }

        let fighterSkillsAmount = 0
        let mageSkillsAmount = 0
        let totalSkillsAssigned = 0

        let skillUsage : AISkillUsage = {
            available: {
                [ AISkillUsageType.Buff ]: [],
                [ AISkillUsageType.Attack ]: [],
                [ AISkillUsageType.Special ]: []
            },
            probability: null,
            ai: AISkillUsageBehavior.None,
            focus: AISkillFocus.None
        }

        skills.forEach( ( skill: Skill ) => {
            if ( skill.isPassive() ) {
                template.passiveSkills.push( skill )
                return
            }

            if ( skill.isStatic() ) {
                return
            }

            if ( skill.isPhysical() ) {
                fighterSkillsAmount++
            }

            if ( skill.isMagic() ) {
                mageSkillsAmount++
            }

            if ( !skill.isDebuff()
                    && !skill.isTrigger()
                    && !skill.isDance()
                    && !skill.isToggle()
                    && !skill.isBad() ) {
                skillUsage.available[ AISkillUsageType.Buff ].push( skill )
                totalSkillsAssigned++
                return
            }

            if ( skill.isSuicideAttack || skill.hasEffectType( L2EffectType.FakeDeath ) ) {
                skillUsage.available[ AISkillUsageType.Special ].push( skill )
                totalSkillsAssigned++
                return
            }

            skillUsage.available[ AISkillUsageType.Attack ].push( skill )
            totalSkillsAssigned++
        } )

        if ( totalSkillsAssigned === 0 ) {
            return
        }

        skillUsage.ai = this.getSkillUsage( fighterSkillsAmount, mageSkillsAmount )
        skillUsage.focus = this.getSkillFocus( skillUsage )
        skillUsage.probability = this.getSkillUsageProbability( skillUsage )
        template.aiSkillUsage = skillUsage
    },

    assignParameters( template: L2NpcTemplate, databaseItem: any ) {
        if ( !databaseItem[ 'parameters_json' ] ) {
            template.parameters = {}
            return
        }

        let parameters = JSON.parse( databaseItem[ 'parameters_json' ] )
        let generalParameters = {}
        if ( parameters.param ) {
            _.castArray( parameters.param ).forEach( ( item: any ) => {
                generalParameters[ item.name ] = item.value
            } )
        }

        /*
            These skills are used by AI, hence need to be separate.
         */
        let skillParameters = {}
        if ( parameters.skill ) {
            _.castArray( parameters.skill ).forEach( ( item: any ) => {
                skillParameters[ item.name ?? item.id ] = {
                    id: item.id,
                    level: item.level,
                } as AISkillData
            } )
        }

        if ( parameters.minions ) {

            _.castArray( parameters.minions ).forEach( ( minion: unknown ) => {
                let name = minion[ 'name' ] as string
                let minionItems = minion[ 'npc' ] as unknown

                if ( name === 'Privates' ) {
                    _.castArray( minionItems ).forEach( ( item: unknown ) => {
                        template.minions.push( {
                            amount: item[ 'count' ],
                            npcId: item[ 'id' ],
                            respawnMs: item[ 'respawnTime' ]
                        } )
                    } )

                    return
                }

                let scriptedMinion : L2NpcScriptedMinion = {
                    minions: [],
                    name
                }

                _.castArray( minionItems ).forEach( ( item: unknown ) => {
                    scriptedMinion.minions.push( {
                        amount: item[ 'count' ],
                        npcId: item[ 'id' ],
                        respawnMs: item[ 'respawnTime' ]
                    } )
                } )

                template.scriptedMinions.push( scriptedMinion )
            } )


        }

        template.parameters = generalParameters
        template.skillParameters = skillParameters
    },

    canGreetPlayer( template: L2NpcTemplate ): boolean {
        return _.get( template.parameters, 'MoveAroundSocial', 0 ) > 0 || template.type === NpcTemplateType.L2Guard
    },

    getSkillUsage( fighterSkills: number, mageSkills: number ) : AISkillUsageBehavior {
        if ( fighterSkills === 0 && mageSkills === 0 ) {
            return AISkillUsageBehavior.None
        }

        if ( fighterSkills === 0 && mageSkills > 0 ) {
            return AISkillUsageBehavior.Magical
        }

        if ( mageSkills === 0 && fighterSkills > 0 ) {
            return AISkillUsageBehavior.Physical
        }

        return AISkillUsageBehavior.Both
    },

    getSkillFocus( usage: AISkillUsage ) : AISkillFocus {
        if ( usage.available[ AISkillUsageType.Buff ].length > 0
                && usage.available[ AISkillUsageType.Attack ].length === 0
                && usage.available[ AISkillUsageType.Special ].length === 0 ) {
            return AISkillFocus.Buff
        }

        if ( usage.available[ AISkillUsageType.Buff ].length === 0
                && usage.available[ AISkillUsageType.Attack ].length > 0
                && usage.available[ AISkillUsageType.Special ].length === 0 ) {
            return AISkillFocus.Attack
        }

        if ( usage.available[ AISkillUsageType.Buff ].length === 0
                && usage.available[ AISkillUsageType.Attack ].length === 0
                && usage.available[ AISkillUsageType.Special ].length > 0 ) {
            return AISkillFocus.Special
        }

        return AISkillFocus.AllSkills
    },

    getSkillUsageProbability( skillUsage: AISkillUsage ) : AIUsageProbabilities {
        return {
            [ AISkillUsageType.Buff ]: 0.5 + Math.min( 0.45, skillUsage.available[ AISkillUsageType.Buff ].length * 0.2 ),
            [ AISkillUsageType.Attack ]: 0.5 + Math.min( 0.45, skillUsage.available[ AISkillUsageType.Attack ].length * 0.15 ),
            [ AISkillUsageType.Special ]: 0.5 + Math.min( 0.45, skillUsage.available[ AISkillUsageType.Special ].length * 0.1 )
        }
    },

    getDropGeometryId( template: L2NpcTemplate ) : GeometryId {
        let radius = template.collisionRadius
        return getGeometryByType( template.type, radius )
    }
}