import { L2HitConditionBonusDataApi } from '../../interface/HitConditionBonusDataApi'
import { SQLiteDataEngine } from '../../engines/sqlite'
import logSymbols from 'log-symbols'
import _ from 'lodash'

const query = 'select * from hitcondition_bonus'

let bonus : { [ type: string] : number }

export const SQLiteHitConditionBonusData : L2HitConditionBonusDataApi = {
    getBackBonus(): number {
        return _.get( bonus, 'back', 0 )
    },

    getFrontBonus(): number {
        return _.get( bonus, 'front', 0 )
    },

    getHighBonus(): number {
        return _.get( bonus, 'high', 0 )
    },

    getLowBonus(): number {
        return _.get( bonus, 'low', 0 )
    },

    getNightBonus(): number {
        return _.get( bonus, 'dark', 0 )
    },

    getSideBonus(): number {
        return _.get( bonus, 'side', 0 )
    },

    async load(): Promise<Array<string>> {
        bonus = {}

        let databaseItems: Array<any> = SQLiteDataEngine.getMany( query )

        _.each( databaseItems, ( databaseItem: any ) => {
            let { type, value } = databaseItem
            bonus[ _.toLower( type ) ] = value
        } )

        return [
            `HitConditionBonusData loaded ${_.size( bonus )} bonuses.`
        ]
    }
}