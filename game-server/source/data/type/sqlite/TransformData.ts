import { TransformDataApi } from '../../interface/TransformDataApi'
import { Transform } from '../../../gameService/models/actor/Transform'
import { SQLiteDataEngine } from '../../engines/sqlite'
import { TransformTemplate } from '../../../gameService/models/actor/transform/TransformTemplate'
import { WeaponType } from '../../../gameService/models/items/type/WeaponType'
import { MoveType } from '../../../gameService/models/stats/MoveType'
import { Stats } from '../../../gameService/models/stats/Stats'
import { getExistingSkill } from '../../../gameService/models/holders/SkillHolder'
import { ExBasicActionsListSpecified } from '../../../gameService/packets/send/ExBasicActionList'
import { TransformLevelData } from '../../../gameService/models/actor/transform/TransformLevelData'
import { InventorySlot } from '../../../gameService/values/InventoryValues'
import logSymbols from 'log-symbols'
import _ from 'lodash'
import { TransformType } from '../../../gameService/enums/TransformType'

const query = 'select * from transformations_data'
let transformData: { [key: number]: Transform }

export const SQLiteTransformData: TransformDataApi = {
    async load() {
        transformData = {}

        let databaseItems: Array<any> = SQLiteDataEngine.getMany( query )

        _.each( databaseItems, Helper.createTransformation )

        return [
            `TranformData loaded ${ _.size( transformData ) } transformations`
        ]
    },

    getTransform( id: number ): Transform {
        return transformData[ id ]
    }
}

const Helper = {
    createTransformation( databaseItem: any ) {
        let tranformation = new Transform()

        tranformation.id = databaseItem.id
        tranformation.displayId = _.get( databaseItem, 'displayId', databaseItem.id )
        tranformation.type = TransformType[ _.get( databaseItem, 'type', 'COMBAT' ) as string ]
        tranformation.canSwimValue = _.get( databaseItem, 'canSwim', 0 ) === 1
        tranformation.canAttackValue = _.get( databaseItem, 'normalAttackable', 1 ) === 1
        tranformation.spawnHeight = _.get( databaseItem, 'spawnHeight', 0 )
        tranformation.name = _.get( databaseItem, 'setName' )
        tranformation.title = _.get( databaseItem, 'setTitle' )

        tranformation.maleTemplate = Helper.createFullTemplate( databaseItem, 'male' )
        tranformation.femaleTemplate = Helper.createFullTemplate( databaseItem, 'female' )

        transformData[ tranformation.getId() ] = tranformation
    },

    createTemplate( databaseItem: any, prefix: string ): TransformTemplate {
        let template = new TransformTemplate()

        let propertyName = `${ prefix }Common_json`
        let commonProperties = databaseItem[ propertyName ] ? JSON.parse( databaseItem[ propertyName ] ) : {}

        template.collisionRadius = _.get( commonProperties, 'collision.radius', 0 )
        template.collisionHeight = _.get( commonProperties, 'collision.height', 0 )
        template.baseAttackType = WeaponType[ _.get( commonProperties, 'base.attackType', 'FIST' ) as string ]
        template.baseAttackRange = _.get( commonProperties, 'base.range', 40 )
        template.baseRandomDamage = _.get( commonProperties, 'base.randomDamage', 0 )

        template.baseSpeed = {
            [ MoveType.WALK ]: _.get( commonProperties, 'moving.walk', 0 ),
            [ MoveType.RUN ]: _.get( commonProperties, 'moving.run', 0 ),
            [ MoveType.SLOW_SWIM ]: _.get( commonProperties, 'moving.waterWalk', 0 ),
            [ MoveType.FAST_SWIM ]: _.get( commonProperties, 'moving.waterRun', 0 ),
            [ MoveType.SLOW_FLY ]: _.get( commonProperties, 'moving.flyWalk', 0 ),
            [ MoveType.FAST_FLY ]: _.get( commonProperties, 'moving.flyRun', 0 ),
        }

        template.baseStats = {
            [ Stats.POWER_ATTACK ]: _.get( commonProperties, 'base.pAtk', 0 ),
            [ Stats.MAGIC_ATTACK ]: _.get( commonProperties, 'base.mAtk', 0 ),
            [ Stats.POWER_ATTACK_RANGE ]: _.get( commonProperties, 'base.range', 0 ),
            [ Stats.POWER_ATTACK_SPEED ]: _.get( commonProperties, 'base.attackSpeed', 0 ),
            [ Stats.CRITICAL_RATE ]: _.get( commonProperties, 'base.critRate', 0 ),
            [ Stats.STAT_STR ]: _.get( commonProperties, 'stats.str', 0 ),
            [ Stats.STAT_INT ]: _.get( commonProperties, 'stats.int', 0 ),
            [ Stats.STAT_CON ]: _.get( commonProperties, 'stats.con', 0 ),
            [ Stats.STAT_DEX ]: _.get( commonProperties, 'stats.dex', 0 ),
            [ Stats.STAT_WIT ]: _.get( commonProperties, 'stats.wit', 0 ),
            [ Stats.STAT_MEN ]: _.get( commonProperties, 'stats.men', 0 ),
        }

        template.baseDefense = {
            [ InventorySlot.Chest ]: _.get( commonProperties, 'defense.chest', 0 ),
            [ InventorySlot.Legs ]: _.get( commonProperties, 'defense.legs', 0 ),
            [ InventorySlot.Head ]: _.get( commonProperties, 'defense.head', 0 ),
            [ InventorySlot.Feet ]: _.get( commonProperties, 'defense.feet', 0 ),
            [ InventorySlot.Gloves ]: _.get( commonProperties, 'defense.gloves', 0 ),
            [ InventorySlot.Under ]: _.get( commonProperties, 'defense.underwear', 0 ),
            [ InventorySlot.Cloak ]: _.get( commonProperties, 'defense.cloak', 0 ),
            [ InventorySlot.RightEar ]: _.get( commonProperties, 'magicDefense.rear', 0 ),
            [ InventorySlot.LeftEar ]: _.get( commonProperties, 'magicDefense.lear', 0 ),
            [ InventorySlot.RightFinger ]: _.get( commonProperties, 'magicDefense.rfinger', 0 ),
            [ InventorySlot.LeftFinger ]: _.get( commonProperties, 'magicDefense.lfinger', 0 ),
            [ InventorySlot.Neck ]: _.get( commonProperties, 'magicDefense.neck', 0 ),
        }

        return template
    },

    createFullTemplate( databaseItem: any, prefix: string ) {
        let template = Helper.createTemplate( databaseItem, prefix )

        Helper.assignSkills( template, databaseItem, prefix )
        Helper.assignActions( template, databaseItem, prefix )
        Helper.assignAdditionalSkills( template, databaseItem, prefix )
        Helper.assignItems( template, databaseItem, prefix )
        Helper.assignLevels( template, databaseItem, prefix )

        return template
    },

    assignSkills( template: TransformTemplate, databaseItem: any, prefix: string ) {
        let skillsProperty = `${ prefix }Skills_json`

        if ( databaseItem[ skillsProperty ] ) {
            let skills = JSON.parse( databaseItem[ skillsProperty ] )

            _.castArray( skills ).forEach( ( skillData: any ) => {
                let { id, level } = skillData
                template.skills.push( getExistingSkill( id, level ) )
            } )
        }
    },

    assignActions( template: TransformTemplate, databaseItem: any, prefix: string ) {
        let actionsProperty = `${ prefix }Actions`

        let actionIds = _.split( databaseItem[ actionsProperty ], ' ' ).map( value => _.parseInt( value ) )
        template.actionListPacket = ExBasicActionsListSpecified( actionIds )
    },

    assignAdditionalSkills( template: TransformTemplate, databaseItem: any, prefix: string ) {
        let propertyName = `${ prefix }AdditionalSkills_json`

        if ( databaseItem[ propertyName ] ) {
            let skills = JSON.parse( databaseItem[ propertyName ] )

            _.castArray( skills ).forEach( ( skillData: any ) => {
                let { id, level, minLevel } = skillData

                template.additionalSkills.push( {
                    minimumLevel: minLevel,
                    skill: getExistingSkill( id, level )
                } )
            } )
        }
    },

    assignItems( template: TransformTemplate, databaseItem: any, prefix: string ) {
        let propertyName = `${ prefix }Items_json`

        if ( databaseItem[ propertyName ] ) {
            let items = JSON.parse( databaseItem[ propertyName ] )

            _.castArray( items ).forEach( ( itemData: any ) => {
                let { id, allowed } = itemData
                template.additionalItems.push( {
                    itemId: id,
                    allowed
                } )
            } )
        }
    },
    assignLevels( template: TransformTemplate, databaseItem: any, prefix: string ) {
        let propertyName = `${ prefix }Levels_json`

        if ( databaseItem[ propertyName ] ) {
            let levels = JSON.parse( databaseItem[ propertyName ] )

            _.castArray( levels ).forEach( ( levelData: any ) => {

                let level: TransformLevelData = Helper.createLevelData( levelData )
                template.data[ level.getLevel() ] = level
            } )
        }
    },

    createLevelData( levelData: any ): TransformLevelData {
        let level = new TransformLevelData()

        level.level = _.get( levelData, 'level' )
        level.levelModifier = _.get( levelData, 'levelMod' )

        level.stats = {
            [ Stats.MAX_HP ]: _.get( levelData, 'hp' ),
            [ Stats.MAX_MP ]: _.get( levelData, 'mp' ),
            [ Stats.MAX_CP ]: _.get( levelData, 'cp' ),
            [ Stats.REGENERATE_HP_RATE ]: _.get( levelData, 'hpRegen' ),
            [ Stats.REGENERATE_MP_RATE ]: _.get( levelData, 'mpRegen' ),
            [ Stats.REGENERATE_CP_RATE ]: _.get( levelData, 'cpRegen' ),
        }

        return level
    }
}