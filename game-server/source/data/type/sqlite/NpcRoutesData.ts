import { L2NpcRouteItem, L2NpcRoutePoint, L2NpcRoutesDataApi } from '../../interface/NpcRoutesDataApi'
import { SQLiteDataEngine } from '../../engines/sqlite'
import logSymbols from 'log-symbols'
import _ from 'lodash'

const query : string = 'select * from npc_routes'
let routes : Record<string, L2NpcRouteItem>

export const SQLiteNpcRoutesData: L2NpcRoutesDataApi = {
    getAllRoutes(): Array<L2NpcRouteItem> {
        return Object.values( routes )
    },

    getRouteByName( name: string ): L2NpcRouteItem {
        return routes[ name ]
    },

    async load(): Promise<Array<string>> {
        routes = {}

        let databaseItems = SQLiteDataEngine.getMany( query )
        let parsingErrorCount = 0

        databaseItems.forEach( ( databaseItem: any ) => {
            let points = _.attempt( JSON.parse, databaseItem[ 'nodes_json' ] ) as Array<[ number, number, number ]>

            if ( _.isError( points ) ) {
                parsingErrorCount++
                points = []
            }

            let route : L2NpcRouteItem = {
                name: databaseItem.name,
                type: databaseItem.type,
                points: points.map( ( value ) : L2NpcRoutePoint => {
                    return {
                        x: value[ 0 ],
                        y: value[ 1 ],
                        z: value[ 2 ],
                    }
                } )
            }

            routes[ route.name ] = route
        } )

        return [
            `NpcRoutes: loaded ${ _.size( routes ) } routes with ${ parsingErrorCount } errors.`
        ]
    }
}