import { L2GamePointProduct, L2GamePointProductsDataApi, L2GamePointProductItem } from '../../interface/GamePointsProductsDataApi'
import { SQLiteDataEngine } from '../../engines/sqlite'
import { ProductType } from '../../../gameService/enums/productInfo/ProductType'
import _ from 'lodash'

let allProducts : Array<L2GamePointProduct>
let productById : Record<number, L2GamePointProduct>

const query = 'select * from product_info'

export const SQLiteGamePointProductData : L2GamePointProductsDataApi = {
    getAll(): ReadonlyArray<L2GamePointProduct> {
        return allProducts
    },

    getProduct( id: number ): L2GamePointProduct {
        return productById[ id ]
    },

    async load(): Promise<Array<string>> {
        allProducts = []
        productById = {}

        let databaseItems: Array<any> = SQLiteDataEngine.getMany( query )
        let disabledCount : number = 0

        for ( const databaseItem of databaseItems ) {
            const product : L2GamePointProduct = {
                category: databaseItem.category,
                id: databaseItem.id,
                items: getItems( databaseItem.items_json ),
                price: databaseItem.price ?? 0,
                type: getType( databaseItem.type )
            }

            if ( !product.items || !databaseItem.enabled ) {
                disabledCount++
                continue
            }

            allProducts.push( product )
            productById[ product.id ] = product
        }

        return [
            `ProductInfo : loaded ${allProducts.length} item shop products`,
            `ProductInfo : rejected ${disabledCount} disabled products`,
        ]
    }
}

function getType( type: string ) : ProductType {
    switch ( type ) {
        case 'best':
            return ProductType.Best

        case 'event':
            return ProductType.Event

        case 'new':
            return ProductType.NewProduct

        default:
            return ProductType.Normal
    }
}

function getItems( jsonValue: string ) : Array<L2GamePointProductItem> {
    let values = _.attempt( JSON.parse, jsonValue )
    if ( _.isError( values ) || !Array.isArray( values ) ) {
        return null
    }

    return values.map( ( value : any ) : L2GamePointProductItem => {
        return {
            amount: value.amount,
            id: value.id
        }
    } )
}