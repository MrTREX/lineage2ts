import { CursedWeaponDataApi } from '../../interface/CursedWeaponDataApi'
import { CursedWeapon } from '../../../gameService/models/CursedWeapon'
import { SQLiteDataEngine } from '../../engines/sqlite'
import { SkillCache } from '../../../gameService/cache/SkillCache'
import logSymbols from 'log-symbols'
import _ from 'lodash'

let allWeapons : { [ key: number ]: CursedWeapon }
let allIds : Array<number>

const query = 'select * from cursed_weapons'

export const SQLiteCursedWeaponData : CursedWeaponDataApi = {
    getCursedWeaponIds(): Array<number> {
        return allIds
    },

    getCursedWeapons(): { [ key: number ]: CursedWeapon } {
        return allWeapons
    },

    async load() {
        allWeapons = {}
        allIds = []

        let databaseItems: Array<any> = SQLiteDataEngine.getMany( query )

        _.each( databaseItems, ( databaseItem: any ) => {
            let item = new CursedWeapon()
            _.assign( item, databaseItem )

            item.skillMaxLevel = SkillCache.getMaxLevel( item.skillId )

            allWeapons[ item.itemId ] = item
            allIds.push( item.itemId )
        } )

        return [
            `CursedWeapons loaded ${_.size( allWeapons )} weapons and ${_.size( allIds )} ids.`
        ]
    }
}