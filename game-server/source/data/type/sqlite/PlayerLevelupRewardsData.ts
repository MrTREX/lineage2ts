import {
    L2PlayerLevelupRewardData,
    L2PlayerLevelupRewardItem,
    L2PlayerLevelupRewardsDataApi,
} from '../../interface/PlayerLevelupRewardsDataApi'
import { SQLiteDataEngine } from '../../engines/sqlite'
import { Race } from '../../../gameService/enums/Race'
import _ from 'lodash'

const query = 'select * from player_levelup_rewards'

let playerLevelupData: { [ level: number ]: Array<L2PlayerLevelupRewardData> }

export const SQLitePlayerLevelupRewardsData: L2PlayerLevelupRewardsDataApi = {
    getRewards( level: number ): Array<L2PlayerLevelupRewardData> {
        return playerLevelupData[ level ]
    },

    async load(): Promise<Array<string>> {
        playerLevelupData = {}
        let processedAmount = 0

        let databaseItems: Array<any> = SQLiteDataEngine.getMany( query )

        _.each( databaseItems, ( databaseItem: any ) => {
            if ( Helper.createRewards( databaseItem ) ) {
                processedAmount++
            }
        } )

        return [
            `PlayerLevelupRewards : loaded ${ _.size( playerLevelupData ) } levels`,
            `PlayerLevelupRewards : rejected ${ _.size( databaseItems ) - processedAmount } records.`,
        ]
    },
}

const Helper = {
    createRewards( databaseItem: any ): boolean {
        let { level, race, itemId, itemAmount } = databaseItem
        let correctedRace = _.toUpper( race )

        if ( !_.has( Race, correctedRace ) && correctedRace !== 'ALL' ) {
            return false
        }

        if ( !playerLevelupData[ level ] ) {
            playerLevelupData[ level ] = []
        }

        let data: L2PlayerLevelupRewardData = _.find( playerLevelupData[ level ], ( currentItem: L2PlayerLevelupRewardData ): boolean => {
            return currentItem.race === correctedRace
        } )

        let rewardItem: L2PlayerLevelupRewardItem = {
            itemId,
            amount: itemAmount,
        }

        if ( data ) {
            data.items.push( rewardItem )
            return true
        }

        let item: L2PlayerLevelupRewardData = {
            items: [ rewardItem ],
            level,
            race: correctedRace,
        }

        playerLevelupData[ level ].push( item )
        return true
    },
}