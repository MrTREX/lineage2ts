import { Condition } from '../../../../gameService/models/conditions/Condition'
import { ConditionLogicAnd } from '../../../../gameService/models/conditions/logic/ConditionLogicAnd'
import { Race } from '../../../../gameService/enums/Race'
import { ConditionPlayerRace } from '../../../../gameService/models/conditions/player/ConditionPlayerRace'
import { PlayerState } from '../../../../gameService/enums/PlayerState'
import { AbnormalType } from '../../../../gameService/models/skills/AbnormalType'
import { CategoryType } from '../../../../gameService/enums/CategoryType'
import { ConditionPlayerLevel } from '../../../../gameService/models/conditions/player/ConditionPlayerLevel'
import { ConditionPlayerLevelRange } from '../../../../gameService/models/conditions/player/ConditionPlayerLevelRange'
import { ConditionPlayerState } from '../../../../gameService/models/conditions/player/ConditionPlayerState'
import { ConditionPlayerIsHero } from '../../../../gameService/models/conditions/player/ConditionPlayerIsHero'
import { ConditionPlayerTransformationId } from '../../../../gameService/models/conditions/player/ConditionPlayerTransformationId'
import { ConditionPlayerHp } from '../../../../gameService/models/conditions/player/ConditionPlayerHp'
import { ConditionPlayerMp } from '../../../../gameService/models/conditions/player/ConditionPlayerMp'
import { ConditionPlayerCp } from '../../../../gameService/models/conditions/player/ConditionPlayerCp'
import { ConditionPlayerPkCount } from '../../../../gameService/models/conditions/player/ConditionPlayerPkCount'
import { ConditionPlayerGrade } from '../../../../gameService/models/conditions/player/ConditionPlayerGrade'
import { ConditionSiegeZone } from '../../../../gameService/models/conditions/ConditionSiegeZone'
import { ConditionPlayerSiegeSide } from '../../../../gameService/models/conditions/player/ConditionPlayerSiegeSide'
import { ConditionPlayerCharges } from '../../../../gameService/models/conditions/player/ConditionPlayerCharges'
import { ConditionPlayerSouls } from '../../../../gameService/models/conditions/player/ConditionPlayerSouls'
import { ConditionPlayerWeight } from '../../../../gameService/models/conditions/player/ConditionPlayerWeight'
import { ConditionPlayerInventorySize } from '../../../../gameService/models/conditions/player/ConditionPlayerInventorySize'
import { ConditionPlayerIsClanLeader } from '../../../../gameService/models/conditions/player/ConditionPlayerIsClanLeader'
import { ConditionPlayerTvTEvent } from '../../../../gameService/models/conditions/player/ConditionPlayerTvTEvent'
import { ConditionPlayerPledgeClass } from '../../../../gameService/models/conditions/player/ConditionPlayerPledgeClass'
import { ConditionPlayerHasClanHalls } from '../../../../gameService/models/conditions/player/ConditionPlayerHasClanHall'
import { ConditionPlayerHasFort } from '../../../../gameService/models/conditions/player/ConditionPlayerHasFort'
import { ConditionPlayerHasCastle } from '../../../../gameService/models/conditions/player/ConditionPlayerHasCastle'
import { ConditionPlayerSex } from '../../../../gameService/models/conditions/player/ConditionPlayerSex'
import { ConditionPlayerFlyMounted } from '../../../../gameService/models/conditions/player/ConditionPlayerFlyMounted'
import { ConditionPlayerVehicleMounted } from '../../../../gameService/models/conditions/player/ConditionPlayerVehicleMounted'
import { ConditionPlayerLandingZone } from '../../../../gameService/models/conditions/player/ConditionPlayerLandingZone'
import { ConditionPlayerActiveEffectId } from '../../../../gameService/models/conditions/player/ConditionPlayerActiveEffectId'
import { ConditionPlayerActiveSkillId } from '../../../../gameService/models/conditions/player/ConditionPlayerActiveSkillId'
import { ConditionPlayerClassIdRestriction } from '../../../../gameService/models/conditions/player/ConditionPlayerClassIdRestriction'
import { ConditionPlayerSubclass } from '../../../../gameService/models/conditions/player/ConditionPlayerSubclass'
import { ConditionPlayerInstanceId } from '../../../../gameService/models/conditions/player/ConditionPlayerInstanceId'
import { ConditionPlayerAgathionId } from '../../../../gameService/models/conditions/player/ConditionPlayerAgathionId'
import { ConditionPlayerCloakStatus } from '../../../../gameService/models/conditions/player/ConditionPlayerCloakStatus'
import { ConditionPlayerHasPet } from '../../../../gameService/models/conditions/player/ConditionPlayerHasPet'
import { ConditionPlayerRangeFromNpc } from '../../../../gameService/models/conditions/player/ConditionPlayerRangeFromNpc'
import { ConditionPlayerCallPc } from '../../../../gameService/models/conditions/player/ConditionPlayerCallPc'
import { ConditionPlayerCanCreateBase } from '../../../../gameService/models/conditions/player/ConditionPlayerCanCreateBase'
import { ConditionPlayerCanCreateOutpost } from '../../../../gameService/models/conditions/player/ConditionPlayerCanCreateOutpost'
import { ConditionPlayerCanEscape } from '../../../../gameService/models/conditions/player/ConditionPlayerCanEscape'
import { ConditionPlayerCanRefuelAirship } from '../../../../gameService/models/conditions/player/ConditionPlayerCanRefuelAirship'
import { ConditionPlayerCanResurrect } from '../../../../gameService/models/conditions/player/ConditionPlayerCanResurrect'
import { ConditionPlayerCanSummon } from '../../../../gameService/models/conditions/player/ConditionPlayerCanSummon'
import { ConditionPlayerCanSummonSiegeGolem } from '../../../../gameService/models/conditions/player/ConditionPlayerCanSummonSiegeGolem'
import { ConditionPlayerCanSweep } from '../../../../gameService/models/conditions/player/ConditionPlayerCanSweep'
import { ConditionPlayerCanTakeCastle } from '../../../../gameService/models/conditions/player/ConditionPlayerCanTakeCastle'
import { ConditionPlayerCanTakeFort } from '../../../../gameService/models/conditions/player/ConditionPlayerCanTakeFort'
import { ConditionPlayerCanTransform } from '../../../../gameService/models/conditions/player/ConditionPlayerCanTransform'
import { ConditionPlayerCanUntransform } from '../../../../gameService/models/conditions/player/ConditionPlayerCanUntransform'
import { ConditionPlayerInsideZoneId } from '../../../../gameService/models/conditions/player/ConditionPlayerInsideZoneId'
import { ConditionPlayerCheckAbnormal } from '../../../../gameService/models/conditions/player/ConditionPlayerCheckAbnormal'
import { ConditionCategoryTypes } from '../../../../gameService/models/conditions/ConditionCategoryType'
import { ConditionPlayerHasAgathion } from '../../../../gameService/models/conditions/player/ConditionPlayerHasAgathion'
import { ConditionPlayerAgathionEnergy } from '../../../../gameService/models/conditions/player/ConditionPlayerAgathionEnergy'
import { ConditionPlayerHasServitor } from '../../../../gameService/models/conditions/player/ConditionPlayerHasServitor'
import { ConditionLogicOr } from '../../../../gameService/models/conditions/logic/ConditionLogicOr'
import { ConditionLogicNot } from '../../../../gameService/models/conditions/logic/ConditionLogicNot'
import { WeaponType, WeaponTypeMask } from '../../../../gameService/models/items/type/WeaponType'
import { ArmorType, ArmorTypeMask } from '../../../../gameService/enums/items/ArmorType'
import { InstanceType } from '../../../../gameService/enums/InstanceType'
import { ConditionTargetRace } from '../../../../gameService/models/conditions/target/ConditionTargetRace'
import { ConditionTargetAggro } from '../../../../gameService/models/conditions/target/ConditionTargetAggro'
import { ConditionTargetLevel } from '../../../../gameService/models/conditions/target/ConditionTargetLevel'
import { ConditionTargetLevelRange } from '../../../../gameService/models/conditions/target/ConditionTargetLevelRange'
import { ConditionTargetMyParty } from '../../../../gameService/models/conditions/target/ConditionTargetMyParty'
import { ConditionTargetPlayable } from '../../../../gameService/models/conditions/target/ConditionTargetPlayable'
import { ConditionTargetClassIdRestriction } from '../../../../gameService/models/conditions/target/ConditionTargetClassIdRestriction'
import { ConditionTargetActiveEffectId } from '../../../../gameService/models/conditions/target/ConditionTargetActiveEffectId'
import { ConditionTargetActiveSkillId } from '../../../../gameService/models/conditions/target/ConditionTargetActiveSkillId'
import { ConditionTargetAbnormal } from '../../../../gameService/models/conditions/target/ConditionTargetAbnormal'
import { ConditionMinDistance } from '../../../../gameService/models/conditions/target/ConditionMinDistance'
import { ConditionTargetUsesWeaponKind } from '../../../../gameService/models/conditions/target/ConditionTargetUsesWeaponKind'
import { ConditionTargetNpcId } from '../../../../gameService/models/conditions/target/ConditionTargetNpcId'
import { ConditionTargetNpcType } from '../../../../gameService/models/conditions/target/ConditionTargetNpcType'
import { ConditionTargetWeight } from '../../../../gameService/models/conditions/target/ConditionTargetWeight'
import { ConditionTargetInventorySize } from '../../../../gameService/models/conditions/target/ConditionTargetInventorySize'
import { ConditionUsingItemType } from '../../../../gameService/models/conditions/using/ConditionUsingItemType'
import { ConditionUsingSlotType } from '../../../../gameService/models/conditions/using/ConditionUsingSlotType'
import { ConditionUsingSkill } from '../../../../gameService/models/conditions/using/ConditionUsingSkill'
import { ConditionSlotItemId } from '../../../../gameService/models/conditions/using/ConditionSlotItemId'
import { ConditionChangeWeapon } from '../../../../gameService/models/conditions/using/ConditionChangeWeapon'
import { ConditionGameChance } from '../../../../gameService/models/conditions/ConditionGameChance'
import { ConditionGameTime, ConditionGameTimePeriods } from '../../../../gameService/models/conditions/ConditionGameTime'
import { ConditionWithSkill } from '../../../../gameService/models/conditions/ConditionWithSkill'
import { ConditionPlayerCompanion } from '../../../../gameService/models/conditions/player/ConditionPlayerCompanion'
import { ConditionPlayerExistNearNpc } from '../../../../gameService/models/conditions/player/ConditionPlayerExistNearNpc'
import { ConditionNotUnderSiege } from '../../../../gameService/models/conditions/world/ConditionNotUnderSiege'
import _ from 'lodash'
import { ItemTableSlots } from '../../../../gameService/enums/L2ItemSlots'
import { ServerLog } from '../../../../logger/Logger'

export const ConditionHelper = {
    createCondition( key: string, value: any, getValueMethod: Function ): Condition {
        switch ( key ) {
            case 'and':
                return ConditionHelper.createConditionAnd( value, getValueMethod )

            case 'or':
                return ConditionHelper.createConditionOr( value, getValueMethod )

            case 'not':
                return ConditionHelper.createConditionNot( value, getValueMethod )

            case 'player':
                return ConditionHelper.createSpecificCondition( value, ConditionHelper.getPlayerCondition.bind( null, getValueMethod ) )

            case 'target':
                return ConditionHelper.createSpecificCondition( value, ConditionHelper.getTargetCondition.bind( null, getValueMethod ) )

            case 'using':
                return ConditionHelper.createSpecificCondition( value, ConditionHelper.getUsingCondition.bind( null, getValueMethod ) )

            case 'game':
                return ConditionHelper.createSpecificCondition( value, ConditionHelper.getGameCondition.bind( null, getValueMethod ) )
        }

        return null
    },

    createConditionAnd( attribute: any, getValueMethod: Function ): Condition {
        let condition = new ConditionLogicAnd()

        _.each( attribute, ( value: any, key: string ) => {
            condition.add( ConditionHelper.createCondition( key, value, getValueMethod ) )
        } )

        return condition
    },

    createConditionOr( attribute: any, getValueMethod: Function ): Condition {
        let condition = new ConditionLogicOr()

        _.each( attribute, ( value: any, key: string ) => {
            condition.add( ConditionHelper.createCondition( key, value, getValueMethod ) )
        } )

        return condition
    },

    createConditionNot( attribute: any, getValueMethod: Function ): Condition {
        let key = _.head( _.keys( attribute ) )
        return new ConditionLogicNot( ConditionHelper.createCondition( key, attribute[ key ], getValueMethod ) )
    },

    createSpecificCondition( attribute: any, createCondition: ( value: any ) => Condition ): Condition {
        let values: any = _.isArray( attribute ) ? _.defaults( {}, ..._.castArray( attribute ) ) : attribute
        let allConditions: Array<Condition> = _.map( values, createCondition )

        if ( allConditions.length === 0 ) {
            return null
        }

        if ( allConditions.length === 1 ) {
            return allConditions[ 0 ]
        }

        let containerCondition = new ConditionLogicAnd()

        _.each( allConditions, ( condition: Condition ) => {
            if ( condition ) {
                containerCondition.add( condition )
            }
        } )

        return containerCondition
    },

    getPlayerCondition( getValueMethod: Function, value: any, key: string ): Condition {
        const getNumber = ( currentValue ) => {
            return _.parseInt( getValueMethod( currentValue ) )
        }

        switch ( _.toLower( key ) ) {
            case 'races':
                let raceValues = _.map( _.split( value, ',' ), ( race: string ) => {
                    return Race[ race ]
                } )
                return new ConditionPlayerRace( raceValues )
            case 'level':
                return new ConditionPlayerLevel( getNumber( value ) )
            case 'levelrange':
                let [ minimumLevel, maximumLevel ] = _.split( getValueMethod( value ), ';' ).map( value => _.parseInt( value ) )
                return new ConditionPlayerLevelRange( minimumLevel, maximumLevel )
            case 'resting':
                return new ConditionPlayerState( PlayerState.RESTING, value === true )
            case 'flying':
                return new ConditionPlayerState( PlayerState.FLYING, value === true )
            case 'moving':
                return new ConditionPlayerState( PlayerState.MOVING, value === true )
            case 'running':
                return new ConditionPlayerState( PlayerState.RUNNING, value === true )
            case 'standing':
                return new ConditionPlayerState( PlayerState.STANDING, value === true )
            case 'behind':
                return new ConditionPlayerState( PlayerState.BEHIND, value === true )
            case 'front':
                return new ConditionPlayerState( PlayerState.FRONT, value === true )
            case 'chaotic':
                return new ConditionPlayerState( PlayerState.CHAOTIC, value === true )
            case 'olympiad':
                return new ConditionPlayerState( PlayerState.OLYMPIAD, value === true )
            case 'ishero':
                return new ConditionPlayerIsHero( value === true )
            case 'transformationid':
                return new ConditionPlayerTransformationId( value )
            case 'hp':
                return new ConditionPlayerHp( getNumber( value ) )
            case 'mp':
                return new ConditionPlayerMp( getNumber( value ) )
            case 'cp':
                return new ConditionPlayerCp( getNumber( value ) )
            case 'grade':
                return new ConditionPlayerGrade( getNumber( value ) )
            case 'pkcount':
                return new ConditionPlayerPkCount( getNumber( value ) )
            case 'siegezone':
                return new ConditionSiegeZone( getNumber( value ), true )
            case 'siegeside':
                return new ConditionPlayerSiegeSide( getNumber( value ) )
            case 'charges':
                return new ConditionPlayerCharges( getNumber( value ) )
            case 'souls':
                return new ConditionPlayerSouls( getNumber( value ) )
            case 'weight':
                return new ConditionPlayerWeight( getNumber( value ) )
            case 'invsize':
                return new ConditionPlayerInventorySize( getNumber( value ) )
            case 'isclanleader':
                return new ConditionPlayerIsClanLeader( value === true )
            case 'ontvtevent':
                return new ConditionPlayerTvTEvent( value === true )
            case 'pledgeclass':
                return new ConditionPlayerPledgeClass( getNumber( value ) )
            case 'clanhall':
                let clanHallIds = _.split( getValueMethod( value ), ',' ).map( value => _.parseInt( value ) )
                return new ConditionPlayerHasClanHalls( clanHallIds )
            case 'fort':
                return new ConditionPlayerHasFort( getNumber( value ) )
            case 'castle':
                return new ConditionPlayerHasCastle( getNumber( value ) )
            case 'sex':
                return new ConditionPlayerSex( getNumber( value ) )
            case 'flymounted':
                return new ConditionPlayerFlyMounted( value === true )
            case 'vehiclemounted':
                return new ConditionPlayerVehicleMounted( value === true )
            case 'landingzone':
                return new ConditionPlayerLandingZone( value === true )
            case 'active_effect_id':
                return new ConditionPlayerActiveEffectId( getNumber( value ) )
            case 'active_effect_id_lvl':
                let [ effectId, effectLevel ] = _.split( getValueMethod( value ), ',' ).map( value => _.parseInt( value ) )
                return new ConditionPlayerActiveEffectId( effectId, effectLevel )
            case 'active_skill_id':
                return new ConditionPlayerActiveSkillId( getNumber( value ) )
            case 'active_skill_id_lvl':
                let [ skillId, skillLevel ] = _.split( getValueMethod( value ), ',' ).map( value => _.parseInt( value ) )
                return new ConditionPlayerActiveSkillId( skillId, skillLevel )
            case 'class_id_restriction':
                let classIds = _.split( getValueMethod( value ), ',' ).map( value => _.parseInt( value ) )
                return new ConditionPlayerClassIdRestriction( classIds )
            case 'subclass':
                return new ConditionPlayerSubclass( value === true )
            case 'instanceid':
                let instanceIds = _.split( getValueMethod( value ), ',' ).map( value => _.parseInt( value ) )
                return new ConditionPlayerInstanceId( instanceIds )
            case 'agathionid':
                return new ConditionPlayerAgathionId( value )
            case 'cloakstatus':
                return new ConditionPlayerCloakStatus( value === true )
            case 'haspet':
                let petIds = _.split( getValueMethod( value ), ',' ).map( value => _.parseInt( value ) )
                return new ConditionPlayerHasPet( petIds )
            case 'hasservitor':
                return new ConditionPlayerHasServitor()
            case 'npcidradius':
                let dataChunks = _.split( value, ',' )

                if ( dataChunks.length === 3 ) {
                    let [ idChunks, radius, shouldExist ] = dataChunks
                    let npcIds = _.split( getValueMethod( idChunks ), ' ' ).map( value => _.parseInt( value ) )
                    let radiusValue = _.parseInt( radius )
                    let shouldExistValue = shouldExist === 'true'

                    return new ConditionPlayerRangeFromNpc( npcIds, radiusValue, shouldExistValue )
                }

                return null
            case 'callpc':
                return new ConditionPlayerCallPc( value === true )
            case 'cancreatebase':
                return new ConditionPlayerCanCreateBase( value === true )
            case 'cancreateoutpost':
                return new ConditionPlayerCanCreateOutpost( value === true )
            case 'canescape':
                return new ConditionPlayerCanEscape( value === true )
            case 'canrefuelairship':
                return new ConditionPlayerCanRefuelAirship( value )
            case 'canresurrect':
                return new ConditionPlayerCanResurrect( value === true )
            case 'cansummon':
                return new ConditionPlayerCanSummon( value === true )
            case 'cansummonsiegegolem':
                return new ConditionPlayerCanSummonSiegeGolem( value === true )
            case 'cansweep':
                return new ConditionPlayerCanSweep( value === true )
            case 'cantakecastle':
                return new ConditionPlayerCanTakeCastle()
            case 'cantakefort':
                return new ConditionPlayerCanTakeFort( value === true )
            case 'cantransform':
                return new ConditionPlayerCanTransform( value === true )
            case 'canuntransform':
                return new ConditionPlayerCanUntransform( value === true )
            case 'insidezoneid':
                let zoneIds = _.split( getValueMethod( value ), ',' ).map( value => _.parseInt( value ) )
                return new ConditionPlayerInsideZoneId( zoneIds )
            case 'checkabnormal':
                let [ typeValue, level, defaultValue ] = _.split( value, ';' )
                return new ConditionPlayerCheckAbnormal( AbnormalType[ typeValue as string ], _.parseInt( getValueMethod( level ) ), defaultValue === 'true' )
            case 'categorytype':
                let groups = _.map( _.split( value, ',' ), ( categoryType: string ) => CategoryType[ getValueMethod( categoryType ) as string ] )
                return new ConditionCategoryTypes( groups )
            case 'hasagathion':
                return new ConditionPlayerHasAgathion( value === true )
            case 'agathionenergy':
                return new ConditionPlayerAgathionEnergy( getValueMethod( value ) )

            case 'companion':
                return new ConditionPlayerCompanion( value )

            case 'existnpc':
                let [ ids, radiusValue, shouldEnforceValue ] = _.split( value, ';' )
                return new ConditionPlayerExistNearNpc( _.split( ids, ',' ).map( value => _.parseInt( value ) ), _.parseInt( radiusValue ), _.toLower( shouldEnforceValue ) === 'true' )

            default:
                ServerLog.error( `ConditionHelper.getPlayerCondition : Cannot process condition with key = '${ key }'` )
                return null
        }
    },

    getTargetCondition( getValueMethod: Function, value: any, key: string ): Condition {
        const getNumber = ( currentValue ) => {
            return _.parseInt( getValueMethod( currentValue ) )
        }

        switch ( _.toLower( key ) ) {
            case 'aggro':
                return new ConditionTargetAggro( value === 'true' )
            case 'siegezone':
                return new ConditionSiegeZone( getNumber( value ), false )
            case 'level':
                return new ConditionTargetLevel( getNumber( value ) )
            case 'levelrange':
                let [ minLevel, maxLevel ] = _.split( getValueMethod( value ), ';' ).map( value => _.parseInt( value ) )
                return new ConditionTargetLevelRange( minLevel, maxLevel )
            case 'myparty':
                return new ConditionTargetMyParty( value )
            case 'playable':
                return new ConditionTargetPlayable()
            case 'class_id_restriction':
                let classIds = _.split( getValueMethod( value ), ',' ).map( value => _.parseInt( value ) )
                return new ConditionTargetClassIdRestriction( classIds )
            case 'active_effect_id':
                return new ConditionTargetActiveEffectId( value )
            case 'active_effect_id_lvl':
                let [ effectId, effectLevel ] = _.split( getValueMethod( value ), ',' ).map( value => _.parseInt( value ) )
                return new ConditionTargetActiveEffectId( effectId, effectLevel )
            case 'active_skill_id':
                return new ConditionTargetActiveSkillId( getNumber( value ) )
            case 'active_skill_id_lvl':
                let [ skillId, skillLevel ] = _.split( getValueMethod( value ), ',' ).map( value => _.parseInt( value ) )
                return new ConditionTargetActiveSkillId( skillId, skillLevel )
            case 'abnormal':
                return new ConditionTargetAbnormal( getNumber( value ) )
            case 'mindistance':
                let distance = getNumber( value )
                return new ConditionMinDistance( distance * distance )
            case 'race':
                return new ConditionTargetRace( Race[ value as string ] )
            case 'using':
                let mask = 0
                let typeValues = _.split( value, ',' )

                _.each( typeValues, ( type: string ) => {
                    let weaponValue = WeaponType[ type ]
                    if ( !_.isUndefined( weaponValue ) ) {
                        mask |= WeaponTypeMask( weaponValue )
                        return
                    }

                    let armorValue = ArmorType[ type ]
                    if ( !_.isUndefined( armorValue ) ) {
                        mask |= ArmorTypeMask( armorValue )
                        return
                    }
                } )

                return new ConditionTargetUsesWeaponKind( mask )
            case 'npcid':
                let npcIds = _.split( getValueMethod( value ), ',' ).map( value => _.parseInt( value ) )
                return new ConditionTargetNpcId( npcIds )
            case 'npctype':
                let instanceTypes: Array<InstanceType> = _.map( _.split( getValueMethod( value ), ',' ), ( type: string ) => {
                    return InstanceType[ type ]
                } )

                return new ConditionTargetNpcType( instanceTypes )
            case 'weight':
                return new ConditionTargetWeight( getNumber( value ) )
            case 'invsize':
                return new ConditionTargetInventorySize( getNumber( value ) )
            case 'checkabnormal':
                let [ typeValue, level, defaultValue ] = _.split( value, ';' )
                return new ConditionPlayerCheckAbnormal( AbnormalType[ typeValue as string ], _.parseInt( getValueMethod( level ) ), defaultValue === 'true' )
            default:
                ServerLog.error( `ConditionHelper.getTargetCondition : Cannot process condition with key = '${ key }'` )
                return null
        }
    },

    getUsingCondition( getValueMethod: Function, value: any, key: string ): Condition {
        switch ( _.toLower( key ) ) {
            case 'kind':
                let mask = 0
                let typeValues = _.split( value, ',' )

                _.each( typeValues, ( type: string ) => {
                    let weaponValue = WeaponType[ type ]
                    if ( !_.isUndefined( weaponValue ) ) {
                        mask |= WeaponTypeMask( weaponValue )
                        return
                    }

                    let armorValue = ArmorType[ type ]
                    if ( !_.isUndefined( armorValue ) ) {
                        mask |= ArmorTypeMask( armorValue )
                        return
                    }
                } )

                return new ConditionUsingItemType( mask )
            case 'slot':
                let slotMask = 0
                let slotValues = _.split( value, ',' )

                _.each( slotValues, ( type: string ) => {
                    let slot = ItemTableSlots[ type ]
                    if ( !_.isUndefined( slot ) ) {
                        slotMask |= slot
                    }
                } )

                return new ConditionUsingSlotType( slotMask )
            case 'skill':
                return new ConditionUsingSkill( value )
            case 'slotitem':
                let [ slotId, slotValue, enchant ] = _.split( value, ';' ).map( value => _.parseInt( value ) )
                return new ConditionSlotItemId( slotValue, slotId, _.defaultTo( enchant, 0 ) )
            case 'weaponchange':
                return new ConditionChangeWeapon( value === 'true' )
            default:
                ServerLog.error( `ConditionHelper.getUsingCondition : Cannot process condition with key = '${ key }'` )
                return null
        }
    },

    getGameCondition( getValueMethod: Function, value: any, key: string ): Condition {
        const getNumber = ( currentValue ) => {
            return _.parseInt( getValueMethod( currentValue ) )
        }

        switch ( _.toLower( key ) ) {
            case 'skill':
                return new ConditionWithSkill( value === 'true' )
            case 'night':
                return new ConditionGameTime( ConditionGameTimePeriods.NIGHT, value === 'true' )
            case 'chance':
                return new ConditionGameChance( getNumber( value ) )
            case 'notSiege':
                return new ConditionNotUnderSiege( getNumber( value ) )
            default:
                ServerLog.error( `ConditionHelper.getGameCondition : Cannot process condition with key = '${ key }'` )
                return null
        }
    },
}