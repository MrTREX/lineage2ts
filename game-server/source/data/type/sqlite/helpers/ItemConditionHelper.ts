import { Condition } from '../../../../gameService/models/conditions/Condition'
import { ConditionPlayerRace } from '../../../../gameService/models/conditions/player/ConditionPlayerRace'
import { ConditionPlayerIsHero } from '../../../../gameService/models/conditions/player/ConditionPlayerIsHero'
import { ConditionPlayerHasCastle } from '../../../../gameService/models/conditions/player/ConditionPlayerHasCastle'
import { ConditionPlayerSex } from '../../../../gameService/models/conditions/player/ConditionPlayerSex'
import { ConditionPlayerHasClanHall } from '../../../../gameService/models/conditions/player/ConditionPlayerHasClanHall'
import { ConditionPlayerNoble } from '../../../../gameService/models/conditions/player/ConditionPlayerNoble'
import {
    ConditionPlayerInClanAcademy
} from '../../../../gameService/models/conditions/player/ConditionPlayerInClanAcademy'
import { ConditionPlayerPledgeClass } from '../../../../gameService/models/conditions/player/ConditionPlayerPledgeClass'
import { ConditionPlayerLevel } from '../../../../gameService/models/conditions/player/ConditionPlayerLevel'
import { ConditionPlayerSubclass } from '../../../../gameService/models/conditions/player/ConditionPlayerSubclass'
import { ConditionPlayerSiegeArea } from '../../../../gameService/models/conditions/player/ConditionPlayerSiegeArea'
import { ConditionPlayerHasFort } from '../../../../gameService/models/conditions/player/ConditionPlayerHasFort'
import { ConditionPlayerState } from '../../../../gameService/models/conditions/player/ConditionPlayerState'
import { PlayerState } from '../../../../gameService/enums/PlayerState'
import { ConditionCategoryType } from '../../../../gameService/models/conditions/ConditionCategoryType'
import { CategoryType } from '../../../../gameService/enums/CategoryType'
import { ConditionPlayerInstanceId } from '../../../../gameService/models/conditions/player/ConditionPlayerInstanceId'
import { ConditionPlayerLevelRange } from '../../../../gameService/models/conditions/player/ConditionPlayerLevelRange'
import {
    ConditionPlayerIsClanLeader
} from '../../../../gameService/models/conditions/player/ConditionPlayerIsClanLeader'

function getPlayerState( name: string, value: boolean ) : Condition {
    switch ( name ) {
        case 'flying':
            return new ConditionPlayerState( PlayerState.FLYING, value )

        case 'non_transform':
            return new ConditionPlayerState( PlayerState.TRANSFORMED, !value )

        default:
            throw new Error( `Unable to get item player state for name=${name}` )
    }
}

export function createCondition( name: string, value: string | number | boolean ) : Condition {
    switch ( name ) {
        case 'playerRace':
            return new ConditionPlayerRace( ( value as string ).split( ',' ).map( race => parseInt( race, 10 ) ), 1518 )

        case 'isHero':
            return new ConditionPlayerIsHero( value as boolean, 1518 )

        case 'castle':
            return new ConditionPlayerHasCastle( value as number )

        case 'hasCastle':
            return new ConditionPlayerHasCastle()

        case 'sex':
            return new ConditionPlayerSex( value as number )

        case 'hasClanHall':
            return new ConditionPlayerHasClanHall()

        case 'isNoblesse':
            return new ConditionPlayerNoble( value as boolean )

        case 'inClanAcademy':
            return new ConditionPlayerInClanAcademy( value as boolean )

        case 'pledgeClass':
            return new ConditionPlayerPledgeClass( value as number )

        case 'requireLevel':
            return new ConditionPlayerLevel( value as number )

        case 'hasSubclass':
            return new ConditionPlayerSubclass( value as boolean )

        case 'clanHall':
            return new ConditionPlayerHasClanHall( value as number )

        case 'inSiegeArea':
            return new ConditionPlayerSiegeArea( value as boolean )

        case 'hasFortress':
            return new ConditionPlayerHasFort()

        case 'chaotic':
            return new ConditionPlayerState( PlayerState.CHAOTIC, value as boolean )

        case 'categoryType':
            return new ConditionCategoryType( CategoryType[ value as string ] )

        case 'instanceIds':
            return new ConditionPlayerInstanceId( ( value as string ).split( ',' ).map( race => parseInt( race, 10 ) ) )

        case 'requireLevelRange':
            let levelRange = ( value as string ).split( '-' ).map( race => parseInt( race, 10 ) )
            if ( levelRange.length !== 2 || !Number.isInteger( levelRange[ 0 ] ) || !Number.isInteger( levelRange[ 1 ] ) ) {
                throw new Error( `Invalid player level range with value=${value}` )
            }

            return new ConditionPlayerLevelRange( Math.min( ...levelRange ), Math.max( ...levelRange ) )

        case 'includedStates':
            return getPlayerState( value as string, true )

        case 'excludedStates':
            return getPlayerState( value as string, false )

        case 'playerClanLeader':
            return new ConditionPlayerIsClanLeader( value as boolean )

        /*
            TODO : figure out what does this condition mean and implement
            - so far there is only one condition with value 22
         */
        case 'uc_restart_point':
            return null

        default:
            throw new Error( `Unknown item condition with name=${name} and value=${value}` )
    }
}