import { FunctionTemplate } from '../../../../gameService/models/stats/functions/FunctionTemplate'
import { AbstractEffect } from '../../../../gameService/models/effects/AbstractEffect'
import _ from 'lodash'
import { StatFunctionName } from '../../../../gameService/enums/StatFunction'
import { Stats } from '../../../../gameService/models/stats/Stats'

let similarFunctionTemplates: Record<string, Record<number, FunctionTemplate>> = {}
let similarFunctionCounter : number = 0

export function createEffectFunctions( definitions: { [ name: string ] : any }, effect: AbstractEffect ) : void {
    _.each( definitions, ( value: any, key: StatFunctionName ) => {
        _.each( _.castArray( value ), ( value: any ) => {
            let { stat, val } = value

            if ( _.isUndefined( stat ) || _.isUndefined( val ) ) {
                throw new Error( 'Unable to create effect function' )
            }

            effect.functions.push( createOrGetFunctionTemplate( key, stat as Stats, val ).generateFunction( effect ) )
        } )
    } )
}

export function createOrGetFunctionTemplate( name: StatFunctionName, stat : Stats, value: number ) : FunctionTemplate {
    if ( !similarFunctionTemplates[ name ] ) {
        similarFunctionTemplates[ name ] = {}
    }

    if ( !similarFunctionTemplates[ name ][ stat ] ) {
        similarFunctionTemplates[ name ][ stat ] = {}
    }

    let existingTemplate = similarFunctionTemplates[ name ][ stat ][ value ]
    if ( existingTemplate ) {
        return existingTemplate
    }

    let newTemplate = new FunctionTemplate( name, stat, value )
    similarFunctionTemplates[ name ][ stat ][ value ] = newTemplate
    similarFunctionCounter++

    return newTemplate
}

export function getFunctionTemplateAmount() : number {
    return similarFunctionCounter
}

export function resetFunctionTemplates() : void {
    similarFunctionTemplates = {}
}