import { Skill } from '../../../../gameService/models/Skill'
import { TraitTypeValues } from '../../../../gameService/models/stats/TraitType'
import { AbnormalType } from '../../../../gameService/models/skills/AbnormalType'
import { ConfigManager } from '../../../../config/ConfigManager'
import { DataManager } from '../../../manager'
import { AbnormalVisualEffect, AbnormalVisualEffectMap, AbnormalVisualType } from '../../../../gameService/models/skills/AbnormalVisualEffect'
import { MountType } from '../../../../gameService/enums/MountType'
import { L2TargetType, L2TargetTypeVerifiers } from '../../../../gameService/models/skills/targets/L2TargetType'
import { AffectSelectionScope } from '../../../../gameService/models/skills/targets/AffectSelectionScope'
import { AttributeType } from '../../../../gameService/models/skills/AttributeType'
import { BaseStats } from '../../../../gameService/models/stats/BaseStats'
import { L2ExtractableProductItem, L2ExtractableSkill } from '../../../../gameService/models/L2ExtractableSkill'
import { Condition } from '../../../../gameService/models/conditions/Condition'
import { ConditionHelper } from './ConditionHelper'
import { EffectRegistry } from '../../../../gameService/effects/EffectRegistry'
import { EffectScope } from '../../../../gameService/models/skills/EffectScope'
import { AffectMethodScope } from '../../../../gameService/models/skills/targets/AffectMethodScope'
import { SkillOperateType, SkillOperateTypeOperations } from '../../../../gameService/models/skills/SkillOperateType'
import { AffectMethodRegistry } from '../../../../gameService/models/skills/targets/AffectMethodRegistry'
import { AffectSelectionMethods } from '../../../../gameService/models/skills/targets/AffectSelectionMethods'
import { ItemData } from '../../../../gameService/interface/ItemData'
import { AbstractEffect } from '../../../../gameService/models/effects/AbstractEffect'
import _, { parseInt } from 'lodash'
import { L2EffectType } from '../../../../gameService/enums/effects/L2EffectType'
import { SkillMagicType } from '../../../../gameService/enums/SkillMagicType'
import { createEffectFunctions } from './FunctionHelper'
import { ServerLog } from '../../../../logger/Logger'

const defaultEmptyParameters = []
const isAOESkill = new Set<L2TargetType>( [
    L2TargetType.AREA,
    L2TargetType.AURA,
    L2TargetType.BEHIND_AREA,
    L2TargetType.BEHIND_AURA,
    L2TargetType.FRONT_AREA,
    L2TargetType.FRONT_AURA
] )

const supportedFunctions : Array<string> = [
    'add',
    'sub',
    'mul',
    'div',
    'set',
    'share',
    'enchant',
    'enchanthp'
]

export class SkillCreator {
    tableValues: any
    rawDataItem: any
    setValues: any

    enchant1SetValues: any
    enchant2SetValues: any
    enchant3SetValues: any
    enchant4SetValues: any
    enchant5SetValues: any
    enchant6SetValues: any
    enchant7SetValues: any
    enchant8SetValues: any

    defaultEffects: any
    enchant1Effects: any
    enchant2Effects: any
    enchant3Effects: any
    enchant4Effects: any
    enchant5Effects: any
    enchant6Effects: any
    enchant7Effects: any
    enchant8Effects: any

    resolvedSetValues: any
    resolvedEffectValues: any

    /*
        There does not seem any difference between levels for capsuled_items_skill property,
        hence values can be cached for all skills (reducing CPU usage and memory footprint).
     */
    extractableItems: Array<L2ExtractableProductItem>

    constructor( rawDataItem: any ) {
        this.rawDataItem = rawDataItem
        this.tableValues = this.getJson( 'table_json' )
        this.setValues = this.getJson( 'set_json' )

        this.setEnchantSetValues()
        this.setEnchantEffectValues()

        this.resolvedSetValues = {}
        this.resolvedEffectValues = {}
    }

    createSingleSkill( level: number ): Skill {
        let item = new Skill()

        item.id = this.rawDataItem.id
        item.level = level + 1
        item.displayId = _.defaultTo( this.processSetValue( 'displayId', level ), item.id )
        item.displayLevel = _.defaultTo( this.processSetValue( 'displayLevel', level ), item.level )

        item.name = _.defaultTo( this.rawDataItem[ 'name' ], '' )
        item.operateType = SkillOperateType[ this.processSetValue( 'operateType', level ) as string ]
        item.magic = _.defaultTo( this.processSetValue( 'isMagic', level ), SkillMagicType.PhysicalCast )
        item.traitType = _.defaultTo( TraitTypeValues[ this.processSetValue( 'trait', level ) as string ], TraitTypeValues.NONE )

        item.reuseDelayLock = _.defaultTo( this.processSetValue( 'reuseDelayLock', level ), false )
        item.startCostMp = _.defaultTo( this.processSetValue( 'mpConsume1', level ), 0 )
        item.successCostMp = _.defaultTo( this.processSetValue( 'mpConsume2', level ), 0 )
        item.mpPerChanneling = _.defaultTo( this.processSetValue( 'mpPerChanneling', level ), item.successCostMp )

        item.hpConsume = _.defaultTo( this.processSetValue( 'hpConsume', level ), 0 )
        item.itemConsumeCount = _.defaultTo( this.processSetValue( 'itemConsumeCount', level ), 0 )
        item.itemConsumeId = _.defaultTo( this.processSetValue( 'itemConsumeId', level ), 0 )
        item.castRange = _.defaultTo( this.processSetValue( 'castRange', level ), -1 )

        item.effectRange = _.defaultTo( this.processSetValue( 'effectRange', level ), -1 )
        item.abnormalLvl = _.defaultTo( this.processSetValue( 'abnormalLvl', level ), 0 )
        item.abnormalType = _.defaultTo( AbnormalType[ this.processSetValue( 'abnormalType', level ) as string ], AbnormalType.NONE )
        item.abnormalTime = this.getAbnormalTime( item.id, level )

        item.isAbnormalInstant = _.defaultTo( this.processSetValue( 'abnormalInstant', level ), false )
        this.populateVisualEffectsMask( item )

        item.stayAfterDeath = _.defaultTo( this.processSetValue( 'stayAfterDeath', level ), false )
        item.stayOnSubclassChange = _.defaultTo( this.processSetValue( 'stayOnSubclassChange', level ), true )

        item.hitTime = _.defaultTo( this.processSetValue( 'hitTime', level ), 0 )
        item.coolTime = _.defaultTo( this.processSetValue( 'coolTime', level ), 0 )
        item.isDebuffValue = _.defaultTo( this.processSetValue( 'isDebuff', level ), false )
        item.isRecoveryHerb = _.defaultTo( this.processSetValue( 'isRecoveryHerb', level ), false )

        item.reuseHashCode = DataManager.getSkillData().getHashCode( item.id, item.level )
        item.reuseDelay = _.defaultTo( this.getReuseDelay( item.id, level ), 0 )
        item.affectRange = _.defaultTo( this.processSetValue( 'affectRange', level ), 0 )
        item.rideState = this.getRideState( level )

        item.affectLimit = _.split( _.defaultTo( this.processSetValue( 'affectLimit', level ), '0-0' ), '-' ).map( value => _.parseInt( value ) )
        item.targetType = _.defaultTo( L2TargetType[ this.processSetValue( 'targetType', level ) as string ], L2TargetType.SELF )
        item.affectScope = _.defaultTo( AffectSelectionScope[ this.processSetValue( 'affectScope', level ) as string ], AffectSelectionScope.SINGLE )
        item.magicLevel = _.defaultTo( this.processSetValue( 'magicLvl', level ), 0 )

        item.lvlBonusRate = _.defaultTo( this.processSetValue( 'lvlBonusRate', level ), 0 )
        item.activateRate = _.defaultTo( this.processSetValue( 'activateRate', level ), -1 )
        item.minChance = _.defaultTo( this.processSetValue( 'minChance', level ), ConfigManager.character.getMinAbnormalStateSuccessRate() )
        item.maxChance = _.defaultTo( this.processSetValue( 'maxChance', level ), ConfigManager.character.getMaxAbnormalStateSuccessRate() )

        item.nextActionIsAttack = _.defaultTo( this.processSetValue( 'nextActionAttack', level ), false )
        item.blockedInOlympiad = _.defaultTo( this.processSetValue( 'blockedInOlympiad', level ), false )
        item.attributeType = _.defaultTo( AttributeType[ this.processSetValue( 'attributeType', level ) as string ], AttributeType.NONE )
        item.attributePower = _.defaultTo( this.processSetValue( 'attributePower', level ), 0 )

        item.basicProperty = _.defaultTo( BaseStats[ this.processSetValue( 'basicProperty', level ) as string ], BaseStats.NONE )
        item.overhit = _.defaultTo( this.processSetValue( 'overHit', level ), false )
        item.isSuicideAttack = _.defaultTo( this.processSetValue( 'isSuicideAttack', level ), false )
        item.minPledgeClass = _.defaultTo( this.processSetValue( 'minPledgeClass', level ), 0 )

        item.chargeConsume = _.defaultTo( this.processSetValue( 'chargeConsume', level ), 0 )
        item.soulMaxConsume = _.defaultTo( this.processSetValue( 'soulMaxConsumeCount', level ), 0 )
        item.directHpDamage = _.defaultTo( this.processSetValue( 'dmgDirectlyToHp', level ), false )
        item.effectPoint = _.defaultTo( this.processSetValue( 'effectPoint', level ), 0 )

        item.irreplaceableBuff = _.defaultTo( this.processSetValue( 'irreplaceableBuff', level ), false )
        item.excludedFromCheck = _.defaultTo( this.processSetValue( 'excludedFromCheck', level ), false )
        item.simultaneousCast = _.defaultTo( this.processSetValue( 'simultaneousCast', level ), false )
        item.extractableItems = this.getExtractableSkill( item.id, level )

        item.icon = _.defaultTo( this.processSetValue( 'icon', level ), 'icon.skill0000' )
        item.channelingSkillId = _.defaultTo( this.processSetValue( 'channelingSkillId', level ), 0 )
        item.channelingTickInterval = _.defaultTo( this.processSetValue( 'channelingTickInterval', level ), 2 ) * 1000
        item.channelingTickInitialDelay = _.defaultTo( this.processSetValue( 'channelingTickInitialDelay', level ), item.channelingTickInterval / 1000 ) * 1000

        item.hitCancelTime = _.defaultTo( this.processSetValue( 'hitCancelTime', level ), 0 )
        item.affectObjectMethod = AffectMethodRegistry[ _.defaultTo( AffectMethodScope[ this.processSetValue( 'affectObject', level ) as string ], AffectMethodScope.ALL ) ]
        item.targetVerifyMethod = L2TargetTypeVerifiers[ item.getTargetType() ]
        item.targetSelectionMethod = AffectSelectionMethods[ item.getAffectScope() ]

        item.fanRange = this.getFanRange( level )
        item.isBadValue = item.effectPoint < 0 && item.targetType !== L2TargetType.SELF && item.targetType != L2TargetType.SUMMON
        item.isChannelingValue = SkillOperateTypeOperations.isChanneling( item.operateType )
        item.isActiveValue = SkillOperateTypeOperations.isActive( item.operateType )

        item.isToggleValue = SkillOperateTypeOperations.isToggle( item.operateType )
        item.isPassiveValue = SkillOperateTypeOperations.isPassive( item.operateType )
        item.isSelfContinuousValue = SkillOperateTypeOperations.isSelfContinuous( item.operateType )
        item.isFlyTypeValue = SkillOperateTypeOperations.isFlyType( item.operateType )

        item.isContinuousValue = SkillOperateTypeOperations.isContinuous( item.operateType )

        this.addEffects( item )
        this.computeFlags( item )

        return item
    }

    getAbnormalTime( skillId: number, level: number ): number {
        let duration = ConfigManager.character.getSkillDuration()[ skillId ]
        let time = _.defaultTo( this.processSetValue( 'abnormalTime', level ), 0 )
        if ( ConfigManager.character.modifySkillDuration() && _.isNumber( duration ) ) {
            if ( level < 100 || level > 140 ) {
                return duration
            }

            return time + duration
        }

        return time
    }

    getAdjustedLevel( level: number ): number {
        return level % 100
    }

    getEffectLevel( level: number ): any {
        let enchantEffect = this.getJson( `enchant${ level }_json` )

        return _.reduce( enchantEffect, ( total: any, value: any, key: string ) => {
            let modifiedKey = _.camelCase( key.replace( `enchant${ level }`, '' ) )
            total[ modifiedKey ] = _.isEmpty( value ) ? {} : value

            return total
        }, {} )
    }

    getEffectValues( level ): any {
        if ( level >= 801 ) {
            return this.enchant8Effects
        }

        if ( level >= 701 ) {
            return this.enchant7Effects
        }

        if ( level >= 601 ) {
            return this.enchant6Effects
        }

        if ( level >= 501 ) {
            return this.enchant5Effects
        }

        if ( level >= 401 ) {
            return this.enchant4Effects
        }

        if ( level >= 301 ) {
            return this.enchant3Effects
        }

        if ( level >= 201 ) {
            return this.enchant2Effects
        }

        if ( level >= 101 ) {
            return this.enchant1Effects
        }

        return this.defaultEffects
    }

    getExtractableSkill( id: number, level: number ): L2ExtractableSkill {
        let contents = this.processSetValue( 'capsuled_items_skill', level )
        if ( !contents ) {
            return null
        }

        if ( !this.extractableItems ) {
            this.extractableItems = _.split( contents, ';' ).map( ( line: string ): L2ExtractableProductItem => {
                let allItems: Array<string> = _.split( line, ',' )
                let dataPairs: Array<number> = _.dropRight( allItems, 1 ).map( value => _.parseInt( value ) )
                let chance: number = parseFloat( _.last( allItems ) )

                if ( dataPairs.length % 2 !== 0 ) {
                    throw new Error( `Bad capsuled items data for skillId = ${ id }` )
                }

                let items: Array<ItemData> = _.reduce( _.chunk( dataPairs, 2 ), ( allItems: Array<ItemData>, currentPair: Array<number> ) => {
                    let [ itemId, amount ] = currentPair

                    if ( itemId > 0 && amount > 0 ) {
                        allItems.push( {
                            itemId,
                            amount,
                        } )
                    }

                    return allItems
                }, [] )

                if ( items.length === 0 ) {
                    ServerLog.error( `SkillCreator: No valid extractable items for skillId = ${ id }` )
                }

                if ( !chance || !_.isNumber( chance ) ) {
                    ServerLog.error( `SkillCreator: Bad extractable items chance for skillId = ${ id }` )
                }

                return {
                    items,
                    chance,
                }
            } )
        }

        return {
            hash: DataManager.getSkillData().getHashCode( id, level ),
            products: this.extractableItems,
        }
    }

    getJson( propertyName: string ) {
        if ( this.rawDataItem[ propertyName ] ) {
            return JSON.parse( this.rawDataItem[ propertyName ] )
        }

        return {}
    }

    getReuseDelay( skillId: number, level: number ): number {
        let defaultReuse = ConfigManager.character.getSkillReuse()[ skillId ]
        if ( ConfigManager.character.modifySkillReuse() && !!defaultReuse ) {
            return defaultReuse
        }

        return this.processSetValue( 'reuseDelay', level )
    }

    getRideState( level: number ): Array<MountType> {
        let outcome = []

        let stateLine = this.processSetValue( 'rideState', level )

        if ( _.isString( stateLine ) ) {
            let states = _.split( stateLine, ';' )

            _.each( states, ( currentState: string ) => {
                outcome.push( MountType[ currentState ] )
            } )
        }

        return outcome
    }

    getSetValues( level ): any {
        if ( level >= 801 ) {
            return this.enchant8SetValues
        }

        if ( level >= 701 ) {
            return this.enchant7SetValues
        }

        if ( level >= 601 ) {
            return this.enchant6SetValues
        }

        if ( level >= 501 ) {
            return this.enchant5SetValues
        }

        if ( level >= 401 ) {
            return this.enchant4SetValues
        }

        if ( level >= 301 ) {
            return this.enchant3SetValues
        }

        if ( level >= 201 ) {
            return this.enchant2SetValues
        }

        if ( level >= 101 ) {
            return this.enchant1SetValues
        }

        return this.setValues
    }

    populateVisualEffectsMask( item: Skill ) : void {
        let effects = this.processSetValue( 'abnormalVisualEffect', item.level )
        if ( effects ) {
            let effectChunks = _.split( effects, ';' )
            _.each( effectChunks, ( currentEffect: string ) => {
                let effectKey = AbnormalVisualEffect[ currentEffect as string ]
                let effectValue = AbnormalVisualEffectMap[ effectKey ]

                if ( !effectValue ) {
                    ServerLog.error( `SkillCreator: Bad skill visual effect, value = ${ currentEffect } for skillId = ${ item.id }` )
                    return
                }

                if ( effectValue.type === AbnormalVisualType.Event ) {
                    item.maskVisualEffectsEvent |= effectValue.mask
                    return
                }

                if ( effectValue.type === AbnormalVisualType.Special ) {
                    item.maskVisualEffectsSpecial |= effectValue.mask
                    return
                }

                item.maskVisualEffectsStandard |= effectValue.mask
            } )
        }
    }

    processSetValue( propertyName: string, level: number = 0 ): any {
        let value = this.getSetValues( level )[ propertyName ]

        if ( _.startsWith( value, '#' ) && this.tableValues[ value ] ) {
            let adjustedLevel = this.getAdjustedLevel( level )
            if ( propertyName === 'capsuled_items_skill' ) {
                return this.tableValues[ value ]
            }

            if ( !this.resolvedSetValues[ propertyName ] ) {
                /*
                    Default values are usually numbers separated by spaces. However, for targetType and others, strings are used.
                 */
                let rawValues: Array<string> = _.split( this.tableValues[ value ], ' ' )
                if ( !_.isNaN( parseInt( rawValues[ 0 ] ) ) ) {
                    this.resolvedSetValues[ propertyName ] = rawValues.map( parseInt )
                } else {
                    this.resolvedSetValues[ propertyName ] = rawValues
                }
            }

            return this.resolvedSetValues[ propertyName ][ adjustedLevel ]
        }

        return value
    }

    setEnchantEffectValues(): void {
        this.defaultEffects = this.getJson( 'defaults_json' )

        this.enchant1Effects = _.defaults( this.getEffectLevel( 1 ), this.defaultEffects )
        this.enchant2Effects = _.defaults( this.getEffectLevel( 2 ), this.enchant1Effects )
        this.enchant3Effects = _.defaults( this.getEffectLevel( 3 ), this.enchant2Effects )
        this.enchant4Effects = _.defaults( this.getEffectLevel( 4 ), this.enchant3Effects )
        this.enchant5Effects = _.defaults( this.getEffectLevel( 5 ), this.enchant4Effects )
        this.enchant6Effects = _.defaults( this.getEffectLevel( 6 ), this.enchant5Effects )
        this.enchant7Effects = _.defaults( this.getEffectLevel( 7 ), this.enchant6Effects )
        this.enchant8Effects = _.defaults( this.getEffectLevel( 8 ), this.enchant7Effects )
    }

    setEnchantSetValues(): void {
        let enchantProperties: any = this.getJson( 'enchantProperties_json' )

        this.enchant1SetValues = _.defaults( _.defaultTo( enchantProperties.enchant1, {} ), this.setValues )
        this.enchant2SetValues = _.defaults( _.defaultTo( enchantProperties.enchant2, {} ), this.setValues )
        this.enchant3SetValues = _.defaults( _.defaultTo( enchantProperties.enchant3, {} ), this.setValues )
        this.enchant4SetValues = _.defaults( _.defaultTo( enchantProperties.enchant4, {} ), this.setValues )
        this.enchant5SetValues = _.defaults( _.defaultTo( enchantProperties.enchant5, {} ), this.setValues )
        this.enchant6SetValues = _.defaults( _.defaultTo( enchantProperties.enchant6, {} ), this.setValues )
        this.enchant7SetValues = _.defaults( _.defaultTo( enchantProperties.enchant7, {} ), this.setValues )
        this.enchant8SetValues = _.defaults( _.defaultTo( enchantProperties.enchant8, {} ), this.setValues )
    }

    addEffects( item: Skill ) {
        let enchantLevel = item.level > 100 ? Math.floor( item.level / 100 ) : 0
        if ( enchantLevel > 0 ) {
            if ( !this.addConditionEffect( 'cond', item.level, item ) ) {
                this.addConditionEffect( 'cond', this.rawDataItem.lastLevel, item )
            }
        } else {
            this.addConditionEffect( 'cond', item.level, item )
        }

        this.addSpecificEffectsLevel( enchantLevel, `enchant${enchantLevel}Effects.effect`, 'effects.effect', item, null )
        this.addSpecificEffectsLevel( enchantLevel, `enchant${enchantLevel}startEffects.effect`, 'startEffects.effect', item, EffectScope.START )
        this.addSpecificEffectsLevel( enchantLevel, `enchant${enchantLevel}channelingEffects.effect`, 'channelingEffects.effect', item, EffectScope.CHANNELING )
        this.addSpecificEffectsLevel( enchantLevel, `enchant${enchantLevel}pveEffects.effect`, 'pveEffects.effect', item, EffectScope.PVE )
        this.addSpecificEffectsLevel( enchantLevel, `enchant${enchantLevel}pvpEffects.effect`, 'pvpEffects.effect', item, EffectScope.PVP )
        this.addSpecificEffectsLevel( enchantLevel, `enchant${enchantLevel}endEffects.effect`, 'endEffects.effect', item, EffectScope.END )
        this.addSpecificEffectsLevel( enchantLevel, `enchant${enchantLevel}selfEffects.effect`, 'selfEffects.effect', item, EffectScope.SELF )
    }

    addSpecificEffectsLevel( enchantLevel: number, enchantProperty: string, defaultProperty: string, item: Skill, effectScope: EffectScope ) :void {
        if ( enchantLevel === 0 ) {
            this.addSpecificEffects( defaultProperty, item.level, item, effectScope )
            return
        }

        if ( !this.addSpecificEffects( enchantProperty, item.level, item, effectScope ) ) {
            this.addSpecificEffects( defaultProperty, this.rawDataItem.lastLevel, item, effectScope )
        }
    }

    addConditionEffect( propertyName: string, level: number, item: Skill ) : boolean {
        let effectValues = this.getEffectValues( level )

        if ( !_.has( effectValues, propertyName ) ) {
            return false
        }

        let getValueMethod: Function = this.getEffectParameter.bind( this, level )
        _.castArray( effectValues.cond ).forEach( ( attribute: any ) => {
            _.each( attribute, ( value: any, key: string ) => {
                if ( !_.isObjectLike( value ) ) {
                    return
                }

                let condition: Condition = ConditionHelper.createCondition( key, value, getValueMethod )
                if ( condition ) {
                    let { msg, msgId, addName } = attribute
                    if ( msg ) {
                        condition.message = msg
                    }

                    if ( msgId ) {
                        condition.messageId = msgId
                        condition.addMessageName = addName && msgId > 0
                    }

                    item.conditions.push( condition )
                }
            } )
        } )

        return true
    }

    addSpecificEffects( propertyName: string, level: number, item: Skill, effectScope: EffectScope ): boolean {
        let effectValues = this.getEffectValues( level )
        let creator = this

        if ( !_.has( effectValues, propertyName ) ) {
            return false
        }

        _.castArray( _.get( effectValues, propertyName ) ).forEach( ( attribute: any ) => {
            let { name, param } = attribute

            let parameters = !param ? defaultEmptyParameters : creator.getEffectParameters( level, _.castArray( param ) )
            let effect: AbstractEffect = EffectRegistry.createEffect( name, parameters )

            if ( !effect ) {
                return
            }

            let rawDefinitions = _.pick( attribute, supportedFunctions )
            let functionDefinitions: { [ name: string ]: any } = creator.getEffectDefinitions( level, rawDefinitions )

            createEffectFunctions( functionDefinitions, effect )

            if ( effectScope ) {
                item.addEffect( effectScope, effect )
                return
            }

            if ( item.isPassive() ) {
                item.addEffect( EffectScope.PASSIVE, effect )
                return
            }

            item.addEffect( EffectScope.GENERAL, effect )
        } )

        return true
    }

    getEffectParameters( level: number, parameters: Array<any> ): any {
        let normalizedParameters = _.assign( {}, ...parameters )
        return _.reduce( normalizedParameters, ( adjustedParameters: any, value: any, key: string ) => {
            let adjustedValue = value
            if ( _.startsWith( value, '#' ) && this.tableValues[ value ] ) {
                let rawTableValue = this.processEffectValue( value, level )
                let tableValue : number | string = parseFloat( rawTableValue )

                if ( Number.isNaN( tableValue ) ) {
                    tableValue = rawTableValue
                } else {
                    if ( !rawTableValue.includes( '.' ) ) {
                        tableValue = parseInt( rawTableValue )
                    }
                }

                adjustedValue = tableValue
            }

            adjustedParameters[ key ] = adjustedValue
            return adjustedParameters
        }, {} )
    }

    getEffectDefinitions( level: number, definitions: Object ): any {
        return _.reduce( definitions, ( adjustedDefinitions: any, value: any, key: string ) => {
            let adjustedValue = this.processEffectDefinition( value, level )

            if ( _.isUndefined( adjustedValue ) ) {
                throw new Error( `Skill id ${this.rawDataItem.id}, level ${ level } has property ${ value } as undefined` )
            }

            adjustedDefinitions[ key ] = adjustedValue
            return adjustedDefinitions
        }, {} )
    }

    processEffectDefinition( value: any, level: number ) : any {
        if ( _.isString( value ) && value.startsWith( '#' ) && this.tableValues[ value ] ) {
            let currentValue = this.processEffectValue( value, level )
            let proposedValue = parseFloat( currentValue )

            return _.isNaN( proposedValue ) ? currentValue : proposedValue
        }

        if ( _.isArray( value ) ) {
            return value.map( currentValue => this.getEffectDefinitions( level, currentValue ) )
        }

        if ( _.isObject( value ) ) {
            return this.getEffectDefinitions( level, value )
        }

        return value
    }

    processEffectValue( propertyName: string, level: number ): string {
        if ( !this.resolvedEffectValues[ propertyName ] ) {
            this.resolvedEffectValues[ propertyName ] = _.split( this.tableValues[ propertyName ], ' ' )
        }

        return this.resolvedEffectValues[ propertyName ][ this.getAdjustedLevel( level ) - 1 ]
    }

    getEffectParameter( level: number, value: any ): any {
        if ( _.startsWith( value, '#' ) && this.tableValues[ value ] ) {
            return this.processEffectValue( value, level )
        }

        return value
    }

    computeFlags( skill: Skill ) : void {
        skill.isDamageValue = this.hasSomeEffectTypes( skill, L2EffectType.MagicalAttack, L2EffectType.HpDrain, L2EffectType.PhysicalAttack )
        skill.isRemovedOnAnyActionExceptMoveValue = skill.abnormalType === AbnormalType.INVINCIBILITY || skill.abnormalType === AbnormalType.HIDE
        skill.isRemovedOnDamageValue = skill.abnormalType === AbnormalType.SLEEP
                || skill.abnormalType === AbnormalType.FORCE_MEDITATION
                || skill.abnormalType === AbnormalType.HIDE

        skill.isAOEValue = isAOESkill.has( skill.targetType )
        skill.is7SignsValue = skill.id > 4360 && skill.id < 4367
        skill.isFishingValue = this.hasSomeEffectTypes( skill, L2EffectType.Fishing, L2EffectType.FishingStart )
        skill.isDisablerValue = this.hasSomeEffectTypes( skill, L2EffectType.Stun, L2EffectType.Root, L2EffectType.Aggression )
        skill.hasPointRecoveryValue = this.hasSomeEffectTypes( skill, L2EffectType.Cp, L2EffectType.Hp, L2EffectType.MpChangeByLevel, L2EffectType.MpChangeByPercent )
        skill.needsVisibleTargetValue = ( skill.getTargetType() !== L2TargetType.PARTY ) || !skill.hasEffectType( L2EffectType.Hp )
    }

    getFanRange( level: number ) : [ number, number, number, number ] {
        let value = this.processSetValue( 'fanRange', level )
        if ( value ) {
            return value.split( ',' ).map( range => parseInt( range, 10 ) )
        }

        return null
    }

    hasSomeEffectTypes( skill: Skill, ...allTypes: Array<L2EffectType> ): boolean {
        return allTypes.some( ( currentType: L2EffectType ) => {
            return skill.effectTypes.has( currentType )
        } )
    }
}