import { L2PlayerCreationPointData, PlayerCreationPointDataApi } from '../../interface/PlayerCreationPointDataApi'
import { SQLiteDataEngine } from '../../engines/sqlite'
import logSymbols from 'log-symbols'
import _ from 'lodash'

let pointData : { [ key: number ]: Array<L2PlayerCreationPointData> } = {}

const query = 'select * from player_creation_points'

export const SQLitePlayerCreationPointData : PlayerCreationPointDataApi = {
    getCreationPoint( classId: number ): L2PlayerCreationPointData {
        return _.sample( pointData[ classId ] )
    },

    async load() {
        pointData = {}

        let databaseItems: Array<any> = SQLiteDataEngine.getMany( query )

        let count = 0
        _.each( databaseItems, ( databaseItem: any ) => {
            let { classId, x, y, z } = databaseItem

            if ( !pointData[ classId ] ) {
                pointData[ classId ] = []
            }

            let data : L2PlayerCreationPointData = {
                classId,
                x,
                y,
                z
            }

            pointData[ classId ].push( data )
            count++
        } )

        return [
                `PlayerCreationData loaded ${count} points for ${_.size( pointData )} classes.`
        ]
    }
}