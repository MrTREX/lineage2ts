import { L2ClassListDataApi, L2ClassListDataItem } from '../../interface/ClassListDataApi'
import { SQLiteDataEngine } from '../../engines/sqlite'
import logSymbols from 'log-symbols'
import _ from 'lodash'


let allData: { [ key: number ]: L2ClassListDataItem }

const query = 'select * from class_list'

export const SQLiteClassListData: L2ClassListDataApi = {
    getAll(): { [ classId: number ]: L2ClassListDataItem } {
        return allData
    },

    getClassInfo( classId: number ): L2ClassListDataItem {
        return allData[ classId ]
    },

    async load(): Promise<Array<string>> {
        allData = {}

        let databaseItems: Array<any> = SQLiteDataEngine.getMany( query )

        _.each( databaseItems, ( databaseItem: any ) => {
            let { classId, name, parentClassId } = databaseItem

            allData[ classId ] = {
                classId,
                name,
                parentClassId
            }
        } )

        return [
            `ClassList: loaded ${ _.size( allData ) } data items`,
        ]
    },
}