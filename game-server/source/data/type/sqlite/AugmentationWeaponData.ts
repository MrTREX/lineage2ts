import { L2AugmentationWeaponsDataApi } from '../../interface/AugmentationWeaponsDataApi'
import { AugmentationChance } from '../../../gameService/models/augmentation/AugmentationChance'
import { SQLiteDataEngine } from '../../engines/sqlite'
import _ from 'lodash'

export const SQLiteAugmentationWeaponData : L2AugmentationWeaponsDataApi = {
    getAll(): Array<AugmentationChance> {
        const query = 'select * from augmentation_weapons'
        let databaseItems = SQLiteDataEngine.getMany( query )
        return _.flatMap( databaseItems, ( databaseItem: any ) : Array<AugmentationChance> => {

            let augmentationData : Array<any> = JSON.parse( databaseItem[ 'chance_json' ] )

            return _.map( augmentationData, ( augmentationItem: any ) : AugmentationChance => {
                return {
                    augmentChance: augmentationItem[ 'chance' ],
                    augmentId: augmentationItem[ 'id' ],
                    categoryChance: databaseItem[ 'categoryProbability' ],
                    stoneId: databaseItem[ 'stoneId' ],
                    type: databaseItem[ 'classType' ],
                    variationId: databaseItem[ 'variationId' ]
                }
            } )
        } )
    }
}