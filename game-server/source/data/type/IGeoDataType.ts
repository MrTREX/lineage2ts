import { L2GeoRegionDataApi } from '../interface/GeoRegionDataApi'

export interface IGeoDataType {
    region: L2GeoRegionDataApi
}