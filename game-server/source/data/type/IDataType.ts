import { AgathionDataApi } from '../interface/AgathionDataApi'
import { ArmorSetsDataApi } from '../interface/ArmorSetsDataApi'
import { BaseStatsDataApi } from '../interface/BaseStatsDataApi'
import { CategoryDataApi } from '../interface/CategoryDataApi'
import { CursedWeaponDataApi } from '../interface/CursedWeaponDataApi'
import { EnchantItemHPBonusDataApi } from '../interface/EnchantItemHPBonusDataApi'
import { EnchantItemOptionsDataApi } from '../interface/EnchantItemOptionsDataApi'
import { ExperienceDataApi } from '../interface/ExperienceDataApi'
import { HtmlDataApi } from '../interface/HtmlDataApi'
import { InstanceDataApi } from '../interface/InstanceDataApi'
import { ItemDataApi } from '../interface/ItemDataApi'
import { KarmaDataApi } from '../interface/KarmaDataApi'
import { NpcDataApi } from '../interface/NpcDataApi'
import { OptionDataApi } from '../interface/OptionDataApi'
import { PetDataApi } from '../interface/PetDataApi'
import { PlayerTemplateData } from '../interface/PlayerTemplateData'
import { SkillDataApi } from '../interface/SkillDataApi'
import { SkillTreesDataApi } from '../interface/SkillTreesDataApi'
import { TransformDataApi } from '../interface/TransformDataApi'
import { UIDataApi } from '../interface/UIDataApi'
import { RecipeDataApi } from '../interface/RecipeDataApi'
import { InitialShortcutDataApi } from '../interface/InitialShortcutDataApi'
import { InitialEquipmentDataApi } from '../interface/InitialEquipmentDataApi'
import { PlayerCreationPointDataApi } from '../interface/PlayerCreationPointDataApi'
import { DoorDataApi } from '../interface/DoorDataApi'
import { FishDataApi } from '../interface/FishDataApi'
import { L2FishingMonsterDataApi } from '../interface/FishingMonsterDataApi'
import { FishingRodsDataApi } from '../interface/FishingRodsDataApi'
import { SkillLearnDataApi } from '../interface/SkillLearnDataApi'
import { L2StaticObjectsDataApi } from '../interface/StaticObjectsDataApi'
import { L2XPLostDataApi } from '../interface/XPLostDataApi'
import { L2HennaDataApi } from '../interface/HennaDataApi'
import { L2BuylistDataApi } from '../interface/BuylistDataApi'
import { L2MerchantPriceDataApi } from '../interface/MerchantPriceDataApi'
import { L2EnchantItemGroupsDataApi } from '../interface/EnchantItemGroupsDataApi'
import { L2EnchantItemDataApi } from '../interface/EnchantItemDataApi'
import { L2MultisellDataApi } from '../interface/MultisellDataApi'
import { L2HitConditionBonusDataApi } from '../interface/HitConditionBonusDataApi'
import { L2ClassListDataApi } from '../interface/ClassListDataApi'
import { L2ItemAuctionDataApi } from '../interface/ItemAuctionDataApi'
import { L2AugmentationAccessoryDataApi } from '../interface/AugmentationAccessoryDataApi'
import { L2AugmentationWeaponsDataApi } from '../interface/AugmentationWeaponsDataApi'
import { L2AugmentationSkillsDataApi } from '../interface/AugmentationSkillsDataApi'
import { L2ManorSeedsDataApi } from '../interface/ManorSeedsDataApi'
import { L2AuctionTemplateDataApi } from '../interface/AuctionTemplateDataApi'
import { L2SoulCrystalDataApi } from '../interface/SouldCrystalDataApi'
import { L2QuestDataApi } from '../interface/QuestDataApi'
import { L2PlayerLevelupRewardsDataApi } from '../interface/PlayerLevelupRewardsDataApi'
import { L2TeleportsDataApi } from '../interface/TeleportsDataApi'
import { L2GeometryDataApi } from '../interface/GeometryDataApi'
import { L2SpawnTerritoriesDataApi } from '../interface/SpawnTerritoriesDataApi'
import { L2SpawnLogicDataApi } from '../interface/SpawnLogicDataApi'
import { L2SpawnNpcDataApi } from '../interface/SpawnNpcDataApi'
import { L2NpcRoutesDataApi } from '../interface/NpcRoutesDataApi'
import { L2CycleStepDataApi } from '../interface/CycleStepDataApi'
import { L2DataOperationsApi } from '../interface/OperationsApi'
import { L2ServitorSkillsDataApi } from '../interface/ServitorSkillsDataApi'
import { L2CubicDataApi } from '../interface/CubicDataApi'
import { L2AreaDataApi } from '../interface/AreaDataApi'
import { L2RespawnPointsDataApi } from '../interface/RespawnPointsDataApi'
import { L2PlayerAccessDataApi } from '../interface/PlayerAccessDataApi'
import { L2GamePointProductsDataApi } from '../interface/GamePointsProductsDataApi'

export interface IDataType {
    loadOrder: Array<Function>
    operations: L2DataOperationsApi

    /*
        Data api items
     */
    playerAccess: L2PlayerAccessDataApi
    agathionData: AgathionDataApi
    armorSetsData: ArmorSetsDataApi
    baseStatsData: BaseStatsDataApi
    categoryData: CategoryDataApi
    cursedWeaponData: CursedWeaponDataApi
    enchantItemHPBonusData: EnchantItemHPBonusDataApi
    enchantItemOptionsData: EnchantItemOptionsDataApi
    experienceData: ExperienceDataApi
    htmlData: HtmlDataApi
    instanceData: InstanceDataApi
    itemData: ItemDataApi
    karmaData: KarmaDataApi
    npcData: NpcDataApi
    optionData: OptionDataApi
    petData: PetDataApi
    playerTemplateData: PlayerTemplateData
    skillData: SkillDataApi
    skillTreesData: SkillTreesDataApi
    transformData: TransformDataApi
    uiData: UIDataApi
    recipeData: RecipeDataApi
    initialShortcutData: InitialShortcutDataApi
    initialEquipmentData: InitialEquipmentDataApi
    playerCreationPointData: PlayerCreationPointDataApi
    doorData: DoorDataApi
    fishData: FishDataApi
    fishingMonsterData: L2FishingMonsterDataApi
    fishingRodsData: FishingRodsDataApi
    skillLearnData: SkillLearnDataApi
    staticObjects: L2StaticObjectsDataApi
    xpLostData: L2XPLostDataApi
    hennaData: L2HennaDataApi
    buylistData: L2BuylistDataApi
    merchantPriceData: L2MerchantPriceDataApi
    enchantItemGroups: L2EnchantItemGroupsDataApi
    enchantItemData: L2EnchantItemDataApi
    multisellData: L2MultisellDataApi
    hitConditionBonus: L2HitConditionBonusDataApi
    npcRoutes: L2NpcRoutesDataApi
    classListData: L2ClassListDataApi
    itemAuctionData: L2ItemAuctionDataApi
    augmentationAccessory: L2AugmentationAccessoryDataApi
    augmentationWeapons: L2AugmentationWeaponsDataApi
    augmentationSkills: L2AugmentationSkillsDataApi
    manorSeeds: L2ManorSeedsDataApi
    auctionTemplates: L2AuctionTemplateDataApi
    soulCrystalData : L2SoulCrystalDataApi
    questData : L2QuestDataApi
    playerLevelupRewards: L2PlayerLevelupRewardsDataApi
    teleports: L2TeleportsDataApi
    geometry: L2GeometryDataApi
    spawnTerritories: L2SpawnTerritoriesDataApi
    spawnLogic: L2SpawnLogicDataApi
    spawnNpcData: L2SpawnNpcDataApi
    cycleStepData: L2CycleStepDataApi
    servitorSkills: L2ServitorSkillsDataApi
    cubics: L2CubicDataApi
    areas: L2AreaDataApi
    respawnPoints: L2RespawnPointsDataApi
    gamePointProducts: L2GamePointProductsDataApi
}