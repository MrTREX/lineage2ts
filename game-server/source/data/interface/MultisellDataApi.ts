import { L2DataApi } from './l2DataApi'
import { ListContainer } from '../../gameService/models/multisell/ListContainer'

export interface L2MultisellDataApi extends L2DataApi{
    getEntries() : { [ key: number ] : ListContainer }
    getEntry( listId: number ): ListContainer
}