import { L2DataApi } from './l2DataApi'
import { L2Item } from '../../gameService/models/items/L2Item'
import { EnchantItemGroup } from '../../gameService/models/items/enchant/EnchantItemGroup'
import { EnchantScrollGroup } from '../../gameService/models/items/enchant/EnchantScrollGroup'

export interface L2EnchantItemGroupsDataApi extends L2DataApi{
    getScrollGroup( id: number ) : EnchantScrollGroup
    getItemGroup( name: string ) : EnchantItemGroup
    getItemGroupByItem( item: L2Item, scrollGroup: number ) : EnchantItemGroup
}