import { L2DataApi } from './l2DataApi'
import { PlayerEquipmentItem } from '../../gameService/interface/ItemDefinition'

export interface InitialEquipmentDataApi extends L2DataApi {
    getEquipmentList( classId: number ) : Array<PlayerEquipmentItem>
}