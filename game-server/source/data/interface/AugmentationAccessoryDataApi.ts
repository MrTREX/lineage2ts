import { AugmentationChance } from '../../gameService/models/augmentation/AugmentationChance'

export interface L2AugmentationAccessoryDataApi {
    getAll() : Array<AugmentationChance>
}