import { L2DataApi } from './l2DataApi'
import { Options } from '../../gameService/models/options/Options'

export interface OptionDataApi extends L2DataApi{
    getOptions( id: number ) : Options
}