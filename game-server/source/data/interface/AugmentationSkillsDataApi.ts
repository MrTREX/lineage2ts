export interface L2AugmentationSkillsDataApi {
    getAll(): Array<L2AugmentationSkillsDataItem>
}

export interface L2AugmentationSkillsDataItem {
    augmentationId: number
    skillId: number
    skillLevel: number
    type: string
}