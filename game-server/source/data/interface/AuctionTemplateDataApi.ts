import { L2DataApi } from './l2DataApi'
import { L2ClanHallAuctionItem } from '../../database/interface/ClanHallAuctionsTableApi'

export interface L2AuctionTemplateDataApi extends L2DataApi {
    getClanHall( id: number ): L2ClanHallAuctionItem
}

export const enum L2AuctionTemplateTypes {
    ClanHall = 'ClanHall'
}