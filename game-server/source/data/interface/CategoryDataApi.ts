import { L2DataApi } from './l2DataApi'
import { CategoryType } from '../../gameService/enums/CategoryType'

export interface CategoryDataApi extends L2DataApi {
    isInCategory( type: CategoryType, id: number ) : boolean
    getCategoryByType( type: CategoryType ) : Array<number>
}