import { L2DataApi } from './l2DataApi'
import { ProductCategory } from '../../gameService/enums/productInfo/ProductCategory'
import { ProductType } from '../../gameService/enums/productInfo/ProductType'

export interface L2GamePointProductsDataApi extends L2DataApi {
    getProduct( id : number ) : L2GamePointProduct
    getAll() : ReadonlyArray<L2GamePointProduct>
}

export interface L2GamePointProduct {
    id: number
    category: ProductCategory
    type: ProductType
    price: number
    items: ReadonlyArray<L2GamePointProductItem>
}

export interface L2GamePointProductItem {
    id: number
    amount: number
}