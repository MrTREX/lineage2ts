import { LocationXY } from '../../gameService/models/LocationProperties'
import { Race } from '../../gameService/enums/Race'
import { L2DataApi } from './l2DataApi'
import { Location } from '../../gameService/models/Location'

export interface L2RespawnPointsDataApi extends L2DataApi {
    getByName( name: string ) : L2RespawnPointData
    getAll() : Array<L2RespawnPointData>
}

export interface L2RespawnPointData {
    name: string
    normalPoints: Array<Location>
    pvpPoints: Array<Location>
    bannedRace: L2RespawnBannedRace
    bbs: number
    messageId: number
    tiles: Array<LocationXY>
}

export interface L2RespawnBannedRace {
    race: Race
    location: string
}