export interface L2ServitorSkillsDataApi {
    getAll() : Array<L2ServitorSkillItem>
}

export interface L2ServitorSkillItem {
    npcId: number
    skillId: number
    skillLevel: number
}