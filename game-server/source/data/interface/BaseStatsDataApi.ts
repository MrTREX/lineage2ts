import { L2DataApi } from './l2DataApi'

export interface BaseStatsDataApi extends L2DataApi {
    getSTR() : Array<number>
    getINT() : Array<number>
    getDEX() : Array<number>
    getWIT() : Array<number>
    getCON() : Array<number>
    getMEN() : Array<number>
}