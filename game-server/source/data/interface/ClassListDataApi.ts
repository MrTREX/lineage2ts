import { L2DataApi } from './l2DataApi'

export interface L2ClassListDataApi extends L2DataApi {
    getClassInfo( classId : number ) : L2ClassListDataItem
    getAll() : { [ classId: number ] : L2ClassListDataItem }
}

export interface L2ClassListDataItem {
    classId: number
    name: string
    parentClassId: number
}