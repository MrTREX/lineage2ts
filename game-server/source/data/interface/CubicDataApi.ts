import { L2DataApi } from './l2DataApi'
import { SkillItem } from '../../gameService/models/skills/SkillItem'

export interface L2CubicDataApi extends L2DataApi{
    getItem( id: number, level: number ) : L2CubicDataItem
    getAllById( id: number ) : Array<L2CubicDataItem>
    getAll() : Array<L2CubicDataItem>
}

export interface L2CubicDataSkill extends SkillItem {
    activationChance: number
    staticTarget: boolean
    useChance: number
}

export interface L2CubicDataCondition {
    applyBelow: boolean
    applyHp: number
    chance: number
}

export enum L2CubicDataTargetType {
    PlayerTarget,
    HealSelection,
    SkillTarget
}

export interface L2CubicDataTarget {
    type: L2CubicDataTargetType
}

export interface L2CubicDataHealTarget extends L2CubicDataTarget {
    hpThreshold: number
    skillThresholds: Array<number>
}

export interface L2CubicDataItem {
    id: number
    level: number
    delay: number
    maxActions: number
    power: number
    target: L2CubicDataTarget
    condition: L2CubicDataCondition
    skills: Array<L2CubicDataSkill>
}