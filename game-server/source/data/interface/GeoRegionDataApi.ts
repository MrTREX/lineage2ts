import { L2DataOperationsApi } from './OperationsApi'

export interface L2GeoRegionDataApi extends L2DataOperationsApi {
    getRegionData( regionX : number, regionY: number ) : Buffer
    initialize() : L2GeoRegionDataStats
    deInitialize() : void
}

export interface L2GeoRegionDataStats {
    regions: number
}