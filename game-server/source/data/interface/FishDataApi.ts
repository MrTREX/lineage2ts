import { L2DataApi } from './l2DataApi'
import { FishGroup } from '../../gameService/enums/FishGroup'
import { FishGrade } from '../../gameService/enums/FishGrade'

export interface FishDataApi extends L2DataApi {
    getFish( level: number, group: FishGroup, grade: FishGrade ): FishDataItem
}

export interface FishDataItem {
    id: number
    itemId: number
    itemName: string
    groupId: FishGroup
    level: number
    biteRate: number
    hp: number
    maxLength: number
    lengthRate: number
    hpRegeneration: number
    startCombatTime: number
    combatDuration: number
    gutsCheckTime: number
    gutsCheckProbability: number
    cheatingProbability: number
    grade: FishGrade
}