import { L2DataApi } from './l2DataApi'
import { CrystalType } from '../../gameService/models/items/type/CrystalType'

export interface EnchantItemHPBonusDataApi extends L2DataApi{
    getHPBonus( grade: CrystalType ) : Array<number>
}