import { L2DataApi } from './l2DataApi'
import { EnchantOptions } from '../../gameService/models/options/EnchantOptions'

export interface EnchantItemOptionsDataApi extends L2DataApi {
    getOptions( itemId: number, enchantLevel : number ) : EnchantOptions
}