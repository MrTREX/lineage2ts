import { RoaringBitmap32 } from 'roaring'

export interface L2SpawnTerritoriesDataApi {
    getAll() : Record<string, L2SpawnTerritoryItem>
}

export type L2SpawnTerritoryPoint = [ number, number ]
export interface L2SpawnTerritoryGeometryParameters {
    distance: number
    maxX: number
    maxY: number
    minX: number
    minY: number
    size: number
    centerX: number
    centerY: number
}

export interface L2SpawnTerritoryItem {
    id: number
    name: string
    points: Array<L2SpawnTerritoryPoint>
    minZ: number
    maxZ: number
    geometryPoints: RoaringBitmap32 | null
    parameters: L2SpawnTerritoryGeometryParameters | null
    regionX: number
    regionY: number
}