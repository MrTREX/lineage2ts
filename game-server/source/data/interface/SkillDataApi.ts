import { L2DataApi } from './l2DataApi'
import { Skill } from '../../gameService/models/Skill'
import { L2EnchantSkillLearn } from '../../gameService/models/L2EnchantSkillLearn'
import { L2EnchantSkillGroup } from '../../gameService/models/L2EnchantSkillGroup'

export interface SkillDataApi extends L2DataApi{
    getHashCode( id: number, level: number ) : number
    getAllHashedSkills() : { [ key: number ]: Skill }
    getSkillsAtMaxLevel() : { [ key: number ] : number }
    getEnchantableSkillIds() : Set<number>
    getSkillEnchantmentBySkillId( id: number ) : L2EnchantSkillLearn
    getEnchantSkillGroupById( id: number ) : L2EnchantSkillGroup
    getIdsByPartialName( name: string ) : Array<number>
}