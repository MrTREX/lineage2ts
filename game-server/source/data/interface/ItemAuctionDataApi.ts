export interface L2ItemAuctionDataApi {
    getAll() : Promise<Array<L2ItemAuctionDataItem>>
}

export interface L2ItemAuctionDataItem {
    npcId: number
    auctionId: number
    itemId: number
    amount: number
    initialBid: number
    durationMinutes: number
}