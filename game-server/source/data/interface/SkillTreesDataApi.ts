import { L2DataApi } from './l2DataApi'
import { L2SkillLearn } from '../../gameService/models/L2SkillLearn'

export interface SkillTreesDataApi extends L2DataApi {
    getClassSkillTrees(): { [key: number]: { [key: number]: L2SkillLearn } }

    getCollectSkillTree(): { [key: number]: L2SkillLearn }

    getCommonSkillTree(): { [key: number]: L2SkillLearn }

    getFishingSkillTree(): { [key: number]: L2SkillLearn }

    getGameMasterAuraSkillTree(): { [key: number]: L2SkillLearn }

    getGameMasterSkillTree(): { [key: number]: L2SkillLearn }

    getHeroSkillTree(): { [key: number]: L2SkillLearn }

    getNobleSkillTree(): { [key: number]: L2SkillLearn }

    getParentClassMap(): { [key: number]: number }

    getPledgeSkillTree(): { [key: number]: L2SkillLearn }

    getSubClassSkillTree(): { [key: number]: L2SkillLearn }

    getSubPledgeSkillTree(): { [key: number]: L2SkillLearn }

    getTransferSkillTrees(): { [key: number]: { [key: number]: L2SkillLearn } }

    getTransformSkillTree(): { [key: number]: L2SkillLearn }
}