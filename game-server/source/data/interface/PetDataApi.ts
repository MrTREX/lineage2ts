import { L2DataApi } from './l2DataApi'
import { L2PetData } from '../../gameService/models/L2PetData'
import { L2PetLevelData } from '../../gameService/models/L2PetLevelData'

export interface PetDataApi extends L2DataApi{
    getPetDataByItemId( itemId: number ) : L2PetData
    getPetLevelData( npcId: number, level: number ) : L2PetLevelData
    getPetDataByNpcId( npcId: number ) : L2PetData
    getPetMinLevel( npcId: number ) : number
    getPetItems() : PetDataMap
}

export type PetDataMap = { [id: number]: L2PetData }