import { L2DataApi } from './l2DataApi'

export interface AgathionDataApi extends L2DataApi {
    getByNpcId( npcId: number ) : L2AgathionDataItem
    getByItemId( itemId: number ) : L2AgathionDataItem
}

export interface L2AgathionDataItem {
    npcId : number
    itemId: number
    energy: number
}