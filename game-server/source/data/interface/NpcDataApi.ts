import { L2DataApi } from './l2DataApi'
import { L2NpcTemplate } from '../../gameService/models/actor/templates/L2NpcTemplate'
import { NpcTemplateType } from '../../gameService/enums/NpcTemplateType'

export interface NpcDataApi extends L2DataApi {
    getTemplate( id: number ) : L2NpcTemplate
    getClanId( name: string ): number
    getTemplates() : { [key: number]: L2NpcTemplate }
    getTemplatesByType( type: NpcTemplateType ) : Array<L2NpcTemplate>
    getClanName( id: number ) : string
    getTemplateIdsByName( name: string, ordering?: L2NpcDataTemplateOrdering ) : Array<number>
    getTemplateIdsByLevel( levelMin: number, levelMax: number, ordering?: L2NpcDataTemplateOrdering ) : Array<number>
    getIdByName( name: string ) : number
}

export const enum L2NpcDataTemplateOrdering {
    Id = 'id',
    Name = 'name'
}