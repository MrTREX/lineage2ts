import { AugmentationChance } from '../../gameService/models/augmentation/AugmentationChance'

export interface L2AugmentationWeaponsDataApi {
    getAll() : Array<AugmentationChance>
}