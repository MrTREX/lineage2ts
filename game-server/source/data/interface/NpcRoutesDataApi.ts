import { L2DataApi } from './l2DataApi'

export interface L2NpcRoutesDataApi extends L2DataApi {
    getRouteByName( name: string ) : L2NpcRouteItem
    getAllRoutes() : Array<L2NpcRouteItem>
}

export interface L2NpcRoutePoint {
    x: number
    y: number
    z: number
}

export const enum L2NpcRouteType {
    Follow = 'follow',
    RandomPoint = 'random',
    TeleportBack = 'teleport',
    Restart = 'restart'
}

export interface L2NpcRouteItem {
    name: string
    type: L2NpcRouteType
    points: Array<L2NpcRoutePoint>
}