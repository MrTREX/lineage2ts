import { L2DataApi } from './l2DataApi'
import { MerchantPriceConfig } from '../../gameService/models/MerchantPriceConfig'

export interface L2MerchantPriceDataApi extends L2DataApi {
    getDefaultConfig() : MerchantPriceConfig
    getAllConfigs() : Array<MerchantPriceConfig>
    getConfigByTownId( id : number ) : MerchantPriceConfig
    getConfigByCastleId( id : number ) : MerchantPriceConfig
}