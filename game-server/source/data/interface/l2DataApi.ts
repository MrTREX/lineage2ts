export interface L2DataApi {
    load() : Promise<Array<string>>
}

export interface L2ReloadableDataApi extends L2DataApi {
    reLoad() : Promise<void>
}