import { L2DataApi } from './l2DataApi'
import { L2Henna } from '../../gameService/models/items/L2Henna'

export interface L2HennaDataApi extends L2DataApi {
    get( symbolId: number ) : L2Henna
    getAll( classId: number ) : Array<L2Henna>
}