import { L2DataApi } from './l2DataApi'

export interface RecipeDataApi extends L2DataApi {
    getRecipeList( recipeId: number ) : L2RecipeDataItem
    getRecipeByItemId( itemId: number ) : L2RecipeDataItem
}

export interface L2RecipeDataInput {
    id: number
    amount: number
}

export interface L2RecipeDataOutput extends L2RecipeDataInput {
    chance: number
}

export interface L2RecipeDataItem {
    readonly id: number
    readonly level: number
    readonly successRate: number
    readonly isCommon: boolean
    readonly inputs: ReadonlyArray<L2RecipeDataInput>
    readonly outputs: ReadonlyArray<L2RecipeDataOutput>
    readonly consumeMp: number
    readonly npcInputs: ReadonlyArray<L2RecipeDataInput> | null
}