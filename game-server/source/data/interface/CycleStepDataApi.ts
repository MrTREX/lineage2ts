export interface L2CycleStepDataApi {
    getAll() : Array<L2CycleStepDataItem>
}

export interface L2CycleStepItemLocation {
    x: number
    y: number
    z: number
}

export interface L2CycleStepItemInterval {
    time: number
    points: number
}

export type L2CycleStepRangePoint = [ number, number, number, number ]

export interface L2CycleStepDataItem {
    cycleId: number
    stepId: number
    points: number
    dropDuration: number
    inverval: L2CycleStepItemInterval
    lockDuration: number
    mapId: number
    changeTimeCron: string
    areaIds: Array<string>
    doorIds: Array<string>
    respawnPoint: L2CycleStepItemLocation
    respawnPvpPoint: L2CycleStepItemLocation
    respawnRange: Array<L2CycleStepRangePoint>
    mapPoint: L2CycleStepItemLocation
}