import { L2DataApi } from './l2DataApi'

export interface L2SoulCrystalDataApi extends L2DataApi {
    getItems(): { [ itemId: number ]: L2SoulCrystalDataItem }
    getNpcs(): { [ npcId: number ]: Array<L2SoulCrystalDataNpc> }
}

export interface L2SoulCrystalDataItem {
    itemId: number
    nextCrystalItemId: number
    level: number
}

export interface L2SoulCrystalDataNpc {
    npcId: number
    chance : number
    levels: Array<number>
    requiresSkill: boolean,
    type: L2SoulCrystalAbsorbType
}

export enum L2SoulCrystalAbsorbType {
    lastHitPlayer,
    eachPlayerInParty,
    randomPlayerInParty,
    randomPlayerPartyChance
}