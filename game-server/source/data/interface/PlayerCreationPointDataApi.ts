import { L2DataApi } from './l2DataApi'

export interface PlayerCreationPointDataApi extends L2DataApi {
    getCreationPoint( classId: number ): L2PlayerCreationPointData
}

export interface L2PlayerCreationPointData {
    classId: number
    x: number
    y: number
    z: number
}