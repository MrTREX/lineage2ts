import { L2DataApi } from './l2DataApi'

export interface L2QuestDataApi extends L2DataApi {
    getQuestName( id : number ) : string
}