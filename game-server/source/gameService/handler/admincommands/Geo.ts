import { IAdminCommand } from '../IAdminCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { formatDescription, formatOptionsDescription } from './helpers/FormatHelpers'
import { GeoPolygonCache } from '../../cache/GeoPolygonCache'
import { L2Object } from '../../models/L2Object'
import { L2World } from '../../L2World'
import { L2WorldRegion } from '../../models/L2WorldRegion'
import { L2Character } from '../../models/actor/L2Character'
import { ValidateLocation } from '../../packets/send/ValidateLocation'
import _ from 'lodash'
import { PointGeometry } from '../../models/drops/PointGeometry'
import { GeometryId } from '../../enums/GeometryId'
import { DrawHelper } from './helpers/DrawHelper'
import { AdminHtml } from './helpers/AdminHtml'
import { PlayerMovementActionCache } from '../../cache/PlayerMovementActionCache'
import { LinePoints3DPool } from '../../../geodata/LinePoints'
import { GeoDataAdjustment, GeoVisibilityOutcome } from '../../../geodata/GeoDataOperations'
import { CellMoveDirection, getLineMoveDirection, hasMovementDirection } from '../../../geodata/enums/CellMoveDirection'
import { ThetaStar } from '../../../geodata/pathfinding/ThetaStar'
import { ExServerPrimitive } from '../../packets/send/builder/ExServerPrimitive'
import { PolygonValues } from '../../../geodata/enums/PolygonValues'

const enum GeoCommands {
    PositionCheck = 'geo-check',
    TargetCheck = 'geo-target',
    Sync = 'geo-sync',
    Region = 'geo-region',
    HeightMap = 'geo-height-map',
    HeightMapClear = 'geo-height-map-clear',
    Menu = 'geo-menu',
    MoveInfo = 'geo-move-info',
    VisibilityPoint = 'geo-visibility-point',
    VisibilityTarget = 'geo-visibility-target',
    VisibilityCircle = 'geo-visibility-circle',
    VisibilityCircleClear = 'geo-visibility-circle-clear',
    FindPath = 'geo-find-path',
    FindPathClear = 'geo-find-path-clear',
    FindPathCircle = 'geo-find-path-circle',
    FindPathCircleClear = 'geo-find-path-circle-clear',
}

const enum GeoHeightColor {
    Normal = 0x00FF00,
    Error = 0xFF0000,
    Height = 0x26F196,
    Visibility = 0x0028B6
}

export const Geo: IAdminCommand = {
    getCommandNames(): Array<string> {
        return [
            GeoCommands.PositionCheck,
            GeoCommands.TargetCheck,
            GeoCommands.Sync,
            GeoCommands.Region,
            GeoCommands.HeightMap,
            GeoCommands.HeightMapClear,
            GeoCommands.Menu,
            GeoCommands.MoveInfo,
            GeoCommands.VisibilityPoint,
            GeoCommands.VisibilityTarget,
            GeoCommands.VisibilityCircle,
            GeoCommands.VisibilityCircleClear,
            GeoCommands.FindPath,
            GeoCommands.FindPathClear,
            GeoCommands.FindPathCircle,
            GeoCommands.FindPathCircleClear,
        ]
    },

    getDescription( command: GeoCommands ): Array<string> {
        switch ( command ) {
            case GeoCommands.PositionCheck:
                return [
                    formatDescription( GeoCommands.PositionCheck, 'Show current player position height' ),
                ]

            case GeoCommands.TargetCheck:
                return [
                    formatDescription( GeoCommands.TargetCheck, 'Show target position height' ),
                ]

            case GeoCommands.Sync:
                return [
                        formatOptionsDescription( GeoCommands.Sync, '[ height = 0 ]', 'Assign current player (or optionally target) current geo position with additional Z height added.' ),
                        'If player has target selected, target is used for such operation. Otherwise, current player will have (x,y,z) position synchronized to geodata.'
                ]

            case GeoCommands.Region:
                return [
                        formatDescription( GeoCommands.Region, 'Shows region and geo tile information' )
                ]

            case GeoCommands.HeightMap:
                return [
                    formatDescription( GeoCommands.HeightMap, 'Shows surrounding geodata height grid' )
                ]

            case GeoCommands.HeightMapClear:
                return [
                    formatDescription( GeoCommands.HeightMapClear, 'Clear visible geodata height grid' )
                ]

            case GeoCommands.Menu:
                return [
                    formatDescription( GeoCommands.Menu, 'Shows available geo commands' )
                ]

            case GeoCommands.MoveInfo:
                return [
                    formatDescription( GeoCommands.MoveInfo, 'Shows information about geodata for clicked position' )
                ]

            case GeoCommands.VisibilityPoint:
                return [
                    formatDescription( GeoCommands.VisibilityPoint, 'Shows visibility line from your position to clicked location' )
                ]

            case GeoCommands.VisibilityTarget:
                return [
                    formatDescription( GeoCommands.VisibilityTarget, 'Shows visibility line from your position to target' )
                ]

            case GeoCommands.VisibilityCircle:
                return [
                    formatDescription( GeoCommands.VisibilityCircle, 'Shows visibility trace around player character' )
                ]

            case GeoCommands.VisibilityCircleClear:
                return [
                    formatDescription( GeoCommands.VisibilityCircleClear, 'Clears visibility circle points from client' )
                ]

            case GeoCommands.FindPath:
                return [
                    formatDescription( GeoCommands.FindPath, 'Shows computed walking path for clicked destination location' )
                ]

            case GeoCommands.FindPathClear:
                return [
                    formatDescription( GeoCommands.FindPathClear, 'Clears previously shown walking path' )
                ]

            case GeoCommands.FindPathCircle:
                return [
                    formatDescription( GeoCommands.FindPathCircle, 'Shows circle pathfinding trace around player character' )
                ]

            case GeoCommands.FindPathCircleClear:
                return [
                    formatDescription( GeoCommands.FindPathCircleClear, 'Clears circle pathfinding trace points from client' )
                ]
        }
    },

    async onCommand( command: string, player: L2PcInstance ): Promise<void> {
        let commandChunks = command.split( ' ' )

        switch ( commandChunks[ 0 ] ) {
            case GeoCommands.PositionCheck:
                return performPositionCheck( player, player )

            case GeoCommands.TargetCheck:
                return performPositionCheck( player.getTarget(), player )

            case GeoCommands.Sync:
                return syncPosition( player.getTarget(), player, commandChunks )

            case GeoCommands.Region:
                return showRegion( player )

            case GeoCommands.HeightMap:
                return onHeightMap( player )

            case GeoCommands.HeightMapClear:
                return onHeightMapClear( player )

            case GeoCommands.Menu:
                return AdminHtml.showHtml( player, 'overrides/html/command/geo/menu.htm' )

            case GeoCommands.MoveInfo:
                player.sendMessage( 'Geo Move Info ready. Click where you want to see geo-data information' )
                return PlayerMovementActionCache.setAction( player.getObjectId(), onMoveInfoClicked )

            case GeoCommands.VisibilityPoint:
                player.sendMessage( 'Geo Visibility ready. Click where you want to trace visibility' )
                return PlayerMovementActionCache.setAction( player.getObjectId(), onVisibilityClicked )

            case GeoCommands.VisibilityTarget:
                return onVisibilityTarget( player )

            case GeoCommands.VisibilityCircle:
                return onVisibilityCircle( player )

            case GeoCommands.VisibilityCircleClear:
                return onVisibilityCircleClear( player )

            case GeoCommands.FindPath:
                player.sendMessage( 'Geo Pathfinding ready. Click location to trace pathfinding.' )
                return PlayerMovementActionCache.setAction( player.getObjectId(), onFindPathClicked )

            case GeoCommands.FindPathClear:
                return DrawHelper.clearExistingPacket( player, GeoCommands.FindPath )

            case GeoCommands.FindPathCircle:
                return onPathfindingCircle( player )

            case GeoCommands.FindPathCircleClear:
                return DrawHelper.clearExistingPacket( player, GeoCommands.FindPathCircle )
        }
    },
}

function performPositionCheck( object: L2Object, player: L2PcInstance ): void {
    if ( !object ) {
        return player.sendMessage( 'Target unavailable, cannot display coordinates.' )
    }

    let geoZ = GeoPolygonCache.getObjectZ( object )
    player.sendMessage( `"${ object.getName() }" geo position (x = ${object.getX()}, y = ${object.getY()}, z = ${geoZ}): current Z = ${object.getZ()}` )
}

function syncPosition( object: L2Object, player: L2PcInstance, commandChunks: Array<string> ) : void {
    if ( object && !object.isCharacter() ) {
        return player.sendMessage( 'Non-character target. Only characters can synchronize geo coordinates' )
    }

    let [ , heightValue ] = commandChunks

    let target = object ? object as L2Character : player
    let height = GeoPolygonCache.getObjectZ( target ) + _.defaultTo( parseInt( heightValue, 10 ), 0 )

    target.setZ( height )

    player.sendMessage( `"${target.getName()}" synchronized geo position (x = ${target.getX()}, y = ${target.getY()}, z = ${target.getZ()})` )
    player.sendOwnedData( ValidateLocation( target ) )
}

function showRegion( player: L2PcInstance ) : void {
    let regions = L2World.getRegionsWithRadius( player.getX(), player.getY(), 10 )
    if ( regions.length === 0 ) {
        player.sendMessage( `Unable to determine world region for (x = ${player.getX()}, y = ${player.getY()})` )
    }

    regions.forEach( ( region: L2WorldRegion ) => {
        player.sendMessage( `Region for tile (x,y) : ${region.tileX} ${region.tileY}` )
        player.sendMessage( `Region boundaries (minX, maxX, minY, maxY): ${ region.geometry.minX } ${ region.geometry.maxX } ${ region.geometry.minY } ${ region.geometry.maxY }` )
    } )
}

function createHeightMapName( value: number ) : string {
    return `${GeoCommands.HeightMap}${value}`
}

function onHeightMap( player: L2PcInstance ) : void {
    let startTime = Date.now()
    let geometry = PointGeometry.acquire( GeometryId.Geo256 )
    let gridGeometry = PointGeometry.acquire( GeometryId.GeoGrid256 )

    gridGeometry.refresh()

    let totalPoints = geometry.size() * gridGeometry.size()

    let baseX = player.getX() + geometry.xOffset
    let baseY = player.getY() + geometry.yOffset

    while ( gridGeometry.size() > 0 ) {
        gridGeometry.prepareNextPoint()

        let x = baseX + gridGeometry.getX()
        let y = baseY + gridGeometry.getY()

        sendGeoHeightGridItem( player, createHeightMapName( gridGeometry.size() ), geometry, x, y, player.getZ() )
    }

    player.sendMessage( `Computed ${totalPoints} height map points in ${Date.now() - startTime} ms` )
    geometry.release()
    gridGeometry.release()
}

function sendGeoHeightGridItem( player : L2PcInstance, name: string, geometry: PointGeometry, positionX: number, positionY: number, positionZ: number ) : void {
    let packet = DrawHelper.getDrawPacket( player, name )

    geometry.refresh()

    while ( geometry.size() > 0 ) {
        geometry.prepareNextPoint()

        let x = positionX + geometry.getX()
        let y = positionY + geometry.getY()
        let z = GeoPolygonCache.getZ( x, y, positionZ )

        if ( z === PolygonValues.LowestHeight ) {
            continue
        }

        packet.addPoint( '', GeoHeightColor.Height, true, x, y, z )
    }

    player.sendOwnedData( packet.getBuffer() )
}

function onMoveInfoClicked( player: L2PcInstance, targetX : number, targetY : number, targetZ : number ) : void {
    PlayerMovementActionCache.clearAction( player.getObjectId() )
    let z = GeoPolygonCache.getZ( targetX, targetY, targetZ )
    player.sendMessage( `Position(x,y,z) : ${targetX}, ${targetY}, ${targetZ} has geo Z=${z}` )
}

function drawVisibilityPoints(
    player: L2PcInstance,
    packetName: string,
    targetX: number,
    targetY: number,
    targetZ: number,
    drawZVisibility: boolean = true,
    skipPoints: number = 1 ) : void {
    let packet = DrawHelper.getDrawPacket( player, packetName )
    let points = LinePoints3DPool.getValue()

    /*
        Please note initial Z value is adjusted using player's height. Adjusted value plays correctly
        with curvature of geodata and how such curvature is seen in client. In short, player coordinates
        are adjusted somewhere to be near head, while actual position in client seen as player's feet.
        Hence, to correct such discrepancy and to account for variances of Z values, adjustment is needed.
     */
    points.initialize3D(
        player.getX(),
        player.getY(),
        player.getZ() + player.getCollisionHeight(),
        targetX,
        targetY,
        targetZ )

    if ( drawZVisibility ) {
        packet.addPoint( '', GeoHeightColor.Visibility, true, points.x, points.y, points.z )
    }

    /*
        In certain drawing, when many visibility paths are made, it is easier on client to display less points,
        hence we can skip some points that are too close to player.
     */
    for ( let i = 0; i < skipPoints; i++ ) {
        points.prepareNextPoint()
    }

    let color: GeoHeightColor = GeoHeightColor.Normal

    for ( let index = 0; index < 400; index++ ) {
        if ( !points.prepareNextPoint() ) {
            break
        }

        if ( drawZVisibility ) {
            packet.addPoint( '', GeoHeightColor.Visibility, true, points.x, points.y, points.z )
        }

        let zValue = GeoPolygonCache.getZ( points.x, points.y, points.z )
        if ( ( zValue - GeoDataAdjustment.ZErrorRate ) > points.z ) {
            color = GeoHeightColor.Error
        }

        let destinationMove = GeoPolygonCache.getMovementDirection( points.x, points.y, points.z )

        packet.addPoint( getDirectionNSWE( destinationMove ), color, true, points.x, points.y, zValue )
    }

    LinePoints3DPool.recycleValue( points )

    player.sendOwnedData( packet.getBuffer() )
}

function onVisibilityClicked( player: L2PcInstance, targetX : number, targetY : number, targetZ : number ) : void {
    PlayerMovementActionCache.clearAction( player.getObjectId() )

    let adjustedZ = GeoPolygonCache.getZ( targetX, targetY, targetZ + 40 ) + player.getCollisionHeight()
    let isReachable = GeoPolygonCache.isVisibleWithDirectPath( player.getX(), player.getY(), player.getZ() + player.getCollisionHeight(), targetX, targetY, adjustedZ )
    let destinationMove = GeoPolygonCache.getMovementDirection( targetX, targetY, adjustedZ )
    let lineMoveDirection = getLineMoveDirection( player.getX(), player.getY(), targetX, targetY )

    /*
        Please note target Z value is adjusted with constant value, normally used for player characters to account for
        values coming from client, and player's height, which simply means that player is able to see itself in
        clicked position.
     */
    drawVisibilityPoints( player, GeoCommands.VisibilityPoint, targetX, targetY, adjustedZ )

    let message = 'is NOT visible'
    switch ( isReachable ) {
        case GeoVisibilityOutcome.NoDirectPath:
            message = 'is VISIBLE, but NO direct path'
            break

        case GeoVisibilityOutcome.VisibleDirectPath:
            message = 'is VISIBLE with direct path'
            break
    }

    player.sendMessage( `Position(x,y,z) : ${targetX}, ${targetY}, ${targetZ} ${ message }` )
    player.sendMessage( `Destination allowed movement is ${getDirectionDescription( destinationMove ).join( ' ' )} or 0x${ destinationMove.toString( 16 ) }` )
    player.sendMessage( `View direction is ${getDirectionDescription( lineMoveDirection ).join( ' ' )} or 0x${ lineMoveDirection.toString( 16 ) }` )
}

function onVisibilityTarget( player: L2PcInstance ) : void {
    let target = player.getTarget()
    if ( !target || !target.isCharacter() ) {
        return player.sendMessage( 'Target unavailable' )
    }

    drawVisibilityPoints( player, GeoCommands.VisibilityPoint, target.getX(), target.getY(), target.getZ() + ( target as L2Character ).getCollisionHeight() )

    let isVisible = GeoPolygonCache.isTargetVisible( player, target as L2Character )
    player.sendMessage( `Target at position(x,y,z) : ${target.getX()}, ${target.getY()}, ${target.getZ()} is ${ isVisible ? 'visible' : 'NOT visible' }` )
}

function createVisibilityCirclePathName( size: number ) : string {
    return `${GeoCommands.VisibilityCircleClear}${size}`
}

function onVisibilityCircle( player: L2PcInstance ) : void {
    let geometry = PointGeometry.acquire( GeometryId.GeoVisibility )
    let startTime = Date.now()

    geometry.refresh()
    let totalPaths = geometry.size()

    while ( geometry.size() > 0 ) {
        geometry.prepareNextPoint()

        let x = player.getX() + geometry.getX()
        let y = player.getY() + geometry.getY()

        drawVisibilityPoints(
            player,
            createVisibilityCirclePathName( geometry.size() ),
            x,
            y,
            GeoPolygonCache.getZ( x, y, player.getZ() ) + player.getCollisionHeight(),
            false,
            4 )
    }

    player.sendMessage( `Computed ${totalPaths} visibility paths in ${Date.now() - startTime} ms` )
}

function onVisibilityCircleClear( player: L2PcInstance ) : void {
    let geometry = PointGeometry.acquire( GeometryId.GeoVisibility )

    geometry.refresh()

    for ( let size = 0; size < geometry.size(); size++ ) {
        DrawHelper.clearExistingPacket( player, createVisibilityCirclePathName( size ) )
    }
}

function onHeightMapClear( player: L2PcInstance ) : void {
    let geometry = PointGeometry.acquire( GeometryId.GeoGrid256 )

    geometry.refresh()

    for ( let size = 0; size < geometry.size(); size++ ) {
        DrawHelper.clearExistingPacket( player, createHeightMapName( size ) )
    }
}

function getDirectionDescription( direction : CellMoveDirection ) : Array<string> {
    const words : Array<string> = []

    if ( hasMovementDirection( direction, CellMoveDirection.North ) ) {
        words.push( 'North' )
    }

    if ( hasMovementDirection( direction, CellMoveDirection.South ) ) {
        words.push( 'South' )
    }

    if ( hasMovementDirection( direction, CellMoveDirection.East ) ) {
        words.push( 'East' )
    }

    if ( hasMovementDirection( direction, CellMoveDirection.West ) ) {
        words.push( 'West' )
    }

    return words
}

function drawPathfinding( packet: ExServerPrimitive, player : L2PcInstance, targetX: number, targetY : number, targetZ: number ) : void {
    if ( GeoPolygonCache.hasDirectPath( player.getX(), player.getY(), player.getZ(), targetX, targetY ) ) {
        packet.addLine( '', GeoHeightColor.Visibility, true, player.getX(), player.getY(), player.getZ() + player.getCollisionHeight(), targetX, targetY, targetZ + player.getCollisionHeight() )
        return
    }

    let star = new ThetaStar()
    let path = star.findPath( player.getX(), player.getY(), player.getZ() + player.getCollisionHeight(), targetX, targetY, targetZ + player.getCollisionHeight() )

    if ( !path ) {
        return
    }

    for ( let index = 1; index < path.length; index++ ) {
        let previousNode = path[ index - 1 ]
        let currentNode = path[ index ]

        packet.addLine( '', GeoHeightColor.Visibility, true, previousNode.x, previousNode.y, previousNode.z + player.getCollisionHeight(), currentNode.x, currentNode.y, currentNode.z + player.getCollisionHeight() )
    }
}

function onFindPathClicked( player: L2PcInstance, targetX : number, targetY : number, targetZ : number ) : void {
    PlayerMovementActionCache.clearAction( player.getObjectId() )

    if ( GeoPolygonCache.hasDirectPath( player.getX(), player.getY(), player.getZ(), targetX, targetY ) ) {
        let packet = DrawHelper.getDrawPacket( player, GeoCommands.FindPath )

        let adjustedTargetZ = GeoPolygonCache.getZ( targetX, targetY, targetZ )
        packet.addLine( '', GeoHeightColor.Visibility, true, player.getX(), player.getY(), player.getZ() + player.getCollisionHeight(), targetX, targetY, adjustedTargetZ + player.getCollisionHeight() )
        player.sendOwnedData( packet.getBuffer() )

        player.sendMessage( 'Direct path detected' )
        return
    }

    DrawHelper.clearExistingPacket( player, GeoCommands.FindPath )

    let star = new ThetaStar()
    let currentTime = Date.now()
    let path = star.findPath( player.getX(), player.getY(), player.getZ() + player.getCollisionHeight(), targetX, targetY, targetZ + player.getCollisionHeight() )

    player.sendMessage( `Computed path in ${Date.now() - currentTime} ms` )
    player.sendMessage( `Used nodes: ${star.exploredNodes.size}` )

    if ( !path ) {
        return player.sendMessage( 'No path found' )
    }

    let packet = DrawHelper.getDrawPacket( player, GeoCommands.FindPath )

    for ( let index = 1; index < path.length; index++ ) {
        let previousNode = path[ index - 1 ]
        let currentNode = path[ index ]

        packet.addLine( '', GeoHeightColor.Visibility, true, previousNode.x, previousNode.y, previousNode.z + player.getCollisionHeight(), currentNode.x, currentNode.y, currentNode.z + player.getCollisionHeight() )
    }

    player.sendOwnedData( packet.getBuffer() )
    player.sendMessage( `Path found using ${path.length} nodes` )
}

function getDirectionNSWE( direction : CellMoveDirection ) : string {
    if ( direction === CellMoveDirection.All ) {
        return ''
    }

    const words : Array<string> = []

    if ( hasMovementDirection( direction, CellMoveDirection.North ) ) {
        words.push( 'N' )
    }

    if ( hasMovementDirection( direction, CellMoveDirection.South ) ) {
        words.push( 'S' )
    }

    if ( hasMovementDirection( direction, CellMoveDirection.East ) ) {
        words.push( 'E' )
    }

    if ( hasMovementDirection( direction, CellMoveDirection.West ) ) {
        words.push( 'W' )
    }

    return words.join( '' )
}

function onPathfindingCircle( player: L2PcInstance ) : void {
    let geometry = PointGeometry.acquire( GeometryId.GeoVisibility )

    geometry.refresh()
    let totalPaths = geometry.size()
    let packet = DrawHelper.getDrawPacket( player, GeoCommands.FindPathCircle )

    let startTime = Date.now()
    while ( geometry.size() > 0 ) {
        geometry.prepareNextPoint()

        let x = player.getX() + geometry.getX()
        let y = player.getY() + geometry.getY()

        drawPathfinding(
            packet,
            player,
            x,
            y,
            GeoPolygonCache.getZ( x, y, player.getZ() ) + player.getCollisionHeight() )
    }

    player.sendOwnedData( packet.getBuffer() )
    player.sendMessage( `Computed ${totalPaths} finding paths in ${Date.now() - startTime} ms` )
}