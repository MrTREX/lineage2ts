import { IAdminCommand } from '../IAdminCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { AdminManager } from '../../cache/AdminManager'
import _ from 'lodash'
import { forceReload } from '../../../config/ConfigManager'
import { formatDescription, formatOptionsDescription } from './helpers/FormatHelpers'
import { AdminHtml } from './helpers/AdminHtml'
import { DataManager } from '../../../data/manager'
import { createPagination } from './helpers/SearchHelper'
import { NpcHtmlMessage } from '../../packets/send/NpcHtmlMessage'
import { AvailableObjectPools, ObjectPool } from '../../helpers/ObjectPoolHelper'

const enum ServerCommands {
    AdminPage = 'admin',
    GmVisibility = 'gmliston',
    GmInvisibility = 'gmlistoff',
    TradeRefusal = 'tradeoff',
    MessageRefusal = 'silence',
    WeightRemoval = 'weightless',
    ReloadConfiguration = 'config-reload',
    ObjectPools = 'server-objectpools'
}

export const ServerAdministration: IAdminCommand = {
    getDescription( command: string ): Array<string> {
        if ( command.startsWith( ServerCommands.AdminPage ) ) {
            return [
                formatDescription( ServerCommands.AdminPage, 'show main admin menu' ),
                `//${ ServerCommands.AdminPage }(1-7) - show various topics of admin menu`,
            ]
        }

        switch ( command ) {
            case ServerCommands.GmVisibility:
                return [
                    formatDescription( ServerCommands.GmVisibility, 'adds account as visible in-game GM' ),
                ]

            case ServerCommands.GmInvisibility:
                return [
                    formatDescription( ServerCommands.GmInvisibility, 'add account as hidden in-game GM' ),
                ]

            case ServerCommands.TradeRefusal:
                return [
                    formatOptionsDescription( ServerCommands.TradeRefusal, '< on|off >', 'set trade refusal mode' ),
                ]

            case ServerCommands.MessageRefusal:
                return [
                    formatDescription( ServerCommands.MessageRefusal, 'toggle message refusal mode' ),
                ]

            case ServerCommands.WeightRemoval:
                return [
                    formatOptionsDescription( ServerCommands.WeightRemoval, '<on|off>', 'sets diet mode' ),
                ]

            case ServerCommands.ObjectPools:
                return [
                    formatOptionsDescription( ServerCommands.ObjectPools, '[ page = 1 ]', 'Show information about object pool usage' )
                ]
        }
    },

    getCommandNames(): Array<string> {
        return [
            ServerCommands.AdminPage,
            ...generateAdminPageCommands(),
            ServerCommands.GmVisibility,
            ServerCommands.GmInvisibility,
            ServerCommands.TradeRefusal,
            ServerCommands.MessageRefusal,
            ServerCommands.WeightRemoval,
            ServerCommands.ReloadConfiguration,
            ServerCommands.ObjectPools,
        ]
    },

    async onCommand( command: string, player: L2PcInstance ): Promise<void> {
        if ( command.startsWith( ServerCommands.AdminPage ) ) {
            showMainPage( player, command )
            return
        }

        let commandChunks: Array<string> = command.split( ' ' )
        let currentCommand: string = commandChunks[ 0 ]

        switch ( currentCommand ) {
            case ServerCommands.GmVisibility:
                AdminManager.addGm( player.getObjectId(), false )
                player.sendMessage( 'Registered into gm list' )
                AdminHtml.showAdminHtml( player, 'gm_menu.htm' )
                return

            case ServerCommands.GmInvisibility:
                AdminManager.addGm( player.getObjectId(), true )
                player.sendMessage( 'Removed from gm list' )
                AdminHtml.showAdminHtml( player, 'gm_menu.htm' )
                return

            case ServerCommands.MessageRefusal:
                if ( player.isSilenceMode() ) {
                    player.setSilenceMode( false )
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.MESSAGE_ACCEPTANCE_MODE ) )
                } else {
                    player.setSilenceMode( true )
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.MESSAGE_REFUSAL_MODE ) )
                }

                AdminHtml.showAdminHtml( player, 'gm_menu.htm' )
                return

            case ServerCommands.WeightRemoval:
                let status: string = _.nth( commandChunks, 1 )
                if ( ![ 'on', 'off' ].includes( status ) ) {
                    return
                }

                let applyDiet: boolean = status === 'on'
                player.setWeightlessMode( applyDiet )
                player.sendMessage( `Weightless mode is ${ status }` )

                player.refreshOverloadPenalty()
                AdminHtml.showAdminHtml( player, 'gm_menu.htm' )
                return

            case ServerCommands.TradeRefusal:
                let mode: string = _.nth( commandChunks, 1 )
                if ( ![ 'on', 'off' ].includes( mode ) ) {
                    return
                }

                let applyMode: boolean = mode === 'on'
                player.setTradeRefusal( applyMode )
                player.sendMessage( `Trade refusal mode: ${ player.getTradeRefusal() }` )
                AdminHtml.showAdminHtml( player, 'gm_menu.htm' )
                return

            case ServerCommands.ReloadConfiguration:
                await forceReload()
                player.sendMessage( 'All configs have been reloaded.' )
                return

            case ServerCommands.ObjectPools:
                return onObjectPools( commandChunks, player )
        }
    },
}

function showMainPage( player: L2PcInstance, command: string ): void {
    let mode: number = parseInt( command.substring( 11 ) )

    switch ( mode ) {
        case 1:
            return AdminHtml.showAdminHtml( player, 'main_menu.htm' )

        case 2:
            return AdminHtml.showAdminHtml( player, 'game_menu.htm' )

        case 3:
            return AdminHtml.showAdminHtml( player, 'effects_menu.htm' )

        case 4:
            return AdminHtml.showAdminHtml( player, 'server_menu.htm' )

        case 5:
            return AdminHtml.showAdminHtml( player, 'mods_menu.htm' )

        case 6:
            return AdminHtml.showAdminHtml( player, 'char_menu.htm' )

        case 7:
            return AdminHtml.showAdminHtml( player, 'gm_menu.htm' )

        default:
            return AdminHtml.showAdminHtml( player, 'main_menu.htm' )
    }
}

function generateAdminPageCommands(): Array<string> {
    return _.times( 7, ( index: number ): string => `${ ServerCommands.AdminPage }${ index + 1 }` )
}

function onObjectPools( commandChunks: Array<string>, player: L2PcInstance ): void {
    let page = commandChunks.length === 1 ? 1 : parseInt( commandChunks[ 1 ] )
    if ( !page || !Number.isInteger( page ) ) {
        return player.sendMessage( 'Please provide numeric value for page' )
    }

    let allPools = Object.values( AvailableObjectPools )
    let paginatedItems = _.chunk( allPools, 60 )
    let paginationIndex = page ? page - 1 : 0
    let currentPageItems = paginatedItems[ paginationIndex ]

    if ( !currentPageItems ) {
        return player.sendMessage( `${ ServerCommands.ObjectPools } : cannot display results for page=${ page }` )
    }

    let poolTemplate = DataManager.getHtmlData().getItem( 'overrides/html/command/server/objectPools-item.htm' )
    let renderedItems : string = currentPageItems.map( ( pool: ObjectPool<unknown> ) : string => {
        return poolTemplate
            .replace( '%name%', pool.name )
            .replace( '%size%', pool.objectPool.size().toString() )
    } ).join( '' )

    let html = DataManager.getHtmlData().getItem( 'overrides/html/command/server/objectPools.htm' )
        .replace( '%pagination%', createPagination( paginatedItems.length, `bypass -h admin_${ ServerCommands.ObjectPools }` ) )
        .replace( '%items%', renderedItems )

    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId() ) )
}