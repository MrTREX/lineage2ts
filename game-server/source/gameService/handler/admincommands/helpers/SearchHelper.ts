import _ from 'lodash'

export function createPagination( pages: number, command: string ): string {
    if ( pages === 1 ) {
        return ''
    }

    let availablePages = _.times( pages, index => index + 1 ).join( ';' )

    return `<td width=40><font color="LEVEL">Page:</font></td>
        <td><combobox width=70 height=17 var=pagebox list="${ availablePages }"></td>
        <td align=right><button value="Go To Page" action="${command} $pagebox" width=100 height=21 back="L2UI_CT1.Button_DF_Down" fore="L2UI_CT1.Button_DF"></td>`
}