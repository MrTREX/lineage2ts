import { L2PcInstance } from '../../../models/actor/instance/L2PcInstance'
import { DataManager } from '../../../../data/manager'
import { NpcHtmlMessagePath } from '../../../packets/send/NpcHtmlMessage'
import { CrystalType } from '../../../models/items/type/CrystalType'
import { ArmorType } from '../../../enums/items/ArmorType'

export const AdminHtml = {
    showAdminHtml( player: L2PcInstance, path: string ): void {
        AdminHtml.showHtml( player, `data/html/admin/${ path }` )
    },

    showHtml( player: L2PcInstance, path: string ): void {
        if ( !DataManager.getHtmlData().hasItem( path ) ) {
            player.sendMessage( `No entry for html path: ${ path }` )
            return
        }

        let html: string = DataManager.getHtmlData().getItem( path )
        player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId() ) )
    },
}

export function createButton( name: string, action: string, width: number = 45 ) : string {
    return `<button value="${ name }" action="${ action }" width=${ width } height=21 back="L2UI_CT1.Button_DF_Down" fore="L2UI_CT1.Button_DF">`
}

export function getCrystalColor( type: CrystalType ): string {
    switch ( type ) {
        case CrystalType.NONE:
            return '999999'

        case CrystalType.D:
            return '1e90ff'

        case CrystalType.C:
            return 'ffff00'

        case CrystalType.B:
            return 'ffc0cb'

        case CrystalType.A:
            return 'ffefd5'

        case CrystalType.S:
            return 'ff8c00'

        default:
            return 'ff4500'
    }
}

export function getArmorTypeColor( type: ArmorType ) : string {
    switch ( type ) {
        case ArmorType.HEAVY:
            return 'fd5959'

        case ArmorType.LIGHT:
            return 'ff9c6d'

        case ArmorType.MAGIC:
            return 'fcff82'

        default:
            return 'afc5ff'
    }
}