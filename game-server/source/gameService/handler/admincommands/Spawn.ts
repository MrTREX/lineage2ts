import { IAdminCommand } from '../IAdminCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Npc } from '../../models/actor/L2Npc'
import { DataManager } from '../../../data/manager'
import _ from 'lodash'
import { formatDescription, formatOptionsDescription } from './helpers/FormatHelpers'
import { SpawnMakerCache } from '../../cache/SpawnMakerCache'
import { ISpawnLogic } from '../../models/spawns/ISpawnLogic'
import { NpcTemplateType } from '../../enums/NpcTemplateType'
import { createTeleportToCoordinatesBypass, createTeleportToObjectBypass } from './Teleport'
import { createGmStatsBypass } from '../bypasshandlers/viewNpc'
import { L2World } from '../../L2World'
import { NpcHtmlMessage } from '../../packets/send/NpcHtmlMessage'
import { L2DefaultSpawn } from '../../models/spawns/type/L2DefaultSpawn'
import { AdminHtml } from './helpers/AdminHtml'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { createPagination } from './helpers/SearchHelper'
import { L2NpcTemplate } from '../../models/actor/templates/L2NpcTemplate'
import { GeneralHelper } from '../../helpers/GeneralHelper'
import { L2ManualSpawn } from '../../models/spawns/type/L2ManualSpawn'
import { L2NpcDataTemplateOrdering } from '../../../data/interface/NpcDataApi'
import { getNameFromCode, getRegionCode } from '../../enums/L2MapTile'
import { DrawHelper } from './helpers/DrawHelper'
import { GeoPolygonCache } from '../../cache/GeoPolygonCache'
import { L2SpawnTerritory } from '../../models/spawns/L2SpawnTerritory'

const pageLimit: number = 60

const enum SpawnOperations {
    SpawnMenu = 'spawn-menu',
    ViewSpawn = 'spawn-view',
    ViewObjectSpawn = 'spawn-view-object',
    DespawnObject = 'despawn-object',
    DespawnTarget = 'despawn-target',
    DespawnAll = 'despawn-all',
    DespawnOne = 'despawn-one',
    SpawnAll = 'spawn-all',
    SpawnMore = 'spawn-more',
    SpawnNpc = 'spawn-npc',
    FindSpawnsByNpcName = 'spawn-find-name',
    FindSpawnsByNpcId = 'spawn-find-id',
    RemoveSpawn = 'spawn-remove',
    ViewRegionSpawns = 'spawn-view-region',
    ViewByType = 'spawn-view-type',
    ShowTerritoryPoints = 'spawn-territory-points'
}

const enum SpawnColors {
    Active = 'ffa600',
    Inactife = '636189',
}

const enum LevelColors {
    Top = 'e127d2',
    Higher = 'f35b24',
    High = 'f39c12',
    Medium = 'f8cc1b',
    Middle = 'e9f10f',
    Normal = '72b043',
    Lower = '27ae60',
    Low = '007f4e'
}

const enum DrawColor {
    Normal = 0x26F196
}

export function createDespawnBypass( objectId: number ) : string {
    return `bypass admin_${SpawnOperations.DespawnObject} ${objectId}`
}

export function createSpawnMenuBypass() : string {
    return `bypass -h admin_${SpawnOperations.SpawnMenu}`
}

export function createDespawnAllBypass( spawnId: string ) : string {
    return `bypass admin_${SpawnOperations.DespawnAll} ${ spawnId }`
}

export function createSpawnAllBypass( spawnId: string ) : string {
    return `bypass admin_${SpawnOperations.SpawnAll} ${ spawnId }`
}

export function createDespawnOneBypass( spawnId: string ) : string {
    return `bypass admin_${SpawnOperations.DespawnOne} ${ spawnId }`
}

export function createSpawnMoreBypass( spawnId: string ) : string {
    return `bypass admin_${SpawnOperations.SpawnMore} ${ spawnId }`
}

export function createViewSpawnBypass( spawnId: string ) : string {
    return `bypass -h admin_${SpawnOperations.ViewSpawn} ${ spawnId }`
}

export function createFindSpawnsByNameBypass( name: string ) : string {
    return `bypass -h admin_${SpawnOperations.FindSpawnsByNpcName} ${name}`
}

export function createFindSpawnsByIdBypass( npcId: number ) : string {
    return `bypass -h admin_${SpawnOperations.FindSpawnsByNpcId} ${npcId}`
}

export function createRemoveSpawnBypass( spawnId: string ) : string {
    return `bypass -h admin_${SpawnOperations.RemoveSpawn} ${ spawnId }`
}

export function createViewRegionSpawnsBypass( regionName: string ) : string {
    return `bypass -h admin_${SpawnOperations.ViewRegionSpawns} ${regionName.split( '_' ).join( ' ' )}`
}

export function createViewByTypeBypass( type: string ) : string {
    return `bypass -h admin_${SpawnOperations.ViewByType} ${type}`
}

export function createShowSpawnTerritoryBypass( territoryId: string ) : string {
    return `bypass admin_${SpawnOperations.ShowTerritoryPoints} ${territoryId}`
}

export const Spawn: IAdminCommand = {
    getCommandNames(): Array<string> {
        return [
            SpawnOperations.SpawnMenu,
            SpawnOperations.ViewSpawn,
            SpawnOperations.ViewObjectSpawn,
            SpawnOperations.DespawnObject,
            SpawnOperations.DespawnTarget,
            SpawnOperations.DespawnAll,
            SpawnOperations.DespawnOne,
            SpawnOperations.SpawnAll,
            SpawnOperations.SpawnMore,
            SpawnOperations.SpawnNpc,
            SpawnOperations.FindSpawnsByNpcName,
            SpawnOperations.FindSpawnsByNpcId,
            SpawnOperations.RemoveSpawn,
            SpawnOperations.ViewRegionSpawns,
            SpawnOperations.ViewByType,
            SpawnOperations.ShowTerritoryPoints,
        ]
    },

    getDescription( command: string ): Array<string> {
        switch ( command ) {
            case SpawnOperations.SpawnMenu:
                return [
                    formatDescription( SpawnOperations.SpawnMenu, 'Shows dialog for spawn operations' )
                ]

            case SpawnOperations.ViewSpawn:
                return [
                    formatOptionsDescription( SpawnOperations.ViewSpawn, '<spawnId>', 'View spawn information using npc id and id of spawn' ),
                    'Id can be omitted and then first found spawn data can be shown'
                ]

            case SpawnOperations.ViewObjectSpawn:
                return [
                    formatOptionsDescription( SpawnOperations.ViewSpawn, '<objectId>', 'View npc spawn information specified by npc objectId' ),
                ]

            case SpawnOperations.DespawnObject:
                return [
                    formatOptionsDescription( SpawnOperations.DespawnObject, '<objectId>', 'Force de-spawn of spawned npc defined by object id' )
                ]

            case SpawnOperations.DespawnTarget:
                return [
                    formatDescription( SpawnOperations.DespawnTarget, 'Force de-spawn of currently selected npc target' )
                ]

            case SpawnOperations.SpawnMore:
                return [
                    formatOptionsDescription( SpawnOperations.SpawnMore, '<spawnId> [ amount = 1 ]', 'Spawns more npcs per amount, in spawn defined by id' )
                ]

            case SpawnOperations.DespawnOne:
                return [
                    formatOptionsDescription( SpawnOperations.DespawnOne, '<spawnId>', 'Force de-spawn of single npc from spawn defined by id.' )
                ]

            case SpawnOperations.SpawnAll:
                return [
                    formatOptionsDescription( SpawnOperations.SpawnAll, '<spawnId>', 'Attempt to spawn all npcs in spawn defined by id' )
                ]

            case SpawnOperations.DespawnAll:
                return [
                    formatOptionsDescription( SpawnOperations.DespawnAll, '<spawnId>', 'Force de-spawn of all npcs in spawn specified by id' )
                ]

            case SpawnOperations.SpawnNpc:
                return [
                    formatOptionsDescription( SpawnOperations.SpawnNpc, '<npcId> <amount> <respawnMs>', 'Create npc spawn around current player position.' ),
                    'Single npc will be spawned near player. Multiple npcs will be spawned in circular area, centered on player position.',
                    'Amount can be any integer between zero and 500. Defaults to 1 otherwise.',
                    'RespawnMs can be zero to stop respawn, otherwise used as a way to respawn npc after death on specified amount of milliseconds.'
                ]

            case SpawnOperations.FindSpawnsByNpcName:
                return [
                    formatOptionsDescription( SpawnOperations.FindSpawnsByNpcName, '<partial name> [ pageIndex = 0 ]', 'Find existing spawns by npc name' ),
                    'Page index is optional parameter used to display additional results'
                ]

            case SpawnOperations.FindSpawnsByNpcId:
                return [
                    formatOptionsDescription( SpawnOperations.FindSpawnsByNpcId, '<npcId> [ pageIndex = 0 ]', 'Find existing spawns by npc id' ),
                    'Page index is optional parameter used to display additional results'
                ]

            case SpawnOperations.RemoveSpawn:
                return [
                    formatOptionsDescription( SpawnOperations.RemoveSpawn, '<spawnId>', 'Permanently removes spawn identified by id' ),
                    'If spawn has any npcs, all will be de-spawned before removal'
                ]

            case SpawnOperations.ViewRegionSpawns:
                return [
                    formatOptionsDescription( SpawnOperations.ViewRegionSpawns, '[ regionX ] [ regionY ] [ page = 1 ]', 'Show map region spawns' ),
                    'If region is not specified, current region is used.',
                    'Region examples: 11 12, 19 20, etc.',
                    'Page is optional, used to show paginated results.'
                ]

            case SpawnOperations.ViewByType:
                return [
                    formatOptionsDescription( SpawnOperations.ViewByType, '<type> [ page = 1 ]', 'Show spawns defined by type' ),
                    'Type can be npc template type, such as L2RaidBoss or L2Teleporter, etc.'
                ]

            case SpawnOperations.ShowTerritoryPoints:
                return [
                    formatOptionsDescription( SpawnOperations.ShowTerritoryPoints, '<territoryId>', 'Show spawn points for territory' )
                ]
        }
    },

    async onCommand( command: string, player: L2PcInstance ): Promise<void> {
        let commandChunks: Array<string> = command.split( ' ' )

        switch ( commandChunks[ 0 ] ) {
            case SpawnOperations.SpawnMenu:
                return AdminHtml.showHtml( player, 'overrides/html/command/spawn/spawn-menu.htm' )

            case SpawnOperations.ViewSpawn:
                return onViewSpawn( commandChunks, player )

            case SpawnOperations.ViewObjectSpawn:
                return onViewObjectSpawn( commandChunks, player )

            case SpawnOperations.DespawnObject:
                return onDespawnObject( commandChunks, player )

            case SpawnOperations.DespawnOne:
                return onDespawnOne( commandChunks, player )

            case SpawnOperations.DespawnAll:
                return onDespawnAll( commandChunks, player )

            case SpawnOperations.SpawnAll:
                return onSpawnAll( commandChunks, player )

            case SpawnOperations.SpawnMore:
                return onSpawnMore( commandChunks, player )

            case SpawnOperations.SpawnNpc:
                return onSpawnNpc( commandChunks, player )

            case SpawnOperations.DespawnTarget:
                return onDespawnTarget( player )

            case SpawnOperations.FindSpawnsByNpcName:
                return onFindByName( commandChunks, player )

            case SpawnOperations.FindSpawnsByNpcId:
                return onFindById( commandChunks, player )

            case SpawnOperations.RemoveSpawn:
                return onRemoveSpawn( commandChunks, player )

            case SpawnOperations.ViewRegionSpawns:
                return onViewRegionSpawns( commandChunks, player )

            case SpawnOperations.ViewByType:
                return onViewByType( commandChunks, player )

            case SpawnOperations.ShowTerritoryPoints:
                return onShowTerritoryPoints( commandChunks, player )
        }
    },
}

function createButtons( spawnId: string ) : string {
    return DataManager.getHtmlData().getItem( 'overrides/html/command/spawn/viewSpawnButtons.htm' )
        .replace( '%spawnMenu%', createSpawnMenuBypass() )
        .replace( '%despawnAll%', createDespawnAllBypass( spawnId ) )
        .replace( '%spawnAll%', createSpawnAllBypass( spawnId ) )
        .replace( '%despawnOne%', createDespawnOneBypass( spawnId ) )
        .replace( '%spawnMore%', createSpawnMoreBypass( spawnId ) )
        .replace( '%removeSpawn%', createRemoveSpawnBypass( spawnId ) )
}

function getLevelColor( level: number ) : string {
    if ( level < 10 ) {
        return LevelColors.Low
    }

    if ( level < 20 ) {
        return LevelColors.Lower
    }

    if ( level < 30 ) {
        return LevelColors.Normal
    }

    if ( level < 40 ) {
        return LevelColors.Middle
    }

    if ( level < 50 ) {
        return LevelColors.Medium
    }

    if ( level < 60 ) {
        return LevelColors.High
    }

    if ( level < 70 ) {
        return LevelColors.Higher
    }

    return LevelColors.Top
}

function showSpawnItem( spawn: ISpawnLogic, itemHtml: string ) : string {
    let template = spawn.getTemplate()
    let level = template.getLevel()
    let name = _.truncate( template.getName(), { length: 50 } )

    return itemHtml
        .replace( '%level%', level.toString() )
        .replace( '%name%', name )
        .replace( '%action%', createViewSpawnBypass( spawn.getId() ) )
        .replace( '%spawnColor%', spawn.hasSpawned() ? SpawnColors.Active : SpawnColors.Inactife )
        .replace( '%levelColor%', getLevelColor( level ) )
}

function getNumericParameters( parameters: Array<string> ) : Array<number> {
    let mappedValues = parameters.map( value => parseInt( value, 10 ) )
    if ( mappedValues.some( value => !Number.isInteger( value ) ) ) {
        return null
    }

    return mappedValues
}

function viewSpawnHtml( player: L2PcInstance, spawn: ISpawnLogic ) : void {
    let npcHtml = DataManager.getHtmlData().getItem( 'overrides/html/command/spawn/viewSpawnNpc.htm' )
    let npcItems : Array<string> = spawn.getNpcObjectIds().slice( 0, 20 ).map( ( objectId ) : string => {
        return npcHtml
            .replace( '%statsAction%', createGmStatsBypass( objectId ) )
            .replace( '%teleportAction%', createTeleportToObjectBypass( objectId ) )
            .replace( '%despawnAction%', createDespawnBypass( objectId ) )
    } )

    let territoryHtml = DataManager.getHtmlData().getItem( 'overrides/html/command/spawn/viewSpawnTerritory.htm' )
    let makerData = spawn.getMakerData()
    let territoryItems : Array<string> = makerData.spawnTerritoryIds.map( ( id: string ) : string => {
        let territory = SpawnMakerCache.getTerritory( id )
        let parameters = territory.data.parameters

        return territoryHtml
            .replace( '%region%', `${territory.data.regionX}_${territory.data.regionY}` )
            .replace( '%regionColor%', SpawnMakerCache.isRegionActiveForTerritoryId( id ) ? SpawnColors.Active : SpawnColors.Inactife )
            .replace( '%showAction%', createShowSpawnTerritoryBypass( id ) )
            .replace( '%teleportAction%', createTeleportToCoordinatesBypass( parameters.centerX, parameters.centerY, territory.data.maxZ ) )
            .replace( '%size%', territory.data.parameters.size.toString() )
    } )

    let template: L2NpcTemplate = spawn.getTemplate()
    let spawnData = spawn.getSpawnData()
    let position = spawnData.position
    let positionMessage = position ? `${position.x}, ${position.y}, ${position.z}` : 'None'
    let spawns = SpawnMakerCache.getAllSpawns( template.getId() )

    let html = DataManager.getHtmlData().getItem( 'overrides/html/command/spawn/viewSpawn.htm' )
        .replace( /%npcId%/g, template.getId().toString() )
        .replace( '%name%', template.getName() )
        .replace( '%type%', NpcTemplateType[ template.getType() ] )
        .replace( '%level%', template.getLevel().toString() )

        .replace( '%spawnAmount%', spawns.length.toString() )
        .replace( '%npcs%', npcItems.join( '' ) )
        .replace( '%makerId%', spawnData.makerId )
        .replace( '%amount%', spawn.getAmount().toString() )

        .replace( '%position%', positionMessage )
        .replace( '%respawn%', GeneralHelper.formatMillis( spawnData.respawnMs ?? 0 ).join( ', ' ).toString() )
        .replace( '%respawnRandom%', spawnData.respawnExtraMs ? GeneralHelper.formatMillis( spawnData.respawnExtraMs ).join( ', ' ).toString() : '0' )
        .replace( '%spawnLogic%', makerData.logicName ?? 'None' )

        .replace( '%returnDistance%', ( spawnData.returnDistance ?? 0 ).toString() )
        .replace( '%minionAmount%', ( spawnData.minions ? spawnData.minions.length : 0 ).toString() )
        .replace( '%buttons%', createButtons( spawn.getId() ) )
        .replace( '%npcAmount%', npcItems.length === spawn.getNpcObjectIds().length ? npcItems.length.toString() : `${npcItems.length} out of ${spawn.getNpcObjectIds().length}` )

        .replace( '%territoryAmount%', position ? `${makerData.spawnTerritoryIds.length} Unused` : makerData.spawnTerritoryIds.length.toString() )
        .replace( '%territories%', territoryItems.join( '' ) )
        .replace( '%heading%', position ? ( position.heading ?? 0 ).toString() : 'None' )

    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId() ) )
}

function onViewSpawn( commandChunks: Array<string>, player: L2PcInstance ) : void {
    let selectedSpawn : ISpawnLogic = getSpawnFromParameters( commandChunks, player )
    if ( !selectedSpawn ) {
        return
    }

    return viewSpawnHtml( player, selectedSpawn )
}

function onViewObjectSpawn( commandChunks: Array<string>, player: L2PcInstance ) : void {
    let idValue = commandChunks[ 1 ]

    if ( !idValue ) {
        return player.sendMessage( 'Please provide object id.' )
    }

    let objectId = parseInt( idValue, 10 )
    if ( !objectId ) {
        return player.sendMessage( 'Please provide numeric object id.' )
    }

    let npc = L2World.getObjectById( objectId ) as L2Npc
    if ( !npc || !npc.isNpc() ) {
        return player.sendMessage( 'Invalid object id' )
    }

    let spawn = npc.getNpcSpawn()
    if ( !spawn ) {
        return player.sendMessage( 'Npc has no spawn' )
    }

    return viewSpawnHtml( player, spawn )
}

function despawnNpc( objectId: number ) : Promise<void> {
    let npc = L2World.getObjectById( objectId ) as L2Npc
    if ( !npc || !npc.isNpc() ) {
        return
    }

    return npc.deleteMe()
}

function getSpawnFromParameters( commandChunks: Array<string>, player: L2PcInstance ) : ISpawnLogic {
    if ( commandChunks.length === 1 ) {
        return
    }

    let spawnId = commandChunks[ 1 ]

    let selectedSpawn : ISpawnLogic = SpawnMakerCache.getSpawnById( spawnId )
    if ( !selectedSpawn ) {
        player.sendMessage( `No valid spawn found with id=${spawnId}` )
        return
    }

    return selectedSpawn
}

function onDespawnObject( commandChunks: Array<string>, player: L2PcInstance ) : Promise<void> {
    let idValue = commandChunks[ 1 ]

    if ( !idValue ) {
        player.sendMessage( 'Please provide object id.' )
        return
    }

    let objectId = parseInt( idValue, 10 )
    if ( !objectId ) {
        player.sendMessage( 'Invalid object id.' )
        return
    }

    return despawnNpc( objectId )
}

function onDespawnOne( commandChunks: Array<string>, player: L2PcInstance ) : Promise<void> {
    let selectedSpawn = getSpawnFromParameters( commandChunks, player )
    if ( !selectedSpawn ) {
        return
    }

    if ( !selectedSpawn.hasSpawned() ) {
        return
    }

    return despawnNpc( _.sample( selectedSpawn.getNpcObjectIds() ) )
}

function onDespawnAll( commandChunks: Array<string>, player: L2PcInstance ) : void {
    let selectedSpawn = getSpawnFromParameters( commandChunks, player )
    if ( !selectedSpawn ) {
        return
    }

    if ( !selectedSpawn.hasSpawned() ) {
        return
    }

    return selectedSpawn.startDespawn()
}

function onSpawnAll( commandChunks: Array<string>, player: L2PcInstance ) : void {
    let selectedSpawn = getSpawnFromParameters( commandChunks, player )
    if ( !selectedSpawn ) {
        return
    }

    if ( selectedSpawn.hasSpawned() ) {
        return
    }

    let amount = selectedSpawn.startSpawn()

    player.sendMessage( `Spawned ${amount} of npcs for npcId=${selectedSpawn.getTemplate().getId()}` )
}

function onSpawnMore( commandChunks: Array<string>, player: L2PcInstance ) : void {
    let selectedSpawn = getSpawnFromParameters( commandChunks, player )
    if ( !selectedSpawn ) {
        return
    }

    let amount = parseInt( commandChunks[ 2 ] )
    if ( !Number.isInteger( amount ) || amount > 500 || amount <= 0 ) {
        amount = 1
    }

    /*
        TODO: use spawn points if position is absolute
        - spawning additional npcs on absolute position does is not practical
        - use spawn points derived from original absolute position
        - no need to create territory spawn points
     */
    let spawnData = structuredClone( selectedSpawn.getSpawnData() )
    spawnData.amount = amount

    let additionalSpawn = new L2DefaultSpawn( spawnData, {
        ...selectedSpawn.getMakerData(),
        maxAmount: amount,
    }, {
        count: 0
    } )

    let spawnedAmount = additionalSpawn.startSpawn()
    player.sendMessage( `Spawned ${spawnedAmount} of npcs for npcId=${additionalSpawn.getTemplate().getId()}` )

    SpawnMakerCache.addDynamicSpawn( additionalSpawn )
}

function onSpawnNpc( commandChunks: Array<string>, player: L2PcInstance ) : void {
    let values = getNumericParameters( commandChunks.slice( 1 ) )
    if ( !values ) {
        return
    }

    let [ npcId, amount, respawnMs ] = values
    let template = DataManager.getNpcData().getTemplate( npcId )
    if ( !template ) {
        return player.sendMessage( `No npc data found for id=${npcId}` )
    }

    if ( !Number.isInteger( amount ) ) {
        return player.sendMessage( 'Please specify amount' )
    }

    if ( !Number.isInteger( respawnMs ) ) {
        return player.sendMessage( 'Please specify respawn interval in ms' )
    }

    if ( amount > 500 || amount <= 0 ) {
        amount = 1
    }

    if ( respawnMs < 0 ) {
        respawnMs = 0
    }

    // TODO : implement custom spawn using geometry
    player.sendMessage( 'Command not implemented' )
}

function onDespawnTarget( player: L2PcInstance ) : Promise<void> {
    let target = player.getTarget()

    if ( !target || !target.isNpc() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_IS_INCORRECT ) )
        return
    }

    return despawnNpc( target.getObjectId() )
}

function displaySearchResults( player: L2PcInstance, npcIds: Array<number>, currentPage : number, title: string, paginationMethod: ( pages: number ) => string ) : void {
    let allSpawns : Array<ISpawnLogic> = npcIds.flatMap( ( npcId: number ) : Array<ISpawnLogic> => {
        return SpawnMakerCache.getAllSpawns( npcId ) ?? []
    } )

    let paginationIndex = ( currentPage && currentPage > 0 ) ? currentPage - 1 : 0
    let paginatedSpawns = _.chunk( allSpawns, pageLimit )
    let currentPageSpawns = paginatedSpawns[ paginationIndex ]
    if ( !currentPageSpawns ) {
        return player.sendMessage( `Cannot display items page ${ paginationIndex } for item search name '${ title }'.` )
    }

    let itemHtml = DataManager.getHtmlData().getItem( 'overrides/html/command/spawn/showSpawns-item.htm' )
    let items : Array<string> = currentPageSpawns.map( ( spawn: ISpawnLogic ) : string => showSpawnItem( spawn, itemHtml ) )

    let truncatedName = _.truncate( title, { length: 50 } )
    let html = DataManager.getHtmlData().getItem( 'overrides/html/command/spawn/showSpawns.htm' )
        .replace( '%search%', `${truncatedName} (p.${paginationIndex + 1})` )
        .replace( '%pagination%', paginationMethod( paginatedSpawns.length ) )
        .replace( '%items%', items.join( '' ) )

    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId() ) )
}

function onFindByName( commandChunks: Array<string>, player: L2PcInstance ) : void {
    if ( commandChunks.length === 1 ) {
        return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.PLEASE_WRITE_NAME_AFTER_COMMAND ) )
    }

    let pageValue = _.last( commandChunks )
    let currentPage = parseInt( pageValue, 10 )
    let npcName = ( currentPage ? commandChunks.slice( 1, commandChunks.length - 1 ).join( ' ' ) : commandChunks.slice( 1 ).join( ' ' ) ).trim()
    if ( !npcName ) {
        return player.sendMessage( 'Please specify non-empty item name for search.' )
    }

    if ( npcName.length <= 3 ) {
        return player.sendMessage( 'Please specify at least three letter name for search.' )
    }

    let npcIds : Array<number> = DataManager.getNpcData().getTemplateIdsByName( npcName, L2NpcDataTemplateOrdering.Name )
    if ( npcIds.length === 0 ) {
        return player.sendMessage( `No spawns found for name: ${npcName}` )
    }

    return displaySearchResults( player, npcIds, currentPage, npcName, ( pages: number ) => createPagination( pages, createFindSpawnsByNameBypass( npcName ) ) )
}

function onFindById( commandChunks: Array<string>, player: L2PcInstance ) : void {
    if ( commandChunks.length === 1 ) {
        return player.sendMessage( 'Please specify npc id' )
    }

    let [ , idValue, pageValue ] = commandChunks

    let npcId = parseInt( idValue, 10 )
    if ( !npcId || npcId <= 0 ) {
        return player.sendMessage( 'Please specify positive integer value for npc id' )
    }

    let template = DataManager.getNpcData().getTemplate( npcId )
    if ( !template ) {
        return player.sendMessage( `No valid npc template found for id=${npcId}` )
    }

    let currentPage = pageValue ? parseInt( pageValue, 10 ) : 1

    if ( !currentPage || currentPage <= 0 ) {
        currentPage = 1
    }

    return displaySearchResults( player, [ npcId ], currentPage, idValue, ( pages: number ) => createPagination( pages, createFindSpawnsByIdBypass( npcId ) ) )
}

async function onRemoveSpawn( commandChunks: Array<string>, player: L2PcInstance ) : Promise<void> {
    let selectedSpawn = getSpawnFromParameters( commandChunks, player )
    if ( !selectedSpawn ) {
        return
    }

    if ( selectedSpawn.hasSpawned() ) {
        await Promise.all( selectedSpawn.getNpcObjectIds().map( objectId => despawnNpc( objectId ) ) )
    }

    /*
        Certain spawns subscribe to events which need to be removed, when spawn should no longer exist.
        Two most used cases are included here.
     */
    if ( selectedSpawn instanceof L2DefaultSpawn ) {
        ( selectedSpawn as L2DefaultSpawn ).deInitializeSpawn()
    }

    if ( selectedSpawn instanceof L2ManualSpawn ) {
        ( selectedSpawn as L2ManualSpawn ).deInitializeSpawn()
    }

    SpawnMakerCache.removeSpawn( selectedSpawn )
}

function onViewRegionSpawns( commandChunks: Array<string>, player: L2PcInstance ) : void {
    let regionCode: number
    let currentPage: number = commandChunks.length === 4 ? parseInt( commandChunks[ 3 ], 10 ) : 1

    if ( commandChunks.length > 2 ) {
        let x = parseInt( commandChunks[ 1 ], 10 )
        let y = parseInt( commandChunks[ 2 ], 10 )
        if ( Number.isInteger( x ) && Number.isInteger( y ) ) {
            regionCode = getRegionCode( x,y )
        }
    }

    if ( !regionCode ) {
        regionCode = player.getWorldRegion().code
    }

    let regionName = getNameFromCode( regionCode )
    let spawns = SpawnMakerCache.getRegionSpawns( regionCode )
    if ( !spawns ) {
        return player.sendMessage( `No spawns found for region=${regionName}` )
    }

    let paginationIndex = ( Number.isInteger( currentPage ) && currentPage > 0 ) ? currentPage - 1 : 0
    let paginatedSpawns = _.chunk( spawns, pageLimit )
    let currentPageSpawns = paginatedSpawns[ paginationIndex ]
    if ( !currentPageSpawns ) {
        return player.sendMessage( `Cannot display items page ${ paginationIndex } for region=${ regionName }` )
    }

    let itemHtml = DataManager.getHtmlData().getItem( 'overrides/html/command/spawn/showSpawns-item.htm' )
    let items : Array<string> = currentPageSpawns.map( ( spawn: ISpawnLogic ) => showSpawnItem( spawn, itemHtml ) )

    let html = DataManager.getHtmlData().getItem( 'overrides/html/command/spawn/showSpawns.htm' )
        .replace( '%search%', `Region: ${regionName} (p.${paginationIndex + 1})` )
        .replace( '%pagination%', createPagination( paginatedSpawns.length, createViewRegionSpawnsBypass( regionName ) ) )
        .replace( '%items%', items.join( '' ) )

    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId() ) )
}

function onViewByType( commandChunks: Array<string>, player: L2PcInstance ) : void {
    if ( commandChunks.length === 1 ) {
        return player.sendMessage( 'Please specify npc template type' )
    }

    let type = commandChunks[ 1 ]
    let currentPage: number = commandChunks.length === 3 ? parseInt( commandChunks[ 2 ], 10 ) : 1

    let templateType = NpcTemplateType[ type ]
    if ( !type || !Number.isInteger( templateType ) ) {
        return player.sendMessage( 'Please specify correct npc template type name' )
    }

    let templates : Array<L2NpcTemplate> = DataManager.getNpcData().getTemplatesByType( templateType )

    let npcIds = templates.map( template => template.getId() )
    return displaySearchResults( player, npcIds, currentPage, `Type: ${type}`, ( pages ) => createPagination( pages, createViewByTypeBypass( type ) ) )
}

function onShowTerritoryPoints( commandChunks: Array<string>, player: L2PcInstance ) : void {
    if ( commandChunks.length === 1 ) {
        return player.sendMessage( 'Please specify territory id' )
    }

    let territory = SpawnMakerCache.getTerritory( commandChunks[ 1 ] )
    if ( !territory ) {
        return player.sendMessage( 'Please specify existing territory id' )
    }

    return drawGeometry( player, territory )
}

function getPacketName( index: number ) : string {
    return `${SpawnOperations.ShowTerritoryPoints}${index}`
}

function clearExistingDrawing( player: L2PcInstance ) : void {
    DrawHelper.clearExistingPacket( player, SpawnOperations.ShowTerritoryPoints )

    for ( let index = 1; index <= 10; index++ ) {
        DrawHelper.clearExistingPacket( player, getPacketName( index ) )
    }
}

function drawGeometry( player: L2PcInstance, territory: L2SpawnTerritory ) : void {
    clearExistingDrawing( player )

    let packet = DrawHelper.getDrawPacket( player, SpawnOperations.ShowTerritoryPoints )

    let points = territory.clone()
    let writtenAmount = 0
    let tries = 0

    while ( !points.hasNoPoints() ) {
        points.generatePoint()

        let x = points.getX()
        let y = points.getY()
        let z = GeoPolygonCache.getZ( x, y, points.data.maxZ )

        packet.addPoint( '', DrawColor.Normal, true, x, y, z )

        writtenAmount++

        /*
            It is possible for geometry points to exceed maximum packet size limit,
            hence we need to send points in multiple packets.
         */
        if ( writtenAmount > 1024 ) {
            writtenAmount = 0
            player.sendOwnedData( packet.getBuffer() )

            tries++
            packet = DrawHelper.getDrawPacket( player, getPacketName( tries ) )

            if ( tries > 10 ) {
                return player.sendMessage( 'Reached limit of drawing. More points available however cannot be shown.' )
            }
        }
    }


    if ( writtenAmount > 0 ) {
        player.sendOwnedData( packet.getBuffer() )
    }
}