import { IAdminCommand } from '../IAdminCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { formatDescription, formatOptionsDescription } from './helpers/FormatHelpers'
import { AdminHtml } from './helpers/AdminHtml'
import { L2MoveManager } from '../../L2MoveManager'
import { L2Character } from '../../models/actor/L2Character'
import { DrawHelper } from './helpers/DrawHelper'
import { L2World } from '../../L2World'
import { ValidateLocation } from '../../packets/send/ValidateLocation'

const enum MovementCommands {
    Menu = 'movement-menu',
    Trace = 'movement-trace',
    Packet = 'movement-packet'
}

const enum Colors {
    Trace = 0x23FF31,
    TraceEnds = 0xF14300
}


export const Movement : IAdminCommand = {
    getCommandNames(): Array<string> {
        return [
            MovementCommands.Menu,
            MovementCommands.Trace,
            MovementCommands.Packet,
        ]
    },

    getDescription( command: string ): Array<string> {
        switch ( command ) {
            case MovementCommands.Menu:
                return [
                    formatDescription( MovementCommands.Menu, 'Show movement options' )
                ]

            case MovementCommands.Trace:
                return [
                    formatDescription( MovementCommands.Trace, 'Toggles on/off trace of current player movement.' )
                ]

            case MovementCommands.Packet:
                return [
                    formatOptionsDescription( MovementCommands.Packet, '<name>', 'Send movement/position packet from server to client' )
                ]
        }
    },

    async onCommand( command: string, player: L2PcInstance ): Promise<void> {
        const commandChunks = command.split( ' ' )
        switch ( commandChunks[ 0 ] ) {
            case MovementCommands.Menu:
                return AdminHtml.showHtml( player, 'overrides/html/command/movement/menu.htm' )

            case MovementCommands.Trace:
                return onTrace( player )

            case MovementCommands.Packet:
                return onPacket( commandChunks, player )
        }
    }
}

function onStartTracingMovement( character: L2Character ) : void {
    let packet = DrawHelper.getDrawPacket( character )

    packet.addLine( '', Colors.Trace, true, character.getX(), character.getY(), character.getZ(), character.getXDestination(), character.getYDestination(), character.getZDestination() )
    packet.addPoint( `Start: ${character.getX()},${character.getY()},${character.getZ()}`, Colors.TraceEnds, true, character.getX(), character.getY(), character.getZ() + 20 )
    packet.addPoint( `End: ${character.getXDestination()},${character.getYDestination()},${character.getZDestination()}`, Colors.TraceEnds, true, character.getXDestination(), character.getYDestination(), character.getZDestination() + 20 )

    let packetData = packet.getBuffer()
    if ( character.isPlayer() ) {
        character.sendOwnedData( packetData )
        return
    }

    L2World.forEachPlayerByPredicate( character, 1000, ( player: L2PcInstance ) => {
        if ( player.isGM() ) {
            player.sendCopyData( packetData )
        }
    }, false )
}

function onTrace( player: L2PcInstance ) : void {
    if ( L2MoveManager.hasMovementHook( player.getMovementId(), onStartTracingMovement ) ) {
        L2MoveManager.removeMovementHook( player.getMovementId(), onStartTracingMovement )

        player.sendMessage( `${MovementCommands.Trace} : Trace movement has been disabled` )

        return DrawHelper.clearPackets( player.getObjectId() )
    }

    L2MoveManager.addMovementHook( player.getMovementId(), onStartTracingMovement )
    player.sendMessage( `${MovementCommands.Trace} : Trace movement has been enabled` )
}

function onPacket( commandChunks: Array<string>, player: L2PcInstance ) : void {
    switch ( commandChunks[ 1 ] ) {
        case ValidateLocation.name:
            player.sendOwnedData( ValidateLocation( player ) )
            return
    }
}