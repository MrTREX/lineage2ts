import { IAdminCommand } from '../IAdminCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Character } from '../../models/actor/L2Character'
import { L2World } from '../../L2World'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import {
    EventType,
    GMHealPlayersEvent,
    GMReduceHpEvent,
    GMSetHpEvent,
    GMSetMpEvent
} from '../../models/events/EventType'
import { ListenerCache } from '../../cache/ListenerCache'
import { L2Object } from '../../models/L2Object'
import { formatOptionsDescription } from './helpers/FormatHelpers'
import aigle from 'aigle'
import _ from 'lodash'

const enum HealCommands {
    HealTargetPlayer = 'heal',
    HealInRadius = 'heal-radius',
    SetTargetHp = 'set-hp',
    ReduceTargetHp = 'reduce-hp',
    ReduceRadiusHp = 'reduce-radius-hp',
    SetTargetHpPercent = 'set-hp-percent',
    SetTargetMpPercent = 'set-mp-percent',
}

export const Heal: IAdminCommand = {
    getCommandNames(): Array<string> {
        return [
            HealCommands.HealTargetPlayer,
            HealCommands.HealInRadius,
            HealCommands.SetTargetHp,
            HealCommands.ReduceTargetHp,
            HealCommands.ReduceRadiusHp,
            HealCommands.SetTargetHpPercent,
            HealCommands.SetTargetMpPercent,
        ]
    },

    getDescription( command: string ): Array<string> {
        switch ( command ) {
            case HealCommands.ReduceRadiusHp:
                return [
                    formatOptionsDescription( HealCommands.ReduceRadiusHp, '< radius > < amount > [fatal = true]', 'Reduces hp of mobs/attackable/players in radius, killing characters when applicable by default.' ),
                    `Example: //${ HealCommands.ReduceRadiusHp } 500 1000 false`,
                    'Reduces hp in 500 radius by 1000, however does not kill characters, setting HP to 1',
                ]

            case HealCommands.HealTargetPlayer:
                return [
                    formatOptionsDescription( HealCommands.HealTargetPlayer, '[player name]', 'Restores max HP and MP (and CP if player) for targeted player' ),
                    'Player name can is supplied if target should not be used',
                ]

            case HealCommands.HealInRadius:
                return [
                    formatOptionsDescription( HealCommands.HealInRadius, '[raidius = 500]', 'Restores max HP/MP/CP for players in radius' ),
                ]

            case HealCommands.SetTargetHp:
                return [
                    formatOptionsDescription( HealCommands.SetTargetHp, '< value >', 'Set target HP to specified value, range is from 1 till maximum integer value' ),
                ]

            case HealCommands.ReduceTargetHp:
                return [
                    formatOptionsDescription( HealCommands.ReduceTargetHp, '< amount > [fatal = true]', 'Reduce target HP by specified amount' ),
                ]

            case HealCommands.SetTargetHpPercent:
                return [
                    formatOptionsDescription( HealCommands.SetTargetHpPercent, '< value >', 'Set target HP to percentage value of maximum HP, range is from 0 to 100. Value 0 causes character death.' ),
                ]

            case HealCommands.SetTargetMpPercent:
                return [
                    formatOptionsDescription( HealCommands.SetTargetMpPercent, '< value >', 'Set target MP to percentage value of maximum MP, range is from 0 to 100.' ),
                ]
        }
    },

    async onCommand( command: string, player: L2PcInstance ): Promise<void> {
        let commandChunks: Array<string> = _.split( command, ' ' )
        let currentCommand: string = _.head( commandChunks )

        switch ( currentCommand ) {
            case HealCommands.HealTargetPlayer:
                return onHeal( commandChunks, player )

            case HealCommands.HealInRadius:
                return onHealRadius( commandChunks, player )

            case HealCommands.SetTargetHp:
                return onSetHp( commandChunks, player )

            case HealCommands.ReduceTargetHp:
                return onReduceHp( commandChunks, player )

            case HealCommands.ReduceRadiusHp:
                return onReduceRadiusHp( commandChunks, player )

            case HealCommands.SetTargetHpPercent:
                return onSetHpPercent( commandChunks, player )

            case HealCommands.SetTargetMpPercent:
                return onSetMpPercent( commandChunks, player )
        }
    },
}

function healCharacter( target: L2Character ): void {
    target.setMaxStats()

    if ( target.isPlayer() ) {
        ( target as L2PcInstance ).setCurrentCp( target.getMaxCp() )
    }
}

function sendFullHealEvent( player: L2PcInstance, targets: Array<number> ) {
    if ( !ListenerCache.hasGeneralListener( EventType.GMHealPlayers ) ) {
        return
    }

    let data: GMHealPlayersEvent = {
        originatorId: player.getObjectId(),
        targetIds: targets,
    }

    ListenerCache.sendGeneralEvent( EventType.GMHealPlayers, data )
}

function onHeal( commandChunks: Array<string>, player: L2PcInstance ): void {
    let [ currentCommand, playerName ] = commandChunks

    if ( playerName ) {
        let otherPlayer = L2World.getPlayerByName( playerName )
        if ( !otherPlayer ) {
            return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
        }

        healCharacter( otherPlayer )
        return sendFullHealEvent( player, [ otherPlayer.getObjectId() ] )
    }

    let target = player.getTarget()
    if ( !target ) {
        return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
    }

    if ( !target.isCharacter() ) {
        return
    }

    healCharacter( target as L2Character )
    target.broadcastInfo()
    return sendFullHealEvent( player, [ target.getObjectId() ] )
}

function onHealRadius( commandChunks: Array<string>, player: L2PcInstance ): void {
    let [ currentCommand, radiusValue ] = commandChunks
    let radius = radiusValue ? _.parseInt( radiusValue ) : 500

    if ( radius > 10000 ) {
        return
    }

    let healedPlayerIds: Array<number> = []
    _.each( L2World.getVisiblePlayers( player, Math.max( radius, 10 ) ), ( otherPlayer: L2PcInstance ) => {
        if ( otherPlayer ) {
            healCharacter( otherPlayer )
            healedPlayerIds.push( otherPlayer.getObjectId() )
        }
    } )

    player.sendMessage( `Healed ${ healedPlayerIds.length } players in radius of ${ radius }` )

    if ( healedPlayerIds.length > 0 ) {
        sendFullHealEvent( player, healedPlayerIds )
    }
}

function onSetHp( commandChunks: Array<string>, player: L2PcInstance ): void {
    let [ currentCommand, hpValue ] = commandChunks

    if ( !hpValue ) {
        return
    }

    let target = player.getTarget() as L2Character
    if ( !target ) {
        return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
    }

    if ( !target.isAttackable() && !target.isPlayable() ) {
        return
    }

    let amount = hpValue ? _.parseInt( hpValue ) : 0

    if ( amount <= 0 ) {
        return
    }

    if ( target.isDead() && amount > 0 ) {
        target.doRevive()
    }

    ( target as L2Character ).setCurrentHp( amount )
    target.broadcastInfo()

    if ( ListenerCache.hasGeneralListener( EventType.GMSetHp ) ) {
        let data: GMSetHpEvent = {
            originatorId: player.getObjectId(),
            targetId: target.getObjectId(),
            hp: amount,
        }

        ListenerCache.sendGeneralEvent( EventType.GMSetHp, data )
    }
}

async function onReduceHp( commandChunks: Array<string>, player: L2PcInstance ): Promise<void> {
    let [ currentCommand, amountValue, isFatalValue ] = commandChunks

    let target = player.getTarget() as L2Character
    if ( !target ) {
        return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
    }

    if ( !isTargetForReduceHp( target ) ) {
        return
    }

    let amount: number = _.defaultTo( _.parseInt( amountValue ), 0 )

    if ( amount <= 0 ) {
        return
    }

    let adjustedValue = _.toLower( isFatalValue ) === 'false' ? Math.min( target.getMaxHp() - 1, amount ) : amount

    await ( target as L2Character ).getStatus().reduceHp( adjustedValue, player, false, false, false )
    target.broadcastInfo()

    if ( ListenerCache.hasGeneralListener( EventType.GMReduceHp ) ) {
        let data: GMReduceHpEvent = {
            originatorId: player.getObjectId(),
            targetIds: [ target.getObjectId() ],
            amount,
        }

        return ListenerCache.sendGeneralEvent( EventType.GMReduceHp, data )
    }
}

function onSetHpPercent( commandChunks: Array<string>, player: L2PcInstance ): Promise<void> {
    let target = player.getTarget() as L2Character
    if ( !target ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
        return
    }

    if ( !target.isAttackable() && !target.isPlayable() ) {
        return
    }

    let [ currentCommand, amountValue ] = commandChunks
    let amount: number = _.defaultTo( parseFloat( amountValue ), 1 )

    if ( amount < 0 && amount > 100 ) {
        return
    }

    if ( target.isDead() && amount > 0 ) {
        target.doRevive()
    }

    let expectedHp = Math.floor( target.getMaxHp() * ( amount / 100 ) )

    target.setCurrentHp( expectedHp )
    target.broadcastInfo()

    if ( ListenerCache.hasGeneralListener( EventType.GMSetHp ) ) {
        let data: GMSetHpEvent = {
            originatorId: player.getObjectId(),
            targetId: target.getObjectId(),
            hp: expectedHp,
        }

        return ListenerCache.sendGeneralEvent( EventType.GMSetHp, data )
    }
}

function onSetMpPercent( commandChunks: Array<string>, player: L2PcInstance ): Promise<void> {
    let target = player.getTarget() as L2Character
    if ( !target ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
        return
    }

    if ( !target.isAttackable() && !target.isPlayable() ) {
        return
    }

    let [ currentCommand, amountValue ] = commandChunks
    let amount: number = _.defaultTo( parseFloat( amountValue ), 1 )

    if ( amount < 0 && amount > 100 ) {
        return
    }

    let expectedMp = Math.floor( target.getMaxMp() * ( amount / 100 ) )

    target.setCurrentMp( expectedMp )
    target.broadcastInfo()

    if ( ListenerCache.hasGeneralListener( EventType.GMSetMp ) ) {
        let data: GMSetMpEvent = {
            originatorId: player.getObjectId(),
            targetId: target.getObjectId(),
            mp: expectedMp,
        }

        return ListenerCache.sendGeneralEvent( EventType.GMSetMp, data )
    }
}

async function onReduceRadiusHp( commandChunks: Array<string>, player: L2PcInstance ): Promise<void> {
    let [ currentCommand, radiusValue, amountValue, isFatalValue ] = commandChunks

    let radius: number = _.defaultTo( _.parseInt( radiusValue ), 0 )

    if ( radius <= 0 && radius > 10000 ) {
        return
    }

    let amount: number = _.defaultTo( _.parseInt( amountValue ), 0 )

    if ( amount <= 0 ) {
        return
    }

    let affectedObjectIds: Array<number> = []
    await aigle.resolve( L2World.getVisibleObjectsByPredicate( player, radius, isTargetForReduceHp ) ).eachLimit( 10, async ( object: L2Object ) => {
        let target = ( object as L2Character )
        let adjustedValue = _.toLower( isFatalValue ) === 'false' ? Math.min( target.getMaxHp() - 1, amount ) : amount

        await target.getStatus().reduceHp( adjustedValue, player, false, false, false )
        affectedObjectIds.push( object.getObjectId() )
    } )

    if ( ListenerCache.hasGeneralListener( EventType.GMReduceHp ) && affectedObjectIds.length > 0 ) {
        let data: GMReduceHpEvent = {
            originatorId: player.getObjectId(),
            targetIds: affectedObjectIds,
            amount,
        }

        await ListenerCache.sendGeneralEvent( EventType.GMReduceHp, data )
    }

    player.sendMessage( `Affected ${ affectedObjectIds.length } characters in ${ radius } radius` )
}

function isTargetForReduceHp( object: L2Object ): boolean {
    return object && ( object.isAttackable() || object.isPlayable() )
}