import { IAdminCommand } from '../IAdminCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { formatDescription, formatOptionsDescription } from './helpers/FormatHelpers'
import { DataManager } from '../../../data/manager'
import { L2GeometryDataItem } from '../../../data/interface/GeometryDataApi'
import { PointGeometry } from '../../models/drops/PointGeometry'
import { GeometryId } from '../../enums/GeometryId'
import { DrawHelper } from './helpers/DrawHelper'
import { GeoPolygonCache } from '../../cache/GeoPolygonCache'
import { ILocational } from '../../models/Location'
import { NpcHtmlMessage } from '../../packets/send/NpcHtmlMessage'
import _ from 'lodash'

const enum Commands {
    Menu = 'geometry-menu',
    ShowOnPlayer = 'geometry-show',
    ShowOnTarget = 'geometry-show-target',
    Info = 'geometry-info'
}

const enum DrawColor {
    Normal = 0x26F196
}

const enum GeometryValues {
    PacketName = 'geometry'
}

/*
    Due to how enums are being used, we cannot simply filter out ids during run time on non-const enums,
    the only way is to enumerate these here.
 */
const AllGeometryIds : Array<string> = [
    GeometryId.MobSmall,
    GeometryId.MobMedium,
    GeometryId.MobLarge,
    GeometryId.RaidbossTiny,
    GeometryId.RaidbossSmall,
    GeometryId.RaidbossMedium,
    GeometryId.RaidbossLarge,
    GeometryId.RaidbossExtraLarge,
    GeometryId.GrandbossSmall,
    GeometryId.GrandbossMedium,
    GeometryId.GrandbossLarge,
    GeometryId.PlayerDrop,
    GeometryId.RaidbossSpawnSmall,
    GeometryId.RaidbossSpawnMedium,

    GeometryId.NpcSpawnSmall,
    GeometryId.NpcSpawnMedium,
    GeometryId.NpcSpawnLarge,
    GeometryId.NpcSpawnXLarge,

    GeometryId.NpcMoveTiny,
    GeometryId.NpcMoveSmall,
    GeometryId.NpcMoveMedium,
    GeometryId.NpcMoveLarge,
    GeometryId.NpcMoveXLarge,
    GeometryId.NpcMoveXXLarge,
    GeometryId.NpcMoveHuge,
    GeometryId.NpcMoveXHuge,

    GeometryId.PartyTeleport,
    GeometryId.ZonePartyTeleport,
    GeometryId.SpellTeleport,
    GeometryId.ValakasTeleport,

    GeometryId.PetMoveSmall,
    GeometryId.Geo256
]

export const GeometryCommands : IAdminCommand = {
    getCommandNames(): Array<string> {
        return [
            Commands.Menu,
            Commands.ShowOnPlayer,
            Commands.ShowOnTarget,
            Commands.Info
        ]
    },

    getDescription( command: string ): Array<string> {
        switch ( command ) {
            case Commands.Menu:
                return [
                    formatDescription( Commands.Menu, 'Show available actions for point geometry' )
                ]

            case Commands.ShowOnPlayer:
                return [
                    formatOptionsDescription( Commands.ShowOnPlayer, '<name>', 'Display point geometry, specified by name, centered around player' )
                ]

            case Commands.ShowOnTarget:
                return [
                    formatOptionsDescription( Commands.ShowOnTarget, '<name>', 'Display point geometry, specified by name, centered around selected target' )
                ]

            case Commands.Info:
                return [
                    formatOptionsDescription( Commands.Info, '<name>', 'Display point geometry information specified by name' )
                ]
        }
    },

    async onCommand( command: string, player: L2PcInstance ): Promise<void> {
        const commandChunks : Array<string> = command.split( ' ' )
        switch ( commandChunks[ 0 ] ) {
            case Commands.Menu:
                return onMenu( player )

            case Commands.Info:
                return onInfo( commandChunks, player )

            case Commands.ShowOnPlayer:
                return onPlayer( commandChunks, player )

            case Commands.ShowOnTarget:
                return onTarget( commandChunks, player )
        }
    }
}

export function createGeometryInfoBypass( name: string ) : string {
    return `bypass -h admin_${Commands.Info} ${name}`
}

function getGeometryName( commandChunks: Array<string>, player: L2PcInstance ) : L2GeometryDataItem {
    let name = commandChunks[ 1 ]
    if ( !name ) {
        player.sendMessage( 'Please specify geometry name' )
        return null
    }

    let geometry = DataManager.getGeometry().getItem( name )
    if ( !geometry ) {
        player.sendMessage( 'Please specify valid geometry name' )
        return null
    }

    return geometry
}

function getPacketName( index: number ) : string {
    return `${GeometryValues.PacketName}${index}`
}

function clearExistingDrawing( player: L2PcInstance ) : void {
    DrawHelper.clearExistingPacket( player, GeometryValues.PacketName )

    for ( let index = 1; index <= 10; index++ ) {
        DrawHelper.clearExistingPacket( player, getPacketName( index ) )
    }
}

function drawGeometry( player: L2PcInstance, target: ILocational, geometry: PointGeometry ) : void {
    clearExistingDrawing( player )

    let packet = DrawHelper.getDrawPacket( player, GeometryValues.PacketName )

    geometry.refresh()

    let positionX = target.getX()
    let positionY = target.getY()
    let positionZ = target.getZ()
    let writtenAmount = 0
    let tries = 0

    let totalPoints = geometry.size()
    let startTime = Date.now()

    while ( geometry.size() > 0 ) {
        geometry.prepareNextPoint()

        let x = positionX + geometry.getX()
        let y = positionY + geometry.getY()
        let z = GeoPolygonCache.getZ( x, y, positionZ )

        packet.addPoint( '', DrawColor.Normal, true, x, y, z )

        writtenAmount++

        /*
            It is possible for geometry points to exceed maximum packet size limit,
            hence we need to send points in multiple packets.
         */
        if ( writtenAmount > 1024 ) {
            writtenAmount = 0
            player.sendOwnedData( packet.getBuffer() )

            tries++
            packet = DrawHelper.getDrawPacket( player, getPacketName( tries ) )

            if ( tries > 10 ) {
                return player.sendMessage( 'Reached limit of drawing. More points available however cannot be shown.' )
            }
        }
    }


    if ( writtenAmount > 0 ) {
        player.sendOwnedData( packet.getBuffer() )
        player.sendMessage( `Computed ${totalPoints} geometry points in ${Date.now() - startTime} ms` )
    }
}

function onInfo( commandChunks: Array<string>, player: L2PcInstance ) : void {
    let data = getGeometryName( commandChunks, player )
    if ( !data ) {
        return
    }

    let itemHtml = DataManager.getHtmlData().getItem( 'overrides/html/command/geometry/info-property.htm' )
    let propertyItems : Array<string> = _.map( data.parameters, ( value: number, key: string ) : string => {
        return itemHtml
            .replace( '%name%', key )
            .replace( '%value%', value.toString() )
    } )

    let html = DataManager.getHtmlData().getItem( 'overrides/html/command/geometry/info.htm' )
        .replaceAll( '%geometryId%', data.id )
        .replace( '%menuAction%', `bypass -h admin_${Commands.Menu}` )
        .replace( '%selfAction%', `bypass admin_${Commands.ShowOnPlayer}` )
        .replace( '%targetAction%', `bypass admin_${Commands.ShowOnTarget}` )
        .replace( '%type%', data.type )
        .replace( '%size%', data.size.toString() )
        .replace( '%properties%', propertyItems.join( '' ) )

    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId() ) )
}

function onPlayer( commandChunks: Array<string>, player: L2PcInstance ) : void {
    let data = getGeometryName( commandChunks, player )
    if ( !data ) {
        return
    }

    let geometry = PointGeometry.acquire( data.id as GeometryId )

    drawGeometry( player, player, geometry )

    geometry.release()
}

function onTarget( commandChunks: Array<string>, player: L2PcInstance ) : void {
    let data = getGeometryName( commandChunks, player )
    if ( !data ) {
        return
    }

    let target = player.getTarget()
    if ( !target ) {
        return player.sendMessage( 'Please select target' )
    }

    let geometry = PointGeometry.acquire( data.id as GeometryId )

    drawGeometry( player, target, geometry )

    geometry.release()
}

function onMenu( player: L2PcInstance ) : void {
    let itemHtml = DataManager.getHtmlData().getItem( 'overrides/html/command/geometry/menu-item.htm' )
    let items = AllGeometryIds.map( ( id: GeometryId ) : string => {
        return itemHtml
            .replace( '%name%', id )
            .replace( '%infoAction%', `bypass -h admin_${Commands.Info} ${id}` )
            .replace( '%selfAction%', `bypass admin_${Commands.ShowOnPlayer} ${id}` )
            .replace( '%targetAction%', `bypass admin_${Commands.ShowOnTarget} ${id}` )
    } )

    let html = DataManager.getHtmlData().getItem( 'overrides/html/command/geometry/menu.htm' )
        .replace( '%items%', items.join( '' ) )

    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId() ) )
}