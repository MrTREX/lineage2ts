import { IAdminCommand } from '../IAdminCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { CreatureSay } from '../../packets/send/builder/CreatureSay'
import { L2Character } from '../../models/actor/L2Character'
import { formatOptionsDescription } from './helpers/FormatHelpers'
import { NpcSayType } from '../../enums/packets/NpcSayType'

const enum TargetSayCommands {
    Say = 'targetsay'
}

export const TargetSay: IAdminCommand = {
    getCommandNames(): Array<string> {
        return [
            TargetSayCommands.Say,
        ]
    },

    getDescription( command: string ): Array<string> {
        switch ( command ) {
            case TargetSayCommands.Say:
                return [
                    formatOptionsDescription( TargetSayCommands.Say, '<message>', 'Makes targeted character/monster say things.' ),
                ]
        }
    },

    async onCommand( command: string, player: L2PcInstance ): Promise<void> {
        let target = player.getTarget()
        if ( !target || !target.isCharacter() || target.isStaticObject() ) {
            return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
        }

        let message = command.substring( 16 )
        let packet = CreatureSay.fromParameters( target.getObjectId(),
                                        target.isPlayer() ? NpcSayType.All : NpcSayType.NpcAll,
                                        target.getName(),
                                        message )
                                .getBuffer()

        BroadcastHelper.dataToSelfInRange( target as L2Character, packet )
    },
}