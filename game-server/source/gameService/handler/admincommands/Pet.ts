import { IAdminCommand } from '../IAdminCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Summon } from '../../models/actor/L2Summon'
import { L2PetInstance } from '../../models/actor/instance/L2PetInstance'
import { L2World } from '../../L2World'
import { formatDescription, formatOptionsDescription } from './helpers/FormatHelpers'
import { DataManager } from '../../../data/manager'
import { InstanceType } from '../../enums/InstanceType'
import { Race } from '../../enums/Race'
import { WeaponType } from '../../models/items/type/WeaponType'
import { ElementalsHelper } from '../../models/Elementals'
import { Formulas } from '../../models/stats/Formulas'
import { formatColoredElement, formatColoredStat, formatNumber, getPrettyObjectId } from '../bypasshandlers/viewNpc'
import _ from 'lodash'
import { AIIntent } from '../../aicontroller/enums/AIIntent'
import { GeneralHelper } from '../../helpers/GeneralHelper'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { L2ServitorInstance } from '../../models/actor/instance/L2ServitorInstance'
import { NpcHtmlMessagePath } from '../../packets/send/NpcHtmlMessage'
import { L2PetData } from '../../models/L2PetData'
import { SkillCache } from '../../cache/SkillCache'
import { L2Object } from '../../models/L2Object'
import aigle from 'aigle'
import { DefenceElementTypes, ElementalType } from '../../enums/ElementalType'

const enum PetCommands {
    UnSummon = 'pet-unsummon',
    ClearName = 'pet-clear-name',
    Rename = 'pet-set-name',
    SetFeedPercent = 'pet-set-feed',
    SetLevel = 'pet-set-level',
    PetInfo = 'pet-info',
    PetMenu = 'pet-menu',
    UnSummonRadius = 'pet-unsummon-radius'
}

const staticAddButton = '<button value="Add To Pet" action="bypass -h admin_create-givetargetid %objectId% %itemId% 10" width=80 height=21 back="L2UI_CT1.Button_DF_Down" fore="L2UI_CT1.Button_DF">'
const noFoodMessage = '<tr><td><font color=B3000A>Not Using Any Food</font></td></tr>'
const noSkillsMessage = '<tr><td><font color=B3000A>No Summon Skills</font></td></tr>'

export const PetManagement: IAdminCommand = {
    getCommandNames(): Array<string> {
        return [
            PetCommands.UnSummon,
            PetCommands.ClearName,
            PetCommands.Rename,
            PetCommands.SetFeedPercent,
            PetCommands.SetLevel,
            PetCommands.PetInfo,
            PetCommands.PetMenu,
            PetCommands.UnSummonRadius,
        ]
    },

    getDescription( command: string ): Array<string> {
        switch ( command ) {
            case PetCommands.UnSummon:
                return [ formatOptionsDescription( PetCommands.UnSummon, '[ objectId ]', 'Unsummon pet or servitor, specified either as objectId or player\'s target' ) ]

            case PetCommands.ClearName:
                return [ formatOptionsDescription( PetCommands.ClearName, '[ objectId ]', 'Clear name of pet, specified either as objectId or player\'s target' ) ]

            case PetCommands.Rename:
                return [
                    formatOptionsDescription( PetCommands.Rename, '< name > [ objectId ]', 'Sets new name of pet' ),
                    'Pet is specified as objectId or current player\'s target',
                ]

            case PetCommands.SetFeedPercent:
                return [
                    formatOptionsDescription( PetCommands.SetFeedPercent, '< percent = 1-100 > [ objectId ]', 'Set pet feed capacity specified by percent amount' ),
                ]

            case PetCommands.SetLevel:
                return [
                    formatOptionsDescription( PetCommands.SetLevel, '< level > [ objectId ]', 'Set pet\'s current level, pet specified as either objectId or player\'s target' ),
                ]

            case PetCommands.PetInfo:
                return [
                    formatOptionsDescription( PetCommands.PetInfo, '[ objectId ]', 'View information for pet, pet specified as either objectId or player\'s target' ),
                ]

            case PetCommands.PetMenu:
                return [
                    formatDescription( PetCommands.PetMenu, 'Opens menu to acquire various pets and servitors.' ),
                ]

            case PetCommands.UnSummonRadius:
                return [
                    formatOptionsDescription( PetCommands.UnSummonRadius, '[ radius = 1000 ]', 'Un-summon any pets within radius, centered on current player or player\'s target' ),
                ]
        }
    },

    onCommand( command: string, player: L2PcInstance ): Promise<void> {
        let commandChunks = command.split( ' ' )
        switch ( commandChunks[ 0 ] ) {
            case PetCommands.UnSummon:
                return onUnsummon( player, commandChunks )

            case PetCommands.ClearName:
                return onClearName( player, commandChunks )

            case PetCommands.Rename:
                return onRename( player, commandChunks )

            case PetCommands.SetFeedPercent:
                return onSetFeed( player, commandChunks )

            case PetCommands.SetLevel:
                onSetLevel( player, commandChunks )
                return

            case PetCommands.PetInfo:
                onViewPet( player, commandChunks )
                return

            case PetCommands.PetMenu:
                onPetMenu( player )
                return

            case PetCommands.UnSummonRadius:
                return onUnsummonInRadius( player, commandChunks )
        }
    },

}

function onUnsummon( player: L2PcInstance, commandChunks: Array<string> ): Promise<void> {
    let [ , objectIdValue ] = commandChunks
    let target = objectIdValue ? L2World.getObjectById( parseInt( objectIdValue ) ) : player.getTarget()
    if ( !target || !target.isSummon() ) {
        player.sendMessage( 'Unable to un-summon target. Specify pet target.' )
        return
    }

    return ( target as L2Summon ).unSummon( player )
}

async function onUnsummonInRadius( player: L2PcInstance, commandChunks: Array<string> ): Promise<void> {
    let [ , radiusValue ] = commandChunks
    let radius = _.defaultTo( parseInt( radiusValue ), 1000 )

    let target = player.getTarget()
    let summons: Array<L2Object> = L2World.getVisibleObjectsByPredicate( target ? target : player, Math.min( radius, 10000 ), ( object: L2Object ) => object.isSummon() )

    await aigle.resolve( summons ).each( ( summon: L2Object ) => ( summon as L2Summon ).unSummon( player ) )
    player.sendMessage( `Un-summoned total of ${ summons.length } pets.` )
}

function onClearName( player: L2PcInstance, commandChunks: Array<string> ): Promise<void> {
    let [ , objectIdValue ] = commandChunks
    let target = objectIdValue ? L2World.getObjectById( parseInt( objectIdValue ) ) : player.getTarget()

    if ( !target || !target.isPet() ) {
        player.sendMessage( 'Unable to clear name of target that is not pet. Specify pet target.' )
        return
    }

    if ( target.getName() === '' ) {
        player.sendMessage( 'Pet already has no name.' )
        return
    }

    return ( target as L2PetInstance ).attemptNameChange( '', player, true )
}

function onRename( player: L2PcInstance, commandChunks: Array<string> ): Promise<void> {
    let [ , name, objectIdValue ] = commandChunks
    if ( !name ) {
        player.sendMessage( 'Please specify a pet name.' )
        return
    }

    let target = objectIdValue ? L2World.getObjectById( parseInt( objectIdValue ) ) : player.getTarget()
    if ( !target || !target.isPet() ) {
        player.sendMessage( 'Unable to set name of target that is not pet. Specify pet target.' )
        return
    }

    return ( target as L2PetInstance ).attemptNameChange( name, player, true )
}

function onSetFeed( player: L2PcInstance, commandChunks: Array<string> ): Promise<void> {
    let [ , percentValue, objectIdValue ] = commandChunks
    let value = parseInt( percentValue )
    if ( !value || value <= 0 ) {
        player.sendMessage( 'Please specify value for pet feed percentage.' )
        return
    }

    let target = objectIdValue ? L2World.getObjectById( parseInt( objectIdValue ) ) : player.getTarget()
    if ( !target || !target.isPet() ) {
        player.sendMessage( 'Unable to set feed level of target that is not pet. Specify pet target.' )
        return
    }

    let pet = target as L2PetInstance
    pet.setCurrentFeed( pet.getPetLevelData().getPetMaxFeed() * ( Math.min( value, 100 ) / 100 ) )
    pet.broadcastStatusUpdate()
    return pet.storeMe()
}

function onSetLevel( player: L2PcInstance, commandChunks: Array<string> ): void {
    let [ , level, objectIdValue ] = commandChunks

    let value = parseInt( level )
    if ( !value || value <= 0 ) {
        player.sendMessage( 'Please specify value for pet level.' )
        return
    }

    let target = objectIdValue ? L2World.getObjectById( parseInt( objectIdValue ) ) : player.getTarget()
    if ( !target || !target.isPet() ) {
        player.sendMessage( 'Unable to set level of target that is not pet. Specify pet target.' )
        return
    }

    let pet = target as L2PetInstance
    pet.getStat().setLevel( value )
    pet.broadcastStatusUpdate()
}

function onViewPet( player: L2PcInstance, commandChunks: Array<string> ): void {
    let [ , objectIdValue ] = commandChunks
    let target = objectIdValue ? L2World.getObjectById( parseInt( objectIdValue ) ) : player.getTarget()

    if ( !target || !target.isSummon() ) {
        player.sendMessage( 'Unable to view pet information. Specify summon target.' )
        return
    }

    let summon = target as L2Summon
    let attackAttribute = summon.getAttackElement()

    let defenceElements: Array<number> = DefenceElementTypes.map( ( type: ElementalType ): number => summon.getDefenceElementValue( type ) )
    let minDefenceElementValue = _.min( defenceElements )

    let htmlPath = 'overrides/html/command/pet/stats.htm'
    let html = DataManager.getHtmlData().getItem( htmlPath )
                          .replace( /%objectId%/g, target.getObjectId().toString() )
                          .replace( /%class%/g, InstanceType[ target.getInstanceType() ] )
                          .replace( /%race%/g, Race[ summon.getTemplate().getRace() ] )
                          .replace( /%templateId%/g, summon.getTemplate().getId().toString() )

                          .replace( /%level%/g, summon.getLevel().toString() )
                          .replace( /%name%/g, summon.getName() ? summon.getName() : summon.getTemplate().getName() )
                          .replace( /%hp%/g, Math.floor( summon.getCurrentHp() ).toString() )
                          .replace( /%hpmax%/g, summon.getMaxHp().toString() )
                          .replace( /%mp%/g, Math.floor( summon.getCurrentMp() ).toString() )
                          .replace( /%mpmax%/g, summon.getMaxMp().toString() )

                          .replace( /%patk%/g, formatNumber( summon.getPowerAttack( null ) ) )
                          .replace( /%matk%/g, formatNumber( summon.getMagicAttack( null, null ) ) )
                          .replace( /%pdef%/g, formatNumber( Math.floor( summon.getPowerDefence( null ) ) ) )
                          .replace( /%mdef%/g, formatNumber( Math.floor( summon.getMagicDefence( null, null ) ) ) )

                          .replace( /%accu%/g, formatNumber( summon.getAccuracy() ) )
                          .replace( /%evas%/g, formatNumber( summon.getEvasionRate( null ) ) )
                          .replace( /%crit%/g, formatNumber( summon.getCriticalHit( null, null ) ) )
                          .replace( /%rspd%/g, formatNumber( Math.floor( summon.getRunSpeed() ) ) )

                          .replace( /%aspd%/g, formatNumber( summon.getPowerAttackSpeed() ) )
                          .replace( /%cspd%/g, formatNumber( summon.getMagicAttackSpeed() ) )
                          .replace( /%atkType%/g, WeaponType[ summon.getTemplate().getBaseAttackType().toString() ] )
                          .replace( /%atkRng%/g, formatNumber( summon.getTemplate().getBaseAttackRange() ) )

                          .replace( /%str%/g, formatColoredStat( summon.getSTR() ) )
                          .replace( /%dex%/g, formatColoredStat( summon.getDEX() ) )
                          .replace( /%con%/g, formatColoredStat( summon.getCON() ) )
                          .replace( /%int%/g, formatColoredStat( summon.getINT() ) )

                          .replace( /%wit%/g, formatColoredStat( summon.getWIT() ) )
                          .replace( /%men%/g, formatColoredStat( summon.getMEN() ) )
                          .replace( /%loc%/g, `${ summon.getX() } ${ summon.getY() } ${ summon.getZ() }` )
                          .replace( /%heading%/g, summon.getHeading().toString() )

                          .replace( /%collision_radius%/g, formatNumber( summon.getTemplate().getFCollisionRadius() ) )
                          .replace( /%collision_height%/g, formatNumber( summon.getTemplate().getFCollisionHeight() ) )
                          .replace( /%distance%/g, formatNumber( Math.round( player.calculateDistance( target, true ) ) ) )
                          .replace( /%ele_atk%/g, ElementalsHelper.getElementName( attackAttribute ) )

                          .replace( /%ele_atk_value%/g, formatNumber( summon.getAttackElementValue( attackAttribute ) ) )
                          .replace( /%ele_dfire%/g, formatColoredElement( defenceElements[ ElementalType.FIRE ], minDefenceElementValue ) )
                          .replace( /%ele_dwater%/g, formatColoredElement( defenceElements[ ElementalType.WATER ], minDefenceElementValue ) )
                          .replace( /%ele_dwind%/g, formatColoredElement( defenceElements[ ElementalType.WIND ], minDefenceElementValue ) )

                          .replace( /%ele_dearth%/g, formatColoredElement( defenceElements[ ElementalType.EARTH ], minDefenceElementValue ) )
                          .replace( /%ele_dholy%/g, formatColoredElement( defenceElements[ ElementalType.HOLY ], minDefenceElementValue ) )
                          .replace( /%ele_ddark%/g, formatColoredElement( defenceElements[ ElementalType.DARK ], minDefenceElementValue ) )
                          .replace( /%objectIdFormatted%/g, getPrettyObjectId( summon.getObjectId() ) )

                          .replace( '%hpRegeneration%', formatNumber( Formulas.calculateHpRegeneration( summon ) ) )
                          .replace( '%mpRegeneration%', formatNumber( Formulas.calculateMpRegeneration( summon ) ) )
                          .replace( '%ai-intent%', AIIntent[ summon.getAIController().currentIntent ] )
                          .replace( '%ai-trait%', summon.getAIController().currentTrait.getName() )

                          .replace( '%ownerName%', summon.getOwner().getName() )
                          .replace( '%petSkills%', getSkills( summon ) )

    if ( summon.isPet() ) {
        let pet = summon as L2PetInstance
        let feedInterval: number = pet.getFeedInterval()
        let feedIntervalMessage = feedInterval > 0 ? GeneralHelper.formatMillis( feedInterval ).join( ', ' ) : 'No interval'
        let item: L2ItemInstance = pet.getControlItem()

        let foodItemLines: Array<string> = pet.getPetData().getFood().map( createFoodItem.bind( null, pet.getObjectId().toString() ) )
        let feedLifetime: Array<string> = GeneralHelper.formatMillis( Math.floor( ( pet.getMaxFeed() / pet.getFeedConsume() ) * feedInterval ), false )
        let untilHungry: Array<string> = pet.isHungry() ? [ 'Hungry Now' ] : GeneralHelper.formatMillis( Math.floor( ( pet.getCurrentFeed() - pet.getHungerLimit() ) / pet.getFeedConsume() ) * feedInterval, false )

        html = html
                .replace( '%currentFood%', pet.getCurrentFeed().toFixed( 0 ) )
                .replace( '%maxFood%', pet.getMaxFeed().toString() )
                .replace( '%feedInterval%', feedIntervalMessage )
                .replace( '%controlItem%', item ? item.getName() : 'No item' )
                .replace( '%foodItems%', foodItemLines.length > 0 ? foodItemLines.join( '' ) : noFoodMessage )
                .replace( '%feedLifetime%', feedLifetime.join( ', ' ) )
                .replace( '%untilHungry%', untilHungry.join( ', ' ) )
                .replace( '%consumeXP%', formatNumber( ( 1 - pet.getPetLevelData().getOwnerXpRatio() ) * 100 ) )
    } else {
        let servitor = summon as L2ServitorInstance
        let lifeTime = GeneralHelper.formatMillis( servitor.getLifeTimeRemaining(), false ).join( ', ' )

        html = html
                .replace( '%currentFood%', '0' )
                .replace( '%maxFood%', '0' )
                .replace( '%feedInterval%', 'No interval' )
                .replace( '%controlItem%', 'No item' )
                .replace( '%foodItems%', servitor.getItemConsume() ? createFoodItem( servitor.getObjectId().toString(), servitor.getItemConsume().id ) : noFoodMessage )
                .replace( '%feedLifetime%', lifeTime )
                .replace( '%untilHungry%', lifeTime )
                .replace( '%consumeXP%', '0' )
    }

    player.sendOwnedData( NpcHtmlMessagePath( html, htmlPath, player.getObjectId() ) )
}

function createFoodItem( objectId: string, itemId: number ): string {
    let item = DataManager.getItems().getTemplate( itemId )
    if ( !item ) {
        return `<tr><td><font color="FFBA46">Bad food for itemId</font></td><td>${ itemId }</td></tr>`
    }

    let button = staticAddButton
            .replace( '%objectId%', objectId )
            .replace( '%itemId%', itemId.toString() )
    return `<tr><td width=190><font color=FF4A00>${ item.getName() }</font></td><td width=80>${ button }</td></tr>`
}

function getSkills( summon: L2Summon ): string {
    let allSkills: Set<number> = _.reduce( summon.getTemplate().getSkillParameters(), ( allItems: Set<number>, data: any ): Set<number> => {
        allItems.add( data.id )
        return allItems
    }, new Set<number>() )

    let lines: Array<string> = Array.from( allSkills ).map( ( skillId: number ): string => {
        let level = summon.getSkillLevel( skillId )
        if ( !level ) {
            return
        }

        let skill = SkillCache.getSkill( skillId, level )
        return `<tr><td width=200><font color=32FF85>${ skill.getName() }</font></td><td width=50>level ${ level }</td></tr>`
    } )

    return lines.length > 0 ? _.compact( lines ).sort().join( '' ) : noSkillsMessage
}

function onPetMenu( player: L2PcInstance ): void {
    let htmlPath = 'overrides/html/command/pet/menu.htm'
    let html = DataManager.getHtmlData().getItem( htmlPath )
                          .replace( '%items%', createPetMenuItems( player.getObjectId().toString() ) )

    GeneralHelper.sendCommunityBoardHtml( player, html )
}

function createPetMenuItems( objectId: string ): string {
    let menuItemHtml = DataManager.getHtmlData().getItem( 'overrides/html/command/pet/menu-item.htm' )

    return _.map( DataManager.getPetData().getPetItems(), ( data: L2PetData, itemId: string ): string => {
        return menuItemHtml
                .replace( '%name%', DataManager.getItems().getTemplate( data.itemId ).getName() )
                .replace( '%petName%', DataManager.getNpcData().getTemplate( data.npcId ).getName() )
                .replace( '%objectId%', objectId )
                .replace( /%itemId%/g, itemId )
    } ).join( '' )
}