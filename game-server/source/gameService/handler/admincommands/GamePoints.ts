import { IAdminCommand } from '../IAdminCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { formatDescription, formatOptionsDescription } from './helpers/FormatHelpers'
import { L2World } from '../../L2World'
import { DataManager } from '../../../data/manager'
import { GamePointsCache, GamePointType } from '../../cache/GamePointsCache'
import { NpcHtmlMessage } from '../../packets/send/NpcHtmlMessage'
import { ExPCCafePointInfo } from '../../packets/send/ExPCCafePointInfo'
import { ExBrGamePoint } from '../../packets/send/ExBrGamePoint'
import { ExBrProductList, ExBrRecentProductList } from '../../packets/send/ExBrProductList'
import { ConfigManager } from '../../../config/ConfigManager'
import { PcCafeUI } from '../../packets/send/PcCafeUI'

const enum Commands {
    Menu = 'game-points-menu',
    Add = 'game-points-add',
    Remove = 'game-points-remove',
    Packet = 'game-points-packet',
    InspectPlayer = 'game-points-inspect'
}

export const enum AddGamePointType {
    PcCafe = 'pcCafe',
    ItemShop = 'itemShop'
}

export const enum GamePointPacketType {
    Products = 'products',
    ItemShopPoints = 'shop-points',
    PcCafePoints = 'pc-points',
    BoughtProducts = 'b-products',
    PcCafeUI = 'pc-ui'
}

export function createAddGamePointsBypass( playerName: string, type: AddGamePointType, points : number = undefined ) : string {
    return `bypass admin_${ Commands.Add } ${ playerName } ${ type } ${ Number.isInteger( points ) ? points : ''}`.trim()
}

export function createRemoveGamePointsBypass( playerName: string, type: AddGamePointType, points : number = undefined ) : string {
    return `bypass admin_${ Commands.Remove } ${ playerName } ${ type } ${ Number.isInteger( points ) ? points : ''}`.trim()
}

export function createGamePointsPacketBypass( type: GamePointPacketType, playerName: string = undefined ) : string {
    return `bypass admin_${ Commands.Packet } ${ type } ${ playerName ? playerName : ''}`.trim()
}

export function createInspectGamePointsBypass( playerName: string ) : string {
    return `bypass -h admin_${ Commands.InspectPlayer } ${playerName}`
}

export const GamePoints: IAdminCommand = {
    getCommandNames(): Array<string> {
        return [
            Commands.Menu,
            Commands.Add,
            Commands.Remove,
            Commands.Packet,
            Commands.InspectPlayer
        ]
    },

    getDescription( command: string ): Array<string> {
        switch ( command ) {
            case Commands.Menu:
                return [
                    formatDescription( Commands.Menu, 'Show menu with available game points actions' )
                ]

            case Commands.Add:
                return [
                    formatOptionsDescription( Commands.Add , '<playerName> <type = pcCafe | itemShop> <points>', 'Add specific amount of game points to player specified by name' )
                ]

            case Commands.Remove:
                return [
                    formatOptionsDescription( Commands.Remove , '<playerName> <type = pcCafe | itemShop> <points>', 'Remove specific amount of game points from player specified by name' )
                ]

            case Commands.Packet:
                return [
                    formatOptionsDescription( Commands.Packet, '<name = products | shop-points | pc-points> [playerName]', 'Send specific packet to player, or admin if name is not provided' )
                ]

            case Commands.InspectPlayer:
                return [
                    formatOptionsDescription( Commands.InspectPlayer, '[playerName]', 'Shows game point information about target player, if playerName is not provided.' )
                ]
        }
    },

    async onCommand( command: string, player: L2PcInstance ): Promise<void> {
        let commandChunks = command.split( ' ' )

        switch ( commandChunks[ 0 ] ) {
            case Commands.Menu:
                return onMenu( player )

            case Commands.InspectPlayer:
                return onInspectPlayer( player, commandChunks )

            case Commands.Add:
                return onAddRemove( player, commandChunks, Commands.Add )

            case Commands.Remove:
                return onAddRemove( player, commandChunks, Commands.Remove )

            case Commands.Packet:
                return onSendPacket( player, commandChunks )
        }
    }
}

function generatePointsSection( player: L2PcInstance ) : string {
    let packetHtml = DataManager.getHtmlData().getItem( 'overrides/html/command/gamePoints/inspect-points-packet.htm' )
    let packets = [
        GamePointPacketType.Products,
        GamePointPacketType.PcCafePoints,
        GamePointPacketType.ItemShopPoints
    ].map( ( type: GamePointPacketType ) : string => {
        return packetHtml
            .replace( '%name%', getPacketName( type ) )
            .replace( '%action%', createGamePointsPacketBypass( type, player.getName() ) )
    } )

    return DataManager.getHtmlData().getItem( 'overrides/html/command/gamePoints/inspect-points.htm' )
        .replace( '%addPcPoints%', createAddGamePointsBypass( player.getName(), AddGamePointType.PcCafe ) )
        .replace( '%addShopPoints%', createAddGamePointsBypass( player.getName(), AddGamePointType.ItemShop ) )
        .replace( '%removePcPoints%', createRemoveGamePointsBypass( player.getName(), AddGamePointType.PcCafe ) )
        .replace( '%removeShopPoints%', createRemoveGamePointsBypass( player.getName(), AddGamePointType.ItemShop ) )
        .replace( '%packets%', packets.join( '' ) )
}

function onMenu( player: L2PcInstance ) : void {
    let playerHtml = DataManager.getHtmlData().getItem( 'overrides/html/command/gamePoints/menu-player.htm' )
    let playerItems : Array<string> = []

    /*
        Admin player will be added to the top of the list.
     */
    L2World.forEachNearestPlayer( player, 31, ( targetPlayer: L2PcInstance ) : void => {
        playerItems.push(
            playerHtml
                .replace( '%name%', targetPlayer.getName() )
                .replace( '%action%', createInspectGamePointsBypass( targetPlayer.getName() ) )
        )
    } )

    let html = DataManager.getHtmlData().getItem( 'overrides/html/command/gamePoints/menu.htm' )
        .replace( '%players%', playerItems.join( '' ) )
        .replace( '%isEnabled%', ConfigManager.character.isGamePointsEnabled() ? 'Yes' : 'No' )
        .replace( '%pcPointsEnabled%', ConfigManager.character.usePcCafePoints() ? 'Yes' : 'No' )
        .replace( '%itemPointsEnabled%', ConfigManager.character.useItemShopPoints() ? 'Yes' : 'No' )

        .replace( '%pcAcquisition%', ConfigManager.character.getPcCafePointsAcquisition() )
        .replace( '%itemAcquisition%', ConfigManager.character.getItemShopPointsAcquisition() )

    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId() ) )
}

function onInspectPlayer( player: L2PcInstance, chunks: Array<string> ) : void {
    let playerName = chunks[ 1 ]
    let targetPlayer = playerName ? L2World.getPlayerByName( playerName ) : player.getTarget()

    if ( !targetPlayer || !targetPlayer.isPlayer() ) {
        return player.sendMessage( `${ Commands.InspectPlayer } : unable to find player` )
    }

    let pcPoints = GamePointsCache.getPoints( player.getAccountName(), GamePointType.PcCafe )
    let itemShopPoints = GamePointsCache.getPoints( player.getAccountName(), GamePointType.ItemShop )

    let html = DataManager.getHtmlData().getItem( 'overrides/html/command/gamePoints/inspect.htm' )
        .replace( '%name%', player.getName() )
        .replace( '%account%', player.getAccountName() )
        .replace( '%id%', player.getObjectId().toString() )
        .replace( '%level%', player.getLevel().toString() )

        .replace( '%pcPoints%', pcPoints.toString() )
        .replace( '%itemShopPoints%', itemShopPoints.toString() )
        .replace( '%points%', generatePointsSection( targetPlayer as L2PcInstance ) )

    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId() ) )
}

function onAddRemove( player: L2PcInstance, chunks: Array<string>, command: Commands ) : void {
    let [ , playerName, type, pointValue ] = chunks

    if ( !playerName ) {
        return player.sendMessage( `${ command } : please specify player name` )
    }

    let targetPlayer = L2World.getPlayerByName( playerName )
    if ( !targetPlayer ) {
        return player.sendMessage( `${ command } : unable to find player` )
    }

    if ( !pointValue ) {
        return player.sendMessage( `${ command } : please specify points value` )
    }

    let points = parseInt( pointValue, 10 )
    if ( !Number.isInteger( points ) ) {
        return player.sendMessage( `${ command } : please specify integer value for points` )
    }

    let name = command === Commands.Add ? 'Added' : 'Removed'
    points = command === Commands.Add ? Math.abs( points ) : -Math.abs( points )
    let fullPoints = Math.abs( points )

    switch ( type ) {
        case AddGamePointType.PcCafe:
            GamePointsCache.addPoints( targetPlayer.getAccountName(), GamePointType.PcCafe, points, `Admin:${ command }` )
            player.sendMessage( `${name} ${fullPoints} pc cafe points successfully for player "${ player.getName()} "` )

            let pcPoints = GamePointsCache.getPoints( targetPlayer.getAccountName(), GamePointType.PcCafe )
            targetPlayer.sendOwnedData( ExPCCafePointInfo( pcPoints ) )
            return

        case AddGamePointType.ItemShop:
            GamePointsCache.addPoints( targetPlayer.getAccountName(), GamePointType.ItemShop, points, `Admin:${ command }` )
            player.sendMessage( `${name} ${fullPoints} item shop points successfully for player "${ player.getName()} "` )

            let shopPoints = GamePointsCache.getPoints( targetPlayer.getAccountName(), GamePointType.ItemShop )
            targetPlayer.sendOwnedData( ExBrGamePoint( targetPlayer.getObjectId(), shopPoints ) )
            return
    }

    player.sendMessage( `${ command } : unknown point type` )
}

function onSendPacket( player: L2PcInstance, chunks: Array<string> ) : void {
    let [ , type, playerName ] = chunks

    let targetPlayer = playerName ? L2World.getPlayerByName( playerName ) : player
    if ( !targetPlayer ) {
        return player.sendMessage( `${ Commands.Packet } : unable to find player` )
    }

    switch ( type as GamePointPacketType ) {
        case GamePointPacketType.PcCafePoints:
            let pcPoints = GamePointsCache.getPoints( targetPlayer.getAccountName(), GamePointType.PcCafe )
            targetPlayer.sendOwnedData( ExPCCafePointInfo( pcPoints ) )
            return player.sendMessage( `Sent ${ExPCCafePointInfo.name} packet to player "${targetPlayer.getName()}"` )

        case GamePointPacketType.ItemShopPoints:
            let shopPoints = GamePointsCache.getPoints( targetPlayer.getAccountName(), GamePointType.ItemShop )
            targetPlayer.sendOwnedData( ExBrGamePoint( targetPlayer.getObjectId(), shopPoints ) )
            return player.sendMessage( `Sent ${ExBrGamePoint.name} packet to player "${targetPlayer.getName()}"` )

        case GamePointPacketType.Products:
            targetPlayer.sendOwnedData( ExBrProductList( DataManager.getGamePointProducts().getAll() ) )
            return player.sendMessage( `Sent ${ExBrProductList.name} packet to player "${targetPlayer.getName()}"` )

        case GamePointPacketType.BoughtProducts:
            targetPlayer.sendOwnedData( ExBrRecentProductList( GamePointsCache.getPurchases( targetPlayer.getAccountName(), GamePointType.ItemShop ) ) )
            return player.sendMessage( `Sent ${ExBrRecentProductList.name} packet to player "${targetPlayer.getName()}"` )

        case GamePointPacketType.PcCafeUI:
            targetPlayer.sendOwnedData( PcCafeUI() )
            return player.sendMessage( `Sent ${PcCafeUI.name} packet to player "${targetPlayer.getName()}"` )
    }

    return player.sendMessage( `${ Commands.Packet } : uknown packet type` )
}

function getPacketName( type: GamePointPacketType ) : string {
    switch ( type ) {
        case GamePointPacketType.Products:
            return `${ExBrProductList.name} (Item Shop products)`

        case GamePointPacketType.ItemShopPoints:
            return `${ExBrGamePoint.name} (Item Shop Points)`

        case GamePointPacketType.PcCafePoints:
            return `${ExPCCafePointInfo.name} (Pc Bang Points)`

        case GamePointPacketType.BoughtProducts:
            return `${ExBrRecentProductList.name} (Item Shop bought)`

        case GamePointPacketType.PcCafeUI:
            return `${PcCafeUI.name} (PC Coupon Entry)`
    }
}