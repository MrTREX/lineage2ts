import { IAdminCommand } from '../IAdminCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { formatDescription, formatOptionsDescription } from './helpers/FormatHelpers'
import { DimensionalTransferManager } from '../../cache/DimensionalTransferManager'
import { DataManager } from '../../../data/manager'
import { ListenerCache } from '../../cache/ListenerCache'
import { EventType, GMDimensionalItemAddEvent, GMDimensionalItemRemoveEvent } from '../../models/events/EventType'
import { L2World } from '../../L2World'
import { ExGetDimensionalItemList } from '../../packets/send/ExGetDimensionalItemList'
import { ExNotifyDimensionalItem } from '../../packets/send/ExNotifyPremiumItem'
import { createPagination } from './helpers/SearchHelper'
import _ from 'lodash'
import { L2DimensionalTransferItem } from '../../models/items/L2DimensionalTransferItem'
import { NpcHtmlMessage } from '../../packets/send/NpcHtmlMessage'

const enum Commands {
    Add = 'ditem-add',
    RemoveAll = 'ditem-remove-all',
    View = 'ditem-view',
    AddInRadius = 'ditem-add-radius',
    AddToPlayerId = 'ditem-add-player',
    Retrieve = 'ditem-retrieve',
    RemoveItem = 'ditem-remove-item'
}

export const DimensionalItems: IAdminCommand = {
    getCommandNames(): Array<string> {
        return [
            Commands.Add,
            Commands.AddInRadius,
            Commands.AddToPlayerId,
            Commands.RemoveAll,
            Commands.View,
            Commands.Retrieve,
            Commands.RemoveItem
        ]
    },

    getDescription( command: string ): Array<string> {
        switch ( command ) {
            case Commands.Add:
                return [
                    formatOptionsDescription( Commands.Add, '<itemId> [ amount = 1 ]', 'Add dimensional item to target player' )
                ]

            case Commands.AddInRadius:
                return [
                    formatOptionsDescription( Commands.AddInRadius, '<itemId> [ amount = 1 ]', 'Add dimensional item to players in 500 radius' )
                ]

            case Commands.AddToPlayerId:
                return [
                    formatOptionsDescription( Commands.AddToPlayerId, '<playerId> <itemId> [ amount = 1 ]', 'Add dimensional item to player specified by name or objectId.' )
                ]

            case Commands.View:
                return [
                    formatDescription( Commands.View, 'View targeted player dimensional items' )
                ]

            case Commands.RemoveAll:
                return [
                    formatDescription( Commands.RemoveAll, 'Remove any and all dimensional items from targeted player' )
                ]

            case Commands.Retrieve:
                return [
                    formatDescription( Commands.Retrieve, 'Show current player dimensional items for retrieval' )
                ]

            case Commands.RemoveItem:
                return [
                    formatOptionsDescription( Commands.RemoveItem, '<itemId> [ amount = 1 ]', 'Remove dimensional item amount from target player' )
                ]
        }
    },

    async onCommand( command: string, player: L2PcInstance ): Promise<void> {
        let commandChunks = command.split( ' ' )

        switch ( commandChunks[ 0 ] ) {
            case Commands.Add:
                return onAdd( commandChunks, player )

            case Commands.AddInRadius:
                return onAddInRadius( commandChunks, player )

            case Commands.AddToPlayerId:
                return onAddToPlayerId( commandChunks, player )

            case Commands.View:
                return onView( commandChunks, player )

            case Commands.RemoveAll:
                return onReset( player )

            case Commands.Retrieve:
                if ( !DimensionalTransferManager.hasItems( player.getObjectId() ) ) {
                    return player.sendMessage( 'You have no dimensional items to retrieve.' )
                }

                return player.sendOwnedData( ExGetDimensionalItemList( player.getObjectId() ) )

            case Commands.RemoveItem:
                return onRemove( commandChunks, player )
        }
    }
}

function addItem( target: L2PcInstance, itemId: number, amount: number, player: L2PcInstance ): void {
    DimensionalTransferManager.addItem( target.getObjectId(), itemId, amount, player.getName() )
    target.sendOwnedData( ExNotifyDimensionalItem() )

    if ( ListenerCache.hasGeneralListener( EventType.GMDimensionalItemAdd ) ) {
        let parameters: GMDimensionalItemAddEvent = {
            amount,
            gmPlayerId: player.getObjectId(),
            itemId,
            playerId: target.getObjectId()
        }

        ListenerCache.sendGeneralEvent( EventType.GMDimensionalItemAdd, parameters, itemId )
    }
}

function onAdd( commandChunks: Array<string>, player: L2PcInstance ): void {
    if ( commandChunks.length === 1 ) {
        return player.sendMessage( 'Please provide command parameters' )
    }

    let target = player.getTarget() as L2PcInstance

    if ( !target || !target.isPlayer() ) {
        return player.sendMessage( 'Target is not player.' )
    }

    let itemId = parseInt( commandChunks[ 1 ] )
    if ( !itemId || !Number.isInteger( itemId ) ) {
        return player.sendMessage( 'Please provide valid numeric item id' )
    }

    let amount = commandChunks.length === 2 ? 1 : parseInt( commandChunks[ 2 ] )
    if ( !amount || !Number.isInteger( amount ) ) {
        return player.sendMessage( 'Please provide valid numeric item amount' )
    }

    let itemTemplate = DataManager.getItems().getTemplate( itemId )
    if ( !itemTemplate ) {
        return player.sendMessage( `No item template found using itemId=${ itemId }` )
    }

    addItem( target, itemId, amount, player )

    player.sendMessage( `Player "${ target.getName() }" received ${ amount } of "${ itemTemplate.getName() }"` )
}

function onAddInRadius( commandChunks: Array<string>, player: L2PcInstance ): void {
    if ( commandChunks.length === 1 ) {
        return player.sendMessage( 'Please provide command parameters' )
    }

    let target = player.getTarget() as L2PcInstance

    if ( !target || !target.isPlayer() ) {
        return player.sendMessage( 'Target is not player.' )
    }

    let itemId = parseInt( commandChunks[ 1 ] )
    if ( !itemId || !Number.isInteger( itemId ) ) {
        return player.sendMessage( 'Please provide valid numeric item id' )
    }

    let amount = commandChunks.length === 2 ? 1 : parseInt( commandChunks[ 2 ] )
    if ( !amount || !Number.isInteger( amount ) ) {
        return player.sendMessage( 'Please provide valid numeric item amount' )
    }

    let itemTemplate = DataManager.getItems().getTemplate( itemId )
    if ( !itemTemplate ) {
        return player.sendMessage( `No item template found using itemId=${ itemId }` )
    }

    let players = L2World.getVisiblePlayers( player, 500, false )
    if ( players.length === 0 ) {
        return player.sendMessage( 'No players in 500 radius' )
    }

    players.forEach( ( otherPlayer: L2PcInstance ) => addItem( otherPlayer, itemId, amount, player ) )
    player.sendMessage( `Found ${ players.length } players, each received ${ amount } of "${ itemTemplate.getName() }"` )
}

function onAddToPlayerId( commandChunks: Array<string>, player: L2PcInstance ): void {
    if ( commandChunks.length === 1 ) {
        return player.sendMessage( 'Please provide command parameters' )
    }

    let playerId = parseInt( commandChunks[ 1 ] )
    if ( !playerId || !Number.isInteger( playerId ) ) {
        return player.sendMessage( 'Please provide valid numeric playerId' )
    }

    let target = player.getTarget() as L2PcInstance

    if ( !target || !target.isPlayer() ) {
        return player.sendMessage( 'Target is not player.' )
    }

    let itemId = parseInt( commandChunks[ 2 ] )
    if ( !itemId || !Number.isInteger( itemId ) ) {
        return player.sendMessage( 'Please provide valid numeric item id' )
    }

    let amount = commandChunks.length === 3 ? 1 : parseInt( commandChunks[ 3 ] )
    if ( !amount || !Number.isInteger( amount ) ) {
        return player.sendMessage( 'Please provide valid numeric item amount' )
    }

    let itemTemplate = DataManager.getItems().getTemplate( itemId )
    if ( !itemTemplate ) {
        return player.sendMessage( `No item template found using itemId=${ itemId }` )
    }

    addItem( target, itemId, amount, player )

    player.sendMessage( `Player "${ target.getName() }" received ${ amount } of "${ itemTemplate.getName() }"` )
}

function onView( commandChunks: Array<string>, player: L2PcInstance ): void {
    let target = player.getTarget() as L2PcInstance

    if ( !target || !target.isPlayer() ) {
        return player.sendMessage( 'Target is not player.' )
    }

    let page = commandChunks.length === 1 ? 1 : parseInt( commandChunks[ 1 ] )
    if ( !page || !Number.isInteger( page ) ) {
        page = 1
    }

    let items = DimensionalTransferManager.getItems( target.getObjectId() )
    if ( !items || items.length === 0 ) {
        return player.sendMessage( 'Target has no dimensional items' )
    }

    let paginatedItems = _.chunk( items, 60 )
    let paginationIndex = page ? page - 1 : 0
    let currentPageItems = paginatedItems[ paginationIndex ]

    if ( !currentPageItems ) {
        return player.sendMessage( `${ Commands.View } : cannot display results for page=${ page }` )
    }

    let itemTemplate = DataManager.getHtmlData().getItem( 'overrides/html/command/dimensionalItems/view-item.htm' )
    let renderedItems: string = currentPageItems.map( ( item: L2DimensionalTransferItem ): string => {
        let template = DataManager.getItems().getTemplate( item.itemId )
        return itemTemplate
            .replace( '%icon%', template ? template.icon : 'icon.etc_adena_i00' )
            .replace( '%name%', template ? template.getName() : 'No Template Found' )
            .replace( '%amount%', item.amount.toString() )
            .replace( '%action%', `admin_${ Commands.RemoveItem } ${ item.itemId } ${ item.amount }` )
    } ).join( '' )

    let html = DataManager.getHtmlData().getItem( 'overrides/html/command/dimensionalItems/view.htm' )
        .replace( '%name%', target.getName() )
        .replace( '%pagination%', createPagination( paginatedItems.length, `bypass -h admin_${ Commands.View }` ) )
        .replace( '%items%', renderedItems )

    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId() ) )
}

function onReset( player: L2PcInstance ): void {
    let target = player.getTarget() as L2PcInstance

    if ( !target || !target.isPlayer() ) {
        return player.sendMessage( 'Target is not player.' )
    }

    let items = DimensionalTransferManager.getItems( target.getObjectId() )
    if ( !items ) {
        return player.sendMessage( 'Target has no dimensional items to reset' )
    }

    DimensionalTransferManager.removeAll( target.getObjectId() )

    player.sendMessage( `Removed ${ items.length } items from player "${ target.getName() }"` )
}

async function onRemove( commandChunks: Array<string>, player: L2PcInstance ): Promise<void> {
    if ( commandChunks.length === 1 ) {
        return player.sendMessage( 'Please provide command parameters' )
    }

    let target = player.getTarget() as L2PcInstance

    if ( !target || !target.isPlayer() ) {
        return player.sendMessage( 'Target is not player.' )
    }

    let items = DimensionalTransferManager.getItems( target.getObjectId() )
    if ( !items || items.length === 0 ) {
        return player.sendMessage( 'Target has no dimensional items' )
    }

    let itemId = parseInt( commandChunks[ 1 ] )
    if ( !itemId || !Number.isInteger( itemId ) ) {
        return player.sendMessage( 'Please provide valid numeric item id' )
    }

    let amount = commandChunks.length === 2 ? 1 : parseInt( commandChunks[ 2 ] )
    if ( !amount || !Number.isInteger( amount ) ) {
        return player.sendMessage( 'Please provide valid numeric item amount' )
    }

    DimensionalTransferManager.reduceItemAmount( target.getObjectId(), itemId, amount )

    if ( ListenerCache.hasGeneralListener( EventType.GMDimensionalItemRemove ) ) {
        let parameters : GMDimensionalItemRemoveEvent = {
            amount,
            gmPlayerId: player.getObjectId(),
            itemId,
            playerId: target.getObjectId()
        }

        return ListenerCache.sendGeneralEvent( EventType.GMDimensionalItemRemove, parameters, itemId )
    }
}