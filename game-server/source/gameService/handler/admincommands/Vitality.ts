import { IAdminCommand } from '../IAdminCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { formatOptionsDescription } from './helpers/FormatHelpers'
import { L2World } from '../../L2World'
import { VitalityLevel, VitalityLevelPoints, VitalityPointsPerLevel } from '../../enums/VitalityLevels'
import _ from 'lodash'
import { DataManager } from '../../../data/manager'
import { GeneralHelper } from '../../helpers/GeneralHelper'
import { NpcHtmlMessage } from '../../packets/send/NpcHtmlMessage'
import { ListenerCache } from '../../cache/ListenerCache'
import { EventType, GMSetVitalityPointsEvent } from '../../models/events/EventType'
import { EventPoolCache } from '../../cache/EventPoolCache'

const enum VitalityCommand {
    Max = 'vitality-max',
    Clear = 'vitality-clear',
    SetLevel = 'vitality-level',
    Scan = 'vitality-scan'
}

interface ScanItem {
    otherPlayer: L2PcInstance
    distance: number
}

export const VitalityCommands : IAdminCommand = {
    getCommandNames(): Array<string> {
        return [
            VitalityCommand.Max,
            VitalityCommand.Clear,
            VitalityCommand.SetLevel,
            VitalityCommand.Scan
        ]
    },

    getDescription( command: string ): Array<string> {
        switch ( command ) {
            case VitalityCommand.Max:
                return [
                    formatOptionsDescription( VitalityCommand.Max, '[ player name ]', 'Sets maximum level of vitality to specified player by name.' ),
                    'If player name is not specified, current player will be used instead. Player name is optional.'
                ]

            case VitalityCommand.Clear:
                return [
                    formatOptionsDescription( VitalityCommand.Clear, '[ player name ]', 'Sets minimum level of vitality to specified player by name.' ),
                    'If player name is not specified, current player will be used instead. Player name is optional.'
                ]

            case VitalityCommand.SetLevel:
                return [
                    formatOptionsDescription( VitalityCommand.SetLevel, '<level=0,1,2,3,4,5>', 'Sets level of vitality to specified player by name.' ),
                    'If player name is not specified, current player will be used instead. Level value is between 0 and 5, required. Player name is optional.'
                ]

            case VitalityCommand.Scan:
                return [
                    formatOptionsDescription( VitalityCommand.Scan, '[ radius = 500 ]', 'Shows other players\' vitality levels in specified radius.' ),
                    'If radius is not specified, default value of 500 will be used instead.'
                ]
        }
    },

    async onCommand( command: string, player: L2PcInstance ): Promise<void> {
        let commandChunks: Array<string> = command.split( ' ' )

        switch ( commandChunks[ 0 ] ) {
            case VitalityCommand.Max:
                return onSetVitality( player, commandChunks, VitalityPointsPerLevel.Top )

            case VitalityCommand.Clear:
                return onSetVitality( player, commandChunks, VitalityPointsPerLevel.None )

            case VitalityCommand.SetLevel:
                return onSetVitalityLevel( player, commandChunks )

            case VitalityCommand.Scan:
                return onShowPlayerVitalityNearby( player, commandChunks )
        }
    }
}

function notifyListeners( adminPlayer: L2PcInstance, target: L2PcInstance, points: number ) : void {
    if ( !ListenerCache.hasGeneralListener( EventType.GMSetVitalityPoints ) ) {
        return
    }

    let eventData = EventPoolCache.getData( EventType.GMSetVitalityPoints ) as GMSetVitalityPointsEvent

    eventData.originatorId = adminPlayer.getObjectId()
    eventData.targetId = target.getObjectId()
    eventData.previousPoints = target.getStat().getVitalityPoints()
    eventData.nextPoints = points

    ListenerCache.sendGeneralEvent( EventType.GMSetVitalityPoints, eventData )
}

function onSetVitality( player: L2PcInstance, commandChunks: Array<string>, points: VitalityPointsPerLevel ) : void {
    let [ , playerName ] = commandChunks
    let targetPlayer = player

    if ( playerName ) {
        targetPlayer = L2World.getPlayerByName( playerName )

        if ( !targetPlayer ) {
            return player.sendMessage( `Unable to find player with name ${playerName}` )
        }
    }

    notifyListeners( player, targetPlayer, points )
    targetPlayer.setVitalityPoints( points, true )
    player.sendMessage( `Set player vitality to ${points}` )
}

function onSetVitalityLevel( player: L2PcInstance, commandChunks: Array<string> ) : void {
    let [ , levelValue, playerName ] = commandChunks

    let level : VitalityLevel = _.parseInt( levelValue )
    if ( !levelValue || level < VitalityLevel.None || level > VitalityLevel.Top ) {
        return player.sendMessage( 'Incorrect vitality level value!' )
    }

    let targetPlayer = player

    if ( playerName ) {
        targetPlayer = L2World.getPlayerByName( playerName )

        if ( !targetPlayer ) {
            return player.sendMessage( `Unable to find player with name ${playerName}` )
        }
    }

    let points = _.get( VitalityLevelPoints, level, VitalityPointsPerLevel.None )

    notifyListeners( player, targetPlayer, points )
    targetPlayer.setVitalityPoints( points, true )
    player.sendMessage( `Set player "${targetPlayer.getName()}" vitality points to ${points}` )
}

function compareItems( one: ScanItem, two: ScanItem ): number {
    if ( one.distance < two.distance ) {
        return -1
    }

    if ( one.distance > two.distance ) {
        return 1
    }

    return 0
}

function onShowPlayerVitalityNearby( player: L2PcInstance, commandChunks: Array<string> ) : void {
    let [ , radiusValue ] = commandChunks
    let radius = Math.min( _.defaultTo( _.parseInt( radiusValue ), 500 ), 20000 )

    let playerTemplate = DataManager.getHtmlData().getItem( 'overrides/html/command/vitality/playerScanItem.htm' )
    let html: string = DataManager.getHtmlData().getItem( 'overrides/html/command/vitality/playerScan.htm' )
    let htmlSize: number = html.length

    let results : Array<ScanItem> = L2World.getVisiblePlayers( player, radius, true ).map( ( otherPlayer: L2PcInstance ): ScanItem => {
        return {
            otherPlayer,
            distance: Math.round( player.calculateDistance( otherPlayer ) ),
        }
    } ).sort( compareItems )

    let lines: Array<string> = []
    let command : string = `bypass admin_${ VitalityCommand.SetLevel }`

    results.some( ( item: ScanItem ) : boolean => {
        let playerName = player === item.otherPlayer ? `<font color=00FF00>${item.otherPlayer.getName()}</font> (You)` : item.otherPlayer.getName()
        let scanItemText: string = playerTemplate
                .replace( '%name%', playerName )
                .replace( '%vitality%', item.otherPlayer.getStat().getVitalityLevel().toString() )
                .replace( '%points%', item.otherPlayer.getStat().getVitalityPoints().toString() )
                .replaceAll( '%playerName%', item.otherPlayer.getName() )
                .replaceAll( '%command%', command )
        htmlSize = htmlSize + scanItemText.length

        if ( GeneralHelper.exceedsHtmlPacketSize( htmlSize ) ) {
            return true
        }

        lines.push( scanItemText )
        return false
    } )

    let finalHtml = html.replace( '%data%', lines.join( '' ) )
                    .replace( '%amount%', lines.length.toString() )
                    .replace( '%size%', results.length.toString() )
                    .replace( '%radius%', radius.toString() )

    return player.sendOwnedData( NpcHtmlMessage( finalHtml, player.getObjectId() ) )
}