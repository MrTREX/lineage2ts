import { IAdminCommand } from '../IAdminCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { formatOptionsDescription } from './helpers/FormatHelpers'
import { AdminCommandManager } from '../managers/AdminCommandManager'
import { NpcHtmlMessage } from '../../packets/send/NpcHtmlMessage'
import { DataManager } from '../../../data/manager'
import { GeneralHelper } from '../../helpers/GeneralHelper'
import _ from 'lodash'

const enum HelpCommands {
    help = 'help',
    explain = 'explain'
}

export const Help: IAdminCommand = {
    getCommandNames(): Array<string> {
        return [
            HelpCommands.help,
            HelpCommands.explain,
        ]
    },

    getDescription( command: string ): Array<string> {
        switch ( command ) {
            case HelpCommands.help:
                return [
                    formatOptionsDescription( HelpCommands.help, '[ list | letter range | letter ]', 'Shows list of available commands and their descriptions' ),
                    'list - option only shows available commands',
                    'letter range - Specify to show commands between particular letter range, ex. A-F',
                    'letter - Specify starting letter of commands',
                    'Example: //help list',
                    'Invoking //help will automatically invoke //help A-F',
                ]

            case HelpCommands.explain:
                return [
                    formatOptionsDescription( HelpCommands.explain, '< commandName >', 'Shows single command and its description' ),
                ]
        }
    },

    async onCommand( command: string, player: L2PcInstance ): Promise<void> {
        const commandChunks = command.split( ' ' )
        switch ( commandChunks[ 0 ] ) {
            case HelpCommands.help:
                return onShowAllCommands( commandChunks, player )

            case HelpCommands.explain:
                return onExplainCommand( commandChunks, player )
        }
    },
}

function onShowAllCommands( commandChunks: Array<string>, player: L2PcInstance ): void {
    let [ , subCommand ] = commandChunks

    if ( subCommand === 'list' ) {
        let path = 'overrides/html/command/help/helpList.htm'
        let htmlItem = DataManager.getHtmlData().getItem( 'overrides/html/command/help/helpListItem.htm' )
        let items: Array<string> = AdminCommandManager.getAllCommands().map( ( command: string ): string => {
            return htmlItem.replace( /%command%/g, command )
        } )

        let html = DataManager.getHtmlData().getItem( path )
                              .replace( '%items%', items.join( '' ) )
        return player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId() ) )
    }

    if ( !subCommand ) {
        subCommand = 'a'
    }

    let characters : Array<number>

    if ( subCommand.includes( '-' ) ) {
        let characterCodes = subCommand.toLowerCase().split( '-' ).map( ( value: string ) => value.charCodeAt( 0 ) )
        characters = _.range( _.min( characterCodes ), _.max( characterCodes ) )
    } else {
        characters = [ subCommand.toLowerCase().charCodeAt( 0 ) ]
    }

    let path = 'overrides/html/command/help/helpFull.htm'
    let htmlItem = DataManager.getHtmlData().getItem( 'overrides/html/command/help/helpFullItem.htm' )
    let items: Array<string> = characters.map( ( code: number ): string => {
        let commands: Array<string> = AdminCommandManager.getCommandsByStartLetter( String.fromCharCode( code ) )
        if ( !commands ) {
            return
        }

        return commands.map( ( command: string ): string => {
            let lines = AdminCommandManager.getHandler( `admin_${ command }` ).getDescription( command )
            let description = lines ? lines.join( '<br>' ) : 'No description'

            return htmlItem
                    .replace( /%command%/g, command )
                    .replace( '%description%', description )
        } ).join( '' )
    } )

    let html = DataManager.getHtmlData().getItem( path )
                          .replace( '%items%', _.compact( items ).join( '' ) )

    if ( GeneralHelper.exceedsHtmlPacketSize( html.length ) ) {
        return player.sendMessage( 'Html cannot be rendered.Requested range of commands is too large.' )
    }

    return player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId() ) )
}

function onExplainCommand( commandChunks: Array<string>, player: L2PcInstance ): void {
    let [ , commandName ] = commandChunks

    let handler = AdminCommandManager.getHandler( `admin_${ commandName }` )
    if ( !handler ) {
        return player.sendMessage( `Cannot find command //${ commandName }` )
    }

    let lines: Array<string> = handler.getDescription( commandName )
    if ( !lines ) {
        return player.sendMessage( `No description available for command //${ commandName }` )
    }

    lines.forEach( ( line: string ) => player.sendMessage( line ) )
}