import { IAdminCommand } from '../IAdminCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2World } from '../../L2World'
import { L2Npc } from '../../models/actor/L2Npc'
import { DataManager } from '../../../data/manager'
import { createGmStatsBypass } from '../bypasshandlers/viewNpc'
import _ from 'lodash'
import { InstanceType } from '../../enums/InstanceType'
import { CommunityBoardHtmlLimit, GeneralHelper } from '../../helpers/GeneralHelper'
import { createTeleportToObjectBypass } from './Teleport'
import { formatOptionsDescription } from './helpers/FormatHelpers'

interface ScanItem {
    npc: L2Npc
    distance: number
}

const enum ScanCommands {
    NpcInRadius = 'npc-scan'
}

export const Scan: IAdminCommand = {
    getCommandNames(): Array<string> {
        return [
            ScanCommands.NpcInRadius,
        ]
    },

    getDescription( command: string ): Array<string> {
        switch ( command ) {
            case ScanCommands.NpcInRadius:
                return [
                    formatOptionsDescription( ScanCommands.NpcInRadius, '[ radius = 1000 ]', 'Displays npc information in specified radius.' ),
                ]
        }
    },

    async onCommand( command: string, player: L2PcInstance ): Promise<void> {
        let commandChunks: Array<string> = command.split( ' ' )

        switch ( commandChunks[ 0 ] ) {
            case ScanCommands.NpcInRadius:
                return onScan( commandChunks, player )
        }
    },
}

function compareItems( one: ScanItem, two: ScanItem ): number {
    if ( one.distance < two.distance ) {
        return -1
    }

    if ( one.distance > two.distance ) {
        return 1
    }

    return 0
}

function onScan( commandChunks: Array<string>, player: L2PcInstance ): void {
    let [ currentCommand, radiusValue ] = commandChunks

    let radius = radiusValue ? Math.max( 10000, _.parseInt( radiusValue ) ) : 1000
    let lines: Array<string> = []
    let npcTemplate = DataManager.getHtmlData().getItem( 'overrides/html/command/scan/npcScanItem.html' )

    let results: Array<ScanItem> = L2World.getVisibleNpcs( player, radius, false ).map( ( npc: L2Npc ): ScanItem => {
        return {
            npc,
            distance: Math.round( player.calculateDistance( npc ) ),
        }
    } ).sort( compareItems )

    let html: string = DataManager.getHtmlData().getItem( 'overrides/html/command/scan/npcScan.htm' )
    let htmlSize: number = html.length

    results.some( ( item: ScanItem ) : boolean => {
        let scanItemText: string = npcTemplate
                .replace( '%npcId%', item.npc.getId().toString() )
                .replace( '%npcName%', item.npc.getName() )
                .replace( '%distance%', item.distance.toString() )
                .replace( '%viewNpc%', createGmStatsBypass( item.npc.getObjectId() ) )
                .replace( '%type%', InstanceType[ item.npc.getInstanceType() ] )
                .replace( '%teleport%', createTeleportToObjectBypass( item.npc.getObjectId(), Math.floor( item.npc.getTemplate().getCollisionRadius() + player.getCollisionRadius() + 20 ) ) )

        htmlSize = htmlSize + scanItemText.length

        if ( htmlSize > CommunityBoardHtmlLimit ) {
            return true
        }

        lines.push( scanItemText )
        return false
    } )


    GeneralHelper.sendCommunityBoardHtml( player, html.replace( '%data%', lines.join( '' ) )
                                                      .replace( '%amount%', lines.length.toString() )
                                                      .replace( '%size%', results.length.toString() )
                                                      .replace( '%radius%', radius.toString() ) )
}