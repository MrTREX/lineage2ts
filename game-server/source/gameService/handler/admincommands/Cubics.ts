import { IAdminCommand } from '../IAdminCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { formatDescription, formatOptionsDescription } from './helpers/FormatHelpers'
import { L2World } from '../../L2World'
import { DataManager } from '../../../data/manager'
import { L2CubicInstance } from '../../models/actor/instance/L2CubicInstance'
import { NpcHtmlMessage } from '../../packets/send/NpcHtmlMessage'
import _ from 'lodash'
import { SummonCubic } from '../../effects/instant/SummonCubic'
import { GeneralHelper } from '../../helpers/GeneralHelper'
import { Skill } from '../../models/Skill'
import {
    L2CubicDataHealTarget,
    L2CubicDataItem,
    L2CubicDataSkill,
    L2CubicDataTargetType
} from '../../../data/interface/CubicDataApi'

const enum CubicCommands {
    Target = 'cubic-target',
    AddCubic = 'cubic-add',
    RemoveCubic = 'cubic-remove',
    Info = 'cubic-info',
    Menu = 'cubic-menu'
}

const idToName: Record<string, string> = {
    1: 'Storm cubic',
    2: 'Vampiric cubic',
    3: 'Life cubic',
    4: 'Viper cubic',
    5: 'Poltergeist cubic',
    6: 'Binding cubic',
    7: 'Aqua cubic',
    8: 'Spark cubic',
    9: 'Attract cubic',
    10: 'EvaTemplar cubic',
    11: 'ShillienTemplar cubic',
    12: 'ArcanaLord cubic',
    13: 'ElementalMaster cubic',
    14: 'SpectralMaster cubic'
}

export const CubicAdministration: IAdminCommand = {
    getCommandNames(): Array<string> {
        return [
            CubicCommands.Target,
            CubicCommands.AddCubic,
            CubicCommands.RemoveCubic,
            CubicCommands.Info,
            CubicCommands.Menu
        ]
    },

    getDescription( command: string ): Array<string> {
        switch ( command ) {
            case CubicCommands.Target:
                return [
                    formatOptionsDescription( CubicCommands.Target, '[ name ]', 'Show available cubics on player specified by name or selected target' ),
                ]

            case CubicCommands.AddCubic:
                return [
                    formatOptionsDescription( CubicCommands.AddCubic, '[ name ] <id> [ level = 1 ]', 'Add/replace cubic for player specified by name or selected target' ),
                ]

            case CubicCommands.RemoveCubic:
                return [
                    formatOptionsDescription( CubicCommands.RemoveCubic, '[ name ] <id> | all', 'Remove cubic from player specified by name or selected target' ),
                    'Instead of id, a keyword "all" can be specified to remove all available cubics.',
                ]

            case CubicCommands.Info:
                return [
                    formatOptionsDescription( CubicCommands.Info, '<id>', 'Show cubic data for given id.' ),
                ]

            case CubicCommands.Menu:
                return [
                    formatDescription( CubicCommands.Menu, 'Show available cubic operations' )
                ]
        }
    },

    async onCommand( command: string, player: L2PcInstance ): Promise<void> {
        let commandChunks = command.split( ' ' )

        switch ( commandChunks[ 0 ] ) {
            case CubicCommands.Target:
                return showTargetCubics( commandChunks, player )

            case CubicCommands.Info:
                return showInfo( commandChunks, player )

            case CubicCommands.RemoveCubic:
                return onRemoveCubic( commandChunks, player )

            case CubicCommands.AddCubic:
                return onAddCubic( commandChunks, player )

            case CubicCommands.Menu:
                return onMenu( player )
        }
    },
}

function getPlayer( name: string, adminPlayer: L2PcInstance ): L2PcInstance {
    if ( name ) {
        return L2World.getPlayerByName( name )
    }

    return adminPlayer.getTarget() as L2PcInstance
}

function showTargetCubics( commandChunks: Array<string>, player: L2PcInstance ): void {
    let [ , name ] = commandChunks

    let targetPlayer: L2PcInstance = getPlayer( name, player )

    if ( !targetPlayer || !targetPlayer.isPlayer() ) {
        return player.sendMessage( 'No target player found.' )
    }

    if ( _.size( targetPlayer.getCubics() ) === 0 ) {
        return player.sendMessage( `No cubics available on target '${ targetPlayer.getName() }'` )
    }

    let htmlItem = DataManager.getHtmlData().getItem( 'overrides/html/command/cubics/target-item.htm' )
    let skillItem = DataManager.getHtmlData().getItem( 'overrides/html/command/cubics/target-skill.htm' )
    let itemsHtml: Array<string> = []

    targetPlayer.getCubics().forEach( ( cubic: L2CubicInstance ) => {
        let cubicName = idToName[ cubic.cubicId ]
        let skillItems: Array<string> = cubic.skills.map( ( skill: Skill, index: number ): string => {
            return skillItem
                .replaceAll( '%counter%', ( index + 1 ).toString() )
                .replace( '%name%', skill.getName() )
                .replace( '%skillId%', skill.getId().toString() )
                .replace( '%skillLevel%', skill.getLevel().toString() )
        } )

        itemsHtml.push( htmlItem
            .replace( '%id%', cubic.cubicId.toString() )
            .replace( '%name%', cubicName ?? 'No cubic data' )
            .replace( '%skillSize%', cubic.skills.length.toString() )
            .replace( '%uses%', cubic.currentUseCount.toString() )

            .replace( '%maxUses%', cubic.maxUseCount.toString() )
            .replace( '%expiration%', `in ${ GeneralHelper.formatMillis( cubic.expireTime - Date.now(), true ).join( ', ' ) }` )
            .replace( '%delay%', cubic.actionDelaySeconds.toString() )
            .replace( '%chance%', `${ cubic.chance } %` )

            .replace( '%useChances%', cubic.useChances.length > 0 ? cubic.useChances.map( value => `${ value }%` ).join( ', ' ) : 'No values' )
            .replace( '%power%', cubic.power.toString() )
            .replace( '%skills%', skillItems.join( '' ) )
        )
    } )

    let html = DataManager.getHtmlData().getItem( 'overrides/html/command/cubics/target.htm' )
        .replace( '%size%', targetPlayer.getCubics().size.toString() )
        .replace( '%playerName%', targetPlayer.getName() )
        .replace( '%items%', itemsHtml.join( '' ) )

    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId() ) )
}

function showInfo( commandChunks: Array<string>, player: L2PcInstance ): void {
    let [ , idValue ] = commandChunks

    if ( !idValue ) {
        return player.sendMessage( 'Please provide cubic id.' )
    }

    let id = _.parseInt( idValue )
    if ( !id ) {
        return player.sendMessage( 'Invalid cubic id provided.' )
    }

    let cubicItems = DataManager.getCubics().getAllById( id )
    if ( !cubicItems ) {
        return player.sendMessage( 'Unable to find cubic with provided id.' )
    }

    let cubicHtml = DataManager.getHtmlData().getItem( 'overrides/html/command/cubics/data-item.htm' )
    let skillHtml = DataManager.getHtmlData().getItem( 'overrides/html/command/cubics/data-skill.htm' )
    let cubicName = idToName[ id ]

    let items: Array<string> = cubicItems.map( ( data: L2CubicDataItem ): string => {
        let skillItems: Array<string> = data.skills.map( ( item: L2CubicDataSkill ): string => {
            return skillHtml
                .replace( '%id%', item.id.toString() )
                .replace( '%level%', item.level.toString() )
                .replace( '%activationChance%', ( item.activationChance * 100 ).toString() )
                .replace( '%useChance%', ( item.useChance * 100 ).toString() )
        } )

        return cubicHtml
            .replace( '%skillSize%', skillItems.length.toString() )
            .replace( '%id%', data.id.toString() )
            .replace( '%skills%', skillItems.join( '' ) )
            .replace( '%level%', data.level.toString() )

            .replace( '%delay%', data.delay.toString() )
            .replace( '%maxActions%', data.maxActions.toString() )
            .replace( '%power%', data.power.toString() )
            .replace( '%target%', L2CubicDataTargetType[ data.target.type ] )

            .replace( '%targetHp%', data.target.type === L2CubicDataTargetType.HealSelection
                ? ( data.target as L2CubicDataHealTarget ).hpThreshold.toString()
                : ( data.condition ? data.condition.applyHp.toString() : 'none' ) )
            .replace( '%chance%', data.condition ? ( data.condition.chance * 100 ).toString() : '1' )
    } )

    let applyInfo = DataManager.getHtmlData().getItem( 'overrides/html/command/cubics/data-apply.htm' )
        .replace( '%cubicLevels%', cubicItems.map( item => item.level ).sort().join( ';' ) )
        .replace( '%bypassTarget%', `bypass admin_${ CubicCommands.AddCubic } ${ id } $level` )
        .replace( '%bypassSelf%', `bypass admin_${ CubicCommands.AddCubic } ${ player.getName() } ${ id } $level` )

    let html = DataManager.getHtmlData().getItem( 'overrides/html/command/cubics/data.htm' )
        .replaceAll( '%name%', cubicName )
        .replace( '%id%', id.toString() )
        .replace( '%items%', items.join( '' ) )
        .replace( '%applyInfo%', applyInfo )

    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId() ) )
}

function onRemoveCubic( commandChunks: Array<string>, player: L2PcInstance ): void {
    let [ , nameOrId, idValue ] = commandChunks

    let targetPlayer: L2PcInstance = getPlayer( idValue ? nameOrId : null, player )

    if ( !targetPlayer || !targetPlayer.isPlayer() ) {
        return player.sendMessage( 'No target player found.' )
    }

    let id = idValue ? idValue : nameOrId
    if ( id === 'all' ) {
        let size = targetPlayer.getCubics().size

        if ( size === 0 ) {
            return player.sendMessage( 'Target has no cubics to remove.' )
        }

        targetPlayer.stopCubics()
        return player.sendMessage( `Removed ${ size } cubics from '${ targetPlayer.getName() }'` )
    }

    let parsedId = _.parseInt( id )
    if ( !parsedId ) {
        return player.sendMessage( 'Incorrect cubic id value provided.' )
    }

    if ( !targetPlayer.getCubics().get( parsedId ) ) {
        return player.sendMessage( `Target '${ targetPlayer.getName() }' has no active cubics with id ${ parsedId }` )
    }

    targetPlayer.removeCubic( parsedId )
    targetPlayer.broadcastUserInfo()

    player.sendMessage( `Removed cubic with id=${ parsedId } from '${ targetPlayer.getName() }'` )
}

function onAddCubic( commandChunks: Array<string>, player: L2PcInstance ): void {

    let [ , nameOrId, idValue, levelValue ] = commandChunks

    let targetPlayer: L2PcInstance = getPlayer( idValue ? nameOrId : null, player )

    if ( !targetPlayer || !targetPlayer.isPlayer() ) {
        return player.sendMessage( 'No target player found.' )
    }

    let id = idValue ? idValue : nameOrId
    let parsedId = _.parseInt( id )
    if ( !parsedId ) {
        return player.sendMessage( 'Incorrect cubic id value provided.' )
    }

    let level = levelValue ? _.parseInt( levelValue ) : 1
    if ( !level ) {
        level = 1
    }

    let skills = SummonCubic.getCubicSkills( parsedId, level )
    let cubic = new L2CubicInstance( targetPlayer, parsedId, skills, 282, 5, 99, 10, 90, [], false )

    targetPlayer.addCubic( cubic )
    targetPlayer.broadcastUserInfo()
}

function onMenu( player: L2PcInstance ): void {
    let menuItem = DataManager.getHtmlData().getItem( 'overrides/html/command/cubics/menu-item.htm' )

    let items: Array<string> = _.map( idToName, ( name: string, id: string ): string => {
        return menuItem
            .replaceAll( '%cubicId%', id )
            .replace( '%name%', name )
    } )

    let html = DataManager.getHtmlData().getItem( 'overrides/html/command/cubics/menu.htm' )
        .replace( '%items%', items.join( '' ) )

    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId() ) )
}