import { IAdminCommand } from '../IAdminCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ClassIdValues } from '../../models/base/ClassId'
import { L2World } from '../../L2World'
import { EventType, GMChangeClassIdEvent } from '../../models/events/EventType'
import { ListenerCache } from '../../cache/ListenerCache'
import { DataManager } from '../../../data/manager'
import { formatOptionsDescription } from './helpers/FormatHelpers'

const enum PlayerClassIdCommands {
    Set = 'set-player-classId'
}

export const PlayerClassId: IAdminCommand = {
    getCommandNames(): Array<string> {
        return [
            PlayerClassIdCommands.Set,
        ]
    },

    getDescription( command: string ): Array<string> {
        switch ( command ) {
            case PlayerClassIdCommands.Set:
                return [
                    formatOptionsDescription( PlayerClassIdCommands.Set, '<className> [playerName]', 'Sets target player\'s class to specified classId' ),
                    'If name is provided, it is used instead of selected target',
                ]
        }
    },

    async onCommand( command: string, player: L2PcInstance ): Promise<void> {
        let [ , className, playerName ] = command.split( ' ' )

        if ( !className || !ClassIdValues[ className ] ) {
            player.sendMessage( 'Incorrect value for class id.' )
            return
        }

        let targetPlayer: L2PcInstance = getTargetPlayer( player, playerName )
        if ( !targetPlayer ) {
            player.sendMessage( 'Incorrect target player.' )
            return
        }

        let classId: number = ClassIdValues[ className ].id

        if ( ListenerCache.hasGeneralListener( EventType.GMChangeClassId ) ) {
            let data: GMChangeClassIdEvent = {
                originatorId: player.getObjectId(),
                receiverId: targetPlayer.getObjectId(),
                receiverNextClassId: classId,
                receiverPreviousClassId: targetPlayer.getClassId(),
            }

            let result = await ListenerCache.getGeneralTerminatedResult( EventType.GMChangeClassId, data )
            if ( result && result.terminate ) {
                return
            }
        }

        await targetPlayer.setClassId( classId )

        if ( targetPlayer.isSubClassActive() ) {
            targetPlayer.getSubClasses()[ targetPlayer.getClassIndex() ].setClassId( targetPlayer.getActiveClass() )
        } else {
            targetPlayer.setBaseClass( targetPlayer.getActiveClass() )
        }

        targetPlayer.broadcastUserInfo()

        let name = DataManager.getClassListData().getClassInfo( classId ).name

        player.sendMessage( `${ targetPlayer.getName() } class has been changed to - ${ name }` )
        targetPlayer.sendMessage( `GM '${ player.getName() }' has changed your class to - ${ name }` )
    },
}

function getTargetPlayer( player: L2PcInstance, name: string ): L2PcInstance {
    if ( !name ) {
        let target = player.getTarget()

        if ( target.isPlayer() ) {
            return target as L2PcInstance
        }

        return
    }

    return L2World.getPlayerByName( name )
}