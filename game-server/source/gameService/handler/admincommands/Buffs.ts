import { IAdminCommand } from '../IAdminCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2World } from '../../L2World'
import { L2Character } from '../../models/actor/L2Character'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { L2Object } from '../../models/L2Object'
import { SkillCoolTime } from '../../packets/send/SkillCoolTime'
import { ConfigManager } from '../../../config/ConfigManager'
import { SkillCache } from '../../cache/SkillCache'
import { Skill } from '../../models/Skill'
import {
    EventType,
    GMCancelBuffsInAreaEvent,
    GMRemovedAllBuffsEvent,
    GMRemovedBuffEvent,
    GMViewBuffsEvent,
} from '../../models/events/EventType'
import { ListenerCache } from '../../cache/ListenerCache'
import { AbnormalType } from '../../models/skills/AbnormalType'
import { NpcHtmlMessage } from '../../packets/send/NpcHtmlMessage'
import { InstanceType } from '../../enums/InstanceType'
import _ from 'lodash'
import { formatOptionsDescription } from './helpers/FormatHelpers'
import { BuffDefinition } from '../../models/skills/BuffDefinition'

const showBuffsStartHtml: string = `
<html><table width="100%"><tr>
<td width=45><button value="Main" action="bypass -h admin_admin" width=45 height=21 back="L2UI_ct1.button_df" fore="L2UI_ct1.button_df"></td>
<td width=180><center><font color="LEVEL">Effects of %targetName%</font></td>
<td width=45><button value="Back" action="bypass -h admin_current_player" width=45 height=21 back="L2UI_ct1.button_df" fore="L2UI_ct1.button_df"></td>
</tr></table><br>
<table width="100%"><tr><td width=200>Skill</td><td width=30>Remaining</td><td width=70>Action</td></tr>`

const enum BuffCommands {
    GetBuffs = 'getbuffs',
    StopBuff = 'stopbuff',
    StopAllBuffs = 'stopallbuffs',
    ResetReuse = 'removereuse',
    CancelBuffsInArea = 'areacancel',
    SwitchGmBuffs = 'switch-gm-buffs'
}

export const Buffs: IAdminCommand = {
    getCommandNames(): Array<string> {
        return [
            BuffCommands.GetBuffs,
            BuffCommands.StopBuff,
            BuffCommands.StopAllBuffs,
            BuffCommands.ResetReuse,
            BuffCommands.CancelBuffsInArea,
            BuffCommands.SwitchGmBuffs,
        ]
    },

    // TODO : complete description
    getDescription( command: string ): Array<string> {
        switch ( command ) {
            case BuffCommands.StopAllBuffs:
                return [ formatOptionsDescription( BuffCommands.StopAllBuffs, '< objectId>', '' ) ]

            case BuffCommands.StopBuff:
                return [ formatOptionsDescription( BuffCommands.StopBuff, '< objectId > < skillId >', '' ) ]
        }
    },

    async onCommand( command: string, player: L2PcInstance ): Promise<void> {
        let commandChunks: Array<string> = _.split( command, ' ' )
        let currentCommand = _.head( commandChunks )

        switch ( currentCommand ) {
            case BuffCommands.GetBuffs:
                return onGetBuffs( commandChunks, player )

            case BuffCommands.StopBuff:
                return onStopBuff( commandChunks, player )

            case BuffCommands.StopAllBuffs:
                return onStopAllBuffs( commandChunks, player )

            case BuffCommands.CancelBuffsInArea:
                return onAreaCancel( commandChunks, player )

            case BuffCommands.ResetReuse:
                return onRemoveReuse( commandChunks, player )

            case BuffCommands.SwitchGmBuffs:
                return onSwitchGmBuffs( player )
        }
    },

}

async function onGetBuffs( commandChunks: Array<string>, player: L2PcInstance ): Promise<void> {
    let [ command, playerName, pageValue ] = commandChunks

    if ( playerName ) {
        let otherPlayer: L2PcInstance = L2World.getPlayerByName( playerName )

        if ( !otherPlayer ) {
            return player.sendMessage( `Player '${ playerName }' is not online.` )
        }

        let page = pageValue ? _.parseInt( pageValue ) : 1
        return showBuffs( player, otherPlayer, page, command.endsWith( '_ps' ) )
    }

    let target = player.getTarget()
    if ( target && target.isCharacter() ) {
        return showBuffs( player, target as L2Character, 1, command.endsWith( '_ps' ) )
    }

    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_IS_INCORRECT ) )
}

async function onStopBuff( commandChunks: Array<string>, player: L2PcInstance ): Promise<void> {
    let [ command, objectIdValue, skillIdValue ] = commandChunks

    let objectId: number = objectIdValue ? _.parseInt( objectIdValue ) : 0
    let skillId: number = skillIdValue ? _.parseInt( skillIdValue ) : 0

    return removeBuff( player, objectId, skillId )
}

async function onStopAllBuffs( commandChunks: Array<string>, player: L2PcInstance ): Promise<void> {
    let [ command, objectIdValue ] = commandChunks

    let objectId: number = objectIdValue ? _.parseInt( objectIdValue ) : 0

    return removeAllBuffs( player, objectId )
}

async function onAreaCancel( commandChunks: Array<string>, player: L2PcInstance ): Promise<void> {
    let [ command, radiusValue ] = commandChunks

    let radius: number = radiusValue ? _.parseInt( radiusValue ) : 300
    let affectedPlayerIds: Array<number> = []

    _.each( L2World.getVisiblePlayers( player, radius, false ), ( otherPlayer: L2PcInstance ) => {
        if ( otherPlayer ) {
            otherPlayer.stopAllEffects()
            affectedPlayerIds.push( otherPlayer.getObjectId() )
        }
    } )

    if ( ListenerCache.hasGeneralListener( EventType.GMCancelBuffsInArea ) ) {
        let data: GMCancelBuffsInAreaEvent = {
            affectedPlayerIds,
            originatorId: player.getObjectId(),
            radius,
        }

        await ListenerCache.sendGeneralEvent( EventType.GMCancelBuffsInArea, data )
    }

    player.sendMessage( `All effects canceled within radius ${ radius } affecting ${ affectedPlayerIds.length } players.` )
}

async function onRemoveReuse( commandChunks: Array<string>, player: L2PcInstance ): Promise<void> {
    let [ command, playerName ] = commandChunks

    let target: L2Object
    if ( playerName ) {
        target = L2World.getPlayerByName( playerName )
        if ( !target ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_IS_NOT_FOUND_IN_THE_GAME ) )
            return
        }
    } else {
        target = player.getTarget()
        if ( !target || !target.isCharacter() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_IS_INCORRECT ) )
            return
        }
    }

    if ( target.isPlayer() ) {
        let otherPlayer = target as L2PcInstance

        otherPlayer.resetSkillReuse()
        otherPlayer.resetDisabledSkills()
        otherPlayer.sendDebouncedPacket( SkillCoolTime )
    }

    player.sendMessage( `Skill reuse was removed from '${ target.getName() }'` )
}

async function onSwitchGmBuffs( player: L2PcInstance ): Promise<void> {
    if ( ConfigManager.general.gmGiveSpecialSkills() !== ConfigManager.general.gmGiveSpecialAuraSkills() ) {
        let toAuraSkills: boolean = !!player.getKnownSkill( 7041 )

        await SkillCache.switchGMSkills( player, toAuraSkills )

        player.sendSkillList()
        player.sendMessage( `You have successfully changed to target ${ toAuraSkills ? 'aura' : 'one' } special skills.` )
        return
    }

    player.sendMessage( 'There is nothing to switch.' )
}

async function removeBuff( player: L2PcInstance, objectId: number, skillId: number ): Promise<void> {
    let target: L2Character = L2World.getObjectById( objectId ) as L2Character

    if ( !target || skillId <= 0 ) {
        return
    }

    if ( target.isAffectedBySkill( skillId ) ) {
        await target.stopSkillEffects( true, skillId )
        player.sendMessage( `Removed skill ID: ${ skillId } (${ SkillCache.getSkill( skillId, 1 ).getName() }) effects from ${ target.getName() } (${ objectId }).` )
    }

    if ( ListenerCache.hasGeneralListener( EventType.GMRemovedBuff ) ) {
        let data: GMRemovedBuffEvent = {
            targetId: objectId,
            skillId,
            originatorId: player.getObjectId(),
        }

        await ListenerCache.sendGeneralEvent( EventType.GMRemovedBuff, data )
    }

    showBuffs( player, target, 1, false )
}

async function removeAllBuffs( player: L2PcInstance, objectId: number ): Promise<void> {
    let target: L2Character = L2World.getObjectById( objectId ) as L2Character

    if ( !target ) {
        return
    }

    await target.stopAllEffects()
    player.sendMessage( `Removed all effects from '${ target.getName() }' (${ objectId })` )

    if ( ListenerCache.hasGeneralListener( EventType.GMRemovedAllBuffs ) ) {
        let data: GMRemovedAllBuffsEvent = {
            targetId: objectId,
            originatorId: player.getObjectId(),
        }

        await ListenerCache.sendGeneralEvent( EventType.GMRemovedAllBuffs, data )
    }

    showBuffs( player, target, 1, false )
}

function wrapRedColor( text: string ): string {
    return `<font color="FF0000">${ text }</font>`
}

function showBuffs( player: L2PcInstance, target: L2Character, pageNumber: number, isPassive: boolean ): void {
    let effects: Array<BuffDefinition> = isPassive ? target.getEffectList().getPassives().map( buff => buff.getDefinition() ) : target.getEffectList().getBuffDefinitions()

    let maxPages: number = Math.floor( effects.length / 20 )
    if ( pageNumber > ( maxPages + 1 ) || pageNumber < 1 ) {
        return
    }

    if ( effects.length % 20 > 0 ) {
        maxPages++
    }

    let lines = [
        showBuffsStartHtml.replace( '%targetName%', target.getName() ),
    ]

    let start: number = ( pageNumber - 1 ) * 20
    let end = Math.min( start + 20, effects.length )

    _.each( _.slice( effects, start, end ), ( info: BuffDefinition ) => {
        let skill: Skill = info.skill
        let skillInfo: string = `${ skill.getName() } Lvl ${ skill.getLevel() }`
        let skillState: string = skill.isToggle() ? 'Toggle' : skill.isPassive() ? 'Passive' : `${ info.timeLeftSeconds } s`
        lines.push( `<tr>
                    <td>${ !info.isInUse ? wrapRedColor( skillInfo ) : skillInfo }</td>
                    <td>${ skillState }</td>
                    <td><button value="X" action="bypass -h admin_stopbuff ${ target.getObjectId() } ${ skill.getId() }" width=30 height=21 back="L2UI_ct1.button_df" fore="L2UI_ct1.button_df"></td></tr>` )
    } )

    lines.push( '</table><table width=300 bgcolor=444444><tr>' )
    _.times( maxPages, ( index: number ) => {
        let currentPage = index + 1
        if ( currentPage === pageNumber ) {
            lines.push( `<td>Page ${ currentPage }</td>` )
            return
        }

        lines.push( `<td><a action="bypass -h admin_getbuffs${ isPassive ? '_ps' : '' } ${ target.getName() } ${ currentPage }"> Page ${ currentPage } </a></td>` )
    } )

    lines.push( '</tr></table>' )
    lines.push( `<br><center><button value="Refresh" action="bypass -h admin_getbuffs${ isPassive ? '_ps' : '' } ${ target.getName() }" width=80 height=21 back="L2UI_ct1.button_df" fore="L2UI_ct1.button_df">` )
    lines.push( `<button value="Remove All" action="bypass -h admin_stopallbuffs ${ target.getObjectId() }" width=80 height=21 back="L2UI_ct1.button_df" fore="L2UI_ct1.button_df"><br>` )

    if ( !isPassive ) {
        lines.push( wrapRedColor( `Inactive buffs: ${ target.getEffectList().getHiddenBuffsCount() }` ) )
        lines.push( '<br>' )
    }

    lines.push( `Total ${ isPassive ? 'passive' : '' } buff count: ${ effects.length }` )

    let blockedSlots: Array<AbnormalType> = _.slice( target.getEffectList().getAllBlockedBuffSlots(), 0, 4 )
    if ( blockedSlots.length > 0 ) {
        lines.push( `<br>Blocked buff slots: ${ _.join( _.map( blockedSlots, ( slot: AbnormalType ) => AbnormalType[ slot ] ), ', ' ) }` )
    }

    lines.push( '</html>' )

    player.sendOwnedData( NpcHtmlMessage( lines.join( '' ), player.getObjectId() ) )

    if ( ListenerCache.hasGeneralListener( EventType.GMViewBuffs ) ) {
        let data: GMViewBuffsEvent = {
            originatorId: player.getObjectId(),
            targetId: target.getObjectId(),
            targetInstance: InstanceType[ target.getInstanceType() ],
        }

        ListenerCache.sendGeneralEvent( EventType.GMViewBuffs, data )
    }
}