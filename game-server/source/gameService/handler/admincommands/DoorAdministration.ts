import { IAdminCommand } from '../IAdminCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { CastleManager } from '../../instancemanager/CastleManager'
import { Castle } from '../../models/entity/Castle'
import { L2DoorInstance } from '../../models/actor/instance/L2DoorInstance'
import { L2World } from '../../L2World'
import { L2Object } from '../../models/L2Object'
import _ from 'lodash'
import { DoorManager } from '../../cache/DoorManager'

const enum DoorCommands {
    Open = 'open',
    Close = 'close',
    OpenAll = 'openall',
    CloseAll = 'closeall',
    OpenInRadius = 'open-radius',
    CloseInRadius = 'close-radius'
}

export const DoorAdministration: IAdminCommand = {
    getCommandNames(): Array<string> {
        return [
            DoorCommands.Open,
            DoorCommands.Close,
            DoorCommands.OpenAll,
            DoorCommands.CloseAll,
            DoorCommands.OpenInRadius,
            DoorCommands.CloseInRadius,
        ]
    },

    getDescription( command: string ): Array<string> {
        return [
            'Implement me',
        ]
    },

    async onCommand( command: string, player: L2PcInstance ): Promise<void> {
        let commandChunks: Array<string> = command.split( ' ' )

        switch ( commandChunks[ 0 ] ) {
            case DoorCommands.Open:
                return onOpen( commandChunks )

            case DoorCommands.Close:
                return onClose( commandChunks )

            case DoorCommands.OpenAll:
                return onOpenAll()

            case DoorCommands.CloseAll:
                return onCloseAll()

            case DoorCommands.OpenInRadius:
                return onOpenRadius( commandChunks, player )

            case DoorCommands.CloseInRadius:
                return onCloseRadius( commandChunks, player )
        }
    },
}

function onOpen( commandChunks: Array<string> ): void {
    let [ currentCommand, doorIdValue ] = commandChunks

    if ( !doorIdValue ) {
        return
    }

    let doorId = parseInt( doorIdValue )
    let door = DoorManager.getDoor( doorId )
    if ( door ) {
        door.openMe()
        return
    }

    CastleManager.getCastles().forEach( ( castle: Castle ) => {
        let castleDoor = castle.getDoor( doorId )
        if ( castleDoor ) {
            castleDoor.openMe()
            return false
        }
    } )
}

function onClose( commandChunks: Array<string> ): void {
    let [ currentCommand, doorIdValue ] = commandChunks

    if ( !doorIdValue ) {
        return
    }

    let doorId = parseInt( doorIdValue )
    let door = DoorManager.getDoor( doorId )
    if ( door ) {
        door.closeMe()
        return
    }

    CastleManager.getCastles().forEach( ( castle: Castle ) => {
        let castleDoor = castle.getDoor( doorId )
        if ( castleDoor ) {
            castleDoor.closeMe()
            return false
        }
    } )
}

function onOpenAll(): void {
    // TODO : target all doors in current geo-region

    CastleManager.getCastles().forEach( ( castle: Castle ) => {
        castle.getDoors().forEach( ( objectId: number ) => {
            let door = L2World.getObjectById( objectId ) as L2DoorInstance
            if ( door ) {
                door.openMe()
            }
        } )
    } )
}

function onCloseAll(): void {
    // TODO : target all doors in current geo-region

    CastleManager.getCastles().forEach( ( castle: Castle ) => {
        castle.getDoors().forEach( ( objectId: number ) => {
            let door = L2World.getObjectById( objectId ) as L2DoorInstance
            if ( door ) {
                door.closeMe()
            }
        } )
    } )
}

function onOpenRadius( commandChunks: Array<string>, player: L2PcInstance ) {
    let [ currentCommand, radiusValue ] = commandChunks

    let radius: number = radiusValue ? Math.max( _.parseInt( radiusValue ), 10 ) : 500

    let count = 0
    L2World.getVisibleObjects( player, radius ).forEach( ( object: L2Object ) => {
        if ( object && object.isDoor() ) {
            ( object as L2DoorInstance ).openMe()
            count++
        }
    } )

    player.sendMessage( `Opened ${ count } doors in radius ${ radius }` )
}

function onCloseRadius( commandChunks: Array<string>, player: L2PcInstance ) {
    let [ currentCommand, radiusValue ] = commandChunks

    let radius: number = radiusValue ? Math.max( _.parseInt( radiusValue ), 10 ) : 500

    let count = 0
    L2World.getVisibleObjects( player, radius ).forEach( ( object: L2Object ) => {
        if ( object && object.isDoor() ) {
            ( object as L2DoorInstance ).closeMe()
            count++
        }
    } )

    player.sendMessage( `Closed ${ count } doors in radius ${ radius }` )
}