import { IAdminCommand } from '../IAdminCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { NpcHtmlMessage } from '../../packets/send/NpcHtmlMessage'
import { L2World } from '../../L2World'
import { ILocational, Location } from '../../models/Location'
import { CharacterNamesCache } from '../../cache/CharacterNamesCache'
import { DatabaseManager } from '../../../database/manager'
import { Race } from '../../enums/Race'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'
import { AIIntent } from '../../aicontroller/enums/AIIntent'
import _ from 'lodash'
import { L2Object } from '../../models/L2Object'
import { AdminHtml } from './helpers/AdminHtml'
import { TeleportMode } from '../../enums/TeleportMode'
import { GeoPolygonCache } from '../../cache/GeoPolygonCache'
import { DataManager } from '../../../data/manager'
import { PlayerPvpFlag } from '../../enums/PlayerPvpFlag'
import { ActionFailed } from '../../packets/send/ActionFailed'
import { PlayerMovementActionCache } from '../../cache/PlayerMovementActionCache'

const enum Commands {
    TeleportToObject = 'teleport-object',
    TeleportPlayer = 'teleport_character',
    TeleportToCoordinates = 'teleport-coordinates',
    TeleportMode = 'teleport-mode',
    TeleportLocations = 'teleports'
}

// TODO : move to separate html file and avoid hardcoding bypass actions
const teleportHtml: string = `
<html><title>Teleport Character</title><body>
The character you will teleport is %name%<br>
Co-ordinate x<edit var="char_cord_x" width=110>
Co-ordinate y<edit var="char_cord_y" width=110>
Co-ordinate z<edit var="char_cord_z" width=110>
<button value="Teleport" action="bypass -h admin_${ Commands.TeleportPlayer } $char_cord_x $char_cord_y $char_cord_z" width=60 height=15 back="L2UI_ct1.button_df" fore="L2UI_ct1.button_df">
<button value="Teleport near you" action="bypass -h admin_${ Commands.TeleportPlayer } %locationX% %locationY% %locationZ%" width=115 height=15 back="L2UI_ct1.button_df" fore="L2UI_ct1.button_df">
<center><button value="Back" action="bypass -h admin_current_player" width=40 height=15 back="L2UI_ct1.button_df" fore="L2UI_ct1.button_df"></center>
</body></html>`.trim()

export function createTeleportToObjectBypass( objectId: number, offset: number = 20 ): string {
    return `bypass admin_${ Commands.TeleportToObject } ${ objectId } ${ offset }`
}

export function createTeleportToCoordinatesBypass( x: number, y: number, z: number ) : string {
    let adjustedZ = GeoPolygonCache.getZ( x, y, z )
    return `bypass admin_${ Commands.TeleportToCoordinates } ${ x } ${ y } ${adjustedZ}`
}

function teleportPlayer( player: L2PcInstance, targetX : number, targetY : number, targetZ : number ) : Promise<void> {
    player.sendOwnedData( ActionFailed() )
    let z = GeoPolygonCache.getZ( targetX, targetY, targetZ )
    if ( GeoPolygonCache.isInvalidZ( z ) ) {
        player.sendOwnedData( ActionFailed() )

        player.sendMessage( `Unable to teleport, bad geodata Z value for x=${targetX}, y=${targetY}` )
        return
    }

    return player.teleportToLocationCoordinates( targetX, targetY, z, player.getHeading() )
}

function teleportPlayerOnce( player: L2PcInstance, targetX : number, targetY : number, targetZ : number ) : Promise<void> {
    PlayerMovementActionCache.clearAction( player.getObjectId() )
    return teleportPlayer( player, targetX, targetY, targetZ )
}

export const Teleport: IAdminCommand = {
    getCommandNames(): Array<string> {
        return [
            'show_moves',
            'show_moves_other',
            'show_teleport',
            'teleport_to_character',
            'teleportto',
            'move_to',
            'teleport_character',
            'recall',
            'walk',
            'recall_npc',
            'gonorth',
            'gosouth',
            'goeast',
            'gowest',
            'goup',
            'godown',
            'tele',
            'teleto',
            'instant_move',
            'sendhome',
            Commands.TeleportToObject,
            Commands.TeleportToCoordinates,
            Commands.TeleportMode,
            Commands.TeleportLocations
        ]
    },

    getDescription( command: string ): Array<string> {
        return [
            'implement me',
        ]
    },

    async onCommand( command: string, player: L2PcInstance ): Promise<void> {
        let commandChunks: Array<string> = command.split( ' ' )

        switch ( commandChunks[ 0 ] ) {
            case 'instant_move':
                player.sendMessage( 'Instant move ready. Click where you want to go.' )
                return PlayerMovementActionCache.setAction( player.getObjectId(), teleportPlayerOnce )

            // TODO : remove L2J style teleport
            case 'teleto':
                let parameter: string = _.nth( commandChunks, 1 )
                if ( parameter === 'r' ) {
                    return PlayerMovementActionCache.setAction( player.getObjectId(), teleportPlayer )
                }

                if ( parameter === 'end' ) {
                    return PlayerMovementActionCache.clearAction( player.getObjectId() )
                }

                return

            case Commands.TeleportMode:
                let teleportMode = parseInt( commandChunks[ 1 ] )

                switch ( teleportMode ) {
                    case TeleportMode.None:
                        return PlayerMovementActionCache.clearAction( player.getObjectId() )

                    case TeleportMode.Single:
                        return PlayerMovementActionCache.setAction( player.getObjectId(), teleportPlayerOnce )

                    case TeleportMode.Repeated:
                        return PlayerMovementActionCache.setAction( player.getObjectId(), teleportPlayer )
                }

                return

            case 'show_moves':
            case Commands.TeleportLocations:
                return AdminHtml.showAdminHtml( player, 'teleports.htm' )

            case 'show_moves_other':
                return AdminHtml.showAdminHtml( player, 'tele/other.html' )

            case 'show_teleport':
                return onShowTeleportToPlayer( player )

            case 'teleport_to_character':
                return onTeleportToCharacter( player )

            case 'walk':
                return onWalk( commandChunks, player )

            case 'move_to':
            case Commands.TeleportToCoordinates:
                return onTeleportToCoordinates( commandChunks, player )

            case Commands.TeleportPlayer:
                return onTeleport( commandChunks, player )

            case 'teleportto':
                return onTeleportToPlayer( commandChunks, player )

            case 'recall':
                return onRecallPlayer( commandChunks, player )

            case 'tele':
                return AdminHtml.showAdminHtml( player, 'move.htm' )

            case 'go':
                return onTeleportToLocation( commandChunks, player )

            case 'sendhome':
                return onSendPlayerHome( commandChunks, player )

            case Commands.TeleportToObject:
                return onTeleportToObject( commandChunks, player )
        }
        return
    },
}

function onShowTeleportToPlayer( player: L2PcInstance ): void {
    let target = player.getTarget()
    if ( !target || !target.isPlayer() ) {
        return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
    }

    let html: string = teleportHtml
            .replace( '%name%', target.getName() )
            .replace( '%locationX%', target.getX().toString() )
            .replace( '%locationY%', target.getY().toString() )
            .replace( '%locationZ%', target.getZ().toString() )

    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId() ) )
}

function onTeleportToCharacter( player: L2PcInstance ): Promise<void> {
    let target = player.getTarget()
    if ( !target || !target.isPlayer() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
        return
    }

    return teleportToCharacter( player, target as L2PcInstance )
}

async function teleportToCharacter( player: L2PcInstance, otherPlayer: L2PcInstance ): Promise<void> {
    if ( player.getObjectId() === otherPlayer.getObjectId() ) {
        return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_USE_ON_YOURSELF ) )
    }

    player.setInstanceId( otherPlayer.getInstanceId() )
    await AIEffectHelper.setNextIntent( player, AIIntent.WAITING )
    player.sendMessage( `Teleported to player '${ player.getName() }'` )

    return player.teleportToLocation( otherPlayer, true )
}

function onTeleportToPlayer( commandChunks: Array<string>, player: L2PcInstance ): Promise<void> {
    let [ , playerName ] = commandChunks

    if ( _.isEmpty( playerName ) ) {
        player.sendMessage( 'Please specify player name' )
        return
    }

    let target = L2World.getPlayerByName( playerName )
    if ( !target ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_IS_NOT_FOUND_IN_THE_GAME ) )
        return
    }

    return teleportToCharacter( player, target )
}

function onWalk( commandChunks: Array<string>, player: L2PcInstance ): void {
    let coordinates: Array<number> = _.map( _.tail( commandChunks ), _.parseInt )

    if ( !_.some( coordinates, _.isNumber ) ) {
        return
    }

    let [ x, y, z ] = coordinates
    return AIEffectHelper.notifyMoveWithCoordinates( player, x, y, z, 0, player.getInstanceId() )
}

async function onTeleportToCoordinates( commandChunks: Array<string>, player: L2PcInstance ): Promise<void> {
    if ( commandChunks.length !== 4 ) {
        return
    }

    let coordinates: Array<number> = commandChunks.slice( 1 ).map( value => parseInt( value, 10 ) )

    if ( coordinates.some( value => !Number.isInteger( value ) ) ) {
        return
    }

    await AIEffectHelper.setNextIntent( player, AIIntent.WAITING )
    return player.teleportToLocationCoordinates( coordinates[ 0 ], coordinates[ 1 ], coordinates[ 2 ], player.getHeading() )
}

function onTeleport( commandChunks: Array<string>, player: L2PcInstance ): Promise<void> {
    let target = player.getTarget()
    if ( !target || !target.isPlayer() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
        return
    }

    if ( player.getObjectId() === target.getObjectId() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_USE_ON_YOURSELF ) )
        return
    }

    let coordinates: Array<number> = _.map( _.tail( commandChunks ), _.parseInt )

    if ( !_.some( coordinates, _.isNumber ) ) {
        AdminHtml.showAdminHtml( player, 'teleports.htm' )
        return
    }

    let [ x, y, z ] = coordinates
    return teleportTargetToLocation( player, target as L2PcInstance, new Location( x, y, z ) )
}

async function teleportTargetToLocation( player: L2PcInstance, target: L2PcInstance, location: ILocational ): Promise<void> {
    if ( target.isJailed() ) {
        return player.sendMessage( `Player '${ target.getName() }' is in jail, cannot teleport to jail.` )
    }

    target.setInstanceId( player.getInstanceId() )
    target.sendMessage( 'You are being teleported by admin.' )

    await AIEffectHelper.setNextIntent( target, AIIntent.WAITING )
    return target.teleportToLocation( location, true )
}

async function onRecallPlayer( commandChunks: Array<string>, player: L2PcInstance ): Promise<void> {
    let [ , playerName ] = commandChunks

    if ( _.isEmpty( playerName ) ) {
        return player.sendMessage( 'Please specify player name' )
    }

    let target = L2World.getPlayerByName( playerName )
    let objectId = await CharacterNamesCache.getIdByName( playerName )
    if ( !target && !objectId ) {
        return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
    }

    if ( target ) {
        return teleportTargetToLocation( player, target, player )
    }

    await DatabaseManager.getCharacterTable().updateLocation( objectId, player.getX(), player.getY(), player.getZ() )
}

async function onTeleportToLocation( commandChunks: Array<string>, player: L2PcInstance ): Promise<void> {
    let [ , direction, rangeValue ] = commandChunks

    if ( _.isEmpty( direction ) ) {
        return
    }

    let range: number = rangeValue ? _.parseInt( rangeValue ) : 150
    let x = player.getX()
    let y = player.getY()
    let z = player.getZ()

    switch ( _.toLower( direction ) ) {
        case 'east':
            x += range
            break

        case 'west':
            x -= range
            break

        case 'south':
            y -= range
            break

        case 'north':
            y += range
            break

        case 'up':
            z += range
            break

        case 'down':
            z -= range
            break
    }

    await player.teleportToLocationCoordinates( x, y, z, player.getHeading(), player.getInstanceId() )
    AdminHtml.showAdminHtml( player, 'move.htm' )
}

async function onSendPlayerHome( commandChunks: Array<string>, player: L2PcInstance ): Promise<void> {
    let [ , playerName ] = commandChunks

    if ( _.isEmpty( playerName ) ) {
        let target = player.getTarget()
        if ( !target || !target.isPlayer() ) {
            return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
        }

        return teleportTargetToHome( player, target as L2PcInstance )
    }

    let target = L2World.getPlayerByName( playerName )
    if ( !target ) {
        return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_IS_NOT_FOUND_IN_THE_GAME ) )
    }

    return teleportTargetToHome( player, target )
}

const raceToMapRegionName : Record<number, string> = {
    [ Race.ELF ]: 'elf_town',
    [ Race.DARK_ELF ]: 'darkelf_town',
    [ Race.ORC ]: 'orc_town',
    [ Race.DWARF ]: 'dwarf_town',
    [ Race.KAMAEL ]: 'kamael_town',
    [ Race.HUMAN ]: 'talking_island_town'
}

// TODO : add teleport to nearest town
async function teleportTargetToHome( player: L2PcInstance, target: L2PcInstance ): Promise<void> {
    let regionName = raceToMapRegionName[ player.getRace() ]
    let respawnData = DataManager.getRespawnPoints().getByName( regionName )
    if ( !respawnData ) {
        return player.sendMessage( `Cannot find respawn points by name '${ regionName }'` )
    }

    let point = ( target as L2PcInstance ).getPvpFlag() === PlayerPvpFlag.Flagged ? _.sample( respawnData.pvpPoints ) : _.sample( respawnData.normalPoints )
    await target.teleportToLocation( point, true )
    target.setInstanceId( 0 )
    target.setIsIn7sDungeon( false )
}

async function onTeleportToObject( commandChunks: Array<string>, player: L2PcInstance ): Promise<void> {
    let [ , objectIdValue, offsetValue ] = commandChunks
    let location: L2Object = L2World.getObjectById( parseInt( objectIdValue ) )
    if ( !location ) {
        player.sendMessage( 'Non-existing object location for teleport!' )
        return
    }

    let offset: number = offsetValue ? parseInt( offsetValue ) : 30
    await player.teleportToLocationWithOffset( location, offset )
    player.setTarget( null )
}