import { IAdminCommand } from '../IAdminCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { formatDescription, formatOptionsDescription } from './helpers/FormatHelpers'
import { NpcHtmlMessage } from '../../packets/send/NpcHtmlMessage'
import { DataManager } from '../../../data/manager'
import { DrawHelper } from './helpers/DrawHelper'
import { L2NpcRouteItem, L2NpcRoutePoint } from '../../../data/interface/NpcRoutesDataApi'
import { GeoPolygonCache } from '../../cache/GeoPolygonCache'
import { createPagination } from './helpers/SearchHelper'
import _ from 'lodash'
import { NpcTemplateType } from '../../enums/NpcTemplateType'
import { createButton } from './helpers/AdminHtml'
import { createViewNpcByIdBypass } from './SearchNpc'

const enum NpcRoutesCommand {
    ShowRoutes = 'routes-menu',
    ViewRoute = 'route-view',
    DrawRoute = 'route-draw',
    ClearDrawnRoutes = 'routes-clear',
    TeleportToRoute = 'route-teleport'
}

const pageLimit: number = 40
const lineColor: number = 0x60FF54

export const NpcRoutes: IAdminCommand = {
    getCommandNames(): Array<string> {
        return [
            NpcRoutesCommand.DrawRoute,
            NpcRoutesCommand.ShowRoutes,
            NpcRoutesCommand.ViewRoute,
            NpcRoutesCommand.ClearDrawnRoutes,
            NpcRoutesCommand.TeleportToRoute
        ]
    },

    getDescription( command: string ): Array<string> {
        switch ( command ) {
            case NpcRoutesCommand.ShowRoutes:
                return [
                    formatOptionsDescription( NpcRoutesCommand.ShowRoutes, '[page=1]', 'Shows static npc routes available on server' ),
                    'Page value is optional and used in case of more results are available'
                ]

            case NpcRoutesCommand.DrawRoute:
                return [
                    formatOptionsDescription( NpcRoutesCommand.DrawRoute, '<id>', 'Draws route specified by id' )
                ]

            case NpcRoutesCommand.ViewRoute:
                return [
                    formatOptionsDescription( NpcRoutesCommand.ViewRoute, '<id>', 'Shows route properties and further actions' )
                ]

            case NpcRoutesCommand.ClearDrawnRoutes:
                return [
                    formatDescription( NpcRoutesCommand.ClearDrawnRoutes, 'Clears any drawn routes' )
                ]

            case NpcRoutesCommand.TeleportToRoute:
                return [
                    formatOptionsDescription( NpcRoutesCommand.TeleportToRoute, '<id>', 'Teleports player to starting point of specified route by id' )
                ]
        }
    },

    async onCommand( command: string, player: L2PcInstance ): Promise<void> {
        let commandChunks = command.split( ' ' )
        switch ( commandChunks[ 0 ] ) {
            case NpcRoutesCommand.ShowRoutes:
                return onShowRoutes( commandChunks, player )

            case NpcRoutesCommand.DrawRoute:
                return onDrawRoute( commandChunks, player )

            case NpcRoutesCommand.ViewRoute:
                return onViewRouteProperties( commandChunks, player )

            // TODO : not ideal since clearing can also affect any other drawing
            case NpcRoutesCommand.ClearDrawnRoutes:
                return DrawHelper.clearPackets( player.getObjectId() )

            case NpcRoutesCommand.TeleportToRoute:
                return onTeleportToRoute( commandChunks, player )
        }
    }
}

export function createRouteViewBypass( name: string ) : string {
    return `bypass -h admin_${ NpcRoutesCommand.ViewRoute } ${name}`
}

function getRouteName( commandChunks: Array<string>, player: L2PcInstance ): L2NpcRouteItem {
    let [ currentCommand, routeId ] = commandChunks

    let route = DataManager.getNpcRoutes().getRouteByName( routeId )
    if ( !route ) {
        player.sendMessage( `Unable to find npc route with name=${ routeId }` )
        return
    }

    return route
}

function onShowRoutes( commandChunks: Array<string>, player: L2PcInstance ): void {
    let [ currentCommand, pageValue ] = commandChunks
    let currentPage = parseInt( pageValue, 10 )
    let paginationIndex = currentPage ? currentPage - 1 : 0

    let allRoutes = DataManager.getNpcRoutes().getAllRoutes()
    let paginatedRoutes = _.chunk( allRoutes, pageLimit )
    let currentPageRoutes = paginatedRoutes[ paginationIndex ]
    if ( !currentPageRoutes ) {
        return player.sendMessage( `Unable to show routes for pagination index=${ paginationIndex }` )
    }

    let itemHtml = DataManager.getHtmlData().getItem( 'overrides/html/command/npcRoutes/showRoutes-item.htm' )
    let contentsHtml = currentPageRoutes.map( ( route: L2NpcRouteItem ): string => {
        let point = route.points[ 0 ]
        let isClose: boolean = player.calculateDistanceWithCoordinates( point.x, point.y, point.z, false ) < 1000

        return itemHtml
            .replace( '%name%', `<font color="${ isClose ? 'LEVEL' : '999999' }">${ route.name }</font>` )
            .replace( '%view%', `admin_${ NpcRoutesCommand.ViewRoute } ${ route.name }` )
            .replace( '%action%', `admin_${ isClose ? NpcRoutesCommand.DrawRoute : NpcRoutesCommand.TeleportToRoute } ${ route.name }` )
            .replace( '%actionName%', isClose ? 'Draw' : 'MoveTo' )
    } ).join( '' )

    let html = DataManager.getHtmlData().getItem( 'overrides/html/command/npcRoutes/showRoutes.htm' )
        .replace( '%items%', contentsHtml )
        .replace( '%pagination%', createPagination( paginatedRoutes.length, `bypass -h admin_${ NpcRoutesCommand.ShowRoutes }` ) )

    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId() ) )
}

function onDrawRoute( commandChunks: Array<string>, player: L2PcInstance ): void {
    let route = getRouteName( commandChunks, player )
    if ( !route ) {
        return
    }

    let packet = DrawHelper.getDrawPacket( player, route.name )

    route.points.forEach( ( point: L2NpcRoutePoint, index: number ) => {
        if ( index > 0 ) {
            let previousPoint = route.points[ index - 1 ]
            packet.addLine( '', lineColor, false, previousPoint.x, previousPoint.y, previousPoint.z, point.x, point.y, point.z )
        }
    } )

    player.sendOwnedData( packet.getBuffer() )
}

function getNpcSection( npcIds: Array<number> ): string {
    let itemHtml = DataManager.getHtmlData().getItem( 'overrides/html/command/npcRoutes/viewRoute-npcs-item.htm' )
    let items = npcIds.map( ( npcId: number ): string => {
        let template = DataManager.getNpcData().getTemplate( npcId )
        return itemHtml
            .replace( '%npcId%', npcId.toString() )
            .replace( '%type%', template ? NpcTemplateType[ template.type ] : 'none' )
            .replace( '%name%', template ? template.name : 'none' )
            .replace( '%action%', createButton( 'View', createViewNpcByIdBypass( npcId ) ) )
    } ).join( '' )

    return DataManager.getHtmlData().getItem( 'overrides/html/command/npcRoutes/viewRoute-npcs.htm' )
        .replace( '%items%', items )
}

function getPointsSection( route: L2NpcRouteItem ): string {
    let itemHtml = DataManager.getHtmlData().getItem( 'overrides/html/command/npcRoutes/viewRoute-points-item.htm' )
    let items = route.points.map( ( point: L2NpcRoutePoint, index: number ): string => {
        let geoZ = GeoPolygonCache.getZ( point.x, point.y, point.z )

        return itemHtml
            .replace( '%index%', index.toString() )
            .replace( '%x%', point.x.toString() )
            .replace( '%y%', point.y.toString() )
            .replace( '%z%', point.z.toString() )
            .replace( '%geoZ%', point.z !== geoZ ? `<font color="LEVEL">${ geoZ }</font>` : geoZ.toString() )
    } ).join( '' )

    return DataManager.getHtmlData().getItem( 'overrides/html/command/npcRoutes/viewRoute-points.htm' )
        .replace( '%items%', items )
}

function getViewButtons( route: L2NpcRouteItem, player: L2PcInstance ): string {
    let isClose: boolean = route.points.some( ( point: L2NpcRoutePoint ) : boolean => {
        return player.calculateDistanceWithCoordinates( point.x, point.y, point.z, false ) < 1000
    } )

    return `<td><button value="Back To Search" action="bypass -h admin_${ NpcRoutesCommand.ShowRoutes }" width=100 height=21 back="L2UI_CT1.Button_DF_Down" fore="L2UI_CT1.Button_DF"></td>
            <td><button value="${ isClose ? 'Draw' : 'Teleport'}" action="bypass ${ isClose ? '' : '-h '}admin_${ isClose ? NpcRoutesCommand.DrawRoute : NpcRoutesCommand.TeleportToRoute } ${ route.name }" width=70 height=21 back="L2UI_CT1.Button_DF_Down" fore="L2UI_CT1.Button_DF"></td>
            <td><button value="Clear Drawing" action="bypass admin_${ NpcRoutesCommand.ClearDrawnRoutes }" width=100 height=21 back="L2UI_CT1.Button_DF_Down" fore="L2UI_CT1.Button_DF"></td>`
}

function onViewRouteProperties( commandChunks: Array<string>, player: L2PcInstance ): void {
    let route = getRouteName( commandChunks, player )
    if ( !route ) {
        return
    }

    let npcIds = DataManager.getSpawnNpcData().getNpcIdsByRoute( route.name )
    let html = DataManager.getHtmlData().getItem( 'overrides/html/command/npcRoutes/viewRoute.htm' )
        .replaceAll( '%routeId%', route.name )
        .replace( '%type%', route.type )
        .replace( '%nodeAmount%', route.points.length.toString() )
        .replace( '%npcAmount%', npcIds.length.toString() )
        .replace( '%npcInfo%', npcIds.length === 0 ? '' : getNpcSection( npcIds ) )
        .replace( '%nodeInfo%', getPointsSection( route ) )
        .replace( '%buttons%', getViewButtons( route, player ) )

    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId() ) )
}

function onTeleportToRoute( commandChunks: Array<string>, player: L2PcInstance ): Promise<void> {
    let route = getRouteName( commandChunks, player )
    if ( !route ) {
        return
    }

    let point = route.points[ 0 ]
    return player.teleportToLocationCoordinates( point.x, point.y, point.z )
}