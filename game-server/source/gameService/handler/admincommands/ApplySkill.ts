import { IAdminCommand } from '../IAdminCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { formatOptionsDescription } from './helpers/FormatHelpers'
import _ from 'lodash'
import { SkillCache } from '../../cache/SkillCache'
import { L2World } from '../../L2World'
import { L2Character } from '../../models/actor/L2Character'
import { EffectScope } from '../../models/skills/EffectScope'

const enum ApplySkillCommands {
    TargetOfPlayer = 'skill-apply-targetof',
    Target = 'skill-apply'
}

export const ApplySkill : IAdminCommand = {
    getCommandNames(): Array<string> {
        return [
            ApplySkillCommands.Target,
            ApplySkillCommands.TargetOfPlayer
        ]
    },

    getDescription( command: string ): Array<string> {
        switch ( command ) {
            case ApplySkillCommands.Target:
                return [
                        formatOptionsDescription( ApplySkillCommands.Target, '<objectId> <skillId> [ skillLevel = 1 ]', 'Apply skill effects to character by objectId, skill id and optional skill level.' )
                ]

            case ApplySkillCommands.TargetOfPlayer:
                return [
                        formatOptionsDescription( ApplySkillCommands.TargetOfPlayer, '<playerId> <skillId> [ skillLevel = 1 ]', 'Apply skill effects to player target, using skill id and optional skill level.' )
                ]
        }
    },

    onCommand( command: string, player: L2PcInstance ): Promise<void> {
        let commandChunks = command.split( ' ' )

        switch ( commandChunks[ 0 ] ) {
            case ApplySkillCommands.Target:
                return applyToTarget( commandChunks, player )

            case ApplySkillCommands.TargetOfPlayer:
                return applyToPlayerTarget( commandChunks, player )
        }
    }
}

async function applySkill( player: L2PcInstance, object: L2Character, skillIdValue: string, skillLevelValue: string ) : Promise<void> {
    let skillId = _.parseInt( skillIdValue )
    if ( !skillId ) {
        return player.sendMessage( 'Bad value for skillId' )
    }

    let skillLevel = Math.min( _.defaultTo( _.parseInt( skillLevelValue ), 1 ), SkillCache.getMaxLevel( skillId ) )
    let skill = SkillCache.getSkill( skillId, skillLevel )
    if ( !skill ) {
        return player.sendMessage( `No existing skill with skillId=${skillId} and skillLevel=${skillLevel}` )
    }

    if ( skill.isPassive() || skill.isToggle() ) {
        return player.sendMessage( 'Unable to apply passive or toggle skill to target' )
    }

    player.sendMessage( `Skill id=${skillId}, level=${skillLevel} effects applied to ${object.getName()}` )

    return skill.applyEffects( object, object, skill.hasEffects( EffectScope.SELF ), false )
}

async function applyToTarget( commandChunks: Array<string>, player: L2PcInstance ) : Promise<void> {
    let [ , objectIdValue, skillIdValue, skillLevelValue ] = commandChunks

    if ( !objectIdValue || !skillIdValue ) {
        return player.sendMessage( 'Unable to apply skill with missing objectId of target and skill id' )
    }

    let objectId = _.parseInt( objectIdValue )
    if ( !objectId ) {
        return player.sendMessage( 'Bad value for target objectId' )
    }

    let object = L2World.getObjectById( objectId ) as L2Character
    if ( !object || !object.isCharacter() ) {
        return player.sendMessage( `Unable to find character with objectId=${objectId}` )
    }

    return applySkill( player, object, skillIdValue, skillLevelValue )
}

async function applyToPlayerTarget( commandChunks: Array<string>, player: L2PcInstance ) : Promise<void> {
    let [ , playerIdValue, skillIdValue, skillLevelValue ] = commandChunks

    if ( !playerIdValue || !skillIdValue ) {
        return player.sendMessage( 'Unable to apply skill with missing player id or skill id' )
    }

    let objectId = _.parseInt( playerIdValue )
    if ( !objectId ) {
        return player.sendMessage( 'Bad value for player objectId' )
    }

    let otherPlayer = L2World.getPlayer( objectId )
    if ( !otherPlayer ) {
        return player.sendMessage( `Unable to find player with objectId=${objectId}` )
    }

    let target = otherPlayer.getTarget() as L2Character
    if ( !target || !target.isCharacter() ) {
        return player.sendMessage( 'Target of player is invalid.' )
    }

    return applySkill( player, target, skillIdValue, skillLevelValue )
}