import { IAdminCommand } from '../IAdminCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { EventType, GMCreateDroppedItemEvent, GMCreateItemEvent, GMGiftItemsEvent } from '../../models/events/EventType'
import { ListenerCache } from '../../cache/ListenerCache'
import { L2Item } from '../../models/items/L2Item'
import { ItemManagerCache } from '../../cache/ItemManagerCache'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { L2World } from '../../L2World'
import _ from 'lodash'
import aigle from 'aigle'
import { formatDescription, formatOptionsDescription } from './helpers/FormatHelpers'
import { AdminHtml } from './helpers/AdminHtml'
import { L2Playable } from '../../models/actor/L2Playable'
import { PointGeometry } from '../../models/drops/PointGeometry'
import { GeometryId } from '../../enums/GeometryId'
import { L2Npc } from '../../models/actor/L2Npc'

const coinItemIdMap = {
    adena: 57,
    ancientadena: 5575,
    festivaladena: 6673,
    blueeva: 4355,
    goldeinhasad: 4356,
    silvershilen: 4357,
    bloodypaagrio: 4358,
    fantasyislecoin: 13067,
}

const enum CreateItemCommands {
    CreatePage = 'create-itempage',
    CreateItem = 'create-item',
    CreateItemLocation = 'create-item-location',
    CreateCoin = 'create-coin',
    GiveToTarget = 'create-givetarget',
    GiveInRadius = 'create-giveradius',
    GiveToTargetId = 'create-givetargetid',
    CreateDroppedItem = 'create-dropped-item'
}

export const CreateItem: IAdminCommand = {
    getCommandNames(): Array<string> {
        return [
            CreateItemCommands.CreatePage,
            CreateItemCommands.CreateCoin,
            CreateItemCommands.CreateItem,
            CreateItemCommands.GiveToTarget,
            CreateItemCommands.GiveInRadius,
            CreateItemCommands.GiveToTargetId,
            CreateItemCommands.CreateItemLocation,
            CreateItemCommands.CreateDroppedItem,
        ]
    },

    getDescription( command: string ): Array<string> {
        switch ( command ) {
            case CreateItemCommands.CreatePage:
                return [
                    formatDescription( CreateItemCommands.CreatePage, 'Shows admin page with item creation options.' ),
                ]

            case CreateItemCommands.CreateItem:
                return [
                    formatOptionsDescription( CreateItemCommands.CreateItem, '< itemId > [amount = 1]', 'Creates item in current player inventory' ),
                ]

            case CreateItemCommands.CreateCoin:
                return [
                    formatOptionsDescription( CreateItemCommands.CreateCoin, '< coinName > [amount = 1]', 'Adds coin item to current player inventory' ),
                    `Acceptable coin names: ${ _.keys( coinItemIdMap ).join( ', ' ) }`,
                ]

            case CreateItemCommands.GiveToTarget:
                return [
                    formatOptionsDescription( CreateItemCommands.GiveToTarget, '< itemId > [amount = 1]', 'Creates item in target player inventory' ),
                ]

            case CreateItemCommands.GiveInRadius:
                return [
                    formatOptionsDescription( CreateItemCommands.GiveInRadius, '< itemId > <amount> [radius = 500]', 'Adds item to each player\'s inventory within specified radius' ),
                ]

            case CreateItemCommands.GiveToTargetId:
                return [
                    formatOptionsDescription( CreateItemCommands.GiveToTargetId, '< objectId > < itemId > [amount = 1]', 'Adds item in target character inventory (player or pet), target is specified by objectId.' ),
                ]

            case CreateItemCommands.CreateItemLocation:
                return [
                    formatOptionsDescription( CreateItemCommands.CreateItemLocation, '<itemId> <x> <y> <z> [amount = 1]', 'Creates item at X/Y/Z coordinates' ),
                    'If item is not stackable, only single item will be created at X/Y/Z position and player current instance, governed by available geodata.',
                    'Amount is capped at 10000'
                ]

            case CreateItemCommands.CreateDroppedItem:
                return [
                    formatOptionsDescription( CreateItemCommands.CreateDroppedItem, '<itemId> [ amount = 1]', 'Creates item from targeted character as if it is dropped by character.' )
                ]
        }
    },

    async onCommand( command: string, player: L2PcInstance ): Promise<void> {
        let commandChunks: Array<string> = _.split( command, ' ' )
        let currentCommand = _.head( commandChunks )

        switch ( currentCommand ) {
            case CreateItemCommands.CreatePage:
                return showItemCreationPage( player )

            case CreateItemCommands.CreateItem:
                return onCreateItem( commandChunks, player )

            case CreateItemCommands.CreateCoin:
                return onCreateCoin( commandChunks, player )

            case CreateItemCommands.GiveToTarget:
                return onGiveItemToTarget( commandChunks, player )

            case CreateItemCommands.GiveInRadius:
                return onGiveItemInRadius( commandChunks, player )

            case CreateItemCommands.GiveToTargetId:
                return onGiveItemToTargetId( commandChunks, player )

            case CreateItemCommands.CreateItemLocation:
                return onCreateItemAtLocation( commandChunks, player )

            case CreateItemCommands.CreateDroppedItem:
                return onCreateDroppedItem( commandChunks, player )
        }
    },
}

async function onCreateItem( commandChunks: Array<string>, player: L2PcInstance ): Promise<void> {
    let [ , itemIdValue, amountValue ] = commandChunks

    if ( !itemIdValue ) {
        return showItemCreationPage( player )
    }

    let itemId: number = _.parseInt( itemIdValue )
    if ( !itemId ) {
        return
    }

    let amount: number = amountValue ? _.parseInt( amountValue ) : 1
    if ( amount === 0 ) {
        return player.sendMessage( 'Amount cannot be zero' )
    }

    return createItem( player, player, itemId, amount )
}

async function onCreateCoin( commandChunks: Array<string>, player: L2PcInstance ): Promise<void> {
    let [ currentCommand, itemName, amountValue ] = commandChunks

    if ( !itemName ) {
        return
    }

    let itemId: number = coinItemIdMap[ itemName.toLowerCase() ]
    if ( !itemId ) {
        return
    }

    let amount: number = amountValue ? _.parseInt( amountValue ) : 1
    if ( !amount ) {
        return player.sendMessage( 'Amount cannot be zero' )
    }

    await createItem( player, player, itemId, amount )
    showItemCreationPage( player )
}

async function createItem( player: L2PcInstance, target: L2Playable, itemId: number, amount: number ): Promise<void> {
    let template: L2Item = ItemManagerCache.getTemplate( itemId )
    if ( !template ) {
        return player.sendMessage( `Invalid template id '${ itemId }'` )
    }

    if ( amount > 10 && !template.isStackable() ) {
        return player.sendMessage( 'Cannot create so many items that do not stack' )
    }

    await target.getInventory().addItem( itemId, amount, null, 'AdminCreateItem' )

    if ( player.getObjectId() !== target.getObjectId() ) {
        target.sendMessage( `Received ${ amount } of '${ template.getName() }' into inventory` )
    }

    player.sendMessage( `Added ${ amount } of '${ template.getName() }' to '${ target.getName() }' inventory` )

    if ( ListenerCache.hasGeneralListener( EventType.GMCreateItem ) ) {
        let data: GMCreateItemEvent = {
            amount,
            itemId,
            originatorId: player.getObjectId(),
            receiverId: player.getObjectId(),
        }

        return ListenerCache.sendGeneralEvent( EventType.GMCreateItem, data )
    }
}

async function onGiveItemToTarget( commandChunks: Array<string>, player: L2PcInstance ): Promise<void> {
    let [ currentCommand, itemIdValue, amountValue ] = commandChunks

    if ( !itemIdValue ) {
        return
    }

    let itemId: number = _.parseInt( itemIdValue )
    if ( !itemId ) {
        return
    }

    let amount: number = amountValue ? _.parseInt( amountValue ) : 1
    if ( amount === 0 ) {
        return player.sendMessage( 'Amount cannot be zero' )
    }

    let target = player.getTarget()
    if ( !target || !target.isPlayer() ) {
        return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_IS_INCORRECT ) )
    }

    await createItem( player, target as L2PcInstance, itemId, amount )

    showItemCreationPage( player )
}

async function onGiveItemInRadius( commandChunks: Array<string>, player: L2PcInstance ): Promise<void> {
    let [ currentCommand, itemIdValue, amountValue, radiusValue ] = commandChunks

    if ( !itemIdValue ) {
        return
    }

    let itemId: number = _.parseInt( itemIdValue )
    if ( itemId === 0 ) {
        return
    }

    let template: L2Item = ItemManagerCache.getTemplate( itemId )
    if ( !template ) {
        return player.sendMessage( `Invalid template id '${ itemId }'` )
    }

    let amount: number = amountValue ? _.parseInt( amountValue ) : 1
    if ( amount === 0 ) {
        return player.sendMessage( 'Amount cannot be zero' )
    }

    let radius: number = radiusValue ? _.parseInt( radiusValue ) : 500
    if ( radius === 0 ) {
        return
    }

    let objectIds: Array<number> = []
    await aigle.resolve( L2World.getVisiblePlayers( player, radius ) ).eachLimit( 10, async ( otherPlayer: L2PcInstance ) => {
        if ( !otherPlayer || !otherPlayer.isOnline() ) {
            return
        }

        await otherPlayer.getInventory().addItem( itemId, amount, 0, 'Admin GiveItemToAll' )
        otherPlayer.sendMessage( `Received ${ amount } of '${ template.getName() }' into inventory` )

        objectIds.push( otherPlayer.getObjectId() )
    } )

    player.sendMessage( `Total of ${ objectIds.length } players were gifted '${ template.getName() }'` )

    if ( ListenerCache.hasGeneralListener( EventType.GMGiftItems ) ) {
        let data: GMGiftItemsEvent = {
            amount,
            itemId,
            originatorId: player.getObjectId(),
            receiverIds: objectIds,
        }

        return ListenerCache.sendGeneralEvent( EventType.GMGiftItems, data )
    }
}

function showItemCreationPage( player: L2PcInstance ): void {
    AdminHtml.showHtml( player, 'overrides/html/command/createItem/main.htm' )
}

async function onGiveItemToTargetId( commandChunks: Array<string>, player: L2PcInstance ): Promise<void> {
    let [ currentCommand, objectIdValue, itemIdValue, amountValue ] = commandChunks

    let target = L2World.getObjectById( parseInt( objectIdValue ) )
    if ( !target || ( !target.isPet() && !target.isPlayer() ) ) {
        player.sendMessage( 'Specify target that is player or pet. Target is not valid.' )
        return
    }

    let itemId: number = _.parseInt( itemIdValue )
    if ( !itemId ) {
        return
    }

    let amount: number = amountValue ? _.parseInt( amountValue ) : 1
    if ( amount === 0 ) {
        return player.sendMessage( 'Amount cannot be zero' )
    }

    return createItem( player, target as L2Playable, itemId, amount )
}

async function onCreateItemAtLocation( commandChunks: Array<string>, player: L2PcInstance ) : Promise<void> {
    let values : Array<number> = commandChunks.slice( 1 ).map( value => parseInt( value, 10 ) )

    if ( !Number.isInteger( values[ 0 ] ) ) {
        return player.sendMessage( 'Please provide correct itemId' )
    }

    let [ itemId, x, y, z, amountValue ] = values
    let displayLocationMessage : boolean = false

    if ( !Number.isInteger( x ) ) {
        x = player.getX()
        displayLocationMessage = true
    }

    if ( !Number.isInteger( y ) ) {
        y = player.getY()
        displayLocationMessage = true
    }

    if ( !Number.isInteger( z ) ) {
        z = player.getZ()
        displayLocationMessage = true
    }

    let template: L2Item = ItemManagerCache.getTemplate( itemId )
    if ( !template ) {
        return player.sendMessage( `Invalid template id '${ itemId }'` )
    }

    let amount = Number.isNaN( amountValue ) ? 1 : Math.min( amountValue, 10000 )
    if ( amount === 0 ) {
        return player.sendMessage( 'Amount cannot be zero' )
    }

    if ( !template.isStackable() ) {
        amount = 1
    }

    let item = ItemManagerCache.createItem( template.getId(), CreateItemCommands.CreateItemLocation, amount, player.getObjectId() )
    item.dropMe( 0, x, y, z, player.getInstanceId(), true )

    if ( displayLocationMessage ) {
        player.sendMessage( `Created item at ${x} ${y} ${z}` )
    }

    if ( ListenerCache.hasGeneralListener( EventType.GMCreateDroppedItem ) ) {
        let data: GMCreateDroppedItemEvent = {
            x,
            y,
            z,
            amount,
            itemId,
            originatorId: player.getObjectId(),
            instanceType: template.instanceType
        }

        return ListenerCache.sendGeneralEvent( EventType.GMCreateDroppedItem, data )
    }
}

async function onCreateDroppedItem( commandChunks: Array<string>, player: L2PcInstance ) : Promise<void> {
    let target = player.getTarget()
    if ( !target || !target.isCharacter() ) {
        return player.sendMessage( 'Target a valid character' )
    }

    let [ currentCommand, itemIdValue, amountValue ] = commandChunks

    if ( !itemIdValue ) {
        return
    }

    let itemId: number = _.parseInt( itemIdValue )
    if ( itemId === 0 ) {
        return
    }

    let template: L2Item = ItemManagerCache.getTemplate( itemId )
    if ( !template ) {
        return player.sendMessage( `Invalid template id '${ itemId }'` )
    }

    let amount: number = amountValue ? _.parseInt( amountValue ) : 1
    if ( amount === 0 ) {
        return player.sendMessage( 'Amount cannot be zero' )
    }

    let geometryType = target.isNpc() ? ( target as L2Npc ).getGeometryId() : GeometryId.PlayerDrop
    let item = ItemManagerCache.createItem( template.getId(), CreateItemCommands.CreateItemLocation, amount, player.getObjectId() )
    let geometry = PointGeometry.acquire( geometryType )
    geometry.prepareNextPoint()

    let x = target.getX() + geometry.getX()
    let y = target.getY() + geometry.getY()
    let z = target.getZ() + 20

    item.dropMe( target.getObjectId(), x, y, z, target.getInstanceId(), true )

    geometry.release()

    if ( ListenerCache.hasGeneralListener( EventType.GMCreateDroppedItem ) ) {
        let data: GMCreateDroppedItemEvent = {
            x,
            y,
            z,
            amount,
            itemId,
            originatorId: player.getObjectId(),
            instanceType: template.instanceType
        }

        return ListenerCache.sendGeneralEvent( EventType.GMCreateDroppedItem, data )
    }
}