import { IAdminCommand } from '../IAdminCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { ConfigManager } from '../../../config/ConfigManager'
import { GeneralHelper } from '../../helpers/GeneralHelper'
import { DataManager } from '../../../data/manager'
import { AnnouncementType } from '../../models/announce/AnnouncementType'
import { AutoAnnouncement } from '../../models/announce/AutoAnnouncement'
import { IAnnouncement } from '../../models/announce/IAnnouncement'
import { AnnouncementManager } from '../../models/announce/AnnouncementManager'
import { GeneralAnnouncement } from '../../models/announce/GeneralAnnouncement'
import { ExShowScreenMessageFromText } from '../../packets/send/ExShowScreenMessage'
import _ from 'lodash'
import { AdminHtml } from './helpers/AdminHtml'

const paginationLinkTemplate = `
<td align=center>
<button action="bypass admin_announces list %pageIndex%" value="%pageNumber%" width=35 height=20 back="L2UI_CT1.Button_DF_Down" fore="L2UI_CT1.Button_DF">
</td>`

export const Announcements: IAdminCommand = {
    getCommandNames(): Array<string> {
        return [
            'announce',
            'announce_crit',
            'announce_screen',
            'announces',
        ]
    },

    getDescription( command: string ): Array<string> {
        return [
            'implement me please!',
        ]
    },

    async onCommand( command: string, player: L2PcInstance ): Promise<void> {
        let commandChunks: Array<string> = _.split( command, ' ' )
        let currentCommand: string = _.head( commandChunks )
        switch ( currentCommand ) {
            case 'announce':
            case 'announce_crit':
            case 'announce_screen':
                if ( commandChunks.length <= 1 ) {
                    player.sendMessage( 'Syntax: //announce <text to announce here>' )
                    return
                }

                let lines: Array<string> = _.tail( commandChunks )
                if ( currentCommand === 'announce_screen' ) {
                    await BroadcastHelper.toAllOnlinePlayersData( ExShowScreenMessageFromText( lines.join( ' ' ), 10000 ) )
                } else {
                    let name = ConfigManager.general.gmShowAnnouncerName() ? player.getName() : 'GM'
                    BroadcastHelper.broadcastTextToAllPlayers( name, lines.join( ' ' ), currentCommand === 'announce_crit' )
                }

                AdminHtml.showAdminHtml( player, 'gm_menu.htm' )
                return

            case 'announces':
                let action: string = _.nth( commandChunks, 1 )
                switch ( action ) {
                    case 'add':
                        await onAnnouncementAdd( commandChunks, player )
                        return

                    case 'edit':
                        await onAnnouncementEdit( commandChunks, player )
                        return

                    case 'remove':
                        await onAnnouncementRemove( commandChunks, player )
                        return

                    case 'restart':
                        await onAnnouncementRestart( commandChunks, player )
                        return

                    case 'show':
                        await onAnnouncementShow( commandChunks, player )
                        return

                    case 'list':
                        await onAnnouncementList( commandChunks, player )
                        return
                }

                return
        }
    },
}

async function onAnnouncementAdd( commandChunks: Array<string>, player: L2PcInstance ): Promise<void> {
    let [ unused, unusedSubcommand, announcementType, initialDelayValue, announceDelayValue, repeatValue, content ] = commandChunks
    if ( !announcementType || !content ) {
        GeneralHelper.sendCommunityBoardHtml( player, DataManager.getHtmlData().getItem( 'data/html/admin/announces-add.htm' ) )
        return
    }

    let typeValue: string = _.toUpper( announcementType )
    let type: AnnouncementType = AnnouncementType[ typeValue ]
    if ( !type ) {
        return
    }

    let isTypeAuto = [ AnnouncementType.AUTO_NORMAL, AnnouncementType.AUTO_CRITICAL ].includes( type )
    let initialDelay = initialDelayValue ? _.parseInt( initialDelayValue ) * 1000 : 0
    let announceDelay = announceDelayValue ? _.parseInt( announceDelayValue ) * 1000 : 0
    if ( announceDelay < ( 10 * 1000 ) && isTypeAuto ) {
        player.sendMessage( 'Delay cannot be less then 10 seconds!' )
        return
    }

    let repeat = repeatValue ? _.parseInt( repeatValue ) : 0
    if ( repeat === 0 ) {
        repeat = -1
    }

    if ( commandChunks.length > 7 ) {
        content = _.drop( commandChunks, 7 ).join( ' ' )
    }

    let message: GeneralAnnouncement
    if ( isTypeAuto ) {
        message = new AutoAnnouncement( type, content, player.getName(), initialDelay, announceDelay, repeat )
    } else {
        message = new GeneralAnnouncement( type, content, player.getName() )
    }

    await AnnouncementManager.addAnnouncement( message )
    player.sendMessage( `Announcement with type [ ${ typeValue } ] has been successfully added!` )

    return Announcements.onCommand( 'announces list', player )
}

async function onAnnouncementEdit( commandChunks: Array<string>, player: L2PcInstance ) {
    let [ unused, unusedSubcommand, idValue, announcementType, initialDelayValue, announceDelayValue, repeatValue, content ] = commandChunks
    if ( !idValue ) {
        return
    }

    let announce: IAnnouncement = AnnouncementManager.getAnnouncement( parseInt( idValue ) )
    if ( !announce ) {
        player.sendMessage( `Announcement with id [ ${ idValue } ] does not exist!` )
        return
    }

    if ( !announcementType ) {
        let initialDelay = 0
        let delay = 0
        let repeat = 0

        if ( announce.isAuto() ) {
            initialDelay = Math.floor( ( announce as AutoAnnouncement ).initial / 1000 )
            delay = Math.floor( ( announce as AutoAnnouncement ).delay / 1000 )
            repeat = ( announce as AutoAnnouncement ).repeat
        }

        let html: string = DataManager.getHtmlData().getItem( 'data/html/admin/announces-edit.htm' )
                                      .replace( /%id%/g, announce.getId().toString() )
                                      .replace( /%type%/g, AnnouncementType[ announce.getType() ] )
                                      .replace( /%initial%/g, initialDelay.toString() )
                                      .replace( /%delay%/g, delay.toString() )
                                      .replace( /%repeat%/g, repeat.toString() )
                                      .replace( /%author%/g, announce.getAuthor() )
                                      .replace( /%content%/g, announce.getContent() )

        return GeneralHelper.sendCommunityBoardHtml( player, html )
    }

    let typeValue: string = _.toUpper( announcementType )
    let type: AnnouncementType = AnnouncementType[ typeValue ]
    if ( !type ) {
        return
    }

    let autoTypes = [ AnnouncementType.AUTO_CRITICAL, AnnouncementType.AUTO_NORMAL ]

    if ( autoTypes.includes( announce.getType() ) && !autoTypes.includes( type ) ) {
        player.sendMessage( 'Announcement types must be: [ AUTO_CRITICAL, AUTO_NORMAL ]' )
        return
    }

    let normalTypes = [ AnnouncementType.NORMAL, AnnouncementType.CRITICAL ]
    if ( normalTypes.includes( announce.getType() ) && !normalTypes.includes( type ) ) {
        player.sendMessage( 'Announcement types must be: [ NORMAL, CRITICAL ]' )
        return
    }

    let initialDelay: number = initialDelayValue ? _.parseInt( initialDelayValue ) * 1000 : 0
    let announceDelay: number = announceDelayValue ? _.parseInt( announceDelayValue ) * 1000 : 0
    if ( announceDelay < ( 10 * 1000 ) && autoTypes.includes( type ) ) {
        player.sendMessage( 'Delay cannot be less then 10 seconds!' )
        return
    }

    let repeat = repeatValue ? _.parseInt( repeatValue ) : 0
    if ( repeat === 0 ) {
        repeat = -1
    }

    if ( commandChunks.length > 7 ) {
        content = _.drop( commandChunks, 7 ).join( ' ' )
    }

    if ( _.isEmpty( content ) ) {
        content = announce.getContent()
    }

    announce.setType( type )
    announce.setContent( content )
    announce.setAuthor( player.getName() )

    if ( announce.isAuto() ) {
        let autoAnnouncement = announce as AutoAnnouncement

        autoAnnouncement.initial = initialDelay * 1000
        autoAnnouncement.delay = announceDelay * 1000
        autoAnnouncement.repeat = repeat
    }

    await announce.updateMe()

    player.sendMessage( 'Announcement has been successfully edited!' )
    return Announcements.onCommand( 'announces list', player )
}

async function onAnnouncementRemove( commandChunks: Array<string>, player: L2PcInstance ) {
    let [ unused, unusedSubcommand, idValue ] = commandChunks

    let result: boolean = await AnnouncementManager.deleteAnnouncement( idValue )
    if ( !result ) {
        player.sendMessage( `Announcement with id [ ${ idValue } ] does not exist!` )
        return
    }

    return Announcements.onCommand( 'announces list', player )
}

async function onAnnouncementRestart( commandChunks: Array<string>, player: L2PcInstance ) {
    let [ unused, unusedSubcommand, idValue ] = commandChunks
    if ( !idValue ) {
        _.each( AnnouncementManager.getAllAnnouncements(), ( value: IAnnouncement ) => {
            if ( value.isAuto() ) {
                ( value as AutoAnnouncement ).restartTask()
            }
        } )

        player.sendMessage( 'Auto announcements has been successfully restarted' )
        return
    }

    let announce: IAnnouncement = AnnouncementManager.getAnnouncement( parseInt( idValue ) )
    if ( !announce ) {
        player.sendMessage( `Announcement with id [ ${ idValue } ] does not exist!` )
        return
    }

    if ( !announce.isAuto() ) {
        player.sendMessage( `Announcement with id [ ${ idValue } ] is not AUTO type, thus cannot be restarted` )
        return
    }

    ( announce as AutoAnnouncement ).restartTask()

    player.sendMessage( `Announcement with id [ ${ idValue } ] has been restarted` )
}

async function onAnnouncementShow( commandChunks: Array<string>, player: L2PcInstance ) {
    let [ unused, unusedSubcommand, idValue ] = commandChunks
    let announce: IAnnouncement = AnnouncementManager.getAnnouncement( parseInt( idValue ) )
    if ( !announce ) {
        player.sendMessage( `Announcement with id [ ${ idValue } ] does not exist!` )
        return
    }

    let initialDelay = 0
    let delay = 0
    let repeat = 0

    if ( announce.isAuto() ) {
        initialDelay = Math.floor( ( announce as AutoAnnouncement ).initial / 1000 )
        delay = Math.floor( ( announce as AutoAnnouncement ).delay / 1000 )
        repeat = ( announce as AutoAnnouncement ).repeat
    }

    let html: string = DataManager.getHtmlData().getItem( 'data/html/admin/announces-show.htm' )
                                  .replace( /%id%/g, announce.getId().toString() )
                                  .replace( /%type%/g, AnnouncementType[ announce.getType() ] )
                                  .replace( /%initial%/g, initialDelay.toString() )
                                  .replace( /%delay%/g, delay.toString() )
                                  .replace( /%repeat%/g, repeat.toString() )
                                  .replace( /%author%/g, announce.getAuthor() )
                                  .replace( /%content%/g, announce.getContent() )

    return GeneralHelper.sendCommunityBoardHtml( player, html )
}

async function onAnnouncementList( commandChunks: Array<string>, player: L2PcInstance ) {
    let [ unused, unusedSubcommand, pageValue ] = commandChunks
    let page = pageValue ? _.parseInt( pageValue ) : 0

    let pages: Array<Array<IAnnouncement>> = _.chunk( AnnouncementManager.getAllAnnouncements(), 8 )
    let pageAnnouncements: Array<IAnnouncement> = _.nth( pages, page )

    let pagination: Array<string> = _.times( pages.length, ( index: number ) => {
        return paginationLinkTemplate
                .replace( '%pageIndex%', index.toString() )
                .replace( '%pageNumber%', ( index + 1 ).toString() )
    } )

    let announcements: Array<string> = _.map( pageAnnouncements, ( announcement: IAnnouncement ) => {
        let lines = [
            `<tr><td width=5></td><td width=80>${ announcement.getId() }</td><td width=100>${ AnnouncementType[ announcement.getType() ] }</td><td width=100>${ announcement.getAuthor() }</td>`,
        ]

        if ( [ AnnouncementType.AUTO_CRITICAL, AnnouncementType.AUTO_NORMAL ].includes( announcement.getType() ) ) {
            lines.push( `<td width=60><button action="bypass -h announces restart ${ announcement.getId() }" value="Restart" width="60" height="21" back="L2UI_CT1.Button_DF_Down" fore="L2UI_CT1.Button_DF"></td>` )
        } else {
            lines.push( '<td width=60><button action="" value="" width="60" height="21" back="L2UI_CT1.Button_DF_Down" fore="L2UI_CT1.Button_DF"></td>' )
        }

        lines.push( `<td width=60><button action="bypass -h announces show ${ announcement.getId() }" value="Show" width="60" height="21" back="L2UI_CT1.Button_DF_Down" fore="L2UI_CT1.Button_DF"></td>` )

        if ( announcement.getType() === AnnouncementType.EVENT ) {
            lines.push( '<td width=60></td>' )
        } else {
            lines.push( `<td width=60><button action="bypass -h announces edit ${ announcement.getId() }" value="Edit" width="60" height="21" back="L2UI_CT1.Button_DF_Down" fore="L2UI_CT1.Button_DF"></td>` )
        }

        lines.push( `<td width=60><button action="bypass -h announces remove ${ announcement.getId() }" value="Remove" width="60" height="21" back="L2UI_CT1.Button_DF_Down" fore="L2UI_CT1.Button_DF"></td>` )
        lines.push( '<td width=5></td></tr>' )

        return lines.join( '' )
    } )

    let html: string = DataManager.getHtmlData().getItem( 'data/html/admin/announces-list.htm' )
                                  .replace( /"%pages%/g, pagination.join( '' ) )
                                  .replace( /%announcements%/g, announcements.join( '' ) )

    return GeneralHelper.sendCommunityBoardHtml( player, html )
}