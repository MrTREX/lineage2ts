import { IAdminCommand } from '../IAdminCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { formatDescription, formatOptionsDescription } from './helpers/FormatHelpers'
import { AdminHtml } from './helpers/AdminHtml'
import { DataManager } from '../../../data/manager'

const enum HtmlCommands {
    RenderPath = 'html',
    Reload = 'html-reload'
}

export function createViewHtmlBypass( path: string ) : string {
    return `bypass -h admin_${HtmlCommands.RenderPath} ${path}`
}

export const Html: IAdminCommand = {
    getCommandNames(): Array<string> {
        return [
            HtmlCommands.RenderPath,
            HtmlCommands.Reload,
        ]
    },

    getDescription( command: string ): Array<string> {
        switch ( command ) {
            case HtmlCommands.RenderPath:
                return [
                    formatOptionsDescription( HtmlCommands.RenderPath, '< path >', 'renders html available at path' ),
                ]

            case HtmlCommands.Reload:
                return [
                    formatDescription( HtmlCommands.Reload, 'Reloads available html contents' )
                ]
        }
    },

    async onCommand( command: string, player: L2PcInstance ): Promise<void> {
        let commandChunks = command.split( ' ' )

        switch ( commandChunks[ 0 ] ) {
            case HtmlCommands.RenderPath:
                if ( !commandChunks[ 1 ] ) {
                    return
                }

                return AdminHtml.showAdminHtml( player, commandChunks[ 1 ] )

            case HtmlCommands.Reload:
                let reloadStart = Date.now()
                await DataManager.getHtmlData().reLoad()

                let size = DataManager.getHtmlData().getSize()
                player.sendMessage( `${HtmlCommands.Reload} : ${size.entries} html items and ${size.actions} actions have been reloaded in ${ Date.now() - reloadStart } ms` )

                return
        }
    },
}