import { IAdminCommand } from '../IAdminCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { formatDescription, formatOptionsDescription } from './helpers/FormatHelpers'
import { SpawnMakerCache } from '../../cache/SpawnMakerCache'
import { DataManager } from '../../../data/manager'
import { NpcHtmlMessage } from '../../packets/send/NpcHtmlMessage'
import _ from 'lodash'
import { createPagination } from './helpers/SearchHelper'
import { NpcTemplateType } from '../../enums/NpcTemplateType'
import { ISpawnLogic } from '../../models/spawns/ISpawnLogic'
import { L2NpcDataTemplateOrdering } from '../../../data/interface/NpcDataApi'
import { createViewSpawnBypass } from './Spawn'
import { AdminHtml, createButton } from './helpers/AdminHtml'

const enum SearchNpcCommands {
    ByName = 'search-npc-name',
    ById = 'search-npc-id',
    ByLevel = 'search-npc-level',
    Menu = 'search-npc-menu'
}

const pageLimit = 60

export const SearchNpc: IAdminCommand = {
    getCommandNames(): Array<string> {
        return [
            SearchNpcCommands.ByName,
            SearchNpcCommands.ById,
            SearchNpcCommands.ByLevel,
            SearchNpcCommands.Menu,
        ]
    },

    getDescription( command: string ): Array<string> {
        switch ( command ) {
            case SearchNpcCommands.ById:
                return [
                    formatOptionsDescription( SearchNpcCommands.ById, '<npcId>', 'Look up available spawn data on npc using an id' )
                ]

            case SearchNpcCommands.ByName:
                return [
                    formatOptionsDescription( SearchNpcCommands.ByName, '<name> [page=1]', 'Look up available spawn data on npc using full or partial name' ),
                    'Optionally provide search page non-zero integer for results that are paginated.'
                ]

            case SearchNpcCommands.ByLevel:
                return [
                    formatOptionsDescription( SearchNpcCommands.ByLevel, '<level|min-max> [page=1]', 'Look up available spawn data on npcs of specified level or level range' ),
                    'Optionally provide search page non-zero integer for results that are paginated.',
                    'Level range is supplied with numeric values separated by dash, example: 10-20'
                ]

            case SearchNpcCommands.Menu:
                return [
                    formatDescription( SearchNpcCommands.Menu, 'Show available operations for searching an npc' )
                ]
        }
    },

    async onCommand( command: string, player: L2PcInstance ): Promise<void> {
        const commandChunks = command.split( ' ' )
        switch ( commandChunks[ 0 ] ) {
            case SearchNpcCommands.ByName:
                return lookUpByName( commandChunks, player )

            case SearchNpcCommands.ById:
                return lookupById( commandChunks, player )

            case SearchNpcCommands.ByLevel:
                return lookupByLevel( commandChunks, player )

            case SearchNpcCommands.Menu:
                return AdminHtml.showHtml( player, 'overrides/html/command/searchNpc/menu.htm' )
        }
    }
}

function getLevelColor( level: number ): string {
    if ( level > 80 ) {
        return 'ff8c00'
    }

    if ( level > 70 ) {
        return 'FF9590'
    }

    if ( level > 60 ) {
        return 'FF8C95'
    }

    if ( level > 40 ) {
        return 'D2D200'
    }

    if ( level > 20 ) {
        return '1e90ff'
    }

    if ( level > 10 ) {
        return '07FF57'
    }

    return '999999'
}

export function createViewNpcByIdBypass( npcId: number ) : string {
    return `bypass -h admin_${ SearchNpcCommands.ById } ${ npcId }`
}

async function lookUpByName( commandChunks: Array<string>, player: L2PcInstance ): Promise<void> {
    if ( commandChunks.length === 1 ) {
        return player.sendMessage( 'Please specify npc name.' )
    }

    let npcName: string
    let page : number = 1

    /*
        Only two parameters would mean command and npc name, since there is
        no point in providing page without search keyword.
     */
    if ( commandChunks.length === 2 ) {
        npcName = commandChunks[ 1 ]
    } else {
        page = parseInt( commandChunks[ commandChunks.length - 1 ], 10 )

        if ( !page || !Number.isInteger( page ) ) {
            npcName = commandChunks.slice( 1 ).join( ' ' )
        } else {
            npcName = commandChunks.slice( 1, commandChunks.length - 1 ).join( ' ' )
        }
    }

    if ( npcName.length < 3 ) {
        return player.sendMessage( 'Please specify longer npc name' )
    }

    let ids = DataManager.getNpcData().getTemplateIdsByName( npcName )
    if ( ids.length === 0 ) {
        return player.sendMessage( `${ SearchNpcCommands.ByName } : No npc templates found with name=${ npcName }` )
    }

    let paginatedIds = _.chunk( ids, pageLimit )
    let paginationIndex = page ? page - 1 : 0
    let currentPageIds = paginatedIds[ paginationIndex ]
    if ( !currentPageIds ) {
        return player.sendMessage( `${ SearchNpcCommands.ByName } : Cannot display npc results for page=${ page }` )
    }

    /*
        There are three outcomes of npc being found:
        - npc spawn exists, however it has not spawned any yet
        - npc spawn exists and available npcs have been spawned
        - npc spawn does not exist, even if npc template can exist pointing to possibility of such npcs not being spawned
     */
    let items = currentPageIds.map( ( npcId: number ) => {
        let template = DataManager.getNpcData().getTemplate( npcId )
        let allSpawns = SpawnMakerCache.getAllSpawns( npcId )
        let nameColor = allSpawns ? ( allSpawns.some( spawn => spawn.hasSpawned() ) ? '07FF57' : '999999' ) : 'FF0002'

        return DataManager.getHtmlData().getItem( 'overrides/html/command/searchNpc/npcName-item.htm' )
            .replace( '%name%', template.getName() )
            .replace( '%nameColor%', nameColor )
            .replace( '%level%', template.getLevel().toString() )
            .replace( '%levelColor%', getLevelColor( template.getLevel() ) )
            .replace( '%button%', allSpawns ? createButton( 'View', createViewNpcByIdBypass( npcId ) ) : 'None' )
    } ).join( '' )

    let html = DataManager.getHtmlData().getItem( 'overrides/html/command/searchNpc/npcName.htm' )
        .replace( '%name%', npcName )
        .replace( '%pagination%', createPagination( paginatedIds.length, `bypass -h admin_${ SearchNpcCommands.ByName } ${npcName}` ) )
        .replace( '%items%', items )

    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId() ) )
}

async function lookupById( commandChunks: Array<string>, player: L2PcInstance ): Promise<void> {
    if ( commandChunks.length === 1 ) {
        return player.sendMessage( 'Please specify npcId.' )
    }

    let npcId = parseInt( commandChunks[ 1 ], 10 )
    if ( !npcId || !Number.isInteger( npcId ) ) {
        return player.sendMessage( 'Please specify integer value for npcId' )
    }

    let template = DataManager.getNpcData().getTemplate( npcId )
    if ( !template ) {
        return player.sendMessage( `${ SearchNpcCommands.ById } : No npc template available for npcId=${ npcId }` )
    }

    let spawns = SpawnMakerCache.getAllSpawns( npcId )
    if ( !spawns ) {
        return player.sendMessage( `${ SearchNpcCommands.ById } : No npc spawns available for npcId=${ npcId }` )
    }

    let isSpawned = false
    let spawnTemplate = DataManager.getHtmlData().getItem( 'overrides/html/command/searchNpc/npcId-spawn.htm' )
    let spawnItems : string = spawns.map( ( spawn: ISpawnLogic ) : string => {
        isSpawned = isSpawned || spawn.hasSpawned()

        return spawnTemplate
            .replace( '%makerId%', spawn.getSpawnData().makerId )
            .replace( '%spawned%', spawn.hasSpawned() ? spawn.getSpawnData().amount.toString() : `0 / ${spawn.getSpawnData().amount}` )
            .replace( '%buttons%', createButton( 'View', createViewSpawnBypass( spawn.getId() ) ) )
    } ).join( '' )

    let html = DataManager.getHtmlData().getItem( 'overrides/html/command/searchNpc/npcId.htm' )
        .replaceAll( '%npcId%', npcId.toString() )
        .replace( '%name%', template.getName() )
        .replace( '%type%', NpcTemplateType[ template.getType() ] )
        .replace( '%level%', template.getLevel().toString() )

        .replace( '%levelColor%', getLevelColor( template.getLevel() ) )
        .replace( '%spawnAmount%', spawns.length.toString() )
        .replace( '%isSpawned%', isSpawned ? 'Yes' : 'No' )
        .replace( '%spawns%', spawnItems )

    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId() ) )
}

async function lookupByLevel( commandChunks: Array<string>, player: L2PcInstance ): Promise<void> {
    if ( commandChunks.length === 1 ) {
        return player.sendMessage( 'Please specify npc level' )
    }

    let levelValues : Array<number> = commandChunks[ 1 ].split( '-' ).map( value => parseInt( value, 10 ) )
    let hasInvalidValues = levelValues.some( value => !value || value <= 0 || !Number.isInteger( value ) || value > 100 )
    if ( hasInvalidValues ) {
        return player.sendMessage( 'Please specify integer value or level range for npc search' )
    }

    let levelMin = levelValues.length > 1 ? Math.min( levelValues[ 0 ], levelValues[ 1 ] ) : levelValues[ 0 ]
    let levelMax = levelValues.length > 1 ? Math.max( levelValues[ 0 ], levelValues[ 1 ] ) : levelValues[ 0 ]
    let page : number = 1

    if ( commandChunks[ 2 ] ) {
        let pageValue = parseInt( commandChunks[ 2 ], 10 )
        if ( Number.isInteger( pageValue ) && page > 0 ) {
            page = pageValue
        }
    }

    let ids = DataManager.getNpcData().getTemplateIdsByLevel( levelMin, levelMax, L2NpcDataTemplateOrdering.Name )
    if ( ids.length === 0 ) {
        return player.sendMessage( `${ SearchNpcCommands.ByLevel } : No npc templates found in level range` )
    }

    let paginatedIds = _.chunk( ids, pageLimit )
    let paginationIndex = page ? page - 1 : 0
    let currentPageIds = paginatedIds[ paginationIndex ]
    if ( !currentPageIds ) {
        return player.sendMessage( `${ SearchNpcCommands.ByLevel } : Cannot display npc results for page=${ page }` )
    }

    /*
        There are three outcomes of npc being found:
        - npc spawn exists, however it has not spawned any yet
        - npc spawn exists and available npcs have been spawned
        - npc spawn does not exist, even if npc template can exist pointing to possibility of such npcs not being spawned
     */
    let items = currentPageIds.map( ( npcId: number ) => {
        let template = DataManager.getNpcData().getTemplate( npcId )
        let allSpawns = SpawnMakerCache.getAllSpawns( npcId )
        let nameColor = allSpawns ? ( allSpawns.some( spawn => spawn.hasSpawned() ) ? '07FF57' : '999999' ) : 'FF0002'

        return DataManager.getHtmlData().getItem( 'overrides/html/command/searchNpc/npcLevel-item.htm' )
            .replace( '%name%', template.getName() )
            .replace( '%nameColor%', nameColor )
            .replace( '%level%', template.getLevel().toString() )
            .replace( '%levelColor%', getLevelColor( template.getLevel() ) )
            .replace( '%button%', allSpawns ? createButton( 'View', createViewNpcByIdBypass( npcId ) ) : '  None' )
    } ).join( '' )

    let html = DataManager.getHtmlData().getItem( 'overrides/html/command/searchNpc/npcLevel.htm' )
        .replace( '%level%', commandChunks[ 1 ] )
        .replace( '%pagination%', createPagination( paginatedIds.length, `bypass -h admin_${ SearchNpcCommands.ByLevel } ${commandChunks[ 1 ]}` ) )
        .replace( '%items%', items )

    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId() ) )
}