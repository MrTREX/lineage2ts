import { IAdminCommand } from '../IAdminCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { formatOptionsDescription } from './helpers/FormatHelpers'
import _ from 'lodash'
import { DataManager } from '../../../data/manager'
import { NpcHtmlMessage } from '../../packets/send/NpcHtmlMessage'
import { L2Item } from '../../models/items/L2Item'
import { ItemInstanceType } from '../../models/items/ItemInstanceType'
import { CrystalType } from '../../models/items/type/CrystalType'
import { L2Weapon } from '../../models/items/L2Weapon'
import { GeneralHelper } from '../../helpers/GeneralHelper'
import { L2EtcItem } from '../../models/items/L2EtcItem'
import { L2ExtractableProduct } from '../../models/L2ExtractableProduct'
import { L2Armor } from '../../models/items/L2Armor'
import { Skill } from '../../models/Skill'
import { ArmorType } from '../../enums/items/ArmorType'
import { createPagination } from './helpers/SearchHelper'
import { createButton, getCrystalColor } from './helpers/AdminHtml'
import { createViewArmorSetBypass } from './ArmorSets'
import { createSearchSkillByIdBypass } from './SearchSkill'
import { L2RecipeDataInput, L2RecipeDataItem, L2RecipeDataOutput } from '../../../data/interface/RecipeDataApi'
import { createViewHtmlBypass } from './Html'
import { createViewItemHtmlBypass, getItemViewHtmlPath } from '../bypasshandlers/ItemHtml'

const enum SearchItemCommands {
    ItemByName = 'search-item-name',
    ItemById = 'search-item-id'
}

export const SearchItem: IAdminCommand = {
    getCommandNames(): Array<string> {
        return [
            SearchItemCommands.ItemByName,
            SearchItemCommands.ItemById,
        ]
    },

    getDescription( command: string ): Array<string> {
        switch ( command ) {
            case SearchItemCommands.ItemByName:
                return [
                    formatOptionsDescription(
                            SearchItemCommands.ItemByName,
                            '<partial name>',
                            'Shows matching items by name for further detailed description' ),
                ]

            case SearchItemCommands.ItemById:
                return [
                    formatOptionsDescription(
                            SearchItemCommands.ItemById,
                            '<itemId>',
                            'Shows detailed description of item specified by numeric id.' ),
                ]
        }
    },

    async onCommand( command: string, player: L2PcInstance ): Promise<void> {
        let commandChunks = command.split( ' ' )

        switch ( commandChunks[ 0 ] ) {
            case SearchItemCommands.ItemByName:
                return onSearchByItemName( commandChunks, player )

            case SearchItemCommands.ItemById:
                return onSearchByItemId( commandChunks, player )
        }
    },
}

export function createSearchItemByIdBypass( itemId: number ) : string {
    return `bypass -h admin_${ SearchItemCommands.ItemById } ${ itemId }`
}

const pageLimit: number = 60

/*
    While command description describes item name as input,
    additional parameter can be supplied for pagination value.
 */
function onSearchByItemName( commandChunks: Array<string>, player: L2PcInstance ): void {
    if ( commandChunks.length === 1 ) {
        return player.sendMessage( 'Please specify item name for search.' )
    }

    let pageValue = _.last( commandChunks )
    let currentPage = parseInt( pageValue, 10 )
    let itemName = currentPage ? commandChunks.slice( 1, commandChunks.length - 1 ).join( ' ' ) : commandChunks.slice( 1 ).join( ' ' )
    if ( !itemName ) {
        return player.sendMessage( 'Please specify non-empty item name for search.' )
    }

    let ids: Array<number> = DataManager.getItems().getIdsByPartialName( itemName )
    if ( ids.length === 0 ) {
        return player.sendMessage( `No item templates found for name '${ itemName }'` )
    }

    let paginationIndex = currentPage ? currentPage - 1 : 0
    let paginatedIds = _.chunk( ids, pageLimit )
    let currentPageIds = paginatedIds[ paginationIndex ]
    if ( !currentPageIds ) {
        return player.sendMessage( `Cannot display items page ${ paginationIndex } for item search name '${ itemName }'.` )
    }

    let itemHtml = DataManager.getHtmlData().getItem( 'overrides/html/command/searchItem/itemName-item.htm' )
    let items: string = currentPageIds.map( ( id: number ): string => {
        let dataItem = DataManager.getItems().getTemplate( id )

        return itemHtml
                .replaceAll( '%id%', id.toString() )
                .replace( '%name%', dataItem ? dataItem.getName() : `Not found item for id = ${ id }` )
                .replace( '%action%', `admin_${ SearchItemCommands.ItemById } ${ id } ${ itemName }` )
                .replace( '%crystalColor%', getCrystalColor( dataItem ? dataItem.getCrystalType() : CrystalType.NONE ) )
    } ).join( '' )

    let html = DataManager.getHtmlData().getItem( 'overrides/html/command/searchItem/itemName.htm' )
                          .replace( '%items%', items )
                          .replace( '%name%', itemName )
                          .replace( '%pagination%', createPagination( paginatedIds.length, `bypass -h admin_${ SearchItemCommands.ItemByName } ${ itemName }` ) )

    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId() ) )
}

function getItemType( item: L2Item ): string {
    switch ( item.instanceType ) {
        case ItemInstanceType.L2Armor:
            return 'Armor'

        case ItemInstanceType.L2Weapon:
            return 'Weapon'

        case ItemInstanceType.L2EtcItem:
            return 'EtcItem'

        default:
            return 'None'
    }
}

function getSearchButton( text: string ): string {
    if ( !text ) {
        return ''
    }

    return `<td><button value="Back To Search" action="bypass -h admin_${ SearchItemCommands.ItemByName } ${ text }" width=100 height=21 back="L2UI_CT1.Button_DF_Down" fore="L2UI_CT1.Button_DF"></td>`
}

function getItemSkills( item: L2Item ) : string {
    if ( item.getSkills().length === 0 ) {
        return ''
    }

    let skillHtml = DataManager.getHtmlData().getItem( 'overrides/html/command/searchItem/itemId-skills-item.htm' )
    let items : string = item.getSkills().map( ( skill : Skill ) : string => {
        return skillHtml
                .replace( '%icon%', skill.icon )
                .replace( '%name%', skill.getName() )
                .replace( '%view%', createButton( 'View', createSearchSkillByIdBypass( skill.getId() ) ) )
    } ).join( '' )

    return DataManager.getHtmlData().getItem( 'overrides/html/command/searchItem/itemId-skills.htm' )
            .replace( '%items%', items )
}

function displaySkill( skill: Skill, sectionPath: string, itemPath: string, sectionName: string ) : string {
    if ( !skill ) {
        return ''
    }

    let skillHtml = DataManager.getHtmlData().getItem( itemPath )
    let item : string = skillHtml
        .replace( '%icon%', skill.icon )
        .replace( '%name%', skill.getName() )
        .replace( '%view%', createButton( 'View', createSearchSkillByIdBypass( skill.getId() ) ) )

    return DataManager.getHtmlData().getItem( sectionPath )
                      .replace( '%name%', sectionName )
                      .replace( '%items%', item )
}

function getItemInfo( item: L2Item ): string {
    if ( item.isRecipe() ) {
        let recipe: L2RecipeDataItem = DataManager.getRecipeData().getRecipeByItemId( item.getId() )
        if ( !recipe ) {
            return ''
        }

        let itemHtml = DataManager.getHtmlData().getItem( 'overrides/html/command/searchItem/itemId-recipe-item.htm' )
        let inputItems = recipe.inputs.map( ( inputItem: L2RecipeDataInput ): string => {
            let requiredItem = DataManager.getItems().getTemplate( inputItem.id )
            if ( !requiredItem ) {
                return ''
            }

            return itemHtml
                    .replace( '%icon%', requiredItem.icon )
                    .replace( '%name%', requiredItem.getName() )
                    .replace( '%amount%', inputItem.amount.toString() )
                    .replace( '%action%', createSearchItemByIdBypass( inputItem.id ) )
        } ).join( '' )

        let recipeOutputHtml = DataManager.getHtmlData().getItem( 'overrides/html/command/searchItem/itemId-recipe-output.htm' )
        let outputItems = recipe.outputs.map( ( outputItem: L2RecipeDataOutput ) : string => {
            let producedItem = DataManager.getItems().getTemplate( outputItem.id )
            if ( !producedItem ) {
                return ''
            }

            return recipeOutputHtml
                    .replace( '%icon%', producedItem.icon )
                    .replace( '%itemId%', outputItem.id.toString() )
                    .replace( '%name%', producedItem.getName() )
                    .replace( '%action%', createSearchItemByIdBypass( outputItem.id ) )

                    .replace( '%amount%', outputItem.amount.toString() )
                    .replace( '%rate%', ( outputItem.chance * 100 ).toFixed( 0 ) )
                    .replace( '%crystalGrade%', CrystalType[ producedItem.getCrystalType() ] )
                    .replace( '%type%', getItemType( producedItem ) )

                    .replace( '%crystalColor%', getCrystalColor( item.getCrystalType() ) )
        } ).join( '' )

        return DataManager.getHtmlData().getItem( 'overrides/html/command/searchItem/itemId-recipe.htm' )
                    .replace( '%inputs%', inputItems )
                    .replace( '%recipeId%', recipe.id.toString() )
                    .replace( '%craftLevel%', recipe.level.toString() )
                    .replace( '%successRate%', ( recipe.successRate * 100 ).toFixed( 0 ) )

                    .replace( '%statAmount%', recipe.consumeMp.toString() )
                    .replace( '%type%', recipe.isCommon ? 'Common' : 'Dwarven' )
                    .replace( '%outputs%', outputItems )
    }

    if ( item.isWeaponItem() ) {
        let weapon = item as L2Weapon
        let skillItemHtml = DataManager.getHtmlData().getItem( 'overrides/html/command/searchItem/itemId-weapon-skills-item.htm' )
        let skillItems : Array<string> = []

        if ( weapon.enchant4Skill ) {
            skillItems.push( skillItemHtml
                    .replace( '%icon%', weapon.enchant4Skill.icon )
                    .replace( '%name%', `Enchant 4+ : ${weapon.enchant4Skill.getName()}` )
                    .replace( '%view%', createButton( 'View', createSearchSkillByIdBypass( weapon.enchant4Skill.getId() ) ) )
            )
        }

        if ( weapon.skillOnCrit ) {
            skillItems.push( skillItemHtml
                    .replace( '%icon%', weapon.skillOnCrit.icon )
                    .replace( '%name%', `Crit (${ weapon.skillOnCrit.activateRate }%) : ${weapon.skillOnCrit.getName()}` )
                    .replace( '%view%', createButton( 'View', createSearchSkillByIdBypass( weapon.skillOnCrit.getId() ) ) )
            )
        }

        if ( weapon.skillOnMagic ) {
            skillItems.push( skillItemHtml
                    .replace( '%icon%', weapon.skillOnMagic.icon )
                    .replace( '%name%', `Magic (${ weapon.skillsOnMagicChance * 100 }%) : ${weapon.skillOnMagic.getName()}` )
                    .replace( '%view%', createButton( 'View', createSearchSkillByIdBypass( weapon.skillOnMagic.getId() ) ) )
            )
        }

        let skillsHtml = skillItems.length === 0 ? '' : DataManager.getHtmlData().getItem( 'overrides/html/command/searchItem/itemId-weapon-skills.htm' ).replace( '%items%', skillItems.join( '' ) )

        return DataManager.getHtmlData().getItem( 'overrides/html/command/searchItem/itemId-weapon.htm' )
              .replace( '%crystalType%', CrystalType[ item.getCrystalType() ] )
              .replace( '%crystalColor%', getCrystalColor( item.getCrystalType() ) )
              .replace( '%ss%', weapon.getSoulShotCount().toString() )
              .replace( '%sps%', weapon.getSpiritShotCount().toString() )

              .replace( '%range%', weapon.getBaseAttackRange().toString() )
              .replace( '%skills%', skillsHtml )
    }

    if ( item.isArmorItem() ) {
        let armor = item as L2Armor
        let armorSet = DataManager.getArmorSetsData().getArmorSet( armor.getId() )
        let showShieldSkills = armor.getItemType() === ArmorType.SHIELD && armorSet && armorSet.shieldSkill

        return DataManager.getHtmlData().getItem( 'overrides/html/command/searchItem/itemId-armor.htm' )
              .replace( '%crystalType%', CrystalType[ item.getCrystalType() ] )
              .replace( '%crystalColor%', getCrystalColor( item.getCrystalType() ) )
              .replace( '%armorSet%', armorSet ? createButton( 'View', createViewArmorSetBypass( armorSet.id ) ) : 'No' )
              .replace( '%itemSkills%', displaySkill( armor.enchant4Skill, 'overrides/html/command/searchItem/itemId-armor-skills.htm', 'overrides/html/command/searchItem/itemId-armor-skills-item.htm', 'Armor Enchant 4+' ) )
              .replace( '%armorSetSkills%', showShieldSkills ? displaySkill( armorSet.shieldSkill, 'overrides/html/command/searchItem/itemId-armor-skills.htm', 'overrides/html/command/searchItem/itemId-armor-skills-item.htm', 'Shield Armor Set' ) : '' )
    }

    if ( item.isEtcItem() && ( item as L2EtcItem ).getExtractableItems().length > 0 ) {
        let etcItem = item as L2EtcItem
        let itemHtml = DataManager.getHtmlData().getItem( 'overrides/html/command/searchItem/itemId-extractable-item.htm' )
        let items : string = etcItem.getExtractableItems().map( ( product: L2ExtractableProduct ) : string => {
            let item = DataManager.getItems().getTemplate( product.itemId )
            if ( !item ) {
                return ''
            }

            return itemHtml
                    .replace( '%icon%', item.icon )
                    .replace( '%name%', `${item.getName()} (${product.chance}%)` )
                    .replace( '%amount%', product.min === product.max ? product.min.toString() : `${product.min}-${product.max}` )
        } ).join( '' )

        return DataManager.getHtmlData().getItem( 'overrides/html/command/searchItem/itemId-extractable.htm' )
                .replace( '%items%', items )
    }

    return ''
}

function createItemHtmlLink( item: L2Item ) : string {
    if ( !item.htmlPath ) {
        return 'No'
    }

    if ( !DataManager.getHtmlData().hasItem( getItemViewHtmlPath( item.htmlPath ) ) ) {
        return 'Missing Html'
    }

    return createButton( 'View Html', createViewItemHtmlBypass( item.htmlPath ), 70 )
}

function onSearchByItemId( commandChunks: Array<string>, player: L2PcInstance ): void {
    if ( commandChunks.length === 1 ) {
        return player.sendMessage( 'Please specify item id for search.' )
    }

    let [ , idValue, ...searchTextValues ] = commandChunks
    let id = parseInt( idValue, 10 )
    if ( !id ) {
        return player.sendMessage( 'Please specify valid number item id for search.' )
    }

    let searchText = searchTextValues.join( ' ' )

    let item = DataManager.getItems().getTemplate( id )
    if ( !item ) {
        return player.sendMessage( `No item template found for id = ${ id }` )
    }

    let html = DataManager.getHtmlData().getItem( 'overrides/html/command/searchItem/itemId.htm' )
              .replaceAll( '%itemId%', id.toString() )
              .replaceAll( '%icon%', item.icon ? item.icon : 'L2UI.SquareBlank' )
              .replace( '%name%', item.getName() )
              .replace( '%type%', getItemType( item ) )

              .replace( '%variety%', item.getItemVariety() )
              .replace( '%price%', GeneralHelper.getAdenaFormat( item.getReferencePrice() ) )
              .replace( '%weight%', item.getWeight().toString() )
              .replace( '%playerId%', player.getObjectId().toString() )

              .replace( '%buttons%', searchText ? getSearchButton( searchText ) : '' )
              .replace( '%questItem%', item.isQuestItem() ? 'Yes' : 'No' )
              .replace( '%skills%', getItemSkills( item ) )
              .replace( '%itemInfo%', getItemInfo( item ) )

              .replace( '%htmlPath%', createItemHtmlLink( item ) )

    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId() ) )
}