import { IAdminCommand } from '../IAdminCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { Olympiad } from '../../cache/Olympiad'
import { formatDescription } from './helpers/FormatHelpers'

const enum OlympiadCommands {
    SaveStatus = 'saveolympiad',
    EndOlympiad = 'endolympiad'
}

export const OlympiadManagement: IAdminCommand = {
    getCommandNames(): Array<string> {
        return [
            OlympiadCommands.SaveStatus,
            OlympiadCommands.EndOlympiad,
        ]
    },

    getDescription( command: string ): Array<string> {
        switch ( command ) {
            case OlympiadCommands.SaveStatus:
                return [
                    formatDescription( OlympiadCommands.SaveStatus, 'saves current olympiad game status' ),
                ]

            case OlympiadCommands.EndOlympiad:
                return [
                    formatDescription( OlympiadCommands.EndOlympiad, 'ends and finalizes current olympiad cycle' ),
                ]
        }
    },

    async onCommand( command: string, player: L2PcInstance ): Promise<void> {
        let currentCommand: string = command.split( ' ' )[ 0 ]

        switch ( currentCommand ) {
            case OlympiadCommands.SaveStatus:
                await Olympiad.saveOlympiadStatus()
                return player.sendMessage( 'Olympiad game status has been saved.' )

            case OlympiadCommands.EndOlympiad:
                await Olympiad.runOlympiadEndTask()
                return player.sendMessage( 'Olympiad heroes have been selected.' )
        }
    },
}