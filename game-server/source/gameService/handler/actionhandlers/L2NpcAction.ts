import { IActionHandler } from '../IActionHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Object } from '../../models/L2Object'
import { InstanceType } from '../../enums/InstanceType'
import { L2Npc } from '../../models/actor/L2Npc'
import { ILocational } from '../../models/Location'
import { MoveToTargetCharacter } from '../../packets/send/moveToTargetCharacter'
import { L2Event } from '../../models/entity/L2Event'
import { EventType, NpcApproachedForTalkEvent } from '../../models/events/EventType'
import { ConfigManager } from '../../../config/ConfigManager'
import { ListenerCache } from '../../cache/ListenerCache'
import { EventPoolCache } from '../../cache/EventPoolCache'
import { GeneralHelper } from '../../helpers/GeneralHelper'
import { StopMove } from '../../packets/send/StopMove'
import { AIIntent } from '../../aicontroller/enums/AIIntent'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import _ from 'lodash'
import { PathFinding } from '../../../geodata/PathFinding'

const enum L2NpcActionValues {
    InteractOffset = 36
}

export const L2NpcAction: IActionHandler = {
    canInteract( player: L2PcInstance, target: L2Object ): boolean {
        let npc: L2Npc = target as L2Npc
        if ( !npc.canTarget( player ) ) {
            return false
        }

        player.setLastFolkNPC( npc )

        if ( player.getTargetId() !== target.getObjectId() ) {
            player.setTarget( target )
            return false
        }

        let canAttack: boolean = npc.isAutoAttackable( player )
        if ( canAttack ) {
            if ( !npc.isAlikeDead() ) {
                if ( PathFinding.canSeeTargetWithPosition( player, npc ) ) {
                    AIEffectHelper.notifyForceAttack( player, npc )
                    return false
                }

                let destination: ILocational = PathFinding.getInteractionDestination( player, npc )
                AIEffectHelper.notifyMove( player, destination )
                return false
            }

            return false
        }

        if ( !PathFinding.canSeeTargetWithPosition( player, npc ) ) {
            let destination: ILocational = PathFinding.getInteractionDestination( player, npc )
            AIEffectHelper.notifyMove( player, destination )
            return false
        }

        return true
    },

    isReadyToInteract( player: L2PcInstance, target: L2Object ): boolean {
        let npc: L2Npc = target as L2Npc
        let canAttack: boolean = npc.isAutoAttackable( player )
        if ( canAttack ) {
            if ( !npc.isAlikeDead() ) {
                if ( PathFinding.canSeeTargetWithPosition( player, npc ) ) {
                    AIEffectHelper.notifyForceAttack( player, npc )
                    return false
                }

                let destination: ILocational = PathFinding.getInteractionDestination( player, npc )
                AIEffectHelper.notifyMove( player, destination )
                return false
            }

            return false
        }

        if ( !PathFinding.canSeeTargetWithPosition( player, npc ) ) {
            let destination: ILocational = PathFinding.getInteractionDestination( player, npc )
            AIEffectHelper.notifyMove( player, destination )
            return false
        }

        return player.canInteract( npc )
    },

    getInstanceType(): InstanceType {
        return InstanceType.L2Npc
    },

    async performActions( player: L2PcInstance, target: L2Object, shouldInteract: boolean ): Promise<void> {
        if ( !shouldInteract ) {
            return
        }

        let npc: L2Npc = target as L2Npc
        if ( npc.hasWalkingRoute() ) {
            await AIEffectHelper.setNextIntent( npc, AIIntent.INTERACT )
        }

        /*
            When interacting, following scenarios may happen:
            - player arrives at npc
            - player runs to another npc, and interacts with another npc along the way (short distance)

            To force player to stop movement we reset AIController into default intent.
         */

        if ( player.isMoving() || !player.getAIController().isIntent( AIIntent.WAITING ) ) {
            await AIEffectHelper.setNextIntent( player, AIIntent.WAITING )
        }

        await playInteractionAnimation( npc, player )

        if ( npc.hasRandomAnimation() ) {
            npc.onRandomAnimation( _.random( 7 ) )
        }

        if ( npc.isEventMob() ) {
            return L2Event.showEventHtml( player, npc.getObjectId() )
        }

        if ( ListenerCache.hasNpcTemplateListeners( npc.getId(), EventType.NpcStartQuest ) ) {
            player.setLastQuestNpc( npc.getObjectId() )
        }

        if ( ListenerCache.hasNpcTemplateListeners( npc.getId(), EventType.NpcApproachedForTalk ) ) {
            let data = EventPoolCache.getData( EventType.NpcApproachedForTalk ) as NpcApproachedForTalkEvent

            data.characterId = npc.getObjectId()
            data.playerId = player.getObjectId()
            data.characterNpcId = npc.getId()

            await ListenerCache.sendNpcTemplateEvent( npc.getId(), EventType.NpcApproachedForTalk, data )
        } else {
            npc.showChatWindowDefault( player )
        }

        if ( ConfigManager.character.getNpcTalkBlockingTime() > 0 && !player.isGM() ) {
            player.updateNotMoveUntil()
        }
    },
}

export function playInteractionAnimation( npc: L2Npc, player: L2PcInstance ): void {
    if ( !npc.canGreetPlayer() ) {
        return
    }

    player.setHeading( GeneralHelper.calculateHeadingFromLocations( player, npc ) )
    npc.setHeading( GeneralHelper.calculateHeadingFromLocations( npc, player ) )

    BroadcastHelper.dataBasedOnVisibility( player, MoveToTargetCharacter( player, npc, L2NpcActionValues.InteractOffset ) )
    player.sendOwnedData( MoveToTargetCharacter( npc, player, 20000 ) )

    /*
        We need to abort MoveToCharacter packet since client will display character following target,
        without server involvement (note MoveToCharacter offset values). Hence, we need to issue StopMove
        packet when animation is finished.
        Normally turn animation is complete within 0.75 seconds. During such time two scenarios can happen:
        - npc moves due to walking route, hence we need to make player client stop moving
        - player moves which forces npc to follow (this is not a problem due to huge range supplied with MoveToPawn),
          so once animation is complete, we terminate MoveToPawn as well
     */

    setTimeout( () => {
        if ( !npc.isMoving() ) {
            player.sendOwnedData( StopMove( npc ) )
        }

        if ( npc.hasWalkingRoute() && !player.isMoving() ) {
            player.sendOwnedData( StopMove( player ) )
        }
    }, 750 )
}