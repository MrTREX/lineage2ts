import { IActionHandler } from '../IActionHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Object } from '../../models/L2Object'
import { InstanceType } from '../../enums/InstanceType'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { SocialAction } from '../../packets/send/SocialAction'
import _ from 'lodash'
import { addQuestKeyIds, hallsKeyItemId, L2SepulcherNpcInstance, spawnMonsterIds } from '../../models/actor/instance/L2SepulcherNpcInstance'
import { InventoryAction } from '../../enums/InventoryAction'
import { ListenerCache } from '../../cache/ListenerCache'
import { EventType, NpcApproachedForTalkEvent } from '../../models/events/EventType'
import { EventPoolCache } from '../../cache/EventPoolCache'

export const L2SepulcherNpcAction: IActionHandler = {
    canInteract( player: L2PcInstance, target: L2Object ): boolean {
        if ( !( target as L2SepulcherNpcInstance ).canTarget( player ) ) {
            return false
        }

        if ( target.getObjectId() !== player.getTargetId() ) {
            player.setTarget( target )
        }

        return player.canInteract( target as L2SepulcherNpcInstance )
    },

    getInstanceType(): InstanceType {
        return InstanceType.L2SepulcherNpcInstance
    },

    isReadyToInteract( player: L2PcInstance, target: L2Object ): boolean {
        return !( target as L2SepulcherNpcInstance ).isAlikeDead()
    },

    async performActions( player: L2PcInstance, target: L2Object, shouldInteract: boolean ): Promise<void> {
        if ( !shouldInteract ) {
            return
        }

        let npc = target as L2SepulcherNpcInstance

        if ( npc.isAutoAttackable( player ) && !npc.isAlikeDead() && Math.abs( player.getZ() - npc.getZ() ) < 400 ) {
            AIEffectHelper.notifyForceAttack( player, npc )
            return
        }

        if ( npc.isAlikeDead() ) {
            return
        }

        BroadcastHelper.dataBasedOnVisibility( npc, SocialAction( npc.getObjectId(), _.random( 8 ) ) )

        if ( spawnMonsterIds.includes( npc.getId() ) ) {
            npc.setIsInvulnerable( false )
            await npc.reduceCurrentHp( npc.getMaxHp() + 1, player, null )

            npc.restartSpawnMonsterTask()
            return
        }

        if ( addQuestKeyIds.includes( npc.getId() ) ) {
            npc.setIsInvulnerable( false )
            await npc.reduceCurrentHp( npc.getMaxHp() + 1, player, null )

            let leader: L2PcInstance = player

            if ( player.getParty() && !player.getParty().isLeader( player ) ) {
                leader = player.getParty().getLeader()
            }

            await leader.addItem( hallsKeyItemId, 1, -1, player.getObjectId(), InventoryAction.Other )
            return
        }

        if ( ListenerCache.hasNpcTemplateListeners( npc.getId(), EventType.NpcStartQuest ) ) {
            player.setLastQuestNpc( npc.getObjectId() )
        }

        if ( ListenerCache.hasNpcTemplateListeners( npc.getId(), EventType.NpcApproachedForTalk ) ) {
            let eventData = EventPoolCache.getData( EventType.NpcApproachedForTalk ) as NpcApproachedForTalkEvent

            eventData.playerId = player.getObjectId()
            eventData.characterId = npc.getObjectId()
            eventData.characterNpcId = npc.getId()

            return ListenerCache.sendNpcTemplateEvent( npc.getId(), EventType.NpcApproachedForTalk, eventData )
        }

        return npc.showChatWindow( player, 0 )
    },
}