import { IActionHandler } from '../IActionHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Object } from '../../models/L2Object'
import { InstanceType } from '../../enums/InstanceType'
import { L2DoorInstance } from '../../models/actor/instance/L2DoorInstance'
import { L2NpcValues } from '../../values/L2NpcValues'
import { SiegableHall } from '../../models/entity/clanhall/SiegableHall'
import { ConfirmDialog } from '../../packets/send/SystemMessage'
import { DoorInteractionEvent, EventType } from '../../models/events/EventType'
import { ListenerCache } from '../../cache/ListenerCache'
import { DoorRequestVariable, VariableNames } from '../../variables/VariableTypes'
import { PlayerVariablesManager } from '../../variables/PlayerVariablesManager'
import { EventPoolCache } from '../../cache/EventPoolCache'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'

function sendActionConfirmation( door: L2DoorInstance, player: L2PcInstance ) : void {
    let data: DoorRequestVariable = {
        objectId: door.getObjectId(),
    }

    PlayerVariablesManager.set( player.getObjectId(), VariableNames.doorRequest, data )

    let messageId = door.isOpen() ? SystemMessageIds.WOULD_YOU_LIKE_TO_CLOSE_THE_GATE : SystemMessageIds.WOULD_YOU_LIKE_TO_OPEN_THE_GATE
    player.sendOwnedData( ConfirmDialog.fromMessageId( messageId ) )
}

export const L2DoorInstanceAction: IActionHandler = {
    canInteract( player: L2PcInstance, target: L2Object ): boolean {
        if ( player.getTargetId() !== target.getObjectId() ) {
            player.setTarget( target )
            return false
        }

        if ( !target.isDoor() ) {
            return false
        }

        let door = target as L2DoorInstance
        let clan = player.getClan()

        if ( clan
            && door.getClanHall()
            && player.getClanId() === door.getClanHall().getOwnerId() ) {
            return false
        }

        if ( clan
            && door.getFort()
            && clan.getId() === door.getFort().getOwnerClan().getId()
            && door.isOpenableBySkill()
            && !door.getFort().getSiege().isInProgress() ) {
            return false
        }

        return true
    },

    isReadyToInteract( player: L2PcInstance, target: L2Object ): boolean {
        return ( target as L2DoorInstance ).isInsideRadius( player, L2NpcValues.interactionDistance )
    },

    getInstanceType(): InstanceType {
        return InstanceType.L2DoorInstance
    },

    async performActions( player: L2PcInstance, target: L2Object, shouldInteract: boolean ): Promise<void> {
        if ( target.isAutoAttackable( player ) ) {
            if ( Math.abs( player.getZ() - target.getZ() ) < 400 ) {
                AIEffectHelper.notifyForceAttack( player, target )
            }

            return
        }

        if ( !shouldInteract ) {
            return
        }

        let door = target as L2DoorInstance

        if ( ListenerCache.hasNpcTemplateListeners( target.getId(), EventType.DoorInteraction ) ) {
            let data = EventPoolCache.getData( EventType.DoorInteraction ) as DoorInteractionEvent

            data.doorId = target.getObjectId()
            data.doorTemplateId = target.getId()
            data.playerId = player.getObjectId()
            data.instanceId = target.getInstanceId()
            data.isOpen = door.isOpen()

            let result = await ListenerCache.getNpcTerminatedResult( door, player, EventType.DoorInteraction, data )
            if ( result.message ) {
                player.sendMessage( result.message )
            }

            if ( result.terminate ) {
                return
            }
        }

        if ( player.getClan()
                && door.getClanHall()
                && player.getClanId() === door.getClanHall().getOwnerId() ) {
            if ( !door.getClanHall().isSiegableHall() || !( door.getClanHall() as SiegableHall ).isInSiege() ) {
                return sendActionConfirmation( door, player )
            }

            return
        }

        if ( player.getClan()
                && door.getFort()
                && player.getClan().getId() === door.getFort().getOwnerClan().getId()
                && door.isOpenableBySkill()
                && !door.getFort().getSiege().isInProgress() ) {
            return sendActionConfirmation( door, player )
        }

        if ( player.isGM() ) {
            return sendActionConfirmation( door, player )
        }
    },
}