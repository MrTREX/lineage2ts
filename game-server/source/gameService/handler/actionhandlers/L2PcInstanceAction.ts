import { IActionHandler } from '../IActionHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Object } from '../../models/L2Object'
import { InstanceType } from '../../enums/InstanceType'
import { TvTEvent } from '../../models/entity/TvTEvent'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { PrivateStoreType } from '../../enums/PrivateStoreType'
import { ILocational } from '../../models/Location'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'
import { PrivateStoreListSell } from '../../packets/send/PrivateStoreListSell'
import { PrivateStoreListBuy } from '../../packets/send/PrivateStoreListBuy'
import { RecipeShopSellList } from '../../packets/send/RecipeShopSellList'
import { PathFinding } from '../../../geodata/PathFinding'

const cursedWeaponMinimumLevel = 21

export const L2PcInstanceAction: IActionHandler = {
    canInteract( player: L2PcInstance, target: L2Object ): boolean {
        if ( player.getTargetId() !== target.getObjectId() ) {
            player.setTarget( target )
            return false
        }

        if ( player.getObjectId() === target.getObjectId() ) {
            player.setTarget( target )
            return false
        }

        if ( !TvTEvent.onAction( player, target.getObjectId() ) ) {
            return false
        }

        if ( player.isOutOfControl() ) {
            return false
        }

        let lockedTarget = player.getLockedTarget()
        if ( lockedTarget && lockedTarget.getObjectId() !== target.getObjectId() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.FAILED_CHANGE_TARGET ) )
            return false
        }

        let targetPlayer = target.getActingPlayer()
        if ( PathFinding.canSeeTargetWithPosition( player, targetPlayer ) ) {
            AIEffectHelper.notifyFollow( player, targetPlayer.getObjectId() )
            return false
        }

        if ( targetPlayer.getPrivateStoreType() !== PrivateStoreType.None ) {
            return true
        }

        if ( targetPlayer.isAutoAttackable( player ) ) {
            if ( ( targetPlayer.isCursedWeaponEquipped() && ( player.getLevel() < cursedWeaponMinimumLevel ) ) //
                    || ( player.isCursedWeaponEquipped() && ( targetPlayer.getLevel() < cursedWeaponMinimumLevel ) ) ) {
                return false
            }

            if ( PathFinding.canSeeTargetWithPosition( player, targetPlayer ) ) {
                AIEffectHelper.notifyForceAttack( player, targetPlayer )
            } else {
                let destination: ILocational = PathFinding.getInteractionDestination( player, targetPlayer )
                AIEffectHelper.notifyMove( player, destination )
            }

            player.onActionRequest()
            return false
        }

        if ( PathFinding.canSeeTargetWithPosition( player, targetPlayer ) ) {
            AIEffectHelper.notifyFollow( player, targetPlayer.getObjectId() )
            return false
        }

        let destination: ILocational = PathFinding.getInteractionDestination( player, targetPlayer )
        AIEffectHelper.notifyMove( player, destination )

        return false
    },

    isReadyToInteract(): boolean {
        return true
    },

    getInstanceType(): InstanceType {
        return InstanceType.L2PcInstance
    },

    async performActions( player: L2PcInstance, target: L2Object ): Promise<void> {
        let targetPlayer: L2PcInstance = target as L2PcInstance

        if ( ( targetPlayer.getPrivateStoreType() === PrivateStoreType.Sell )
                || ( targetPlayer.getPrivateStoreType() === PrivateStoreType.PackageSell ) ) {
            return player.sendOwnedData( PrivateStoreListSell( player, targetPlayer ) )
        }

        if ( targetPlayer.getPrivateStoreType() === PrivateStoreType.Buy ) {
            return player.sendOwnedData( PrivateStoreListBuy( player, targetPlayer ) )
        }

        if ( targetPlayer.getPrivateStoreType() === PrivateStoreType.Manufacture ) {
            return player.sendOwnedData( RecipeShopSellList( player, targetPlayer ) )
        }
    },
}
