import { IActionHandler } from '../IActionHandler'
import { InstanceType } from '../../enums/InstanceType'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Object } from '../../models/L2Object'
import { AggroCache } from '../../cache/AggroCache'
import _ from 'lodash'
import { ListenerCache } from '../../cache/ListenerCache'
import { EventType, NpcApproachedForTalkEvent } from '../../models/events/EventType'
import { EventPoolCache } from '../../cache/EventPoolCache'
import { L2GuardInstance } from '../../models/actor/instance/L2GuardInstance'
import { playInteractionAnimation } from './L2NpcAction'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'

export const L2GuardAction: IActionHandler = {
    canInteract( player: L2PcInstance, target: L2Object ): boolean {
        if ( !( target as L2GuardInstance ).canTarget( player ) ) {
            return false
        }

        if ( player.getTargetId() !== target.getObjectId() ) {
            player.setTarget( target )
            return false
        }

        return true
    },

    isReadyToInteract( player: L2PcInstance, target: L2Object ): boolean {
        return player.canInteract( target as L2GuardInstance )
    },

    async performActions( player: L2PcInstance, target: L2Object, shouldInteract: boolean ): Promise<void> {
        if ( AggroCache.hasAggro( target.getObjectId(), player.getObjectId() ) ) {
            AIEffectHelper.notifyForceAttack( player, target )
            return
        }

        if ( !shouldInteract ) {
            return
        }

        let guard = target as L2GuardInstance

        await playInteractionAnimation( guard, player )

        if ( guard.hasRandomAnimation() ) {
            guard.onRandomAnimation( _.random( 7 ) )
        }

        player.setLastFolkNPC( guard )

        if ( ListenerCache.hasNpcTemplateListeners( guard.getId(), EventType.NpcStartQuest ) ) {
            player.setLastQuestNpc( guard.getObjectId() )
        }

        if ( ListenerCache.hasNpcTemplateListeners( guard.getId(), EventType.NpcApproachedForTalk ) ) {
            let eventData = EventPoolCache.getData( EventType.NpcApproachedForTalk ) as NpcApproachedForTalkEvent

            eventData.playerId = player.getObjectId()
            eventData.characterId = guard.getObjectId()
            eventData.characterNpcId = guard.getId()

            await ListenerCache.sendNpcTemplateEvent( guard.getId(), EventType.NpcApproachedForTalk, eventData )
            return
        }

        guard.showChatWindow( player, 0 )
    },

    getInstanceType(): InstanceType {
        return InstanceType.L2GuardInstance
    },
}