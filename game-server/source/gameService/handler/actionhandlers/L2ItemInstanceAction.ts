import { IActionHandler } from '../IActionHandler'
import { InstanceType } from '../../enums/InstanceType'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Object } from '../../models/L2Object'
import { MercenaryTicketManager } from '../../instancemanager/MercenaryTicketManager'

export const L2ItemInstanceAction: IActionHandler = {
    canInteract( player: L2PcInstance, target: L2Object ): boolean {
        if ( MercenaryTicketManager.isMercenaryTicket( target.getId() ) ) {
            let castleId = MercenaryTicketManager.getTicketCastleId( target.getId() )

            if ( castleId > 0 && ( !player.isCastleLord( castleId ) || player.isInParty() ) ) {
                if ( player.isInParty() ) {
                    player.sendMessage( 'You cannot pickup mercenaries while in a party.' )
                } else {
                    player.sendMessage( 'Only the castle lord can pickup mercenaries.' )
                }

                player.setTarget( target )
                return false
            }
        }

        return !player.isFlying()
    },

    /*
        Item pick up is automatically done by AIController, hence we only need to engage here.
     */
    isReadyToInteract(): boolean {
        return false
    },

    getInstanceType(): InstanceType {
        return InstanceType.L2ItemInstance
    },

    async performActions(): Promise<void> {
        /*
            Empty actions here
         */
    },
}