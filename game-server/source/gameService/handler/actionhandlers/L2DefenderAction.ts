import { IActionHandler } from '../IActionHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Object } from '../../models/L2Object'
import { InstanceType } from '../../enums/InstanceType'
import { L2DefenderInstance } from '../../models/actor/instance/L2DefenderInstance'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'
import { ListenerCache } from '../../cache/ListenerCache'
import { EventType, NpcApproachedForTalkEvent } from '../../models/events/EventType'
import { EventPoolCache } from '../../cache/EventPoolCache'
import { L2SepulcherNpcInstance } from '../../models/actor/instance/L2SepulcherNpcInstance'

export const L2DefenderAction: IActionHandler = {
    canInteract( player: L2PcInstance, target: L2Object ): boolean {
        if ( !( target as L2DefenderInstance ).canTarget( player ) ) {
            return false
        }

        if ( target.getObjectId() !== player.getTargetId() ) {
            player.setTarget( target )
        }

        return player.canInteract( target as L2DefenderInstance )
    },

    getInstanceType(): InstanceType {
        return InstanceType.L2DefenderInstance
    },

    isReadyToInteract( player: L2PcInstance, target: L2Object ): boolean {
        return !( target as L2DefenderInstance ).isAlikeDead()
    },

    async performActions( player: L2PcInstance, target: L2Object, shouldInteract: boolean ): Promise<void> {
        if ( !shouldInteract ) {
            return
        }

        let npc = target as L2SepulcherNpcInstance

        if ( npc.isAutoAttackable( player ) && !npc.isAlikeDead() && Math.abs( player.getZ() - npc.getZ() ) < 600 ) {
            AIEffectHelper.notifyForceAttack( player, npc )
            return
        }

        if ( npc.isAlikeDead() ) {
            return
        }

        if ( ListenerCache.hasNpcTemplateListeners( npc.getId(), EventType.NpcStartQuest ) ) {
            player.setLastQuestNpc( npc.getObjectId() )
        }

        if ( ListenerCache.hasNpcTemplateListeners( npc.getId(), EventType.NpcApproachedForTalk ) ) {
            let eventData = EventPoolCache.getData( EventType.NpcApproachedForTalk ) as NpcApproachedForTalkEvent

            eventData.playerId = player.getObjectId()
            eventData.characterId = npc.getObjectId()
            eventData.characterNpcId = npc.getId()

            return ListenerCache.sendNpcTemplateEvent( npc.getId(), EventType.NpcApproachedForTalk, eventData )
        }
    },
}