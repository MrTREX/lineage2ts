import { IActionHandler } from '../IActionHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Object } from '../../models/L2Object'
import { InstanceType } from '../../enums/InstanceType'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { ActionFailed } from '../../packets/send/ActionFailed'
import { L2Summon } from '../../models/actor/L2Summon'
import { PetStatusShow } from '../../packets/send/PetStatusShow'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'
import { L2PetInstance } from '../../models/actor/instance/L2PetInstance'
import { PathFinding } from '../../../geodata/PathFinding'

export const L2SummonAction : IActionHandler = {
    canInteract( player: L2PcInstance, target: L2Object ): boolean {
        let lockedTarget = player.getLockedTarget()
        if ( lockedTarget && lockedTarget.getObjectId() !== target.getObjectId() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.FAILED_CHANGE_TARGET ) )
            return false
        }

        if ( player.getObjectId() === ( target as L2Summon ).getOwnerId()
                && player.getTargetId() === target.getObjectId() ) {
            player.sendOwnedData( PetStatusShow( target as L2PetInstance ) )
            player.updateNotMoveUntil()
            return false
        }

        return target.isSummon()
    },

    isReadyToInteract( player: L2PcInstance, target: L2Object ): boolean {
        if ( player.getTargetId() !== target.getObjectId() ) {
            player.setTarget( target )
            return false
        }

        return true
    },

    getInstanceType(): InstanceType {
        return InstanceType.L2Summon
    },

    async performActions( player: L2PcInstance, target: L2Object, shouldInteract: boolean ): Promise<void> {
        if ( !shouldInteract ) {
            return
        }

        if ( target.isAutoAttackable( player ) ) {
            if ( PathFinding.canSeeTargetWithPosition( player, target ) ) {
                AIEffectHelper.notifyForceAttack( player, target )
                return player.onActionRequest()
            }

            return
        }

        player.sendOwnedData( ActionFailed() )

        if ( ( target as L2Summon ).isInsideRadius( player, 150 ) ) {
            player.updateNotMoveUntil()
            return
        }

        if ( PathFinding.canSeeTargetWithPosition( player, target ) ) {
            return AIEffectHelper.notifyFollow( player, target.getObjectId() )
        }
    }
}