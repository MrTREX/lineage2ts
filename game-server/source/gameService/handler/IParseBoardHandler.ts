import { L2PcInstance } from '../models/actor/instance/L2PcInstance'

export interface IParseBoardHandler {
    parseCommand( command: string, player: L2PcInstance ) : boolean
    getCommands() : Array<string>
    isWriteHandler() : boolean
}