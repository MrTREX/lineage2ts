import { L2PcInstance } from '../models/actor/instance/L2PcInstance'

export interface IUserCommandHandler {
    onStart( player: L2PcInstance, commandId: number ) : Promise<boolean>

    getCommandIds() : Array<number>
}