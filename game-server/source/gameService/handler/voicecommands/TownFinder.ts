import { IVoicedCommand } from '../IVoicedCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2World } from '../../L2World'
import { L2Radar } from '../../models/L2Radar'
import { ILocational } from '../../models/Location'
import { GeneralHelper } from '../../helpers/GeneralHelper'
import { PlayerRadarCache } from '../../cache/PlayerRadarCache'
import { L2WorldArea } from '../../models/areas/WorldArea'
import { TownArea } from '../../models/areas/type/Town'
import { AreaType } from '../../models/areas/AreaType'
import Timeout = NodeJS.Timeout

interface TownFinderData {
    marker: L2Radar
    timer: Timeout
}

const markerRegistry: { [ playerId: number ]: TownFinderData } = {}

const enum TownFinderCommands {
    ManyTowns = 'towns',
    SingleTown = 'town'
}

export const TownFinder: IVoicedCommand = {
    isEnabled(): boolean {
        return true
    },

    getCommandNames(): Array<string> {
        return [
            TownFinderCommands.ManyTowns,
            TownFinderCommands.SingleTown,
        ]
    },

    async onStart( command: string, player: L2PcInstance ): Promise<void> {
        switch ( command ) {
            case TownFinderCommands.ManyTowns:
                return onFindTowns( player )

            case TownFinderCommands.SingleTown:
                return onFindTown( player )
        }
    },
}

function onFindTowns( player: L2PcInstance ): void {
    /*
        Proper town areas are always taxable. There are areas that are not taxable, and these are in remote locations,
        hence do not represent proper towns.
     */
    let areas: Array<L2WorldArea> = L2World.getNearestTowns( player, 6 )

    if ( areas.length === 0 ) {
        return
    }

    player.sendMessage( 'Distances to town center:' )
    return displayTownInformation( player, areas )
}

function clearData( objectId: number ): void {
    let data: TownFinderData = markerRegistry[ objectId ]
    if ( data ) {
        data.marker.removeAllMarkers()
        clearTimeout( data.timer )
    }

    delete markerRegistry[ objectId ]
}

function onFindTown( player: L2PcInstance ): void {
    /*
        Proper town areas are always taxable. There are areas that are not taxable, and these are in remote locations,
        hence do not represent proper towns.
     */
    let areas: Array<L2WorldArea> = L2World.getNearestTowns( player, 2 )

    if ( areas.length === 0 ) {
        return
    }

    return displayTownInformation( player, areas, 30000 )
}

function displayTownInformation( player: L2PcInstance, areas: Array<L2WorldArea>, duration: number = 15000 ): void {
    if ( player.isInArea( AreaType.Town ) ) {
        areas.shift()
    } else {
        areas.pop()
    }

    let speed = player.getMoveSpeed()
    areas.forEach( ( area: TownArea ) => {
        let distance = player.calculateDistance( area.getCenterPointLocation() )
        player.sendMessage( `${ area.properties.name }, ${ GeneralHelper.formatSeconds( Math.floor( distance / speed ) ).join( ' ' ) } away` )
    } )

    let data: TownFinderData = markerRegistry[ player.getObjectId() ]
    if ( !data ) {
        data = {
            marker: PlayerRadarCache.getRadar( player.getObjectId() ),
            timer: undefined,
        }

        markerRegistry[ player.getObjectId() ] = data
    } else {
        data.marker.removeAllMarkers()
        clearTimeout( data.timer )
    }

    let location: ILocational = ( areas[ 0 ] as TownArea ).getCenterPointLocation()
    data.marker.addMarker( location.getX(), location.getY(), location.getZ() )
    data.timer = setTimeout( clearData, duration, player.getObjectId() )
}