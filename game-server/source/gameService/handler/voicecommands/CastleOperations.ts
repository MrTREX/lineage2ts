import { IVoicedCommand } from '../IVoicedCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { L2DoorInstance } from '../../models/actor/instance/L2DoorInstance'
import { CastleManager } from '../../instancemanager/CastleManager'
import { Castle } from '../../models/entity/Castle'

export const CastleOperations: IVoicedCommand = {
    isEnabled(): boolean {
        return true
    },

    getCommandNames(): Array<string> {
        return [
            'opendoors',
            'closedoors',
            'ridewyvern',
        ]
    },

    async onStart( command: string, player: L2PcInstance, parametersLine: string ): Promise<void> {
        switch ( command ) {
            case 'opendoors':
                return onOpenDoors( player, parametersLine )

            case 'closedoors':
                return onCloseDoors( player, parametersLine )

            case 'ridewyvern':
                if ( player.isClanLeader() && ( player.getClan().getCastleId() > 0 ) ) {
                    await player.mountWithId( 12621, 0, true )
                }

                return
        }
    },
}

function onOpenDoors( player: L2PcInstance, parameter: string ): void {
    if ( parameter !== 'castle' ) {
        player.sendMessage( 'Only Castle doors can be open' )
        return
    }

    if ( !player.isClanLeader() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ONLY_CLAN_LEADER_CAN_ISSUE_COMMANDS ) )
        return
    }

    let door: L2DoorInstance = player.getTarget() as L2DoorInstance
    if ( !door || !door.isDoor() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
        return
    }

    let castle: Castle = CastleManager.getCastleById( player.getClan().getCastleId() )
    if ( !castle ) {
        player.sendMessage( `Clan '${ player.getClan().getName() }' does not own a castle` )
        return
    }

    if ( castle.getSiege().isInProgress() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.GATES_NOT_OPENED_CLOSED_DURING_SIEGE ) )
        return
    }

    if ( castle.isInSiegeArea( door.getX(), door.getY(), door.getZ() ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.GATE_IS_OPENING ) )
        door.openMe()
        return
    }
}

function onCloseDoors( player: L2PcInstance, parameter: string ): void {
    if ( parameter !== 'castle' ) {
        player.sendMessage( 'Only Castle doors can be closed' )
        return
    }

    if ( !player.isClanLeader() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ONLY_CLAN_LEADER_CAN_ISSUE_COMMANDS ) )
        return
    }

    let door: L2DoorInstance = player.getTarget() as L2DoorInstance
    if ( !door || !door.isDoor() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
        return
    }

    let castle: Castle = CastleManager.getCastleById( player.getClan().getCastleId() )
    if ( !castle ) {
        player.sendMessage( `Clan '${ player.getClan().getName() }' does not own a castle` )
        return
    }

    if ( castle.getSiege().isInProgress() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.GATES_NOT_OPENED_CLOSED_DURING_SIEGE ) )
        return
    }

    if ( castle.isInSiegeArea( door.getX(), door.getY(), door.getZ() ) ) {
        player.sendMessage( 'Gate is closing' )
        door.closeMe()
    }
}