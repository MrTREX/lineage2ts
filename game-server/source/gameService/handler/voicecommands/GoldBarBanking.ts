import { IVoicedCommand } from '../IVoicedCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ItemTypes } from '../../values/InventoryValues'
import { ConfigManager } from '../../../config/ConfigManager'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { GeneralHelper } from '../../helpers/GeneralHelper'

const goldBarItemId = 3470

export const GoldBarBanking: IVoicedCommand = {
    isEnabled(): boolean {
        return true
    },

    getCommandNames(): Array<string> {
        return [
            'bank',
            'withdraw',
            'deposit',
        ]
    },

    async onStart( command: string, player: L2PcInstance ): Promise<void> {
        if ( !ConfigManager.customs.bankingEnabled() ) {
            player.sendMessage( 'Gold Bar Banking is disabled' )
            return
        }

        switch ( command ) {
            case 'bank':
                return onExplainBanking( player )

            case 'deposit':
                return onDeposit( player )

            case 'withdraw':
                return onWithdraw( player )
        }
    },

}

function onExplainBanking( player: L2PcInstance ): void {
    player.sendMessage( `.deposit (${ ConfigManager.customs.getBankingAdenaCount() } Adena => ${ ConfigManager.customs.getBankingGoldbarCount() } Goldbars) / .withdraw (${ ConfigManager.customs.getBankingGoldbarCount() } Goldbars => ${ ConfigManager.customs.getBankingAdenaCount() } Adena)` )
}

async function onDeposit( player: L2PcInstance ): Promise<void> {
    if ( player.getInventory().getInventoryItemCount( ItemTypes.Adena, 0 ) >= ConfigManager.customs.getBankingAdenaCount() ) {
        if ( !await player.reduceAdena( ConfigManager.customs.getBankingAdenaCount(), false, 'GoldBarBanking.onDeposit' ) ) {
            return
        }

        await player.getInventory().addItem( goldBarItemId, ConfigManager.customs.getBankingGoldbarCount(), null, 'GoldBarBanking.onDeposit' )

        let adenaMessage = new SystemMessageBuilder( SystemMessageIds.S1_DISAPPEARED_ADENA )
                .addNumber( ConfigManager.customs.getBankingAdenaCount() )
                .getBuffer()
        player.sendOwnedData( adenaMessage )

        let goldMessage = new SystemMessageBuilder( SystemMessageIds.YOU_OBTAINED_S1_S2 )
                .addItemNameWithId( goldBarItemId )
                .addNumber( ConfigManager.customs.getBankingGoldbarCount() )
                .getBuffer()
        player.sendOwnedData( goldMessage )
        return
    }

    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ADENA ) )
    player.sendMessage( `You need at least ${ GeneralHelper.getAdenaFormat( ConfigManager.customs.getBankingAdenaCount() ) } Adena.` )
}

async function onWithdraw( player: L2PcInstance ): Promise<void> {
    if ( player.getInventory().getInventoryItemCount( goldBarItemId, 0 ) >= ConfigManager.customs.getBankingGoldbarCount()
            && await player.destroyItemByItemId( goldBarItemId, ConfigManager.customs.getBankingGoldbarCount(), false, 'GoldBarBanking.onWithdraw' ) ) {

        await player.getInventory().addAdena( ConfigManager.customs.getBankingAdenaCount(), null, 'GoldBarBanking.onWithdraw' )

        let adenaMessage = new SystemMessageBuilder( SystemMessageIds.YOU_PICKED_UP_S1_ADENA )
                .addString( GeneralHelper.getAdenaFormat( ConfigManager.customs.getBankingAdenaCount() ) )
                .getBuffer()
        player.sendOwnedData( adenaMessage )

        let goldMessage = new SystemMessageBuilder( SystemMessageIds.S2_S1_DISAPPEARED )
                .addItemNameWithId( goldBarItemId )
                .addNumber( ConfigManager.customs.getBankingGoldbarCount() )
                .getBuffer()
        player.sendOwnedData( goldMessage )

        return
    }

    player.sendMessage( `Need at least ${ ConfigManager.customs.getBankingGoldbarCount() } Goldbars to receive ${ GeneralHelper.getAdenaFormat( ConfigManager.customs.getBankingAdenaCount() ) } Adena` )
}