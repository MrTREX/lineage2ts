import { PunishmentRecord } from '../models/punishment/PunishmentRecord'
import { PunishmentType } from '../models/punishment/PunishmentType'
import { PunishmentEffect } from '../models/punishment/PunishmentEffect'
import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { L2World } from '../L2World'
import { GameClient } from '../GameClient'
import { L2GameClientRegistry } from '../L2GameClientRegistry'
import _ from 'lodash'

export interface IPunishmentHandler {
    onStart( record: PunishmentRecord ): Promise<void>

    onEnd( record: PunishmentRecord ): Promise<void>

    getType(): PunishmentType

    applyPunishment( player: L2PcInstance, record: PunishmentRecord ): void

    removePunishment( player: L2PcInstance, record: PunishmentRecord ): void
}

export class PunishmentHandlerBase implements IPunishmentHandler {
    applyPunishment( player: L2PcInstance, record: PunishmentRecord ): void {

    }

    getType(): PunishmentType {
        return PunishmentType.NONE
    }

    onEnd( record: PunishmentRecord ): Promise<void> {
        switch ( record.effect ) {
            case PunishmentEffect.CHARACTER:
                let objectId = _.parseInt( record.targetId )
                let player: L2PcInstance = L2World.getPlayer( objectId )
                if ( player ) {
                    this.removePunishment( player, record )
                }

                return

            case PunishmentEffect.ACCOUNT:
                let accountName = record.targetId
                let client: GameClient = L2GameClientRegistry.getClientByAccount( accountName )
                if ( client && client.player ) {
                    this.removePunishment( client.player, record )
                    return
                }

                return

            case PunishmentEffect.IP: {
                let punishment = this
                L2GameClientRegistry.getClientByIpd( record.targetId ).forEach( ( client: GameClient ) => {
                    if ( client.player ) {
                        punishment.removePunishment( client.player, record )
                    }
                } )

                return
            }
        }
    }

    removePunishment( player: L2PcInstance, record: PunishmentRecord ): void {

    }

    async onStart( record: PunishmentRecord ): Promise<void> {
        switch ( record.effect ) {
            case PunishmentEffect.CHARACTER:
                let objectId = _.parseInt( record.targetId )
                let player: L2PcInstance = L2World.getPlayer( objectId )
                if ( player ) {
                    this.applyPunishment( player, record )
                }

                return

            case PunishmentEffect.ACCOUNT:
                let accountName = record.targetId
                let client: GameClient = L2GameClientRegistry.getClientByAccount( accountName )
                if ( client ) {
                    if ( client.player ) {
                        return this.applyPunishment( client.player, record )
                    }

                    client.abortConnection()
                }

                return

            case PunishmentEffect.IP: {
                let punishment = this
                L2GameClientRegistry.getClientByIpd( record.targetId ).forEach( ( client: GameClient ) => {
                    if ( client.player ) {
                        punishment.applyPunishment( client.player, record )
                    }
                } )

                return
            }
        }
    }
}