import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { L2Playable } from '../../models/actor/L2Playable'
import { DataManager } from '../../../data/manager'
import { ActionFailed } from '../../packets/send/ActionFailed'
import { NpcHtmlMessagePath } from '../../packets/send/NpcHtmlMessage'
import { getItemViewHtmlPath } from '../bypasshandlers/ItemHtml'

export async function Book( character: L2Playable, item: L2ItemInstance ): Promise<boolean> {
    if ( !character.isPlayer() ) {
        character.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ITEM_NOT_FOR_PETS ) )
        return false
    }

    if ( !item.getItem().htmlPath ) {
        return false
    }

    let player: L2PcInstance = character as L2PcInstance
    let path = getItemViewHtmlPath( item.getItem().htmlPath )
    let content: string

    if ( !DataManager.getHtmlData().hasItem( path ) ) {
        content = `Text is missing for entry: ${ path }`
    } else {
        content = DataManager.getHtmlData().getItem( path )
    }

    player.sendOwnedData( NpcHtmlMessagePath( content, path, player.getObjectId(), 0, item.getId() ) )
    player.sendOwnedData( ActionFailed() )
    return true
}