import { L2Playable } from '../../models/actor/L2Playable'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { DataManager } from '../../../data/manager'
import { PlayerRecipeCache } from '../../cache/PlayerRecipeCache'
import { L2RecipeDataItem } from '../../../data/interface/RecipeDataApi'

export async function Recipes( character: L2Playable, item: L2ItemInstance ): Promise<boolean> {
    if ( !character.isPlayer() ) {
        character.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ITEM_NOT_FOR_PETS ) )
        return false
    }

    let player : L2PcInstance = character as L2PcInstance
    if ( player.isInCraftMode() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_ALTER_RECIPEBOOK_WHILE_CRAFTING ) )
        return false
    }

    let recipe : L2RecipeDataItem = DataManager.getRecipeData().getRecipeByItemId( item.getId() )
    if ( !recipe ) {
        return false
    }

    if ( player.inRecipeBook( recipe.id ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.RECIPE_ALREADY_REGISTERED ) )
        return false
    }

    let canCraft = player.hasDwarvenCraft() || player.hasCommonCraft()

    if ( !canCraft ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_REGISTER_NO_ABILITY_TO_CRAFT ) )
        return false
    }

    let recipeLevel: boolean
    let recipeLimit: boolean
    let books = PlayerRecipeCache.getBooks( player.getObjectId() )

    if ( recipe.isCommon ) {
        recipeLevel = recipe.level > player.getCommonCraft()
        recipeLimit = books.common.size >= player.getCommonRecipeLimit()
    } else {
        recipeLevel = recipe.level > player.getDwarvenCraft()
        recipeLimit = books.dwarven.size >= player.getDwarfRecipeLimit()
    }

    if ( recipeLevel ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CREATE_LVL_TOO_LOW_TO_REGISTER ) )
        return false
    }

    if ( recipeLimit ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.UP_TO_S1_RECIPES_CAN_REGISTER )
                .addNumber( recipe.isCommon ? player.getCommonRecipeLimit() : player.getDwarfRecipeLimit() )
                .getBuffer()

        player.sendOwnedData( packet )
        return false
    }

    PlayerRecipeCache.addRecipe( player.getObjectId(), recipe, player.getClassIndex() )

    await player.destroyItemByObjectId( item.getObjectId(), 1, false )

    let packet = new SystemMessageBuilder( SystemMessageIds.S1_ADDED )
            .addItemInstanceName( item )
            .getBuffer()
    player.sendOwnedData( packet )
    return true
}