import { L2Playable } from '../../models/actor/L2Playable'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { L2PetInstance } from '../../models/actor/instance/L2PetInstance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { Skill } from '../../models/Skill'
import { MagicSkillUse } from '../../packets/send/MagicSkillUse'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { DataManager } from '../../../data/manager'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'

export async function PetFood( character: L2Playable, item: L2ItemInstance, forceUse: boolean ): Promise<boolean> {
    if ( character.isPet() && !forceUse && !( character as L2PetInstance ).canEatFoodId( item.getId() ) ) {
        character.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.PET_CANNOT_USE_ITEM ) )
        return false
    }

    if ( item.getItem().hasSkills() ) {
        await Promise.all( item.getItem().getSkills().map( skill => tryUsingFood( character, skill, item ) ) )
    }

    return true
}

async function tryUsingFood( character: L2Playable, skill: Skill, item: L2ItemInstance ) : Promise<void> {
    if ( character.isPet() ) {
        let pet : L2PetInstance = character as L2PetInstance
        let isConsumed : boolean = await pet.destroyItemByObjectId( item.getObjectId(), 1, false )
        if ( isConsumed ) {
            if ( pet.isHungry() ) {
                pet.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOUR_PET_ATE_A_LITTLE_BUT_IS_STILL_HUNGRY ) )
            }

            BroadcastHelper.dataBasedOnVisibility( pet, MagicSkillUse( pet.getObjectId(), pet.getObjectId(), skill.getId(), skill.getLevel(), 0, 0 ) )

            /*
                Status update should be deferred some ms later, so triggering before skill effects application is adequate.
             */
            pet.broadcastStatusUpdate()
            return skill.applyEffects( pet, pet, false, false, true, 0 )
        }

        return
    }

    if ( character.isPlayer() ) {
        let player : L2PcInstance = character as L2PcInstance
        if ( !player.isMounted() ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.S1_CANNOT_BE_USED )
                    .addItemInstanceName( item )
                    .getBuffer()
            return player.sendOwnedData( packet )
        }

        let foodIds : Array<number> = DataManager.getPetData().getPetDataByNpcId( player.getMountNpcId() ).getFood()
        if ( foodIds.includes( item.getId() ) ) {
            let isConsumed = await player.destroyItemByObjectId( item.getObjectId(), 1, false )
            if ( isConsumed ) {
                BroadcastHelper.dataToSelfBasedOnVisibility( player, MagicSkillUse( player.getObjectId(), player.getObjectId(), skill.getId(), skill.getLevel(), 0, 0 ) )
                return skill.applyEffects( player, player, false, false, true, 0 )
            }
        }

        return
    }
}