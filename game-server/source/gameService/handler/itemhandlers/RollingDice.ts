import { L2Playable } from '../../models/actor/L2Playable'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { Dice } from '../../packets/send/Dice'
import _ from 'lodash'
import { AreaType } from '../../models/areas/AreaType'

export async function RollingDice( character: L2Playable, item: L2ItemInstance ): Promise<boolean> {
    if ( !character.isPlayer() ) {
        character.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ITEM_NOT_FOR_PETS ) )
        return false
    }

    let player: L2PcInstance = character as L2PcInstance
    let itemId = item.getId()

    if ( player.isInOlympiadMode() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THIS_ITEM_IS_NOT_AVAILABLE_FOR_THE_OLYMPIAD_EVENT ) )
        return false
    }

    // TODO : L2J uses rate limit for random generation? why not have a rate limiter check at start?
    let value = _.random( 1, 6 )
    if ( value === 0 ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_MAY_NOT_THROW_THE_DICE_AT_THIS_TIME_TRY_AGAIN_LATER ) )
        return false
    }

    BroadcastHelper.dataToSelfBasedOnVisibility( player, Dice( player.getObjectId(), itemId, value, player.getX() - 30, player.getY() - 30, player.getZ() ) )

    let packet = new SystemMessageBuilder( SystemMessageIds.C1_ROLLED_S2 )
            .addPlayerCharacterName( player )
            .addNumber( value )
            .getBuffer()
    player.sendOwnedData( packet )

    if ( player.isInArea( AreaType.Peace ) ) {
        BroadcastHelper.dataBasedOnVisibility( player, packet )
    } else if ( player.isInParty() ) {
        player.getParty().broadcastDataToPartyMembers( player.getObjectId(), packet )
    }

    return true
}