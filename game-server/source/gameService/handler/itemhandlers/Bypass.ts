import { L2Playable } from '../../models/actor/L2Playable'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { DataManager } from '../../../data/manager'
import { NpcHtmlMessagePath } from '../../packets/send/NpcHtmlMessage'

// TODO : remove or merge with Book handler that does same thing
export async function Bypass( character: L2Playable, item: L2ItemInstance ): Promise<boolean> {
    if ( !character.isPlayer() ) {
        return false
    }

    if ( !item.getItem().htmlPath ) {
        return false
    }

    let player: L2PcInstance = character as L2PcInstance
    let path = `overrides/html/item/${ item.getItem().htmlPath }`
    let content: string

    if ( !DataManager.getHtmlData().hasItem( path ) ) {
        content = `Text is missing for entry: ${ path }`
    } else {
        content = DataManager.getHtmlData().getItem( path )
                .replace( '%itemId%', item.getObjectId.toString() )
    }

    player.sendOwnedData( NpcHtmlMessagePath( content, path, player.getObjectId(), 0, item.getId() ) )
    return true
}