import { L2Playable } from '../../models/actor/L2Playable'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2EtcItem } from '../../models/items/L2EtcItem'
import { L2ExtractableProduct } from '../../models/L2ExtractableProduct'
import { ConfigManager } from '../../../config/ConfigManager'
import _ from 'lodash'
import aigle from 'aigle'
import { InventoryAction } from '../../enums/InventoryAction'

export async function ExtractableItems( character: L2Playable, item: L2ItemInstance ): Promise<boolean> {
    if ( !character.isPlayer() ) {
        character.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ITEM_NOT_FOR_PETS ) )
        return false
    }

    if ( !item.isEtcItem() ) {
        return false
    }

    let player : L2PcInstance = character.getActingPlayer()
    let template : L2EtcItem = item.getItem() as L2EtcItem
    let products : Array<L2ExtractableProduct> = template.getExtractableItems()
    if ( !products || products.length === 0 ) {
        return false
    }

    let isConsumed = await player.destroyItemByObjectId( item.getObjectId(), 1, true )
    if ( !isConsumed ) {
        return false
    }

    let created = false
    await aigle.resolve( products ).eachSeries( async ( product: L2ExtractableProduct ) => {
        /*
            Item data has chance for extractable items always set at 100 percent (no chance of failure).
            Hence, such check for chance is redundant and only left here as way for further data customization.
         */
        if ( product.chance !== 1 && Math.random() > product.chance ) {
            return
        }

        let min = Math.floor( product.min * ConfigManager.rates.getRateExtractable() )
        let max = Math.floor( product.max * ConfigManager.rates.getRateExtractable() )

        let createItemAmount = ( max === min ) ? min : ( _.random( ( max - min ) + 1 ) + min )
        if ( createItemAmount === 0 ) {
            return
        }

        if ( item.isStackable() || createItemAmount === 1 ) {
            await player.addItem( product.itemId, createItemAmount, -1, player.getObjectId(), InventoryAction.ExtractableItem )
        } else {
            while ( createItemAmount > 0 ) {
                await player.addItem( product.itemId, 1, -1, player.getObjectId(), InventoryAction.ExtractableItem )
                createItemAmount--
            }
        }

        created = true
    } )

    if ( !created ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOTHING_INSIDE_THAT ) )
    }

    return true
}