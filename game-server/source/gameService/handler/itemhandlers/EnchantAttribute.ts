import { L2Playable } from '../../models/actor/L2Playable'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ExChooseInventoryAttributeItem } from '../../packets/send/ExChooseInventoryAttributeItem'

export async function EnchantAttribute( character: L2Playable, item: L2ItemInstance ): Promise<boolean> {
    if ( !character.isPlayer() ) {
        character.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ITEM_NOT_FOR_PETS ) )
        return false
    }

    let player: L2PcInstance = character as L2PcInstance
    if ( player.isCastingNow() ) {
        return false
    }

    if ( player.isEnchanting() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ENCHANTMENT_ALREADY_IN_PROGRESS ) )
        return false
    }

    player.setActiveEnchantAttributeItemId( item.getObjectId() )
    player.sendOwnedData( ExChooseInventoryAttributeItem( item.getId() ) )
    return true
}
