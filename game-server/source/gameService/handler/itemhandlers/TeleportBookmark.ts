import { L2Playable } from '../../models/actor/L2Playable'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export async function TeleportBookmark( character: L2Playable, item: L2ItemInstance ): Promise<boolean> {
    if ( !character.isPlayer() ) {
        character.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ITEM_NOT_FOR_PETS ) )
        return false
    }

    let player : L2PcInstance = character as L2PcInstance

    if ( player.getAvailableBookmarkSlots() >= 9 ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOUR_NUMBER_OF_MY_TELEPORTS_SLOTS_HAS_REACHED_ITS_MAXIMUM_LIMIT ) )
        return false
    }

    await player.destroyItemByObjectId( item.getObjectId(), 1, false )

    player.setAvailableBookmarkSlots( player.getAvailableBookmarkSlots() + 3 )
    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THE_NUMBER_OF_MY_TELEPORTS_SLOTS_HAS_BEEN_INCREASED ) )

    let packet = new SystemMessageBuilder( SystemMessageIds.S1_DISAPPEARED )
            .addItemNameWithId( item.getId() )
            .getBuffer()
    player.sendOwnedData( packet )
    return true
}