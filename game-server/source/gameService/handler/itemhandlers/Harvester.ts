import { L2Playable } from '../../models/actor/L2Playable'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { ConfigManager } from '../../../config/ConfigManager'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SkillHolder } from '../../models/holders/SkillHolder'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Object } from '../../models/L2Object'
import { L2Character } from '../../models/actor/L2Character'
import { ActionFailed } from '../../packets/send/ActionFailed'
import aigle from 'aigle'

export async function Harvester( character: L2Playable, item: L2ItemInstance ): Promise<boolean> {
    if ( !ConfigManager.general.allowManor() || !item.getItem().hasSkills() ) {
        return false
    }

    if ( !character.isPlayer() ) {
        character.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ITEM_NOT_FOR_PETS ) )
        return false
    }

    let player : L2PcInstance = character as L2PcInstance
    let target : L2Object = player.getTarget()

    if ( !target || !target.isMonster() || ( target.isCharacter() && !( target as L2Character ).isDead() ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
        player.sendOwnedData( ActionFailed() )
        return false
    }

    // TODO: ensure that skills get queued up for AIController
    await Promise.all( item.getItem().getSkills().map( skill => player.useMagic( skill, false, false ) ) )
    return true
}