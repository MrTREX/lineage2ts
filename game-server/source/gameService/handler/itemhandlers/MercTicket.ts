import { L2Playable } from '../../models/actor/L2Playable'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { CastleManager } from '../../instancemanager/CastleManager'
import { Castle } from '../../models/entity/Castle'
import { MercenaryTicketManager } from '../../instancemanager/MercenaryTicketManager'
import { SevenSigns } from '../../directives/SevenSigns'
import { SevenSignsPeriod, SevenSignsSeal, SevenSignsSide } from '../../values/SevenSignsValues'

export async function MercTicket( character: L2Playable, item: L2ItemInstance ): Promise<boolean> {
    if ( !character.isPlayer() ) {
        character.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ITEM_NOT_FOR_PETS ) )
        return false
    }

    let itemId = item.getId()
    let player = character as L2PcInstance
    let castle : Castle = CastleManager.getCastle( player )
    let castleId = castle ? castle.getResidenceId() : -1

    if ( MercenaryTicketManager.getTicketCastleId( itemId ) !== castleId ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.MERCENARIES_CANNOT_BE_POSITIONED_HERE ) )
        return false
    }

    if ( !player.isCastleLord( castleId ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_DO_NOT_HAVE_AUTHORITY_TO_POSITION_MERCENARIES ) )
        return false
    }

    if ( castle && castle.getSiege().isInProgress() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THIS_MERCENARY_CANNOT_BE_POSITIONED_ANYMORE ) )
        return false
    }

    if ( SevenSigns.getCurrentPeriod() !== SevenSignsPeriod.SealValidation ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THIS_MERCENARY_CANNOT_BE_POSITIONED_ANYMORE ) )
        return false
    }

    switch ( SevenSigns.getWinningSealSide( SevenSignsSeal.Strife ) ) {
        case SevenSignsSide.None:
            if ( checkIsDawnPostingTicket( itemId ) ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THIS_MERCENARY_CANNOT_BE_POSITIONED_ANYMORE ) )
                return false
            }

            break

        case SevenSignsSide.Dusk:
            if ( !checkIsRookiePostingTicket( itemId ) ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THIS_MERCENARY_CANNOT_BE_POSITIONED_ANYMORE ) )
                return false
            }

            break

        case SevenSignsSide.Dawn:
            break
    }

    if ( MercenaryTicketManager.isAtCastleLimit( item.getId() ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THIS_MERCENARY_CANNOT_BE_POSITIONED_ANYMORE ) )
        return false
    }

    if ( MercenaryTicketManager.isAtTypeLimit( item.getId() ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THIS_MERCENARY_CANNOT_BE_POSITIONED_ANYMORE ) )
        return false
    }

    if ( MercenaryTicketManager.isTooCloseToAnotherTicket( player.getX(), player.getY(), player.getZ() ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.POSITIONING_CANNOT_BE_DONE_BECAUSE_DISTANCE_BETWEEN_MERCENARIES_TOO_SHORT ) )
        return false
    }

    MercenaryTicketManager.addTicket( item.getId(), player )

    await player.destroyItemByObjectId( item.getObjectId(), 1, false )

    // TODO : how does direction get finalized? recheck if message needs additional parameters
    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.PLACE_CURRENT_LOCATION_DIRECTION ) )
    return true
}

function checkIsRookiePostingTicket( itemId: number ): boolean {
    if ( itemId > 6174 && itemId < 6295 ) {
        return true
    }

    if ( itemId > 6811 && itemId < 6832 ) {
        return true
    }

    if ( itemId > 7950 && itemId < 7971 ) {
        return true
    }

    return itemId > 8007 && itemId < 8028
}

function checkIsDawnPostingTicket( itemId: number ): boolean {
    if ( itemId > 6114 && itemId < 6175 ) {
        return true
    }

    if ( itemId > 6801 && itemId < 6812 ) {
        return true
    }

    if ( itemId > 7997 && itemId < 8008 ) {
        return true
    }

    if ( itemId > 7940 && itemId < 7951 ) {
        return true
    }

    if ( itemId > 6294 && itemId < 6307 ) {
        return true
    }

    if ( itemId > 6831 && itemId < 6834 ) {
        return true
    }

    if ( itemId > 8027 && itemId < 8030 ) {
        return true
    }

    return itemId > 7970 && itemId < 7973
}