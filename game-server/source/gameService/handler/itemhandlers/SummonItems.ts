import { L2Playable } from '../../models/actor/L2Playable'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { TvTEvent } from '../../models/entity/TvTEvent'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2PetData } from '../../models/L2PetData'
import { DataManager } from '../../../data/manager'
import { ItemSkillsTemplate } from './ItemSkillsTemplate'
import { PetRequestVariable, VariableNames } from '../../variables/VariableTypes'
import { PlayerVariablesManager } from '../../variables/PlayerVariablesManager'

export async function SummonItems( character: L2Playable, item: L2ItemInstance, forceUse: boolean ): Promise<boolean> {
    if ( !character.isPlayer() ) {
        character.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ITEM_NOT_FOR_PETS ) )
        return false
    }

    if ( !TvTEvent.onItemSummon( character.getObjectId() ) ) {
        return false
    }

    let player: L2PcInstance = character as L2PcInstance

    if ( player.isSitting() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_MOVE_SITTING ) )
        return false
    }

    if ( player.hasSummon() || player.isMounted() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ALREADY_HAVE_A_PET ) )
        return false
    }

    if ( player.isAttackingNow() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_SUMMON_IN_COMBAT ) )
        return false
    }

    let petData: L2PetData = DataManager.getPetData().getPetDataByItemId( item.getId() )
    if ( !petData || petData.getNpcId() === -1 ) {
        return false
    }

    let data: PetRequestVariable = {
        objectId: item.getObjectId(),
        itemId: item.getId(),
    }

    PlayerVariablesManager.set( player.getObjectId(), VariableNames.petRequest, data )

    return ItemSkillsTemplate( character, item, forceUse )
}