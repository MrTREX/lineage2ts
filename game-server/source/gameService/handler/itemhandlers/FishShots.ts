import { L2Playable } from '../../models/actor/L2Playable'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Weapon } from '../../models/items/L2Weapon'
import { WeaponType } from '../../models/items/type/WeaponType'
import { ShotType } from '../../enums/ShotType'
import { ActionType } from '../../models/items/type/ActionType'
import { L2Object } from '../../models/L2Object'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { MagicSkillUseWithCharacters } from '../../packets/send/MagicSkillUse'
import { Skill } from '../../models/Skill'

export async function FishShots( character: L2Playable, item: L2ItemInstance ): Promise<boolean> {
    if ( !character.isPlayer() ) {
        character.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ITEM_NOT_FOR_PETS ) )
        return false
    }

    if ( !item.getItem().hasSkills() ) {
        return false
    }

    if ( item.getCount() < 1 ) {
        return false
    }

    let player: L2PcInstance = character.getActingPlayer()
    let weapon: L2ItemInstance = player.getActiveWeaponInstance()
    let weaponItem: L2Weapon = player.getActiveWeaponItem()

    if ( !weapon || ( weaponItem.getItemType() !== WeaponType.FISHINGROD ) ) {
        return false
    }

    if ( player.isChargedShot( ShotType.FishingSoulshot ) ) {
        return false
    }

    let skill: Skill = item.getItem().getSkills()[ 0 ]

    let gradeCheck = item.isEtcItem()
            && ( item.getEtcItem().getDefaultAction() === ActionType.FISHINGSHOT )
            && ( weapon.getItem().getItemGradeSPlus() === item.getItem().getItemGradeSPlus() )

    if ( !gradeCheck ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.WRONG_FISHINGSHOT_GRADE ) )
        return false
    }

    player.setChargedShot( ShotType.FishingSoulshot, true )
    await player.destroyItemByObjectId( item.getObjectId(), 1, false )

    let oldTarget: L2Object = player.getTarget()
    player.setTarget( player )

    BroadcastHelper.dataBasedOnVisibility( player, MagicSkillUseWithCharacters( player, player, skill.getId(), skill.getLevel(), 0, 0 ) )

    player.setTarget( oldTarget )
    return true
}