import { IUserCommandHandler } from '../IUserCommandHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { RespawnRegionCache } from '../../cache/RespawnRegionCache'

export const Loc: IUserCommandHandler = {
    getCommandIds(): Array<number> {
        return [ 0 ]
    },

    async onStart( player: L2PcInstance ): Promise<boolean> {
        let messageId: number = RespawnRegionCache.getLocationMessageId( player )

        let packet = new SystemMessageBuilder( messageId )
            .addString( player.getX().toString() )
            .addString( player.getY().toString() )
            .addString( player.getZ().toString() )
            .getBuffer()

        player.sendOwnedData( packet )
        return true
    },
}