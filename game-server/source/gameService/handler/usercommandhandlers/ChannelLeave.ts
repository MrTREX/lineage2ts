import { IUserCommandHandler } from '../IUserCommandHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { L2CommandChannel } from '../../models/L2CommandChannel'
import { L2Party } from '../../models/L2Party'

export const ChannelLeave: IUserCommandHandler = {
    getCommandIds(): Array<number> {
        return [ 96 ]
    },

    async onStart( player: L2PcInstance ): Promise<boolean> {
        if ( !player.isInParty() || !player.getParty().isLeader( player ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ONLY_PARTY_LEADER_CAN_LEAVE_CHANNEL ) )
            return false
        }

        if ( player.getParty().isInCommandChannel() ) {
            let channel: L2CommandChannel = player.getParty().getCommandChannel()
            let party: L2Party = player.getParty()

            channel.removeParty( party )
            let partyLeader: L2PcInstance = party.getLeader()
            partyLeader.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.LEFT_COMMAND_CHANNEL ) )

            let message: Buffer = new SystemMessageBuilder( SystemMessageIds.C1_PARTY_LEFT_COMMAND_CHANNEL )
                    .addPlayerCharacterName( partyLeader )
                    .getBuffer()
            channel.broadcastPacket( message )

            return true
        }

        return false
    },

}