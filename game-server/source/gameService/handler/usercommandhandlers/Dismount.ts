import { IUserCommandHandler } from '../IUserCommandHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export const Dismount : IUserCommandHandler = {
    getCommandIds(): Array<number> {
        return [ 62 ]
    },

    async onStart( player: L2PcInstance, commandId: number ): Promise<boolean> {
        if ( player.isRentedPet() ) {
            await player.stopRentPet()
            return true
        }

        if ( player.isMounted() ) {
            await player.dismount()
        }

        return true
    }
}