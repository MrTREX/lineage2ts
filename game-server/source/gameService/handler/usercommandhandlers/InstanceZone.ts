import { IUserCommandHandler } from '../IUserCommandHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { InstanceManager } from '../../instancemanager/InstanceManager'
import { InstanceWorld } from '../../models/instancezone/InstanceWorld'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'

export const InstanceZone : IUserCommandHandler = {
    getCommandIds(): Array<number> {
        return [ 114 ]
    },

    async onStart( player: L2PcInstance ): Promise<boolean> {

        let world : InstanceWorld = InstanceManager.getPlayerWorld( player.getObjectId() )
        if ( world && world.getTemplateId() >= 0 ) {

            let packet = new SystemMessageBuilder( SystemMessageIds.INSTANT_ZONE_CURRENTLY_INUSE_S1 )
                    .addInstanceName( world.getTemplateId() )
                    .getBuffer()
            player.sendOwnedData( packet )
        }

        let instanceTimes = await InstanceManager.getAllInstanceTimes( player.getObjectId() )
        let isMessageSent = false
        let currentTime = Date.now()
        let deletedPromises = []

        instanceTimes.forEach( ( time: number, instanceId: number ) => {
            let remainingTime = ( time - currentTime ) / 1000
            if ( remainingTime > 60 ) {
                if ( isMessageSent ) {
                    isMessageSent = true
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INSTANCE_ZONE_TIME_LIMIT ) )
                }

                let hours = Math.floor( remainingTime / 3600 )
                let minutes = Math.floor( ( remainingTime % 3600 ) / 60 )
                let packet = new SystemMessageBuilder( SystemMessageIds.AVAILABLE_AFTER_S1_S2_HOURS_S3_MINUTES )
                        .addInstanceName( instanceId )
                        .addNumber( hours )
                        .addNumber( minutes )
                        .getBuffer()
                player.sendOwnedData( packet )
                return
            }

            deletedPromises.push( InstanceManager.deleteInstanceTime( player.getObjectId(), instanceId ) )
        } )

        await Promise.all( deletedPromises )

        if ( !isMessageSent ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NO_INSTANCEZONE_TIME_LIMIT ) )
        }

        return true
    }
}