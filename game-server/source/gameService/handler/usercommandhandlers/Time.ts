import { IUserCommandHandler } from '../IUserCommandHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { GameTime } from '../../cache/GameTime'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { ConfigManager } from '../../../config/ConfigManager'
import moment from 'moment'

export const Time: IUserCommandHandler = {
    getCommandIds(): Array<number> {
        return [ 77 ]
    },

    async onStart( player: L2PcInstance ): Promise<boolean> {
        let messageId: number = GameTime.isNight() ? SystemMessageIds.TIME_S1_S2_IN_THE_NIGHT : SystemMessageIds.TIME_S1_S2_IN_THE_DAY
        let packet : Buffer = new SystemMessageBuilder( messageId )
                .addString( GameTime.getHour().toString() )
                .addString( GameTime.getMinutes().toString() )
                .getBuffer()

        player.sendOwnedData( packet )

        if ( ConfigManager.customs.displayServerTime() ) {
            player.sendMessage( `Server time: ${ moment().format( 'dddd, MMMM Do YYYY, h:mm:ss a' ) }` )
        }

        return true
    },
}