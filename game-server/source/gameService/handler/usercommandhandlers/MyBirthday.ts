import { IUserCommandHandler } from '../IUserCommandHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import moment from 'moment'

export const MyBirthday: IUserCommandHandler = {
    getCommandIds(): Array<number> {
        return [ 126 ]
    },

    async onStart( player: L2PcInstance ): Promise<boolean> {
        let createDate = moment( player.getCreateDate() ).add( 1, 'year' )

        let packet = new SystemMessageBuilder( SystemMessageIds.C1_BIRTHDAY_IS_S3_S4_S2 )
                .addPlayerCharacterName( player )
                .addString( createDate.format( 'YYYY' ) )
                .addString( createDate.format( 'MMMM' ) )
                .addString( createDate.format( 'Do' ) )
                .getBuffer()

        player.sendOwnedData( packet )
        return true
    },
}