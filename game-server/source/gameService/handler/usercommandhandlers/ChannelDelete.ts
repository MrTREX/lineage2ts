import { IUserCommandHandler } from '../IUserCommandHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Party } from '../../models/L2Party'
import { L2CommandChannel } from '../../models/L2CommandChannel'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'

export const ChannelDelete : IUserCommandHandler = {
    getCommandIds(): Array<number> {
        return [ 93 ]
    },

    async onStart( player: L2PcInstance, commandId: number ): Promise<boolean> {
        if ( player.isInParty() ) {
            let party: L2Party = player.getParty()
            if ( party.isLeader( player )
                    && party.isInCommandChannel()
                    && party.getCommandChannel().getLeaderObjectId() === player.getObjectId() ) {
                let channel: L2CommandChannel = party.getCommandChannel()
                channel.broadcastPacket( SystemMessageBuilder.fromMessageId( SystemMessageIds.COMMAND_CHANNEL_DISBANDED ) )
                channel.disbandChannel()

                return true
            }
        }

        return false
    }
}