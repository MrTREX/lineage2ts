import { IUserCommandHandler } from '../IUserCommandHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ActionFailed } from '../../packets/send/ActionFailed'
import { TvTEvent } from '../../models/entity/TvTEvent'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { ConfigManager } from '../../../config/ConfigManager'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { MagicSkillUseWithCharacters } from '../../packets/send/MagicSkillUse'
import { SetupGauge, SetupGaugeColors } from '../../packets/send/SetupGauge'
import { TeleportWhereType } from '../../enums/TeleportWhereType'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'
import { AIIntent } from '../../aicontroller/enums/AIIntent'
import { L2World } from '../../L2World'
import { SkillItem } from '../../models/skills/SkillItem'
import { getExistingSkill } from '../../models/holders/SkillHolder'

const escapeOneSecond : SkillItem = {
    id: 2100,
    level: 1
}
const escapeFiveMinutes : SkillItem = {
    id: 2099,
    level: 1
}

export const Unstuck : IUserCommandHandler = {
    getCommandIds(): Array<number> {
        return [ 52 ]
    },

    async onStart( player: L2PcInstance ): Promise<boolean> {
        if ( !TvTEvent.onEscapeUse( player.getObjectId() ) ) {
            player.sendOwnedData( ActionFailed() )
            return false
        }

        if ( player.isJailed() ) {
            player.sendMessage( 'You cannot use unstuck while you are in jail.' )
            return false
        }

        if ( player.isInOlympiadMode() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THIS_SKILL_IS_NOT_AVAILABLE_FOR_THE_OLYMPIAD_EVENT ) )
            return false
        }

        if ( player.isCastingNow()
                || player.isMovementDisabled()
                || player.isMuted()
                || player.isAlikeDead()
                || player.inObserverMode()
                || player.isCombatFlagEquipped() ) {
            return false
        }

        if ( player.isGM() ) {
            await player.doCast( getExistingSkill( escapeOneSecond.id, escapeOneSecond.level ) )
            return true
        }

        if ( ConfigManager.character.getUnstuckInterval() === 300 ) {
            await player.doCast( getExistingSkill( escapeFiveMinutes.id, escapeFiveMinutes.level ) )
            return true
        }

        let escapeSeconds = ConfigManager.character.getUnstuckInterval() / 1000
        if ( escapeSeconds > 100 ) {
            player.sendMessage( `You use Escape: ${Math.floor( escapeSeconds / 60 )} minutes.` )
        } else {
            player.sendMessage( `You use Escape: ${Math.floor( escapeSeconds )} seconds.` )
        }

        await AIEffectHelper.setNextIntent( player, AIIntent.WAITING )
        player.setTarget( player )
        player.disableAllSkills()

        let unstuckValue = player.isGM() ? 1000 : ConfigManager.character.getUnstuckInterval()
        player.setCastInterruptTime( Date.now() + unstuckValue )
        BroadcastHelper.dataToSelfInRange( player, MagicSkillUseWithCharacters( player, player, 1050, 1, unstuckValue, 0 ), 900 )

        player.sendOwnedData( SetupGauge( player.getObjectId(), SetupGaugeColors.Blue, unstuckValue ) )

        // TODO : convert to MagicUseTask to allow blocking of other skill casts
        // TODO : refactor .doCast calls to use AIController
        setTimeout( performTeleport.bind( null, player.getObjectId() ), unstuckValue )

        return true
    }
}

async function performTeleport( playerId: number ) : Promise<void> {
    let player = L2World.getPlayer( playerId )
    if ( player && !player.isDead() ) {
        player.setIsIn7sDungeon( false )
        player.enableAllSkills()
        player.setInstanceId( 0 )

        return player.teleportToLocationType( TeleportWhereType.TOWN )
    }
}