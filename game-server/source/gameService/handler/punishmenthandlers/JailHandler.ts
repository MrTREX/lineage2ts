import { PunishmentHandlerBase } from '../IPunishmentHandler'
import { PunishmentType } from '../../models/punishment/PunishmentType'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { PunishmentRecord } from '../../models/punishment/PunishmentRecord'
import { TvTEvent } from '../../models/entity/TvTEvent'
import { OlympiadManager } from '../../models/olympiad/OlympiadManager'
import { DataManager } from '../../../data/manager'
import { NpcHtmlMessagePath } from '../../packets/send/NpcHtmlMessage'
import { ListenerCache } from '../../cache/ListenerCache'
import { EventType, PlayerLoginEvent } from '../../models/events/EventType'
import { L2World } from '../../L2World'
import { DeferredMethods } from '../../helpers/DeferredMethods'
import { Location } from '../../models/Location'
import { AreaType } from '../../models/areas/AreaType'

/*
    TODO : instead of hardcoded coordinates use jail area names
 */
const JailInsideLocation = new Location( -114356, -249645, -2984 )
const JailOutsideLocation = new Location( 17836, 170178, -3507 )
/*
    TODO : convert to event listener
 */
export class JailHandler extends PunishmentHandlerBase {

    constructor() {
        super()

        ListenerCache.addGeneralListener( EventType.PlayerLogin, null, this.onPlayerLogin.bind( this ), null )
    }

    applyPunishment( player: L2PcInstance, record: PunishmentRecord ): void {
        player.setInstanceId( 0 )
        player.setIsIn7sDungeon( false )

        if ( !TvTEvent.isInactive() && TvTEvent.isPlayerParticipant( player.getObjectId() ) ) {
            TvTEvent.removeParticipant( player.getObjectId() )
        }

        if ( OlympiadManager.isRegisteredInCompetition( player ) ) {
            OlympiadManager.removeDisconnectedCompetitor( player )
        }

        setTimeout( DeferredMethods.playerTeleport, 2000, player.getObjectId(), JailInsideLocation )

        let html: string
        let path = 'data/html/jail_in.htm'
        if ( DataManager.getHtmlData().hasItem( path ) ) {
            html = DataManager.getHtmlData().getItem( path )
                    .replace( /%reason%/g, record ? record.reason : '' )
                    .replace( /%punishedBy%/g, record ? record.punishedBy : '' )
        } else {
            html = '<html><body>You have been put in jail by an admin.</body></html>'
        }

        player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId() ) )

        if ( record ) {
            let delay = Math.floor( ( record.expirationTime - Date.now() ) / 1000 )

            if ( delay > 0 ) {
                let amount: string = delay > 60 ? `${ Math.floor( delay / 60 ) } minutes` : `${ delay } seconds`
                player.sendMessage( `You've been jailed for ${ amount }.` )
            } else {
                player.sendMessage( 'You\'ve been jailed forever.' )
            }
        }
    }

    getType(): PunishmentType {
        return PunishmentType.JAIL
    }

    removePunishment( player: L2PcInstance, record: PunishmentRecord ): void {
        setTimeout( DeferredMethods.playerTeleport, 2000, player.getObjectId(), JailOutsideLocation )

        let html: string
        let path = 'data/html/jail_out.htm'
        if ( !DataManager.getHtmlData().hasItem( path ) ) {
            html = '<html><body>You are free for now, respect server rules!</body></html>'
        } else {
            html = DataManager.getHtmlData().getItem( path )
        }

        player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId() ) )
    }

    onPlayerLogin( data: PlayerLoginEvent ): void {
        let player: L2PcInstance = L2World.getPlayer( data.playerId )
        if ( !player ) {
            return
        }

        if ( player.isJailed() && !player.isInArea( AreaType.Jail ) ) {
            this.applyPunishment( player, null )
            return
        }

        if ( !player.isJailed() && player.isInArea( AreaType.Jail ) && !player.isGM() ) {
            this.removePunishment( player, null )
            return
        }
    }
}