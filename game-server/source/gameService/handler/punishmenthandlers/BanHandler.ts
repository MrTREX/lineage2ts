import { PunishmentHandlerBase } from '../IPunishmentHandler'
import { PunishmentType } from '../../models/punishment/PunishmentType'
import { PunishmentRecord } from '../../models/punishment/PunishmentRecord'
import { PunishmentEffect } from '../../models/punishment/PunishmentEffect'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2World } from '../../L2World'
import { L2GameClientRegistry } from '../../L2GameClientRegistry'
import { GameClient } from '../../GameClient'

export class BanHandler extends PunishmentHandlerBase {
    applyPunishment( player: L2PcInstance ): void {
        let client = L2GameClientRegistry.getClientByPlayerId( player.getObjectId() )
        if ( client ) {
            return client.logout()
        }

        player.unload()
    }

    getType(): PunishmentType {
        return PunishmentType.BAN
    }

    async onEnd( record: PunishmentRecord ): Promise<void> {
        return
    }

    async onStart( record: PunishmentRecord ): Promise<void> {
        switch ( record.effect ) {
            case PunishmentEffect.CHARACTER:
                let objectId = parseInt( record.targetId )
                let player: L2PcInstance = L2World.getPlayer( objectId )
                if ( player ) {
                    this.applyPunishment( player )
                }

                return

            case PunishmentEffect.ACCOUNT:
                let accountName = record.targetId
                let client: GameClient = L2GameClientRegistry.getClientByAccount( accountName )
                if ( client ) {
                    if ( client.player ) {
                        return this.applyPunishment( client.player )
                    }

                    client.abortConnection()
                }

                return

            case PunishmentEffect.IP: {
                let punishment = this
                L2GameClientRegistry.getClientByIpd( record.targetId ).forEach( ( client: GameClient ) => {
                    if ( client.player ) {
                        punishment.applyPunishment( client.player )
                    }
                } )

                return
            }
        }
    }

    removePunishment(): void {

    }
}