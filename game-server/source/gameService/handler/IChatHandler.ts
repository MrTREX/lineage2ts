import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { ChatType } from './chathandlers/ChatTypes'

export interface IChatHandler {
    applyMessage( message: string, type: number, player: L2PcInstance, target: L2PcInstance ) : Promise<void>
    getType() : ChatType
}