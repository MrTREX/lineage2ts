import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { L2Object } from '../models/L2Object'
import { InstanceType } from '../enums/InstanceType'

export interface IActionShiftHandler {
    getInstanceType(): InstanceType

    onAction( player: L2PcInstance, target: L2Object, shouldInteract: boolean ): Promise<boolean>
}