import { IBypassHandler } from '../IBypassHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Character } from '../../models/actor/L2Character'
import { L2Npc } from '../../models/actor/L2Npc'
import { CommonSkill } from '../../models/holders/SkillHolder'

export const SupportBlessing: IBypassHandler = {
    hasRangeValidation(): boolean {
        return true
    },

    getCommandNames(): Array<string> {
        return [
            'GiveBlessing',
        ]
    },

    async onStart( command: string, player: L2PcInstance, originator: L2Character ): Promise<boolean> {
        if ( !originator || !originator.isNpc() ) {
            return false
        }

        let npc = originator as L2Npc

        if ( player.getLevel() > 39 || player.getClassId() >= 2 ) {
            npc.showHtml( player, 'data/html/default/SupportBlessingHighLevel.htm' )
            return true
        }

        npc.setTarget( player )
        await npc.doCast( CommonSkill.BLESSING_OF_PROTECTION.getSkill() )
        return false
    },
}