import { IBypassHandler } from '../IBypassHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Character } from '../../models/actor/L2Character'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { PackageToList } from '../../packets/send/PackageToList'
import { ExGetDimensionalItemList } from '../../packets/send/ExGetDimensionalItemList'
import { DimensionalTransferManager } from '../../cache/DimensionalTransferManager'
import _ from 'lodash'

const enum AcceptedCommands {
    withdraw = 'DFreightWithdraw',
    deposit = 'DFreightDeposit',
}

export const DimensionalTransferFreight: IBypassHandler = {
    hasRangeValidation(): boolean {
        return true
    },

    getCommandNames(): Array<string> {
        return [
            AcceptedCommands.withdraw,
            AcceptedCommands.deposit,
        ]
    },

    async onStart( command: string, player: L2PcInstance, originator: L2Character ): Promise<boolean> {
        if ( !originator.isNpc() ) {
            return false
        }

        switch ( command ) {
            case AcceptedCommands.withdraw:
                if ( !DimensionalTransferManager.hasItems( player.getObjectId() ) ) {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THERE_ARE_NO_MORE_D_ITEMS_TO_BE_FOUND ) )
                    return false
                }

                player.sendOwnedData( ExGetDimensionalItemList( player.getObjectId() ) )
                return true

            case AcceptedCommands.deposit:
                if ( player.getAccountCharacters().size < 1 ) {
                    player.sendMessage( 'Need at least two characters on account.' )
                    return false
                }

                player.sendOwnedData( PackageToList( player ) )
                return true
        }

        return false
    },
}