import { IBypassHandler } from '../IBypassHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { VoicedCommandManager } from '../managers/VoicedCommandManager'
import { IVoicedCommand } from '../IVoicedCommand'

export const VoiceCommand: IBypassHandler = {
    hasRangeValidation(): boolean {
        return true
    },

    getCommandNames(): Array<string> {
        return [
            'voice',
        ]
    },

    async onStart( command: string, player: L2PcInstance ): Promise<boolean> {
        if ( ( command.length > 7 ) && ( command.charAt( 6 ) === '.' ) ) {
            let voiceCommand: string
            let parameters: string
            let endOfCommand = command.indexOf( ' ', 7 )
            if ( endOfCommand > 0 ) {
                voiceCommand = command.substring( 7, endOfCommand ).trim()
                parameters = command.substring( endOfCommand ).trim()
            } else {
                voiceCommand = command.substring( 7 ).trim()
            }

            if ( voiceCommand.length > 0 ) {
                let handler: IVoicedCommand = VoicedCommandManager.getHandler( voiceCommand )
                if ( handler ) {
                    await handler.onStart( voiceCommand, player, parameters )
                    return
                }
            }
        }

        return false
    },

}