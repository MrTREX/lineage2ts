import { IBypassHandler } from '../IBypassHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Character } from '../../models/actor/L2Character'
import { L2Npc } from '../../models/actor/L2Npc'
import { DimensionalRiftManager } from '../../instancemanager/DimensionalRiftManager'
import _ from 'lodash'

export const Rift: IBypassHandler = {
    hasRangeValidation(): boolean {
        return true
    },

    getCommandNames(): Array<string> {
        return [
            'enterrift',
            'changeriftroom',
            'exitrift',
        ]
    },

    async onStart( command: string, player: L2PcInstance, originator: L2Character ): Promise<boolean> {
        if ( !originator || !originator.isNpc() ) {
            return false
        }

        if ( command.toLowerCase().startsWith( 'enterrift' ) ) {
            let value = _.parseInt( command.substring( 10 ) ) // Selected Area: Recruit, Soldier etc
            await DimensionalRiftManager.start( player, value, originator as L2Npc )
            return true
        }

        let inRift = player.isInParty() && player.getParty().isInDimensionalRift()
        if ( command.toLowerCase().startsWith( 'changeriftroom' ) ) {
            if ( inRift ) {
                player.getParty().getDimensionalRift().manualTeleport( player, originator as L2Npc )
            } else {
                DimensionalRiftManager.handleCheat( player, originator as L2Npc )
            }

            return true
        }

        if ( command.toLowerCase().startsWith( 'exitrift' ) ) {
            if ( inRift ) {
                player.getParty().getDimensionalRift().manualExitRift( player, originator as L2Npc )
            } else {
                DimensionalRiftManager.handleCheat( player, originator as L2Npc )
            }
        }

        return true
    },
}