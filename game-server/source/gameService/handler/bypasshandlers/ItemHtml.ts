import { IBypassHandler } from '../IBypassHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Character } from '../../models/actor/L2Character'
import { DataManager } from '../../../data/manager'
import { NpcHtmlMessagePath } from '../../packets/send/NpcHtmlMessage'

const enum ItemHtmlCommands {
    ViewHtml = 'item-html'
}

export const ItemHtml : IBypassHandler = {
    getCommandNames(): Array<string> {
        return [
            ItemHtmlCommands.ViewHtml
        ]
    },

    hasRangeValidation(): boolean {
        return false
    },

    async onStart( command: string, player: L2PcInstance, originator: L2Character ): Promise<boolean> {
        let [ , htmlPath, idValue ] = command.split( ' ' )
        if ( !htmlPath ) {
            return false
        }

        let path = getItemViewHtmlPath( htmlPath )
        let content: string = DataManager.getHtmlData().getItem( path )
        let itemId = parseInt( idValue ) ?? 0

        player.sendOwnedData( NpcHtmlMessagePath( content, path, player.getObjectId(), 0, itemId ) )
        return true
    }
}

export function createViewItemHtmlBypass( itemPath: string ) : string {
    return `bypass -h ${ItemHtmlCommands.ViewHtml} ${itemPath}`
}

export function getItemViewHtmlPath( htmlPath: string ) : string {
    return `overrides/html/item/${ htmlPath }`
}