import { IBypassHandler } from '../IBypassHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Character } from '../../models/actor/L2Character'
import { L2Npc } from '../../models/actor/L2Npc'
import { ConfigManager } from '../../../config/ConfigManager'
import { NpcHtmlMessagePath } from '../../packets/send/NpcHtmlMessage'
import { ActionFailed } from '../../packets/send/ActionFailed'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { DataManager } from '../../../data/manager'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { LotteryValues } from '../../values/LotteryValues'
import { LotteryManager } from '../../cache/LotteryManager'
import _ from 'lodash'
import { ItemManagerCache } from '../../cache/ItemManagerCache'
import aigle from 'aigle'
import moment from 'moment'

const dateFormat = 'h:mm a [on] dddd, MMMM Do'

export const Lottery: IBypassHandler = {
    hasRangeValidation(): boolean {
        return true
    },

    getCommandNames(): Array<string> {
        return [ 'Loto' ]
    },

    async onStart( command: string, player: L2PcInstance, originator: L2Character ): Promise<boolean> {
        if ( !originator.isNpc() ) {
            return false
        }

        let value = _.parseInt( command.substring( 5 ) )
        if ( value === 0 ) {
            _.times( 5, ( index: number ) => {
                player.setLottery( index, 0 )
            } )
        }

        await showMessage( player, originator as L2Npc, value )

        return false
    },
}

function sendMessage( message: string, path: string, player: L2PcInstance, npc: L2Npc ) {
    if ( !message ) {
        return
    }

    let html: string = message.replace( /%race%/g, LotteryManager.getLotteryId().toString() )
            .replace( /%adena%/g, LotteryManager.getPrize().toString() )
            .replace( /%ticket_price%/g, ConfigManager.general.getLotteryTicketPrice().toString() )
            .replace( /%prize5%/g, ( ConfigManager.general.getLottery5NumberRate() * 100 ).toString() )
            .replace( /%prize4%/g, ( ConfigManager.general.getLottery4NumberRate() * 100 ).toString() )
            .replace( /%prize3%/g, ( ConfigManager.general.getLottery3NumberRate() * 100 ).toString() )
            .replace( /%prize2%/g, ConfigManager.general.getLottery2and1NumberPrize().toString() )
            .replace( /%enddate%/g, moment( LotteryManager.getEndDate() ).format( 'dddd, MMMM Do YYYY, h:mm:ss a Z' ) )

    player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), npc.getObjectId() ) )
    player.sendOwnedData( ActionFailed() )
}

// TODO : refactor values to use const enums
// and separate all logic into multiple functions
async function showMessage( player: L2PcInstance, npc: L2Npc, value: number ): Promise<void> {
    let npcId = npc.getTemplate().getId()

    if ( value === 0 ) {
        let htmlPath = npc.getHtmlPath( npcId, 1 )
        let html: string = DataManager.getHtmlData().getItem( htmlPath )
        return sendMessage( html, htmlPath, player, npc )
    }

    // 1-20 - buttons, 21 - second buy lottery ticket window
    if ( ( value >= 1 ) && ( value <= 21 ) ) {
        if ( !LotteryManager.hasStarted() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NO_LOTTERY_TICKETS_CURRENT_SOLD ) )
            return
        }

        if ( !LotteryManager.hasSellableTickets() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NO_LOTTERY_TICKETS_AVAILABLE ) )
            return
        }

        let count = 0
        let found = 0

        // counting buttons and unsetting button if found
        _.times( 5, ( index: number ) => {
            if ( player.getLottery( index ) === value ) {
                player.setLottery( index, 0 )
                found = 1
                return
            }

            if ( player.getLottery( index ) > 0 ) {
                count++
                return
            }
        } )

        // if not rearched limit 5 and not unseted value
        if ( count < 5 && found === 0 && value <= 20 ) {
            for ( let index = 0; index < 5; index++ ) {
                if ( player.getLottery( index ) === 0 ) {
                    player.setLottery( index, value )
                    break
                }
            }
        }

        let htmlPath = npc.getHtmlPath( npcId, 5 )
        let html: string = DataManager.getHtmlData().getItem( htmlPath )

        // setting pushed buttons
        let limit = 0
        _.times( 5, ( index: number ) => {
            if ( player.getLottery( index ) === 0 ) {
                return
            }

            limit++
            let button: string = player.getLottery( index ).toString()
            if ( player.getLottery( index ) < 10 ) {
                button = `0${ button }`
            }

            let target: string = `fore="L2UI.lottoNum${ button }" back="L2UI.lottoNum${ button }a_check"`
            let value: string = `fore="L2UI.lottoNum${ button }a_check" back="L2UI.lottoNum${ button }"`
            html = html.replace( target, value )
        } )

        if ( limit === 5 ) {
            html = html.replace( '0">Return', '22">Your lucky numbers have been selected above.' )
        }

        return sendMessage( html, htmlPath, player, npc )
    }

    // 22 - selected ticket with 5 numbers
    if ( value === 22 ) {
        if ( !LotteryManager.hasStarted() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NO_LOTTERY_TICKETS_CURRENT_SOLD ) )
            return
        }

        if ( !LotteryManager.hasSellableTickets() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NO_LOTTERY_TICKETS_AVAILABLE ) )
            return
        }

        let price = ConfigManager.general.getLotteryTicketPrice()
        let lotonumber = LotteryManager.getLotteryId()
        let enchant = 0
        let type2 = 0

        _.times( 5, ( index: number ) => {
            let lotteryValue = player.getLottery( index )
            if ( lotteryValue === 0 ) {
                return
            }

            if ( lotteryValue < 17 ) {
                enchant += Math.pow( 2, lotteryValue - 1 )
                return
            }

            type2 += Math.pow( 2, lotteryValue - 17 )
        } )

        if ( player.getAdena() < price ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ADENA ) )
            return
        }

        if ( !await player.reduceAdena( price, true, 'Lottery.showMessage' ) ) {
            return
        }

        await LotteryManager.increasePrize( price )

        let earnedMessage = new SystemMessageBuilder( SystemMessageIds.EARNED_ITEM_S1 )
                .addItemNameWithId( LotteryValues.lotteryItemId )
                .getBuffer()
        player.sendOwnedData( earnedMessage )

        let item: L2ItemInstance = ItemManagerCache.createItem( LotteryValues.lotteryItemId, 'Lottery.showMessage', 1, player.getObjectId() )

        item.setCount( 1 )
        item.setCustomType1( lotonumber )
        await item.setEnchantLevel( enchant )
        item.setCustomType2( type2 )

        await player.getInventory().addExistingItem( item, 'Lottery.showMessage' )

        let endDate: string = moment( LotteryManager.getEndDate() ).format( dateFormat )
        let htmlPath = npc.getHtmlPath( npcId, 6 )
        let html: string = DataManager.getHtmlData().getItem( htmlPath )
                .replace( 'The winners will be announced at 7:00 pm on Saturday', `The winners will be announced at ${ endDate }` )

        return sendMessage( html, htmlPath, player, npc )
    }

    // 23 - current lottery jackpot
    if ( value === 23 ) {
        let htmlPath = npc.getHtmlPath( npcId, 3 )
        let html: string = DataManager.getHtmlData().getItem( htmlPath )
        return sendMessage( html, htmlPath, player, npc )
    }

    // 24 - Previous winning numbers/Prize claim
    if ( value === 24 ) {
        let lotteryId = LotteryManager.getLotteryId()
        let messages: Array<string> = await aigle.resolve( player.getInventory().getItems() ).mapLimit( 10, async ( item: L2ItemInstance ): Promise<string> => {
            if ( !item || !( item.getId() === LotteryValues.lotteryItemId && item.getCustomType1() < lotteryId ) ) {
                return ''
            }

            let message: string = `<a action="bypass -h Loto ${ item.getObjectId() }">${ item.getCustomType1() } Event Number `
            let numbers: Array<number> = LotteryManager.decodeNumbers( item.getEnchantLevel(), item.getCustomType2() )
            message += _.join( numbers, ' ' )

            let check: Array<number> = await LotteryManager.checkTicket( item )
            if ( check[ 0 ] > 0 ) {
                switch ( check[ 0 ] ) {
                    case 1:
                        message += '- 1st Prize'
                        break

                    case 2:
                        message += '- 2nd Prize'
                        break

                    case 3:
                        message += '- 3th Prize'
                        break

                    case 4:
                        message += '- 4th Prize'
                        break
                }

                message += ` ${ check[ 1 ] }a.`
            }

            return `${ message }</a><br>`
        } )

        if ( messages.length === 0 ) {
            messages.push( 'There has been no winning lottery ticket.<br>' )
        }

        let htmlPath = npc.getHtmlPath( npcId, 4 )
        let html: string = DataManager.getHtmlData().getItem( htmlPath )
                .replace( /%result%/g, messages.join( '' ) )
        return sendMessage( html, htmlPath, player, npc )
    }

    // 25 - lottery instructions
    if ( value === 25 ) {
        let htmlPath = npc.getHtmlPath( npcId, 2 )
        let html: string = DataManager.getHtmlData().getItem( htmlPath )

        if ( ConfigManager.general.getLotteryPeriodDays() > 0 ) {
            let isHourly = ConfigManager.general.getLotteryPeriodDays() < 1

            let basis = isHourly ? 'hourly' : `every ${ Math.floor( ConfigManager.general.getLotteryPeriodDays() ) } day`
            let startTime = moment( LotteryManager.getEndDate() )
            let endDate = moment( LotteryManager.getEndDate() ).subtract( 10, 'minutes' ).format( dateFormat )
            let announceDate = moment( LotteryManager.getEndDate() ).format( dateFormat )

            if ( isHourly ) {
                let hours: number = Math.ceil( ConfigManager.general.getLotteryPeriodDays() * 24 )
                startTime.subtract( hours, 'hours' )
            } else {
                startTime.subtract( Math.floor( ConfigManager.general.getLotteryPeriodDays() ), 'days' )
            }

            let startDate = startTime.add( 10, 'minutes' ).format( dateFormat )

            html = html.replace(
                    'Lottery Tickets are sold on a weekly basis, starting at 7:10 pm every Saturday until 6:50 pm the following Saturday.',
                    `Lottery Tickets are sold on an ${ basis } basis, starting at ${ startDate } until ${ endDate }.` )
                    .replace( 'The winning numbers are announced at 7:00 pm every Saturday.', `The winning numbers will be announced at ${ announceDate }.` )
        }

        return sendMessage( html, htmlPath, player, npc )
    }

    // >25 - check lottery ticket by item object id
    if ( value > 25 ) {
        let lotteryId: number = LotteryManager.getLotteryId()
        let item: L2ItemInstance = player.getInventory().getItemByObjectId( value )
        if ( !item
                || item.getId() !== LotteryValues.lotteryItemId
                || item.getCustomType1() >= lotteryId ) {
            return
        }

        let message = new SystemMessageBuilder( SystemMessageIds.S1_DISAPPEARED )
                .addItemNameWithId( LotteryValues.lotteryItemId )
                .getBuffer()
        player.sendOwnedData( message )

        let check: Array<number> = await LotteryManager.checkTicket( item )
        let adena = check[ 1 ]
        if ( adena > 0 ) {
            await player.addAdena( adena, 'Lottery.showMessage', npc.getObjectId(), true )
        }

        await player.destroyItem( item, false, 'Lottery.showMessage' )
        return
    }
}