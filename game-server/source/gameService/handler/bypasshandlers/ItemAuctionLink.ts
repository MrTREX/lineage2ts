import { IBypassHandler } from '../IBypassHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Character } from '../../models/actor/L2Character'
import { ConfigManager } from '../../../config/ConfigManager'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { ItemAuctionManager } from '../../cache/ItemAuctionManager'
import { NpcItemAuction } from '../../models/auction/NpcItemAuction'
import { ItemAuction } from '../../models/auction/ItemAuction'
import { ExItemAuctionInfoPacket } from '../../packets/send/ExItemAuctionInfoPacket'
import { GeneralHelper } from '../../helpers/GeneralHelper'
import { ItemAuctionState } from '../../models/auction/ItemAuctionState'
import moment from 'moment'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 2, // time-to-live value of token bucket (in seconds)
} )


const commandPrefix: string = 'ItemAuction'
export const ItemAuctionLink: IBypassHandler = {
    hasRangeValidation(): boolean {
        return true
    },

    getCommandNames(): Array<string> {
        return [
            commandPrefix,
        ]
    },

    async onStart( command: string, player: L2PcInstance, originator: L2Character ): Promise<boolean> {
        if ( !originator.isNpc() ) {
            return false
        }

        if ( !ConfigManager.general.itemAuctionEnabled() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NO_AUCTION_PERIOD ) )
            return true
        }

        let auction: NpcItemAuction = ItemAuctionManager.getAuction( originator.getId() )
        if ( !auction ) {
            return false
        }

        let action: string = command.replace( `${ commandPrefix } `, '' )
        switch ( action ) {
            case 'show':
                if ( !packetLimiter.consumeSync( player.getObjectId().toString() ) ) {
                    return
                }

                let currentAuction: ItemAuction = auction.getCurrentAuction()
                let nextAuction: ItemAuction = auction.getNextAuction()

                if ( !currentAuction ) {
                    Helper.showNoAuctionMessage( player, nextAuction )
                    return true
                }

                if ( currentAuction.getAuctionState() === ItemAuctionState.CREATED ) {
                    Helper.showNoAuctionMessage( player, currentAuction )
                    return true
                }

                player.sendOwnedData( ExItemAuctionInfoPacket( false, currentAuction, nextAuction ) )
                return true

            case 'cancel':
                let item: ItemAuction = auction.getActiveAuctionByBidder( player.getObjectId() )
                if ( !item || !( await item.cancelBid( player ) ) ) {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NO_OFFERINGS_OWN_OR_MADE_BID_FOR ) )
                }

                return true
        }

        return false
    },
}

const Helper = {
    showNoAuctionMessage( player: L2PcInstance, auction: ItemAuction ): void {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NO_AUCTION_PERIOD ) )

        if ( !auction ) {
            return
        }

        let futureTime = moment( auction.getStartingTime() )
        let date: string = futureTime.format( 'dddd, MMMM Do YYYY, h:mm a Z' )
        let seconds: number = futureTime.diff( moment(), 'seconds' )

        player.sendMessage( `The next auction will begin on the ${ date }, or approximately in ${ GeneralHelper.formatSeconds( seconds, true ).join( ' ' ) }` )
    },
}