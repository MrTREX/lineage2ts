import { IBypassHandler } from '../IBypassHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Character } from '../../models/actor/L2Character'
import { L2Npc } from '../../models/actor/L2Npc'
import { EventType, NpcApproachedForTalkEvent } from '../../models/events/EventType'
import { ListenerCache } from '../../cache/ListenerCache'
import { EventPoolCache } from '../../cache/EventPoolCache'
import _ from 'lodash'

export const ChatLink: IBypassHandler = {
    hasRangeValidation(): boolean {
        return true
    },

    getCommandNames(): Array<string> {
        return [
            'Chat',
        ]
    },

    async onStart( command: string, player: L2PcInstance, originator: L2Character ): Promise<boolean> {
        if ( !originator || !originator.isNpc() ) {
            return false
        }

        let value = _.parseInt( command.substring( 5 ) )

        let npc: L2Npc = originator as L2Npc
        if ( !value ) {
            if ( ListenerCache.hasNpcTemplateListeners( npc.getId(), EventType.NpcApproachedForTalk ) ) {
                let data = EventPoolCache.getData( EventType.NpcApproachedForTalk ) as NpcApproachedForTalkEvent

                data.characterId = npc.getObjectId()
                data.playerId = player.getObjectId()
                data.characterNpcId = npc.getId()


                await ListenerCache.sendNpcTemplateEvent( npc.getId(), EventType.NpcApproachedForTalk, data )
                return false
            }

            npc.showChatWindowDefault( player )
            return false
        }

        npc.showChatWindow( player, value )

        return false
    },
}