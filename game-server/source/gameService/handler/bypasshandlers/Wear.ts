import { IBypassHandler } from '../IBypassHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Character } from '../../models/actor/L2Character'
import { ConfigManager } from '../../../config/ConfigManager'
import { L2BuyList } from '../../models/buylist/L2BuyList'
import { ActionFailed } from '../../packets/send/ActionFailed'
import { BuyListManager } from '../../cache/BuyListManager'
import { ShopPreviewList } from '../../packets/send/ShopPreviewList'
import _ from 'lodash'

export const Wear: IBypassHandler = {
    hasRangeValidation(): boolean {
        return true
    },

    getCommandNames(): Array<string> {
        return [
            'Wear',
        ]
    },

    async onStart( command: string, player: L2PcInstance, originator: L2Character ): Promise<boolean> {
        if ( !originator.isNpc() ) {
            return false
        }

        if ( !ConfigManager.general.allowWear() ) {
            return false
        }

        let commandChunks = _.split( command, ' ' )
        if ( commandChunks.length < 2 ) {
            return false
        }

        let buyList: L2BuyList = BuyListManager.getBuyList( _.parseInt( commandChunks[ 1 ] ) )
        if ( !buyList ) {
            player.sendOwnedData( ActionFailed() )
            return
        }

        player.setInventoryBlockingStatus( true )
        player.sendOwnedData( ShopPreviewList( buyList, player.getAdena(), player.getExpertiseLevel() ) )
        return true
    },
}