import { IBypassHandler } from '../IBypassHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { NpcHtmlMessagePath } from '../../packets/send/NpcHtmlMessage'
import { DataManager } from '../../../data/manager'
import _ from 'lodash'

export const PlayerHelp: IBypassHandler = {
    hasRangeValidation(): boolean {
        return true
    },

    getCommandNames(): Array<string> {
        return [
            'player_help',
        ]
    },

    async onStart( commandLine: string, player: L2PcInstance ): Promise<boolean> {
        if ( commandLine.length < 13 ) {
            return false
        }

        let path = commandLine.substring( 12 )
        if ( path.indexOf( '..' ) !== -1 ) {
            return false
        }

        let commandChunks: Array<string> = _.split( path, ' ' )
        let commands: Array<string> = commandChunks[ 0 ].split( '#' )

        let [ name, itemId ] = commands

        let finalPath = `data/html/help/${ name }`
        let html = DataManager.getHtmlData().getItem( finalPath )
        player.sendOwnedData( NpcHtmlMessagePath( html, finalPath, player.getObjectId(), 0, _.defaultTo( parseInt( itemId ), 0 ) ) )
        return true
    },
}