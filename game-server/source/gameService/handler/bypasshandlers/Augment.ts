import { IBypassHandler } from '../IBypassHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Character } from '../../models/actor/L2Character'
import { ExShowVariationCancelWindow } from '../../packets/send/ExShowVariationCancelWindow'
import { ExShowVariationMakeWindow } from '../../packets/send/ExShowVariationMakeWindow'
import _ from 'lodash'

export const Augment: IBypassHandler = {
    hasRangeValidation(): boolean {
        return true
    },

    getCommandNames(): Array<string> {
        return [
            'Augment',
        ]
    },

    async onStart( command: string, player: L2PcInstance, originator: L2Character ): Promise<boolean> {
        if ( !originator || !originator.isNpc() ) {
            return false
        }

        let value = _.parseInt( command.substring( 8, 9 ).trim() )
        switch ( value ) {
            case 1:
                player.sendOwnedData( ExShowVariationMakeWindow() )
                return true
            case 2:
                player.sendOwnedData( ExShowVariationCancelWindow() )
                return true
        }

        return false
    },
}