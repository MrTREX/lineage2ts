import { IBypassHandler } from '../IBypassHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Character } from '../../models/actor/L2Character'
import { SkillHolder } from '../../models/holders/SkillHolder'
import { L2Npc } from '../../models/actor/L2Npc'
import { ClassId } from '../../models/base/ClassId'
import { CategoryType } from '../../enums/CategoryType'
import aigle from 'aigle'

const LOWEST_LEVEL = 6
const HIGHEST_LEVEL = 75
const CUBIC_LOWEST = 16
const CUBIC_HIGHEST = 34
const HASTE_LEVEL_2 = 40

const FIGHTER_BUFFS: Array<SkillHolder> = [
    new SkillHolder( 4322, 1 ), // Wind Walk
    new SkillHolder( 4323, 1 ), // Shield
    new SkillHolder( 5637, 1 ), // Magic Barrier
    new SkillHolder( 4324, 1 ), // Bless the Body
    new SkillHolder( 4325, 1 ), // Vampiric Rage
    new SkillHolder( 4326, 1 ), // Regeneration
]
const MAGE_BUFFS: Array<SkillHolder> = [
    new SkillHolder( 4322, 1 ), // Wind Walk
    new SkillHolder( 4323, 1 ), // Shield
    new SkillHolder( 5637, 1 ), // Magic Barrier
    new SkillHolder( 4328, 1 ), // Bless the Soul
    new SkillHolder( 4329, 1 ), // Acumen
    new SkillHolder( 4330, 1 ), // Concentration
    new SkillHolder( 4331, 1 ), // Empower
]
const SUMMON_BUFFS: Array<SkillHolder> = [
    new SkillHolder( 4322, 1 ), // Wind Walk
    new SkillHolder( 4323, 1 ), // Shield
    new SkillHolder( 5637, 1 ), // Magic Barrier
    new SkillHolder( 4324, 1 ), // Bless the Body
    new SkillHolder( 4325, 1 ), // Vampiric Rage
    new SkillHolder( 4326, 1 ), // Regeneration
    new SkillHolder( 4328, 1 ), // Bless the Soul
    new SkillHolder( 4329, 1 ), // Acumen
    new SkillHolder( 4330, 1 ), // Concentration
    new SkillHolder( 4331, 1 ), // Empower
]

const HASTE_1 = new SkillHolder( 4327, 1 )
const HASTE_2 = new SkillHolder( 5632, 1 )
const CUBIC = new SkillHolder( 4338, 1 )

export const SupportMagic: IBypassHandler = {
    hasRangeValidation(): boolean {
        return true
    },

    getCommandNames(): Array<string> {
        return [
            'supportmagicservitor',
            'supportmagic',
        ]
    },

    async onStart( command: string, player: L2PcInstance, originator: L2Character ): Promise<boolean> {
        if ( !originator.isNpc() || player.isCursedWeaponEquipped() ) {
            return false
        }

        switch ( command.toLowerCase() ) {
            case 'supportmagicservitor':
                await castSupportMagic( player, originator as L2Npc, true )
                break

            case 'supportmagic':
                await castSupportMagic( player, originator as L2Npc, false )
                break
        }

        return true
    },
}

async function castSupportMagic( player: L2PcInstance, npc: L2Npc, isSummon: boolean ): Promise<void> {
    let level = player.getLevel()
    let castSkill: Function = ( skill: SkillHolder ): Promise<void> => {
        return npc.doCast( skill.getSkill() )
    }

    if ( isSummon && !player.hasServitor() ) {
        npc.showHtml( player, 'data/html/default/SupportMagicNoSummon.htm' )
        return
    }

    if ( level > HIGHEST_LEVEL ) {
        npc.showHtml( player, 'data/html/default/SupportMagicHighLevel.htm' )
        return
    }

    if ( level < LOWEST_LEVEL ) {
        npc.showHtml( player, 'data/html/default/SupportMagicLowLevel.htm' )
        return
    }

    if ( ClassId.getClassIdByIdentifier( player.getClassId() ).level === 3 ) {
        player.sendMessage( 'Only adventurers who have not completed their 3rd class transfer may receive these buffs.' )
        return
    }

    if ( isSummon ) {
        npc.setTarget( player.getSummon() )

        await aigle.resolve( SUMMON_BUFFS ).each( ( skill: SkillHolder ) => castSkill( skill ) )

        if ( level >= HASTE_LEVEL_2 ) {
            await npc.doCast( HASTE_2.getSkill() )
        } else {
            await npc.doCast( HASTE_1.getSkill() )
        }

        return
    }

    npc.setTarget( player )
    if ( player.isInCategory( CategoryType.BEGINNER_MAGE ) ) {
        await aigle.resolve( MAGE_BUFFS ).each( ( skill: SkillHolder ) => castSkill( skill ) )

    } else {
        await aigle.resolve( FIGHTER_BUFFS ).each( ( skill: SkillHolder ) => castSkill( skill ) )

        if ( level >= HASTE_LEVEL_2 ) {
            await npc.doCast( HASTE_2.getSkill() )
        } else {
            await npc.doCast( HASTE_1.getSkill() )
        }
    }

    if ( ( level >= CUBIC_LOWEST ) && ( level <= CUBIC_HIGHEST ) ) {
        return player.doSimultaneousCast( CUBIC.getSkill() )
    }
}