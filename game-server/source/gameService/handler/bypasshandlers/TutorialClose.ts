import { IBypassHandler } from '../IBypassHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { TutorialCloseHtml } from '../../packets/send/TutorialCloseHtml'

export const TutorialClose: IBypassHandler = {
    hasRangeValidation(): boolean {
        return true
    },

    getCommandNames(): Array<string> {
        return [
            'tutorial_close'
        ]
    },

    async onStart( command: string, player: L2PcInstance ): Promise<boolean> {
        player.sendOwnedData( TutorialCloseHtml( player.getObjectId() ) )
        return false
    }
}