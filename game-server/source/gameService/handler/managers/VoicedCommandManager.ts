import { IVoicedCommand } from '../IVoicedCommand'
import { AutoLoot } from '../voicecommands/AutoLoot'
import { L2DataApi } from '../../../data/interface/l2DataApi'
import { GoldBarBanking } from '../voicecommands/GoldBarBanking'
import { CastleOperations } from '../voicecommands/CastleOperations'
import { ChangePassword } from '../voicecommands/ChangePassword'
import { ChatBan } from '../voicecommands/ChatBan'
import { TownFinder } from '../voicecommands/TownFinder'
import { ViewNpc } from '../voicecommands/ViewNpc'
import _ from 'lodash'
import { AccountPrivileges } from '../voicecommands/AccountPrivileges'
import { ServerLog } from '../../../logger/Logger'

const allHandlers: Array<IVoicedCommand> = [
    AutoLoot,
    GoldBarBanking,
    CastleOperations,
    ChangePassword,
    ChatBan,
    TownFinder,
    ViewNpc,
    AccountPrivileges
]

type VoiceCommandRegistry = Record<string, IVoicedCommand>

class Manager implements L2DataApi {
    registry: VoiceCommandRegistry = {}

    getHandler( name: string ): IVoicedCommand {
        return this.registry[ name ]
    }

    async load(): Promise<Array<string>> {
        allHandlers.forEach( this.registerCommand.bind( this ) )

        return [
            `VoicedCommandManager: loaded ${ _.size( this.registry ) } default commands.`,
        ]
    }

    registerCommand( command: IVoicedCommand ): void {
        this.registry = command.getCommandNames().reduce( ( registry: VoiceCommandRegistry, name: string ) => {
            if ( registry[ name ] ) {
                ServerLog.error( 'VoicedCommandManager: redefining non-unique command', name )
            }

            registry[ name ] = command

            return registry
        }, this.registry )
    }

    unRegisterCommand( name: string ) : void {
        delete this.registry[ name ]
    }
}

export const VoicedCommandManager = new Manager()