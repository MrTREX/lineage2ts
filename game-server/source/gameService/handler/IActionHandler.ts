import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { L2Object } from '../models/L2Object'
import { InstanceType } from '../enums/InstanceType'

export interface IActionHandler {
    getInstanceType(): InstanceType
    performActions( player: L2PcInstance, target: L2Object, shouldInteract: boolean ): Promise<void>
    canInteract( player: L2PcInstance, target: L2Object ) : boolean
    isReadyToInteract( player: L2PcInstance, target: L2Object ) : boolean
}