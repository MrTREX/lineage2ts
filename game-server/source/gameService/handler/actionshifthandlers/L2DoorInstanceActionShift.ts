import { IActionShiftHandler } from '../IActionShiftHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Object } from '../../models/L2Object'
import { InstanceType } from '../../enums/InstanceType'
import { L2DoorInstance } from '../../models/actor/instance/L2DoorInstance'
import { StaticDoorObject } from '../../packets/send/StaticObject'
import { NpcHtmlMessagePath } from '../../packets/send/NpcHtmlMessage'
import { DataManager } from '../../../data/manager'

export const L2DoorInstanceActionShift : IActionShiftHandler = {
    getInstanceType(): InstanceType {
        return InstanceType.L2DoorInstance
    },

    async onAction( player: L2PcInstance, target: L2Object ): Promise<boolean> {
        if ( player.isGM() ) {
            player.setTarget( target )

            let door : L2DoorInstance = target as L2DoorInstance
            player.sendOwnedData( StaticDoorObject( door, player.isGM() ) )


            let path = 'data/html/admin/doorinfo.htm'
            let html : string = DataManager.getHtmlData().getItem( path )
                    .replace( /%class%/, InstanceType[ target.getInstanceType() ] )
                    .replace( /%hp%/, Math.floor( door.getCurrentHp() ).toString() )
                    .replace( /%hpmax%/, door.getMaxHp().toString() )
                    .replace( /%objid%/, target.getObjectId().toString() )
                    .replace( /%doorid%/, door.getId().toString() )
                    .replace( /%minx%/, door.getNodeX( 0 ).toString() )
                    .replace( /%miny%/, door.getNodeY( 0 ).toString() )
                    .replace( /%minz%/, door.getZMin().toString() )
                    .replace( /%maxx%/, door.getNodeX( 2 ).toString() )
                    .replace( /%maxy%/, door.getNodeY( 2 ).toString() )
                    .replace( /%maxz%/, door.getZMax().toString() )
                    .replace( /%unlock%/,
                            door.isOpenableBySkill() ? '<font color=00FF00>YES<font>' : '<font color=FF0000>NO</font>' )

            player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId() ) )
        }

        return true
    }
}