import { IActionShiftHandler } from '../IActionShiftHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Object } from '../../models/L2Object'
import { InstanceType } from '../../enums/InstanceType'
import { StaticObjectInstance } from '../../packets/send/StaticObject'
import { NpcHtmlMessagePath } from '../../packets/send/NpcHtmlMessage'
import { L2StaticObjectInstance } from '../../models/actor/instance/L2StaticObjectInstance'
import { DataManager } from '../../../data/manager'


export const L2StaticObjectInstanceActionShift : IActionShiftHandler = {
    getInstanceType(): InstanceType {
        return InstanceType.L2StaticObjectInstance
    },

    async onAction( player: L2PcInstance, target: L2Object ): Promise<boolean> {
        if ( player.isGM() ) {
            player.setTarget( target )
            player.sendOwnedData( StaticObjectInstance( target as L2StaticObjectInstance ) )

            let path = 'html/actions/staticobjectactionshift.htm'
            let html : string = DataManager.getHtmlData().getItem( 'html/actions/staticobjectactionshift.htm' )
                    .replace( /%x%/, target.getX().toString() )
                    .replace( /%y%/, target.getY().toString() )
                    .replace( /%z%/, target.getZ().toString() )
                    .replace( /%objectId%/, target.getObjectId().toString() )

                    .replace( /%targetId%/, target.getId().toString() )
                    .replace( /%meshIndex%/, ( target as L2StaticObjectInstance ).getMeshIndex().toString() )
                    .replace( /%class%/, InstanceType[ target.getInstanceType() ] )

            player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId() ) )
        }

        return true
    }

}