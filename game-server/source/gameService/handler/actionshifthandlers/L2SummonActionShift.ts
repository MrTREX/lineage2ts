import { IActionShiftHandler } from '../IActionShiftHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Object } from '../../models/L2Object'
import { InstanceType } from '../../enums/InstanceType'
import { IAdminCommand } from '../IAdminCommand'
import { AdminCommandManager } from '../managers/AdminCommandManager'

export const L2SummonActionShift : IActionShiftHandler = {
    getInstanceType(): InstanceType {
        return InstanceType.L2Summon
    },

    async onAction( player: L2PcInstance, target: L2Object, shouldInteract: boolean ): Promise<boolean> {
        if ( player.isGM() ) {
            let playerTarget = player.getTarget()
            if ( !playerTarget || playerTarget.getObjectId() !== target.getObjectId() ) {
                player.setTarget( target )
            }

            let handler : IAdminCommand = AdminCommandManager.getHandler( 'admin_summon_info' )
            if ( handler ) {
                await handler.onCommand( 'admin_summon_info', player )
            }
        }

        return true
    }

}