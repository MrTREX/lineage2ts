import { IChatHandler } from '../IChatHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { PetitionManager } from '../../instancemanager/PetitionManager'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { ChatType } from './ChatTypes'

function applyMessage( message: string, type: number, player: L2PcInstance ): Promise<void> {
    if ( !PetitionManager.isPlayerInConsultation( player ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ARE_NOT_IN_PETITION_CHAT ) )
        return
    }

    PetitionManager.sendActivePetitionMessage( player, message )
}

export const TypePetition : IChatHandler = {
    applyMessage,
    getType(): ChatType {
        return ChatType.PetitionPlayer
    }
}

export const TypePetitionGM : IChatHandler = {
    applyMessage,
    getType(): ChatType {
        return ChatType.PetitionGM
    }
}