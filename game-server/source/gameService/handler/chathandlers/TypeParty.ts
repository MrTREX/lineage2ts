import { IChatHandler } from '../IChatHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { CreatureSay } from '../../packets/send/builder/CreatureSay'
import { ChatType } from './ChatTypes'

export const TypeParty : IChatHandler = {
    async applyMessage( message: string, type: number, player: L2PcInstance ): Promise<void> {
        if ( !player.isInParty() ) {
            return
        }

        let packet : Buffer = CreatureSay.fromText( 0, player.getObjectId(), type, player.getName(), message )
        return player.getParty().broadcastBlockedDataToPartyMembers( packet, player )
    },

    getType(): ChatType {
        return ChatType.Party
    }
}