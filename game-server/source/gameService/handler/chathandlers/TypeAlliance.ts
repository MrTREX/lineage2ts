import { IChatHandler } from '../IChatHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { CreatureSay } from '../../packets/send/builder/CreatureSay'
import { ChatType } from './ChatTypes'

export const TypeAlliance : IChatHandler = {
    applyMessage( message: string, type: number, player: L2PcInstance ): Promise<void> {
        if ( !player.getClan() ) {
            return
        }

        player.getClan().broadcastDataToOnlineAllyMembers( CreatureSay.fromText( 0, player.getObjectId(), type, player.getName(), message ) )
    },

    getType(): ChatType {
        return ChatType.Alliance
    }
}