import { IChatHandler } from '../IChatHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { IVoicedCommand } from '../IVoicedCommand'
import { VoicedCommandManager } from '../managers/VoicedCommandManager'
import { ConfigManager } from '../../../config/ConfigManager'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { CreatureSay } from '../../packets/send/builder/CreatureSay'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { L2World } from '../../L2World'
import { ListenerCache } from '../../cache/ListenerCache'
import { EventType, VoiceCommandEvent } from '../../models/events/EventType'
import { EventPoolCache } from '../../cache/EventPoolCache'
import { BlocklistCache } from '../../cache/BlocklistCache'
import _ from 'lodash'
import { ChatType } from './ChatTypes'

export const TypeAll: IChatHandler = {
    async applyMessage( message: string, type: number, player: L2PcInstance ): Promise<void> {
        if ( message.startsWith( '.' ) ) {
            let isVoiceCommand: boolean = await isVoicedHandlerActivated( message, player )

            if ( !isVoiceCommand ) {
                return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_SYNTAX ) )
            }

            return
        }

        if ( player.isChatBanned() && ConfigManager.general.getBanChatChannels().has( type ) ) {
            return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CHATTING_IS_CURRENTLY_PROHIBITED ) )
        }

        let players: Array<L2PcInstance> = L2World.getVisiblePlayers( player, ConfigManager.range.getChatNormalRange(), false )
        let packet: Buffer = CreatureSay.fromText( 0, player.getObjectId(), type, player.getAppearance().getVisibleName(), message )

        player.sendCopyData( packet )

        if ( players.length > 0 ) {
            players.forEach( ( otherPlayer: L2PcInstance ) => {
                if ( !BlocklistCache.isBlocked( otherPlayer.getObjectId(), player.getObjectId() ) ) {
                    otherPlayer.sendCopyData( packet )
                }
            } )
        }
    },

    getType(): ChatType {
        return ChatType.All
    },
}

async function isVoicedHandlerActivated( text: string, player: L2PcInstance ): Promise<boolean> {
    let [ command, parameters ] = getHandlerParameters( text )
    let handler: IVoicedCommand = VoicedCommandManager.getHandler( command )

    let outcome: boolean = false

    if ( handler && handler.isEnabled() ) {
        await handler.onStart( command, player, parameters )
        outcome = true
    }

    if ( ListenerCache.hasGeneralListener( EventType.VoiceCommand ) ) {
        let data = EventPoolCache.getData( EventType.VoiceCommand ) as VoiceCommandEvent

        data.playerId = player.getObjectId()
        data.command = command
        data.parameters = parameters

        await ListenerCache.sendGeneralEvent( EventType.VoiceCommand, data )

        outcome = true
    }

    return outcome
}

function getHandlerParameters( text: string ): [ string, string ] {
    let chunks: Array<string> = _.split( text, ' ' )

    if ( chunks.length > 1 ) {
        let command = chunks[ 1 ].substring( 1 )
        return [ command, text.substring( command.length + 2 ) ]
    }

    return [ text.substring( 1 ), null ]
}