import { IChatHandler } from '../IChatHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { CreatureSay } from '../../packets/send/builder/CreatureSay'
import { ConfigManager } from '../../../config/ConfigManager'
import { PlayerActionOverride } from '../../values/PlayerConditions'
import { L2World } from '../../L2World'
import { BlocklistCache } from '../../cache/BlocklistCache'
import _ from 'lodash'
import { ChatType } from './ChatTypes'

export const TypeTrade: IChatHandler = {
    applyMessage( message: string, type: number, player: L2PcInstance ): Promise<void> {
        let packet: Buffer = CreatureSay.fromText( 0, player.getObjectId(), type, player.getName(), message )
        let globalType = ConfigManager.general.getTradeChat()

        if ( globalType === 'ON' || ( globalType === 'GM' && player.hasActionOverride( PlayerActionOverride.ChatAccess ) ) ) {
            L2World.getVisiblePlayers( player, ConfigManager.range.getChatTradeRange(), true ).forEach( ( otherPlayer: L2PcInstance ) => {
                if ( !BlocklistCache.isBlocked( otherPlayer.getObjectId(), player.getObjectId() ) ) {
                    otherPlayer.sendCopyData( packet )
                }
            } )

            return
        }

        if ( globalType === 'GLOBAL' ) {
            // TODO : rate protection here

            _.each( L2World.getAllPlayers(), ( otherPlayer: L2PcInstance ) => {
                if ( !BlocklistCache.isBlocked( otherPlayer.getObjectId(), player.getObjectId() ) ) {
                    otherPlayer.sendCopyData( packet )
                }
            } )
        }
    },

    getType(): ChatType {
        return ChatType.Trade
    },
}