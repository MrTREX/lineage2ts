import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { EffectFlag } from '../../enums/EffectFlag'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2Character } from '../../models/actor/L2Character'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import _ from 'lodash'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'
import { AIIntent } from '../../aicontroller/enums/AIIntent'

export class ConsumeChameleonRest extends AbstractEffect {
    power: number
    effectFlags = EffectFlag.SILENT_MOVE | EffectFlag.RELAXING
    effectType = L2EffectType.Relaxing

    constructor( parameters: Object ) {
        super()
        this.power = parseFloat( _.get( parameters, 'power', '0' ) )
        this.ticks = _.parseInt( _.get( parameters, 'ticks', '0' ) )
    }

    async onTick( info: BuffInfo ) : Promise<boolean> {
        let target : L2Character = info.getAffected()
        if ( target.isDead() ) {
            return false
        }

        if ( target.isPlayer() ) {
            if ( !target.getActingPlayer().isSitting() ) {
                return false
            }
        }

        let damage = this.power * this.getTicksMultiplier()
        if ( ( damage < 0 ) && ( ( target.getCurrentMp() + damage ) <= 0 ) ) {
            if ( target.isPlayable() ) {
                target.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SKILL_REMOVED_DUE_LACK_MP ) )
            }

            return false
        }

        target.setCurrentMp( Math.min( target.getCurrentMp() + damage, target.getMaxRecoverableMp() ) )
        return true
    }

    async onStart( info: BuffInfo ) {
        let character = info.getAffected()
        if ( character.isPlayer() ) {
            await character.getActingPlayer().sitDown( false )
            return
        }

        return AIEffectHelper.setNextIntent( character, AIIntent.REST )
    }
}