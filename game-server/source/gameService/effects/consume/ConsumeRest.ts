import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { EffectFlag } from '../../enums/EffectFlag'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import _ from 'lodash'

export class ConsumeRest extends AbstractEffect {
    power: number
    effectFlags = EffectFlag.RELAXING
    effectType = L2EffectType.Relaxing

    constructor( parameters: Object ) {
        super()
        this.power = parseFloat( _.get( parameters, 'power', '0' ) )
        this.ticks = _.parseInt( _.get( parameters, 'ticks', '0' ) )
    }

    async onTick( info: BuffInfo ): Promise<boolean> {
        let target = info.getAffected()
        if ( target.isDead() ) {
            return false
        }

        if ( target.isPlayer() && !target.getActingPlayer().isSitting() ) {
            return false
        }

        if ( ( target.getCurrentHp() + 1 ) > target.getMaxRecoverableHp() ) {
            target.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SKILL_DEACTIVATED_HP_FULL ) )
            return false
        }

        let amount = this.power * this.getTicksMultiplier()
        let currentMp = target.getCurrentMp()
        if ( ( amount < 0 ) && ( ( currentMp + amount ) <= 0 ) ) {
            target.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SKILL_REMOVED_DUE_LACK_MP ) )
            return false
        }

        target.setCurrentMp( Math.min( currentMp + amount, target.getMaxRecoverableMp() ) )
        return true
    }
}