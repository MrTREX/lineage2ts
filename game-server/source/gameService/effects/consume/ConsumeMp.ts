import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import _ from 'lodash'

export class ConsumeMp extends AbstractEffect {
    power: number

    constructor( parameters: Object ) {
        super()
        this.power = _.get( parameters, 'power', 0 )
        this.ticks = _.get( parameters, 'ticks', 0 )
    }

    async onTick( info: BuffInfo ) : Promise<boolean> {
        let target = info.getAffected()
        if ( target.isDead() ) {
            return false
        }

        /*
            Computed amount is expected to be negative. However,
         */
        let amount = this.power * this.getTicksMultiplier()
        if ( !amount || amount > 0 ) {
            return true
        }

        let finalMp = target.getCurrentMp() + amount
        if ( finalMp <= 0 && target.isPlayable() ) {
            target.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SKILL_REMOVED_DUE_LACK_MP ) )
            return false
        }

        target.setCurrentMp( Math.min( finalMp, target.getMaxRecoverableMp() ) )
        return true
    }
}