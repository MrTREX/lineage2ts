import { AbstractEffect } from '../../models/effects/AbstractEffect'
import _ from 'lodash'
import { EffectCalculationType } from '../../enums/EffectCalculationType'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2Character } from '../../models/actor/L2Character'

export class TickHpFatal extends AbstractEffect {
    power: number
    mode: EffectCalculationType
    effectType = L2EffectType.DamageOverTime

    constructor( parameters: Object ) {
        super()
        this.power = parseFloat( _.get( parameters, 'power', '0' ) )
        this.mode = EffectCalculationType[ _.get( parameters, 'mode', 'DIFF' ) as string ]
        this.ticks = _.get( parameters, 'ticks', 1 )
    }

    async onTick( info: BuffInfo ): Promise<boolean> {
        let target: L2Character = info.getAffected()
        if ( target.isDead() ) {
            return false
        }

        let damage = 0
        switch ( this.mode ) {
            case EffectCalculationType.DIFF: {
                damage = this.power * this.getTicksMultiplier()
                break
            }
            case EffectCalculationType.PER: {
                damage = target.getCurrentHp() * this.power * this.getTicksMultiplier()
                break
            }
        }

        await target.reduceCurrentHpByDOT( damage, info.getEffector(), info.getSkill() )
        target.notifyDamageReceived( damage, info.getEffector(), info.getSkill(), false, true, false )

        return false
    }
}