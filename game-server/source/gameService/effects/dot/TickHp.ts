import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { EffectCalculationType } from '../../enums/EffectCalculationType'
import _ from 'lodash'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2Character } from '../../models/actor/L2Character'
import { AbnormalType } from '../../models/skills/AbnormalType'
import { ExRegenMax } from '../../packets/send/ExRegenMax'

export class TickHp extends AbstractEffect {
    power: number
    mode: EffectCalculationType
    effectType = L2EffectType.DamageOverTime

    constructor( parameters: Object ) {
        super()
        this.power = parseFloat( _.get( parameters, 'power', '0' ) )
        this.mode = EffectCalculationType[ _.get( parameters, 'mode', 'DIFF' ) as string ]
        this.ticks = _.get( parameters, 'ticks', 1 )
    }

    canStart( info: BuffInfo ): boolean {
        return info.getAffected().isPlayer() && ( this.getTicks() > 0 ) && info.getSkill().getAbnormalType() === AbnormalType.HP_RECOVER
    }

    async onTick( info: BuffInfo ) : Promise<boolean> {
        let target: L2Character = info.getAffected()
        if ( target.isDead() ) {
            return false
        }

        let power = 0
        let hp = target.getCurrentHp()

        switch ( this.mode ) {
            case EffectCalculationType.DIFF: {
                power = this.power * this.getTicksMultiplier()
                break
            }
            case EffectCalculationType.PER: {
                power = hp * this.power * this.getTicksMultiplier()
                break
            }
        }

        if ( power < 0 ) {
            power = Math.abs( power )
            if ( power >= ( target.getCurrentHp() - 1 ) ) {
                power = target.getCurrentHp() - 1
            }

            await target.reduceCurrentHpByDOT( power, info.getEffector(), info.getSkill() )
            target.notifyDamageReceived( power, info.getEffector(), info.getSkill(), false, true, false )
        } else {
            let maxHp = target.getMaxRecoverableHp()

            if ( hp > maxHp ) {
                return true
            }

            target.setCurrentHp( Math.min( hp + power, maxHp ) )
        }

        return false
    }

    async onStart( info: BuffInfo ) {
        info.getAffected().sendOwnedData( ExRegenMax( info.getDuration() / 1000, this.getTicks(), this.power ) )
    }
}