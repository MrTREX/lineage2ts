import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2Attackable } from '../../models/actor/L2Attackable'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'
import { AIIntent } from '../../aicontroller/enums/AIIntent'

export class Passive extends AbstractEffect {

    canStart( info: BuffInfo ): boolean {
        return info.getAffected().isAttackable()
    }

    async onExit( buff: BuffInfo ) {
        let target = buff.getAffected()
        target.enableAllSkills()
        target.setIsImmobilized( false )
    }

    async onStart( info: BuffInfo ) {
        let target: L2Attackable = info.getAffected() as L2Attackable

        await AIEffectHelper.setNextIntent( target, AIIntent.WAITING )
        target.disableAllSkills()
        target.setIsImmobilized( true )
    }
}