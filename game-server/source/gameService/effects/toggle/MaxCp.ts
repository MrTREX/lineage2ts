import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { EffectCalculationType } from '../../enums/EffectCalculationType'
import _ from 'lodash'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { FunctionAdd } from '../../models/stats/functions/operations/FunctionAdd'
import { Stats } from '../../models/stats/Stats'
import { FunctionMultiply } from '../../models/stats/functions/operations/FunctionMultiply'
import { FunctionTemplateValues } from '../../models/stats/functions/FunctionTemplate'
import { AbstractFunction } from '../../models/stats/functions/AbstractFunction'
import { L2Character } from '../../models/actor/L2Character'

export class MaxCp extends AbstractEffect {
    power: number
    type: EffectCalculationType
    heal: boolean

    functionAdd: AbstractFunction
    functionMultiply: AbstractFunction

    constructor( parameters: Object ) {
        super()
        this.type = EffectCalculationType[ _.get( parameters, 'flyRadius', 'DIFF' ) as string ]
        this.power = parseFloat( _.get( parameters, 'power', '0' ) )
        this.heal = _.get( parameters, 'heal', false )

        if ( this.type !== EffectCalculationType.DIFF ) {
            this.power = 1 + this.power / 100
        }

        let values: FunctionTemplateValues = {
            order: 1,
            stat: Stats.MAX_CP,
            value: this.power,
        }

        this.functionAdd = new FunctionAdd( values, this )
        this.functionMultiply = new FunctionMultiply( values, this )
    }

    async onExit( info: BuffInfo ) {
        return info.getAffected().removeStatsOwner( this )
    }

    async onStart( info: BuffInfo ) {
        let affected : L2Character = info.getAffected()
        let currentCp = affected.getCurrentCp()
        let amount = this.power

        switch ( this.type ) {
            case EffectCalculationType.DIFF:
                affected.addSingleStatFunction( this.functionAdd )

                if ( this.heal ) {
                    affected.setCurrentCp( ( currentCp + this.power ) )
                }

                break

            case EffectCalculationType.PER:
                let maxCp = affected.getMaxCp()
                affected.addSingleStatFunction( this.functionMultiply )

                if ( this.heal ) {
                    amount = ( this.power - 1 ) * maxCp
                    affected.setCurrentCp( currentCp + amount )
                }

                break
        }

        if ( this.heal && affected.isPlayable() ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.S1_CP_HAS_BEEN_RESTORED )
                    .addNumber( amount )
                    .getBuffer()
            affected.sendOwnedData( packet )
        }
    }
}