import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { EffectFlag } from '../../enums/EffectFlag'
import { BuffInfo } from '../../models/skills/BuffInfo'

export class Disarm extends AbstractEffect {
    effectFlags = EffectFlag.DISARMED

    canStart( info: BuffInfo ): boolean {
        return info.getAffected().isPlayer()
    }

    async onStart( info: BuffInfo ) {
        await info.getAffected().getActingPlayer().disarmWeapons()
    }
}