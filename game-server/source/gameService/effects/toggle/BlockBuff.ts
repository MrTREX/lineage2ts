import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { EffectFlag } from '../../enums/EffectFlag'

export class BlockBuff extends AbstractEffect {
    effectFlags = EffectFlag.BLOCK_BUFF
}