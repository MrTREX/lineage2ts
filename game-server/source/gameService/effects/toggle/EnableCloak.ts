import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'

export class EnableCloak extends AbstractEffect {
    canStart( info: BuffInfo ): boolean {
        let target = info.getAffected()
        return info.getEffector() && target && target.isPlayer()
    }

    async onTick( info: BuffInfo ) : Promise<boolean> {
        return info.getSkill().isPassive()
    }

    async onStart( info: BuffInfo ) {
        info.getAffected().getActingPlayer().getStat().setCloakSlotStatus( true )
    }

    async onExit( info: BuffInfo ) {
        info.getAffected().getActingPlayer().getStat().setCloakSlotStatus( false )
    }
}