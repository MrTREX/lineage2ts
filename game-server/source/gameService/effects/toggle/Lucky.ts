import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'

export class Lucky extends AbstractEffect {
    canStart( info: BuffInfo ): boolean {
        let target = info.getAffected()
        return info.getEffector() && target && target.isPlayer()
    }

    async onTick( info: BuffInfo ) : Promise<boolean> {
        return info.getSkill().isPassive()
    }
}