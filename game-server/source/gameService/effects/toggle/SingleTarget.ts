import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { EffectFlag } from '../../enums/EffectFlag'

export class SingleTarget extends AbstractEffect {
    effectFlags = EffectFlag.SINGLE_TARGET
}