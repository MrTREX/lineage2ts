import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { EffectFlag } from '../../enums/EffectFlag'

export class BlockDebuff extends AbstractEffect {
    effectFlags = EffectFlag.BLOCK_DEBUFF
}