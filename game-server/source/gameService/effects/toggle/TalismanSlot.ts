import { AbstractEffect } from '../../models/effects/AbstractEffect'
import _ from 'lodash'
import { BuffInfo } from '../../models/skills/BuffInfo'

export class TalismanSlot extends AbstractEffect {
    slots: number

    constructor( parameters: Object ) {
        super()
        this.slots = _.parseInt( _.get( parameters, 'slots', '0' ) )
    }

    canStart( info: BuffInfo ): boolean {
        let target = info.getAffected()
        return info.getEffector() && target && target.isPlayer()
    }

    async onTick( info: BuffInfo ) : Promise<boolean> {
        return info.getSkill().isPassive()
    }

    async onStart( info: BuffInfo ) {
        info.getAffected().getActingPlayer().getStat().addTalismanSlots( this.slots )
    }

    async onExit( info: BuffInfo ) {
        info.getAffected().getActingPlayer().getStat().addTalismanSlots( -this.slots )
    }
}