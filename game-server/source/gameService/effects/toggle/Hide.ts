import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Character } from '../../models/actor/L2Character'
import { L2World } from '../../L2World'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'
import { AIIntent } from '../../aicontroller/enums/AIIntent'

export class Hide extends AbstractEffect {

    canStart( info: BuffInfo ): boolean {
        return info.getAffected().isPlayer()
    }

    async onExit( buff: BuffInfo ) {
        let activeChar: L2PcInstance = buff.getAffected().getActingPlayer()
        if ( !activeChar.inObserverMode() ) {
            activeChar.setInvisible( false )
        }
    }

    async onStart( info: BuffInfo ) {
        let character: L2PcInstance = info.getAffected().getActingPlayer()
        character.setInvisible( true )

        if ( character.hasAIController() && character.getAIController().isIntent( AIIntent.ATTACK ) ) {
            await AIEffectHelper.setNextIntent( character, AIIntent.WAITING )
        }

        L2World.getVisibleCharacters( character, character.getBroadcastRadius(), true ).forEach( ( otherCharacter: L2Character ) => {
            if ( otherCharacter && otherCharacter.getTargetId() === character.getObjectId() ) {
                otherCharacter.setTarget( null )
                AIEffectHelper.notifyTargetDead( otherCharacter, character )
            }
        } )
    }
}