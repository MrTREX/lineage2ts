import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { EffectFlag } from '../../enums/EffectFlag'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { Race } from '../../enums/Race'
import { InstanceType } from '../../enums/InstanceType'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'
import { AIIntent } from '../../aicontroller/enums/AIIntent'
import { ConfigManager } from '../../../config/ConfigManager'

export class Fear extends AbstractEffect {
    effectFlags = EffectFlag.FEAR
    effectType = L2EffectType.Fear

    getTicks(): number {
        return 5
    }

    async onTick( info: BuffInfo ) : Promise<boolean> {
        AIEffectHelper.scheduleNextIntent( info.getAffected(), AIIntent.ATTACK )
        return false
    }

    canStart( info: BuffInfo ): boolean {
        let target = info.getAffected()
        return target.isPlayer()
                || target.isSummon()
                || ( target.isAttackable()
                        && !( target.isInstanceType( InstanceType.L2DefenderInstance )
                        || target.isInstanceType( InstanceType.L2FortCommanderInstance )
                        || target.isInstanceType( InstanceType.L2SiegeFlagInstance )
                        || target.getTemplate().getRace() === Race.SIEGE_WEAPON ) )
    }

    async onStart( info: BuffInfo ) {
        let target = info.getAffected()
        if ( target.isCasting() && target.canAbortCast() ) {
            target.abortCast()
        }

        // TODO : replace ticks with common time calculation
        return AIEffectHelper.notifyFlee( target, this.getTicks() * ConfigManager.character.getEffectTickRatio() )
    }
}