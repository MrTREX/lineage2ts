import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { EffectFlag } from '../../enums/EffectFlag'
import { BuffInfo } from '../../models/skills/BuffInfo'

export class ProtectionBlessing extends AbstractEffect {
    effectFlags = EffectFlag.PROTECTION_BLESSING

    canStart( info: BuffInfo ): boolean {
        let target = info.getAffected()
        return info.getEffector() && target && target.isPlayer()
    }
}