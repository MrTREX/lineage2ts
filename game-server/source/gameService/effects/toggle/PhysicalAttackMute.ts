import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { EffectFlag } from '../../enums/EffectFlag'
import { BuffInfo } from '../../models/skills/BuffInfo'

export class PhysicalAttackMute extends AbstractEffect {
    effectFlags = EffectFlag.PSYCHICAL_ATTACK_MUTED

    async onStart( info: BuffInfo ) {
        info.getAffected().startPhysicalAttackMuted()
    }
}