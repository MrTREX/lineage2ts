import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { PunishmentManager } from '../../instancemanager/PunishmentManager'
import { PunishmentType } from '../../models/punishment/PunishmentType'
import { PunishmentEffect } from '../../models/punishment/PunishmentEffect'

export class BlockChat extends AbstractEffect {
    effectType = L2EffectType.ChatBlock

    async onExit( info: BuffInfo ) {
        return PunishmentManager.stopPunishment( info.getAffectedId(), PunishmentEffect.CHARACTER, PunishmentType.ChatBlocked )
    }

    async onStart( info: BuffInfo ) {
        return PunishmentManager.startPunishment( info.getAffectedId().toString(), PunishmentEffect.CHARACTER, PunishmentType.ChatBlocked, 0, 'Chat banned bot report', 'BlockChat', true )
    }
}