import { AbstractEffect } from '../../models/effects/AbstractEffect'
import _ from 'lodash'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { BuffInfo } from '../../models/skills/BuffInfo'

export class CrystalGradeModify extends AbstractEffect {
    grade: number

    constructor( parameters: Object ) {
        super()
        this.grade = _.parseInt( _.get( parameters, 'grade', '0' ) )
    }

    canStart( info: BuffInfo ): boolean {
        return info.getAffected().isPlayer()
    }

    async onExit( info: BuffInfo ) {
        let player: L2PcInstance = info.getAffected() as L2PcInstance
        player.setExpertisePenaltyBonus( 0 )
        player.refreshExpertisePenalty()
    }

    async onStart( info: BuffInfo ) {
        let player: L2PcInstance = info.getAffected() as L2PcInstance
        player.setExpertisePenaltyBonus( this.grade )
        player.refreshExpertisePenalty()
    }
}