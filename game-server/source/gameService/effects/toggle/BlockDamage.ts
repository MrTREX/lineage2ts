import { AbstractEffect } from '../../models/effects/AbstractEffect'
import _ from 'lodash'
import { EffectFlag } from '../../enums/EffectFlag'

enum BlockType {
    HP,
    MP
}

export class BlockDamage extends AbstractEffect {
    type: BlockType

    constructor( parameters: Object ) {
        super()

        let type = BlockType[ _.get( parameters, 'type', 'HP' ) as string ]
        this.effectFlags = type === BlockType.HP ? EffectFlag.BLOCK_HP : EffectFlag.BLOCK_MP
    }
}