import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { SkillHolder } from '../../models/holders/SkillHolder'
import { L2TargetType } from '../../models/skills/targets/L2TargetType'
import { InstanceType } from '../../enums/InstanceType'
import _ from 'lodash'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { CharacterReceivedDamageEvent, EventType } from '../../models/events/EventType'
import { EventListenerData, EventListenerMethod } from '../../models/events/listeners/EventListenerData'
import { Skill } from '../../models/Skill'
import { L2Object } from '../../models/L2Object'
import { L2Character } from '../../models/actor/L2Character'
import { ListenerCache, ListenerPredicate } from '../../cache/ListenerCache'
import { L2World } from '../../L2World'

export class TriggerSkillByDamage extends AbstractEffect {
    minLevel: number
    maxLevel: number
    minDamage: number
    chance: number
    skill: SkillHolder
    targetType: L2TargetType
    attackerType: InstanceType
    removeListenerMethod: ListenerPredicate
    onDamageReceivedMethod: EventListenerMethod

    constructor( parameters: Object ) {
        super()
        this.minLevel = _.parseInt( _.get( parameters, 'minAttackerLevel', '1 ' ) )
        this.maxLevel = _.parseInt( _.get( parameters, 'maxAttackerLevel', '100' ) )
        this.minDamage = _.parseInt( _.get( parameters, 'minDamage', '1' ) )
        this.chance = parseFloat( _.get( parameters, 'chance', '100' ) )

        let skillId = _.parseInt( _.get( parameters, 'skillId', '1' ) )
        let skillLevel = _.parseInt( _.get( parameters, 'skillLevel', '1' ) )

        this.skill = new SkillHolder( skillId, skillLevel )
        this.targetType = L2TargetType[ _.get( parameters, 'targetType', 'SELF' ) as string ]
        this.attackerType = InstanceType[ _.get( parameters, 'attackerType', 'L2Character' ) as string ]

        this.removeListenerMethod = this.removeListenerPredicate.bind( this )
        this.onDamageReceivedMethod = this.onDamageReceived.bind( this )
    }

    canStart(): boolean {
        if ( ( this.chance === 0 ) || ( this.skill.getLevel() === 0 ) ) {
            return false
        }

        return _.random( 100 ) <= this.chance
    }

    async onDamageReceived( eventData: CharacterReceivedDamageEvent ) : Promise<void> {
        if ( eventData.isDOT || eventData.attackerId === eventData.targetId ) {
            return
        }

        let attacker: L2Character = L2World.getObjectById( eventData.attackerId ) as L2Character
        let target: L2Character = L2World.getObjectById( eventData.targetId ) as L2Character
        if ( ( ( this.targetType === L2TargetType.SELF ) && ( this.skill.getSkill().getCastRange() > 0 ) )
                && ( attacker.calculateDistance( target, true ) > this.skill.getSkill().getCastRange() ) ) {
            return
        }

        if ( attacker.getLevel() < this.minLevel || attacker.getLevel() > this.maxLevel ) {
            return
        }

        if ( eventData.damage < this.minDamage || !attacker.isInstanceType( this.attackerType ) ) {
            return
        }

        let triggerSkill: Skill = this.skill.getSkill()
        let triggerCasts: Array<Promise<void>> = triggerSkill
                .getTargetWithSelectedTarget( attacker, target )
                .reduce( ( promises: Array<Promise<void>>, target: L2Object ) => {
                    if ( target && target.isCharacter() && !( target as L2Character ).isInvulnerable() ) {
                        promises.push( attacker.makeTriggerCast( triggerSkill, ( target as L2Character ) ) )
                    }

                    return promises
                }, [] )

        if ( triggerCasts.length > 0 ) {
            await Promise.all( triggerCasts )
        }
    }

    async onExit( info: BuffInfo ) {
        ListenerCache.removeObjectListener( info.getAffectedId(), EventType.CharacterReceivedDamage, this.removeListenerMethod )
    }

    async onStart( info: BuffInfo ) {
        ListenerCache.addObjectListener( info.getAffectedId(), EventType.CharacterReceivedDamage, this, this.onDamageReceivedMethod )
    }

    removeListenerPredicate( data: EventListenerData ): boolean {
        return data.owner === this
    }
}