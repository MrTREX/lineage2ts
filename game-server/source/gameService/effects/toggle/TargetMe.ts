import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Playable } from '../../models/actor/L2Playable'

export class TargetMe extends AbstractEffect {

    canStart( info: BuffInfo ): boolean {
        return info.getAffected().isPlayable()
    }

    async onStart( info: BuffInfo ) {
        let attacker = info.getEffector()
        let target = info.getAffected()

        if ( target.getTargetId() !== attacker.getObjectId() ) {
            let player : L2PcInstance = attacker.getActingPlayer()
            if ( !player || player.checkPvpSkill( target, info.getSkill() ) ) {
                target.setTarget( attacker )
            }
        }

        ( target as L2Playable ).setLockedTarget( attacker )
    }
}