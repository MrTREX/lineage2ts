import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { TeleportWhereType } from '../../enums/TeleportWhereType'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { ILocational } from '../../models/Location'
import _ from 'lodash'
import { getTeleportLocation } from '../../helpers/TeleportHelper'

export class Escape extends AbstractEffect {
    escapeType: TeleportWhereType
    effectType = L2EffectType.Teleport

    constructor( parameters: Object ) {
        super()
        this.escapeType = TeleportWhereType[ _.get( parameters, 'escapeType', 'TOWN' ) as string ]
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        let target = info.getAffected()
        let location: ILocational = getTeleportLocation( target, this.escapeType )

        await target.teleportToLocation( location, true )
        target.getActingPlayer().setIsIn7sDungeon( false )
        target.setInstanceId( 0 )
    }
}