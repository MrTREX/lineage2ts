import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { TerritoryWarManager } from '../../instancemanager/TerritoryWarManager'
import { L2SiegeFlagInstance } from '../../models/actor/instance/L2SiegeFlagInstance'
import { DataManager } from '../../../data/manager'

const flagNpcId = 35062

export class TakeTerritoryFlag extends AbstractEffect {

    canStart( info: BuffInfo ): boolean {
        let attacker = info.getEffector()
        return attacker.isPlayer() && attacker.getActingPlayer().isClanLeader() && TerritoryWarManager.isTWInProgress
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        let player = info.getEffector().getActingPlayer()
        let flag : L2SiegeFlagInstance = new L2SiegeFlagInstance( player, DataManager.getNpcData().getTemplate( flagNpcId ), false, false )

        flag.setTitle( player.getClan().getName() )
        flag.setMaxStats()
        flag.setHeading( player.getHeading() )
        flag.spawnMe( player.getX(), player.getY(), player.getZ() + 50 )

        TerritoryWarManager.addClanFlag( player.getClan(), flag )
    }
}