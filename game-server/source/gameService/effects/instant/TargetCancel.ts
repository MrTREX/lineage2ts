import { AbstractEffect } from '../../models/effects/AbstractEffect'
import _ from 'lodash'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { Formulas } from '../../models/stats/Formulas'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'

export class TargetCancel extends AbstractEffect {
    chance: number

    constructor( parameters: Object ) {
        super()

        this.chance = _.parseInt( _.get( parameters, 'chance', '100' ) )
    }

    isInstant(): boolean {
        return true
    }

    calculateSuccess( info: BuffInfo ): boolean {
        return Formulas.calculateProbability( this.chance, info.getEffector(), info.getAffected(), info.getSkill() )
    }

    async onStart( info: BuffInfo ) {
        let target = info.getAffected()

        target.setTarget( null )

        // TODO : should aggro also be removed?
        return AIEffectHelper.notifyTargetDead( target, null )
    }
}