import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { FortManager } from '../../instancemanager/FortManager'
import { Fort } from '../../models/entity/Fort'
import { L2Clan } from '../../models/L2Clan'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'

export class TakeFortStart extends AbstractEffect {

    canStart( info: BuffInfo ): boolean {
        return info.getEffector().isPlayer()
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        let player: L2PcInstance = info.getEffector().getActingPlayer()
        let fort: Fort = FortManager.getFort( player )
        let clan: L2Clan = player.getClan()

        if ( fort && clan ) {
            let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.S1_TRYING_RAISE_FLAG )
                    .addString( clan.getName() )
                    .getBuffer()
            fort.getSiege().announceDataToPlayers( packet )
        }
    }
}