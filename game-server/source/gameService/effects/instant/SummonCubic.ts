import { AbstractEffect } from '../../models/effects/AbstractEffect'
import _ from 'lodash'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2CubicInstance } from '../../models/actor/instance/L2CubicInstance'
import { Skill } from '../../models/Skill'
import { DataManager } from '../../../data/manager'
import { SkillCache } from '../../cache/SkillCache'
import { L2CubicDataSkill } from '../../../data/interface/CubicDataApi'

const skillChancesProperties = [ 'cubicSkill1Chance','cubicSkill2Chance','cubicSkill3Chance' ]
const defaultChances = []

export class SummonCubic extends AbstractEffect {
    cubicId: number
    power: number
    duration: number
    delay: number
    maxCount: number
    skillChance: number
    useChances: Array<number> = defaultChances

    constructor( parameters: Object ) {
        super()

        this.cubicId = _.parseInt( _.get( parameters, 'cubicId', '-1' ) )
        this.power = _.parseInt( _.get( parameters, 'cubicPower', '0' ) )
        this.duration = _.parseInt( _.get( parameters, 'cubicDuration', '0' ) )
        this.delay = _.parseInt( _.get( parameters, 'cubicDelay', '0' ) )
        this.maxCount = _.parseInt( _.get( parameters, 'cubicMaxCount', '-1' ) )
        this.skillChance = _.parseInt( _.get( parameters, 'cubicSkillChance', '0' ) )

        if ( parameters[ skillChancesProperties[ 0 ] ] ) {
            this.useChances = _.compact( skillChancesProperties.map( ( propertyName: string ) : number => parameters[ propertyName ] ) )
        }
    }

    isInstant(): boolean {
        return true
    }

    canStart( info: BuffInfo ): boolean {
        let target = info.getAffected()
        if ( !target || !target.isPlayer() || target.isAlikeDead() || this.cubicId < 0 ) {
            return false
        }

        let player: L2PcInstance = target.getActingPlayer()
        return !( player.inObserverMode() || player.isMounted() )
    }

    async onStart( info: BuffInfo ) {
        let player: L2PcInstance = info.getAffected().getActingPlayer()
        player.removeCubic( this.cubicId )

        let allowedCubicCount = player.getStat().getMaxCubicCount()
        let currentCubicCount = player.getCubics().size

        if ( allowedCubicCount <= currentCubicCount ) {
            _.sampleSize( Array.from( player.getCubics().keys() ), currentCubicCount - allowedCubicCount + 1 ).forEach( ( id: number ) => {
                player.removeCubic( id )
            } )
        }

        /*
            TODO : Instead relying on effect parameters use cubic data directly.
            - consider that such effect needs only cubicId and level, instead of using bound skill level
         */
        let skills : Array<Skill> = SummonCubic.getCubicSkills( this.cubicId, info.getSkill().getLevel() )
        let newCubic: L2CubicInstance = new L2CubicInstance(
                player,
                this.cubicId,
                skills,
                this.power,
                this.delay,
                this.skillChance,
                this.maxCount,
                this.duration,
                this.useChances,
                info.getAffectedId() !== info.getEffectorId() )

        player.addCubic( newCubic )
        player.broadcastUserInfo()
    }

    static getCubicSkills( id: number, level: number ) : Array<Skill> {
        let item = DataManager.getCubics().getItem( id, level )

        if ( !item || !item.skills ) {
            return []
        }

        return item.skills.reduce( ( allSkills: Array<Skill>, item: L2CubicDataSkill ) : Array<Skill> => {
            let skill = SkillCache.getSkill( item.id, item.level )

            if ( skill ) {
                allSkills.push( skill )
            }

            return allSkills
        }, [] )
    }
}