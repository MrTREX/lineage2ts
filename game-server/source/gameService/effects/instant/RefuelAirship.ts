import { AbstractEffect } from '../../models/effects/AbstractEffect'
import _ from 'lodash'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2AirShipInstance } from '../../models/actor/instance/L2AirShipInstance'

export class RefuelAirship extends AbstractEffect {
    value: number
    effectType = L2EffectType.RefuelAirship

    constructor( parameters: Object ) {
        super()

        this.value = parseFloat( _.get( parameters, 'value', '0' ) )
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        let ship : L2AirShipInstance = info.getEffector().getActingPlayer().getAirShip()

        ship.setFuel( ship.getFuel() + this.value )
        ship.onUpdateAbnormalEffect()
    }
}