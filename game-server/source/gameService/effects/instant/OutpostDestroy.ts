import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { TerritoryWarManager } from '../../instancemanager/TerritoryWarManager'
import { L2SiegeFlagInstance } from '../../models/actor/instance/L2SiegeFlagInstance'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export class OutpostDestroy extends AbstractEffect {
    canStart( info: BuffInfo ): boolean {
        let player = info.getEffector()
        return player.isPlayer()
                && player.getActingPlayer().isClanLeader()
                && TerritoryWarManager.isTWInProgress
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        let player : L2PcInstance = info.getEffector().getActingPlayer()
        let flag : L2SiegeFlagInstance = TerritoryWarManager.getSiegeFlagForClan( player.getClan() )
        if ( flag ) {
            await flag.deleteMe()
        }

        TerritoryWarManager.setSiegeFlagForClan( player.getClan(), null )
    }
}