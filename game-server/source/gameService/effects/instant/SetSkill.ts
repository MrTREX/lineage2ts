import { AbstractEffect } from '../../models/effects/AbstractEffect'
import _ from 'lodash'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2Character } from '../../models/actor/L2Character'
import { Skill } from '../../models/Skill'
import { SkillCache } from '../../cache/SkillCache'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export class SetSkill extends AbstractEffect {
    id: number
    level: number

    constructor( parameters: Object ) {
        super()

        this.id = _.parseInt( _.get( parameters, 'skillId', '0' ) )
        this.level = _.parseInt( _.get( parameters, 'skillLvl', '1' ) )
    }

    canStart( info: BuffInfo ): boolean {
        let target: L2Character = info.getAffected()
        return target && target.isPlayer()
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        let skill: Skill = SkillCache.getSkill( this.id, this.level )
        if ( !skill ) {
            return
        }

        let target: L2PcInstance = info.getAffected() as L2PcInstance
        await target.addSkill( skill, true )
        target.sendSkillList()
    }
}