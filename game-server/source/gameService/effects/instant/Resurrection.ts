import { AbstractEffect } from '../../models/effects/AbstractEffect'
import _ from 'lodash'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2Character } from '../../models/actor/L2Character'
import { Formulas } from '../../models/stats/Formulas'
import { DecayTaskManager } from '../../taskmanager/DecayTaskManager'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export class Resurrection extends AbstractEffect {
    power: number
    effectType = L2EffectType.Resurrection

    constructor( parameters: Object ) {
        super()

        this.power = parseFloat( _.get( parameters, 'power', '0' ) )
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        let target: L2Character = info.getAffected()
        let attacker: L2Character = info.getEffector()

        if ( attacker.isPlayer() && target.isPlayable() ) {
            target.getActingPlayer().reviveRequest( attacker as L2PcInstance, target.isPet(), this.power, 0 )
            return
        }

        DecayTaskManager.cancel( target )
        target.doReviveWithPercentage( Formulas.calculateSkillResurrectRestorePercent( this.power, attacker ) )
    }
}