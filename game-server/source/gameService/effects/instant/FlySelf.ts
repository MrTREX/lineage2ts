import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2Character } from '../../models/actor/L2Character'
import { Location } from '../../models/Location'
import { FlyToLocation, FlyType } from '../../packets/send/FlyToLocation'
import { ValidateLocation } from '../../packets/send/ValidateLocation'

import _ from 'lodash'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { PathFinding } from '../../../geodata/PathFinding'

export class FlySelf extends AbstractEffect {
    radius: number

    constructor( parameters: Object ) {
        super()

        this.radius = _.parseInt( _.get( parameters, 'flyRadius', '0' ) )
    }

    isInstant(): boolean {
        return true
    }

    canStart( info: BuffInfo ): boolean {
        return !info.getAffected().isMovementDisabled()
    }

    async onStart( info: BuffInfo ) {
        let target: L2Character = info.getAffected()
        let attacker: L2Character = info.getEffector()

        let curX = attacker.getX()
        let curY = attacker.getY()
        let curZ = attacker.getZ()

        let dx = target.getX() - curX
        let dy = target.getY() - curY
        let dz = target.getZ() - curZ
        let distance = Math.hypot( dx, dy )
        if ( distance > 2000 ) {
            return
        }

        let offset = Math.max( distance - this.radius, 30 )


        offset -= Math.abs( dz )
        if ( offset < 5 ) {
            offset = 5
        }

        if ( ( distance < 1 ) || ( ( distance - offset ) <= 0 ) ) {
            return
        }

        let sin = dy / distance
        let cos = dx / distance

        let x = curX + Math.round( ( distance - offset ) * cos )
        let y = curY + Math.round( ( distance - offset ) * sin )
        let z = target.getZ()

        let destination: Location = PathFinding.getApproximateDestination( attacker, x, y, z )

        BroadcastHelper.dataToSelfBasedOnVisibility( attacker, FlyToLocation( attacker, destination, FlyType.Charge ) )

        attacker.setXYZLocation( destination )
        BroadcastHelper.dataToSelfBasedOnVisibility( attacker, ValidateLocation( attacker ) )
    }
}