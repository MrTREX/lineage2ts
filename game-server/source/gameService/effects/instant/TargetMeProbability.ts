import { AbstractEffect } from '../../models/effects/AbstractEffect'
import _ from 'lodash'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { Formulas } from '../../models/stats/Formulas'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export class TargetMeProbability extends AbstractEffect {
    chance: number

    constructor( parameters: Object ) {
        super()

        this.chance = _.parseInt( _.get( parameters, 'chance', '100' ) )
    }

    isInstant(): boolean {
        return true
    }

    calculateSuccess( info: BuffInfo ): boolean {
        return Formulas.calculateProbability( this.chance, info.getEffector(), info.getAffected(), info.getSkill() )
    }

    canStart( info: BuffInfo ): boolean {
        let target = info.getAffected()
        return target.isPlayable() && target.getTarget().getObjectId() !== info.getEffectorId()
    }

    async onStart( info: BuffInfo ) {
        let attacker = info.getEffector()
        let attackerPlayer : L2PcInstance = attacker.getActingPlayer()
        let target = info.getAffected()

        if ( attackerPlayer || attackerPlayer.checkPvpSkill( target, info.getSkill() ) ) {
            target.setTarget( attacker )
        }
    }
}