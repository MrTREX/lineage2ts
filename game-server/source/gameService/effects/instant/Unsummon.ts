import { AbstractEffect } from '../../models/effects/AbstractEffect'
import _ from 'lodash'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { Formulas } from '../../models/stats/Formulas'
import { L2Summon } from '../../models/actor/L2Summon'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'

export class Unsummon extends AbstractEffect {
    chance: number

    constructor( parameters: Object ) {
        super()

        this.chance = _.parseInt( _.get( parameters, 'chance', '100' ) )
    }

    calculateSuccess( info: BuffInfo ): boolean {
        let magicLevel = info.getSkill().getMagicLevel()
        let target = info.getAffected()
        if ( ( magicLevel <= 0 ) || ( ( target.getLevel() - 9 ) <= magicLevel ) ) {
            let attacker = info.getEffector()
            let chance = this.chance
                    * Formulas.calculateAttributeBonus( attacker, target, info.getSkill() )
                    * Formulas.calculateGeneralTraitBonus( attacker, target, info.getSkill().getTraitType(), false )
            if ( chance > _.random( 100 ) ) {
                return true
            }
        }

        return false
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        let summon: L2Summon = info.getAffected().getSummon()
        if ( summon ) {
            let summonOwner: L2PcInstance = summon.getOwner()

            await summon.stopAllEffects()
            await summon.unSummon( summonOwner )
            summonOwner.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOUR_SERVITOR_HAS_VANISHED ) )
        }
    }
}