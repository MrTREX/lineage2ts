import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { Formulas } from '../../models/stats/Formulas'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { L2MonsterInstance } from '../../models/actor/instance/L2MonsterInstance'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'
import { PacketDispatcher } from '../../PacketDispatcher'

export class Spoil extends AbstractEffect {

    /*
        TODO : revisit level check for spoiler character to avoid farming
        Example:
        - spoiler uses Spoil to mark mob as spoiled
        - another character (in order to help spoiler, high level) kills spoiled mob
        - spoiler receives items
     */
    calculateSuccess( info: BuffInfo ): boolean {
        return Formulas.calculateMagicSuccess( info.getEffector(), info.getAffected(), info.getSkill() )
    }

    canStart( info: BuffInfo ): boolean {
        let monster = info.getAffected() as L2MonsterInstance
        if ( !monster.isMonster() || monster.isDead() ) {
            PacketDispatcher.sendOwnedData( info.getEffectorId(), SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
            return false
        }

        if ( monster.isSpoiled() ) {
            PacketDispatcher.sendOwnedData( info.getEffectorId(), SystemMessageBuilder.fromMessageId( SystemMessageIds.ALREADY_SPOILED ) )
            return false
        }

        return !!info.getEffector().getActingPlayer()
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        let target: L2MonsterInstance = info.getAffected() as L2MonsterInstance
        let player = info.getEffector().getActingPlayer()
        target.setSpoilerObjectId( player.objectId )
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SPOIL_SUCCESS ) )

        AIEffectHelper.notifyAttacked( target, player )
    }
}