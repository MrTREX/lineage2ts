import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { EffectCalculationType } from '../../enums/EffectCalculationType'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import _ from 'lodash'

export class Cp extends AbstractEffect {
    amount: number
    mode: EffectCalculationType
    effectType = L2EffectType.Cp

    constructor( parameters: Object ) {
        super()
        this.amount = parseFloat( _.get( parameters, 'amount', '0' ) )
        this.mode = EffectCalculationType[ _.get( parameters, 'mode', 'DIFF' ) as string ]
    }

    isInstant(): boolean {
        return true
    }

    canStart( info: BuffInfo ): boolean {
        let target = info.getAffected()
        return target && !target.isDead() && target.isPlayer()
    }

    async onStart( info: BuffInfo ) {
        let target = info.getAffected()
        let amount = 0
        switch ( this.mode ) {
            case EffectCalculationType.DIFF:
                amount = Math.min( this.amount, target.getMaxRecoverableCp() - target.getCurrentCp() )
                break
            case EffectCalculationType.PER:
                if ( this.amount < 0 ) {
                    amount = ( target.getCurrentCp() * this.amount ) / 100
                } else {
                    amount = Math.min( ( target.getMaxCp() * this.amount ) / 100.0, target.getMaxRecoverableCp() - target.getCurrentCp() )
                }
                break
        }

        if ( amount !== 0 ) {
            target.setCurrentCp( amount + target.getCurrentCp() )
        }

        if ( amount >= 0 ) {
            let attacker = info.getEffector()
            if ( attacker && ( attacker.objectId !== target.objectId ) ) {
                let packet : Buffer = new SystemMessageBuilder( SystemMessageIds.S2_CP_HAS_BEEN_RESTORED_BY_C1 )
                        .addCharacterName( attacker )
                        .addNumber( amount )
                        .getBuffer()
                target.sendOwnedData( packet )
                return
            }

            let packet : Buffer = new SystemMessageBuilder( SystemMessageIds.S1_CP_HAS_BEEN_RESTORED )
                    .addNumber( amount )
                    .getBuffer()
            target.sendOwnedData( packet )
        }
    }
}