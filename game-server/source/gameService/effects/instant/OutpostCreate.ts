import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { TerritoryWarManager } from '../../instancemanager/TerritoryWarManager'
import { L2SiegeFlagInstance } from '../../models/actor/instance/L2SiegeFlagInstance'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { DataManager } from '../../../data/manager'

const flagNpcId = 36590
export class OutpostCreate extends AbstractEffect {

    canStart( info: BuffInfo ): boolean {
        let player = info.getEffector()
        return player.isPlayer()
                && player.getActingPlayer().isClanLeader()
                && TerritoryWarManager.isTWInProgress
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        let player : L2PcInstance = info.getEffector().getActingPlayer()
        let flag : L2SiegeFlagInstance = new L2SiegeFlagInstance( player, DataManager.getNpcData().getTemplate( flagNpcId ), true, true )

        flag.setTitle( player.getClan().getName() )
        flag.setMaxStats()
        flag.setHeading( player.getHeading() )
        flag.spawnMe( player.getX(), player.getY(), player.getZ() + 50 )

        TerritoryWarManager.setSiegeFlagForClan( player.getClan(), flag )
    }
}