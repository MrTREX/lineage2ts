import { AbstractEffect } from '../../models/effects/AbstractEffect'
import _ from 'lodash'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export class SummonAgathion extends AbstractEffect {
    npcId: number

    constructor( parameters: Object ) {
        super()

        this.npcId = _.parseInt( _.get( parameters, 'npcId', '0' ) )
    }

    canStart( info: BuffInfo ): boolean {
        return info.getAffected().isPlayer()
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        let player: L2PcInstance = info.getAffected().getActingPlayer()
        player.setAgathionId( this.npcId )
        player.broadcastUserInfo()
    }
}