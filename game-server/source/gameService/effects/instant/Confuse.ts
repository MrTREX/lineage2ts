import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { Formulas } from '../../models/stats/Formulas'
import { EffectFlag } from '../../enums/EffectFlag'
import { L2Character } from '../../models/actor/L2Character'
import { L2Object } from '../../models/L2Object'
import { L2World } from '../../L2World'
import { L2Npc } from '../../models/actor/L2Npc'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'
import _ from 'lodash'

export class Confuse extends AbstractEffect {
    chance: number
    effectFlags = EffectFlag.CONFUSED

    constructor( parameters: Object ) {
        super()
        this.chance = _.parseInt( _.get( parameters, 'chance', '0' ) )
    }

    isInstant(): boolean {
        return true
    }

    calculateSuccess( info: BuffInfo ): boolean {
        return Formulas.calculateProbability( this.chance, info.getEffector(), info.getAffected(), info.getSkill() )
    }

    async onStart( info: BuffInfo ) {
        let affected: L2Character = info.getAffected()

        let isMonster = affected.isMonster()
        let targetList: Array<L2Character> = L2World.getNearestObjects( affected, 5, ( object: L2Object ) => {
            return ( ( isMonster && object.isAttackable() ) || object.isCharacter() ) && object.getObjectId() !== info.getAffectedId()
        } ) as Array<L2Character>

        if ( targetList.length > 0 ) {
            let target: L2Character = _.sample( targetList )
            affected.setTarget( target )

            AIEffectHelper.notifyAttacked( affected as L2Npc, target )
        }
    }
}