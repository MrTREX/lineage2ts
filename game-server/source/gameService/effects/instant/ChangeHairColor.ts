import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

import _ from 'lodash'

export class ChangeHairColor extends AbstractEffect {
    value: number

    constructor( parameters: Object ) {
        super()
        this.value = _.parseInt( _.get( parameters, 'value', '0' ) )
    }

    isInstant(): boolean {
        return true
    }

    canStart( info: BuffInfo ): boolean {
        let attacker = info.getEffector()
        let target = info.getAffected()
        return attacker && target && attacker.isPlayer() && target.isPlayer() && !target.isAlikeDead()
    }

    async onStart( info: BuffInfo ) {
        let player : L2PcInstance = info.getEffector().getActingPlayer()
        player.getAppearance().setHairColor( this.value )
        player.broadcastUserInfo()
    }
}