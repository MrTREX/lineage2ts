import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import _ from 'lodash'
import { SkillItem } from '../../models/skills/SkillItem'
import { getExistingSkill } from '../../models/holders/SkillHolder'

export class CallSkill extends AbstractEffect {
    skill: SkillItem

    constructor( parameters: Object ) {
        super()

        let id = _.parseInt( _.get( parameters, 'skillId', '0' ) )
        let level = _.parseInt( _.get( parameters, 'skillLevel', '1' ) )

        this.skill = {
            id,
            level
        }
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        return info.getEffector().makeTriggerCast( getExistingSkill( this.skill.id, this.skill.level ), info.getAffected(), true )
    }
}