import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2Npc } from '../../models/actor/L2Npc'

export class ConsumeBody extends AbstractEffect {

    canStart( info: BuffInfo ): boolean {
        let attacker = info.getEffector()
        let target = info.getAffected()
        return attacker && target && target.isNpc() && target.isDead()
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        ( info.getAffected() as L2Npc ).endDecayTask()
    }
}