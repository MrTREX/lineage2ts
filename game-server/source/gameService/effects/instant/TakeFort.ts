import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { Fort } from '../../models/entity/Fort'
import { FortManager } from '../../instancemanager/FortManager'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export class TakeFort extends AbstractEffect {

    isInstant(): boolean {
        return true
    }

    canStart( info: BuffInfo ): boolean {
        return info.getEffector().isPlayer()
    }

    async onStart( info: BuffInfo ) {
        let player = info.getEffector() as L2PcInstance
        let fort : Fort = FortManager.getFort( player )
        if ( fort ) {
            fort.endOfSiege( player.getClan() )
        }
    }
}