import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2Character } from '../../models/actor/L2Character'
import { L2Party } from '../../models/L2Party'
import { Skill } from '../../models/Skill'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2World } from '../../L2World'
import { GeneralHelper } from '../../helpers/GeneralHelper'
import { L2Summon } from '../../models/actor/L2Summon'
import { L2Playable } from '../../models/actor/L2Playable'

export class RebalanceMP extends AbstractEffect {
    effectType = L2EffectType.RebalanceMp

    isInstant(): boolean {
        return true
    }

    canStart( info: BuffInfo ): boolean {
        let attacker: L2Character = info.getEffector()
        return attacker.isPlayer() && attacker.isInParty()
    }

    async onStart( info: BuffInfo ) {
        let fullMP = 0
        let currentMPs = 0
        let party: L2Party = info.getEffector().getParty()
        let skill: Skill = info.getSkill()
        let effector: L2Character = info.getEffector()

        let allCharacters: Array<L2Playable> = []

        party.getMembers().forEach( ( memberId: number ) => {
            let member: L2PcInstance = L2World.getPlayer( memberId )
            if ( !member.isDead() && GeneralHelper.checkIfInRange( skill.getAffectRange(), effector, member, true ) ) {
                fullMP += member.getMaxMp()
                currentMPs += member.getCurrentMp()
                allCharacters.push( member )
            }

            let summon: L2Summon = member.getSummon()
            if ( summon && ( !summon.isDead() && GeneralHelper.checkIfInRange( skill.getAffectRange(), effector, summon, true ) ) ) {
                fullMP += summon.getMaxMp()
                currentMPs += summon.getCurrentMp()
                allCharacters.push( summon )
            }
        } )

        let percentHP = currentMPs / fullMP

        allCharacters.forEach( ( character: L2Playable ) => {
            let newMP = character.getMaxMp() * percentHP
            if ( newMP > character.getCurrentMp() ) {
                if ( character.getCurrentMp() > character.getMaxRecoverableMp() ) {
                    newMP = character.getCurrentMp()
                } else if ( newMP > character.getMaxRecoverableMp() ) {
                    newMP = character.getMaxRecoverableMp()
                }
            }

            character.setCurrentMp( newMP )
        } )
    }
}