import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { CastleManager } from '../../instancemanager/CastleManager'
import { Castle } from '../../models/entity/Castle'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export class TakeCastle extends AbstractEffect {

    isInstant(): boolean {
        return true
    }

    canStart( info: BuffInfo ): boolean {
        return info.getEffector().isPlayer()
    }

    async onStart( info: BuffInfo ) {
        let castle : Castle = CastleManager.getCastle( info.getEffector() )
        castle.engrave( ( info.getEffector() as L2PcInstance ).getClan(), info.getAffected() )
    }
}