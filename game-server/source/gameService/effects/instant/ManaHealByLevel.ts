import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { L2Character } from '../../models/actor/L2Character'
import { Stats } from '../../models/stats/Stats'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'

import _ from 'lodash'

export class ManaHealByLevel extends AbstractEffect {
    power: number
    effectType = L2EffectType.MpChangeByLevel

    constructor( parameters: Object ) {
        super()

        this.power = parseFloat( _.get( parameters, 'power', '0' ) )
    }

    canStart( info: BuffInfo ): boolean {
        let target: L2Character = info.getAffected()
        return !( !target || target.isDead() || target.isDoor() || target.isInvulnerable() || target.isMpBlocked() )
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        let target: L2Character = info.getAffected()

        // recharged mp influenced by difference between target level and skill level
        // if target is within 5 levels or lower then skill level there's no penalty.
        let amount = target.calculateStat( Stats.MANA_CHARGE, this.power, null, null )
        if ( target.getLevel() > info.getSkill().getMagicLevel() ) {
            let levelDifference = target.getLevel() - info.getSkill().getMagicLevel()
            // if target is too high compared to skill level, the amount of recharged mp gradually decreases.
            if ( levelDifference === 6 ) {
                amount *= 0.9 // only 90% effective
            } else if ( levelDifference === 7 ) {
                amount *= 0.8 // 80%
            } else if ( levelDifference === 8 ) {
                amount *= 0.7 // 70%
            } else if ( levelDifference === 9 ) {
                amount *= 0.6 // 60%
            } else if ( levelDifference === 10 ) {
                amount *= 0.5 // 50%
            } else if ( levelDifference === 11 ) {
                amount *= 0.4 // 40%
            } else if ( levelDifference === 12 ) {
                amount *= 0.3 // 30%
            } else if ( levelDifference === 13 ) {
                amount *= 0.2 // 20%
            } else if ( levelDifference === 14 ) {
                amount *= 0.1 // 10%
            } else if ( levelDifference >= 15 ) {
                amount = 0 // 0mp recharged
            }
        }

        // Prevents overheal and negative amount
        amount = Math.max( Math.min( amount, target.getMaxRecoverableMp() - target.getCurrentMp() ), 0 )
        if ( amount !== 0 ) {
            target.setCurrentMp( amount + target.getCurrentMp() )
        }

        if ( !target.isPlayable() ) {
            return
        }

        let packet: Buffer
        if ( info.getAffectedId() !== info.getEffectorId() ) {
            packet = new SystemMessageBuilder( SystemMessageIds.S2_MP_HAS_BEEN_RESTORED_BY_C1 )
                    .addCharacterName( info.getEffector() )
                    .addNumber( amount )
                    .getBuffer()
        } else {
            packet = new SystemMessageBuilder( SystemMessageIds.S1_MP_HAS_BEEN_RESTORED )
                    .addNumber( amount )
                    .getBuffer()
        }

        target.sendOwnedData( packet )
    }
}