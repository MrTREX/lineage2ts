import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Attackable } from '../../models/actor/L2Attackable'
import { ItemData } from '../../interface/ItemData'
import { InventoryAction } from '../../enums/InventoryAction'
import aigle from 'aigle'

export class Sweeper extends AbstractEffect {

    isInstant(): boolean {
        return true
    }

    canStart( info: BuffInfo ): boolean {
        let attacker = info.getEffector()
        let target = info.getAffected()

        return attacker
                && target
                && attacker.isPlayer()
                && target.isAttackable()
                && ( target as L2Attackable ).isSpoiled()
    }

    async onStart( info: BuffInfo ) {
        let player: L2PcInstance = info.getEffector().getActingPlayer()
        let monster: L2Attackable = info.getAffected() as L2Attackable

        if ( monster.isSweepActive() ) {
            await aigle.resolve( monster.getSweepItems() ).each( ( item: ItemData ) => {
                if ( player.isInParty() ) {
                    return player.getParty().distributeItem( player, item.itemId, item.amount, true, monster )
                }

                return player.addItem( item.itemId, item.amount, -1, monster.getObjectId(), InventoryAction.Sweeper )
            } )
        }

        monster.emptySweepItems()
        monster.setSpoilerObjectId( 0 )
    }
}