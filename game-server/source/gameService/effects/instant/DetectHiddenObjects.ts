import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2DoorInstance } from '../../models/actor/instance/L2DoorInstance'

export class DetectHiddenObjects extends AbstractEffect {

    isInstant(): boolean {
        return true
    }

    canStart( info: BuffInfo ): boolean {
        return info.getAffected().isDoor()
    }

    async onStart( info: BuffInfo ) {
        let door : L2DoorInstance = info.getAffected() as L2DoorInstance
        if ( door.getTemplate().isStealth() ) {
            door.setMeshIndex( 1 )
            door.setTargetable( door.getTemplate().getOpenType() !== 0 )
            door.broadcastStatusUpdate()
        }
    }
}