import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'

import _ from 'lodash'

export class FocusEnergy extends AbstractEffect {
    charge: number

    constructor( parameters: Object ) {
        super()

        this.charge = _.parseInt( _.get( parameters, 'charge', '0' ) )
    }

    isInstant(): boolean {
        return true
    }

    canStart( info: BuffInfo ): boolean {
        return info.getAffected().isPlayer()
    }

    async onStart( info: BuffInfo ) {
        return info.getAffected().getActingPlayer().increaseCharges( 1, this.charge )
    }
}