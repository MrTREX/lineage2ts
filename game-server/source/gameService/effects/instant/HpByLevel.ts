import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2Character } from '../../models/actor/L2Character'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { PacketDispatcher } from '../../PacketDispatcher'

import _ from 'lodash'

export class HpByLevel extends AbstractEffect {
    power : number
    effectType = L2EffectType.Buff

    constructor( parameters: Object ) {
        super()

        this.power = parseFloat( _.get( parameters, 'power', '0' ) )
    }

    isInstant(): boolean {
        return true
    }

    canStart( info: BuffInfo ): boolean {
        return !!info.getEffector()
    }

    async onStart( info: BuffInfo ) {
        let attacker : L2Character = info.getEffector()
        let powerValue = this.power
        let amount = ( ( attacker.getCurrentHp() + powerValue ) > attacker.getMaxHp() ? attacker.getMaxHp() : ( attacker.getCurrentHp() + powerValue ) )
        let restored = Math.round( amount - attacker.getCurrentHp() )

        attacker.setCurrentHp( amount )

        let packet : Buffer = new SystemMessageBuilder( SystemMessageIds.S1_HP_HAS_BEEN_RESTORED )
                .addNumber( restored )
                .getBuffer()

        PacketDispatcher.sendOwnedData( attacker.objectId, packet )
    }
}