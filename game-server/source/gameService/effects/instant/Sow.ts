import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2Character } from '../../models/actor/L2Character'
import { L2Seed } from '../../models/L2Seed'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2MonsterInstance } from '../../models/actor/instance/L2MonsterInstance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { SoundPacket } from '../../packets/send/SoundPacket'
import { AIIntent } from '../../aicontroller/enums/AIIntent'
import { ConfigManager } from '../../../config/ConfigManager'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'
import _ from 'lodash'

export class Sow extends AbstractEffect {

    canSeed( player: L2Character, target: L2Character, seed: L2Seed ): boolean {
        let minlevelSeed = seed.getLevel() - 5
        let maxlevelSeed = seed.getLevel() + 5
        let levelPlayer = player.getLevel()
        let levelTarget = target.getLevel()
        let basicSuccess = seed.isAlternative() ? 20 : 90

        if ( levelTarget < minlevelSeed ) {
            basicSuccess -= 5 * ( minlevelSeed - levelTarget )
        }
        if ( levelTarget > maxlevelSeed ) {
            basicSuccess -= 5 * ( levelTarget - maxlevelSeed )
        }

        // 5% decrease in chance if player level
        // is more than +/- 5 levels from target's level
        let levelDifference: number = levelPlayer - levelTarget
        if ( levelDifference < 0 ) {
            levelDifference = -levelDifference
        }
        if ( levelDifference > 5 ) {
            basicSuccess -= 5 * ( levelDifference - 5 )
        }

        return _.random( 99 ) < Math.max( basicSuccess, 1 )
    }

    canStart( info: BuffInfo ): boolean {
        return info.getEffector().isPlayer() && info.getAffected().isMonster()
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        let player: L2PcInstance = info.getEffector().getActingPlayer()
        let target: L2MonsterInstance = info.getAffected() as L2MonsterInstance

        if ( target.isDead()
                || !target.getTemplate().canBeSown()
                || target.isSeeded()
                || target.getSeederId() !== player.getObjectId() ) {
            return
        }

        let seed: L2Seed = target.getSeed()
        if ( !( await player.destroyItemByItemId( seed.getSeedId(), getRequiredSeedCount( target ), false, 'Effect:Sow' ) ) ) {
            return
        }

        let messageId: number
        if ( this.canSeed( player, target, seed ) ) {
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
            target.setSeededByPlayer( player )
            messageId = SystemMessageIds.THE_SEED_WAS_SUCCESSFULLY_SOWN
        } else {
            messageId = SystemMessageIds.THE_SEED_WAS_NOT_SOWN
        }

        if ( player.isInParty() ) {
            player.getParty().broadcastPacket( SystemMessageBuilder.fromMessageId( messageId ) )
        } else {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( messageId ) )
        }

        return AIEffectHelper.setNextIntent( target, AIIntent.WAITING )
    }
}

function getRequiredSeedCount( target: L2MonsterInstance ): number {
    if ( !ConfigManager.tuning.isSowingUsingLeveledSeed() ) {
        return 1
    }

    let levels: number = Math.floor( target.getLevel() / ConfigManager.tuning.getSowingLevelsIncrement() )
    return Math.min( levels, 1 )
}