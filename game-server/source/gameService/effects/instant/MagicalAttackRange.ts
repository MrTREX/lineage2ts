import { AbstractEffect } from '../../models/effects/AbstractEffect'
import _ from 'lodash'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2Character } from '../../models/actor/L2Character'
import { Skill } from '../../models/Skill'
import { Formulas } from '../../models/stats/Formulas'
import { ShotType } from '../../enums/ShotType'
import { Stats } from '../../models/stats/Stats'

export class MagicalAttackRange extends AbstractEffect {
    power: number
    shieldDefencePercent: number
    effectType = L2EffectType.MagicalAttack

    constructor( parameters: Object ) {
        super()

        this.power = parseFloat( _.get( parameters, 'power', '0' ) )
        this.shieldDefencePercent = parseFloat( _.get( parameters, 'shieldDefensePercent', '0' ) )
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        let target : L2Character = info.getAffected()
        let attacker : L2Character = info.getEffector()
        let skill : Skill = info.getSkill()

        if ( target.isPlayer() && target.getActingPlayer().isFakeDeath() ) {
            await target.stopFakeDeath( true )
        }

        let sps = skill.useSpiritShot() && attacker.isChargedShot( ShotType.Spiritshot )
        let bss = skill.useSpiritShot() && attacker.isChargedShot( ShotType.BlessedSpiritshot )
        let magicCrit = Formulas.calculateMagicCrit( attacker, target, skill )
        let shieldUse = Formulas.calculateShieldUse( attacker, target, skill )
        let damage = Formulas.calculateMagicDamageWithShield( attacker, target, skill, shieldUse, this.shieldDefencePercent, sps, bss, magicCrit, this.power )

        if ( damage > 0 ) {
            if ( !target.isRaid() && Formulas.calculateAttackBreak( target, damage ) ) {
                target.notifyAttackInterrupted()
                target.attemptCastStop()
            }

            if ( target.getStat().calculateStat( Stats.VENGEANCE_SKILL_MAGIC_DAMAGE, 0, target, skill ) > _.random( 100 ) ) {
                attacker.notifyDamageReceived( damage, target, skill, magicCrit, false, true )

                return attacker.reduceCurrentHp( damage, target, skill )
            }

            /*
                When skill is cast it is possible to deal damage to kill target.
                In such case message about damage must come first before damage
                is applied, since once target is dead additional rewards are
                possible, such as messages about EXP and SP being gained.

                Thus, order must be preserved to achieve logical flow of messages (damage -> rewards).
             */
            target.notifyDamageReceived( damage, attacker, skill, magicCrit, false, false )
            attacker.sendDamageMessage( target, damage, magicCrit, false, false )

            return target.reduceCurrentHp( damage, attacker, skill )
        }
    }
}