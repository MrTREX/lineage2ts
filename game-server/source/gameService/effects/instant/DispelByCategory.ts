import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { DispelCategory } from '../../enums/DispelCategory'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { Formulas } from '../../models/stats/Formulas'
import aigle from 'aigle'
import _ from 'lodash'
import { Skill } from '../../models/Skill'

export class DispelByCategory extends AbstractEffect {
    rate: number
    max: number
    slot: DispelCategory
    effectType = L2EffectType.Dispell

    constructor( parameters: Object ) {
        super()
        this.rate = _.parseInt( _.get( parameters, 'rate', '0' ) )
        this.max = _.parseInt( _.get( parameters, 'max', '0' ) )
        this.slot = DispelCategory[ _.get( parameters, 'slot', 'BUFF' ) as string ]
    }

    isInstant(): boolean {
        return true
    }

    canStart( info: BuffInfo ): boolean {
        return !info.getAffected().isDead()
    }

    async onStart( info: BuffInfo ) {
        let effectSkills: Array<Skill> = Formulas.calculateCancelEffects( info.getEffector(), info.getAffected(), info.getSkill(), this.slot, this.rate, this.max )

        let target = info.getAffected()
        await aigle.resolve( effectSkills ).each( ( skill: Skill ): Promise<void> => {
            return target.getEffectList().stopSkillEffects( true, skill.getId() )
        } )
    }
}