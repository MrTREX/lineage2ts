import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2Character } from '../../models/actor/L2Character'
import { L2Party } from '../../models/L2Party'
import { Skill } from '../../models/Skill'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2World } from '../../L2World'
import { GeneralHelper } from '../../helpers/GeneralHelper'
import { L2Summon } from '../../models/actor/L2Summon'
import { L2Playable } from '../../models/actor/L2Playable'

export class RebalanceHP extends AbstractEffect {

    effectType = L2EffectType.RebalanceHp

    isInstant(): boolean {
        return true
    }

    canStart( info: BuffInfo ): boolean {
        let attacker: L2Character = info.getEffector()
        return attacker.isPlayer() && attacker.isInParty()
    }

    async onStart( info: BuffInfo ) {
        let fullHP = 0
        let currentHPs = 0
        let party: L2Party = info.getEffector().getParty()
        let skill: Skill = info.getSkill()
        let effector: L2Character = info.getEffector()

        let allCharacters: Array<L2Playable> = []

        party.getMembers().forEach( ( memberId: number ) => {
            let member: L2PcInstance = L2World.getPlayer( memberId )
            if ( !member.isDead() && GeneralHelper.checkIfInRange( skill.getAffectRange(), effector, member, true ) ) {
                fullHP += member.getMaxHp()
                currentHPs += member.getCurrentHp()
                allCharacters.push( member )
            }

            let summon: L2Summon = member.getSummon()
            if ( summon && ( !summon.isDead() && GeneralHelper.checkIfInRange( skill.getAffectRange(), effector, summon, true ) ) ) {
                fullHP += summon.getMaxHp()
                currentHPs += summon.getCurrentHp()
                allCharacters.push( summon )
            }
        } )

        let hpRatio = currentHPs / fullHP

        allCharacters.forEach( ( character: L2Playable ) => {
            let newHP = character.getMaxHp() * hpRatio
            if ( newHP > character.getCurrentHp() ) {
                if ( character.getCurrentHp() > character.getMaxRecoverableHp() ) {
                    newHP = character.getCurrentHp()
                } else if ( newHP > character.getMaxRecoverableHp() ) {
                    newHP = character.getMaxRecoverableHp()
                }
            }

            character.setCurrentHp( newHP )
        } )
    }
}