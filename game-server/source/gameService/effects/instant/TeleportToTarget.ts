import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2Character } from '../../models/actor/L2Character'
import { GeneralHelper } from '../../helpers/GeneralHelper'
import { Location } from '../../models/Location'
import { FlyToLocation, FlyType } from '../../packets/send/FlyToLocation'
import { ValidateLocation } from '../../packets/send/ValidateLocation'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'
import { AIIntent } from '../../aicontroller/enums/AIIntent'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { PathFinding } from '../../../geodata/PathFinding'

export class TeleportToTarget extends AbstractEffect {

    canStart( info: BuffInfo ): boolean {
        return !!info.getAffected()
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        let attacker: L2Character = info.getEffector()
        let target: L2Character = info.getAffected()

        let startX = target.getX()
        let startY = target.getY()
        let heading = GeneralHelper.convertHeadingToDegree( target.getHeading() )

        heading += 180
        if ( heading > 360 ) {
            heading -= 360
        }

        heading = ( Math.PI * heading ) / 180
        let x = Math.round( startX + ( 25 * Math.cos( heading ) ) )
        let y = Math.round( startY + ( 25 * Math.sin( heading ) ) )
        let z = target.getZ()

        let location: Location = PathFinding.getApproximateDestination( attacker, x, y, z )

        await AIEffectHelper.setNextIntent( attacker, AIIntent.WAITING )
        BroadcastHelper.dataToSelfBasedOnVisibility( attacker, FlyToLocation( attacker, location, FlyType.Dummy ) )

        attacker.setXYZLocation( location )
        BroadcastHelper.dataToSelfBasedOnVisibility( attacker, ValidateLocation( attacker ) )
    }
}