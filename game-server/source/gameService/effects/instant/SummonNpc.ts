import { AbstractEffect } from '../../models/effects/AbstractEffect'
import _ from 'lodash'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2NpcTemplate } from '../../models/actor/templates/L2NpcTemplate'
import { DataManager } from '../../../data/manager'
import { L2TargetType } from '../../models/skills/targets/L2TargetType'
import { ILocational } from '../../models/Location'
import { L2Npc } from '../../models/actor/L2Npc'
import { L2EffectPointInstance } from '../../models/actor/instance/L2EffectPointInstance'
import { L2DecoyInstance } from '../../models/actor/instance/L2DecoyInstance'
import { NpcTemplateType } from '../../enums/NpcTemplateType'
import { L2ManualSpawn } from '../../models/spawns/type/L2ManualSpawn'

export class SummonNpc extends AbstractEffect {
    despawnDelay: number
    npcId: number
    count: number
    offset: boolean
    isSpawn: boolean
    effectType = L2EffectType.SummonNpc

    constructor( parameters: Object ) {
        super()

        this.despawnDelay = _.parseInt( _.get( parameters, 'despawnDelay', '20000' ) )
        this.npcId = _.parseInt( _.get( parameters, 'npcId', '0' ) )
        this.count = _.parseInt( _.get( parameters, 'npcCount', '1' ) )
        this.offset = _.get( parameters, 'randomOffset', false )
        this.isSpawn = _.get( parameters, 'isSummonSpawn', false )
    }

    canStart( info: BuffInfo ): boolean {
        let target = info.getAffected()
        if ( !target || !target.isPlayer() || target.isAlikeDead() ) {
            return false
        }

        if ( ( this.npcId <= 0 ) || ( this.count <= 0 ) ) {
            return false
        }

        let player: L2PcInstance = target.getActingPlayer()
        if ( player.isMounted() || player.inObserverMode() ) {
            return false
        }

        return true
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        let npcTemplate: L2NpcTemplate = DataManager.getNpcData().getTemplate( this.npcId )
        if ( !npcTemplate ) {
            return
        }

        let player = info.getAffected().getActingPlayer()
        switch ( npcTemplate.getType() ) {
            case NpcTemplateType.L2Decoy:
                this.createDecoy( npcTemplate, player )
                break
            case NpcTemplateType.L2EffectPoint:
                this.createEffectPoint( info, npcTemplate, player )
                break
            default:
                this.createSpawn( npcTemplate, player )
                break
        }
    }

    createDecoy( template: L2NpcTemplate, player: L2PcInstance ) {
        let decoy: L2DecoyInstance = new L2DecoyInstance( template, player, this.despawnDelay )
        decoy.setCurrentHp( decoy.getMaxHp() )
        decoy.setCurrentMp( decoy.getMaxMp() )
        decoy.setHeading( player.getHeading() )
        decoy.setInstanceId( player.getInstanceId() )
        decoy.setSummoner( player.getObjectId() )
        decoy.spawnMe( player.getX(), player.getY(), player.getZ() )
        player.setDecoy( decoy )
    }

    createEffectPoint( info: BuffInfo, template: L2NpcTemplate, player: L2PcInstance ) {
        let effectPoint: L2EffectPointInstance = new L2EffectPointInstance( template, player )
        effectPoint.setCurrentHp( effectPoint.getMaxHp() )
        effectPoint.setCurrentMp( effectPoint.getMaxMp() )
        let x = player.getX()
        let y = player.getY()
        let z = player.getZ()

        if ( info.getSkill().getTargetType() === L2TargetType.GROUND ) {
            let wordPosition: ILocational = player.getGroundSkillLocation()
            if ( wordPosition ) {
                x = wordPosition.getX()
                y = wordPosition.getY()
                z = wordPosition.getZ()
            }
        }

        effectPoint.setIsInvulnerable( true )
        effectPoint.setSummoner( player.getObjectId() )
        effectPoint.spawnMe( x, y, z )
        if ( this.despawnDelay > 0 ) {
            effectPoint.scheduleDespawn( this.despawnDelay )
        }
    }

    createSpawn( template: L2NpcTemplate, player: L2PcInstance ) {
        let x = player.getX()
        let y = player.getY()
        if ( this.offset ) {
            x += Math.random() > 0.5 ? _.random( 20, 50 ) : _.random( -50, -20 )
            y += Math.random() > 0.5 ? _.random( 20, 50 ) : _.random( -50, -20 )
        }

        let spawn: L2ManualSpawn = L2ManualSpawn.fromParameters( template, 1, x, y, player.getZ(), player.getHeading(), player.getInstanceId(), this.isSpawn, false )
        spawn.startSpawn()

        let npc: L2Npc = spawn.getNpcs()[ 0 ]

        npc.setSummoner( player.getObjectId() )
        npc.setName( template.getName() )
        npc.setTitle( template.getName() )
        if ( this.despawnDelay > 0 ) {
            npc.scheduleDespawn( this.despawnDelay )
        }

        npc.setIsRunning( false )
    }
}