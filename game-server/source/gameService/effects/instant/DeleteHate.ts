import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { Formulas } from '../../models/stats/Formulas'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { L2Attackable } from '../../models/actor/L2Attackable'
import _ from 'lodash'

export class DeleteHate extends AbstractEffect {
    chance: number
    effectType = L2EffectType.Hate

    constructor( parameters: Object ) {
        super()
        this.chance = _.parseInt( _.get( parameters, 'chance', '100' ) )
    }

    calculateSuccess( info: BuffInfo ): boolean {
        return Formulas.calculateProbability( this.chance, info.getEffector(), info.getAffected(), info.getSkill() )
    }

    isInstant(): boolean {
        return true
    }

    canStart( info: BuffInfo ): boolean {
        return info.getAffected().isAttackable()
    }

    async onStart( info: BuffInfo ) {
        let target : L2Attackable = info.getAffected() as L2Attackable
        target.clearOverhitData()
        target.setWalking()
    }
}