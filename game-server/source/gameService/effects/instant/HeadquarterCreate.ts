import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2SiegeFlagInstance } from '../../models/actor/instance/L2SiegeFlagInstance'
import { DataManager } from '../../../data/manager'
import { CastleManager } from '../../instancemanager/CastleManager'
import { Castle } from '../../models/entity/Castle'
import { FortManager } from '../../instancemanager/FortManager'
import { Fort } from '../../models/entity/Fort'
import { SiegableHall } from '../../models/entity/clanhall/SiegableHall'
import { ClanHallSiegeManager } from '../../instancemanager/ClanHallSiegeManager'
import _ from 'lodash'

const flagNpcId = 35062

export class HeadquarterCreate extends AbstractEffect {
    isAdvanced: boolean

    constructor( parameters: Object ) {
        super()

        this.isAdvanced = !!_.get( parameters, 'isAdvanced', false )
    }

    isInstant(): boolean {
        return true
    }

    canStart( info: BuffInfo ): boolean {
        let player = info.getEffector()
        return player.isPlayer() && player.getActingPlayer().isClanLeader()
    }

    async onStart( info: BuffInfo ) {
        let player = info.getEffector().getActingPlayer()
        let flag : L2SiegeFlagInstance = new L2SiegeFlagInstance( player, DataManager.getNpcData().getTemplate( flagNpcId ), this.isAdvanced, false )

        await flag.setTitle( player.getClan().getName() )
        flag.setMaxStats()
        flag.setHeading( player.getHeading() )
        flag.spawnMe( player.getX(), player.getY(), player.getZ() + 50 )

        let castle : Castle = CastleManager.getCastle( player )
        let fort : Fort = FortManager.getFort( player )
        let hall : SiegableHall = ClanHallSiegeManager.getNearbyClanHall( player )
        let flagId = flag.objectId

        if ( castle ) {
            castle.getSiege().getFlag( player.getClan() ).push( flagId )
            return
        }

        if ( fort ) {
            fort.getSiege().getFlag( player.getClan() ).push( flagId )
            return
        }

        hall.getSiege().getFlag( player.getClan() ).push( flagId )
    }
}