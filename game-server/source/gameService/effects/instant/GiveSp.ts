import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'

import _ from 'lodash'

export class GiveSp extends AbstractEffect {
    value: number

    constructor( parameters: Object ) {
        super()

        this.value = _.parseInt( _.get( parameters, 'sp', '0' ) )
    }

    isInstant(): boolean {
        return true
    }

    canStart( info: BuffInfo ): boolean {
        let attacker = info.getEffector()
        let target = info.getAffected()
        return attacker
                && target
                && attacker.isPlayer()
                && target.isPlayer()
                && !target.isAlikeDead()
    }

    async onStart( info: BuffInfo ) {
        return info.getEffector().getActingPlayer().addExpAndSp( 0, this.value )
    }
}