import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2Weapon } from '../../models/items/L2Weapon'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { Elementals } from '../../models/Elementals'
import { ItemInstanceType } from '../../models/items/ItemInstanceType'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { InventorySlot } from '../../values/InventoryValues'
import { ElementalType } from '../../enums/ElementalType'

export class ConvertItem extends AbstractEffect {

    isInstant(): boolean {
        return true
    }

    canStart( info: BuffInfo ): boolean {
        let target = info.getAffected()
        return info.getEffector()
                && target
                && !target.isAlikeDead()
                && target.isPlayer()
                && !target.getActingPlayer().isEnchanting()
    }

    async onStart( info: BuffInfo ) {
        let player = info.getAffected().getActingPlayer()
        let weaponItem: L2Weapon = player.getActiveWeaponItem()
        if ( !weaponItem ) {
            return
        }

        let weapon: L2ItemInstance = player.getInventory().getPaperdollItem( InventorySlot.RightHand )
        if ( !weapon ) {
            weapon = player.getInventory().getPaperdollItem( InventorySlot.LeftHand )
        }

        if ( !weapon || weapon.isAugmented() || ( weaponItem.getChangeWeaponId() === 0 ) ) {
            return
        }

        let newItemId = weaponItem.getChangeWeaponId()
        if ( newItemId === -1 ) {
            return
        }

        let uniequippedItems: Array<L2ItemInstance> = await player.getInventory().unEquipItemInBodySlotAndRecord( weapon.getItem().getBodyPart() )
        if ( uniequippedItems.length <= 0 ) {
            return
        }

        let count = 0
        uniequippedItems.forEach( ( item: L2ItemInstance ) => {
            if ( !item.getItem().isInstanceType( ItemInstanceType.L2Weapon ) ) {
                count++
                return
            }

            if ( item.getEnchantLevel() > 0 ) {
                let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.EQUIPMENT_S1_S2_REMOVED )
                        .addNumber( item.getEnchantLevel() )
                        .addItemInstanceName( item )
                        .getBuffer()

                player.sendOwnedData( packet )
                return
            }

            let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.S1_DISARMED )
                    .addItemInstanceName( item )
                    .getBuffer()
            player.sendOwnedData( packet )
        } )

        if ( count === uniequippedItems.length ) {
            return
        }

        let destroyItem: L2ItemInstance = await player.getInventory().destroyItem( weapon, weapon.getCount(), 0, 'ConvertItem.onStart' )
        if ( !destroyItem ) {
            return
        }

        let addedItem: L2ItemInstance = await player.getInventory().addItem( newItemId, 1, destroyItem.getId(), 'ConvertItem.onStart' )
        if ( !addedItem ) {
            return
        }

        let enchantLevel = weapon.getEnchantLevel()
        let elementals: Elementals = weapon.getElementals()[ 0 ]

        if ( elementals && ( elementals.getElement() !== ElementalType.NONE ) && ( elementals.getValue() !== -1 ) ) {
            addedItem.setElementAttribute( elementals.getElement(), elementals.getValue() )
        }

        await addedItem.setEnchantLevel( enchantLevel )
        await player.getInventory().equipItem( addedItem )

        if ( addedItem.getEnchantLevel() > 0 ) {
            let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.S1_S2_EQUIPPED )
                    .addNumber( addedItem.getEnchantLevel() )
                    .addItemInstanceName( addedItem )
                    .getBuffer()

            player.sendOwnedData( packet )
            return
        }

        let messagePacket: Buffer = new SystemMessageBuilder( SystemMessageIds.S1_EQUIPPED )
                .addItemInstanceName( addedItem )
                .getBuffer()
        player.sendOwnedData( messagePacket )

        player.broadcastUserInfo()
    }
}