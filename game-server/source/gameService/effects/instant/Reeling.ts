import { AbstractEffect } from '../../models/effects/AbstractEffect'
import _ from 'lodash'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Fishing } from '../../models/fishing/L2Fishing'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { ActionFailed } from '../../packets/send/ActionFailed'
import { L2Weapon } from '../../models/items/L2Weapon'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { ShotType } from '../../enums/ShotType'
import { L2FishingRod } from '../../models/fishing/L2FishingRod'
import { DataManager } from '../../../data/manager'
import { Stats } from '../../models/stats/Stats'

export class Reeling extends AbstractEffect {
    power: number
    effectType = L2EffectType.Fishing

    constructor( parameters: Object ) {
        super()

        this.power = parseFloat( _.get( parameters, 'power', '0' ) )
    }

    isInstant(): boolean {
        return true
    }

    canStart( info: BuffInfo ): boolean {
        return info.getEffector().isPlayer()
    }

    async onStart( info: BuffInfo ): Promise<void> {
        let attacker = info.getEffector()
        let player: L2PcInstance = attacker.getActingPlayer()
        let action: L2Fishing = player.getFishCombat()

        if ( !action ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CAN_USE_REELING_ONLY_WHILE_FISHING ) )
            player.sendOwnedData( ActionFailed() )
            return
        }

        let weaponItem: L2Weapon = player.getActiveWeaponItem()
        let weaponInstance: L2ItemInstance = attacker.getActiveWeaponInstance()
        if ( !weaponItem || !weaponInstance ) {
            return
        }

        let soulShots = 1
        let penetrationModifier = 0
        if ( attacker.isChargedShot( ShotType.FishingSoulshot ) ) {
            soulShots = 2
        }

        let fishingRod: L2FishingRod = DataManager.getFishingRodsData().getFishingRod( weaponItem.getId() )
        let gradeBonus = fishingRod.level * 0.1
        let damage = ( fishingRod.damage + player.calculateStat( Stats.FISHING_EXPERTISE, 1, null, null ) + this.power ) * gradeBonus * soulShots

        if ( player.getSkillLevel( 1315 ) <= ( info.getSkill().getLevel() - 2 ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.REELING_PUMPING_3_LEVELS_HIGHER_THAN_FISHING_PENALTY ) )

            penetrationModifier = damage * 0.05
            damage = damage - penetrationModifier
        }

        if ( soulShots > 1 ) {
            weaponInstance.setChargedShot( ShotType.FishingSoulshot, false )
        }

        return action.useReeling( Math.floor( damage ), Math.floor( penetrationModifier ) )
    }
}