import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import _ from 'lodash'
import { AggroCache } from '../../cache/AggroCache'

export class AddHate extends AbstractEffect {
    power: number

    constructor( parameters: Object ) {
        super()
        this.power = parseFloat( _.get( parameters, 'power', '0' ) )
    }

    canStart( info: BuffInfo ): boolean {
        return info.getAffected().isAttackable()
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {

        if ( this.power > 0 ) {
            AggroCache.addAggro( info.getAffectedId(), info.getEffectorId(), 0, this.power )
            return
        }

        AggroCache.reduceAggroByAmount( info.getAffectedId(), this.power, info.getEffectorId() )
    }
}