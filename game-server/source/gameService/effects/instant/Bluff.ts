import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { Formulas } from '../../models/stats/Formulas'
import { L2Character } from '../../models/actor/L2Character'
import { StartRotation } from '../../packets/send/StartRotation'
import { StopRotation } from '../../packets/send/StopRotation'
import _ from 'lodash'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'

export class Bluff extends AbstractEffect {
    chance: number

    constructor( parameters: Object ) {
        super()
        this.chance = _.parseInt( _.get( parameters, 'chance', '100' ) )
    }

    calculateSuccess( info: BuffInfo ): boolean {
        return Formulas.calculateProbability( this.chance, info.getEffector(), info.getAffected(), info.getSkill() )
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        let target: L2Character = info.getAffected()
        if ( target.getId() === 35062
                || target.isRaid()
                || target.isRaidMinion() ) {
            return
        }

        let attacker: L2Character = info.getEffector()

        BroadcastHelper.dataBasedOnVisibility( target, StartRotation( target.getObjectId(), target.getHeading(), 1, 65535 ) )
        BroadcastHelper.dataBasedOnVisibility( target, StopRotation( target.getObjectId(), attacker.getHeading(), 65535 ) )

        target.setHeading( attacker.getHeading() )
    }
}