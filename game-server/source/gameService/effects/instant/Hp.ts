import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { EffectCalculationType } from '../../enums/EffectCalculationType'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2Character } from '../../models/actor/L2Character'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { PacketDispatcher } from '../../PacketDispatcher'

import _ from 'lodash'

export class Hp extends AbstractEffect {
    amount: number
    mode: EffectCalculationType
    effectType = L2EffectType.Hp

    constructor( parameters: Object ) {
        super()

        this.amount = parseFloat( _.get( parameters, 'amount', '0' ) )
        this.mode = EffectCalculationType[ _.get( parameters, 'mode', 'DIFF' ) as string ]
    }

    isInstant(): boolean {
        return true
    }

    canStart( info: BuffInfo ): boolean {
        let target : L2Character = info.getAffected()
        return !( !target || target.isDead() || target.isDoor() || target.isInvulnerable() || target.isHpBlocked() )
    }

    async onStart( info: BuffInfo ) {
        let target : L2Character = info.getAffected()
        let attacker : L2Character = info.getEffector()
        let amount = 0

        switch ( this.mode ) {
            case EffectCalculationType.DIFF:
                amount = Math.min( this.amount, target.getMaxRecoverableHp() - target.getCurrentHp() )
                break
            case EffectCalculationType.PER:
                if ( this.amount < 0 ) {
                    amount = ( target.getCurrentHp() * this.amount ) / 100
                } else {
                    amount = Math.min( ( target.getMaxHp() * this.amount ) / 100.0, target.getMaxRecoverableHp() - target.getCurrentHp() )
                }
                break
        }

        if ( amount !== 0 ) {
            target.setCurrentHp( amount + target.getCurrentHp() )
        }

        if ( amount >= 0 ) {
            if ( attacker && ( attacker.objectId !== target.objectId ) ) {
                let packet : Buffer = new SystemMessageBuilder( SystemMessageIds.S2_HP_HAS_BEEN_RESTORED_BY_C1 )
                        .addCharacterName( attacker )
                        .addNumber( Math.round( amount ) )
                        .getBuffer()

                PacketDispatcher.sendOwnedData( target.objectId, packet )
                return
            }

            let packet : Buffer = new SystemMessageBuilder( SystemMessageIds.S1_HP_HAS_BEEN_RESTORED )
                    .addNumber( Math.round( amount ) )
                    .getBuffer()

            PacketDispatcher.sendOwnedData( target.objectId, packet )
        }
    }
}