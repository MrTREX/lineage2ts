import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2Character } from '../../models/actor/L2Character'
import { Skill } from '../../models/Skill'
import { Formulas } from '../../models/stats/Formulas'
import { ShotType } from '../../enums/ShotType'
import _ from 'lodash'

export class HpDrain extends AbstractEffect {
    damagePower: number
    drainMultiplier: number
    effectType = L2EffectType.HpDrain

    constructor( parameters: Object ) {
        super()

        this.damagePower = parseFloat( _.get( parameters, 'power', '0' ) )
        this.drainMultiplier = parseFloat( _.get( parameters, 'drain', '0' ) )
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        let attacker = info.getEffector()
        let skill: Skill = info.getSkill()
        let target: L2Character = info.getAffected()

        if ( attacker.isAlikeDead()
                || skill.getId() === 4050 // TODO : cubic drain skill
                || target.isDead() ) {
            return
        }

        let isSpirishotUse = skill.useSpiritShot() && attacker.isChargedShot( ShotType.Spiritshot )
        let isBlessedSpiritshot = skill.useSpiritShot() && attacker.isChargedShot( ShotType.BlessedSpiritshot )
        let magicCrit = Formulas.calculateMagicCrit( attacker, target, skill )
        let shieldUse = Formulas.calculateShieldUse( attacker, target, skill )
        let damage = Formulas.calculateMagicDamage( attacker, target, skill, shieldUse, isSpirishotUse, isBlessedSpiritshot, magicCrit, this.damagePower )

        let drainHp
        let cp = target.getCurrentCp()
        let hp = target.getCurrentHp()

        if ( cp > 0 ) {
            drainHp = ( damage < cp ) ? 0 : ( damage - cp )
        } else if ( damage > hp ) {
            drainHp = hp
        } else {
            drainHp = damage
        }

        let hpAdd = this.drainMultiplier * drainHp
        if ( hpAdd > 0 ) {
            attacker.setCurrentHp( attacker.getCurrentHp() + hpAdd )
        }

        if ( damage > 0 ) {
            if ( !target.isRaid() && Formulas.calculateAttackBreak( target, damage ) ) {
                target.notifyAttackInterrupted()
                target.attemptCastStop()
            }

            attacker.sendDamageMessage( target, damage, magicCrit, false, false )
            await target.reduceCurrentHp( damage, attacker, skill )
            target.notifyDamageReceived( damage, attacker, skill, magicCrit, false, false )
        }
    }
}