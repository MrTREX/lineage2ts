import { AbstractEffect } from '../../models/effects/AbstractEffect'
import _ from 'lodash'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Fishing } from '../../models/fishing/L2Fishing'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { ActionFailed } from '../../packets/send/ActionFailed'
import { L2Weapon } from '../../models/items/L2Weapon'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { ShotType } from '../../enums/ShotType'
import { DataManager } from '../../../data/manager'
import { Stats } from '../../models/stats/Stats'
import { L2FishingRod } from '../../models/fishing/L2FishingRod'

export class Pumping extends AbstractEffect {
    power: number
    effectType = L2EffectType.Fishing

    constructor( parameters: Object ) {
        super()

        this.power = parseFloat( _.get( parameters, 'power', '0' ) )
    }

    canStart( info: BuffInfo ): boolean {
        return info.getEffector().isPlayer()
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ): Promise<void> {
        let player: L2PcInstance = info.getEffector().getActingPlayer()
        let action: L2Fishing = player.getFishCombat()
        if ( !action ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CAN_USE_PUMPING_ONLY_WHILE_FISHING ) )
            player.sendOwnedData( ActionFailed() )
            return
        }

        let weaponItem: L2Weapon = player.getActiveWeaponItem()
        let currentWeapon: L2ItemInstance = player.getActiveWeaponInstance()
        if ( !weaponItem || !currentWeapon ) {
            return
        }

        let soulShots = 1
        let penetrationModifier = 0
        if ( player.isChargedShot( ShotType.FishingSoulshot ) ) {
            soulShots = 2
        }

        let fishingRod: L2FishingRod = DataManager.getFishingRodsData().getFishingRod( weaponItem.getId() )
        let gradeBonus = fishingRod.level * 0.1
        let damage = ( fishingRod.damage + player.calculateStat( Stats.FISHING_EXPERTISE, 1, null, null ) + this.power ) * gradeBonus * soulShots

        if ( player.getSkillLevel( 1315 ) <= ( info.getSkill().getLevel() - 2 ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.REELING_PUMPING_3_LEVELS_HIGHER_THAN_FISHING_PENALTY ) )

            penetrationModifier = damage * 0.05
            damage = damage - penetrationModifier
        }

        if ( soulShots > 1 ) {
            currentWeapon.setChargedShot( ShotType.FishingSoulshot, false )
        }

        return action.usePumping( Math.floor( damage ), Math.floor( penetrationModifier ) )
    }
}