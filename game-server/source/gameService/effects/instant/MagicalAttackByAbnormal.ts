import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2Character } from '../../models/actor/L2Character'
import { Skill } from '../../models/Skill'
import { ShotType } from '../../enums/ShotType'
import { Formulas } from '../../models/stats/Formulas'
import { Stats } from '../../models/stats/Stats'

import _ from 'lodash'

export class MagicalAttackByAbnormal extends AbstractEffect {
    power: number
    effectType = L2EffectType.MagicalAttack

    constructor( parameters: Object ) {
        super()

        this.power = parseFloat( _.get( parameters, 'power', '0' ) )
    }

    canStart( info: BuffInfo ): boolean {
        return !info.getEffector().isAlikeDead()
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        let attacker: L2Character = info.getEffector()
        let skill: Skill = info.getSkill()
        let target: L2Character = info.getAffected()

        if ( target.isPlayer() && target.getActingPlayer().isFakeDeath() ) {
            await target.stopFakeDeath( true )
        }

        let isSpiritshot = skill.useSpiritShot() && attacker.isChargedShot( ShotType.Spiritshot )
        let isBlessed = skill.useSpiritShot() && attacker.isChargedShot( ShotType.BlessedSpiritshot )
        let magicCrit = Formulas.calculateMagicCrit( attacker, target, skill )
        let shieldUse = Formulas.calculateShieldUse( attacker, target, skill )
        let damage = Formulas.calculateMagicDamage( attacker, target, skill, shieldUse, isSpiritshot, isBlessed, magicCrit, this.power )

        if ( damage > 0 ) {
            if ( !target.isRaid() && Formulas.calculateAttackBreak( target, damage ) ) {
                target.notifyAttackInterrupted()
                target.attemptCastStop()
            }

            if ( target.getStat().calculateStat( Stats.VENGEANCE_SKILL_MAGIC_DAMAGE, 0, target, skill ) > _.random( 100 ) ) {
                await attacker.reduceCurrentHp( damage, target, skill )
                attacker.notifyDamageReceived( damage, target, skill, magicCrit, false, true )
            } else {
                await target.reduceCurrentHp( damage, attacker, skill )
                target.notifyDamageReceived( damage, attacker, skill, magicCrit, false, false )
                attacker.sendDamageMessage( target, damage, magicCrit, false, false )
            }
        }
    }
}