import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { AbnormalType } from '../../models/skills/AbnormalType'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2Character } from '../../models/actor/L2Character'
import { CharacterEffects } from '../../models/characterEffects'
import _ from 'lodash'
import aigle from 'aigle'

export class DispelBySlotProbability extends AbstractEffect {
    dispelMap: Record<string, number> = {}
    rate: number
    effectType = L2EffectType.Dispell

    constructor( parameters: Object ) {
        super()

        this.rate = _.parseInt( _.get( parameters, 'rate', '0' ) )
        let items = _.split( _.get( parameters, 'dispel' ), ';' )

        let currentEffect = this
        items.forEach( ( line: string ) => {
            let [ name, value ] = _.split( line, ',' )
            currentEffect.dispelMap[ name ] = _.defaultTo( _.parseInt( value ), Number.MAX_SAFE_INTEGER )
        } )
    }

    isInstant(): boolean {
        return true
    }

    canStart(): boolean {
        return !_.isEmpty( this.dispelMap )
    }

    async onStart( info: BuffInfo ) {
        let affected: L2Character = info.getAffected()
        let effectList: CharacterEffects = affected.getEffectList()
        let rate = this.rate

        await aigle.resolve( this.dispelMap ).eachSeries( ( value: number, name: string ): Promise<void> => {
            if ( _.random( 100 ) >= rate ) {
                return
            }

            let currentType = AbnormalType[ name ]
            if ( currentType === AbnormalType.TRANSFORM && ( affected.isTransformed() || ( affected.isPlayer() || ( value === affected.getActingPlayer().getTransformationId() ) || ( value < 0 ) ) ) ) {
                return affected.stopTransformation( true )
            }

            let buffToDispel: BuffInfo = effectList.getBuffInfoByAbnormalType( currentType )
            if ( !buffToDispel ) {
                return
            }

            if ( ( currentType === buffToDispel.getSkill().getAbnormalType() ) && ( ( value < 0 ) || ( value >= buffToDispel.getSkill().getAbnormalLevel() ) ) ) {
                return effectList.stopSkillEffects( true, currentType )
            }
        } )
    }
}