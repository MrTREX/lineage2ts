import { AbstractEffect } from '../../models/effects/AbstractEffect'
import _ from 'lodash'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { Formulas } from '../../models/stats/Formulas'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { L2Character } from '../../models/actor/L2Character'
import { Skill } from '../../models/Skill'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { ShotType } from '../../enums/ShotType'

export class PhysicalAttackHpLink extends AbstractEffect {
    power: number
    effectType = L2EffectType.PhysicalAttack

    constructor( parameters: Object ) {
        super()

        this.power = parseFloat( _.get( parameters, 'power', '0' ) )
    }

    calculateSuccess( info: BuffInfo ): boolean {
        return !Formulas.calculatePhysicalSkillEvasion( info.getEffector(), info.getAffected(), info.getSkill() )
    }

    canStart( info: BuffInfo ): boolean {
        return !info.getEffector().isAlikeDead()
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        let target: L2Character = info.getAffected()
        let attacker: L2Character = info.getEffector()
        let skill: Skill = info.getSkill()

        if ( attacker.isMovementDisabled() && attacker.isPlayable() ) {
            let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.S1_CANNOT_BE_USED )
                    .addSkillName( skill )
                    .getBuffer()

            return attacker.sendOwnedData( packet )
        }

        let shieldUse = Formulas.calculateShieldUse( attacker, target, skill )
        let ss = skill.isPhysical() && attacker.isChargedShot( ShotType.Soulshot )
        let power = this.power * ( -( ( target.getCurrentHp() * 2 ) / target.getMaxHp() ) + 2 )

        let damage = Formulas.calculateSkillPhysicalDamage( attacker, target, skill, shieldUse, false, ss, power )

        if ( damage > 0 ) {
            attacker.sendDamageMessage( target, damage, false, false, false )
            await target.reduceCurrentHp( damage, attacker, skill )
            target.notifyDamageReceived( damage, attacker, skill, false, false, false )

            return Formulas.calculateDamageReflected( attacker, target, skill, false )
        }

        if ( !attacker.isPlayer() ) {
            return
        }

        attacker.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ATTACK_FAILED ) )
    }
}