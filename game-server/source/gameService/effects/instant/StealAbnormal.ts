import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { DispelCategory } from '../../enums/DispelCategory'
import _ from 'lodash'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { Formulas } from '../../models/stats/Formulas'
import { EffectScope } from '../../models/skills/EffectScope'
import aigle from 'aigle'
import { BuffProperties } from '../../models/skills/BuffDefinition'

export class StealAbnormal extends AbstractEffect {
    slot: DispelCategory
    rate: number
    max: number
    effectType = L2EffectType.Steal

    constructor( parameters: Object ) {
        super()

        this.slot = DispelCategory[ _.get( parameters, 'slot', 'BUFF' ) as string ]
        this.rate = _.parseInt( _.get( parameters, 'rate', '0' ) )
        this.max = _.parseInt( _.get( parameters, 'max', '0' ) )
    }

    canStart( info: BuffInfo ): boolean {
        let target = info.getAffected()
        return target && target.isPlayer() && ( info.getEffectorId() !== target.objectId )
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        let allBuffs: Array<BuffProperties> = Formulas.calculateStealEffects( info.getEffector(), info.getAffected(), info.getSkill(), this.slot, this.rate, this.max )
        if ( allBuffs.length === 0 ) {
            return
        }

        let target = info.getAffected()
        let attacker = info.getEffector()
        await aigle.resolve( allBuffs ).each( async ( info: BuffProperties ) : Promise<void> => {
            let stolen: BuffInfo = BuffInfo.fromParameters( target, attacker, info.skill )

            stolen.setDuration( info.durationMs )
            await info.skill.applyEffectScope( EffectScope.GENERAL, stolen, true, true )

            let targetBuff = target.getEffectList().getBuffInfoBySkillId( info.skill.getId() )
            await target.getEffectList().remove( true, targetBuff, false )

            return attacker.getEffectList().add( stolen )
        } )

        target.getEffectList().sendEffectUpdates( true )
        attacker.getEffectList().sendEffectUpdates( true )
    }
}