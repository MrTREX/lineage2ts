import { AbstractEffect } from '../../models/effects/AbstractEffect'
import _ from 'lodash'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2NpcTemplate } from '../../models/actor/templates/L2NpcTemplate'
import { DataManager } from '../../../data/manager'
import { L2TrapInstance } from '../../models/actor/instance/L2TrapInstance'

export class SummonTrap extends AbstractEffect {
    despawnTime: number
    npcId: number

    constructor( parameters: Object ) {
        super()

        this.despawnTime = _.parseInt( _.get( parameters, 'despawnTime', '0' ) )
        this.npcId = _.parseInt( _.get( parameters, 'npcId', '0' ) )
    }

    isInstant(): boolean {
        return true
    }

    canStart( info: BuffInfo ): boolean {
        let target = info.getAffected()
        if ( !target || !target.isPlayer() || target.isAlikeDead() ) {
            return false
        }

        if ( this.npcId <= 0 ) {
            return false
        }

        let player = info.getAffected().getActingPlayer()
        return !( player.inObserverMode() || player.isMounted() || player.inObserverMode() )
    }

    async onStart( info: BuffInfo ) {
        let npcTemplate: L2NpcTemplate = DataManager.getNpcData().getTemplate( this.npcId )
        if ( !npcTemplate ) {
            return
        }

        let player = info.getAffected().getActingPlayer()
        if ( player.getTrap() ) {
            player.getTrap().unSummon()
        }

        let trap: L2TrapInstance = L2TrapInstance.fromOwner( npcTemplate, player, this.despawnTime )
        trap.setCurrentHp( trap.getMaxHp() )
        trap.setCurrentMp( trap.getMaxMp() )
        trap.setIsMortal( false )
        trap.setHeading( player.getHeading() )
        trap.spawnMe( player.getX(), player.getY(), player.getZ() )
        player.setTrap( trap )
    }
}