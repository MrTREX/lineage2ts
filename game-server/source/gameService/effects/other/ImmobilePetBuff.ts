import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2Summon } from '../../models/actor/L2Summon'

export class ImmobilePetBuff extends AbstractEffect {
    effectType = L2EffectType.Buff

    async onExit( buff: BuffInfo ) {
        buff.getAffected().setIsImmobilized( false )
    }

    canStart( info: BuffInfo ): boolean {
        let target = info.getAffected()
        let player = info.getEffector()
        return target.isSummon() && player.isPlayer() && ( ( target as L2Summon ).getOwnerId() === player.getObjectId() )
    }

    async onStart( info: BuffInfo ) {
        info.getAffected().setIsImmobilized( true )
    }
}