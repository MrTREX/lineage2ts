import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2Character } from '../../models/actor/L2Character'
import { L2DoorInstance } from '../../models/actor/instance/L2DoorInstance'
import { InstanceManager } from '../../instancemanager/InstanceManager'
import { Instance } from '../../models/entity/Instance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'

import _ from 'lodash'

export class OpenDoor extends AbstractEffect {
    chance: number
    isItem: boolean

    constructor( parameters: Object ) {
        super()

        this.chance = _.parseInt( _.get( parameters, 'chance', '0' ) )
        this.isItem = parameters[ 'isItem' ] === 'true'
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        let target = info.getAffected()
        if ( !target.isDoor() ) {
            return
        }

        let attacker: L2Character = info.getEffector()
        let door: L2DoorInstance = target as L2DoorInstance

        if ( attacker.getInstanceId() !== door.getInstanceId() ) {

            let instance: Instance = InstanceManager.getInstance( attacker.getInstanceId() )
            if ( !instance ) {
                return
            }

            let instanceDoor: L2DoorInstance = instance.getDoor( door.getId() )
            if ( instanceDoor ) {
                door = instanceDoor
            }

            if ( attacker.getInstanceId() !== door.getInstanceId() ) {
                return
            }
        }

        if ( ( !door.isOpenableBySkill() && !this.isItem ) || ( door.getFort() ) ) {
            attacker.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.UNABLE_TO_UNLOCK_DOOR ) )
            return
        }

        if ( ( _.random( 100 ) < this.chance ) && !door.isOpen() ) {
            door.openMe()
            return
        }

        attacker.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.FAILED_TO_UNLOCK_DOOR ) )
    }
}