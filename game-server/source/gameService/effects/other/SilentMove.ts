import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { EffectFlag } from '../../enums/EffectFlag'

export class SilentMove extends AbstractEffect {
    effectFlags = EffectFlag.SILENT_MOVE
}