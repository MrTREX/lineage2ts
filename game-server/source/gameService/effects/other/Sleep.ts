import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { EffectFlag } from '../../enums/EffectFlag'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'
import { AIIntent } from '../../aicontroller/enums/AIIntent'

export class Sleep extends AbstractEffect {
    effectFlags = EffectFlag.SLEEP
    effectType = L2EffectType.Sleep

    async onExit( buff: BuffInfo ) {
        let affected = buff.getAffected()
        if ( !affected.isPlayer() ) {
            return AIEffectHelper.scheduleNextIntent( affected, AIIntent.ATTACK )
        }
    }

    async onStart( info: BuffInfo ) {
        let target = info.getAffected()

        return AIEffectHelper.scheduleNextIntent( target, AIIntent.WAITING )
    }
}