import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { PunishmentManager } from '../../instancemanager/PunishmentManager'
import { PunishmentEffect } from '../../models/punishment/PunishmentEffect'
import { PunishmentType } from '../../models/punishment/PunishmentType'
import _ from 'lodash'
import { BlockedAction } from '../../enums/BlockedAction'

export class BlockAction extends AbstractEffect {
    blockedActions: Set<number> = new Set<number>()

    constructor( parameters: Object ) {
        super()
        let value = _.get( parameters, 'blockedActions', 0 )
        if ( _.isNumber( value ) ) {
            this.blockedActions.add( value )
            return
        }

        _.split( value, ',' ).forEach( ( value: string ) => {
            this.blockedActions.add( _.parseInt( value, 10 ) )
        } )
    }

    canStart( info: BuffInfo ): boolean {
        return info.getAffected() && info.getAffected().isPlayer()
    }

    blocksAction( value: BlockedAction ): boolean {
        return this.blockedActions.has( value )
    }

    async onExit( buff: BuffInfo ) {
        if ( this.blockedActions.has( BlockedAction.Party ) ) {
            await PunishmentManager.stopPunishment( buff.getAffected().getObjectId().toString(), PunishmentEffect.CHARACTER, PunishmentType.PARTY_BAN )
        }

        if ( this.blockedActions.has( BlockedAction.Chat ) ) {
            await PunishmentManager.stopPunishment( buff.getAffected().getObjectId().toString(), PunishmentEffect.CHARACTER, PunishmentType.ChatBlocked )
        }
    }

    async onStart( info: BuffInfo ) {
        if ( this.blockedActions.has( BlockedAction.Party ) ) {
            await PunishmentManager.startPunishment( info.getAffected().getObjectId().toString(), PunishmentEffect.CHARACTER, PunishmentType.PARTY_BAN, 0, 'block action debuff', 'BlockAction', true )
        }

        if ( this.blockedActions.has( BlockedAction.Chat ) ) {
            await PunishmentManager.startPunishment( info.getAffected().getObjectId().toString(), PunishmentEffect.CHARACTER, PunishmentType.ChatBlocked, 0, 'block action debuff', 'BlockAction', true )
        }
    }
}