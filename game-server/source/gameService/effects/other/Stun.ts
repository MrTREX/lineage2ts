import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { EffectFlag } from '../../enums/EffectFlag'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { BuffInfo } from '../../models/skills/BuffInfo'

export class Stun extends AbstractEffect {
    effectFlags = EffectFlag.STUNNED
    effectType = L2EffectType.Stun

    async onExit( buff: BuffInfo ) {
        return buff.getAffected().stopStunning( false )
    }

    async onStart( info: BuffInfo ) {
        return info.getAffected().startStunning()
    }
}