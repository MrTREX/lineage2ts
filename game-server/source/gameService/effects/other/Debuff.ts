import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { L2EffectType } from '../../enums/effects/L2EffectType'

export class Debuff extends AbstractEffect {
    effectType = L2EffectType.Debuff
}