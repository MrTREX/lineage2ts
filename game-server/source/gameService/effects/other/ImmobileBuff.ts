import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { BuffInfo } from '../../models/skills/BuffInfo'

export class ImmobileBuff extends AbstractEffect {
    effectType = L2EffectType.Buff

    async onExit( buff: BuffInfo ) {
        buff.getAffected().setIsImmobilized( false )
    }

    async onStart( info: BuffInfo ) {
        info.getAffected().setIsImmobilized( true )
    }
}