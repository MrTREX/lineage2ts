import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2Npc } from '../../models/actor/L2Npc'

export class Grow extends AbstractEffect {
    effectType = L2EffectType.Buff

    canStart( info: BuffInfo ): boolean {
        let target = info.getAffected()
        return target.isNpc()
    }

    async onExit( buff: BuffInfo ) {
        let npc: L2Npc = buff.getAffected() as L2Npc
        npc.setCollisionRadius( npc.getTemplate().getFCollisionRadius() )
    }

    async onStart( info: BuffInfo ) {
        let npc: L2Npc = info.getAffected() as L2Npc
        npc.setCollisionRadius( npc.getTemplate().getCollisionRadiusGrown() )
    }
}