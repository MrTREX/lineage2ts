import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { EffectFlag } from '../../enums/EffectFlag'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2Character } from '../../models/actor/L2Character'
import { Location } from '../../models/Location'
import { FlyToLocation, FlyType } from '../../packets/send/FlyToLocation'
import { ValidateLocation } from '../../packets/send/ValidateLocation'
import _ from 'lodash'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { PathFinding } from '../../../geodata/PathFinding'

export class ThrowUp extends AbstractEffect {
    radius: number
    effectFlags = EffectFlag.STUNNED

    constructor( parameters: Object ) {
        super()
        this.radius = _.parseInt( _.get( parameters, 'flyRadius', '0' ) )
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        let target: L2Character = info.getAffected()
        let character: L2Character = info.getEffector()
        let differenceX = character.getX() - target.getX()
        let differenceY = character.getY() - target.getY()

        let distance = Math.hypot( differenceX, differenceY )
        if ( distance > 2000 ) {
            return
        }

        let offset = Math.min( distance + this.radius, 1400 ) + Math.abs( character.getZ() - target.getZ() )
        if ( offset < 5 ) {
            offset = 5
        }

        if ( distance < 1 ) {
            return
        }

        let angleX = differenceX / distance
        let angleY = differenceY / distance

        let x = Math.floor( character.getX() - ( offset * angleX ) )
        let y = Math.floor( character.getY() - ( offset * angleY ) )
        let z = target.getZ()

        let destination: Location = PathFinding.getApproximateDestination( target, x, y, z )

        BroadcastHelper.dataBasedOnVisibility( target, FlyToLocation( target, destination, FlyType.ThrowUpward ) )
        target.setXYZLocation( destination )
        BroadcastHelper.dataBasedOnVisibility( target, ValidateLocation( target ) )
    }
}