import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { EffectFlag } from '../../enums/EffectFlag'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'

export class Mute extends AbstractEffect {

    effectFlags = EffectFlag.MUTED
    effectType = L2EffectType.Mute

    async onStart( info: BuffInfo ) {
        let target = info.getAffected()
        target.abortCast()

        return AIEffectHelper.notifyAttackedWithTargetId( target, info.getEffectorId(), 1, 1 )
    }
}