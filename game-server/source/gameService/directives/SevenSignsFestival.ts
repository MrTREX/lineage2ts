import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { L2Party } from '../models/L2Party'
import { SevenSignsSide } from '../values/SevenSignsValues'
import { SevenSigns } from './SevenSigns'
import {
    AllFestivalLevels,
    FestivalLevelScores,
    SevenSignsFestivalData,
    SevenSignsFestivalLevel,
    SevenSignsFestivalVariables
} from '../values/SevenSignsFestivalValues'
import { L2World } from '../L2World'
import { ConfigManager } from '../../config/ConfigManager'
import { L2DarknessFestival, L2FestivalRoom } from '../models/L2DarknessFestival'
import aigle from 'aigle'
import { GeneralHelper } from '../helpers/GeneralHelper'
import { ServerVariablesManager } from '../variables/ServerVariablesManager'
import { L2DataApi } from '../../data/interface/l2DataApi'
import { DefaultEnchantLevel } from '../../listeners/helpers/QuestVariables'
import _ from 'lodash'
import { ClanCache } from '../cache/ClanCache'
import { L2Clan } from '../models/L2Clan'
import { CharacterNamesCache } from '../cache/CharacterNamesCache'
import { SystemMessageBuilder } from '../packets/send/SystemMessage'
import { SystemMessageIds } from '../packets/SystemMessageIdValues'
import { ListenerCache } from '../cache/ListenerCache'
import { EventType, SevenSignsFestivalRewardEvent } from '../models/events/EventType'
import { EventPoolCache } from '../cache/EventPoolCache'
import { ServerLog } from '../../logger/Logger'
import Timeout = NodeJS.Timeout

const EmptyHighestScoreData: Readonly<SevenSignsFestivalData> = Object.freeze( {
    side: SevenSignsSide.None,
    date: 0,
    level: 0,
    score: 0,
    members: [],
    rewardedMembers: []
} )

const BloodOfOfferingItemId: number = 5901

class Manager implements L2DataApi {
    isStarted: boolean = false
    startFestivalTimeout: Timeout
    nextFestivalStart: number
    accumulatedBonuses: Record<number, number> = {}

    roomFestivals: { [ key: number ]: L2FestivalRoom } = {}
    dawnFestivalScore: number = 0
    duskFestivalScore: number = 0

    currentDawnScores: Record<number, SevenSignsFestivalData>
    currentDuskScores: Record<number, SevenSignsFestivalData>
    historicalData: Record<number, Readonly<SevenSignsFestivalData>> = {}
    roomToPartyId: Record<number, number> = {}

    getAccumulatedBonus( level: SevenSignsFestivalLevel ): number {
        return this.accumulatedBonuses[ level ] ?? 0
    }

    getHighestScore( type: number, level: SevenSignsFestivalLevel ): number {
        return this.getScoreData( type, level ).score
    }

    getScoreData( type: SevenSignsSide, level: SevenSignsFestivalLevel ): Readonly<SevenSignsFestivalData> {
        let record = type === SevenSignsSide.Dusk ? this.currentDuskScores : this.currentDawnScores
        return record[ level ] ?? EmptyHighestScoreData
    }

    private setScoreData( type: SevenSignsSide, level: SevenSignsFestivalLevel, data: SevenSignsFestivalData ) : void {
        let record = type === SevenSignsSide.Dusk ? this.currentDuskScores : this.currentDawnScores
        record[ level ] = data
    }

    getMinutesToNextFestival(): number {
        if ( SevenSigns.isSealValidationPeriod() ) {
            return -1
        }

        return Math.floor( ( ( this.nextFestivalStart - Date.now() ) / 60000 ) + 1 )
    }

    updateSideScore( side: SevenSignsSide, level: SevenSignsFestivalLevel, score: number, playerIds: ReadonlyArray<number> ): void {
        let dawnScore = this.getHighestScore( SevenSignsSide.Dawn, level )
        let duskScore = this.getHighestScore( SevenSignsSide.Dusk, level )

        let currentSideScore: number
        let oppositeSideScore: number

        if ( side === SevenSignsSide.Dawn ) {
            currentSideScore = dawnScore
            oppositeSideScore = duskScore
        } else {
            currentSideScore = duskScore
            oppositeSideScore = dawnScore
        }

        if ( score > currentSideScore ) {

            if ( currentSideScore > oppositeSideScore ) {
                return
            }

            let festivalData: SevenSignsFestivalData = this.getScoreData( side, level )

            if ( festivalData === EmptyHighestScoreData ) {
                festivalData = {
                    date: 0,
                    level,
                    members: [],
                    rewardedMembers: [],
                    score: 0,
                    side
                }
            }

            festivalData.date = Date.now()
            festivalData.score = score
            festivalData.members = structuredClone( playerIds ) as Array<number>
            festivalData.rewardedMembers.length = 0

            this.setScoreData( side, level, festivalData )

            if ( score > oppositeSideScore ) {
                this.recalculateFestivalScore()
            }

            this.saveScoreData()
        }

        return
    }

    async resetFestivalData(): Promise<void> {
        this.accumulatedBonuses = {}
        this.dawnFestivalScore = 0
        this.duskFestivalScore = 0

        this.updateTopScore()
        this.currentDuskScores = {}
        this.currentDawnScores = {}
        this.roomFestivals = {}

        this.saveScoreData()
        this.saveTopScoreData()
        this.saveBonusData()

        await aigle.resolve( L2World.getAllPlayers() ).eachSeries( ( player: L2PcInstance ) => {
            let amount = player.getInventory().getInventoryItemCount( BloodOfOfferingItemId, DefaultEnchantLevel, false )
            if ( amount > 0 ) {
                return player.destroyItemByItemId( BloodOfOfferingItemId, amount, false, 'SevenSignsFestival' )
            }
        } )

        ServerLog.info( 'SevenSignsFestival: Reset festival data is complete for next competition period.' )
    }

    private updateTopScore(): void {
        for ( const level of AllFestivalLevels ) {
            let duskData = this.getScoreData( SevenSignsSide.Dusk, level )
            let dawnData = this.getScoreData( SevenSignsSide.Dawn, level )
            let bestData = this.historicalData[ level ] ?? EmptyHighestScoreData

            if ( duskData.score > bestData.score ) {
                bestData = duskData
            }

            if ( dawnData.score > bestData.score ) {
                bestData = dawnData
            }

            this.historicalData[ level ] = structuredClone( bestData )
        }
    }

    startFestivalActivities() {
        if ( this.isStarted ) {
            return
        }

        this.isStarted = true
        this.nextFestivalStart = Date.now() + ConfigManager.sevenSigns.getFestivalManagerStart() + this.getFestivalSignupTime()

        setTimeout( () => {
            this.startFestivalTimeout = setTimeout( this.runFestivalTask.bind( this ), ConfigManager.sevenSigns.getFestivalCycleLength() )
        }, ConfigManager.sevenSigns.getFestivalManagerStart() )

        ServerLog.info( `SevenSignsFestival : Festival of Darkness begins now, and runs for ${GeneralHelper.formatMillis( ConfigManager.sevenSigns.getFestivalManagerStart() ).join( ', ' )}` )
    }

    private getFestivalSignupTime(): number {
        return ConfigManager.sevenSigns.getFestivalCycleLength() - ConfigManager.sevenSigns.getFestivalLength() - 60000
    }

    getRoomFestival( side: SevenSignsSide, festivalId: SevenSignsFestivalLevel ): L2FestivalRoom {
        let roomId = this.getRoomId( side, festivalId )
        let room = this.roomFestivals[ roomId ]
        if ( !room ) {
            room = new L2DarknessFestival()
            this.roomFestivals[ roomId ] = room
        }

        return room
    }

    async runFestivalTask(): Promise<void> {
        if ( SevenSigns.isSealValidationPeriod() ) {
            return
        }
        if ( SevenSigns.getTimeToPeriodChange() < ConfigManager.sevenSigns.getFestivalCycleLength() ) {
            return
        }

        // TODO : figure out wait times and implement
    }

    async rewardFestivalWinners( side: SevenSignsSide ): Promise<void> {
        let reputationScore = ConfigManager.clan.getFestivalOfDarknessWin()

        for ( const level of AllFestivalLevels ) {
            let data = this.getScoreData( side, level )

            for ( const memberId of data.members ) {
                let clan : L2Clan = await ClanCache.getClanForMember( memberId )
                if ( !clan ) {
                    continue
                }

                await clan.addReputationScore( reputationScore, true )
                let name = await CharacterNamesCache.getNameById( memberId )
                let packet = new SystemMessageBuilder( SystemMessageIds.CLAN_MEMBER_C1_WAS_IN_HIGHEST_RANKED_PARTY_IN_FESTIVAL_OF_DARKNESS_AND_GAINED_S2_REPUTATION )
                    .addString( name )
                    .addNumber( reputationScore )

                clan.broadcastDataToOnlineMembers( packet.getBuffer() )
            }

            if ( ListenerCache.hasGeneralListener( EventType.SevenSignsFestivalReward ) ) {
                let eventData = EventPoolCache.getData( EventType.SevenSignsFestivalReward ) as SevenSignsFestivalRewardEvent

                eventData.score = data.score
                eventData.side = data.side
                eventData.level = level
                eventData.objectIds = data.members

                await ListenerCache.sendGeneralEvent( EventType.SevenSignsFestivalReward, eventData, level )
            }
        }
    }

    recalculateFestivalScore(): void {
        this.dawnFestivalScore = 0
        this.duskFestivalScore = 0

        for ( const level of AllFestivalLevels ) {
            let duskScore = this.getHighestScore( SevenSignsSide.Dusk, level )
            let dawnScore = this.getHighestScore( SevenSignsSide.Dawn, level )

            if ( duskScore > dawnScore ) {
                if ( duskScore > 0 ) {
                    this.duskFestivalScore += FestivalLevelScores[ level ]
                }
            } else {
                if ( dawnScore > 0 ) {
                    this.dawnFestivalScore += FestivalLevelScores[ level ]
                }
            }
        }
    }

    getCurrentFestivalScore( side: SevenSignsSide ): number {
        switch ( side ) {
            case SevenSignsSide.None:
                return 0
            case SevenSignsSide.Dawn:
                return this.dawnFestivalScore
            case SevenSignsSide.Dusk:
                return this.duskFestivalScore
        }

        return 0
    }

    getTopHistoricalData( festivalId: SevenSignsFestivalLevel ): Readonly<SevenSignsFestivalData> {
        return this.historicalData[ festivalId ] ?? EmptyHighestScoreData
    }

    private getDataForPlayer( objectId: number, festivalId: SevenSignsFestivalLevel ): SevenSignsFestivalData {
        let dawnData = this.getScoreData( SevenSignsSide.Dawn, festivalId )
        if ( dawnData.members.includes( objectId ) ) {
            return dawnData
        }

        let duskData = this.getScoreData( SevenSignsSide.Dusk, festivalId )
        if ( duskData.members.includes( objectId ) ) {
            return duskData
        }

        return null
    }

    hasScore( objectId: number, festivalId: SevenSignsFestivalLevel ): boolean {
        return !!this.getDataForPlayer( objectId, festivalId )
    }

    hasReceivedReward( objectId: number, festivalId: SevenSignsFestivalLevel ): boolean {
        let data = this.getDataForPlayer( objectId, festivalId )

        if ( !data ) {
            return false
        }

        return data.rewardedMembers.includes( objectId )
    }

    recordReceivedReward( objectId: number, festivalId: SevenSignsFestivalLevel ): void {
        let data = this.getDataForPlayer( objectId, festivalId )

        if ( !data ) {
            return
        }

        data.rewardedMembers.push( objectId )

        this.saveScoreData()
    }

    isRoomOccupied( side: SevenSignsSide, festivalId: SevenSignsFestivalLevel ): boolean {
        return !!this.roomToPartyId[ this.getRoomId( side, festivalId ) ]
    }

    private getRoomId( side: SevenSignsSide, festivalId: SevenSignsFestivalLevel ): number {
        return side + ( festivalId << 8 )
    }

    addPartyToEventRoom( side: SevenSignsSide, festivalId: SevenSignsFestivalLevel, party: L2Party ): void {
        this.roomToPartyId[ this.getRoomId( side, festivalId ) ] = party.getId()
    }

    getRoomPartyId( side: SevenSignsSide, festivalId: SevenSignsFestivalLevel ) : number {
        return this.roomToPartyId[ this.getRoomId( side, festivalId ) ]
    }

    increaseBonus( festivalId: SevenSignsFestivalLevel, amount: number ): void {
        this.accumulatedBonuses[ festivalId ] = this.getAccumulatedBonus( festivalId ) + amount
        this.saveBonusData()
    }

    async load(): Promise<Array<string>> {
        this.currentDawnScores = ServerVariablesManager.getValue( SevenSignsFestivalVariables.DawnScores ) ?? {}
        this.currentDuskScores = ServerVariablesManager.getValue( SevenSignsFestivalVariables.DuskScores ) ?? {}
        this.historicalData = ServerVariablesManager.getValue( SevenSignsFestivalVariables.HistoricalData ) ?? {}

        this.recalculateFestivalScore()

        if ( SevenSigns.isCompetitionPeriod() ) {
            this.startFestivalActivities()
        }

        return [
            `SevenSignsFestival : loaded ${ _.size( this.historicalData ) } all time top scores`,
            `SevenSignsFestival : loaded ${ _.size( this.currentDuskScores ) } dusk scores`,
            `SevenSignsFestival : loaded ${ _.size( this.currentDawnScores ) } dawn scores`,
        ]
    }

    saveScoreData(): void {
        ServerVariablesManager.setValue( SevenSignsFestivalVariables.DawnScores, this.currentDawnScores )
        ServerVariablesManager.setValue( SevenSignsFestivalVariables.DuskScores, this.currentDuskScores )
    }

    saveTopScoreData(): void {
        ServerVariablesManager.setValue( SevenSignsFestivalVariables.HistoricalData, this.historicalData )
    }

    saveBonusData(): void {
        ServerVariablesManager.setValue( SevenSignsFestivalVariables.Bonuses, this.accumulatedBonuses )
    }
}

export const SevenSignsFestival = new Manager()