import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../packets/send/SystemMessage'
import { SystemMessageIds } from '../packets/SystemMessageIdValues'
import { DatabaseManager } from '../../database/manager'
import { L2DataApi } from '../../data/interface/l2DataApi'
import { CastleManager } from '../instancemanager/CastleManager'
import { Castle } from '../models/entity/Castle'
import { BroadcastHelper } from '../helpers/BroadcastHelper'
import { SSQInfo } from '../packets/send/SSQInfo'
import { L2SevenSignsCharacterData } from '../../database/interface/SevenSignsTableApi'
import moment from 'moment'
import { L2World } from '../L2World'
import { TeleportWhereType } from '../enums/TeleportWhereType'
import { CommonSkill } from '../models/holders/SkillHolder'
import { AllSeals, SevenSignsPeriod, SevenSignsSeal, SevenSignsSide, SevenSignsValues } from '../values/SevenSignsValues'
import { SevenSignsFestival } from './SevenSignsFestival'
import { ConfigManager } from '../../config/ConfigManager'
import _, { DebouncedFunc } from 'lodash'
import aigle from 'aigle'
import { ServerVariablesManager } from '../variables/ServerVariablesManager'
import { RoaringBitmap32 } from 'roaring'
import {
    EnumToSide,
    L2SevenSignsCharacterSide,
    NormalSealName,
    PeriodNames,
    ShortSealName,
    SideToName
} from '../enums/SevenSigns'
import schedule, { Job } from 'node-schedule'
import parser from 'cron-parser'
import { ServerLog } from '../../logger/Logger'

const periodToMessageId : Record<number, number> = {
    [ SevenSignsPeriod.Recruiting ]: SystemMessageIds.PREPARATIONS_PERIOD_BEGUN,
    [ SevenSignsPeriod.Competing ]: SystemMessageIds.COMPETITION_PERIOD_BEGUN,
    [ SevenSignsPeriod.CompetitionResults ]: SystemMessageIds.RESULTS_PERIOD_BEGUN,
    [ SevenSignsPeriod.SealValidation ]: SystemMessageIds.VALIDATION_PERIOD_BEGUN
}

interface L2SevenSignsStatusData {
    currentCycle: number
    activePeriod: SevenSignsPeriod
    previousWinner: SevenSignsSide

    dawnStoneScore: number
    dawnFestivalScore: number

    duskStoneScore: number
    duskFestivalScore: number

    avariceSealOwners: number
    gnosisSealOwners: number
    strifeSealOwners: number

    avariceDawnScore: number
    gnosisDawnScore: number
    strifeDawnScore: number

    avariceDuskScore: number
    gnosisDuskScore: number
    strifeDuskScore: number

    lastSavedDate: number
}

class Manager implements L2DataApi {
    private activePeriod: SevenSignsPeriod = SevenSignsPeriod.Recruiting
    private currentCycle: number = 0
    private stoneScores: Record<number, number>

    private previousWinner: SevenSignsSide
    private lastSaveDate: number = Date.now()
    private playerData: Record<number, L2SevenSignsCharacterData> = {}
    private sealWinners: Record<number, SevenSignsSide> = {}
    private sideTotalMembers: Record<number, number> = {}
    private duskTotals: Map<SevenSignsSeal, number> = new Map<SevenSignsSeal, number>()
    private dawnTotals: Map<SevenSignsSeal, number> = new Map<SevenSignsSeal, number>()
    private periodChangeJob: Job

    private idsToUpdate: RoaringBitmap32 = new RoaringBitmap32()
    private readonly debouncePlayerUpdate: DebouncedFunc<() => void>

    constructor() {
        this.debouncePlayerUpdate = _.debounce( this.runPlayerDataUpdate.bind( this ), 15000, {
            maxWait: 30000
        } )
    }

    private calculateSealWinnerSides(): void {
        let totalDawnMembers = this.getTotalMembers( SevenSignsSide.Dawn ) === 0 ? 1 : this.getTotalMembers( SevenSignsSide.Dawn )
        let totalDuskMembers = this.getTotalMembers( SevenSignsSide.Dusk ) === 0 ? 1 : this.getTotalMembers( SevenSignsSide.Dusk )
        let projectedWinningSide = this.getProjectedWinningSide()

        for ( const seal of AllSeals ) {
            this.calculateSeal( seal, totalDawnMembers, totalDuskMembers, projectedWinningSide )
        }
    }

    calculateSeal( seal: SevenSignsSeal, totalDawnMembers: number, totalDuskMembers: number, projectedWinningSide: SevenSignsSide ): void {
        let previousSealSide = this.sealWinners[ seal ] ?? SevenSignsSide.None
        let newSealSide = SevenSignsSide.None
        let dawnProportion = this.getSealTotal( seal, SevenSignsSide.Dawn )

        let dawnPercent = Math.round( ( dawnProportion / totalDawnMembers ) * 100 )
        let duskProportion = this.getSealTotal( seal, SevenSignsSide.Dusk )
        let duskPercent = Math.round( ( duskProportion / totalDuskMembers ) * 100 )

        /*
            If a Seal was already closed or owned by the opponent and the new winner wants to assume ownership of the Seal, 35% or more of the members of the Cabal must have chosen the Seal.
            If they chose less than 35%, they cannot own the Seal.
            If the Seal was owned by the winner in the previous Seven Signs, they can retain that seal if 10% or more members have chosen it.
            If they want to possess a new Seal, at least 35% of the members of the Cabal must have chosen the new Seal.
         */
        switch ( previousSealSide ) {
            case SevenSignsSide.None:
                switch ( projectedWinningSide ) {
                    case SevenSignsSide.None:
                        newSealSide = SevenSignsSide.None
                        break

                    case SevenSignsSide.Dawn:
                        if ( dawnPercent >= 35 ) {
                            newSealSide = SevenSignsSide.Dawn
                        } else {
                            newSealSide = SevenSignsSide.None
                        }
                        break

                    case SevenSignsSide.Dusk:
                        if ( duskPercent >= 35 ) {
                            newSealSide = SevenSignsSide.Dusk
                        } else {
                            newSealSide = SevenSignsSide.None
                        }
                        break
                }
                break

            case SevenSignsSide.Dawn:
                switch ( projectedWinningSide ) {
                    case SevenSignsSide.None:
                        if ( dawnPercent >= 10 ) {
                            newSealSide = SevenSignsSide.Dawn
                        } else {
                            newSealSide = SevenSignsSide.None
                        }
                        break

                    case SevenSignsSide.Dawn:
                        if ( dawnPercent >= 10 ) {
                            newSealSide = SevenSignsSide.Dawn
                        } else {
                            newSealSide = SevenSignsSide.None
                        }
                        break

                    case SevenSignsSide.Dusk:
                        if ( duskPercent >= 35 ) {
                            newSealSide = SevenSignsSide.Dusk
                        } else if ( dawnPercent >= 10 ) {
                            newSealSide = SevenSignsSide.Dawn
                        } else {
                            newSealSide = SevenSignsSide.None
                        }
                        break
                }
                break

            case SevenSignsSide.Dusk:
                switch ( projectedWinningSide ) {
                    case SevenSignsSide.None:
                        if ( duskPercent >= 10 ) {
                            newSealSide = SevenSignsSide.Dusk
                        } else {
                            newSealSide = SevenSignsSide.None
                        }
                        break

                    case SevenSignsSide.Dawn:
                        if ( dawnPercent >= 35 ) {
                            newSealSide = SevenSignsSide.Dawn
                        } else if ( duskPercent >= 10 ) {
                            newSealSide = SevenSignsSide.Dusk
                        } else {
                            newSealSide = SevenSignsSide.None
                        }
                        break

                    case SevenSignsSide.Dusk:
                        if ( duskPercent >= 10 ) {
                            newSealSide = SevenSignsSide.Dusk
                        } else {
                            newSealSide = SevenSignsSide.None
                        }
                        break
                }
                break
        }

        this.sealWinners[ seal ] = newSealSide

        switch ( seal ) {
            case SevenSignsSeal.Avarice:
                if ( newSealSide === SevenSignsSide.Dawn ) {
                    this.sendMessageToAll( SystemMessageIds.DAWN_OBTAINED_AVARICE )
                } else if ( newSealSide === SevenSignsSide.Dusk ) {
                    this.sendMessageToAll( SystemMessageIds.DUSK_OBTAINED_AVARICE )
                }
                break

            case SevenSignsSeal.Gnosis:
                if ( newSealSide === SevenSignsSide.Dawn ) {
                    this.sendMessageToAll( SystemMessageIds.DAWN_OBTAINED_GNOSIS )
                } else if ( newSealSide === SevenSignsSide.Dusk ) {
                    this.sendMessageToAll( SystemMessageIds.DUSK_OBTAINED_GNOSIS )
                }
                break

            case SevenSignsSeal.Strife:
                if ( newSealSide === SevenSignsSide.Dawn ) {
                    this.sendMessageToAll( SystemMessageIds.DAWN_OBTAINED_STRIFE )
                } else if ( newSealSide === SevenSignsSide.Dusk ) {
                    this.sendMessageToAll( SystemMessageIds.DUSK_OBTAINED_STRIFE )
                }

                CastleManager.validateTaxes( newSealSide )
                break
        }

    }

    getProjectedWinningSide(): SevenSignsSide {
        let duskScore = this.getCurrentScore( SevenSignsSide.Dusk )
        let dawnScore = this.getCurrentScore( SevenSignsSide.Dawn )

        if ( duskScore === dawnScore ) {
            return SevenSignsSide.None
        }

        if ( duskScore > dawnScore ) {
            return SevenSignsSide.Dusk
        }

        return SevenSignsSide.Dawn
    }

    getSideName( cabal: SevenSignsSide ): string {
        return SideToName[ cabal ] ?? 'No Cabal'
    }

    getSideShortName( type: SevenSignsSide ): string {
        return EnumToSide[ type ] ?? L2SevenSignsCharacterSide.None
    }

    getCurrentCycle(): number {
        return this.currentCycle
    }

    getCurrentPeriod(): SevenSignsPeriod {
        return this.activePeriod
    }

    getCurrentPeriodName(): string {
        return PeriodNames[ this.activePeriod ] ?? 'No Active Period'
    }

    getCurrentScore( side: SevenSignsSide ): number {
        let dawnScore = this.stoneScores[ SevenSignsSide.Dawn ]
        let duskScore = this.stoneScores[ SevenSignsSide.Dusk ]
        let totalStoneScore = dawnScore + duskScore

        switch ( side ) {
            case SevenSignsSide.None:
                return 0
            case SevenSignsSide.Dawn:
                return Math.round( ( dawnScore / ( totalStoneScore === 0 ? 1 : totalStoneScore ) ) * 500 ) + SevenSignsFestival.dawnFestivalScore
            case SevenSignsSide.Dusk:
                return Math.round( ( duskScore / ( totalStoneScore === 0 ? 1 : totalStoneScore ) ) * 500 ) + SevenSignsFestival.duskFestivalScore
        }

        return 0
    }

    getCurrentStoneScore( side: SevenSignsSide ): number {
        return this.stoneScores[ side ] ?? 0
    }

    getPlayerAdenaCollect( objectId: number ): number {
        let data: L2SevenSignsCharacterData = this.playerData[ objectId ]
        if ( !data ) {
            return 0
        }

        return data.ancientAdenaCount
    }

    getPlayerSide( objectId: number ): SevenSignsSide {
        let data: L2SevenSignsCharacterData = this.playerData[ objectId ]
        if ( !data ) {
            return SevenSignsSide.None
        }

        return data.side
    }

    getPlayerSeal( objectId: number ): SevenSignsSeal {
        let data: L2SevenSignsCharacterData = this.playerData[ objectId ]
        if ( !data ) {
            return SevenSignsSeal.None
        }

        return data.seal
    }

    getPlayerStoneContribution( objectId: number ): number {
        let data: L2SevenSignsCharacterData = this.playerData[ objectId ]
        if ( !data ) {
            return 0
        }

        return data.redStoneCount + data.greenStoneCount + data.blueStoneCount
    }

    getWinningSealSide( seal: SevenSignsSeal ): SevenSignsSide {
        return this.sealWinners[ seal ] ?? SevenSignsSide.None
    }

    getSealTotal( seal: SevenSignsSeal, side: SevenSignsSide ): number {
        switch ( side ) {
            case SevenSignsSide.None:
                return 0

            case SevenSignsSide.Dusk:
                return this.duskTotals.get( seal ) ?? 0

            case SevenSignsSide.Dawn:
                return this.dawnTotals.get( seal ) ?? 0
        }

        return 0
    }

    getTimeToPeriodChange() {
        return this.periodChangeJob.nextInvocation().getTime() - Date.now()
    }

    getTotalMembers( type: SevenSignsSide ): number {
        return this.sideTotalMembers[ type ] ?? 0
    }

    private async addPeriodSkills( player: L2PcInstance ): Promise<unknown> {
        if ( !this.hasWinningSide( player.getObjectId() ) ) {
            return
        }

        if ( this.previousWinner === this.getWinningSealSide( SevenSignsSeal.Strife )
            && this.getPlayerSeal( player.getObjectId() ) === SevenSignsSeal.Strife ) {
            return player.addSkill( CommonSkill.THE_VICTOR_OF_WAR.getSkill() )
        }

        return player.addSkill( CommonSkill.THE_VANQUISHED_OF_WAR.getSkill() )
    }

    hasWinningSide( objectId: number ) : boolean {
        return this.getPlayerSide( objectId ) === this.previousWinner
    }

    isCompareResultsPeriod(): boolean {
        return this.activePeriod === SevenSignsPeriod.CompetitionResults
    }

    isNextPeriodChangeInPast(): boolean {
        let previousTime : number

        switch ( this.getCurrentPeriod() ) {
            case SevenSignsPeriod.SealValidation:
            case SevenSignsPeriod.Competing:
                let nextChangeCron = parser.parseExpression( ConfigManager.sevenSigns.getPeriodStartCron() )
                previousTime = nextChangeCron.prev().toDate().valueOf()
                break

            case SevenSignsPeriod.Recruiting:
                previousTime = Date.now() - ConfigManager.sevenSigns.getInitialPeriodDuration()
                break

            case SevenSignsPeriod.CompetitionResults:
                previousTime = Date.now() - ConfigManager.sevenSigns.getCompetitionPeriodDuration()
                break
        }

        return this.lastSaveDate > 0 && this.lastSaveDate < previousTime
    }

    isSealValidationPeriod(): boolean {
        return this.activePeriod === SevenSignsPeriod.SealValidation
    }

    async load(): Promise<Array<string>> {
        let logs: Array<string> = []

        await this.restoreData()

        logs.push( `Currently in the ${ this.getCurrentPeriodName() } period.` )

        if ( this.isSealValidationPeriod() ) {
            if ( this.getWinnerSide() === SevenSignsSide.None ) {
                logs.push( 'The competition ended with a tie last week.' )
            } else {
                logs.push( `The ${ this.getSideName( this.getWinnerSide() ) } were victorious last week.` )
            }
        } else if ( this.getProjectedWinningSide() === SevenSignsSide.None ) {
            logs.push( 'The competition, if the current trend continues, will end in a tie this week.' )
        } else {
            logs.push( `The ${ this.getSideName( this.getProjectedWinningSide() ) } are in the lead this week.` )
        }

        this.scheduleNextPeriodChange()

        if ( this.isNextPeriodChangeInPast() ) {
            logs.push( 'Next period change was in the past (server was offline), running period change.' )
            this.periodChangeJob.cancel()
            this.periodChangeJob.schedule( Date.now() + 3000 )
        }

        return logs.map( ( line: string ): string => {
            return `SevenSigns: ${ line }`
        } )
    }

    private async removePeriodSkills( player: L2PcInstance ) : Promise<void> {
        if ( player.getKnownSkill( CommonSkill.THE_VICTOR_OF_WAR.getId() ) ) {
            await player.removeSkill( CommonSkill.THE_VICTOR_OF_WAR.getSkill() )
        }

        if ( player.getKnownSkill( CommonSkill.THE_VANQUISHED_OF_WAR.getId() ) ) {
            await player.removeSkill( CommonSkill.THE_VANQUISHED_OF_WAR.getSkill() )
        }
    }

    resetSealCounts(): void {
        this.duskTotals.set( SevenSignsSeal.Avarice, 0 )
        this.duskTotals.set( SevenSignsSeal.Gnosis, 0 )
        this.duskTotals.set( SevenSignsSeal.Strife, 0 )

        this.dawnTotals.set( SevenSignsSeal.Avarice, 0 )
        this.dawnTotals.set( SevenSignsSeal.Gnosis, 0 )
        this.dawnTotals.set( SevenSignsSeal.Strife, 0 )
    }

    async restoreData(): Promise<void> {
        this.playerData = {}
        this.sealWinners = {}
        this.stoneScores = {}
        this.dawnTotals.clear()
        this.duskTotals.clear()

        let statusData: L2SevenSignsStatusData = ServerVariablesManager.getValue( SevenSignsValues.StatusVariableName )
        if ( !statusData ) {
            statusData = this.getDefaultStatusData()
            ServerVariablesManager.setValue( SevenSignsValues.StatusVariableName, statusData )
        }

        this.activePeriod = statusData.activePeriod ?? SevenSignsPeriod.Recruiting
        this.currentCycle = statusData.currentCycle ?? 1
        this.previousWinner = statusData.previousWinner ?? SevenSignsSide.None

        this.stoneScores[ SevenSignsSide.Dawn ] = statusData.dawnStoneScore ?? 0
        SevenSignsFestival.dawnFestivalScore = statusData.dawnFestivalScore

        this.stoneScores[ SevenSignsSide.Dusk ] = statusData.duskStoneScore ?? 0
        SevenSignsFestival.duskFestivalScore = statusData.duskFestivalScore

        this.sealWinners[ SevenSignsSeal.Avarice ] = statusData.avariceSealOwners ?? 0
        this.sealWinners[ SevenSignsSeal.Gnosis ] = statusData.gnosisSealOwners ?? 0
        this.sealWinners[ SevenSignsSeal.Strife ] = statusData.strifeSealOwners ?? 0

        this.duskTotals.set( SevenSignsSeal.Avarice, statusData.avariceDuskScore ?? 0 )
        this.duskTotals.set( SevenSignsSeal.Gnosis, statusData.gnosisDuskScore ?? 0 )
        this.duskTotals.set( SevenSignsSeal.Strife, statusData.strifeDuskScore ?? 0 )

        this.dawnTotals.set( SevenSignsSeal.Avarice, statusData.avariceDawnScore ?? 0 )
        this.dawnTotals.set( SevenSignsSeal.Gnosis, statusData.gnosisDawnScore ?? 0 )
        this.dawnTotals.set( SevenSignsSeal.Strife, statusData.strifeDawnScore ?? 0 )

        this.sideTotalMembers = await DatabaseManager.getSevenSigns().getSideTotals()
    }

    async runSevenSignsPeriodChange(): Promise<void> {
        switch ( this.activePeriod ) {
            case SevenSignsPeriod.Recruiting:
                this.activePeriod = SevenSignsPeriod.Competing

                SevenSignsFestival.startFestivalActivities()
                this.sendMessageToAll( SystemMessageIds.QUEST_EVENT_PERIOD_BEGUN )
                break

            case SevenSignsPeriod.Competing:
                this.activePeriod = SevenSignsPeriod.CompetitionResults
                this.sendMessageToAll( SystemMessageIds.QUEST_EVENT_PERIOD_ENDED )

                let side = this.getProjectedWinningSide()

                // TODO : decouple festival timer via listener events
                clearInterval( SevenSignsFestival.startFestivalTimeout )
                await SevenSignsFestival.rewardFestivalWinners( side )

                this.calculateSealWinnerSides()

                switch ( side ) {
                    case SevenSignsSide.Dawn:
                        this.sendMessageToAll( SystemMessageIds.DAWN_WON )
                        break

                    case SevenSignsSide.Dusk:
                        this.sendMessageToAll( SystemMessageIds.DUSK_WON )
                        break
                }

                this.previousWinner = side

                CastleManager.getCastles().forEach( ( castle: Castle ) => {
                    castle.setTicketBuyCount( 0 )
                } )

                break

            case SevenSignsPeriod.CompetitionResults:
                this.activePeriod = SevenSignsPeriod.SealValidation
                await aigle.resolve( L2World.getAllPlayers() ).eachSeries( player => this.addPeriodSkills( player ) )
                this.sendMessageToAll( SystemMessageIds.SEAL_VALIDATION_PERIOD_BEGUN )

                ServerLog.info( `SevenSigns : the ${ this.getSideName( this.previousWinner ) } have won the competition with ${ this.getCurrentScore( this.previousWinner ) } points!` )
                break

            case SevenSignsPeriod.SealValidation:

                this.activePeriod = SevenSignsPeriod.Recruiting
                this.sendMessageToAll( SystemMessageIds.SEAL_VALIDATION_PERIOD_ENDED )
                await aigle.resolve( L2World.getAllPlayers() ).eachSeries( player => this.removePeriodSkills( player ) )
                await DatabaseManager.getSevenSigns().removeAll()
                this.resetSealCounts()

                this.currentCycle++

                await SevenSignsFestival.resetFestivalData()

                this.stoneScores = {}
                this.playerData = {}

                break
        }

        this.saveSevenSignsStatus()

        await this.teleportLosingPlayersFromDungeons( this.isSealValidationPeriod() ? this.getWinnerSide() : this.getProjectedWinningSide() )

        BroadcastHelper.toAllOnlinePlayersData( SSQInfo() )

        ServerLog.info( `SevenSigns : the ${ this.getCurrentPeriodName() } period has begun!` )

        this.scheduleNextPeriodChange()
    }

    setPeriod( period: SevenSignsPeriod ) : Promise<void> {
        switch ( period ) {
            case SevenSignsPeriod.Competing:
                this.activePeriod = SevenSignsPeriod.Recruiting
                break

            case SevenSignsPeriod.CompetitionResults:
                this.activePeriod = SevenSignsPeriod.Competing
                break

            case SevenSignsPeriod.SealValidation:
                this.activePeriod = SevenSignsPeriod.CompetitionResults
                break

            case SevenSignsPeriod.Recruiting:
                this.activePeriod = period
                break

            default:
                return
        }

        return this.runSevenSignsPeriodChange()
    }

    saveSevenSignsStatus(): void {
        this.lastSaveDate = Date.now()

        let statusData: L2SevenSignsStatusData = {
            currentCycle: this.currentCycle,
            activePeriod: this.activePeriod,
            previousWinner: this.previousWinner,

            dawnStoneScore: this.stoneScores[ SevenSignsSide.Dawn ],
            dawnFestivalScore: SevenSignsFestival.dawnFestivalScore,

            duskStoneScore: this.stoneScores[ SevenSignsSide.Dusk ],
            duskFestivalScore: SevenSignsFestival.duskFestivalScore,

            avariceSealOwners: this.sealWinners[ SevenSignsSeal.Avarice ],
            gnosisSealOwners: this.sealWinners[ SevenSignsSeal.Gnosis ],
            strifeSealOwners: this.sealWinners[ SevenSignsSeal.Strife ],

            avariceDawnScore: this.dawnTotals.get( SevenSignsSeal.Avarice ),
            gnosisDawnScore: this.dawnTotals.get( SevenSignsSeal.Gnosis ),
            strifeDawnScore: this.dawnTotals.get( SevenSignsSeal.Strife ),

            avariceDuskScore: this.duskTotals.get( SevenSignsSeal.Avarice ),
            gnosisDuskScore: this.duskTotals.get( SevenSignsSeal.Gnosis ),
            strifeDuskScore: this.duskTotals.get( SevenSignsSeal.Strife ),

            lastSavedDate: this.lastSaveDate,
        }

        ServerVariablesManager.setValue( SevenSignsValues.StatusVariableName, statusData )
    }

    private getPeriodMessageId(): number {
        return periodToMessageId[ this.activePeriod ]
    }

    sendCurrentPeriodMesssage( player: L2PcInstance ) {
        let messageId = this.getPeriodMessageId()
        if ( messageId ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( messageId ) )
        }
    }

    sendMessageToAll( id: number ): void {
        BroadcastHelper.toAllOnlinePlayersData( SystemMessageBuilder.fromMessageId( id ) )
    }

    scheduleNextPeriodChange(): void {
        if ( this.periodChangeJob ) {
            this.periodChangeJob.cancel()
        }

        switch ( this.getCurrentPeriod() ) {
            case SevenSignsPeriod.SealValidation:
            case SevenSignsPeriod.Competing:
                this.periodChangeJob = schedule.scheduleJob( ConfigManager.sevenSigns.getPeriodStartCron(), this.runSevenSignsPeriodChange.bind( this ) )
                break

            case SevenSignsPeriod.Recruiting:
                this.periodChangeJob = schedule.scheduleJob( Date.now() + ConfigManager.sevenSigns.getInitialPeriodDuration(), this.runSevenSignsPeriodChange.bind( this ) )
                break

            case SevenSignsPeriod.CompetitionResults:
                this.periodChangeJob = schedule.scheduleJob( Date.now() + ConfigManager.sevenSigns.getCompetitionPeriodDuration(), this.runSevenSignsPeriodChange.bind( this ) )
                break
        }

        ServerLog.info( `SevenSigns: Next period change set to ${ this.getNextPeriodDate() }` )
    }

    getNextPeriodDate() : string {
        return moment( this.periodChangeJob.nextInvocation().getTime() ).format( 'dddd, MMMM Do YYYY, h:mm:ss a Z' )
    }

    async teleportLosingPlayersFromDungeons( side: SevenSignsSide ): Promise<unknown> {
        const isSealValidation = this.isSealValidationPeriod() || this.isCompareResultsPeriod()

        // TODO : avoid getting all players, track only those that are in seven signs dungeon, perhaps through areas
        return aigle.resolve( L2World.getAllPlayers() ).eachSeries( ( player: L2PcInstance ) => {
            let data: L2SevenSignsCharacterData = this.playerData[ player.getObjectId() ]

            if ( isSealValidation ) {
                if ( !player.isGM() && player.isIn7sDungeon() && ( !data || data.side !== side ) ) {
                    player.setIsIn7sDungeon( false )
                    player.sendMessage( 'You have been teleported to the nearest town due to the beginning of the Seal Validation period.' )
                    return player.teleportToLocationType( TeleportWhereType.TOWN )
                }

                return
            }

            if ( !player.isGM() && player.isIn7sDungeon() && ( !data || !_.isEmpty( data.side ) ) ) {
                player.setIsIn7sDungeon( false )
                player.sendMessage( 'You have been teleported to the nearest town because you have not signed for any cabal.' )
                return player.teleportToLocationType( TeleportWhereType.TOWN )
            }
        } )
    }

    isCompetitionPeriod(): boolean {
        return this.activePeriod === SevenSignsPeriod.Competing
    }

    getPlayerContributionScore( objectId: number ): number {
        let data = this.playerData[ objectId ]
        if ( !data ) {
            return 0
        }

        return data.contributionScore
    }

    addPlayerStoneContribution( objectId: number, blue: number, green: number, red: number ): number {
        let data: L2SevenSignsCharacterData = this.playerData[ objectId ]

        /*
            We cannot dynamically create new player data since one must register for side and seal.
         */
        if ( !data ) {
            return 0
        }

        let score = this.calculateContributionScore( blue, green, red )
        let ancientAdena = data.ancientAdenaCount + this.calculateAncientAdenaReward( blue, green, red )
        let totalScore = Math.min( data.contributionScore + score, ConfigManager.sevenSigns.getMaxPlayerContribution() )

        data.blueStoneCount = Math.max( 0, data.blueStoneCount + blue )
        data.greenStoneCount = Math.max( 0, data.greenStoneCount + green )
        data.redStoneCount = Math.max( 0, data.redStoneCount + red )
        data.ancientAdenaCount = Math.max( 0, ancientAdena )
        data.contributionScore = Math.max( 0, totalScore )
        data.lastContributionTime = Date.now()

        this.updateScores( data.side, score )
        this.schedulePlayerUpdate( objectId )

        return score
    }

    private schedulePlayerUpdate( objectId: number ) : void {
        this.idsToUpdate.add( objectId )
        this.debouncePlayerUpdate()
    }

    calculateContributionScore( blue: number, green: number, red: number ): number {
        return blue * ConfigManager.sevenSigns.getBlueStoneContributionPoints()
            + green * ConfigManager.sevenSigns.getGreenStoneContributionPoints()
            + red * ConfigManager.sevenSigns.getRedStoneContributionPoints()
    }

    calculateAncientAdenaReward( blue: number, green: number, red: number ): number {
        return blue * ConfigManager.sevenSigns.getBlueStoneAAReward()
            + green * ConfigManager.sevenSigns.getGreenStoneAAReward()
            + red * ConfigManager.sevenSigns.getRedStoneAAReward()
    }

    updatePlayerInformation( objectId: number, side: SevenSignsSide, seal: SevenSignsSeal ): void {
        let data: L2SevenSignsCharacterData = this.playerData[ objectId ]

        if ( data ) {
            if ( data.side !== side ) {
                /*
                    If for some reason a player is switching sides, we need to ensure totals are kept properly.
                 */
                this.updateTotals( data.side, seal, -1 )
            }

            data.side = side
            data.seal = seal
            data.joinTime = Date.now()
        } else {
            data = {
                ancientAdenaCount: 0,
                blueStoneCount: 0,
                side,
                contributionScore: 0,
                greenStoneCount: 0,
                objectId,
                redStoneCount: 0,
                seal,
                lastContributionTime: 0,
                joinTime: Date.now()
            }

            this.playerData[ objectId ] = data
        }

        this.updateTotals( data.side, data.seal, 1 )
        return this.schedulePlayerUpdate( objectId )
    }

    private updateTotals( side: SevenSignsSide, seal: SevenSignsSeal, modifier: number ) : void {
        if ( side === SevenSignsSide.Dawn ) {
            this.dawnTotals.set( seal, Math.max( this.dawnTotals.get( seal ) + modifier, 0 ) )
        } else {
            this.duskTotals.set( seal, Math.max( this.duskTotals.get( seal ) + modifier, 0 ) )
        }

        this.sideTotalMembers[ side ] = Math.max( this.sideTotalMembers[ side ] + modifier, 0 )
    }

    private updateScores( side: SevenSignsSide, modifier: number ) : void {
        let currentScore = this.stoneScores[ side ] ?? 0
        this.stoneScores[ side ] = Math.max( 0, modifier + currentScore )
    }

    getSealName( seal: SevenSignsSeal, useShortName: boolean = false ): string {
        return useShortName ? ShortSealName[ seal ] : NormalSealName[ seal ]
    }

    getAncientAdenaReward( objectId: number, shouldRemoveReward: boolean ): number {
        let data: L2SevenSignsCharacterData = this.playerData[ objectId ]
        if ( !data ) {
            return 0
        }

        let rewardAmount = data.ancientAdenaCount

        if ( shouldRemoveReward ) {
            data.redStoneCount = 0
            data.greenStoneCount = 0
            data.blueStoneCount = 0
            data.ancientAdenaCount = 0

            this.schedulePlayerUpdate( objectId )
        }

        return rewardAmount
    }

    private getDefaultStatusData() : L2SevenSignsStatusData {
        return {
            activePeriod: SevenSignsPeriod.Recruiting,
            avariceDawnScore: 0,
            avariceDuskScore: 0,
            avariceSealOwners: 0,
            currentCycle: 0,
            dawnFestivalScore: 0,
            dawnStoneScore: 0,
            duskFestivalScore: 0,
            duskStoneScore: 0,
            gnosisDawnScore: 0,
            gnosisDuskScore: 0,
            gnosisSealOwners: 0,
            lastSavedDate: 0,
            previousWinner: SevenSignsSide.None,
            strifeDawnScore: 0,
            strifeDuskScore: 0,
            strifeSealOwners: 0
        }
    }

    runPlayerDataUpdate() : Promise<void> {
        this.saveSevenSignsStatus()

        if ( this.idsToUpdate.size === 0 ) {
            return
        }

        let itemsToUpdate : Array<L2SevenSignsCharacterData> = []
        for ( const objectId of this.idsToUpdate ) {
            let data = this.playerData[ objectId ]
            if ( data ) {
                itemsToUpdate.push( data )
            }
        }

        if ( itemsToUpdate.length === 0 ) {
            return
        }

        return DatabaseManager.getSevenSigns().savePlayers( itemsToUpdate )
    }

    getParticipationAmount() : number {
        return _.size( this.playerData )
    }

    getLastUpdateDate() : number {
        return this.lastSaveDate
    }

    getTotals( side: SevenSignsSide ) : ReadonlyMap<SevenSignsSeal, number> {
        return side === SevenSignsSide.Dawn ? this.dawnTotals : this.duskTotals
    }

    getPlayerData( objectId: number ) : Readonly<L2SevenSignsCharacterData> {
        return this.playerData[ objectId ]
    }

    async onLogin( player: L2PcInstance ) : Promise<unknown> {
        await this.loadPlayerData( player.getObjectId() )
        await this.removePeriodSkills( player )

        if ( this.isSealValidationPeriod() ) {
            return this.addPeriodSkills( player )
        }
    }

    onLogout( player: L2PcInstance ) : void {
        delete this.playerData[ player.getObjectId() ]
    }

    private async loadPlayerData( objectId: number ) : Promise<void> {
        let data = await DatabaseManager.getSevenSigns().getPlayer( objectId )
        if ( !data ) {
            return
        }

        this.playerData[ objectId ] = data
    }

    getWinnerSide() : SevenSignsSide {
        return this.previousWinner
    }

    resetPlayerContribution( objectId: number ) : void {
        let data = this.playerData[ objectId ]
        if ( !data ) {
            return
        }

        this.updateScores( data.side, -data.contributionScore )

        data.blueStoneCount = 0
        data.greenStoneCount = 0
        data.blueStoneCount = 0
        data.ancientAdenaCount = 0
        data.contributionScore = 0

        this.schedulePlayerUpdate( objectId )
    }
}

export const SevenSigns = new Manager()