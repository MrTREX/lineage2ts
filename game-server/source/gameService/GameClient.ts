import { Socket } from 'net'
import { GameCrypt } from './packets/GameCrypt'
import { AuthorizedLogin, AuthorizedLoginData } from './packets/receive/AuthorizedLogin'
import { SessionKey } from './interface/SessionKey'
import { Logout } from './packets/manager/Logout'
import { CharacterCreate } from './packets/manager/CharacterCreate'
import { L2PcInstance } from './models/actor/instance/L2PcInstance'
import { L2World } from './L2World'
import { DatabaseManager } from '../database/manager'
import { ConfigManager } from '../config/ConfigManager'
import { CharacterDelete, CharacterDeleteOutcome } from './packets/manager/CharacterDelete'
import { CharacterSelect } from './packets/manager/CharacterSelect'
import { CharacterInfoPackage, getCharacterSelectionPackages } from './models/CharacterInfoPackage'
import { NewCharacter } from './packets/send/NewCharacter'
import { CharacterRestore } from './packets/manager/CharacterRestore'
import { ProcessExtraRequest } from './packets/manager/ProcessExtraRequest'
import { EnterWorld } from './packets/manager/EnterWorld'
import { ProtocolVersion } from './packets/receive/ProtocolVersion'
import { KeyPacket, KeyPacketStatus } from './packets/send/KeyPacket'
import { LoginServerService } from '../loginService/LoginServerService'
import { BlowfishKeyGenerator } from './packets/BlowfishKeyGenerator'
import { ClanCache } from './cache/ClanCache'
import { L2GameClientRegistry } from './L2GameClientRegistry'
import { ServerClose } from './packets/send/ServerClose'
import { L2PcInstanceHelper } from './helpers/L2PcInstanceHelper'
import { RequestManorList } from './packets/receive/RequestManorList'
import { DeletionManager } from './cache/DeletionManager'
import { PacketDispatcher } from './PacketDispatcher'
import { LeaveWorld } from './packets/send/LeaveWorld'
import { HighFivePacketMethods } from './packets/ReadPacketTranslator'
import chalk from 'chalk'
import _ from 'lodash'
import { CharacterSelectionInfo } from './packets/send/CharacterSelectionInfo'
import { GameClientState } from './enums/GameClientState'
import { ServerLog } from '../logger/Logger'
import Timeout = NodeJS.Timeout

type IncomingPacketMethod = ( data: Buffer ) => Promise<void>

export class GameClient {
    connection: Socket
    state: GameClientState
    encryptionEngine: GameCrypt
    ipAddress: string
    isProtocolOk: boolean = false
    accountName: string
    sessionId: SessionKey = null
    characterSlotMapping: Array<CharacterInfoPackage>
    player: L2PcInstance = null
    isAuthenticatedGameGuard: boolean = false
    isDetachedValue: boolean = false
    cleanUpTask: Timeout
    isOnline: boolean = false
    sessionStartTime: number = 0

    processIncomingPacketMethod: IncomingPacketMethod

    constructor( connection: Socket ) {
        /*
            Disabling Nagle's algorithm delay before sending packet data.
         */
        connection.setNoDelay( true )

        this.connection = connection
        this.setState( GameClientState.Initial )

        this.encryptionEngine = new GameCrypt()
        this.ipAddress = connection.remoteAddress

        this.connection.on( 'data', this.onConnectionData.bind( this ) )
        this.connection.on( 'close', this.onConnectionEnd.bind( this ) )
        this.connection.on( 'error', this.onConnectionEnd.bind( this ) )
    }

    unloadPlayer() {
        if ( this.cleanUpTask ) {
            clearTimeout( this.cleanUpTask )
        }

        this.cleanUpTask = setTimeout( this.runCleanUpTask.bind( this ), 100 )
    }

    closeConnection( packet : Buffer ) : void {
        this.connection.end( this.encodePacket( packet ) )
        this.connection.destroy()
    }

    abortConnection() : void {
        this.connection.end()
        this.connection.destroy()
    }

    encodePacket( packet: Buffer ) : Buffer {
        this.encryptionEngine.encrypt( packet.subarray( 2 ) )
        return packet
    }

    enableEncryptionKey(): Buffer {
        let currentKey = BlowfishKeyGenerator.getRandomKey()
        this.encryptionEngine.setKey( currentKey )

        return currentKey
    }

    getAccountName(): string {
        return this.accountName
    }

    getCharacterSelection( slot: number ): CharacterInfoPackage {
        return this.characterSlotMapping[ slot ]
    }

    getObjectIdForSlot( slot: number ): number {
        let character = this.getCharacterSelection( slot )
        return character ? character.objectId : null
    }

    getSessionId(): SessionKey {
        return this.sessionId
    }

    getState(): GameClientState {
        return this.state
    }

    hasCharacter( objectId: number ) {
        return _.some( this.characterSlotMapping, ( slot: CharacterInfoPackage ) => {
            return slot.objectId === objectId
        } )
    }

    isDetached(): boolean {
        return this.isDetachedValue
    }

    async loadCharacterInstance( slot: number ): Promise<L2PcInstance> {
        let objectId = this.getObjectIdForSlot( slot )

        if ( objectId < 0 ) {
            return null
        }

        let existingPlayer = L2World.getPlayer( objectId )
        if ( existingPlayer ) {
            let existingClient: GameClient = L2GameClientRegistry.getClientByPlayerId( objectId )
            if ( existingClient ) {
                existingClient.closeConnection( ServerClose() )
            }

            return null
        }

        let player: L2PcInstance = await L2PcInstanceHelper.load( objectId )
        if ( player ) {
            PacketDispatcher.createDebouncedPackets( objectId )

            player.setRunning()
            await player.standUp()
            player.refreshOverloadPenalty()
            player.refreshExpertisePenalty()
            await player.setOnlineStatus( true )
        }

        return player
    }

    async markCharacterAsDeleted( characterSlot: number ): Promise<CharacterDeleteOutcome> {
        let objectId = this.getObjectIdForSlot( characterSlot )

        if ( !objectId ) {
            return CharacterDeleteOutcome.GeneralFailure
        }

        let clanId = await DatabaseManager.getCharacterTable().getClanIdForCharacter( objectId )

        if ( clanId ) {
            let clan = ClanCache.getClan( clanId )

            if ( clan && clan.getLeaderId() === objectId ) {
                return CharacterDeleteOutcome.ClanLeaderFailure
            }

            if ( clan ) {
                return CharacterDeleteOutcome.ClanMemberFailure
            }
        }

        let daysLeft: number = ConfigManager.character.getDaysToDeleteCharacter()
        switch ( daysLeft ) {
            case 0:
                await DeletionManager.removeCharacter( objectId )
                break

            case -1:
                return CharacterDeleteOutcome.GeneralFailure

            default:
                await DatabaseManager.getCharacterTable().updateDeleteTime( objectId, daysLeft, false )
                break
        }

        return CharacterDeleteOutcome.Success
    }

    async markCharacterAsRestored( slot: number ) {
        let objectId = this.getObjectIdForSlot( slot )
        if ( objectId < 0 ) {
            return
        }

        return DatabaseManager.getCharacterTable().updateDeleteTime( objectId, 0, true )
    }

    async onConnectionData( incomingData: Buffer ): Promise<void> {
        let packetSizeIndex = 0

        while ( packetSizeIndex < incomingData.length ) {
            let packetSize = incomingData.readInt16LE( packetSizeIndex )
            let packetData = incomingData.subarray( packetSizeIndex + 2, packetSizeIndex + packetSize )
            packetSizeIndex += packetSize

            this.encryptionEngine.decrypt( packetData )

            try {
                await this.processIncomingPacketMethod( packetData )
            } catch ( error ) {
                ServerLog.fatal( error, 'Failed to process packet for account: %s', this.accountName )
            }
        }
    }

    onConnectionEnd() : void {
        this.isDetachedValue = true
        this.unloadPlayer()
    }

    async processOnVerified( packetData: Buffer ): Promise<void> {
        switch ( packetData.readUInt8() ) {
            case 0x00:
                return Logout( this )

            case 0x0c:
                return CharacterCreate( this, packetData )

            case 0x0d:
                return CharacterDelete( this, packetData )

            case 0x12:
                return CharacterSelect( this, packetData )

            case 0x13:
                return this.sendPacket( NewCharacter() )

            case 0x7b:
                return CharacterRestore( this, packetData )

            case 0xd0:
                return ProcessExtraRequest( this, packetData )
        }
    }

    async processOnConnected( packetData: Buffer ): Promise<void> {
        // TODO : support ping option when value for version = -2
        switch ( packetData.readUInt8() ) {
            case 0x0e:
                /*
                    Repeated requests for protocol version can reset encryption key,
                    which can be used to gain access to all available pooled keys on server,
                    an attack vector of sorts. In case a KeyPacket has already been dispatched with
                    a key, we should be able to terminate connection.
                 */
                if ( this.encryptionEngine.shouldEncrypt ) {
                    return this.abortConnection()
                }

                let version = ProtocolVersion( packetData )
                if ( _.includes( ConfigManager.server.getAllowedProtocolRevisions(), version ) ) {
                    this.sendPacket( KeyPacket( this.enableEncryptionKey(), KeyPacketStatus.Success ) )
                    this.isProtocolOk = true

                    return
                }

                this.isProtocolOk = false
                this.closeConnection( KeyPacket( Buffer.from( [ 0,0,0,0,0,0,0,0 ] ), KeyPacketStatus.UnsupportedProtocol ) )
                return

            case 0x2b:
                /*
                    Client can send first connection packet as authorized login,
                    which presents out of sequence situation, possibly probing for exploit.
                    Hence, only action is to terminate connection before packet is parsed.
                 */
                if ( !this.isProtocolOk || this.accountName ) {
                    return this.abortConnection()
                }

                let data : AuthorizedLoginData = AuthorizedLogin( this, packetData )

                /*
                    We need to check if login server has indeed requested an account characters information,
                    which is part of packet flow between game server and login server. And while
                    it may seem that one can specify a different account name, such name is immediately
                    verified with login server using provided four session numbers.
                 */
                let hasRequestedLogin = LoginServerService.hasRequestedAccount( data.loginName )
                let canBeLoggedIn = LoginServerService.addGameServerLogin( data.loginName, this )
                if ( !data.loginName
                    || !hasRequestedLogin
                    || !canBeLoggedIn ) {
                    return this.abortConnection()
                }

                LoginServerService.resetRequestedAccount( data.loginName )

                this.accountName = data.loginName

                ServerLog.info( 'Account \'%s\' joined game server', this.accountName )

                let key: SessionKey = {
                    playOk1: data.playKey1,
                    playOk2: data.playKey2,
                    loginOk1: data.loginKey1,
                    loginOk2: data.loginKey2,
                }

                return LoginServerService.addWaitingClientAndVerifyLogin( data.loginName, this, key )
        }

        ServerLog.trace( `onConnected operation '${ packetData[ 0 ] }' is not recognized from accountName '%s'`, this.accountName )
        this.abortConnection()
    }

    async processOnPlaying( packetData: Buffer ): Promise<void> {
        let opcode = packetData.readUInt8()

        let method = HighFivePacketMethods[ opcode ]
        if ( ConfigManager.diagnostic.showIncomingPackets() ) {
            ServerLog.info( '%s received %s with code 0x%s', this.accountName, 'received', chalk.magenta( method.name ), chalk.yellow( opcode.toString( 16 ) ) )
        }

        return method( this, packetData )
    }

    async processOnSelectingCharacter( packetData: Buffer ): Promise<void> {
        switch ( packetData.readUInt8() ) {
            case 0x11:
                return EnterWorld( this )

            case 0xD0:
                let subOpCode: number = packetData.readInt16LE( 1 )

                if ( subOpCode === 0x01 ) {
                    return RequestManorList( this )
                }

                break
        }
    }

    async runCleanUpTask() {
        if ( this.player ) {
            let objectId = this.player.getObjectId()

            if ( this.player.isOnline() ) {
                await this.player.deleteMe()
            }

            L2GameClientRegistry.removePlayerAssociation( objectId )
            PacketDispatcher.removeDebouncedPackets( objectId )

            this.player = null
        }

        this.setState( GameClientState.None )
        L2GameClientRegistry.removeClient( this )

        if ( this.accountName ) {
            LoginServerService.removeGameServerLogin( this.accountName )
            LoginServerService.sendLogout( this.accountName )

            ServerLog.info( 'Account \'%s\' left game server', this.accountName )
        }
    }

    sendPacket( packet: Buffer ) : void {
        // TODO : remove when packets are stable
        if ( !Buffer.isBuffer( packet ) ) {
            ServerLog.fatal( 'Bad buffer data detected!' )
            return
        }

        this.connection.write( this.encodePacket( packet ) )
    }

    async updateCharacterSelection() : Promise<void> {
        this.characterSlotMapping = await getCharacterSelectionPackages( this.getAccountName() )
    }

    sendCharacterSelection() : void {
        this.sendPacket( CharacterSelectionInfo( this.getAccountName(), this.getSessionId().playOk1, this.characterSlotMapping ) )
    }

    hasCharacterSelection() : boolean {
        return !!this.characterSlotMapping
    }

    setGameGuardOk( value: boolean ) {
        this.isAuthenticatedGameGuard = value
    }

    async setOnlineStatus( isOnline: boolean, updateAccessStatus: boolean ) {
        if ( this.isOnline !== isOnline ) {
            this.isOnline = isOnline
        }

        if ( updateAccessStatus && this.player ) {
            await DatabaseManager.getCharacterTable().updateOnlineStatus( this.player )
        }
    }

    setPlayer( player: L2PcInstance ) {
        if ( player ) {
            if ( this.player ) {
                L2GameClientRegistry.removePlayerAssociation( this.player.getObjectId() )
            } else {
                L2GameClientRegistry.addPlayerAssociation( player.getObjectId(), this )
            }
        }

        this.player = player
    }

    getPlayerId(): number {
        return this.player.getObjectId()
    }

    setSessionId( value: SessionKey ) {
        this.sessionId = value
    }

    setState( value: GameClientState ) {
        this.state = value

        switch ( this.state ) {
            case GameClientState.Initial:
                this.processIncomingPacketMethod = this.processOnConnected.bind( this )
                return

            case GameClientState.Verified:
                this.processIncomingPacketMethod = this.processOnVerified.bind( this )
                return

            case GameClientState.SelectingCharacter:
                this.processIncomingPacketMethod = this.processOnSelectingCharacter.bind( this )
                return

            case GameClientState.Playing:
                this.sessionStartTime = Date.now()
                this.processIncomingPacketMethod = this.processOnPlaying.bind( this )
                return

            case GameClientState.None:
                this.processIncomingPacketMethod = () => Promise.resolve()
                return
        }
    }

    logout() : void {
        if ( this.isDetached() ) {
            this.unloadPlayer()
            return
        }

        this.closeConnection( LeaveWorld() )
    }
}