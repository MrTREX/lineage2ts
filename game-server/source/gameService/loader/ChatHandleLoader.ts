import { L2DataApi } from '../../data/interface/l2DataApi'
import { ChatTypeManager } from '../handler/managers/ChatTypeManager'
import { IChatHandler } from '../handler/IChatHandler'
import { TypeAll } from '../handler/chathandlers/TypeAll'
import { TypeAlliance } from '../handler/chathandlers/TypeAlliance'
import { TypeBattlefield } from '../handler/chathandlers/TypeBattlefield'
import { TypeClan } from '../handler/chathandlers/TypeClan'
import { TypeHeroVoice } from '../handler/chathandlers/TypeHeroVoice'
import { TypeParty } from '../handler/chathandlers/TypeParty'
import { TypePartyMatchRoom } from '../handler/chathandlers/TypePartyMatchRoom'
import { TypePartyRoomAll } from '../handler/chathandlers/TypePartyRoomAll'
import { TypePartyRoomCommander } from '../handler/chathandlers/TypePartyRoomCommander'
import { TypePetition, TypePetitionGM } from '../handler/chathandlers/TypePetition'
import { TypeShout } from '../handler/chathandlers/TypeShout'
import { TypeTell } from '../handler/chathandlers/TypeTell'
import { TypeTrade } from '../handler/chathandlers/TypeTrade'
import _ from 'lodash'

const allHandlers : Array<IChatHandler> = [
    TypeAll,
    TypeAlliance,
    TypeBattlefield,
    TypeClan,
    TypeHeroVoice,
    TypeParty,
    TypePartyMatchRoom,
    TypePartyRoomAll,
    TypePartyRoomCommander,
    TypePetition,
    TypeShout,
    TypeTell,
    TypeTrade,
    TypePetitionGM
]

class Manager implements L2DataApi {
    async load(): Promise<Array<string>> {
        ChatTypeManager.load( allHandlers )
        return [
            `ChatHandleLoader: loaded ${_.size( ChatTypeManager.registry )} handlers.`
        ]
    }

}

export const ChatHandleLoader = new Manager()