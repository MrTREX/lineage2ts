import { L2DataApi } from '../../data/interface/l2DataApi'
import { ActionShiftManager } from '../handler/managers/ActionShiftManager'
import { IActionShiftHandler } from '../handler/IActionShiftHandler'
import { L2DoorInstanceActionShift } from '../handler/actionshifthandlers/L2DoorInstanceActionShift'
import { L2ItemInstanceActionShift } from '../handler/actionshifthandlers/L2ItemInstanceActionShift'
import { L2NpcActionShift } from '../handler/actionshifthandlers/L2NpcActionShift'
import { L2PcInstanceActionShift } from '../handler/actionshifthandlers/L2PcInstanceActionShift'
import { L2StaticObjectInstanceActionShift } from '../handler/actionshifthandlers/L2StaticObjectInstanceActionShift'
import { L2SummonActionShift } from '../handler/actionshifthandlers/L2SummonActionShift'
import _ from 'lodash'

const allHandlers : Array<IActionShiftHandler> = [
        L2DoorInstanceActionShift,
        L2ItemInstanceActionShift,
        L2NpcActionShift,
        L2PcInstanceActionShift,
        L2StaticObjectInstanceActionShift,
        L2SummonActionShift
]

export const ActionHandleShiftLoader: L2DataApi = {
    async load(): Promise<Array<string>> {
        ActionShiftManager.load( allHandlers )

        return [
            `ActionShiftManager: loaded ${ _.size( ActionShiftManager.actions ) } handlers.`,
        ]
    },
}