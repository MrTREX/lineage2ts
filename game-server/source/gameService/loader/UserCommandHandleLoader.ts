import { L2DataApi } from '../../data/interface/l2DataApi'
import { IUserCommandHandler } from '../handler/IUserCommandHandler'
import { UserCommandManager } from '../handler/managers/UserCommandManager'
import { ChannelDelete } from '../handler/usercommandhandlers/ChannelDelete'
import { ChannelInfo } from '../handler/usercommandhandlers/ChannelInfo'
import { ChannelLeave } from '../handler/usercommandhandlers/ChannelLeave'
import { ClanPenalty } from '../handler/usercommandhandlers/ClanPenalty'
import { ClanWarsList } from '../handler/usercommandhandlers/ClanWarsList'
import { Dismount } from '../handler/usercommandhandlers/Dismount'
import { InstanceZone } from '../handler/usercommandhandlers/InstanceZone'
import { Loc } from '../handler/usercommandhandlers/Loc'
import { Mount } from '../handler/usercommandhandlers/Mount'
import { MyBirthday } from '../handler/usercommandhandlers/MyBirthday'
import { OlympiadStat } from '../handler/usercommandhandlers/OlympiadStat'
import { PartyInfo } from '../handler/usercommandhandlers/PartyInfo'
import { SiegeStatus } from '../handler/usercommandhandlers/SiegeStatus'
import { Time } from '../handler/usercommandhandlers/Time'
import { Unstuck } from '../handler/usercommandhandlers/Unstuck'
import _ from 'lodash'

const allHandlers : Array<IUserCommandHandler> = [
        ChannelDelete,
        ChannelInfo,
        ChannelLeave,
        ClanPenalty,
        ClanWarsList,
        Dismount,
        InstanceZone,
        Loc,
        Mount,
        MyBirthday,
        OlympiadStat,
        PartyInfo,
        SiegeStatus,
        Time,
        Unstuck
]

class Manager implements L2DataApi {
    async load(): Promise<Array<string>> {
        UserCommandManager.load( allHandlers )
        return [
            `UserCommandManager: loaded ${_.size( UserCommandManager.registry )} handlers.`
        ]
    }
}

export const UserCommandHandleLoader = new Manager()