import { DatabaseManager } from '../../database/manager'
import { DataManager } from '../../data/manager'
import { DatabaseLoadOrder } from '../../database/LoadOrder'
import { SkillCache } from '../cache/SkillCache'
import { BypassHandleLoader } from './BypassHandleLoader'
import { L2DataApi } from '../../data/interface/l2DataApi'
import { ActionHandleLoader } from './ActionHandleLoader'
import { ActionHandleShiftLoader } from './ActionShiftHandleLoader'
import { Olympiad } from '../cache/Olympiad'
import { UserCommandHandleLoader } from './UserCommandHandleLoader'
import { InstanceManager } from '../instancemanager/InstanceManager'
import { GrandBossManager } from '../cache/GrandBossManager'
import { ItemAuctionManager } from '../cache/ItemAuctionManager'
import { AugmentationCache } from '../cache/AugmentationCache'
import { BotReportCache } from '../cache/BotReportCache'
import { CastleManorManager } from '../instancemanager/CastleManorManager'
import { MercenaryTicketManager } from '../instancemanager/MercenaryTicketManager'
import { ClanCache } from '../cache/ClanCache'
import { ClanHallManager } from '../instancemanager/ClanHallManager'
import { PunishmentHandleLoader } from './PunishmentHandleLoader'
import { LotteryManager } from '../cache/LotteryManager'
import { HeroCache } from '../cache/HeroCache'
import { ChatHandleLoader } from './ChatHandleLoader'
import { PluginLoader } from '../../plugins/PluginLoader'
import { AdminCommandManager } from '../handler/managers/AdminCommandManager'
import { FishingChampionshipManager } from '../cache/FishingChampionshipManager'
import { VoicedCommandManager } from '../handler/managers/VoicedCommandManager'
import { ItemsOnGroundManager } from '../cache/ItemsOnGroundManager'
import { CycleStepManager } from '../cache/CycleStepManager'
import { DeletedAccountsProcessor } from '../cache/DeletedAccountsProcessor'
import { ClanHallSiegeManager } from '../instancemanager/ClanHallSiegeManager'
import aigle from 'aigle'
import { SpawnMakerCache } from '../cache/SpawnMakerCache'
import { DoorManager } from '../cache/DoorManager'
import { GameTime } from '../cache/GameTime'
import { MainListenerLoader } from '../../listeners/loader/MainListenerLoader'
import { CastleManager } from '../instancemanager/CastleManager'
import { FortManager } from '../instancemanager/FortManager'
import { RespawnRegionCache } from '../cache/RespawnRegionCache'
import { PlayerAccessCache } from '../cache/PlayerAccessCache'
import { CommandLink } from '../../rpc/CommandLink'
import { ServerLog } from '../../logger/Logger'
import { resetFunctionTemplates } from '../../data/type/sqlite/helpers/FunctionHelper'
import { GeoPolygonCache } from '../cache/GeoPolygonCache'

// TODO : better import organization, right now forcing load of Database imports
DatabaseManager.loadMe()


const showLoadStatus = async ( method: Function ) => {
    let statusLines: Array<string> = await method()
    for ( const statusLine of statusLines ) {
        ServerLog.info( statusLine )
    }
}

/**
 * Load of various server subsystems that need to be initialized
 * after various data and database sources have been processed.
 */
const PostDataLoadOrder: Array<L2DataApi> = [
    SkillCache,
    DoorManager,
    SpawnMakerCache,
    RespawnRegionCache,
    CastleManager, // castles need residence skill trees
    FortManager,
    Olympiad,
    InstanceManager,
    BypassHandleLoader,
    ActionHandleLoader,
    ActionHandleShiftLoader,
    UserCommandHandleLoader,
    ChatHandleLoader,
    PunishmentHandleLoader,
    AdminCommandManager,
    VoicedCommandManager,
    GrandBossManager,
    ItemAuctionManager,
    AugmentationCache,
    BotReportCache,
    CastleManorManager,
    MercenaryTicketManager,
    ClanCache,
    ClanHallManager,
    ClanHallSiegeManager,
    LotteryManager,
    HeroCache,
    FishingChampionshipManager,
    ItemsOnGroundManager,
    CycleStepManager,
    DeletedAccountsProcessor,
    PlayerAccessCache,
    GameTime, // ensure that game time is last due to spawns may require certain trigger hour,
    GeoPolygonCache,
]

/**
 *  Non-essential to server functionality data loaders
 */
const AuxiliaryLoadOrder: Array<L2DataApi> = [
    MainListenerLoader,
    CommandLink
]

const loadDataApi = async ( dataApi: L2DataApi ) => {
    return showLoadStatus( dataApi.load.bind( dataApi ) )
}

export const L2DataLoader = {
    async start(): Promise<void> {
        await aigle.resolve( DataManager.getLoadOrder() ).eachSeries( showLoadStatus )
        await aigle.resolve( DatabaseLoadOrder ).eachSeries( loadDataApi )
        await aigle.resolve( PostDataLoadOrder ).eachSeries( loadDataApi )
        await aigle.resolve( AuxiliaryLoadOrder ).eachSeries( loadDataApi )

        await loadDataApi( PluginLoader )

        resetFunctionTemplates()
    },
}