import { PunishmentType } from '../models/punishment/PunishmentType'
import { PunishmentEffect } from '../models/punishment/PunishmentEffect'
import { PunishmentRecord } from '../models/punishment/PunishmentRecord'
import { DatabaseManager } from '../../database/manager'
import { L2DataApi } from '../../data/interface/l2DataApi'
import { IPunishmentHandler } from '../handler/IPunishmentHandler'
import { UserPunishmentManager } from '../handler/managers/UserPunishmentMananger'
import _ from 'lodash'

class Manager implements L2DataApi {
    owners: { [key: number]: Array<PunishmentRecord> }

    hasPunishment( mappedId: string | number, effect: PunishmentEffect, type: PunishmentType ): boolean {
        let records = this.owners[ mappedId ]
        return _.some( records, ( record: PunishmentRecord ) => {
            return record.effect === effect && record.type === type
        } )
    }

    async startPunishment( objectId: string, effect: PunishmentEffect, type: PunishmentType, expiration: number, reason: string = null, punishedBy: string = 'System', isStored = false ): Promise<void> {
        if ( !this.owners[ objectId ] ) {
            this.owners[ objectId ] = []
        }

        let record = new PunishmentRecord( objectId, effect, type, expiration, reason, punishedBy, isStored )

        if ( record.isExpired() ) {
            return
        }

        this.owners[ objectId ].push( record )

        if ( !isStored ) {
            await DatabaseManager.getPunishments().addRecord( record )
        }

        let handler : IPunishmentHandler = UserPunishmentManager.getHandler( type )
        if ( handler ) {
            await handler.onStart( record )
            record.startPunishmentTask()
        }
    }

    async stopPunishment( targetId: number | string, effect: PunishmentEffect, type: PunishmentType ): Promise<void> {
        let predicate : Function = ( record: PunishmentRecord ) => {
            return record.effect === effect && record.type === type
        }

        let record : PunishmentRecord = _.find( this.owners[ targetId ], predicate )
        if ( !record ) {
            return
        }

        let handler : IPunishmentHandler = UserPunishmentManager.getHandler( type )
        if ( handler ) {
            record.stopPunishmentTask()
            await handler.onEnd( record )
        }

        if ( record.isStored ) {
            await DatabaseManager.getPunishments().updateRecord( record )
        }

        this.owners[ targetId ] = _.remove( this.owners[ targetId ], predicate )
    }

    async load() {
        this.owners = {}

        let records : Array<PunishmentRecord> = await DatabaseManager.getPunishments().getAll()

        let manager = this
        let counter = 0
        let removedRecords: Array<PunishmentRecord> = []

       records.forEach( ( record: PunishmentRecord ) => {
            if ( record.isExpired() ) {
                removedRecords.push( record )
                return
            }

            if ( !manager.owners[ record.targetId ] ) {
                manager.owners[ record.targetId ] = []
            }

            manager.owners[ record.targetId ].push( record )
            counter++
        } )

        if ( removedRecords.length > 0 ) {
            await DatabaseManager.getPunishments().removeRecords( removedRecords )
        }

        return [
            `PunishmentManager : loaded ${counter} punishments for ${_.size( this.owners )} users`,
            `PunishmentManager : removed ${removedRecords.length} expired punishments `,
        ]
    }
}

export const PunishmentManager = new Manager()