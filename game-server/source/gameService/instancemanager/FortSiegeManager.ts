import { FortSiegeSpawn } from '../models/FortSiegeSpawn'
import { FortSiege } from '../models/entity/FortSiege'
import { CombatFlag } from '../models/CombatFlag'
import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { Fort } from '../models/entity/Fort'
import { FortManager } from './FortManager'
import { L2ItemInstance } from '../models/items/instance/L2ItemInstance'
import { SystemMessageBuilder } from '../packets/send/SystemMessage'
import { SystemMessageIds } from '../packets/SystemMessageIdValues'
import { L2Clan } from '../models/L2Clan'
import { DatabaseManager } from '../../database/manager'
import { L2DataApi } from '../../data/interface/l2DataApi'
import _ from 'lodash'
import { ILocational } from '../models/Location'

class Manager implements L2DataApi {
    commanderSpawns: { [key: number]: Array<FortSiegeSpawn> } = {}
    flags: { [key: number]: Array<CombatFlag> } = {}
    sieges: Array<FortSiege> = []

    addSiege( siege: FortSiege ) {
        this.sieges.push( siege )
    }

    dropCombatFlag( player: L2PcInstance, fortId: number ): void {
        let fort: Fort = FortManager.getFortById( fortId )
        let flags: Array<CombatFlag> = this.flags[ fortId ]
        _.each( flags, ( flag: CombatFlag ) => {
            if ( flag.getPlayerObjectId() === player.getObjectId() ) {
                flag.dropIt()

                if ( fort.getSiege().isInProgress() ) {
                    flag.spawnMe()
                }
            }
        } )
    }

    getSiege( object: ILocational ): FortSiege {
        let fort : Fort = FortManager.findNearestFort( object )

        if ( fort ) {
            return fort.getSiege()
        }

        return null
    }

    getSieges(): Array<FortSiege> {
        return this.sieges
    }

    isCombatFlag( itemId: number ) {
        return itemId === 9819
    }

    getCommanderSpawnList( fortId: number ) : Array<FortSiegeSpawn> {
        return this.commanderSpawns[ fortId ]
    }

    activateCombatFlag( player: L2PcInstance, item: L2ItemInstance ) : boolean {
        if ( !this.checkIfCanPickup( player ) ) {
            return false
        }

        let fort : Fort = FortManager.getFort( player )

        _.each( this.flags[ fort.getResidenceId() ], ( flag: CombatFlag ) => {
            let flagInstance = flag.getInstance()
            if ( flagInstance && flagInstance.getObjectId() === item.getObjectId() ) {
                flag.activate( player, item )
            }
        } )

        return true
    }

    checkIfCanPickup( player: L2PcInstance ) : boolean {
        let battleHasFinishedMessage : Buffer = new SystemMessageBuilder( SystemMessageIds.THE_FORTRESS_BATTLE_OF_S1_HAS_FINISHED )
                .addItemNameWithId( 9819 )
                .getBuffer()

        if ( player.isCombatFlagEquipped() ) {
            player.sendOwnedData( battleHasFinishedMessage )
            return false
        }

        let fort : Fort = FortManager.getFort( player )
        if ( !fort || ( fort.getResidenceId() <= 0 ) ) {
            player.sendOwnedData( battleHasFinishedMessage )
            return false
        }

        if ( !fort.getSiege().isInProgress() ) {
            player.sendOwnedData( battleHasFinishedMessage )
            return false
        }

        if ( !fort.getSiege().getAttackerClan( player.getClan() ) ) {
            player.sendOwnedData( battleHasFinishedMessage )
            return false
        }

        return true
    }

    async checkIsRegistered( clan: L2Clan, fortId: number ) : Promise<boolean> {
        return DatabaseManager.getFortSiegeClans().hasRegistration( clan.getId(), fortId )
    }

    async checkIsRegisteredForAll( clan: L2Clan, residenceIds: ReadonlyArray<number> ) : Promise<boolean> {
        return DatabaseManager.getFortSiegeClans().hasRegistrationForAny( clan.getId(), residenceIds )
    }

    getFlagList( fortId: number ) : Array<CombatFlag> {
        return this.flags[ fortId ]
    }

    async load(): Promise<Array<string>> {
        // TODO : implement logic to create spawns and flags
        // TODO : add region activation to kick off any siege activities/timers

        return [
            `FortSiegeManager : loaded ${_.size( this.commanderSpawns )} commander spawns`,
            `FortSiegeManager : loaded ${_.size( this.flags )} flags`,
        ]
    }
}

export const FortSiegeManager = new Manager()