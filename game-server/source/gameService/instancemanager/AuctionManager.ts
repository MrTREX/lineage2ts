import { ClanHallAuction } from '../models/auction/ClanHallAuction'
import { DataManager } from '../../data/manager'
import { L2ClanHallAuctionItem } from '../../database/interface/ClanHallAuctionsTableApi'

class Manager {
    auctions: { [ id: number ]: ClanHallAuction } = {}

    async initNPC( id: number ): Promise<void> {
        let auctionData: L2ClanHallAuctionItem = DataManager.getAuctionTemplates().getClanHall( id )

        if ( !auctionData ) {
            return
        }

        let auction = new ClanHallAuction()

        auction.setProperties( auctionData )
        auction.startAutoTask()

        await auction.loadBids()

        this.auctions[ auction.getId() ] = auction
    }

    getAuction( id: number ): ClanHallAuction {
        return this.auctions[ id ]
    }

    removeAuction( id: number ): void {
        delete this.auctions[ id ]
    }

    addAuction( auction: ClanHallAuction ): void {
        if ( this.auctions[ auction.getId() ] ) {
            return
        }

        this.auctions[ auction.getId() ] = auction
    }

    getAll(): Array<ClanHallAuction> {
        return Object.values( this.auctions )
    }
}

export const AuctionManager = new Manager()