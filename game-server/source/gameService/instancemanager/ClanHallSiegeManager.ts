import { L2Character } from '../models/actor/L2Character'
import { ClanHallSiegeEngine } from '../models/entity/clanhall/ClanHallSiegeEngine'
import { L2Clan } from '../models/L2Clan'
import { ClanHall } from '../models/entity/ClanHall'
import { SiegableHall } from '../models/entity/clanhall/SiegableHall'
import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { ConfigManager } from '../../config/ConfigManager'
import { SystemMessageBuilder } from '../packets/send/SystemMessage'
import { SystemMessageIds } from '../packets/SystemMessageIdValues'
import _, { DebouncedFunc } from 'lodash'
import { L2DataApi } from '../../data/interface/l2DataApi'
import aigle from 'aigle'
import { DatabaseManager } from '../../database/manager'
import { L2SiegableHallTableData } from '../../database/interface/SiegableHallTableApi'
import { L2World } from '../L2World'
import { AreaType } from '../models/areas/AreaType'
import { FortSiegeArea } from '../models/areas/type/FortSiege'
import { AreaCache } from '../cache/AreaCache'
import { ClanHallArea } from '../models/areas/type/ClanHall'
import { ClanHallFunction } from '../models/clan/ClanHallFunction'

const clanHallUpdateInterval = 30000

class Manager implements L2DataApi {
    siegableHalls: { [ key: number ]: SiegableHall } = {}

    updateTask: DebouncedFunc<any>
    updateIds: Set<number> = new Set<number>()

    scheduleHallUpdate( id: number, forceUpdate: boolean = false ): void {
        this.updateIds.add( id )
        this.updateTask()

        if ( forceUpdate ) {
            this.updateTask.flush()
        }
    }

    getConquerableHalls() {
        return this.siegableHalls
    }

    getClanHallByOwner( clan: L2Clan ): SiegableHall {
        return Object.values( this.siegableHalls ).find( ( hall: SiegableHall ) : boolean => {
            return hall.getOwnerId() === clan.getId()
        } )
    }

    getNearbyClanHall( character: L2Character ): SiegableHall {
        return this.getNearbyClanHallWithCoordinates( character.getX(), character.getY(), 10000 )
    }

    getNearbyClanHallWithCoordinates( x: number, y: number, distance: number ): SiegableHall {
        let area = L2World.getNearestAreaType( x, y, AreaType.FortSiege ) as FortSiegeArea
        if ( !area || area.form.getDistanceToPoint( x, y ) > distance ) {
            return
        }

        return this.getSiegableHall( area.properties.residenceId )
    }

    getSiege( character: L2Character ): ClanHallSiegeEngine {
        let hall: SiegableHall = this.getNearbyClanHall( character )
        if ( hall ) {
            return hall.getSiege()
        }

        return null
    }

    getSiegableHall( id: number ): SiegableHall {
        return this.siegableHalls[ id ]
    }

    registerClan( clan: L2Clan, hall: SiegableHall, player: L2PcInstance ): void {
        if ( clan.getLevel() < ConfigManager.clanhall.getMinClanLevel() ) {
            player.sendMessage( `Only clans of level ${ ConfigManager.clanhall.getMinClanLevel() } or higher may register for a castle siege` )
            return
        }

        if ( hall.isWaitingBattle() ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.DEADLINE_FOR_SIEGE_S1_PASSED )
                    .addString( hall.getName() )
                    .getBuffer()
            player.sendOwnedData( packet )
            return
        }

        if ( hall.isInSiege() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_SIEGE_REGISTRATION_TIME2 ) )
            return
        }

        if ( hall.getOwnerId() === clan.getId() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLAN_THAT_OWNS_CASTLE_IS_AUTOMATICALLY_REGISTERED_DEFENDING ) )
            return
        }

        if ( ( clan.getCastleId() !== 0 ) || ( clan.getHideoutId() !== 0 ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLAN_THAT_OWNS_CASTLE_CANNOT_PARTICIPATE_OTHER_SIEGE ) )
            return
        }

        if ( hall.getSiege().checkIsAttacker( clan ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ALREADY_REQUESTED_SIEGE_BATTLE ) )
            return
        }

        if ( this.isClanParticipating( clan ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.APPLICATION_DENIED_BECAUSE_ALREADY_SUBMITTED_A_REQUEST_FOR_ANOTHER_SIEGE_BATTLE ) )
            return
        }

        if ( _.size( hall.getSiege().getAttackers() ) >= ConfigManager.clanhall.getMaxAttackers() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ATTACKER_SIDE_FULL ) )
            return
        }

        hall.addAttacker( clan )
    }

    isClanParticipating( clan: L2Clan ) {
        return _.some( this.siegableHalls, ( hall: SiegableHall ) => {
            return hall.getSiege() && hall.getSiege().checkIsAttacker( clan )
        } )
    }

    unRegisterClan( clan: L2Clan, hall: SiegableHall ): void {
        if ( !hall.isRegistering() ) {
            return
        }

        hall.removeAttacker( clan )
    }

    async load(): Promise<Array<string>> {
        this.updateTask = _.debounce( this.runUpdateTask.bind( this ), clanHallUpdateInterval, {
            trailing: true,
            maxWait: clanHallUpdateInterval * 2,
        } )

        /*
            TODO : separate data into pieces:
            - data layer with names and siege cron, siege duration etc.
            - stored layer for ownership, perhaps in server variables to simplify access and storage
         */
        let data: Array<L2SiegableHallTableData> = await DatabaseManager.getSiegableHalls().getAll()
        this.siegableHalls = await aigle.resolve( data ).reduce( async ( total: { [ key: number ]: SiegableHall }, item: L2SiegableHallTableData ) => {
            let hall = new SiegableHall( AreaCache.getAreaByResidenceId( item.hallId, AreaType.ClanHall ) as ClanHallArea )

            hall.clanHallId = item.hallId
            hall.name = item.name
            hall.ownerId = item.ownerId
            hall.description = item.description

            hall.location = item.location
            hall.nextSiege = item.nextSiege
            hall.duration = item.duration
            hall.startCron = item.siegeStartCron

            await hall.initialize()

            total[ item.hallId ] = hall

            return total
        }, {} )

        return [
            `ClanHallSiegeManager : loaded total ${ _.size( this.siegableHalls ) } siegable halls.`,
        ]
    }

    async runUpdateTask(): Promise<void> {
        let manager = this
        let clanHalls: Array<SiegableHall> = _.compact( Array.from( this.updateIds ).map( ( id: number ): SiegableHall => {
            return manager.siegableHalls[ id ]
        } ) )

        this.updateIds.clear()

        if ( clanHalls.length === 0 ) {
            return
        }

        await DatabaseManager.getSiegableHalls().updateHalls( clanHalls )

        let hallFunctions = _.flatMap( clanHalls, ( clanHall: ClanHall ): Array<ClanHallFunction> => Object.values( clanHall.functions ) )
        return DatabaseManager.getClanhallFunctions().updateFunctions( hallFunctions )
    }
}

export const ClanHallSiegeManager = new Manager()