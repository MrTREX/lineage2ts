import { L2DataApi } from '../../data/interface/l2DataApi'
import { L2ItemInstance } from '../models/items/instance/L2ItemInstance'
import { L2World } from '../L2World'
import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { Castle } from '../models/entity/Castle'
import { CastleManager } from './CastleManager'
import { ConfigManager } from '../../config/ConfigManager'
import { DatabaseManager } from '../../database/manager'
import { L2CastleSiegeGuardsTableData } from '../../database/interface/CastleSiegeGuardsTableApi'
import _ from 'lodash'
import { RoaringBitmap32 } from 'roaring'
import { ItemManagerCache } from '../cache/ItemManagerCache'

const maxMercenaryPerType: Array<number> = [
    10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, // Gludio
    15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, // Dion
    10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, // Giran
    10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, // Oren
    20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, // Aden
    20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, // Innadril
    20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, // Goddard
    20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, // Rune
    20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, // Schuttgart
]

const ticketItemIds: Array<number> = [
    3960, 3961, 3962, 3963, 3964, 3965, 3966, 3967, 3968, 3969, 6115, 6116, 6117, 6118, 6119, 6120, 6121, 6122, 6123, 6124, 6038, 6039, 6040, 6041, 6042, 6043, 6044, 6045, 6046, 6047, 6175, 6176, 6177, 6178, 6179, 6180, 6181, 6182, 6183, 6184, 6235, 6236, 6237, 6238, 6239, 6240, 6241, 6242, 6243, 6244, 6295, 6296, // Gludio
    3973, 3974, 3975, 3976, 3977, 3978, 3979, 3980, 3981, 3982, 6125, 6126, 6127, 6128, 6129, 6130, 6131, 6132, 6133, 6134, 6051, 6052, 6053, 6054, 6055, 6056, 6057, 6058, 6059, 6060, 6185, 6186, 6187, 6188, 6189, 6190, 6191, 6192, 6193, 6194, 6245, 6246, 6247, 6248, 6249, 6250, 6251, 6252, 6253, 6254, 6297, 6298, // Dion
    3986, 3987, 3988, 3989, 3990, 3991, 3992, 3993, 3994, 3995, 6135, 6136, 6137, 6138, 6139, 6140, 6141, 6142, 6143, 6144, 6064, 6065, 6066, 6067, 6068, 6069, 6070, 6071, 6072, 6073, 6195, 6196, 6197, 6198, 6199, 6200, 6201, 6202, 6203, 6204, 6255, 6256, 6257, 6258, 6259, 6260, 6261, 6262, 6263, 6264, 6299, 6300, // Giran
    3999, 4000, 4001, 4002, 4003, 4004, 4005, 4006, 4007, 4008, 6145, 6146, 6147, 6148, 6149, 6150, 6151, 6152, 6153, 6154, 6077, 6078, 6079, 6080, 6081, 6082, 6083, 6084, 6085, 6086, 6205, 6206, 6207, 6208, 6209, 6210, 6211, 6212, 6213, 6214, 6265, 6266, 6267, 6268, 6269, 6270, 6271, 6272, 6273, 6274, 6301, 6302, // Oren
    4012, 4013, 4014, 4015, 4016, 4017, 4018, 4019, 4020, 4021, 6155, 6156, 6157, 6158, 6159, 6160, 6161, 6162, 6163, 6164, 6090, 6091, 6092, 6093, 6094, 6095, 6096, 6097, 6098, 6099, 6215, 6216, 6217, 6218, 6219, 6220, 6221, 6222, 6223, 6224, 6275, 6276, 6277, 6278, 6279, 6280, 6281, 6282, 6283, 6284, 6303, 6304, // Aden
    5205, 5206, 5207, 5208, 5209, 5210, 5211, 5212, 5213, 5214, 6165, 6166, 6167, 6168, 6169, 6170, 6171, 6172, 6173, 6174, 6105, 6106, 6107, 6108, 6109, 6110, 6111, 6112, 6113, 6114, 6225, 6226, 6227, 6228, 6229, 6230, 6231, 6232, 6233, 6234, 6285, 6286, 6287, 6288, 6289, 6290, 6291, 6292, 6293, 6294, 6305, 6306, // Innadril
    6779, 6780, 6781, 6782, 6783, 6784, 6785, 6786, 6787, 6788, 6802, 6803, 6804, 6805, 6806, 6807, 6808, 6809, 6810, 6811, 6792, 6793, 6794, 6795, 6796, 6797, 6798, 6799, 6800, 6801, 6812, 6813, 6814, 6815, 6816, 6817, 6818, 6819, 6820, 6821, 6822, 6823, 6824, 6825, 6826, 6827, 6828, 6829, 6830, 6831, 6832, 6833, // Goddard
    7973, 7974, 7975, 7976, 7977, 7978, 7979, 7980, 7981, 7982, 7998, 7999, 8000, 8001, 8002, 8003, 8004, 8005, 8006, 8007, 7988, 7989, 7990, 7991, 7992, 7993, 7994, 7995, 7996, 7997, 8008, 8009, 8010, 8011, 8012, 8013, 8014, 8015, 8016, 8017, 8018, 8019, 8020, 8021, 8022, 8023, 8024, 8025, 8026, 8027, 8028, 8029, // Rune
    7918, 7919, 7920, 7921, 7922, 7923, 7924, 7925, 7926, 7927, 7941, 7942, 7943, 7944, 7945, 7946, 7947, 7948, 7949, 7950, 7931, 7932, 7933, 7934, 7935, 7936, 7937, 7938, 7939, 7940, 7951, 7952, 7953, 7954, 7955, 7956, 7957, 7958, 7959, 7960, 7961, 7962, 7963, 7964, 7965, 7966, 7967, 7968, 7969, 7970, 7971, 7972, // Schuttgart
]
const mercenaryTickets = new RoaringBitmap32( ticketItemIds )

const npcIds: Array<number> = [
    35010, 35011, 35012, 35013, 35014, 35015, 35016, 35017, 35018, 35019, 35020, 35021, 35022, 35023, 35024, 35025, 35026, 35027, 35028, 35029, 35030, 35031, 35032, 35033, 35034, 35035, 35036, 35037, 35038, 35039, 35040, 35041, 35042, 35043, 35044, 35045, 35046, 35047, 35048, 35049, 35050, 35051, 35052, 35053, 35054, 35055, 35056, 35057, 35058, 35059, 35060, 35061, // Gludio
    35010, 35011, 35012, 35013, 35014, 35015, 35016, 35017, 35018, 35019, 35020, 35021, 35022, 35023, 35024, 35025, 35026, 35027, 35028, 35029, 35030, 35031, 35032, 35033, 35034, 35035, 35036, 35037, 35038, 35039, 35040, 35041, 35042, 35043, 35044, 35045, 35046, 35047, 35048, 35049, 35050, 35051, 35052, 35053, 35054, 35055, 35056, 35057, 35058, 35059, 35060, 35061, // Dion
    35010, 35011, 35012, 35013, 35014, 35015, 35016, 35017, 35018, 35019, 35020, 35021, 35022, 35023, 35024, 35025, 35026, 35027, 35028, 35029, 35030, 35031, 35032, 35033, 35034, 35035, 35036, 35037, 35038, 35039, 35040, 35041, 35042, 35043, 35044, 35045, 35046, 35047, 35048, 35049, 35050, 35051, 35052, 35053, 35054, 35055, 35056, 35057, 35058, 35059, 35060, 35061, // Giran
    35010, 35011, 35012, 35013, 35014, 35015, 35016, 35017, 35018, 35019, 35020, 35021, 35022, 35023, 35024, 35025, 35026, 35027, 35028, 35029, 35030, 35031, 35032, 35033, 35034, 35035, 35036, 35037, 35038, 35039, 35040, 35041, 35042, 35043, 35044, 35045, 35046, 35047, 35048, 35049, 35050, 35051, 35052, 35053, 35054, 35055, 35056, 35057, 35058, 35059, 35060, 35061, // Oren
    35010, 35011, 35012, 35013, 35014, 35015, 35016, 35017, 35018, 35019, 35020, 35021, 35022, 35023, 35024, 35025, 35026, 35027, 35028, 35029, 35030, 35031, 35032, 35033, 35034, 35035, 35036, 35037, 35038, 35039, 35040, 35041, 35042, 35043, 35044, 35045, 35046, 35047, 35048, 35049, 35050, 35051, 35052, 35053, 35054, 35055, 35056, 35057, 35058, 35059, 35060, 35061, // Aden
    35010, 35011, 35012, 35013, 35014, 35015, 35016, 35017, 35018, 35019, 35020, 35021, 35022, 35023, 35024, 35025, 35026, 35027, 35028, 35029, 35030, 35031, 35032, 35033, 35034, 35035, 35036, 35037, 35038, 35039, 35040, 35041, 35042, 35043, 35044, 35045, 35046, 35047, 35048, 35049, 35050, 35051, 35052, 35053, 35054, 35055, 35056, 35057, 35058, 35059, 35060, 35061, // Innadril
    35010, 35011, 35012, 35013, 35014, 35015, 35016, 35017, 35018, 35019, 35020, 35021, 35022, 35023, 35024, 35025, 35026, 35027, 35028, 35029, 35030, 35031, 35032, 35033, 35034, 35035, 35036, 35037, 35038, 35039, 35040, 35041, 35042, 35043, 35044, 35045, 35046, 35047, 35048, 35049, 35050, 35051, 35052, 35053, 35054, 35055, 35056, 35057, 35058, 35059, 35060, 35061, // Goddard
    35010, 35011, 35012, 35013, 35014, 35015, 35016, 35017, 35018, 35019, 35020, 35021, 35022, 35023, 35024, 35025, 35026, 35027, 35028, 35029, 35030, 35031, 35032, 35033, 35034, 35035, 35036, 35037, 35038, 35039, 35040, 35041, 35042, 35043, 35044, 35045, 35046, 35047, 35048, 35049, 35050, 35051, 35052, 35053, 35054, 35055, 35056, 35057, 35058, 35059, 35060, 35061, // Rune
    35010, 35011, 35012, 35013, 35014, 35015, 35016, 35017, 35018, 35019, 35020, 35021, 35022, 35023, 35024, 35025, 35026, 35027, 35028, 35029, 35030, 35031, 35032, 35033, 35034, 35035, 35036, 35037, 35038, 35039, 35040, 35041, 35042, 35043, 35044, 35045, 35046, 35047, 35048, 35049, 35050, 35051, 35052, 35053, 35054, 35055, 35056, 35057, 35058, 35059, 35060, 35061, // Schuttgart
]

const enum CastleMercenaries {
    Gludio = 100,
    Dion = 150,
    Giran = 200,
    Oren = 300,
    Aden = 400,
    Innadril = 400,
    Goddard = 400,
    Rune = 400,
    Schuttgart = 400
}

const mercenariesPerCastle: Array<number> = [
    CastleMercenaries.Gludio,
    CastleMercenaries.Dion,
    CastleMercenaries.Giran,
    CastleMercenaries.Oren,
    CastleMercenaries.Aden,
    CastleMercenaries.Innadril,
    CastleMercenaries.Goddard,
    CastleMercenaries.Rune,
    CastleMercenaries.Schuttgart,
]

const guardianTypes = 52

class Manager implements L2DataApi {
    droppedTickets: Array<number> = []

    async load(): Promise<Array<string>> {
        let data: Array<L2CastleSiegeGuardsTableData> = await DatabaseManager.getCastleSiegeGuards().getHiredGuards()
        let manager = this

        let capacity = _.times( 20, _.constant( 0 ) )
        data.forEach( ( item: L2CastleSiegeGuardsTableData ) => {

            let startingIndex = 0
            let castle: Castle = CastleManager.getCastleByCoordinates( item.x, item.y, item.z )
            if ( castle ) {
                if ( castle.getSiege().isInProgress() ) {
                    return
                }

                if ( capacity[ castle.getResidenceId() - 1 ] >= mercenariesPerCastle[ castle.getResidenceId() - 1 ] ) {
                    return
                }

                startingIndex = guardianTypes * ( castle.getResidenceId() - 1 )
                capacity[ castle.getResidenceId() - 1 ] += 1
            }

            for ( let index = startingIndex; index < ( startingIndex + guardianTypes ); index++ ) {
                if ( npcIds[ index ] === item.npcId ) {
                    let ticket: L2ItemInstance = ItemManagerCache.createItem( ticketItemIds[ index ], 'MercenaryTicketManager.load', 1, 0 )

                    ticket.dropMe( 0, item.x, item.y, item.z, 0, false )
                    ticket.setDropTime( 0 )

                    L2World.storeObject( ticket )

                    manager.droppedTickets.push( ticket.getObjectId() )
                    break
                }
            }
        } )

        return [
            `MercenaryTicketManager: loaded mercenary ${ _.size( this.droppedTickets ) } tickets.`,
        ]
    }

    getTicketCastleId( itemId: number ): number {
        for ( let firstIndex = 0; firstIndex < 9; firstIndex++ ) {
            for ( let secondIndex = 0; secondIndex < 50; secondIndex += 10 ) {
                if ( ( ( itemId >= ticketItemIds[ secondIndex + ( firstIndex * guardianTypes ) ] ) && ( itemId <= ticketItemIds[ secondIndex + 9 + ( firstIndex * guardianTypes ) ] ) ) ) {
                    return firstIndex + 1
                }
            }

            if ( ( itemId >= ticketItemIds[ 50 ] ) && ( itemId <= ticketItemIds[ 51 ] ) ) {
                return firstIndex + 1
            }
        }

        return -1
    }

    isMercenaryTicket( itemId: number ): boolean {
        return mercenaryTickets.has( itemId )
    }

    // TODO : store counts separately to avoid iterations
    isAtCastleLimit( itemId: number ): boolean {
        let castleId = this.getTicketCastleId( itemId )
        if ( castleId <= 0 ) {
            return true
        }

        let limit = this.getMaxMercenaryPerCastle( castleId - 1 )
        if ( limit <= 0 ) {
            return true
        }

        let count = 0
        _.each( this.droppedTickets, ( objectId: number ) => {
            let item: L2ItemInstance = L2World.getObjectById( objectId ) as L2ItemInstance
            if ( item && ( MercenaryTicketManager.getTicketCastleId( item.getId() ) === castleId ) ) {
                count++
            }
        } )

        return count >= limit
    }

    // TODO : store counts separately to avoid iterations
    isAtTypeLimit( itemId: number ): boolean {
        let index = _.findIndex( ticketItemIds, ( id: number ) => {
            return id === itemId
        } )

        let limit = _.nth( maxMercenaryPerType, index ) || -1
        if ( limit <= 0 ) {
            return true
        }

        let count = 0
        _.each( this.droppedTickets, ( objectId: number ) => {
            let item: L2ItemInstance = L2World.getObjectById( objectId ) as L2ItemInstance
            if ( item && item.getId() === itemId ) {
                count++
            }
        } )

        return count >= limit
    }

    // TODO : use spacial query
    isTooCloseToAnotherTicket( x: number, y: number, z: number ): boolean {
        return this.droppedTickets.some( ( objectId: number ) => {
            let item: L2ItemInstance = L2World.getObjectById( objectId ) as L2ItemInstance
            if ( !item ) {
                return false
            }

            let dx = x - item.getX()
            let dy = y - item.getY()
            let dz = z - item.getZ()

            return ( ( dx * dx ) + ( dy * dy ) + ( dz * dz ) ) < ( 25 * 25 )
        } )
    }

    addTicket( itemId: number, activeChar: L2PcInstance ): number {
        let x = activeChar.getX()
        let y = activeChar.getY()
        let z = activeChar.getZ()
        let heading = activeChar.getHeading()

        let castle: Castle = CastleManager.getCastle( activeChar )
        if ( !castle ) {
            return -1
        }

        // TODO : implement creation of mercenary ticket

        return -1
    }

    removeTicket( item: L2ItemInstance ): void {
        let itemId = item.getId()
        let npcId = -1

        // TODO : implement removal of mercenary ticket
    }

    getMaxMercenaryPerCastle( index: number ) {
        switch ( index ) {
            case 0:
                return Math.min( ConfigManager.siege.getGludioMaxMercenaries(), CastleMercenaries.Gludio )

            case 1:
                return Math.min( ConfigManager.siege.getDionMaxMercenaries(), CastleMercenaries.Dion )

            case 2:
                return Math.min( ConfigManager.siege.getGiranMaxMercenaries(), CastleMercenaries.Giran )

            case 3:
                return Math.min( ConfigManager.siege.getOrenMaxMercenaries(), CastleMercenaries.Oren )

            case 4:
                return Math.min( ConfigManager.siege.getAdenMaxMercenaries(), CastleMercenaries.Aden )

            case 5:
                return Math.min( ConfigManager.siege.getInnadrilMaxMercenaries(), CastleMercenaries.Innadril )

            case 6:
                return Math.min( ConfigManager.siege.getGoddardMaxMercenaries(), CastleMercenaries.Goddard )

            case 7:
                return Math.min( ConfigManager.siege.getRuneMaxMercenaries(), CastleMercenaries.Rune )

            case 8:
                return Math.min( ConfigManager.siege.getSchuttgartMaxMercenaries(), CastleMercenaries.Schuttgart )
        }

        return 0
    }
}

export const MercenaryTicketManager = new Manager()