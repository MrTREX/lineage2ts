import { ConfigManager } from '../../config/ConfigManager'
import { DatabaseManager } from '../../database/manager'
import { CropProcure } from '../models/manor/CropProcure'
import { SeedProduction } from '../models/manor/SeedProduction'
import { ManorMode } from '../enums/ManorMode'
import { L2Seed } from '../models/L2Seed'
import { L2DataApi } from '../../data/interface/l2DataApi'
import { DataManager } from '../../data/manager'
import { L2CastleManorProductionTableData } from '../../database/interface/CastleManorProductionTableApi'
import { L2CastleManorProcureTableData } from '../../database/interface/CastleManorProcureTableApi'
import { CastleManager } from './CastleManager'
import moment, { Moment } from 'moment'
import { GeneralHelper } from '../helpers/GeneralHelper'
import { Castle } from '../models/entity/Castle'
import { L2Clan } from '../models/L2Clan'
import { L2ClanMember } from '../models/L2ClanMember'
import { SystemMessageBuilder } from '../packets/send/SystemMessage'
import { SystemMessageIds } from '../packets/SystemMessageIdValues'
import { ItemContainer } from '../models/itemcontainer/ItemContainer'
import _ from 'lodash'
import Timeout = NodeJS.Timeout

class Manager implements L2DataApi {

    procure: { [ key: number ]: Array<CropProcure> }
    procureNext: { [ key: number ]: Array<CropProcure> }
    production: { [ key: number ]: Array<SeedProduction> }
    productionNext: { [ key: number ]: Array<SeedProduction> }

    seeds: { [ key: number ]: L2Seed }
    mode: ManorMode = ManorMode.DISABLED
    saveTask: Timeout

    changeModeTask: Timeout
    nextModeChange: Moment

    createCropProcure( item: L2CastleManorProcureTableData ): CropProcure {
        return new CropProcure( item.cropId, item.amount, item.rewardType, item.startAmount, item.price )
    }

    createSeedProduction( item: L2CastleManorProductionTableData ): SeedProduction {
        return new SeedProduction( item.seedId, item.amount, item.price, item.startAmount )
    }

    getAllCropProcure( castleId: number, nextPeriod: boolean ): Array<CropProcure> {
        return nextPeriod ? this.procureNext[ castleId ] : this.procure[ castleId ]
    }

    getCropIds(): Array<number> {
        return _.map( this.seeds, ( seed: L2Seed ) => {
            return seed.getCropId()
        } )
    }

    getCropProcure( castleId: number, cropId: number, nextPeriod: boolean ): CropProcure {
        return _.find( this.getAllCropProcure( castleId, nextPeriod ), ( procure: CropProcure ) => {
            return procure.getId() === cropId
        } )
    }

    getCrops(): Array<L2Seed> {
        let outcome: Array<L2Seed> = []
        let uniqueIds: Set<number> = new Set<number>()

        _.each( this.seeds, ( seed: L2Seed ) => {
            if ( !uniqueIds.has( seed.getCropId() ) ) {
                outcome.push( seed )
                uniqueIds.add( seed.getCropId() )
            }
        } )

        return outcome
    }

    getSeed( seedId: number ): L2Seed {
        return this.seeds[ seedId ]
    }

    getSeedByCrop( cropId: number ): L2Seed {
        return _.find( this.seeds, ( seed: L2Seed ) => {
            return seed.getCropId() === cropId
        } )
    }

    getSeedByCropWithResidence( cropId: number, residenceId: number ): L2Seed {
        return _.find( this.seeds, ( seed: L2Seed ) => {
            return seed.getCropId() === cropId && seed.getCastleId() === residenceId
        } )
    }

    getSeedProduct( castleId: number, seedId: number, nextPeriod: boolean ): SeedProduction {
        return _.find( this.getSeedProduction( castleId, nextPeriod ), ( seedProduct: SeedProduction ) => {
            return seedProduct.getId() === seedId
        } )
    }

    getSeedProduction( castleId: number, nextPeriod: boolean ): Array<SeedProduction> {
        return nextPeriod ? this.productionNext[ castleId ] : this.production[ castleId ]
    }

    getSeedsForCastle( castleId: number ): Array<L2Seed> {
        return _.filter( this.seeds, ( seed: L2Seed ) => {
            return seed.getCastleId() === castleId
        } )
    }

    isManorApproved(): boolean {
        return this.mode === ManorMode.APPROVED
    }

    isModifiablePeriod(): boolean {
        return this.mode === ManorMode.MODIFIABLE
    }

    isNextPeriod( item: L2CastleManorProductionTableData | L2CastleManorProcureTableData ): boolean {
        return item.nextPeriod !== 0
    }

    isUnderMaintenance(): boolean {
        return this.mode === ManorMode.MAINTENANCE
    }

    /*
        TODO : hide all modifications to seeds behind debounced saving
        - certain packets call to modify and directly save items to database
        - certain actions can be called multiple times which can create a problem with too many updates
     */
    async load(): Promise<Array<string>> {
        this.seeds = {}
        this.procure = {}
        this.procureNext = {}
        this.production = {}
        this.productionNext = {}

        if ( ConfigManager.general.allowManor() ) {
            await this.loadSeeds()
            await this.loadCastleData()
            this.loadManorMode()
        }

        return [
            `CastleManorManager : contains ${ _.size( this.seeds ) } seed definitions.`,
            `CastleManorManager : manor is in ${ ManorMode[ this.mode ] } mode.`,
        ]
    }

    async loadCastleData(): Promise<void> {
        let productionData: { [ castleId: number ]: Array<L2CastleManorProductionTableData> } = _.groupBy( await DatabaseManager.getCastleManorProduction().getAll(), 'castleId' )
        let procureData: { [ castleId: number ]: Array<L2CastleManorProcureTableData> } = _.groupBy( await DatabaseManager.getCastleManorProcure().getAll(), 'castleId' )

        let cropIds: Array<number> = _.map( this.seeds, 'cropId' )
        let manager = this

        _.each( CastleManager.getResidenceIds(), ( castleId: number ) => {
            let [ nextProductionItems, currentProductionItems ] = _.partition( productionData[ castleId ], this.isNextPeriod )

            manager.production[ castleId ] = _.map( currentProductionItems, manager.createSeedProduction )
            manager.productionNext[ castleId ] = _.map( nextProductionItems, manager.createSeedProduction )

            let validProcureItems: Array<L2CastleManorProcureTableData> = _.filter( procureData[ castleId ], ( item: L2CastleManorProcureTableData ) => {
                return cropIds.includes( item.cropId )
            } )

            let [ nextProcureItems, currentProcureItems ] = _.partition( validProcureItems, manager.isNextPeriod )

            manager.procure[ castleId ] = _.map( currentProcureItems, manager.createCropProcure )
            manager.procureNext[ castleId ] = _.map( nextProcureItems, manager.createCropProcure )
        } )
    }

    async loadSeeds(): Promise<void> {
        // TODO : use data interface instead of class to decouple loaded import dependencies
        let data: Array<L2Seed> = await DataManager.getManorSeeds().getAll()
        this.seeds = _.keyBy( data, 'seedId' )
    }

    async resetManorData( castleId: number ) {
        if ( !ConfigManager.general.allowManor() ) {
            return
        }

        this.procure[ castleId ] = []
        this.procureNext[ castleId ] = []
        this.production[ castleId ] = []
        this.productionNext[ castleId ] = []

        await DatabaseManager.getCastleManorProduction().removeCastle( castleId )
        await DatabaseManager.getCastleManorProcure().removeCastle( castleId )
    }

    async setNextCropProcure( castleId: number, items: Array<CropProcure> ) {
        this.procureNext[ castleId ] = items
        await DatabaseManager.getCastleManorProcure().removeCastleForNextPeriod( castleId )
        await DatabaseManager.getCastleManorProcure().addCastleProducts( castleId, items )
    }

    async setNextSeedProduction( castleId: number, items: Array<SeedProduction> ): Promise<void> {
        this.productionNext[ castleId ] = items
        await DatabaseManager.getCastleManorProduction().removeCastleForNextPeriod( castleId )
        await DatabaseManager.getCastleManorProduction().addCastleProducts( castleId, items )
    }

    updateCurrentProduction( castleId: number, items: Array<SeedProduction> ): Promise<void> {
        return DatabaseManager.getCastleManorProduction().updateSeedProduction( castleId, items )
    }

    loadManorMode() : void {
        this.mode = this.getManorMode()

        this.scheduleModeChange()

        if ( ConfigManager.general.getManorSavePeriodRate() > 0 ) {
            this.saveTask = setInterval( this.saveToDatabase.bind( this ), GeneralHelper.hoursToMillis( ConfigManager.general.getManorSavePeriodRate() ) )
        }
    }

    getManorMode() : ManorMode {
        let currentTime : Moment = moment()
        let hours = currentTime.hours()
        let minutes = currentTime.minutes()

        let maintenanceMinutes = ConfigManager.general.getManorRefreshMin() + ConfigManager.general.getManorMaintenanceMin()
        if ( ( hours >= ConfigManager.general.getManorRefreshTime() && minutes >= maintenanceMinutes )
                || hours < ConfigManager.general.getManorApproveTime()
                || ( hours === ConfigManager.general.getManorApproveTime() && minutes <= ConfigManager.general.getManorApproveMin() ) ) {
            return ManorMode.MODIFIABLE
        }

        if ( hours === ConfigManager.general.getManorRefreshTime()
                && ( minutes >= ConfigManager.general.getManorRefreshMin() && minutes < maintenanceMinutes ) ) {
            return ManorMode.MAINTENANCE
        }

        return ManorMode.APPROVED
    }

    scheduleModeChange() : void {
        let time : Moment = moment()

        switch ( this.mode ) {
            case ManorMode.MODIFIABLE:
                time.hours( ConfigManager.general.getManorApproveTime() ).minutes( ConfigManager.general.getManorApproveMin() )

                if ( time.isBefore( moment() ) ) {
                    time.add( 1, 'days' )
                }
                break

            case ManorMode.MAINTENANCE:
                time.hours( ConfigManager.general.getManorRefreshTime() ).minutes( ConfigManager.general.getManorRefreshMin() + ConfigManager.general.getManorMaintenanceMin() )
                break

            case ManorMode.APPROVED:
                time.hours( ConfigManager.general.getManorRefreshTime() ).minutes( ConfigManager.general.getManorRefreshMin() )
                break
        }

        this.nextModeChange = time.second( 0 )
        this.changeModeTask = setTimeout( this.changeMode.bind( this ), this.nextModeChange.valueOf() - Date.now() )
    }

    async changeMode() : Promise<void> {
        let manager = this

        switch ( this.mode ) {
            case ManorMode.APPROVED:
                this.mode = ManorMode.MAINTENANCE

                _.each( CastleManager.getCastles(), ( castle: Castle ) => {
                    let clan : L2Clan = castle.getOwner()
                    if ( !clan ) {
                        return
                    }

                    let castleId = castle.getResidenceId()
                    let warehouse : ItemContainer = clan.getWarehouse()

                    _.each( manager.procure[ castleId ], ( crop: CropProcure ) => {
                        if ( crop.getStartAmount() > 0 ) {
                            if ( crop.getStartAmount() !== crop.getAmount() ) {
                                let count = Math.floor( ( crop.getStartAmount() - crop.getAmount() ) * 0.9 )
                                if ( count < 1 && _.random( 99 ) < 90 ) {
                                    count = 1
                                }

                                if ( count > 0 ) {
                                    warehouse.addItem( manager.getSeedByCrop( crop.getId() ).getMatureId(), count, null, 'CastleManorManager.changeMode' )
                                }
                            }
                            if ( crop.getAmount() > 0 ) {
                                castle.addToTreasuryNoTax( crop.getAmount() * crop.getPrice() )
                            }
                        }
                    } )

                    manager.production[ castleId ] = manager.productionNext[ castleId ]
                    manager.procure[ castleId ] = manager.procureNext[ castleId ]

                    if ( castle.getTreasury() < manager.getManorCost( castleId, false ) ) {
                        manager.productionNext[ castleId ] = []
                        manager.procureNext[ castleId ] = []
                        return
                    }

                    _.each( manager.productionNext[ castleId ], ( production: SeedProduction ) => {
                        production.amount = production.startAmount
                    } )

                    _.each( manager.procureNext[ castleId ], ( procure: CropProcure ) => {
                        procure.amount = procure.startAmount
                    } )
                } )

                await this.saveToDatabase()
                break

            case ManorMode.MAINTENANCE:
                _.each( CastleManager.getCastles(), ( castle: Castle ) => {
                    let clan : L2Clan = castle.getOwner()
                    if ( !clan ) {
                        return
                    }

                    let clanLeader : L2ClanMember = clan.getLeader()
                    let leaderPlayer = clanLeader.getPlayerInstance()
                    if ( leaderPlayer && leaderPlayer.isOnline() ) {
                        leaderPlayer.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THE_MANOR_INFORMATION_HAS_BEEN_UPDATED ) )
                    }
                } )

                this.mode = ManorMode.MODIFIABLE
                break

            case ManorMode.MODIFIABLE:
                this.mode = ManorMode.APPROVED

                _.each( CastleManager.getCastles(), ( castle: Castle ) => {
                    let clan : L2Clan = castle.getOwner()
                    if ( !clan ) {
                        return
                    }

                    let slots = 0
                    let castleId = castle.getResidenceId()
                    let warehouse : ItemContainer = clan.getWarehouse()

                    _.each( manager.procureNext[ castleId ], ( crop: CropProcure ) => {
                        if ( crop.getStartAmount() > 0 && !warehouse.getItemsByItemId( manager.getSeedByCrop( crop.getId() ).getMatureId() ) ) {
                            slots++
                        }
                    } )

                    let manorCost = manager.getManorCost( castleId, true )
                    if ( !warehouse.validateCapacity( slots ) && castle.getTreasury() < manorCost ) {
                        manager.productionNext[ castleId ] = []
                        manager.procureNext[ castleId ] = []

                        let clanLeader : L2ClanMember = clan.getLeader()
                        let leaderPlayer = clanLeader.getPlayerInstance()
                        if ( leaderPlayer && leaderPlayer.isOnline() ) {
                            leaderPlayer.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THE_AMOUNT_IS_NOT_SUFFICIENT_AND_SO_THE_MANOR_IS_NOT_IN_OPERATION ) )
                        }

                        return
                    }

                    castle.addToTreasuryNoTax( -manorCost )
                } )

                if ( ConfigManager.general.manorSaveAllActions() ) {
                    await this.saveToDatabase()
                }

                break
        }

        this.scheduleModeChange()
    }

    getManorCost( castleId: number, isNextPeriod: boolean = false ) : number {
        let seedProduction : Array<SeedProduction> = this.getSeedProduction( castleId, isNextPeriod )
        let cropProcure : Array<CropProcure> = this.getAllCropProcure( castleId, isNextPeriod )

        let total = 0
        let manager = this

        _.each( seedProduction, ( production: SeedProduction ) => {
            let currentSeed : L2Seed = manager.getSeed( production.getId() )
            total += !currentSeed ? 1 : ( currentSeed.getSeedReferencePrice() * production.getStartAmount() )

        } )

        _.each( cropProcure, ( crop: CropProcure ) => {
            total += crop.getPrice() * crop.getStartAmount()
        } )

        return total
    }

    async saveToDatabase() : Promise<void> {
        await DatabaseManager.getCastleManorProduction().deleteAll()
        await DatabaseManager.getCastleManorProcure().deleteAll()

        await DatabaseManager.getCastleManorProduction().addAll( this.getDatabaseProductionModels( this.production ) )
        await DatabaseManager.getCastleManorProduction().addAll( this.getDatabaseProductionModels( this.productionNext, true ) )

        await DatabaseManager.getCastleManorProcure().addAll( this.getDatabaseProcureModels( this.procure ) )
        await DatabaseManager.getCastleManorProcure().addAll( this.getDatabaseProcureModels( this.procureNext, true ) )
    }

    getDatabaseProductionModels( data : { [ key: number ]: Array<SeedProduction> }, isNextPeriod: boolean = false ) : Array<L2CastleManorProductionTableData> {
        let nextPeriod : number = isNextPeriod ? 1 : 0

        return _.flatMap( data, ( items: Array<SeedProduction>, castleIdString: string ) : Array<L2CastleManorProductionTableData> => {
            let castleId : number = _.parseInt( castleIdString )

            return _.map( items, ( item: SeedProduction ) : L2CastleManorProductionTableData => {
                return {
                    amount: item.amount,
                    castleId,
                    nextPeriod,
                    price: item.price,
                    seedId: item.seedId,
                    startAmount: item.startAmount
                }
            } )
        } )
    }

    getDatabaseProcureModels( data : { [ key: number ]: Array<CropProcure> }, isNextPeriod: boolean = false ) : Array<L2CastleManorProcureTableData> {
        let nextPeriod : number = isNextPeriod ? 1 : 0

        return _.flatMap( data, ( items: Array<CropProcure>, castleIdString: string ) : Array<L2CastleManorProcureTableData> => {
            let castleId : number = _.parseInt( castleIdString )

            return _.map( items, ( item: CropProcure ) : L2CastleManorProcureTableData => {
                return {
                    rewardType: item.rewardType,
                    amount: item.amount,
                    castleId,
                    nextPeriod,
                    price: item.price,
                    cropId: item.seedId,
                    startAmount: item.startAmount
                }
            } )
        } )
    }
}

export const CastleManorManager = new Manager()