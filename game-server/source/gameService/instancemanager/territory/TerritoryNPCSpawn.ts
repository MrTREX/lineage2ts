import { L2Npc } from '../../models/actor/L2Npc'
import { L2World } from '../../L2World'
import { Location } from '../../models/Location'

export class TerritoryNPCSpawn {
    location: Location
    npcId: number
    castleId: number
    type: number
    npc: L2Npc

    getId() {
        return this.npcId
    }

    getNpc() : L2Npc {
        return L2World.getObjectById( this.npcId ) as L2Npc
    }

    setNPC( npc: L2Npc ) {
        if ( this.npcId ) {
            let oldNpc = L2World.getObjectById( this.npcId ) as L2Npc
            oldNpc.deleteMe()
        }

        this.npcId = npc ? npc.getObjectId() : 0
    }
}