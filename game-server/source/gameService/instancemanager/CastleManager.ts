import { L2Object } from '../models/L2Object'
import { L2Clan } from '../models/L2Clan'
import { L2ClanMember } from '../models/L2ClanMember'
import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { L2ItemInstance } from '../models/items/instance/L2ItemInstance'
import { DatabaseManager } from '../../database/manager'
import { L2DataApi } from '../../data/interface/l2DataApi'
import { L2CastleTableData } from '../../database/interface/CastleTableApi'
import { SevenSignsSide } from '../values/SevenSignsValues'
import { Castle } from '../models/entity/Castle'
import aigle from 'aigle'
import _, { DebouncedFunc } from 'lodash'
import { ListenerCache } from '../cache/ListenerCache'
import { ListenerRegisterType } from '../enums/ListenerRegisterType'
import { EventType, GeoRegionActivatedEvent } from '../models/events/EventType'
import { getGeoRegionCode } from '../enums/L2MapTile'
import chalk from 'chalk'
import { RegionActivation } from '../models/RegionActivation'
import { L2World } from '../L2World'
import { CastleUpdateType } from '../enums/CastleUpdateType'
import { RespawnRegionCache } from '../cache/RespawnRegionCache'
import { AreaType } from '../models/areas/AreaType'
import { CastleArea } from '../models/areas/type/Castle'
import { ServerLog } from '../../logger/Logger'

const messageIdToResidenceId : Record<number, number> = {
    912: 1,
    916: 2,
    918: 3,
    922: 4,
    924: 5,
    926: 6,
    1538: 7,
    1537: 8,
    1714: 9,
}

class Manager implements L2DataApi {
    castles: Array<Castle>
    castleCirclets: Array<number> = [
        0,
        6838,
        6835,
        6839,
        6837,
        6840,
        6834,
        6836,
        8182,
        8183
    ]

    activatedRegions: RegionActivation = new RegionActivation()
    regions: Record<number, Castle>
    castleByResidenceId: Record<number, Castle>
    castlesToUpdate: Set<Castle> = new Set<Castle>()
    debounceCastleUpdate: DebouncedFunc<() => void>

    async load(): Promise<Array<string>> {
        this.activatedRegions.clear()
        this.regions = {}
        this.castleByResidenceId = {}
        this.debounceCastleUpdate = _.debounce( this.runCastleUpdate.bind( this ), 5000, {
            maxWait: 10000
        } )

        let castleData: Array<L2CastleTableData> = await DatabaseManager.getCastleTable().getAll()
        let castleOwnership: Record<number, number> = await DatabaseManager.getClanDataTable().getCastleOwnerIds()

        this.castles = await aigle.resolve( castleData ).mapSeries( async ( data: L2CastleTableData ) => {
            let castle = new Castle( data.id )

            castle.name = data.name
            castle.siegeDate = data.siegeDate
            castle.siegeTimeRegistrationEndDate = data.registrationTimeEnd
            castle.isTimeRegistrationOver = data.isRegistrationOver

            castle.taxPercent = data.taxPercent
            castle.treasury = data.treasury
            castle.showNpcCrest = data.showNpcCrest
            castle.ticketBuyCount = data.ticketBuyCount

            castle.ownerId = castleOwnership[ data.id ] ?? 0
            castle.initAreas()

            let location = castle.residenceArea.getCenterPointLocation()
            let code = getGeoRegionCode( location.getX(), location.getY() )
            if ( this.regions[ code ] ) {
                throw new Error( 'Too many castles per region' )
            }

            this.regions[ code ] = castle
            this.castleByResidenceId[ castle.getResidenceId() ] = castle

            // TODO: consider creating own castle search grid based on castle zones in L2World

            return castle
        } )

        ListenerCache.registerListener( ListenerRegisterType.General, EventType.GeoRegionActivated, this, this.onRegionActivation.bind( this ) )

        return [
            `CastleManager : loaded ${ _.size( this.castles ) } castles.`,
            `CastleManager : prepared ${ _.size( this.regions ) } geo-regions for on-demand load.`,
        ]
    }

    async onRegionActivation( data: GeoRegionActivatedEvent ): Promise<void> {
        if ( this.activatedRegions.isActivated( data.code ) ) {
            return
        }

        let castle = this.regions[ data.code ]
        if ( !castle || castle.isActivated() ) {
            return
        }

        this.activatedRegions.markActivated( data.code )
        let startTime = Date.now()
        await castle.initialize()

        ServerLog.info( `CastleManager: activated ${ chalk.greenBright( castle.getName() ) } castle in ${ Date.now() - startTime } ms` )
    }

    getCastleByOwner( clan: L2Clan ): Castle {
        return this.castles.find( ( currentCastle: Castle ) => {
            return currentCastle.getOwnerId() === clan.getId()
        } )
    }

    getCastle( object: L2Object ): Castle {
        return this.getCastleByCoordinates( object.getX(), object.getY(), object.getZ() )
    }

    getCastleByCoordinates( x: number, y: number, z: number ): Castle {
        let area = L2World.getNearestAreaType( x, y, AreaType.Castle ) as CastleArea
        if ( !area || area.isInsideWithCoordinates( x, y, z ) ) {
            return null
        }

        return this.getCastleById( area.getResidenceId() )
    }

    getCastles(): Array<Castle> {
        return this.castles
    }

    getCastleById( id: number ): Castle {
        return this.castleByResidenceId[ id ]
    }

    findNearestCastleId( object: L2Object ): number {
        let area = L2World.getNearestAreaType( object.getX(), object.getY(), AreaType.Castle ) as CastleArea
        if ( !area ) {
            return 0
        }

        return area.getResidenceId()
    }

    async removeCirclet( clan: L2Clan, castleId: number ): Promise<void> {
        let circletId = this.getCircletByCastleId( castleId )
        if ( circletId === 0 ) {
            return
        }

        let castle = this
        let memberIds = await aigle.resolve( clan.getMembers() ).reduce( async ( allIds: Array<number>, member: L2ClanMember ): Promise<Array<number>> => {
            if ( !member.isOnline() ) {
                allIds.push( member.getObjectId() )
            } else {
                let player: L2PcInstance = member.getPlayerInstance()
                if ( player ) {
                    await castle.removeCircletById( player, circletId )
                }
            }

            return allIds
        }, [] )

        return DatabaseManager.getItems().deleteItemFromOwners( circletId, memberIds )
    }

    async removeMemberCirclet( member: L2ClanMember, castleId: number ): Promise<void> {
        if ( !member ) {
            return
        }

        let circletId = this.getCircletByCastleId( castleId )

        if ( circletId !== 0 && member.isOnline() ) {
            let player: L2PcInstance = member.getPlayerInstance()
            if ( player ) {
                return this.removeCircletById( player, circletId )
            }
        }
    }

    async removeCircletById( player: L2PcInstance, circletId: number ): Promise<void> {
        let circlet: L2ItemInstance = player.getInventory().getItemByItemId( circletId )
        if ( circlet ) {
            if ( circlet.isEquipped() ) {
                await player.getInventory().unEquipItemInSlot( circlet.getLocationSlot() )
            }

            await player.destroyItemByItemId( circletId, 1, true, 'CastleManager.removeCircletById' )
        }
    }

    getCircletByCastleId( castleId: number ) {
        if ( ( castleId > 0 ) && ( castleId < 10 ) ) {
            return this.castleCirclets[ castleId ]
        }

        return 0
    }

    async validateTaxes( sealOwner: SevenSignsSide ): Promise<void> {
        let taxValue = 15

        if ( sealOwner === SevenSignsSide.Dusk ) {
            taxValue = 5
        }

        if ( sealOwner === SevenSignsSide.Dawn ) {
            taxValue = 25
        }

        await aigle.resolve( this.castles ).eachSeries( async ( castle: Castle ) => {
            if ( castle.getTaxPercent() > taxValue ) {
                await castle.setTaxPercent( taxValue )
            }
        } )
    }

    getResidenceIds(): Array<number> {
        return this.castles.map( ( castle: Castle ) => castle.residenceId )
    }

    async runCastleUpdate() : Promise<void> {
        let castles = Array.from( this.castlesToUpdate )
        this.castlesToUpdate.clear()

        let promises : Array<Promise<void>> = []
        let treasuryUpdates : Array<Castle> = []
        let crestUpdates: Array<Castle> = []
        let taxUpdate: Array<Castle> = []
        let ticketUpdates: Array<Castle> = []

        castles.forEach( ( castle: Castle ) => {
            if ( castle.hasUpdateType( CastleUpdateType.Tax ) ) {
                taxUpdate.push( castle )
            }

            if ( castle.hasUpdateType( CastleUpdateType.Treasury ) ) {
                treasuryUpdates.push( castle )
            }

            if ( castle.hasUpdateType( CastleUpdateType.CrestStatus ) ) {
                crestUpdates.push( castle )
            }

            if ( castle.hasUpdateType( CastleUpdateType.TicketCount ) ) {
                ticketUpdates.push( castle )
            }

            castle.clearUpdates()
        } )

        if ( treasuryUpdates.length > 0 ) {
            promises.push( DatabaseManager.getCastleTable().updateTreasury( treasuryUpdates ) )
        }

        if ( crestUpdates.length > 0 ) {
            promises.push ( DatabaseManager.getCastleTable().updateCrestStatus( crestUpdates ) )
        }

        if ( taxUpdate.length > 0 ) {
            promises.push( DatabaseManager.getCastleTable().updateTaxValue( taxUpdate ) )
        }

        if ( ticketUpdates.length > 0 ) {
            promises.push( DatabaseManager.getCastleTable().updateTicketBuyCount( ticketUpdates ) )
        }

        await Promise.all( promises )
    }

    scheduleUpdate( castle: Castle ) : void {
        this.castlesToUpdate.add( castle )
        this.debounceCastleUpdate()
    }

    townHasCastleInSiege( x: number, y: number ) : boolean {
        let messageId = RespawnRegionCache.getLocationMessageIdByCoordinates( x, y )
        let castleIndex : number = messageIdToResidenceId[ messageId ]

        if ( castleIndex > 0 ) {
            let castle : Castle = CastleManager.getCastleById( castleIndex )
            if ( castle ) {
                return castle.getSiege().isInProgress()
            }
        }

        return false
    }

    getCastleIdByRespawn( object: L2Object ) : number {
        let messageId = RespawnRegionCache.getLocationMessageId( object )
        return messageIdToResidenceId[ messageId ]
    }
}

export const CastleManager = new Manager()