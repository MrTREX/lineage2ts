import { ListenerLogic } from '../models/ListenerLogic'
import { ConfigManager } from '../../config/ConfigManager'
import _ from 'lodash'
import { ServerLog } from '../../logger/Logger'

class Manager {
    allListeners: { [ key: string ]: ListenerLogic } = {}
    questIdMap: { [ questId: number ]: ListenerLogic } = {}

    getQuest( id: number ): ListenerLogic {
        return this.questIdMap[ id ]
    }

    getListenerByName( name: string ): ListenerLogic {
        return this.allListeners[ name ]
    }

    register( listener: ListenerLogic ): void {
        if ( this.allListeners[ listener.getName() ] || this.questIdMap[ listener.getId() ] ) {
            let existingQuest = _.defaultTo( this.allListeners[ listener.getName() ], this.questIdMap[ listener.getId() ] )
            ServerLog.error( `Failed to re-register new quest for '${ listener.getId() } - ${ listener.getDescription() }'\nExisting quest is '${ existingQuest.getId() } - ${ existingQuest.getDescription() }'` )
            return
        }

        this.allListeners[ listener.getName() ] = listener

        if ( listener.getId() > 0 ) {
            this.questIdMap[ listener.getId() ] = listener

            if ( !listener.getPathPrefix() ) {
                ServerLog.error( `Quest Id ${ listener.getId() } - ${ listener.getDescription() } : must have path prefix` )
            }

            if ( ConfigManager.diagnostic.showDebugMessages() ) {
                ServerLog.info( `Registered quest: ${ listener.getId() } - ${ listener.getDescription() }` )
            }
        }
    }
}

export const ListenerManager = new Manager()