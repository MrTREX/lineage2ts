import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { Petition } from '../models/Petition'
import { PacketDispatcher } from '../PacketDispatcher'
import { AdminManager } from '../cache/AdminManager'
import { CreatureSay } from '../packets/send/builder/CreatureSay'
import { ConfigManager } from '../../config/ConfigManager'
import { PetitionState } from '../enums/PetitionState'
import _ from 'lodash'
import { NpcSayType } from '../enums/packets/NpcSayType'

class Manager {
    pendingPetitions: { [ key: number ]: Petition } = {}
    completedPetitions: { [ key: number ]: Petition } = {}

    checkPetitionMessages( player: L2PcInstance ) {
        if ( player ) {
            _.each( this.pendingPetitions, ( petition: Petition ) => {
                if ( !petition ) {
                    return
                }

                if ( petition.getPetitioner() === player.getObjectId() ) {
                    petition.getLogMessages().forEach( ( packet: Buffer ) => {
                        player.sendCopyData( packet )
                    } )
                }
            } )
        }
    }

    getPendingPetitionCount() {
        return _.size( this.pendingPetitions )
    }

    submitPetition( player: L2PcInstance, message: string, type: number ) : number {
        let petition : Petition = new Petition( player, message, type )
        let currentId = petition.getId()
        this.pendingPetitions[ currentId ] = petition

        let line = `${player.getName()} has submitted a petition.`

        /*
        Since CreatureSay requires origin player to be defined we have to manually provide this step instead of normally
        handing off packet creation to methods such as sendPacket.
         */

        AdminManager.getAllGMs().forEach( ( adminId: number ) => {
            PacketDispatcher.sendOwnedData( adminId, CreatureSay.fromText( adminId, player.getObjectId(), NpcSayType.HeroVoice, 'Petition System', line ) )
        } )

        return currentId
    }

    getPlayerTotalPetitionCount( player: L2PcInstance ) : number {
        if ( !player ) {
            return 0
        }

        let count = 0

        let countPetition = ( petition: Petition ) => {
            if ( petition && petition.getPetitioner() === player.getObjectId() ) {
                count++
            }
        }

        _.each( this.pendingPetitions, countPetition )
        _.each( this.completedPetitions, countPetition )

        return count
    }

    isPlayerPetitionPending( player: L2PcInstance ) : boolean {
        return _.some( this.pendingPetitions, ( petition: Petition ) => {
            return petition && petition.getPetitioner() === player.getObjectId()
        } )
    }

    isPetitioningAllowed() : boolean {
        return ConfigManager.character.petitioningAllowed()
    }

    cancelActivePetition( player: L2PcInstance ) : boolean {
        return _.some( this.pendingPetitions, ( petition: Petition ) => {
            if ( petition.getPetitioner() === player.getObjectId() ) {
                petition.endPetitionConsultation( PetitionState.PETITIONER_CANCEL )
                return true
            }

            if ( petition.getResponder() === player.getObjectId() ) {
                petition.endPetitionConsultation( PetitionState.RESPONDER_CANCEL )
                return true
            }

            return false
        } )
    }

    markPetitionCompleted( petitionId: number ) {
        let petition : Petition = this.pendingPetitions[ petitionId ]

        if ( petition ) {
            this.completedPetitions[ petition.getId() ] = petition
        }

        _.unset( this.pendingPetitions, petition.getId() )
    }

    isPlayerInConsultation( player: L2PcInstance ) {
        return _.some( this.pendingPetitions, ( petition: Petition ) => {
            return petition.getState() === PetitionState.IN_PROCESS
                    && ( petition.getPetitioner() === player.getObjectId() || petition.getResponder() === player.getObjectId() )
        } )
    }

    endActivePetition( player: L2PcInstance ) : boolean {
        if ( !player.isGM() ) {
            return false
        }

        return _.some( this.pendingPetitions, ( petition: Petition ) => {
            if ( petition.getResponder() === player.getObjectId() ) {
                petition.endPetitionConsultation( PetitionState.COMPLETED )
                return true
            }

            return false
        } )
    }

    sendActivePetitionMessage( player: L2PcInstance, message: string ) : void {
        _.each( this.pendingPetitions, ( petition: Petition ) => {
            if ( !petition ) {
                return
            }

            let messageType : number
            switch ( player.getObjectId() ) {
                case petition.getPetitioner():
                    messageType = NpcSayType.PetitionPlayer
                    break

                case petition.getResponder():
                    messageType = NpcSayType.PetitionGM
                    break
            }

            if ( messageType > 0 ) {
                let packet : Buffer = CreatureSay.fromText( 0, player.getObjectId(), messageType, player.getName(), message )

                petition.addLogMessage( packet )
                petition.sendResponderPacket( packet )
                petition.sendPetitionerPacket( packet )
                return
            }
        } )
    }
}

export const PetitionManager = new Manager()