import { L2BoatInstance } from '../models/actor/instance/L2BoatInstance'
import { VehiclePathPoint } from '../models/VehiclePathPoint'
import { GeneralHelper } from '../helpers/GeneralHelper'
import { L2World } from '../L2World'
import { L2CharacterTemplate } from '../models/actor/templates/L2CharacterTemplate'
import { ConfigManager } from '../../config/ConfigManager'
import { ListenerCache } from '../cache/ListenerCache'
import { EventType, VehiclePathEndedEvent, VehiclePathStartedEvent } from '../models/events/EventType'
import { AIEffectHelper } from '../aicontroller/helpers/AIEffectHelper'
import _ from 'lodash'

interface BoatInformation {
    objectId: number
    path: Array<VehiclePathPoint>
    pointIndex: number
}

export enum ShipDock {
    TalkingIsland,
    GludinHarbor,
    RuneHarbor
}

class Manager {
    boats: Array<number> = []
    docksBusy : Array<boolean> = _.times( _.size( ShipDock ) / 2, _.constant( false ) )
    paths : { [ objectId: number ] : BoatInformation } = {}

    isBoat( objectId: number ) : boolean {
        return this.boats.includes( objectId )
    }

    isDockBusy( type: ShipDock ) : boolean {
        return _.defaultTo( _.nth( this.docksBusy, type ), false )
    }

    dockShip( type: ShipDock, isDocked : boolean = true ) : void {
        this.docksBusy[ type ] = isDocked
    }

    async moveBoat( objectId: number ) : Promise<boolean> {
        let info : BoatInformation = this.paths[ objectId ]

        if ( !info ) {
            return false
        }

        if ( info.path ) {
            info.pointIndex++

            if ( info.pointIndex < info.path.length ) {
                let point : VehiclePathPoint = info.path[ info.pointIndex ]
                let boat : L2BoatInstance = L2World.getObjectById( objectId ) as L2BoatInstance

                if ( !boat.isMovementDisabled() ) {
                    if ( point.getMoveSpeed() === 0 ) {
                        point.setHeading( point.getRotationSpeed() )
                        await boat.teleportToLocation( point, false )
                        info.path = null
                    } else {
                        if ( point.getMoveSpeed() > 0 ) {
                            boat.getStat().setMoveSpeed( point.getMoveSpeed() )
                        }
                        if ( point.getRotationSpeed() > 0 ) {
                            boat.getStat().setRotationSpeed( point.getRotationSpeed() )
                        }

                        boat.setDestinationCoordinates( point.getX(), point.getY(), point.getZ() )

                        // TODO : remove, heading setting should be in Moving Manager
                        let distance = Math.hypot( point.getX() - boat.getX(), point.getY() - boat.getY() )
                        if ( distance > 1 ) {
                            boat.setHeading( GeneralHelper.calculateHeadingFromLocations( boat, point ) )
                        }


                        boat.startMoving()
                        return true
                    }
                }
            } else {
                info.path = null
            }
        }

        let data : VehiclePathEndedEvent = {
            objectId
        }

        // TODO : refactor to better send event off either object ids (right now includes general listeners) or another id
        await ListenerCache.sendObjectEvent( objectId, EventType.VehiclePathEnded, data )
        return false
    }

    getBoatTemplate() : L2CharacterTemplate {
        let template = new L2CharacterTemplate()

        template.baseHpMax = 50000
        template.basePDef = 100
        template.baseMDef = 100

        return template
    }

    registerBoat( boatId: number, x: number, y: number, z: number, heading: number ) : L2BoatInstance {
        if ( !ConfigManager.general.allowBoat() ) {
            return
        }

        let boat : L2BoatInstance = new L2BoatInstance( this.getBoatTemplate() )

        boat.setHeading( heading )
        boat.setXYZInvisible( x, y, z )
        boat.spawnMeNow()

        this.boats.push( boat.getObjectId() )

        return boat
    }

    async setBoathPath( objectId: number, path: Array<VehiclePathPoint>, startIndex: number = 0 ) : Promise<void> {
        this.paths[ objectId ] = {
            objectId,
            path,
            pointIndex: startIndex
        }

        AIEffectHelper.notifyMove( L2World.getObjectById( objectId ) as L2BoatInstance, path[ startIndex ] )

        let data : VehiclePathStartedEvent = {
            objectId
        }

        return ListenerCache.sendObjectEvent( objectId, EventType.VehiclePathStarted, data )
    }
}

export const BoatManager = new Manager()