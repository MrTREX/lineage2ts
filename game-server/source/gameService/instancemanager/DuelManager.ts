import { Duel } from '../models/entity/Duel'
import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { PrivateStoreType } from '../enums/PrivateStoreType'
import { SystemMessageBuilder } from '../packets/send/SystemMessage'
import { SystemMessageIds } from '../packets/SystemMessageIdValues'
import _ from 'lodash'
import { AreaType } from '../models/areas/AreaType'

class Manager {
    duels: { [ key: number ]: Duel } = {}
    currentDuelId: number = 0

    getDuel( id: number ) : Duel {
        return this.duels[ id ]
    }

    broadcastToOpposingTeam( player: L2PcInstance, method: ( id: number ) => Buffer, id: number ) : void {
        if ( !player || !player.isInDuel() ) {
            return
        }

        let duel: Duel = this.getDuel( player.getDuelId() )
        if ( !duel ) {
            return
        }

        let packet : Buffer = method( id )
        if ( duel.getTeamA().includes( player.getObjectId() ) ) {
            return duel.broadcastToTeamB( packet )
        }

        return duel.broadcastToTeamA( packet )
    }

    onPlayerDefeat( player: L2PcInstance ) : void {
        if ( player || !player.isInDuel() ) {
            return
        }

        let duel : Duel = this.getDuel( player.getDuelId() )
        if ( duel ) {
            duel.onPlayerDefeat( player )
        }
    }

    canDuel( player: L2PcInstance, target: L2PcInstance, isPartyDuel: boolean ) : boolean {
        let messageId : number = this.getDuelMessageId( target, isPartyDuel )

        if ( messageId ) {
            let packet = new SystemMessageBuilder( messageId )
                    .addString( target.getName() )
                    .getBuffer()
            player.sendOwnedData( packet )
            return false
        }

        return true
    }

    addDuel( player: L2PcInstance, target: L2PcInstance, isPartyDuel: boolean ) : void {
        if ( !player || !target ) {
            return
        }

        let duelId = this.currentDuelId
        this.currentDuelId++

        this.duels[ duelId ] = new Duel( player, target, isPartyDuel, duelId )
    }

    removeDuel( duel: Duel ) : void {
        _.unset( this.duels, duel.getId() )
    }

    doSurrender( player: L2PcInstance ) : void {
        if ( !player || !player.isInDuel() ) {
            return
        }

        let duel : Duel = this.getDuel( player.getDuelId() )
        duel.doSurrender( player )
    }

    private getDuelMessageId( target: L2PcInstance, isPartyDuel: boolean ) : number {
        if ( target.isInCombat() || target.isJailed() ) {
            return SystemMessageIds.C1_CANNOT_DUEL_BECAUSE_C1_IS_CURRENTLY_ENGAGED_IN_BATTLE
        }

        if ( target.isTransformed() ) {
            return SystemMessageIds.C1_CANNOT_DUEL_WHILE_POLYMORPHED
        }

        if ( target.isInDuel() ) {
            return SystemMessageIds.C1_CANNOT_DUEL_BECAUSE_C1_IS_ALREADY_ENGAGED_IN_A_DUEL
        }

        if ( target.isInOlympiadMode() ) {
            return SystemMessageIds.C1_CANNOT_DUEL_BECAUSE_C1_IS_PARTICIPATING_IN_THE_OLYMPIAD
        }

        if ( target.isCursedWeaponEquipped()
                || target.getKarma() > 0
                || target.hasPvPFlag() ) {
            return SystemMessageIds.C1_CANNOT_DUEL_BECAUSE_C1_IS_IN_A_CHAOTIC_STATE
        }

        if ( target.getPrivateStoreType() !== PrivateStoreType.None ) {
            return SystemMessageIds.C1_CANNOT_DUEL_BECAUSE_C1_IS_CURRENTLY_ENGAGED_IN_A_PRIVATE_STORE_OR_MANUFACTURE
        }

        if ( target.isMounted() || target.isInBoat() ) {
            return SystemMessageIds.C1_CANNOT_DUEL_BECAUSE_C1_IS_CURRENTLY_RIDING_A_BOAT_STEED_OR_STRIDER
        }

        if ( target.isFishing() ) {
            return SystemMessageIds.C1_CANNOT_DUEL_BECAUSE_C1_IS_CURRENTLY_FISHING
        }

        if ( ( !isPartyDuel && ( target.isInArea( AreaType.Peace ) || target.isInArea( AreaType.Water ) ) )
                || target.isInArea( AreaType.PVP )
                || target.isInSiegeArea() ) {
            return SystemMessageIds.C1_CANNOT_MAKE_A_CHALLANGE_TO_A_DUEL_BECAUSE_C1_IS_CURRENTLY_IN_A_DUEL_PROHIBITED_AREA
        }

        if ( target.isDead()
                || ( ( target.getCurrentHp() < ( target.getMaxHp() / 2 ) ) || ( target.getCurrentMp() < ( target.getMaxMp() / 2.0 ) ) ) ) {
            return SystemMessageIds.C1_CANNOT_DUEL_BECAUSE_C1_HP_OR_MP_IS_BELOW_50_PERCENT
        }

        return null
    }
}

export const DuelManager = new Manager()