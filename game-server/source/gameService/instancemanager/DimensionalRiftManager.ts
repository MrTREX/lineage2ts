import { DimensionalRiftRoom } from '../models/DimensionalRiftRoom'
import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { DimensionalRift } from '../models/entity/DimensionalRift'
import { DataManager } from '../../data/manager'
import { L2Npc } from '../models/actor/L2Npc'
import { NpcHtmlMessagePath } from '../packets/send/NpcHtmlMessage'
import { ConfigManager } from '../../config/ConfigManager'
import { L2World } from '../L2World'
import { L2ItemInstance } from '../models/items/instance/L2ItemInstance'
import _ from 'lodash'
import aigle from 'aigle'

const DIMENSIONAL_FRAGMENT_ITEM_ID = 7079

class Manager {
    rooms: { [key: number]: { [key: number]: DimensionalRiftRoom } } = {}

    checkIfInPeaceZone( x: number, y: number, z: number ): boolean {
        return this.rooms[ 0 ][ 0 ].checkIfInZone( x, y, z )
    }

    checkIfInRiftZone( x: number, y: number, z: number, ignorePeaceZone: boolean = false ): boolean {
        if ( ignorePeaceZone ) {
            return this.rooms[ 0 ][ 1 ].checkIfInZone( x, y, z )
        }

        return this.rooms[ 0 ][ 1 ].checkIfInZone( x, y, z ) || !this.rooms[ 0 ][ 0 ].checkIfInZone( x, y, z )
    }

    getFreeRooms( type: number ): Array<number> {
        return []
    }

    getNeededItemCount( type: number ) {
        switch ( type ) {
            case 1:
                return ConfigManager.general.getRecruitCost()
            case 2:
                return ConfigManager.general.getSoldierCost()
            case 3:
                return ConfigManager.general.getOfficerCost()
            case 4:
                return ConfigManager.general.getCaptainCost()
            case 5:
                return ConfigManager.general.getCommanderCost()
            case 6:
                return ConfigManager.general.getHeroCost()
        }

        throw new Error( `DimensionalRiftManager.getNeededItemCount provided with bad type = ${ type }` )
    }

    getRoom( type: number, room: number ): DimensionalRiftRoom {
        return this.rooms[ type ][ room ]
    }

    handleCheat( player: L2PcInstance, npc: L2Npc, message: string = '' ): void {
        this.showHtml( player, 'data/html/seven_signs/rift/Cheater.htm', npc )

        // TODO : log player activity and message where it came from
    }

    showHtml( player: L2PcInstance, path: string, npc: L2Npc ): void {
        let html = DataManager.getHtmlData().getItem( path )
        player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), npc.getObjectId() ) )
    }

    async start( player: L2PcInstance, type: number, npc: L2Npc ): Promise<void> {
        if ( !player.isInParty() ) {
            this.showHtml( player, 'data/html/seven_signs/rift/NoParty.htm', npc )
            return
        }

        if ( player.getParty().getLeaderObjectId() !== player.getObjectId() ) {
            this.showHtml( player, 'data/html/seven_signs/rift/NotPartyLeader.htm', npc )
            return
        }

        if ( player.getParty().isInDimensionalRift() ) {
            this.handleCheat( player, npc )
            return
        }

        if ( player.getParty().getMemberCount() < ConfigManager.general.getRiftMinPartySize() ) {
            let path = 'data/html/seven_signs/rift/SmallParty.htm'
            let html = DataManager.getHtmlData().getItem( path )
                    .replace( /%npc_name%/g, npc.getName() )
                    .replace( /%count%/g, ConfigManager.general.getRiftMinPartySize().toString() )
            player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), npc.getObjectId() ) )
            return
        }

        if ( !this.isAllowedToEnter( type ) ) {
            player.sendMessage( 'Rift is full. Try later.' )
            return
        }

        let manager = this
        let canPass = _.some( player.getParty().getMembers(), ( memberId: number ) => {
            let member: L2PcInstance = L2World.getPlayer( memberId )
            return member && manager.checkIfInPeaceZone( member.getX(), member.getY(), member.getZ() )
        } )

        if ( !canPass ) {
            this.showHtml( player, 'data/html/seven_signs/rift/NotInWaitingRoom.htm', npc )
            return
        }

        let count: number = this.getNeededItemCount( type )
        canPass = _.some( player.getParty().getMembers(), ( memberId: number ) => {
            let member: L2PcInstance = L2World.getPlayer( memberId )
            if ( !member ) {
                return false
            }

            let item: L2ItemInstance = member.getInventory().getItemByItemId( DIMENSIONAL_FRAGMENT_ITEM_ID )
            if ( !item ) {
                return true
            }

            return !( item.getCount() > 0 && item.getCount() >= count )
        } )

        if ( !canPass ) {
            let path = 'data/html/seven_signs/rift/NoFragments.htm'
            let html = DataManager.getHtmlData().getItem( path )
                    .replace( /%npc_name%/g, npc.getName() )
                    .replace( /%count%/g, count.toString() )
            player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), npc.getObjectId() ) )
            return
        }

        let shouldExit: boolean = await aigle.resolve( player.getParty().getMembers() ).someSeries( async ( memberId: number ) => {
            let member: L2PcInstance = L2World.getPlayer( memberId )
            if ( !member ) {
                return false
            }

            let item: L2ItemInstance = member.getInventory().getItemByItemId( DIMENSIONAL_FRAGMENT_ITEM_ID )
            if ( !( await member.destroyItemWithCount( item, count, false, 'RiftEntrance' ) ) ) {
                let path = 'data/html/seven_signs/rift/NoFragments.htm'
                let html = DataManager.getHtmlData().getItem( path )
                        .replace( /%npc_name%/g, npc.getName() )
                        .replace( /%count%/g, count.toString() )
                player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), npc.getObjectId() ) )
                return true
            }

            return false
        } )

        if ( shouldExit ) {
            return
        }

        let roomId: number
        let emptyRooms: Array<number> = this.getFreeRooms( type )

        do {
            roomId = _.sample( emptyRooms )
            _.pull( emptyRooms, roomId )

            if ( emptyRooms.length === 0 ) {
                return
            }
        } while ( this.rooms[ type ][ roomId ].isPartyInside )

        new DimensionalRift( player.getParty(), type, roomId )
    }

    teleportToWaitingRoom( player: L2PcInstance ): Promise<void> {
        return player.teleportToLocation( this.getRoom( 0, 0 ).getTeleportCoordinates(), false )
    }

    async teleportAllToWaitingRoom( players: Array<L2PcInstance> ) : Promise<void> {
        let location = this.getRoom( 0, 0 ).getTeleportCoordinates()
        await Promise.all( players.map( player => player.teleportToLocation( location, false ) ) )
    }

    isAllowedToEnter( type: number ) : boolean {
        let count = 0
        _.each( this.rooms[ type ], ( room: DimensionalRiftRoom ) => {
            if ( room.isPartyInside ) {
                count++
            }
        } )

        return count < ( _.size( this.rooms[ type ] ) - 1 )
    }
}

export const DimensionalRiftManager = new Manager()