import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { L2AirShipInstance } from '../models/actor/instance/L2AirShipInstance'
import { L2DataApi } from '../../data/interface/l2DataApi'
import { AirShipTeleportList } from '../models/airship/AirShipTeleportList'
import { DatabaseManager } from '../../database/manager'
import { ExAirShipTeleportList } from '../packets/send/ExAirShipTeleportList'
import { VehiclePathPoint } from '../models/VehiclePathPoint'
import { L2CharacterTemplate } from '../models/actor/templates/L2CharacterTemplate'
import _ from 'lodash'

const template : L2CharacterTemplate = new L2CharacterTemplate()

template.baseCritRate = 38
template.baseAttackRange = 0
template.baseHpMax = 50000
template.baseHpReg = 0.003

template.baseMpReg = 0.003
template.basePDef = 100
template.baseMDef = 100

class Manager implements L2DataApi {
    teleports: { [ key: number ]: AirShipTeleportList }
    shipInfo: { [ key: number ]: { [ key: string ] : any } }

    sendAirShipTeleportList( player: L2PcInstance ) : void {
        if ( !player || !player.isInAirShip() ) {
            return
        }

        let ship : L2AirShipInstance = player.getAirShip()
        if ( !ship.isCaptain( player ) || !ship.isInDock() || ship.isMoving() ) {
            return
        }

        let dockId = ship.getDockId()
        if ( !this.teleports[ dockId ] ) {
            return
        }

        player.sendOwnedData( ExAirShipTeleportList( this.teleports[ dockId ] ) )
    }

    async load(): Promise<Array<string>> {
        this.teleports = {}
        this.shipInfo = await DatabaseManager.getAirships().getAll()

        return [
           `AirShipManager loaded ${_.size( this.shipInfo )} existing ships.`
        ]
    }

    getTeleportDestination( dockId: number, index: number ) : Array<VehiclePathPoint> {
        let all : AirShipTeleportList = this.teleports[ dockId ]
        if ( !all ) {
            return null
        }

        if ( index < -1 || index >= all.getRoute().length ) {
            return null
        }

        return all.getRoute()[ index + 1 ]
    }

    getFuelConsumption( dockId: number, index: number ) : number {
        let all : AirShipTeleportList = this.teleports[ dockId ]
        if ( !all ) {
            return null
        }

        if ( index < -1 || index >= all.getFuel().length ) {
            return null
        }

        return all.getFuel()[ index + 1 ]
    }

    removeAirShip( ship: L2AirShipInstance ) : Promise<void> {
        if ( ship.getOwnerId() !== 0 ) {
            let data : any = this.shipInfo[ ship.getOwnerId() ]
            if ( data ) {
                data[ 'fuel' ] = ship.getFuel()
            }

            return DatabaseManager.getAirships().updateShip( ship.getOwnerId(), ship.getFuel() )
        }
    }
}

export const AirShipManager = new Manager()