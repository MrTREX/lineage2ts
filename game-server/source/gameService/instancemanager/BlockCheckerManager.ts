import { ArenaParticipantsHolder } from '../models/ArenaParticipantsHolder'
import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { Team } from '../enums/Team'
import { PcInventory } from '../models/itemcontainer/PcInventory'
import { ExCubeGameRemovePlayer } from '../packets/send/ExCubeGameRemovePlayer'
import { ExCubeGameChangeTeam } from '../packets/send/ExCubeGameChangeTeam'
import { ConfigManager } from '../../config/ConfigManager'
import _ from 'lodash'

class Manager {
    arenaPlayers: Array<ArenaParticipantsHolder> = []
    arenaVotes: { [ key: number ]: number } = {}
    arenaStatus: { [ key: number ]: boolean } = {}
    registrationPenalty: Array<number> = []

    changePlayerToTeam( player: L2PcInstance, arena: number ): void {
        let holder: ArenaParticipantsHolder = this.arenaPlayers[ arena ]
        let isFromRed: boolean = holder.getRedPlayers().includes( player.getObjectId() )

        if ( isFromRed && ( holder.getBlueTeamSize() === 6 ) ) {
            player.sendMessage( 'The team is full' )
            return
        }

        if ( !isFromRed && ( holder.getRedTeamSize() === 6 ) ) {
            player.sendMessage( 'The team is full' )
            return
        }

        let futureTeam: number = isFromRed ? 1 : 0
        holder.addPlayer( player, futureTeam )

        if ( isFromRed ) {
            holder.removePlayer( player, 0 )
        } else {
            holder.removePlayer( player, 1 )
        }

        holder.broadCastPacketToTeam( ExCubeGameChangeTeam( player.getObjectId(), isFromRed ) )
    }

    clearArenaVotes( arenaId: number ) {
        this.arenaVotes[ arenaId ] = 0
    }

    getHolder( arena: number ): ArenaParticipantsHolder {
        return this.arenaPlayers[ arena ]
    }

    increaseArenaVotes( arenaId: number ): void {
        let newVotes = this.arenaVotes[ arenaId ] + 1
        let holder: ArenaParticipantsHolder = this.arenaPlayers[ arenaId ]

        if ( ( newVotes > ( holder.getAllPlayerCount() / 2 ) ) && !holder.getEvent().isStarted ) {
            this.clearArenaVotes( arenaId )

            if ( holder.getBlueTeamSize() === 0 || holder.getRedTeamSize() === 0 ) {
                return
            }

            if ( ConfigManager.general.isHBCEFairPlay() ) {
                holder.checkAndShuffle()
            }

            // TODO : start event
            return
        }

        this.arenaVotes[ arenaId ] = newVotes
    }

    isArenaBeingUsed( arenaId: number ): boolean {
        if ( ( arenaId < 0 ) || ( arenaId > 3 ) ) {
            return false
        }

        return this.arenaStatus[ arenaId ]
    }

    async onDisconnect( player: L2PcInstance ): Promise<void> {
        let arena: number = player.getBlockCheckerArena()

        if ( !_.isNumber( arena ) || arena < 0 ) {
            return
        }

        let team: number = this.getHolder( arena ).getPlayerTeam( player )
        this.removePlayer( player, arena, team )

        if ( player.getTeam() !== Team.None ) {
            await player.stopAllEffects()
            player.setTeam( Team.None )

            let inventory: PcInventory = player.getInventory()

            if ( inventory.getItemByItemId( 13787 ) ) {
                let count: number = inventory.getInventoryItemCount( 13787, 0 )
                await inventory.destroyItemByItemId( 13787, count, 0, 'BlockCheckerManager.onDisconnect' )
            }
            if ( inventory.getItemByItemId( 13788 ) ) {
                let count = inventory.getInventoryItemCount( 13788, 0 )
                await inventory.destroyItemByItemId( 13788, count, 0, 'BlockCheckerManager.onDisconnect' )
            }

            await player.teleportToLocationCoordinates( -57478, -60367, -2370, 0, -1, 0 )
        }
    }

    removePenalty( objectId: number ) {
        _.pull( this.registrationPenalty, objectId )
    }

    removePlayer( player: L2PcInstance, arenaId: number, team: number ) {
        let holder: ArenaParticipantsHolder = this.arenaPlayers[ arenaId ]
        let isRed = team === 0
        let objectId = player.getObjectId()

        holder.removePlayer( player, team )
        holder.broadCastPacketToTeam( ExCubeGameRemovePlayer( objectId, isRed ) )

        // End event if theres an empty team
        let teamSize = isRed ? holder.getRedTeamSize() : holder.getBlueTeamSize()
        if ( teamSize === 0 ) {
            holder.getEvent().endEventAbnormally()
        }

        this.registrationPenalty.push( objectId )
        this.schedulePenaltyRemoval( player.getObjectId() )
    }

    schedulePenaltyRemoval( objectId: number ) {
        setTimeout( this.removePenalty.bind( this ), 10000, objectId )
    }
}

export const BlockCheckerManager = new Manager()