import { L2DataApi } from '../../data/interface/l2DataApi'
import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { DatabaseManager } from '../../database/manager'
import _ from 'lodash'
import Timeout = NodeJS.Timeout

/*
    TODO: give 10 points per week to all nobles
    - use DB query to modify data directly
    - then load points for currently logged in players
    - add config to specify cron period and amount of points
 */
class Manager implements L2DataApi {
    olympiadEnd: number
    validationEnd: number
    period: number
    nextWeeklyChange: number
    currentCycle: number
    competitionEnd: number
    competitionStart: number
    inCompetitionPeriod: boolean = false
    competitionStarted: boolean = false

    nobles: { [ objectId: number] : any } = {}
    heroesToBe: Array<any> = []
    noblesRank: { [ objectId: number ] : number } = {}

    weeklyTask : Timeout

    isInCompetitionPeriod() : boolean {
        return this.inCompetitionPeriod
    }

    getCompetitionDone( objectId: number ) : number {
        return 0
    }

    getCompetitionWon( objectId: number ) : number {
        return 0
    }

    getCompetitionLost( objectId: number ) : number {
        return 0
    }

    getNoblePoints( objectId: number ) : number {
        return 0
    }

    getRemainingWeeklyMatches( objectId: number ) : number {
        return 0
    }

    getRemainingWeeklyMatchesClassed( objectId: number ) : number {
        return 0
    }

    getRemainingWeeklyMatchesNonClassed( objectId: number ) : number {
        return 0
    }

    getRemainingWeeklyMatchesTeam( objectId: number ) : number {
        return 0
    }

    async load(): Promise<Array<string>> {

        // TODO : implement loading

        return [
              `Olympiad loaded ${_.size( this.nobles )} existing nobles.`
        ]
    }

    getNoblessePasses( player: L2PcInstance, shouldClearPoints: boolean = false ) : number {
        // TODO : implement me
        return 0
    }

    async getClassLeaderBoard( classId: number ) : Promise<Array<string>> {
        // TODO : implement me
        return []
    }

    getPeriod() : number {
        return this.period
    }

    getCurrentCycle() : number {
        return this.currentCycle
    }

    stopWeeklyTask() : void {
        if ( this.weeklyTask ) {
            clearTimeout( this.weeklyTask )
        }

        this.weeklyTask = null
    }

    async runOlympiadEndTask() : Promise<void> {
        // TODO : make olympiad things proper
    }

    async saveOlympiadStatus() : Promise<void> {
        await this.saveNobleData()
        await DatabaseManager.getOlympiadData().addStatus(
                this.currentCycle,
                this.period,
                this.olympiadEnd,
                this.validationEnd,
                this.nextWeeklyChange,
        )
    }

    async saveNobleData() : Promise<void> {
        // TODO : add saving of noble data
    }
}

export const Olympiad = new Manager()