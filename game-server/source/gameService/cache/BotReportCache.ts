import { L2DataApi } from '../../data/interface/l2DataApi'
import { BotPunishment, RangeBotPunishment } from '../models/punishment/BotPunishment'
import { BotReport, BotReporter } from '../models/punishment/BotReport'
import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { L2Object } from '../models/L2Object'
import { SystemMessageBuilder } from '../packets/send/SystemMessage'
import { SystemMessageIds } from '../packets/SystemMessageIdValues'
import { ConfigManager } from '../../config/ConfigManager'
import { L2GameClientRegistry } from '../L2GameClientRegistry'
import { GameClient } from '../GameClient'
import { L2Clan } from '../models/L2Clan'
import { SkillCache } from './SkillCache'
import { Skill } from '../models/Skill'
import { DatabaseManager } from '../../database/manager'
import { L2BotReportsTableData } from '../../database/interface/BotReportsTableApi'
import _ from 'lodash'
import moment from 'moment'
import { AreaType } from '../models/areas/AreaType'
import { MsInDays } from '../enums/MsFromTime'
import Timeout = NodeJS.Timeout

const defaultPunishments: { [ triggerLimit: number ]: BotPunishment } = {
    25: { skillId: 6038, skillLevel: 1, messageId: 2473 },
    75: { skillId: 6039, skillLevel: 1, messageId: 2474 },
    100: { skillId: 6055, skillLevel: 1, messageId: 2477 },
    125: { skillId: 6056, skillLevel: 1, messageId: 2478 },
    150: { skillId: 6057, skillLevel: 1, messageId: 2480 },
    175: { skillId: 6057, skillLevel: 1, messageId: 2480 },
}

const rangePunishments : Array<RangeBotPunishment> = [
    { skillId: 6040, skillLevel: 1, messageId: null, limit: 150 }
]

class Manager implements L2DataApi {
    ipDateMap: { [ ip: string ]: number } // when IP was reported
    reporters: { [ reporterObjectId: number ]: BotReporter } // who reported
    botReports: { [ botObjectId: number ]: Array<BotReport> }
    punishments: { [ triggerLimit: number ]: BotPunishment }

    resetPointsTask: Timeout

    async load(): Promise<Array<string>> {
        this.ipDateMap = {}
        this.reporters = {}
        this.botReports = {}
        this.punishments = {}

        if ( ConfigManager.general.enableBotReportButton() ) {
            this.punishments = _.cloneDeep( defaultPunishments )

            await this.loadReports()
            this.scheduleResetPointTask()
        }

        return [
            `BotReport : contains ${ _.size( this.punishments ) } punishments.`,
            `BotReport : loaded ${ _.size( this.botReports ) } previous bot reports.`,
            `BotReport : loaded ${ _.size( this.reporters ) } reporter quotas.`,
            `BotReport : loaded ${ _.size( this.ipDateMap ) } IP addresses.`,
        ]
    }

    reportBot( reporter: L2PcInstance ) : boolean {
        let target : L2Object = reporter.getTarget()

        if ( !target || !target.isPlayable() || target.getObjectId() === reporter.getObjectId() ) {
            return false
        }

        let bot : L2PcInstance = target.getActingPlayer()

        if ( !bot ) {
            return false
        }

        if ( bot.isInArea( AreaType.Peace ) || bot.isInArea( AreaType.PVP ) ) {
            reporter.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_REPORT_CHARACTER_IN_PEACE_OR_BATTLE_ZONE ) )
            return false
        }

        if ( bot.isInOlympiadMode() ) {
            reporter.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_NOT_REPORT_CANNOT_REPORT_PEACE_PVP_ZONE_OR_OLYMPIAD_OR_CLAN_WAR_ENEMY ) )
            return false
        }

        if ( bot.getClan() && bot.getClan().isAtWarWith( reporter.getClanId() ) ) {
            reporter.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_REPORT_CLAN_WAR_ENEMY ) )
            return false
        }

        if ( bot.getExp() === bot.getStat().getStartingExp() ) {
            reporter.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_REPORT_CHAR_WHO_ACQUIRED_XP ) )
            return false
        }

        let botReports : Array<BotReport> = this.botReports[ bot.getObjectId() ]
        let reporterId = reporter.getObjectId()
        let reporterData : BotReporter = this.reporters[ reporterId ]

        if ( this.botReports[ reporterId ] ) {
            reporter.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_HAVE_BEEN_REPORTED_AND_CANNOT_REPORT ) )
            return false
        }

        let client : GameClient = L2GameClientRegistry.getClientByPlayerId( reporter.getObjectId() )
        if ( !client ) {
            return false
        }

        let ip : string = client.ipAddress
        if ( !this.canReportIp( ip ) ) {
            reporter.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_REPORT_TARGET_ALREDY_REPORTED_BY_CLAN_ALLY_MEMBER_OR_SAME_IP ) )
            return false
        }

        if ( botReports ) {
            if ( this.isAlreadyReportedBy( botReports, reporterId ) ) {
                reporter.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_REPORT_CHAR_AT_THIS_TIME_1 ) )
                return false
            }

            if ( !ConfigManager.general.allowReportsFromSameClanMembers() && this.reportedBySameClan( botReports, reporter.getClan() ) ) {
                reporter.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_REPORT_TARGET_ALREDY_REPORTED_BY_CLAN_ALLY_MEMBER_OR_SAME_IP ) )
                return false
            }
        }

        if ( reporterData ) {
            if ( reporterData.reportPoints === 0 ) {
                reporter.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_HAVE_USED_ALL_POINTS_POINTS_ARE_RESET_AT_NOON ) )
                return false
            }

            let reuse = Date.now() - reporterData.time
            if ( reuse < ConfigManager.general.getBotReportDelay() ) {
                let message = new SystemMessageBuilder( SystemMessageIds.YOU_CAN_REPORT_IN_S1_MINS_YOU_HAVE_S2_POINTS_LEFT )
                        .addNumber( Math.floor( reuse / 60000 ) )
                        .addNumber( reporterData.reportPoints )
                        .getBuffer()
                reporter.sendOwnedData( message )
                return false
            }
        }

        let time = Date.now()

        if ( !botReports ) {
            let report : BotReport = {
                time,
                reporterId
            }

            botReports = [ report ]
            this.botReports[ bot.getObjectId() ] = botReports
        }

        if ( !reporterData == null ) {
            reporterData = {
                reportPoints: 7,
                time
            }
        }

        reporterData.reportPoints--

        this.ipDateMap[ ip ] = time
        this.reporters[ reporterId ] = reporterData

        let statusMessage = new SystemMessageBuilder( SystemMessageIds.C1_WAS_REPORTED_AS_BOT )
                .addPlayerCharacterName( bot )
                .getBuffer()
        reporter.sendOwnedData( statusMessage )

        let pointsMessage = new SystemMessageBuilder( SystemMessageIds.YOU_HAVE_USED_REPORT_POINT_ON_C1_YOU_HAVE_C2_POINTS_LEFT )
                .addPlayerCharacterName( bot )
                .addNumber( reporterData.reportPoints )
                .getBuffer()
        reporter.sendOwnedData( pointsMessage )

        this.applyPunishment( bot, botReports )

        return true
    }

    canReportIp( ip: string ) : boolean {
        let time = this.ipDateMap[ ip ]
        if ( time > 0 ) {
            return ( Date.now() - time ) > ConfigManager.general.getBotReportDelay()
        }

        return true
    }

    isAlreadyReportedBy( reports: Array<BotReport>, reporterId: number ) : boolean {
        return _.some( reports, ( report: BotReport ) : boolean => {
            return report.reporterId === reporterId
        } )
    }

    reportedBySameClan( reports: Array<BotReport>, clan: L2Clan ) : boolean {
        return _.some( reports, ( report: BotReport ) : boolean => {
            return clan.isMember( report.reporterId )
        } )
    }

    applyPunishment( bot: L2PcInstance, botReports: Array<BotReport> ) {
        let reportSize = _.size( botReports )
        let punishment : BotPunishment = this.punishments[ reportSize ]
        if ( punishment ) {
            this.punishBot( bot, punishment )
        }

        let manager = this
        _.each( rangePunishments, ( currentPunishment: RangeBotPunishment ) => {
            if ( currentPunishment.limit <= reportSize ) {
                manager.punishBot( bot, currentPunishment )
            }
        } )
    }

    punishBot( bot: L2PcInstance, punishment: BotPunishment ) {
        let skill : Skill = SkillCache.getSkill( punishment.skillId, punishment.skillLevel )
        if ( !skill ) {
            return
        }

        skill.applyEffects( bot, bot )

        if ( punishment.messageId ) {
            bot.sendOwnedData( SystemMessageBuilder.fromMessageId( punishment.messageId ) )
        }
    }

    scheduleResetPointTask() : void {
        let resetTime : number = this.getReportsResetTime()

        if ( Date.now() > resetTime ) {
            resetTime = resetTime + MsInDays.One
        }

        this.resetPointsTask = setInterval( this.runResetPointTask.bind( this ), Date.now() - resetTime )
    }

    runResetPointTask() : void {
        _.each( this.reporters, ( report: BotReporter ) => {
            report.reportPoints = 7
        } )
    }

    async loadReports() : Promise<void> {
        let data : Array<L2BotReportsTableData> = await DatabaseManager.getBotReports().getAll()
        let resetTime : number = this.getReportsResetTime()
        let manager = this

        if ( Date.now() < resetTime ) {
            resetTime = resetTime - MsInDays.One
        }

        _.each( data, ( item: L2BotReportsTableData ) => {
            if ( !manager.botReports[ item.botObjectId ] ) {
                manager.botReports[ item.botObjectId ] = []
            }

            let botReport : BotReport = {
                reporterId: item.reporterObjectId,
                time: item.reportTime
            }

            manager.botReports[ item.botObjectId ].push( botReport )

            if ( item.reportTime > resetTime ) {
                let report : BotReporter = manager.reporters[ item.reporterObjectId ]
                if ( report ) {
                    report.reportPoints = report.reportPoints - 1
                    return
                }

                manager.reporters[ item.reporterObjectId ] = {
                    reportPoints: 6,
                    time: 0
                }
            }
        } )
    }

    getReportsResetTime() : number {
        let [ hours, minutes ] = _.split( ConfigManager.general.getBotReportPointsResetHour(), ':' )

        if ( hours && minutes ) {
            return moment().hour( _.parseInt( hours ) ).minute( _.parseInt( minutes ) ).valueOf()
        }

        return Date.now() + MsInDays.One
    }
}

export const BotReportCache = new Manager()