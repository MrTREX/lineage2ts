import { QuestState } from '../models/quest/QuestState'
import { ListenerLogic } from '../models/ListenerLogic'
import { ListenerManager } from '../instancemanager/ListenerManager'
import { L2QuestStatesTableData } from '../../database/interface/QuestStatesTableApi'
import { DatabaseManager } from '../../database/manager'
import { ConfigManager } from '../../config/ConfigManager'
import { QuestStateValues } from '../models/quest/State'
import _, { DebouncedFunc } from 'lodash'

class Manager {
    registry: { [ playerId: number ]: { [ questName: string ]: QuestState } } = {}
    statesToDelete: Set<QuestState> = new Set<QuestState>()
    statesToUpdate: Set<QuestState> = new Set<QuestState>()
    stateVariablesToUpdate: Set<QuestState> = new Set<QuestState>()

    debounceDeletion: DebouncedFunc<() => void>
    debounceUpdate: DebouncedFunc<() => void>
    debounceVariables: DebouncedFunc<() => void>

    constructor() {
        this.debounceDeletion = _.debounce( this.runDeletion.bind( this ), 10000, {
            trailing: true,
            maxWait: 30000,
        } )

        this.debounceUpdate = _.debounce( this.runNormalUpdate.bind( this ), 10000, {
            trailing: true,
            maxWait: 30000,
        } )

        this.debounceVariables = _.debounce( this.runVariablesUpdate.bind( this ), 10000, {
            trailing: true,
            maxWait: 30000,
        } )
    }

    createQuestState( playerId: number, name: string ): QuestState {
        let quest: ListenerLogic = ListenerManager.getListenerByName( name )
        if ( !quest ) {
            return
        }

        let state = new QuestState( playerId, quest, QuestStateValues.CREATED )

        _.set( this.registry, [ playerId, name ], state )

        this.scheduleUpdate( state )

        return state
    }

    deleteQuestState( playerId: number, name: string ) {
        _.unset( this.registry, [ playerId, name ] )
    }

    getAllActiveStates( playerId: number ): Array<QuestState> {
        return _.filter( this.registry[ playerId ], ( state: QuestState ) => {
            return state && state.hasProperQuest()
        } )
    }

    getAllActiveStatesSize( playerId: number ): number {
        return _.reduce( this.registry[ playerId ], ( total: number, state: QuestState ) => {
            if ( state && state.hasProperQuest() ) {
                total++
            }

            return total
        }, 0 )
    }

    getQuestState( playerId: number, name: string, createState: boolean = false ): QuestState {
        let state: QuestState = _.get( this.registry, [ playerId, name ] )
        if ( state || !createState ) {
            return state
        }

        return this.createQuestState( playerId, name )
    }

    hasQuestState( playerId: number, name: string ): boolean {
        return _.has( this.registry, [ playerId, name ] )
    }

    async loadForPlayer( playerId: number ) : Promise<void> {
        let dataItems: Array<L2QuestStatesTableData> = await DatabaseManager.getQuestStatesTable().getData( playerId )
        let manager = this
        let namesToDelete: Array<string> = []

        dataItems.forEach( ( data: L2QuestStatesTableData ) => {
            let quest: ListenerLogic = ListenerManager.getListenerByName( data.questName )
            if ( !quest ) {
                if ( ConfigManager.general.autoDeleteInvalidQuestData() ) {
                    namesToDelete.push( data.questName )
                }

                return
            }

            let state = new QuestState( playerId, quest, data.completionState )

            state.condition = data.condition
            state.stateFlags = data.stateFlags
            state.variables = data.variables

            _.set( manager.registry, [ playerId, data.questName ], state )
        } )

        if ( namesToDelete.length > 0 ) {
            return DatabaseManager.getQuestStatesTable().deleteManyNames( playerId, namesToDelete )
        }
    }

    unLoadForPlayer( playerId: number ): void {
        _.unset( this.registry, playerId )
    }

    scheduleDeletion( playerId: number, name: string ) : void {
        let state = this.getQuestState( playerId, name )
        if ( !state ) {
            return
        }

        this.statesToDelete.add( state )
        this.deleteQuestState( playerId, name )
        this.debounceDeletion()
    }

    scheduleUpdate( state: QuestState ) : void {
        this.statesToUpdate.add( state )
        this.debounceUpdate()
    }

    scheduleVariablesUpdate( state: QuestState ) : void {
        this.stateVariablesToUpdate.add( state )
        this.debounceVariables()
    }

    private runNormalUpdate() : Promise<void> {
        if ( this.statesToUpdate.size === 0 ) {
            return
        }

        let statesToUpdate = this.statesToUpdate
        this.statesToUpdate = new Set<QuestState>()
        return DatabaseManager.getQuestStatesTable().upsertMany( statesToUpdate )
    }

    private runVariablesUpdate() : Promise<void> {
        if ( this.stateVariablesToUpdate.size === 0 ) {
            return
        }

        let stateVariablesToUpdate = this.stateVariablesToUpdate
        this.stateVariablesToUpdate = new Set<QuestState>()
        return DatabaseManager.getQuestStatesTable().updateVariables( stateVariablesToUpdate )
    }

    private runDeletion() : Promise<void> {
        if ( this.statesToDelete.size === 0 ) {
            return
        }

        let statesToDelete = this.statesToDelete
        this.statesToDelete = new Set<QuestState>()
        return DatabaseManager.getQuestStatesTable().deleteMany( statesToDelete )
    }
}

export const QuestStateCache = new Manager()