import { L2DataApi } from '../../data/interface/l2DataApi'
import _ from 'lodash'
import { PetSkills } from '../models/pet/PetSkills'
import { DataManager } from '../../data/manager'
import { L2ServitorSkillItem } from '../../data/interface/ServitorSkillsDataApi'

type ServitorSkills = Record<number, PetSkills>

class Manager implements L2DataApi {
    skills: ServitorSkills

    async load(): Promise<Array<string>> {
        let items = DataManager.getServitorSkills().getAll()
        this.skills = items.reduce( ( allSkills : ServitorSkills, item: L2ServitorSkillItem ) : ServitorSkills => {
            let npcSkills = allSkills[ item.npcId ]
            if ( !npcSkills ) {
                npcSkills = new PetSkills()
                allSkills[ item.npcId ] = npcSkills
            }

            npcSkills.skills.push( {
                id: item.skillId,
                level: item.skillLevel,
                minLevel: 1
            } )

            return allSkills
        }, {} )

        return [
            `ServitorSkills : loaded ${ _.size( this.skills ) } npcs and ${items.length} skills.`,
        ]
    }

    getAvailableLevel( npcId: number, npcLevel: number, skillId: number ): number {
        let skills = this.skills[ npcId ]
        if ( !skills ) {
            return 0
        }

        return skills.getAvailableLevel( skillId, npcLevel )
    }
}

export const ServitorSkillCache = new Manager()