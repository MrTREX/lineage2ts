import { L2Object } from '../models/L2Object'
import { L2World } from '../L2World'
import { L2DataApi } from '../../data/interface/l2DataApi'
import { DataManager } from '../../data/manager'
import { ILocational } from '../models/Location'
import _ from 'lodash'
import perfy from 'perfy'
import { ListenerCache } from './ListenerCache'
import { ListenerRegisterType } from '../enums/ListenerRegisterType'
import { EventType, GeoRegionActivatedEvent } from '../models/events/EventType'
import { RegionActivation } from '../models/RegionActivation'
import { getGeoRegionCode } from '../enums/L2MapTile'
import { L2WorldArea } from '../models/areas/WorldArea'
import { AreaType } from '../models/areas/AreaType'
import { WorldAreaActions } from '../models/areas/WorldAreaActions'
import { L2AreaItem, L2AreaPoint } from '../../data/interface/AreaDataApi'
import { AreaCreator, AreaRegistry } from '../models/areas/AreaRegistry'
import { L2UnknownArea } from '../models/areas/type/Unknown'
import { AreaForm, SquareAreaForm } from '../models/areas/AreaForm'
import { Polyline } from 'math2d'
import { SpawnMakerCache } from './SpawnMakerCache'
import { TerritoryArea } from '../models/areas/type/TerritoryArea'
import { L2SpawnTerritory } from '../models/spawns/L2SpawnTerritory'
import { L2SpawnTerritoryPoint } from '../../data/interface/SpawnTerritoriesDataApi'
import { L2ResidenceWorldArea } from '../models/areas/ResidenceWorldArea'
import { CastleSiegeArea } from '../models/areas/type/CastleSiege'
import { FortSiegeArea } from '../models/areas/type/FortSiege'
import { CastleTeleportArea } from '../models/areas/type/CastleTeleport'
import { ISpatialIndexArea } from '../models/SpatialIndexData'
import { ServerLog } from '../../logger/Logger'

// TODO : adjust area finding based on instance id


class Manager implements L2DataApi {
    allAreas: Record<string, L2WorldArea>
    activatedRegions: RegionActivation = new RegionActivation()
    regionActivatedAreas: Record<number, Array<L2WorldArea>>
    currentAreaId: number = 0
    territoryAreas: Record<string, L2WorldArea>
    idToArea: Record<number, L2WorldArea>
    residenceIdToArea: Record<number, Record<number, L2WorldArea>>

    getArea( object: L2Object, type: AreaType ) : L2WorldArea {
        if ( object ) {
            return this.getAreaWithCoordinates( object.getX(), object.getY(), object.getZ(), type )
        }

        return null
    }

    getAreaWithCoordinates( x: number, y: number, z: number, type: AreaType ) : L2WorldArea {
        return L2World.getAreasWithCoordinates( x, y ).find( ( area: L2WorldArea ) => {
            return area.form.isPointInside( x, y, z ) && area.type === type
        } )
    }

    getFirstAreaForType( object: ILocational, type: AreaType ) : L2WorldArea {
        return L2World.getAreasWithCoordinates( object.getX(), object.getY() ).find( ( area : L2WorldArea ) => {
            return area.type === type && area.isObjectInside( object )
        } )
    }

    async load(): Promise<Array<string>> {
        perfy.start( 'AreaCache' )

        let dataItems = await DataManager.getAreas().getAllAreas()

        this.territoryAreas = {}
        this.residenceIdToArea = {}
        this.idToArea = {}
        this.allAreas = {}
        this.regionActivatedAreas = {}

        let areasWithoutSupportedType = 0

        let allAreas : Array<ISpatialIndexArea> = []

        dataItems.forEach( ( data: L2AreaItem ) : void => {
            let areaConstructor = AreaRegistry[ data.type ]
            if ( !areaConstructor ) {
                areasWithoutSupportedType++
                areaConstructor = L2UnknownArea
            }

            let area = this.createArea( areaConstructor, data )
            if ( area.supportsAction( WorldAreaActions.RegionActivation ) ) {
                this.addToRegionActivation( area )
            }

            this.allAreas[ data.id ] = area
            this.idToArea[ area.id ] = area

            if ( area.addToInitialGrid() ) {
                allAreas.push( area.getSpatialIndex() )
            }
        } )

        L2World.addAreas( allAreas )

        if ( _.size( this.regionActivatedAreas ) > 0 ) {
            ListenerCache.registerListener( ListenerRegisterType.General, EventType.GeoRegionActivated, this, this.onRegionActivation.bind( this ) )
        }

        let metrics = perfy.end( 'AreaCache' )

        return [
            `AreaCache : created ${dataItems.length} unique areas`,
            `AreaCache : placed ${allAreas.length} world areas`,
            `AreaCache : identified ${areasWithoutSupportedType} areas as unknown type`,
            `AreaCache : loaded in ${ metrics.time } seconds`
        ]
    }

    getAreaByName( name: string ) : L2WorldArea {
        return this.allAreas[ name ]
    }

    onRegionActivation( data: GeoRegionActivatedEvent ) : void {
        if ( this.activatedRegions.isActivated( data.code ) ) {
            return
        }

        this.activatedRegions.markActivated( data.code )
        let areas = this.regionActivatedAreas[ data.code ]
        if ( !areas ) {
            return
        }

        let startTime = Date.now()
        areas.forEach( ( zone: L2WorldArea ) => {
            zone.processAction( null, WorldAreaActions.RegionActivation )
        } )

        ServerLog.info( `AreaManager: activated ${ areas.length } areas in ${ Date.now() - startTime } ms` )
    }

    private addToRegionActivation( zone: L2WorldArea ) : void {
        let point = zone.getCenterPointLocation()
        let code = getGeoRegionCode( point.getX(), point.getY() )

        let areas = this.regionActivatedAreas[ code ]
        if ( !areas ) {
            areas = []
            this.regionActivatedAreas[ code ] = areas
        }

        areas.push( zone )
    }

    private createArea( creator: AreaCreator, data: L2AreaItem ) : L2WorldArea {
        let area = new creator( this.createAreaId(), this.createForm( data ), data )

        this.recordArea( area )

        return area
    }

    private recordArea( area: L2WorldArea ) : void {
        switch ( area.type ) {
            case AreaType.Castle:
            case AreaType.Fort:
            case AreaType.ClanHall:
                return this.recordResidenceArea( ( area as L2ResidenceWorldArea ).getResidenceId(), area )

            case AreaType.CastleSiege:
                return this.recordResidenceArea( ( area as CastleSiegeArea ).properties.residenceId, area )

            case AreaType.FortSiege:
                return this.recordResidenceArea( ( area as FortSiegeArea ).properties.residenceId, area )

            case AreaType.CastleTeleport:
                return this.recordResidenceArea( ( area as CastleTeleportArea ).properties.residenceId, area )
        }
    }

    private recordResidenceArea( residenceId: number, area: L2WorldArea ) : void {
        let residenceType = this.residenceIdToArea[ residenceId ]
        if ( !residenceType ) {
            residenceType = {}

            this.residenceIdToArea[ residenceId ] = residenceType
        }

        residenceType[ area.type ] = area
    }

    private createForm( data: L2AreaItem ) : AreaForm {
        let areaPoints = data.points
        let minZ = data.minZ
        let maxZ = data.maxZ

        if ( areaPoints.length === 2 ) {
            return new SquareAreaForm( areaPoints[ 0 ].x, areaPoints[ 0 ].y, areaPoints[ 1 ].x, areaPoints[ 1 ].y, minZ, maxZ )
        }

        let points = areaPoints.reduce( ( allValues: Polyline, point: L2AreaPoint ) : Polyline => {
            allValues.push( point.x )
            allValues.push( point.y )

            return allValues
        }, [] )

        return new AreaForm( points, minZ, maxZ )
    }

    private createAreaId() : number {
        return this.currentAreaId++
    }

    getTerritoryArea( name: string, addToAreaGrid : boolean = false ) : L2WorldArea {
        let area = this.territoryAreas[ name ]
        if ( area ) {
            return area
        }

        this.createTerritoryArea( name, addToAreaGrid )
        return this.territoryAreas[ name ]
    }

    private createTerritoryArea( name: string, addToAreaGrid : boolean = false ) : void {
        let territory = SpawnMakerCache.getTerritoryByName( name )
        if ( !territory ) {
            return
        }

        let form = this.createTerritoryForm( territory )
        let area = new TerritoryArea( this.createAreaId(), form, {
            id: name,
            maxZ: form.maximumZ,
            minZ: form.minimumZ,
            points: undefined,
            properties: undefined,
            type: 'dynamicTerritory'
        } )

        this.territoryAreas[ name ] = area
        this.idToArea[ area.id ] = area

        if ( addToAreaGrid ) {
            L2World.addArea( area )
        }
    }

    private createTerritoryForm( territory : L2SpawnTerritory ) : AreaForm {
        let areaPoints = territory.data.points
        let minZ = territory.data.minZ
        let maxZ = territory.data.maxZ

        let points = areaPoints.reduce( ( allValues: Polyline, point: L2SpawnTerritoryPoint ) : Polyline => {
            allValues.push( point[ 0 ] )
            allValues.push( point[ 1 ] )

            return allValues
        }, [] )

        return new AreaForm( points, minZ, maxZ )
    }

    getAreaById( id: number ) : L2WorldArea {
        return this.idToArea[ id ]
    }

    getAreaByResidenceId( id: number, type: AreaType ) : L2WorldArea {
        let residenceAreas = this.residenceIdToArea[ id ]
        if ( !residenceAreas ) {
            return
        }

        return residenceAreas[ type ]
    }
}

export const AreaCache = new Manager()