import { ReadonlyRoaringBitmap32, RoaringBitmap32 } from 'roaring'
import { L2Character } from '../models/actor/L2Character'
import { L2World } from '../L2World'
import { ISpatialIndexArea } from '../models/SpatialIndexData'
import { WorldAreaActions } from '../models/areas/WorldAreaActions'
import { AreaCache } from './AreaCache'
import { AreaType } from '../models/areas/AreaType'
import { L2WorldArea } from '../models/areas/WorldArea'

interface CharacterAreas {
    entered: RoaringBitmap32
    types: RoaringBitmap32
}

type AreaIds = Record<number, CharacterAreas>
const enteredBitmap = new RoaringBitmap32()
const enteredTypes = new RoaringBitmap32()
const emptyEnteredAreas : ReadonlyRoaringBitmap32 = new RoaringBitmap32()

class Manager {
    discoveredAreas: AreaIds = {}

    findAreas( character: L2Character ) : void {
        const areas = this.discoveredAreas[ character.getObjectId() ]
        if ( !areas ) {
            return
        }

        enteredTypes.clear()
        enteredBitmap.clear()

        L2World.forAreasByBox( character.getSpatialIndex(), ( data : ISpatialIndexArea ) => {
            let currentArea = data.area
            if ( !currentArea.isObjectInside( character )
                || !currentArea.canAffectObject( character ) ) {
                return
            }

            if ( !areas.entered.has( data.area.id ) && currentArea.supportsAction( WorldAreaActions.Enter ) ) {
                currentArea.processAction( character, WorldAreaActions.Enter )
            }

            enteredBitmap.add( currentArea.id )
            enteredTypes.add( currentArea.type as number )
        } )

        if ( enteredBitmap.size !== 0 ) {
            /*
                Since we want to avoid creation of new bitmaps, we can remove discovered area ids directly,
                and later load only discovered ids.
             */

            areas.entered.andNotInPlace( enteredBitmap )
        }

        /*
            Since we did removal in place, existing areas are now inverted leaving only area ids that we should exit.
         */
        for ( const areaId of areas.entered ) {
            let currentArea = AreaCache.getAreaById( areaId )
            if ( currentArea.supportsAction( WorldAreaActions.Exit ) ) {
                currentArea.processAction( character, WorldAreaActions.Exit )
            }
        }

        /*
            At this point both bitmaps are equal
         */
        areas.entered.copyFrom( enteredBitmap )
        areas.types.copyFrom( enteredTypes )
    }

    /*
        When adding character we can choose if new areas need to be entered or just data be initialized.
        It is useful to have character immediately enter areas (when applicable) so that any events or
        effects applied to character be immediately applicable, such as players or their summons. However,
        due to nature of area activation/deactivation same situation will not be applicable to npcs.
     */
    addCharacter( character: L2Character, applyAreas: boolean ) {
        this.discoveredAreas[ character.getObjectId() ] = {
            entered: new RoaringBitmap32(),
            types: new RoaringBitmap32(),
        }

        if ( applyAreas ) {
            this.findAreas( character )
        }
    }

    removeCharacter( character: L2Character ) {
        delete this.discoveredAreas[ character.getObjectId() ]
    }

    hasAreaType( objectId: number, type: AreaType ) : boolean {
        let data = this.discoveredAreas[ objectId ]
        if ( !data ) {
            return false
        }

        return data.types.has( type )
    }

    forSomeDiscoveredArea( objectId: number, predicate: ( area: L2WorldArea ) => boolean ) : boolean {
        let areas = this.discoveredAreas[ objectId ]
        if ( !areas ) {
            return false
        }

        for ( const areaId of areas.entered ) {
            let currentArea = AreaCache.getAreaById( areaId )
            if ( currentArea && predicate( currentArea ) ) {
                return true
            }
        }

        return false
    }

    reset( objectId: number ) : void {
        let areas = this.discoveredAreas[ objectId ]
        if ( !areas ) {
            return
        }

        areas.entered.clear()
        areas.types.clear()
    }

    exitAllAreas( character: L2Character ) : void {
        let areas = this.discoveredAreas[ character.getObjectId() ]
        if ( !areas ) {
            return
        }

        for ( const areaId of areas.entered ) {
            let currentArea = AreaCache.getAreaById( areaId )
            if ( currentArea.supportsAction( WorldAreaActions.Exit ) ) {
                currentArea.processAction( character, WorldAreaActions.Exit )
            }
        }

        areas.entered.clear()
        areas.types.clear()
    }

    getEnteredAreaByType( objectId: number, type: AreaType ) : L2WorldArea {
        let areas = this.discoveredAreas[ objectId ]
        if ( !areas ) {
            return
        }

        for ( const areaId of areas.entered ) {
            let currentArea = AreaCache.getAreaById( areaId )
            if ( currentArea.type === type ) {
                return currentArea
            }
        }
    }

    getEnteredAreaIds( objectId: number ) : ReadonlyRoaringBitmap32 {
        return this.discoveredAreas[ objectId ]?.entered ?? emptyEnteredAreas
    }
}

export const AreaDiscoveryManager = new Manager()