import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { L2Party } from '../models/L2Party'
import { PartyDistributionType } from '../enums/PartyDistributionType'
import { L2PartyMessageType } from '../enums/L2PartyMessageType'
import { ObjectPool } from '../helpers/ObjectPoolHelper'

const objectPool = new ObjectPool<L2Party>( 'PlayerParty', () : L2Party => {
    return new L2Party()
} )

class Manager {
    playerParties: Record<number,L2Party> = {}

    createNewParty( player: L2PcInstance, distributionType: PartyDistributionType ) : L2Party {
        let party : L2Party = objectPool.getValue()

        party.initialize( player, distributionType )
        this.addPartyRecord( player.getObjectId(), party )

        return party
    }

    addPartyRecord( objectId: number, party : L2Party ) : void {
        this.playerParties[ objectId ] = party
    }

    joinParty( leader : L2PcInstance, member : L2PcInstance ) : void {
        let party = this.getParty( leader.getObjectId() )

        if ( !party ) {
            return
        }

        party.addPartyMember( member )
        this.addPartyRecord( member.getObjectId(), party )
    }

    getParty( objectId: number ) : L2Party {
        return this.playerParties[ objectId ]
    }

    leaveParty( member : L2PcInstance ) : void {
        let party = this.getParty( member.getObjectId() )

        if ( !party ) {
            return
        }

        party.removePartyMember( member, L2PartyMessageType.Disconnected )
        this.removePartyRecord( member.getObjectId() )
    }

    removePartyRecord( objectId : number ) : void {
        delete this.playerParties[ objectId ]
    }

    recycleParty( party: L2Party ) : void {
        objectPool.recycleValue( party )
    }
}

export const PlayerGroupCache = new Manager()