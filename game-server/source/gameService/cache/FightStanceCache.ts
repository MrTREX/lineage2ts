import _, { DebouncedFunc } from 'lodash'
import { L2World } from '../L2World'
import { AutoAttackStop } from '../packets/send/AutoAttackStop'
import { L2Character } from '../models/actor/L2Character'
import { AutoAttackStart } from '../packets/send/AutoAttackStart'
import { BroadcastHelper } from '../helpers/BroadcastHelper'
import { L2Summon } from '../models/actor/L2Summon'
import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { RoaringBitmap32 } from 'roaring'
import { L2Region } from '../enums/L2Region'
import { L2Object } from '../models/L2Object'
import { L2Playable } from '../models/actor/L2Playable'
import { ConfigManager } from '../../config/ConfigManager'

const enum FightStanceValues {
    DefaultDuration = 15000,
}

class FightStance {
    stances: { [ objectId: number ]: DebouncedFunc<any> } = {}
    permanentStances: RoaringBitmap32 = new RoaringBitmap32()
    hasPlayableStance( objectId: number ): boolean {
        return !!this.stances[ objectId ]
    }

    // TODO : consider using timestamp with interval
    addPlayableStance( playable: L2Playable ): void {
        let objectId = playable.getObjectId()

        if ( !this.stances[ objectId ] ) {
            this.stances[ objectId ] = _.debounce( this.removePlayableStance.bind( this, objectId ), FightStanceValues.DefaultDuration )
            this.broadcastAttackAction( playable, AutoAttackStart )
        }

        this.stances[ objectId ]()

        if ( playable.isPlayer() ) {
            ( playable as L2PcInstance ).startPcPointsInterval()
        }
    }

    clearStances( objectId: number ) : void {
        this.removePlayableStance( objectId )
        this.permanentStances.remove( objectId )
    }

    addPermanentStance( object: L2Object ) : void {
        this.permanentStances.add( object.getObjectId() )
        BroadcastHelper.dataInLocation( object, AutoAttackStart( object.getObjectId() ), L2Region.Length )
    }

    removePermanentStance( object: L2Object ) : void {
        if ( this.permanentStances.remove( object.getObjectId() ) ) {
            BroadcastHelper.dataInLocation( object, AutoAttackStop( object.getObjectId() ), L2Region.Length )
        }
    }

    hasPermanentStance( objectId: number ) : boolean {
        return this.permanentStances.has( objectId )
    }

    private removePlayableStance( objectId: number ): void {
        let debouncedMethod = this.stances[ objectId ]
        if ( debouncedMethod ) {
            debouncedMethod.cancel()
            delete this.stances[ objectId ]

            let character = L2World.getObjectById( objectId ) as L2Playable
            if ( !character || !character.isPlayable() ) {
                return
            }

            this.broadcastAttackAction( character, AutoAttackStop )

            if ( character.isPlayer() ) {
                ( character as L2PcInstance ).stopPcPointsInterval()
            }
        }
    }

    broadcastAttackAction( character: L2Character, action: ( id : number ) => Buffer ): void {
        BroadcastHelper.dataToSelfBasedOnVisibility( character, action( character.getObjectId() ) )

        if ( character.isPlayer() ) {
            let summon: L2Summon = character.getSummon()
            if ( !summon || summon.isDead() ) {
                return
            }

            return BroadcastHelper.dataBasedOnVisibility( summon, action( summon.getObjectId() ) )
        }

        if ( character.isSummon() ) {
            let player: L2PcInstance = character.getActingPlayer()
            if ( !player || player.isDead() ) {
                return
            }

            return BroadcastHelper.dataBasedOnVisibility( player, action( player.getObjectId() ) )
        }
    }
}

export const FightStanceCache = new FightStance()