import { L2Henna } from '../models/items/L2Henna'
import { L2CharacterHennaItem } from '../../database/interface/CharacterHennasTableApi'
import _, { DebouncedFunc } from 'lodash'
import { DatabaseManager } from '../../database/manager'
import { DataManager } from '../../data/manager'
import { ObjectPool } from '../helpers/ObjectPoolHelper'
import { ServerLog } from '../../logger/Logger'

const enum ModificationType {
    Add,
    Remove
}

interface HennaModification extends L2CharacterHennaItem {
    type: ModificationType
}

type PlayerHennaModifications = Map<number, HennaModification> // symbolId to henna data
const objectPool = new ObjectPool<HennaModification>( 'PlayerHennaCache', () : HennaModification => {
    return {
        type: undefined,
        classIndex: 0,
        objectId: 0,
        slot: 0,
        symbolId: 0
    }
} )

class Manager {
    hennas: Record<number, Array<L2Henna>> = {}
    modifications: Map<number, PlayerHennaModifications> = new Map<number, PlayerHennaModifications>()

    debouncedDatabaseWrite: DebouncedFunc<() => void>

    constructor() {
        this.debouncedDatabaseWrite = _.debounce( this.runDatabaseWrite.bind( this ), 10000, {
            maxWait: 20000
        } )
    }

    async loadPlayer( objectId: number, classIndex: number ) : Promise<void> {
        if ( this.modifications.has( objectId ) ) {
            await this.storePlayerModifications( objectId )
        }

        let hennaItems = await DatabaseManager.getCharacterHennas().getAll( objectId, classIndex )
        this.hennas[ objectId ] = hennaItems.map( item => DataManager.getHennas().get( item.symbolId ) )
    }

    async unLoadPlayer( objectId: number ) : Promise<void> {
        delete this.hennas[ objectId ]

        if ( this.modifications.has( objectId ) ) {
            await this.storePlayerModifications( objectId )
            this.modifications.delete( objectId )
        }
    }

    getHennas( objectId: number ) : ReadonlyArray<L2Henna> {
        return this.hennas[ objectId ]
    }

    async runDatabaseWrite() : Promise<void> {
        let toUpdate = []
        let toDelete = []

        for ( const objectId of this.modifications.keys() ) {
            this.extractPlayerModifications( objectId, toUpdate, toDelete )
        }

        return this.writeData( toUpdate, toDelete )
    }

    private async storePlayerModifications( objectId: number ) : Promise<void> {
        let toUpdate = []
        let toDelete = []

        this.extractPlayerModifications( objectId, toUpdate, toDelete )
        return this.writeData( toUpdate, toDelete )
    }

    private async writeData( toUpdate: Array<HennaModification>, toDelete: Array<HennaModification> ) : Promise<void> {
        if ( toDelete.length > 0 ) {
            await DatabaseManager.getCharacterHennas().delete( toDelete )
            objectPool.recycleValues( toDelete )
        }

        if ( toUpdate.length > 0 ) {
            await DatabaseManager.getCharacterHennas().add( toUpdate )
            objectPool.recycleValues( toUpdate )
        }

        ServerLog.info( `PlayerHennaCache: removed ${toDelete.length} and updated ${toUpdate.length} items` )
    }

    private extractPlayerModifications( objectId: number, toUpdate: Array<HennaModification>, toDelete: Array<HennaModification> ) : void {
        let updates = this.modifications.get( objectId )

        for ( const updateItem of updates.values() ) {
            switch ( updateItem.type ) {
                case ModificationType.Remove:
                    toDelete.push( updateItem )
                    break

                case ModificationType.Add:
                    toUpdate.push( updateItem )
                    break
            }
        }

        updates.clear()
    }

    private getHennaForUpdate( objectId: number, slot: number ) : HennaModification {
        let updates = this.modifications.get( objectId )
        if ( !updates ) {
            updates = new Map<number, HennaModification>()
            this.modifications.set( objectId, updates )
        }

        let item = updates.get( slot )
        if ( !item ) {
            item = objectPool.getValue()
            updates.set( slot, item )
        }

        return item
    }

    removeHenna( objectId: number, classIndex: number, slot: number ) : L2Henna {
        let playerHennas = this.hennas[ objectId ]
        let henna = playerHennas[ slot ]
        if ( !henna ) {
            return
        }

        playerHennas[ slot ] = null

        let update = this.getHennaForUpdate( objectId, slot )

        update.objectId = objectId
        update.classIndex = classIndex
        update.slot = slot
        update.symbolId = henna.getDyeId()
        update.type = ModificationType.Remove

        this.debouncedDatabaseWrite()

        return henna
    }

    addHenna( objectId: number, classIndex: number, slot: number, symbolId: number ) : void {
        let playerHennas = this.hennas[ objectId ]
        playerHennas[ slot ] = DataManager.getHennas().get( symbolId )

        let update = this.getHennaForUpdate( objectId, slot )

        update.objectId = objectId
        update.classIndex = classIndex
        update.slot = slot
        update.symbolId = symbolId
        update.type = ModificationType.Add

        this.debouncedDatabaseWrite()
    }

    getFreeSlot( objectId: number ) : number {
        let hennas = PlayerHennaCache.getHennas( objectId )
        return hennas.findIndex( item => !item )
    }

    getConsumedSlots( objectId: number ) : number {
        let hennas = PlayerHennaCache.getHennas( objectId )
        let amount = 0

        for ( const henna of hennas ) {
            if ( henna ) {
                amount++
            }
        }

        return amount
    }
}

export const PlayerHennaCache = new Manager()