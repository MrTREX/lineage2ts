import { L2DataApi } from '../../data/interface/l2DataApi'
import { DatabaseManager } from '../../database/manager'
import { DeletionManager } from './DeletionManager'
import aigle from 'aigle'
import schedule, { Job } from 'node-schedule'
import { ServerLog } from '../../logger/Logger'

const cronExpression: string = '0 * * * *' // every hour

class Manager implements L2DataApi {
    deletionTask: Job
    batchAmount: number = 5

    async load(): Promise<Array<string>> {
        let amount = await DatabaseManager.getCharacterTable().getDeleteCharacterCount()
        this.deletionTask = schedule.scheduleJob( cronExpression, this.runDeletionTask.bind( this ) )

        return [
            `DeleteAccountManager : detected ${ amount } characters to delete`,
        ]
    }

    async runDeletionTask(): Promise<void> {
        let objectIds: Array<number> = await DatabaseManager.getCharacterTable().getDeleteCharacterIds( this.batchAmount )

        if ( objectIds.length === 0 ) {
            return
        }

        ServerLog.info( 'DeleteAccountManager : removing %s expired characters.', objectIds.length )

        await aigle.resolve( objectIds ).eachLimit( 10, DeletionManager.removeCharacter )
    }
}

export const DeletedAccountsProcessor = new Manager()