import { L2NevitDataItem } from '../../database/interface/NevitDataTableApi'
import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { DatabaseManager } from '../../database/manager'
import { SystemMessageBuilder } from '../packets/send/SystemMessage'
import { SystemMessageIds } from '../packets/SystemMessageIdValues'
import { ExNevitAdventPointInfoPacket } from '../packets/send/ExNevitAdventPointInfoPacket'
import { ExNevitAdventTimeChange } from '../packets/send/ExNevitAdventTimeChange'
import { ConfigManager } from '../../config/ConfigManager'
import { PacketDispatcher } from '../PacketDispatcher'
import { ExNevitAdventEffect } from '../packets/send/ExNevitAdventEffect'
import { AbnormalVisualEffect } from '../models/skills/AbnormalVisualEffect'
import { L2World } from '../L2World'
import Timeout = NodeJS.Timeout
import { ExVoteSystemInfo } from '../packets/send/ExVoteSystemInfo'
import moment from 'moment'

export enum NevitHourglassStatus {
    Paused,
    Active,
    Maintain
}

interface NevitSettings extends L2NevitDataItem {
    adventTask: Timeout
    effectTask: Timeout
    hourglassTime: number
    hourglassPaused: boolean
}

/*
    - Quit status with hourglass need show Paused in UI
    - If Hourglass end with Quit status, UI need show Quit 0%
    - If have Quit status, and not have hourglass, in peace zone also UI show Quit 0%
    - If you have amount of recs (1~255), and have Quit status, and not have hourglass, also UI show Quit 0% not the percent rec bonus
    - All other cases, only show Pause status and percent bonus

    TODO : add debounced way of saving points
 */
class Manager {

    playerData: { [ objectId: number ]: NevitSettings } = {}

    async addHuntingPoints( objectId: number, amount: number ): Promise<void> {
        let data = this.playerData[ objectId ]

        if ( !data ) {
            let data = await DatabaseManager.getNevitData().getData( objectId )

            if ( !data ) {
                data = this.getDefaultPlayerData( objectId )
            }

            this.playerData[ data.objectId ] = data as NevitSettings
        }

        if ( this.getRemainingEffectTime( data ) > 0 ) {
            data.huntingPoints = 0
            return
        }

        data.huntingPoints += amount

        if ( data.huntingPoints > ConfigManager.character.getNevitHuntingPointLimit() ) {
            data.huntingPoints = 0
            let effectTimeSeconds = ConfigManager.character.getNevitEffectTime() * 1000
            await this.startNevitEffect( objectId, effectTimeSeconds )
        } else {
            this.notifyPlayer( data )
        }

        return DatabaseManager.getNevitData().setData( data )
    }

    calculateBlessingPercentage( data: NevitSettings ): number {
        return ( 100 / ConfigManager.character.getNevitHuntingPointLimit() ) * data.huntingPoints
    }

    calculateHuntingTime( player: L2PcInstance, currentValue: number ): number {
        let currentDate: number = moment().hour( 6 ).minute( 30 ).second( 0 ).valueOf()

        if ( player.getLastAccess() < currentDate && Date.now() > currentDate ) {
            return 0
        }

        return currentValue
    }

    getRemainingEffectTime( data: NevitSettings ): number {
        return Math.min( 0, data.effectEndTime - Date.now() )
    }

    notifyPlayer( data: NevitSettings ): void {
        PacketDispatcher.sendOwnedData( data.objectId, ExNevitAdventPointInfoPacket( data.huntingPoints ) )

        let percent = this.calculateBlessingPercentage( data )
        if ( percent >= 45 && percent < 50 ) {
            return PacketDispatcher.sendOwnedData( data.objectId, SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ARE_STARTING_TO_FEEL_THE_EFFECTS_OF_NEVIT_S_ADVENT_BLESSING ) )
        }

        if ( ( percent >= 50 ) && ( percent < 75 ) ) {
            return PacketDispatcher.sendOwnedData( data.objectId, SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ARE_FURTHER_INFUSED_WITH_THE_BLESSINGS_OF_NEVIT_CONTINUE_TO_BATTLE_EVIL_WHEREVER_IT_MAY_LURK ) )
        }

        if ( percent >= 75 ) {
            return PacketDispatcher.sendOwnedData( data.objectId, SystemMessageBuilder.fromMessageId( SystemMessageIds.NEVIT_S_ADVENT_BLESSING_SHINES_STRONGLY_FROM_ABOVE_YOU_CAN_ALMOST_SEE_HIS_DIVINE_AURA ) )
        }
    }

    async onPlayerLogin( player: L2PcInstance ): Promise<void> {
        if ( !ConfigManager.character.isNevitEnabled() ) {
            return
        }

        let data = await DatabaseManager.getNevitData().getData( player.getObjectId() )

        if ( !data ) {
            data = this.getDefaultPlayerData( player.getObjectId() )
        }

        data.huntingTime = this.calculateHuntingTime( player, data.huntingTime )

        player.sendOwnedData( ExNevitAdventTimeChange( data.effectEndTime ) )

        this.playerData[ data.objectId ] = data as NevitSettings

        await this.startNevitEffect( data.objectId )
        this.notifyPlayer( data as NevitSettings )
    }

    async onPlayerLogout( player: L2PcInstance ): Promise<void> {
        await this.stopNevitEffectTask( player.getObjectId() )
        this.stopAdventTask( player.getObjectId(), false )

        let data = this.playerData[ player.getObjectId() ]
        if ( !data ) {
            return
        }

        delete this.playerData[ player.getObjectId() ]
        return DatabaseManager.getNevitData().setData( data )
    }

    async runNevitEffectEnd( objectId: number ): Promise<void> {
        let data = this.playerData[ objectId ]

        if ( ConfigManager.character.getNevitIgnoreAdventTime() ) {
            data.huntingTime = 0
        }

        data.effectEndTime = 0

        PacketDispatcher.sendOwnedData( objectId, ExNevitAdventEffect( 0 ) )
        PacketDispatcher.sendOwnedData( objectId, ExNevitAdventPointInfoPacket( data.huntingPoints ) )
        PacketDispatcher.sendOwnedData( objectId, SystemMessageBuilder.fromMessageId( SystemMessageIds.NEVIT_S_ADVENT_BLESSING_HAS_ENDED_CONTINUE_YOUR_JOURNEY_AND_YOU_WILL_SURELY_MEET_HIS_FAVOR_AGAIN_SOMETIME_SOON ) )

        let player = L2World.getPlayer( objectId )
        player.removeVisualEffect( AbnormalVisualEffect.NAVIT_ADVENT )

        data.effectTask = null
    }

    async startNevitEffect( objectId: number, effectTimeSeconds: number = 0 ): Promise<void> {
        let data = this.playerData[ objectId ]

        if ( !data ) {
            return
        }

        let remainingSeconds = Math.floor( this.getRemainingEffectTime( data ) / 1000 )
        if ( remainingSeconds > 0 ) {
            await this.stopNevitEffectTask( objectId )
            effectTimeSeconds += remainingSeconds
        }

        if ( ( ConfigManager.character.getNevitIgnoreAdventTime() || data.huntingTime < ConfigManager.character.getNevitAdventTime() ) && effectTimeSeconds > 0 ) {
            PacketDispatcher.sendOwnedData( objectId, ExNevitAdventEffect( effectTimeSeconds ) )
            PacketDispatcher.sendOwnedData( objectId, SystemMessageBuilder.fromMessageId( SystemMessageIds.THE_ANGEL_NEVIT_HAS_BLESSED_YOU_FROM_ABOVE_YOU_ARE_IMBUED_WITH_FULL_VITALITY_AS_WELL_AS_A_VITALITY_REPLENISHING_EFFECT_AND_SHOULD_YOU_DIE_YOU_WILL_NOT_LOSE_EXP ) )

            let player = L2World.getPlayer( objectId )
            player && player.addVisualEffect( AbnormalVisualEffect.NAVIT_ADVENT )

            data.effectEndTime = Date.now() + effectTimeSeconds * 1000
            data.effectTask = setTimeout( this.runNevitEffectEnd.bind( this ), effectTimeSeconds * 1000, objectId )
        }
    }

    stopAdventTask( objectId: number, sendMessage: boolean ): void {
        let data = this.playerData[ objectId ]

        if ( !data ) {
            return
        }

        if ( data.adventTask ) {
            clearTimeout( data.adventTask )
            data.adventTask = null
        }

        if ( sendMessage ) {
            PacketDispatcher.sendOwnedData( objectId, ExNevitAdventTimeChange( data.huntingTime ) )
        }
    }

    async stopNevitEffectTask( objectId: number ): Promise<void> {
        let data = this.playerData[ objectId ]

        if ( !data ) {
            return
        }

        if ( data.effectTask ) {
            clearTimeout( data.effectTask )
            data.effectTask = null
        }
    }

    startAdventTask( objectId: number ) : void {
        let data = this.playerData[ objectId ]

        if ( !data || data.adventTask ) {
            return
        }

        if ( data.huntingTime < ConfigManager.character.getNevitAdventTime() ) {
            data.adventTask = setTimeout( this.runAdventTask.bind( this ), 30000, objectId )
            PacketDispatcher.sendOwnedData( objectId, ExNevitAdventTimeChange( data.huntingTime ) )
        }
    }

    async runAdventTask( objectId: number ) : Promise<void> {
        let data = this.playerData[ objectId ]

        data.huntingTime += 30
        data.adventTask = null

        if ( data.huntingTime >= ConfigManager.character.getNevitAdventTime() ) {
            data.huntingTime = ConfigManager.character.getNevitAdventTime()
            return PacketDispatcher.sendOwnedData( objectId, ExNevitAdventTimeChange( data.huntingTime ) )
        }

        await this.addHuntingPoints( objectId, 72 )
        if ( data.huntingTime % 60 === 0 ) {
            PacketDispatcher.sendOwnedData( objectId, ExNevitAdventTimeChange( data.huntingTime ) )
        }
    }

    isBlessingActive( objectId: number ) : boolean {
        return this.isBlessingActiveWithData( this.playerData[ objectId ] )
    }

    private isBlessingActiveWithData( data : NevitSettings ) : boolean {
        return data && data.effectTask && data.effectEndTime > Date.now()
    }

    onEnterPeaceZone( objectId: number ) : void {
        this.stopAdventTask( objectId, true )
        this.setPauseHourglassStatus( objectId, true )
    }

    isHourglassActive( objectId: number ) : boolean {
        let data = this.playerData[ objectId ]

        return data && data.hourglassTime > 0 && data.hourglassPaused
    }

    getHourglassStatus( objectId: number ) : NevitHourglassStatus {
        let data = this.playerData[ objectId ]

        if ( !data || data.hourglassPaused ) {
            return NevitHourglassStatus.Paused
        }

        if ( data.hourglassTime > 0 ) {
            return NevitHourglassStatus.Active
        }

        return NevitHourglassStatus.Maintain
    }

    setPauseHourglassStatus( objectId: number, status: boolean ) : void {
        let data = this.playerData[ objectId ]
        if ( !data ) {
            return
        }

        data.hourglassPaused = status

        PacketDispatcher.sendOwnedData( objectId, ExVoteSystemInfo( objectId ) )
    }

    playerLevelup( objectId: number ) : Promise<void> {
        if ( !ConfigManager.character.isNevitEnabled() ) {
            return
        }

        let data = this.playerData[ objectId ]
        if ( !data ) {
            return
        }

        let points = this.isBlessingActiveWithData( data ) ? ConfigManager.character.getNevitBlessedLevelupPoints() : ConfigManager.character.getNevitLevelupPoints()
        return this.addHuntingPoints( objectId, points )
    }

    getDefaultPlayerData( objectId: number ) : NevitSettings {
        return {
            adventTask: undefined,
            effectTask: undefined,
            hourglassPaused: false,
            hourglassTime: 0,
            effectEndTime: 0,
            huntingPoints: 0,
            huntingTime: 0,
            objectId
        }
    }
}

export const NevitManager = new Manager()