import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { ConfigManager } from '../../config/ConfigManager'
import { SystemMessageBuilder } from '../packets/send/SystemMessage'
import { SystemMessageIds } from '../packets/SystemMessageIdValues'
import { IParseBoardHandler } from '../handler/IParseBoardHandler'
import { IWriteBoardHandler } from '../handler/IWriteBoardHandler'
import _ from 'lodash'

const urlToCommandMap = {
    'Topic': '_bbstop',
    'Post': '_bbspos',
    'Region': '_bbsloc',
    'Notice': '_bbsclan',
}

class Manager {

    handlers: { [ key: string ] : IParseBoardHandler } = {}

    onWriteCommand( player: L2PcInstance, values: Array<string> ) : void {
        if ( !player ) {
            return
        }

        if ( !ConfigManager.general.enableCommunityBoard() ) {
            return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CB_OFFLINE ) )
        }

        let command = urlToCommandMap[ _.head( values ) ]

        if ( !command ) {
            return
        }

        let handler : IParseBoardHandler = this.getHandler( command )
        if ( handler && handler.isWriteHandler() ) {
            ( handler as IWriteBoardHandler ).writeCommand( player, values )
        }
    }

    onParseCommand( command: string, player: L2PcInstance ) : void {
        if ( !player ) {
            return
        }

        if ( !ConfigManager.general.enableCommunityBoard() ) {
            return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CB_OFFLINE ) )
        }

        let handler : IParseBoardHandler = this.getHandler( command )
        if ( handler ) {
            handler.parseCommand( command, player )
        }
    }

    registerHandler( handler: IParseBoardHandler ) {
        let manager = this
        _.each( handler.getCommands(), ( command: string ) => {
            manager.handlers[ _.toLower( command ) ] = handler
        } )
    }

    removeHandler( handler: IParseBoardHandler ) {
        let manager = this
        _.each( handler.getCommands(), ( command: string ) => {
            _.unset( manager.handlers, _.toLower( command ) )
        } )
    }

    getHandler( command: string ) : IParseBoardHandler {
        return this.handlers[ _.toLower( command ) ]
    }

    isCommunityBoardCommand( command: string ) : boolean {
        return !!this.getHandler( command )
    }
}

export const CommunityBoardManager = new Manager()