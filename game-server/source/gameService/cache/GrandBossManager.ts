import { L2World } from '../L2World'
import { DatabaseManager } from '../../database/manager'
import { L2GrandBossInstance } from '../models/actor/instance/L2GrandBossInstance'
import { L2GrandbossDataItem } from '../../database/interface/GrandbossDataTableApi'
import { L2DataApi } from '../../data/interface/l2DataApi'
import { L2GrandbossPlayerData } from '../../database/interface/GrandbossPlayersTableApi'
import _ from 'lodash'
import aigle from 'aigle'
import Timeout = NodeJS.Timeout

class Manager implements L2DataApi {
    bossStatus: { [ bossId: number ]: number } = {}
    bossInstances: { [ bossId: number ]: number } = {}
    previousData: { [ bossId: number ]: L2GrandbossDataItem } = {}
    updateTask: Timeout

    getBoss( bossId: number ): L2GrandBossInstance {
        return L2World.getObjectById( this.bossInstances[ bossId ] ) as L2GrandBossInstance
    }

    getBossStatus( bossId: number ): number {
        return _.get( this.bossStatus, bossId, 0 )
    }

    async load(): Promise<Array<string>> {
        let records: Array<L2GrandbossDataItem> = await DatabaseManager.getGrandbossData().getAll()
        let playerRecords: Array<L2GrandbossPlayerData> = await DatabaseManager.getGrandbossPlayers().getAll()
        let manager = this

        _.each( records, ( data: L2GrandbossDataItem ) => {
            manager.previousData[ data.bossId ] = data
            manager.bossStatus[ data.bossId ] = data.status
        } )

        // TODO : add player records outside of area
        // _.each( playerRecords, ( data: L2GrandbossPlayerData ) => {
        //     let area = this.zones[ data.zoneId ]
        //     if ( area ) {
        //         area.addAllowedPlayer( data.objectId )
        //     }
        // } )

        this.updateTask = setInterval( this.storeAll.bind( this ), 300 * 1000 )

        return [
            `GrandBossManager : loaded ${ _.size( this.previousData ) } boss records.`,
            `GrandBossManager : updated ${ _.size( playerRecords ) } players in boss zones.`,
        ]
    }

    async setBossStatus( bossId: number, status: number ): Promise<void> {
        this.bossStatus[ bossId ] = status
        await this.updateDatabase( bossId, true )
    }

    async updateDatabase( bossId: number, isStatusOnly: boolean ): Promise<void> {
        let bossInstance: L2GrandBossInstance = this.getBoss( bossId )

        if ( !bossInstance ) {
            return
        }

        if ( isStatusOnly ) {
            return DatabaseManager.getGrandbossData().setStatus( bossId, this.bossStatus[ bossId ] )
        }

        let data: L2GrandbossDataItem = {
            bossId,
            currentHp: bossInstance.getCurrentHp(),
            currentMp: bossInstance.getCurrentMp(),
            heading: bossInstance.getHeading(),
            respawnTime: this.previousData[ bossId ].respawnTime,
            status: this.bossStatus[ bossId ],
            x: bossInstance.getX(),
            y: bossInstance.getY(),
            z: bossInstance.getZ(),
        }

        return DatabaseManager.getGrandbossData().update( data )
    }

    // TODO : convert to save only modified data instead all data
    // TODO : use batch update instead of individual calls
    async storeAll() : Promise<void> {
        let manager = this
        await aigle.resolve( this.previousData ).eachLimit( 10, async ( data: L2GrandbossDataItem ) => {
            return manager.updateDatabase( data.bossId, false )
        } )
    }
}

export const GrandBossManager = new Manager()