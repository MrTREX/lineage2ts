import { Skill } from '../models/Skill'
import { L2PetInstance } from '../models/actor/instance/L2PetInstance'
import { DatabaseManager } from '../../database/manager'
import { L2Summon } from '../models/actor/L2Summon'
import { L2ServitorInstance } from '../models/actor/instance/L2ServitorInstance'
import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import _ from 'lodash'
import aigle from 'aigle'
import { AbnormalType } from '../models/skills/AbnormalType'
import { ConfigManager } from '../../config/ConfigManager'
import { BuffProperties } from '../models/skills/BuffDefinition'
import { BuffInfo } from '../models/skills/BuffInfo'
import { CharacterSummonSkillsItem } from '../../database/interface/CharacterSummonSkillsSaveTableApi'
import { SkillCache } from './SkillCache'
import { EffectScope } from '../models/skills/EffectScope'
import {
    CharacterSkillsSaveItem,
    CharacterSkillsSaveItemType
} from '../../database/interface/CharacterSkillsSaveTableApi'
import { L2ReuseTime } from '../models/SkillTimestamp'

export type PetEffect = { [ key: number ]: BuffProperties }

class Manager {
    servitorEffects: { [ objectId : number ]: { [ classIndex : number ]: { [ skillId: number ] : PetEffect}}} = {}
    petEffects: { [ controlId: number ]: PetEffect} = {}

    async updatePetEffects( pet: L2PetInstance, storeEffects: boolean ) : Promise<void> {
        this.petEffects[ pet.getControlObjectId() ] = {}
        let storedBuffs: Array<BuffProperties> = this.getStoredBuffs( pet )

        await DatabaseManager.getPetSkillsSave().delete( pet.getControlObjectId() )

        if ( storeEffects ) {
            await DatabaseManager.getPetSkillsSave().update( pet.getControlObjectId(), storedBuffs )
        }

        storedBuffs.forEach( ( effect: BuffProperties ) => {
            this.addPetEffect( pet.getControlObjectId(), effect )
        } )
    }

    async restorePetEffects( pet: L2PetInstance ) : Promise<void> {
        let effects: Array<BuffProperties> = await DatabaseManager.getPetSkillsSave().load( pet.getControlObjectId() )

        if ( effects.length === 0 ) {
            return
        }

        effects.forEach( ( data: BuffProperties ) => {
            this.addPetEffect( pet.getControlObjectId(), data )
        } )

        await DatabaseManager.getPetSkillsSave().delete( pet.getControlObjectId() )
        return this.applyEffects( pet, this.petEffects[ pet.getControlObjectId() ] )
    }

    addPetEffect( controlObjectId: number, effect: BuffProperties ) : void {
        if ( !this.petEffects[ controlObjectId ] ) {
            this.petEffects[ controlObjectId ] = {}
        }

        this.petEffects[ controlObjectId ][ effect.skill.getId() ] = effect
    }

    applyEffects( summon: L2Summon, effectItems: PetEffect ) : Promise<any> {
        return aigle.resolve( effectItems ).each( ( effect: BuffProperties ): Promise<void> => {
            return effect.skill.applyEffects( summon, summon, false, false, false, effect.durationMs )
        } ) as unknown as Promise<any>
    }

    addServitorEffect( player: L2PcInstance, referenceSkill: number, skill: Skill, durationMs: number ) : void {
        let path = [ player.getObjectId(), player.getClassIndex(), referenceSkill, skill.getId() ].join( '.' )
        let buff : BuffProperties = {
            skill,
            durationMs
        }

        _.setWith( this.servitorEffects, path, buff, Object )
    }

    async updateServitorEffects( servitor: L2ServitorInstance, storeEffects: boolean ) : Promise<void> {
        let player : L2PcInstance = servitor.getOwner()
        let path = [ player.getObjectId(), player.getClassIndex(), servitor.getSummonSkillId() ].join( '.' )
        _.unset( this.servitorEffects, path )

        let storedBuffs: Array<BuffProperties> = this.getStoredBuffs( servitor )

        await DatabaseManager.getCharacterSummonSkillsSave().delete( servitor.getOwnerId(), servitor.getOwner().getClassIndex(), servitor.getSummonSkillId() )

        if ( storeEffects ) {
            await DatabaseManager.getCharacterSummonSkillsSave().update( servitor.getOwnerId(), servitor.getOwner().getClassIndex(), servitor.getSummonSkillId(), storedBuffs )
        }

        storedBuffs.forEach( ( effect: BuffProperties ) => {
            this.addServitorEffect( player, servitor.getSummonSkillId(), effect.skill, effect.durationMs )
        } )
    }

    async restoreServitorEffects( servitor: L2ServitorInstance ) : Promise<void> {
        let effects: Array<CharacterSummonSkillsItem> = await DatabaseManager.getCharacterSummonSkillsSave().load( servitor.getOwnerId(), servitor.getOwner().getClassIndex(), servitor.getSummonSkillId() )

        if ( effects.length === 0 ) {
            return
        }

        let player : L2PcInstance = servitor.getOwner()
        effects.forEach( ( effect: CharacterSummonSkillsItem ) => {
            let skill = SkillCache.getSkill( effect.skillId, effect.skillLevel )
            if ( !skill || !skill.hasEffects( EffectScope.GENERAL ) ) {
                return
            }

            this.addServitorEffect( player, servitor.getSummonSkillId(), skill, effect.remainingTime )
        } )

        await DatabaseManager.getCharacterSummonSkillsSave().delete( servitor.getOwnerId(), servitor.getOwner().getClassIndex(), servitor.getSummonSkillId() )

        return this.applyEffects( servitor, this.getServitorEffects( player, servitor.getSummonSkillId() ) )
    }

    getServitorEffects( player: L2PcInstance, referenceSkillId: number ) : PetEffect {
        let path = [ player.getObjectId(), player.getClassIndex(), referenceSkillId ].join( '.' )
        return _.get( this.servitorEffects, path )
    }

    private getStoredBuffs( summon: L2Summon ) : Array<BuffProperties> {
        let storedBuffs: Array<BuffProperties> = []
        let visitedSkillCodes : Set<number> = new Set<number>()
        summon.getEffectList().applyToEffects( ( info: BuffInfo ) => {
            if ( !info ) {
                return
            }

            let skill : Skill = info.getSkill()
            if ( skill.getAbnormalType() === AbnormalType.LIFE_FORCE_OTHERS
                    || skill.isToggle()
                    || ( skill.isDance() && !ConfigManager.character.storeDances() )
                    || visitedSkillCodes.has( skill.getReuseHashCode() ) ) {
                return
            }

            visitedSkillCodes.add( skill.getReuseHashCode() )
            storedBuffs.push( info.getProperties() )
        } )

        return storedBuffs
    }

    async restorePlayerEffects( player: L2PcInstance ) : Promise<void> {
        const items : Array<CharacterSkillsSaveItem> = await DatabaseManager.getCharacterSkillsSave().load( player.getObjectId(), player.getClassIndex() )

        await aigle.resolve( items ).each( ( item : CharacterSkillsSaveItem ) => {
            let skill: Skill = SkillCache.getSkill( item.skillId, item.skillLevel )
            if ( !skill ) {
                return
            }

            let time: number = item.expirationTime - Date.now()
            if ( time > 100 ) {
                player.disableSkill( skill, time )
                player.getSkillReuse().add( skill, item.reuseDelay, item.expirationTime )
            }

            if ( item.type !== CharacterSkillsSaveItemType.Effect ) {
                return
            }

            return skill.applyEffects( player, player, false, false, false, item.remainingMs )
        } )

        return DatabaseManager.getCharacterSkillsSave().deleteByClassIndex( player.getObjectId(), player.getClassIndex() )
    }

    async updatePlayerEffects( player: L2PcInstance, storeEffects : boolean ) : Promise<void> {
        await DatabaseManager.getCharacterSkillsSave().deleteByClassIndex( player.getObjectId(), player.getClassIndex() )

        let skillsToSave : Array<CharacterSkillsSaveItem> = []
        let storedSkills: Set<number> = new Set<number>()
        let buffIndex = 0

        if ( storeEffects ) {
            player.getEffectList().applyToEffects( ( info: BuffInfo ) => {
                if ( !info ) {
                    return
                }

                /*
                    Avoid storing effect that is about to expire:
                    - consider how long does it take for client to fully load and display character
                    - consider that few seconds of loaded character may not play significant outcome
                 */
                let remainingMs = info.getRemainingMs()
                if ( remainingMs < 5000 ) {
                    return
                }

                let skill: Skill = info.getSkill()
                if ( skill.getAbnormalType() === AbnormalType.LIFE_FORCE_OTHERS
                        || skill.isToggle()
                        || ( skill.isDance() && !ConfigManager.character.storeDances() )
                        || storedSkills.has( skill.getReuseHashCode() ) ) {
                    return
                }

                storedSkills.add( skill.getReuseHashCode() )

                let skillTime: L2ReuseTime = player.getSkillReuseData( skill )
                let isActive = skillTime && skillTime.isActive()

                let data : CharacterSkillsSaveItem = {
                    classIndex: player.getClassIndex(),
                    expirationTime: isActive ? skillTime.getExpirationTime() : 0,
                    index: ++buffIndex,
                    objectId: player.getObjectId(),
                    remainingMs,
                    reuseDelay: isActive ? skillTime.getReuseMs() : 0,
                    skillId: skill.getId(),
                    skillLevel: skill.getLevel(),
                    type: CharacterSkillsSaveItemType.Effect
                }

                skillsToSave.push( data )
            } )
        }

        player.getSkillReuse().getAllItems().forEach( ( data: L2ReuseTime, hash: number ) => {
            if ( storedSkills.has( hash ) ) {
                return
            }

            skillsToSave.push( {
                classIndex: player.getClassIndex(),
                expirationTime: data.getExpirationTime(),
                index: ++buffIndex,
                objectId: player.getObjectId(),
                remainingMs: -1,
                reuseDelay: data.getReuseMs(),
                skillId: data.getId(),
                skillLevel: data.getLevel(),
                type: CharacterSkillsSaveItemType.SkillReuse
            } )
        } )

        if ( skillsToSave.length === 0 ) {
            return
        }

        return DatabaseManager.getCharacterSkillsSave().update( player.getObjectId(), player.getClassIndex(), skillsToSave )
    }
}

export const CharacterEffectsCache = new Manager()