import _ from 'lodash'
import { ItemData, ItemDataHelper } from '../interface/ItemData'

export interface CustomDrop {
    templateId? : number
    itemId: number
    minCount: number
    maxCount: number
    chance: number
}

interface TrackableCustomDrop extends CustomDrop {
    dropId: number
}

class Manager {
    currentId: number = 0
    genericDrops: Array<TrackableCustomDrop> = []
    templateDrops: { [ templateId: number ] : Array<TrackableCustomDrop> } = {}
    genericSpoils: Array<TrackableCustomDrop> = []
    templateSpoils: { [ templateId: number ] : Array<TrackableCustomDrop> } = {}

    getGenericDrops() : Array<TrackableCustomDrop> {
        return this.genericDrops
    }

    getTemplateDrops( templateId: number ) : Array<TrackableCustomDrop> {
        return this.templateDrops[ templateId ]
    }

    getGenericSpoils() : Array<TrackableCustomDrop> {
        return this.genericSpoils
    }

    getTemplateSpoils( templateId: number ) : Array<TrackableCustomDrop> {
        return this.templateSpoils[ templateId ]
    }

    addGenericDrop( itemId: number, minCount: number, maxCount: number, chance: number ) : number {
        let drop : TrackableCustomDrop = {
            itemId,
            minCount,
            maxCount,
            chance,
            dropId: this.currentId
        }

        this.currentId++

        this.genericDrops.push( drop )

        return drop.dropId
    }

    addTemplateDrop( itemId: number, minCount: number, maxCount: number, chance: number, templateId: number ) : number {
        let drop : TrackableCustomDrop = {
            itemId,
            minCount,
            maxCount,
            chance,
            dropId: this.currentId,
            templateId
        }

        this.currentId++

        if ( !this.templateDrops[ templateId ] ) {
            this.templateDrops[ templateId ] = []
        }

        this.templateDrops[ templateId ].push( drop )

        return drop.dropId
    }

    addGenericSpoil( itemId: number, minCount: number, maxCount: number, chance: number ) : number {
        let drop : TrackableCustomDrop = {
            itemId,
            minCount,
            maxCount,
            chance,
            dropId: this.currentId,
            templateId: null
        }

        this.currentId++

        this.genericSpoils.push( drop )

        return drop.dropId
    }

    addTemplateSpoil( itemId: number, minCount: number, maxCount: number, chance: number, templateId: number ) : number {
        let drop : TrackableCustomDrop = {
            itemId,
            minCount,
            maxCount,
            chance,
            dropId: this.currentId,
            templateId
        }

        this.currentId++

        if ( !this.templateSpoils[ templateId ] ) {
            this.templateSpoils[ templateId ] = []
        }

        this.templateSpoils[ templateId ].push( drop )

        return drop.dropId
    }

    removeGenericDrops( dropIds: Array<number> ) : void {
        _.remove( this.genericDrops, ( drop: TrackableCustomDrop ) : boolean => {
            return dropIds.includes( drop.dropId )
        } )
    }

    removeTemplateDrops( dropIds: Array<number> ) : void {
        _.each( this.templateDrops, ( drops: Array<TrackableCustomDrop> ) => {
            _.remove( drops, ( drop: TrackableCustomDrop ) : boolean => {
                return dropIds.includes( drop.dropId )
            } )
        } )
    }

    removeGenericSpoils( dropIds: Array<number> ) : void {
        _.remove( this.genericSpoils, ( drop: TrackableCustomDrop ) : boolean => {
            return dropIds.includes( drop.dropId )
        } )
    }

    removeTemplateSpoils( dropIds: Array<number> ) : void {
        _.each( this.templateSpoils, ( drops: Array<TrackableCustomDrop> ) => {
            _.remove( drops, ( drop: TrackableCustomDrop ) : boolean => {
                return dropIds.includes( drop.dropId )
            } )
        } )
    }

    addSpoils( id: number, existingItems: Array<ItemData> ) : void {
        const addSpoil = ( drop: CustomDrop ) : void => {
            if ( _.random( 0, 100, true ) >= drop.chance ) {
                return
            }

            existingItems.push( ItemDataHelper.createItem( drop.itemId, _.random( drop.minCount, drop.maxCount ) ) )
        }

        let spoils = this.getGenericSpoils()
        if ( spoils ) {
            spoils.forEach( addSpoil )
        }

        let templateSpoils = this.getTemplateSpoils( id )
        if ( templateSpoils ) {
            templateSpoils.forEach( addSpoil )
        }
    }
}

export const AdditionalDropManager = new Manager()