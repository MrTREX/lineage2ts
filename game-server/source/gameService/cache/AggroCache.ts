import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { L2World } from '../L2World'
import { L2Character } from '../models/actor/L2Character'
import { L2Attackable } from '../models/actor/L2Attackable'
import _ from 'lodash'
import { ObjectPool } from '../helpers/ObjectPoolHelper'
import { L2Playable } from '../models/actor/L2Playable'

export interface AggroData {
    attackerId: number
    hateAmount: number
    totalDamage : number
}

export type CharacterAggroData = {[ objectId : number ] : AggroData }
const aggroData : {[ objectId : number ] : CharacterAggroData } = {}

const aggroDataPool = new ObjectPool( 'AggroCache', () : AggroData => {
    return {
        attackerId: 0,
        hateAmount: 0,
        totalDamage: 0
    }
} )

class Manager {
    addAggro( ownerId: number, attackerId : number, damage: number, aggroAmount: number ) : void {
        let data : AggroData = AggroCache.getAttackerData( ownerId, attackerId )
        data.totalDamage = Math.floor( data.totalDamage + damage )

        if ( aggroAmount !== 0 ) {
            let attacker = L2World.getObjectById( attackerId )
            let player : L2PcInstance = attacker.getActingPlayer()

            if ( !player || !player.getTrap() || !player.getTrap().isTriggered() ) {
                data.hateAmount = Math.floor( data.hateAmount + aggroAmount )
            }

            return
        }

        data.hateAmount += 1

        // TODO : needs to be part of AITrait code since event will be triggered each time target receives damage
        // if(false && ListenerCache.hasNpcTemplateListeners(this.getId(), EventType.AttackableAggroRangeEnter)) {
        //     let eventData : AttackableAggroRangeEnterEvent = {
        //         targetId: this.getObjectId(),
        //         attackerId: attacker.getObjectId(),
        //         attackerPlayerId: player.getObjectId()
        //     }
        //
        //     ListenerCache.sendEvent(this, EventType.AttackableAggroRangeEnter, this.getActingPlayer(), eventData)
        // }
    }

    clearData( ownerId : number ) : void {
        _.each( aggroData[ ownerId ], ( data : AggroData ) => {
            aggroDataPool.recycleValue( data )
        } )

        _.unset( aggroData, ownerId )
    }

    clearAttackerData( ownerId : number, attackerId : number ) : void {
        let data = aggroData[ ownerId ][ attackerId ]
        if ( !data ) {
            return
        }

        aggroDataPool.recycleValue( data )
        _.unset( aggroData[ ownerId ], attackerId )
    }

    getAttackerData( ownerId : number, attackerId: number ) : AggroData {
        if ( !aggroData[ ownerId ] ) {
            aggroData[ ownerId ] = {}
        }

        if ( !aggroData[ ownerId ][ attackerId ] ) {
            let data : AggroData = aggroDataPool.getValue()

            data.attackerId = attackerId
            data.hateAmount = 0
            data.totalDamage = 0

            aggroData[ ownerId ][ attackerId ] = data
        }

        return aggroData[ ownerId ][ attackerId ]
    }

    getOwnedData( ownerId : number ) : CharacterAggroData {
        return aggroData[ ownerId ]
    }

    hasAggroData( ownerId: number ) : boolean {
        return !!this.getOwnedData( ownerId )
    }

    getAggroAmount( ownerId : number, targetId : number ) : number {
        let characterData : CharacterAggroData = this.getOwnedData( ownerId )
        if ( !characterData ) {
            return 0
        }

        let record : AggroData = characterData[ targetId ]
        if ( !record ) {
            return 0
        }

        let target = L2World.getObjectById( targetId ) as L2Character
        if ( target.isPlayer() ) {
            let player : L2PcInstance = target as L2PcInstance
            if ( player.isInvisible() || player.isInvulnerable() || player.isSpawnProtected() ) {
                this.clearAttackerData( ownerId, targetId )

                return 0
            }
        }

        if ( !target.isVisible() || target.isInvisible() ) {
            this.clearAttackerData( ownerId, targetId )
            return 0
        }

        if ( target.isAlikeDead() ) {
            record.hateAmount = 0
            return 0
        }

        return record.hateAmount
    }

    getAggroCharacters( ownerId : number ) : Array<L2Character> {
        return _.map( this.getOwnedData( ownerId ), ( data: AggroData ) : L2Character => {
            return L2World.getObjectById( data.attackerId ) as L2Character
        } )
    }

    getMostHated( owner: L2Attackable | L2Playable ) : L2Character {
        let characterData : CharacterAggroData = this.getOwnedData( owner.getObjectId() )
        if ( !characterData ) {
            return
        }

        let amount : number = 0
        let mostHatedAttacker : L2Character
        let isAttackable = owner.isAttackable()
        let aggroRange = isAttackable ? ( owner as L2Attackable ).getAggroRange() : 1000

        Object.values( characterData ).forEach( ( data : AggroData ) => {
            let attacker = L2World.getObjectById( data.attackerId ) as L2Character
            if ( isAttackable &&
                    ( attacker.isAlikeDead()
                    || !attacker.isVisible()
                    || owner.calculateDistance( attacker ) > aggroRange ) ) {
                data.hateAmount = 0
            }

            if ( data.hateAmount > amount ) {
                mostHatedAttacker = attacker
                amount = data.hateAmount
            }
        } )

        return mostHatedAttacker
    }

    reduceAggroByAmount( ownerId : number, amount : number, targetId : number = 0 ) : void {
        let characterData : CharacterAggroData = this.getOwnedData( ownerId )

        if ( !characterData ) {
            return
        }

        if ( !targetId ) {

            _.each( characterData, ( data : AggroData ) => {
                data.hateAmount = Math.max( data.hateAmount - amount, 0 )
            } )

            return
        }

        let data : AggroData = characterData[ targetId ]
        if ( !data ) {
            return
        }

        data.hateAmount = Math.max( data.hateAmount - amount, 0 )
    }

    resetAggro( ownerId : number, targetId : number, amount : number = 0 ) : void {
        let characterData : CharacterAggroData = this.getOwnedData( ownerId )

        if ( !characterData ) {
            return
        }

        let data : AggroData = characterData[ targetId ]
        if ( !data ) {
            return
        }

        data.hateAmount = Math.max( amount, 0 )
    }

    removeAggro( ownerId : number, targetId : number ) : void {
        let characterData : CharacterAggroData = this.getOwnedData( ownerId )

        if ( !characterData ) {
            return
        }

        delete characterData[ targetId ]
    }

    hasAggroTarget( ownerId: number, aggroThreshold : number = 1 ) : boolean {
        return _.some( this.getOwnedData( ownerId ), ( data : AggroData ) : boolean => {
            return data.hateAmount > aggroThreshold
        } )
    }

    hasAggro( ownerId: number, targetId: number ) : boolean {
        return _.has( aggroData, [ ownerId, targetId ] )
    }
}

export const AggroCache = new Manager()