import { DatabaseManager } from '../../database/manager'
import { ExGetBookMarkInfoPacket, ExGetBookMarkInfoPacketWithPlayer } from '../packets/send/ExGetBookMarkInfoPacket'
import _ from 'lodash'
import { SystemMessageBuilder } from '../packets/send/SystemMessage'
import { SystemMessageIds } from '../packets/SystemMessageIdValues'
import { PacketDispatcher } from '../PacketDispatcher'
import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { ConfigManager } from '../../config/ConfigManager'
import { L2CharacterTeleportBookmarkItem } from '../../database/interface/CharacterTeleportBookmarksTableApi'
import { SiegeRole } from '../enums/SiegeRole'
import { AreaType } from '../models/areas/AreaType'
import { teleportCharacterToGeometryCoordinates } from '../helpers/TeleportHelper'
import { GeometryId } from '../enums/GeometryId'

type BookmarkMap = { [ slot: number ]: L2CharacterTeleportBookmarkItem }
const bookmarks: { [ playerId: number ]: BookmarkMap } = {}

const enum PlayerTeleportRestrictions {
    PlayerOnly,
    PlayerGroup
}

export const TeleportBookmarkCache = {
    modify( playerId: number, id: number, icon: number, tag: string, name: string ): Promise<void> {
        let data: L2CharacterTeleportBookmarkItem = bookmarks[ playerId ][ id ]
        if ( data ) {
            data.icon = icon
            data.tag = tag
            data.name = name

            PacketDispatcher.sendOwnedData( playerId, ExGetBookMarkInfoPacket( playerId ) )
            return DatabaseManager.getCharacterTeleportBookmarks().update( playerId, id, icon, tag, name )
        }
    },

    async remove( playerId: number, id: number ): Promise<void> {
        if ( bookmarks[ playerId ][ id ] ) {
            _.unset( bookmarks[ playerId ], id )
            PacketDispatcher.sendOwnedData( playerId, ExGetBookMarkInfoPacket( playerId ) )

            return DatabaseManager.getCharacterTeleportBookmarks().delete( playerId, id )
        }
    },

    async teleport( player: L2PcInstance, id: number ): Promise<void> {
        if ( !this.canProceed( player, PlayerTeleportRestrictions.PlayerOnly ) ) {
            return
        }

        let item = player.getInventory().getItemByItemId( 13016 )
        if ( !item ) {
            return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_TELEPORT_BECAUSE_YOU_DO_NOT_HAVE_A_TELEPORT_ITEM ) )
        }

        let data: L2CharacterTeleportBookmarkItem = bookmarks[ player.getObjectId() ][ id ]
        if ( data && await player.destroyItemByObjectId( item.getObjectId(), 1, true, 'L2PcInstance.teleportUsingBookmark' ) ) {
            await teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, data.x, data.y, data.z, data.heading, 0 )
        }

        player.sendOwnedData( ExGetBookMarkInfoPacketWithPlayer( player ) )
    },

    async add( player: L2PcInstance, icon: number, tag: string, name: string ): Promise<void> {
        if ( player.getInstanceId() !== 0 ) {
            return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_USE_MY_TELEPORTS_TO_REACH_THIS_AREA ) )
        }

        if ( !this.canProceed( player, PlayerTeleportRestrictions.PlayerGroup ) ) {
            return
        }

        let data = bookmarks[ player.getObjectId() ]
        if ( _.size( data ) >= player.getAvailableBookmarkSlots() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_HAVE_NO_SPACE_TO_SAVE_THE_TELEPORT_LOCATION ) )
            return
        }

        if ( !await player.destroyItemByItemId( 20033, 1, false, 'TeleportBookmarkCache.add' ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_BOOKMARK_THIS_LOCATION_BECAUSE_YOU_DO_NOT_HAVE_A_MY_TELEPORT_FLAG ) )
            return
        }

        let id = 1
        for ( ; id <= player.getAvailableBookmarkSlots(); ++id ) {
            if ( !data[ id ] ) {
                break
            }
        }

        data[ id ] = {
            heading: player.getHeading(),
            icon,
            id,
            name,
            tag,
            x: player.getX(),
            y: player.getY(),
            z: player.getZ(),
        }

        let packet = new SystemMessageBuilder( SystemMessageIds.S1_DISAPPEARED )
                .addItemNameWithId( 20033 )
                .getBuffer()
        player.sendOwnedData( packet )
        player.sendOwnedData( ExGetBookMarkInfoPacketWithPlayer( player ) )

        return DatabaseManager.getCharacterTeleportBookmarks().add( player.getObjectId(), id, player, icon, tag, name )
    },

    canProceed( player: L2PcInstance, type: PlayerTeleportRestrictions ): boolean {
        if ( player.isInCombat() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_USE_MY_TELEPORTS_DURING_A_BATTLE ) )
            return false
        }

        if ( player.isInSiege() || ( player.getSiegeRole() !== SiegeRole.None ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_USE_MY_TELEPORTS_WHILE_PARTICIPATING ) )
            return false
        }

        if ( player.isInDuel() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_USE_MY_TELEPORTS_DURING_A_DUEL ) )
            return false
        }

        if ( player.isFlying() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_USE_MY_TELEPORTS_WHILE_FLYING ) )
            return false
        }

        if ( player.isInOlympiadMode() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_USE_MY_TELEPORTS_WHILE_PARTICIPATING_IN_AN_OLYMPIAD_MATCH ) )
            return false
        }

        if ( player.isStunned() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_USE_MY_TELEPORTS_WHILE_YOU_ARE_PARALYZED ) )
            return false
        }

        if ( player.isDead() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_USE_MY_TELEPORTS_WHILE_YOU_ARE_DEAD ) )
            return false
        }

        if ( type === PlayerTeleportRestrictions.PlayerGroup && ( player.isIn7sDungeon() || ( player.isInParty() && player.getParty().isInDimensionalRift() ) ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_USE_MY_TELEPORTS_TO_REACH_THIS_AREA ) )
            return false
        }

        if ( player.isInWater() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_USE_MY_TELEPORTS_UNDERWATER ) )
            return false
        }

        if ( type === PlayerTeleportRestrictions.PlayerGroup && ( player.isInSiegeArea()
                || player.isInArea( AreaType.ClanHall )
                || player.isInArea( AreaType.Jail )
                || player.isInArea( AreaType.Castle )
                || player.isInArea( AreaType.NoSummonPlayer )
                || player.isInArea( AreaType.Fort ) ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_USE_MY_TELEPORTS_TO_REACH_THIS_AREA ) )
            return false
        }

        if ( player.isInArea( AreaType.NoBookmarkUse )
                || player.isInBoat()
                || player.isInAirShip() ) {
            if ( type === PlayerTeleportRestrictions.PlayerOnly ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_USE_MY_TELEPORTS_IN_THIS_AREA ) )
            } else if ( type === PlayerTeleportRestrictions.PlayerGroup ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_USE_MY_TELEPORTS_TO_REACH_THIS_AREA ) )
            }

            return false
        }

        return true
    },

    get( playerId: number ): BookmarkMap {
        return bookmarks[ playerId ]
    },

    async loadPlayer( playerId: number ): Promise<void> {
        bookmarks[ playerId ] = await DatabaseManager.getCharacterTeleportBookmarks().getAll( playerId )
    },

    unloadPlayer( playerId: number ): void {
        delete bookmarks[ playerId ]
    },

    hasBookmark( playerId: number, slot: number ) : boolean {
        let data = bookmarks[ playerId ]

        return !!data[ slot ]
    }
}