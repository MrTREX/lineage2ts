import { ConfigManager } from '../../config/ConfigManager'
import { L2DataApi } from '../../data/interface/l2DataApi'
import { L2LotteryTableData } from '../../database/interface/LotteryTableApi'
import { DatabaseManager } from '../../database/manager'
import { GeneralHelper } from '../helpers/GeneralHelper'
import { BroadcastHelper } from '../helpers/BroadcastHelper'
import moment, { Moment } from 'moment'
import { SystemMessageBuilder } from '../packets/send/SystemMessage'
import { SystemMessageIds } from '../packets/SystemMessageIdValues'
import { L2ItemsTableItem } from '../../database/interface/ItemsTableApi'
import { LotteryValues } from '../values/LotteryValues'
import { L2ItemInstance } from '../models/items/instance/L2ItemInstance'
import _ from 'lodash'
import { ServerLog } from '../../logger/Logger'
import Timeout = NodeJS.Timeout

class Manager implements L2DataApi {
    currentId: number = 1
    adenaPrize: number = ConfigManager.general.getLotteryPrize()
    isSellingTickets: boolean = false
    isStarted: boolean = false
    endDate: number = 0
    lotteryTask: Timeout

    async load(): Promise<Array<string>> {
        if ( !ConfigManager.general.allowLottery() ) {
            return [
               'LotteryManager : lottery is disabled.'
            ]
        }

        await this.startLottery()

        return [
           'LotteryManager : started lottery system.',
           `LotteryManager : jackpot is ${this.adenaPrize} adena`
        ]
    }

    getLotteryId() : number {
        return this.currentId
    }

    async startLottery() : Promise<void> {
        let data : L2LotteryTableData = await DatabaseManager.getLottery().getLastItem()
        let currentTime = Date.now()

        if ( data ) {
            this.currentId = data.id

            if ( data.isFinished ) {
                this.currentId++
                this.adenaPrize = data.nextPrize
            } else {
                this.adenaPrize = data.prize
                this.endDate = data.endDate

                if ( this.endDate <= ( currentTime + GeneralHelper.minutesToMillis( 2 ) ) ) {
                    return this.finishLottery()
                }

                if ( this.endDate > currentTime ) {
                    this.isStarted = true
                    this.lotteryTask = setTimeout( this.finishLottery.bind( this ), this.endDate - currentTime )

                    if ( this.endDate > ( currentTime + GeneralHelper.minutesToMillis( 12 ) ) ) {
                        this.isSellingTickets = true
                        setTimeout( this.stopSellingTickets.bind( this ), this.endDate - currentTime - GeneralHelper.minutesToMillis( 10 ) )
                    }

                    return
                }
            }
        }

        this.isSellingTickets = true
        this.isStarted = true

        BroadcastHelper.broadcastTextToAllPlayers( 'Lottery Manager', `Lottery tickets are now available for Lucky Lottery #${this.getLotteryId()}.` )
        this.endDate = this.getNextDate()

        let currentLotteryDuration = this.endDate - currentTime
        setTimeout( this.stopSellingTickets.bind( this ), currentLotteryDuration - GeneralHelper.minutesToMillis( 10 ) )
        this.lotteryTask = setTimeout( this.finishLottery.bind( this ), currentLotteryDuration )

        await DatabaseManager.getLottery().addItem( this.currentId, this.endDate, this.adenaPrize, this.adenaPrize )
    }

    getNextDate() : number {
        let finishDate : Moment = this.endDate > 0 ? moment( this.endDate ) : moment()

        if ( ConfigManager.general.getLotteryPeriodDays() <= 0 ) {
            finishDate.weekday( 0 )

            if ( finishDate.isBefore( moment() ) ) {
                finishDate.add( 7, 'days' )
            }

            finishDate.hour( 19 ).minute( 0 ).second( 0 )

            return finishDate.valueOf()
        }

        if ( ConfigManager.general.getLotteryPrize() >= 1 ) {
            finishDate.add( Math.floor( ConfigManager.general.getLotteryPeriodDays() ), 'days' ).hour( 19 ).minute( 0 ).second( 0 )

            return finishDate.valueOf()
        }

        let hours = Math.ceil( ConfigManager.general.getLotteryPeriodDays() * 24 )
        finishDate.hour( 19 ).add( hours, 'hours' ).minute( 0 ).second( 0 )

        return finishDate.valueOf()
    }

    stopSellingTickets() : void {
        ServerLog.info( `LotteryManager : stopping to sell tickets for lottery #${this.currentId}` )

        this.isSellingTickets = false
        BroadcastHelper.toAllOnlinePlayersData( SystemMessageBuilder.fromMessageId( SystemMessageIds.LOTTERY_TICKET_SALES_TEMP_SUSPENDED ) )
    }

    async finishLottery() : Promise<void> {
        let luckyNumbers : Array<number> = []

        while ( luckyNumbers.length < 5 ) {
            let currentNumber = _.random( 1, 20 )

            if ( !luckyNumbers.includes( currentNumber ) ) {
                luckyNumbers.push( currentNumber )
            }
        }

        let enchant = 0
        let type = 0

        _.each( luckyNumbers, ( currentNumber: number ) => {
            if ( currentNumber < 17 ) {
                enchant += Math.pow( 2, currentNumber - 1 )
                return
            }

            type += Math.pow( 2, currentNumber - 17 )
        } )

        let count1 = 0
        let count2 = 0
        let count3 = 0
        let count4 = 0

        let itemRecords : Array<L2ItemsTableItem> = await DatabaseManager.getItems().getItemsByItemIdAndType( LotteryValues.lotteryItemId, this.currentId )

        _.each( itemRecords, ( item: L2ItemsTableItem ) => {
            let currentEnchant = item.enchantLevel & enchant
            let currentType = item.customTypeTwo & type

            if ( currentEnchant === 0 && currentType === 0 ) {
                return
            }

            let count = 0

            _.times( 16, () => {
                let enchantValue = Math.floor( currentEnchant / 2 )

                if ( enchantValue !== Math.round( currentEnchant / 2 ) ) {
                    count++
                }

                let typeValue = Math.floor( currentType / 2 )

                if ( typeValue !== ( currentType / 2 ) ) {
                    count++
                }

                currentEnchant = enchantValue
                currentType = typeValue
            } )


            if ( count === 5 ) {
                count1++
                return
            }

            if ( count === 4 ) {
                count2++
                return
            }

            if ( count === 3 ) {
                count3++
                return
            }

            if ( count > 0 ) {
                count4++
                return
            }
        } )

        let topPrize = count4 * ConfigManager.general.getLottery2and1NumberPrize()
        let prizeOne = 0
        let prizeTwo = 0
        let prizeThree = 0

        if ( count1 > 0 ) {
            prizeOne = Math.floor( ( ( this.adenaPrize - topPrize ) * ConfigManager.general.getLottery5NumberRate() ) / count1 )
        }

        if ( count2 > 0 ) {
            prizeTwo = Math.floor( ( ( this.adenaPrize - topPrize ) * ConfigManager.general.getLottery4NumberRate() ) / count2 )
        }

        if ( count3 > 0 ) {
            prizeThree = Math.floor( ( ( this.adenaPrize - topPrize ) * ConfigManager.general.getLottery3NumberRate() ) / count3 )
        }

        let message = new SystemMessageBuilder( count1 > 0
                ? SystemMessageIds.AMOUNT_FOR_WINNER_S1_IS_S2_ADENA_WE_HAVE_S3_PRIZE_WINNER
                : SystemMessageIds.AMOUNT_FOR_LOTTERY_S1_IS_S2_ADENA_NO_WINNER )
                .addNumber( this.currentId )
                .addNumber( this.adenaPrize )

        if ( count1 > 0 ) {
            message.addNumber( count1 )
        }

        await BroadcastHelper.toAllOnlinePlayersData( message.getBuffer() )

        let nextPrize = this.adenaPrize - ( prizeOne + prizeTwo + prizeThree + topPrize )
        let data : L2LotteryTableData = {
            enchant,
            endDate: this.endDate,
            id: this.currentId,
            isFinished: true,
            nextPrize,
            prize: this.adenaPrize,
            prizeOne,
            prizeThree,
            prizeTwo,
            type
        }

        await DatabaseManager.getLottery().updateItem( data )

        this.lotteryTask = setTimeout( this.startLottery.bind( this ), GeneralHelper.minutesToMillis( 1 ) )
        this.currentId++
        this.isStarted = false
    }

    hasStarted() : boolean {
        return this.isStarted
    }

    hasSellableTickets() : boolean {
        return this.isSellingTickets
    }

    async increasePrize( value: number ) : Promise<void> {
        this.adenaPrize = this.adenaPrize + value
        await DatabaseManager.getLottery().updatePrice( this.currentId, this.adenaPrize, this.adenaPrize )
    }

    getPrize() : number {
        return this.adenaPrize
    }

    getEndDate() : number {
        return this.endDate
    }

    async checkTicket( item: L2ItemInstance ) : Promise<Array<number>> {
        let data : Array<L2LotteryTableData> = await DatabaseManager.getLottery().getItems( item.getCustomType1() )

        let result : Array<number> = [ 0, 0 ]

        _.each( data, ( dataItem: L2LotteryTableData ) => {
            let currentEnchant = dataItem.enchant & item.getEnchantLevel()
            let currentType = dataItem.type & item.getCustomType2()

            if ( currentEnchant === 0 && currentType === 0 ) {
                return false
            }

            let count = 0

            _.times( 16, () => {
                let enchantValue = Math.floor( currentEnchant / 2 )
                if ( enchantValue !== Math.round( currentEnchant / 2 ) ) {
                    count++
                }

                let typeValue = Math.floor( currentType / 2 )
                if ( typeValue !== ( currentType / 2 ) ) {
                    count++
                }

                currentEnchant = enchantValue
                currentType = typeValue
            } )

            switch ( count ) {
                case 0:
                    break

                case 5:
                    result = [ 1, dataItem.prizeOne ]
                    break

                case 4:
                    result = [ 2, dataItem.prizeTwo ]
                    break

                case 3:
                    result = [ 3, dataItem.prizeThree ]
                    break

                default:
                    result = [ 4, ConfigManager.general.getLottery2and1NumberPrize() ]
                    break
            }
        } )

        return result
    }

    decodeNumbers( enchant: number, type: number ) : Array<number> {
        let result = [ 0, 0, 0, 0, 0 ]
        let id = 0
        let currentValue = 1

        while ( enchant > 0 ) {
            let val = Math.floor( enchant / 2 )
            if ( val !== Math.round( enchant / 2 ) ) {
                result[ id++ ] = currentValue
            }

            enchant /= 2
            currentValue++
        }

        currentValue = 17

        while ( type > 0 ) {
            let val = Math.floor( type / 2 )
            if ( val !== Math.round( type / 2 ) ) {
                result[ id++ ] = currentValue
            }

            type /= 2
            currentValue++
        }

        return result
    }

    async scheduleLotteryStart( minutesBeforeStart: number, jackpotAmount : number ) : Promise<void> {
        if ( this.isStarted ) {
            return
        }

        clearTimeout( this.lotteryTask )

        let data : L2LotteryTableData = await DatabaseManager.getLottery().getLastItem()
        data.nextPrize = jackpotAmount
        await DatabaseManager.getLottery().updateItem( data )

        this.lotteryTask = setTimeout( this.startLottery.bind( this ), GeneralHelper.minutesToMillis( minutesBeforeStart ) )
    }

    scheduleLotteryStop( minutesBeforeStop: number ) : void {
        if ( !this.isStarted ) {
            return
        }

        clearTimeout( this.lotteryTask )
        this.lotteryTask = setTimeout( this.finishLottery.bind( this ), GeneralHelper.minutesToMillis( minutesBeforeStop ) )
    }
}

export const LotteryManager = new Manager()