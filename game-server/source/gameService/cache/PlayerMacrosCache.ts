import { DatabaseManager } from '../../database/manager'
import _, { DebouncedFunc } from 'lodash'
import { L2CharacterMacroItem } from '../../database/interface/CharacterMacrosTableApi'
import { PacketDispatcher } from '../PacketDispatcher'
import { SendMacroList } from '../packets/send/SendMacroList'
import { PlayerShortcutCache } from './PlayerShortcutCache'
import { ShortcutType } from '../enums/ShortcutType'
import { L2PcInstance } from '../models/actor/instance/L2PcInstance'

interface PlayerData {
    revision: number
    nextId: number
    data: Array<L2CharacterMacroItem>
}

class Manager {
    macros : { [ playerId : number ] : PlayerData } = {}
    debouncedDatabaseWrite: DebouncedFunc<() => void>
    itemsToDelete: Array<L2CharacterMacroItem> = []
    itemsToUpsert: Array<L2CharacterMacroItem> = []

    constructor() {
        this.debouncedDatabaseWrite = _.debounce( this.runDatabaseWrite.bind( this ), 10000, {
            maxWait: 20000
        } )
    }

    private async runDatabaseWrite() : Promise<void> {
        if ( this.itemsToDelete.length > 0 ) {
            let items = this.itemsToDelete
            this.itemsToDelete.length = 0
            await DatabaseManager.getCharacterMacros().deleteMany( items )
        }

        if ( this.itemsToUpsert.length > 0 ) {
            let items = this.itemsToUpsert
            this.itemsToUpsert.length = 0
            await DatabaseManager.getCharacterMacros().upsertMany( items )
        }
    }

    private scheduleDelete( item: L2CharacterMacroItem ) : void {
        this.itemsToDelete.push( item )
        this.debouncedDatabaseWrite()
    }

    private scheduleUpsert( item: L2CharacterMacroItem ) : void {
        this.itemsToUpsert.push( item )
        this.debouncedDatabaseWrite()
    }

    async loadPlayer( playerId: number ) : Promise<void> {
        let data = await DatabaseManager.getCharacterMacros().getMacros( playerId )
        let nextId = data.length > 0 ? _.max( _.map( data, 'id' ) ) + 1 : 1000

        this.macros[ playerId ] = {
            data,
            nextId,
            revision: 1
        }

        this.sendUpdate( playerId )
    }

    unloadPlayer( playerId: number ) : void {
        delete this.macros[ playerId ]
    }

    private sendUpdate( playerId: number ) : void {
        let playerMacros = this.macros[ playerId ]
        playerMacros.revision++

        if ( playerMacros.data.length === 0 ) {
            return PacketDispatcher.sendOwnedData( playerId, SendMacroList( playerMacros.revision, 0, null ) )
        }

        playerMacros.data.forEach( ( macro: L2CharacterMacroItem ) => {
            PacketDispatcher.sendOwnedData( playerId, SendMacroList( playerMacros.revision, playerMacros.data.length, macro ) )
        } )
    }

    registerMacro( playerId: number, macro: L2CharacterMacroItem ) {
        let playerMacros = this.macros[ playerId ]
        if ( !macro.id ) {
            playerMacros.data.push( macro )
        } else {
            let existingItem : L2CharacterMacroItem = playerMacros.data.find( ( item: L2CharacterMacroItem ) => item.id === macro.id )
            if ( existingItem ) {
                _.pull( playerMacros.data, existingItem )
                this.scheduleDelete( existingItem )
            }
        }

        this.scheduleUpsert( macro )
        this.sendUpdate( playerId )
    }

    getSize( playerId: number ) {
        return this.macros[ playerId ].data.length
    }

    deleteMacro( player: L2PcInstance, macroId: number ) : void {
        let playerMacros = this.macros[ player.getObjectId() ]
        let existingItem : L2CharacterMacroItem = playerMacros.data.find( ( item: L2CharacterMacroItem ) => item.id === macroId )

        if ( existingItem ) {
            _.pull( playerMacros.data, existingItem )
            this.scheduleDelete( existingItem )
        }

        if ( PlayerShortcutCache.deleteById( player, macroId, ShortcutType.MACRO ) ) {
            this.sendUpdate( player.getObjectId() )
        }
    }

    hasMacro( playerId: number, macroId: number ) : boolean {
        let playerMacros = this.macros[ playerId ]
        return playerMacros.data.some( item => item.id === macroId )
    }
}

export const PlayerMacrosCache = new Manager()