import { L2DataApi } from '../../data/interface/l2DataApi'
import { L2BuyList, L2DisabledBuyList } from '../models/buylist/L2BuyList'
import { DataManager } from '../../data/manager'
import { DatabaseManager } from '../../database/manager'
import { L2BuylistsTableItem } from '../../database/interface/BuylistsTableApi'
import { Product } from '../models/buylist/Product'
import _, { DebouncedFunc } from 'lodash'
import { RoaringBitmap32 } from 'roaring'
import { L2Item } from '../models/items/L2Item'
import { ItemManagerCache } from './ItemManagerCache'
import { L2BuylistDataItem, L2BuylistDataProduct } from '../../data/interface/BuylistDataApi'

export const EmptyBuylist : L2BuyList = new L2DisabledBuyList( -1 )

let productId = 0

function createInternalProductId() : number {
    productId++

    return productId
}

class Manager implements L2DataApi {
    lists: { [key: number]: L2BuyList } = {}
    productIdsToUpdate: RoaringBitmap32 = new RoaringBitmap32()
    debounceProductUpdate: DebouncedFunc<() => void>
    products : Record<number, Product> = {}

    constructor() {
        this.debounceProductUpdate = _.debounce( this.runBuyListUpdatesTask.bind( this ), 30000, {
            maxWait: 60000
        } )
    }

    getBuyList( id: number ): L2BuyList {
        return this.lists[ id ]
    }

    async load(): Promise<Array<string>> {
        this.lists = {}
        this.products = {}

        let dataItems : Array<L2BuylistDataItem> = await DataManager.getBuylistData().getAll()

        dataItems.forEach( ( data: L2BuylistDataItem ) => {
            this.lists[ data.id ] = this.createBuyList( data )
        } )

        let records = await DatabaseManager.getBuylists().getAll()

        let currentTime = Date.now()
        records.forEach( ( record: L2BuylistsTableItem ) => {
            let list = this.lists[ record.id ]
            if ( !list ) {
                return
            }

            let product : Product = list.getProductByItemId( record.itemId )
            if ( product && record.restockTime > currentTime && record.count < product.maxCount ) {
                product.count = record.count
                product.futureRestockTime = record.restockTime
                return
            }

            product.restockFully()
        } )

        this.lists[ EmptyBuylist.getListId() ] = EmptyBuylist

        return [
            `BuyListManager : loaded ${ dataItems.length } static lists.`,
            `BuyListManager : created ${ _.size( this.products ) } products`,
            `BuyListManager : refreshed ${ records.length } lists from stored records.`,
            `BuyListManager : scheduled to update ${ this.productIdsToUpdate.size } product records`,
        ]
    }

    async runBuyListUpdatesTask() : Promise<void> {
        let productsToUpdate : Array<Product> = []
        let productsToRemove : Array<Product> = []

        let currentTime = Date.now()
        for ( const productId of this.productIdsToUpdate ) {
            let product = this.products[ productId ]
            if ( !product ) {
                continue
            }

            if ( product.shouldRestock( currentTime ) ) {
                product.restockAll()
            }

            if ( product.count === product.maxCount ) {
                productsToRemove.push( product )
                continue
            }

            if ( product.futureRestockTime > currentTime ) {
                productsToUpdate.push( product )
            }
        }

        this.productIdsToUpdate.clear()

        if ( productsToUpdate.length > 0 ) {
            await DatabaseManager.getBuylists().saveProducts( productsToUpdate )
        }

        if ( productsToRemove.length > 0 ) {
            await DatabaseManager.getBuylists().removeProducts( productsToRemove )
        }
    }

    scheduleProductUpdate( id: number ) : void {
        this.productIdsToUpdate.add( id )
        this.debounceProductUpdate()
    }

    private createBuyList( data: L2BuylistDataItem ): L2BuyList {
        let list = new L2BuyList( data.id )

        let allProducts : Record<number, Product> = {}
        data.products.forEach( ( productData : L2BuylistDataProduct ) : void => {
            let template: L2Item = ItemManagerCache.getTemplate( productData.itemId )
            if ( template ) {
                let product = new Product( createInternalProductId(),
                    data.id,
                    template,
                    productData.price,
                    productData.restockDelay,
                    productData.amount )

                allProducts[ productData.itemId ] = product
                this.products[ product.id ] = product
            }
        } )

        list.itemIdToProduct = allProducts
        list.allowedNpcId = data.npcId
        list.products = new Set<Product>( Object.values( allProducts ) )

        return list
    }
}

export const BuyListManager = new Manager()