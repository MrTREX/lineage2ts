import { DatabaseManager } from '../../database/manager'
import { CharacterNamesCache } from './CharacterNamesCache'

class Manager {
    // TODO : add ability to harvest all object ids that were deleted, so these can be reused
    async removeCharacter( objectId: number ): Promise<void> {

        await DatabaseManager.getCharacterContacts().removeCharacter( objectId )
        await DatabaseManager.getCharacterFriends().removeCharacter( objectId )
        await DatabaseManager.getCharacterHennas().removeCharacter( objectId )
        await DatabaseManager.getCharacterMacros().removeCharacter( objectId )

        await DatabaseManager.getQuestStatesTable().removeCharacter( objectId )
        await DatabaseManager.getCharacterRecipebook().removeCharacter( objectId )
        await DatabaseManager.getCharacterShortcuts().removeCharacter( objectId )

        await DatabaseManager.getCharacterSkills().removeCharacter( objectId )
        await DatabaseManager.getCharacterSkillsSave().removeCharacter( objectId )
        await DatabaseManager.getCharacterSubclasses().removeCharacter( objectId )
        await DatabaseManager.getHeroes().removeCharacter( objectId )

        await DatabaseManager.getOlympiadNobles().removeCharacter( objectId )
        await DatabaseManager.getSevenSigns().removeCharacter( objectId )
        await DatabaseManager.getPets().removeCharacter( objectId )
        await DatabaseManager.getItems().removeCharacter( objectId )

        await DatabaseManager.getMerchantLease().removeCharacter( objectId )
        await DatabaseManager.getCharacterRaidPoints().removeCharacter( objectId )
        await DatabaseManager.getRecommendationBonus().removeCharacter( objectId )
        await DatabaseManager.getCharacterInstanceTime().removeCharacter( objectId )

        await DatabaseManager.getCharacterVariables().removeCharacter( objectId )
        await DatabaseManager.getCharacterTable().removeCharacter( objectId )
        await DatabaseManager.getMessages().removeCharacter( objectId )

        // TODO : consider moving all items to a different character on same account
        await DatabaseManager.getDimensionalTransferItems().removeCharacter( objectId )

        CharacterNamesCache.removePlayer( objectId )
    }
}

export const DeletionManager = new Manager()