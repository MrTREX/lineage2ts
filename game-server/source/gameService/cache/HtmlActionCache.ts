import { HtmlActionPrefixToScope, HtmlActionScope, HtmlActionScopeToPrefix } from '../enums/HtmlActionScope'
import { DataManager } from '../../data/manager'
import _ from 'lodash'

const emptyHtmlActionCache = {}

let currentId: number = Date.now()

function getNextId() {
    currentId++
    return currentId.toString( 36 )
}

class Manager {

    htmlActionOriginObjectIds: { [ playerId: number ]: { [ scope: number ]: number } } = {}
    htmlActionCaches: { [ playerId: number ]: { [ scope: number ]: { [ action: string ]: string } } } = {}

    buildCache( playerId: number, scope: HtmlActionScope, npcObjectId: number, html: string ): string {
        if ( !html ) {
            return
        }

        this.setHtmlActionOriginObjectId( playerId, scope, npcObjectId )

        let actions: { [ bypassId: string ]: Array<string> } = {}
        this.populateBypassActions( scope, html, actions )
        this.populateLinkActions( scope, html, actions )

        return this.applyCache( playerId, scope, html, actions )
    }

    applyCache( playerId: number, scope: HtmlActionScope, html: string, actions: { [ bypassId: string ]: Array<string> } ): string {
        const prefix = HtmlActionScopeToPrefix[ scope ]

        let actionCache = {}
        const obfuscatedHtml: string = _.reduce( actions, ( finalHtml: string, viableActions: Array<string>, bypassId: string ) => {
            viableActions.forEach( ( currentAction: string ) => {

                let obfuscatedId: string = `${ prefix }${ getNextId() }`
                finalHtml = finalHtml.replace( `${ bypassId } ${ currentAction }`, `${ bypassId } ${ obfuscatedId }` )

                actionCache[ obfuscatedId ] = currentAction
            } )

            return finalHtml
        }, html )

        this.addDirectHtmlActions( playerId, scope, actionCache )

        return obfuscatedHtml
    }

    buildCacheWithPath( playerId: number, scope: HtmlActionScope, npcObjectId: number, html: string, htmlPath: string ): string {
        if ( !html ) {
            return
        }

        this.setHtmlActionOriginObjectId( playerId, scope, npcObjectId )

        let actions: { [ bypassId: string ]: Array<string> } = DataManager.getHtmlData().getActions( htmlPath )
        if ( !actions ) {
            actions = {}
            this.populateBypassActions( scope, html, actions )
            this.populateLinkActions( scope, html, actions )
        }

        return this.applyCache( playerId, scope, html, actions )
    }

    buildCacheWithEmptyActions( playerId: number, scope: HtmlActionScope, npcObjectId: number ): void {
        this.setHtmlActionOriginObjectId( playerId, scope, npcObjectId )
        this.addDirectHtmlActions( playerId, scope, emptyHtmlActionCache )
    }

    populateBypassActions( scope: HtmlActionScope, html: string, outcome: { [ bypassId: string ]: Array<string> } ): void {
        let htmlLower = html.toLowerCase()
        let bypassEnd = 0
        let bypassStart = htmlLower.indexOf( '="bypass ', bypassEnd )
        let bypassStartEnd

        while ( bypassStart !== -1 ) {
            bypassStartEnd = bypassStart + 9
            bypassEnd = htmlLower.indexOf( '"', bypassStartEnd )
            if ( bypassEnd === -1 ) {
                break
            }

            let hParameterPosition = htmlLower.indexOf( '-h ', bypassStartEnd )
            let bypass: string
            let bypassId: string

            if ( hParameterPosition !== -1 && hParameterPosition < bypassEnd ) {
                bypass = html.substring( hParameterPosition + 3, bypassEnd ).trim()
                bypassId = 'bypass -h'
            } else {
                bypass = html.substring( bypassStartEnd, bypassEnd ).trim()
                bypassId = 'bypass'
            }

            let firstParameterStart = bypass.indexOf( '$' )
            if ( firstParameterStart !== -1 ) {
                bypass = bypass.substring( 0, firstParameterStart - 1 )
            }

            if ( !outcome[ bypassId ] ) {
                outcome[ bypassId ] = []
            }

            if ( bypass ) {
                outcome[ bypassId ].push( bypass )
            }

            bypassStart = htmlLower.indexOf( '="bypass ', bypassEnd )
        }
    }

    populateLinkActions( scope: HtmlActionScope, html: string, outcome: { [ bypassId: string ]: Array<string> } ) {
        let htmlLower = html.toLowerCase()
        let linkEnd = 0
        let linkStart = htmlLower.indexOf( '="link ', linkEnd )
        let linkStartEnd : number

        while ( linkStart !== -1 ) {
            linkStartEnd = linkStart + 7
            linkEnd = htmlLower.indexOf( '"', linkStartEnd )
            if ( linkEnd === -1 ) {
                break
            }

            let htmlLink = html.substring( linkStartEnd, linkEnd ).trim()
            if ( _.isEmpty( htmlLink ) ) {
                continue
            }

            if ( htmlLink.includes( '..' ) ) {
                continue
            }

            if ( !outcome[ 'link' ] ) {
                outcome[ 'link' ] = []
            }

            outcome[ 'link' ].push( htmlLink )

            linkStart = htmlLower.indexOf( '="link ', linkEnd )
        }
    }

    setHtmlActionOriginObjectId( objectId: number, scope: HtmlActionScope, npcObjectId: number ) {
        _.setWith( this.htmlActionOriginObjectIds, [ objectId, scope ], npcObjectId, Object )
    }

    addDirectHtmlActions( objectId: number, scope: HtmlActionScope, actions: { [ action: string ]: string } ): void {
        _.setWith( this.htmlActionCaches, [ objectId, scope ], actions, Object )
    }

    validateHtmlAction( playerId: number, action: string ): [ number, string ] {
        let scope = HtmlActionPrefixToScope[ action.charAt( 0 ) ]
        let npcObjectId: number = _.get( this.htmlActionOriginObjectIds, [ playerId, scope ] )

        if ( !_.isNumber( npcObjectId ) ) {
            return [ -1, null ]
        }

        let [ decodedAction, encodedAction ] = this.getValidHtmlAction( playerId, scope, action )

        if ( decodedAction ) {
            if ( encodedAction ) {
                return [ npcObjectId, action.replace( encodedAction, decodedAction ) ]
            }

            return [ npcObjectId, decodedAction ]
        }

        return [ -1, null ]
    }

    getValidHtmlAction( playerId: number, scope: number, action: string ): [ string, string ] {
        let decodedAction: string = _.get( this.htmlActionCaches, [ playerId, scope, action ] )

        if ( decodedAction ) {
            return [ decodedAction, null ]
        }

        let index: number = action.indexOf( ' ', 7 )
        if ( index === -1 ) {
            return [ null, null ]
        }

        let encodedAction: string = action.substring( 0, index )
        let completeAction: string = _.get( this.htmlActionCaches, [ playerId, scope, encodedAction ] )

        return [ completeAction, encodedAction ]
    }

    clearHtmlActions( playerId: number, scope: HtmlActionScope ): void {
        _.unset( this.htmlActionCaches, [ playerId, scope ] )
        _.unset( this.htmlActionOriginObjectIds, [ playerId, scope ] )
    }

    // TODO : clear actions on player removing item from inventory to avoid issues with listener item checks before action was generated
    removeActions( playerId: number ): void {
        _.unset( this.htmlActionCaches, playerId )
        _.unset( this.htmlActionOriginObjectIds, playerId )
    }

    // TODO : remove html actions per scope based on specific timer, or possibly all scopes after X amount of minutes has passed from last action generation
    removeHtmlAction( playerId: number, scope: number, action: string ): void {
        _.unset( this.htmlActionCaches, [ playerId, scope, action ] )
    }
}

export const HtmlActionCache = new Manager()