import _ from 'lodash'
import { AllMoveSpeedStats, Stats } from '../models/stats/Stats'
import { ObjectPool } from '../helpers/ObjectPoolHelper'
import { ServerLog } from '../../logger/Logger'

interface StatValues {
    outcome: number
    time: number
}

type StatProperties = { [ name: string ]: StatValues }

const dataFactory = new ObjectPool( 'StatsCache', (): StatValues => {
    return {
        outcome: 0,
        time: 0
    }
}, 500 )


function removeSingleProperty( name: string, properties: StatProperties ): void {
    let values = properties[ name ]
    if ( !values ) {
        return
    }

    dataFactory.recycleValue( values )
    properties[ name ] = null
}

class Cache {
    allStats: { [ id: number ]: StatProperties } = {}

    getStat( id: number, statName: string, updateTime: number ): number {
        let properties = this.allStats[ id ]
        if ( !properties ) {
            return
        }

        let values: StatValues = properties[ statName ]
        if ( !values || values.time !== updateTime ) {
            return
        }

        return values.outcome
    }

    setStat( id: number, statName: string, outcome: number, updateTime: number ): void {
        let properties = this.allStats[ id ]
        if ( !properties ) {
            properties = {}
            this.allStats[ id ] = properties
        }

        let values = properties[ statName ]
        if ( !values ) {
            values = dataFactory.getValue()
        }

        values.outcome = outcome
        values.time = updateTime

        properties[ statName ] = values
    }

    removeStats( id: number, statNames: Set<string> ): void {
        let properties = this.allStats[ id ]
        if ( !properties ) {
            return
        }

        statNames.forEach( ( name: string ) => {
            removeSingleProperty( name, properties )
        } )

        if ( statNames.has( Stats.MOVE_SPEED ) ) {
            this.removeMoveSpeedStats( properties )
        }
    }

    removeStat( id: number, name: string ): void {
        let properties = this.allStats[ id ]
        if ( !properties ) {
            return
        }

        removeSingleProperty( name, properties )
        if ( name === Stats.MOVE_SPEED ) {
            this.removeMoveSpeedStats( properties )
        }
    }

    private removeMoveSpeedStats( properties: StatProperties ): void {
        AllMoveSpeedStats.forEach( ( name: string ) => {
            removeSingleProperty( name, properties )
        } )
    }

    deleteStats( id: number ): void {
        this.removeAllStats( id )
        delete this.allStats[ id ]
    }

    removeAllStats( id: number ): void {
        _.each( this.allStats[ id ], ( values: StatValues, key: string, collection: StatProperties ) => {
            if ( values ) {
                dataFactory.recycleValue( values )
                collection[ key ] = null
            }
        } )
    }

    duplicateStats( fromId: number, toId: number ): void {
        let fromProperties: StatProperties = this.allStats[ fromId ]
        if ( !fromProperties ) {
            return
        }

        let toProperties: StatProperties = this.allStats[ toId ]
        if ( !toProperties ) {
            toProperties = {}
        }

        _.each( fromProperties, ( values: StatValues, key: string ) => {
            if ( !values ) {
                ServerLog.warn( `Unable to copy stat fromId=${fromId}, toId=${toId} for statName=${key}` )
                return
            }

            let copyValues = toProperties[ key ] ?? dataFactory.getValue()

            copyValues[ 0 ] = values[ 0 ]
            copyValues[ 1 ] = values[ 1 ]

            toProperties[ key ] = copyValues
        } )

        this.allStats[ toId ] = toProperties
    }

    hasStats( id: number ): boolean {
        return !!this.allStats[ id ]
    }
}

export const StatsCache = new Cache()