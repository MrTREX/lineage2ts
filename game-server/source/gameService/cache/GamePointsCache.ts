import _, { DebouncedFunc } from 'lodash'
import { L2GamePointItem } from '../../database/interface/GamePointsTableApi'
import { DatabaseManager } from '../../database/manager'
import { ConfigManager } from '../../config/ConfigManager'
import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { L2GamePointPurchasesTableItem } from '../../database/interface/GamePointPurchasesTableApi'

export const enum GamePointType {
    ItemShop = 0,
    PcCafe = 1
}

/*
    Please note that enum values must correspond to configuration properties for character.
 */
export const enum AcquisitionPointType {
    Exp = 'exp',
    Sp = 'sp',
    CraftMp = 'craftMp',
    MobKill = 'mobKill'
}

type AccountGamePoints = Record<Partial<GamePointType>, L2GamePointItem>
type PlayerPointTracking = Record<Partial<GamePointType>, number>
type AccountPurchaseItems = Record<Partial<GamePointType>, Array<L2GamePointPurchasesTableItem>>

class Manager {
    accounts : Record<string, AccountGamePoints> = {}
    purchaseHistory: Record<string, AccountPurchaseItems> = {}
    tracking : Record<number, PlayerPointTracking> = {}

    debouncedPointsWrite: DebouncedFunc<() => void>
    debouncedPurchasesWrite: DebouncedFunc<() => void>
    accountsToUpdate: Set<L2GamePointItem> = new Set<L2GamePointItem>()
    purchasesToInsert: Array<L2GamePointPurchasesTableItem> = []

    constructor() {
        this.debouncedPointsWrite = _.debounce( this.runPointsWrite.bind( this ), 10000, {
            maxWait: 20000
        } )

        this.debouncedPurchasesWrite = _.debounce( this.runPurchasesWrite.bind( this ), 10000, {
            maxWait: 20000
        } )
    }

    async loadAccount( name: string, objectId: number ) : Promise<void> {
        this.createTrackingPoints( objectId )

        let points = this.accounts[ name ]
        if ( !points ) {
            let items = await DatabaseManager.getGamePoints().getPoints( name )
            points = {} as AccountGamePoints

            for ( const item of items ) {
                points[ item.type ] = item
            }

            this.accounts[ name ] = points
        }

        let purchases = this.purchaseHistory[ name ]
        if ( !purchases ) {
            let purchaseItems = await DatabaseManager.getGamePointPurchases().getPurchases( name )

            purchases = {} as AccountPurchaseItems

            for ( const item of purchaseItems ) {
                if ( !purchases[ item.type ] ) {
                    purchases[ item.type ] = []
                }

                purchases[ item.type ].push( item )
            }

            this.purchaseHistory[ name ] = purchases
        }
    }

    private createTrackingPoints( objectId: number ) : void {
        let tracking = this.tracking[ objectId ]
        if ( tracking ) {
            return
        }

        this.tracking[ objectId ] = {} as PlayerPointTracking
    }

    unLoadAccount( name: string ) : void {
        delete this.accounts[ name ]
        delete this.tracking[ name ]
        delete this.purchaseHistory[ name ]

        /*
            When player logs out and logs in we want to make sure that all the data is read fresh from database.
            While server wide store of records may not be needed, shorter interval for debounce
            will contribute more writes than necessary. Hence, invoking save of all records is
            comparable, while may not be optimal.
         */
        if ( this.accountsToUpdate.size > 0 ) {
            this.debouncedPointsWrite.flush()
        }

        if ( this.purchasesToInsert.length > 0 ) {
            this.debouncedPurchasesWrite.flush()
        }
    }

    private runPointsWrite() : Promise<void> {
        if ( this.accountsToUpdate.size === 0 ) {
            return
        }

        let items = Array.from( this.accountsToUpdate )
        this.accountsToUpdate.clear()

        return DatabaseManager.getGamePoints().updatePoints( items )
    }

    private async runPurchasesWrite() : Promise<void> {
        if ( this.purchasesToInsert.length === 0 ) {
            return
        }

        await DatabaseManager.getGamePointPurchases().addPurchases( this.purchasesToInsert )
        this.purchasesToInsert.length = 0
    }

    getPoints( name: string, type: GamePointType ) : number {
        let points = this.accounts[ name ]
        if ( !points ) {
            return 0
        }

        return points[ type ]?.amount ?? 0
    }

    /*
        Points can be positive or negative integer.
     */
    addPoints( name: string, type: GamePointType, amount: number, reason: string ) : void {
        let points = this.accounts[ name ]
        if ( !points ) {
            return
        }

        let item : L2GamePointItem = points[ type ]
        if ( !item ) {
            item = {
                amount: 0,
                name,
                type,
                reason: null,
                lastUpdate: 0
            }

            points[ type ] = item
        }

        let limit = this.getTypeLimit( type )
        item.amount = Math.floor( Math.max( Math.min( limit, item.amount + amount ), 0 ) )
        item.lastUpdate = Date.now()
        item.reason = reason

        this.accountsToUpdate.add( item )
        this.debouncedPointsWrite()
    }

    private getTypeLimit( type: GamePointType ) : number {
        switch ( type ) {
            case GamePointType.PcCafe:
                return ConfigManager.character.getPcCafePointsLimit()

            case GamePointType.ItemShop:
                return ConfigManager.character.getItemShopPointsLimit()
        }

        return 0
    }

    earnPoints( player: L2PcInstance, type: AcquisitionPointType, value: number ) : void {
        if ( !ConfigManager.character.isGamePointsEnabled() || player.unableToEarnGamePoints() ) {
            return
        }

        if ( ConfigManager.character.usePcCafePoints() && ConfigManager.character.getPcCafePointsAcquisition() === type ) {
            let earnedAmount = this.synchronizePoints(
                player,
                GamePointType.PcCafe,
                value,
                ConfigManager.character.getPcCafePointsAcquisitionRate(),
                ConfigManager.character.getPcCafePointsAcquisitionAmount(),
                type )


            if ( earnedAmount !== 0 ) {
                player.onPcPointsAcquired( earnedAmount )
            }
        }

        if ( ConfigManager.character.useItemShopPoints() && ConfigManager.character.getItemShopPointsAcquisition() === type ) {
            let earnedAmount = this.synchronizePoints(
                player,
                GamePointType.ItemShop,
                value,
                ConfigManager.character.getItemShopPointsAcquisitionRate(),
                ConfigManager.character.getItemShopPointsAcquisitionAmount(),
                type )

            if ( earnedAmount !== 0 ) {
                player.onItemShopPointsAcquired( earnedAmount )
            }
        }
    }

    private synchronizePoints(
        player: L2PcInstance,
        type: GamePointType,
        amount: number,
        rate: number,
        pointsPerRate : number,
        reason: string ) : number {
        let updatedPoints = this.getTrackingPoints( player.getObjectId(), type ) + amount
        let remainderPoints = updatedPoints % rate

        this.setTrackingPoints( player.getObjectId(), type, remainderPoints )

        let earnedPoints = Math.floor( ( updatedPoints / rate ) ) * pointsPerRate
        if ( earnedPoints === 0 ) {
            return 0
        }

        this.addPoints( player.getAccountName(), type, earnedPoints, reason )

        return earnedPoints
    }

    private getTrackingPoints( objectId : number, type: GamePointType ) : number {
        return this.tracking[ objectId ][ type ] ?? 0
    }

    private setTrackingPoints( objectId : number, type: GamePointType, amount: number ) : void {
        this.tracking[ objectId ][ type ] = amount
    }

    getCurrentPoints( name: string, type: GamePointType ) : number {
        let points = this.accounts[ name ]
        if ( !points ) {
            return 0
        }

        return points[ type ]?.amount ?? 0
    }

    recordPurchase( player: L2PcInstance, type: GamePointType, productId: number, amount: number, paidPrice: number, category: number ) : void {
        let purchases = this.purchaseHistory[ player.getAccountName() ]
        if ( !purchases ) {
            return
        }

        if ( !purchases[ type ] ) {
            purchases[ type ] = []
        }

        let purchase : L2GamePointPurchasesTableItem = {
            category,
            paidPrice,
            amount,
            accountName: player.getAccountName(),
            productId,
            time: Date.now(),
            type,
            playerId: player.getObjectId()
        }

        purchases[ type ].unshift( purchase )

        this.purchasesToInsert.push( purchase )
        this.debouncedPurchasesWrite()
    }

    getPurchases( name: string, type: GamePointType, maxAmount: number = ConfigManager.character.getItemShopRecentPurchaseLimit() ) : ReadonlyArray<L2GamePointPurchasesTableItem> {
        const accountPurchases = this.purchaseHistory[ name ]
        if ( !accountPurchases ) {
            return []
        }

        const purchases = accountPurchases[ type ]
        if ( !purchases ) {
            return []
        }

        if ( maxAmount > 0 ) {
            let limit = Math.min( maxAmount, 200 )
            return purchases.slice( 0, limit )
        }

        return purchases
    }
}

export const GamePointsCache = new Manager()