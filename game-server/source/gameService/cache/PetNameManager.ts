import { DatabaseManager } from '../../database/manager'
import { ConfigManager } from '../../config/ConfigManager'

class Manager {

    isValidPetName( name: string ): boolean {
        return ConfigManager.character.getPetNameTemplate().test( name ) && !ConfigManager.character.getForbiddenNames().has( name )
    }

    async isNameTaken( name: string ): Promise<boolean> {
        return DatabaseManager.getPets().hasPetName( name )
    }
}

export const PetNameManager = new Manager()