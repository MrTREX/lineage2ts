import { L2Character } from '../models/actor/L2Character'
import { DataManager } from '../../data/manager'
import { GameTime } from './GameTime'

class Manager {
    getBonus( attacker: L2Character, target: L2Character ): number {
        let mod = 100

        if ( ( attacker.getZ() - target.getZ() ) > 50 ) {
            mod += DataManager.getHitConditionBonus().getHighBonus()
        } else if ( ( attacker.getZ() - target.getZ() ) < -50 ) {
            mod += DataManager.getHitConditionBonus().getLowBonus()
        }

        if ( GameTime.isNight() ) {
            mod += DataManager.getHitConditionBonus().getNightBonus()
        }

        if ( attacker.isBehindTarget() ) {
            mod += DataManager.getHitConditionBonus().getBackBonus()
        } else if ( attacker.isInFrontOfTarget() ) {
            mod += DataManager.getHitConditionBonus().getFrontBonus()
        } else {
            mod += DataManager.getHitConditionBonus().getSideBonus()
        }

        // If (mod / 100) is less than 0, return 0, because we can't lower more than 100%.
        return Math.max( mod / 100, 0 )
    }
}

export const HitConditionBonusManager = new Manager()