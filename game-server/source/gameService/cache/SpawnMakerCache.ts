import { L2DataApi } from '../../data/interface/l2DataApi'
import { L2SpawnTerritoryItem } from '../../data/interface/SpawnTerritoriesDataApi'
import { DataManager } from '../../data/manager'
import _ from 'lodash'
import { getRegionCode } from '../enums/L2MapTile'
import perfy from 'perfy'
import { ListenerCache } from './ListenerCache'
import { ListenerRegisterType } from '../enums/ListenerRegisterType'
import { EventType, GeoRegionActivatedEvent } from '../models/events/EventType'
import { SpawnLogicRegistry } from '../models/spawns/SpawnLogicRegistry'
import { ISpawnLogic, SpawnSharedParameters } from '../models/spawns/ISpawnLogic'
import { L2SpawnTerritory } from '../models/spawns/L2SpawnTerritory'
import { L2DefaultSpawn } from '../models/spawns/type/L2DefaultSpawn'
import { L2World } from '../L2World'
import { RegionActivation } from '../models/RegionActivation'
import { ServerLog } from '../../logger/Logger'

class Manager implements L2DataApi {
    territories: Record<string, L2SpawnTerritory>
    territoriesByName: Record<string, L2SpawnTerritory>
    npcSpawns: Record<number, Array<ISpawnLogic>>
    makerParameters: Record<string, SpawnSharedParameters>
    staticRegionSpawns: Record<number, Array<ISpawnLogic>>
    activatedRegions: RegionActivation = new RegionActivation()
    makerToSpawn: Record<string, Array<ISpawnLogic>>
    idToSpawn: Record<string, ISpawnLogic>

    async load(): Promise<Array<string>> {
        perfy.start( 'SpawnMakerCache' )

        ListenerCache.unRegisterGeneralListener( EventType.GeoRegionActivated, this )

        this.territories = _.mapValues( DataManager.getSpawnTerritories().getAll(), ( data: L2SpawnTerritoryItem ) : L2SpawnTerritory => new L2SpawnTerritory( data ) )
        this.territoriesByName = _.keyBy( this.territories, ( territory: L2SpawnTerritory ) : string => territory.data.name )
        this.activatedRegions.clear()
        this.staticRegionSpawns = {}
        this.makerParameters = {}
        this.npcSpawns = {}
        this.makerToSpawn = {}
        this.idToSpawn = {}

        let makers = DataManager.getSpawnLogic().getAll()
        let errors: Array<string> = []
        let counter: number = 0
        let spawnsWithoutMaker : Record<string, number> = {}
        let spawnsWithoutMakerCounter : number = 0

        for ( const item of DataManager.getSpawnNpcData().getAll() ) {
            let maker = makers[ item.makerId ]
            if ( !maker ) {
                errors.push( `SpawnMaker: unable to locate makerId=${ item.makerId } for npcId=${ item.npcId }` )
                continue
            }

            let parameters = this.makerParameters[ item.makerId ]
            if ( !parameters ) {
                parameters = {
                    count: 0
                }

                this.makerParameters[ item.makerId ] = parameters
            }

            let logicConstructor = SpawnLogicRegistry[ maker.logicName ]
            if ( !logicConstructor ) {
                spawnsWithoutMaker[ maker.logicName ] = ( spawnsWithoutMaker[ maker.logicName ] ?? 0 ) + 1
                spawnsWithoutMakerCounter++
                logicConstructor = L2DefaultSpawn
            }

            let spawn = new logicConstructor( item, maker, parameters )

            this.addNpcSpawn( item.npcId, spawn )
            this.addMakerSpawn( maker.logicName, spawn )

            counter++

            for ( const territoryId of maker.spawnTerritoryIds ) {
                let code = this.getTerritoryCode( territoryId )
                if ( code === null ) {
                    errors.push( `SpawnMaker: unable to find linked territoryId=${ territoryId } for makerId=${ item.makerId }` )
                    continue
                }

                this.addRegionSpawn( code, spawn )
            }
        }

        let metrics = perfy.end( 'SpawnMakerCache' )

        ListenerCache.registerListener( ListenerRegisterType.General, EventType.GeoRegionActivated, this, this.onRegionActivation.bind( this ) )

        if ( spawnsWithoutMakerCounter > 0 ) {
            let allMissingMakers : Array<string> = Object.keys( spawnsWithoutMaker ).sort( ( one: string, two: string ) : number => {
                let oneCount = spawnsWithoutMaker[ one ]
                let twoCount = spawnsWithoutMaker[ two ]

                if ( oneCount > twoCount ) {
                    return -1
                }

                if ( oneCount < twoCount ) {
                    return 1
                }

                return 0
            } )

            let lines = allMissingMakers.map( ( name: string ) : string => `${name}(${spawnsWithoutMaker[ name ]})` )
            errors.push( `SpawnMaker: detected ${lines.length} missing makers` )
            errors.push( `SpawnMaker: missing maker logic and amounts - ${ lines.join( ',' )}` )
        }

        return [
            `SpawnMaker: loaded ${ _.size( this.territories ) } territories`,
            `SpawnMaker: loaded ${ _.size( makers ) } spawn makers`,
            `SpawnMaker: loaded ${ counter } npc spawns`,
            `SpawnMaker: prepared ${ _.size( this.staticRegionSpawns ) } on-demand spawn geo-regions`,
            `SpawnMaker: detected ${ spawnsWithoutMakerCounter } spawns without maker logic`,
            `SpawnMaker: loaded in ${ metrics.time } seconds`,
            ...errors,
        ]
    }

    getTerritory( id: string ): L2SpawnTerritory {
        return this.territories[ id ]
    }

    getFirstSpawn( npcId: number ) : ISpawnLogic {
        let spawns = this.npcSpawns[ npcId ]
        if ( spawns ) {
            return spawns[ 0 ]
        }

        return null
    }

    getAllSpawns( npcId: number ) : Array<ISpawnLogic> {
        return this.npcSpawns[ npcId ]
    }

    onRegionActivation( data: GeoRegionActivatedEvent ): void {
        if ( this.hasSpawnedRegion( data.code ) ) {
            return
        }

        let spawns = this.staticRegionSpawns[ data.code ]
        if ( !spawns ) {
            return
        }

        let startTime = Date.now()

        let npcCount : number = 0
        for ( const spawn of spawns ) {
            if ( !spawn.hasSpawned() ) {
                npcCount += spawn.startSpawn()
            }
        }

        this.activatedRegions.markActivated( data.code )

        ServerLog.info( `SpawnMakerCache: activated region X=${ data.x } Y=${ data.y }, created ${npcCount} npcs in ${ Date.now() - startTime } ms` )
    }

    hasSpawnedRegion( code: number ) : boolean {
        return this.activatedRegions.isActivated( code )
    }

    private addNpcSpawn( id: number, spawn: ISpawnLogic ) : void {
        let spawns = this.npcSpawns[ id ]

        if ( !spawns ) {
            spawns = []
            this.npcSpawns[ id ] = spawns
        }

        spawns.push( spawn )

        this.idToSpawn[ spawn.getId() ] = spawn
    }

    private addRegionSpawn( code: number, spawn: ISpawnLogic ) : void {
        let spawnItems = this.staticRegionSpawns[ code ]

        if ( !spawnItems ) {
            spawnItems = []
            this.staticRegionSpawns[ code ] = spawnItems
        }

        spawnItems.push( spawn )
    }

    private addMakerSpawn( name: string, spawn: ISpawnLogic ) : void {
        let spawns = this.makerToSpawn[ name ]

        if ( !spawns ) {
            spawns = []
            this.makerToSpawn[ name ] = spawns
        }

        spawns.push( spawn )
    }

    addDynamicSpawn( spawn : ISpawnLogic ) : void {
        this.addNpcSpawn( spawn.getTemplate().getId(), spawn )

        if ( spawn instanceof L2DefaultSpawn ) {
            let maker = spawn.getMakerData()
            let hasActiveRegion : boolean = false

            for ( const territoryId of maker.spawnTerritoryIds ) {
                let code = this.getTerritoryCode( territoryId )
                if ( code === null ) {
                    continue
                }

                this.addRegionSpawn( code, spawn )

                hasActiveRegion = hasActiveRegion || this.hasSpawnedRegion( code )
            }

            if ( !spawn.hasSpawned() && hasActiveRegion ) {
                spawn.startSpawn()
            }

            return
        }

        let region = L2World.getRegion( spawn.getX(), spawn.getY() )
        if ( region ) {
            this.addRegionSpawn( region.code, spawn )

            if ( !spawn.hasSpawned() && this.hasSpawnedRegion( region.code ) ) {
                spawn.startSpawn()
            }
        }
    }

    getMakerSpawn( name: string ) : Array<ISpawnLogic> {
        return this.makerToSpawn[ name ]
    }

    private getTerritoryCode( territoryId: string ) : number {
        let territory = this.territories[ territoryId ]
        if ( !territory ) {
            return null
        }

        return getRegionCode( territory.data.regionX, territory.data.regionY )
    }

    removeSpawn( spawn: ISpawnLogic ) : void {
        this.removeSpawnFromRegion( spawn )
        this.removeSpawnFromMakers( spawn )
        this.removeSpawnFromNpcs( spawn )

        delete this.idToSpawn[ spawn.getId() ]
    }

    private removeSpawnFromMakers( spawn: ISpawnLogic ) : void {
        let name = spawn.getMakerData().logicName
        if ( !name ) {
            return
        }

        let spawns = this.getMakerSpawn( name )

        if ( !spawns ) {
            return
        }

        this.makerToSpawn[ name ] = _.pull( spawns, spawn )
    }

    private removeSpawnFromNpcs( spawn: ISpawnLogic ) : void {
        let npcId = spawn.getTemplate().getId()
        let spawns = this.getAllSpawns( npcId )

        if ( !spawns ) {
            return
        }

        this.npcSpawns[ npcId ] = _.pull( spawns, spawn )
    }

    private removeSpawnFromRegion( spawn: ISpawnLogic ) : void {
        let region = L2World.getRegion( spawn.getX(), spawn.getY() )
        if ( !region ) {
            return
        }

        let spawns = this.staticRegionSpawns[ region.code ]
        if ( !spawns ) {
            return
        }

        this.staticRegionSpawns[ region.code ] = _.pull( spawns, spawn )
    }

    getRegionSpawns( code: number ) : Array<ISpawnLogic> {
        return this.staticRegionSpawns[ code ]
    }

    getSpawnById( id: string ) : ISpawnLogic {
        return this.idToSpawn[ id ]
    }

    isRegionActiveForTerritoryId( id: string ) : boolean {
        return this.hasSpawnedRegion( this.getTerritoryCode( id ) )
    }

    getTerritoryByName( name: string ) : L2SpawnTerritory {
        return this.territoriesByName[ name ]
    }
}

export const SpawnMakerCache = new Manager()