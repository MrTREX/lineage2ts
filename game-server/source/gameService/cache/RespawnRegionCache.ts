import { L2DataApi } from '../../data/interface/l2DataApi'
import { DataManager } from '../../data/manager'
import { getRegionCode, L2MapTile } from '../enums/L2MapTile'
import _ from 'lodash'
import { L2RespawnPointData } from '../../data/interface/RespawnPointsDataApi'
import { LocationXY } from '../models/LocationProperties'
import { L2Object } from '../models/L2Object'
import { L2World } from '../L2World'

const defaultPoint : L2RespawnPointData = {
    bannedRace: undefined,
    bbs: 0,
    messageId: 0,
    name: '',
    normalPoints: [],
    pvpPoints: [],
    tiles: []
}

class Manager implements L2DataApi {
    mapToPoint: Record<number, L2RespawnPointData> = {}

    async load(): Promise<Array<string>> {

        DataManager.getRespawnPoints().getAll().forEach( ( item: L2RespawnPointData ) => {
            if ( !item.tiles ) {
                return
            }

            item.tiles.forEach( ( tile: LocationXY ) => {
                this.mapToPoint[ getRegionCode( tile.x, tile.y ) ] = item
            } )
        } )

        return [
            `RespawnRegions: loaded tile associations for ${ _.size( this.mapToPoint ) } regions`,
        ]
    }

    private getCodePoint( code: number ): L2RespawnPointData {
        return this.mapToPoint[ code ] ?? defaultPoint
    }

    private createRegionCode( x: number, y: number ) : number {
        let mapRegionX = ( x >> L2MapTile.SizeShift ) + 20
        let mapRegionY = ( y >> L2MapTile.SizeShift ) + 18
        return getRegionCode( mapRegionX, mapRegionY )
    }

    getLocationMessageId( object: L2Object ): number {
        let region = object.getWorldRegion() ?? L2World.getRegion( object.getX(), object.getY() )
        let regionCode = region ? region.code : this.createRegionCode( object.getX(), object.getY() )
        let respawnPoint = this.getCodePoint( regionCode )

        return respawnPoint.messageId
    }

    getLocationMessageIdByCoordinates( x: number, y: number ): number {
        let region = L2World.getRegion( x, y )
        let respawnPoint = this.getCodePoint( region.code )

        return respawnPoint.messageId
    }

    getRespawnPoint( object: L2Object ): L2RespawnPointData {
        let region = object.getWorldRegion()
        if ( region ) {
            return this.getCodePoint( region.code )
        }

        return this.getRespawnPointByCoordinates( object.getX(), object.getY() )
    }

    getRespawnPointByCoordinates( x: number, y: number ): L2RespawnPointData {
        let worldRegion = L2World.getRegion( x, y )
        return this.getCodePoint( worldRegion.code )
    }
}

export const RespawnRegionCache = new Manager()