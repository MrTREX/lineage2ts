import { DatabaseManager } from '../../database/manager'
import { L2DataApi } from '../../data/interface/l2DataApi'
import { DeserializationFormat, RoaringBitmap32, SerializationFormat } from 'roaring'
import _ from 'lodash'
import { L2DatabaseOperations } from '../../database/operations/DatabaseOperations'
import { ServerVariablesManager } from '../variables/ServerVariablesManager'

const enum IdFactoryValues {
    VariableName = 'IDFactoryRecycleV1',
    StorageLimit = 1_000_000 // at 1M values bitmap is approximately 131_209 bytes long
}

/*
    TODO : consider pre-generating ids for initial server run to avoid limit of starting ids from 0x10000000
 */
class Manager implements L2DataApi, L2DatabaseOperations {
    recycleIds: RoaringBitmap32 = new RoaringBitmap32()
    usableIds: RoaringBitmap32 = new RoaringBitmap32()
    currentId: number = 0x10000000

    async load(): Promise<Array<string>> {
        this.usableIds.clear()

        const data = ServerVariablesManager.getValue( IdFactoryValues.VariableName )
        this.currentId = await this.getHighestUsedId()

        if ( data ) {
            this.usableIds.deserialize( data, DeserializationFormat.croaring )
            this.usableIds.runOptimize()
            this.usableIds.shrinkToFit()

            this.currentId = Math.max( this.currentId, this.usableIds.maximum() ) + 1
        } else {
            this.currentId = this.currentId + 1
        }

        return [
            `IdFactory: NextId = ${ this.currentId }`,
            `IdFactory: Restored previously recycled ${ this.usableIds.size } ids.`,
        ]
    }

    storeRecycledIds() {
        if ( this.recycleIds.size > 0 ) {
            ServerVariablesManager.setValue( IdFactoryValues.VariableName, this.recycleIds.serialize( SerializationFormat.croaring ) )
            this.recycleIds.clear()
        }
    }

    getNextId() {
        if ( this.usableIds.size > 0 ) {
            return this.usableIds.shift()
        }

        return this.currentId++
    }

    releaseId( id: number ) {
        if ( this.recycleIds.size >= IdFactoryValues.StorageLimit ) {
            return
        }

        this.recycleIds.add( id )
        this.storeRecycledIds()
    }

    async getHighestUsedId(): Promise<number> {
        return _.max( [
            await DatabaseManager.getCharacterContacts().getHighestId(),
            await DatabaseManager.getClanDataTable().getHighestId(),
            await DatabaseManager.getCharacterTable().getHighestId(),
            await DatabaseManager.getItems().getHighestId(),
            await DatabaseManager.getPets().getHighestId(),
            await DatabaseManager.getItemsOnGround().getHighestId(),
            this.currentId,
        ] )
    }

    shutdown(): Promise<void> {
        this.storeRecycledIds()
        return ServerVariablesManager.runPartialSave()
    }
}

export const IDFactoryCache = new Manager()