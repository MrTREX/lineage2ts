import { ShortcutType } from '../enums/ShortcutType'
import { DatabaseManager } from '../../database/manager'
import { L2ItemInstance } from '../models/items/instance/L2ItemInstance'
import { EtcItemType } from '../enums/items/EtcItemType'
import { ExAutoSoulShot, ExAutoSoulShotStatus } from '../packets/send/ExAutoSoulShot'
import _, { DebouncedFunc } from 'lodash'
import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { L2PlayerShortcutsTableItem } from '../../database/interface/CharacterShortcutsTableApi'
import { ShortCutRegister } from '../packets/send/ShortCutRegister'
import HashArray from 'hasharray'
import { ShortCutDelete } from '../packets/send/ShortCutDelete'

function createIndex( slot: number, page: number ): number {
    return slot + page * 12
}

interface PlayerData {
    indexed: { [ index: number ] : L2PlayerShortcutsTableItem }
    idBased: typeof HashArray
}

class Manager {
    allShortcuts: { [ playerId: number ]: PlayerData } = {}
    debouncedDatabaseWrite: DebouncedFunc<() => void>

    itemsToDelete: Array<L2PlayerShortcutsTableItem> = []
    itemsToUpsert: Array<L2PlayerShortcutsTableItem> = []
    constructor() {
        this.debouncedDatabaseWrite = _.debounce( this.runDatabaseWrite.bind( this ), 10000, {
            maxWait: 20000
        } )
    }

    private async runDatabaseWrite() : Promise<void> {
        if ( this.itemsToDelete.length > 0 ) {
            await DatabaseManager.getCharacterShortcuts().deleteMany( this.itemsToDelete )
            this.itemsToDelete.length = 0
        }

        if ( this.itemsToUpsert.length > 0 ) {
            await DatabaseManager.getCharacterShortcuts().upsertMany( this.itemsToUpsert )
            this.itemsToUpsert.length = 0
        }
    }

    private scheduleDelete( item: L2PlayerShortcutsTableItem ) : void {
        this.itemsToDelete.push( item )
        this.debouncedDatabaseWrite()
    }

    private scheduleUpsert( item: L2PlayerShortcutsTableItem ) : void {
        this.itemsToUpsert.push( item )
        this.debouncedDatabaseWrite()
    }

    async loadPlayer( player: L2PcInstance ): Promise<void> {
        let items: Array<L2PlayerShortcutsTableItem> = await DatabaseManager.getCharacterShortcuts().getAll( player.getObjectId(), player.getClassIndex() )

        let idsToDelete: Array<number> = []
        let playerShortcuts = this.initializePlayer( player.getObjectId() )

        _.each( items, ( data: L2PlayerShortcutsTableItem ) => {
            if ( data.type === ShortcutType.ITEM ) {
                let item: L2ItemInstance = player.getInventory().getItemByObjectId( data.id )
                if ( !item ) {
                    idsToDelete.push( data.id )
                    return
                }

                if ( item.isEtcItem() ) {
                    data.itemReuseGroup = item.getEtcItem().getSharedReuseGroup()
                }
            }

            data.index = createIndex( data.slot, data.page )
            playerShortcuts.idBased.addOne( data )
            playerShortcuts.indexed[ data.index ] = data
        } )

        if ( idsToDelete.length > 0 ) {
            await DatabaseManager.getCharacterShortcuts().deleteShortcutByIds( player.getObjectId(), player.getClassIndex(), idsToDelete, ShortcutType.ITEM )
        }
    }

    async savePlayer( playerId: number ) : Promise<void> {
        let items = this.getShortcuts( playerId )
        if ( !items || items.length === 0 ) {
            return
        }

        return DatabaseManager.getCharacterShortcuts().upsertMany( items )
    }

    initializePlayer( playerId: number ) : PlayerData {
        const playerShortcuts : PlayerData = {
            idBased: new HashArray( 'id' ),
            indexed: {}
        }

        this.allShortcuts[ playerId ] = playerShortcuts

        return playerShortcuts
    }

    unloadPlayer( playerId: number ) : void {
        delete this.allShortcuts[ playerId ]
    }

    getShortcuts( playerId: number ) : Array<L2PlayerShortcutsTableItem> {
        let playerShortcuts = this.allShortcuts[ playerId ]
        if ( !playerShortcuts ) {
            return
        }

        return playerShortcuts.idBased.all
    }

    deleteShortCut( player: L2PcInstance, slot: number, page: number, sendUpdate: boolean ): void {
        let playerShortcuts = this.allShortcuts[ player.getObjectId() ]
        if ( !playerShortcuts ) {
            return
        }

        let index = createIndex( slot, page )
        let data = playerShortcuts.indexed[ index ]
        if ( !data ) {
            return
        }

        delete playerShortcuts.indexed[ index ]
        playerShortcuts.idBased.remove( data )

        this.scheduleDelete( data )

        if ( sendUpdate ) {
            player.sendOwnedData( ShortCutDelete( data.slot ) )

            if ( data.type === ShortcutType.ITEM ) {
                this.sendShotUpdate( player, [ data ] )
            }
        }
    }

    deleteById( player: L2PcInstance, id: number, type: ShortcutType ) : boolean {
        let playerShortcuts = this.allShortcuts[ player.getObjectId() ]
        if ( !playerShortcuts ) {
            return false
        }

        let shortcuts: Array<L2PlayerShortcutsTableItem> = _.filter( playerShortcuts.idBased.getAsArray( id ), ( data: L2PlayerShortcutsTableItem ): boolean => {
            return data.type === type
        } )

        if ( shortcuts.length === 0 ) {
            return false
        }

        shortcuts.forEach( ( item: L2PlayerShortcutsTableItem ) => {
            delete playerShortcuts.indexed[ item.index ]
            playerShortcuts.idBased.remove( item )
            this.scheduleDelete( item )
        } )

        if ( type === ShortcutType.ITEM ) {
            this.sendShotUpdate( player, shortcuts )
        }

        shortcuts.forEach( data => player.sendOwnedData( ShortCutDelete( data.slot ) ) )

        return true
    }

    private sendShotUpdate( player: L2PcInstance, shortcuts: Array<L2PlayerShortcutsTableItem> ): void {
        shortcuts.forEach( ( data: L2PlayerShortcutsTableItem ) => {
            let item: L2ItemInstance = player.getInventory().getItemByObjectId( data.id )

            if ( item
                    && item.isEtcItem()
                    && item.getItemType() === EtcItemType.SHOT
                    && player.removeAutoSoulShot( item.getId() ) ) {
                player.sendOwnedData( ExAutoSoulShot( item.getId(), ExAutoSoulShotStatus.Disabled ) )
            }
        } )

        player.getAutoSoulShot().forEach( ( itemId: number ) => {
            player.sendOwnedData( ExAutoSoulShot( itemId, ExAutoSoulShotStatus.Enabled ) )
        } )
    }

    registerShortcut( player: L2PcInstance, data: L2PlayerShortcutsTableItem ): boolean {
        let playerShortcuts = this.allShortcuts[ player.getObjectId() ]
        if ( !playerShortcuts ) {
            return false
        }

        const index = createIndex( data.slot, data.page )
        let existingData: L2PlayerShortcutsTableItem = playerShortcuts.indexed[ index ]

        if ( existingData ) {
            playerShortcuts.idBased.remove( existingData )
        }

        data.index = index
        playerShortcuts.idBased.addOne( data )
        playerShortcuts.indexed[ index ] = data
        this.scheduleUpsert( data )

        return true
    }

    updateSkillShortcut( player: L2PcInstance, skillId: number, skillLevel: number ): Promise<void> {
        let playerShortcuts = this.allShortcuts[ player.getObjectId() ]
        if ( !playerShortcuts ) {
            return
        }

        _.each( playerShortcuts.idBased.getAsArray( skillId ), ( data: L2PlayerShortcutsTableItem ) : void => {
            if ( data.type === ShortcutType.SKILL ) {
                data.level = skillLevel
                player.sendOwnedData( ShortCutRegister( data ) )

                this.scheduleUpsert( data )
            }
        } )
    }
}

export const PlayerShortcutCache = new Manager()