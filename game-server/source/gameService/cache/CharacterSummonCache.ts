import { L2DataApi } from '../../data/interface/l2DataApi'
import { DatabaseManager } from '../../database/manager'
import { L2PetTableData } from '../../database/interface/PetsTableApi'
import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { L2ItemInstance } from '../models/items/instance/L2ItemInstance'
import { L2PetData } from '../models/L2PetData'
import { DataManager } from '../../data/manager'
import { L2NpcTemplate } from '../models/actor/templates/L2NpcTemplate'
import { L2PetInstance } from '../models/actor/instance/L2PetInstance'
import { PetItemList } from '../packets/send/PetItemList'
import { L2World } from '../L2World'
import { L2ServitorInstance } from '../models/actor/instance/L2ServitorInstance'
import { L2ServitorTableData } from '../../database/interface/ServitorsTableApi'
import { SkillCache } from './SkillCache'
import { Skill } from '../models/Skill'
import { Race } from '../enums/Race'
import { ConfigManager } from '../../config/ConfigManager'
import { PetInfoAnimation } from '../packets/send/PetInfo'
import { GeneralHelper } from '../helpers/GeneralHelper'
import _ from 'lodash'
import { L2Npc } from '../models/actor/L2Npc'
import { SystemMessageBuilder } from '../packets/send/SystemMessage'
import { SystemMessageIds } from '../packets/SystemMessageIdValues'
import { MagicSkillUse } from '../packets/send/MagicSkillUse'
import { recordSummonViolation } from '../helpers/PlayerViolations'
import { RoaringBitmap32 } from 'roaring'
import { ItemDefinition } from '../interface/ItemDefinition'
import { PacketDispatcher } from '../PacketDispatcher'
import { MagicSkillLaunched } from '../packets/send/MagicSkillLaunched'
import { L2Summon } from '../models/actor/L2Summon'

class Manager implements L2DataApi {
    playersWithPet: RoaringBitmap32
    playersWithServitor: RoaringBitmap32

    async load(): Promise<Array<string>> {
        let petPlayerIds: Array<number> = await DatabaseManager.getPets().getRestoredPlayerIds()
        this.playersWithPet = new RoaringBitmap32( petPlayerIds )

        let servitorPlayerIds: Array<number> = await DatabaseManager.getServitors().getRestoredPlayerIds()
        this.playersWithServitor = new RoaringBitmap32( servitorPlayerIds )

        return [
            `CharacterSummonCache: has ${ this.playersWithServitor.size } players with respawned servitors.`,
            `CharacterSummonCache: has ${ this.playersWithPet.size } players with respawned pets.`,
        ]
    }

    hasRespawnedServitor( playerId: number ): boolean {
        return this.playersWithServitor.has( playerId )
    }

    hasRespawnedPet( playerId: number ): boolean {
        return this.playersWithPet.has( playerId )
    }

    removeRestoredServitor( playerId: number ): void {
        this.playersWithServitor.remove( playerId )
    }

    removeRestoredPet( playerId: number ): void {
        this.playersWithPet.remove( playerId )
    }

    async restoreRespawnedPet( player: L2PcInstance ): Promise<L2PetInstance> {
        let data: L2PetTableData = await DatabaseManager.getPets().getRestoredPet( player.getObjectId() )
        if ( !data ) {
            return
        }

        let item: L2ItemInstance = player.getInventory().getItemByObjectId( data.controlId )
        if ( !item ) {
            return
        }

        let petData: L2PetData = DataManager.getPetData().getPetDataByItemId( item.getId() )
        if ( !petData ) {
            return
        }

        let template: L2NpcTemplate = DataManager.getNpcData().getTemplate( petData.getNpcId() )
        if ( !template ) {
            return
        }

        let pet: L2PetInstance = await this.createPet( data, template, player, item )
        if ( !pet ) {
            return
        }

        await this.spawnPet( pet, petData, player, item )

        return pet
    }

    async restorePet( petData: L2PetData, player: L2PcInstance, item: L2ItemInstance ): Promise<L2PetInstance> {
        let template: L2NpcTemplate = DataManager.getNpcData().getTemplate( petData.getNpcId() )
        if ( !template ) {
            return
        }

        let data: L2PetTableData = await DatabaseManager.getPets().getPet( player.getObjectId(), item.getObjectId() )
        let pet = await this.createPet( data, template, player, item )

        await this.spawnPet( pet, petData, player, item )
        if ( !data ) {
            await DatabaseManager.getPets().createPet( pet )
        } else {
            await DatabaseManager.getPets().setRestored( pet, true )
        }

        return pet
    }

    async restoreServitor( player: L2PcInstance ): Promise<L2ServitorInstance> {
        let data: L2ServitorTableData = await DatabaseManager.getServitors().getServitor( player.getObjectId() )
        if ( !data ) {
            return
        }

        let skill: Skill = SkillCache.getSkill( data.summonSkillId, player.getSkillLevel( data.summonSkillId ) )
        if ( !skill ) {
            await DatabaseManager.getServitors().removeServitor( player.getObjectId() )
            return
        }

        await skill.applyEffects( player, player, false, false, true, 0 )

        if ( !player.hasServitor() ) {
            return
        }

        let summon: L2ServitorInstance = player.getSummon() as L2ServitorInstance

        summon.setCurrentHp( data.hp )
        summon.setCurrentMp( data.mp )
        summon.setLifeTimeRemaining( data.remainingTime )

        return summon
    }

    private async spawnPet( pet: L2PetInstance, data: L2PetData, player: L2PcInstance, item: L2ItemInstance ): Promise<void> {
        if ( data.isSyncLevel() && pet.getLevel() !== player.getLevel() ) {
            pet.getStat().setLevel( player.getLevel() )
            pet.setExp( pet.getExpForLevel( player.getLevel() ) )
        } else {
            pet.setExp( pet.getExpForThisLevel() )
        }

        L2World.addPet( player.getObjectId(), pet )
        pet.setShowSummonAnimation( true )

        pet.setRunning()
        pet.setTitle( player.getName() )
        player.setPet( pet )
        await item.setEnchantLevel( pet.getLevel() )

        pet.setHeading( GeneralHelper.calculateHeadingFromLocations( pet, player ) )
        pet.spawnMe( ...this.getSpawnCoordinates( player ) )

        pet.startFeeding()
        pet.setAIFollow( true )
        pet.sendOwnedData( PetItemList( pet ) )
        pet.broadcastStatusUpdate()

        this.playersWithPet.add( player.getObjectId() )
    }

    private async createPet( petData: L2PetTableData, template: L2NpcTemplate, player: L2PcInstance, controlItem: L2ItemInstance ): Promise<L2PetInstance> {
        let pet: L2PetInstance = petData ? new L2PetInstance( template, player, controlItem, petData.level ) : L2PetInstance.fromTemplate( template, player, controlItem )
        pet.setName( petData && petData.name ? petData.name : '' )

        await pet.loadTemplateSkills( pet.getTemplate() )

        if ( ConfigManager.character.summonStoreSkillCooltime() ) {
            await pet.restoreEffects()
        }

        if ( petData ) {
            let expValue = petData.exp

            let data = DataManager.getPetData().getPetLevelData( pet.getId(), pet.getLevel() )
            if ( data && expValue < data.getPetMaxExp() ) {
                expValue = data.getPetMaxExp()
            }

            pet.setExp( expValue )
            pet.setSp( petData.sp )
            pet.getStatus().setCurrentHp( petData.hp )
            pet.getStatus().setCurrentMp( petData.mp )
            pet.setCurrentFeed( petData.feed )

            if ( petData.hp < 1 ) {
                pet.setIsDead( true )
                pet.stopHpMpRegeneration()
            }
        } else {
            pet.getStatus().setCurrentHp( pet.getMaxHp() )
            pet.getStatus().setCurrentMp( pet.getMaxMp() )
            pet.getStatus().setCurrentCp( pet.getMaxCp() )
            pet.setCurrentFeed( pet.getMaxFeed() )
        }

        await pet.getInventory().restore()
        return pet
    }

    async createServitor( npcId: number, expMultiplier: number, lifeTime: number, consumeItem: ItemDefinition, consumeInterval: number, skillId: number, player: L2PcInstance ): Promise<void> {
        let template: L2NpcTemplate = DataManager.getNpcData().getTemplate( npcId )
        let servitor: L2ServitorInstance = new L2ServitorInstance( template, player )
        let consumeItemInterval = ( consumeInterval > 0 ? consumeInterval : ( template.getRace() !== Race.SIEGE_WEAPON ? 240 : 60 ) ) * 1000

        servitor.setName( template.getName() )
        servitor.setTitle( player.getName() )
        servitor.setReferenceSkill( skillId )
        servitor.setExpMultiplier( expMultiplier )

        servitor.setLifeTime( lifeTime )
        servitor.setItemConsume( consumeItem )
        servitor.setItemConsumeInterval( consumeItemInterval )
        await servitor.loadTemplateSkills( servitor.getTemplate() )

        if ( ConfigManager.character.summonStoreSkillCooltime() ) {
            await servitor.restoreEffects()
        }

        await servitor.updateAndBroadcastStatus( PetInfoAnimation.None )

        if ( servitor.getLevel() >= ConfigManager.character.getMaxPetLevel() ) {
            servitor.getStat().setExp( DataManager.getExperienceData().getExpForLevel( ConfigManager.character.getMaxPetLevel() - 1 ) )
        } else {
            servitor.getStat().setExp( DataManager.getExperienceData().getExpForLevel( servitor.getLevel() % ConfigManager.character.getMaxPetLevel() ) )
        }

        servitor.setCurrentHp( servitor.getMaxHp() )
        servitor.setCurrentMp( servitor.getMaxMp() )
        servitor.setHeading( player.getHeading() )
        servitor.setRunning()

        player.setPet( servitor )

        servitor.setHeading( GeneralHelper.calculateHeadingFromLocations( servitor, player ) )
        servitor.spawnMe( ...this.getSpawnCoordinates( player ) )
    }

    /*
        TODO : use spawn geometry
        - ensure that spawn point has same Z value as player to avoid spawn dropping and dying
        - geometry does not have to have too many points, however there can be a need for two sets of points: front and back
     */
    getSpawnCoordinates( player: L2PcInstance ): [ number, number, number ] {
        let angle: number = ( GeneralHelper.convertHeadingToDegree( player.getHeading() ) + _.random( -30, 30 ) + 360 ) % 360

        if ( player.isUnderAttack() ) {
            angle = ( angle + 180 ) % 360
        }

        return GeneralHelper.getAngleLocation( player, angle, 50 )
    }

    async restoreAndEvolvePet( player: L2PcInstance, npc: L2Npc, consumedItemId: number = 0, gainedItemId: number = 0, petLevel: number = 0 ): Promise<boolean> {
        if ( !consumedItemId || !gainedItemId || !petLevel ) {
            return false
        }

        let item: L2ItemInstance = player.getInventory().getItemByItemId( consumedItemId )
        if ( !item ) {
            return false
        }

        let oldPetLevel = item.getEnchantLevel()
        if ( oldPetLevel < petLevel ) {
            oldPetLevel = petLevel
        }

        let oldData: L2PetData = DataManager.getPetData().getPetDataByItemId( consumedItemId )
        if ( !oldData ) {
            return false
        }

        let petData: L2PetData = DataManager.getPetData().getPetDataByItemId( gainedItemId )
        if ( !petData ) {
            return false
        }

        let npcId = petData.getNpcId()
        if ( npcId === 0 ) {
            return false
        }

        let removedItem: L2ItemInstance = await player.getInventory().destroyItem( item, item.getCount(), 0, 'PetEvolution.restorePet' )
        let removedItemMessage = new SystemMessageBuilder( SystemMessageIds.S1_DISAPPEARED )
                .addItemInstanceName( removedItem )
                .getBuffer()
        player.sendOwnedData( removedItemMessage )

        let addedItem: L2ItemInstance = await player.getInventory().addItem( gainedItemId, 1, npc.getObjectId(), 'PetEvolution.restorePet' )
        let petSummon: L2PetInstance = await CharacterSummonCache.restorePet( petData, player, addedItem )
        if ( !petSummon ) {
            return false
        }

        await petSummon.addDirectXp( petSummon.getExpForLevel( oldPetLevel ) )

        petSummon.setCurrentHp( petSummon.getMaxHp() )
        petSummon.setCurrentMp( petSummon.getMaxMp() )
        petSummon.setCurrentFeed( petSummon.getMaxFeed() )
        petSummon.setTitle( player.getName() )

        petSummon.setRunning()
        await petSummon.storeMe()
        player.setPet( petSummon )
        petSummon.spawnMe( ...this.getSpawnCoordinates( player ) )

        petSummon.startFeeding()
        await addedItem.setEnchantLevel( petSummon.getLevel() )

        player.sendOwnedData( MagicSkillUse( npc.getObjectId(), petSummon.getObjectId(), 2046, 1, 1000, 600000 ) )
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SUMMON_A_PET ) )

        player.broadcastUserInfo()

        setTimeout( this.runSummonEvolveFinalizer, 900, player.getObjectId(), petSummon.getObjectId() )
        await DatabaseManager.getPets().deleteByControlId( removedItem.getObjectId() )
        return true
    }

    async upgradePet( player: L2PcInstance, npc: L2Npc, consumedItemId: number = 0, gainedItemId: number = 0, petLevel: number = 0 ): Promise<boolean> {
        if ( !consumedItemId || !gainedItemId || !petLevel ) {
            return false
        }

        if ( !player.hasPet() ) {
            return false
        }

        let currentPet: L2PetInstance = player.getSummon() as L2PetInstance
        if ( currentPet.isAlikeDead() ) {
            await recordSummonViolation( player.getObjectId(), 'ed9eaec5-cfb9-4ab0-87bd-6eff050f8701', 'Player upgrades dead pet', currentPet.getId(), currentPet.getObjectId() )
            return false
        }

        let petExp = currentPet.getExp()
        let oldName = currentPet.getName()
        let oldX = currentPet.getX()
        let oldY = currentPet.getY()
        let oldZ = currentPet.getZ()

        let oldData: L2PetData = DataManager.getPetData().getPetDataByItemId( consumedItemId )

        if ( !oldData ) {
            return false
        }

        let oldNpcID = oldData.getNpcId()
        if ( ( currentPet.getLevel() < petLevel ) || ( currentPet.getId() !== oldNpcID ) ) {
            return false
        }

        let petData: L2PetData = DataManager.getPetData().getPetDataByItemId( gainedItemId )

        if ( !petData ) {
            return false
        }

        let npcId = petData.getNpcId()
        if ( npcId === 0 ) {
            return false
        }

        await currentPet.unSummon( player )
        await currentPet.destroyControlItem( player, true )

        let item = await player.getInventory().addItem( gainedItemId, 1, npc.getObjectId(), 'CharacterSummonCache.upgradePet' )
        let petSummon: L2PetInstance = await CharacterSummonCache.restorePet( petData, player, item )

        if ( !petSummon ) {
            return false
        }

        let maxExp = petSummon.getExpForLevel( petLevel )
        if ( petExp < maxExp ) {
            petExp = maxExp
        }

        await petSummon.addDirectXp( petExp )
        petSummon.setCurrentHp( petSummon.getMaxHp() )
        petSummon.setCurrentMp( petSummon.getMaxMp() )
        petSummon.setCurrentFeed( petSummon.getMaxFeed() )

        petSummon.setTitle( player.getName() )
        petSummon.setName( oldName )
        petSummon.setXYZ( oldX, oldY, oldZ )
        petSummon.spawnMe( ...this.getSpawnCoordinates( player ) )

        petSummon.startFeeding()
        await item.setEnchantLevel( petSummon.getLevel() )

        player.sendOwnedData( MagicSkillUse( npc.getObjectId(), petSummon.getObjectId(), 2046, 1, 1000, 600000 ) )
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SUMMON_A_PET ) )

        setTimeout( this.runSummonEvolveFinalizer, 900, player.getObjectId(), petSummon.getObjectId() )
        player.broadcastUserInfo()

        return true
    }

    private runSummonEvolveFinalizer( playerId: number, summonId: number ): void {
        let player = L2World.getPlayer( playerId )
        if ( !player || !player.isOnline() ) {
            return
        }

        PacketDispatcher.sendOwnedData( playerId, MagicSkillLaunched( playerId, 2046, 1 ) )

        let summon = L2World.getObjectById( summonId ) as L2Summon
        if ( !summon ) {
            return
        }

        summon.setAIFollow( true )
        summon.setShowSummonAnimation( false )
    }
}

export const CharacterSummonCache = new Manager()