import { ServerVariablesManager } from '../variables/ServerVariablesManager'
import { ConfigManager } from '../../config/ConfigManager'
import { L2DataApi } from '../../data/interface/l2DataApi'
import { ListenerCache } from './ListenerCache'
import { CycleStepChangeEvent, EventType } from '../models/events/EventType'
import { EventPoolCache } from './EventPoolCache'
import _ from 'lodash'
import { CycleType } from '../enums/CycleType'
import { DataManager } from '../../data/manager'
import { L2CycleStepDataItem } from '../../data/interface/CycleStepDataApi'

const configurationName: string = 'CycleStepConfiguration'

interface CycleConfiguration {
    points: number
    step: number
    isLocked: boolean
}

type ConfigurationBundle = Record<number, CycleConfiguration>

const defaultConfiguration : ConfigurationBundle = {
    [ CycleType.Hellbound ]: {
        points: 0,
        step: 1,
        isLocked: false,
    },
    [ CycleType.SeedOfDestruction ]: {
        points: 0,
        step: 1,
        isLocked: false,
    },
    [ CycleType.SeedOfInfinity ]: {
        points: 0,
        step: 1,
        isLocked: false,
    },
    [ CycleType.SOABistakon ]: {
        points: 0,
        step: 1,
        isLocked: false,
    },
    [ CycleType.SOACokracon ]: {
        points: 0,
        step: 1,
        isLocked: false,
    },
    [ CycleType.SOAReptilikon ]: {
        points: 0,
        step: 1,
        isLocked: false,
    },
}

interface CycleStepProperties {
    data: Record<number, L2CycleStepDataItem>
    maxStepId: number
}

class Manager implements L2DataApi {
    configuration: ConfigurationBundle
    stepData: Record<number, CycleStepProperties>

    async load(): Promise<Array<string>> {
        this.loadData()
        this.initializeConfiguration()

        let currentConfiguration = this.getConfiguration()
        let descriptions : Array<string> = Object.keys( currentConfiguration ).map( ( key: string ): string => {
            let type : CycleType = CycleType[ key ]
            if ( !type ) {
                delete currentConfiguration[ key ]
            }

            let value : CycleConfiguration = currentConfiguration[ key ]
            return `CycleStep Manager : loaded ${value.isLocked ? 'locked ' : ''}${ type } with ${ value.step } step and ${ value.points } points`
        } )

        return [
            `CycleStep Manager : loaded data with ${ _.size( this.stepData )} cycle types`,
            ...descriptions
        ]
    }

    loadData() : void {
        this.stepData = DataManager.getStepCycleData().getAll().reduce( ( allData: Record<number, CycleStepProperties>, item: L2CycleStepDataItem ) : Record<number, CycleStepProperties> => {
            let cycleData = allData[ item.cycleId ]
            if ( !cycleData ) {
                cycleData = {
                    data: {},
                    maxStepId: 0
                }

                allData[ item.cycleId ] = cycleData
            }

            cycleData.data[ item.stepId ] = item
            cycleData.maxStepId = Math.max( cycleData.maxStepId ,item.stepId )

            return allData
        }, {} )
    }

    initializeConfiguration(): void {
        /*
            In order to provide extendable configuration we merge existing values with
            default properties we use, ensuring that if names change we can still get
            correct values (even if old ones did not get migrated properly).
         */
        let existingConfiguration = this.getConfiguration() ?? {}

        this.configuration = {
            ...defaultConfiguration,
            ...existingConfiguration
        }

        ServerVariablesManager.setValue( configurationName, this.configuration )
    }

    private getConfiguration(): ConfigurationBundle {
        return this.configuration
    }

    saveConfiguration(): void {
        ServerVariablesManager.setValue( configurationName, this.getConfiguration() )
    }

    isLocked( type: CycleType ): boolean {
        return this.getConfiguration()[ type ].isLocked
    }

    setStep( type: CycleType, step: number ): void {
        let configuration = this.getConfiguration()[ type ]

        if ( !configuration ) {
            return
        }

        let previousStep = configuration.step
        configuration.step = Math.max( 1, Math.min( step, 100 ) )

        // TODO : adjust points to minimum required per current step

        this.saveConfiguration()

        if ( ListenerCache.hasGeneralListener( EventType.CycleStepChange ) ) {
            let data = EventPoolCache.getData( EventType.CycleStepChange ) as CycleStepChangeEvent

            data.previousStep = previousStep
            data.nextStep = step
            data.type = type

            ListenerCache.sendGeneralEvent( EventType.CycleStepChange, data )
        }
    }

    getPoints( type: CycleType ): number {
        return this.getConfiguration()[ type ].points
    }

    incrementPoints( type: CycleType, points: number ): void {
        if ( this.isLocked( type ) ) {
            return
        }

        let configuration = this.getConfiguration()[ type ]
        if ( !configuration ) {
            return
        }

        configuration.points = Math.max( 0, this.getAdjustedPoints( type, points ) + configuration.points )
    }

    // TODO : use step data to get max points possible
    private getAdjustedPoints( type: CycleType, points: number ) : number {
        switch ( type ) {
            case CycleType.Hellbound:
                return Math.floor( points * ( points > 0 ? ConfigManager.rates.getHellboundPointsIncreaseMultiplier() : ConfigManager.rates.getHellboundPointsDecreaseMultiplier() ) )

            default:
                return points
        }
    }

    getStep( type: CycleType ) : number {
        return this.getConfiguration()[ type ].step
    }
}

export const CycleStepManager = new Manager()