import { DatabaseManager } from '../../database/manager'
import { CharacterNamesCache } from './CharacterNamesCache'
import _ from 'lodash'
import { PacketDispatcher } from '../PacketDispatcher'
import { SystemMessageBuilder, SystemMessageHelper } from '../packets/send/SystemMessage'
import { SystemMessageIds } from '../packets/SystemMessageIdValues'

class Manager {
    contacts : { [ playerId: number ] : Array<number> } = {}

    async loadPlayer( playerId: number ) : Promise<void> {
        let playerIds: Array<number> = await DatabaseManager.getCharacterContacts().getContacts( playerId )
        await CharacterNamesCache.prefetchNames( playerIds )

        this.contacts[ playerId ] = _.filter( playerIds, ( currentId: number ) : boolean => {
            return !!CharacterNamesCache.getCachedNameById( currentId )
        } )
    }

    unloadPlayer( playerId: number ) : void {
        delete this.contacts[ playerId ]
    }

    async add( playerId: number, name: string ): Promise<boolean> {
        let otherPlayerId: number = await CharacterNamesCache.getIdByName( name )
        if ( !_.isNumber( otherPlayerId ) ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.NAME_S1_NOT_EXIST_TRY_ANOTHER_NAME )
                    .addString( name )
                    .getBuffer()
            PacketDispatcher.sendOwnedData( playerId, packet )
            return false
        }

        if ( playerId === otherPlayerId ) {
            PacketDispatcher.sendOwnedData( playerId, SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_ADD_YOUR_NAME_ON_CONTACT_LIST ) )
            return false
        }

        let contactIds : Array<number> = this.contacts[ playerId ]

        if ( !contactIds || contactIds.length >= 100 ) {
            PacketDispatcher.sendOwnedData( playerId, SystemMessageBuilder.fromMessageId( SystemMessageIds.CONTACT_LIST_LIMIT_REACHED ) )
            return false
        }

        if ( contactIds.includes( otherPlayerId ) ) {
            PacketDispatcher.sendOwnedData( playerId, SystemMessageBuilder.fromMessageId( SystemMessageIds.NAME_ALREADY_EXIST_ON_CONTACT_LIST ) )
            return false
        }

        await DatabaseManager.getCharacterContacts().addContact( playerId, otherPlayerId )
        this.contacts[ playerId ].push( otherPlayerId )

        let packet = new SystemMessageBuilder( SystemMessageIds.S1_SUCCESSFULLY_ADDED_TO_CONTACT_LIST )
                .addString( name )
                .getBuffer()
        PacketDispatcher.sendOwnedData( playerId, packet )
        return true
    }

    async remove( playerId: number, name: string ): Promise<void> {
        let otherPlayerId = await CharacterNamesCache.getIdByName( name )

        if ( !_.isNumber( otherPlayerId ) ) {
            PacketDispatcher.sendOwnedData( playerId, SystemMessageHelper.sendString( `Player with name ${ name } no longer exists!` ) )
            return
        }

        let contactIds : Array<number> = this.contacts[ playerId ]
        if ( !contactIds || !contactIds.includes( otherPlayerId ) ) {
            PacketDispatcher.sendOwnedData( playerId, SystemMessageBuilder.fromMessageId( SystemMessageIds.NAME_NOT_REGISTERED_ON_CONTACT_LIST ) )
            return
        }

        _.pull( this.contacts[ playerId ], otherPlayerId )

        await DatabaseManager.getCharacterContacts().removeContact( playerId, otherPlayerId )

        let packet = new SystemMessageBuilder( SystemMessageIds.S1_SUCCESFULLY_DELETED_FROM_CONTACT_LIST )
                .addString( name )
                .getBuffer()
        PacketDispatcher.sendOwnedData( playerId, packet )
    }

    getContacts( playerId: number ) : Array<number> {
        return this.contacts[ playerId ]
    }
}

export const PlayerContactCache = new Manager()