import { EventType, EventTypeData } from '../models/events/EventType'
import { EventObjectFactory, SupportedEventTypes } from '../models/events/EventObjectProducers'
import { ConfigManager } from '../../config/ConfigManager'
import _ from 'lodash'
import { ObjectPool } from '../helpers/ObjectPoolHelper'
import { ServerLog } from '../../logger/Logger'

const defaultStartSize = 2

class Manager {
    events: { [ type: number ]: ObjectPool<EventTypeData> }
    isLoggingEnabled: boolean = false

    constructor() {
        this.events = _.mapValues( SupportedEventTypes, ( method: EventObjectFactory, key: string ) => {
            return new ObjectPool<EventTypeData>( `SupportedEventTypes - ${ key }`, method, defaultStartSize )
        } )
    }

    // TODO : convert all usages of event data to use method, even if not all event type objects are cached
    getData( type: EventType ): EventTypeData {
        if ( !this.events[ type ] ) {
            return {}
        }

        return this.events[ type ].getValue()
    }

    recycleData( type: EventType, data: EventTypeData ): void {
        if ( this.isLoggingEnabled && ConfigManager.diagnostic.showEventTypeMessages() ) {
            ServerLog.debug( `EventPoolCache: recycling event EventType ${type}` )
        }

        if ( !this.events[ type ] ) {
            return
        }

        this.events[ type ].recycleValue( data )
    }

    setLogging( value: boolean ) : void {
        this.isLoggingEnabled = value
    }
}

export const EventPoolCache = new Manager()