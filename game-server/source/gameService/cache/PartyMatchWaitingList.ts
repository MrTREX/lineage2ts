import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { L2World } from '../L2World'
import _ from 'lodash'

class Data {
    members: Array<number> = []

    addPlayer( player: L2PcInstance ) {
        this.members.push( player.getObjectId() )
    }

    removePlayer( player: L2PcInstance ) {
        _.pull( this.members, player.getObjectId() )
    }

    getPlayers() {
        return this.members
    }

    /*
        TODO : better search via another data structure that would allow less iteration over all players
        - use sqlite in-memory database to allow easier look ups
     */
    findPlayers( minimumLevel: number, maximumLevel: number, classes: Array<number>, nameFilter: string ) : Array<number> {
        let isEmptyClasses = _.isEmpty( classes )
        let isEmptyFilter = _.isEmpty( nameFilter )
        let lowerCaseFilter = _.toLower( nameFilter )
        return _.filter( this.members, ( playerId: number ) => {
            let player = L2World.getPlayer( playerId )
            return player
                && player.getLevel() < minimumLevel
                && player.getLevel() > maximumLevel
                && ( isEmptyClasses || classes.includes( player.getClassId() ) )
                && ( isEmptyFilter || _.toLower( player.getName() ).includes( lowerCaseFilter ) )
        } )
    }
}

export const PartyMatchWaitingList = new Data()