import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { L2World } from '../L2World'
import { SystemMessageBuilder } from '../packets/send/SystemMessage'
import { SystemMessageIds } from '../packets/SystemMessageIdValues'
import { ExClosePartyRoom } from '../packets/send/ExClosePartyRoom'
import { ExManagePartyRoomMember, PartyRoomMemberOperation } from '../packets/send/ExManagePartyRoomMember'
import { PacketDispatcher } from '../PacketDispatcher'
import { PartyMatchDetail } from '../packets/send/PartyMatchDetail'
import { ExPartyRoomMember, ExPartyRoomMemberOperation } from '../packets/send/ExPartyRoomMember'
import _ from 'lodash'
import { RespawnRegionCache } from './RespawnRegionCache'

// TODO : consider object pool
export interface PartyMatchRoom {
    id: number
    title: string
    lootType: number
    minimumLevel: number
    maximumLevel: number
    maximumMembers: number
    members: Set<number>
    leaderId: number
}

class Manager {
    currentId: number = 1
    rooms: { [ roomId : number ] : PartyMatchRoom } = {}
    playerMapping: { [ playerId: number ]: PartyMatchRoom } = {}

    getPlayerRoom( playerId: number ): PartyMatchRoom {
        return this.playerMapping[ playerId ]
    }

    deleteRoomById( id: number ): void {
        let room: PartyMatchRoom = this.getRoom( id )
        if ( !room ) {
            return
        }

        let manager = this
        room.members.forEach( ( memberId: number ) => {
            delete manager.playerMapping[ memberId ]

            let player = L2World.getPlayer( memberId )
            if ( !player ) {
                return
            }

            player.sendOwnedData( ExClosePartyRoom() )
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.PARTY_ROOM_DISBANDED ) )
            player.broadcastUserInfo()
        } )

        delete this.rooms[ room.id ]
    }

    getRoom( id: number ): PartyMatchRoom {
        return _.find( this.rooms, ( room: PartyMatchRoom ) => room.id === id )
    }

    getAvailableRooms( locationId: number, useLevel: boolean = false, playerLevel: number = 0 ): Array<PartyMatchRoom> {
        let manager = this
        return Object.values( this.rooms ).reduce( ( allRooms: Array<PartyMatchRoom>, currentRoom: PartyMatchRoom ): Array<PartyMatchRoom> => {
            if ( locationId > 0 && locationId !== manager.getRoomLocation( currentRoom ) ) {
                return allRooms
            }

            if ( useLevel && ( playerLevel < currentRoom.minimumLevel || playerLevel > currentRoom.maximumLevel ) ) {
                return allRooms
            }

            allRooms.push( currentRoom )

            return allRooms
        }, [] )
    }

    makeLeader( playerId: number ): void {
        let room: PartyMatchRoom = this.getPlayerRoom( playerId )
        if ( !room || !room.members.has( playerId ) ) {
            return
        }

        let oldLeaderId = room.leaderId
        room.leaderId = playerId

        let newLeaderPacket: Buffer = ExManagePartyRoomMember( playerId, room, PartyRoomMemberOperation.PromoteLeader )
        let oldLeaderPacket: Buffer = ExManagePartyRoomMember( oldLeaderId, room, PartyRoomMemberOperation.PromoteLeader )
        let messagePacket: Buffer = SystemMessageBuilder.fromMessageId( SystemMessageIds.PARTY_ROOM_LEADER_CHANGED )

        // TODO : inspect order of packets, why send old and new leader packets?
        room.members.forEach( ( memberId: number ) => {
            PacketDispatcher.sendCopyData( memberId, oldLeaderPacket )
            PacketDispatcher.sendCopyData( memberId, newLeaderPacket )
            PacketDispatcher.sendCopyData( memberId, messagePacket )
        } )
    }

    removePlayer( playerId: number ): void {
        let room: PartyMatchRoom = this.getPlayerRoom( playerId )
        if ( !room ) {
            return
        }

        if ( playerId !== room.leaderId ) {
            room.members.delete( playerId )
            delete this.playerMapping[ playerId ]
            return this.notifyMembersAboutExit( room, playerId )
        }

        if ( room.members.size === 1 ) {
            delete this.playerMapping[ room.leaderId ]
            delete this.rooms[ room.id ]

            return
        }

        this.makeLeader( room.members[ 1 ] )

        return this.notifyMembersAboutExit( room, playerId )
    }

    notifyMembersAboutExit( room: PartyMatchRoom, playerId: number ): void {
        let messagePacket = new SystemMessageBuilder( SystemMessageIds.C1_LEFT_PARTY_ROOM )
                .addPlayerNameWithId( playerId )
                .getBuffer()
        let uiPacket = ExManagePartyRoomMember( playerId, room, PartyRoomMemberOperation.Remove )

        room.members.forEach( ( memberId: number ) => {
            PacketDispatcher.sendCopyData( memberId, messagePacket )
            PacketDispatcher.sendCopyData( memberId, uiPacket )
        } )
    }

    getRoomLocation( room: PartyMatchRoom ): number {
        let player = L2World.getPlayer( room.leaderId )
        return RespawnRegionCache.getRespawnPoint( player ).bbs
    }

    updateRoom( leaderId: number, roomId: number, maximumMembers: number, minLevel: number, maxLevel: number, lootType: number, title: string ): void {
        let room: PartyMatchRoom = this.getRoom( roomId )
        if ( !room || leaderId !== room.leaderId ) {
            return
        }

        room.maximumMembers = maximumMembers
        room.minimumLevel = minLevel
        room.maximumLevel = maxLevel
        room.lootType = lootType
        room.title = title

        let roomPacket: Buffer = PartyMatchDetail( room )
        let messagePacket: Buffer = SystemMessageBuilder.fromMessageId( SystemMessageIds.PARTY_ROOM_REVISED )

        room.members.forEach( ( memberId: number ) => {
            PacketDispatcher.sendCopyData( memberId, roomPacket )
            PacketDispatcher.sendCopyData( memberId, messagePacket )
        } )
    }

    addToExistingRoom( leaderId: number, player: L2PcInstance ): boolean {
        return this.addToRoom( this.getPlayerRoom( leaderId ), player )
    }

    addToExistingRoomById( id: number, player: L2PcInstance ): boolean {
        return this.addToRoom( this.getRoom( id ), player )
    }

    private addToRoom( room: PartyMatchRoom, player: L2PcInstance ): boolean {
        if ( !room ) {
            return false
        }

        if ( player.getLevel() < room.minimumLevel || player.getLevel() > room.maximumLevel ) {
            return false
        }

        let enterMessage: Buffer = new SystemMessageBuilder( SystemMessageIds.C1_ENTERED_PARTY_ROOM )
                .addPlayerCharacterName( player )
                .getBuffer()

        let partyPacket: Buffer = ExManagePartyRoomMember( player.getObjectId(), room, PartyRoomMemberOperation.Add )

        room.members.forEach( ( memberId: number ) => {
            PacketDispatcher.sendCopyData( memberId, partyPacket )
            PacketDispatcher.sendCopyData( memberId, enterMessage )
        } )

        room.members.add( player.getObjectId() )
        return true
    }

    createRoom( player: L2PcInstance, maximumMembers: number, minimumLevel: number, maximumLevel: number, lootType: number, title: string ): void {
        let room: PartyMatchRoom = {
            id: this.currentId++,
            lootType,
            maximumLevel: 0,
            maximumMembers,
            members: new Set<number>( [ player.getObjectId() ] ),
            minimumLevel: 0,
            title,
            leaderId: player.getObjectId()
        }

        this.rooms[ room.id ] = room
        this.playerMapping[ player.getObjectId() ] = room

        player.sendOwnedData( PartyMatchDetail( room ) )
        player.sendOwnedData( ExPartyRoomMember( room, ExPartyRoomMemberOperation.Add ) )
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.PARTY_ROOM_CREATED ) )

        player.broadcastUserInfo()
    }

    addPlayersToRoom( leaderId: number, memberIds: Array<number> ): void {
        let room = this.getPlayerRoom( leaderId )
        if ( !room ) {
            return
        }

        room.members = new Set<number>( [ leaderId, ...memberIds, ...room.members ] )

        let detailPacket = PartyMatchDetail( room )
        let memberPacket = ExPartyRoomMember( room, ExPartyRoomMemberOperation.Add )

        room.members.forEach( ( memberId: number ) => {
            // TODO : why ignore leader?
            if ( memberId === leaderId ) {
                return
            }

            PacketDispatcher.sendCopyData( memberId, detailPacket )
            PacketDispatcher.sendCopyData( memberId, memberPacket )
        } )
    }
}

export const PartyMatchManager = new Manager()