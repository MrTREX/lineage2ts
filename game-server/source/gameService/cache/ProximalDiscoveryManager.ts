import { L2Object } from '../models/L2Object'
import { L2World } from '../L2World'
import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { SpawnItem } from '../packets/send/SpawnItem'
import { L2AirShipInstance } from '../models/actor/instance/L2AirShipInstance'
import { DeleteObject } from '../packets/send/DeleteObject'
import { EventType, NpcSeePlayerEvent } from '../models/events/EventType'
import { ListenerCache } from './ListenerCache'
import { GeneralHelper } from '../helpers/GeneralHelper'
import { PacketDispatcher } from '../PacketDispatcher'
import { EventPoolCache } from './EventPoolCache'
import { L2Character } from '../models/actor/L2Character'
import _ from 'lodash'
import { RoaringBitmap32 } from 'roaring'
import { L2Region } from '../enums/L2Region'

/*
    TODO : optimize for static objects that do not change their location, such as dropped items
    these can have their own search grid, with possible lower rate of discovery or keeping track of already discovered items in short range
    TODO : consider triggering findObjects off character movement on simple debounced/throttled interval
    example: player movement is updated every 0.4 seconds, while discovery interval can be 1.5 or so seconds, trigger discovery on stop too
    if player is moving he needs to discover objects, if stationary he should receive packets from other players/objects that
    broadcast packets
 */

type DiscoveredObjects = Record<number, RoaringBitmap32>

class Manager {
    discoveredObjects: DiscoveredObjects = {}
    seenPlayerObjects: DiscoveredObjects = {}

    findObjects( player: L2PcInstance ) {
        L2World.forEachObjectByPredicate( player, this.getDistanceToWatchObject(), ( object: L2Object ): void => {
            ProximalDiscoveryManager.addKnownObject( player, object )
        }, false )
    }

    getDistanceToWatchObject(): number {
        return L2Region.Length
    }

    addPlayer( player: L2PcInstance ) {
        this.discoveredObjects[ player.getObjectId() ] = new RoaringBitmap32()
        this.seenPlayerObjects[ player.getObjectId() ] = new RoaringBitmap32()
    }

    removePlayer( player: L2PcInstance ) {
        delete this.discoveredObjects[ player.getObjectId() ]
        delete this.seenPlayerObjects[ player.getObjectId() ]
    }

    addKnownObject( player: L2PcInstance, object: L2Object ): Promise<void> {
        if ( object.isNpc()
                && !this.seenPlayerObjects[ player.getObjectId() ].has( object.getObjectId() )
                && ListenerCache.hasNpcTemplateListeners( object.getId(), EventType.NpcSeePlayer )
                && GeneralHelper.checkIfInRange( 400, player, object, false ) ) {

            this.seenPlayerObjects[ player.getObjectId() ].add( object.getObjectId() )

            let data = EventPoolCache.getData( EventType.NpcSeePlayer ) as NpcSeePlayerEvent

            data.npcId = object.getId()
            data.npcObjectId = object.getObjectId()
            data.playerId = player.getObjectId()

            return ListenerCache.sendNpcTemplateEvent( object.getId(), EventType.NpcSeePlayer, data )
        }

        if ( this.discoveredObjects[ player.getObjectId() ].has( object.getObjectId() ) ) {
            return
        }

        if ( object.isPlayer() && ( object as L2PcInstance ).getAppearance().isGhost() ) {
            return
        }

        // TODO : add conditional check for distance
        // for example, treasure chests can be targeted automatically in client
        // to avoid this, have check for distance on these types of objects, but not all
        // to avoid unnecessary math, thus objects can be shown much closer in client (think of LOD in graphics)
        // if(object.hasVisibleDistance() && object.isInVisibleDistance(player)) {}

        this.discoveredObjects[ player.getObjectId() ].add( object.getObjectId() )
        return this.showObject( player, object )
    }

    getDistanceToForgetObject(): number {
        return L2Region.Length + 1000
    }

    async showObject( player: L2PcInstance, object: L2Object ): Promise<void> {
        if ( object.isPolymorphed() && object.getPolymorphName() === 'item' ) {
            player.sendOwnedData( SpawnItem( object ) )
            return
        }

        if ( !object.hasLoadedSkills() ) {
            await object.prepareSkills()
        }

        /*
            Object visibility affects AIController, turning its state to active. Such operation avoids cases when we need to keep track when
            to turn it on or off. It also allows AIController behavior to return npc to original position, since turning AIController off in
            normal npcs will stop movement (npcs that have route movements will not be affected since their movement trait will keep working
            until switched off).
            World regions will switch AIController off on deactivation.
         */
        if ( object.shouldDescribeState( player ) ) {
            let aiController = ( object as L2Character ).getAIController()
            if ( !aiController.isActive() ) {
                await aiController.activate()
            }

            return aiController.describeState( player.getObjectId() )
        }
    }

    forgetAllObjects( playerId: number ) {
        for ( const objectId of this.discoveredObjects[ playerId ] ) {
            this.hideObject( playerId, objectId )
        }

        this.discoveredObjects[ playerId ].clear()
        this.seenPlayerObjects[ playerId ].clear()
    }

    /*
        TODO : see area discovery manager for improved bitmap handling
        - use two bitmaps instead of three
        - use pre-allocated bitmap, which works due to single threaded nature of Nodejs process running discovery
     */
    forgetObjects( object: L2Character ) {
        let currentObjectIds: RoaringBitmap32 = new RoaringBitmap32( L2World.getVisibleObjectIdsByPredicate( object, this.getDistanceToForgetObject(), _.constant( true ), false ) )
        let removedEntries: RoaringBitmap32 = RoaringBitmap32.andNot( this.discoveredObjects[ object.getObjectId() ], currentObjectIds )

        if ( removedEntries.has( object.getTargetId() ) ) {
            object.setTarget( null )
        }

        for ( const objectId of removedEntries ) {
            this.hideObject( object.getObjectId(), objectId )
        }

        this.discoveredObjects[ object.getObjectId() ].andNotInPlace( removedEntries )
        this.seenPlayerObjects[ object.getObjectId() ].andNotInPlace( removedEntries )
    }

    isDiscoveredBy( playerId: number, objectId: number ): boolean {
        return this.discoveredObjects[ playerId ].has( objectId )
    }

    hideObject( playerId: number, objectId: number ): void {
        let object: L2Object = L2World.getObjectById( objectId )
        if ( object && object.isAirShip() ) {
            let airship = object as unknown as L2AirShipInstance
            if ( airship.getCaptainId() !== 0 && airship.getCaptainId() !== playerId ) {
                PacketDispatcher.sendOwnedData( playerId, DeleteObject( airship.getCaptainId() ) )
            }

            if ( airship.getHelmObjectId() !== 0 ) {
                PacketDispatcher.sendOwnedData( playerId, DeleteObject( airship.getHelmObjectId() ) )
            }
        }

        PacketDispatcher.sendOwnedData( playerId, DeleteObject( objectId ) )
    }

    forgetObjectId( playerId: number, objectId: number ) : void {
        this.discoveredObjects[ playerId ].remove( objectId )
        this.seenPlayerObjects[ playerId ].remove( objectId )
    }
}

export const ProximalDiscoveryManager = new Manager()