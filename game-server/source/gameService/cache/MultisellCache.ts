import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { ExBrExtraUserInfo } from '../packets/send/ExBrExtraUserInfo'
import { UserInfo } from '../packets/send/UserInfo'
import { SystemMessageBuilder } from '../packets/send/SystemMessage'
import { SystemMessageIds } from '../packets/SystemMessageIdValues'
import { L2Npc } from '../models/actor/L2Npc'
import { ListContainer } from '../models/multisell/ListContainer'
import { DataManager } from '../../data/manager'
import { PreparedListContainer } from '../models/multisell/PreparedListContainer'
import { Entry } from '../models/multisell/Entry'
import { Ingredient } from '../models/multisell/Ingredient'
import { MultisellValues } from '../values/MultisellValues'
import { MultiSellList } from '../packets/send/MultiSellList'
import { GamePointsCache, GamePointType } from './GamePointsCache'
import { ConfigManager } from '../../config/ConfigManager'
import { ExPCCafePointInfo } from '../packets/send/ExPCCafePointInfo'

/*
    TODO : no caching here, convert to normal helper functions
 */
class Manager {
    giveSpecialProduct( id: number, amount: number, player: L2PcInstance ): Promise<void> {
        switch ( id ) {
            case MultisellValues.CLAN_REPUTATION:
                return player.getClan().addReputationScore( Math.round( amount ), true )

            case MultisellValues.FAME:
                player.setFame( Math.round( player.getFame() + amount ) )
                player.sendDebouncedPacket( UserInfo )
                player.sendDebouncedPacket( ExBrExtraUserInfo )
                return

            case MultisellValues.PC_BANG_POINTS:
                if ( !ConfigManager.character.isGamePointsEnabled() || !ConfigManager.character.usePcCafePoints() ) {
                    return
                }

                GamePointsCache.addPoints( player.getAccountName(), GamePointType.PcCafe, amount, 'Multisell.giveSpecialProduct' )
                player.onPcPointsAcquired( amount )
                return
        }
    }

    async takeSpecialIngredient( id: number, amount: number, player: L2PcInstance ): Promise<boolean> {
        switch ( id ) {
            case MultisellValues.CLAN_REPUTATION:
                await player.getClan().takeReputationScore( Math.round( amount ), true )

                let packet = new SystemMessageBuilder( SystemMessageIds.S1_DEDUCTED_FROM_CLAN_REP )
                        .addNumber( amount )
                        .getBuffer()

                player.sendOwnedData( packet )
                return true

            case MultisellValues.FAME:
                player.setFame( player.getFame() - Math.round( amount ) )
                player.sendDebouncedPacket( UserInfo )
                player.sendDebouncedPacket( ExBrExtraUserInfo )
                return true

            case MultisellValues.PC_BANG_POINTS:
                if ( !ConfigManager.character.isGamePointsEnabled() || !ConfigManager.character.usePcCafePoints() ) {
                    return false
                }

                if ( GamePointsCache.getCurrentPoints( player.getAccountName(), GamePointType.PcCafe ) < amount ) {
                    return false
                }

                GamePointsCache.addPoints( player.getAccountName(), GamePointType.PcCafe, -amount, 'Multisell.takeSpecialIngredient' )

                let message = new SystemMessageBuilder( SystemMessageIds.USING_S1_PCPOINT )
                    .addNumber( amount )
                    .getBuffer()

                player.sendOwnedData( message )
                player.sendOwnedData( ExPCCafePointInfo( GamePointsCache.getCurrentPoints( player.getAccountName(), GamePointType.PcCafe ) ) )
                return true
        }

        return false
    }

    hasSpecialIngredient( id: number, amount: number, player: L2PcInstance ) : boolean {
        switch ( id ) {
            case MultisellValues.CLAN_REPUTATION:
                if ( !player.getClan() ) {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ARE_NOT_A_CLAN_MEMBER ) )
                    break
                }

                if ( !player.isClanLeader() ) {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ONLY_THE_CLAN_LEADER_IS_ENABLED ) )
                    break
                }

                if ( player.getClan().getReputationScore() < amount ) {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THE_CLAN_REPUTATION_SCORE_IS_TOO_LOW ) )
                    break
                }

                return true

            case MultisellValues.FAME:
                if ( player.getFame() < amount ) {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_FAME_POINTS ) )
                    break
                }

                return true

            case MultisellValues.PC_BANG_POINTS:
                if ( !ConfigManager.character.isGamePointsEnabled() || !ConfigManager.character.usePcCafePoints() ) {
                    return false
                }

                if ( GamePointsCache.getCurrentPoints( player.getAccountName(), GamePointType.PcCafe ) < amount ) {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SHORT_OF_ACCUMULATED_POINTS ) )
                    return false
                }

                return true
        }

        return false
    }

    separateAndSend( listId: number, player: L2PcInstance, npc: L2Npc, inventoryOnly: boolean, productMultiplier: number = 1, ingredientMultiplier: number = 1 ) : void {
        let template : ListContainer = DataManager.getMultisellData().getEntry( listId )
        if ( !template ) {
            return
        }

        if ( ( npc && !template.isNpcAllowed( npc.getId() ) ) || ( !npc && template.isNpcOnly() ) ) {
            return
        }

        let list : PreparedListContainer = new PreparedListContainer( template, inventoryOnly, player, npc )

        if ( ( productMultiplier !== 1 ) || ( ingredientMultiplier !== 1 ) ) {
            list.getEntries().forEach( ( entry: Entry ) => {

                entry.getProducts().forEach( ( ingredient: Ingredient ) => {
                    ingredient.setItemCount( Math.max( ingredient.getItemCount() * productMultiplier, 1 ) )
                } )

               entry.getIngredients().forEach( ( ingredient: Ingredient ) => {
                    ingredient.setItemCount( Math.max( ingredient.getItemCount() * ingredientMultiplier, 1 ) )
                } )
            } )
        }

        let index = 0
        do {
            player.sendOwnedData( MultiSellList( list, index ) )
            index += MultisellValues.PAGE_SIZE
        }
        while ( index < list.getEntries().length )

        if ( player.isGM() ) {
            player.sendMessage( `Using multisell ${listId} with ${list.getEntries().length} items.` )
        }

        player.setMultiSell( list )
    }
}

export const MultisellCache = new Manager()