import { L2ItemInstance } from '../models/items/instance/L2ItemInstance'
import { DataManager } from '../../data/manager'
import { ConfigManager } from '../../config/ConfigManager'
import _ from 'lodash'
import { L2ItemSlots } from '../enums/L2ItemSlots'

export const EnchantItemBonusManager = {
    getHPBonus( item: L2ItemInstance ): number {
        let grade = item.getItem().getItemGradeSPlus()
        let values : Array<number> = DataManager.getEnchantItemHPBonusData().getHPBonus( grade )

        if ( !_.isArray( values ) || values.length === 0 || item.getOlympiadEnchantLevel() <= 0 ) {
            return 0
        }

        let bonus = values[ Math.min( item.getOlympiadEnchantLevel(), values.length ) - 1 ]
        if ( item.getItem().getBodyPart() === L2ItemSlots.FullArmor ) {
            return bonus * ConfigManager.tuning.getFullArmorHPBonusMultiplier()
        }

        return bonus
    },
}