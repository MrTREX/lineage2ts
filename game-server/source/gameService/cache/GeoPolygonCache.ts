import { DataManager } from '../../data/manager'
import { GeoRegion } from '../../geodata/GeoRegion'
import { L2WorldRegion } from '../models/L2WorldRegion'
import { L2Object } from '../models/L2Object'
import _ from 'lodash'
import { L2MoveManager } from '../L2MoveManager'
import { GeoDataOperations } from '../../geodata/GeoDataOperations'
import { RoaringBitmap32 } from 'roaring'
import { ListenerCache } from './ListenerCache'
import { EventType, GeoRegionActivatedEvent, WorldRegionDeactivatedEvent } from '../models/events/EventType'
import { L2WorldLimits } from '../enums/L2MapTile'
import { ServerLog } from '../../logger/Logger'
import { ConfigManager } from '../../config/ConfigManager'
import { formatMemoryUsage } from '../helpers/MemoryHelper'
import { L2DataApi } from '../../data/interface/l2DataApi'

class Manager extends GeoDataOperations implements L2DataApi {
    regionOwnership: Record<number, number> = {}
    loadingRegions: RoaringBitmap32 = new RoaringBitmap32()
    failedRegions: RoaringBitmap32 = new RoaringBitmap32()

    private async loadRegion( region: L2WorldRegion ) : Promise<void> {
        let startTime = Date.now()
        let loadedData : Buffer = DataManager.getGeoRegionData().getRegionData( region.tileX, region.tileY )
        if ( !loadedData ) {
            return this.onRegionNotFound( region )
        }

        let sharedData = new SharedArrayBuffer( loadedData.byteLength )
        let regionData = Buffer.from( sharedData )

        loadedData.copy( regionData )
        this.regions[ region.code ] = new GeoRegion( regionData )

        L2MoveManager.registerGeoData( region.code, sharedData )
        ServerLog.info( `GeoPolygonCache : loaded region (x = ${region.tileX}, y = ${region.tileY}) in ${ Date.now() - startTime } ms, using ${formatMemoryUsage( loadedData.byteLength ).join( ', ' )} of data` )

        this.addRegionOwnership( region.code )
        this.hasLoadedRegions = true

        if ( ListenerCache.hasGeneralListener( EventType.GeoRegionActivated ) ) {
            let data: GeoRegionActivatedEvent = {
                x: region.tileX,
                y: region.tileY,
                code: region.code
            }

            return ListenerCache.sendGeneralEvent( EventType.GeoRegionActivated, data )
        }
    }

    private onRegionNotFound( region: L2WorldRegion ) {
        ServerLog.error( `GeoPolygonCache : Unable to load geodata for region x = ${ region.tileX }, y = ${ region.tileY }` )

        this.loadingRegions.remove( region.code )
        this.failedRegions.add( region.code )
    }

    hasRegion( regionCode: number ) : boolean {
        if ( this.loadingRegions.has( regionCode ) ) {
            return true
        }

        return !!this.regions[ regionCode ]
    }

    async startRegionLoad( region: L2WorldRegion ): Promise<void> {
        if ( this.hasRegion( region.code ) ) {
            this.addRegionOwnership( region.code )
            return
        }

        if ( this.failedRegions.has( region.code ) ) {
            return
        }

        this.loadingRegions.add( region.code )
        return this.loadRegion( region )
    }

    unLoadRegion( region: L2WorldRegion ) : void {
        if ( !this.removeRegionOwnership( region.code ) ) {
            return
        }

        if ( ConfigManager.server.isGeodataCanUnload() ) {
            delete this.regions[ region.code ]
            this.hasLoadedRegions = _.isEmpty( this.regions )
            this.loadingRegions.remove( region.code )

            L2MoveManager.unRegisterGeoData( region.code )

            ServerLog.info( `GeoPolygonCache : un-loaded region (x = ${region.tileX}, y = ${region.tileY})` )
        }

        if ( ListenerCache.hasGeneralListener( EventType.GeoRegionDeactivated ) ) {
            let data: WorldRegionDeactivatedEvent = {
                x: region.tileX,
                y: region.tileY,
                code: region.code
            }

            ListenerCache.sendGeneralEvent( EventType.GeoRegionDeactivated, data )
        }
    }

    getObjectZ( object: L2Object ) : number {
        if ( !this.hasLoadedRegions ) {
            return object.getZ()
        }

        let objectRegion = object.getWorldRegion()
        if ( !objectRegion ) {
            return this.getZ( object.getX(), object.getY(), object.getZ() )
        }

        let region = this.regions[ objectRegion.code ]

        if ( !region ) {
            return object.getZ()
        }

        let xDifference = object.getX() - L2WorldLimits.MinX
        let yDifference = object.getY() - L2WorldLimits.MinY
        return region.getHeight( xDifference, yDifference, object.getZ() )
    }

    addRegionOwnership( regionCode: number ) : void {
        if ( !this.regionOwnership[ regionCode ] ) {
            this.regionOwnership[ regionCode ] = 0
        }

        this.regionOwnership[ regionCode ]++
    }

    /*
        We need to check if any other region can claim geodata. There would be many of small regions (not related to tiles on game map)
        that would need geodata loaded. However, removing geodata from server would need to happen when no regions would needed, hence
        all this incremental tracking.
     */
    removeRegionOwnership( regionCode: number ) : boolean {
        this.regionOwnership[ regionCode ]--

        return this.regionOwnership[ regionCode ] === 0
    }

    /*
        Seeing target does not mean target is reachable via walking in direct line.
     */
    canSeeTarget( initiator: L2Object, target: L2Object ) : boolean {
        if ( !this.hasLoadedRegions ) {
            return false
        }

        return this.isVisible( initiator.getX(), initiator.getY(), initiator.getZ(), target.getX(), target.getY(), target.getZ() )
    }

    async load(): Promise<Array<string>> {
        const stats = DataManager.getGeoRegionData().initialize()

        return [
            `GeoPolygonCache: ready to load ${stats.regions} geo-regions`
        ]
    }
}

export const GeoPolygonCache = new Manager()