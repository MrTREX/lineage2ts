import { ConfigManager } from '../../config/ConfigManager'
import { L2Attackable } from '../models/actor/L2Attackable'
import { L2Item } from '../models/items/L2Item'
import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import _ from 'lodash'

export function isGlobalAutolootEnabled( item: L2Item, mob: L2Attackable ) : boolean {
    if ( item.hasExImmediateEffect() ) {
        return ConfigManager.character.autoLootHerbs()
    }

    if ( mob.isRaid() ) {
        return ConfigManager.character.autoLootRaids()
    }

    return ConfigManager.character.autoLoot()
}

export function isPlayerAutolootEnabled( item: L2Item, mob: L2Attackable, player: L2PcInstance ) : boolean {
    if ( !player.isAutoLootEnabled() ) {
        return false
    }

    if ( item.hasExImmediateEffect() ) {
        return player.shouldLootHerb() && ConfigManager.customs.getAutoLootHerbsList().includes( item.getId() )
    }

    if ( mob.isRaid() ) {
        return false
    }

    return player.shouldLootItem() && ConfigManager.customs.getAutoLootItemsList().includes( item.getId() )
}

export function shouldDropChampionLoot( mob: L2Attackable, player: L2PcInstance ) : boolean {
    return ( player.getLevel() <= mob.getLevel() && _.random( 100 ) < ConfigManager.customs.getChampionRewardLowerLevelItemChance() )
            || ( player.getLevel() > mob.getLevel() && _.random( 100 ) < ConfigManager.customs.getChampionRewardHigherLevelItemChance() )
}