import { L2World } from '../L2World'
import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { NpcSay } from '../packets/send/builder/NpcSay'
import { L2Npc } from '../models/actor/L2Npc'
import { CreatureSay } from '../packets/send/builder/CreatureSay'
import { ILocational } from '../models/Location'
import { PacketDispatcher } from '../PacketDispatcher'
import { L2Character } from '../models/actor/L2Character'
import { PlayerActionOverride } from '../values/PlayerConditions'
import * as Buffer from 'buffer'
import _ from 'lodash'
import { NpcSayType } from '../enums/packets/NpcSayType'

export const BroadcastHelper = {
    toAllOnlinePlayersData( packet: Buffer ) {
        _.each( L2World.getAllPlayers(), ( player: L2PcInstance ) => {
            if ( player.isOnline() ) {
                player.sendCopyData( packet )
            }
        } )
    },

    multiplePacketsToSelfAndKnownPlayers( character: L2Character, packets: Array<Buffer> ): void {
        if ( character.isPlayable() ) {
            let player = character.getActingPlayer()
            packets.forEach( ( data: Buffer ) => {
                player.sendCopyData( data )
            } )
        }

        BroadcastHelper.multiplePacketsToKnownPlayers( character, packets )
    },

    multiplePacketsToKnownPlayers( character: L2Character, packets: Array<Buffer> ): void {
        L2World.getVisiblePlayers( character, character.getBroadcastRadius() ).forEach( ( player: L2PcInstance ) => {
            packets.forEach( ( data: Buffer ) => {
                player.sendCopyData( data )
            } )
        } )
    },

    broadcastNpcSayStringId( npc: L2Npc, type: NpcSayType, stringId: number, ...parameters: Array<string> ): void {
        let packet = NpcSay.fromNpcString(
                npc.getObjectId(),
                type,
                npc.getId(),
                stringId,
                ...parameters ).getBuffer()

        return BroadcastHelper.dataInLocation( npc, packet, npc.getBroadcastRadius() )
    },

    broadcastTextToAllPlayers( announcer: string, message: string, isCriticalMessage: boolean = false ): void {
        let packet: Buffer = CreatureSay.fromParameters( 0,
                                                isCriticalMessage ? NpcSayType.CriticalAnnouncement : NpcSayType.Announce,
                                                announcer,
                                                message )
                                        .getBuffer()

        return BroadcastHelper.toAllOnlinePlayersData( packet )
    },

    broadcastDataAtLocations( locations: Array<ILocational>, radius: number, ...packets: Array<Buffer> ): void {
        locations.forEach( ( location: ILocational ) => {
            L2World.getAllVisiblePlayerIds( location, radius ).forEach( ( playerId: number ) => {
                packets.forEach( ( packet: Buffer ) => {
                    PacketDispatcher.sendCopyData( playerId, packet )
                } )
            } )
        } )
    },

    dataInLocation( location: ILocational, data: Buffer, range: number ): void {
        L2World.getAllVisiblePlayerIds( location, range ).forEach( ( playerId: number ) => {
            PacketDispatcher.sendCopyData( playerId, data )
        } )
    },

    dataBasedOnVisibility( character: L2Character, data: Buffer, range: number = character.getBroadcastRadius() ): void {
        let players: Array<L2PcInstance> = L2World.getVisiblePlayersByPredicate( character, range, ( player: L2PcInstance ): boolean => {
            return !character.isInvisible() || player.hasActionOverride( PlayerActionOverride.SeeInvisible )
        }, false )

        players.forEach( ( player: L2PcInstance ) => {
            player.sendCopyData( data )
        } )
    },

    dataToSelfBasedOnVisibility( character: L2Character, data: Buffer, range: number = character.getBroadcastRadius() ): void {
        character.sendCopyData( data )
        this.dataBasedOnVisibility( character, data, range )
    },

    dataToSelfInRange( character: L2Character, data: Buffer, range: number = character.getBroadcastRadius() ): void {
        character.sendCopyData( data )
        this.dataInLocation( character, data, range )
    },
}