import { EffectFunctionCalculator, L2CharacterCalculatorFunctions } from '../models/EffectFunctionCalculator'
import { AbstractFunction } from '../models/stats/functions/AbstractFunction'
import { MaxCpMultiplier, MaxHpMultiplier, MaxMpMultiplier } from '../models/stats/functions/formulas/MaxStatMultiplier'
import {
    PowerAttackModifier,
    PowerAttackSpeed,
    PowerDefenceModifier
} from '../models/stats/functions/formulas/PowerAttackEffects'
import {
    MagicAttackCritical,
    MagicAttackModifier, MagicAttackSpeed,
    MagicDefenceModifier
} from '../models/stats/functions/formulas/MagicEffects'
import { AttackAccuracy, AttackCritical, AttackEvasion } from '../models/stats/functions/formulas/AttackEffects'
import { MoveSpeed } from '../models/stats/functions/formulas/MoveEffects'
import {
    HennaCON,
    HennaDEX,
    HennaINT,
    HennaMEN,
    HennaSTR,
    HennaWIT
} from '../models/stats/functions/formulas/HennaEffects'
import {
    ArmorCON,
    ArmorDEX,
    ArmorINT,
    ArmorMEN,
    ArmorSTR,
    ArmorWIT
} from '../models/stats/functions/formulas/ArmorSetEffects'

const npcFunctions: Array<AbstractFunction> = [
    MaxHpMultiplier,
    MaxMpMultiplier,
    PowerAttackModifier,
    MagicAttackModifier,
    PowerDefenceModifier,
    MagicDefenceModifier,
    AttackCritical,
    MagicAttackCritical,
    AttackAccuracy,
    AttackEvasion,
    PowerAttackSpeed,
    MagicAttackSpeed,
    MoveSpeed,
]

const doorFunctions: Array<AbstractFunction> = [
    AttackAccuracy,
    AttackEvasion,
    PowerDefenceModifier,
    MagicDefenceModifier,
]

const playerFunctions: Array<AbstractFunction> = [
    MaxHpMultiplier,
    MaxCpMultiplier,
    MaxMpMultiplier,
    PowerAttackModifier,
    MagicAttackModifier,
    PowerDefenceModifier,
    MagicDefenceModifier,
    AttackCritical,
    MagicAttackCritical,
    AttackAccuracy,
    AttackEvasion,
    PowerAttackSpeed,
    MagicAttackSpeed,
    MoveSpeed,

    HennaSTR,
    HennaDEX,
    HennaINT,
    HennaMEN,
    HennaCON,
    HennaWIT,

    ArmorSTR,
    ArmorDEX,
    ArmorINT,
    ArmorMEN,
    ArmorCON,
    ArmorWIT,
]

const summonFunctions: Array<AbstractFunction> = [
    MaxHpMultiplier,
    MaxMpMultiplier,
    PowerAttackModifier,
    MagicAttackModifier,
    PowerDefenceModifier,
    MagicDefenceModifier,
    AttackCritical,
    MagicAttackCritical,
    AttackAccuracy,
    AttackEvasion,
    MoveSpeed,
    PowerAttackSpeed,
    MagicAttackSpeed,
]

export const CalculatorHelper = {
    getStdNPCCalculators() : EffectFunctionCalculator {
        return new EffectFunctionCalculator().fromFunctions( npcFunctions )
    },

    getStandardDoorCalculators() : EffectFunctionCalculator {
        return new EffectFunctionCalculator().fromFunctions( doorFunctions )
    },

    getSummonCalculators() : EffectFunctionCalculator {
        return new EffectFunctionCalculator().fromFunctions( summonFunctions )
    },

    getPlayerCalculators() : EffectFunctionCalculator {
        return new EffectFunctionCalculator().fromFunctions( playerFunctions )
    }
}