const enum MemoryUnits {
    MB = 1048576,
    KB = 1024
}

export function formatMemoryUsage( valueInBytes: number, addBytes: boolean = false ) : Array<string> {
    const messages : Array<string> = []
    let allBytes = valueInBytes

    if ( allBytes > MemoryUnits.MB ) {
        messages.push( `${ Math.floor( allBytes / MemoryUnits.MB )} MB` )
        allBytes = allBytes % MemoryUnits.MB
    }

    if ( allBytes > MemoryUnits.KB ) {
        messages.push( `${ Math.floor( allBytes / MemoryUnits.KB )} KB` )
        allBytes = allBytes % MemoryUnits.KB
    }

    if ( addBytes && allBytes > 0 ) {
        messages.push( `${allBytes} bytes` )
    }

    return messages
}