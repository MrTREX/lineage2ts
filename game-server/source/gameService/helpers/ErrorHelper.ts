const newlinesRegex = new RegExp( /(\r\n)+|\r+|\n+|\t+/g )

export const ErrorHelper = {
    formatError( prefix: string, error : Error ) : string {
        return `<html><body>${prefix}<br1>Error stack:<br1>${ error.stack.replace( newlinesRegex, '<br1>     ' ) }</body></html>`
    }
}