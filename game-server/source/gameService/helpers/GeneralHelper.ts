import { L2Object } from '../models/L2Object'
import { L2Character } from '../models/actor/L2Character'
import { ILocational } from '../models/Location'
import { L2World } from '../L2World'
import { HtmlActionScope } from '../enums/HtmlActionScope'
import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { ShowBoard, ShowBoardArray } from '../packets/send/ShowBoard'
import { HtmlActionCache } from '../cache/HtmlActionCache'
import _ from 'lodash'

const radianDegreesMultiplier = 57.29577951308232
const headingDegreeMultiplier = Math.pow( 2, 16 ) / 360
const alphaNumbericRegex = new RegExp( /^[a-zA-Z0-9]+$/ )
const adenaFormat = new Intl.NumberFormat()
export const htmlPacketLimit: number = 16230
export const CommunityBoardHtmlLimit = htmlPacketLimit * 3
const showBoardChunkCutoff = htmlPacketLimit * 2
const communityBoardError = '<html><body><br><center>Error: HTML was too long!</center></body></html>'
export const DegreeToRadian = Math.PI / 180

export const GeneralHelper = {
    checkIfInShortRadius( radius: number, observingObject: L2Object, target: L2Object, includeZAxis: boolean ): boolean {
        if ( !observingObject || !target ) {
            return false
        }

        if ( radius === -1 ) {
            return true
        }

        let distance = Math.hypot(
                observingObject.getX() - target.getX(),
                observingObject.getY() - target.getY(),
                includeZAxis ? observingObject.getZ() - target.getZ() : 0,
        )

        return Math.floor( distance ) <= radius
    },

    checkIfInRange( range: number, target: L2Object, attacker: L2Object, includeZAxis: boolean ) {
        if ( !target || !attacker ) {
            return false
        }

        if ( target.getInstanceId() !== attacker.getInstanceId() ) {
            return false
        }

        if ( range === -1 ) {
            return true
        }

        let radius = 0
        if ( target.isCharacter() ) {
            radius += ( target as L2Character ).getTemplate().getCollisionRadius()
        }
        if ( attacker.isCharacter() ) {
            radius += ( attacker as L2Character ).getTemplate().getCollisionRadius()
        }

        let diameter = Math.hypot( target.getX() - attacker.getX(), target.getY() - attacker.getY() )
        if ( includeZAxis ) {
            diameter = Math.hypot( diameter, target.getZ() - attacker.getZ() )
        }

        return ( Math.floor( diameter ) - ( radius / 2 ) ) <= range
    },

    calculateAngleFromLocations( from: ILocational, to: ILocational ): number {
        return GeneralHelper.calculateAngleFromCoordinates( from.getX(), from.getY(), to.getX(), to.getY() )
    },

    calculateAngleFromCoordinates( fromX: number, fromY: number, toX: number, toY: number ): number {
        let angle = radianDegreesMultiplier * Math.atan2( toY - fromY, toX - fromX )
        if ( angle < 0 ) {
            angle = 360 + angle
        }

        return angle
    },

    convertHeadingToDegree( value: number ): number {
        return value / headingDegreeMultiplier
    },

    remapRange( startValue: number, min: number, max: number, endValueMin: number, endValueMax: number ): number {
        let value = _.clamp( startValue, min, max )
        return ( ( ( value - min ) * ( endValueMax - endValueMin ) ) / ( max - min ) ) + endValueMin
    },

    calculateHeadingFromLocations( from: ILocational, to: ILocational ): number {
        return this.calculateHeadingFromCoordinates( from.getX(), from.getY(), to.getX(), to.getY() )
    },

    calculateHeadingFromCoordinates( fromX: number, fromY: number, toX: number, toY: number ): number {
        let angleTarget = Math.atan2( toY - fromY, toX - fromX ) * 57.29577951308232
        if ( angleTarget < 0 ) {
            angleTarget = 360 + angleTarget
        }

        return Math.floor( angleTarget * headingDegreeMultiplier )
    },

    isInsideRangeOfObjectId( player: L2PcInstance, targetId: number, radius: number ): boolean {
        let target: L2Object = L2World.getObjectById( targetId )
        if ( !target ) {
            return false
        }

        return !( player.calculateDistance( target ) > radius )
    },

    angleToRadians( value: number ): number {
        return DegreeToRadian * value
    },

    calculateHeadingFromMovement( differenceX: number, differenceY: number ): number {
        let angleTarget = radianDegreesMultiplier * Math.atan2( differenceY, differenceX )
        if ( angleTarget < 0 ) {
            angleTarget = 360 + angleTarget
        }

        return Math.floor( angleTarget * headingDegreeMultiplier )
    },

    isAlphanumericString( name: string ): boolean {
        return alphaNumbericRegex.test( name )
    },

    // TODO : replace one day computates with const enum
    daysToMillis( amount: number ): number {
        return 86400000 * amount
    },

    hoursToMillis( amount: number ): number {
        return 3600000 * amount
    },

    minutesToMillis( amount: number ): number {
        return 60000 * amount
    },

    getAdenaFormat( amount: number ): string {
        return adenaFormat.format( amount )
    },

    sendCommunityBoardHtml( player: L2PcInstance, html: string, editFiller: string = '', objectId: number = 0 ): void {
        let htmlToSend = HtmlActionCache.buildCache( player.getObjectId(), HtmlActionScope.CommunityBoard, objectId, html )

        if ( editFiller && htmlToSend.length <= htmlPacketLimit ) {
            player.sendOwnedData( ShowBoard( htmlToSend, '1001' ) )
            GeneralHelper.fillMultiEditContent( player, editFiller )
            return
        }

        if ( htmlToSend.length >= CommunityBoardHtmlLimit ) {
            player.sendOwnedData( ShowBoard( communityBoardError, '101' ) )
            player.sendOwnedData( ShowBoard( '', '102' ) )
            player.sendOwnedData( ShowBoard( '', '103' ) )
            return
        }

        player.sendOwnedData( ShowBoard( htmlToSend.substring( 0, htmlPacketLimit ), '101' ) )
        player.sendOwnedData( ShowBoard( htmlToSend.substring( htmlPacketLimit, showBoardChunkCutoff ), '102' ) )
        player.sendOwnedData( ShowBoard( htmlToSend.substring( showBoardChunkCutoff ), '103' ) )
    },

    fillMultiEditContent( player: L2PcInstance, text: string ) {
        let values: Array<string> = [ '0', '0', '0', '0', '0', '0', player.getName(), player.getObjectId().toString(), player.getAccountName(), '9', ' ', ' ', text.replace( /<br>/g, '\n' ), '0', '0', '0', '0' ]
        player.sendOwnedData( ShowBoardArray( values ) )
    },

    formatSeconds( amount: number, applySeconds: boolean = true ): Array<string> {
        let seconds = amount
        let messageChunks: Array<string> = []
        if ( seconds > 3600 ) {
            messageChunks.push( `${ Math.floor( seconds / 3600 ) } hours` )
            seconds = seconds % 3600
        }

        if ( seconds > 60 ) {
            messageChunks.push( `${ Math.floor( seconds / 60 ) } minutes` )
            seconds = seconds % 60
        }

        if ( applySeconds && seconds > 0 ) {
            messageChunks.push( `${ Math.floor( seconds ) } seconds` )
        }

        return messageChunks
    },

    formatMillis( amount: number, showSeconds: boolean = true ): Array<string> {
        return this.formatSeconds( amount / 1000, showSeconds )
    },

    getTimeDifference( epoch: number ): Array<number> {
        let seconds = Math.floor( epoch / 1000 )
        let timeChunks: Array<number> = []
        if ( seconds > 3600 ) {
            timeChunks.push( Math.floor( seconds / 3600 ) )
            seconds = seconds % 3600
        }

        if ( seconds > 60 ) {
            timeChunks.push( Math.floor( seconds / 60 ) )
            seconds = seconds % 60
        }

        if ( seconds > 0 ) {
            timeChunks.push( seconds )
        }

        return timeChunks
    },

    getAngleLocation( object: ILocational, angle: number, distance: number ): [ number, number, number ] {
        let radians: number = GeneralHelper.angleToRadians( angle )
        let x = object.getX() + ( distance * Math.cos( radians ) )
        let y = object.getY() + ( distance * Math.sin( radians ) )
        let z = object.getZ()

        return [ x, y, z ]
    },

    exceedsHtmlPacketSize( stringSize: number ): boolean {
        return ( stringSize * 2 ) > htmlPacketLimit
    },

    getCurrentMinute() : number {
        return Math.floor( ( Date.now() % 3600 ) / 60 )
    },

    getCurrentHour() : number {
        return Math.floor( ( Date.now() % 86400 ) / 3600 )
    },

    getRandomHeading() : number {
        return Math.floor( Math.random() * 65536 )
    }
}