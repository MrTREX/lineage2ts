import { BroadcastHelper } from './BroadcastHelper'
import { FlyToLocation, FlyType } from '../packets/send/FlyToLocation'
import { L2Character } from '../models/actor/L2Character'
import { L2Object } from '../models/L2Object'
import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { ILocational } from '../models/Location'
import { SystemMessageBuilder } from '../packets/send/SystemMessage'
import { SystemMessageIds } from '../packets/SystemMessageIdValues'
import { DataManager } from '../../data/manager'
import _ from 'lodash'
import { L2ItemInstance } from '../models/items/instance/L2ItemInstance'
import { L2PetInstance } from '../models/actor/instance/L2PetInstance'
import { ItemHandlerMethod } from '../handler/ItemHandler'
import { Inventory } from '../models/itemcontainer/Inventory'
import { L2World } from '../L2World'

// TODO : inspect all method usages and find timeouts that are not tracked for cancellation (these need to be reviewed for usage)
function getInventoryFood( ids: Array<number>, inventory: Inventory ): L2ItemInstance {
    let outcome: L2ItemInstance

    _.each( ids, ( id: number ) => {
        outcome = inventory.getItemByItemId( id )
        if ( outcome ) {
            return false
        }
    } )

    return outcome
}

export const DeferredMethods = {
    flyToLocation( character: L2Character, target: L2Object, type: FlyType ): void {
        if ( !character || !target ) {
            return
        }

        character.setLocation( target )
        BroadcastHelper.dataToSelfBasedOnVisibility( character, FlyToLocation( character, target, type ) )
    },

    playerTeleport( playerId: number, location: ILocational ): Promise<void> {
        let player = L2World.getPlayer( playerId )
        if ( !player ) {
            return
        }

        if ( player.isOnline() ) {
            return player.teleportToLocation( location, true )
        }
    },

    async playerFeedMount( player: L2PcInstance ): Promise<void> {
        if ( !player.isMounted()
                || ( player.getMountNpcId() === 0 )
                || !DataManager.getPetData().getPetDataByNpcId( player.getMountNpcId() ) ) {
            player.stopFeed()
            return
        }

        if ( player.getCurrentFeed() > player.getFeedConsume() ) {
            player.setCurrentFeed( player.getCurrentFeed() - player.getFeedConsume() )
        } else {
            player.setCurrentFeed( 0 )
            player.stopFeed()
            await player.dismount()
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.OUT_OF_FEED_MOUNT_CANCELED ) )
            return
        }

        let foodIds: Array<number> = DataManager.getPetData().getPetDataByNpcId( player.getMountNpcId() ).getFood()
        if ( foodIds.length === 0 ) {
            return
        }

        let food: L2ItemInstance = getInventoryFood( foodIds, player.getInventory() )

        if ( !food && player.hasSummon() ) {
            food = getInventoryFood( foodIds, ( player.getSummon() as L2PetInstance ).getInventory() )
        }

        if ( food && player.isHungry() && food.isEtcItem() ) {
            let handler: ItemHandlerMethod = food.getEtcItem().getItemHandler()
            if ( handler ) {
                await handler( player, food, false )

                let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.PET_TOOK_S1_BECAUSE_HE_WAS_HUNGRY )
                        .addItemNameWithId( food.getId() )
                        .getBuffer()

                player.sendOwnedData( packet )
            }
        }
    }
}