import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { L2ItemInstance } from '../models/items/instance/L2ItemInstance'
import { ItemLocation } from '../enums/ItemLocation'
import { LifeStone, LifeStoneCache } from '../cache/LifeStoneCache'
import { CrystalType } from '../models/items/type/CrystalType'
import { ConfigManager } from '../../config/ConfigManager'
import { L2Weapon } from '../models/items/L2Weapon'
import { WeaponType } from '../models/items/type/WeaponType'
import { PrivateStoreType } from '../enums/PrivateStoreType'
import { SystemMessageBuilder } from '../packets/send/SystemMessage'
import { SystemMessageIds } from '../packets/SystemMessageIdValues'
import { LifeStoneGrade } from '../enums/LifeStoneGrade'
import { L2ItemSlots } from '../enums/L2ItemSlots'

export function isValidForAugmentation( player: L2PcInstance, item: L2ItemInstance, refinerItem: L2ItemInstance ) : boolean {
    if ( !canManipulateItem( player, item ) ) {
        return false
    }

    if ( refinerItem.getOwnerId() !== player.getObjectId() ) {
        return false
    }

    if ( refinerItem.getItemLocation() !== ItemLocation.INVENTORY ) {
        return false
    }

    let lifeStone : LifeStone = LifeStoneCache.getLifeStone( refinerItem.getId() )
    if ( !lifeStone ) {
        return false
    }

    if ( item.isWeapon() && ( lifeStone.grade === LifeStoneGrade.Accessory ) ) {
        return false
    }

    if ( item.isArmor() && ( lifeStone.grade !== LifeStoneGrade.Accessory ) ) {
        return false
    }


    return player.getLevel() >= LifeStoneCache.getPlayerLevel( lifeStone )
}

export function canAugmentItem( player: L2PcInstance, item: L2ItemInstance, refinerItem: L2ItemInstance, gemStoneItem: L2ItemInstance ) : boolean {
    if ( !isValidForAugmentation( player, item, refinerItem ) ) {
        return false
    }

    if ( gemStoneItem.getOwnerId() !== player.getObjectId() ) {
        return false
    }

    if ( gemStoneItem.getItemLocation() !== ItemLocation.INVENTORY ) {
        return false
    }

    let grade : CrystalType = item.getItem().getItemGrade()
    if ( LifeStoneCache.getGemStoneId( grade ) !== gemStoneItem.getId() ) {
        return false
    }

    let lifeStone : LifeStone = LifeStoneCache.getLifeStone( refinerItem.getId() )
    return LifeStoneCache.getGemStoneCount( grade, lifeStone ) <= gemStoneItem.getCount()
}

function isValidPlayer( player: L2PcInstance ): boolean {
    if ( player.isCursedWeaponEquipped() || player.isEnchanting() ) {
        return false
    }

    if ( player.getPrivateStoreType() !== PrivateStoreType.None ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_AUGMENT_ITEMS_WHILE_A_PRIVATE_STORE_OR_PRIVATE_WORKSHOP_IS_IN_OPERATION ) )
        return false
    }

    if ( player.getActiveTradeList() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_AUGMENT_ITEMS_WHILE_TRADING ) )
        return false
    }

    if ( player.isDead() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_AUGMENT_ITEMS_WHILE_DEAD ) )
        return false
    }

    if ( player.isStunned() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_AUGMENT_ITEMS_WHILE_PARALYZED ) )
        return false
    }

    if ( player.isFishing() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_AUGMENT_ITEMS_WHILE_FISHING ) )
        return false
    }

    if ( player.isSitting() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_AUGMENT_ITEMS_WHILE_SITTING_DOWN ) )
        return false
    }

    return !player.isProcessingTransaction()
}

export function canManipulateItem( player: L2PcInstance, item: L2ItemInstance ): boolean {
    if ( !isValidPlayer( player ) ) {
        return false
    }

    if ( item.getOwnerId() !== player.getObjectId() ) {
        return false
    }

    if ( item.isAugmented() ) {
        return false
    }

    if ( item.isHeroItem() ) {
        return false
    }

    if ( item.isShadowItem() ) {
        return false
    }

    if ( item.isCommonItem() ) {
        return false
    }

    if ( item.isEtcItem() ) {
        return false
    }

    if ( item.isTimeLimitedItem() ) {
        return false
    }

    if ( item.isPvp() && !ConfigManager.character.allowAugmentPvPItems() ) {
        return false
    }
    if ( item.getItem().getCrystalType() < CrystalType.C ) {
        return false
    }

    switch ( item.getItemLocation() ) {
        case ItemLocation.INVENTORY:
        case ItemLocation.PAPERDOLL:
            break
        default:
            return false
    }

    if ( item.isWeapon() ) {
        switch ( ( item.getItem() as L2Weapon ).getItemType() ) {
            case WeaponType.NONE:
            case WeaponType.FISHINGROD:
                return false
            default:
                break
        }
    }

    if ( item.isArmor() ) {
        switch ( item.getItem().getBodyPart() ) {
            case L2ItemSlots.AllFingerSlots:
            case L2ItemSlots.AllEarSlots:
            case L2ItemSlots.Neck:
                break
            default:
                return false
        }
    }

    if ( item.isEtcItem() ) {
        return false
    }

    return !ConfigManager.character.getAugmentationBlacklist().includes( item.getId() )
}