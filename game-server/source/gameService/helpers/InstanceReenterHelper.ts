export enum InstanceReenterDayOfWeek {
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY
}

export interface InstanceReenterTime {
    day: InstanceReenterDayOfWeek
    hour: number
    minute: number
    time: number
}

export const InstanceReenterHelper = {
    fromTime( time: number ) : InstanceReenterTime {
        return {
            day: undefined,
            hour: -1,
            minute: -1,
            time
        }
    },

    fromTimeOptions( day: InstanceReenterDayOfWeek, hour: number, minute: number ) : InstanceReenterTime {
        return {
            day,
            hour,
            minute,
            time: -1
        }
    }
}