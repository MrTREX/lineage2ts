export type AbstractVariableValueType = number | boolean | string | AbstractVariablesMap | Array<AbstractVariableValueType>
export type AbstractVariablesMap = {
    [ key in string | number ]: AbstractVariableValueType
}

export interface AbstractVariablesManager {
    emptyData( objectId: number | string ): void

    get( objectId: number | string, name: string ): AbstractVariableValueType

    getData( objectId: number | string ): AbstractVariablesMap

    has( objectId: number | string, name: string ): boolean

    remove( objectId: number | string, name: string, updateDatabase: boolean ): void

    restoreMe( objectId: number | string ): Promise<void>

    set( objectId: number | string, name: string, value: AbstractVariableValueType, updateDatabase: boolean ): void

    setData( objectId: number | string, value: AbstractVariablesMap, updateDatabase: boolean ): void
}