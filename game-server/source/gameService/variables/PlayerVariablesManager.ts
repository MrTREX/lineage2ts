import { DatabaseManager } from '../../database/manager'
import { BasicVariablesManager } from './BasicVariablesManager'
import _ from 'lodash'
import { AbstractVariablesMap } from './AbstractVariablesManager'
import Timeout = NodeJS.Timeout

class Manager extends BasicVariablesManager {
    protected tasks: { [ objectId: number ]: Timeout } = {}

    async restoreMe( objectId: number ): Promise<void> {
        this.cancelDeletion( objectId )

        if ( this.registry[ objectId ] ) {
            return
        }

        let data = await DatabaseManager.getCharacterVariables().getVariables( objectId )
        if ( _.isObject( data ) ) {
            this.registry[ objectId ] = data as AbstractVariablesMap
        }
    }

    scheduleDeletion( objectId: number | string ): void {
        if ( this.tasks[ objectId ] ) {
            return
        }

        this.tasks[ objectId ] = setTimeout( this.emptyData.bind( this ), 30000, objectId )
    }

    cancelDeletion( objectId: number ): void {
        if ( this.tasks[ objectId ] ) {
            clearTimeout( this.tasks[ objectId ] )
            delete this.tasks[ objectId ]
        }
    }

    protected performStoreUpdate(): Promise<void> {
        return DatabaseManager.getCharacterVariables().setManyVariables( this.registry, this.updateIds )
    }
}

export const PlayerVariablesManager = new Manager()