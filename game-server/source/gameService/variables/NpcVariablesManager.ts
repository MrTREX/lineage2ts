import { BasicVariablesManager } from './BasicVariablesManager'

class Manager extends BasicVariablesManager {
    protected scheduleUpdate() {}
}

export const NpcVariablesManager = new Manager()