import { L2DataApi } from '../../data/interface/l2DataApi'
import { DatabaseManager } from '../../database/manager'
import { L2ServerVariableData, L2ServerVariablesTableItem } from '../../database/interface/ServerVariablesTableApi'
import _, { DebouncedFunc } from 'lodash'
import { ServerLog } from '../../logger/Logger'

class Manager implements L2DataApi {
    variables: { [ name: string ]: L2ServerVariableData }
    debounceSave: DebouncedFunc<() => void>
    variablesToSave: Set<string> = new Set<string>()

    async load(): Promise<Array<string>> {
        this.variables = {}
        this.debounceSave = _.debounce<() => void>( this.runPartialSave.bind( this ), 15000 )
        let currentDate = Date.now()
        let expiredCount = 0

        this.variables = ( await DatabaseManager.getServerVariables().getAll() ).reduce( ( variables: { [ name: string ]: L2ServerVariableData }, item: L2ServerVariablesTableItem ) => {
            if ( currentDate < item.expirationDate || item.expirationDate === 0 ) {
                let parsedData = _.attempt( JSON.parse, item.value )

                variables[ item.name ] = {
                    value: _.isError( parsedData ) ? item.value : parsedData,
                    expirationDate: item.expirationDate,
                }
            } else {
                expiredCount++
            }

            return variables
        }, {} )

        return [
            `ServerVariablesManager : loaded ${ _.size( this.variables ) } variables.`,
            `ServerVariablesManager : ignored ${ expiredCount } expired variables.`,
        ]
    }

    getValue( name: string ): any {
        return _.get( this.variables[ name ], 'value' )
    }

    setValue( name: string, value: any, expirationDate: number = 0 ): void {
        this.variables[ name ] = {
            expirationDate,
            value,
        }

        this.variablesToSave.add( name )
        this.debounceSave()
    }

    async store(): Promise<void> {
        let allData: Array<L2ServerVariablesTableItem> = _.map( this.variables, ( dataItem: L2ServerVariableData, name: string ): L2ServerVariablesTableItem => {
            return {
                expirationDate: dataItem.expirationDate,
                name,
                value: JSON.stringify( dataItem.value ),
            }
        } )

        return DatabaseManager.getServerVariables().addAll( allData )
    }

    async runPartialSave() : Promise<void> {
        let variableNames = Array.from( this.variablesToSave )

        this.variablesToSave.clear()

        let allData: Array<L2ServerVariablesTableItem> = variableNames.reduce( ( allItems: Array<L2ServerVariablesTableItem>, name: string ): Array<L2ServerVariablesTableItem> => {
            let dataItem: L2ServerVariableData = this.variables[ name ]
            if ( dataItem ) {
                allItems.push( {
                    expirationDate: dataItem.expirationDate,
                    name,
                    value: Buffer.isBuffer( dataItem.value ) ? dataItem.value : JSON.stringify( dataItem.value ),
                } )
            }

            return allItems
        }, [] )

        await DatabaseManager.getServerVariables().addAll( allData )
        ServerLog.trace( `ServerVariableManager: updated ${allData.length} variables` )
    }
}

export const ServerVariablesManager = new Manager()