import { AbstractVariablesManager, AbstractVariablesMap, AbstractVariableValueType } from './AbstractVariablesManager'
import _, { DebouncedFunc } from 'lodash'

export class BasicVariablesManager implements AbstractVariablesManager {
    protected registry: { [ objectId: string ]: AbstractVariablesMap } = {}

    protected debounceUpdate: DebouncedFunc<() => void>
    protected updateIds: Set<number | string> = new Set<number | string>()

    constructor() {
        this.debounceUpdate = _.debounce( this.runStoreUpdate.bind( this ), 5000, {
            trailing: true,
            maxWait: 15000,
        } )
    }

    emptyData( objectId: number | string ): void {
        delete this.registry[ objectId ]
    }

    get( objectId: number | string, name: string ): AbstractVariableValueType {
        return _.get( this.registry[ objectId ], name )
    }

    getData( objectId: number | string ): AbstractVariablesMap {
        return this.registry[ objectId ]
    }

    has( objectId: number | string, name: string ): boolean {
        return !_.isUndefined( this.get( objectId, name ) )
    }

    remove( objectId: number | string, name: string ): void {
        _.unset( this.registry, [ objectId, name ] )

        this.scheduleUpdate( objectId )
    }

    async restoreMe( id: number | string ): Promise<void> {}

    set( objectId: number | string, name: string, value: AbstractVariableValueType ): void {
        _.setWith( this.registry, [ objectId, name ], value, Object )

        this.scheduleUpdate( objectId )
    }

    setData( objectId: number | string, value: AbstractVariablesMap ): void {
        this.registry[ objectId ] = value

        this.scheduleUpdate( objectId )
    }

    protected scheduleUpdate( objectId: number | string ) : void {
        this.updateIds.add( objectId )
        this.debounceUpdate()
    }

    async runStoreUpdate() : Promise<void> {
        if ( this.updateIds.size === 0 ) {
            return
        }

        await this.performStoreUpdate()
        this.updateIds.clear()
    }

    protected performStoreUpdate() : Promise<void> {
        return Promise.resolve()
    }
}