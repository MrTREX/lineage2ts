import { TopicType } from '../enums/TopicType'

export interface Topic {
    id: number,
    forumId: number,
    name: string,
    date: number,
    ownerName: string,
    ownerId: number,
    type: TopicType,
    reply: number
}