import { ForumType } from '../enums/ForumType'
import { ForumVisibility } from '../enums/ForumVisibility'
import { Topic } from './Topic'

export interface Forum {
    id: number,
    name: string,
    type: ForumType,
    post: number,
    visibility: ForumVisibility,
    parent: number, // parent forum id
    ownerId: number,
    children: Array<Forum>,
    topics: Array<Topic>
}