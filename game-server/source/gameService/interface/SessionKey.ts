export interface SessionKey {
    playOk1: number,
    playOk2: number,
    loginOk1: number,
    loginOk2: number,
}