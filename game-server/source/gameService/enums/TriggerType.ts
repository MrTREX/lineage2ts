export const TriggerType = {
    // You hit an enemy
    ON_HIT: 1,
    // You hit an enemy - was crit
    ON_CRIT: 2,
    // You cast a skill
    ON_CAST: 4,
    // You cast a skill - it was a physical one
    ON_PHYSICAL: 8,
    // You cast a skill - it was a magic one
    ON_MAGIC: 16,
    // You cast a skill - it was a magic one - good magic
    ON_MAGIC_GOOD: 32,
    // You cast a skill - it was a magic one - offensive magic
    ON_MAGIC_OFFENSIVE: 64,
    // You are attacked by enemy
    ON_ATTACKED: 128,
    // You are attacked by enemy - by hit
    ON_ATTACKED_HIT: 256,
    // You are attacked by enemy - by hit - was crit
    ON_ATTACKED_CRIT: 512,
    // A skill was casted on you
    ON_HIT_BY_SKILL: 1024,
    // An evil skill was casted on you
    ON_HIT_BY_OFFENSIVE_SKILL: 2048,
    // A good skill was casted on you
    ON_HIT_BY_GOOD_MAGIC: 4096,
    // Evading melee attack
    ON_EVADED_HIT: 8192,
    // Effect only - on start
    ON_START: 16384,
    // Effect only - each second
    ON_ACTION_TIME: 32768,
    // Effect only - on exit
    ON_EXIT: 65536
}