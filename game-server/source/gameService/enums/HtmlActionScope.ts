import Chance from 'chance'
import _ from 'lodash'
const chance = new Chance()

export enum HtmlActionScope {
    Npc,
    Item,
    Quest,
    Tutorial,
    CommunityBoard
}

const scopeLetters : Array<string> = chance.unique( chance.letter, _.size( HtmlActionScope ) / 2 )
const scopeValues = _.filter( HtmlActionScope, _.isNumber )

export const HtmlActionScopeToPrefix = _.zipObject( scopeValues, scopeLetters )
export const HtmlActionPrefixToScope = _.zipObject( scopeLetters, scopeValues )