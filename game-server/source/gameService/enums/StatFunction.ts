import { FunctionAdd } from '../models/stats/functions/operations/FunctionAdd'
import { FunctionDivide } from '../models/stats/functions/operations/FunctionDivide'
import { FunctionEnchant } from '../models/stats/functions/operations/FunctionEnchant'
import { FunctionEnchantHp } from '../models/stats/functions/operations/FunctionEnchantHp'
import { FunctionMultiply } from '../models/stats/functions/operations/FunctionMultiply'
import { FunctionSet } from '../models/stats/functions/operations/FunctionSet'
import { FunctionSubtract } from '../models/stats/functions/operations/FunctionSubtract'

export const enum StatFunctionName {
    Add = 'ADD',
    Divide = 'DIV',
    Enchant = 'ENCHANT',
    EnchantHp = 'ENCHANTHP',
    Multiply = 'MUL',
    SetProperty = 'SET',
    Subtract = 'SUB'
}

export const StatFunction = {
    [ StatFunctionName.Add ]: [ FunctionAdd.fromParameters , 30 ],
    [ StatFunctionName.Divide ]: [ FunctionDivide.fromParameters, 20 ],
    [ StatFunctionName.Enchant ]: [ FunctionEnchant.fromParameters, 0 ],
    [ StatFunctionName.EnchantHp ]: [ FunctionEnchantHp.fromParameters, 40 ],
    [ StatFunctionName.Multiply ]: [ FunctionMultiply.fromParameters, 20 ],
    [ StatFunctionName.SetProperty ]: [ FunctionSet.fromParameters, 0 ],
    [ StatFunctionName.Subtract ]: [ FunctionSubtract.fromParameters, 30 ]
}