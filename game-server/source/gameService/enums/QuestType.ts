export enum QuestType {
    REPEATABLE,
    ONE_TIME,
    DAILY
}