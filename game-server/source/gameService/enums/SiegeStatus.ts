export enum SiegeStatus {
    REGISTERING,
    WAITING_BATTLE,
    RUNNING
}