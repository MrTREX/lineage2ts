export const enum ChanceMultiplierStrategy {
    DROP,
    SPOIL,
    STATIC,
    QUEST
}