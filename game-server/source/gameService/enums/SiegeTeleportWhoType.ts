export enum SiegeTeleportWhoType {
    Attacker,
    Owner,
    NotOwner,
    Spectator
}