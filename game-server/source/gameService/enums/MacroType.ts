export enum MacroType {
    NONE,
    SKILL,
    ACTION,
    TEXT,
    SHORTCUT,
    ITEM,
    DELAY
}