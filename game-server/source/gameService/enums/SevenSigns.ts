import { SevenSignsPeriod, SevenSignsSeal, SevenSignsSide } from '../values/SevenSignsValues'

export const enum L2SevenSignsCharacterSide {
    None = 'none',
    Dusk = 'dusk',
    Dawn = 'dawn'
}

export const enum L2SevenSignsCharacterSeal {
    None = 'none',
    Avarice = 'avarice',
    Gnosis = 'gnosis',
    Strife = 'strife',
}

export const SealToEnum : Record<string, SevenSignsSeal> = {
    [ L2SevenSignsCharacterSeal.None ]: SevenSignsSeal.None,
    [ L2SevenSignsCharacterSeal.Strife ]: SevenSignsSeal.Strife,
    [ L2SevenSignsCharacterSeal.Gnosis ]: SevenSignsSeal.Gnosis,
    [ L2SevenSignsCharacterSeal.Avarice ]: SevenSignsSeal.Avarice
}

export const EnumToSeal : Record<number, string> = {
    [ SevenSignsSeal.None ]: L2SevenSignsCharacterSeal.None,
    [ SevenSignsSeal.Strife ]: L2SevenSignsCharacterSeal.Strife,
    [ SevenSignsSeal.Gnosis ]: L2SevenSignsCharacterSeal.Gnosis,
    [ SevenSignsSeal.Avarice ]: L2SevenSignsCharacterSeal.Avarice
}

export const SideToEnum : Record<string, SevenSignsSide> = {
    [ L2SevenSignsCharacterSide.None ]: SevenSignsSide.None,
    [ L2SevenSignsCharacterSide.Dusk ]: SevenSignsSide.Dusk,
    [ L2SevenSignsCharacterSide.Dawn ]: SevenSignsSide.Dawn
}

export const EnumToSide : Record<number, string> = {
    [ SevenSignsSide.None ]: L2SevenSignsCharacterSide.None,
    [ SevenSignsSide.Dusk ]: L2SevenSignsCharacterSide.Dusk,
    [ SevenSignsSide.Dawn ]: L2SevenSignsCharacterSide.Dawn
}

export const PeriodNames : Record<number, string> = {
    [ SevenSignsPeriod.Recruiting ]: 'Quest Event Initialization',
    [ SevenSignsPeriod.Competing ]: 'Competition (Quest Event)',
    [ SevenSignsPeriod.CompetitionResults ]: 'Quest Event Results',
    [ SevenSignsPeriod.SealValidation ]: 'Seal Validation'
}

export const SideToName : Record<number, string> = {
    [ SevenSignsSide.Dawn ]: 'Lords of Dawn',
    [ SevenSignsSide.Dusk ]: 'Revolutionaries of Dusk',
    [ SevenSignsSide.None ]: 'None'
}

export const SideToShortName : Record<number, string> = {
    [ SevenSignsSide.Dawn ]: 'Dawn',
    [ SevenSignsSide.Dusk ]: 'Dusk',
    [ SevenSignsSide.None ]: 'None'
}

export const NormalSealName : Record<number, string> = {
    [ SevenSignsSeal.Avarice ]: 'Seal of Avarice',
    [ SevenSignsSeal.Gnosis ]: 'Seal of Gnosis',
    [ SevenSignsSeal.Strife ]: 'Seal of Strife',
}

export const ShortSealName : Record<number, string> = {
    [ SevenSignsSeal.Avarice ]: 'Avarice',
    [ SevenSignsSeal.Gnosis ]: 'Gnosis',
    [ SevenSignsSeal.Strife ]: 'Strife',
}