export enum TeleportWhereType {
    CASTLE,
    CLANHALL,
    SIEGEFLAG,
    TOWN,
    FORTRESS
}