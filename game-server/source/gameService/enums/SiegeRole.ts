export const enum SiegeRole {
    None,
    Attacker,
    Defender
}