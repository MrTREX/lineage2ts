export enum PlayerPermission {
    /** Ability to execute admin commands */
    IsGm,

    /** Attack forced attack or attack of characters not normally being attacked */
    AllowPeaceAttack,

    /** Fixed resurrection */
    AllowFixedRes,

    /** Player to player transactions */
    AllowTransaction,

    /** AltG command usage */
    AllowAltG,

    /** Give damage */
    GiveDamage,

    /** Take aggro from monsters*/
    TakeAggro,

    /** Gain exp in party */
    GainExp,

}

export const AllPermissions : Set<PlayerPermission> = new Set<PlayerPermission>( [
    PlayerPermission.IsGm,
    PlayerPermission.AllowAltG,
    PlayerPermission.AllowFixedRes,
    PlayerPermission.AllowPeaceAttack,
    PlayerPermission.GainExp,
    PlayerPermission.AllowTransaction,
    PlayerPermission.GiveDamage,
    PlayerPermission.TakeAggro
] )