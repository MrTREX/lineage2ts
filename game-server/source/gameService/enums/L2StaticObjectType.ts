export const enum L2StaticObjectType {
    None = -1,
    MapSign = 0,
    Throne = 1,
    ArenaSign = 2,
    FortFlagpole
}