export const enum SiegeClanType {
    Attacker = 0,
    Owner = 1,
    DefenderPending = 2,
    DefenderConfirmed = 3
}