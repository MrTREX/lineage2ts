export const enum FishGroup {
    Fat,
    Nimble,
    Ugly,
    TreasureChest,
    FatBeginners,
    NimbleBeginners,
    UglyBeginners,
    FatUpperGrade,
    NimbleUpperGrade,
    UglyUpperGrade,
    OldBox
}