export const enum LifeStoneGrade {
    None = 0,
    Mid = 1,
    High = 2,
    Top = 3,
    Accessory = 4,
    GemstoneD = 2130,
    GemstoneC = 2131,
    GemstoneB = 2132,
}