export const enum AmountMultiplierStrategy {
    DROP,
    SPOIL,
    STATIC
}