/*
    Please note that enum values are part of ExBrProductList packet
 */
export const enum ProductCategory {
    Enchant = 1,
    Supplies = 2,
    Decoration = 3,
    Package = 4,
    Other = 5,
    Event = 6,
    Best = 7,
    EventAndBest = 8
}