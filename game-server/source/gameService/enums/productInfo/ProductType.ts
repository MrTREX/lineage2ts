/*
    Please note that enum values are part of ExBrProductList packet
 */
export const enum ProductType {
    Normal = 0,
    Event = 1,
    Best = 2,
    NewProduct = 3
}