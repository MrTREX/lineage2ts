export enum TrapAction {
    TRAP_TRIGGERED,
    TRAP_DETECTED,
    TRAP_DISARMED
}