export const enum ItemType2 {
    WEAPON,
    SHIELD_ARMOR,
    ACCESSORY,
    QUEST,
    MONEY,
    OTHER
}