import { WeaponTypeMap } from '../../models/items/type/WeaponType'
import _ from 'lodash'

export enum ArmorType {
    NONE,
    LIGHT,
    HEAVY,
    MAGIC,
    SIGIL,
    SHIELD
}

const armorMaskBase = Object.keys( WeaponTypeMap ).length
const ArmorTypeMaskValues : Array<number> = _.times( armorMaskBase, ( value: number ) => {
    return 1 << ( value + armorMaskBase )
} )

export function ArmorTypeMask( value: ArmorType ) {
    return ArmorTypeMaskValues[ value ]
}