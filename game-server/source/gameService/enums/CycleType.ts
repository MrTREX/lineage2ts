export enum CycleType {
    None,
    Hellbound,
    SeedOfDestruction,
    SeedOfInfinity,
    SOABistakon,
    SOACokracon,
    SOAReptilikon,
}