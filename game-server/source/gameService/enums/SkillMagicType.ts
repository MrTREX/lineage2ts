export const enum SkillMagicType {
    PhysicalCast,
    MagicCast,
    StaticAbility,
    Dance,
    Trigger
}