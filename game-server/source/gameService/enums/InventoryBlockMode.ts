/**
 * Block modes:
 * -1 - no block
 * 0 - block items from normal inventory, allow usage of other items
 * 1 - allow usage of items from inventory, block other items
 * 2 - disallow usage of specific items
 */
export const enum InventoryBlockMode {
    None = -1,
    InventoryDisallowed = 0,
    AllowedSome = 1,
    DisallowedSome = 2
}