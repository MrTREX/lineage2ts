export const enum CommonSkillIds {
    WeaponGradePenalty = 6209,
    ArmorGradePenalty = 6213,
    CreateDwarvenItems = 172,
    CreateCommonItems = 1320,
    CrystallizeItems = 248,
    ServitorShare = 1557,
    CaravanSecretMedicine = 2341,
    OnyxBeastTransformation = 617,
    DivineInspiration = 1405
}