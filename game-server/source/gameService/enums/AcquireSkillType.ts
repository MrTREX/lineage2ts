export const enum AcquireSkillType {
    Class,
    Fishing,
    Pledge,
    SubPledge,
    Transform,
    Transfer,
    Subclass,
    Collect
}