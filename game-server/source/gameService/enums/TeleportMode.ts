export const enum TeleportMode {
    None,
    Single,
    Repeated
}