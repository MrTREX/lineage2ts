export enum L2ItemUpdateStatus {
    Create,
    Delete,
    Update,
    Undecided
}