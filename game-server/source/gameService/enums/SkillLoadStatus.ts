export const enum SkillLoadStatus {
    Initialized,
    Loading,
    Finished
}