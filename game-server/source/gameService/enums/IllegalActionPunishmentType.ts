export enum IllegalActionPunishmentType {
    NONE,
    BROADCAST,
    KICK,
    KICKBAN,
    JAIL
}