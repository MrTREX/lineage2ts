export const enum FortFeatureType {
    Reinforcements,
    Supplies,
    Doors,
    Cannons,
    Scouts
}