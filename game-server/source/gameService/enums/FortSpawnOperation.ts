/*
    Value is derived from isSpawn1, isSpawn2 and isSpawn3 variables on maker.
    Code indicates that:
    - 0 is used for despawn
    - 1 is used for spawn
    - 3 is used for door operations\
 */
export const enum FortSpawnOperation {
    Despawn,
    Normal,
    UnUsed,
    Doors
}