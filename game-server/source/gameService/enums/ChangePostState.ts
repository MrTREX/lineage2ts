export const enum ChangePostState {
    Deleted,
    Read,
    Rejected,
}