import { DataManager } from '../../data/manager'
import { CategoryType } from './CategoryType'

// TODO : convert to const enum, requires changes to skill data since string values are used
export enum MountType {
    NONE,
    STRIDER,
    WYVERN,
    WOLF
}

export const MountTypeHelper = {
    findByNpcId( npcId: number ) : MountType {
        let data = DataManager.getCategoryData()
        if ( data.isInCategory( CategoryType.STRIDER, npcId ) ) {
            return MountType.STRIDER
        }

        if ( data.isInCategory( CategoryType.WYVERN_GROUP, npcId ) ) {
            return MountType.WYVERN
        }

        if ( data.isInCategory( CategoryType.WOLF_GROUP, npcId ) ) {
            return MountType.WOLF
        }

        return MountType.NONE
    },

    isMountable( npcId: number ) : boolean {
        return this.findByNpcId( npcId ) !== MountType.NONE
    }
}