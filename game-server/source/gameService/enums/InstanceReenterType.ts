export enum InstanceReenterType {
    NONE,
    ON_INSTANCE_ENTER,
    ON_INSTANCE_FINISH,
}