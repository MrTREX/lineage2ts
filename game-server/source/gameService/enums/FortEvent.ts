export const enum FortEvent {
    None = 0,
    SiegeAtGates = 1,
    SiegeGatesBreached = 2,
    SiegeGuardiansDefeated = 3,
    SiegeEnded = 4,
    DefendedByOwner = 5,
    ToggleDoors = 6,
    RegistrationForMercs = 7
}