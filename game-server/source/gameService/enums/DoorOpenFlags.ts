export const enum DoorOpenFlags {
    Click = 1,
    Time = 2,
    Item = 4,
    Skill = 8,
    Cycle = 16,
}