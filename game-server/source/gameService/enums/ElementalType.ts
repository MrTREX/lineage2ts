export const enum ElementalType {
    NONE = -1,
    FIRE = 0,
    WATER = 1,
    WIND = 2,
    EARTH = 3,
    HOLY = 4,
    DARK = 5,
}

export const DefenceElementTypes : Array<ElementalType> = [ ElementalType.FIRE, ElementalType.WATER, ElementalType.WIND, ElementalType.EARTH, ElementalType.HOLY, ElementalType.DARK ]
export const DefaultDefenceElementAttributes = DefenceElementTypes.map( () => 0 )