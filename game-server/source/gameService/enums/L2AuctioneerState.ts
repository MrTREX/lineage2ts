export const enum L2AuctioneerState {
    Disabled,
    Siege,
    Normal
}