export const enum SpawnLogicEventType {
    Spawn,
    Despawn,
    DespawnTimer,
    SignalParameters,
}