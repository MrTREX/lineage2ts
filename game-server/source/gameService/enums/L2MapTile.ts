export const enum L2MapTile {
    XMin = 11,
    YMin = 10,
    XMax = 26,
    YMax = 26,

    /*
        Same value for size, however used differently depending on situation to avoid usage of Math.floor for division.
     */
    Size = 32768,
    SizeShift = 15
}

export const enum L2WorldLimits {
    MinX = ( L2MapTile.XMin - 20 ) * L2MapTile.Size,
    MaxX = ( L2MapTile.XMax - 19 ) * L2MapTile.Size,

    MinY = ( L2MapTile.YMin - 18 ) * L2MapTile.Size,
    MaxY = ( L2MapTile.YMax - 17 ) * L2MapTile.Size,
}

export function getRegionKey( x: number, y: number ) : string {
    return `${x}_${y}`
}

export function getRegionCode( tileX: number, tileY: number ) : number {
    return ( tileX << 8 ) + tileY
}

export function getNameFromCode( code: number ) : string {
    let tileX = ( 0xFFFFFF00 & code ) >> 8
    let tileY = 0x000000FF & code

    return getRegionKey( tileX, tileY )
}

export function getGeoRegionCode( x: number, y: number ) : number {
    let xDifference = x - L2WorldLimits.MinX
    let xTile = ( xDifference >> L2MapTile.SizeShift ) + L2MapTile.XMin
    let yDifference = y - L2WorldLimits.MinY
    let yTile = ( yDifference >> L2MapTile.SizeShift ) + L2MapTile.YMin

    return getRegionCode( xTile, yTile )
}