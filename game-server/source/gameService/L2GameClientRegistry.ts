import { GameClient } from './GameClient'
import _ from 'lodash'

class ClientRegistry {
    playerToClientMap: { [ playerId: number ]: GameClient } = {}
    accountNameToClient: { [ name: string ]: GameClient } = {}
    ipToClientMap: Record<string, Array<GameClient>> = {}

    addClient( client: GameClient ) : void {
        this.accountNameToClient[ client.getAccountName() ] = client

        if ( !this.ipToClientMap[ client.connection.remoteAddress ] ) {
            this.ipToClientMap[ client.connection.remoteAddress ] = [ client ]

            return
        }

        this.ipToClientMap[ client.connection.remoteAddress ].push( client )
    }

    getClientByPlayerId( playerId: number ): GameClient {
        return this.playerToClientMap[ playerId ]
    }

    getClientByAccount( name: string ): GameClient {
        return this.accountNameToClient[ name ]
    }

    removeClient( client: GameClient ) {
        delete this.accountNameToClient[ client.getAccountName() ]
        _.pull( this.ipToClientMap[ client.connection.remoteAddress ], client )
    }

    removePlayerAssociation( playerId: number ): void {
        _.unset( this.playerToClientMap, playerId )
    }

    addPlayerAssociation( playerId: number, client: GameClient ): void {
        this.playerToClientMap[ playerId ] = client
    }

    getClientByIpd( ip: string ): Array<GameClient> {
        return this.ipToClientMap[ ip ]
    }
}

export const L2GameClientRegistry = new ClientRegistry()