import _ from 'lodash'

export const enum PlayerActionOverride {
    MaximumStat = 1 << 0,
    ItemAction = 1 << 1,
    SkillAction = 1 << 2,
    ZoneRequirements = 1 << 3,
    CastleRights = 1 << 4,
    FortressAccess = 1 << 5,
    ClanHallAccess = 1 << 6,
    ChatAccess = 1 << 7,
    InstanceChecks = 1 << 8,
    QuestRequirements = 1 << 9,
    DeathPenalty = 1 << 10,
    DestroyItem = 1 << 11,
    SeeInvisible = 1 << 12,
    TargetBlock = 1 << 13,
    DropItem = 1 << 14,
    PetCommands = 1 << 15,
    BlockedActionPenalty = 1 << 16
}

export const allExceptions: number = _.range( 20 ).reduce( ( finalValue: number, value: number ) => {
    return finalValue | ( 1 << value )
}, 0 )