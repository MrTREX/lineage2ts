import { SevenSignsSide } from './SevenSignsValues'

export const enum SevenSignsFestivalLevel {
    Max31 = 1,
    Max42 = 2,
    Max53 = 3,
    Max64 = 4,
    Unlimited = 5,
}

export const AllFestivalLevels = [
    SevenSignsFestivalLevel.Max31,
    SevenSignsFestivalLevel.Max42,
    SevenSignsFestivalLevel.Max53,
    SevenSignsFestivalLevel.Max64,
    SevenSignsFestivalLevel.Unlimited
]

export const FestivalLevelScores : Record<number, number> = {
    [ SevenSignsFestivalLevel.Max31 ]: 60,
    [ SevenSignsFestivalLevel.Max42 ]: 70,
    [ SevenSignsFestivalLevel.Max53 ]: 100,
    [ SevenSignsFestivalLevel.Max64 ]: 120,
    [ SevenSignsFestivalLevel.Unlimited ]: 150,
}

export interface SevenSignsFestivalData {
    level: number
    side: SevenSignsSide
    date: number
    score: number
    members: Array<number>
    rewardedMembers: Array<number>
}

export const enum SevenSignsFestivalVariables {
    DuskScores = 'SSF.DuskScoresV1',
    DawnScores = 'SSF.DawnScoresV1',
    HistoricalData = 'SSF.TopScoresV1',
    Bonuses = 'SSF.BonusesV1'
}