export const enum SevenSignsValues {
    PERIOD_START_HOUR = 18,
    PERIOD_START_MINS = 0,
    PERIOD_START_DAY = 1,
    SEVEN_SIGNS_HTML_PATH = 'data/html/seven_signs/',

    SEAL_STONE_BLUE_ID = 6360,
    SEAL_STONE_GREEN_ID = 6361,
    SEAL_STONE_RED_ID = 6362,

    SEAL_STONE_BLUE_VALUE = 3,
    SEAL_STONE_GREEN_VALUE = 5,
    SEAL_STONE_RED_VALUE = 10,

    RECORD_SEVEN_SIGNS_ID = 5707,
    RECORD_SEVEN_SIGNS_COST = 500,

    BLUE_CONTRIB_POINTS = 3,
    GREEN_CONTRIB_POINTS = 5,
    RED_CONTRIB_POINTS = 10,

    StatusVariableName = 'SevenSignsStatusV1',
    FestivalStatusVariableName = 'SevenSignsFestivalStatusV1',
}

export const enum SevenSignsSeal {
    None = 0,
    Avarice = 1,
    Gnosis = 2,
    Strife = 3,
}

export const enum SevenSignsSide {
    None = 0,
    Dusk = 1,
    Dawn = 2
}

export const enum SevenSignsPeriod {
    Recruiting = 0,
    Competing = 1,
    CompetitionResults = 2,
    SealValidation = 3,
}

export const AllSeals : Array<SevenSignsSeal> = [
    SevenSignsSeal.Avarice,
    SevenSignsSeal.Gnosis,
    SevenSignsSeal.Strife
]

export const AllSides : Array<SevenSignsSide> = [
    SevenSignsSide.Dawn,
    SevenSignsSide.Dusk
]