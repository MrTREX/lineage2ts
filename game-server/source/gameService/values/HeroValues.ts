export interface HeroPlayerData {
    objectId: number
    name: string
    classId: number
    clanName: string
    clanCrestId: number
    allyName: string
    allyCrestId: number
    count: number
    isClaimed: boolean
    played: number
}

export interface HeroDiaryEntry {
    date: string
    action: string
}