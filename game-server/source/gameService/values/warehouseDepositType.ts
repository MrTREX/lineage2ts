export const enum WarehouseDepositType {
    Private = 1,
    Clan = 2,
    Castle = 3,
    Normal = 4
}