export const enum L2ClanValues {
    // Ally Penalty Types
    /** Clan left ally */
    PENALTY_TYPE_CLAN_LEFT = 1,
    /** Clan was dismissed from ally */
    PENALTY_TYPE_CLAN_DISMISSED = 2,
    /** Leader clan dismiss clan from ally */
    PENALTY_TYPE_DISMISS_CLAN = 3,
    /** Leader clan dissolve ally */
    PENALTY_TYPE_DISSOLVE_ALLY = 4,
    // Sub-unit types
    /** Clan subunit type of Academy */
    SUBUNIT_ACADEMY = -1,
    /** Clan subunit type of Royal Guard A */
    SUBUNIT_ROYAL1 = 100,
    /** Clan subunit type of Royal Guard B */
    SUBUNIT_ROYAL2 = 200,
    /** Clan subunit type of Order of Knights A-1 */
    SUBUNIT_KNIGHT1 = 1001,
    /** Clan subunit type of Order of Knights A-2 */
    SUBUNIT_KNIGHT2 = 1002,
    /** Clan subunit type of Order of Knights B-1 */
    SUBUNIT_KNIGHT3 = 2001,
    /** Clan subunit type of Order of Knights B-2 */
    SUBUNIT_KNIGHT4 = 2002,
    SUBUNIT_NONE = 0,

    CLAN_NAME_MAX_LENGTH = 16
}