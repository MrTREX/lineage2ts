import _ from 'lodash'

export enum WarehouseWithdrawSort {
    A2Z = 1,
    Z2A = -1,
    GRADE = 2,
    LEVEL = 3,
    TYPE = 4,
    WEAR = 5,
}

export function getWarehouseWithdrawSortOrder( order: string ): WarehouseWithdrawSort {
    if ( !order ) {
        return WarehouseWithdrawSort.A2Z
    }

    let sort = WarehouseWithdrawSort[ order ]
    if ( sort ) {
        return sort
    }

    let value = parseInt( order, 10 )
    if ( !_.isNumber( value ) ) {
        return WarehouseWithdrawSort.A2Z
    }

    return value
}