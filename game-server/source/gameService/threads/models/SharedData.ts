import { InstanceType } from '../../enums/InstanceType'

const enum PropertyPosition {
    objectId = 0,
    currentX = 4,
    currentY = 8,
    currentZ = 12,
    destinationX = 16,
    destinationY = 20,
    destinationZ = 24,
    moveSpeed = 28,
    collisionRaidus = 32,
    heading = 36,
    movementId = 40,
    type = 44,
    dataFlags = 45
}

export const enum DataFlag {
    Moving = 1,
    Flying = 2,
    InWater = 4,
    Visible = 8,
    InCombat = 16,
    MovingAborted = 32
}

export class SharedData {
    buffer: SharedArrayBuffer
    view: DataView

    getObjectId(): number {
        return this.view.getUint32( PropertyPosition.objectId )
    }

    setObjectId( value: number ): void {
        this.view.setUint32( PropertyPosition.objectId, value )
    }

    getX(): number {
        return this.view.getInt32( PropertyPosition.currentX )
    }

    setX( value: number ): void {
        this.view.setInt32( PropertyPosition.currentX, value )
    }

    getY(): number {
        return this.view.getInt32( PropertyPosition.currentY )
    }

    setY( value: number ): void {
        this.view.setInt32( PropertyPosition.currentY, value )
    }

    getZ(): number {
        return this.view.getInt32( PropertyPosition.currentZ )
    }

    setZ( value: number ): void {
        this.view.setInt32( PropertyPosition.currentZ, value )
    }

    getDestinationX(): number {
        return this.view.getInt32( PropertyPosition.destinationX )
    }

    setDestinationX( value: number ): void {
        this.view.setInt32( PropertyPosition.destinationX, value )
    }

    getDestinationY(): number {
        return this.view.getInt32( PropertyPosition.destinationY )
    }

    setDestinationY( value: number ): void {
        this.view.setInt32( PropertyPosition.destinationY, value )
    }

    getDestinationZ(): number {
        return this.view.getInt32( PropertyPosition.destinationZ )
    }

    setDestinationZ( value: number ): void {
        this.view.setInt32( PropertyPosition.destinationZ, value )
    }

    getMoveSpeed(): number {
        return this.view.getFloat32( PropertyPosition.moveSpeed )
    }

    setMoveSpeed( value: number ): void {
        this.view.setFloat32( PropertyPosition.moveSpeed, value )
    }

    getCollisionRadius() : number {
        return this.view.getFloat32( PropertyPosition.collisionRaidus )
    }

    setCollisionRadius( value: number ): void {
        this.view.setFloat32( PropertyPosition.collisionRaidus, value )
    }

    getHeading(): number {
        return this.view.getUint32( PropertyPosition.heading )
    }

    setHeading( value: number ): void {
        this.view.setUint32( PropertyPosition.heading, value )
    }

    getMovementId(): number {
        return this.view.getUint32( PropertyPosition.movementId )
    }

    setMovementId( value: number ): void {
        this.view.setUint32( PropertyPosition.movementId, value )
    }

    getDataFlags(): number {
        return this.view.getUint32( PropertyPosition.dataFlags )
    }

    setDataFlags( value: number ): void {
        this.view.setUint32( PropertyPosition.dataFlags, value )
    }

    getType() : InstanceType {
        return this.view.getUint8( PropertyPosition.type )
    }

    setType( value: InstanceType ) : void {
        this.view.setUint8( PropertyPosition.type, value )
    }

    hasDataFlag( value: DataFlag ): boolean {
        return ( this.getDataFlags() & value ) === value
    }

    addDataFlag( value: DataFlag ): void {
        this.setDataFlags( this.getDataFlags() | value )
    }

    removeDataFlag( value: DataFlag ): void {
        this.setDataFlags( this.getDataFlags() & ~value )
    }

    getBuffer(): SharedArrayBuffer {
        return this.buffer
    }

    static fromBuffer( value: SharedArrayBuffer ): SharedData {
        let data = new SharedData()
        data.buffer = value
        data.view = new DataView( data.buffer )

        return data
    }

    static create(): SharedData {
        let data = new SharedData()

        data.buffer = new SharedArrayBuffer( PropertyPosition.dataFlags + 4 )
        data.view = new DataView( data.buffer )

        return data
    }
}

