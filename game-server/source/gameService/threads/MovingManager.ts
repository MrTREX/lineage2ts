import { expose } from 'threads/worker'
import { IMovableInformation, MoveData, MoveState } from './models/IMovableInformation'
import { IMovingData, IMovingManager, MovingThreadVariables } from './models/IMovingManager'
import { DataFlag, SharedData } from './models/SharedData'
import { GeoDataManager } from './GeoDataManager'
import { ObjectPool } from '../helpers/ObjectPoolHelper'
import { PolygonValues } from '../../geodata/enums/PolygonValues'
import { InstanceType } from '../enums/InstanceType'

const channel = new BroadcastChannel( MovingThreadVariables.StopChannel )
const registry: { [ movementId: number ]: IMovableInformation } = {}

// TODO : remove wrapper once movement logic is stable enough
function updateWrapper( objectId: number ) {
    try {
        MovingManager.updatePosition( objectId )
    } catch ( error ) {
        console.error( error )
    }
}

const moveDataCache : ObjectPool<MoveData> = new ObjectPool<MoveData>( 'MoveData', (): MoveData => {
    return {
        lastTime: 0,
        lastX: 0,
        lastY: 0,
        counter: 0,
        state: MoveState.Default,
        waiting: 0
    }
} )

function notifyMovementFinished( id: IMovingData ) : void {
    return channel.postMessage( id )
}

/*
    TODO : convert to vector movement
    - having start and destination, with start time, compute when timer would hit arrival (to stop movement)
    - on cancel movement, compute current position based on start and end time
    - listen on speed change event so you can re-compute stop movement timer
 */
const MovingManager: IMovingManager = {
    revalidateZValues( movementIds: Array<number> ): void {
        for ( const movementId of movementIds ) {
            let data = registry[ movementId ]
            if ( !data ) {
                continue
            }

            data.sharedData.setZ( GeoDataManager.getZ( data.sharedData.getX(), data.sharedData.getY(), data.sharedData.getZ() ) )
        }
    },

    startMovement( movementId: number ): void {
        let data: IMovableInformation = registry[ movementId ]

        if ( !data ) {
            console.log( 'Cannot start movement without shared data! movementId =', movementId )
            return
        }

        if ( data.move ) {
            console.log( 'Attempted to perform move on already moving character, movementId =', movementId )
            return
        }

        data.move = moveDataCache.getValue()

        data.move.lastTime = Date.now()
        data.move.lastX = data.sharedData.getX()
        data.move.lastY = data.sharedData.getY()
        data.move.counter = 0

        let hasDelayedFinish = data.sharedData.getType() === InstanceType.L2PcInstance
        data.move.state = hasDelayedFinish ? MoveState.ApplyFinishDelay : MoveState.Default
        data.move.waiting = 1

        /*
            TODO : add route planning
            We need to outline path of 3D points where character can move:
            - calculate path that is closest to destination (destination can be unreachable, pick a point in straight line reachable destination)
            - stop movement if destination has Z value bigger than possible (think client view, you can go down, but you cannot go up too much)
            - at each arrival point (path segment), send event to main thread to notify character movement (stored in shared data destination point)
            - stop movement event is sent if path is reached or abnormal situation occurs (such as crashing)
         */
        let previousZ = data.sharedData.getDestinationZ()
        let correctedZ = GeoDataManager.getZ( data.sharedData.getDestinationX(), data.sharedData.getDestinationY(), previousZ )

        data.sharedData.setDestinationZ( correctedZ )
        data.task = setInterval( updateWrapper, 100, movementId )

        setImmediate( updateWrapper, movementId )
    },

    registerObject( buffer: SharedArrayBuffer ): void {
        let data = SharedData.fromBuffer( buffer )

        registry[ data.getMovementId() ] = {
            sharedData: data,
            move: null,
            task: null,
        }
    },

    unRegisterObject( movementId: number ): void {
        let data: IMovableInformation = registry[ movementId ]

        if ( data.task ) {
            clearInterval( data.task )
        }

        if ( data.move ) {
            moveDataCache.recycleValue( data.move )
            data.move = null
        }

        delete registry[ movementId ]
    },

    cancelMovement( movementId: number ): void {
        let data: IMovableInformation = registry[ movementId ]

        if ( data ) {
            clearInterval( data.task )

            if ( data.move ) {
                moveDataCache.recycleValue( data.move )
            }

            data.move = null
        }
    },

    updatePosition( movementId: number ): void {
        let data: IMovableInformation = registry[ movementId ]

        if ( !data.sharedData.hasDataFlag( DataFlag.Visible ) ) {
            this.cancelMovement( movementId )
            return notifyMovementFinished( movementId )
        }

        /*
            If we have been marked as not moving we must stop,
            however we do not have to notify parent thread since
            most likely parent thread terminated movement in first place.
            The only thing we can do is clean up and wait until movement
            starts again.
         */
        if ( !data.sharedData.hasDataFlag( DataFlag.Moving ) ) {
            return this.cancelMovement( movementId )
        }

        /*
            Special case where we must wait for last tick to synchronize movement in client.
            Movement on server occurs in tick intervals that can pose situation, where:
            - next tick resolves to destination position
            - client position is catching up at this time
            - movement finish results in StopMove packet, terminating ANY movement on client
            - server already finished movement, while client did not arrive at it yet
         */
        if ( data.move.state === MoveState.FinishingMove ) {
            if ( data.move.waiting > 0 ) {
                data.move.waiting--
                return
            }

            this.cancelMovement( movementId )
            return notifyMovementFinished( movementId )
        }

        let currentTime = Date.now()
        let currentZ = data.sharedData.getZ()

        let differenceX = data.sharedData.getDestinationX() - data.move.lastX
        let differenceY = data.sharedData.getDestinationY() - data.move.lastY
        let differenceZ = data.sharedData.getDestinationZ() - currentZ

        let distanceToTravel = Math.pow( differenceX, 2 ) + Math.pow( differenceY, 2 )
        let squaredZ = Math.pow( differenceZ, 2 )

        let noZAdjustment = !( data.sharedData.hasDataFlag( DataFlag.InWater ) || data.sharedData.hasDataFlag( DataFlag.Flying ) )
        if ( distanceToTravel < 10000
                && squaredZ > 2500
                && noZAdjustment ) {
            distanceToTravel = Math.sqrt( distanceToTravel )
        } else {
            distanceToTravel = Math.sqrt( distanceToTravel + squaredZ )
        }

        let distancePerTick = ( data.sharedData.getMoveSpeed() * ( currentTime - data.move.lastTime ) ) / 1000
        if ( distanceToTravel < distancePerTick ) {
            data.sharedData.setX( data.sharedData.getDestinationX() )
            data.sharedData.setY( data.sharedData.getDestinationY() )
            data.sharedData.setZ( data.sharedData.getDestinationZ() )

            if ( data.move.state === MoveState.ApplyFinishDelay && data.move.waiting > 0 ) {
                data.move.state = MoveState.FinishingMove
                return
            }

            this.cancelMovement( movementId )
            return notifyMovementFinished( movementId )
        }

        let distanceDelta = distancePerTick / distanceToTravel

        data.move.counter++

        data.move.lastX += differenceX * distanceDelta
        data.move.lastY += differenceY * distanceDelta

        data.sharedData.setX( data.move.lastX )
        data.sharedData.setY( data.move.lastY )

        let approximateZ = currentZ + Math.round( differenceZ * distanceDelta )
        if ( ( data.move.counter % 5 ) === 1 ) {
            let correctedZ = GeoDataManager.getZ( data.move.lastX, data.move.lastY, approximateZ )

            /*
                Due to absence of pathfinding it is possible to "fall under textures" when
                geo-data does not find accurate Z position, represented by negative 16-bit integer.
                In such case we must preserve current Z value and hope for best.
             */
            if ( correctedZ !== PolygonValues.LowestHeight ) {
                if ( noZAdjustment && approximateZ <= correctedZ ) {
                    correctedZ = approximateZ
                }

                data.sharedData.setZ( correctedZ )
            }
        } else {
            data.sharedData.setZ( approximateZ )
        }

        data.move.lastTime = currentTime
    }
}

expose( {
    startMovement: MovingManager.startMovement.bind( MovingManager ),
    cancelMovement: MovingManager.cancelMovement.bind( MovingManager ),
    registerObject: MovingManager.registerObject.bind( MovingManager ),
    unRegisterObject: MovingManager.unRegisterObject.bind( MovingManager ),
    registerGeoData: GeoDataManager.registerGeoData.bind( GeoDataManager ),
    unRegisterGeoData: GeoDataManager.unRegisterGeoData.bind( GeoDataManager ),
    revalidateZValues: MovingManager.revalidateZValues.bind( MovingManager )
} )