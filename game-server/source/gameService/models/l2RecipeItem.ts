export interface L2RecipeItem {
    itemId: number
    quantity: number
}