export enum CrestType {
    PLEDGE = 1,
    PLEDGE_LARGE = 2,
    ALLY = 3
}

export class L2Crest {
    id: number
    imageData: Buffer
    type: CrestType

    getId() {
        return this.id
    }

    getData() {
        return this.imageData
    }

    getType() {
        return this.type
    }
}