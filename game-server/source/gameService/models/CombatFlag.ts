import { L2ItemInstance } from './items/instance/L2ItemInstance'
import { Location } from './Location'
import { L2World } from '../L2World'
import { ItemManagerCache } from '../cache/ItemManagerCache'
import { L2PcInstance } from './actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../packets/send/SystemMessage'
import { SystemMessageIds } from '../packets/SystemMessageIdValues'
import { getSlotFromItem } from './itemcontainer/ItemSlotHelper'
import { L2ItemSlots } from '../enums/L2ItemSlots'

export class CombatFlag {
    playerId: number = 0
    itemId: number
    itemInstance: L2ItemInstance
    location: Location

    spawnMe() {
        this.itemInstance = ItemManagerCache.createItem( this.itemId, 'CombatFlag.spawnMe', 1, 0 )
        this.itemInstance.dropMe( 0, this.location.getX(), this.location.getY(), this.location.getZ(), 0, false )
    }

    async dropIt() : Promise<void> {
        let player = L2World.getPlayer( this.playerId )

        player.setCombatFlagEquipped( false )

        let item : L2ItemInstance = L2World.getObjectById( this.itemId ) as L2ItemInstance
        let slot : L2ItemSlots = getSlotFromItem( item )
        await player.getInventory().unEquipItemInBodySlot( slot )
        await player.destroyItem( item, true, 'CombatFlag.dropIt' )
        player.broadcastUserInfo()

        this.itemId = null
        this.playerId = 0
    }

    getPlayerObjectId() {
        return this.playerId
    }

    getInstance() {
        return this.itemInstance
    }

    async activate( player: L2PcInstance, item: L2ItemInstance ) : Promise<void> {
        if ( player.isMounted() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_EQUIP_ITEM_DUE_TO_BAD_CONDITION ) )
            return
        }

        this.playerId = player.getObjectId()
        this.itemInstance = null

        this.itemId = item.getObjectId()
        await player.getInventory().equipItem( item )

        let message = new SystemMessageBuilder( SystemMessageIds.S1_EQUIPPED )
                .addItemInstanceName( item )
                .getBuffer()
        player.sendOwnedData( message )

        player.broadcastUserInfo()
        player.setCombatFlagEquipped( true )
    }

    unSpawnMe() {
        if ( this.playerId > 0 ) {
            this.dropIt()
        }

        if ( this.itemInstance ) {
            this.itemInstance.decayMe()
        }
    }
}