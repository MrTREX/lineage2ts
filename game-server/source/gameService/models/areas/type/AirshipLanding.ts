import { L2WorldArea } from '../WorldArea'
import { AreaType } from '../AreaType'

export class AirshipLandingArea extends L2WorldArea {
    type: Readonly<AreaType> = AreaType.AirLanding
}