import { EnterExitActions, L2WorldArea } from '../WorldArea'
import { AreaType } from '../AreaType'
import { WorldAreaActions } from '../WorldAreaActions'
import { Race, RaceMapping } from '../../../enums/Race'
import { L2Object } from '../../L2Object'
import { SystemMessageBuilder } from '../../../packets/send/SystemMessage'
import { L2PcInstance } from '../../actor/instance/L2PcInstance'
import { L2AreaItem } from '../../../../data/interface/AreaDataApi'

interface MotherTreeAreaProperties {
    race: Partial<Race>
    enterMessageId: number
    exitMessageId: number
    hpBonus: number
    mpBonus: number
}

export class MotherTreeArea extends L2WorldArea {
    type: Readonly<AreaType> = AreaType.MotherTree
    properties : MotherTreeAreaProperties

    /*
        Default values if data does not specify particular values.
     */
    hpBonus: number = 2
    mpBonus: number = 1

    protected getSupportedActions(): ReadonlySet<WorldAreaActions> {
        return EnterExitActions
    }

    onEnter( object: L2Object ) : void {
        ( object as L2PcInstance ).sendOwnedData( SystemMessageBuilder.fromMessageId( this.properties.enterMessageId ) )
    }

    onExit( object: L2Object ) : void {
        ( object as L2PcInstance ).sendOwnedData( SystemMessageBuilder.fromMessageId( this.properties.exitMessageId ) )
    }

    canAffectObject( object: L2Object ): boolean {
        return object.isPlayer() && ( this.properties.race === undefined || this.properties.race === ( object as L2PcInstance ).getRace() )
    }

    loadProperties( data: L2AreaItem ) {
        this.properties = {
            enterMessageId: data.properties.enteringMessageNo as number,
            exitMessageId: data.properties.leavingMessageNo as number,
            hpBonus: data.properties.hpRegenBonus as number ?? 2,
            mpBonus: data.properties.mpRegenBonus as number ?? 1,
            race: RaceMapping[ data.properties.affectRace as string ] ?? Race.NONE
        }
    }
}