import { L2WorldArea } from '../WorldArea'
import { AreaType } from '../AreaType'

export class NoItemDropArea extends L2WorldArea {
    type: Readonly<AreaType> = AreaType.NoItemDrop
}