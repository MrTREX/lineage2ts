import { L2WorldArea } from '../WorldArea'
import { AreaType } from '../AreaType'

/*
    Quests may have a separate area to monitor for entrance, hence
    special type to accommodate various usages.
 */
export class QuestArea extends L2WorldArea {
    type: Readonly<AreaType> = AreaType.Quest
}