import { L2WorldArea } from '../WorldArea'
import { AreaType } from '../AreaType'
import { L2AreaItem } from '../../../../data/interface/AreaDataApi'

interface CastleHqAreaProperties {
    residenceId: number
    name: string
}

export class CastleHqArea extends L2WorldArea {
    type: Readonly<AreaType> = AreaType.CastleHq
    properties: CastleHqAreaProperties

    loadProperties( data: L2AreaItem ) {
        this.properties = {
            residenceId: data.properties.castleId as number,
            name: data.properties.name as string
        }
    }
}