import { EnterExitActions, L2WorldArea } from '../WorldArea'
import { AreaType } from '../AreaType'
import { WorldAreaActions } from '../WorldAreaActions'
import { L2Object } from '../../L2Object'
import { L2PcInstance } from '../../actor/instance/L2PcInstance'
import { ItemDropState } from '../../../enums/ItemDropState'
import { L2AreaItem } from '../../../../data/interface/AreaDataApi'

interface SevenSignsAreaProperties {
    itemDrop: ItemDropState
    expPenalty: number
}

export class SevenSignsArea extends L2WorldArea {
    type: Readonly<AreaType> = AreaType.SevenSigns
    itemDrop: ItemDropState = ItemDropState.NoRestrictions
    expPenalty: number = 100

    properties: SevenSignsAreaProperties

    protected getSupportedActions(): ReadonlySet<WorldAreaActions> {
        return EnterExitActions
    }

    canAffectObject( object: L2Object ): boolean {
        return object.isPlayer()
    }

    onEnter( object: L2Object ) {
        let player = object as L2PcInstance

        player.setItemDropState( this.itemDrop )
        player.setExpPenalty( this.expPenalty )
    }

    onExit( object: L2Object ) {
        let player = object as L2PcInstance

        player.setItemDropState( ItemDropState.NoRestrictions )
        player.setExpPenalty( 1 )
    }

    loadProperties( data: L2AreaItem ) {
        this.properties = {
            itemDrop: data.properties.itemDrop === 'off' ? ItemDropState.DisallowAny : ItemDropState.NoRestrictions,
            expPenalty: data.properties.expPenaltyPer as number ?? 100
        }
    }
}