import { L2WorldArea } from '../WorldArea'
import { AreaType } from '../AreaType'
import { L2AreaItem } from '../../../../data/interface/AreaDataApi'

interface DominionAreaProperties {
    name: string
    territoryId: number
}

export class DominionArea extends L2WorldArea {
    type: Readonly<AreaType> = AreaType.Dominion
    properties: DominionAreaProperties

    loadProperties( data: L2AreaItem ) {
        this.properties = {
            name: data.properties.name as string,
            territoryId: data.properties.territoryId as number
        }
    }
}