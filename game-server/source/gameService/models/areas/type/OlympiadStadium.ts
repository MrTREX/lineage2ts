import { L2WorldArea } from '../WorldArea'
import { AreaType } from '../AreaType'
import { ILocational, Location } from '../../Location'
import { L2AreaItem } from '../../../../data/interface/AreaDataApi'
import { LocationProperties } from '../../LocationProperties'
import _ from 'lodash'

interface OlympiadStadiumAreaProperties {
    instance: string
    teleports: Array<ILocational>
    spectator: ILocational
}

export class OlympiadStadiumArea extends L2WorldArea {
    type: Readonly<AreaType> = AreaType.OlympiadStadium
    properties: OlympiadStadiumAreaProperties

    loadProperties( data: L2AreaItem ) {
        let spectatorItem = data.properties.spectator as Object
        let spectator = new Location( spectatorItem[ 'x' ], spectatorItem[ 'y' ], spectatorItem[ 'z' ] )
        this.properties = {
            instance: data.properties.instance as string,
            teleports: _.map( data.properties.teleports as unknown as Array<LocationProperties>,( item : LocationProperties ) : ILocational => new Location( item.x, item.y, item.z ) ),
            spectator
        }
    }
}