import { L2WorldArea } from '../WorldArea'
import { AreaType } from '../AreaType'
import { L2AreaItem } from '../../../../data/interface/AreaDataApi'

interface SkillReferenceAreaProperties {
    referenceId: number
}

export class SkillReferenceArea extends L2WorldArea {
    type: Readonly<AreaType> = AreaType.SkillReference
    properties: SkillReferenceAreaProperties

    loadProperties( data: L2AreaItem ) {
        this.properties = {
            referenceId: data.properties.referenceId as number
        }
    }

    getSkillId() : number {
        return this.properties.referenceId
    }
}