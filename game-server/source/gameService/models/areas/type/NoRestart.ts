import { L2WorldArea } from '../WorldArea'
import { AreaType } from '../AreaType'
import { WorldAreaActions } from '../WorldAreaActions'
import { L2Object } from '../../L2Object'
import { GameServerProperties } from '../../../../config/GameServerProperties'
import { TeleportWhereType } from '../../../enums/TeleportWhereType'
import { L2PcInstance } from '../../actor/instance/L2PcInstance'
import { L2AreaItem } from '../../../../data/interface/AreaDataApi'

interface NoRestartAreaProperties {
    restartTimeMs: number
    restartAllowedTimeMs: number
}

export class NoRestartArea extends L2WorldArea {
    type: Readonly<AreaType> = AreaType.NoRestart
    properties: NoRestartAreaProperties

    protected getSupportedActions(): ReadonlySet<WorldAreaActions> {
        return new Set<WorldAreaActions>( [
            WorldAreaActions.Enter
        ] )
    }

    canAffectObject( object: L2Object ): boolean {
        return object.isPlayer()
    }

    onEnter( object: L2Object ) {
        let currentTime = Date.now()

        if ( ( ( currentTime - ( object as L2PcInstance ).getLastAccess() ) > this.properties.restartTimeMs )
            && ( ( currentTime - GameServerProperties.getStartTime() ) > this.properties.restartAllowedTimeMs ) ) {
            ( object as L2PcInstance ).teleportToLocationType( TeleportWhereType.TOWN )
        }
    }

    loadProperties( data: L2AreaItem ) {
        this.properties = {
            restartAllowedTimeMs: data.properties.restartAllowedTime as number * 1000,
            restartTimeMs: data.properties.restartTime as number * 1000
        }
    }
}