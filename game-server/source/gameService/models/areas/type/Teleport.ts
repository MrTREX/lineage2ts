import { L2WorldArea } from '../WorldArea'
import { AreaType } from '../AreaType'
import { Location } from '../../Location'
import { L2AreaItem } from '../../../../data/interface/AreaDataApi'

/*
    TODO: figure out how to tie these with instance
 */

interface TeleportAreaProperties {
    intervalMs: number
    instanceId: number
    location: Location
}

export class TeleportArea extends L2WorldArea {
    type: Readonly<AreaType> = AreaType.Teleport
    properties: TeleportAreaProperties

    loadProperties( data: L2AreaItem ) {
        let point = data.properties.teleportPoints[ 0 ] as [ number, number, number ]

        this.properties = {
            intervalMs: data.properties.unitTick as number * 1000,
            instanceId: data.properties.instantzoneId as number ?? 0,
            location: new Location( point[ 0 ], point[ 1 ], point[ 2 ] )
        }
    }
}