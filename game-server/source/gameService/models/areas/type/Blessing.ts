import { AreaType } from '../AreaType'
import { SkillEffectArea } from './SkillEffect'

export class BlessingArea extends SkillEffectArea {
    type: Readonly<AreaType> = AreaType.Blessing
}