import { createTeleportLocation } from '../WorldArea'
import { AreaType } from '../AreaType'
import { ResidenceTeleportType } from '../../../enums/ResidenceTeleportType'
import { L2AreaItem } from '../../../../data/interface/AreaDataApi'
import _ from 'lodash'
import { L2ResidenceWorldArea } from '../ResidenceWorldArea'

export class ClanHallArea extends L2ResidenceWorldArea {
    type: Readonly<AreaType> = AreaType.ClanHall

    loadProperties( data: L2AreaItem ) {
        this.properties = {
            residenceId: data.properties.clanHallId as number,
            name: data.properties.name as string,
            teleports: {
                [ ResidenceTeleportType.Banishment ]: _.map( data.properties.teleports[ 'banish' ], createTeleportLocation ),
                [ ResidenceTeleportType.PVP ]: _.map( data.properties.teleports[ 'chaotic' ], createTeleportLocation ),
                [ ResidenceTeleportType.Normal ]: _.map( data.properties.teleports[ 'normal' ], createTeleportLocation ),
                [ ResidenceTeleportType.Other ]: _.map( data.properties.teleports[ 'other' ], createTeleportLocation ),
            }
        }
    }
}