import { EnterExitActions, L2WorldArea } from '../WorldArea'
import { AreaType } from '../AreaType'
import { L2AreaItem } from '../../../../data/interface/AreaDataApi'
import { WorldAreaActions } from '../WorldAreaActions'
import { L2Object } from '../../L2Object'
import { L2PcInstance } from '../../actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../packets/SystemMessageIdValues'
import { ConfigManager } from '../../../../config/ConfigManager'
import { MountType } from '../../../enums/MountType'
import { L2Character } from '../../actor/L2Character'
import { FortManager } from '../../../instancemanager/FortManager'
import { Fort } from '../../entity/Fort'
import { FortSiegeManager } from '../../../instancemanager/FortSiegeManager'
import { getSlotFromItem } from '../../itemcontainer/ItemSlotHelper'
import { FameOperations } from '../../entity/FameOperations'
import { Race } from '../../../enums/Race'
import { L2ServitorInstance } from '../../actor/instance/L2ServitorInstance'

interface FortSiegeAreaProperties {
    residenceId: number
    name: string
}

export class FortSiegeArea extends L2WorldArea {
    type: Readonly<AreaType> = AreaType.FortSiege
    properties: FortSiegeAreaProperties

    loadProperties( data: L2AreaItem ) {
        this.properties = {
            name: data.properties.name as string,
            residenceId: ( data.properties.castleId ?? data.properties.clanHallId ?? data.properties.fortId ) as number
        }
    }

    protected getSupportedActions(): ReadonlySet<WorldAreaActions> {
        return EnterExitActions
    }

    canAffectObject( object: L2Object ): boolean {
        return object.isPlayer() || ( object.isServitor() && ( object as L2Character ).getRace() === Race.SIEGE_WEAPON )
    }

    onEnter( object: L2Object ) {
        let character = object as L2Character

        if ( character.isPlayer() ) {
            let player: L2PcInstance = character.getActingPlayer()
            if ( player.isRegisteredOnThisSiegeField( this.properties.residenceId ) ) {
                player.setIsInSiege( true )

                let residenceFame = this.getResidenceFame()
                if ( residenceFame && residenceFame.canGiveFame() && residenceFame.getFameInterval() > 0 ) {
                    player.startFameTask( residenceFame.getFameInterval(), residenceFame.getFameAmount() )
                }
            }

            character.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ENTERED_COMBAT_ZONE ) )

            if ( !ConfigManager.castle.allowRideWyvernDuringSiege() && player.getMountType() === MountType.WYVERN ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.AREA_CANNOT_BE_ENTERED_WHILE_MOUNTED_WYVERN ) )
                player.startDismountTask( 5000 )
            }
        }
    }

    onExit( object: L2Object ) {
        let character = object as L2Character

        if ( character.isPlayer() ) {
            let player: L2PcInstance = character.getActingPlayer()
            character.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.LEFT_COMBAT_ZONE ) )

            if ( player.getMountType() === MountType.WYVERN ) {
                player.clearDismountTask()
            }

            if ( player.isRegisteredOnThisSiegeField( this.properties.residenceId ) ) {
                player.debounceEngagePvPFlag( ConfigManager.pvp.getPvPVsNormalTime() )
            }

            player.stopFameTask()
            player.setIsInSiege( false )

            let combatFlag = player.getInventory().getItemByItemId( 9819 )
            if ( combatFlag ) {

                if ( this.type === AreaType.FortSiege ) {
                    let fort: Fort = FortManager.getFortById( this.properties.residenceId )
                    if ( fort ) {
                        FortSiegeManager.dropCombatFlag( player, fort.getResidenceId() )
                        return
                    }
                }

                let slot = getSlotFromItem( combatFlag )

                player.getInventory().unEquipItemInBodySlot( slot ).then( () => {
                    player.destroyItem( combatFlag, true, 'FortSiege exit' )
                } )

            }

            return
        }

        if ( character.isServitor() ) {
            let servitor = character as L2ServitorInstance
            servitor.unSummon( servitor.getOwner() )

            return
        }
    }

    getResidenceFame() : FameOperations {
        return FortManager.getFortById( this.properties.residenceId )
    }

    getResidenceId() : number {
        return this.properties.residenceId
    }

    addToInitialGrid(): boolean {
        return false
    }
}