import { ActivationActions, EnterAndActivationActions, L2WorldArea } from '../WorldArea'
import { AreaType } from '../AreaType'
import { WorldAreaActions } from '../WorldAreaActions'
import { SkillItem } from '../../skills/SkillItem'
import { L2Object } from '../../L2Object'
import { L2World } from '../../../L2World'
import { SkillCache } from '../../../cache/SkillCache'
import { InstanceType } from '../../../enums/InstanceType'
import { L2Character } from '../../actor/L2Character'
import { L2AreaItem } from '../../../../data/interface/AreaDataApi'
import _ from 'lodash'
import { EtcStatusUpdate } from '../../../packets/send/EtcStatusUpdate'
import { L2PcInstance } from '../../actor/instance/L2PcInstance'

interface SkillEffectAreaProperties {
    isEnabled: boolean
    probability: number
    skills: Array<SkillItem>
    intervalMs: number
    target: InstanceType
}

export class SkillEffectArea extends L2WorldArea {
    type: Readonly<AreaType> = AreaType.ApplyEffect
    properties: Readonly<SkillEffectAreaProperties>
    private interval: NodeJS.Timeout

    protected getSupportedActions(): ReadonlySet<WorldAreaActions> {
        return EnterAndActivationActions
    }

    canAffectObject( object: L2Object ): boolean {
        return object.getInstanceType() === this.properties.target
    }

    onActivation() {
        if ( this.interval || this.properties.skills.length === 0 ) {
            return
        }

        this.interval = setInterval( this.runApplySkills.bind( this ), this.properties.intervalMs )
    }

    onDeactivation() {
        if ( !this.interval ) {
            return
        }

        clearInterval( this.interval )
        this.interval = null
    }

    runApplySkills() : void {
        L2World.forObjectsByBox( this.getSpatialIndex(), ( object: L2Object ) => {
            if ( object.getInstanceType() !== this.properties.target ) {
                return
            }

            let character = object as L2Character

            this.properties.skills.forEach( ( item: SkillItem ) => {
                if ( character.isAffectedBySkill( item.id ) ) {
                    return
                }

                let skill = SkillCache.getSkill( item.id, item.level )
                if ( !skill || this.properties.probability < Math.random() ) {
                    return
                }

                skill.applyEffects( character, character )
            } )
        } )
    }

    loadProperties( data: L2AreaItem ) : void {
        this.properties = {
            intervalMs: data.properties.unitTick as number * 1000,
            probability: data.properties.skillProbability as number,
            skills: _.map( data.properties.skills as unknown as Array<Object>, ( item: Object ) : SkillItem => {
                return {
                    id: item[ 'id' ],
                    level: item[ 'level' ]
                }
            } ),
            target: data.properties.target === 'npc' ? InstanceType.L2Npc : InstanceType.L2PcInstance,
            isEnabled: data.properties.isEnabled === true
        }
    }

    onEnter( object: L2Object ) {
        if ( object.isPlayer() ) {
            ( object as L2PcInstance ).sendDebouncedPacket( EtcStatusUpdate )
        }
    }

    onExit( object: L2Object ) {
        this.onEnter( object )
    }

    addToInitialGrid(): boolean {
        return this.properties.isEnabled
    }
}