import { AreaType } from '../AreaType'
import { FortSiegeArea } from './FortSiege'
import { FameOperations } from '../../entity/FameOperations'
import { CastleManager } from '../../../instancemanager/CastleManager'
import { L2AreaItem } from '../../../../data/interface/AreaDataApi'


export class CastleSiegeArea extends FortSiegeArea {
    type: Readonly<AreaType> = AreaType.CastleSiege

    getResidenceFame(): FameOperations {
        return CastleManager.getCastleById( this.properties.residenceId )
    }

    loadProperties( data: L2AreaItem ) {
        this.properties = {
            residenceId: data.properties.castleId as number,
            name: data.properties.name as string
        }
    }
}