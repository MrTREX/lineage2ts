import { L2WorldArea } from '../WorldArea'
import { AreaType } from '../AreaType'
import { L2AreaItem } from '../../../../data/interface/AreaDataApi'

interface NoSummonPlayerAreaProperties {
    messageId: number
}

export class NoSummonPlayerArea extends L2WorldArea {
    type: Readonly<AreaType> = AreaType.NoSummonPlayer
    properties : NoSummonPlayerAreaProperties

    loadProperties( data: L2AreaItem ) {
        this.properties = {
            messageId: data.properties.messageId as number
        }
    }
}