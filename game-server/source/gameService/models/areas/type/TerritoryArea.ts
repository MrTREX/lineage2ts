import { L2WorldArea } from '../WorldArea'
import { AreaType } from '../AreaType'

export class TerritoryArea extends L2WorldArea {
    type: Readonly<AreaType> = AreaType.SpawnTerritory
}