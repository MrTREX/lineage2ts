import { EnterExitActions, L2WorldArea } from '../WorldArea'
import { AreaType } from '../AreaType'
import { WorldAreaActions } from '../WorldAreaActions'
import { L2Object } from '../../L2Object'
import { ConfigManager } from '../../../../config/ConfigManager'
import { SystemMessageBuilder } from '../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../packets/SystemMessageIdValues'
import { L2PcInstance } from '../../actor/instance/L2PcInstance'
import { DeferredMethods } from '../../../helpers/DeferredMethods'

export class JailArea extends L2WorldArea {
    type: Readonly<AreaType> = AreaType.Jail

    protected getSupportedActions(): ReadonlySet<WorldAreaActions> {
        return EnterExitActions
    }

    canAffectObject( object: L2Object ): boolean {
        return object.isPlayer()
    }

    onEnter( object: L2Object ): void {
        let player = object as L2PcInstance

        if ( ConfigManager.general.jailIsPvp() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ENTERED_COMBAT_ZONE ) )
        }
    }

    onExit( object: L2Object ): void {
        let player = object as L2PcInstance

        if ( ConfigManager.general.jailIsPvp() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.LEFT_COMBAT_ZONE ) )
        }

        if ( player.isJailed() ) {
            setTimeout( DeferredMethods.playerTeleport, 2000, player.getObjectId(), this.getCenterPointLocation() )
            player.sendMessage( 'You cannot live area. You must wait until your jail time is over.' )
        }
    }
}