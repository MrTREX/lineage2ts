import { L2WorldArea } from '../WorldArea'
import { AreaType } from '../AreaType'
import { WorldAreaActions } from '../WorldAreaActions'
import { L2Object } from '../../L2Object'
import { ConfigManager } from '../../../../config/ConfigManager'
import { L2PcInstance } from '../../actor/instance/L2PcInstance'
import { NevitManager } from '../../../cache/NevitManager'
import { TerritoryWarManager } from '../../../instancemanager/TerritoryWarManager'
import { L2AreaItem } from '../../../../data/interface/AreaDataApi'

interface PeaceAreaProperties {
    isStoreBlocked: boolean
}

const PeaceAreaActions = new Set<WorldAreaActions>( [
    WorldAreaActions.Enter
] )

export class PeaceArea extends L2WorldArea {
    type: Readonly<AreaType> = AreaType.Peace
    properties: PeaceAreaProperties

    protected getSupportedActions(): ReadonlySet<WorldAreaActions> {
        return PeaceAreaActions
    }

    canAffectObject( object: L2Object ): boolean {
        return object.isPlayer()
    }

    onEnter( object: L2Object ) {
        if ( ConfigManager.character.isNevitEnabled() ) {
            NevitManager.onEnterPeaceZone( object.getObjectId() )
        }

        let player = object as L2PcInstance
        if ( player.isCombatFlagEquipped() && TerritoryWarManager.isTWInProgress ) {
            TerritoryWarManager.dropCombatFlag( player, false, true )
        }
    }

    loadProperties( data: L2AreaItem ) {
        this.properties = {
            isStoreBlocked: !!data.properties.blockedActions
        }
    }
}