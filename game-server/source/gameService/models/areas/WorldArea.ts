import { ILocational, Location } from '../Location'
import { AreaForm } from './AreaForm'
import { ISpatialIndexArea } from '../SpatialIndexData'
import { WorldAreaActions } from './WorldAreaActions'
import { L2Object } from '../L2Object'
import { AreaType } from './AreaType'
import { L2AreaItem } from '../../../data/interface/AreaDataApi'
import { LocationProperties } from '../LocationProperties'
import { ListenerCache } from '../../cache/ListenerCache'
import { CharacterEnterZoneEvent, CharacterExitZoneEvent, EventType } from '../events/EventType'
import { EventPoolCache } from '../../cache/EventPoolCache'

export const EmptyActions : ReadonlySet<WorldAreaActions> = new Set<WorldAreaActions>()
export const EnterExitActions : ReadonlySet<WorldAreaActions> = new Set<WorldAreaActions>( [
    WorldAreaActions.Enter,
    WorldAreaActions.Exit
] )

export const ActivationActions : ReadonlySet<WorldAreaActions> = new Set<WorldAreaActions>( [
    WorldAreaActions.Activation,
    WorldAreaActions.Deactivation
] )

export const EnterAndActivationActions : ReadonlySet<WorldAreaActions> = new Set<WorldAreaActions>( [
    WorldAreaActions.Enter,
    WorldAreaActions.Exit,
    WorldAreaActions.Activation,
    WorldAreaActions.Deactivation
] )

export function createTeleportLocation( item: LocationProperties ) : Location {
    return new Location( item.x, item.y, item.z )
}

export abstract class L2WorldArea {
    id: Readonly<number>
    abstract type: Readonly<AreaType>
    form: Readonly<AreaForm>
    data: L2AreaItem

    private spatialIndex: ISpatialIndexArea
    private centerPoint: Location
    actions: ReadonlySet<WorldAreaActions>

    constructor( id: number, form: AreaForm, data: L2AreaItem ) {
        this.id = id
        this.form = form

        this.spatialIndex = this.form.getBoundingRectangle() as ISpatialIndexArea
        this.spatialIndex.area = this
        this.actions = this.getSupportedActions()

        let x = Math.floor( this.spatialIndex.minX + ( this.spatialIndex.maxX - this.spatialIndex.minX ) / 2 )
        let y = Math.floor( this.spatialIndex.minY + ( this.spatialIndex.maxY - this.spatialIndex.minY ) / 2 )
        this.centerPoint = new Location( x, y, this.form.minimumZ )

        this.data = data
        this.loadProperties( data )
    }

    protected getSupportedActions() : ReadonlySet<WorldAreaActions> {
        return EmptyActions
    }

    getSpatialIndex(): ISpatialIndexArea {
        return this.spatialIndex
    }

    getCenterPointLocation(): Readonly<ILocational> {
        return this.centerPoint
    }

    isObjectInside( location: ILocational ): boolean {
        return this.isInsideWithCoordinates( location.getX(), location.getY(), location.getZ() )
    }

    isInsideWithCoordinates( x: number, y: number, z: number ): boolean {
        return this.form.isPointInside( x, y, z )
    }

    canAffectObject( object: L2Object ) : boolean {
        return true
    }

    hasAction( object: L2Object, action: WorldAreaActions ) : boolean {
        return this.canAffectObject( object ) && this.supportsAction( action )
    }

    supportsAction( action: WorldAreaActions ) : boolean {
        return this.actions.has( action )
    }

    processAction( object: L2Object, action: WorldAreaActions ) : void {
        switch ( action ) {
            case WorldAreaActions.Enter:
                if ( ListenerCache.hasAreaListeners( this.id, EventType.CharacterEnterArea ) ) {
                    let eventData = EventPoolCache.getData( EventType.CharacterEnterArea ) as CharacterEnterZoneEvent

                    eventData.characterId = object.getObjectId()
                    eventData.areaId = this.id
                    eventData.areaType = this.type
                    eventData.characterInstanceType = object.getInstanceType()

                    ListenerCache.sendZoneEvent( this.id, EventType.CharacterEnterArea, eventData )
                }

                return this.onEnter( object )

            case WorldAreaActions.Exit:
                if ( ListenerCache.hasAreaListeners( this.id, EventType.CharacterExitArea ) ) {
                    let eventData = EventPoolCache.getData( EventType.CharacterExitArea ) as CharacterExitZoneEvent

                    eventData.characterId = object.getObjectId()
                    eventData.areaId = this.id
                    eventData.areaType = this.type

                    ListenerCache.sendZoneEvent( this.id, EventType.CharacterExitArea, eventData )
                }

                return this.onExit( object )

            case WorldAreaActions.Activation:
                return this.onActivation()

            case WorldAreaActions.Deactivation:
                return this.onDeactivation()

            case WorldAreaActions.Death:
                return this.onDeath( object )

            case WorldAreaActions.RegionActivation:
                return this.onRegionActivation()
        }
    }

    onEnter( object: L2Object ): void {}

    onExit( object: L2Object ): void {}

    onActivation(): void {}

    onDeactivation(): void {}

    onRegionActivation() : void {}

    // TODO : use or consider removing whole action
    onDeath( object: L2Object ): void {}

    getDistanceToCoordinates( x: number, y: number ) : number {
        return this.form.getDistanceToPoint( x, y )
    }

    getDistanceToObject( object: L2Object ): number {
        return this.form.getDistanceToPoint( object.getX(), object.getY() )
    }

    loadProperties( data: L2AreaItem ) : void {}

    addToInitialGrid() : boolean {
        return true
    }
}