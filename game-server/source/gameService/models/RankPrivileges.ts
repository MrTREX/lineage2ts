export interface RankPrivileges {
    rank: number
    privileges: number // bitmask for priviledges
}