import { L2Character } from './actor/L2Character'
import { Skill } from './Skill'
import { AIEffectHelper } from '../aicontroller/helpers/AIEffectHelper'
import { L2PcInstance } from './actor/instance/L2PcInstance'
import { L2Summon } from './actor/L2Summon'
import { PetInfoAnimation } from '../packets/send/PetInfo'
import { AggroCache } from '../cache/AggroCache'
import { SystemMessageBuilder } from '../packets/send/SystemMessage'
import { SystemMessageIds } from '../packets/SystemMessageIdValues'
import { L2TargetType } from './skills/targets/L2TargetType'
import { L2World } from '../L2World'
import { PathFinding } from '../../geodata/PathFinding'
import { BroadcastHelper } from '../helpers/BroadcastHelper'
import { MagicSkillLaunched } from '../packets/send/MagicSkillLaunched'
import { scheduler } from 'node:timers/promises'
import { ObjectPool } from '../helpers/ObjectPoolHelper'
import { ListenerCache } from '../cache/ListenerCache'
import { EventType, NpcFinishedSkillEvent } from './events/EventType'
import { EventPoolCache } from '../cache/EventPoolCache'
import { EffectsCache } from '../aicontroller/EffectCache'
import { AIEffect, AIForceAttackTargetEvent } from '../aicontroller/enums/AIEffect'

const enum MagicProcessStep {
    Start,
    SkillHit,
    End,
    Complete
}

/*
    TODO : add skill cast queue
    Such logic should be part of queue system on AIController
 */

export class MagicProcess {
    character: L2Character
    targets: Array<L2Character> = []
    targetIds: Array<number>
    skill: Skill
    skillTime: number
    step: MagicProcessStep
    isAborted: boolean
    delay: number
    isSimultaneous: boolean
    process: Promise<void>

    setParameters( character: L2Character, targets: Array<number>, skill: Skill, time: number, isSimultaneous: boolean ) {
        this.character = character
        this.targetIds = targets
        this.skill = skill
        this.skillTime = time
        this.isSimultaneous = isSimultaneous
        this.isAborted = false
        this.step = MagicProcessStep.Start
    }

    recycleMagicProcess() : void {
        this.character = null
        this.targets.length = 0
        this.targetIds = null
        this.skill = null
        this.process = null

        MagicProcessPool.recycleValue( this )
    }

    onStart() : void {
        if ( !this.skill || !this.targetIds ) {
            return this.abortCast()
        }

        if ( this.targetIds.length === 0 ) {
            switch ( this.skill.getTargetType() ) {
                case L2TargetType.AURA:
                case L2TargetType.FRONT_AURA:
                case L2TargetType.BEHIND_AURA:
                case L2TargetType.AURA_CORPSE_MOB:
                case L2TargetType.AURA_FRIENDLY:
                case L2TargetType.AURA_UNDEAD_ENEMY:
                    break
                default:
                    return this.abortCast()
            }
        }

        let escapeRange = 0
        if ( this.skill.getEffectRange() > escapeRange ) {
            escapeRange = this.skill.getEffectRange()
        } else if ( ( this.skill.getCastRange() < 0 ) && ( this.skill.getAffectRange() > 80 ) ) {
            escapeRange = this.skill.getAffectRange()
        }

        if ( escapeRange > 0 ) {
            let radius = escapeRange + this.character.getTemplate().getCollisionRadius()
            let character = this.character
            let player = this.character.getActingPlayer()
            let skillIsBad = this.skill.isBad()

            let notInRange : boolean = false
            let targetNotVisible : boolean = false
            let targetInPeaceZone : boolean = false

            this.targets = this.targetIds.reduce( ( allTargets: Array<L2Character>, objectId: number ) : Array<L2Character> => {
                let target = L2World.getObjectById( objectId )
                if ( !target || !target.isCharacter() ) {
                    return allTargets
                }

                if ( !character.isInsideRadiusCoordinates( target.getX(), target.getY(), target.getZ(), radius, true ) ) {
                    notInRange = true
                    return allTargets
                }

                if ( this.skill.needsVisibleTarget() && !PathFinding.canSeeTargetWithPosition( character, target ) ) {
                    targetNotVisible = true
                    return allTargets
                }

                if ( skillIsBad ) {
                    if ( character.isPlayer() ) {
                        if ( ( target as L2Character ).isInsidePeaceZone( player ) ) {
                            targetInPeaceZone = true
                            return allTargets
                        }
                    } else {
                        if ( ( target as L2Character ).isInsidePeaceZoneWithObjects( character, target ) ) {
                            targetInPeaceZone = true
                            return allTargets
                        }
                    }
                }

                allTargets.push( target as L2Character )
                return allTargets
            }, this.targets )

            if ( this.targets.length === 0 ) {
                if ( this.character.isPlayer() ) {
                    if ( notInRange ) {
                        this.character.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.DIST_TOO_FAR_CASTING_STOPPED ) )
                    } else if ( targetNotVisible ) {
                        this.character.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_SEE_TARGET ) )
                    } else if ( targetInPeaceZone ) {
                        this.character.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.A_MALICIOUS_SKILL_CANNOT_BE_USED_IN_PEACE_ZONE ) )
                    }
                }

                return this.abortCast()
            }
        } else {
            this.targetIds.forEach( ( objectId: number ) => {
                let target = L2World.getObjectById( objectId ) as L2Character
                if ( target && target.isCharacter() ) {
                    this.targets.push( target )
                }
            } )
        }

        if ( this.targets.length === 0 || ( this.character.isAlikeDead() && !this.skill.isStatic() ) ) {
            return this.abortCast()
        }

        if ( !this.skill.isToggle() ) {
            BroadcastHelper.dataToSelfBasedOnVisibility( this.character, MagicSkillLaunched( this.character.getObjectId(), this.skill.getDisplayId(), this.skill.getDisplayLevel(), this.targets ) )
        }

        this.delay = this.skillTime === 0 ? 0 : 400
    }

    async onHit() : Promise<void> {
        /*
            Avoiding changing MP value for npcs since that triggers chain of attribute updates, that in the end is pointless since
            only HP values are sent over.
         */
        let mpValue = this.character.isAttackable() ? 0 : this.character.getStat().getMpConsume2( this.skill )
        if ( mpValue > 0 ) {
            if ( mpValue > this.character.getCurrentMp() ) {
                this.character.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_MP ) )
                return this.abortCast()
            }

            this.character.getStatus().reduceMp( mpValue )
        }

        let hpValue = this.skill.getHpConsume()
        if ( hpValue > 0 ) {
            if ( hpValue >= this.character.getCurrentHp() && this.character.isPlayable() ) {
                this.character.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_HP ) )
                return this.abortCast()
            }

            await this.character.getStatus().reduceHp( hpValue, this.character, true, false, true )
        }

        if ( this.character.isPlayable() ) {
            this.targets.forEach( ( target: L2Character ) : void => {
                if ( this.character.isPlayer() && target.isSummon() ) {
                    ( target as L2Summon ).updateAndBroadcastStatus( PetInfoAnimation.StatUpgrade )
                    return
                }

                if ( !target.isAttackable() ) {
                    return
                }

                if ( this.skill.getEffectPoint() > 0 ) {
                    AggroCache.addAggro( target.getObjectId(), this.character.getObjectId(), 0, this.skill.getEffectPoint() )
                    return
                }

                if ( this.skill.getEffectPoint() < 0 ) {
                    AggroCache.reduceAggroByAmount( target.getObjectId(), this.character.getObjectId(), this.skill.getEffectPoint() )
                    return
                }
            } )
        }

        await this.character.rechargeShots( this.skill.useSoulShot(), this.skill.useSpiritShot() )

        if ( this.character.isPlayer() && this.skill.getChargeConsume() > 0 ) {
            ( this.character as L2PcInstance ).decreaseCharges( this.skill.getChargeConsume() )
        }

        this.delay = 0
        return this.character.callSkill( this.skill, this.targets )
    }

    async onEnd() : Promise<void> {
        this.character.clearCastProcess( this )
        this.character.castInterruptTime = 0

        if ( this.character.isPlayer() ) {
            ( this.character as L2PcInstance ).clearMainSkillFlags()
        }

        if ( this.character.isSummon() ) {
            let playerOwner = ( this.character as L2Summon ).getOwner()
            if ( playerOwner ) {
                playerOwner.clearPetSkillFlags()
            }
        }

        if ( this.character.isChanneling() ) {
            this.character.getSkillChannelizer().stopChanneling()
        }

        // TODO : why recharge shots second time?
        if ( this.skillTime > 0 ) {
            await this.character.rechargeShots( this.skill.useSoulShot(), this.skill.useSpiritShot() )
        }

        /*
            There are small amount of skills that specify attacking target using equipped weapon
            upon completion of skill cast. In such case, we must store information for AIController
            to act, all due to disconnect between cast and AI intent logic promises.

            It is assumed that after cast, the caster character will default to attack intent in order
            to check if it should engage in attack.
         */

        let mainTarget = this.targets[ 0 ]
        if ( this.skill.nextActionIsAttack
            && mainTarget.getObjectId() !== this.character.objectId
            && mainTarget.canBeAttacked() ) {

            // TODO : refactor using object pool
            let data : AIForceAttackTargetEvent = {
                targetId: mainTarget.getObjectId()
            }

            EffectsCache.setParameters( this.character.getObjectId(), AIEffect.ForceAttackTarget, data )
        }

        AIEffectHelper.notifyCastingFinished( this.character, this.skill.getId(), this.isSimultaneous )

        if ( this.character.isNpc()
                && mainTarget.isPlayable()
                && ListenerCache.hasNpcTemplateListeners( this.character.getId(), EventType.NpcSkillFinished ) ) {
            let eventData = EventPoolCache.getData( EventType.NpcSkillFinished ) as NpcFinishedSkillEvent

            eventData.attackerId = this.character.getObjectId()
            eventData.targetId = mainTarget.getObjectId()
            eventData.skillId = this.skill.getId()
            eventData.skillLevel = this.skill.getLevel()
            eventData.attackerNpcId = this.character.getId()
            eventData.targetInstanceType = mainTarget.getInstanceType()

            return ListenerCache.sendNpcTemplateEvent( this.character.getId(), EventType.NpcSkillFinished, eventData, this.skill.getId() )
        }
    }

    startProcessWithDelay( delay: number ) : void {
        this.step = MagicProcessStep.Start
        this.delay = delay
        this.process = this.runProcess().finally( () => this.recycleMagicProcess() )
    }

    private async runProcess() : Promise<void> {
        while ( this.step !== MagicProcessStep.Complete ) {
            if ( this.isAborted ) {
                break
            }

            if ( this.delay > 0 ) {
                await scheduler.wait( this.delay )
            }

            if ( this.isAborted ) {
                break
            }

            switch ( this.step ) {
                case MagicProcessStep.Start:
                    this.onStart()
                    this.step = MagicProcessStep.SkillHit
                    break

                case MagicProcessStep.SkillHit:
                    await this.onHit()
                    this.step = MagicProcessStep.End
                    break

                case MagicProcessStep.End:
                    await this.onEnd()
                    this.step = MagicProcessStep.Complete
                    break
            }
        }

        if ( this.isAborted ) {
            return AIEffectHelper.notifyCastingCancelled( this.character, this.skill.getId(), this.isSimultaneous )
        }
    }

    abortCast() : void {
        this.isAborted = true
    }
}

export const MagicProcessPool = new ObjectPool( 'MagicProcess', () : MagicProcess => {
    return new MagicProcess()
} )