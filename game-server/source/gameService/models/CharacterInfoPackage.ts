import { DatabaseManager } from '../../database/manager'
import { L2ItemsTableEquippedItem, L2ItemTableAttributes } from '../../database/interface/ItemsTableApi'
import { InventorySlot } from '../values/InventoryValues'
import { L2CharactersTableFullInfo } from '../../database/interface/CharactersTableApi'
import _ from 'lodash'
import { ConfigManager } from '../../config/ConfigManager'
import { L2CharacterSubclassItem } from '../../database/interface/CharacterSubclassesApi'

export interface CharacterInfoPackage {
    name: string
    objectId: number
    exp: number
    sp: number
    clanId: number
    race: number
    classId: number
    baseClassId: number
    deleteSecondsLeft: number
    lastAccess: number
    face: number
    hairStyle: number
    hairColor: number
    sex: number
    level: number
    maxHp: number
    currentHp: number
    maxMp: number
    currentMp: number
    paperdoll: Array<PaperdollDetailItem>
    karma: number
    pkKills: number
    pvpKills: number
    augmentationId: number
    x: number
    y: number
    z: number
    vitalityPoints: number
    accessLevel: number
}

interface WeaponInfo {
    ownerId: number
    itemObjectId: number
}

export type PaperdollDetailItem = L2ItemsTableEquippedItem

interface PaperdollInfo {
    ownerId: number
    paperdoll: Array<PaperdollDetailItem>
}

const defaultPaperdollValue : PaperdollDetailItem = {
    locationData: 0,
    ownerId: 0,
    objectId: 0,
    itemId: 0,
    enchantLevel: 0
}

function createNewPaperdoll(): Array<PaperdollDetailItem> {
    return new Array( InventorySlot.TotalSlots ).fill( defaultPaperdollValue )
}

const getPaperdollSlots = ( items: Array<L2ItemsTableEquippedItem>, ownerId: string ): PaperdollInfo => {
    let paperdoll: Array<PaperdollDetailItem> = createNewPaperdoll()

    items.forEach( ( currentItem: L2ItemsTableEquippedItem ) => {
        if ( currentItem.locationData < 0 || currentItem.locationData >= InventorySlot.TotalSlots ) {
            return
        }

        paperdoll[ currentItem.locationData ] = currentItem
    } )

    return {
        ownerId: items.length > 0 ? items[ 0 ].ownerId : parseInt( ownerId ),
        paperdoll,
    }
}

async function getPaperdollInventory( ids: Array<number> ): Promise<Array<PaperdollInfo>> {
    let data: Array<L2ItemsTableEquippedItem> = await DatabaseManager.getItems().getEquippedInventory( ids )

    let groupedData: { [ objectId: string ]: Array<L2ItemsTableEquippedItem> } = _.groupBy( data, 'ownerId' )

    ids.forEach( ( id: number ) => {
        if ( !groupedData[ id ] ) {
            groupedData[ id ] = []
        }
    } )

    return _.map( groupedData, getPaperdollSlots )
}

function getAugmentationId( objectId: number, weapons: Array<WeaponInfo>, augmentationItems: Array<L2ItemTableAttributes> ) : number {
    let currentWeapon = weapons.find( ( item ) : boolean => {
        return item.ownerId === objectId
    } )

    if ( !currentWeapon ) {
        return 0
    }

    let augmentation = augmentationItems.find( ( item ) => item.objectId === currentWeapon.itemObjectId )
    if ( !augmentation || augmentation.attributes === -1 ) {
        return 0
    }

    return augmentation.attributes
}

export function buildCharacterInfoPackage( paperdolls: Array<PaperdollInfo>,
        subclassData: Array<L2CharacterSubclassItem>,
        augmentationItems: Array<L2ItemTableAttributes>,
        weapons: Array<WeaponInfo>,
        character: L2CharactersTableFullInfo ): CharacterInfoPackage {

    let objectId: number = character.objectId
    let subclassInfo : L2CharacterSubclassItem = subclassData.find( ( data ) : boolean => {
        return data.objectId === objectId && data.classId === character.classId
    } )

    let deleteSecondsLeft = character.deleteTime > 0 ? ( character.deleteTime - Date.now() ) / 1000 : 0

    return {
        name: character.name,
        objectId,
        exp: subclassInfo ? subclassInfo.exp : character.exp,
        sp: subclassInfo ? subclassInfo.sp : character.sp,
        clanId: character.clanId,
        race: character.race,
        classId: character.classId,
        baseClassId: character.baseClassId,
        deleteSecondsLeft,
        lastAccess: character.lastAccessTime,
        face: character.face,
        hairStyle: character.hairStyle,
        hairColor: character.hairColor,
        sex: character.sex,
        level: subclassInfo ? subclassInfo.level : character.level,
        maxHp: character.maxHp,
        currentHp: character.currentHp,
        maxMp: character.maxMp,
        currentMp: character.currentMp,
        paperdoll: paperdolls.find( item => item.ownerId === objectId ).paperdoll,
        karma: character.karma,
        pkKills: character.pkKills,
        pvpKills: character.pvpKills,
        augmentationId: getAugmentationId( objectId, weapons, augmentationItems ),
        x: character.x,
        y: character.y,
        z: character.z,
        vitalityPoints: character.vitalityPoints,
        accessLevel: character.accessLevel,
    }
}

export async function getCharacterSelectionPackages( loginName: string ): Promise<Array<CharacterInfoPackage>> {
    let characters: Array<L2CharactersTableFullInfo> = await DatabaseManager.getCharacterTable().getActiveCharacters( loginName )

    let characterIds: Array<number> = characters.map( character => character.objectId )
    let subclassCharacters: Array<L2CharactersTableFullInfo> = characters.filter( ( currentCharacter ) => {
        return currentCharacter.baseClassId !== currentCharacter.classId
    } )

    let paperdollData: Array<PaperdollInfo> = await getPaperdollInventory( characterIds )
    let subclassData = await DatabaseManager.getCharacterSubclasses().getSubclassInfo( subclassCharacters.map( ( character ) => character.objectId ) )
    let weaponIds: Array<WeaponInfo> = _.reduce( paperdollData, ( allIds: Array<WeaponInfo>, currentItem: PaperdollInfo ) => {

        let itemObjectId = currentItem.paperdoll[ InventorySlot.RightHand ].objectId
        if ( !_.isNumber( itemObjectId ) || itemObjectId < 1 ) {
            itemObjectId = currentItem.paperdoll[ InventorySlot.LeftHand ].objectId
        }

        if ( itemObjectId > 0 ) {
            allIds.push( {
                ownerId: currentItem.ownerId,
                itemObjectId
            } )
        }

        return allIds
    }, [] )

    let augmentationData : Array<L2ItemTableAttributes> = await DatabaseManager.getItems().getMultipleItemAttributes( weaponIds.map( item => item.itemObjectId ) )
    let packages = characters.map( ( data ) => buildCharacterInfoPackage( paperdollData, subclassData, augmentationData, weaponIds, data ) )

    return ConfigManager.server.randomizeCharacterSelection() ? _.shuffle( packages ) : packages
}