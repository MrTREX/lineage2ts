export interface BotPunishment {
    skillId: number
    skillLevel: number
    messageId: number
}

export interface RangeBotPunishment extends BotPunishment{
    limit: number
}