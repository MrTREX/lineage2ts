export enum PunishmentType {
    NONE,
    BAN,
    ChatBlocked,
    PARTY_BAN,
    JAIL
}