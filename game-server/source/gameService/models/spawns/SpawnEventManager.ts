import _ from 'lodash'
import { FortEventSpawn, NamedEventAction, NamedEventSpawn, SpawnEventData, SpawnEventType } from './SpawnEventType'
import { ListenerCache } from '../../cache/ListenerCache'
import { ListenerRegisterType } from '../../enums/ListenerRegisterType'
import { EventType, GameTimeHourEvent } from '../events/EventType'
import { FortEvent } from '../../enums/FortEvent'


export type EventCallback = ( data: SpawnEventData ) => unknown

interface CallbackTrigger {
    triggerIds: Set<number>
    callback: EventCallback
}

class Manager {
    namedTriggers: Record<number, Array<EventCallback>> = {}
    dayNightTriggers: Record<number, Array<EventCallback>> = {}
    fortEvents: Record<number, Record<number, Array<EventCallback>>> = {}

    callbacks: Record<number, Array<CallbackTrigger>> = {}

    constructor() {
        ListenerCache.registerListener( ListenerRegisterType.General, EventType.GameTimeHour, this, this.onGameHourEvent.bind( this ) )
    }

    registerNamedTrigger( name: string, callback: EventCallback ): void {
        let callbacks = this.namedTriggers[ name ]

        if ( !callbacks ) {
            callbacks = []
            this.namedTriggers[ name ] = callbacks
        }

        callbacks.push( callback )
    }

    unRegisterNamedTrigger( name: string, callback: EventCallback ) : void {
        let callbacks = this.namedTriggers[ name ]
        if ( !callbacks ) {
            return
        }

        this.namedTriggers[ name ] = _.pull( callbacks, callback )
    }

    callNamedTrigger( name: string, action: NamedEventAction ) : void {
        let callbacks = this.namedTriggers[ name ]
        if ( !callbacks ) {
            return
        }

        let data:NamedEventSpawn = {
            action
        }

        callbacks.forEach( ( callback: EventCallback ) => callback( data ) )
    }

    registerFortressEvent( fortId: number, eventId: number, callback: EventCallback ): void {
        let events = this.fortEvents[ fortId ]

        if ( !events ) {
            this.fortEvents[ fortId ] = {
                [ eventId ]: [ callback ]
            }

            return
        }

        let callbacks = events[ eventId ]

        if ( !callbacks ) {
            callbacks = []
            events[ eventId ] = callbacks
        }

        callbacks.push( callback )
    }

    triggerFortressEvent( fortId: number, eventId: FortEvent ) : void {
        let fortEvents = this.fortEvents[ fortId ]
        if ( !fortEvents ) {
            return
        }

        let triggers = fortEvents[ eventId ]
        if ( !triggers ) {
            return
        }

        let data : FortEventSpawn = {
            eventId,
        }

        triggers.forEach( callback => callback( data ) )
    }

    registerDayNightTimer( hours: Set<number>, callback: EventCallback ): void {
        hours.forEach( ( hour: number ) => {
            let callbacks = this.dayNightTriggers[ hour ]

            if ( !callbacks ) {
                callbacks = []
                this.dayNightTriggers[ hour ] = callbacks
            }

            callbacks.push( callback )
        } )
    }

    unRegisterDayNightTimer( hours: Set<number>, callback: EventCallback ): void {
        hours.forEach( ( hour: number ) => {
            let callbacks = this.dayNightTriggers[ hour ]

            if ( !callbacks ) {
                return
            }

            if ( callbacks.length === 1 ) {
                delete this.dayNightTriggers[ hour ]
                return
            }

            _.pull( callbacks, callback )
        } )
    }

    onGameHourEvent( data: GameTimeHourEvent ): void {
        let callbacks = this.dayNightTriggers[ data.gameHour ]
        if ( !callbacks ) {
            return
        }

        for ( const callback of callbacks ) {
            callback( data )
        }
    }

    registerCallback( type: SpawnEventType, callback: EventCallback, triggerIds: Set<number> = null ): void {
        let existingCallbacks: Array<CallbackTrigger> = this.callbacks[ type ]

        if ( !existingCallbacks ) {
            existingCallbacks = []
            this.callbacks[ type ] = existingCallbacks
        }

        existingCallbacks.push( {
            callback,
            triggerIds
        } )
    }

    unRegisterCallback( type: SpawnEventType, callback: EventCallback ) : void {
        let existingCallbacks: Array<CallbackTrigger> = this.callbacks[ type ]
        if ( !existingCallbacks ) {
            return
        }

        _.remove( existingCallbacks, ( trigger: CallbackTrigger ) : boolean => {
            return trigger.callback === callback
        } )
    }
}

export const SpawnEventManager = new Manager()