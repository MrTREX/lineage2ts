import { L2DefaultSpawn } from './L2DefaultSpawn'
import { SpawnLogicEventData } from '../ISpawnLogic'
import { GeneralHelper } from '../../../helpers/GeneralHelper'
import Timeout = NodeJS.Timeout
import { SpawnLogicEventType } from '../../../enums/spawn/SpawnLogicEventType'

export class CleftOne extends L2DefaultSpawn {
    timer: Timeout

    processSpawnEvent( eventType: SpawnLogicEventType, data: SpawnLogicEventData ) {
        if ( eventType === SpawnLogicEventType.DespawnTimer ) {
            if ( this.timer ) {
                this.timer.refresh()
                return
            }

            this.timer = setTimeout( this.onDefaultSpawnEvent.bind( this ), GeneralHelper.minutesToMillis( 3 ), eventType, structuredClone( data ) )
        }

        return this.onDefaultSpawnEvent( eventType, data )
    }
}