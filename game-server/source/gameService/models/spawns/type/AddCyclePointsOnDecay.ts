import { DefaultCycleManagementSpawn } from './DefaultCycleManagementSpawn'
import { L2Npc } from '../../actor/L2Npc'
import { CycleStepManager } from '../../../cache/CycleStepManager'
import _ from 'lodash'

const enum Type {
    SeedEnergy = 'seed_energy'
}

export class AddCyclePointsOnDecay extends DefaultCycleManagementSpawn {

    startDecay( npc: L2Npc ) {
        CycleStepManager.incrementPoints( this.getRequiredCycle(), 100 )

        super.startDecay( npc )
    }

    protected getRespawnDelay(): number {
        if ( this.maker.aiParameters?.type === Type.SeedEnergy ) {
            return 0
        }

        return super.getRespawnDelay()
    }

    getAmount() : number {
        if ( this.maker.aiParameters?.type === Type.SeedEnergy ) {
            return _.random( 1, this.data.amount )
        }

        return this.data.amount
    }
}