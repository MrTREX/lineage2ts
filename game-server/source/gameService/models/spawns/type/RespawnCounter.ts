import { L2DefaultSpawn } from './L2DefaultSpawn'
import { L2Npc } from '../../actor/L2Npc'
import { SpawnLogicEventType } from '../../../enums/spawn/SpawnLogicEventType'
import { SpawnLogicEventData } from '../ISpawnLogic'

export class RespawnCounter extends L2DefaultSpawn {
    despawnAmount : number

    protected initializeSpawn() {
        this.despawnAmount = this.maker.aiParameters?.loopCnt as number ?? 1
    }

    startDecay( npc: L2Npc ): void {
        super.startDecay( npc )

        this.despawnAmount--
        if ( this.despawnAmount !== 0 ) {
            return
        }

        let makerName = this.maker.aiParameters?.makerName as string
        if ( !makerName ) {
            return
        }

        return this.sendEvent( makerName, SpawnLogicEventType.Spawn )
    }

    processSpawnEvent( eventType: SpawnLogicEventType, data: SpawnLogicEventData = null ) : void {
        this.initializeSpawn()
        super.processSpawnEvent( eventType, data )
    }
}