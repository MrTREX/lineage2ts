import { GameTime } from '../../../cache/GameTime'
import { GameTimeHourEvent } from '../../events/EventType'
import { L2DefaultSpawn } from './L2DefaultSpawn'
import { SpawnEventManager } from '../SpawnEventManager'
import { EventListenerMethod } from '../../events/listeners/EventListenerData'

const enum GameTimePeriodHours {
    MorningHour = 6,
    DayEndHour = 18,
}

const dayNightHours = new Set<number>( [
    GameTimePeriodHours.MorningHour,
    GameTimePeriodHours.DayEndHour
] )

export class DayNightSpawn extends L2DefaultSpawn {
    readyToSpawn: boolean = false
    callback: EventListenerMethod

    protected initializeSpawn() {
        this.callback = this.onEvent.bind( this )
        this.registerEvent()
    }

    deInitializeSpawn() {
        this.unRegisterEvent()
    }

    startSpawn(): number {
        this.readyToSpawn = true
        let isNightSpawn : boolean = this.maker.aiParameters?.isNight === 1

        if ( isNightSpawn === GameTime.isNight() ) {
            return this.processStartSpawn()
        }

        return 0
    }

    getSpawnHours() : Set<number> {
        return dayNightHours
    }

    onEvent( data: GameTimeHourEvent ) : void {
        let isNightSpawn : boolean = this.maker.aiParameters?.isNight === 1

        switch ( data.gameHour ) {
            case GameTimePeriodHours.MorningHour:
                if ( isNightSpawn ) {
                    return this.startDespawn()
                }

                if ( this.readyToSpawn ) {
                    this.processStartSpawn()
                    return
                }

                return

            case GameTimePeriodHours.DayEndHour:
                if ( isNightSpawn ) {

                    if ( this.readyToSpawn ) {
                        this.processStartSpawn()
                        return
                    }

                    return
                }

                return this.startDespawn()
        }
    }

    protected registerEvent() : void {
        SpawnEventManager.registerDayNightTimer( this.getSpawnHours(), this.callback )
    }

    protected unRegisterEvent() : void {
        SpawnEventManager.unRegisterDayNightTimer( this.getSpawnHours(), this.callback )
    }
}