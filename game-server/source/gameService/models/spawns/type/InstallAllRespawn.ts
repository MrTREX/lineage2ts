import { L2DefaultSpawn } from './L2DefaultSpawn'
import { L2Npc } from '../../actor/L2Npc'
import { L2World } from '../../../L2World'

export class InstallAllRespawn extends L2DefaultSpawn {
    protected startNormalDecay( npc: L2Npc ) {
        this.decrementCount()

        if ( this.parameters.count === 0 ) {
            this.spawnedObjectIds.forEach( this.respawnNpcByObjectId.bind( this ) )
            this.minions?.spawnedObjectIds.forEach( this.respawnNpcByObjectId.bind( this ) )

            return
        }

        return this.processNormalDecay( npc )
    }

    respawnNpcByObjectId( objectId: number ): void {
        let npc = L2World.getObjectById( objectId ) as L2Npc
        if ( npc ) {
            this.runRespawnTask( npc )
        }
    }
}