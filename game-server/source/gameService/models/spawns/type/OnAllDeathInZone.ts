import { ZoneEntry } from './ZoneEntry'
import { L2Npc } from '../../actor/L2Npc'
import { sendListenerEvent } from '../SpawnEventHelper'

export class OnAllDeathInZone extends ZoneEntry {
    startDecay( npc: L2Npc ): void {
        super.startDecay( npc )

        if ( this.parameters.count === 0 ) {
            let name = ( this.maker.aiParameters?.controlMaker as string ) ?? 'godard32_1713_002m1'
            sendListenerEvent( name, '10001001' )
        }
    }
}