import { L2DefaultSpawn } from './L2DefaultSpawn'
import { SpawnEventManager } from '../SpawnEventManager'
import { SpawnEventType } from '../SpawnEventType'
import { EventListenerMethod } from '../../events/listeners/EventListenerData'

export class InstantSiegeSpawn extends L2DefaultSpawn {
    callback: EventListenerMethod

    protected initializeSpawn() {
        this.callback = this.onSiegeEnd.bind( this )
        this.registerEvent()
    }

    deInitializeSpawn() {
        this.unRegisterEvent()
    }

    onSiegeEnd() : void {
        this.processStartSpawn()
    }

    protected startNormalDecay() : void {
        this.decrementCount()
    }

    protected registerEvent() : void {
        let ids = new Set<number>( [ this.maker.aiParameters.castleId as number ] )

        SpawnEventManager.registerCallback( SpawnEventType.SiegeEnd, this.callback, ids )
    }

    protected unRegisterEvent() : void {
        SpawnEventManager.unRegisterCallback( SpawnEventType.SiegeEnd, this.callback )
    }
}