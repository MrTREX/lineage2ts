import { L2DefaultSpawn } from './L2DefaultSpawn'
import { L2Npc } from '../../actor/L2Npc'
import { MsInMinutes } from '../../../enums/MsFromTime'

export class RespawnOnAllDead extends L2DefaultSpawn {
    respawnTimer: NodeJS.Timeout

    protected processNormalDecay( npc: L2Npc ) {
        if ( this.parameters.count > 0 ) {
            return super.processNormalDecay( npc )
        }

        this.stopRespawn()
        this.respawnTimer = setTimeout( this.startSpawn.bind( this ), MsInMinutes.One )
    }

    deInitializeSpawn() : void {
        if ( this.respawnTimer ) {
            clearTimeout( this.respawnTimer )
            this.respawnTimer = null
        }
    }
}