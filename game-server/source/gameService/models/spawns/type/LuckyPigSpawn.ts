import { L2DefaultSpawn } from './L2DefaultSpawn'
import { SpawnEventManager } from '../SpawnEventManager'
import { EventListenerMethod } from '../../events/listeners/EventListenerData'
import { GeneralHelper } from '../../../helpers/GeneralHelper'

const spawnHours = new Set<number>( [
    1,
    7,
    13,
    19
] )

export class LuckyPigSpawn extends L2DefaultSpawn {
    shouldSpawn: boolean = false
    readyToSpawn: boolean = false
    callback: EventListenerMethod
    timeout: NodeJS.Timeout

    protected initializeSpawn() {
        this.callback = this.onEvent.bind( this )
        this.registerEvent()
    }

    onEvent() : void {
        if ( this.timeout ) {
            return
        }

        this.timeout = setTimeout( this.runEventSpawn.bind( this ), GeneralHelper.minutesToMillis( 10 ) )
    }

    runEventSpawn() : void {
        this.shouldSpawn = true
        this.unRegisterEvent()
        this.timeout = null

        if ( !this.readyToSpawn ) {
            return
        }

        this.processStartSpawn()
    }

    getSpawnHours() : Set<number> {
        return spawnHours
    }

    protected registerEvent() : void {
        SpawnEventManager.registerDayNightTimer( this.getSpawnHours(), this.callback )
    }

    protected unRegisterEvent() : void {
        SpawnEventManager.unRegisterDayNightTimer( this.getSpawnHours(), this.callback )
    }
}