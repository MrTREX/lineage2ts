import { L2DefaultSpawn } from './L2DefaultSpawn'

export class OnOffSpawn extends L2DefaultSpawn {
    startSpawn(): number {
        return 0
    }
}