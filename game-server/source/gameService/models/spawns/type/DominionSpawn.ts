import { L2DefaultSpawn } from './L2DefaultSpawn'
import { TerritoryWarManager } from '../../../instancemanager/TerritoryWarManager'
import { TerritoryWard } from '../../../instancemanager/territory/TerritoryWard'

export class DominionSpawn extends L2DefaultSpawn {
    ward: TerritoryWard
    /*
        Dominion spawns are used to create 'Territory Manager' npcs.
        Due to geo-region spawn activation, we must map spawns to each wards in order to make
        territory wards activate together with spawns.
     */
    protected initializeSpawn() {
        let dominionId = this.maker.aiParameters?.dominionId as number
        this.ward = TerritoryWarManager.getTerritoryWard( dominionId )
        if ( !this.ward ) {
            return
        }

        this.ward.npcSpawns.push( this )
    }


    /*
        TODO: refactor and implement territory ward spawns
     */
    startSpawn(): number {
        if ( !TerritoryWarManager.isTWInProgress ) {
            return 0
        }

        return 0
    }
}