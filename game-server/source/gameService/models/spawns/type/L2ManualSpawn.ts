import { AITraitCache } from '../../../cache/AITraitCache'
import { ISpawnLogic, SpawnLogicEventData } from '../ISpawnLogic'
import { L2SpawnLogicItem } from '../../../../data/interface/SpawnLogicDataApi'
import { L2SpawnNpcDataItem } from '../../../../data/interface/SpawnNpcDataApi'
import { L2NpcTemplate } from '../../actor/templates/L2NpcTemplate'
import { L2Npc } from '../../actor/L2Npc'
import { getSpawnMethod, SpawnMethod } from '../SpawnHelper'
import { ConfigManager } from '../../../../config/ConfigManager'
import { L2Attackable } from '../../actor/L2Attackable'
import { GeoPolygonCache } from '../../../cache/GeoPolygonCache'
import { L2World } from '../../../L2World'
import { getNextSpawnId } from '../SpawnId'
import { TraitSettings } from '../../../aicontroller/interface/IAITrait'
import { SpawnLogicEventType } from '../../../enums/spawn/SpawnLogicEventType'

const manualSpawnMakerData : L2SpawnLogicItem = {
    aiParameters: undefined,
    avoidTerritoryIds: undefined,
    eventType: undefined,
    isFlying: false,
    logicName: 'manualSpawn',
    makerId: '',
    maxAmount: 0,
    spawnTerritoryIds: []
}

export interface L2ManualSpawnModifiers {
    instanceId: number
    isSummonSpawn: boolean
    isChampion: boolean
}

export class L2ManualSpawn implements ISpawnLogic {
    spawnedObjectIds: Set<number> = new Set<number>()
    template: L2NpcTemplate
    data: L2SpawnNpcDataItem
    id: string
    modifiers: L2ManualSpawnModifiers

    constructor( template: L2NpcTemplate,
                 data: L2SpawnNpcDataItem,
                 modifiers: L2ManualSpawnModifiers ) {
        this.id = getNextSpawnId()
        this.template = template
        this.data = data
        this.modifiers = modifiers
    }

    static fromParameters<Type extends InstanceType<typeof L2ManualSpawn>>( template: L2NpcTemplate,
                          amount: number,
                          x: number,
                          y: number,
                          z: number,
                          heading: number = 0,
                          instanceId: number = 0,
                          isSummonSpawn: boolean = false,
                          isChampion: boolean = false,
                          constructor: typeof L2ManualSpawn = L2ManualSpawn
    ) : Type {
        let spawnData : L2SpawnNpcDataItem = {
            recordId: null,
            aiName: null,
            aiParameters: null,
            amount,
            makerId: '',
            minions: null,
            npcId: template.getId(),
            position: {
                x,
                y,
                z,
                heading
            },
            respawnExtraMs: 0,
            respawnMs: 0,
            returnDistance: 0
        }

        let modifierData : L2ManualSpawnModifiers = {
            instanceId,
            isSummonSpawn,
            isChampion
        }

        return new constructor( template, spawnData, modifierData ) as Type
    }

    processSpawnEvent?( eventType: SpawnLogicEventType, data: SpawnLogicEventData ): void {
        switch ( eventType ) {
            case SpawnLogicEventType.Despawn:
                return this.startDespawn()

            case SpawnLogicEventType.Spawn:
                if ( this.hasSpawned() ) {
                    return
                }

                this.startSpawn()

                return
        }
    }

    deInitializeSpawn() : void {
        /*
            Method intended to be used to remove any initialization and event subscription logic.
         */
    }

    /*
        TODO : add minion functionality similar to what L2DefaultSpawn does
     */
    isMinion( npc: L2Npc ): boolean {
        return false
    }
    hasMinions(): boolean {
        return false
    }
    hasSpawnedMinions(): boolean {
        return false
    }
    onMasterTeleported( npc: L2Npc ): void {

    }
    removeMinions( npc: L2Npc ): void {

    }
    getMinionLeaderId( objectId: number ): number {
        return 0
    }
    getMinionIds( npc: L2Npc ): Set<number> {
        return null
    }

    getHeading(): number {
        return this.data.position.heading
    }

    getInstanceId(): number {
        return this.modifiers.instanceId
    }

    getMakerData(): L2SpawnLogicItem {
        return manualSpawnMakerData
    }

    getSpawnData(): L2SpawnNpcDataItem {
        return this.data
    }

    getX(): number {
        return this.data.position.x
    }

    getY(): number {
        return this.data.position.y
    }

    getZ(): number {
        return this.data.position.z
    }

    startDecay( npc: L2Npc ) {

    }

    startSpawn(): number {
        if ( this.hasSpawned() ) {
            return 0
        }

        this.createNpcs()

        return this.data.amount
    }

    getTemplate(): L2NpcTemplate {
        return this.template
    }

    private createNpcs(): void {
        let spawnMethod: SpawnMethod = getSpawnMethod( this.template.getType() )
        let aiTraits: TraitSettings

        for ( let index = 0; index < this.data.amount; index++ ) {
            let npc = spawnMethod( this.template ) as L2Npc

            this.spawnedObjectIds.add( npc.getObjectId() )

            if ( !aiTraits ) {
                aiTraits = AITraitCache.getNpcTraits( npc )
            }

            npc.setDefaultTraits( aiTraits )
            this.resetNpc( npc )
            this.spawnAtLocation( npc )
        }
    }

    protected resetNpc( npc: L2Npc ) {
        npc.setIsDead( false )
        npc.setDecayed( false )

        if ( npc.hasLoadedSkills() ) {
            npc.setMaxStats()
        }

        npc.setInstanceId( this.modifiers.instanceId )

        if ( npc.isAttackable() && ConfigManager.customs.championEnable() ) {
            ( npc as L2Attackable ).setChampion( this.modifiers.isChampion )
        }

        npc.setSummoner( 0 )
        npc.resetSummonedNpcs()
        npc.setNpcSpawn( this )
    }

    private spawnAtLocation( npc: L2Npc ): void {
        let correctedZ: number = GeoPolygonCache.getZ( this.data.position.x, this.data.position.y, this.data.position.z + npc.getTemplate().getCollisionHeight() )

        npc.setHeading( this.data.position.heading )
        npc.spawnMe( this.data.position.x, this.data.position.y, correctedZ )
    }

    getNpcs() : Array<L2Npc> {
        let npcs : Array<L2Npc> = []

        this.spawnedObjectIds.forEach( ( objectId: number ) => npcs.push( L2World.getObjectById( objectId ) as L2Npc ) )

        return npcs
    }

    remove( npc: L2Npc ) {
        this.spawnedObjectIds.delete( npc.getObjectId() )
    }

    hasSpawned(): boolean {
        return this.spawnedObjectIds.size > 0
    }

    getNpcObjectIds(): Array<number> {
        return Array.from( this.spawnedObjectIds )
    }

    stopRespawn() : void {
    }

    startDespawn(): void {
        this.spawnedObjectIds.forEach( ( objectId: number ) => {
            let npc = L2World.getObjectById( objectId ) as L2Npc
            if ( !npc ) {
                return
            }

            npc.deleteMe()
        } )

        this.spawnedObjectIds.clear()
    }

    getAmount(): number {
        return this.data.amount
    }

    getId(): string {
        return this.id
    }
}