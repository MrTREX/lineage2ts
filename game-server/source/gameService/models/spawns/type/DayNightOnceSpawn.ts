import { L2DefaultSpawn } from './L2DefaultSpawn'
import { GameTimeHourEvent } from '../../events/EventType'
import { EventListenerMethod } from '../../events/listeners/EventListenerData'
import { SpawnEventManager } from '../SpawnEventManager'

const nightTimeHours = new Set<number>( [ 2, 20 ] )
const dayTimeHours = new Set<number>( [ 8, 14 ] )
const anyHours = new Set<number>( [ ...nightTimeHours, ...dayTimeHours ] )

const enum DayNightType {
    Night,
    Day,
    Any
}

export class DayNightOnceSpawn extends L2DefaultSpawn {
    shouldSpawn: boolean = false
    readyToSpawn: boolean = false
    callback: EventListenerMethod

    protected initializeSpawn() {
        this.callback = this.onEvent.bind( this )
        this.registerEvent()
    }

    deInitializeSpawn() {
        this.unRegisterEvent()
    }

    startSpawn(): number {
        this.readyToSpawn = true

        if ( this.shouldSpawn ) {
            return this.processStartSpawn()
        }

        return 0
    }

    getSpawnHours() : Set<number> {
        switch ( this.maker.aiParameters?.isDay ) {
            case DayNightType.Night:
                return nightTimeHours

            case DayNightType.Day:
                return dayTimeHours
        }

        return anyHours
    }

    protected registerEvent() : void {
        SpawnEventManager.registerDayNightTimer( this.getSpawnHours(), this.callback )
    }

    protected unRegisterEvent() : void {
        SpawnEventManager.unRegisterDayNightTimer( this.getSpawnHours(), this.callback )
    }

    /*
        There are two ways npcs can be spawned:
        - via direct action from startSpawn, normally on geo-region activation
        - via timer event on specified hour, depending on night/day

        Hence, we must be waiting for spawn event:
        - when event is received, npcs can now be spawned
        - conditional on geo-region activation
        - if geo-region has already activated, we must spawn right away

        Spawn hours (game time) are:
        - 2 am
        - 8 am
        - 2 pm (14 military time)
        - 8 pm (20 military time)
     */
    onEvent( data: GameTimeHourEvent ) : void {
        this.shouldSpawn = true
        this.unRegisterEvent()

        if ( !this.readyToSpawn ) {
            return
        }

        this.processStartSpawn()
    }

    startDespawn() {
        this.shouldSpawn = false

        this.processDespawn()
        this.registerEvent()
    }
}