import { L2DefaultSpawn } from './L2DefaultSpawn'
import { L2Npc } from '../../actor/L2Npc'
import { DataManager } from '../../../../data/manager'
import { SpawnLogicEventType } from '../../../enums/spawn/SpawnLogicEventType'

export class ExclusiveSpawn extends L2DefaultSpawn {
    targetNpcId: number = 0

    protected initializeSpawn() {
        if ( !( this.maker.aiParameters?.makerName as string ) ) {
            return
        }

        let referenceId = this.maker.aiParameters?.uniqueNpc as string
        if ( !referenceId ) {
            return
        }

        /*
            Npc reference ids are prefixed with @ sign that need to be stripped off for lookups.
         */
        this.targetNpcId = DataManager.getNpcData().getIdByName( referenceId.slice( 1 ) )

        if ( !this.targetNpcId ) {
            throw new Error( `${ExclusiveSpawn.name} : unable to find npc by referenceId=${referenceId}` )
        }
    }

    protected spawnAtLocation( npc: L2Npc, x: number, y: number, z: number, heading: number ) {
        super.spawnAtLocation( npc, x, y, z, heading )

        if ( !this.targetNpcId || npc.getId() !== this.targetNpcId ) {
            return
        }

        let makerName = this.maker.aiParameters?.makerName as string
        if ( makerName ) {
            return this.sendEvent( makerName, SpawnLogicEventType.Despawn )
        }
    }
}