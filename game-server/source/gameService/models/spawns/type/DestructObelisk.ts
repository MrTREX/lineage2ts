import { ZoneEntry } from './ZoneEntry'
import { DoorManager } from '../../../cache/DoorManager'
import { L2Npc } from '../../actor/L2Npc'
import { SpawnLogicEventType } from '../../../enums/spawn/SpawnLogicEventType'

const allDoorNames: Array<string> = [
    'secret_door_001',
    'secret_door_002',
    'secret_door_003',
    'secret_door_004',
    'secret_door_005',
    'secret_door_006',
]

const eventDoorName: string = 'secret_door_007'

export class DestructObelisk extends ZoneEntry {
    counter: number
    spawnTimeout: NodeJS.Timeout

    protected initializeSpawn() {
        this.counter = 0

        for ( const name of allDoorNames ) {
            this.doorOperation( name, false )
        }
    }

    deInitializeSpawn() : void {
        this.clearTimeout()
    }

    processSignalParameters() : void {
        this.counter++

        this.doorOperation( eventDoorName, true )
        this.clearTimeout()

        this.spawnTimeout = setTimeout( this.sendSpawnEvent.bind( this ), 1000 )
    }

    doorOperation( name: string, isOpen: boolean ) : void {
        const door = DoorManager.getDoorByName( name )
        if ( door ) {
            isOpen ? door.openMe() : door.closeMe()
        }
    }

    startDecay( npc: L2Npc ): void {
        super.startDecay( npc )

        for ( const name of allDoorNames ) {
            this.doorOperation( name, true )
        }
        this.counter++
    }

    protected sendSpawnEvent() : void {
        const makerNames : Array<string> = [
            this.maker.aiParameters?.nearObeliskMaker1 as string,
            this.maker.aiParameters?.nearObeliskMaker2 as string,
            this.maker.aiParameters?.nearObeliskMaker3 as string,
            this.maker.aiParameters?.nearObeliskMaker4 as string,
            this.maker.aiParameters?.nearObeliskMaker5 as string,
            this.maker.aiParameters?.nearObeliskMaker6 as string,
            this.maker.aiParameters?.nearObeliskMaker7 as string,
            this.maker.aiParameters?.nearObeliskMaker8 as string,
        ]

        for ( const makerName of makerNames ) {
            this.sendEvent( makerName, SpawnLogicEventType.Spawn )
        }
    }

    protected clearTimeout() : void {
        if ( this.spawnTimeout ) {
            clearTimeout( this.spawnTimeout )
            this.spawnTimeout = null
        }
    }
}