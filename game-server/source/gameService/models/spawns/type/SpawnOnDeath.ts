import { L2Npc } from '../../actor/L2Npc'
import { NoRespawnOnDeath } from './NoRespawnOnDeath'
import { SpawnLogicEventType } from '../../../enums/spawn/SpawnLogicEventType'

export class SpawnOnDeath extends NoRespawnOnDeath {
    startDecay( npc: L2Npc ) {
        this.remove( npc )

        let name = this.maker.aiParameters?.gateMakerName as string ?? 'rumwarsha1224_331m1'
        this.sendEvent( name, SpawnLogicEventType.Spawn, null )
    }
}