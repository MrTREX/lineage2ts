import { L2DefaultSpawn } from './L2DefaultSpawn'
import { SpawnEventManager } from '../SpawnEventManager'
import { NamedEventAction, NamedEventSpawn } from '../SpawnEventType'
import { EventListenerMethod } from '../../events/listeners/EventListenerData'

export class EventSpawn extends L2DefaultSpawn {
    eventAction: NamedEventAction = NamedEventAction.None
    readyToSpawn: boolean = false
    callback: EventListenerMethod
    eventName: string

    startSpawn(): number {
        this.readyToSpawn = true

        if ( this.eventAction === NamedEventAction.Start ) {
            return this.processStartSpawn()
        }

        return 0
    }

    protected initializeSpawn() : void {
        this.eventName = this.maker.eventType || 'none'
        this.callback = this.onEvent.bind( this )

        this.registerEvent()
    }

    deInitializeSpawn() {
        this.unRegisterEvent()
    }

    onEvent( data: NamedEventSpawn ) : void {
        this.eventAction = data.action

        if ( !this.readyToSpawn ) {
            return
        }

        switch ( this.eventAction ) {
            case NamedEventAction.Start:
                this.processStartSpawn()
                return

            case NamedEventAction.End:
                this.startDespawn()
                return
        }
    }

    startDespawn() {
        this.eventAction = NamedEventAction.None
        this.processDespawn()
    }

    protected registerEvent() : void {
        SpawnEventManager.registerNamedTrigger( this.eventName, this.callback )
    }

    protected unRegisterEvent() : void {
        SpawnEventManager.unRegisterNamedTrigger( this.eventName, this.callback )
    }
}