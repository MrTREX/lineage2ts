import { L2DefaultSpawn } from './L2DefaultSpawn'

export class TreasureBoxSpawn extends L2DefaultSpawn {

    /*
        No code required yet. Treasure boxes appear to spawn across various territories at random.
        Meaning repeated territories are possible. Current implementation already works same way.
     */
}