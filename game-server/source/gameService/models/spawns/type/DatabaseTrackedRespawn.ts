import { L2DefaultSpawn } from './L2DefaultSpawn'
import { NpcRespawnManager } from '../../../cache/NpcRespawnManager'
import { L2Npc } from '../../actor/L2Npc'
import { L2NpcRespawnTableItem } from '../../../../database/interface/NpcRespawnTableApi'
import Timeout = NodeJS.Timeout

export class DatabaseTrackedRespawn extends L2DefaultSpawn {
    spawnTimer: Timeout
    existingRecord: L2NpcRespawnTableItem

    startSpawn(): number {
        this.existingRecord = NpcRespawnManager.getRecord( this.getRecordId() )
        if ( !this.existingRecord ) {
            return this.processStartSpawn()
        }

        /*
            Due to spawn activation we can immediately spawn npcs
            when we have less than five seconds left of waiting. Normally every second would
            count, however in goal of simplicity when region activates and spawns are active
            it would matter little if few seconds would not be counted. Otherwise, one
            can encounter creation of timer, which is not strictly necessary.
         */
        let respawnTime = Date.now() - this.existingRecord.killTime - this.getRespawnDelay()
        if ( respawnTime < 5000 ) {
            NpcRespawnManager.removeRecord( this.data.recordId )
            return this.processStartSpawn()
        }

        if ( this.spawnTimer ) {
            clearTimeout( this.spawnTimer )
        }

        this.spawnTimer = setTimeout( this.runSpawnTask.bind( this ), respawnTime )

        return 0
    }

    /*
        Possible to have empty recordId on npc data, in such case we can use npcId.
     */
    private getRecordId() : string {
        return this.data.recordId ?? this.data.npcId.toString()
    }

    protected getRespawnDelay() : number {
        return this.data.respawnMs ?? 0
    }

    runSpawnTask() : void {
        this.existingRecord = null
        NpcRespawnManager.removeRecord( this.getRecordId() )
        this.processStartSpawn()
    }

    startDecay( npc: L2Npc ) {
        if ( this.data.recordId ) {
            this.existingRecord = {
                heading: npc.getHeading(),
                hp: 0,
                id: this.getRecordId(),
                killTime: Date.now(),
                lastUpdate: Date.now(),
                mp: 0,
                x: npc.getX(),
                y: npc.getY(),
                z: npc.getZ()
            }

            NpcRespawnManager.updateRecord( this.existingRecord )
        }

        super.startDecay( npc )
    }

    protected setStats( npc: L2Npc ) {
        if ( !this.existingRecord ) {
            return super.setStats( npc )
        }

        /*
            Saved hp and mp values depend on any passive skills and attributes.
            Hence, we must load npc skills first to ensure that existing stats
            can apply properly.
         */
        npc.prepareSkills().then( () => {
            npc.getStatus().setCurrentHp( this.existingRecord.hp )
            npc.getStatus().setCurrentMp( this.existingRecord.mp )
        } )
    }

    protected spawnAtLocation( npc: L2Npc, x: number, y: number, z: number, heading: number ): void {
        if ( !this.existingRecord ) {
            return super.spawnAtLocation( npc, x, y, z, heading )
        }

        return super.spawnAtLocation( npc, this.existingRecord.x, this.existingRecord.y, this.existingRecord.z, this.existingRecord.heading )
    }
}