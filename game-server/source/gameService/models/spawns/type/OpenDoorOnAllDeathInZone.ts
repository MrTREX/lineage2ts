import { ZoneEntry } from './ZoneEntry'
import { L2Npc } from '../../actor/L2Npc'
import { DoorManager } from '../../../cache/DoorManager'
import { SpawnLogicEventType } from '../../../enums/spawn/SpawnLogicEventType'

/*
    While original implementation has a way to open door on individual npc death,
    there are no known maker parameters that show use of such functionality.
 */
export class OpenDoorOnAllDeathInZone extends ZoneEntry {
    startDecay( npc: L2Npc ): void {
        super.startDecay( npc )

        if ( this.parameters.count !== 0 ) {
            return
        }

        let doorName = this.maker.aiParameters?.doorName as string
        if ( !doorName ) {
            return
        }

        let door = DoorManager.getDoorByName( doorName )
        if ( !door ) {
            return
        }

        door.openMe()

        let nextMakerName = this.maker.aiParameters?.nextMaker as string
        if ( !nextMakerName ) {
            return
        }

        return this.sendEvent( nextMakerName, SpawnLogicEventType.Spawn )
    }
}