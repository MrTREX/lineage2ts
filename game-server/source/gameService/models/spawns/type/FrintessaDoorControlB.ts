import { ZoneEntry } from './ZoneEntry'
import { SignalParametersData } from '../ISpawnLogic'
import { DoorManager } from '../../../cache/DoorManager'
import { SpawnLogicEventType } from '../../../enums/spawn/SpawnLogicEventType'

/*
    Very odd spawn that ties door logic to specific single npc being spawned.
 */
export class FrintessaDoorControlB extends ZoneEntry {
    processSpawnEvent( eventType: SpawnLogicEventType, data: SignalParametersData ) {
        if ( eventType !== SpawnLogicEventType.SignalParameters || !data ) {
            return
        }

        switch ( data.signalId ) {
            case 10001002:
                return this.onAttacked()

            case 10001003:
                return this.onAllDie()
        }
    }

    private onAttacked() : void {
        this.doorOperation( [
            'frintessa_new_door_151',
            'frintessa_new_door_251',
            'frintessa_new_door_252',
            'frintessa_new_door_351',
        ], false )

        this.doorOperation( [
            'frintessa_new_door_201',
            'frintessa_new_door_202',
            'frintessa_new_door_203',
            'frintessa_new_door_204',
            'frintessa_new_door_205',
            'frintessa_new_door_206',
            'frintessa_new_door_207',
            'frintessa_new_door_208',
            'frintessa_new_door_209',
            'frintessa_new_door_210',
        ], true )
    }

    private onAllDie() : void {
        this.doorOperation( [
            'frintessa_new_door_251',
            'frintessa_new_door_351',
        ], true )
    }

    private doorOperation( doorNames: Array<string>, isOpen: boolean ) : void {
        return doorNames.forEach( ( name: string ) : void => {
            let door = DoorManager.getDoorByName( name )
            if ( door ) {
                isOpen ? door.openMe() : door.closeMe()
            }
        } )
    }
}