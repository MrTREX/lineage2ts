import { L2DefaultSpawn } from './L2DefaultSpawn'
import { L2Npc } from '../../actor/L2Npc'
import { SpawnLogicEventType } from '../../../enums/spawn/SpawnLogicEventType'

export class LevelSpawn extends L2DefaultSpawn {
    startDecay( npc: L2Npc ) {
        this.remove( npc )

        let name = this.maker.aiParameters?.nextMakerName as string ?? 'level_switch'
        this.sendEvent( name, SpawnLogicEventType.Spawn, null )
    }
}