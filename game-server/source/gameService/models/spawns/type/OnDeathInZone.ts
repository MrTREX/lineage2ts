import { ZoneEntry } from './ZoneEntry'
import { L2Npc } from '../../actor/L2Npc'
import { SpawnMakerCache } from '../../../cache/SpawnMakerCache'
import { ListenerCache } from '../../../cache/ListenerCache'
import { ISpawnLogic } from '../ISpawnLogic'
import { EventType, NpcReceivingEvent } from '../../events/EventType'
import { EventPoolCache } from '../../../cache/EventPoolCache'

export class OnDeathInZone extends ZoneEntry {
    startDecay( npc: L2Npc ): void {
        super.startDecay( npc )

        let name = this.maker.aiParameters?.dMakerName as string
        if ( !name ) {
            return
        }

        let spawns = SpawnMakerCache.getMakerSpawn( name )
        if ( !spawns ) {
            return
        }

        spawns.forEach( ( spawn: ISpawnLogic ) => {
            let npcId = spawn.getTemplate().getId()

            if ( ListenerCache.hasNpcTemplateListeners( npcId, EventType.NpcReceivingEvent ) ) {
                let eventData = EventPoolCache.getData( EventType.NpcReceivingEvent ) as NpcReceivingEvent

                eventData.eventName = '1624003'
                eventData.referenceId = 0
                eventData.senderId = 0
                eventData.receiverId = npcId

                return ListenerCache.sendNpcTemplateEvent( npcId, EventType.NpcReceivingEvent, eventData )
            }
        } )
    }
}