import { ZoneEntry } from './ZoneEntry'
import { L2Npc } from '../../actor/L2Npc'
import { SpawnLogicEventType } from '../../../enums/spawn/SpawnLogicEventType'

const defaultMaker = 'rumwarsha1224_010m1'

export class SendEventOnDeathInZone extends ZoneEntry {
    startDecay( npc: L2Npc ): void {
        this.remove( npc )

        let makerName = this.maker.aiParameters?.obeliskMaker as string ?? defaultMaker
        return this.sendEvent( makerName, SpawnLogicEventType.SignalParameters )
    }
}