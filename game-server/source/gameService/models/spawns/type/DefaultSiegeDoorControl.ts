import { SpawnEventManager } from '../SpawnEventManager'
import { SpawnEventType } from '../SpawnEventType'
import { DoorManager } from '../../../cache/DoorManager'
import { DatabaseTrackedRespawn } from './DatabaseTrackedRespawn'
import { EventListenerMethod } from '../../events/listeners/EventListenerData'

export class DefaultSiegeDoorControl extends DatabaseTrackedRespawn {

    startCallback: EventListenerMethod
    endCallback: EventListenerMethod

    protected initializeSpawn() {
        // TODO : add logic to handle database stored npc data

        this.startCallback = this.onSiegeStart.bind( this )
        this.endCallback = this.onSiegeEnd.bind( this )

        this.registerEvents()
    }

    deInitializeSpawn() {
        this.unRegisterEvents()
    }

    startSpawn(): number {
        return 0
    }

    onSiegeStart() : void {
        this.processStartSpawn()
        this.doorOperation( false )
    }

    onSiegeEnd() : void {
        this.processDespawn()
        this.doorOperation( true )
    }

    private doorOperation( isOpen: boolean ) : void {
        return [
            'doorName1',
            'doorName2',
            'doorName3',
            'doorName4',
            'doorName5',
        ].forEach( ( propertyName: string ) : void => {
            let doorName = this.maker.aiParameters[ propertyName ] as string
            if ( doorName ) {
                let door = DoorManager.getDoorByName( doorName )
                if ( door ) {
                    isOpen ? door.openMe() : door.closeMe()
                }
            }
        } )
    }

    protected registerEvents() : void {
        let eventIds = new Set<number>( [ this.maker.aiParameters.castleId as number ] )

        SpawnEventManager.registerCallback( SpawnEventType.SiegeStart, this.startCallback, eventIds )
        SpawnEventManager.registerCallback( SpawnEventType.SiegeEnd, this.endCallback, eventIds )
    }

    protected unRegisterEvents() : void {
        SpawnEventManager.unRegisterCallback( SpawnEventType.SiegeStart, this.startCallback )
        SpawnEventManager.unRegisterCallback( SpawnEventType.SiegeEnd, this.endCallback )
    }
}