import { L2DefaultSpawn } from './L2DefaultSpawn'

export class RandomAmountSpawn extends L2DefaultSpawn {

    /*
        Very odd implementation in terms of how it works.
        In original implementation single npc is picked from spawn to be spawned in random territory.
        In current implementation, we simply would provide amount of npcs to be spawned, while rest of logic
        will take care of picking random territory and single point out of such territory for location.
     */
    getAmount(): number {
        return 1
    }
}