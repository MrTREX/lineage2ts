import { L2SpawnTerritoryItem } from '../../../data/interface/SpawnTerritoriesDataApi'
import _ from 'lodash'
import { RoaringBitmap32 } from 'roaring'

export class L2SpawnTerritory {
    data: L2SpawnTerritoryItem
    private x: number
    private y: number
    private z: number
    private points: RoaringBitmap32

    constructor( data: L2SpawnTerritoryItem ) {
        this.data = data
        this.points = new RoaringBitmap32( data.geometryPoints )
    }

    hasNoPoints() : boolean {
        return this.points.size === 0
    }

    hasNoGeometry() : boolean {
        return !this.data.geometryPoints
    }

    generatePoint() : void {
        if ( this.hasNoPoints() ) {
            this.points.addMany( this.data.geometryPoints )
        }

        let value = this.data.geometryPoints.at( Math.random() * this.data.geometryPoints.size )
        this.points.remove( value )

        this.x = ( 0x0000FFFF & value ) + this.data.parameters.minX
        this.y = ( value >> 16 ) + this.data.parameters.minY
        this.z = _.random( this.data.minZ, this.data.maxZ )
    }

    getX() : number {
        return this.x
    }

    getY() : number {
        return this.y
    }

    getZ() : number {
        return this.z
    }

    clone() : L2SpawnTerritory {
        return new L2SpawnTerritory( this.data )
    }
}