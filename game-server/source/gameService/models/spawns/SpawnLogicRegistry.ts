import { ISpawnLogic, SpawnSharedParameters } from './ISpawnLogic'
import { L2DefaultSpawn } from './type/L2DefaultSpawn'
import { L2SpawnNpcDataItem } from '../../../data/interface/SpawnNpcDataApi'
import { L2SpawnLogicItem } from '../../../data/interface/SpawnLogicDataApi'
import { EventSpawn } from './type/EventSpawn'
import { DayNightOnceSpawn } from './type/DayNightOnceSpawn'
import { InstantRandomSpawn } from './type/InstantRandomSpawn'
import { ZoneEntry } from './type/ZoneEntry'
import { RandomAmountSpawn } from './type/RandomAmountSpawn'
import { DefaultCrataeSpawn } from './type/DefaultCrataeSpawn'
import { FortressSpawn } from './type/FortressSpawn'
import { OnOffSpawn } from './type/OnOffSpawn'
import { TreasureBoxSpawn } from './type/TreasureBoxSpawn'
import { DayNightSpawn } from './type/DayNightSpawn'
import { DominionSpawn } from './type/DominionSpawn'
import { OlympiadManager } from './type/OlympiadManager'
import { DatabaseTrackedRespawn } from './type/DatabaseTrackedRespawn'
import { CleftOne } from './type/CleftOne'
import { DefaultCycleManagementSpawn } from './type/DefaultCycleManagementSpawn'
import { AddCyclePointsOnDecay } from './type/AddCyclePointsOnDecay'
import { InstantRespawn } from './type/InstantRespawn'
import { GuardSpawn } from './type/GuardSpawn'
import { DefaultSiegeDoorControl } from './type/DefaultSiegeDoorControl'
import { InstantSiegeSpawn } from './type/InstantSiegeSpawn'
import { FrintessaDoorControlB } from './type/FrintessaDoorControlB'
import { FrintessaDoorControlA } from './type/FrintessaDoorControlA'
import { LevelSpawn } from './type/LevelSpawn'
import { SpawnOnDeath } from './type/SpawnOnDeath'
import { NoRespawnOnDeath } from './type/NoRespawnOnDeath'
import { OpenDoorOnDeath } from './type/OpenDoorOnDeath'
import { SeedGathering } from './type/SeedGathering'
import { L2SpawnDefaults } from '../../enums/spawn/L2SpawnDefaults'
import { LuckyPigSpawn } from './type/LuckyPigSpawn'
import { SepulcherSpawn } from './type/SepulcherSpawn'
import { InstallAllRespawn } from './type/InstallAllRespawn'
import { GuardControlSpawn } from './type/GuardControlSpawn'
import { DoorZoneSpawn } from './type/DoorZoneSpawn'
import { ExclusiveSpawn } from './type/ExclusiveSpawn'
import { OnDeathInZone } from './type/OnDeathInZone'
import { OnAllDeathInZone } from './type/OnAllDeathInZone'
import { RespawnOnAllDead } from './type/RespawnOnAllDead'
import { NoRespawnInZone } from './type/NoRespawnInZone'
import { OpenDoorOnAllDeathInZone } from './type/OpenDoorOnAllDeathInZone'
import { RespawnCounter } from './type/RespawnCounter'
import { SendEventOnDeathInZone } from './type/SendEventOnDeathInZone'
import { DestructObelisk } from './type/DestructObelisk'

export type SpawnCreator = new ( data: L2SpawnNpcDataItem, maker: L2SpawnLogicItem, parameters: SpawnSharedParameters ) => ISpawnLogic

// TODO : correct discrepancies between logic and logicEx spawns
export const SpawnLogicRegistry : Record<string, SpawnCreator> = {
    [ L2SpawnDefaults.SpawnLogicEx ]: L2DefaultSpawn,
    [ L2SpawnDefaults.SpawnLogic ]: L2DefaultSpawn,
    'event_maker': EventSpawn,
    'on_timer_parade_maker': DayNightOnceSpawn,
    'maker_instant_spawn_random': InstantRandomSpawn,
    'inzone_maker': ZoneEntry,
    'random_spawn': RandomAmountSpawn,
    'maker_cratae_monster': DefaultCrataeSpawn,
    'fortress_maker': FortressSpawn,
    'fortress_db_maker': FortressSpawn,
    'no_on_start_maker': OnOffSpawn,
    'random_spawn_treasurebox': TreasureBoxSpawn,
    'on_day_night_spawn': DayNightSpawn,
    'dominion_maker': DominionSpawn,
    'olympiad_manager_maker': OlympiadManager,
    'valley_of_dragon_event_maker': L2DefaultSpawn,
    'default_use_db_maker': DatabaseTrackedRespawn,
    'adv_master_maker': L2DefaultSpawn,
    'cleft_maker': CleftOne,
    'cleft_maker_2': L2DefaultSpawn,
    'fieldcycle_maker': DefaultCycleManagementSpawn,
    'native_town_maker': DefaultCycleManagementSpawn,
    'hellbound_maker': L2DefaultSpawn,
    'immo_npc_maker': AddCyclePointsOnDecay,
    'cruma_instant_spawn': InstantRespawn,
    'cruma_default_maker': L2DefaultSpawn,
    'guard_control_maker': GuardSpawn,
    'zaken_day_maker': ZoneEntry,
    'zaken_night_maker': ZoneEntry,
    'zaken_candle_maker': ZoneEntry,
    'named_a2_caller_maker': ZoneEntry,
    'named_a3_caller_maker': ZoneEntry,
    'farm_maker': DefaultSiegeDoorControl,
    'dream_boundary_battle_maker': ZoneEntry, // TODO : integrate with main spawn logic,
    'rainbow_maker': InstantSiegeSpawn,
    'statue_of_shilen_maker': L2DefaultSpawn,
    'maker_beastfarm_petmanager': L2DefaultSpawn,
    'crystal_piece_drop_maker': L2DefaultSpawn,
    'b_time_renew1_maker': L2DefaultSpawn,
    'hall_a_controller_maker': FrintessaDoorControlA,
    'hall_b_controller_maker': FrintessaDoorControlB,
    'sm_level_default': LevelSpawn,
    'maker_destruct_fort_teleporter': ZoneEntry,
    'maker_destruct_default': NoRespawnOnDeath,
    'maker_destruct_1die_to_help': SpawnOnDeath,
    'maker_destruct_2side_seal': OpenDoorOnDeath,
    'a_seed_gathering_maker': SeedGathering,
    'jackpot_event_maker': LuckyPigSpawn,
    'royal_rush_maker': SepulcherSpawn,
    'maker_castle_pailaka_invader_private': ZoneEntry,
    'royal_req_next_maker': SepulcherSpawn, // TODO : add instance handling logic since maker requires triggering other makers
    'manage_teleport_dungeon': L2DefaultSpawn,
    'sm_level': L2DefaultSpawn,
    'maker_instant_spawn': InstallAllRespawn,
    'maker_ssq2_temple_of_silence': ZoneEntry,
    'unique_npc_kill_event': L2DefaultSpawn, // TODO : implement on kill logic using event listeners
    'guarders_maker': GuardControlSpawn,
    'inzone_door_open_maker': DoorZoneSpawn,
    'exclusive_spawn_normal': ExclusiveSpawn,
    'maker_labyrinth_b': OnDeathInZone, // TODO : add listener implementation to cast skill on event
    'hall_a_monster_maker': OnAllDeathInZone, // TODO : find out if event does anything
    'level_switch': RespawnOnAllDead,
    'maker_destruct_no_start_spawn': NoRespawnInZone,
    'ssq_emperor_seal_maker': OpenDoorOnAllDeathInZone,
    'ssq_emperor_seal_battlezone': ZoneEntry,
    'ssq_emperor_seal_battlezone_manager': ZoneEntry,
    'maker_instant_spawn_serial': RespawnCounter,
    'maker_instant_spawn_serial_last': RespawnCounter, // TODO : find out if there are any differences from maker_instant_spawn_serial
    'maker_destruct_center_seal': SendEventOnDeathInZone,
    'maker_destruct_obelisk': DestructObelisk,
}