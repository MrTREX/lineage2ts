import { SpawnMakerCache } from '../../cache/SpawnMakerCache'
import { ISpawnLogic } from './ISpawnLogic'
import { ListenerCache } from '../../cache/ListenerCache'
import { EventType, NpcReceivingEvent } from '../events/EventType'
import { EventPoolCache } from '../../cache/EventPoolCache'

export function sendListenerEvent( makerName: string, eventName: string, referenceId: number = 0, senderId: number = 0, receiverId: number = 0 ) : void {
    if ( !makerName ) {
        return
    }

    let spawns = SpawnMakerCache.getMakerSpawn( makerName )
    if ( !spawns ) {
        return
    }

    spawns.forEach( ( spawn: ISpawnLogic ) => {
        let npcId = spawn.getTemplate().getId()

        if ( ListenerCache.hasNpcTemplateListeners( npcId, EventType.NpcReceivingEvent ) ) {
            let eventData = EventPoolCache.getData( EventType.NpcReceivingEvent ) as NpcReceivingEvent

            eventData.eventName = eventName
            eventData.referenceId = referenceId
            eventData.senderId = senderId
            eventData.receiverId = receiverId

            return ListenerCache.sendNpcTemplateEvent( npcId, EventType.NpcReceivingEvent, eventData )
        }
    } )
}