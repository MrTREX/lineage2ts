import { L2Npc } from '../actor/L2Npc'
import { ILocational } from '../Location'
import { L2NpcTemplate } from '../actor/templates/L2NpcTemplate'
import { L2SpawnNpcDataItem } from '../../../data/interface/SpawnNpcDataApi'
import { L2SpawnLogicItem } from '../../../data/interface/SpawnLogicDataApi'
import { SpawnLogicEventType } from '../../enums/spawn/SpawnLogicEventType'

export interface SpawnLogicEventData {
    [ key: string ]: string | number | boolean
}

export interface ISpawnMinionOperations {
    isMinion( npc: L2Npc ) : boolean
    hasMinions() : boolean
    hasSpawnedMinions() : boolean
    onMasterTeleported( npc: L2Npc ) : void
    removeMinions( npc: L2Npc ) : void
    getMinionLeaderId( objectId: number ) : number
    getMinionIds( npc: L2Npc ) : Set<number>
}

export interface ISpawnLogic extends ILocational, ISpawnMinionOperations {
    getId() : string
    startSpawn() : number
    startDecay( npc: L2Npc ) : void
    getTemplate() : L2NpcTemplate
    getSpawnData() : L2SpawnNpcDataItem
    getMakerData() : L2SpawnLogicItem
    remove( npc: L2Npc ) : void
    hasSpawned() : boolean
    getNpcs() : Array<L2Npc>
    getNpcObjectIds() : Array<number>
    stopRespawn(): void
    startDespawn() : void
    getAmount() : number
    processSpawnEvent?( eventType: SpawnLogicEventType, data: SpawnLogicEventData ) : void
}

export interface IInstanceSpawn {
    setInstanceId( instanceId: number ) : void
    setCanSpawn( value: boolean ) : void
}

export interface SpawnSharedParameters {
    count: number
}

export interface RespawnEventData extends SpawnLogicEventData {
    respawnMs: number
}

export interface SignalParametersData extends SpawnLogicEventData {
    signalId: number
}