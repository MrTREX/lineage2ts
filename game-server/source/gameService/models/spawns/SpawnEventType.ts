import { FortEvent } from '../../enums/FortEvent'

export const defaultTriggerIds = new Set<number>( [ 0 ] )

export const enum SpawnEventType {
    OlympiadZone,
    CycleStepChange,
    ZoneEntry,
    SiegeStart,
    SiegeEnd,
}

export interface SpawnEventData {
    [ key: string ]: string | number | boolean | Array<number>
}

export const enum NamedEventAction {
    None,
    Start,
    End
}
export interface NamedEventSpawn extends SpawnEventData {
    action: NamedEventAction
}

export interface ZoneEntrySpawn extends SpawnEventData {
    clusterId: number
    inZone: boolean
}

export interface FortEventSpawn extends SpawnEventData {
    eventId: FortEvent
}

export interface OlympiadZoneSpawn extends SpawnEventData {
    enabled: boolean
}