export enum MoveType {
    WALK,
    RUN,
    FAST_SWIM,
    SLOW_SWIM,
    FAST_FLY,
    SLOW_FLY
}