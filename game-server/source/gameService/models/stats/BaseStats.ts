import { DataManager } from '../../../data/manager'
import { L2Character } from '../actor/L2Character'
import _ from 'lodash'

export const BaseStats = {
    STR( character: L2Character ) : number {
        return _.nth( DataManager.getBaseStatsData().getSTR(), character.getSTR() ) || 0
    },

    CON( character: L2Character ) : number {
        return _.nth( DataManager.getBaseStatsData().getCON(), character.getCON() ) || 0
    },

    INT( character: L2Character ) : number {
        return _.nth( DataManager.getBaseStatsData().getINT(), character.getINT() ) || 0
    },

    DEX( character: L2Character ) : number {
        return _.nth( DataManager.getBaseStatsData().getDEX(), character.getDEX() ) || 0
    },

    WIT( character: L2Character ) : number {
        return _.nth( DataManager.getBaseStatsData().getWIT(), character.getWIT() ) || 0
    },

    MEN( character: L2Character ) : number {
        return _.nth( DataManager.getBaseStatsData().getMEN(), character.getMEN() ) || 0
    },

    NONE() : number {
        return 1
    }
}