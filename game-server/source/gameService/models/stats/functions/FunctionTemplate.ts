import { StatFunction } from '../../../enums/StatFunction'
import { AbstractFunction } from './AbstractFunction'

export class FunctionTemplate {
    values: FunctionTemplateValues
    method: ( values: FunctionTemplateValues, owner: unknown ) => AbstractFunction

    constructor( name: string, stat: string, value: number ) {
        let [ statMethod, order ] = StatFunction[ name.toUpperCase() ]

        this.values = {
            order,
            stat,
            value,
        }

        this.method = statMethod
    }

    generateFunction( owner: any ) : AbstractFunction {
        return this.method( this.values, owner )
    }
}

export interface FunctionTemplateValues {
    stat: string
    order: number
    value: number
}

export const NullTemplateValues: FunctionTemplateValues = {
    order: 1,
    stat: null,
    value: 0,
}