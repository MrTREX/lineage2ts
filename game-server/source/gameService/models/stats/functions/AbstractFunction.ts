import { L2Character } from '../../actor/L2Character'
import { Condition } from '../../conditions/Condition'
import { Skill } from '../../Skill'
import { FunctionTemplateValues } from './FunctionTemplate'

export abstract class AbstractFunction {
    functionOwner: any
    template : FunctionTemplateValues

    constructor( values: FunctionTemplateValues, owner: any ) {
        this.template = values
        this.functionOwner = owner
    }

    abstract calculate( effector: L2Character, affected: L2Character, skill: Skill, initialValue: number ) : number

    getApplyCondition() : Condition {
        return null
    }

    getStat() : string {
        return this.template.stat
    }

    getValue() : number {
        return this.template.value
    }

    getFunctionOwner() : any {
        return this.functionOwner
    }
}