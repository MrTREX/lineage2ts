import { AbstractFunction } from '../AbstractFunction'
import { L2Character } from '../../../actor/L2Character'
import { Skill } from '../../../Skill'
import { L2ItemInstance } from '../../../items/instance/L2ItemInstance'
import { ConfigManager } from '../../../../../config/ConfigManager'
import { Stats } from '../../Stats'
import { CrystalType } from '../../../items/type/CrystalType'
import { WeaponType } from '../../../items/type/WeaponType'
import { FunctionTemplateValues } from '../FunctionTemplate'
import { L2ItemSlots } from '../../../../enums/L2ItemSlots'

export class FunctionEnchant extends AbstractFunction {
    static fromParameters( values: FunctionTemplateValues, owner: any ): FunctionEnchant {
        return new FunctionEnchant( values, owner )
    }

    calculate( effector: L2Character, affected: L2Character, skill: Skill, initialValue: number ) {
        if ( this.getApplyCondition() && !this.getApplyCondition().test( effector, affected, skill ) ) {
            return initialValue
        }

        let item: L2ItemInstance = this.getFunctionOwner() as L2ItemInstance
        let enchantLevel = item.getEnchantLevel()
        if ( enchantLevel <= 0 ) {
            return initialValue
        }

        let overEnchant = 0
        if ( enchantLevel > 3 ) {
            overEnchant = enchantLevel - 3
            enchantLevel = 3
        }

        let olympidEnchant = ConfigManager.olympiad.getEnchantLimit()
        if ( effector.isPlayer() ) {
            if ( effector.getActingPlayer().isInOlympiadMode()
                    && olympidEnchant >= 0
                    && ( enchantLevel + overEnchant ) > olympidEnchant ) {

                if ( olympidEnchant > 3 ) {
                    overEnchant = olympidEnchant - 3
                } else {
                    overEnchant = 0
                    enchantLevel = olympidEnchant
                }
            }
        }

        if ( this.getStat() === Stats.MAGIC_DEFENCE || this.getStat() === Stats.POWER_DEFENCE ) {
            return initialValue + enchantLevel + ( 3 * overEnchant )
        }

        let outcome = initialValue
        if ( this.getStat() === Stats.MAGIC_ATTACK ) {
            // M. Atk. increases by 4 for all weapons.
            // Starting at +4, M. Atk. bonus double.
            // M. Atk. increases by 3 for all weapons.
            // Starting at +4, M. Atk. bonus double.
            // M. Atk. increases by 2 for all weapons. Starting at +4, M. Atk. bonus double.
            // Starting at +4, M. Atk. bonus double.

            switch ( item.getItem().getItemGradeSPlus() ) {
                case CrystalType.S:
                    outcome = outcome + ( 4 * enchantLevel ) + ( 8 * overEnchant )
                    break
                case CrystalType.A:
                case CrystalType.B:
                case CrystalType.C:
                    outcome = outcome + ( 3 * enchantLevel ) + ( 6 * overEnchant )
                    break
                case CrystalType.D:
                case CrystalType.NONE:
                    outcome = outcome + ( 2 * enchantLevel ) + ( 4 * overEnchant )
                    break
            }

            return outcome
        }

        if ( item.isWeapon() ) {
            let type: WeaponType = item.getItemType() as WeaponType
            switch ( item.getItem().getItemGradeSPlus() ) {
                case CrystalType.S:
                    if ( item.getWeaponItem().getBodyPart() === L2ItemSlots.AllHandSlots ) {
                        if ( type === WeaponType.BOW || type === WeaponType.CROSSBOW ) {
                            // P. Atk. increases by 10 for bows.
                            // Starting at +4, P. Atk. bonus double.
                            outcome += ( 10 * enchantLevel ) + ( 20 * overEnchant )
                        } else {
                            // P. Atk. increases by 6 for two-handed swords, two-handed blunts, dualswords, and two-handed combat weapons.
                            // Starting at +4, P. Atk. bonus double.
                            outcome += ( 6 * enchantLevel ) + ( 12 * overEnchant )
                        }
                    } else {
                        // P. Atk. increases by 5 for one-handed swords, one-handed blunts, daggers, spears, and other weapons.
                        // Starting at +4, P. Atk. bonus double.
                        outcome += ( 5 * enchantLevel ) + ( 10 * overEnchant )
                    }
                    break
                case CrystalType.A:
                    if ( item.getWeaponItem().getBodyPart() === L2ItemSlots.AllHandSlots ) {
                        if ( type === WeaponType.BOW || type === WeaponType.CROSSBOW ) {
                            // P. Atk. increases by 8 for bows.
                            // Starting at +4, P. Atk. bonus double.
                            outcome += ( 8 * enchantLevel ) + ( 16 * overEnchant )
                        } else {
                            // P. Atk. increases by 5 for two-handed swords, two-handed blunts, dualswords, and two-handed combat weapons.
                            // Starting at +4, P. Atk. bonus double.
                            outcome += ( 5 * enchantLevel ) + ( 10 * overEnchant )
                        }
                    } else {
                        // P. Atk. increases by 4 for one-handed swords, one-handed blunts, daggers, spears, and other weapons.
                        // Starting at +4, P. Atk. bonus double.
                        outcome += ( 4 * enchantLevel ) + ( 8 * overEnchant )
                    }
                    break
                case CrystalType.B:
                case CrystalType.C:
                    if ( item.getWeaponItem().getBodyPart() === L2ItemSlots.AllHandSlots ) {
                        if ( type === WeaponType.BOW || type === WeaponType.CROSSBOW ) {
                            // P. Atk. increases by 6 for bows.
                            // Starting at +4, P. Atk. bonus double.
                            outcome += ( 6 * enchantLevel ) + ( 12 * overEnchant )
                        } else {
                            // P. Atk. increases by 4 for two-handed swords, two-handed blunts, dualswords, and two-handed combat weapons.
                            // Starting at +4, P. Atk. bonus double.
                            outcome += ( 4 * enchantLevel ) + ( 8 * overEnchant )
                        }
                    } else {
                        // P. Atk. increases by 3 for one-handed swords, one-handed blunts, daggers, spears, and other weapons.
                        // Starting at +4, P. Atk. bonus double.
                        outcome += ( 3 * enchantLevel ) + ( 6 * overEnchant )
                    }
                case CrystalType.D:
                case CrystalType.NONE:
                    switch ( type ) {
                        case WeaponType.BOW:
                        case WeaponType.CROSSBOW:
                            outcome += ( 4 * enchantLevel ) + ( 8 * overEnchant )
                            break
                        default:
                            outcome += ( 2 * enchantLevel ) + ( 4 * overEnchant )
                            break
                    }
                    break
            }
        }

        return outcome
    }
}