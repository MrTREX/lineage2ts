import { AbstractFunction } from '../AbstractFunction'
import { L2Character } from '../../../actor/L2Character'
import { Skill } from '../../../Skill'
import { FunctionTemplateValues } from '../FunctionTemplate'

export class FunctionAdd extends AbstractFunction {

    static fromParameters( values: FunctionTemplateValues, owner: any ): FunctionAdd {
        return new FunctionAdd( values, owner )
    }

    calculate( effector: L2Character, affected: L2Character, skill: Skill, initialValue: number ) {
        if ( !this.getApplyCondition() || this.getApplyCondition().test( effector, affected, skill ) ) {
            return initialValue + this.getValue()
        }

        return initialValue
    }
}