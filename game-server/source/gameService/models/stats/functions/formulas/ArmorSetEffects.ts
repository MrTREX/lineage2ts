import { Stats } from '../../Stats'
import { DataManager } from '../../../../../data/manager'
import { L2Character } from '../../../actor/L2Character'
import { L2PcInstance } from '../../../actor/instance/L2PcInstance'
import { AbstractFunction } from '../AbstractFunction'
import { Skill } from '../../../Skill'
import { L2ArmorSet } from '../../../L2ArmorSet'
import { NullTemplateValues } from '../FunctionTemplate'

class FunctionArmorSet extends AbstractFunction {

    constructor() {
        super( NullTemplateValues, null )
    }

    calculate( effector: L2Character, affected: L2Character, skill: Skill, initialValue: number ): number {
        let player: L2PcInstance = effector.getActingPlayer()

        if ( effector.isPlayer() ) {
            let chestItem = player.getChestArmorInstance()
            if ( chestItem ) {

                let set: L2ArmorSet = DataManager.getArmorSetsData().getArmorSet( chestItem.itemId )
                if ( set && set.containAll( effector as L2PcInstance ) ) {
                    switch ( this.getStat() ) {
                        case Stats.STAT_STR:
                            return initialValue + set.str

                        case Stats.STAT_DEX:
                            return initialValue + set.dex

                        case Stats.STAT_INT:
                            return initialValue + set.int

                        case Stats.STAT_MEN:
                            return initialValue + set.men

                        case Stats.STAT_CON:
                            return initialValue + set.con

                        case Stats.STAT_WIT:
                            return initialValue + set.wit
                    }
                }
            }
        }

        return initialValue
    }
}

/**
 * Using getStat() method to return static value to reduce memory footprint
 * vs storing string
 */

class ArmorSetStr extends FunctionArmorSet {
    getStat(): string {
        return Stats.STAT_STR
    }
}

class ArmorSetCon extends FunctionArmorSet {
    getStat(): string {
        return Stats.STAT_CON
    }
}

class ArmorSetDex extends FunctionArmorSet {
    getStat(): string {
        return Stats.STAT_DEX
    }
}

class ArmorSetInt extends FunctionArmorSet {
    getStat(): string {
        return Stats.STAT_WIT
    }
}

class ArmorSetWit extends FunctionArmorSet {
    getStat(): string {
        return Stats.STAT_WIT
    }
}

class ArmorSetMen extends FunctionArmorSet {
    getStat(): string {
        return Stats.STAT_MEN
    }
}

export const ArmorSTR: AbstractFunction = new ArmorSetStr()
export const ArmorCON: AbstractFunction = new ArmorSetCon()
export const ArmorDEX: AbstractFunction = new ArmorSetDex()
export const ArmorINT: AbstractFunction = new ArmorSetInt()
export const ArmorWIT: AbstractFunction = new ArmorSetWit()
export const ArmorMEN: AbstractFunction = new ArmorSetMen()