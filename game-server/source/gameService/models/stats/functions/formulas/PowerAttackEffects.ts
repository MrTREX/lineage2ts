import { Stats } from '../../Stats'
import { BaseStats } from '../../BaseStats'
import { L2PcInstance } from '../../../actor/instance/L2PcInstance'
import { AbstractFunction } from '../AbstractFunction'
import { L2Character } from '../../../actor/L2Character'
import { Skill } from '../../../Skill'
import { InventorySlot } from '../../../../values/InventoryValues'
import { NullTemplateValues } from '../FunctionTemplate'

const inventorySlots : Array<InventorySlot> = [
    InventorySlot.Chest,
    InventorySlot.Legs,
    InventorySlot.Head,
    InventorySlot.Feet,
    InventorySlot.Gloves,
    InventorySlot.Under,
    InventorySlot.Cloak,
]

const getPlayerValue = ( initialValue: number, player: L2PcInstance ): number => {
    let outcome = initialValue
    let isPlayerTransformed = player.isTransformed()

    inventorySlots.some( ( slot: number ) => {
        if ( !player.getInventory().isPaperdollSlotEmpty( slot ) ) {

            let bestSlot
            if ( isPlayerTransformed ) {
                bestSlot = player.getTransformation().getBaseDefenceBySlot( player, slot )
            } else {
                bestSlot = slot
            }

            outcome -= player.getTemplate().getBaseDefBySlot( bestSlot )

            if ( outcome <= 0 ) {
                outcome = 0
                return true
            }
        }

        return false
    } )

    return outcome
}

export class FunctionPowerDefenceModifier extends AbstractFunction {
    constructor() {
        super( NullTemplateValues, null )
    }

    calculate( effector: L2Character, affected: L2Character, skill: Skill, initialValue: number ) {
        let startValue = initialValue
        if ( effector.isPlayer() ) {
            let player: L2PcInstance = effector.getActingPlayer()
            startValue = getPlayerValue( startValue, player )
        }

        return startValue * effector.getLevelModifier()
    }

    getStat(): string {
        return Stats.POWER_DEFENCE
    }
}

export class FunctionPowerAttackModifier extends AbstractFunction {
    constructor() {
        super( NullTemplateValues, null )
    }

    calculate( effector: L2Character, affected: L2Character, skill: Skill, initialValue: number ) {
        return initialValue * BaseStats.STR( effector ) * effector.getLevelModifier()
    }

    getStat(): string {
        return Stats.POWER_ATTACK
    }
}

export class FunctionPowerAttackSpeed extends AbstractFunction {

    constructor() {
        super( NullTemplateValues, null )
    }

    calculate( effector: L2Character, affected: L2Character, skill: Skill, initialValue: number ) {
        return initialValue * BaseStats.DEX( effector )
    }

    getStat(): string {
        return Stats.POWER_ATTACK_SPEED
    }
}

export const PowerDefenceModifier: AbstractFunction = new FunctionPowerDefenceModifier()
export const PowerAttackModifier: AbstractFunction = new FunctionPowerAttackModifier()
export const PowerAttackSpeed: AbstractFunction = new FunctionPowerAttackSpeed()