import { Stats } from '../../Stats'
import { BaseStats } from '../../BaseStats'
import { AbstractFunction } from '../AbstractFunction'
import { L2Character } from '../../../actor/L2Character'
import { Skill } from '../../../Skill'
import { NullTemplateValues } from '../FunctionTemplate'

export class FunctionAttackCritical extends AbstractFunction {
    constructor() {
        super( NullTemplateValues, null )
    }

    calculate( effector: L2Character, affected: L2Character, skill: Skill, initialValue: number ) {
        return initialValue * BaseStats.DEX( effector ) * 10
    }

    getStat(): string {
        return Stats.CRITICAL_RATE
    }
}

export class FunctionAttackAccuracy extends AbstractFunction {
    constructor() {
        super( NullTemplateValues, null )
    }

    calculate( effector: L2Character, affected: L2Character, skill: Skill, initialValue: number ) {
        let level = effector.getLevel()
        let outcome = initialValue + level + Math.sqrt( effector.getDEX() ) * 6

        if ( level > 77 ) {
            outcome = outcome + level - 76
        }

        if ( level > 69 ) {
            outcome = outcome + level - 69
        }

        return outcome
    }

    getStat(): string {
        return Stats.ACCURACY_COMBAT
    }
}

export class FunctionAttackEvasion extends AbstractFunction {
    constructor() {
        super( NullTemplateValues, null )
    }

    calculate( effector: L2Character, affected: L2Character, skill: Skill, initialValue: number ) {
        let level = effector.getLevel()
        let outcome = initialValue + level + Math.sqrt( effector.getDEX() ) * 6

        if ( effector.isPlayer() ) {
            let difference = level - 69
            if ( level >= 78 ) {
                difference = difference * 1.2
            }
            if ( level >= 70 ) {
                outcome = outcome + difference
            }

            return outcome
        }

        if ( level > 69 ) {
            outcome = outcome + level - 67
        }

        return outcome
    }

    getStat(): string {
        return Stats.EVASION_RATE
    }
}

export const AttackCritical: AbstractFunction = new FunctionAttackCritical()
export const AttackAccuracy: AbstractFunction = new FunctionAttackAccuracy()
export const AttackEvasion: AbstractFunction = new FunctionAttackEvasion()