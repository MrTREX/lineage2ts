import { Stats } from '../../Stats'
import { AbstractFunction } from '../AbstractFunction'
import { L2Character } from '../../../actor/L2Character'
import { L2PcInstance } from '../../../actor/instance/L2PcInstance'
import { Skill } from '../../../Skill'
import { NullTemplateValues } from '../FunctionTemplate'

class FunctionHenna extends AbstractFunction {

    constructor() {
        super( NullTemplateValues, null )
    }

    calculate( effector: L2Character, affected: L2Character, skill: Skill, initialValue: number ) {
        let player: L2PcInstance = effector.getActingPlayer()
        if ( player ) {
            return initialValue + player.getHennaStat( this.getStat() )
        }

        return initialValue
    }
}

class HennaStr extends FunctionHenna {
    getStat(): string {
        return Stats.STAT_STR
    }
}

class HennaCon extends FunctionHenna {
    getStat(): string {
        return Stats.STAT_CON
    }
}

class HennaDex extends FunctionHenna {
    getStat(): string {
        return Stats.STAT_DEX
    }
}

class HennaInt extends FunctionHenna {
    getStat(): string {
        return Stats.STAT_INT
    }
}

class HennaWit extends FunctionHenna {
    getStat(): string {
        return Stats.STAT_WIT
    }
}

class HennaMen extends FunctionHenna {
    getStat(): string {
        return Stats.STAT_MEN
    }
}

export const HennaSTR: AbstractFunction = new HennaStr()
export const HennaCON: AbstractFunction = new HennaCon()
export const HennaDEX: AbstractFunction = new HennaDex()
export const HennaINT: AbstractFunction = new HennaInt()
export const HennaWIT: AbstractFunction = new HennaWit()
export const HennaMEN: AbstractFunction = new HennaMen()

