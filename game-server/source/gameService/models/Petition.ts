import { PetitionType } from '../enums/PetitionType'
import { PetitionState } from '../enums/PetitionState'
import { L2PcInstance } from './actor/instance/L2PcInstance'
import { IDFactoryCache } from '../cache/IDFactoryCache'
import { L2World } from '../L2World'
import { SystemMessageBuilder } from '../packets/send/SystemMessage'
import { SystemMessageIds } from '../packets/SystemMessageIdValues'
import { PetitionManager } from '../instancemanager/PetitionManager'
import { PetitionVotePacket } from '../packets/send/PetitionVotePacket'

export class Petition {
    submitTime: number = Date.now()
    id: number
    type: PetitionType
    state: PetitionState
    content: string
    messageLog: Array<Buffer> = []
    petitioner: number
    responder: number

    constructor( player: L2PcInstance, message: string, type: number ) {
        this.id = IDFactoryCache.getNextId()
        this.type = type - 1
        this.content = message
        this.petitioner = player.getObjectId()
    }

    getPetitioner() {
        return this.petitioner
    }

    getLogMessages() {
        return this.messageLog
    }

    getId() {
        return this.id
    }

    getResponder() {
        return this.responder
    }

    endPetitionConsultation( state: PetitionState ) {
        this.state = state

        let responder = L2World.getPlayer( this.responder )
        let petitioner = L2World.getPlayer( this.petitioner )
        if ( responder && responder.isOnline() ) {
            if ( state === PetitionState.RESPONDER_REJECT && petitioner ) {
                petitioner.sendMessage( 'Your petition was rejected. Please try again later.' )
            } else {
                let endStatus = new SystemMessageBuilder( SystemMessageIds.PETITION_ENDED_WITH_C1 )
                        .addString( petitioner.getName() )
                        .getBuffer()
                responder.sendOwnedData( endStatus )

                if ( state === PetitionState.PETITIONER_CANCEL ) {

                    let cancelStatus = new SystemMessageBuilder( SystemMessageIds.RECENT_NO_S1_CANCELED )
                            .addNumber( this.getId() )
                            .getBuffer()
                    responder.sendOwnedData( cancelStatus )
                }
            }
        }

        /*
            - End petition consultation and inform them, if they are still online.
            - If petitioner is online, enable Evaluation button
         */
        if ( petitioner && petitioner.isOnline() ) {
            petitioner.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THIS_END_THE_PETITION_PLEASE_PROVIDE_FEEDBACK ) )
            petitioner.sendOwnedData( PetitionVotePacket() )
        }

        PetitionManager.markPetitionCompleted( this.getId() )
    }

    getState() {
        return this.state
    }

    addLogMessage( packet: Buffer ) {
        this.messageLog.push( packet )
    }

    sendResponderPacket( packet: Buffer ) {
        let responder : L2PcInstance = L2World.getPlayer( this.responder )
        if ( !responder || !responder.isOnline() ) {
            this.endPetitionConsultation( PetitionState.PETITIONER_MISSING )
            return
        }

        responder.sendCopyData( packet )
    }

    sendPetitionerPacket( packet: Buffer ) {
        let petitioner : L2PcInstance = L2World.getPlayer( this.petitioner )
        if ( !petitioner || !petitioner.isOnline() ) {
            return
        }

        petitioner.sendCopyData( packet )
    }
}