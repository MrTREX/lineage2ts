import { Race } from '../../enums/Race'
import _ from 'lodash'

export type ClassIdData = [ number, boolean, number, string, boolean? ]

export interface IClassId {
    /** The Identifier of the Class */
    id: number

    /** True if the class is a mage class */
    isMage: boolean

    /** True if the class is a summoner class */
    isSummoner: boolean

    /** The Race object of the class */
    race: Race

    /** The parent ClassId or null if this class is a root */
    parent: string

    level: number
}

const ClassIdHelper = {
    getClassIdByValue: ( values: ClassIdData ) : IClassId => {
        let [ id, isMage, race, parent, isSummoner = false ] = values

        /*
            When we compute these values first time level cannot be determined,
            hence it is set to zero at fist, later it can be filled via separate operation
         */

        return {
            id,
            isMage,
            isSummoner,
            race,
            parent,
            level: 0
        }
    },

    getLevel: ( currentClass: IClassId ) : number => {
        if ( !currentClass.parent ) {
            return 0
        }

        return ClassIdHelper.getLevel( ClassIdValues[ currentClass.parent ] ) + 1
    },
}

export const ClassId = {
    getClassId: ( name: string ) : IClassId => {
        return ClassIdValues[ name ]
    },

    getClassIdByIdentifier: ( id: number ) : IClassId => {
        let name = IdToNameMap[ id ]
        return ClassIdValues[ name ]
    },

    isChildOf: ( currentClass: IClassId, parent: string ) : boolean => {

        if ( !currentClass || !currentClass.parent ) {
            return false
        }

        if ( currentClass.parent === parent ) {
            return true
        }

        let parentClass : IClassId = ClassIdValues[ currentClass.parent ]
        return ClassId.isChildOf( parentClass, parent )
    },

    getName( currentClass: IClassId ) : string {
        return IdToNameMap[ currentClass.id ]
    },

    getNameById( id: number ) : string {
        return IdToNameMap[ id ]
    },

    getLevelByClassId( id: number ) : number {
        let classId : IClassId = ClassId.getClassIdByIdentifier( id )
        return classId ? classId.level : 0
    },

    getClientCode( id: number ) : string {
        return `&$${ClassId.getClassClientId( id )};`
    },

    getClassClientId( id: number ) : number {
        if ( ( id >= 0 ) && ( id <= 57 ) ) {
            return id + 247
        }

        if ( ( id >= 88 ) && ( id <= 118 ) ) {
            return id + 1071
        }

        if ( ( id >= 123 ) && ( id <= 136 ) ) {
            return id + 1438
        }

        return id
    }
}

export const ClassIdValues : { [ name: string ] : IClassId } = {
    fighter: ClassIdHelper.getClassIdByValue( [ 0x00, false, Race.HUMAN, null ] ),

    warrior: ClassIdHelper.getClassIdByValue( [ 0x01, false, Race.HUMAN, 'fighter' ] ),
    gladiator: ClassIdHelper.getClassIdByValue( [ 0x02, false, Race.HUMAN, 'warrior' ] ),
    warlord: ClassIdHelper.getClassIdByValue( [ 0x03, false, Race.HUMAN, 'warrior' ] ),
    knight: ClassIdHelper.getClassIdByValue( [ 0x04, false, Race.HUMAN, 'fighter' ] ),
    paladin: ClassIdHelper.getClassIdByValue( [ 0x05, false, Race.HUMAN, 'knight' ] ),
    darkAvenger: ClassIdHelper.getClassIdByValue( [ 0x06, false, Race.HUMAN, 'knight' ] ),
    rogue: ClassIdHelper.getClassIdByValue( [ 0x07, false, Race.HUMAN, 'fighter' ] ),
    treasureHunter: ClassIdHelper.getClassIdByValue( [ 0x08, false, Race.HUMAN, 'rogue' ] ),
    hawkeye: ClassIdHelper.getClassIdByValue( [ 0x09, false, Race.HUMAN, 'rogue' ] ),

    mage: ClassIdHelper.getClassIdByValue( [ 0x0a, true, Race.HUMAN, null ] ),
    wizard: ClassIdHelper.getClassIdByValue( [ 0x0b, true, Race.HUMAN, 'mage' ] ),
    sorceror: ClassIdHelper.getClassIdByValue( [ 0x0c, true, Race.HUMAN, 'wizard' ] ),
    necromancer: ClassIdHelper.getClassIdByValue( [ 0x0d, true, Race.HUMAN, 'wizard' ] ),
    warlock: ClassIdHelper.getClassIdByValue( [ 0x0e, true, Race.HUMAN, 'wizard', true ] ),
    cleric: ClassIdHelper.getClassIdByValue( [ 0x0f, true, Race.HUMAN, 'mage' ] ),
    bishop: ClassIdHelper.getClassIdByValue( [ 0x10, true, Race.HUMAN, 'cleric' ] ),
    prophet: ClassIdHelper.getClassIdByValue( [ 0x11, true, Race.HUMAN, 'cleric' ] ),

    elvenFighter: ClassIdHelper.getClassIdByValue( [ 0x12, false, Race.ELF, null ] ),
    elvenKnight: ClassIdHelper.getClassIdByValue( [ 0x13, false, Race.ELF, 'elvenFighter' ] ),
    templeKnight: ClassIdHelper.getClassIdByValue( [ 0x14, false, Race.ELF, 'elvenKnight' ] ),
    swordSinger: ClassIdHelper.getClassIdByValue( [ 0x15, false, Race.ELF, 'elvenKnight' ] ),
    elvenScout: ClassIdHelper.getClassIdByValue( [ 0x16, false, Race.ELF, 'elvenFighter' ] ),
    plainsWalker: ClassIdHelper.getClassIdByValue( [ 0x17, false, Race.ELF, 'elvenScout' ] ),
    silverRanger: ClassIdHelper.getClassIdByValue( [ 0x18, false, Race.ELF, 'elvenScout' ] ),

    elvenMage: ClassIdHelper.getClassIdByValue( [ 0x19, true, Race.ELF, null ] ),
    elvenWizard: ClassIdHelper.getClassIdByValue( [ 0x1a, true, Race.ELF, 'elvenMage' ] ),
    spellsinger: ClassIdHelper.getClassIdByValue( [ 0x1b, true, Race.ELF, 'elvenWizard' ] ),
    elementalSummoner: ClassIdHelper.getClassIdByValue( [ 0x1c, true, Race.ELF, 'elvenWizard', true ] ),
    oracle: ClassIdHelper.getClassIdByValue( [ 0x1d, true, Race.ELF, 'elvenMage' ] ),
    elder: ClassIdHelper.getClassIdByValue( [ 0x1e, true, Race.ELF, 'oracle' ] ),

    darkFighter: ClassIdHelper.getClassIdByValue( [ 0x1f, false, Race.DARK_ELF, null ] ),
    palusKnight: ClassIdHelper.getClassIdByValue( [ 0x20, false, Race.DARK_ELF, 'darkFighter' ] ),
    shillienKnight: ClassIdHelper.getClassIdByValue( [ 0x21, false, Race.DARK_ELF, 'palusKnight' ] ),
    bladedancer: ClassIdHelper.getClassIdByValue( [ 0x22, false, Race.DARK_ELF, 'palusKnight' ] ),
    assassin: ClassIdHelper.getClassIdByValue( [ 0x23, false, Race.DARK_ELF, 'darkFighter' ] ),
    abyssWalker: ClassIdHelper.getClassIdByValue( [ 0x24, false, Race.DARK_ELF, 'assassin' ] ),
    phantomRanger: ClassIdHelper.getClassIdByValue( [ 0x25, false, Race.DARK_ELF, 'assassin' ] ),

    darkMage: ClassIdHelper.getClassIdByValue( [ 0x26, true, Race.DARK_ELF, null ] ),
    darkWizard: ClassIdHelper.getClassIdByValue( [ 0x27, true, Race.DARK_ELF, 'darkMage' ] ),
    spellhowler: ClassIdHelper.getClassIdByValue( [ 0x28, true, Race.DARK_ELF, 'darkWizard' ] ),
    phantomSummoner: ClassIdHelper.getClassIdByValue( [ 0x29, true, Race.DARK_ELF, 'darkWizard', true ] ),
    shillienOracle: ClassIdHelper.getClassIdByValue( [ 0x2a, true, Race.DARK_ELF, 'darkMage' ] ),
    shillenElder: ClassIdHelper.getClassIdByValue( [ 0x2b, true, Race.DARK_ELF, 'shillienOracle' ] ),

    orcFighter: ClassIdHelper.getClassIdByValue( [ 0x2c, false, Race.ORC, null ] ),
    orcRaider: ClassIdHelper.getClassIdByValue( [ 0x2d, false, Race.ORC, 'orcFighter' ] ),
    destroyer: ClassIdHelper.getClassIdByValue( [ 0x2e, false, Race.ORC, 'orcRaider' ] ),
    orcMonk: ClassIdHelper.getClassIdByValue( [ 0x2f, false, Race.ORC, 'orcFighter' ] ),
    tyrant: ClassIdHelper.getClassIdByValue( [ 0x30, false, Race.ORC, 'orcMonk' ] ),

    orcMage: ClassIdHelper.getClassIdByValue( [ 0x31, true, Race.ORC, null ] ),
    orcShaman: ClassIdHelper.getClassIdByValue( [ 0x32, true, Race.ORC, 'orcMage' ] ),
    overlord: ClassIdHelper.getClassIdByValue( [ 0x33, true, Race.ORC, 'orcShaman' ] ),
    warcryer: ClassIdHelper.getClassIdByValue( [ 0x34, true, Race.ORC, 'orcShaman' ] ),

    dwarvenFighter: ClassIdHelper.getClassIdByValue( [ 0x35, false, Race.DWARF, null ] ),
    scavenger: ClassIdHelper.getClassIdByValue( [ 0x36, false, Race.DWARF, 'dwarvenFighter' ] ),
    bountyHunter: ClassIdHelper.getClassIdByValue( [ 0x37, false, Race.DWARF, 'scavenger' ] ),
    artisan: ClassIdHelper.getClassIdByValue( [ 0x38, false, Race.DWARF, 'dwarvenFighter' ] ),
    warsmith: ClassIdHelper.getClassIdByValue( [ 0x39, false, Race.DWARF, 'artisan' ] ),

    dummyEntry1: ClassIdHelper.getClassIdByValue( [ 58, false, null, null ] ),
    dummyEntry2: ClassIdHelper.getClassIdByValue( [ 59, false, null, null ] ),
    dummyEntry3: ClassIdHelper.getClassIdByValue( [ 60, false, null, null ] ),
    dummyEntry4: ClassIdHelper.getClassIdByValue( [ 61, false, null, null ] ),
    dummyEntry5: ClassIdHelper.getClassIdByValue( [ 62, false, null, null ] ),
    dummyEntry6: ClassIdHelper.getClassIdByValue( [ 63, false, null, null ] ),
    dummyEntry7: ClassIdHelper.getClassIdByValue( [ 64, false, null, null ] ),
    dummyEntry8: ClassIdHelper.getClassIdByValue( [ 65, false, null, null ] ),
    dummyEntry9: ClassIdHelper.getClassIdByValue( [ 66, false, null, null ] ),
    dummyEntry10: ClassIdHelper.getClassIdByValue( [ 67, false, null, null ] ),
    dummyEntry11: ClassIdHelper.getClassIdByValue( [ 68, false, null, null ] ),
    dummyEntry12: ClassIdHelper.getClassIdByValue( [ 69, false, null, null ] ),
    dummyEntry13: ClassIdHelper.getClassIdByValue( [ 70, false, null, null ] ),
    dummyEntry14: ClassIdHelper.getClassIdByValue( [ 71, false, null, null ] ),
    dummyEntry15: ClassIdHelper.getClassIdByValue( [ 72, false, null, null ] ),
    dummyEntry16: ClassIdHelper.getClassIdByValue( [ 73, false, null, null ] ),
    dummyEntry17: ClassIdHelper.getClassIdByValue( [ 74, false, null, null ] ),
    dummyEntry18: ClassIdHelper.getClassIdByValue( [ 75, false, null, null ] ),
    dummyEntry19: ClassIdHelper.getClassIdByValue( [ 76, false, null, null ] ),
    dummyEntry20: ClassIdHelper.getClassIdByValue( [ 77, false, null, null ] ),
    dummyEntry21: ClassIdHelper.getClassIdByValue( [ 78, false, null, null ] ),
    dummyEntry22: ClassIdHelper.getClassIdByValue( [ 79, false, null, null ] ),
    dummyEntry23: ClassIdHelper.getClassIdByValue( [ 80, false, null, null ] ),
    dummyEntry24: ClassIdHelper.getClassIdByValue( [ 81, false, null, null ] ),
    dummyEntry25: ClassIdHelper.getClassIdByValue( [ 82, false, null, null ] ),
    dummyEntry26: ClassIdHelper.getClassIdByValue( [ 83, false, null, null ] ),
    dummyEntry27: ClassIdHelper.getClassIdByValue( [ 84, false, null, null ] ),
    dummyEntry28: ClassIdHelper.getClassIdByValue( [ 85, false, null, null ] ),
    dummyEntry29: ClassIdHelper.getClassIdByValue( [ 86, false, null, null ] ),
    dummyEntry30: ClassIdHelper.getClassIdByValue( [ 87, false, null, null ] ),

    duelist: ClassIdHelper.getClassIdByValue( [ 0x58, false, Race.HUMAN, 'gladiator' ] ),
    dreadnought: ClassIdHelper.getClassIdByValue( [ 0x59, false, Race.HUMAN, 'warlord' ] ),
    phoenixKnight: ClassIdHelper.getClassIdByValue( [ 0x5a, false, Race.HUMAN, 'paladin' ] ),
    hellKnight: ClassIdHelper.getClassIdByValue( [ 0x5b, false, Race.HUMAN, 'darkAvenger' ] ),
    sagittarius: ClassIdHelper.getClassIdByValue( [ 0x5c, false, Race.HUMAN, 'hawkeye' ] ),
    adventurer: ClassIdHelper.getClassIdByValue( [ 0x5d, false, Race.HUMAN, 'treasureHunter' ] ),
    archmage: ClassIdHelper.getClassIdByValue( [ 0x5e, true, Race.HUMAN, 'sorceror' ] ),
    soultaker: ClassIdHelper.getClassIdByValue( [ 0x5f, true, Race.HUMAN, 'necromancer' ] ),
    arcanaLord: ClassIdHelper.getClassIdByValue( [ 0x60, true, Race.HUMAN, 'warlock', true ] ),
    cardinal: ClassIdHelper.getClassIdByValue( [ 0x61, true, Race.HUMAN, 'bishop' ] ),
    hierophant: ClassIdHelper.getClassIdByValue( [ 0x62, true, Race.HUMAN, 'prophet' ] ),

    evaTemplar: ClassIdHelper.getClassIdByValue( [ 0x63, false, Race.ELF, 'templeKnight' ] ),
    swordMuse: ClassIdHelper.getClassIdByValue( [ 0x64, false, Race.ELF, 'swordSinger' ] ),
    windRider: ClassIdHelper.getClassIdByValue( [ 0x65, false, Race.ELF, 'plainsWalker' ] ),
    moonlightSentinel: ClassIdHelper.getClassIdByValue( [ 0x66, false, Race.ELF, 'silverRanger' ] ),
    mysticMuse: ClassIdHelper.getClassIdByValue( [ 0x67, true, Race.ELF, 'spellsinger' ] ),
    elementalMaster: ClassIdHelper.getClassIdByValue( [ 0x68, true, Race.ELF, 'elementalSummoner', true ] ),
    evaSaint: ClassIdHelper.getClassIdByValue( [ 0x69, true, Race.ELF, 'elder' ] ),

    shillienTemplar: ClassIdHelper.getClassIdByValue( [ 0x6a, false, Race.DARK_ELF, 'shillienKnight' ] ),
    spectralDancer: ClassIdHelper.getClassIdByValue( [ 0x6b, false, Race.DARK_ELF, 'bladedancer' ] ),
    ghostHunter: ClassIdHelper.getClassIdByValue( [ 0x6c, false, Race.DARK_ELF, 'abyssWalker' ] ),
    ghostSentinel: ClassIdHelper.getClassIdByValue( [ 0x6d, false, Race.DARK_ELF, 'phantomRanger' ] ),
    stormScreamer: ClassIdHelper.getClassIdByValue( [ 0x6e, true, Race.DARK_ELF, 'spellhowler' ] ),
    spectralMaster: ClassIdHelper.getClassIdByValue( [ 0x6f, true, Race.DARK_ELF, 'phantomSummoner', true ] ),
    shillienSaint: ClassIdHelper.getClassIdByValue( [ 0x70, true, Race.DARK_ELF, 'shillenElder' ] ),

    titan: ClassIdHelper.getClassIdByValue( [ 0x71, false, Race.ORC, 'destroyer' ] ),
    grandKhavatari: ClassIdHelper.getClassIdByValue( [ 0x72, false, Race.ORC, 'tyrant' ] ),
    dominator: ClassIdHelper.getClassIdByValue( [ 0x73, true, Race.ORC, 'overlord' ] ),
    doomcryer: ClassIdHelper.getClassIdByValue( [ 0x74, true, Race.ORC, 'warcryer' ] ),

    fortuneSeeker: ClassIdHelper.getClassIdByValue( [ 0x75, false, Race.DWARF, 'bountyHunter' ] ),
    maestro: ClassIdHelper.getClassIdByValue( [ 0x76, false, Race.DWARF, 'warsmith' ] ),

    dummyEntry31: ClassIdHelper.getClassIdByValue( [ 0x77, false, null, null ] ),
    dummyEntry32: ClassIdHelper.getClassIdByValue( [ 0x78, false, null, null ] ),
    dummyEntry33: ClassIdHelper.getClassIdByValue( [ 0x79, false, null, null ] ),
    dummyEntry34: ClassIdHelper.getClassIdByValue( [ 0x7a, false, null, null ] ),

    maleSoldier: ClassIdHelper.getClassIdByValue( [ 0x7b, false, Race.KAMAEL, null ] ),
    femaleSoldier: ClassIdHelper.getClassIdByValue( [ 0x7C, false, Race.KAMAEL, null ] ),
    trooper: ClassIdHelper.getClassIdByValue( [ 0x7D, false, Race.KAMAEL, 'maleSoldier' ] ),
    warder: ClassIdHelper.getClassIdByValue( [ 0x7E, false, Race.KAMAEL, 'femaleSoldier' ] ),
    berserker: ClassIdHelper.getClassIdByValue( [ 0x7F, false, Race.KAMAEL, 'trooper' ] ),
    maleSoulbreaker: ClassIdHelper.getClassIdByValue( [ 0x80, false, Race.KAMAEL, 'trooper' ] ),
    femaleSoulbreaker: ClassIdHelper.getClassIdByValue( [ 0x81, false, Race.KAMAEL, 'warder' ] ),
    arbalester: ClassIdHelper.getClassIdByValue( [ 0x82, false, Race.KAMAEL, 'warder' ] ),
    doombringer: ClassIdHelper.getClassIdByValue( [ 0x83, false, Race.KAMAEL, 'berserker' ] ),
    maleSoulhound: ClassIdHelper.getClassIdByValue( [ 0x84, false, Race.KAMAEL, 'maleSoulbreaker' ] ),
    femaleSoulhound: ClassIdHelper.getClassIdByValue( [ 0x85, false, Race.KAMAEL, 'femaleSoulbreaker' ] ),
    trickster: ClassIdHelper.getClassIdByValue( [ 0x86, false, Race.KAMAEL, 'arbalester' ] ),
    inspector: ClassIdHelper.getClassIdByValue( [ 0x87, false, Race.KAMAEL, 'warder' ] ), // DS: yes, both male/female inspectors use skills from warder
    judicator: ClassIdHelper.getClassIdByValue( [ 0x88, false, Race.KAMAEL, 'inspector' ] )
}

const IdToNameMap : { [ classId: number ] : string } = _.reduce( ClassIdValues, ( finalMap: { [ classId: number ] : string }, value: IClassId, key: string ) => {

    finalMap[ value.id ] = key

    return finalMap
}, {} )

_.each( ClassIdValues, ( value: IClassId ) => {
    value.level = ClassIdHelper.getLevel( value )
} )