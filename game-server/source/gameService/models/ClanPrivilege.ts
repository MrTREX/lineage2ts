import { ClanPrivilege } from '../enums/ClanPriviledge'

const fullMask: number = ClanPrivilege.InviteToClan |
        ClanPrivilege.GrantTitle |
        ClanPrivilege.WarehouseAccess |
        ClanPrivilege.ManageRanks |
        ClanPrivilege.PledgeWar |
        ClanPrivilege.DismissMember |
        ClanPrivilege.RegisterCrest |
        ClanPrivilege.InviteApprentice |
        ClanPrivilege.NameTroops |
        ClanPrivilege.SummonAirship |
        ClanPrivilege.ClanHallMiscellaneous |
        ClanPrivilege.ClanHallAuction |
        ClanPrivilege.ClanHallDismiss |
        ClanPrivilege.ClanHallSetFunctions |
        ClanPrivilege.CastleOpenDoors |
        ClanPrivilege.CastleManor |
        ClanPrivilege.CastleSiege |
        ClanPrivilege.CastleUseFunctions |
        ClanPrivilege.CastleDismiss |
        ClanPrivilege.CastleTaxes |
        ClanPrivilege.CastleMercenaries |
        ClanPrivilege.CastleSetFunctions

export const ClanPriviledgeHelper = {
    getFullPrivileges(): number {
        return fullMask
    },

    includes( value: number, ...privileges: Array<ClanPrivilege> ) {
        return privileges.every( ( privilege: ClanPrivilege ): boolean => {
            return ( value & privilege ) !== 0
        } )
    },

    add( value: number, ...privileges: Array<ClanPrivilege> ) {
        return privileges.reduce( ( total: ClanPrivilege, currentPrivilege: ClanPrivilege ): ClanPrivilege => {
            return total & currentPrivilege
        }, value )
    },

    remove( value: number, ...privileges: Array<ClanPrivilege> ) {
        return privileges.reduce( ( total: ClanPrivilege, currentPrivilege: ClanPrivilege ): ClanPrivilege => {
            return total & ~currentPrivilege
        }, value )
    },

    /*
    Consider config option for default clan privileges.
     */
    getDefaultPrivileges() {
        return ClanPrivilege.None
    },
}