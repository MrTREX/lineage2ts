import { L2World } from '../L2World'
import { L2Npc } from './actor/L2Npc'
import { SiegeClanType } from '../enums/SiegeClanType'

// TODO : reduce to interface
export class L2SiegeClan {
    clanId: number
    flags: Array<number> = []
    type: SiegeClanType

    constructor( clanId: number, type: SiegeClanType ) {
        this.clanId = clanId
        this.type = type
    }

    addFlag( objectId : number ) {
        this.flags.push( objectId )
    }

    removeFlags() {
        this.flags.forEach( ( currentObjectId : number ) => {
            let currentFlag : L2Npc = L2World.getObjectById( currentObjectId ) as L2Npc
            return currentFlag && currentFlag.deleteMe()
        } )

        this.flags = []
    }

    getClanId() : number {
        return this.clanId
    }

    getType() : SiegeClanType {
        return this.type
    }

    setType( type: SiegeClanType ) : void {
        this.type = type
    }

    getFlag() : Array<number> {
        return this.flags
    }

    getNumFlags() {
        return this.flags.length
    }
}