import { L2PcInstance } from './actor/instance/L2PcInstance'
import { FunctionAdd } from './stats/functions/operations/FunctionAdd'
import { Stats } from './stats/Stats'
import { FunctionTemplateValues } from './stats/functions/FunctionTemplate'
import _ from 'lodash'
import { ElementalType } from '../enums/ElementalType'
import { AbstractFunction } from './stats/functions/AbstractFunction'

const WEAPON_VALUES = [
    0, // Level 1
    25, // Level 2
    75, // Level 3
    150, // Level 4
    175, // Level 5
    225, // Level 6
    300, // Level 7
    325, // Level 8
    375, // Level 9
    450, // Level 10
    475, // Level 11
    525, // Level 12
    600, // Level 13
]

const ARMOR_VALUES = [
    0, // Level 1
    12, // Level 2
    30, // Level 3
    60, // Level 4
    72, // Level 5
    90, // Level 6
    120, // Level 7
    132, // Level 8
    150, // Level 9
    180, // Level 10
    192, // Level 11
    210, // Level 12
    240, // Level 13
]

export const ElementalValues = {
    FIRST_WEAPON_BONUS: 20,
    NEXT_WEAPON_BONUS: 5,
    ARMOR_BONUS: 6,
    WEAPON_VALUES,
    ARMOR_VALUES,
}

export const ElementalItemType = {
    Stone: 3,
    Roughore: 3,
    Crystal: 6,
    Jewel: 9,
    Energy: 12,
}

export interface ElementalItem {
    element: number
    itemId: number
    maxLevel: number
}

export const ElementalsHelper = {
    createElementalItem( data: Array<number> ): ElementalItem {
        let [ element, itemId, maxLevel ] = data

        return {
            element,
            itemId,
            maxLevel,
        }
    },

    getByItemId( itemId: number ): ElementalItem {
        return _.find( ElementalItems,
                ( entry: ElementalItem ) => {
                    return entry.itemId === itemId
                } )
    },

    getElementName( id: number ): string {
        switch ( id ) {
            case ElementalType.FIRE:
                return 'Fire'
            case ElementalType.WATER:
                return 'Water'
            case ElementalType.WIND:
                return 'Wind'
            case ElementalType.EARTH:
                return 'Earth'
            case ElementalType.DARK:
                return 'Dark'
            case ElementalType.HOLY:
                return 'Holy'
        }

        return 'None'
    },
}

export const ElementalItems = {
    fireStone: ElementalsHelper.createElementalItem( [ ElementalType.FIRE, 9546, ElementalItemType.Stone ] ),
    waterStone: ElementalsHelper.createElementalItem( [ ElementalType.WATER, 9547, ElementalItemType.Stone ] ),
    windStone: ElementalsHelper.createElementalItem( [ ElementalType.WIND, 9549, ElementalItemType.Stone ] ),
    earthStone: ElementalsHelper.createElementalItem( [ ElementalType.EARTH, 9548, ElementalItemType.Stone ] ),
    divineStone: ElementalsHelper.createElementalItem( [ ElementalType.HOLY, 9551, ElementalItemType.Stone ] ),
    darkStone: ElementalsHelper.createElementalItem( [ ElementalType.DARK, 9550, ElementalItemType.Stone ] ),

    fireRoughtore: ElementalsHelper.createElementalItem( [ ElementalType.FIRE, 10521, ElementalItemType.Roughore ] ),
    waterRoughtore: ElementalsHelper.createElementalItem( [ ElementalType.WATER, 10522, ElementalItemType.Roughore ] ),
    windRoughtore: ElementalsHelper.createElementalItem( [ ElementalType.WIND, 10524, ElementalItemType.Roughore ] ),
    earthRoughtore: ElementalsHelper.createElementalItem( [ ElementalType.EARTH, 10523, ElementalItemType.Roughore ] ),
    divineRoughtore: ElementalsHelper.createElementalItem( [ ElementalType.HOLY, 10526, ElementalItemType.Roughore ] ),
    darkRoughtore: ElementalsHelper.createElementalItem( [ ElementalType.DARK, 10525, ElementalItemType.Roughore ] ),

    fireCrystal: ElementalsHelper.createElementalItem( [ ElementalType.FIRE, 9552, ElementalItemType.Crystal ] ),
    waterCrystal: ElementalsHelper.createElementalItem( [ ElementalType.WATER, 9553, ElementalItemType.Crystal ] ),
    windCrystal: ElementalsHelper.createElementalItem( [ ElementalType.WIND, 9555, ElementalItemType.Crystal ] ),
    earthCrystal: ElementalsHelper.createElementalItem( [ ElementalType.EARTH, 9554, ElementalItemType.Crystal ] ),
    divineCrystal: ElementalsHelper.createElementalItem( [ ElementalType.HOLY, 9557, ElementalItemType.Crystal ] ),
    darkCrystal: ElementalsHelper.createElementalItem( [ ElementalType.DARK, 9556, ElementalItemType.Crystal ] ),

    fireJewel: ElementalsHelper.createElementalItem( [ ElementalType.FIRE, 9558, ElementalItemType.Jewel ] ),
    waterJewel: ElementalsHelper.createElementalItem( [ ElementalType.WATER, 9559, ElementalItemType.Jewel ] ),
    windJewel: ElementalsHelper.createElementalItem( [ ElementalType.WIND, 9561, ElementalItemType.Jewel ] ),
    earthJewel: ElementalsHelper.createElementalItem( [ ElementalType.EARTH, 9560, ElementalItemType.Jewel ] ),
    divineJewel: ElementalsHelper.createElementalItem( [ ElementalType.HOLY, 9563, ElementalItemType.Jewel ] ),
    darkJewel: ElementalsHelper.createElementalItem( [ ElementalType.DARK, 9562, ElementalItemType.Jewel ] ),

    // not yet supported by client : ElementalsHelper.createElementalItem([ Freya pts ])
    fireEnergy: ElementalsHelper.createElementalItem( [ ElementalType.FIRE, 9564, ElementalItemType.Energy ] ),
    waterEnergy: ElementalsHelper.createElementalItem( [ ElementalType.WATER, 9565, ElementalItemType.Energy ] ),
    windEnergy: ElementalsHelper.createElementalItem( [ ElementalType.WIND, 9567, ElementalItemType.Energy ] ),
    earthEnergy: ElementalsHelper.createElementalItem( [ ElementalType.EARTH, 9566, ElementalItemType.Energy ] ),
    divineEnergy: ElementalsHelper.createElementalItem( [ ElementalType.HOLY, 9569, ElementalItemType.Energy ] ),
    darkEnergy: ElementalsHelper.createElementalItem( [ ElementalType.DARK, 9568, ElementalItemType.Energy ] ),
}

export class Elementals {
    private element: ElementalType
    private value: number
    private isActive: boolean
    readonly statFunction: AbstractFunction

    constructor( type: ElementalType, value: number, isArmor: boolean ) {
        this.element = type
        this.value = value
        this.isActive = false
        this.statFunction = this.generateFunction( isArmor )
    }

    setValue( value: number ) {
        this.value = value
    }

    getElement(): ElementalType {
        return this.element
    }

    getValue(): number {
        return this.value
    }

    removeBonus( player: L2PcInstance ) : void {
        if ( !this.isActive ) {
            return
        }

        player.removeStatsOwner( this )

        this.isActive = false
    }

    getStat(): AbstractFunction {
        if ( this.isActive ) {
            return null
        }

        this.isActive = true

        return this.statFunction
    }

    static getOppositeElement( value: number ) {
        return ( ( value % 2 ) === 0 ) ? ( value + 1 ) : ( value - 1 )
    }

    static getItemElement( itemId: number ): number {
        let value: ElementalItem = ElementalsHelper.getByItemId( itemId )
        if ( value ) {
            return value.element
        }

        return ElementalType.NONE
    }

    static getMaxElementLevel( itemId: number ): number {
        let item: ElementalItem = ElementalsHelper.getByItemId( itemId )

        if ( item ) {
            return item.maxLevel
        }

        return -1
    }

    protected generateFunction( isArmor: boolean ) : AbstractFunction {
        let values: FunctionTemplateValues = {
            order: 0x40,
            stat: null,
            value: this.value,
        }

        switch ( this.element ) {
            case ElementalType.FIRE:
                values.stat = isArmor ? Stats.FIRE_RES : Stats.FIRE_POWER
                break

            case ElementalType.WATER:
                values.stat = isArmor ? Stats.WATER_RES : Stats.WATER_POWER
                break

            case ElementalType.WIND:
                values.stat = isArmor ? Stats.WIND_RES : Stats.WIND_POWER
                break

            case ElementalType.EARTH:
                values.stat = isArmor ? Stats.EARTH_RES : Stats.EARTH_POWER
                break

            case ElementalType.DARK:
                values.stat = isArmor ? Stats.DARK_RES : Stats.DARK_POWER
                break

            case ElementalType.HOLY:
                values.stat = isArmor ? Stats.HOLY_RES : Stats.HOLY_POWER
                break
        }

        if ( !values.stat ) {
            return null
        }

        return new FunctionAdd( values, this )
    }
}