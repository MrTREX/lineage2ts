import { L2NpcRouteItem, L2NpcRouteType } from '../../data/interface/NpcRoutesDataApi'

export interface NpcWalkParameters {
    route: L2NpcRouteItem
    index: number
    forward: boolean
    routeType: L2NpcRouteType
}