import { L2Item } from './items/L2Item'
import { L2ItemInstance } from './items/instance/L2ItemInstance'
import _ from 'lodash'
import { ObjectPool } from '../helpers/ObjectPoolHelper'
import { DefaultEnchantOptions, EnchantOptionsParameters } from './options/EnchantOptions'
import { DefenceElementTypes, ElementalType } from '../enums/ElementalType'
import { L2ItemDefaults } from '../enums/L2ItemDefaults'

export const TradeItemObjectPool = new ObjectPool( 'TradeItem', () : TradeItem => {
    return {
        count: 0,
        elementaryAttackPower: 0,
        elementaryAttackType: 0,
        elementalDefenceAttributes: [
            0,
            0,
            0,
            0,
            0,
            0
        ],
        enchant: 0,
        enchantOptions: undefined,
        itemTemplate: undefined,
        location: 0,
        objectId: 0,
        price: 0,
        storeCount: 0,
        type1: 0,
        type2: 0
    }
} )

export interface TradeItem {
    objectId: number
    itemTemplate: L2Item
    location: number
    enchant: number
    type1: number
    type2: number
    count: number
    storeCount: number
    price: number
    elementaryAttackType: number
    elementaryAttackPower: number
    elementalDefenceAttributes: ReadonlyArray<number>
    enchantOptions: Readonly<EnchantOptionsParameters>
}

export const TradeItemHelper = {
    recycle( item : TradeItem ) : void {
        item.elementalDefenceAttributes = null
        TradeItemObjectPool.recycleValue( item )
    },

    fromItemInstance( item: L2ItemInstance, count: number, price: number ) : TradeItem {
        let tradeItem = TradeItemObjectPool.getValue()

        tradeItem.objectId = item.getObjectId()
        tradeItem.itemTemplate = item.getItem()
        tradeItem.location = item.getLocationSlot()
        tradeItem.enchant = item.getEnchantLevel()

        tradeItem.type1 = item.getCustomType1()
        tradeItem.type2 = item.getCustomType2()
        tradeItem.count = count
        tradeItem.price = price

        tradeItem.elementaryAttackType = item.getAttackElementType()
        tradeItem.elementaryAttackPower = item.getAttackElementPower()
        tradeItem.enchantOptions = item.getEnchantOptions()
        tradeItem.elementalDefenceAttributes = structuredClone( item.elementalDefenceAttributes )

        return tradeItem
    },

    fromItem( item: L2Item, count: number, price: number ) : TradeItem {
        let tradeItem = TradeItemObjectPool.getValue()

        tradeItem.objectId = 0
        tradeItem.itemTemplate = item
        tradeItem.location = 0
        tradeItem.enchant = 0

        tradeItem.type1 = 0
        tradeItem.type2 = 0
        tradeItem.count = count
        tradeItem.storeCount = count

        tradeItem.price = price
        tradeItem.elementaryAttackType = ElementalType.NONE
        tradeItem.elementaryAttackPower = 0
        tradeItem.enchantOptions = DefaultEnchantOptions

        return tradeItem
    },

    fromTradeItem( item: TradeItem ) : TradeItem {
        let tradeItem = TradeItemObjectPool.getValue()
        _.assign( tradeItem, item )

        return tradeItem
    }
}