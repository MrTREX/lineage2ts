import { L2PcInstance } from './actor/instance/L2PcInstance'
import { Inventory } from './itemcontainer/Inventory'
import { L2ItemInstance } from './items/instance/L2ItemInstance'
import { InventorySlot } from '../values/InventoryValues'
import { Skill } from './Skill'

export class L2ArmorSet {
    chestId: number = 0
    legs: Set<number>
    head: Set<number>
    gloves: Set<number>
    feet: Set<number>
    shield: Set<number>
    itemIds: Set<number>

    setSkill: Skill
    shieldSkill: Skill
    enchant6Skill: Skill

    con: number = 0
    dex: number = 0
    str: number = 0
    men: number = 0
    wit: number = 0
    int: number = 0

    readonly name: string
    readonly id: number

    constructor( id: number, name: string ) {
        this.id = id
        this.name = name
    }

    containAll( player: L2PcInstance ): boolean {
        let inventory = player.getInventory()
        let legsItem = inventory.getPaperdollItem( InventorySlot.Legs )
        let headItem = inventory.getPaperdollItem( InventorySlot.Head )
        let glovesItem = inventory.getPaperdollItem( InventorySlot.Gloves )
        let feetItem = inventory.getPaperdollItem( InventorySlot.Feet )

        let legsId = legsItem ? legsItem.itemId : 0
        let headId = headItem ? headItem.itemId : 0
        let glovesId = glovesItem ? glovesItem.itemId : 0
        let feetId = feetItem ? feetItem.itemId : 0

        return this.containAllIds( this.chestId, legsId, headId, glovesId, feetId )
    }

    hasLegItem( id: number ) {
        return this.legs && this.legs.has( id )
    }

    hasHeadItem( id: number ) : boolean {
        return this.head && this.head.has( id )
    }

    hasGlovesItem( id: number ) : boolean {
        return this.gloves && this.gloves.has( id )
    }

    hasFeetItem( id: number ) : boolean {
        return this.feet && this.feet.has( id )
    }

    private containAllIds( chestId: number, legsId: number, headId: number, glovesId: number, feetId: number ): boolean {
        if ( chestId !== 0 && chestId !== this.chestId ) {
            return false
        }

        if ( !this.hasLegItem( legsId ) ) {
            return false
        }

        if ( !this.hasHeadItem( headId ) ) {
            return false
        }

        if ( !this.hasGlovesItem( glovesId ) ) {
            return false
        }

        return this.hasFeetItem( feetId )
    }

    containItem( slot: number, itemId: number ) {
        switch ( slot ) {
            case InventorySlot.Chest:
                return this.chestId === itemId

            case InventorySlot.Legs:
                return this.legs && this.legs.has( itemId )

            case InventorySlot.Head:
                return this.head && this.head.has( itemId )

            case InventorySlot.Gloves:
                return this.gloves && this.gloves.has( itemId )

            case InventorySlot.Feet:
                return this.feet && this.feet.has( itemId )

            default:
                return false
        }
    }

    containShieldByPlayer( player: L2PcInstance ): boolean {
        let inventory: Inventory = player.getInventory()

        let shieldItem: L2ItemInstance = inventory.getPaperdollItem( InventorySlot.LeftHand )
        return shieldItem && this.shield.has( shieldItem.getId() )
    }

    isEnchanted6( player: L2PcInstance ): boolean {
        let inventory: Inventory = player.getInventory()

        let chestItem = inventory.getPaperdollItem( InventorySlot.Chest )
        let legsItem = inventory.getPaperdollItem( InventorySlot.Legs )
        let headItem = inventory.getPaperdollItem( InventorySlot.Head )
        let glovesItem = inventory.getPaperdollItem( InventorySlot.Gloves )
        let feetItem = inventory.getPaperdollItem( InventorySlot.Feet )

        if ( !chestItem || chestItem.getEnchantLevel() < 6 ) {
            return false
        }

        if ( this.legs && ( !legsItem || legsItem.getEnchantLevel() < 6 ) ) {
            return false
        }

        if ( this.gloves && ( !glovesItem || glovesItem.getEnchantLevel() < 6 ) ) {
            return false
        }

        if ( this.head && ( !headItem || headItem.getEnchantLevel() < 6 ) ) {
            return false
        }

        if ( this.feet && ( !feetItem || feetItem.getEnchantLevel() < 6 ) ) {
            return false
        }

        return true
    }

    getSetSkill() : Skill {
        return this.setSkill
    }

    getShieldSkill() : Skill {
        return this.shieldSkill
    }

    getEnchant6Skill() : Skill {
        return this.enchant6Skill
    }

    containShieldById( id: number ) {
        return this.shield && this.shield.has( id )
    }
}