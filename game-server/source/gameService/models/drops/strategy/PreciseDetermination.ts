import { ConfigManager } from '../../../../config/ConfigManager'
import { PreciseDeterminationStrategy } from '../../../enums/drops/PreciseDeterminationStrategy'

export const PreciseDeterminationFunctions: { [ strategy: number ]: () => boolean } = {
    [ PreciseDeterminationStrategy.ALWAYS ]: () => true,
    [ PreciseDeterminationStrategy.DEFAULT ]: () => ConfigManager.general.preciseDropCalculation(),
    [ PreciseDeterminationStrategy.NEVER ]: () => false,
}