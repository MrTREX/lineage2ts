import { L2Character } from '../../actor/L2Character'
import { ConfigManager } from '../../../../config/ConfigManager'
import { GeneralHelper } from '../../../helpers/GeneralHelper'
import { GeneralDropItem } from '../GeneralDropItem'
import { ItemTypes } from '../../../values/InventoryValues'
import { AttackerChanceModifierStrategy } from '../../../enums/drops/AttackerChanceModifierStrategy'

function defaultStrategy( item: GeneralDropItem, target: L2Character, attacker: L2Character ): number {
    let levelDifference = target.getLevel() - attacker.getLevel()
    if ( ( target.isRaid() ) && ConfigManager.npc.useDeepBlueDropRulesRaid() ) {
        return Math.max( 0, Math.min( 1, ( levelDifference * 0.15 ) + 1 ) )
    }

    if ( ConfigManager.npc.useDeepBlueDropRules() ) {
        return GeneralHelper.remapRange( levelDifference,
                -ConfigManager.npc.getDropItemMaxLevelDifference(),
                -ConfigManager.npc.getDropItemMinLevelDifference(),
                ConfigManager.npc.getDropItemMinLevelGapChance(), 100.0 ) / 100
    }

    return 1
}

function nonGroupStrategy( item: GeneralDropItem, target: L2Character, attacker: L2Character ) : number {
    if ( ( ( !( target.isRaid() ) ) && ConfigManager.npc.useDeepBlueDropRules() )
            || ( ( target.isRaid() ) && ConfigManager.npc.useDeepBlueDropRulesRaid() ) ) {

        let levelDifference = target.getLevel() - attacker.getLevel()
        if ( item && item.getItemId() === ItemTypes.Adena ) {
            return GeneralHelper.remapRange( levelDifference,
                    -ConfigManager.npc.getDropAdenaMaxLevelDifference(),
                    -ConfigManager.npc.getDropAdenaMinLevelDifference(),
                    ConfigManager.npc.getDropAdenaMinLevelGapChance(), 100.0 ) / 100
        }

        return GeneralHelper.remapRange( levelDifference,
                -ConfigManager.npc.getDropItemMaxLevelDifference(),
                -ConfigManager.npc.getDropItemMinLevelDifference(),
                ConfigManager.npc.getDropItemMinLevelGapChance(), 100.0 ) / 100
    }

    return 1
}

type ModifierMethod = ( item: GeneralDropItem | null, target: L2Character, attacker: L2Character ) => number
export const AttackerChanceModifierFunctions : Record<number, ModifierMethod> = {
    [ AttackerChanceModifierStrategy.DEFAULT ]: defaultStrategy,
    [ AttackerChanceModifierStrategy.NONGROUP ]: nonGroupStrategy,
    [ AttackerChanceModifierStrategy.NORULES ]: () => 1
}