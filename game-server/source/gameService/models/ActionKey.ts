export interface ActionKey {
    categoryId: number
    commandId: number
    key: number
    toggleKeyOne: number
    toggleKeyTwo: number
    visibleStatus: number
}