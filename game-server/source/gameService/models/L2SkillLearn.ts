import { Race } from '../enums/Race'
import { SkillHolder } from './holders/SkillHolder'
import { SocialClass } from '../enums/SocialClass'
import { IClassId } from './base/ClassId'
import { ConfigManager } from '../../config/ConfigManager'
import { ItemDefinition } from '../interface/ItemDefinition'
import { Skill } from './Skill'

class SubClassData {
    slot: number
    level: number

    constructor( slot: number, level: number ) {
        this.slot = slot
        this.level = level
    }
}

export class L2SkillLearn {
    skillName: string
    skillId: number
    skillLevel: number
    getLevel: number
    autoGet: boolean
    levelUpSp: number
    requiredItems: Array<ItemDefinition> = []
    races: Array<Race> = []
    prerequisiteSkills: Array<SkillHolder> = []
    socialClass: SocialClass
    residenceSkill: boolean
    residenceIds: Array<number> = []
    subClassLevelNumbers: Array<SubClassData> = []
    learnedByNpc: boolean
    learnedByFS: boolean

    addPrerequisiteSkill( skill: SkillHolder ) {
        this.prerequisiteSkills.push( skill )
    }

    addRace( race: Race ) {
        this.races.push( race )
    }

    addRequiredItem( item: ItemDefinition ) {
        this.requiredItems.push( item )
    }

    addSubclassConditions( slot: number, level: number ) {
        this.subClassLevelNumbers.push( new SubClassData( slot, level ) )
    }

    getCalculatedLevelUpSp( playerClass: IClassId, learningClass: IClassId ): number {
        if ( !playerClass || !learningClass ) {
            return this.levelUpSp
        }

        let levelUpSp = this.levelUpSp
        // If the alternative skill learn system is enabled and the player is learning a skill from a different class apply a fee.
        if ( ConfigManager.character.skillLearn() && ( playerClass.id !== learningClass.id ) ) {
            // If the player is learning a skill from other class type (mage learning warrior skills or vice versa) the fee is higher.
            if ( playerClass.isMage !== learningClass.isMage ) {
                levelUpSp *= 3
            } else {
                levelUpSp *= 2
            }
        }

        return levelUpSp
    }

    getGetLevel() {
        return this.getLevel
    }

    getLevelUpSp() {
        return this.levelUpSp
    }

    getName() {
        return this.skillName
    }

    getPrerequisiteSkills() {
        return this.prerequisiteSkills
    }

    getRaces() {
        return this.races
    }

    getRequiredItems() {
        return this.requiredItems
    }

    getResidenceIds() {
        return this.residenceIds
    }

    getSkillId() {
        return this.skillId
    }

    getSkillLevel() {
        return this.skillLevel
    }

    getSocialClass(): SocialClass {
        return this.socialClass
    }

    getSubClassConditions() : Array<SubClassData> {
        return this.subClassLevelNumbers
    }

    isAutoGet() {
        return this.autoGet
    }

    isLearnedByFS() {
        return this.learnedByFS
    }

    isLearnedByNpc() {
        return this.learnedByNpc
    }

    isResidentialSkill() {
        return this.residenceSkill
    }

    setSocialClass( socialClass: SocialClass ) {
        if ( !this.socialClass ) {
            this.socialClass = socialClass
        }
    }
}