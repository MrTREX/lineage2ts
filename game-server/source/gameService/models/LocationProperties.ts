export interface LocationXY {
    x: number
    y: number
}

export interface LocationProperties extends LocationXY {
    z: number
}