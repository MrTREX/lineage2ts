export interface AugmentationChance {
    type: string
    stoneId: number
    variationId: number
    categoryChance: number
    augmentId: number
    augmentChance: number
}