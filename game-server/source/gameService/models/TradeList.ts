import { TradeItem, TradeItemHelper } from './TradeItem'
import { L2PcInstance } from './actor/instance/L2PcInstance'
import { L2World } from '../L2World'
import { L2Object } from './L2Object'
import { L2ItemInstance } from './items/instance/L2ItemInstance'
import { ConfigManager } from '../../config/ConfigManager'
import { SystemMessageBuilder } from '../packets/send/SystemMessage'
import { SystemMessageIds } from '../packets/SystemMessageIdValues'
import { ItemManagerCache } from '../cache/ItemManagerCache'
import { L2Item } from './items/L2Item'
import { RequestPrivateStoreBuyItem } from '../packets/receive/RequestPrivateStoreBuy'
import { PcInventory } from './itemcontainer/PcInventory'
import { RequestPrivateStoreSellItem } from '../packets/receive/RequestPrivateStoreSell'
import { ListenerCache } from '../cache/ListenerCache'
import { EventType, PlayerReceivesTradeItemEvent } from './events/EventType'
import { EventPoolCache } from '../cache/EventPoolCache'
import aigle from 'aigle'
import _ from 'lodash'
import { getMaxAdena } from '../helpers/ConfigurationHelper'
import { TradeListOutcome } from '../enums/TradeListOutcome'

export class TradeList {
    owner: number
    partner: number
    items: Array<TradeItem> = []
    title: string = ''
    isPackagedValue: boolean = false

    confirmed: boolean = false
    locked: boolean = false

    constructor( player: L2PcInstance ) {
        this.owner = player.objectId
    }

    getTitle() {
        return this.title
    }

    lock() {
        this.locked = true
    }

    isLocked() {
        return this.locked
    }

    clear() {
        this.items.forEach( TradeItemHelper.recycle )

        this.items.length = 0
        this.locked = false
        this.confirmed = false
        this.isPackagedValue = false
    }

    invalidateConfirmation() {
        this.confirmed = false
    }

    setPartner( partner: L2PcInstance ) {
        this.partner = partner.getObjectId()
    }

    getPartnerId(): number {
        return this.partner
    }

    getPartner(): L2PcInstance {
        return L2World.getPlayer( this.partner )
    }

    isConfirmed() {
        return this.confirmed
    }

    getOwner(): L2PcInstance {
        return L2World.getPlayer( this.owner )
    }

    addItem( objectId: number, count: number, price: number = 0 ) {
        if ( this.isLocked() ) {
            return null
        }

        let existingItem: L2Object = L2World.getObjectById( objectId )
        if ( !existingItem || !existingItem.isItem() ) {
            return null
        }

        let item: L2ItemInstance = existingItem as L2ItemInstance
        let owner = this.getOwner()
        if ( !owner
                || !( item.isTradeable() || ( owner.isGM() && ConfigManager.general.gmTradeRestrictedItems() ) )
                || item.isQuestItem() ) {
            return null
        }

        if ( !owner.getInventory().canManipulateWithItemId( item.getId() ) ) {
            return null
        }

        if ( count <= 0 || count > item.getCount() ) {
            return null
        }

        if ( !item.isStackable() && count > 1 ) {
            return null
        }

        if ( ( getMaxAdena() / count ) < price ) {
            return null
        }

        let isExistingItem = this.items.some( ( currentItem: TradeItem ) => {
            return currentItem.objectId === objectId
        } )

        if ( isExistingItem ) {
            return null
        }

        let tradeItem: TradeItem = TradeItemHelper.fromItemInstance( item, count, price )
        this.items.push( tradeItem )

        this.invalidateConfirmation()
        return tradeItem
    }

    async confirm(): Promise<boolean> {
        if ( this.confirmed ) {
            return true
        }

        let partner = this.getPartner()
        if ( partner ) {
            let partnerList: TradeList = partner.getActiveTradeList()
            if ( !partnerList ) {
                return false
            }

            if ( partnerList.isConfirmed() ) {
                partnerList.lock()
                this.lock()

                if ( !partnerList.validate() ) {
                    return false
                }

                if ( !this.validate() ) {
                    return false
                }

                await this.doExchange( partnerList )
            } else {
                partner.onTradeConfirm( this.getOwner() )
            }
        }

        this.confirmed = true

        return this.confirmed
    }

    validate() : boolean {
        let owner = this.getOwner()
        if ( !owner ) {
            return false
        }

        return !this.items.some( ( tradeItem: TradeItem ) => {
            let item: L2ItemInstance = owner.checkItemManipulation( tradeItem.objectId, tradeItem.count )
            return !item || item.getCount() < 1
        } )
    }

    async doExchange( receiverData: TradeList ): Promise<void> {
        let outcome = false

        let owner = this.getOwner()
        let partner = receiverData.getOwner()
        if ( ( !owner.getInventory().validateWeight( receiverData.calculateItemsWeight() ) )
                || !( partner.getInventory().validateWeight( this.calculateItemsWeight() ) ) ) {
            partner.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.WEIGHT_LIMIT_EXCEEDED ) )
            owner.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.WEIGHT_LIMIT_EXCEEDED ) )

        } else if ( ( !owner.getInventory().validateCapacity( receiverData.countItemsSlots( owner ) ) )
                || ( !partner.getInventory().validateCapacity( this.countItemsSlots( partner ) ) ) ) {
            partner.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SLOTS_FULL ) )
            owner.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SLOTS_FULL ) )
        } else {
            await receiverData.transferItems( owner )
            await this.transferItems( partner )

            outcome = true
        }

        partner.onTradeFinish( outcome )
        owner.onTradeFinish( outcome )
    }

    async transferItems( partner: L2PcInstance ): Promise<void> {
        let owner = this.getOwner()
        let shouldLogItems: boolean = ListenerCache.hasGeneralListener( EventType.PlayerReceivesTradeItem )
        let eventPromises: Array<Promise<void>> = shouldLogItems ? [] : null

        await aigle.resolve( this.items ).each( async ( tradeItem: TradeItem ) => {
            let oldItem: L2ItemInstance = owner.getInventory().getItemByObjectId( tradeItem.objectId )
            if ( !oldItem ) {
                return false
            }

            let newItem: L2ItemInstance = await owner.getInventory().transferItem( tradeItem.objectId, tradeItem.count, partner.getInventory() )

            if ( shouldLogItems ) {
                let data = EventPoolCache.getData( EventType.PlayerReceivesTradeItem ) as PlayerReceivesTradeItemEvent

                data.type = 'TradeList'
                data.itemId = oldItem.getId()
                data.itemAmount = tradeItem.count
                data.receiverId = partner.getObjectId()
                data.senderId = owner.getObjectId()

                eventPromises.push( ListenerCache.sendGeneralEvent( EventType.PlayerReceivesTradeItem, data ) )
            }

            if ( !newItem ) {
                return false
            }
        } )

        if ( eventPromises && eventPromises.length > 0 ) {
            await Promise.all( eventPromises )
        }
    }

    calculateItemsWeight(): number {
        let weight = 0

        this.items.forEach( ( tradeItem: TradeItem ) => {
            if ( !tradeItem ) {
                return
            }

            let template: L2Item = ItemManagerCache.getTemplate( tradeItem.itemTemplate.getId() )
            if ( !template ) {
                return
            }

            weight += tradeItem.count * template.getWeight()
        } )

        return Math.min( weight, Number.MAX_SAFE_INTEGER )
    }

    countItemsSlots( partner: L2PcInstance ) {
        let slots = 0

        this.items.forEach( ( tradeItem: TradeItem ) => {
            if ( !tradeItem ) {
                return
            }

            let template: L2Item = ItemManagerCache.getTemplate( tradeItem.itemTemplate.getId() )
            if ( !template ) {
                return
            }

            if ( !template.isStackable() ) {
                slots += tradeItem.count
            } else if ( !partner.getInventory().getItemByItemId( tradeItem.itemTemplate.getId() ) ) {
                slots++
            }
        } )

        return slots
    }

    updateItems(): void {
        let owner = this.getOwner()
        let list = this
        this.items.forEach( ( tradeItem: TradeItem ) => {
            let item: L2ItemInstance = owner.getInventory().getItemByObjectId( tradeItem.objectId )

            if ( !item || tradeItem.count < 1 ) {
                list.removeItem( tradeItem.objectId, -1, -1 )
                return
            }

            if ( tradeItem.count > item.getCount() ) {
                tradeItem.count = item.getCount()
                return
            }
        } )
    }

    // TODO : review logic around usage and avoid changing items array in place
    removeItem( objectId: number, itemId: number, count: number ): void {
        if ( this.isLocked() ) {
            return
        }

        let list = this

        _.each( this.items, ( tradeItem: TradeItem ) => {
            if ( tradeItem.objectId === objectId || tradeItem.itemTemplate.getId() === itemId ) {
                let partner: L2PcInstance = list.getPartner()
                if ( partner ) {
                    let partnerList: TradeList = partner.getActiveTradeList()
                    if ( !partner ) {
                        return false
                    }

                    partnerList.invalidateConfirmation()
                }

                if ( count !== -1 && tradeItem.count > count ) {
                    tradeItem.count = tradeItem.count - count
                } else {
                    _.pull( list.items, tradeItem )
                    TradeItemHelper.recycle( tradeItem )
                }

                return false
            }
        } )
    }

    adjustAvailableItem( item: L2ItemInstance ): TradeItem {
        if ( item.isStackable() ) {
            let tradeItem: TradeItem = this.items.find( ( currentItem: TradeItem ) => {
                return currentItem.itemTemplate.getId() === item.getId()
            } )

            if ( tradeItem ) {
                if ( item.getCount() <= tradeItem.count ) {
                    return null
                }

                // TODO : examine chain of calls to make sure to recycle item
                return TradeItemHelper.fromItemInstance( item, item.getCount() - tradeItem.count, item.getReferencePrice() )
            }
        }

        return TradeItemHelper.fromItemInstance( item, item.getCount(), item.getReferencePrice() )
    }

    setPackaged( value: boolean ) {
        this.isPackagedValue = value
    }

    getItems(): Array<TradeItem> {
        return this.items
    }

    async privateStoreBuy( player: L2PcInstance, items: ReadonlyArray<RequestPrivateStoreBuyItem> ): Promise<TradeListOutcome> {
        if ( this.locked ) {
            return TradeListOutcome.Failure
        }

        if ( !this.validate() ) {
            this.lock()
            return TradeListOutcome.Failure
        }

        let owner = this.getOwner()
        if ( !owner.isOnline() || !player.isOnline() ) {
            return TradeListOutcome.Failure
        }

        let slots = 0
        let weight = 0
        let totalPrice = 0

        let ownerInventory: PcInventory = owner.getInventory()
        let playerInventory: PcInventory = player.getInventory()
        let maxAdena = getMaxAdena()

        for ( let item of items ) {
            let found: boolean = this.items.some( ( tradeItem: TradeItem ) => {
                if ( tradeItem.objectId === item.objectId ) {
                    if ( tradeItem.price === item.price ) {
                        if ( tradeItem.count < item.count ) {
                            item.count = tradeItem.count
                        }
                        found = true
                    }

                    return true
                }

                return false
            } )

            if ( !found ) {
                if ( this.isPackaged() ) {
                    return TradeListOutcome.Violation
                }

                item.count = 0
                continue
            }

            if ( ( maxAdena / item.count ) < item.price ) {
                this.lock()
                return TradeListOutcome.Failure
            }

            totalPrice += item.count * item.price
            if ( ( maxAdena < totalPrice ) || ( totalPrice < 0 ) ) {
                this.lock()
                return TradeListOutcome.Failure
            }

            let oldItem: L2ItemInstance = owner.checkItemManipulation( item.objectId, item.count )
            if ( !oldItem || !oldItem.isTradeable() ) {
                this.lock()
                return TradeListOutcome.Violation
            }

            let template: L2Item = ItemManagerCache.getTemplate( oldItem.getId() )
            if ( !template ) {
                continue
            }

            weight += item.count * template.getWeight()
            if ( !template.isStackable() ) {
                slots += item.count
            } else if ( !playerInventory.getItemByItemId( oldItem.getId() ) ) {
                slots++
            }
        }


        if ( totalPrice > playerInventory.getAdenaAmount() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ADENA ) )
            return TradeListOutcome.Failure
        }

        if ( !playerInventory.validateWeight( weight ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.WEIGHT_LIMIT_EXCEEDED ) )
            return TradeListOutcome.Failure
        }

        if ( !playerInventory.validateCapacity( slots ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SLOTS_FULL ) )
            return TradeListOutcome.Failure
        }

        let isAdenaReduced: boolean = await playerInventory.reduceAdena( totalPrice, 'TradeList.privateStoreBuy' )
        if ( !isAdenaReduced ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ADENA ) )
            return TradeListOutcome.Failure
        }

        await ownerInventory.addAdena( totalPrice, player.getObjectId(), 'TradeList.privateStoreBuy' )

        let isSuccess = true

        let tradeList = this
        await aigle.resolve( items ).eachSeries( async ( item: RequestPrivateStoreBuyItem ) => {
            if ( item.count === 0 ) {
                return
            }

            let oldItem: L2ItemInstance = owner.checkItemManipulation( item.objectId, item.count )
            if ( !oldItem ) {
                tradeList.lock()
                isSuccess = false
                return false
            }

            let newItem: L2ItemInstance = await ownerInventory.transferItem( item.objectId, item.count, playerInventory )
            if ( !newItem ) {
                isSuccess = false
                return false
            }

            tradeList.removeItem( item.objectId, -1, item.count )

            if ( newItem.isStackable() ) {
                let ownerUpdate = new SystemMessageBuilder( SystemMessageIds.C1_PURCHASED_S3_S2_S )
                        .addString( player.getName() )
                        .addItemInstanceName( newItem )
                        .addNumber( item.count )
                        .getBuffer()
                owner.sendOwnedData( ownerUpdate )

                let playerUpdate = new SystemMessageBuilder( SystemMessageIds.PURCHASED_S3_S2_S_FROM_C1 )
                        .addString( owner.getName() )
                        .addItemInstanceName( newItem )
                        .addNumber( item.count )
                        .getBuffer()
                player.sendOwnedData( playerUpdate )
            } else {
                let ownerUpdate = new SystemMessageBuilder( SystemMessageIds.C1_PURCHASED_S2 )
                        .addString( player.getName() )
                        .addItemInstanceName( newItem )
                        .getBuffer()
                owner.sendOwnedData( ownerUpdate )

                let playerUpdate = new SystemMessageBuilder( SystemMessageIds.PURCHASED_S2_FROM_C1 )
                        .addString( owner.getName() )
                        .addItemInstanceName( newItem )
                        .getBuffer()
                player.sendOwnedData( playerUpdate )
            }
        } )

        if ( isSuccess ) {
            return TradeListOutcome.Success
        }

        return TradeListOutcome.Violation
    }

    isPackaged(): boolean {
        return this.isPackagedValue
    }

    setTitle( title: string ): void {
        this.title = title
    }

    addItemByItemId( itemId: number, count: number, price: number ): boolean {
        if ( this.isLocked() ) {
            return false
        }

        let item: L2Item = ItemManagerCache.getTemplate( itemId )
        if ( !item ) {
            return false
        }

        if ( !item.isTradeable() || item.isQuestItem() ) {
            return false
        }

        if ( !item.isStackable() && ( count > 1 ) ) {
            return false
        }

        if ( ( getMaxAdena() / count ) < price ) {
            return false
        }

        this.items.push( TradeItemHelper.fromItem( item, count, price ) )
        this.invalidateConfirmation()

        return true
    }

    async privateStoreSell( player: L2PcInstance, items: Array<RequestPrivateStoreSellItem> ): Promise<TradeListOutcome> {
        if ( this.isLocked() ) {
            return TradeListOutcome.Failure
        }

        let owner = this.getOwner()
        if ( !owner.isOnline() || !player.isOnline() ) {
            return TradeListOutcome.Failure
        }

        let ownerInventory: PcInventory = owner.getInventory()
        let playerInventory: PcInventory = player.getInventory()

        let totalPrice = 0
        let maxAdena = getMaxAdena()

        for ( let item of items ) {
            let found: boolean = this.items.some( ( tradeItem: TradeItem ) => {
                if ( tradeItem.itemTemplate.getId() === item.itemId ) {
                    if ( tradeItem.price === item.price ) {
                        if ( tradeItem.count < item.count ) {
                            item.count = tradeItem.count
                        }

                        found = item.count > 0
                    }

                    return true
                }

                return false
            } )

            if ( !found ) {
                continue
            }

            if ( ( maxAdena / item.count ) < item.price ) {
                this.lock()
                break
            }

            let priceCheck = totalPrice + ( item.count * item.price )
            if ( ( maxAdena < priceCheck ) || ( priceCheck < 0 ) ) {
                this.lock()
                break
            }

            if ( ownerInventory.getAdenaAmount() < priceCheck ) {
                continue
            }

            let objectId: number = item.objectId
            let oldItem: L2ItemInstance = player.checkItemManipulation( item.objectId, item.count )
            if ( !oldItem ) {
                oldItem = playerInventory.getItemByItemId( item.itemId )
                if ( !oldItem ) {
                    continue
                }

                objectId = oldItem.getObjectId()
                oldItem = player.checkItemManipulation( oldItem.getObjectId(), item.count )
                if ( !oldItem ) {
                    continue
                }
            }

            if ( oldItem.getId() !== item.itemId ) {
                return TradeListOutcome.Violation
            }

            if ( !oldItem.isTradeable() ) {
                continue
            }

            let newItem: L2ItemInstance = await playerInventory.transferItem( objectId, item.count, ownerInventory )
            if ( !newItem ) {
                continue
            }

            this.removeItem( -1, item.itemId, item.count )

            totalPrice = priceCheck

            if ( newItem.isStackable() ) {
                let ownerMessage = new SystemMessageBuilder( SystemMessageIds.PURCHASED_S3_S2_S_FROM_C1 )
                        .addString( player.getName() )
                        .addItemInstanceName( newItem )
                        .addNumber( item.count )
                        .getBuffer()
                owner.sendOwnedData( ownerMessage )

                let playerMessage = new SystemMessageBuilder( SystemMessageIds.C1_PURCHASED_S3_S2_S )
                        .addString( owner.getName() )
                        .addItemInstanceName( newItem )
                        .addNumber( item.count )
                        .getBuffer()
                player.sendOwnedData( playerMessage )
            } else {
                let ownerMessage = new SystemMessageBuilder( SystemMessageIds.PURCHASED_S2_FROM_C1 )
                        .addString( player.getName() )
                        .addItemInstanceName( newItem )
                        .getBuffer()
                owner.sendOwnedData( ownerMessage )

                let playerMessage = new SystemMessageBuilder( SystemMessageIds.C1_PURCHASED_S2 )
                        .addString( owner.getName() )
                        .addItemInstanceName( newItem )
                        .getBuffer()
                player.sendOwnedData( playerMessage )
            }
        }

        if ( totalPrice > 0 ) {
            if ( totalPrice > ownerInventory.getAdenaAmount() || !( await ownerInventory.reduceAdena( totalPrice, 'TradeList.PrivateStoreSell' ) ) ) {
                return TradeListOutcome.Failure
            }

            await playerInventory.addAdena( totalPrice, owner.getObjectId(), 'TradeList.PrivateStoreSell' )
        }

        return TradeListOutcome.Success
    }

    getAvailableItems( inventory: PcInventory ): Array<TradeItem> {
        return this.items.map( ( item: TradeItem ): TradeItem => {
            let availableItem: TradeItem = TradeItemHelper.fromTradeItem( item )

            inventory.adjustAvailableItem( availableItem )

            return availableItem
        } )
    }
}