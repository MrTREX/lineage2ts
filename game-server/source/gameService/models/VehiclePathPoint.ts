import { Location } from './Location'

export class VehiclePathPoint extends Location {
    moveSpeed: number
    rotationSpeed: number

    constructor( x: number, y: number, z: number, moveSpeed: number, rotationSpeed: number ) {
        super( x, y, z )
        this.moveSpeed = moveSpeed
        this.rotationSpeed = rotationSpeed
    }

    getMoveSpeed() {
        return this.moveSpeed
    }

    getRotationSpeed() {
        return this.rotationSpeed
    }
}