import { TrapAction } from '../../enums/TrapAction'
import { AcquireSkillType } from '../../enums/AcquireSkillType'
import { PrivateStoreType } from '../../enums/PrivateStoreType'
import { InstanceType } from '../../enums/InstanceType'
import { SevenSignsSide } from '../../values/SevenSignsValues'
import { SevenSignsFestivalLevel } from '../../values/SevenSignsFestivalValues'

export const enum EventType {
    AttackableAttacked,
    AttackableKilled,
    AttackableInitialAttack,

    ClanWarFinish,
    ClanWarStart,
    ClanChangeLevel,

    CreatureAvoidAttack,
    CharacterReceivedDamage,
    CharacterKilled,
    CharacterUsesSkill,
    CharacterTeleported,
    NpcAttemptsDying,
    CharacterEnterArea,
    CharacterExitArea,

    FortSiegeStart,
    FortSiegeFinish,

    ItemBypass,
    ItemCreate,
    ItemTalking,
    NpcSeePlayer,
    NpcReceivingEvent,
    NpcApproachedForTalk,
    CharacterStoppedMoving,
    PlayerStoppedMoving,
    NpcRouteNodeArrived,
    NpcRouteFinished,
    NpcStartQuest,
    NpcSkillFinished,
    NpcSeeSkill,
    NpcSpawn,
    NpcTalk,
    NpcTeleport,
    NpcManorBypass,
    NpcEvent,
    TerminatedNpcStartsAttack,
    NpcHasSweepItems,
    NpcHasDropItems,
    NpcTeachesSkills,

    OlympiadMatchResult,

    PlayableExpChanged,
    PlayableKilled,

    PlayerAugmentItem,
    PlayerBypass,
    PlayerChat,
    PlayerPrivateChat,

    PlayerTutorialLink,
    PlayerTutorialCommand,
    PlayerTutorialQuestionMark,
    PlayerTutorialClient,

    PlayerCreatedClan,
    PlayerDestroyedClan,
    PlayerJoinsClan,
    PlayerClanLeaderChange,
    PlayerClanLeaderChangeScheduled,
    PlayerLeavesClan,
    PlayerClanLevelup,

    PlayerAddItemToClanWarehouse,
    PlayerDestroyItemInClanWarehouse,
    PlayerTransferItemFromClanWarehouse,
    PlayerCanDestroyItemInClanWarehouse,
    PlayerCreated,
    PlayerDeleted,
    PlayerAnswerDialog,
    PlayerEquipItem,
    PlayerUnEquipsItem,
    PlayerFameChanged,

    PlayerAddHenna,
    PlayerRemoveHenna,

    PlayerAddItem,
    PlayerCanDestroyItem,
    PlayerDestroyItem,
    PlayerCanDropItem,
    PlayerDropItem,
    PlayerPickupItem,
    PlayerTransferItem,

    PlayerKarmaChanged,
    PlayerChangedLevel,
    PlayerLogin,
    PlayerStartsLogout,
    PlayerLoggedOut,
    PlayerPKAmountChanged,
    PlayerChangeProfession,
    PlayerCancelProfession,
    PlayerKillsChanged,
    PlayerPvpKill,
    PlayerRestored,
    PlayerSelectCharacter,
    PlayerSit,
    PlayerLearnSkill,
    PlayerStandup,
    PlayerSummonSpawn,
    PlayerStartsAttack,
    PlayerTransform,
    PlayerPartyRequest,
    PlayerVitalityChanged,

    TrapTriggered,

    TvTPlayerKilled,
    PlayerStartCreateClan,
    PlayerStartDestroyClan,
    PlayerAttemptsDying,
    PlayerKilled,
    VehiclePathEnded,
    VehiclePathStarted,
    GMRemovedBuff,
    GMRemovedAllBuffs,
    GMCancelBuffsInArea,
    GMViewBuffs,
    GMChangeAccessLevel,
    GMSiegeHallSetDate,
    GMSiegeHallEndSiege,
    GMSiegeHallStartSiege,
    GMSiegeHallAddAttacker,
    GMSiegeHallRemoveAttacker,
    GMSiegeHallClearAttackers,
    GMSiegeHallForwardSiege,
    GMClanChangeLeader,
    GMCreateItem,
    GMGiftItems,
    GMAddLevel,
    GMSetLevel,
    GMHealPlayers,
    GMSetHp,
    GMSetMp,
    GMReduceHp,
    GMKillTargets,
    GMSpawnNpc,
    GMChangeClassId,
    GMSetVitalityPoints,
    GMCreateDroppedItem,
    PlayerAdminCommand,
    DoorInteraction,
    NpcBypass,
    StatusCheckRequested,
    ItemAuctionStart,
    ItemAuctionEnd,
    ItemAuctionBid,
    PlayerReceivesTradeItem,
    GeoRegionActivated,
    GeoRegionDeactivated,
    ItemEndOfLife,
    VoiceCommand,
    PlayerSummonTalk,
    ItemAmountChange,
    PlayerAccessLevelChanged,
    TWPlayerLogin,
    TWPlayerDeath,
    GameTimeHour,
    CycleStepChange,

    DimensionalItemAdd,
    DimensionalItemRemove,
    DimensionalItemUpdate,
    GMDimensionalItemAdd,
    GMDimensionalItemRemove,

    SevenSignsFestivalReward,
    PlayerBuyGPProduct,
    PlayerConsumeCraftMP,

    /**
     * Violations are both for reporting and logging, hence require log-like parameters such as error code, message and any ids captured
     */
    TradeViolation,
    GeneralViolation,
    ItemViolation,
    PacketDataViolation,
    SkillViolation,
    BuySellViolation,
    SummonViolation,
    DataIntegrityViolation,
    MailViolation
}

export interface EventTerminationResult {
    terminate: boolean
    priority: number
    message: string
}

export type EventTypeData = {
    [ key: string ]: string | number | boolean | Array<number>
}

export interface PlayerKarmaChangedEvent extends EventTypeData {
    playerId: number
    oldKarma: number
    newKarma: number
}

export interface PlayerPKChangedEvent extends EventTypeData {
    playerId: number
    oldValue: number
    newValue: number
}

export interface PlayerClanJoinEvent extends EventTypeData {
    playerId: number
    clanId: number
    clanLevel: number
}

export interface PlayerClanLevelUpEvent extends EventTypeData {
    clanId: number
    oldLevel: number
    newLevel: number
}

export interface PlayerLeftClanEvent extends EventTypeData {
    playerId: number
    clanId: number
    clanLevel: number
}

export interface PlayerClanLeaderChangeEvent extends EventTypeData {
    oldLeaderId: number
    newLeaderId: number
    clanId: number
}

export interface PlayerClanLeaderScheduledEvent extends EventTypeData {
    newLeaderId: number
    clanId: number
    oldLeaderId: number
}

export interface NpcGeneralEvent extends EventTypeData {
    eventName: string
    characterId: number
    characterNpcId: number
    playerId: number
    isTimer: boolean
}

export interface AttackableAttackedEvent extends EventTypeData {
    attackerPlayerId: number
    targetId: number
    damage: number
    skillId: number
    skillLevel: number
    targetNpcId: number
    attackerInstanceType: number
    attackerId: number
    isChampion: boolean
}

export interface AttackableInitialAttackEvent extends EventTypeData {
    attackerId: number
    attackerInstanceType: number
    targetId: number
    targetNpcId: number
}

export interface PlayerCreatedClanEvent extends EventTypeData {
    leaderId: number
    clanId: number
    clanName: string
}

export interface PlayerDestroysClanEvent extends EventTypeData {
    playerId: number
    clanId: number
    clanName: string
    clanSize: number
    clanLevel: number
}

export interface PlayerStartDestroyClanEvent extends EventTypeData {
    playerId: number
    clanName: string
    clanSize: number
    clanLevel: number
    clanId: number
}

export interface ClanWarStartEvent extends EventTypeData {
    clanOneId: number
    clanTwoId: number
}

export interface ClanWarFinishEvent extends EventTypeData {
    clanOneId: number
    clanTwoId: number
}

export interface ItemCreateEvent extends EventTypeData {
    reason: string
    itemObjectId: number
    playerId: number
    amount: number
}

export interface NpcRouteNodeArrivedEvent extends EventTypeData {
    characterId: number
    nodeIndex: number
    routeName: string
}

export interface NpcRouteFinishedEvent extends EventTypeData {
    characterId: number
    routeName: string
}

export interface CreatureAvoidAttackEvent extends EventTypeData {
    attackerId: number
    targetId: number
    isDOT: boolean
}

export interface NpcFinishedSkillEvent extends EventTypeData {
    attackerId: number
    targetId: number
    skillId: number
    skillLevel: number
    attackerNpcId: number
    targetInstanceType: InstanceType
}

export interface NpcTeleportEvent extends EventTypeData {
    characterId: number
    npcId: number
}

export interface NpcSpawnEvent extends EventTypeData {
    characterId: number
    npcId: number
}

export interface NpcReceivingEvent extends EventTypeData {
    eventName: string
    senderId: number
    receiverId: number
    referenceId: number
}

export interface NpcSeeSkillEvent extends EventTypeData {
    originatorId: number // who casts skill
    playerId: number // any player involved
    receiverId: number // who is seeing skill
    receiverNpcId: number
    receiverIsChampion: boolean // is champion mob
    skillId: number
    skillLevel: number
    targetIds: Array<number>
}

export interface PlayerStartsAttackEvent extends EventTypeData {
    playerId: number
    targetId: number
    targetType: number
}

export interface TerminatedNpcStartsAttackEvent extends EventTypeData {
    attackerId: number
    targetId: number
    npcId: number
    targetType: number
}

export interface CharacterReceivedDamageEvent extends EventTypeData {
    attackerId: number
    targetId: number
    damage: number
    skillId: number
    skillLevel: number
    isCritical: boolean
    isDOT: boolean
    isReflected: boolean
}

export interface CharacterTeleportedEvent extends EventTypeData {
    characterId: number
    characterNpcId: number
}

export interface PlayerSummonSpawnEvent extends EventTypeData {
    objectId: number
    npcId: number
    ownerId: number
}

export interface NpcApproachedForTalkEvent extends EventTypeData {
    playerId: number
    characterId: number
    characterNpcId: number
}

export interface CreatureUsesSkillEvent extends EventTypeData {
    attackerId: number
    skillId: number
    skillLevel: number
    isSimultaneous: boolean
    mainTargetId: number
    targetIds: Array<number>
}

export interface CharacterKilledEvent extends EventTypeData {
    attackerId: number
    targetId: number
}

export interface AttackableKillEvent extends EventTypeData {
    playerId: number
    attackerId: number
    targetId: number
    npcId: number
    isChampion: boolean
    instanceId: number
}

export interface PlayerSitEvent extends EventTypeData {
    playerId: number
}

export interface PlayerStandEvent extends EventTypeData {
    playerId: number
}

export interface PlayableExpChangedEvent extends EventTypeData {
    characterId: number
    oldValue: number
    newValue: number
    currentLevel: number
}

export interface PlayerSelectCharacterEvent extends EventTypeData {
    playerId: number
    accountName: string
    slot: number
}

export interface PlayerAnswerDialogEvent extends EventTypeData {
    playerId: number
    messageId: number
    answer: number
    requesterId: number
}

export interface NpcSeePlayerEvent extends EventTypeData {
    npcObjectId: number
    npcId: number
    playerId: number
}

export interface PlayerBypassEvent extends EventTypeData {
    playerId: number
    command: string
    bypassOriginId: number
}

export interface CharacterExitZoneEvent extends EventTypeData {
    characterId: number
    areaId: number
    areaType: number
}

export interface CharacterEnterZoneEvent extends EventTypeData {
    characterId: number
    areaId: number
    areaType: number
    characterInstanceType: number
}

export interface ItemBypassEvent extends EventTypeData {
    objectId: number
    playerId: number
    eventName: string
    itemId: number
    isArmor: boolean
    isWeapon: boolean
    isEtc: boolean
}

export interface ItemTalkingEvent extends EventTypeData {
    objectId: number
    playerId: number
    itemId: number
    isArmor: boolean
    isWeapon: boolean
    isEtc: boolean
}

export interface PlayerAugmentItemEvent extends EventTypeData {
    playerId: number
    objectId: number
    isAugmented: boolean
    itemId: number
}

export interface PlayerEquipItemEvent extends EventTypeData {
    playerId: number
    objectId: number
    itemId: number
}

export interface PlayerKillsChangedEvent extends EventTypeData {
    playerId: number
    oldValue: number
    newValue: number
}

export interface PlayerChangedFameEvent extends EventTypeData {
    playerId: number
    oldValue: number
    newValue: number
}

export interface PlayerChangedLevelEvent extends EventTypeData {
    playerId: number
    oldLevelValue: number
    newLevelValue: number
}

export interface PlayerTransformEvent extends EventTypeData {
    playerId: number
    transformId: number
}

export interface PlayerPvpKillEvent extends EventTypeData {
    attackerId: number
    targetId: number
}

export interface TvTKillEvent extends EventTypeData {
    attackerId: number
    targetId: number
    teamId: number
}

export interface PlayerRemoveHennaEvent extends EventTypeData {
    playerId: number
    dyeItemId: number
    cancelCount: number
    dyeId: number
    cancelFee: number
}

export interface PlayerAddHennaEvent extends EventTypeData {
    playerId: number
    dyeItemId: number
    dyeId: number
    classId: number
}

export interface PlayerCancelProfessionEvent extends EventTypeData {
    playerId: number
    classId: number
}

export interface PlayerChangeProfessionEvent extends EventTypeData {
    playerId: number
    classId: number
    isSubclassActive: boolean
}

export interface PlayerLoginEvent extends EventTypeData {
    playerId: number
    classId: number
    level: number
    race: number
}

export interface PlayerStartsLogoutEvent extends EventTypeData {
    playerId: number
    x: number
    y: number
    z: number
    instanceId: number
}

export interface PlayerLoggedOutEvent extends EventTypeData {
    playerId: number
    playerName: string
    sessionDurationMs: number
}

export interface TrapActionEvent extends EventTypeData {
    trapObjectId: number
    triggerObjectId: number
    action: TrapAction
}

export interface FortSiegeFinishEvent extends EventTypeData {
    fortId: number
}

export interface FortSiegeStartEvent extends EventTypeData {
    fortId: number
}

export interface PlayerAddItemToClanWarehouseEvent extends EventTypeData {
    reason: string
    playerId: number
    itemObjectId: number
    itemTemplateId: number
    clanId: number
    amount: number
}

export interface PlayerDestroyItemInClanWarehouseEvent extends EventTypeData {
    reason: string
    playerId: number
    itemObjectId: number
    itemTemplateId: number
    amount: number
    clanId: number
}

export interface PlayerTransferItemFromClanWarehouseEvent extends EventTypeData {
    reason: string
    playerId: number
    itemObjectId: number
    itemTemplateId: number
    amount: number
    clanId: number
}

export interface PlayerAddItemEvent extends EventTypeData {
    playerId: number
    itemObjectId: number
    itemTemplateId: number
    amount: number
    reason: string
    previousOwnerId: number
}

export interface PlayerTransferItemEvent extends EventTypeData {
    playerId: number
    itemObjectId: number
    itemTemplateId: number
    amount: number
    destinationType: string
    destinationPlayerId: number
}

export interface PlayerDestroyItemEvent extends EventTypeData {
    playerId: number
    itemObjectId: number
    itemTemplateId: number
    amount: number
}

export interface PlayerCanDestroyItemEvent extends PlayerDestroyItemEvent {}

export interface PlayerCanDestroyItemInClanWarehouseEvent extends PlayerDestroyItemInClanWarehouseEvent {}

export interface PlayerDropItemEvent extends EventTypeData {
    playerId: number
    itemObjectId: number
    locationX: number
    locationY: number
    locationZ: number
    itemTemplateId: number
    amount: number
}

export interface PlayerCanDropItemEvent extends PlayerDropItemEvent {
}

export interface PlayerPickupItemEvent extends EventTypeData {
    playerId: number
    itemObjectId: number
    itemId: number
    amount: number
}

export interface PlayerLearnSkillEvent extends EventTypeData {
    trainerObjectId: number
    playerId: number
    skillId: number
    skillLevel: number
    type: AcquireSkillType
}

export interface NpcManorBypassEvent extends EventTypeData {
    playerId: number
    characterId: number
    requestId: number
    manorId: number
    isNextPeriod: boolean
}

export interface PlayerTutorialClientEvent extends EventTypeData {
    playerId: number
    eventId: number
}

export interface PlayerTutorialLinkEvent extends EventTypeData {
    playerId: number
    command: string
}

export interface PlayerTutorialCommandEvent extends EventTypeData {
    playerId: number
    command: string
}

export interface PlayerTutorialQuestionMarkEvent extends EventTypeData {
    playerId: number
    responseId: number
}

export interface NpcTalkEvent extends EventTypeData {
    playerId: number
    characterId: number
    characterNpcId: number
}

export interface PlayerStartCreateClanEvent extends EventTypeData {
    playerId: number
    clanName: string
}

export interface PlayerChatEvent extends EventTypeData {
    playerId: number
    targetId: number
    type: number
    text: string
}

export interface VehiclePathEndedEvent extends EventTypeData {
    objectId: number
}

export interface VehiclePathStartedEvent extends EventTypeData {
    objectId: number
}

export interface GMRemovedBuffEvent extends EventTypeData {
    targetId: number
    skillId: number
    originatorId: number
}

export interface GMRemovedAllBuffsEvent extends EventTypeData {
    targetId: number
    originatorId: number
}

export interface GMCancelBuffsInAreaEvent extends EventTypeData {
    originatorId: number
    radius: number
    affectedPlayerIds: Array<number>
}

export interface GMViewBuffsEvent extends EventTypeData {
    originatorId: number
    targetId: number
    targetInstance: string
}

export interface GMChangeAccessLevelEvent extends EventTypeData {
    originatorId: number
    targetId: number
    level: number
}

export interface GMSiegeHallSetDateEvent extends EventTypeData {
    originatorId: number
    hallId: number
    nextDate: number
}

export interface GMSiegeHallEndSiegeEvent extends EventTypeData {
    originatorId: number
    hallId: number
}

export interface GMSiegeHallStartSiegeEvent extends EventTypeData {
    originatorId: number
    hallId: number
}

export interface GMSiegeHallAddAttackerEvent extends EventTypeData {
    originatorId: number
    hallId: number
    attackerClanId: number
}

export interface GMSiegeHallRemoveAttackerEvent extends EventTypeData {
    originatorId: number
    hallId: number
    attackerClanId: number
}

export interface GMSiegeHallClearAttackersEvent extends EventTypeData {
    originatorId: number
    hallId: number
}

export interface GMSiegeHallForwardSiegeEvent extends EventTypeData {
    originatorId: number
    hallId: number
}

export interface GMClanChangeLeaderEvent extends EventTypeData {
    originatorId: number
    leaderId: number
    clanId: number
}

export interface GMCreateItemEvent extends EventTypeData {
    originatorId: number
    amount: number
    itemId: number
    receiverId: number
}

export interface GMCreateDroppedItemEvent extends EventTypeData {
    x: number
    y: number
    z: number
    originatorId: number
    amount: number
    itemId: number
    instanceType: number
}

export interface GMGiftItemsEvent extends EventTypeData {
    originatorId: number
    amount: number
    itemId: number
    receiverIds: Array<number>
}

export interface GMAddLevelEvent extends EventTypeData {
    originatorId: number
    targetId: number
    amount: number
    previousLevel: number
}

export interface GMSetLevelEvent extends EventTypeData {
    originatorId: number
    targetId: number
    amount: number
    previousLevel: number
}

export interface GMHealPlayersEvent extends EventTypeData {
    originatorId: number
    targetIds: Array<number>
}

export interface GMSetHpEvent extends EventTypeData {
    originatorId: number
    targetId: number
    hp: number
}

export interface GMSetMpEvent extends EventTypeData {
    originatorId: number
    targetId: number
    mp: number
}

export interface GMReduceHpEvent extends EventTypeData {
    originatorId: number
    targetIds: Array<number>
    amount: number
}

export interface NpcAttemptsDyingEvent extends EventTypeData {
    npcId: number
    objectId: number
    currentHp: number
    reduceHpAmount: number
}

export interface PlayerDieEvent extends EventTypeData {
    objectId: number
    currentHp: number
    reduceHpAmount: number
}

export interface GMKillTargetsEvent extends EventTypeData {
    originatorId: number
    targetIds: Array<number>
}

export interface PlayerPartyRequestEvent extends EventTypeData {
    requesterId: number
    targetId: number
    partyType: number
}

export interface GMSpawnNpcEvent extends EventTypeData {
    originatorId: number
    npcId: number
    amount: number
    isStored: boolean
    templateType: number
    locationX: number
    locationY: number
    locationZ: number
}

export interface PlayerAdminCommandEvent extends EventTypeData {
    playerId: number
    command: string
    isExecuted: boolean
}

export interface PlayerCreatedEvent extends EventTypeData {
    playerId: number
    playerName: string
    classId: number
    race: number
}

export interface PlayerDeletedEvent extends EventTypeData {
    playerId: number
    playerName: string
    classId: number
    level: number
    sex: number
    accessLevel: number
    slot: number
}

export interface PlayerRestoredEvent extends EventTypeData {
    playerId: number
    playerName: string
    classId: number
    level: number
    sex: number
    accessLevel: number
}

export interface PlayerKilledEvent extends EventTypeData {
    attackerId: number
    targetId: number
    attackerInstanceType: number
    instanceId: number
    targetLevel: number
    attackerLevel: number
    targetClassId: number
}

export interface DoorInteractionEvent extends EventTypeData {
    playerId: number
    doorId: number
    doorTemplateId: number
    instanceId: number
    isOpen: boolean
}

export interface NpcBypassEvent extends EventTypeData {
    playerId: number
    npcId: number
    originatorId: number
    command: string
    instanceType: number
    isBusy: boolean
}

export interface CharacterStoppedMovingEvent extends EventTypeData {
    characterId: number
    npcId: number
    x: number
    y: number
    z: number
    instanceId: number
}

export interface PlayerStoppedMovingEvent extends EventTypeData {
    playerId: number
    x: number
    y: number
    z: number
    instanceId: number
}

export interface OlympiadMatchResultEvent extends EventTypeData {
    playerIds: Array<number>
    type: number
    winnerPlayerId: number
}

// To be used to provide text results about each listener (aka Seven Sign progress, Activities going on, etc.)
export interface StatusCheckRequestedEvent extends EventTypeData {
    playerId: number
    instanceId: number
}

export interface ItemAuctionStartEvent extends EventTypeData {
    auctionId: number
    itemId: number
    npcId: number
    itemAmount: number
    startTime: number
    startBidAmount: number
}

export interface ItemAuctionEndEvent extends EventTypeData {
    auctionId: number
    itemId: number
    npcId: number
    itemAmount: number
    endTime: number
    winnerPlayerId: number
    winnerBidAmount: number
    totalBidCount: number
}

export interface ItemAuctionBidEvent extends EventTypeData {
    auctionId: number
    playerId: number
    bidAmount: number
}

export interface PlayerReceivesTradeItemEvent extends EventTypeData {
    receiverId: number
    senderId: number
    itemId: number
    itemAmount: number
    type: string
}

export interface GMChangeClassIdEvent extends EventTypeData {
    receiverId: number
    receiverPreviousClassId: number
    receiverNextClassId: number
    originatorId: number
}

export interface GeoRegionActivatedEvent extends EventTypeData {
    x: number
    y: number
    code: number
}

export interface WorldRegionDeactivatedEvent extends EventTypeData {
    x: number
    y: number
}

export interface ItemEndOfLifeEvent extends EventTypeData {
    itemId: number
    objectId: number
    playerId: number
}

export interface PlayerPrivateChatEvent extends EventTypeData {
    message: string
    receiverId: number
    senderId: number
    receiverName: string
}

export interface VoiceCommandEvent extends EventTypeData {
    playerId: number
    command: string
    parameters: string
}

export interface PlayerSummonTalkEvent extends EventTypeData {
    playerId: number
    targetId: number
    shouldInteract: boolean
}

export interface BaseViolationProperties extends EventTypeData {
    message: string
    errorId: string
}

export interface BaseViolationEvent extends BaseViolationProperties {
    playerId: number
}

export interface TradeViolationEvent extends BaseViolationProperties {
    sellerPlayerId: number
    buyerPlayerId: number
    storeType: PrivateStoreType
    objectIds: Array<number>
}

export interface GeneralViolationEvent extends BaseViolationEvent {}

export interface BuySellViolationEvent extends BaseViolationEvent {
    ids: Array<number>
}

export interface ItemViolationEvent extends BaseViolationEvent {
    itemId: number
    itemAmount: number
    itemObjectId: number
}

export interface PacketDataViolationEvent extends BaseViolationEvent {
    values: Array<number>
    packetName: string
}

export interface SkillViolationEvent extends EventTypeData {
    skillId: number
    skillLevel: number
    playerId: number
    message: string
    errorId: string
}

export interface SummonViolationEvent extends BaseViolationEvent {
    summonObjectId: number
    summonNpcId: number
}

export interface ItemAmountChangeEvent extends EventTypeData {
    itemId: number
    oldAmount: number
    newAmount: number
    objectId: number
}

export interface PlayerAccessLevelChangedEvent extends EventTypeData {
    playerId: number
    oldLevel: number
    newLevel: number
    accountName: string
    reason: string
}

export interface NpcHasTypeItemsEvent extends EventTypeData {
    npcId: number
    npcObjectId: number
    itemIds: Array<number>
}

export interface NpcHasSweepItemsEvent extends NpcHasTypeItemsEvent {}
export interface NpcHasDropItemsEvent extends NpcHasTypeItemsEvent {}

export interface GMSetVitalityPointsEvent extends EventTypeData {
    originatorId: number
    targetId: number
    previousPoints: number
    nextPoints: number
    nextLevel: number
}

export interface PlayerVitalityChangedEvent extends EventTypeData {
    playerId: number
    previousPoints: number
    nextPoints: number
}

export interface PlayableKilledEvent extends CharacterKilledEvent {
    isPlayer: boolean
}

export interface ClanChangeLevelEvent extends EventTypeData {
    clanId: number
    previousLevel: number
    nextLevel: number
}

export const enum DataIntegrityViolationType {
    Skill,
    Item,
    PlayerChat,
    Craft,
    ItemEnchant,
    Recipe
}
export interface DataIntegrityViolationEvent extends GeneralViolationEvent {
    type: DataIntegrityViolationType
    ids: Array<number>
}

export interface MailViolationEvent extends GeneralViolationEvent {
    messageId: number
    receiverId: number
    senderId: number
}

export interface TWPlayerLoginEvent extends EventTypeData {
    territoryId: number
    playerId: number
    classId: number
}

export interface TWPlayerDeathEvent extends EventTypeData {
    partyPlayerIds: Array<number>
    territoryId: number
    attackerId: number
    targetId: number
    targetClassId: number
}

export interface GameTimeHourEvent extends EventTypeData {
    isNight: boolean
    gameHour: number
}

export interface CycleStepChangeEvent extends EventTypeData {
    previousStep: number
    nextStep: number
    type: number
}

export interface DimensionalItemEvent extends EventTypeData {
    playerId: number
    itemId: number
}

export interface DimensionalItemAddEvent extends DimensionalItemEvent {
    amount: number
}

export interface DimensionalItemUpdateEvent extends DimensionalItemEvent {
    oldAmount: number
    newAmount: number
}

export interface DimensionalItemRemoveEvent extends DimensionalItemEvent {}

export interface GMDimensionalItemAddEvent extends DimensionalItemAddEvent {
    gmPlayerId: number
}

export interface GMDimensionalItemRemoveEvent extends DimensionalItemEvent {
    amount: number
    gmPlayerId: number
}

export interface NpcTeachesSkillsEvent extends EventTypeData {
    playerId: number
    playerLevel: number
    npcId: number
    npcObjectId: number
}

export interface SevenSignsFestivalRewardEvent extends EventTypeData {
    side: SevenSignsSide
    level: SevenSignsFestivalLevel
    objectIds: Array<number>
    score: number
}

export interface PlayerUnEquipsItemEvent extends PlayerEquipItemEvent {
    slot: number
}

export interface PlayerBuyGPProduct extends EventTypeData {
    playerId: number
    productId: number
    amount: number
}

export interface PlayerConsumeCraftMPEvent extends EventTypeData {
    playerId: number
    customerId: number
    recipeId: number
}