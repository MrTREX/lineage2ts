import { L2PcInstance } from './actor/instance/L2PcInstance'
import { L2World } from '../L2World'
import { SystemMessageBuilder } from '../packets/send/SystemMessage'
import { SystemMessageIds } from '../packets/SystemMessageIdValues'

export class L2Request {
    playerId: number
    partnerId: number
    isRequester: boolean = false
    isResponder: boolean = false
    requestData: L2RequestData

    constructor( player: L2PcInstance ) {
        this.playerId = player.getObjectId()
    }

    clear() {
        this.partnerId = 0
        this.isRequester = false
        this.isResponder = false
        this.requestData = null
    }

    setPartner( player: L2PcInstance ) {
        this.partnerId = player.getObjectId()
    }

    getPartner() : L2PcInstance {
        return L2World.getPlayer( this.partnerId )
    }

    getPlayer() : L2PcInstance {
        return L2World.getPlayer( this.playerId )
    }

    setRequest( partner: L2PcInstance, data: L2RequestData ) {
        let player = this.getPlayer()

        if ( !partner ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_HAVE_INVITED_THE_WRONG_TARGET ) )
            return false
        }

        if ( partner.getRequest().isProcessingRequest() ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.C1_IS_BUSY_TRY_LATER )
                    .addString( partner.getName() )
                    .getBuffer()
            player.sendOwnedData( packet )
            return false
        }
        if ( this.isProcessingRequest() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.WAITING_FOR_ANOTHER_REPLY ) )
            return false
        }

        this.partnerId = partner.getObjectId()
        this.requestData = data
        this.setOnRequestTimer( true )

        partner.getRequest().setPartner( player )
        partner.getRequest().setOnRequestTimer( false )

        return true
    }


    setOnRequestTimer( isRequester: boolean ) {
        this.isRequester = isRequester
        this.isResponder = !isRequester

        setTimeout( this.clear.bind( this ), 15000 )
    }

    onRequestResponse() {
        let partner = this.getPartner()
        if ( !partner ) {
            partner.getRequest().clear()
        }

        this.clear()
    }

    isProcessingRequest() {
        return this.partnerId !== 0
    }

    getRequestData() {
        return this.requestData
    }
}

export interface L2RequestData {
    type: Function
    parameters: any
}