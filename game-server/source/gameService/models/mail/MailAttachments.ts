import { ItemContainer } from '../itemcontainer/ItemContainer'
import { L2ItemInstance } from '../items/instance/L2ItemInstance'
import { ItemLocation } from '../../enums/ItemLocation'
import aigle from 'aigle'
import { ItemManagerCache } from '../../cache/ItemManagerCache'

/**
 * we don't need to create actual items and do various checks on them since attachments are consumed
 * by players when they receive message, hence we can delay usage of object id generation till later time
 * so things like item deletion or even movement of item to warehouse can be done in much easier
 * and resource free way
 * Only reason to keep object ids of items is for traceability reasons
 *
 * Need to rewrite whole bunch of ItemContainer methods to ensure that inventory actions are applicable, or
 * possibly not derive from ItemContainer in first place.
 */
export class MailAttachments extends ItemContainer {
    messageId: number

    constructor( ownerId: number, messageId: number ) {
        super()
        this.ownerId = ownerId
        this.messageId = messageId
    }

    async deleteMe() : Promise<void> {
        this.getItems().forEach( ( item: L2ItemInstance ) => {
            item.resetProperties()
        } )

        this.resetInventory()
    }

    getBaseLocation(): ItemLocation {
        return ItemLocation.MAIL
    }

    async returnToWarehouse( warehouse: ItemContainer ) : Promise<void> {
        let mail = this
        await aigle.resolve( this.getItems() ).eachLimit( 10, ( item: L2ItemInstance ) : Promise<any> => {
            if ( !item ) {
                return
            }

            if ( !warehouse ) {
                return item.setItemLocationForNewOwner( ItemLocation.WAREHOUSE )
            }

            return mail.transferItem( item.getObjectId(), item.getCount(), warehouse, 0, 'MailAttachments.returnToWarehouse' )
        } )
    }

    setNewMessageId( messageId: number ) : void {
        this.messageId = messageId

        let location = this.getBaseLocation()
        for ( const item of this.getItems() ) {
            item.setItemLocationProperties( location, messageId )
        }
    }
}