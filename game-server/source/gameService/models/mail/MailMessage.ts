import { MailAttachments } from './MailAttachments'
import { MailManager } from '../../instancemanager/MailManager'
import { SendBySystem } from './SendBySystem'
import { L2MessagesTableItem, L2MessagesTableItemStatus } from '../../../database/interface/MessagesTableApi'

export class MailMessage {
    attachments: MailAttachments
    data: L2MessagesTableItem

    constructor( item: L2MessagesTableItem ) {
        this.data = item
        this.data.expirationTime = this.getExpirationTime( this.isCOD() )
    }

    static asReturned( message: MailMessage ): MailMessage {
        let copyData = structuredClone( message.data )
        let returnedMessage = new MailMessage( copyData )

        returnedMessage.data.senderId = 0
        returnedMessage.data.receiverId = message.data.senderId
        returnedMessage.data.isReturned = true
        returnedMessage.data.status = L2MessagesTableItemStatus.Unread

        if ( message.hasAttachments() ) {
            message.attachments = null
            message.data.hasAttachments = false

            returnedMessage.data.hasAttachments = true
            returnedMessage.attachments = message.getLoadedAttachments()

            returnedMessage.attachments.setNewMessageId( returnedMessage.getId() )
        }

        return returnedMessage
    }

    static fromValues( senderId: number, receiverId: number, subject: string, content: string, codAdena: number ): MailMessage {
        return new MailMessage( {
            codAdena: Math.max( 0, codAdena ),
            content,
            expirationTime: 0,
            hasAttachments: false,
            isReturned: false,
            messageId: MailManager.getNextId(),
            receiverId,
            senderId,
            sentBySystem: SendBySystem.Player,
            status: L2MessagesTableItemStatus.Unread,
            subject,
            creationTime: Date.now()
        } )
    }

    static asSystemMessage( receiverId: number, subject: string, content: string, sender: SendBySystem ) : MailMessage {
        return new MailMessage( {
            codAdena: 0,
            content,
            expirationTime: 0,
            hasAttachments: false,
            isReturned: false,
            messageId: MailManager.getNextId(),
            receiverId,
            senderId: -1,
            sentBySystem: sender,
            status: L2MessagesTableItemStatus.Unread,
            subject,
            creationTime: Date.now()
        } )
    }

    private getExpirationTime( isCod : boolean ) : number {
        return Date.now() + ( isCod ? MailManager.getCodExpirationTime() : MailManager.getDefaultExpirationTime() )
    }

    createAttachments(): MailAttachments {
        if ( this.data.hasAttachments || this.attachments ) {
            return null
        }

        this.attachments = new MailAttachments( this.data.senderId, this.data.messageId )
        this.data.hasAttachments = true

        return this.attachments
    }

    loadAttachments() : Promise<void> {
        if ( this.attachments ) {
            return
        }

        this.attachments = new MailAttachments( this.data.senderId, this.data.messageId )
        return this.attachments.restore()
    }

    getContent() {
        return this.data.content
    }

    getExpirationSeconds() {
        return Math.floor( this.data.expirationTime / 1000 )
    }

    getId() {
        return this.data.messageId
    }

    getReceiverId() {
        return this.data.receiverId
    }

    getCODAdena() {
        return this.data.codAdena
    }

    getSendBySystem() {
        return this.data.sentBySystem
    }

    getSenderId() {
        return this.data.senderId
    }

    getSubject() {
        return this.data.subject
    }

    hasAttachments() {
        return this.data.hasAttachments
    }

    getLoadedAttachments() : MailAttachments {
        return this.attachments
    }

    isCOD() {
        return this.data.codAdena > 0
    }

    isReturned() {
        return this.data.isReturned
    }

    getStatus() : L2MessagesTableItemStatus {
        return this.data.status
    }
}