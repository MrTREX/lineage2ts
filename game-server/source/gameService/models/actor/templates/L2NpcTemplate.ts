import { AIType } from '../../../enums/AIType'
import { L2CharacterSkillMap, L2CharacterTemplate } from './L2CharacterTemplate'
import { Skill } from '../../Skill'
import { IDropItem } from '../../drops/IDropItem'
import { ClassId, IClassId } from '../../base/ClassId'
import { DataManager } from '../../../../data/manager'
import { AbstractFunction } from '../../stats/functions/AbstractFunction'
import _ from 'lodash'
import { AISkillUsage } from '../../../enums/AISkillUsage'
import { NpcTemplateType } from '../../../enums/NpcTemplateType'
import { GeometryId } from '../../../enums/GeometryId'
import { SkillItem } from '../../skills/SkillItem'
import { DropListScope } from '../../../enums/drops/DropListScope'
import { L2NpcMinion, L2NpcScriptedMinion } from './L2NpcMinion'

const npcWithSupportStance = new Set<number>( [
    16043,
    16044,
    16045,
    16046,
] )

const EmptyDrops : ReadonlyArray<IDropItem> = []

export interface AISkillData extends SkillItem {}

export class L2NpcTemplate extends L2CharacterTemplate {
    id: number
    displayId: number
    infoId: number
    level: number
    type: NpcTemplateType
    name: string
    usingServerSideName: boolean
    title: string
    usingServerSideTitle: boolean
    chestId: number
    rhandId: number
    lhandId: number
    weaponEnchant: number
    expRate: number
    sp: number
    raidPoints: number
    unique: boolean
    attackable: boolean
    targetable: boolean
    undying: boolean
    showName: boolean
    flying: boolean
    canMoveValue: boolean
    noSleepMode: boolean
    passableDoor: boolean
    hasSummoner: boolean
    canBeSownValue: boolean
    corpseTime: number
    aiType: AIType = AIType.FIGHTER
    aggroRange: number
    clanHelpRange: number
    isChaosValue: boolean
    isAggressive: boolean
    soulShot: number
    spiritShot: number
    soulShotChance: number
    spiritShotChance: number
    maxSkillChance: number
    skills: Array<Skill>
    clans: Array<number> = []
    ignoreClanNpcIds: Array<number> = []
    dropLists: { [ key: number ]: Array<IDropItem> } = {}
    collisionRadiusGrown: number
    collisionHeightGrown: number
    parameters: { [ key: string ]: any }
    skillParameters: { [ name: string ]: AISkillData }
    minions: Array<L2NpcMinion> = []
    scriptedMinions: Array<L2NpcScriptedMinion> = []
    teachableClassIds: Array<number> = []
    canGreetPlayerValue: boolean
    aggroInterval: number
    skillMap: L2CharacterSkillMap
    passiveSkills: Array<Skill> = []
    vitalityPointsPerHp: number
    expReward: number
    aiSkillUsage: AISkillUsage
    customTitle: string
    moveGeometryId: GeometryId

    canBeSown() {
        return this.canBeSownValue
    }

    canMove() {
        return this.canMoveValue
    }

    getAIType() {
        return this.aiType
    }

    getAggroRange() {
        return this.aggroRange
    }

    getChest() {
        return this.chestId
    }

    getCollisionRadiusGrown() {
        return this.collisionRadiusGrown
    }

    getCorpseTime() {
        return this.corpseTime
    }

    getDisplayId() {
        return this.displayId
    }

    getExpReward() {
        return this.expReward
    }

    getId() {
        return this.id
    }

    getLHandId() {
        return this.lhandId
    }

    getLevel() {
        return this.level
    }

    getName(): string {
        return this.name
    }

    getRHandId() {
        return this.rhandId
    }

    getSP() {
        return this.sp
    }

    getSkills(): Array<Skill> {
        return this.skills
    }

    getSkillMap(): L2CharacterSkillMap {
        return this.skillMap
    }

    getPassiveSkills(): Array<Skill> {
        return this.passiveSkills
    }

    getSoulShot() {
        return this.soulShot
    }

    getSoulshotChance() {
        return this.soulShotChance
    }

    getSpiritShot() {
        return this.spiritShot
    }

    getSpiritShotChance() {
        return this.spiritShotChance
    }

    getTitle() {
        return this.title
    }

    getType() {
        return this.type
    }

    getWeaponEnchant() {
        return this.weaponEnchant
    }

    isShowName() {
        return this.showName
    }

    isTargetable() {
        return this.targetable
    }

    isUndying() {
        return this.undying
    }

    isUsingServerSideName(): boolean {
        return this.usingServerSideName
    }

    isUsingServerSideTitle(): boolean {
        return this.usingServerSideTitle
    }

    getParameters(): { [ key: string ]: string | number } {
        return this.parameters
    }

    getClanHelpRange() {
        return this.clanHelpRange
    }

    canTeach( classId: IClassId ): boolean {
        if ( classId.level === 3 ) {
            return this.teachableClassIds.includes( ClassId.getClassId( classId.parent ).id )
        }

        return this.teachableClassIds.includes( classId.id )
    }

    getTeachableClassIds(): Array<number> {
        return this.teachableClassIds
    }

    getClans(): Array<number> {
        return this.clans
    }

    getIgnoreClanNpcIds() {
        return this.ignoreClanNpcIds
    }

    isClan( clans: Array<number> ) {
        if ( _.isEmpty( this.clans ) || _.isEmpty( clans ) ) {
            return false
        }

        if ( this.clans.includes( DataManager.getNpcData().getClanId( 'ALL' ) ) ) {
            return true
        }

        return _.intersection( this.clans, clans ).length > 0
    }

    isFlying() {
        return this.flying
    }

    isChaos() {
        return this.isChaosValue
    }

    getDrops( scope: DropListScope ) : ReadonlyArray<IDropItem> {
        return this.dropLists[ scope ] ?? EmptyDrops
    }

    getMaxSkillChance() {
        return this.maxSkillChance
    }

    isSevenSignsNpc(): boolean {
        return _.includes( this.clans, DataManager.getNpcData().getClanId( 'C_DUNGEON' ) )
                && !_.includes( this.clans, DataManager.getNpcData().getClanId( 'ALL' ) )
    }

    canGreetPlayer(): boolean {
        return this.canGreetPlayerValue
    }

    getAggroInterval(): number {
        return this.aggroInterval
    }

    hasInitialSkillStance(): boolean {
        return npcWithSupportStance.has( this.id )
    }

    getSkillParameters() {
        return this.skillParameters
    }

    getVitalityPointsPerHp() : number {
        return this.vitalityPointsPerHp
    }

    getCustomTitle() : string {
        return this.customTitle
    }

    setCustomTitle( title: string ) : void {
        this.customTitle = title
    }

    getMinions() : ReadonlyArray<L2NpcMinion> {
        return this.minions
    }

    getScriptedMinions() : ReadonlyArray<L2NpcScriptedMinion> {
        return this.scriptedMinions
    }
}