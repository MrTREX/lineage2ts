export interface L2NpcMinion {
    amount: number
    npcId: number
    respawnMs: number
}

export interface L2NpcScriptedMinion {
    name: string
    minions: Array<L2NpcMinion>
}