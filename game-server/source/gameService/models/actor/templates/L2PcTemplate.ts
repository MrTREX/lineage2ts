import { L2CharacterTemplate } from './L2CharacterTemplate'
import { GeometryId } from '../../../enums/GeometryId'

export class L2PcTemplate extends L2CharacterTemplate {
    classId: number
    baseHp: Array<number>
    baseMp: Array<number>
    baseCp: Array<number>

    baseHpRegeneration: Array<number>
    baseMpRegeneration: Array<number>
    baseCpRegeneration: Array<number>

    fCollisionHeightFemale: number
    fCollisionRadiusFemale: number

    baseSafeFallHeight: number
    baseSlotDef: { [key: number]: number } = {}
    dropGeometryId: GeometryId = GeometryId.PlayerDrop

    getBaseCpMaxByLevel( level: number ) {
        return this.baseCp[ level - 1 ]
    }

    getBaseCpRegeneration( level: number ) {
        return this.baseCpRegeneration[ level - 1 ]
    }

    getBaseDefBySlot( slot: number ) {
        return this.baseSlotDef[ slot ] || 0
    }

    getBaseHpMaxByLevel( level: number ): number {
        return this.baseHp[ level - 1 ]
    }

    getBaseHpRegeneration( level: number ) {
        return this.baseHpRegeneration[ level - 1 ]
    }

    getBaseMpMaxByLevel( level: number ): number {
        return this.baseMp[ level - 1 ]
    }

    getBaseMpRegeneration( level: number ) {
        return this.baseMpRegeneration[ level - 1 ]
    }

    getClassId(): number {
        return this.classId
    }

    getRace() {
        return this.race
    }

    getSafeFallHeight() {
        return this.baseSafeFallHeight
    }
}