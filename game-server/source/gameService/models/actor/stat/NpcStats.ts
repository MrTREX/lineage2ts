import { CharacterStats } from './CharacterStats'
import { L2Npc } from '../L2Npc'
import { StatsCache } from '../../../cache/StatsCache'

export class NpcStats extends CharacterStats {
    getLevel(): number {
        return this.getActiveCharacter().getTemplate().getLevel()
    }

    getActiveCharacter(): L2Npc {
        return this.character as L2Npc
    }
}