import { MoveSpeedStat, Stats } from '../../stats/Stats'
import { L2Character } from '../L2Character'
import { L2PcInstance } from '../instance/L2PcInstance'
import { ConfigManager } from '../../../../config/ConfigManager'
import { Skill } from '../../Skill'
import { L2Weapon } from '../../items/L2Weapon'
import { TraitType, TraitTypeValues } from '../../stats/TraitType'
import { L2ItemInstance } from '../../items/instance/L2ItemInstance'
import { MoveType } from '../../stats/MoveType'
import { PlayerActionOverride } from '../../../values/PlayerConditions'
import _ from 'lodash'
import { StatsCache } from '../../../cache/StatsCache'
import { AbstractFunction } from '../../stats/functions/AbstractFunction'
import { AreaType } from '../../areas/AreaType'
import { ElementalType } from '../../../enums/ElementalType'

const nonNegativeStats = new Set<string>( [
    Stats.MAX_HP,
    Stats.MAX_MP,
    Stats.MAX_CP,
    Stats.MAGIC_DEFENCE,
    Stats.POWER_DEFENCE,
    Stats.POWER_ATTACK,
    Stats.MAGIC_ATTACK,
    Stats.POWER_ATTACK_SPEED,
    Stats.MAGIC_ATTACK_SPEED,
    Stats.SHIELD_DEFENCE,
    Stats.STAT_CON,
    Stats.STAT_DEX,
    Stats.STAT_INT,
    Stats.STAT_MEN,
    Stats.STAT_STR,
    Stats.STAT_WIT,
] )

export class CharacterStats {
    character: L2Character
    level: number = 1
    attackTraits: Array<number>
    attackTraitsCount: Array<number>
    defenceTraits: Array<number>
    defenceTraitsCount: Array<number>
    traitsInvulnerable: Array<number>

    constructor( character: L2Character ) {
        this.character = character

        this.resetTraits()
    }

    resetTraits(): void {
        this.attackTraits = new Array<number>( _.size( TraitTypeValues ) ).fill( 1 )
        this.attackTraitsCount = new Array<number>( _.size( TraitTypeValues ) ).fill( 0 )
        this.defenceTraits = new Array<number>( _.size( TraitTypeValues ) ).fill( 1 )
        this.defenceTraitsCount = new Array<number>( _.size( TraitTypeValues ) ).fill( 0 )
        this.traitsInvulnerable = new Array<number>( _.size( TraitTypeValues ) ).fill( 0 )
    }

    getActiveCharacter(): L2Character {
        return this.character
    }

    getActiveCharacterId(): number {
        return this.character.getObjectId()
    }

    calculateStat( statName: string,
            startingValue: number,
            targetCharacter: L2Character = null,
            appliedSkill: Skill = null,
            subStatName: string = null ): number {

        if ( !statName ) {
            return startingValue
        }

        let calculators = this.character.calculators.getFunctions( statName )
        if ( !calculators ) {
            return startingValue
        }

        let initialValue = startingValue

        if ( this.character.isPlayer() && this.character.isTransformed() ) {
            let statValue = this.character.getTransformation().getStat( this.character as L2PcInstance, statName )

            if ( statValue > 0 ) {
                initialValue = statValue
            }
        }

        let shouldUseCache: boolean = !targetCharacter && !appliedSkill
        if ( shouldUseCache ) {
            let cachedValue = StatsCache.getStat( this.getActiveCharacterId(), subStatName ?? statName, this.character.calculators.getUpdateTime( statName ) )
            if ( cachedValue ) {
                return cachedValue
            }
        }

        let currentCharacter = this.character
        let outcome: number = calculators.reduce( ( lastOutcome: number, currentFunction: AbstractFunction ) => {
            return currentFunction.calculate( currentCharacter, targetCharacter, appliedSkill, lastOutcome )
        }, initialValue )

        if ( outcome <= 0 && nonNegativeStats.has( statName ) ) {
            outcome = 1
        }

        if ( shouldUseCache ) {
            StatsCache.setStat( this.character.getObjectId(), subStatName ?? statName, Math.floor( outcome ), this.character.calculators.getUpdateTime( statName ) )
        }

        return outcome
    }

    getSTR(): number {
        return this.calculateStat( Stats.STAT_STR, this.getActiveCharacter().getTemplate().baseSTR )
    }

    getWIT(): number {
        return this.calculateStat( Stats.STAT_WIT, this.getActiveCharacter().getTemplate().baseWIT )
    }

    getCON(): number {
        return this.calculateStat( Stats.STAT_CON, this.getActiveCharacter().getTemplate().baseCON )
    }

    getINT(): number {
        return this.calculateStat( Stats.STAT_INT, this.getActiveCharacter().getTemplate().baseINT )
    }

    getDEX(): number {
        return this.calculateStat( Stats.STAT_DEX, this.getActiveCharacter().getTemplate().baseDEX )
    }

    getMEN(): number {
        return this.calculateStat( Stats.STAT_MEN, this.getActiveCharacter().getTemplate().baseMEN )
    }

    getShieldDefence(): number {
        return this.calculateStat( Stats.SHIELD_DEFENCE, 0, this.getActiveCharacter() )
    }

    /*
        Note that there are no items that use 'atkReuse' stat, hence whole call is pointless and can
        be reduced to returning digit 1.
     */
    getWeaponReuseModifier( target: L2Character ): number {
        return this.calculateStat( Stats.ATK_REUSE, 1, target, null )
    }

    getMaxBuffCount(): number {
        return this.calculateStat( Stats.ENLARGE_ABNORMAL_SLOT, ConfigManager.character.getMaxBuffAmount() )
    }

    getPhysicalAttackRange(): number {
        let character = this.getActiveCharacter()
        let currentWeapon: L2Weapon = character.getActiveWeaponItem()
        let baseAttackRange: number

        if ( character.isTransformed() && character.isPlayer() ) {
            baseAttackRange = character.getTransformation().getBaseAttackRange( character.getActingPlayer() )
        } else if ( currentWeapon ) {
            baseAttackRange = currentWeapon.getBaseAttackRange()
        } else {
            baseAttackRange = character.getTemplate().getBaseAttackRange()
        }

        return this.calculateStat( Stats.POWER_ATTACK_RANGE, baseAttackRange )
    }

    getMaxHp(): number {
        return Math.floor( this.calculateStat( Stats.MAX_HP, this.getActiveCharacter().getTemplate().baseHpMax ) )
    }

    getMaxCp(): number {
        return Math.floor( this.calculateStat( Stats.MAX_CP, this.getActiveCharacter().getTemplate().baseCpMax ) )
    }

    getMaxMp(): number {
        return Math.floor( this.calculateStat( Stats.MAX_MP, this.getActiveCharacter().getTemplate().baseMpMax ) )
    }

    getLevel(): number {
        return this.level
    }

    setLevel( desiredLevel: number ) {
        this.level = desiredLevel
    }

    isTraitInvulnerable( trait: TraitType ) {
        return this.traitsInvulnerable[ trait.id ] > 0
    }

    hasAttackTrait( trait: TraitType ) {
        return this.attackTraitsCount[ trait.id ] > 0
    }

    hasDefenceTrait( trait: TraitType ) {
        return this.defenceTraitsCount[ trait.id ] > 0
    }

    getAttackTrait( trait: TraitType ) {
        return this.attackTraits[ trait.id ]
    }

    getDefenceTrait( trait: TraitType ) {
        return this.defenceTraits[ trait.id ]
    }

    getAttackElement() {
        let character = this.getActiveCharacter()
        let weapon: L2ItemInstance = character.getActiveWeaponInstance()

        if ( weapon && weapon.getAttackElementType() >= 0 ) {
            return weapon.getAttackElementType()
        }

        let stats = [
            [ Stats.FIRE_POWER, character.getTemplate().getBaseFire() ],
            [ Stats.WATER_POWER, character.getTemplate().getBaseWater() ],
            [ Stats.WIND_POWER, character.getTemplate().getBaseWind() ],
            [ Stats.EARTH_POWER, character.getTemplate().getBaseEarth() ],
            [ Stats.HOLY_POWER, character.getTemplate().getBaseHoly() ],
            [ Stats.DARK_POWER, character.getTemplate().getBaseDark() ],
        ]

        let calculateStat: Function = this.calculateStat.bind( this )
        let startValue = 0
        return _.reduce( stats, ( outcome, parameters: Array<number>, index ) => {
            let currentValue = calculateStat( parameters[ 0 ], parameters[ 1 ] )
            if ( currentValue > startValue ) {
                outcome = index
                startValue = currentValue
            }

            return outcome
        }, -2 )
    }

    getAttackElementValue( value: ElementalType ): number {
        let character = this.getActiveCharacter()
        switch ( value ) {
            case ElementalType.FIRE:
                return this.calculateStat( Stats.FIRE_POWER, character.getTemplate().getBaseFire() )
            case ElementalType.WATER:
                return this.calculateStat( Stats.WATER_POWER, character.getTemplate().getBaseWater() )
            case ElementalType.WIND:
                return this.calculateStat( Stats.WIND_POWER, character.getTemplate().getBaseWind() )
            case ElementalType.EARTH:
                return this.calculateStat( Stats.EARTH_POWER, character.getTemplate().getBaseEarth() )
            case ElementalType.HOLY:
                return this.calculateStat( Stats.HOLY_POWER, character.getTemplate().getBaseHoly() )
            case ElementalType.DARK:
                return this.calculateStat( Stats.DARK_POWER, character.getTemplate().getBaseDark() )
        }

        return 0
    }

    getDefenseElementValue( value: ElementalType ): number {
        let character = this.getActiveCharacter()
        switch ( value ) {
            case ElementalType.FIRE:
                return this.calculateStat( Stats.FIRE_RES, character.getTemplate().getBaseFireRes() )
            case ElementalType.WATER:
                return this.calculateStat( Stats.WATER_RES, character.getTemplate().getBaseWaterRes() )
            case ElementalType.WIND:
                return this.calculateStat( Stats.WIND_RES, character.getTemplate().getBaseWindRes() )
            case ElementalType.EARTH:
                return this.calculateStat( Stats.EARTH_RES, character.getTemplate().getBaseEarthRes() )
            case ElementalType.HOLY:
                return this.calculateStat( Stats.HOLY_RES, character.getTemplate().getBaseHolyRes() )
            case ElementalType.DARK:
                return this.calculateStat( Stats.DARK_RES, character.getTemplate().getBaseDarkRes() )
        }

        return character.getTemplate().getBaseElementRes()
    }

    getMoveSpeed(): number {
        let character = this.getActiveCharacter()
        if ( character.isInArea( AreaType.Water ) ) {
            return character.isRunning() ? this.getSwimRunSpeed() : this.getSwimWalkSpeed()
        }

        return character.isRunning() ? this.getRunSpeed() : this.getWalkSpeed()
    }

    getSwimRunSpeed(): number {
        let speed = this.getBaseMoveSpeed( MoveType.FAST_SWIM )
        if ( speed <= 0 ) {
            return 0
        }

        return this.calculateStat( Stats.MOVE_SPEED, speed, null, null, MoveSpeedStat.MoveSwimRunSpeed )
    }

    getSwimWalkSpeed(): number {
        let speed = this.getBaseMoveSpeed( MoveType.SLOW_SWIM )
        if ( speed <= 0 ) {
            return 0
        }

        return this.calculateStat( Stats.MOVE_SPEED, speed, null, null, MoveSpeedStat.MoveSwimWalkSpeed )
    }

    getBaseMoveSpeed( type: MoveType ): number {
        let character = this.getActiveCharacter()
        return character.getTemplate().getBaseMoveSpeed( type )
    }

    getRunSpeed(): number {
        let character = this.getActiveCharacter()
        let speed = character.isInArea( AreaType.Water ) ? this.getSwimRunSpeed() : this.getBaseMoveSpeed( MoveType.RUN )

        if ( speed <= 0 ) {
            return 0
        }

        return this.calculateStat( Stats.MOVE_SPEED, speed, null, null, MoveSpeedStat.MoveRunSpeed )
    }

    getWalkSpeed(): number {
        let character = this.getActiveCharacter()
        let speed = character.isInArea( AreaType.Water ) ? this.getSwimRunSpeed() : this.getBaseMoveSpeed( MoveType.WALK )

        if ( speed <= 0 ) {
            return 0
        }

        return this.calculateStat( Stats.MOVE_SPEED, speed, null, null, MoveSpeedStat.MoveWalkSpeed )
    }

    getPowerAttack( target: L2Character ): number {
        let bonus = 1
        let character = this.getActiveCharacter()
        if ( ConfigManager.customs.championEnable() && character.isChampion() ) {
            bonus = ConfigManager.customs.getChampionAttack()
        }

        if ( character.isRaid() ) {
            bonus = bonus * ConfigManager.npc.getRaidPowerAttackMultiplier()
        }

        return this.calculateStat( Stats.POWER_ATTACK, character.getTemplate().getBasePowerAttack() * bonus, target )
    }

    getPowerAttackSpeed(): number {
        let bonus = 1
        let character = this.getActiveCharacter()
        if ( ConfigManager.customs.championEnable() && character.isChampion() ) {
            bonus = ConfigManager.customs.getChampionSpeedAttack()
        }

        return Math.round( this.calculateStat( Stats.POWER_ATTACK_SPEED, character.getTemplate().getBasePowerAttackSpeed() * bonus ) )
    }

    getPowerDefence( target: L2Character ): number {
        let bonus = 1
        let character = this.getActiveCharacter()
        if ( character.isRaid() ) {
            bonus = ConfigManager.npc.getRaidPowerDefenceMultiplier()
        }
        return this.calculateStat( Stats.POWER_DEFENCE, character.getTemplate().getBasePowerDefence() * bonus, target )
    }

    getMovementSpeedMultiplier(): number {
        let baseSpeed
        let character = this.getActiveCharacter()
        if ( character.isInArea( AreaType.Water ) ) {
            baseSpeed = this.getBaseMoveSpeed( character.isRunning() ? MoveType.FAST_SWIM : MoveType.SLOW_SWIM )
        } else {
            baseSpeed = this.getBaseMoveSpeed( character.isRunning() ? MoveType.RUN : MoveType.WALK )
        }

        return this.getMoveSpeed() * ( 1 / baseSpeed )
    }

    getMagicAttackSpeed(): number {
        let bonus = 1
        let character = this.getActiveCharacter()
        if ( ConfigManager.customs.championEnable() && character.isChampion() ) {
            bonus = ConfigManager.customs.getChampionSpeedAttack()
        }

        let outcome = this.calculateStat( Stats.MAGIC_ATTACK_SPEED, character.getTemplate().getBaseMagicAttackSpeed() * bonus )
        if ( !character.hasActionOverride( PlayerActionOverride.MaximumStat ) ) {
            outcome = Math.min( outcome, ConfigManager.character.getMaxMagicAttackSpeed() )
        }

        return outcome
    }

    getMagicAttack( target: L2Character, skill: Skill ): number {
        let bonus = 1
        let character = this.getActiveCharacter()
        if ( ConfigManager.customs.championEnable() && character.isChampion() ) {
            bonus = ConfigManager.customs.getChampionAttack()
        }

        if ( character.isRaid() ) {
            bonus = bonus * ConfigManager.npc.getRaidMagicAttackMultiplier()
        }

        return this.calculateStat( Stats.MAGIC_ATTACK, character.getTemplate().getBaseMagicAttack() * bonus, target, skill )
    }

    getMagicDefence( target: L2Character, skill: Skill ): number {
        let character = this.getActiveCharacter()
        let defence = character.getTemplate().getBaseMagicDefence()
        if ( character.isRaid() ) {
            defence = defence * ConfigManager.npc.getRaidMagicDefenceMultiplier()
        }

        return this.calculateStat( Stats.MAGIC_DEFENCE, defence, target, skill )
    }

    getAttackSpeedMultiplier() {
        return ( ( 1.1 ) * this.getPowerAttackSpeed() ) / this.getActiveCharacter().getTemplate().getBasePowerAttackSpeed()
    }

    getEvasionRate( target: L2Character ): number {
        let value = this.calculateStat( Stats.EVASION_RATE, 0, target, null )
        if ( !this.getActiveCharacter().hasActionOverride( PlayerActionOverride.MaximumStat ) ) {
            value = Math.min( value, ConfigManager.character.getMaxEvasion() )
        }

        return value
    }

    getAccuracy(): number {
        return Math.round( this.calculateStat( Stats.ACCURACY_COMBAT, 0, null, null ) )
    }

    getCriticalHit( target: L2Character, skill: Skill ) {
        let character = this.getActiveCharacter()
        let value = Math.floor( this.calculateStat( Stats.CRITICAL_RATE, character.getTemplate().getBaseCritRate(), target, skill ) )
        if ( !character.hasActionOverride( PlayerActionOverride.MaximumStat ) ) {
            value = Math.min( value, ConfigManager.character.getMaxPowerCritRate() )
        }

        return Math.floor( value + 0.5 )
    }

    getMaxRecoverableMp() {
        return Math.floor( this.calculateStat( Stats.MAX_RECOVERABLE_MP, this.getMaxMp() ) )
    }

    getMaxRecoverableHp() {
        return Math.floor( this.calculateStat( Stats.MAX_RECOVERABLE_HP, this.getMaxHp() ) )
    }

    getMaxRecoverableCp() {
        return Math.floor( this.calculateStat( Stats.MAX_RECOVERABLE_CP, this.getMaxCp() ) )
    }

    getDefenceTraits() {
        return this.defenceTraits
    }

    getMCriticalHit( target: L2Character, skill: Skill ): number {
        let value = this.calculateStat( Stats.MCRITICAL_RATE, 1, target, skill ) * 10
        if ( !this.getActiveCharacter().hasActionOverride( PlayerActionOverride.MaximumStat ) ) {
            value = Math.min( value, ConfigManager.character.getMaxMagicCritRate() )
        }

        return value
    }

    getMpConsume1( skill: Skill ): number {
        if ( !skill ) {
            return 1
        }

        return this.calculateStat( Stats.MP_CONSUME, skill.getStartCostMp(), null, skill )
    }

    getMpConsume2( skill: Skill ): number {
        if ( !skill ) {
            return 1
        }

        let value = skill.getFinishCostMp()
        let nextDanceMpCost = Math.ceil( skill.getFinishCostMp() / 2 )
        if ( skill.isDance() ) {
            let character = this.getActiveCharacter()
            if ( ConfigManager.character.danceConsumeAdditionalMP() && character && ( character.getDanceCount() > 0 ) ) {
                value += character.getDanceCount() * nextDanceMpCost
            }
        }

        value = this.calculateStat( Stats.MP_CONSUME, value, null, skill )

        if ( skill.isDance() ) {
            return this.calculateStat( Stats.DANCE_MP_CONSUME_RATE, value )
        }

        if ( skill.isMagic() ) {
            return this.calculateStat( Stats.MAGICAL_MP_CONSUME_RATE, value )
        }

        return this.calculateStat( Stats.PHYSICAL_MP_CONSUME_RATE, value )
    }

    getAttackTraits() {
        return this.attackTraits
    }

    getAttackTraitsCount() {
        return this.attackTraitsCount
    }

    getDefenceTraitsCount() {
        return this.defenceTraitsCount
    }

    getTraitsInvulnerable() {
        return this.traitsInvulnerable
    }

    getPhysicalAttackAngle(): number {
        let weapon: L2Weapon = this.getActiveCharacter().getActiveWeaponItem()
        if ( weapon ) {
            return weapon.getBaseAttackAngle()
        }

        return 120
    }

    getMagicalAttackRange( skill: Skill ): number {
        if ( skill ) {
            return Math.floor( this.calculateStat( Stats.MAGIC_ATTACK_RANGE, skill.getCastRange(), null, skill ) )
        }

        return this.getActiveCharacter().getTemplate().getBaseAttackRange()
    }
}