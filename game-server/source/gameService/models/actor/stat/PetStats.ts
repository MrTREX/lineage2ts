import { SummonStats } from './SummonStats'
import { ConfigManager } from '../../../../config/ConfigManager'
import { L2PetInstance } from '../instance/L2PetInstance'
import { Stats } from '../../stats/Stats'
import { L2Character } from '../L2Character'
import { Skill } from '../../Skill'
import { PetInfoAnimation } from '../../../packets/send/PetInfo'
import { DataManager } from '../../../../data/manager'
import { SystemMessageBuilder } from '../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../packets/SystemMessageIdValues'

export class PetStats extends SummonStats {

    getMaxFeed(): number {
        return this.getActiveCharacter().getPetLevelData().getPetMaxFeed()
    }

    getMaxLevel(): number {
        return ConfigManager.character.getMaxPetLevel()
    }

    getMaxExpLevel(): number {
        return ConfigManager.character.getMaxPetLevel() + 1
    }

    getActiveCharacter(): L2PetInstance {
        return this.character as L2PetInstance
    }

    getMaxHp(): number {
        return this.calculateStat( Stats.MAX_HP, this.getActiveCharacter().getPetLevelData().getMaxHP(), null, null )
    }

    getMaxMp(): number {
        return this.calculateStat( Stats.MAX_MP, this.getActiveCharacter().getPetLevelData().getMaxMP(), null, null )
    }

    getMagicAttack( target: L2Character, skill: Skill ): number {
        return this.calculateStat( Stats.MAGIC_ATTACK, this.getActiveCharacter().getPetLevelData().getMagicAttack(), null, null )
    }

    getMagicDefence( target: L2Character, skill: Skill ): number {
        return this.calculateStat( Stats.MAGIC_DEFENCE, this.getActiveCharacter().getPetLevelData().getMagicDefence(), null, null )
    }

    getPowerAttack( target: L2Character ): number {
        return this.calculateStat( Stats.POWER_ATTACK, this.getActiveCharacter().getPetLevelData().getPowerAttack(), null, null )
    }

    getPowerDefence( target: L2Character ): number {
        return this.calculateStat( Stats.POWER_DEFENCE, this.getActiveCharacter().getPetLevelData().getPowerDefence(), null, null )
    }

    getPowerAttackSpeed(): number {
        let value = super.getPowerAttackSpeed()

        if ( this.getActiveCharacter().isHungry() ) {
            value = value / 2
        }

        return value
    }

    getMagicAttackSpeed(): number {
        let value = super.getMagicAttackSpeed()

        if ( this.getActiveCharacter().isHungry() ) {
            value = value / 2
        }

        return value
    }

    getExpForLevel( level: number ): number {
        let summon = this.getActiveCharacter()
        return DataManager.getPetData().getPetLevelData( summon.getTemplate().getId(), Math.min( level, this.getMaxExpLevel() ) ).getPetMaxExp()
    }

    setLevel( desiredLevel: number ) {
        let summon = this.getActiveCharacter()
        summon.stopFeeding()
        this.level = desiredLevel
        summon.startFeeding()

        let item = summon.getControlItem()
        if ( item ) {
            item.setEnchantLevel( this.getLevel() )
        }
    }

    async addExp( xp: number ): Promise<boolean> {
        let summon = this.getActiveCharacter()
        if ( summon.isUncontrollable() || !await super.addExp( xp ) ) {
            return false
        }

        await summon.updateAndBroadcastStatus( PetInfoAnimation.StatUpgrade )
        summon.updateEffectIcons()

        if ( ConfigManager.tuning.isPetShowingEarnedExp() ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.PET_EARNED_S1_EXP )
                    .addNumber( xp )
                    .getBuffer()

            summon.sendOwnedData( packet )
        }
        return true
    }
}