import { CharacterStats } from './CharacterStats'
import { L2Playable } from '../L2Playable'
import { ConfigManager } from '../../../../config/ConfigManager'
import { DataManager } from '../../../../data/manager'
import {
    EventTerminationResult,
    EventType,
    PlayableExpChangedEvent,
    PlayerChangedLevelEvent
} from '../../events/EventType'
import { ListenerCache } from '../../../cache/ListenerCache'
import { EventPoolCache } from '../../../cache/EventPoolCache'
import _ from 'lodash'
import { AreaType } from '../../areas/AreaType'
import { SlowArea } from '../../areas/type/Slow'
import { AreaDiscoveryManager } from '../../../cache/AreaDiscoveryManager'
import { AcquisitionPointType, GamePointsCache } from '../../../cache/GamePointsCache'
import { L2PcInstance } from '../instance/L2PcInstance'

export class PlayableStats extends CharacterStats {
    exp: number = 0
    sp: number = 0

    getSp(): number {
        return this.sp
    }

    getExp(): number {
        return this.exp
    }

    setSp( amount: number ): void {
        this.sp = amount
    }

    setExp( amount: number ): void {
        this.exp = amount
    }

    removeExp( amount: number ): Promise<void> {
        let value = this.exp - amount

        if ( value < 0 ) {
            value = 0
        }

        this.exp = value
        return this.synchronizeExperienceLevel( false )
    }

    removeSp( amount: number ): boolean {
        let value = this.sp - amount
        if ( value < 0 ) {
            value = 0
        }

        this.sp = value

        return true
    }

    getMaxLevel(): number {
        return ConfigManager.character.getMaxPlayerLevel()
    }

    getExpForLevel( level: number ): number {
        return DataManager.getExperienceData().getExpForLevel( level )
    }

    getActiveCharacter(): L2Playable {
        return super.getActiveCharacter() as L2Playable
    }

    addLevel( levels: number ): boolean {
        let maxLevel = this.getMaxLevel()
        if ( this.getLevel() === maxLevel ) {
            return false
        }

        let currentLevel = _.clamp( this.getLevel() + levels, maxLevel )
        let isLevelIncreased = levels > 0
        this.setLevel( currentLevel )

        let updatedLevel = this.getLevel()
        let currentExp = this.getExp()
        if ( ( currentExp >= this.getExpForLevel( updatedLevel + 1 ) ) || ( this.getExpForLevel( updatedLevel ) > currentExp ) ) {
            this.setExp( this.getExpForLevel( updatedLevel ) )
        }

        if ( !isLevelIncreased ) {
            return false
        }

        this.getActiveCharacter().setMaxStats()

        return true
    }

    getMaxExpLevel() {
        return ConfigManager.character.getMaxPlayerLevel()
    }

    async addExp( value: number ): Promise<boolean> {
        let currentExp = this.getExp()
        let totalExp = currentExp + value

        if ( ListenerCache.hasGeneralListener( EventType.PlayableExpChanged ) ) {
            let eventData = EventPoolCache.getData( EventType.PlayableExpChanged ) as PlayableExpChangedEvent

            eventData.characterId = this.character.getObjectId()
            eventData.newValue = value
            eventData.oldValue = currentExp
            eventData.currentLevel = this.getLevel()


            let result: EventTerminationResult = await ListenerCache.getGeneralTerminatedResult( EventType.PlayableExpChanged, eventData )
            if ( result && result.terminate ) {
                return false
            }
        }

        let maxExpLevel = this.getExpForLevel( this.getMaxExpLevel() )
        if ( ( totalExp < 0 ) || ( ( value > 0 ) && ( currentExp === ( maxExpLevel - 1 ) ) ) ) {
            return true
        }

        if ( totalExp >= maxExpLevel ) {
            value = ( maxExpLevel - 1 - currentExp )
        }

        this.exp += Math.floor( value )
        if ( this.exp >= this.getExpForLevel( this.getLevel() + 1 ) ) {
            await this.synchronizeExperienceLevel( true )
        }

        if ( this.character.isPlayer() ) {
            /*
                Please note that while exp value can be supplied to earn points, high level players
                will ultimately earn too many points in shorter amount of time vs low level. Hence,
                sp and exp earning of points is based on "occurence" rather than scaled point value.
             */
            GamePointsCache.earnPoints( this.character as L2PcInstance, AcquisitionPointType.Exp, 1 )
        }

        return true
    }

    async synchronizeExperienceLevel( isExpIncreased: boolean ) : Promise<void> {
        let minimumLevel = this.getActiveCharacter().getMinimumLevel()
        let currentExp = this.getExp()
        let maxLevel = this.getMaxLevel()
        let currentLevel = this.getLevel()

        let stats: PlayableStats = this
        let playable = this.getActiveCharacter()
        if ( isExpIncreased ) {
            for ( let level = currentLevel; level <= maxLevel; level++ ) {
                if ( currentExp < stats.getExpForLevel( level ) ) {
                    break
                }

                if ( currentExp >= stats.getExpForLevel( level + 1 ) ) {
                    continue
                }

                if ( level < minimumLevel ) {
                    level = minimumLevel
                }

                if ( level !== currentLevel ) {
                    let newLevel = level - currentLevel
                    if ( ListenerCache.hasGeneralListener( EventType.PlayerChangedLevel ) ) {
                        let eventData = EventPoolCache.getData( EventType.PlayerChangedLevel ) as PlayerChangedLevelEvent

                        eventData.newLevelValue = newLevel
                        eventData.oldLevelValue = currentLevel
                        eventData.playerId = playable.getObjectId()

                        await ListenerCache.sendGeneralEvent( EventType.PlayerChangedLevel, eventData )
                    }

                    await playable.addLevel( newLevel )
                }

                break
            }

            return
        }

        for ( let level = currentLevel; level >= minimumLevel; level-- ) {
            if ( currentExp >= stats.getExpForLevel( level ) ) {
                break
            }

            if ( currentExp < stats.getExpForLevel( level - 1 ) ) {
                continue
            }

            level--

            if ( level < minimumLevel ) {
                level = minimumLevel
            }

            if ( level !== currentLevel ) {
                let newLevel = level - currentLevel

                if ( ListenerCache.hasGeneralListener( EventType.PlayerChangedLevel ) ) {
                    let eventData = EventPoolCache.getData( EventType.PlayerChangedLevel ) as PlayerChangedLevelEvent

                    eventData.newLevelValue = level
                    eventData.oldLevelValue = currentLevel
                    eventData.playerId = playable.getObjectId()

                    await ListenerCache.sendGeneralEvent( EventType.PlayerChangedLevel, eventData )
                }

                await playable.addLevel( newLevel )
            }

            return
        }
    }

    addSp( amount: number ): void {
        if ( amount < 0 ) {
            return
        }

        let currentSp = this.getSp()
        if ( currentSp === Number.MAX_SAFE_INTEGER ) {
            return
        }

        if ( amount > ( Number.MAX_SAFE_INTEGER - currentSp ) ) {
            this.sp = Number.MAX_SAFE_INTEGER
        } else {
            this.sp += Math.floor( amount )

            if ( this.character.isPlayer() ) {
                /*
                    Please note that while exp value can be supplied to earn points, high level players
                    will ultimately earn too many points in shorter amount of time vs low level. Hence,
                    sp and exp earning of points is based on "occurence" rather than scaled point value.
                 */
                GamePointsCache.earnPoints( this.character as L2PcInstance, AcquisitionPointType.Sp, 1 )
            }
        }
    }

    getRunSpeed(): number {
        let character = this.getActiveCharacter()
        if ( character.isInArea( AreaType.Slow ) ) {
            // TODO : store ids and look up swamp zone id directly
            let area = AreaDiscoveryManager.getEnteredAreaByType( character.getObjectId(), AreaType.Slow ) as SlowArea
            if ( area ) {
                return super.getRunSpeed() + area.properties.moveBonus
            }
        }

        return super.getRunSpeed()
    }

    getWalkSpeed(): number {
        let character = this.getActiveCharacter()
        if ( character.isInArea( AreaType.Slow ) ) {
            // TODO : store ids and look up swamp zone id directly
            let area = AreaDiscoveryManager.getEnteredAreaByType( character.getObjectId(), AreaType.Slow ) as SlowArea
            if ( area ) {
                return super.getWalkSpeed() + area.properties.moveBonus
            }
        }

        return super.getWalkSpeed()
    }
}