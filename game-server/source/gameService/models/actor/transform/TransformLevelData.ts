export class TransformLevelData {
    level: number
    levelModifier: number
    stats: { [ key: string ] : number}

    getLevel() : number {
        return this.level
    }

    getLevelModifier() {
        return this.levelModifier
    }

    getStats( name: string ) {
        return this.stats[ name ]
    }
}