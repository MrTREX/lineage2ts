import { WeaponType } from '../../items/type/WeaponType'
import { TransformLevelData } from './TransformLevelData'
import { MoveType } from '../../stats/MoveType'
import _ from 'lodash'
import { Skill } from '../../Skill'
import { TransformSkill } from '../../../enums/TransformSkill'

export interface AdditionalItem {
    itemId: number
    allowed: boolean
}

export class TransformTemplate {
    collisionRadius: number
    collisionHeight: number
    baseAttackType: WeaponType
    baseAttackRange: number
    baseRandomDamage: number
    skills: Array<Skill> = []
    additionalSkills: Array<TransformSkill> = []
    additionalItems: Array<AdditionalItem> = []
    baseDefense: { [ key: number ]: number } = {}
    baseStats: { [ key: string ]: number } = {}
    baseSpeed: { [ key: number ]: number } = {}

    actionListPacket: Buffer | null
    data: { [ key: number ]: TransformLevelData } = {}


    getData( level: number ) : TransformLevelData {
        return this.data[ level ]
    }

    getDefence( type: number ) : number {
        let value = this.baseDefense[ type ]

        if ( !value ) {
            return 0
        }

        return value
    }

    getCollisionHeight() {
        return this.collisionHeight
    }

    getCollisionRadius() {
        return this.collisionRadius
    }

    getBaseAttackRange() {
        return this.baseAttackRange
    }

    getStats( name: string ) {
        let value = this.baseStats[ name ]

        if ( !value ) {
            return 0
        }

        return value
    }

    getBaseMoveSpeed( type: MoveType ) {
        return _.defaultTo( this.baseSpeed[ type ], 0 )
    }

    getSkills() {
        return this.skills
    }

    getAdditionalSkills() {
        return this.additionalSkills
    }

    getAdditionalItems() : Array<AdditionalItem> {
        return this.additionalItems
    }

    hasBasicActionPacket() {
        return !!this.actionListPacket
    }

    setBasicActionPacket( packet: Buffer ) {
        this.actionListPacket = packet
    }

    getBasicActionPacket() : Buffer {
        return this.actionListPacket
    }

    getBaseAttackType() : WeaponType {
        return this.baseAttackType
    }
}