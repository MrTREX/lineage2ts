import { L2World } from '../../../L2World'

export class PcAppearance {
    ownerId: number
    face: number
    hairColor: number
    hairStyle: number

    isFemale: boolean // Female true(1)

    /** true if the player is invisible */
    ghostmode: boolean

    /** The current visible name of this player, not necessarily the real one */
    visibleName: string

    /** The current visible title of this player, not necessarily the real one */
    visibleTitle: string

    /** The default name color is 0xFFFFFF. */
    nameColor: number = 0xFFFFFF

    /** The default title color is 0xECF9A2. */
    titleColor: number = 0xECF9A2

    constructor( face: number, hairColor: number, hairStyle: number, isFemale: boolean ) {
        this.face = face
        this.hairColor = hairColor
        this.hairStyle = hairStyle
        this.isFemale = isFemale
    }

    getFace(): number {
        return this.face
    }

    getHairColor(): number {
        return this.hairColor
    }

    getHairStyle(): number {
        return this.hairStyle
    }

    getNameColor() {
        return this.nameColor
    }

    getSex(): boolean {
        return this.isFemale
    }

    getTitleColor() {
        return this.titleColor
    }

    getVisibleName(): string {
        if ( this.visibleName ) {
            return this.visibleName
        }

        return L2World.getPlayer( this.ownerId ).getName()
    }

    getVisibleTitle(): string {
        if ( this.visibleTitle ) {
            return this.visibleTitle
        }

        return L2World.getPlayer( this.ownerId ).getTitle()
    }

    isGhost() {
        return this.ghostmode
    }

    setOwner( objectId: number ) {
        this.ownerId = objectId
    }

    setVisibleName( name: string ) {
        this.visibleName = name
    }

    setVisibleTitle( title: string ) {
        this.visibleTitle = title
    }

    setFace( value: number ) {
        this.face = value
    }

    setHairColor( value: number ) {
        this.hairColor = value
    }

    setHairStyle( value: number ) {
        this.hairStyle = value
    }

    setTitleColor( value: number ) {
        this.titleColor = Math.abs( value )
    }

    setNameColor( value: number ) {
        this.nameColor = Math.max( 0, value )
    }
}