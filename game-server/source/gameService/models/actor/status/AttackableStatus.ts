import { NpcStatus } from './NpcStatus'
import { L2Attackable } from '../L2Attackable'
import { L2Character } from '../L2Character'
import { AggroCache } from '../../../cache/AggroCache'

export class AttackableStatus extends NpcStatus {

    character : L2Attackable

    async reduceHp( value: number, attacker: L2Character, isAwake: boolean, isDOT: boolean, isHpConsumed: boolean ) : Promise<void> {
        if ( this.character.isDead() ) {
            return
        }

        if ( value > 0 ) {
            if ( this.character.isOverhit() ) {
                this.character.setOverhitValues( attacker, value )
            } else {
                this.character.overhitEnabled( false )
            }
        } else {
            this.character.overhitEnabled( false )
        }

        if ( attacker ) {
            AggroCache.addAggro( this.character.getObjectId(), attacker.getObjectId(), value, 1 )
        }

        await super.reduceHp( value, attacker, isAwake, isDOT, isHpConsumed )

        if ( !this.character.isDead() ) {
            this.character.overhitEnabled( false )
        }
    }
}