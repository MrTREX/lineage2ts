import { L2Object } from '../L2Object'
import { InstanceType } from '../../enums/InstanceType'
import { Formulas } from '../stats/Formulas'
import { CharacterStats } from './stat/CharacterStats'
import { Transform } from './Transform'
import { L2CharacterSkillMap, L2CharacterTemplate } from './templates/L2CharacterTemplate'
import { CharacterStatus } from './status/CharacterStatus'
import { L2PcInstance } from './instance/L2PcInstance'
import { AbstractFunction } from '../stats/functions/AbstractFunction'
import { Inventory } from '../itemcontainer/Inventory'
import { L2Weapon } from '../items/L2Weapon'
import { Skill } from '../Skill'
import { InvulnerableSkillHolder } from '../holders/InvulnerableSkillHolder'
import { CharacterEffects } from '../characterEffects'
import { SkillChannelized } from '../skills/SkillChannelized'
import { PlayerActionOverride } from '../../values/PlayerConditions'
import { ILocational } from '../Location'
import { ConfigManager } from '../../../config/ConfigManager'
import { L2WorldRegion } from '../L2WorldRegion'
import { L2Party } from '../L2Party'
import { createStatusUpdateForHp, StatusUpdate, StatusUpdateProperty } from '../../packets/send/builder/StatusUpdate'
import { PacketDispatcher } from '../../PacketDispatcher'
import { Revive } from '../../packets/send/Revive'
import { StopMove } from '../../packets/send/StopMove'
import { ActionFailed } from '../../packets/send/ActionFailed'
import { TeleportToLocation } from '../../packets/send/TeleportToLocation'
import { MagicSkillCanceled } from '../../packets/send/MagicSkillCanceled'
import { GameClient } from '../../GameClient'
import { L2GameClientRegistry } from '../../L2GameClientRegistry'
import { L2World } from '../../L2World'
import { DeleteObject } from '../../packets/send/DeleteObject'
import { L2Summon } from './L2Summon'
import { InstanceManager } from '../../instancemanager/InstanceManager'
import { Stats } from '../stats/Stats'
import { EffectFlag } from '../../enums/EffectFlag'
import { AbnormalVisualEffect, AbnormalVisualEffectMap, AbnormalVisualType } from '../skills/AbnormalVisualEffect'
import { CommonSkill, SkillHolder } from '../holders/SkillHolder'
import { ISkillCapable } from '../../interface/ISkillCapable'
import { L2Clan } from '../L2Clan'
import { AbnormalType } from '../skills/AbnormalType'
import { L2ItemInstance } from '../items/instance/L2ItemInstance'
import { GeneralHelper } from '../../helpers/GeneralHelper'
import { DataManager } from '../../../data/manager'
import { CategoryType } from '../../enums/CategoryType'
import { Race } from '../../enums/Race'
import { TransformTemplate } from './transform/TransformTemplate'
import { WeaponType } from '../items/type/WeaponType'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { ChangeMoveType } from '../../packets/send/ChangeMoveType'
import { L2Item } from '../items/L2Item'
import { MagicSkillUse, MagicSkillUseWithCharacters } from '../../packets/send/MagicSkillUse'
import { MagicSkillLaunched } from '../../packets/send/MagicSkillLaunched'
import { ChangeWaitType, ChangeWaitTypeValue } from '../../packets/send/ChangeWaitType'
import { SetupGauge, SetupGaugeColors } from '../../packets/send/SetupGauge'
import { FlyType } from '../../packets/send/FlyToLocation'
import { EffectScope } from '../skills/EffectScope'
import { BuffInfo } from '../skills/BuffInfo'
import { ExRotation } from '../../packets/send/ExRotation'
import { L2Attackable } from './L2Attackable'
import { SkillChannelizer } from '../skills/SkillChannelizer'
import { TerritoryWarManager } from '../../instancemanager/TerritoryWarManager'
import aigle from 'aigle'
import { ShotType } from '../../enums/ShotType'
import { AttackBuilder } from '../../packets/send/builder/AttackBuilder'
import { L2PetInstance } from './instance/L2PetInstance'
import { Die } from '../../packets/send/Die'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { ListenerCache } from '../../cache/ListenerCache'
import {
    CharacterKilledEvent,
    CharacterReceivedDamageEvent,
    CharacterTeleportedEvent,
    CreatureAvoidAttackEvent,
    CreatureUsesSkillEvent,
    EventTerminationResult,
    EventType,
} from '../events/EventType'
import { EventPoolCache } from '../../cache/EventPoolCache'
import { AggroCache } from '../../cache/AggroCache'
import _, { DebouncedFunc } from 'lodash'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'
import { AIControllerCapable } from './AIControllerCapable'
import { IAIController } from '../../aicontroller/interface/IAIController'
import { DataFlag } from '../../threads/models/SharedData'
import { L2NpcTemplate } from './templates/L2NpcTemplate'
import { AIIntent } from '../../aicontroller/enums/AIIntent'
import { DeferredMethods } from '../../helpers/DeferredMethods'
import { FightStanceCache } from '../../cache/FightStanceCache'
import { StatsCache } from '../../cache/StatsCache'
import { MovementType } from '../../enums/MovementType'
import { SiegeRole } from '../../enums/SiegeRole'
import { ProximalDiscoveryManager } from '../../cache/ProximalDiscoveryManager'
import { PathFinding } from '../../../geodata/PathFinding'
import { L2MoveManager } from '../../L2MoveManager'
import { MagicProcess, MagicProcessPool } from '../MagicProcess'
import { AIEffect } from '../../aicontroller/enums/AIEffect'
import { GeoPolygonCache } from '../../cache/GeoPolygonCache'
import { GeometryId } from '../../enums/GeometryId'
import { PhysicalDamageParameters } from '../PhysicalDamageParameters'
import { TraitSettings } from '../../aicontroller/interface/IAITrait'
import { L2WorldArea } from '../areas/WorldArea'
import { WorldAreaActions } from '../areas/WorldAreaActions'
import { AreaType } from '../areas/AreaType'
import { L2Playable } from './L2Playable'
import { PlayerAccess } from '../PlayerAccess'
import { PlayerPermission } from '../../enums/PlayerPermission'
import { teleportCharacterToGeometryCoordinates } from '../../helpers/TeleportHelper'
import { EffectFunctionCalculator } from '../EffectFunctionCalculator'
import Timeout = NodeJS.Timeout

type PhysicalDamageMethod = ( target: L2Character, damage: number, isCrit: boolean, isMiss: boolean ) => Promise<void>

export class L2Character extends L2Object implements ISkillCapable, AIControllerCapable {
    lastSkillCast: Skill
    lastSimultaneousSkillCast: Skill

    isDeadValue: boolean = false
    isImmobilizedValue: boolean = false
    isOverloadedValue: boolean = false
    isPendingReviveValue: boolean = false
    isRunningValue: boolean = false
    showSummonAnimation: boolean = false
    isTeleportingValue: boolean = false
    isInvulnerableValue: boolean = false
    isMortalValue: boolean = true
    isFlyingValue: boolean = false

    stats: CharacterStats
    status: CharacterStatus
    template: L2CharacterTemplate
    title: string = ''

    calculators: EffectFunctionCalculator
    skills: L2CharacterSkillMap = {}
    allSkillsDisabled: boolean = false

    lethalable: boolean = true
    invulnerableAgainst: { [ key: number ]: InvulnerableSkillHolder } = {}
    effects: CharacterEffects
    channelizer: SkillChannelizer
    channelized: SkillChannelized

    abnormalVisualEffects: number = 0
    abnormalVisualEffectsSpecial: number = 0
    abnormalVisualEffectsEvent: number = 0

    targetId: number = 0
    castInterruptTime: number = 0
    skillCast: MagicProcess
    skillCastSimultaneous: MagicProcess
    updateMovingParametersTask: Timeout

    runPhysicalDamageMethod: PhysicalDamageMethod
    physicalDamageData: PhysicalDamageParameters

    modifiedStats: Set<string> = new Set<string>()
    movementStopTime: number = 0

    /*
        Throttled or debounced methods.
     */

    debounceAreaRevalidation: DebouncedFunc<() => void>
    debounceBroadcastStatusUpdate: DebouncedFunc<() => void>
    debounceBroadcastAllStats: DebouncedFunc<() => void>
    debounceBroadcastModifiedStats: DebouncedFunc<() => void>

    constructor( objectId: number, template: L2CharacterTemplate ) {
        super( objectId )

        this.instanceType = InstanceType.L2Character
        this.stats = this.createStats()
        this.status = this.createStatus()
        this.template = template

        this.isInvulnerableValue = true
        this.effects = new CharacterEffects( this )

        this.createMethods()
        this.assignCalculators()

        if ( this.isMoveCapable() ) {
            this.sharedData.setMovementId( objectId )
            L2MoveManager.registerMovingCharacter( this )
        }
    }

    createMethods() {
        this.debounceBroadcastStatusUpdate = _.debounce( this.runBroadcastStatusUpdate.bind( this ), 150, {
            trailing: true,
        } )

        this.debounceBroadcastAllStats = _.debounce( this.broadcastAllStatsMethod.bind( this ), 150, {
            trailing: true,
        } )

        this.debounceBroadcastModifiedStats = _.debounce( this.broadcastModifiedStats.bind( this ), 150, {
            trailing: true,
        } )

        this.runPhysicalDamageMethod = this.runPhysicalDamageTask.bind( this )
    }

    cancelMethods() {
        this.debounceAreaRevalidation.cancel()
        this.debounceBroadcastStatusUpdate.cancel()
        this.debounceBroadcastAllStats.cancel()
        this.debounceBroadcastModifiedStats.cancel()
    }

    assignCalculators() : void {
        this.calculators = new EffectFunctionCalculator()
    }

    isMoveCapable(): boolean {
        return true
    }

    createStats(): CharacterStats {
        return new CharacterStats( this )
    }

    createStatus(): CharacterStatus {
        return new CharacterStatus( this )
    }

    abortAttack() {}

    abortCast( notifyAIController : boolean = true ) : void {
        if ( !this.isCasting() ) {
            return
        }

        this.abortCastOne()
        this.abortCastTwo()

        if ( this.isChanneling() ) {
            this.getSkillChannelizer().stopChanneling()
        }

        if ( this.allSkillsDisabled ) {
            this.enableAllSkills()
        }

        this.castInterruptTime = 0

        BroadcastHelper.dataToSelfBasedOnVisibility( this, MagicSkillCanceled( this.objectId ) )

        if ( this.isPlayer() ) {
            this.sendOwnedData( ActionFailed() )
        }

        if ( this.hasAIController() && notifyAIController && this.isCasting() ) {
            let skillId = this.skillCast ? this.skillCast.skill.getId() : this.skillCastSimultaneous.skill.getId()
            let isSimultaneous = !!this.skillCastSimultaneous
            AIEffectHelper.notifyCastingCancelled( this, skillId, isSimultaneous )
        }
    }

    /*
        TODO : better names for methods since it is hard to reason which cast is happening
         - primarily simultaneous cast is used by herbs, meaning it is a way to track indirect skill execution applied to character

         Hence, why the naming?
     */
    isCasting() : boolean {
        return this.isCastingNow() || this.isCastingSimultaneouslyNow()
    }

    async addExpAndSp( addExp: number, addSp: number ): Promise<void> {
    }

    async addSkill( skill: Skill ): Promise<Skill> {
        let oldSkill
        if ( skill ) {
            oldSkill = this.skills[ skill.getId() ]
            this.skills[ skill.getId() ] = skill

            if ( oldSkill ) {
                this.removeStatsOwner( oldSkill )

                if ( oldSkill.isPassive() ) {
                    await this.stopSkillEffects( false, oldSkill.getId() )
                }
            }

            if ( skill.isPassive() ) {
                await skill.applyEffects( this, this, false, true, false, 0 )
            }
        }

        return oldSkill
    }

    getKnownSkill( skillId: number ): Skill {
        return this.skills[ skillId ]
    }

    getSkillLevel( skillId: number ): number {
        let skill: Skill = this.getKnownSkill( skillId )

        if ( skill ) {
            return skill.getLevel()
        }

        return -1
    }

    getSkills() {
        return this.skills
    }

    addSingleStatFunction( currentFunction: AbstractFunction ): void {
        this.calculators.addFunction( currentFunction )
        this.calculators.sortFunctions( currentFunction.getStat() )

        StatsCache.removeStat( this.getObjectId(), currentFunction.getStat() )
    }

    addStatFunctions( functions: ReadonlyArray<AbstractFunction>, performFullUpdate: boolean ): void {
        let modifiedStats = new Set<string>()
        for ( const effect of functions ) {
            this.calculators.addFunction( effect )
            modifiedStats.add( effect.getStat() )
        }

        for ( let name of modifiedStats ) {
            this.calculators.sortFunctions( name )
        }

        StatsCache.removeStats( this.getObjectId(), modifiedStats )

        if ( !performFullUpdate ) {
            return
        }

        if ( this.isPlayer()
                && this.getWorldRegion()
                && this.getWorldRegion().isActive()
                && this.hasLoadedSkills() ) {
            return this.debounceBroadcastAllStats()
        }
    }

    broadcastAllStatsMethod(): void {
        this.modifiedStats
            .add( Stats.MOVE_SPEED )
            .add( Stats.POWER_ATTACK_SPEED )
            .add( Stats.MAGIC_ATTACK_SPEED )

        this.debounceBroadcastModifiedStats()
    }

    addSkillReuse( skill: Skill, reuseTime: number ): void {}

    async beginCast( skill: Skill, isSimultaneous: boolean = skill.isSimultaneousCast() ): Promise<void> {
        if ( !this.canCastSkill( skill ) ) {
            AIEffectHelper.notifyCastingCancelled( this, skill.getId(), isSimultaneous )
            return
        }

        if ( this.effects.hasEffectsRemovedOnAction() ) {
            await this.effects.stopEffectsOnAction()
        }

        let target = skill.getSingleTargetOf( this ) as L2Character
        let targets: Array<L2Object> = skill.getTargetsOf( this )

        return this.beginCastWithTargets( skill, isSimultaneous, target, targets )
    }

    async beginCastWithTargets( skill: Skill, isSimultaneous: boolean, target: L2Character, targets: Array<L2Object> ) {
        let character = this
        let finishAction: Function = () => {
            if ( character.isPlayer() ) {
                character.sendOwnedData( ActionFailed() )
            }

            AIEffectHelper.notifyCastingCancelled( character, skill.getId(), isSimultaneous )
        }

        if ( !target ) {
            finishAction()
            return
        }

        let targetIds: Array<number> = targets.map( target => target.getObjectId() )
        if ( ListenerCache.hasGeneralListener( EventType.CharacterUsesSkill ) ) {
            let eventData = EventPoolCache.getData( EventType.CharacterUsesSkill ) as CreatureUsesSkillEvent

            eventData.attackerId = this.getObjectId()
            eventData.skillId = skill.getId()
            eventData.skillLevel = skill.getLevel()
            eventData.isSimultaneous = isSimultaneous
            eventData.mainTargetId = target.getObjectId()
            eventData.targetIds = targetIds

            let result: EventTerminationResult = await ListenerCache.getGeneralTerminatedResult( EventType.CharacterUsesSkill, eventData )
            if ( result && result.terminate ) {
                finishAction()
                return
            }
        }

        if ( skill.hasEffectType( L2EffectType.Resurrection ) ) {
            if ( this.isResurrectionBlocked() || target.isResurrectionBlocked() ) {
                if ( this.isPlayable() || target.isPlayable() ) {
                    this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.REJECT_RESURRECTION ) )
                    target.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.REJECT_RESURRECTION ) )
                }

                finishAction()
                return
            }
        }

        let magicId = skill.getId()
        let skillAnimationTime = Formulas.calculateCastTime( this, skill )

        if ( this.isCastingSimultaneouslyNow() && isSimultaneous ) {
            if ( this.isPlayer() ) {
                ( this as unknown as L2PcInstance ).setAIEffectAction( () => this.beginCastWithTargets( skill, isSimultaneous, target, targets ), AIEffect.SkillCastFinished )
            }
            return AIEffectHelper.notifyCastingCancelled( this, skill.getId(), isSimultaneous )
        }

        if ( isSimultaneous ) {
            this.setLastSimultaneousSkillCast( skill )
        } else {
            this.castInterruptTime = Date.now() + skillAnimationTime - 200
            this.setLastSkillCast( skill )
        }

        let reuseDelay
        if ( skill.isReuseDelayLocked() || skill.isStatic() ) {
            reuseDelay = skill.getReuseDelay()
        } else if ( skill.isMagic() ) {
            reuseDelay = Math.floor( skill.getReuseDelay() * this.calculateStat( Stats.MAGIC_REUSE_RATE, 1, null, null ) )
        } else if ( skill.isPhysical() ) {
            reuseDelay = Math.floor( skill.getReuseDelay() * this.calculateStat( Stats.P_REUSE, 1, null, null ) )
        } else {
            reuseDelay = Math.floor( skill.getReuseDelay() * this.calculateStat( Stats.DANCE_REUSE, 1, null, null ) )
        }

        let skillMastery = Formulas.calculateSkillMastery( this, skill )

        if ( reuseDelay > 30000 && !skillMastery ) {
            this.addSkillReuse( skill, reuseDelay )
        }

        let startMp = this.isAttackable() ? 0 : this.getStat().getMpConsume1( skill )
        let player = this.getActingPlayer()
        if ( startMp > 0 ) {
            this.getStatus().reduceMp( startMp )
        }

        if ( reuseDelay > 10 ) {
            if ( skillMastery ) {
                reuseDelay = 100

                if ( player ) {
                    target.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SKILL_READY_TO_USE_AGAIN ) )
                }
            }

            this.disableSkill( skill, reuseDelay )
        }

        if ( target.getObjectId() !== this.getObjectId() ) {
            this.setHeading( GeneralHelper.calculateHeadingFromLocations( this, target ) )
            BroadcastHelper.dataToSelfBasedOnVisibility( this, ExRotation( this.objectId, this.getHeading() ) )
        }

        if ( this.isPlayable() ) {
            if ( skill.getItemConsumeId() > 0 ) {
                let isItemConsumed = await this.destroyItemByItemId( skill.getItemConsumeId(), skill.getItemConsumeCount(), true, 'L2Character.beginCastWithTargets' )
                if ( !isItemConsumed ) {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ITEMS ) )
                    return AIEffectHelper.notifyCastingCancelled( this, skill.getId(), isSimultaneous )
                }
            }

            let talisman = this.getInventory().getTalismanWithSkill( skill.getId() )
            if ( talisman ) {
                if ( talisman.getMana() < talisman.getManaConsumeOnSkill() ) {
                    return AIEffectHelper.notifyCastingCancelled( this, skill.getId(), isSimultaneous )
                }

                await talisman.decreaseMana( talisman.getManaConsumeOnSkill() )
            }
        }

        BroadcastHelper.dataToSelfBasedOnVisibility( this, MagicSkillUseWithCharacters( this, target, skill.getDisplayId(), skill.getDisplayLevel(), Math.floor( skillAnimationTime ), reuseDelay ) )

        if ( this.isPlayer() && !skill.isAbnormalInstant ) {
            switch ( magicId ) {
                case 2046: // Wolf Collar
                    this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SUMMON_A_PET ) )
                    break

                default:
                    let packet = new SystemMessageBuilder( SystemMessageIds.USE_S1 )
                            .addSkillName( skill )
                            .getBuffer()
                    this.sendOwnedData( packet )
                    break
            }
        }

        if ( skill.hasEffects( EffectScope.START ) ) {
            await skill.applyEffectScope( EffectScope.START, BuffInfo.fromParameters( this, target, skill ), true, false )
        }

        if ( skill.isFlyType() ) {
            // TODO : add offset to location based on collision radius
            setTimeout( DeferredMethods.flyToLocation, 100, this, target, FlyType.Charge )
        }

        let magicCast = MagicProcessPool.getValue()
        magicCast.setParameters( this, targetIds, skill, skillAnimationTime, isSimultaneous )

        if ( skillAnimationTime > 0 ) {
            if ( this.isPlayer() && !isSimultaneous ) {
                player.sendOwnedData( SetupGauge( this.getObjectId(), SetupGaugeColors.Blue, skillAnimationTime ) )
            }

            if ( skill.isChanneling() && ( skill.getChannelingSkillId() > 0 ) ) {
                this.getSkillChannelizer().startChanneling( skill )
            } else if ( skill.isContinuousType() ) {
                skillAnimationTime -= 300
            }


            if ( isSimultaneous ) {
                this.abortCastTwo()
                this.skillCastSimultaneous = magicCast
            } else {
                this.abortCastOne()
                this.skillCast = magicCast
            }

            magicCast.startProcessWithDelay( Math.max( 0, skillAnimationTime - 400 ) )
        } else {
            this.abortCastOne()
            this.skillCast = magicCast
            magicCast.startProcessWithDelay( 0 )
        }

        // TODO : revisit to understand how to chain skill casting better
        // primarily this is concern with npc buffs, see usages of L2Character.doCast
    }

    notifyAttackInterrupted() : void {}

    attemptCastStop() {
        if ( this.isCastingNow()
                && this.canAbortCast()
                && this.getLastSkillCast()
                && ( this.getLastSkillCast().isMagic() || this.getLastSkillCast().isStatic() ) ) {
            this.abortCast()

            if ( this.isPlayer() ) {
                this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CASTING_INTERRUPTED ) )
            }
        }
    }

    createStatusUpdate() : StatusUpdate {
        if ( !this.modifiedStats.has( Stats.POWER_ATTACK_SPEED ) && !this.modifiedStats.has( Stats.MAGIC_ATTACK_SPEED ) ) {
            this.modifiedStats.clear()

            return null
        }

        let statusUpdate: StatusUpdate = new StatusUpdate( this.objectId )

        if ( this.modifiedStats.has( Stats.POWER_ATTACK_SPEED ) ) {
            statusUpdate.addAttribute( StatusUpdateProperty.AttackSpeed, this.getPowerAttackSpeed() )
        }

        if ( this.modifiedStats.has( Stats.MAGIC_ATTACK_SPEED ) ) {
            statusUpdate.addAttribute( StatusUpdateProperty.CastSpeed, this.getMagicAttackSpeed() )
        }

        this.modifiedStats.clear()

        return statusUpdate
    }

    broadcastModifiedStats(): void {
        this.broadcastStatusAttributes()
    }

    broadcastStatusAttributes() : void {
        let statusUpdate: StatusUpdate = this.createStatusUpdate()
        if ( statusUpdate && statusUpdate.hasAttributes() ) {
            BroadcastHelper.dataToSelfBasedOnVisibility( this, statusUpdate.getBuffer() )
        }
    }

    runBroadcastStatusUpdate() {
        let packet = createStatusUpdateForHp( this.getObjectId(), this.getMaxHp(), this.getCurrentHp() )

        this.getStatus().getStatusListeners().forEach( ( objectId: number ) => {
            PacketDispatcher.sendCopyData( objectId, packet )
        } )

        BroadcastHelper.dataBasedOnVisibility( this, packet )
    }

    broadcastStatusUpdate() {
        if ( this.getStatus().getStatusListeners().size === 0 ) {
            return
        }

        this.debounceBroadcastStatusUpdate()
    }

    calculateRewards( killer: L2Character ) {

    }

    calculateStat( stat: string, initialValue: number, target: L2Character = null, skill: Skill = null ): number {
        return this.getStat().calculateStat( stat, initialValue, target, skill )
    }

    async callSkill( skill: Skill, targets: Array<L2Character> ) {
        if ( skill.isToggle() && this.isAffectedBySkill( skill.getId() ) ) {
            return
        }


        if ( ConfigManager.npc.raidCurse() ) {
            let raidAttacker : L2Character
            let raidTarget = targets.find( ( target: L2Character ) : boolean => {
                raidAttacker = target

                if ( target.isRaid() && target.hasRaidCurse() && ( this.getLevel() > ( target.getLevel() + 8 ) ) ) {
                    return true
                }

                let attackTarget = target.getTarget() as L2Character

                return attackTarget
                    && !skill.isBad()
                    && attackTarget.isRaid()
                    && attackTarget.hasRaidCurse()
                    && AggroCache.hasAggro( attackTarget.getObjectId(), target.getObjectId() )
                    && ( this.getLevel() > ( attackTarget.getLevel() + 8 ) )
            } )

            if ( raidTarget ) {
                let curse: SkillHolder = skill.isMagic() ? CommonSkill.RAID_CURSE : CommonSkill.RAID_CURSE2
                let curseSkill: Skill = curse.getSkill()
                if ( curseSkill ) {
                    await curseSkill.applyEffects( raidAttacker, this, false, false, true, 0 )
                }

                this.abortAttack()
                this.abortCast()
                return
            }
        }

        /*
            Static skills cannot have overhit enabled, nor able to trigger character skills.
            - skill data does not have combination of skills with overhit and static type
         */
        if ( !skill.isStatic() ) {
            let activeWeapon: L2Weapon = this.getActiveWeaponItem()

            await aigle.resolve( targets ).eachSeries( async ( target: L2Character ) => {
                if ( skill.isOverhit() && target.isAttackable() ) {
                    ( target as L2Attackable ).overhitEnabled( true )
                }

                if ( activeWeapon && activeWeapon.hasCastOnMagicSkill() && !target.isDead() ) {
                    await activeWeapon.castOnMagicSkill( this, target, skill )
                }

                if ( this.hasTriggerSkills() ) {
                    await this.applySkillTriggerSkills( skill, target )
                }
            } )
        }

        await skill.activateCasterSkill( this, targets, true )

        await this.onCallSkill( skill, targets )

        if ( skill.isBad() && !skill.hasEffectType( L2EffectType.Hate ) ) {
            targets.forEach( ( currentTarget: L2Character ) => {
                AIEffectHelper.notifyAttackedWithTargetId( currentTarget, this.getObjectId(), 0, 1 )
            } )
        }
    }

    onCallSkill( skill: Skill, targets: Array<L2Object> ) : Promise<void> {
        return Promise.resolve()
    }

    canAbortCast() {
        return this.castInterruptTime > Date.now()
    }

    hasActionOverride( mask: PlayerActionOverride ): boolean {
        return false
    }

    canRevive(): boolean {
        return true
    }

    canCastSkill( skill: Skill ): boolean {
        if ( !skill || this.isSkillDisabled( skill ) || ( skill.isFlyType() && this.isMovementDisabled() ) ) {
            return false
        }

        if ( this.getCurrentHp() <= skill.getHpConsume() ) {
            return false
        }

        if ( !skill.isStatic() && ( ( skill.isMagic() && this.isMuted() ) || this.isPhysicalMuted() ) ) {
            return false
        }

        if ( skill.isChanneling()
                && skill.getChannelingSkillId() > 0
                && L2World.isInAreaRange( this, skill.getEffectRange(), AreaType.Peace ) ) {
            return false
        }

        return true
    }

    deleteMe(): Promise<void> {
        this.debounceBroadcastStatusUpdate.cancel()
        this.debounceAreaRevalidation.cancel()

        if ( this.updateMovingParametersTask ) {
            clearInterval( this.updateMovingParametersTask )
        }

        return Promise.resolve()
    }

    async destroyItem( item: L2ItemInstance, sendMessage: boolean, reason: string = null ): Promise<boolean> {
        return false
    }

    async destroyItemByItemId( itemId: number, count: number, sendMessage: boolean, reason: string = null ): Promise<boolean> {
        return false
    }

    disableSkill( skill: Skill, delay: number ): void {}

    /*
        TODO : refactor to force flow through AIController
        all due to fact that often we need to cast a skill while another cast might be in progress
        solution to such problem would be to keep scheduled skill casts (solves multiple casts)
        and have AIController take care of these one by one (no other actions permitted by AI design)

        Please note that currently there are four methods that need to be uplifted:
        - doCast
        - doCastWithHolder
        - doSimulteneousCast
        - doSimultaneousCastWithHolder
     */
    async doCast( skill: Skill ): Promise<void> {
        return this.beginCast( skill, false )
    }

    async doCastWithHolder( skill: SkillHolder ): Promise<void> {
        return this.beginCast( skill.getSkill(), false )
    }

    async doDie( killer: L2Character ): Promise<boolean> {
        if ( ListenerCache.hasGeneralListener( EventType.CharacterKilled ) ) {
            let eventData: CharacterKilledEvent = EventPoolCache.getData( EventType.CharacterKilled ) as CharacterKilledEvent

            eventData.attackerId = killer.getObjectId()
            eventData.targetId = this.getObjectId()

            let result: EventTerminationResult = await ListenerCache.getGeneralTerminatedResult( EventType.CharacterKilled, eventData )
            if ( result && result.terminate ) {
                return false
            }
        }

        if ( this.isDead() ) {
            return false
        }

        this.setCurrentHp( 0 )
        this.setIsDead( true )

        this.setTarget( null )
        this.abortMoving()
        this.getStatus().stopHpMpRegeneration()
        await this.stopAllEffectsExceptThoseThatLastThroughDeath()
        this.calculateRewards( killer )
        this.broadcastStatusUpdate()

        this.getAIController().deactivate()
        BroadcastHelper.dataToSelfBasedOnVisibility( this, Die( this ) )

        this.onDeathInZones()

        if ( this.isChannelized() ) {
            this.getSkillChannelized().abortChannelization()
        }

        StatsCache.deleteStats( this.getObjectId() )

        return true
    }

    doRevive(): void {
        if ( !this.isDead() ) {
            return
        }

        if ( this.isTeleporting() ) {
            return this.setIsPendingRevive( true )
        }

        this.setIsPendingRevive( false )
        this.setIsDead( false )

        let desiredCP = ConfigManager.character.getRespawnRestoreCP()
        let desiredHP = ConfigManager.character.getRespawnRestoreHP()
        let desiredMP = ConfigManager.character.getRespawnRestoreMP()

        if ( desiredCP > 0 && this.getCurrentCp() < desiredCP ) {
            this.getStatus().setCurrentCp( this.getMaxCp() * desiredCP )
        }

        if ( desiredHP > 0 && this.getCurrentHp() < desiredHP ) {
            this.getStatus().setCurrentHp( this.getMaxHp() * desiredHP )
        }

        if ( desiredMP > 0 && this.getCurrentMp() < desiredMP ) {
            this.getStatus().setCurrentMp( this.getMaxMp() * desiredMP )
        }

        BroadcastHelper.dataToSelfInRange( this, Revive( this.getObjectId() ) )
    }

    doReviveWithPercentage( value: number ) {
        this.doRevive()
    }

    doSimultaneousCast( skill: Skill ) {
        return this.beginCast( skill, true )
    }

    doSimultaneousCastWithHolder( skill: SkillHolder ) : Promise<void> {
        return this.beginCast( skill.getSkill(), true )
    }

    enableAllSkills() {
        this.allSkillsDisabled = false
    }

    getAbnormalVisualEffectEvent(): number {
        return this.abnormalVisualEffectsEvent
    }

    getAbnormalVisualEffectSpecial(): number {
        return this.abnormalVisualEffectsSpecial
    }

    getAbnormalVisualEffects(): number {
        return this.abnormalVisualEffects
    }

    getAccessLevel(): PlayerAccess {
        return null
    }

    getAccuracy(): number {
        return this.getStat().getAccuracy()
    }

    getActingPlayer(): L2PcInstance {
        return null
    }

    getActingPlayerId(): number {
        return 0
    }

    isCharacter(): boolean {
        return true
    }

    getActiveWeaponInstance(): L2ItemInstance {
        return null
    }

    getActiveWeaponItem(): L2Weapon {
        return null
    }

    getAllSkills(): Array<Skill> {
        return _.values( this.skills )
    }

    getAttackElement() {
        return this.getStat().getAttackElement()
    }

    getAttackElementValue( attackAttribute: number ): number {
        return this.getStat().getAttackElementValue( attackAttribute )
    }

    getAttackSpeedMultiplier(): number {
        return this.getStat().getAttackSpeedMultiplier()
    }

    getAttackType(): WeaponType {
        if ( this.isTransformed() ) {
            let template: TransformTemplate = this.getTransformation().getTemplate( this.getActingPlayer() )
            if ( template ) {
                return template.getBaseAttackType()
            }
        }

        let weapon: L2Weapon = this.getActiveWeaponItem()
        if ( weapon ) {
            return weapon.getItemType()
        }

        return this.getTemplate().getBaseAttackType()
    }

    getCON() {
        return this.getStat().getCON()
    }

    getClan(): L2Clan {
        return null
    }

    getCriticalHit( target: L2Character, skill: Skill ): number {
        return this.getStat().getCriticalHit( target, skill )
    }

    getCurrentCp(): number {
        return this.getStatus().getCurrentCp()
    }

    getCurrentHp(): number {
        return this.getStatus().getCurrentHp()
    }

    getCurrentLoad(): number {
        return 0
    }

    getCurrentMp(): number {
        return this.getStatus().getCurrentMp()
    }

    getDEX() {
        return this.getStat().getDEX()
    }

    getDanceCount() {
        return this.effects.getDanceCount()
    }

    getDefenceElementValue( defenceAttribute: number ) {
        return this.getStat().getDefenseElementValue( defenceAttribute )
    }

    getDuelId(): number {
        return 0
    }

    getEffectList(): CharacterEffects {
        return this.effects
    }

    getEvasionRate( target: L2Character ): number {
        return this.getStat().getEvasionRate( target )
    }

    getINT() {
        return this.getStat().getINT()
    }

    getInventory(): Inventory {
        return null
    }

    getInvulnerabilityAgainstSkills() {
        return this.invulnerableAgainst
    }

    getLastSimultaneousSkillCast(): Skill {
        return this.lastSimultaneousSkillCast
    }

    getLastSkillCast(): Skill {
        return this.lastSkillCast
    }

    getLevel(): number {
        return 0
    }

    getLevelModifier() {
        return ( this.getLevel() + 89 ) / 100
    }

    getMCriticalHit( target: L2Character, skill: Skill ) {
        return this.getStat().getMCriticalHit( target, skill )
    }

    getMEN() {
        return this.getStat().getMEN()
    }

    getMagicAttack( target: L2Character, skill: Skill ) {
        return this.getStat().getMagicAttack( target, skill )
    }

    getMagicAttackSpeed(): number {
        return this.getStat().getMagicAttackSpeed()
    }

    getMagicDefence( target: L2Character, skill: Skill ) {
        return this.getStat().getMagicDefence( target, skill )
    }

    getMaxCp() {
        return this.getStat().getMaxCp()
    }

    getMaxHp() {
        return this.getStat().getMaxHp()
    }

    getMaxLoad(): number {
        return 0
    }

    getMaxMp() {
        return this.getStat().getMaxMp()
    }

    getMaxRecoverableCp() {
        return this.getStat().getMaxRecoverableCp()
    }

    getMaxRecoverableHp(): number {
        return this.getStat().getMaxRecoverableHp()
    }

    getMaxRecoverableMp(): number {
        return this.getStat().getMaxRecoverableMp()
    }

    getMinimumLevel(): number {
        return 1
    }

    getMoveSpeed(): number {
        return this.getStat().getMoveSpeed()
    }

    getMovementSpeedMultiplier(): number {
        return this.getStat().getMovementSpeedMultiplier()
    }

    getParty(): L2Party {
        return null
    }

    getPowerAttack( target: L2Character ): number {
        return this.getStat().getPowerAttack( target )
    }

    getPowerAttackSpeed(): number {
        return this.getStat().getPowerAttackSpeed()
    }

    getPowerDefence( target: L2Character ): number {
        return this.getStat().getPowerDefence( target )
    }

    getRace(): Race {
        return this.getTemplate().getRace()
    }

    getRandomDamageMultiplier() {
        let activeWeapon: L2Weapon = this.getActiveWeaponItem()
        let value = activeWeapon ? activeWeapon.getRandomDamage() : 5 + Math.sqrt( this.getLevel() )

        return ( 1 + ( _.random( -value, value ) / 100 ) )
    }

    getRunSpeed(): number {
        return this.getStat().getRunSpeed()
    }

    getSTR() {
        return this.getStat().getSTR()
    }

    getSecondaryWeaponInstance(): L2ItemInstance {
        return null
    }

    getSecondaryWeaponItem(): L2Item {
        return null
    }

    getShieldDefence() {
        return this.getStat().getShieldDefence()
    }

    getSiegeSide(): number {
        return 0
    }

    getSkillChannelized() {
        if ( !this.channelized ) {
            this.channelized = new SkillChannelized()
        }

        return this.channelized
    }

    getSkillChannelizer(): SkillChannelizer {
        if ( !this.channelizer ) {
            this.channelizer = new SkillChannelizer( this )
        }

        return this.channelizer
    }

    getStat(): CharacterStats {
        return this.stats
    }

    getStatus(): CharacterStatus {
        return this.status
    }

    getSummon(): L2Summon {
        return null
    }

    getSwimRunSpeed(): number {
        return this.getStat().getSwimRunSpeed()
    }

    getSwimWalkSpeed(): number {
        return this.getStat().getSwimWalkSpeed()
    }

    getTarget(): L2Object {
        return L2World.getObjectById( this.targetId )
    }

    getTargetId(): number {
        return this.targetId
    }

    getTemplate(): L2CharacterTemplate {
        // TODO : do not store template directly here due to size concerns, linked reference and mutation
        // TODO : use either Cache manager or data source directly to pull template
        return this.template
    }

    getTitle(): string {
        return this.title
    }

    getTransformation(): Transform {
        return null
    }

    getWIT() {
        return this.getStat().getWIT()
    }

    getWalkSpeed(): number {
        return this.getStat().getWalkSpeed()
    }

    hasRaidCurse(): boolean {
        return false
    }

    hasPet() {
        return this.hasSummon() && this.getSummon().isPet()
    }

    hasServitor() {
        return this.hasSummon() && this.getSummon().isServitor()
    }

    hasSummon(): boolean {
        return !!this.getSummon()
    }

    isAffected( flag: EffectFlag ): boolean {
        return this.effects.isAffected( flag )
    }

    isAffectedBySkill( skillId: number ): boolean {
        return this.effects.isAffectedBySkill( skillId )
    }

    isAfraid(): boolean {
        return this.isAffected( EffectFlag.FEAR )
    }

    isAlikeDead(): boolean {
        return this.isDeadValue
    }

    isAllSkillsDisabled(): boolean {
        return this.allSkillsDisabled || this.isStunned() || this.isSleeping()
    }

    isAttackingDisabled() {
        return this.isFlying()
                || this.isStunned()
                || this.isSleeping()
                || this.isAttackingNow()
                || this.isAlikeDead()
                || this.isPhysicalAttackMuted()
    }

    isAttackingNow() : boolean {
        return this.physicalDamageData?.isActive
    }

    isBehind( target: L2Object ): boolean {
        if ( !target ) {
            return false
        }

        if ( target.isCharacter() ) {
            let maxAngleDiff = 60
            let character = target as L2Character
            let characterAngle = GeneralHelper.calculateAngleFromLocations( this, character )
            let targetAngle = GeneralHelper.convertHeadingToDegree( character.getHeading() )

            let difference = characterAngle - targetAngle
            if ( difference <= ( -360 + maxAngleDiff ) ) {
                difference += 360
            }
            if ( difference >= ( 360 - maxAngleDiff ) ) {
                difference -= 360
            }

            return Math.abs( difference ) <= maxAngleDiff
        }

        return false
    }

    isBehindTarget(): boolean {
        return this.isBehind( this.getTarget() )
    }

    isBetrayed() {
        return this.isAffected( EffectFlag.BETRAYED )
    }

    isBuffBlocked(): boolean {
        return this.isAffected( EffectFlag.BLOCK_BUFF )
    }

    isCastingNow() : boolean {
        return !!this.skillCast
    }

    isCastingSimultaneouslyNow() : boolean {
        return !!this.skillCastSimultaneous
    }

    isChanneling(): boolean {
        return !!this.channelizer && this.channelizer.isChanneling()
    }

    isChannelized(): boolean {
        return !!this.channelized && !this.channelized.isChannelized()
    }

    isConfused() {
        return this.isAffected( EffectFlag.CONFUSED )
    }

    isDead(): boolean {
        return this.isDeadValue
    }

    isDebuffBlocked() {
        return this.isAffected( EffectFlag.BLOCK_DEBUFF )
    }

    isFacing( target: L2Object, angle: number ): boolean {
        if ( !target ) {
            return false
        }

        let angleDifference = angle / 2
        let targetDifference = GeneralHelper.calculateAngleFromLocations( this, target )
        let attackerDifference = GeneralHelper.convertHeadingToDegree( this.getHeading() )

        let difference = attackerDifference - targetDifference
        if ( difference <= ( -360 + angleDifference ) ) {
            difference += 360
        }

        if ( difference >= ( 360 - angleDifference ) ) {
            difference -= 360
        }

        return Math.abs( difference ) <= angleDifference
    }

    isFlying() {
        return this.isFlyingValue
    }

    isGM(): boolean {
        return false
    }

    isHpBlocked(): boolean {
        return this.isAffected( EffectFlag.BLOCK_HP )
    }

    isImmobilized() {
        return this.isImmobilizedValue
    }

    isInCategory( type: CategoryType ) {
        return DataManager.getCategoryData().isInCategory( type, this.getId() )
    }

    isInCombat(): boolean {
        return FightStanceCache.hasPermanentStance( this.getObjectId() )
    }

    isInDuel(): boolean {
        return false
    }

    isInFrontOf( target: L2Character ): boolean {
        return this.isFacing( target, 120 )
    }

    isInFrontOfTarget(): boolean {
        return this.isInFrontOf( this.getTarget() as L2Character )
    }

    isInParty(): boolean {
        return false
    }

    isInsidePeaceZone( player: L2PcInstance ) {
        return this.isInsidePeaceZoneWithTarget( player, this )
    }

    isInsidePeaceZoneWithObjects( attacker: L2Object, target: L2Object ) {
        if ( !target || !( target.isPlayable() && attacker.isPlayable() ) ) {
            return false
        }

        if ( InstanceManager.getInstance( this.getInstanceId() ).isPvPInstance ) {
            return false
        }

        if ( ConfigManager.territoryWar.playerWithWardCanBeKilledInPeaceZone() && TerritoryWarManager.isTWInProgress ) {
            if ( target.isPlayer() && target.getActingPlayer().isCombatFlagEquipped() ) {
                return false
            }
        }

        if ( ConfigManager.character.karmaPlayerCanBeKilledInPeaceZone() ) {
            let targetPlayer = target.getActingPlayer()
            if ( targetPlayer && targetPlayer.getKarma() > 0 ) {
                return false
            }

            let attackerPlayer = attacker.getActingPlayer()
            if ( attackerPlayer
                    && attackerPlayer.getKarma() > 0
                    && targetPlayer
                    && targetPlayer.hasPvPFlag() ) {
                return false
            }
        }

        return ( target as L2Playable ).isInArea( AreaType.Peace ) || ( attacker as L2Playable ).isInArea( AreaType.Peace )
    }

    isInsidePeaceZoneWithTarget( player: L2PcInstance, target: L2Object ) {
        return !player.getAccessLevel().hasPermission( PlayerPermission.AllowPeaceAttack ) && this.isInsidePeaceZoneWithObjects( player, target )
    }

    isInsideRadius( location: ILocational, radius: number, checkZAxis: boolean = false ): boolean {
        return this.isInsideRadiusCoordinates( location.getX(), location.getY(), location.getZ(), radius, checkZAxis )
    }

    isInsideRadiusCoordinates( x: number, y: number, z: number, radius: number, checkZAxis: boolean ): boolean {
        return this.calculateDistanceWithCoordinates( x, y, z, checkZAxis ) <= radius
    }

    isInvulnerable(): boolean {
        return this.isInvulnerableValue || this.isTeleportingValue
    }

    isInvulnerableAgainst( skillId: number, skillLevel: number ): boolean {
        if ( this.invulnerableAgainst ) {
            let holder: SkillHolder = this.getInvulnerabilityAgainstSkills()[ skillId ]
            return holder && ( ( holder.getLevel() < 1 ) || ( holder.getLevel() === skillLevel ) )
        }

        return false
    }

    isLethalable() {
        return this.lethalable
    }

    isMinion() {
        return false
    }

    isMortal() {
        return this.isMortalValue
    }

    isMovementDisabled() {
        return this.isStunned()
                || this.isRooted()
                || this.isSleeping()
                || this.isOverloaded()
                || this.isImmobilized()
                || this.isAlikeDead()
                || this.isTeleporting()
    }

    isMoving() {
        return this.sharedData.hasDataFlag( DataFlag.Moving )
    }

    isMpBlocked(): boolean {
        return this.isAffected( EffectFlag.BLOCK_MP )
    }

    isMuted() {
        return this.isAffected( EffectFlag.MUTED )
    }

    isOnEvent() {
        return false
    }

    isOutOfControl(): boolean {
        return this.isConfused() || this.isAfraid()
    }

    isOverloaded() {
        return this.isOverloadedValue
    }

    isPendingRevive(): boolean {
        return this.isDead() && this.isPendingReviveValue
    }

    isPhysicalAttackMuted() {
        return this.isAffected( EffectFlag.PSYCHICAL_ATTACK_MUTED )
    }

    isPhysicalMuted() {
        return this.isAffected( EffectFlag.PSYCHICAL_MUTED )
    }

    isRaid() {
        return false
    }

    isRaidMinion() {
        return false
    }

    isResurrectionBlocked(): boolean {
        return this.isAffected( EffectFlag.BLOCK_RESURRECTION )
    }

    isRooted() {
        return this.isAffected( EffectFlag.ROOTED )
    }

    isRunning() {
        return this.isRunningValue
    }

    isShowSummonAnimation(): boolean {
        return this.showSummonAnimation
    }

    isSkillDisabled( skill: Skill ): boolean {
        return this.isAllSkillsDisabled()
    }

    isSleeping() {
        return this.isAffected( EffectFlag.SLEEP )
    }

    isStunned() {
        return this.isAffected( EffectFlag.STUNNED )
    }

    isSweepActive() {
        return false
    }

    isTeleporting() {
        return this.isTeleportingValue
    }

    isTransformed() {
        return false
    }

    isUndead(): boolean {
        return false
    }

    async makeTriggerCast( skill: Skill, target: L2Character, ignoreTargetType: boolean = false ) {
        if ( !skill ) {
            return
        }

        if ( skill.checkCondition( this, target ) ) {
            if ( this.isSkillDisabled( skill ) ) {
                return
            }

            if ( skill.getReuseDelay() > 0 ) {
                this.disableSkill( skill, skill.getReuseDelay() )
            }

            let targets: Array<L2Object> = !ignoreTargetType ? skill.getTargetWithSelectedTarget( this, target ) : [ target ]
            if ( targets.length === 0 ) {
                return
            }

            let skillTarget : L2Object = targets.find( ( currentTarget: L2Object ) => currentTarget.isCharacter() )
            if ( ConfigManager.character.validateTriggerSkills() && this.isPlayable() && skillTarget && skillTarget.isPlayable() ) {
                let player: L2PcInstance = this.getActingPlayer()
                if ( !player.checkPvpSkill( skillTarget, skill ) ) {
                    return
                }
            }

            BroadcastHelper.dataToSelfBasedOnVisibility( this, MagicSkillUse( this.objectId, skillTarget.objectId, skill.getDisplayId(), skill.getLevel(), 0, 0 ) )
            BroadcastHelper.dataToSelfBasedOnVisibility( this, MagicSkillLaunched( this.objectId, skill.getDisplayId(), skill.getLevel(), targets ) )

            return skill.activateCasterSkill( this, targets, true )
        }
    }

    notifyDamageReceived( damage: number, attacker: L2Character, skill: Skill, isCritical: boolean, isDOT: boolean, isReflected: boolean ) {
        if ( ListenerCache.hasGeneralListener( EventType.CharacterReceivedDamage ) ) {
            let eventData = EventPoolCache.getData( EventType.CharacterReceivedDamage ) as CharacterReceivedDamageEvent

            eventData.attackerId = attacker.getObjectId()
            eventData.targetId = this.getObjectId()
            eventData.skillId = skill ? skill.getId() : 0
            eventData.skillLevel = skill ? skill.getLevel() : 0
            eventData.isCritical = isCritical
            eventData.isDOT = isDOT
            eventData.isReflected = isReflected
            eventData.damage = damage

            ListenerCache.sendGeneralEvent( EventType.CharacterReceivedDamage, eventData )
        }
    }

    onDecay() {
        this.getStat().resetTraits()
        this.decayMe()
        this.movementStopTime = 0
    }

    async onTeleported(): Promise<void> {
        if ( !this.isTeleporting() ) {
            return
        }

        this.spawnMe( this.getX(), this.getY(), this.getZ() )
        this.setIsTeleporting( false )

        if ( ListenerCache.hasGeneralListener( EventType.CharacterTeleported ) ) {
            let eventData = EventPoolCache.getData( EventType.CharacterTeleported ) as CharacterTeleportedEvent

            eventData.characterId = this.getObjectId()
            eventData.characterNpcId = this.isNpc() ? this.getId() : 0

            return ListenerCache.sendGeneralEvent( EventType.CharacterTeleported, eventData )
        }
    }

    reduceCurrentHp( damage: number, attacker: L2Character, skill: Skill, isAwake: boolean = true, isDOT: boolean = false ): Promise<void> {
        return this.getStatus().reduceHp( damage, attacker, isAwake, isDOT, false )
    }

    reduceCurrentMp( value: number ) {
        this.getStatus().reduceMp( value )
    }

    removeSkill( skill: Skill, cancelEffect: boolean = true ): Promise<Skill> {
        if ( !skill ) {
            return null
        }

        return this.removeSkillById( skill.getId(), cancelEffect )
    }

    async removeSkillById( skillId: number, cancelEffect: boolean = true ): Promise<Skill> {
        let oldSkill = this.skills[ skillId ]
        _.unset( this.skills, skillId )

        if ( oldSkill ) {
            let lastCastSkill: Skill = this.getLastSkillCast()
            if ( lastCastSkill && this.isCastingNow() ) {
                if ( oldSkill.getId() === lastCastSkill.getId() ) {
                    this.abortCast()
                }
            }

            let simultaneousSkill: Skill = this.getLastSimultaneousSkillCast()
            if ( simultaneousSkill && this.isCastingSimultaneouslyNow ) {
                if ( oldSkill.getId() === simultaneousSkill.getId() ) {
                    this.abortCast()
                }
            }

            if ( cancelEffect || oldSkill.isToggle() || oldSkill.isPassive() ) {
                this.removeStatsOwner( oldSkill )
                await this.stopSkillEffects( false, oldSkill.getId() )
            }
        }

        return oldSkill
    }

    removeStatsOwner( owner: Object, eraseStats: boolean = true ): void {
        let modifiedStats: Set<string> = this.calculators.removeOwner( owner, this.modifiedStats )
        if ( modifiedStats.size === 0 ) {
            return
        }

        if ( eraseStats ) {
            StatsCache.removeStats( this.getObjectId(), modifiedStats )
        }

        /*
            When modified stats are broadcast they should be cleared.
         */
        if ( this.hasLoadedSkills() ) {
            this.debounceBroadcastModifiedStats()
        }
    }

    revalidateZone( force: boolean ) {}

    runDirectAreaRevalidation(): void {}

    sendDamageMessage( target: L2Character, damage: number, mcrit: boolean, powerCrit: boolean, isMiss: boolean ) {}

    sendMessage( text: string ): void {}

    setCurrentCp( maxCp: number ) {
        this.getStatus().setCurrentCp( maxCp )
    }

    setCurrentHp( maxHp: number ) {
        this.getStatus().setCurrentHp( maxHp )
    }

    setMaxStats() {
        this.getStatus().setMaxStats()
    }

    setCurrentMp( maxMp: number ) {
        this.getStatus().setCurrentMp( maxMp )
    }

    setInvisible( value: boolean ): void {
        this.isInvisibleValue = value
        if ( value ) {
            let character = this
            let visiblePlayers: Array<L2PcInstance> = L2World.getVisiblePlayers( this, this.getBroadcastRadius() )

            if ( visiblePlayers.length > 0 ) {
                let packet: Buffer = DeleteObject( this.getObjectId() )

                visiblePlayers.forEach( ( player: L2PcInstance ) => {
                    if ( player && !character.isVisibleFor( player ) ) {
                        ProximalDiscoveryManager.forgetObjectId( player.getObjectId(), character.getObjectId() )
                        player.sendCopyData( packet )
                    }
                } )
            }
        }

        this.broadcastInfo()
    }

    setIsDead( value: boolean ) {
        this.isDeadValue = value
    }

    setIsFlying( value: boolean ): void {
        this.isFlyingValue = value
    }

    setIsImmobilized( value: boolean ) {
        this.isImmobilizedValue = value
    }

    setIsInvulnerable( value: boolean ): void {
        this.isInvulnerableValue = value
    }

    setIsOverloaded( value: boolean ): void {
        this.isOverloadedValue = value
    }

    setIsPendingRevive( value: boolean ) {
        this.isPendingReviveValue = value
    }

    setIsRunning( value: boolean, shouldSendUpdate: boolean = true ) : void {
        if ( this.isRunningValue === value ) {
            return
        }

        this.isRunningValue = value
        if ( this.getRunSpeed() !== 0 ) {
            BroadcastHelper.dataToSelfBasedOnVisibility( this, ChangeMoveType( this.objectId, value ) )
        }

        if ( !this.canBroadcastUpdate() ) {
            return
        }

        if ( this.isPlayer() ) {
            this.getActingPlayer().broadcastUserInfo()
            return
        }

        if ( this.isSummon() ) {
            return this.broadcastStatusUpdate()
        }
    }

    setIsTeleporting( value: boolean ): void {
        this.isTeleportingValue = value
    }

    setLastSimultaneousSkillCast( skill: Skill ) {
        this.lastSimultaneousSkillCast = skill
    }

    setLastSkillCast( skill: Skill ) {
        this.lastSkillCast = skill
    }

    setShowSummonAnimation( value: boolean ) {
        this.showSummonAnimation = value
    }

    setTarget( object: L2Object ) {
        if ( !object || ( object && !object.isVisible() ) ) {
            this.targetId = 0
            return
        }

        if ( this.isPlayable() && this.getObjectId() !== object.getObjectId() ) {
            object.shouldDescribeState( this.getActingPlayer() )
        }

        this.targetId = object.getObjectId()
    }

    setTargetId( objectId: number ): void {
        this.targetId = objectId
    }

    setTitle( name: string ): void {
        this.title = _.truncate( name, { length: 20, omission: '' } )
    }

    setWalking( sendUpdate: boolean = true ) {
        if ( this.isRunning() ) {
            this.setIsRunning( false, sendUpdate )
        }
    }

    addVisualEffect( currentEffect: AbnormalVisualEffect ): void {
        let effectValue = AbnormalVisualEffectMap[ currentEffect ]
        if ( !effectValue ) {
            return
        }

        if ( effectValue.type === AbnormalVisualType.Event ) {
            this.abnormalVisualEffectsEvent |= effectValue.mask
        }

        if ( effectValue.type === AbnormalVisualType.Special ) {
            this.abnormalVisualEffectsSpecial |= effectValue.mask
        }

        this.abnormalVisualEffects |= effectValue.mask
        this.onUpdateAbnormalEffect()
    }

    removeVisualEffect( currentEffect: AbnormalVisualEffect ): void {
        let effectValue = AbnormalVisualEffectMap[ currentEffect ]
        if ( effectValue.type === AbnormalVisualType.Event ) {
            this.abnormalVisualEffectsEvent &= ~effectValue.mask
        }

        if ( effectValue.type === AbnormalVisualType.Special ) {
            this.abnormalVisualEffectsSpecial &= ~effectValue.mask
        }

        this.abnormalVisualEffects &= ~effectValue.mask
        this.onUpdateAbnormalEffect()
    }

    addVisualEffectMasks( skill: Skill ) : void {
        this.abnormalVisualEffects |= skill.maskVisualEffectsStandard
        this.abnormalVisualEffectsEvent |= skill.maskVisualEffectsEvent
        this.abnormalVisualEffectsSpecial |= skill.maskVisualEffectsSpecial
    }

    removeVisualEffectMasks( skill: Skill ) : void {
        this.abnormalVisualEffects &= ~skill.maskVisualEffectsStandard
        this.abnormalVisualEffectsEvent &= ~skill.maskVisualEffectsEvent
        this.abnormalVisualEffectsSpecial &= ~skill.maskVisualEffectsSpecial
    }

    // TODO : convert to be handled by AIController intent
    async startParalyze(): Promise<void> {
        this.abortMoving()
        this.abortAttack()
        this.abortCast()
    }

    startStunning(): void {
        this.abortAttack()
        this.abortCast()
        this.abortMoving()
        this.onUpdateAbnormalEffect()
    }

    stopAllEffects(): Promise<void> {
        return this.effects.stopAllEffects( false )
    }

    stopAllEffectsExceptThoseThatLastThroughDeath(): Promise<void> {
        return this.effects.stopAllEffectsExceptThoseThatLastThroughDeath()
    }

    stopEffects( type: L2EffectType ): Promise<void> {
        return this.effects.stopEffects( type )
    }

    stopEffectsOnDamage( isAwake: boolean ) : Promise<void> {
        if ( !isAwake || !this.effects.hasEffectsRemovedOnDamage() ) {
            return
        }

        return this.effects.stopEffectsOnDamage()
    }

    async stopFakeDeath( removeEffects: boolean ): Promise<void> {
        if ( removeEffects ) {
            await this.stopEffects( L2EffectType.FakeDeath )
        }

        // if this is a player instance, start the grace period for this character (grace from mobs only)!
        if ( this.isPlayer() ) {
            let player = this.getActingPlayer()
            player.setIsFakeDeath( false )
            player.setRecentFakeDeath( true )
        }

        BroadcastHelper.dataToSelfBasedOnVisibility( this, ChangeWaitType( this, ChangeWaitTypeValue.StoppingFakeDeath ) )
        // TODO: Temp hack: players see FD on ppl that are moving: Teleport to someone who uses FD - if he gets up he will fall down again for that client -
        // even tho he is actually standing... Probably bad info in CharInfo packet?
        BroadcastHelper.dataToSelfBasedOnVisibility( this, Revive( this.getObjectId() ) )
    }

    stopHpMpRegeneration() {
        this.getStatus().stopHpMpRegeneration()
    }

    /*
        Stopping of move is opposite of aborting of movement,
        hence notification to AI must happen. Care must be taken to differentiate between two
        since AI event processing of stopping a movement can trigger undesirable consequence
        of just switched AI trait to receive event that it did not cause.
     */
    stopMove(): void {
        if ( !this.isMoving() ) {
            return
        }

        this.abortMoving()
        this.sendStopMovingEvent()
        AIEffectHelper.notifyMoveFinished( this )
    }

    sendStopMovingEvent(): void {}

    stopSkillEffects( isRemoved: boolean, skillId: number ): Promise<void> {
        return this.effects.stopSkillEffects( isRemoved, skillId )
    }

    async stopStunning( removeEffects: boolean ): Promise<void> {
        if ( removeEffects ) {
            await this.stopEffects( L2EffectType.Stun )
        }

        this.onUpdateAbnormalEffect()
    }

    async stopTransformation( removeEffects: boolean ): Promise<void> {
        if ( removeEffects ) {
            await this.getEffectList().stopSkillEffects( false, AbnormalType.TRANSFORM )
        }

        if ( this.isPlayer() ) {
            let player = this as unknown as L2PcInstance
            if ( player.getTransformation() ) {
                await player.untransform()
            }
        }

        this.onUpdateAbnormalEffect()
    }

    teleportToLocation( location: ILocational, allowRandomOffset: boolean, instanceId: number = 0 ): Promise<void> {
        if ( allowRandomOffset ) {
            return teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport,
                this,
                location.getX(),
                location.getY(),
                location.getZ(),
                location.getHeading(),
                instanceId ? instanceId : location.getInstanceId() )
        }

        return this.teleportToLocationCoordinates(
                location.getX(),
                location.getY(),
                location.getZ(),
                location.getHeading(),
                instanceId ? instanceId : location.getInstanceId()
        )
    }

    async teleportToLocationWithOffset( location: ILocational, offset: number = 0 ): Promise<void> {
        return this.teleportToLocationCoordinates(
                location.getX(),
                location.getY(),
                location.getZ(),
                location.getHeading(),
                location.getInstanceId(),
                offset,
        )
    }

    async teleportToLocationCoordinates( x: number, y: number, z: number, heading: number = 0, instanceId: number = 0, offset: number = 0 ): Promise<void> {
        this.setInstanceId( instanceId )
        this.movementStopTime = Date.now()

        if ( this.isPendingRevive() ) {
            this.doRevive()
        }

        if ( this.hasAIController() ) {
            await AIEffectHelper.setNextIntent( this, AIIntent.WAITING )
        }

        this.setIsTeleporting( true )
        this.setTarget( null )

        if ( ConfigManager.character.offsetOnTeleport() && offset > 0 ) {
            x = x + _.random( -offset, offset )
            y = y + _.random( -offset, offset )
        }

        this.setXYZ( x, y, GeoPolygonCache.getZ( x,y,z ) )
        BroadcastHelper.dataToSelfBasedOnVisibility( this, TeleportToLocation( this.objectId, x, y, this.getZ(), heading ) )

        if ( heading !== 0 ) {
            this.setHeading( heading )
        }

        let client: GameClient = L2GameClientRegistry.getClientByPlayerId( this.objectId )
        if ( !this.isPlayer() || ( client && client.isDetached() ) ) {
            await this.onTeleported()
        }

        this.revalidateZone( true )
    }

    onUpdateAbnormalEffect(): void {}

    updateEffectIcons() {}

    setRunning(): void {
        if ( !this.isRunning() ) {
            this.setIsRunning( true )
        }
    }

    disableAllSkills() {
        this.allSkillsDisabled = true
    }

    startPhysicalAttackMuted() {
        this.abortAttack()
    }

    addInvulnerabilityAgainstSkill( id: number, level: number ) {
        let holder = this.getInvulnerabilityAgainstSkills()[ id ]
        if ( !holder ) {
            this.getInvulnerabilityAgainstSkills()[ id ] = new InvulnerableSkillHolder( id, level )
            return
        }

        this.getInvulnerabilityAgainstSkills()[ id ].instances++
    }

    removeInvulnerabilityAgainstSkill( id: number ) {
        let holder = this.getInvulnerabilityAgainstSkills()[ id ]
        if ( !holder ) {
            return
        }

        if ( holder.instances < 1 ) {
            return _.unset( this.invulnerableAgainst, id )
        }

        this.getInvulnerabilityAgainstSkills()[ id ].instances--
    }

    async reduceCurrentHpByDOT( power: number, attacker: L2Character, skill: Skill ): Promise<void> {
        return this.reduceCurrentHp( power, attacker, skill, !skill.isToggle(), true )
    }

    setLethalable( value: boolean ) {
        this.lethalable = value
    }

    setTemplate( template: L2CharacterTemplate ) {
        this.template = template
    }

    onForcedAttack( player: L2PcInstance ): Promise<void> {
        if ( this.isInsidePeaceZone( player ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_IN_PEACEZONE ) )
            player.sendOwnedData( ActionFailed() )
            return
        }

        if ( player.isConfused() ) {
            player.sendOwnedData( ActionFailed() )
            return
        }

        if ( player.isInOlympiadMode() && player.getTarget() && player.getTarget().isPlayable() ) {
            let target: L2PcInstance
            let object: L2Object = player.getTarget()

            if ( object && object.isPlayable() ) {
                target = object.getActingPlayer()
            }

            if ( !target || ( target.isInOlympiadMode() && ( !player.isOlympiadStart() || ( player.getOlympiadGameId() !== target.getOlympiadGameId() ) ) ) ) {
                player.sendOwnedData( ActionFailed() )
                return
            }
        }

        if ( player.getTarget() && !player.getTarget().canBeAttacked() && !player.getAccessLevel().hasPermission( PlayerPermission.AllowPeaceAttack ) ) {
            player.sendOwnedData( ActionFailed() )
            return
        }

        if ( !PathFinding.canSeeTargetWithPosition( player, this ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_SEE_TARGET ) )
            player.sendOwnedData( ActionFailed() )
            return
        }

        if ( player.getBlockCheckerArena() !== -1 ) {
            player.sendOwnedData( ActionFailed() )
            return
        }

        AIEffectHelper.notifyForceAttackWithTargetId( player, this.getObjectId(), 1, 1 )
    }

    isDisarmed() {
        return this.isAffected( EffectFlag.DISARMED )
    }

    moveToLocation( x: number, y: number, z: number, range: number ): boolean {
        if ( this.getMoveSpeed() <= 0 ) {
            return false
        }

        let currentX = this.getX()
        let currentY = this.getY()
        let currentZ = this.getZ()

        let dx = ( x - currentX )
        let dy = ( y - currentY )
        let dz = ( z - currentZ )
        let distance : number = Math.hypot( dx, dy )

        if ( this.isInArea( AreaType.Water ) && distance > 700 ) {
            let divider = 700 / distance
            x = currentX + Math.floor( divider * dx )
            y = currentY + Math.floor( divider * dy )
            z = currentZ + Math.floor( divider * dz )
            dx = ( x - currentX )
            dy = ( y - currentY )
            dz = ( z - currentZ )
            distance = Math.hypot( dx, dy )
        }

        if ( distance < 1 ) {
            return false
        }

        let xRatio: number = dx / distance
        let yRatio: number = dy / distance

        let destinationX : number = x
        let destinationY : number = y

        if ( range > 0 && distance > range ) {
            distance -= range

            destinationX = currentX + Math.floor( distance * xRatio )
            destinationY = currentY + Math.floor( distance * yRatio )
        }

        this.setDestinationCoordinates( destinationX, destinationY, z )

        if ( !( this.isFlying() && dx === 0 && dy === 0 && ( dz !== 0 ) ) ) {
            this.setHeading( GeneralHelper.calculateHeadingFromMovement( xRatio, yRatio ) )
        }

        this.startMoving()

        return true
    }

    isOnGeodataPath() {
        return false
    }

    getXDestination(): number {
        return this.sharedData.getDestinationX()
    }

    getYDestination(): number {
        return this.sharedData.getDestinationY()
    }

    getZDestination(): number {
        return this.sharedData.getDestinationZ()
    }

    validateMovementHeading( heading: number ): boolean {
        if ( !this.isMoving() || this.getHeading() === heading ) {
            return true
        }

        let outcome = this.getHeading() === 0
        this.sharedData.setHeading( heading )

        return outcome
    }

    getInventoryLimit() {
        return 0
    }

    async destroyItemByObjectId( objectId: number, count: number, sendMessage: boolean, reason: string = null ): Promise<boolean> {
        return false
    }

    addStatusListener( objectId: number ) {
        this.getStatus().addStatusListener( objectId )
    }

    removeStatusListener( objectId: number ) {
        this.getStatus().removeStatusListener( objectId )
    }

    isInActiveRegion() {
        let region: L2WorldRegion = this.getWorldRegion()
        return region && region.isActive()
    }

    getSiegeRole() : SiegeRole {
        return SiegeRole.None
    }

    getAllyId() {
        return 0
    }

    getPledgeType() {
        return 0
    }

    getClanId() {
        return 0
    }

    isAcademyMember(): boolean {
        return false
    }

    async doAttack( target: L2Character ): Promise<void> {
        if ( this.physicalDamageData?.isActive ) {
            return
        }

        if ( this.isAlikeDead()
                || !target
                || !target.isCharacter()
                || this.isAttackingDisabled() ) {
            return AIEffectHelper.notifyTargetDead( this, target )
        }

        if ( target.isAlikeDead() ) {
            return AIEffectHelper.notifyTargetDead( this, target )
        }

        if ( this.isPlayer() ) {
            if ( target.isDead() ) {
                AIEffectHelper.notifyTargetDead( this, target )
                return this.sendOwnedData( ActionFailed() )
            }

            let player = this.getActingPlayer()
            if ( player.isTransformed() && !player.getTransformation().canAttack() ) {
                return this.sendOwnedData( ActionFailed() )
            }
        }

        let currentWeapon: L2Weapon = this.getActiveWeaponItem()
        if ( currentWeapon && !currentWeapon.isAttackWeapon && !this.isGM() ) {
            if ( this.isPlayer() ) {
                if ( currentWeapon.getItemType() === WeaponType.FISHINGROD ) {
                    this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_ATTACK_WITH_FISHING_POLE ) )
                } else {
                    this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THAT_WEAPON_CANT_ATTACK ) )
                }

                this.sendOwnedData( ActionFailed() )
            }

            return
        }

        if ( this.getActingPlayer() ) {
            let currentPlayer: L2PcInstance = this.getActingPlayer()
            let targetPlayer: L2PcInstance = target.getActingPlayer()
            if ( currentPlayer.inObserverMode() ) {
                this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.OBSERVERS_CANNOT_PARTICIPATE ) )
                this.sendOwnedData( ActionFailed() )
                return AIEffectHelper.scheduleNextIntent( this, AIIntent.WAITING )
            } else if ( this.isInSiegeArea()
                    && targetPlayer
                    && currentPlayer.getSiegeRole() !== SiegeRole.None
                    && targetPlayer.getSiegeRole() === currentPlayer.getSiegeRole()
                    && targetPlayer.getObjectId() !== this.getObjectId()
                    && targetPlayer.getSiegeSide() === currentPlayer.getSiegeSide() ) {
                if ( TerritoryWarManager.isTWInProgress ) {
                    this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_ATTACK_A_MEMBER_OF_THE_SAME_TERRITORY ) )
                } else {
                    this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.FORCED_ATTACK_IS_IMPOSSIBLE_AGAINST_SIEGE_SIDE_TEMPORARY_ALLIED_MEMBERS ) )
                }

                this.sendOwnedData( ActionFailed() )
                return AIEffectHelper.scheduleNextIntent( this, AIIntent.WAITING )
            } else if ( target.isInsidePeaceZone( currentPlayer ) ) {
                this.sendOwnedData( ActionFailed() )
                AIEffectHelper.scheduleNextIntent( this, AIIntent.WAITING )
                return
            }
        } else if ( this.isInsidePeaceZoneWithObjects( this, target ) ) {
            if ( this.isPlayer() ) {
                this.sendOwnedData( ActionFailed() )
            }

            return AIEffectHelper.scheduleNextIntent( this, AIIntent.WAITING )
        }

        if ( this.effects.hasEffectsRemovedOnAction() ) {
            await this.effects.stopEffectsOnAction()
        }

        if ( !PathFinding.canSeeTargetWithPosition( this, target ) ) {

            if ( this.isPlayer() ) {
                this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_SEE_TARGET ) )
                this.sendOwnedData( ActionFailed() )
            }

            return AIEffectHelper.notifyTargetDead( this, target )
        }

        let weaponItem: L2Weapon = this.getActiveWeaponItem()
        let millisToAttack = this.calculateTimeBetweenAttacks()
        let millisToEngage = millisToAttack

        let attackPacket = new AttackBuilder( this,
                target,
                this.isChargedShot( ShotType.Soulshot ),
                weaponItem ? weaponItem.getItemGradeSPlus() : 0 )

        this.setHeading( GeneralHelper.calculateHeadingFromLocations( this, target ) )

        let isTargetHit = false
        switch ( this.getAttackType() ) {
            case WeaponType.BOW:
                if ( !this.hasArrowsEquipped() ) {
                    await this.checkAndEquipArrows()
                }

                if ( !this.canUseRangeWeapon() ) {
                    return
                }

                millisToEngage = millisToAttack + this.getReuseDelay( weaponItem )
                if ( this.isPlayer() ) {
                    await this.prepareAttackByBow( millisToEngage )
                }

                isTargetHit = await this.doAttackHitByBow( attackPacket, target, millisToAttack )
                break

            case WeaponType.CROSSBOW:
                if ( !this.hasBoltsEquipped() ) {
                    await this.checkAndEquipBolts()
                }

                if ( !this.canUseRangeWeapon() ) {
                    return
                }

                millisToEngage = millisToAttack + this.getReuseDelay( weaponItem )
                if ( this.isPlayer() ) {
                    await this.prepareAttackByCrossBow( millisToEngage )
                }

                isTargetHit = await this.doAttackHitByCrossBow( attackPacket, target, millisToAttack )
                break

            case WeaponType.POLE:
                isTargetHit = this.doAttackHitByPole( attackPacket, target, millisToAttack / 2 )
                break

            case WeaponType.FIST:
                if ( !this.isPlayer() ) {
                    isTargetHit = this.doAttackHitSimple( attackPacket, target, millisToAttack / 2 )
                    break
                }

            case WeaponType.DUAL:
            case WeaponType.DUALFIST:
            case WeaponType.DUALDAGGER:
                isTargetHit = this.doAttackHitByDual( attackPacket, target, millisToAttack / 2 )
                break

            default:
                isTargetHit = this.doAttackHitSimple( attackPacket, target, millisToAttack / 2 )
                break
        }

        let player: L2PcInstance = this.getActingPlayer()
        if ( player ) {
            let summon = player.getSummon()
            if ( summon && summon.getObjectId() !== target.getObjectId() ) {
                player.updatePvPStatusWithTarget( target )
            }
        }

        if ( !isTargetHit ) {
            this.sendOwnedData( ActionFailed() )
        } else {
            this.setChargedShot( ShotType.Soulshot, false )

            if ( player ) {
                if ( player.isCursedWeaponEquipped() ) {
                    if ( !target.isInvulnerable() ) {
                        target.setCurrentCp( 0 )
                    }
                } else if ( player.isHero() ) {
                    if ( target.isPlayer() && target.getActingPlayer().isCursedWeaponEquipped() ) {
                        target.setCurrentCp( 0 )
                    }
                }
            }
        }

        if ( attackPacket.hasHits() ) {
            BroadcastHelper.dataToSelfBasedOnVisibility( this, attackPacket.getBuffer() )
            attackPacket.clear()
        }

        this.refreshFullAttackTimeout( millisToEngage )
    }

    // TODO : possible to cache this value ?
    calculateTimeBetweenAttacks(): number {
        return Math.floor( 500000 / this.getPowerAttackSpeed() )
    }

    calculateReuseTime( weapon: L2Weapon ): number {
        if ( this.isTransformed() ) {
            switch ( this.getAttackType() ) {
                case WeaponType.BOW:
                    return Math.floor( ( 1500 * 333 * this.getStat().getWeaponReuseModifier( null ) ) / this.getStat().getPowerAttackSpeed() )
                case WeaponType.CROSSBOW:
                    return Math.floor( ( 1200 * 333 * this.getStat().getWeaponReuseModifier( null ) ) / this.getStat().getPowerAttackSpeed() )
            }
        }

        if ( !weapon || weapon.getReuseDelay() === 0 ) {
            return 0
        }

        return Math.floor( ( weapon.getReuseDelay() * 333 ) / this.getPowerAttackSpeed() )
    }

    canUseRangeWeapon() : boolean {
        if ( this.isTransformed() ) {
            return true
        }

        let weaponItem: L2Weapon = this.getActiveWeaponItem()

        if ( !weaponItem || !weaponItem.isRanged() ) {
            return false
        }

        return this.getRangedWeaponUseOutcome()
    }

    getRangedWeaponUseOutcome() : boolean {
        return true
    }

    checkAndEquipArrows(): Promise<any> {
        return Promise.resolve()
    }

    checkAndEquipBolts() : Promise<any> {
        return Promise.resolve()
    }

    prepareAttackByBow( millisToAttack: number ) : Promise<void> {
        return Promise.resolve()
    }

    prepareAttackByCrossBow( millisToAttack: number ) : Promise<void> {
        return Promise.resolve()
    }

    async doAttackHitByBow( packet: AttackBuilder, target: L2Character, millisBeforeHit: number ): Promise<boolean> {
        let currentDamage: number = 0
        let shieldUse: number = 0
        let isCrit: boolean = false
        let isMiss: boolean = Formulas.calculateHitMiss( this, target )

        if ( !isMiss ) {
            shieldUse = Formulas.calculateShieldUse( this, target, null, true )
            isCrit = Formulas.calculateCrit( this, target )
            currentDamage = Math.floor( Formulas.calculatePhysicalDamage( this, target, shieldUse, isCrit, packet.hasSoulshot() ) )

            // Bows Ranged Damage Formula (Damage gradually decreases when 60% or lower than full hit range, and increases when 60% or higher).
            // full hit range is 500 which is the base bow range, and the 60% of this is 800.
            currentDamage *= ( this.calculateDistance( target, true ) / 4000 ) + 0.8
        }

        this.physicalDamageData.hits.push( {
            target,
            damage: currentDamage,
            isMiss,
            isCrit
        } )

        this.refreshDamageTimeout( millisBeforeHit )
        packet.addHit( target.getObjectId(), currentDamage, isMiss, isCrit, shieldUse )

        return !isMiss
    }

    notifyAttackAvoided( target: L2Object ) : void {
        /*
            Made only to satisfy skill buff which triggers additional effects when attack is avoided.
            Hence, targeting is only possible for object ids.
         */
        if ( ListenerCache.hasObjectListener( this.getObjectId(), EventType.CreatureAvoidAttack ) ) {
            let data = EventPoolCache.getData( EventType.CreatureAvoidAttack ) as CreatureAvoidAttackEvent

            data.attackerId = this.getObjectId()
            data.targetId = target.getObjectId()
            data.isDOT = false

            ListenerCache.sendObjectEvent( this.getObjectId(), EventType.CreatureAvoidAttack, data )
        }
    }

    async runPhysicalDamageTask(): Promise<void> {
        return this.rechargeShots( true, false )
    }

    private getReflectedDamage( weapon: L2Weapon, target: L2Character, damage: number ) : number {
        if ( target.isInvulnerable()
                || target.isRaid()
                || !weapon
                || weapon.isBow()
                || weapon.isCrossBow() ) {
            return 0
        }

        let player = this.getActingPlayer()
        if ( player && player.getLevel() > ( target.getLevel() + 8 ) ) {
            return 0
        }

        let reflectPercent = target.getStat().calculateStat( Stats.REFLECT_DAMAGE_PERCENT, 0, null, null )

        if ( reflectPercent > 0 ) {
            let reflectedDamage = Math.floor( ( reflectPercent / 100 ) * damage )

            return Math.min( target.getMaxHp(), reflectedDamage )
        }

        return 0
    }

    async applyDamageToTarget( target: L2Character, damage: number, isCrit: boolean ) : Promise<void> {
        let weapon: L2Weapon = this.getActiveWeaponItem()
        let isBow: boolean = weapon && ( weapon.isBow() || weapon.isCrossBow() )
        let reflectedDamage = this.getReflectedDamage( weapon, target, damage )

        await target.reduceCurrentHp( damage, this, null )
        target.notifyDamageReceived( damage, this, null, isCrit, false, false )

        if ( reflectedDamage > 0 ) {
            if ( target.isPlayable() ) {
                let message = new SystemMessageBuilder( SystemMessageIds.C1_DONE_S3_DAMAGE_TO_C2 )
                        .addCharacterName( target )
                        .addCharacterName( this )
                        .addNumber( reflectedDamage )
                        .getBuffer()

                target.sendOwnedData( message )
            }

            if ( this.isSummon() ) {
                let message = new SystemMessageBuilder( SystemMessageIds.C1_RECEIVED_DAMAGE_OF_S3_FROM_C2 )
                        .addCharacterName( this )
                        .addCharacterName( target )
                        .addNumber( reflectedDamage )
                        .getBuffer()

                this.sendOwnedData( message )
            }

            await this.reduceCurrentHp( reflectedDamage, target, null, true, false )
            this.notifyDamageReceived( reflectedDamage, target, null, isCrit, false, true )
        }

        if ( !isBow ) {
            let absorbHPPercent = this.getStat().calculateStat( Stats.ABSORB_DAMAGE_PERCENT, 0, null, null )

            if ( absorbHPPercent > 0 ) {
                let maxCanAbsorb = Math.floor( this.getMaxRecoverableHp() - this.getCurrentHp() )
                let absorbDamage = Math.floor( ( absorbHPPercent / 100 ) * damage )

                if ( absorbDamage > maxCanAbsorb ) {
                    absorbDamage = maxCanAbsorb
                }

                if ( absorbDamage > 0 ) {
                    this.setCurrentHp( this.getCurrentHp() + absorbDamage )
                }
            }

            let absorbMPPercent = this.getStat().calculateStat( Stats.ABSORB_MANA_DAMAGE_PERCENT, 0, null, null )

            if ( absorbMPPercent > 0 ) {
                let maxCanAbsorb = Math.floor( this.getMaxRecoverableMp() - this.getCurrentMp() )
                let absorbDamage = Math.floor( ( absorbMPPercent / 100 ) * damage )

                if ( absorbDamage > maxCanAbsorb ) {
                    absorbDamage = maxCanAbsorb
                }

                if ( absorbDamage > 0 ) {
                    this.setCurrentMp( this.getCurrentMp() + absorbDamage )
                }
            }

        }

        if ( !target.isRaid() && Formulas.calculateAttackBreak( target, damage ) ) {
            target.notifyAttackInterrupted()
            target.attemptCastStop()
        }

        if ( this.hasTriggerSkills() ) {
            await this.applyTriggerSkills( target, isCrit )
        }

        if ( isCrit && weapon && weapon.hasSkillOnCrit() ) {
            await weapon.castOnCriticalSkill( this, target )
        }
    }

    async doAttackHitByCrossBow( packet: AttackBuilder, target: L2Character, millisToAttack: number ): Promise<boolean> {
        let currentDamage = 0
        let shieldUse = 0
        let isCrit: boolean = false
        let isMiss: boolean = Formulas.calculateHitMiss( this, target )

        if ( !isMiss ) {
            shieldUse = Formulas.calculateShieldUse( this, target, null, true )
            isCrit = Formulas.calculateCrit( this, target )
            currentDamage = Math.floor( Formulas.calculatePhysicalDamage( this, target, shieldUse, isCrit, packet.hasSoulshot() ) )
        }

        this.physicalDamageData.hits.push( {
            target,
            damage: currentDamage,
            isMiss,
            isCrit
        } )

        this.refreshDamageTimeout( millisToAttack )

        packet.addHit( target.getObjectId(), currentDamage, isMiss, isCrit, shieldUse )

        return !isMiss
    }

    doAttackHitByPole( packet: AttackBuilder, target: L2Character, timeToHit: number ) {
        let isHit = this.doAttackHitSimple( packet, target, 100, timeToHit )

        if ( this.isAffected( EffectFlag.SINGLE_TARGET ) ) {
            return isHit
        }

        let maxRadius = this.getStat().getPhysicalAttackRange()
        let maxAngleDiff = this.getStat().getPhysicalAttackAngle()

        // o1 x: 83420 y: 148158 (Giran)
        // o2 x: 83379 y: 148081 (Giran)
        // dx = -41
        // dy = -77
        // distance between o1 and o2 = 87.24
        // arctan2 = -120 (240) degree (excel arctan2(dx, dy); java arctan2(dy, dx))
        //
        // o2
        //
        // o1 ----- (heading)
        // In the diagram above:
        // o1 has a heading of 0/360 degree from horizontal (facing East)
        // Degree of o2 in respect to o1 = -120 (240) degree
        //
        // o2 / (heading)
        // /
        // o1
        // In the diagram above
        // o1 has a heading of -80 (280) degree from horizontal (facing north east)
        // Degree of o2 in respect to 01 = -40 (320) degree

        // H5 Changes: without Polearm Mastery (skill 216) max simultaneous attacks is 3 (1 by default + 2 in skill 3599).
        let attackRandomCountMax = Math.floor( this.getStat().calculateStat( Stats.ATTACK_COUNT_MAX, 1, null, null ) )
        let attackcount = 0
        let attackpercent = 85
        let character = this

        const predicate = ( object: L2Object ): void | boolean => {
            if ( !object.isCharacter() || object.getObjectId() === target.getObjectId() ) {
                return
            }

            if ( object.isPet() && character.isPlayer() && ( ( object as L2PetInstance ).getOwnerId() === character.getActingPlayer().getObjectId() ) ) {
                return
            }

            if ( Math.abs( object.getZ() - character.getZ() ) > 650 ) {
                return
            }

            if ( !character.isFacing( object, maxAngleDiff ) ) {
                return
            }

            if ( character.isAttackable() && object.isPlayer() && object.isAttackable() ) {
                return
            }

            if ( character.isAttackable() && object.isAttackable() && !( character as unknown as L2Attackable ).isChaos() ) {
                return
            }

            if ( ( object as L2Character ).isAlikeDead() || !( object as L2Character ).isAutoAttackable( character ) ) {
                return
            }

            isHit = isHit || character.doAttackHitSimple( packet, object as L2Character, timeToHit, attackpercent )
            attackpercent /= 1.15

            attackcount++
            return attackcount <= attackRandomCountMax
        }

        L2World.forEachObjectByPredicate( this, maxRadius, predicate, false )

        return isHit
    }

    doAttackHitSimple( packet: AttackBuilder, target: L2Character, timeToHit: number, attackPercent: number = 100 ): boolean {
        let currentDamage = 0
        let shieldUse = 0
        let isCrit = false
        let isMiss = Formulas.calculateHitMiss( this, target )

        if ( !isMiss ) {
            shieldUse = Formulas.calculateShieldUse( this, target, null, false )
            isCrit = Formulas.calculateCrit( this, target )
            currentDamage = Math.floor( Formulas.calculatePhysicalDamage( this, target, shieldUse, isCrit, packet.hasSoulshot() ) )

            if ( attackPercent !== 100 ) {
                currentDamage = Math.round( ( currentDamage * attackPercent ) / 100 )
            }
        }

        this.physicalDamageData.hits.push( {
            target,
            damage: currentDamage,
            isMiss,
            isCrit
        } )

        this.refreshDamageTimeout( timeToHit )
        packet.addHit( target.getObjectId(), currentDamage, isMiss, isCrit, shieldUse )

        return !isMiss
    }

    doAttackHitByDual( packet: AttackBuilder, target: L2Character, timeToHit: number ): boolean {
        let damageOne = 0
        let damageTwo = 0
        let shieldUseOne = 0
        let shieldUseTwo = 0
        let isCritOne = false
        let isCritTwo = false

        let isMissOne = Formulas.calculateHitMiss( this, target )
        let isMissTwo = Formulas.calculateHitMiss( this, target )

        // TODO : use weapon dual modifier for each hit, usually around 60% first then 40% next one
        if ( !isMissOne ) {
            shieldUseOne = Formulas.calculateShieldUse( this, target, null, false )
            isCritOne = Formulas.calculateCrit( this, target )
            damageOne = Math.floor( Formulas.calculatePhysicalDamage( this, target, shieldUseOne, isCritOne, packet.hasSoulshot() ) / 2 )
        }

        if ( !isMissTwo ) {
            shieldUseTwo = Formulas.calculateShieldUse( this, target, null, false )
            isCritTwo = Formulas.calculateCrit( this, target )
            damageTwo = Math.floor( Formulas.calculatePhysicalDamage( this, target, shieldUseTwo, isCritTwo, packet.hasSoulshot() ) / 2 )
        }

        this.physicalDamageData.hits.push( {
            damage: damageOne,
            target,
            isMiss: isMissOne,
            isCrit: isCritOne
        },
        {
            damage: damageTwo,
            target,
            isMiss: isMissTwo,
            isCrit: isCritTwo
        } )

        let hitDuration = timeToHit / 2
        this.refreshDamageTimeout( hitDuration )

        packet.addHit( target.getObjectId(), damageOne, isMissOne, isCritOne, shieldUseOne )
        packet.addHit( target.getObjectId(), damageTwo, isMissTwo, isCritTwo, shieldUseTwo )

        return !isMissOne || !isMissTwo
    }

    private refreshDamageTimeout( delay: number ) : void {
        if ( delay === this.physicalDamageData.hitDelay ) {
            this.physicalDamageData.hitTimeout.refresh()
            return
        }

        this.physicalDamageData.hitDelay = delay
        this.physicalDamageData.hitTimeout = setTimeout( this.runPhysicalDamageMethod, delay )
    }

    private refreshFullAttackTimeout( delay: number ) : void {
        this.physicalDamageData.isActive = true

        if ( this.physicalDamageData.attackDelay === delay ) {
            this.physicalDamageData.attackTimeout.refresh()
            return
        }

        this.physicalDamageData.attackDelay = delay
        this.physicalDamageData.attackTimeout = setTimeout( () => {
            if ( !this.physicalDamageData.isActive ) {
                return
            }

            this.physicalDamageData.isActive = false
            AIEffectHelper.notifyReadyForAttack( this )
        }, delay )
    }

    resetPhysicalDamageData() : void {
        if ( !this.physicalDamageData ) {
            return
        }

        clearTimeout( this.physicalDamageData.attackTimeout )
        clearTimeout( this.physicalDamageData.hitTimeout )

        this.physicalDamageData = null
    }

    getPhysicalAttackRange(): number {
        return this.getStat().getPhysicalAttackRange()
    }

    getMagicalAttackRange( skill: Skill ): number {
        return this.getStat().getMagicalAttackRange( skill )
    }

    hasRandomWalk(): boolean {
        return false
    }

    startMoving() {
        if ( this.isMoving() ) {
            return
        }

        // TODO : what if we're in water and we need to use different speed?
        this.sharedData.setMoveSpeed( this.getMoveSpeed() )
        this.sharedData.setCollisionRadius( this.getCollisionRadius() )
        this.sharedData.setType( this.instanceType )

        this.isInArea( AreaType.Water ) ? this.sharedData.addDataFlag( DataFlag.InWater ) : this.sharedData.removeDataFlag( DataFlag.InWater )
        this.sharedData.addDataFlag( DataFlag.Moving )

        L2MoveManager.startMovement( this )

        if ( !this.updateMovingParametersTask ) {
            this.updateMovingParametersTask = setInterval( this.runUpdateMovingParametersTask.bind( this ), 400 )
        }
    }

    /*
        Aborting of movement implies sudden, un-planned stop,
        hence no notification to AI is required.
     */
    abortMoving() : void {
        if ( !this.isMoving() ) {
            return
        }

        L2MoveManager.stopMoving( this )
        this.onStopMoving( false )

        BroadcastHelper.dataToSelfBasedOnVisibility( this, StopMove( this ) )
    }

    onStopMoving( notify: boolean ) {
        this.sharedData.removeDataFlag( DataFlag.Moving )
        this.movementStopTime = Date.now()

        clearInterval( this.updateMovingParametersTask )
        this.updateMovingParametersTask = null

        this.updateSpatialIndex()
        this.updateWorldRegion()

        if ( notify ) {
            AIEffectHelper.notifyMoveFinished( this )
        }
    }

    runUpdateMovingParametersTask() {
        this.updateSpatialIndex()
        this.revalidateZone( false )
        this.updateWorldRegion()

        this.sharedData.setMoveSpeed( this.getMoveSpeed() )
        this.isInArea( AreaType.Water ) ? this.sharedData.addDataFlag( DataFlag.InWater ) : this.sharedData.removeDataFlag( DataFlag.InWater )
    }

    createAIController(): IAIController {
        return
    }

    getAIController(): IAIController {
        return
    }

    hasAIController() {
        return false
    }

    setAIController( value: IAIController ): void {}

    setDefaultTraits( traits: TraitSettings ): void {
        return
    }

    getBuffCount(): number {
        return this.effects.getBuffCount()
    }

    getCollisionHeight(): number {
        return 1
    }

    isUnderAttack(): boolean {
        return AggroCache.hasAggroData( this.getObjectId() )
    }

    async loadTemplateSkills( template: L2NpcTemplate ): Promise<void> {
        this.skills = template.getSkillMap()

        await aigle.resolve( template.getPassiveSkills() ).each( ( skill: Skill ) => skill.applyEffects( this, this, false, true, false, 0 ) )

        if ( this.getId() !== 0 ) {
            /*
                It is possible for npc stats to be partially initialized due to calls to get cached stats
                before all skills are loaded. Hence, these would need to be removed and all stats recalculated.
             */
            StatsCache.removeAllStats( this.getObjectId() )

            if ( StatsCache.hasStats( this.getId() ) ) {
                StatsCache.duplicateStats( this.getId(), this.getObjectId() )
            } else {
                this.generateTemplateStats()
                StatsCache.duplicateStats( this.getObjectId(), this.getId() )
            }
        }

        this.getStatus().setMissingStats()
    }

    getMovementId(): number {
        return this.sharedData.getMovementId()
    }

    // TODO : integrate with manually spawned npcs that get deleted
    removeSharedData() {
        L2MoveManager.unRegisterMovingCharacter( this )
    }

    getBonusWeightPenalty(): number {
        return 0
    }

    /*
        Invoke specific stats to populate cached values in StatsCache
        Please note only stats without extra parameters.
     */
    generateTemplateStats(): void {
        this.getStat().getMaxHp()
        this.getStat().getMaxMp()
        this.getStat().getMaxRecoverableHp()
        this.getStat().getMaxRecoverableMp()

        this.getStat().getMoveSpeed()
        this.getStat().getWalkSpeed()
        this.getStat().getMovementSpeedMultiplier()
        this.getStat().getSwimRunSpeed()
        this.getStat().getSwimWalkSpeed()

        this.getStat().getRunSpeed()
        this.getStat().getPowerAttackSpeed()
        this.getStat().getMagicAttackSpeed()
        this.getStat().getAccuracy()
        this.getStat().getAttackSpeedMultiplier()

        this.getStat().getCON()
        this.getStat().getDEX()
        this.getStat().getINT()
        this.getStat().getMEN()
        this.getStat().getSTR()
        this.getStat().getWIT()
    }

    getMovementType() : MovementType {
        if ( this.isInArea( AreaType.Water ) ) {
            return MovementType.Swimming
        }

        return this.isFlying() ? MovementType.Flying : MovementType.Walking
    }

    sendOwnedData( data: Buffer ) : void {
        return
    }

    canBroadcastUpdate() : boolean {
        let region = this.getWorldRegion()
        return region && region.isActive()
    }

    abortCastOne() : void {
        if ( this.skillCast ) {
            this.skillCast.abortCast()
        }

        this.skillCast = null
    }

    abortCastTwo() : void {
        if ( this.skillCastSimultaneous ) {
            this.skillCastSimultaneous.abortCast()
        }

        this.skillCastSimultaneous = null
    }

    clearCastProcess( process : MagicProcess ) : void {
        if ( this.skillCast === process ) {
            this.skillCast = null
            return
        }

        if ( this.skillCastSimultaneous === process ) {
            this.skillCastSimultaneous = null
            return
        }
    }

    onDeathInZones() : void {
        L2World.getEnteredAreas( this ).forEach( ( area: L2WorldArea ) => {
            if ( area.hasAction( this, WorldAreaActions.Death ) ) {
                area.processAction( this, WorldAreaActions.Death )
            }
        } )
    }

    stopAttackTimers() : void {
        /*
            Note about keeping timer objects. Calling clearTimeout
            on any timers will render timer non-reusable (cannot call to refresh),
            hence boolean values are used to avoid cancelling timers.
         */
        if ( !this.physicalDamageData || !this.physicalDamageData.isActive ) {
            return
        }

        this.physicalDamageData.isActive = false
        this.physicalDamageData.hits.length = 0
    }

    getGeometryId(): GeometryId {
        return this.getTemplate().dropGeometryId
    }

    hasArrowsEquipped() : boolean {
        return true
    }

    hasBoltsEquipped() : boolean {
        return true
    }

    getReuseDelay( weapon: L2Weapon ) : number {
        return this.calculateReuseTime( weapon )
    }

    applyTriggerSkills( target: L2Character, isCrit: boolean ) : Promise<void> {
        return Promise.resolve()
    }

    hasTriggerSkills() : boolean {
        return false
    }

    applySkillTriggerSkills( skill: Skill, target: L2Character ) : Promise<void> {
        return Promise.resolve()
    }

    getCollisionRadius() : number {
        return 1
    }

    setWorldRegion( region: L2WorldRegion ) {
        if ( region && region.isActive() && this.debounceAreaRevalidation ) {
            this.debounceAreaRevalidation()
        }

        this.worldRegion = region
    }

    isInArea( type: AreaType ) : boolean {
        return false
    }

    isInSiegeArea() : boolean {
        return false
    }
}