import { IAIController } from '../../aicontroller/interface/IAIController'
import { IIdentifiable } from '../IIdentifiable'
import { TraitSettings } from '../../aicontroller/interface/IAITrait'

export interface AIControllerCapable extends IIdentifiable {
    createAIController(): IAIController
    getAIController(): IAIController
    hasAIController(): boolean
    setAIController( value: IAIController ): void
    setDefaultTraits( traits: TraitSettings ): void
}