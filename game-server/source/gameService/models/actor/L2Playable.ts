import { L2Character } from './L2Character'
import { PlayableStats } from './stat/PlayableStats'
import { InstanceType } from '../../enums/InstanceType'
import { L2CharacterTemplate } from './templates/L2CharacterTemplate'
import { EffectFlag } from '../../enums/EffectFlag'
import { Skill } from '../Skill'
import { L2PcInstance } from './instance/L2PcInstance'
import { L2World } from '../../L2World'
import { TeleportWhereType } from '../../enums/TeleportWhereType'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { EtcStatusUpdate } from '../../packets/send/EtcStatusUpdate'
import { InstanceManager } from '../../instancemanager/InstanceManager'
import { Instance } from '../entity/Instance'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { Die } from '../../packets/send/Die'
import { EventTerminationResult, EventType, PlayableKilledEvent, PlayerKilledEvent } from '../events/EventType'
import { ListenerCache } from '../../cache/ListenerCache'
import { EventPoolCache } from '../../cache/EventPoolCache'
import { TraitRegistry } from '../../aicontroller/TraitRegistry'
import {
    NextEffectFunction,
    NextTraitActionFunction,
    PlayableAIController
} from '../../aicontroller/types/PlayableAIController'
import { ExSendUIEvent } from '../../packets/send/ExSendUIEvent'
import { L2ItemInstance } from '../items/instance/L2ItemInstance'
import { PlayerPvpFlag } from '../../enums/PlayerPvpFlag'
import { AIEffect } from '../../aicontroller/enums/AIEffect'
import { ConfigManager } from '../../../config/ConfigManager'
import { CommonSkill } from '../holders/SkillHolder'
import { ActionFailed } from '../../packets/send/ActionFailed'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { L2TargetType } from '../skills/targets/L2TargetType'
import { ILocational } from '../Location'
import { L2Weapon } from '../items/L2Weapon'
import { L2Object } from '../L2Object'
import { MagicSkillUse } from '../../packets/send/MagicSkillUse'
import { MagicSkillLaunched } from '../../packets/send/MagicSkillLaunched'
import { L2ReuseTime } from '../SkillTimestamp'
import { ItemReuseTime, SkillReuseTime } from '../ReuseTime'
import { ExpirationTime } from '../ExpirationTime'
import { getTeleportLocation } from '../../helpers/TeleportHelper'
import { AreaType } from '../areas/AreaType'
import { Team } from '../../enums/Team'
import { AreaDiscoveryManager } from '../../cache/AreaDiscoveryManager'

export class L2Playable extends L2Character {
    lockedTargetId: number
    transferDamageToPlayerId: number
    aiController: PlayableAIController
    skillReuse: SkillReuseTime = new SkillReuseTime()
    itemReuse: ItemReuseTime = new ItemReuseTime()
    disabledSkills: ExpirationTime = new ExpirationTime()
    team: number = Team.None

    constructor( objectId: number, template: L2CharacterTemplate ) {
        super( objectId, template )

        this.instanceType = InstanceType.L2Playable
        this.setIsInvulnerable( false )
        this.lockedTargetId = null
        this.transferDamageToPlayerId = null

        AreaDiscoveryManager.addCharacter( this, false )
    }

    createAIController(): PlayableAIController {
        return new PlayableAIController( this, TraitRegistry.getPlayableTraits( this ) )
    }

    getAIController(): PlayableAIController {
        if ( !this.aiController ) {
            this.aiController = this.createAIController()
        }

        return this.aiController
    }

    hasAIController() {
        return !!this.aiController
    }

    setAIController( value: PlayableAIController ): void {
        let controller = this.aiController
        this.aiController = value

        if ( !controller ) {
            return
        }

        return controller.deactivate()
    }

    getStat(): PlayableStats {
        return super.getStat() as PlayableStats
    }

    updateEffectIcons(): void {
        return this.getEffectList().updateEffectIconsList()
    }

    isResurrectSpecialAffected(): boolean {
        return this.isAffected( EffectFlag.RESURRECTION_SPECIAL )
    }

    getExp(): number {
        return 0
    }

    getSp(): number {
        return 0
    }

    getKarma(): number {
        return 0
    }

    getPvpFlag(): PlayerPvpFlag {
        return PlayerPvpFlag.None
    }

    async storeMe(): Promise<void> {
    }

    async storeEffect( store: boolean ): Promise<void> {
    }

    async restoreEffects(): Promise<void> {
    }

    isPlayable(): boolean {
        return true
    }

    async onLevelChange( isIncreased: boolean ): Promise<void> {
    }

    async doPickupItem( item: L2ItemInstance ): Promise<void> {
    }

    async useMagic( skill: Skill, forceUse: boolean, dontMove: boolean ): Promise<boolean> {
        return false
    }

    addLevel( amount: number ): Promise<void> {
        return Promise.resolve()
    }

    getTransferringDamageTo(): L2PcInstance {
        return L2World.getPlayer( this.transferDamageToPlayerId )
    }

    getLockedTarget(): L2Character {
        return L2World.getObjectById( this.lockedTargetId ) as L2Character
    }

    isLockedTarget(): boolean {
        return !!this.lockedTargetId
    }

    isProtectionBlessingAffected(): boolean {
        return this.isAffected( EffectFlag.PROTECTION_BLESSING )
    }

    isSilentMovingAffected(): boolean {
        return this.isAffected( EffectFlag.SILENT_MOVE )
    }

    isNoblesseBlessedAffected(): boolean {
        return this.isAffected( EffectFlag.NOBLESS_BLESSING )
    }

    canBeAttacked(): boolean {
        return true
    }

    teleportToLocationType( generalLocation: TeleportWhereType ): Promise<void> {
        return this.teleportToLocation( getTeleportLocation( this, generalLocation ), true )
    }

    checkIfPvP( target: L2Character ) {
        if ( !target ) {
            return false
        }

        if ( target.objectId === this.objectId ) {
            return false
        }

        if ( !target.isPlayable() ) {
            return false
        }

        let player: L2PcInstance = this.getActingPlayer()
        if ( !player ) {
            return false
        }

        let targetPlayer: L2PcInstance = target.getActingPlayer()
        if ( !targetPlayer ) {
            return false
        }

        if ( targetPlayer === player ) {
            return false
        }

        if ( player.getKarma() !== 0 || targetPlayer.getKarma() !== 0 ) {
            return false
        }

        if ( player.isInArea( AreaType.PVP ) && targetPlayer.isInArea( AreaType.PVP ) ) {
            return true
        }

        if ( player.getClan()
            && targetPlayer.getClan()
            && targetPlayer.getClan().isAtWarWith( player.getClanId() )
            && player.getClan().isAtWarWith( targetPlayer.getClanId() )
            && ( player.getWantsPeace() === 0 )
            && ( targetPlayer.getWantsPeace() === 0 )
            && !player.isAcademyMember()
            && !targetPlayer.isAcademyMember() ) {
            return true
        }

        return targetPlayer.hasPvPFlag()
    }

    setLockedTarget( character: L2Character ) {
        this.lockedTargetId = character.getObjectId()
    }

    setTransferDamageTo( player: L2PcInstance ) {
        this.transferDamageToPlayerId = player.getObjectId()
    }

    async doDie( attacker: L2Character ): Promise<boolean> {
        // TODO : multiple events exist for player dying/dead (see one below too), keep only one of these since it is confusing to have many types
        if ( ListenerCache.hasGeneralListener( EventType.PlayableKilled ) ) {
            let eventData = EventPoolCache.getData( EventType.PlayableKilled ) as PlayableKilledEvent

            eventData.attackerId = attacker ? attacker.getObjectId() : 0
            eventData.targetId = this.getObjectId()
            eventData.isPlayer = this.isPlayer()

            let result: EventTerminationResult = await ListenerCache.getGeneralTerminatedResult( EventType.PlayableKilled, eventData )
            if ( result && result.terminate ) {
                return false
            }
        }

        if ( this.isDead() ) {
            return false
        }

        this.setCurrentHp( 0 )
        this.setIsDead( true )

        this.setTarget( null )
        this.abortMoving()
        this.getStatus().stopHpMpRegeneration()

        let deleteBuffs: boolean = true

        if ( this.isNoblesseBlessedAffected() ) {
            await this.stopEffects( L2EffectType.NoblesseBlessing )
            deleteBuffs = false
        }

        if ( this.isResurrectSpecialAffected() ) {
            await this.stopEffects( L2EffectType.ResurrectionRecovery )
            deleteBuffs = false
        }

        if ( this.isPlayer() ) {
            let player: L2PcInstance = this as unknown as L2PcInstance

            if ( player.hasCharmOfCourage() ) {
                if ( player.isInSiege() ) {
                    player.reviveRequest( player, false, 0, 0 )
                }

                player.setCharmOfCourage( false )
                player.sendDebouncedPacket( EtcStatusUpdate )
            }

            if ( ListenerCache.hasGeneralListener( EventType.PlayerKilled ) ) {
                let data = EventPoolCache.getData( EventType.PlayerKilled ) as PlayerKilledEvent

                data.attackerId = attacker ? attacker.getObjectId() : 0
                data.attackerInstanceType = attacker ? attacker.getInstanceType() : InstanceType.L2Object
                data.targetId = this.getObjectId()
                data.instanceId = this.getInstanceId()
                data.targetLevel = this.getLevel()
                data.attackerLevel = attacker ? attacker.getLevel() : 0
                data.targetClassId = player.getClassId()

                await ListenerCache.sendGeneralEvent( EventType.PlayerKilled, data )
            }
        }

        if ( deleteBuffs ) {
            await this.stopAllEffectsExceptThoseThatLastThroughDeath()
        }

        this.broadcastStatusUpdate()
        this.onDeathInZones()

        if ( this.getInstanceId() > 0 ) {
            let instance: Instance = InstanceManager.getInstance( this.getInstanceId() )
            if ( instance ) {
                instance.notifyDeath( this )
            }
        }

        if ( attacker ) {
            let killerPlayer: L2PcInstance = attacker.getActingPlayer()

            if ( killerPlayer ) {
                await killerPlayer.onKillUpdatePvPKarma( this )
            }
        }

        this.getAIController().deactivate()
        BroadcastHelper.dataToSelfInRange( this, Die( this ) )

        this.updateEffectIcons()
        return true
    }

    sendInstanceUpdate( instance: Instance, shouldHide: boolean ): void {
        let startTime = Math.floor( ( Date.now() - instance.getInstanceStartTime() ) / 1000 )
        let endTime = Math.floor( ( instance.getInstanceEndTime() - instance.getInstanceStartTime() ) / 1000 )

        let playerId = this.getActingPlayer().getObjectId()
        if ( instance.isTimerIncrease() ) {
            return this.sendOwnedData( ExSendUIEvent( playerId, shouldHide, true, startTime, endTime, -1, instance.getTimerText() ) )
        }

        this.sendOwnedData( ExSendUIEvent( playerId, shouldHide, false, endTime - startTime, 0, -1, instance.getTimerText() ) )
    }

    setAITraitAction( method: NextTraitActionFunction ): void {
        this.getAIController().traitAction = method
    }

    setAIEffectAction( method: NextEffectFunction, ...effects: Array<AIEffect> ): void {
        this.getAIController().setNextEffectAction( method, effects )
    }

    async runPhysicalDamageTask(): Promise<void> {
        let parameters = this.physicalDamageData.hits.pop()
        if ( !parameters ) {
            return
        }

        let isMiss = parameters.isMiss
        let isCrit = parameters.isCrit
        let target = parameters.target

        if ( isMiss ) {
            this.notifyAttackAvoided( target )
        }

        this.sendDamageMessage( target, parameters.damage, false, isCrit, isMiss )
        await this.rechargeShots( true, false )

        if ( target.isRaid()
            && target.hasRaidCurse()
            && ConfigManager.npc.raidCurse()
            && this.getLevel() > ( target.getLevel() + 8 ) ) {
            let skill: Skill = CommonSkill.RAID_CURSE2.getSkill()

            if ( skill ) {
                this.abortAttack()
                this.abortCast()

                return skill.applyEffects( target, this )
            }

            return
        }

        if ( isMiss || parameters.damage < 1 ) {
            return
        }

        this.addFightStance()

        if ( this.physicalDamageData.hits.length > 0 ) {
            this.physicalDamageData.hitTimeout.refresh()
        }

        return this.applyDamageToTarget( target, parameters.damage, isCrit )
    }

    addFightStance(): void {
    }

    canCastSkill( skill: Skill ): boolean {
        if ( !skill || this.isSkillDisabled( skill ) || ( skill.isFlyType() && this.isMovementDisabled() ) ) {
            this.sendOwnedData( ActionFailed() )
            return false
        }

        if ( this.getCurrentMp() < ( this.getStat().getMpConsume1( skill ) + this.getStat().getMpConsume2( skill ) ) ) {
            this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_MP ) )
            this.sendOwnedData( ActionFailed() )
            return false
        }

        if ( this.getCurrentHp() <= skill.getHpConsume() ) {
            this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_HP ) )
            this.sendOwnedData( ActionFailed() )
            return false
        }

        if ( !skill.isStatic() && ( ( skill.isMagic() && this.isMuted() ) || this.isPhysicalMuted() ) ) {
            this.sendOwnedData( ActionFailed() )
            return false
        }

        if ( skill.isChanneling() && skill.getChannelingSkillId() > 0 ) {
            let canCast = true
            if ( ( skill.getTargetType() === L2TargetType.GROUND ) && this.isPlayer() ) {
                let position: ILocational = this.getActingPlayer().getGroundSkillLocation()
                if ( L2World.isInAreaRange( position, skill.getEffectRange(), AreaType.Peace ) ) {
                    canCast = false
                }

            } else if ( L2World.isInAreaRange( this, skill.getEffectRange(), AreaType.Peace ) ) {
                canCast = false
            }

            if ( !canCast ) {
                let packet = new SystemMessageBuilder( SystemMessageIds.S1_CANNOT_BE_USED )
                    .addSkillName( skill )
                    .getBuffer()

                this.sendOwnedData( packet )
                return false
            }
        }

        if ( this.getActiveWeaponItem() ) {
            let weapon: L2Weapon = this.getActiveWeaponItem()
            if ( weapon.useWeaponSkillsOnly && !this.isGM() && weapon.hasSkills() ) {
                if ( !weapon.hasSkillWithId( skill.getId() ) ) {
                    if ( this.isPlayable() ) {
                        this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.WEAPON_CAN_USE_ONLY_WEAPON_SKILL ) )
                    }

                    return false
                }
            }
        }

        if ( skill.getItemConsumeId() > 0 && this.getInventory() ) {
            let requiredItems: L2ItemInstance = this.getInventory().getItemByItemId( skill.getItemConsumeId() )

            if ( !requiredItems || ( requiredItems.getCount() < skill.getItemConsumeCount() ) ) {
                if ( skill.hasEffectType( L2EffectType.Summon ) ) {
                    let packet = new SystemMessageBuilder( SystemMessageIds.SUMMONING_SERVITOR_COSTS_S2_S1 )
                        .addItemNameWithId( skill.getItemConsumeId() )
                        .addNumber( skill.getItemConsumeCount() )
                        .getBuffer()
                    this.sendOwnedData( packet )
                } else {
                    this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THERE_ARE_NOT_ENOUGH_NECESSARY_ITEMS_TO_USE_THE_SKILL ) )
                }

                return false
            }
        }

        return true
    }

    /*
        Shortened form of casting without any additional checks or processes (like MagicProcess).
        - meant to execute skill for herbs
        - allows very fast execution and non-blocking/non-cancellable cast compared to normal skill launch path
     */
    async doImmediateCast( skill: Skill ): Promise<boolean> {
        if ( !this.canCastSkill( skill ) ) {
            return true
        }

        if ( this.isSkillDisabled( skill ) ) {
            return true
        }

        if ( skill.getReuseDelay() > 0 ) {
            this.disableSkill( skill, skill.getReuseDelay() )
        }

        let target = skill.getSingleTargetOf( this ) as L2Character
        let targets: Array<L2Object> = skill.getTargetsOf( this )

        BroadcastHelper.dataToSelfBasedOnVisibility( this, MagicSkillUse( this.objectId, target.getObjectId(), skill.getDisplayId(), skill.getLevel(), 0, 0 ) )
        BroadcastHelper.dataToSelfBasedOnVisibility( this, MagicSkillLaunched( this.objectId, skill.getDisplayId(), skill.getLevel(), [ target ] ) )

        await skill.activateCasterSkill( this, targets, false )

        return false
    }

    getItemRemainingReuseTime( item: L2ItemInstance ) : number {
        let time = this.itemReuse.getItem( item )
        if ( !time ) {
            return 0
        }

        return time.getRemainingMs()
    }

    getSkillRemainingReuseTime( skill: Skill ) : number {
        let time = this.skillReuse.getItem( skill )
        if ( !time ) {
            return 0
        }

        return time.getRemainingMs()
    }

    hasSkillActiveReuse( skill: Skill ): boolean {
        let time: L2ReuseTime = this.skillReuse.getItem( skill )
        return time && time.isActive()
    }

    addSkillReuse( skill: Skill, reuseTime: number ) {
        this.skillReuse.add( skill, reuseTime )
    }

    getSkillReuseData( skill: Skill ): L2ReuseTime {
        return this.skillReuse.getItem( skill )
    }

    getSkillReuse() : SkillReuseTime {
        this.skillReuse.removeInactive()

        return this.skillReuse
    }

    resetSkillReuse() {
        this.skillReuse.reset()
    }

    getReuseDelayOnGroup( group: number ): number {
        if ( group > 0 ) {
            return this.itemReuse.getRemainingTimeByGroup( group )
        }

        return 0
    }

    isSkillDisabled( skill: Skill ): boolean {
        if ( !skill ) {
            return false
        }

        if ( this.isAllSkillsDisabled() ) {
            return true
        }

        return this.disabledSkills.isActive( skill.getReuseHashCode() )
    }

    enableSkill( skill: Skill ) {
        if ( !skill ) {
            return
        }

        this.disabledSkills.remove( skill.getReuseHashCode() )
        this.skillReuse.removeItem( skill.getReuseHashCode() )
    }

    resetDisabledSkills() {
        this.disabledSkills.reset()
    }

    disableSkill( skill: Skill, delay: number ) {
        if ( !skill ) {
            return
        }

        this.disabledSkills.set( skill.getReuseHashCode(), delay )
    }

    getTeam(): Team {
        return this.team
    }

    setTeam( team: Team ) {
        this.team = team
    }

    isInArea( type: AreaType ) : boolean {
        return AreaDiscoveryManager.hasAreaType( this.getObjectId(), type )
    }

    isInSiegeArea() : boolean {
        return this.isInArea( AreaType.FortSiege ) || this.isInArea( AreaType.CastleSiege )
    }

    revalidateZone( force: boolean ) : void {
        if ( force ) {
            this.debounceAreaRevalidation.cancel()
            this.runDirectAreaRevalidation()
            return
        }

        this.debounceAreaRevalidation()
    }
}