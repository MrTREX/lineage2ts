import { L2Character } from '../L2Character'
import { ClanHall } from '../../entity/ClanHall'
import { L2DoorTemplate } from '../templates/L2DoorTemplate'
import { FortManager } from '../../../instancemanager/FortManager'
import { Fort } from '../../entity/Fort'
import { DataManager } from '../../../../data/manager'
import { Instance } from '../../entity/Instance'
import { InstanceManager } from '../../../instancemanager/InstanceManager'
import { Castle } from '../../entity/Castle'
import { CastleManager } from '../../../instancemanager/CastleManager'
import { StaticDoorObject } from '../../../packets/send/StaticObject'
import { DoorStatusUpdate } from '../../../packets/send/DoorStatusUpdate'
import { L2World } from '../../../L2World'
import { OnEventTrigger } from '../../../packets/send/OnEventTrigger'
import { Skill } from '../../Skill'
import { IDFactoryCache } from '../../../cache/IDFactoryCache'
import { InstanceType } from '../../../enums/InstanceType'
import { ClanHallManager } from '../../../instancemanager/ClanHallManager'
import { DoorStat } from '../stat/DoorStat'
import { L2PcInstance } from './L2PcInstance'
import { L2Object } from '../../L2Object'
import { L2Weapon } from '../../items/L2Weapon'
import { L2ItemInstance } from '../../items/instance/L2ItemInstance'
import { L2Item } from '../../items/L2Item'
import { ClanHallSiegeManager } from '../../../instancemanager/ClanHallSiegeManager'
import _ from 'lodash'
import { DoorManager } from '../../../cache/DoorManager'
import { DoorOpenFlags } from '../../../enums/DoorOpenFlags'
import { CalculatorHelper } from '../../../helpers/CalculatorHelper'
import Timeout = NodeJS.Timeout

export class L2DoorInstance extends L2Character {
    clanHallId: number = -1
    open: boolean
    isAttackableDoor: boolean
    isTargetableValue: boolean
    meshIndex: number = 1
    autoCloseTask: Timeout

    constructor( template: L2DoorTemplate ) {
        super( IDFactoryCache.getNextId(), template )

        this.instanceType = InstanceType.L2DoorInstance
        this.setIsInvulnerable( false )
        this.setLethalable( false )

        this.open = template.defaultStatus
        this.isAttackableDoor = template.isAttackableDoor
        this.isTargetableValue = template.isTargetable

        if ( this.isOpenableByTime() ) {
            this.startTimerOpen()
        }

        this.clanHallId = template.getClanHallId()
        if ( this.clanHallId > 0 ) {
            let hall: ClanHall = this.getClanHall()
            if ( hall ) {
                hall.getDoorIds().push( this.objectId )

                return
            }

            this.clanHallId = 0
        }
    }

    assignCalculators() {
        this.calculators = CalculatorHelper.getStandardDoorCalculators()
    }

    isMoveCapable(): boolean {
        return false
    }

    createStats(): DoorStat {
        return new DoorStat( this )
    }

    runBroadcastStatusUpdate() {
        let knownPlayers: Array<L2PcInstance> = L2World.getAllVisiblePlayers( this, this.getBroadcastRadius() )
        if ( knownPlayers.length === 0 ) {
            return
        }

        let mainObject: Buffer = StaticDoorObject( this, false )
        let targetableObject: Buffer = StaticDoorObject( this, true )
        let doorStatus: Buffer = DoorStatusUpdate( this.objectId )
        let triggerStatus: Buffer = this.getEmitter() > 0 ? OnEventTrigger( this, !this.isOpen() ) : null

        let isTargetable : boolean = ( this.getCastle() && this.getCastle().getResidenceId() > 0 )
            || ( this.getFort() && ( this.getFort().getResidenceId() > 0 ) )

        knownPlayers.forEach( ( player: L2PcInstance ) => {
            if ( !player || !this.isVisibleFor( player ) ) {
                return
            }

            if ( player.isGM() || isTargetable ) {
                player.sendCopyData( targetableObject )
            } else {
                player.sendCopyData( mainObject )
            }

            player.sendCopyData( doorStatus )

            if ( triggerStatus ) {
                player.sendCopyData( triggerStatus )
            }
        } )
    }

    getLevel(): number {
        return this.getTemplate().getLevel()
    }

    getTemplate(): L2DoorTemplate {
        return super.getTemplate() as L2DoorTemplate
    }

    checkCollision() {
        return this.getTemplate().isCheckCollision()
    }

    getCastle(): Castle {
        return CastleManager.getCastle( this )
    }

    getClanHall(): ClanHall {
        if ( this.clanHallId === 0 ) {
            return
        }

        let hall = ClanHallManager.getClanHallById( this.clanHallId )
        return hall ?? ClanHallSiegeManager.getSiegableHall( this.clanHallId )
    }

    getDamage() {
        let damage = 6 - Math.ceil( ( this.getCurrentHp() / this.getMaxHp() ) * 6 )
        if ( damage > 6 ) {
            return 6
        }

        return Math.max( damage, 0 )
    }

    getEmitter() {
        return this.getTemplate().getEmitter()
    }

    getFort(): Fort {
        return FortManager.getFort( this )
    }

    getGroupName() {
        return this.getTemplate().getGroupName()
    }

    getId(): number {
        return this.getTemplate().getId()
    }

    getIsShowHp() {
        return this.getTemplate().isShowHp()
    }

    getMeshIndex() {
        return this.meshIndex
    }

    getNodeX( id: number ): number {
        return this.getTemplate().getNodeX()[ id ]
    }

    getNodeY( id: number ) {
        return this.getTemplate().getNodeY()[ id ]
    }

    isOpen() : boolean {
        return this.open
    }

    getSiblingDoor( doorId: number ): L2DoorInstance {
        if ( this.getInstanceId() === 0 ) {
            return DoorManager.getDoor( doorId )
        }

        let instance: Instance = InstanceManager.getInstance( this.getInstanceId() )
        if ( instance ) {
            return instance.getDoor( doorId )
        }

        return null
    }

    getZMax() {
        return this.getTemplate().getNodeZ() + this.getTemplate().getHeight()
    }

    getZMin() {
        return this.getTemplate().getNodeZ()
    }

    isEnemy() {
        if ( !this.getIsShowHp() ) {
            return false
        }

        let castle = this.getCastle()
        if ( castle && ( castle.getResidenceId() > 0 ) && castle.isSiegeActive() ) {
            return true
        }
        let fort = this.getFort()
        if ( fort && fort.isSiegeActive() ) {
            return true
        }

        let clanHall = ClanHallSiegeManager.getSiegableHall( this.clanHallId )
        return clanHall && clanHall.isSiegeActive()
    }

    isOpenableByClick(): boolean {
        return this.hasOpenFlag( DoorOpenFlags.Click )
    }

    isOpenableByCycle(): boolean {
        return this.hasOpenFlag( DoorOpenFlags.Cycle )
    }

    isOpenableByItem(): boolean {
        return this.hasOpenFlag( DoorOpenFlags.Item )
    }

    isOpenableBySkill() {
        return this.hasOpenFlag( DoorOpenFlags.Skill )
    }

    isOpenableByTime(): boolean {
        return this.hasOpenFlag( DoorOpenFlags.Time )
    }

    private hasOpenFlag( flag: DoorOpenFlags ) : boolean {
        return ( this.getTemplate().getOpenType() & flag ) === flag
    }

    isTargetable() {
        return this.isTargetableValue
    }

    manageGroupOpen( isOpen: boolean, groupName: string ) {
        let firstDoor: L2DoorInstance

        let currentDoor = this
        DataManager.getDoorData().getDoorsByGroup( groupName ).forEach( ( doorId: number ) => {
            let door: L2DoorInstance = currentDoor.getSiblingDoor( doorId )
            if ( !firstDoor ) {
                firstDoor = door
            }

            if ( door.isOpen() !== currentDoor.open ) {
                door.setOpen( currentDoor.open )
                door.broadcastStatusUpdate()
            }
        } )

        if ( firstDoor && open ) {
            firstDoor.startAutoCloseTask()
        }
    }

    openMe() {
        if ( this.getGroupName() ) {
            this.manageGroupOpen( true, this.getGroupName() )
            return
        }

        this.setOpen( true )
        this.broadcastStatusUpdate()
        this.startAutoCloseTask()
    }

    setMeshIndex( value: number ) {
        this.meshIndex = value
    }

    setOpen( value: boolean ) {
        this.open = value
    }

    setTargetable( value: boolean ) {
        this.isTargetableValue = value
        this.broadcastStatusUpdate()
    }

    startAutoCloseTask() {
        if ( ( this.getTemplate().getCloseTime() < 0 ) || this.isOpenableByTime() ) {
            return
        }

        this.stopAutoCloseTask()
        this.autoCloseTask = setTimeout( this.runAutoCloseTask.bind( this ), this.getTemplate().getCloseTime() * 1000 )
    }

    isDoor(): boolean {
        return true
    }

    async reduceCurrentHpByDOT( power: number, attacker: L2Character, skill: Skill ): Promise<void> {
        return
    }

    setClanHall( hall: ClanHall ) {
        this.clanHallId = hall.getId()
    }

    startTimerOpen() {
        let delay = this.open ? this.getTemplate().getOpenTime() : this.getTemplate().getCloseTime()
        let randomTime = this.getTemplate().getRandomTime()
        if ( randomTime > 0 ) {
            delay += _.random( randomTime )
        }

        setTimeout( this.runTimerOpenTask.bind( this ), delay * 1000 )
    }

    runTimerOpenTask() {
        let open = this.isOpen()
        if ( open ) {
            this.closeMe()
        } else {
            this.openMe()
        }

        this.startTimerOpen()
    }

    stopAutoCloseTask() {
        if ( this.autoCloseTask ) {
            clearTimeout( this.autoCloseTask )
            this.autoCloseTask = null
        }
    }

    closeMe() {
        this.stopAutoCloseTask()

        if ( this.getGroupName() ) {
            this.manageGroupOpen( false, this.getGroupName() )
            return
        }

        this.setOpen( false )
        this.broadcastStatusUpdate()
    }

    runAutoCloseTask() {
        if ( this.isOpen() ) {
            this.closeMe()
        }
    }

    getStat(): DoorStat {
        return super.getStat() as DoorStat
    }

    getKnownDefenders(): Array<L2Object> {
        const predicate = ( object: L2Object ): boolean => {
            return object.isDefender()
        }
        return L2World.getVisibleObjectsByPredicate( this, this.getBroadcastRadius(), predicate )
    }

    shouldDescribeState( player: L2PcInstance ): boolean {
        if ( this.isVisibleFor( player ) ) {
            if ( this.getEmitter() > 0 ) {
                player.sendOwnedData( OnEventTrigger( this, !this.isOpen() ) )
            }

            player.sendOwnedData( StaticDoorObject( this, player.isGM() ) )
        }

        return false
    }

    async doCast( skill: Skill ): Promise<void> {

    }

    getActiveWeaponItem(): L2Weapon {
        return null
    }

    getActiveWeaponInstance(): L2ItemInstance {
        return null
    }

    getSecondaryWeaponItem(): L2Item {
        return null
    }

    getSecondaryWeaponInstance(): L2ItemInstance {
        return null
    }
}