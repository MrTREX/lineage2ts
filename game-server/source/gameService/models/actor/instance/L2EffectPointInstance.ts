import { L2Npc } from '../L2Npc'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { L2Character } from '../L2Character'
import { InstanceType } from '../../../enums/InstanceType'
import { L2PcInstance } from './L2PcInstance'
import { L2World } from '../../../L2World'
import { PacketDispatcher } from '../../../PacketDispatcher'
import { ActionFailed } from '../../../packets/send/ActionFailed'

export class L2EffectPointInstance extends L2Npc {
    ownerId: number

    constructor( template: L2NpcTemplate, owner: L2Character ) {
        super( template )
        this.instanceType = InstanceType.L2EffectPointInstance
        this.setIsInvulnerable( false )

        this.ownerId = owner ? owner.getActingPlayer().getObjectId() : null
        if ( this.ownerId ) {
            this.setInstanceId( owner.getInstanceId() )
        }
    }

    getActingPlayer(): L2PcInstance {
        return L2World.getPlayer( this.ownerId )
    }

    getActingPlayerId() : number {
        return this.ownerId
    }

    onInteraction() : Promise<void> {
        PacketDispatcher.sendOwnedData( this.ownerId, ActionFailed() )
        return
    }

    async onActionShift( player: L2PcInstance ) {
        if ( !player ) {
            return
        }

        player.sendOwnedData( ActionFailed() )
    }
}