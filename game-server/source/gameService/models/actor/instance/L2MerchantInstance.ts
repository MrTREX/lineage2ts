import { L2NpcInstance } from './L2NpcInstance'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'
import { L2PcInstance } from './L2PcInstance'
import { BuyListManager, EmptyBuylist } from '../../../cache/BuyListManager'
import { L2BuyList } from '../../buylist/L2BuyList'
import { ActionFailed } from '../../../packets/send/ActionFailed'
import { ExBuySellList } from '../../../packets/send/ExBuySellList'
import { BuyList } from '../../../packets/send/BuyList'
import { MerchantPriceConfig } from '../../MerchantPriceConfig'
import { ConfigManager } from '../../../../config/ConfigManager'
import { L2World } from '../../../L2World'
import { DataManager } from '../../../../data/manager'
import { TownArea } from '../../areas/type/Town'
import { AreaCache } from '../../../cache/AreaCache'
import { AreaType } from '../../areas/AreaType'
import { CastleArea } from '../../areas/type/Castle'

export class L2MerchantInstance extends L2NpcInstance {

    priceConfiguration: MerchantPriceConfig

    constructor( template: L2NpcTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2MerchantInstance
    }

    onSpawn() {
        super.onSpawn()
        this.preparePriceConfig()
    }

    private preparePriceConfig() : void {
        let castle = AreaCache.getArea( this, AreaType.Castle ) as CastleArea
        if ( castle ) {
            this.priceConfiguration = DataManager.getMerchantPriceData().getConfigByCastleId( castle.getResidenceId() )
            return
        }

        let town : TownArea = L2World.getNearestTowns( this, 1 )[ 0 ] as TownArea
        let config : MerchantPriceConfig

        if ( town ) {
            config = DataManager.getMerchantPriceData().getConfigByTownId( town.properties.townId )
        }

        this.priceConfiguration = config ? config : DataManager.getMerchantPriceData().getDefaultConfig()
    }

    getHtmlPath( npcId: number, value: number ): string {
        let fileName: string = npcId.toString()

        if ( value !== 0 ) {
            fileName = `${ fileName }-${ value }`
        }

        return 'data/html/merchant/' + fileName + '.htm'
    }

    getPriceConfig(): MerchantPriceConfig {
        return this.priceConfiguration
    }

    showBuyWindow( player: L2PcInstance, listId: number, applyTax: boolean = true ): void {
        let taxRate = applyTax ? this.getPriceConfig().getTotalTaxRate() : 0
        return this.showBuylist( player, listId, this.getId(), taxRate )
    }

    showBuylist( player: L2PcInstance, listId: number, npcId: number, taxRate: number ): void {
        let buyList: L2BuyList = BuyListManager.getBuyList( listId )
        if ( !buyList ) {
            return player.sendOwnedData( ActionFailed() )
        }

        if ( !buyList.isNpcAllowed( npcId ) ) {
            if ( !player.isGM() ) {
                return player.sendOwnedData( ActionFailed() )
            }

            player.sendMessage( `Buy List [ ${ buyList.getListId() } ] not allowed for NPC id ${ npcId }` )
        }

        if ( ConfigManager.npc.getDisabledMerchantIds().includes( npcId ) ) {
            buyList = EmptyBuylist
        }

        player.setInventoryBlockingStatus( true )
        if ( player.isGM() ) {
            player.sendMessage( `Using Buy List [ ${ buyList.getListId() } ]` )
        }

        player.sendOwnedData( BuyList( buyList, player.getAdena(), taxRate ) )
        player.sendOwnedData( ExBuySellList( player, false ) )
        player.sendOwnedData( ActionFailed() )
    }

    static fromTemplate( template: L2NpcTemplate ): L2MerchantInstance {
        return new L2MerchantInstance( template )
    }

    isMerchant(): boolean {
        return true
    }
}