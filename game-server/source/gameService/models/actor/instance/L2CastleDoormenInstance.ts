import { L2DoormenInstance } from './L2DoormenInstance'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'
import { SiegableHall } from '../../entity/clanhall/SiegableHall'
import { L2PcInstance } from './L2PcInstance'
import { ClanPrivilege } from '../../../enums/ClanPriviledge'

export class L2CastleDoormenInstance extends L2DoormenInstance {
    hall: SiegableHall

    constructor( template: L2NpcTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2CastleDoormenInstance
    }

    static fromTemplate( template: L2NpcTemplate ): L2CastleDoormenInstance {
        return new L2CastleDoormenInstance( template )
    }

    isUnderSiege(): boolean {
        let hall : SiegableHall = this.getConquerableHall()
        if ( hall ) {
            return hall.isInSiege()
        }

        return this.getCastle().isSiegeActive()
    }

    isOwnerClan( player: L2PcInstance ): boolean {
        if ( player.getClan() && player.hasClanPrivilege( ClanPrivilege.CastleOpenDoors ) ) {
            let hall : SiegableHall = this.getConquerableHall()
            if ( hall ) {
                return player.getClanId() === hall.getOwnerId()
            }

            if ( this.getCastle() ) {
                return player.getClanId() === this.getCastle().getOwnerId()
            }
        }

        return false
    }

    getConquerableHall(): SiegableHall {
        if ( !this.hall ) {
            this.hall = super.getConquerableHall()
        }

        return this.hall
    }
}