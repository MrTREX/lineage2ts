import { L2VillageMasterInstance } from './L2VillageMasterInstance'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'
import { Race } from '../../../enums/Race'
import { ConfigManager } from '../../../../config/ConfigManager'
import { L2PcInstance } from './L2PcInstance'
import { getPlayerClassById } from '../../base/PlayerClass'
import { DataManager } from '../../../../data/manager'

export class L2VillageMasterKamaelInstance extends L2VillageMasterInstance {
    constructor( template: L2NpcTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2VillageMasterKamaelInstance
    }

    static fromTemplate( template: L2NpcTemplate ): L2VillageMasterKamaelInstance {
        return new L2VillageMasterKamaelInstance( template )
    }

    checkQuests( player: L2PcInstance ): boolean {
        return player.isNoble()
                || player.hasQuestCompleted( 'Q00234_FatesWhisper' )
                || player.hasQuestCompleted( 'Q00236_SeedsOfChaos' )
    }

    checkVillageMasterRace( classId: number ): boolean {
        return getPlayerClassById( classId ).race === Race.KAMAEL
    }

    getSubClassFail(): [ string, string ] {
        let path = 'data/html/villagemaster/SubClass_Fail_Kamael.htm'
        return [ DataManager.getHtmlData().getItem( path ), path ]
    }

    getSubclassMenu( race: Race ): string {
        if ( ConfigManager.character.subclassEverywhere() || race === Race.KAMAEL ) {
            return 'data/html/villagemaster/SubClass.htm'
        }

        return 'data/html/villagemaster/SubClass_NoKamael.htm'
    }
}