import { L2NpcInstance } from './L2NpcInstance'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'
import { L2PcInstance } from './L2PcInstance'
import { ActionFailed } from '../../../packets/send/ActionFailed'
import { NpcHtmlMessagePath } from '../../../packets/send/NpcHtmlMessage'
import { DataManager } from '../../../../data/manager'

export class L2DoormenInstance extends L2NpcInstance {
    constructor( template: L2NpcTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2DoormenInstance
    }

    static fromTemplate( template: L2NpcTemplate ): L2DoormenInstance {
        return new L2DoormenInstance( template )
    }

    isOwnerClan( player: L2PcInstance ): boolean {
        return true
    }

    isUnderSiege(): boolean {
        return false
    }

    showChatWindowDefault( player: L2PcInstance ): void {
        player.sendOwnedData( ActionFailed() )

        let suffix = ''
        if ( !this.isOwnerClan( player ) ) {
            suffix = '-no'
        }

        let path = `data/html/doormen/${ this.getId() }${ suffix }.htm`
        let html: string = DataManager.getHtmlData().getItem( path )

        player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), this.getObjectId() ) )
    }
}