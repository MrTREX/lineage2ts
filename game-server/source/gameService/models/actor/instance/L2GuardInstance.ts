import { L2Attackable } from '../L2Attackable'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'
import { L2Character } from '../L2Character'

export class L2GuardInstance extends L2Attackable {

    constructor( template: L2NpcTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2GuardInstance
    }

    static fromTemplate( template: L2NpcTemplate ) : L2GuardInstance {
        return new L2GuardInstance( template )
    }

    isAutoAttackable( attacker: L2Character ): boolean {
        return attacker && attacker.isMonster()
    }

    getHtmlPath( npcId: number, value: number ): string {
        let fileName : string = npcId.toString()

        if ( value !== 0 ) {
            fileName = `${fileName}-${value}`
        }

        return 'data/html/guard/' + fileName + '.htm'
    }

    isGuard(): boolean {
        return true
    }

    hasRandomWalk(): boolean {
        return false
    }
}