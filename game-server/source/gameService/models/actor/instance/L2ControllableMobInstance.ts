import { L2MonsterInstance } from './L2MonsterInstance'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'

export class L2ControllableMobInstance extends L2MonsterInstance {
    invulnerable: boolean = false

    constructor( template: L2NpcTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2ControllableMobInstance
    }

    isAggressive(): boolean {
        return true
    }

    getAggroRange(): number {
        return 500
    }

    isInvulnerable(): boolean {
        return this.invulnerable
    }

    setIsInvulnerable( value: boolean ): void {
        this.invulnerable = value
    }
}