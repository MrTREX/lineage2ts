import { L2NpcInstance } from './L2NpcInstance'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'

export class L2WarehouseInstance extends L2NpcInstance {
    constructor( template: L2NpcTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2WarehouseInstance
    }

    isWarehouse(): boolean {
        return true
    }

    getHtmlPath( npcId: number, value: number ) : string {
        let fileName : string = npcId.toString()

        if ( value !== 0 ) {
            fileName = `${fileName}-${value}`
        }

        return 'data/html/warehouse/' + fileName + '.htm'
    }

    static fromTemplate( template: L2NpcTemplate ) : L2WarehouseInstance {
        return new L2WarehouseInstance( template )
    }
}