import { L2GuardInstance } from './L2GuardInstance'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'
import { L2Character } from '../L2Character'

export class L2QuestGuardInstance extends L2GuardInstance {
    isPassiveValue: boolean = false

    constructor( template: L2NpcTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2QuestGuardInstance
    }

    static fromTemplate( template: L2NpcTemplate ): L2GuardInstance {
        return new L2QuestGuardInstance( template )
    }

    isPassive(): boolean {
        return this.isPassiveValue
    }

    setPassive( value: boolean ): void {
        this.isPassiveValue = value
    }

    isAutoAttackable( attacker: L2Character ): boolean {
        return !attacker.isPlayable()
    }
}