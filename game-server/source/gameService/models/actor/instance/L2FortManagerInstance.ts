import { L2MerchantInstance } from './L2MerchantInstance'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'
import { L2PcInstance } from './L2PcInstance'
import { ActionFailed } from '../../../packets/send/ActionFailed'
import { NpcHtmlMessagePath } from '../../../packets/send/NpcHtmlMessage'
import { DataManager } from '../../../../data/manager'

const enum InteractionRestriction {
    AccessDisabled = 0,
    Busy = 1,
    OnlyOwner = 2
}

export class L2FortManagerInstance extends L2MerchantInstance {

    constructor( template: L2NpcTemplate ) {
        super( template )

        this.instanceType = InstanceType.L2FortManagerInstance
    }

    static fromTemplate( template: L2NpcTemplate ): L2FortManagerInstance {
        return new L2FortManagerInstance( template )
    }

    isWarehouse(): boolean {
        return true
    }

    async onBypassFeedback( player: L2PcInstance, command: string ): Promise<void> {
        if ( player.getLastFolkNPC() !== this.getObjectId() ) {
            return
        }

        let restriction = this.getRestriction( player )
        if ( restriction !== InteractionRestriction.OnlyOwner ) {
            return
        }

        return super.onBypassFeedback( player, command )
    }

    showChatWindowDefault( player: L2PcInstance ): void {
        player.sendOwnedData( ActionFailed() )

        let path = this.getRestrictionHtmlPath( this.getRestriction( player ) )
        let html: string = DataManager.getHtmlData().getItem( path )
                                      .replace( /%npcname%/g, this.getName() )

        player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), this.getObjectId() ) )
    }

    getRestrictionHtmlPath( restriction: InteractionRestriction ): string {
        switch ( restriction ) {
            case InteractionRestriction.Busy:
                return 'data/html/fortress/foreman-busy.htm'

            case InteractionRestriction.OnlyOwner:
                return 'data/html/fortress/foreman.htm'
        }

        return 'data/html/fortress/foreman-no.htm'
    }

    getRestriction( player: L2PcInstance ): number {
        let fort = this.getFort()
        if ( fort
                && fort.getResidenceId() > 0
                && player.getClan() ) {

            if ( fort.isSiegeActive() ) {
                return InteractionRestriction.Busy
            }

            if ( fort.getOwnerClan()
                    && fort.getOwnerClan().getId() === player.getClanId() ) {
                return InteractionRestriction.OnlyOwner
            }
        }

        return InteractionRestriction.AccessDisabled
    }
}