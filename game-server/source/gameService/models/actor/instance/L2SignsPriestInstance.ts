import { L2Npc } from '../L2Npc'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'

/*
    Used by Merchant/Blacksmith/Marketeer of Mammon
 */
export class L2SignsPriestInstance extends L2Npc {
    constructor( template: L2NpcTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2SignsPriestInstance
    }

    static fromTemplate( template: L2NpcTemplate ): L2SignsPriestInstance {
        return new L2SignsPriestInstance( template )
    }

    isMoveCapable(): boolean {
        return false
    }
}