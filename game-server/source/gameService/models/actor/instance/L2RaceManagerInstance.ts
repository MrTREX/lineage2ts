import { L2Npc } from '../L2Npc'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'

export class L2RaceManagerInstance extends L2Npc {
    constructor( template: L2NpcTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2RaceManagerInstance
    }

    static fromTemplate( template: L2NpcTemplate ): L2RaceManagerInstance {
        return new L2RaceManagerInstance( template )
    }

    // TODO : implement lots of stuff here
}