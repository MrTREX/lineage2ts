import { L2MonsterInstance } from './L2MonsterInstance'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'

export class L2RiftInvaderInstance extends L2MonsterInstance {
    constructor( template: L2NpcTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2RiftInvaderInstance
    }

    static fromTemplate( template: L2NpcTemplate ): L2RiftInvaderInstance {
        return new L2RiftInvaderInstance( template )
    }
}