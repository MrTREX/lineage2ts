import { L2MonsterInstance } from './L2MonsterInstance'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'
import { L2Character } from '../L2Character'
import { L2PcInstance } from './L2PcInstance'
import { SystemMessageBuilder } from '../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../packets/SystemMessageIdValues'
import { L2World } from '../../../L2World'
import { RaidBossPointsManager } from '../../../cache/RaidBossPointsManager'
import { HeroCache } from '../../../cache/HeroCache'
import { BroadcastHelper } from '../../../helpers/BroadcastHelper'
import aigle from 'aigle'
import _ from 'lodash'
import { MusicPacket } from '../../../packets/send/Music'
import { PlayMusic } from '../../../packets/send/builder/PlaySound'

export class L2RaidBossInstance extends L2MonsterInstance {
    useRaidCurse: boolean = true

    constructor( template: L2NpcTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2RaidBossInstance

        this.setIsRaid( true )
        this.setLethalable( false )
    }

    static fromTemplate( template: L2NpcTemplate ): L2RaidBossInstance {
        return new L2RaidBossInstance( template )
    }

    useVitalityRate(): boolean {
        return false
    }

    getVitalityPoints( damage: number ): number {
        return -super.getVitalityPoints( damage ) / 100
    }

    hasRaidCurse(): boolean {
        return this.useRaidCurse
    }

    async doDie( killer: L2Character ): Promise<boolean> {
        if ( !await super.doDie( killer ) ) {
            return false
        }

        let player : L2PcInstance = killer.getActingPlayer()
        if ( player ) {
            BroadcastHelper.dataInLocation( this, SystemMessageBuilder.fromMessageId( SystemMessageIds.RAID_WAS_SUCCESSFUL ), this.getBroadcastRadius() )

            let party = player.getParty()
            await aigle.resolve( party ? party.getMembers() : [ player.getObjectId() ] ).each( async ( objectId: number ) : Promise<void> => {
                let member : L2PcInstance = L2World.getPlayer( objectId )
                if ( !member ) {
                    return
                }

                await RaidBossPointsManager.addPoints( objectId, this.getId(), ( this.getLevel() / 2 ) + _.random( -5, 5 ) )
                if ( member.isNoble() ) {
                    return HeroCache.setRaidBossKilled( member.getObjectId(), this.getId() )
                }
            } )
        }

        return true
    }

    hasRandomWalk(): boolean {
        return false
    }

    onSpawn() {
        super.onSpawn()

        /*
            When raidboss is spawned always there is music being played,
            this in contrast to grand raidboss which may have spawn music,
            or can play music instead on death.
         */
        let musicName = this.getTemplate().parameters[ 'RaidSpawnMusic' ]
        let packet = musicName ? new PlayMusic( musicName, this, 8000 ).getBuffer() : MusicPacket.RM01_A_8000

        BroadcastHelper.dataInLocation( this, packet, 2000 )
    }
}