import { L2AirShipInstance } from './L2AirShipInstance'
import { L2CharacterTemplate } from '../templates/L2CharacterTemplate'
import { InstanceType } from '../../../enums/InstanceType'
import { IDFactoryCache } from '../../../cache/IDFactoryCache'
import { L2PcInstance } from './L2PcInstance'
import { L2World } from '../../../L2World'
import { SystemMessageBuilder } from '../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../packets/SystemMessageIdValues'
import { DeleteObject } from '../../../packets/send/DeleteObject'
import { ConfigManager } from '../../../../config/ConfigManager'
import { BroadcastHelper } from '../../../helpers/BroadcastHelper'
import { ExStopMoveAirShip } from '../../../packets/send/ExStopMoveAirShip'
import { L2Region } from '../../../enums/L2Region'
import Timeout = NodeJS.Timeout

const HELM = 13556
const LOW_FUEL = 40

export class L2ControllableAirShipInstance extends L2AirShipInstance {
    fuel: number = 0
    maxFuel: number = 0
    ownerId: number
    helmId: number
    capitainId: number

    consumeFuelTask: Timeout
    checkTask: Timeout

    constructor( template: L2CharacterTemplate, ownerId: number ) {
        super( template )
        this.instanceType = InstanceType.L2ControllableAirShipInstance
        this.ownerId = ownerId
        this.helmId = IDFactoryCache.getNextId() // TODO : how to release it?
    }

    setCaptain( player: L2PcInstance ): boolean {
        if ( !player ) {
            this.capitainId = null
        } else {
            let captain = L2World.getPlayer( this.capitainId )
            if ( !captain && ( player.getAirShip().getObjectId() === this.getObjectId() ) ) {
                let x = player.getInVehiclePosition().getX() - 0x16e
                let y = player.getInVehiclePosition().getY()
                let z = player.getInVehiclePosition().getZ() - 0x6b

                if ( ( ( x * x ) + ( y * y ) + ( z * z ) ) > 2500 ) {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_CONTROL_TOO_FAR ) )
                    return false
                }

                if ( player.isInCombat() ) {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_CONTROL_THE_HELM_WHILE_IN_A_BATTLE ) )
                    return false
                }

                if ( player.isSitting() ) {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_CONTROL_THE_HELM_WHILE_IN_A_SITTING_POSITION ) )
                    return false
                }

                if ( player.isStunned() ) {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_CONTROL_THE_HELM_WHILE_YOU_ARE_PETRIFIED ) )
                    return false
                }

                if ( player.isCursedWeaponEquipped() ) {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_CONTROL_THE_HELM_WHILE_A_CURSED_WEAPON_IS_EQUIPPED ) )
                    return false
                }

                if ( player.isFishing() ) {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_CONTROL_THE_HELM_WHILE_FISHING ) )
                    return false
                }

                if ( player.isDead() || player.isFakeDeath() ) {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_CONTROL_THE_HELM_WHEN_YOU_ARE_DEAD ) )
                    return false
                }

                if ( player.isCastingNow() ) {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_CONTROL_THE_HELM_WHILE_USING_A_SKILL ) )
                    return false
                }

                if ( player.isTransformed() ) {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_CONTROL_THE_HELM_WHILE_TRANSFORMED ) )
                    return false
                }

                if ( player.isCombatFlagEquipped() ) {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_CONTROL_THE_HELM_WHILE_HOLDING_A_FLAG ) )
                    return false
                }

                if ( player.isInDuel() ) {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_CONTROL_THE_HELM_WHILE_IN_A_DUEL ) )
                    return false
                }

                this.capitainId = player.getObjectId()
                player.broadcastUserInfo()
            } else {
                return false
            }
        }

        this.onUpdateAbnormalEffect()
        return true
    }

    refreshId() {
        this.objectId = IDFactoryCache.getNextId()
        L2World.storeObject( this )

        this.spatialIndex.objectId = this.getObjectId()
        this.sharedData.setObjectId( this.getObjectId() )

        IDFactoryCache.releaseId( this.helmId )
        this.helmId = IDFactoryCache.getNextId()
    }

    shouldDescribeState( player: L2PcInstance ) : boolean {
        if ( super.shouldDescribeState( player ) ) {
            let captain = L2World.getPlayer( this.capitainId )
            if ( captain && captain.shouldDescribeState( player ) ) {
                return true
            }
        }

        return false
    }

    onSpawn() {
        super.onSpawn()
        this.checkTask = setInterval( this.runCheckTask.bind( this ), 10000 )
        this.consumeFuelTask = setInterval( this.runConsumeFuelTask.bind( this ), 60000 )
    }

    stopTasks() {
        if ( this.checkTask ) {
            clearInterval( this.checkTask )
        }

        this.checkTask = null

        if ( this.consumeFuelTask ) {
            clearInterval( this.consumeFuelTask )
        }

        this.consumeFuelTask = null
    }

    async deleteMe(): Promise<void> {
        this.stopTasks()
        BroadcastHelper.dataBasedOnVisibility( this, DeleteObject( this.helmId ) )

        return super.deleteMe()
    }

    getBroadcastRadius(): number {
        return L2Region.LargeObjectUpdateRadius
    }

    runCheckTask() {
        if ( this.isVisible() && this.isEmpty() && !this.isInDock() ) {
            setImmediate( this.deleteMe.bind( this ) )
        }
    }

    runConsumeFuelTask() {
        let fuel = this.getFuel()
        if ( fuel > 0 ) {
            fuel -= Math.abs( ConfigManager.tuning.getAirshipFuelConsumptionRate() )
            if ( fuel < 0 ) {
                fuel = 0
            }

            this.setFuel( fuel )
            this.onUpdateAbnormalEffect()
        }
    }

    stopMove(): void {
        super.abortMoving()
        BroadcastHelper.dataBasedOnVisibility( this, ExStopMoveAirShip( this ) )
    }
}