import { L2Vehicle } from '../L2Vehicle'
import { L2PcInstance } from './L2PcInstance'
import { L2CharacterTemplate } from '../templates/L2CharacterTemplate'
import { InstanceType } from '../../../enums/InstanceType'
import { ExMoveToLocationAirShip } from '../../../packets/send/ExMoveToLocationAirShip'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { ExAirShipInfo } from '../../../packets/send/ExAirShipInfo'
import { AirShipManager } from '../../../instancemanager/AirShipManager'
import { BroadcastHelper } from '../../../helpers/BroadcastHelper'
import { L2Region } from '../../../enums/L2Region'

export class L2AirShipInstance extends L2Vehicle {

    constructor( template: L2CharacterTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2AirShipInstance
    }

    isAirShip(): boolean {
        return true
    }

    getOwnerId() {
        return 0
    }

    isCaptain( player: L2PcInstance ) {
        return false
    }

    getCaptainId() {
        return 0
    }

    getHelmObjectId() {
        return 0
    }

    getHelmItemId() {
        return 0
    }

    getFuel() {
        return 0
    }

    setFuel( amount: number ) {

    }

    getMaxFuel() {
        return 0
    }

    setMaxFuel( amount: number ) {

    }

    getId() {
        return 0
    }

    setCaptain( player: L2PcInstance ) {
        return false
    }

    getDockId(): number {
        return this.dockId
    }

    canBeControlled() {
        return !this.engine
    }

    async moveToNextRoutePoint(): Promise<boolean> {

        // TODO : actually move vehicle
        let result = false
        if ( result ) {
            BroadcastHelper.dataBasedOnVisibility( this, ExMoveToLocationAirShip( this ) )
        }

        return result
    }

    static fromTemplate( template: L2NpcTemplate ): L2AirShipInstance {
        return new L2AirShipInstance( template )
    }

    shouldDescribeState( player: L2PcInstance ): boolean {
        if ( this.isVisibleFor( player ) ) {
            player.sendOwnedData( ExAirShipInfo( this ) )
            return true
        }

        return false
    }

    onUpdateAbnormalEffect(): void {
        BroadcastHelper.dataInLocation( this, ExAirShipInfo( this ) , this.getBroadcastRadius() )
    }

    async deleteMe(): Promise<void> {
        await super.deleteMe()
        return AirShipManager.removeAirShip( this )
    }

    getBroadcastRadius(): number {
        return L2Region.LargeObjectUpdateRadius
    }
}