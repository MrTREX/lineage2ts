import { L2VillageMasterInstance } from './L2VillageMasterInstance'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'
import { Race } from '../../../enums/Race'
import { getPlayerClassById } from '../../base/PlayerClass'

export class L2VillageMasterOrcInstance extends L2VillageMasterInstance {
    constructor( template: L2NpcTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2VillageMasterOrcInstance
    }

    static fromTemplate( template: L2NpcTemplate ): L2VillageMasterOrcInstance {
        return new L2VillageMasterOrcInstance( template )
    }

    checkVillageMasterRace( classId: number ): boolean {
        return getPlayerClassById( classId ).race === Race.ORC
    }
}