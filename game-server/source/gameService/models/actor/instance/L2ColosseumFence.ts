import { L2Object } from '../../L2Object'
import { FenceState } from '../../../values/L2ColosseumFenceValues'
import { Box } from 'math2d'
import { L2PcInstance } from './L2PcInstance'
import { ExColosseumFenceInfo } from '../../../packets/send/ExColosseumFenceInfo'

export class L2ColosseumFence extends L2Object {
    state: FenceState
    minZ: number
    maxZ: number
    box : Box
    height: number
    width: number

    constructor( objectId: number,
            instanceId: number,
            x: number,
            y: number,
            z: number,
            minZ: number,
            maxZ: number,
            width: number,
            height: number,
            state: FenceState ) {
        super( objectId )

        this.setInstanceId( instanceId )
        this.setXYZ( x, y, z )

        this.minZ = minZ
        this.maxZ = maxZ
        this.state = state
        this.height = height
        this.width = width

        let halfWidth = Math.floor( width / 2 )
        let halfHeight = Math.floor( height / 2 )

        this.box = {
            maxX: y + halfHeight,
            maxY: x + halfWidth,
            minX: x - halfWidth,
            minY: y - halfHeight
        }
    }

    shouldDescribeState( player: L2PcInstance ): boolean {
        player.sendOwnedData( ExColosseumFenceInfo( this ) )
        return true
    }

    getState() : FenceState {
        return this.state
    }

    isAutoAttackable(): boolean {
        return false
    }

    getId(): number {
        return this.getObjectId()
    }
}