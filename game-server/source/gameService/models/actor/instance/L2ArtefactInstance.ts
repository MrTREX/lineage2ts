import { L2Npc } from '../L2Npc'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'
import { L2Character } from '../L2Character'
import { ActionFailed } from '../../../packets/send/ActionFailed'
import { L2PcInstance } from './L2PcInstance'
import { Skill } from '../../Skill'

export class L2ArtefactInstance extends L2Npc {

    constructor( template: L2NpcTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2ArtefactInstance
    }

    static fromTemplate( template: L2NpcTemplate ): L2ArtefactInstance {
        return new L2ArtefactInstance( template )
    }

    canBeAttacked(): boolean {
        return false
    }

    isAutoAttackable( attacker: L2Character ): boolean {
        return false
    }

    onForcedAttack( player: L2PcInstance ) : Promise<void> {
        player.sendOwnedData( ActionFailed() )
        return
    }

    onSpawn() {
        super.onSpawn()
        this.getCastle().registerArtifact( this )
    }

    async reduceCurrentHp( damage: number, attacker: L2Character, skill: Skill, isAwake: boolean = true, isDOT: boolean = false ): Promise<void> {

    }

    async reduceCurrentHpByDOT( power: number, attacker: L2Character, skill: Skill ): Promise<void> {

    }
}