import { L2VillageMasterInstance } from './L2VillageMasterInstance'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'
import { Race } from '../../../enums/Race'
import { getPlayerClassById } from '../../base/PlayerClass'
import { ClassType } from '../../base/ClassType'

export class L2VillageMasterFighterInstance extends L2VillageMasterInstance {
    constructor( template: L2NpcTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2VillageMasterFighterInstance
    }

    static fromTemplate( template: L2NpcTemplate ): L2VillageMasterFighterInstance {
        return new L2VillageMasterFighterInstance( template )
    }

    checkVillageMasterRace( classId: number ): boolean {
        return [ Race.HUMAN, Race.ELF ].includes( getPlayerClassById( classId ).race )
    }

    checkVillageMasterTeachType( classId: number ): boolean {
        return getPlayerClassById( classId ).type === ClassType.Fighter
    }
}