import { Skill } from '../../Skill'
import { L2FeedableBeastInstance } from './L2FeedableBeastInstance'
import { L2Character } from '../L2Character'
import { L2PcInstance } from './L2PcInstance'
import { L2World } from '../../../L2World'
import { L2EffectType } from '../../../enums/effects/L2EffectType'
import { DataManager } from '../../../../data/manager'
import { InstanceType } from '../../../enums/InstanceType'
import { AIEffectHelper } from '../../../aicontroller/helpers/AIEffectHelper'
import Timeout = NodeJS.Timeout
import _ from 'lodash'

export const L2TamedBeastValues = {
    MAX_DISTANCE_FROM_HOME: 30000,
    MAX_DISTANCE_FROM_OWNER: 2000,
    MAX_DURATION: 1200000, // 20 minutes
    DURATION_CHECK_INTERVAL: 60000, // 1 minute
    DURATION_INCREASE_INTERVAL: 20000, // 20 secs (gained upon feeding)
    BUFF_INTERVAL: 5000, // 5 seconds
}

export class L2TamedBeastInstance extends L2FeedableBeastInstance {
    foodSkillId: number
    remainingTime: number
    homeX: number
    homeY: number
    homeZ: number
    owner: number // object id
    isFreyaBeast: boolean
    beastSkills: Array<Skill>

    constructor( templateId: number, ownerId: number, foodType: number, x: number, y: number, z: number, isFreyaBeast: boolean = false ) {
        super( DataManager.getNpcData().getTemplate( templateId ) )
        this.instanceType = InstanceType.L2TamedBeastInstance

        this.setCurrentHp( this.getMaxHp() )
        this.setCurrentMp( this.getMaxMp() )
        this.owner = ownerId
        this.foodSkillId = foodType

        this.homeX = x
        this.homeY = y
        this.homeZ = z

        this.spawnMe( x, y, z )
        if ( isFreyaBeast ) {
            AIEffectHelper.notifyFollow( this, ownerId )
        }
    }

    // TODO : move to AITrait since current logic will be trigger by AIOwnerHpReduce effect
    async onOwnerGotAttacked( attackerId: number ) : Promise<void> {
        let owner = this.getOwner()
        if ( !owner || !owner.isOnline() ) {
            await this.deleteMe()
            return
        }

        if ( !owner.isInsideRadius( this, L2TamedBeastValues.MAX_DISTANCE_FROM_OWNER, true ) ) {
            AIEffectHelper.notifyFollow( this, owner.getId() )
            return
        }

        if ( owner.isDead() || this.isFreyaBeast ) {
            return
        }

        if ( this.isCastingNow() ) {
            return
        }

        let attacker = L2World.getObjectById( attackerId ) as L2Character
        let ratio = owner.getCurrentHp() / owner.getMaxHp()
        let predicate = null

        if ( ratio >= 0.8 ) {
            predicate = ( skill : Skill ) : boolean => skill.isDebuff() && ( _.random( 3 ) < 1 ) && ( attacker && attacker.isAffectedBySkill( skill.getId() ) )
        } else if ( ratio < 0.5 ) {
            let chance = 1
            if ( ratio < 0.25 ) {
                chance = 2
            }

            predicate = ( skill : Skill ) : boolean => ( _.random( 5 ) < chance ) && skill.hasPointRecovery()
        }

        if ( predicate ) {
            let skill = this.getTemplate().getSkills().find( predicate )
            if ( skill ) {
                await this.sitCastAndFollow( skill, owner )
            }
        }
    }

    getOwner() : L2PcInstance {
        return L2World.getPlayer( this.owner )
    }

    async sitCastAndFollow( skill: Skill, target: L2Character ) : Promise<void> {
        this.abortMoving()
        this.setTarget( target )
        await this.doCast( skill )

        return AIEffectHelper.notifyFollow( this, this.owner )
    }
}