import { L2VillageMasterInstance } from './L2VillageMasterInstance'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'
import { getPlayerClassById } from '../../base/PlayerClass'
import { Race } from '../../../enums/Race'

export class L2VillageMasterDElfInstance extends L2VillageMasterInstance {
    constructor( template: L2NpcTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2VillageMasterDElfInstance
    }

    static fromTemplate( template: L2NpcTemplate ): L2VillageMasterDElfInstance {
        return new L2VillageMasterDElfInstance( template )
    }

    checkVillageMasterRace( classId: number ): boolean {
        return getPlayerClassById( classId ).race === Race.DARK_ELF
    }
}