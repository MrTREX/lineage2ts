import { L2Npc } from '../L2Npc'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'

export class L2DungeonGatekeeperInstance extends L2Npc {
    constructor( template: L2NpcTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2DungeonGatekeeperInstance
    }

    static fromTemplate( template: L2NpcTemplate ): L2DungeonGatekeeperInstance {
        return new L2DungeonGatekeeperInstance( template )
    }

    getHtmlPath( npcId: number, value: number ): string {
        let fileName: string = npcId.toString()

        if ( value !== 0 ) {
            fileName = `${ fileName }-${ value }`
        }

        return `data/html/teleporter/${ fileName }.htm`
    }
}