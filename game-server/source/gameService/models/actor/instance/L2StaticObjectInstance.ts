import { L2Character } from '../L2Character'
import { emptyTemplate } from '../templates/L2CharacterTemplate'
import { InstanceType } from '../../../enums/InstanceType'
import { IDFactoryCache } from '../../../cache/IDFactoryCache'
import { StaticObjectInstance } from '../../../packets/send/StaticObject'
import { L2PcInstance } from './L2PcInstance'
import { L2ItemInstance } from '../../items/instance/L2ItemInstance'
import { L2Weapon } from '../../items/L2Weapon'
import { L2Item } from '../../items/L2Item'
import { StaticObjectStat } from '../stat/StaticObjectStat'
import { Skill } from '../../Skill'
import { BroadcastHelper } from '../../../helpers/BroadcastHelper'
import { L2StaticObjectType } from '../../../enums/L2StaticObjectType'
import { L2StaticObjectMapLocation } from '../L2StaticObjectMapLocation'

export class L2StaticObjectInstance extends L2Character {
    staticObjectId: number
    meshIndex: number = 0 // 0 - static objects, alternate static objects
    type: L2StaticObjectType = L2StaticObjectType.None
    mapLocation: L2StaticObjectMapLocation

    constructor( staticId: number ) {
        super( IDFactoryCache.getNextId(), emptyTemplate )
        this.instanceType = InstanceType.L2StaticObjectInstance
        this.staticObjectId = staticId
    }

    getId(): number {
        return this.staticObjectId
    }

    isMoveCapable(): boolean {
        return false
    }

    getMeshIndex() {
        return this.meshIndex
    }

    setMeshIndex( index: number ) {
        this.meshIndex = index
        BroadcastHelper.dataBasedOnVisibility( this, StaticObjectInstance( this ) )
    }

    getType() : L2StaticObjectType {
        return this.type
    }

    shouldDescribeState( player: L2PcInstance ) : boolean {
        player.sendOwnedData( StaticObjectInstance( this ) )
        return false
    }

    getLevel(): number {
        return 1
    }

    getActiveWeaponInstance(): L2ItemInstance {
        return null
    }

    getActiveWeaponItem(): L2Weapon {
        return null
    }

    getSecondaryWeaponInstance(): L2ItemInstance {
        return null
    }

    getSecondaryWeaponItem(): L2Item {
        return null
    }

    isAutoAttackable( character: L2Character ): boolean {
        return false
    }

    createStats(): StaticObjectStat {
        return new StaticObjectStat( this )
    }

    getStat(): StaticObjectStat {
        return super.getStat() as StaticObjectStat
    }

    moveToLocation( x: number, y: number, z: number, offset: number ) : boolean {
        return false
    }

    stopMove(): void {}

    async doAttack( target: L2Character ) : Promise<void> {}

    async doCast( skill: Skill ) : Promise<void> {}

    isStaticObject(): boolean {
        return true
    }
}