import { L2DoormenInstance } from './L2DoormenInstance'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'
import { ClanHall } from '../../entity/ClanHall'
import { L2PcInstance } from './L2PcInstance'
import { NpcHtmlMessagePath } from '../../../packets/send/NpcHtmlMessage'
import { DataManager } from '../../../../data/manager'
import { L2Clan } from '../../L2Clan'
import { ClanCache } from '../../../cache/ClanCache'
import { ClanHallManager } from '../../../instancemanager/ClanHallManager'

export class L2ClanHallDoormenInstance extends L2DoormenInstance {
    clanHall: ClanHall = null

    constructor( template: L2NpcTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2ClanHallDoormenInstance
    }

    static fromTemplate( template: L2NpcTemplate ): L2ClanHallDoormenInstance {
        return new L2ClanHallDoormenInstance( template )
    }

    showChatWindowDefault( player: L2PcInstance ): void {
        let npcClanHall = this.getClanHall()
        if ( !npcClanHall ) {
            return
        }

        let owner: L2Clan = ClanCache.getClan( npcClanHall.getOwnerId() )
        let html: string
        let htmlPath: string

        if ( this.isOwnerClan( player ) ) {
            if ( npcClanHall.hasPetEvolve ) {
                htmlPath = 'data/html/clanHallDoormen/doormen2.htm'
                html = DataManager.getHtmlData().getItem( htmlPath )
                                  .replace( /%clanname%/g, owner.getName() )
            } else {
                htmlPath = 'data/html/clanHallDoormen/doormen1.htm'
                html = DataManager.getHtmlData().getItem( htmlPath )
                                  .replace( /%clanname%/g, owner.getName() )
            }
        } else {
            if ( owner && owner.getLeader() ) {
                htmlPath = 'data/html/clanHallDoormen/doormen-no.htm'
                html = DataManager.getHtmlData().getItem( htmlPath )
                                  .replace( /%leadername%/g, owner.getLeaderName() )
                                  .replace( /%clanname%/g, owner.getName() )
            } else {
                htmlPath = 'data/html/clanHallDoormen/emptyowner.htm'
                html = DataManager.getHtmlData().getItem( htmlPath )
                                  .replace( /%hallname%/g, npcClanHall.getName() )
            }
        }

        player.sendOwnedData( NpcHtmlMessagePath( html, htmlPath, player.getObjectId(), this.getObjectId() ) )
    }

    getClanHall(): ClanHall {
        if ( !this.clanHall ) {
            this.clanHall = ClanHallManager.getNearbyClanHall( this.getX(), this.getY() )
        }

        return this.clanHall
    }

    isOwnerClan( player: L2PcInstance ): boolean {
        let npcClanHall = this.getClanHall()
        if ( npcClanHall && player.getClanId() > 0 ) {
            return player.getClanId() === npcClanHall.getOwnerId()
        }

        return false
    }
}