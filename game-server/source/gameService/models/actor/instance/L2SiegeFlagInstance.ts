import { L2Clan } from '../../L2Clan'
import { Siegable } from '../../entity/Siegable'
import { L2Npc } from '../L2Npc'
import { L2PcInstance } from './L2PcInstance'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'
import { TerritoryWarManager } from '../../../instancemanager/TerritoryWarManager'
import { SiegeManager } from '../../../instancemanager/SiegeManager'
import { FortSiegeManager } from '../../../instancemanager/FortSiegeManager'
import { ClanHallSiegeManager } from '../../../instancemanager/ClanHallSiegeManager'
import { L2SiegeClan } from '../../L2SiegeClan'
import { ActionFailed } from '../../../packets/send/ActionFailed'
import { L2Character } from '../L2Character'
import { AIEffectHelper } from '../../../aicontroller/helpers/AIEffectHelper'

export class L2SiegeFlagInstance extends L2Npc {
    clan: L2Clan
    siege: Siegable
    isAdvanced: boolean
    canTalk: boolean

    constructor( player: L2PcInstance, template: L2NpcTemplate, advanced: boolean, outpost: boolean ) {
        super( template )
        this.instanceType = InstanceType.L2SiegeFlagInstance

        if ( TerritoryWarManager.isTWInProgress ) {
            this.clan = player.getClan()
            this.canTalk = false
            if ( !this.clan ) {
                this.deleteMe()
            }

            if ( outpost ) {
                this.isAdvanced = false
                this.setIsInvulnerable( true )
            } else {
                this.isAdvanced = advanced
                this.setIsInvulnerable( false )
            }
            return
        }

        this.clan = player.getClan()
        this.canTalk = true
        this.siege = SiegeManager.getSiege( player.getX(), player.getY(), player.getZ() )

        if ( !this.siege ) {
            this.siege = FortSiegeManager.getSiege( player )
        }

        if ( !this.siege ) {
            this.siege = ClanHallSiegeManager.getSiege( player )
        }

        if ( !this.clan || !this.siege ) {
            throw new Error( 'Can\'t initialize L2SiegeFlagInstance since both clan and siege cannot be established' )
        }

        let siegeClan : L2SiegeClan = this.siege.getAttackerClan( this.clan )
        if ( !siegeClan ) {
            throw new Error( 'L2SiegeFlagInstance cannot find attacker clan' )
        }

        siegeClan.addFlag( this.objectId )
        this.isAdvanced = advanced
        this.setIsInvulnerable( false )
    }

    onForcedAttack( player: L2PcInstance ) {
        return this.onInteraction( player )
    }

    async onInteraction( player: L2PcInstance, interact: boolean = true ) {
        if ( !player || !this.canTarget( player ) ) {
            return
        }

        if ( this.objectId !== player.getTargetId() ) {
            player.setTarget( this )
            return
        }

        if ( interact ) {
            if ( this.isAutoAttackable( player ) && ( Math.abs( player.getZ() - this.getZ() ) < 100 ) ) {
                AIEffectHelper.notifyForceAttack( player, this )
            } else {
                player.sendOwnedData( ActionFailed() )
            }
        }
    }

    canBeAttacked(): boolean {
        return !( this.isInvulnerable() || this.isHpBlocked() )
    }

    isAutoAttackable( attacker: L2Character ): boolean {
        return this.canBeAttacked()
    }
}