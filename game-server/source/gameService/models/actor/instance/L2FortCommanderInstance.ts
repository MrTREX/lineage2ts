import { L2DefenderInstance } from './L2DefenderInstance'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'
import { L2Character } from '../L2Character'
import { Skill } from '../../Skill'
import { FortSiegeManager } from '../../../instancemanager/FortSiegeManager'
import { FortSiegeSpawn } from '../../FortSiegeSpawn'
import { NpcStringIds } from '../../../packets/NpcStringIds'
import { L2Summon } from '../L2Summon'
import { BroadcastHelper } from '../../../helpers/BroadcastHelper'
import { NpcSay } from '../../../packets/send/builder/NpcSay'
import { L2Object } from '../../L2Object'
import { AIEffectHelper } from '../../../aicontroller/helpers/AIEffectHelper'
import { ISpawnLogic } from '../../spawns/ISpawnLogic'
import { NpcSayType } from '../../../enums/packets/NpcSayType'

export class L2FortCommanderInstance extends L2DefenderInstance {
    canTalk: boolean = true

    constructor( template: L2NpcTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2FortCommanderInstance
    }

    static fromTemplate( template: L2NpcTemplate ): L2FortCommanderInstance {
        return new L2FortCommanderInstance( template )
    }

    isAutoAttackable( attacker: L2Character ): boolean {
        if ( !attacker || !attacker.isPlayer() ) {
            return false
        }

        let fort = this.getFort()
        return fort
                && fort.getResidenceId() > 0
                && fort.getSiege().isInProgress()
                && !fort.getSiege().checkIsDefender( attacker.getClan() )
    }

    canAddAggro( attacker: L2Object, damage: number, aggroAmount: number ): boolean {
        if ( !attacker || attacker.isInstanceType( InstanceType.L2FortCommanderInstance ) ) {
            return false
        }

        return super.canAddAggro( attacker, damage, aggroAmount )
    }

    async doDie( killer: L2Character ): Promise<boolean> {
        if ( !await super.doDie( killer ) ) {
            return false
        }

        let fort = this.getFort()
        if ( fort && fort.getSiege().isInProgress() ) {
            fort.getSiege().killedCommander( this )

        }

        return true
    }

    returnHome() {
        if ( !this.isInsideRadius( this.getSpawnLocation(), 200 ) ) {

            this.setIsReturningToSpawnPoint( true )
            this.clearOverhitData()

            if ( this.hasAIController() ) {
                AIEffectHelper.notifyMove( this, this.getSpawnLocation() )
            }
        }
    }

    notifyAboutDamage( attacker: L2Character, damage: number, skill: Skill ): void {
        let spawn: ISpawnLogic = this.getNpcSpawn()
        let currentAttacker = attacker
        if ( spawn && this.canTalk ) {
            let npcId = spawn.getTemplate().getId()
            let commanderSpawns: Array<FortSiegeSpawn> = FortSiegeManager.getCommanderSpawnList( this.getFort().getResidenceId() ).filter( ( siegeSpawn: FortSiegeSpawn ) => {
                return siegeSpawn.getId() === npcId
            } )

            commanderSpawns.forEach( ( siegeSpawn: FortSiegeSpawn ) => {
                let packet: Buffer
                switch ( siegeSpawn.getMessageId() ) {
                    case 1:
                        packet = NpcSay.fromNpcString( this.getObjectId(),
                                NpcSayType.NpcShout,
                            this.getId(),
                                NpcStringIds.ATTACKING_THE_ENEMYS_REINFORCEMENTS_IS_NECESSARY_TIME_TO_DIE ).getBuffer()
                        break

                    case 2:
                        if ( attacker && attacker.isSummon() ) {
                            currentAttacker = ( attacker as L2Summon ).getOwner()
                        }

                        packet = NpcSay.fromNpcString( this.getObjectId(),
                                NpcSayType.NpcShout,
                            this.getId(),
                                NpcStringIds.EVERYONE_CONCENTRATE_YOUR_ATTACKS_ON_S1_SHOW_THE_ENEMY_YOUR_RESOLVE,
                                attacker.getName() ).getBuffer()
                        break

                    case 3:
                        packet = NpcSay.fromNpcString( this.getObjectId(),
                                NpcSayType.NpcShout,
                            this.getId(),
                                NpcStringIds.SPIRIT_OF_FIRE_UNLEASH_YOUR_POWER_BURN_THE_ENEMY ).getBuffer()
                        break
                }

                if ( packet ) {
                    BroadcastHelper.dataInLocation( this, packet, this.getBroadcastRadius() )
                    this.canTalk = false

                    // TODO : either track timeout or use debounced action
                    setTimeout( () => {
                        this.canTalk = true
                    }, 10000 )
                }
            } )
        }

        super.notifyAboutDamage( attacker, damage, skill )
    }

    hasRandomAnimation(): boolean {
        return false
    }
}