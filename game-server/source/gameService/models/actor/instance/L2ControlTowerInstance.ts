import { L2Tower } from '../L2Tower'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'

export class L2ControlTowerInstance extends L2Tower {
    constructor( template: L2NpcTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2ControlTowerInstance
    }

    static fromTemplate( template: L2NpcTemplate ) : L2ControlTowerInstance {
        return new L2ControlTowerInstance( template )
    }
}