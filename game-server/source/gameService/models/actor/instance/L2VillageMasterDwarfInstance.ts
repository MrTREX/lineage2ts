import { L2VillageMasterInstance } from './L2VillageMasterInstance'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'
import { Race } from '../../../enums/Race'
import { getPlayerClassById } from '../../base/PlayerClass'

export class L2VillageMasterDwarfInstance extends L2VillageMasterInstance {
    constructor( template: L2NpcTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2VillageMasterDwarfInstance
    }

    static fromTemplate( template: L2NpcTemplate ): L2VillageMasterDwarfInstance {
        return new L2VillageMasterDwarfInstance( template )
    }

    checkVillageMasterRace( classId: number ): boolean {
        return getPlayerClassById( classId ).race === Race.DWARF
    }
}