import { L2Vehicle } from '../L2Vehicle'
import { L2PcInstance } from './L2PcInstance'
import { VehicleInformation } from '../../../packets/send/VehicleInformation'
import { ILocational, Location } from '../../Location'
import { VehicleDeparture } from '../../../packets/send/VehicleDeparture'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { BroadcastHelper } from '../../../helpers/BroadcastHelper'
import { L2CharacterTemplate } from '../templates/L2CharacterTemplate'
import { InstanceType } from '../../../enums/InstanceType'
import { L2World } from '../../../L2World'
import aigle from 'aigle'
import { SystemMessageBuilder } from '../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../packets/SystemMessageIdValues'
import _ from 'lodash'
import { L2Region } from '../../../enums/L2Region'
import { teleportCharacterToGeometryCoordinates } from '../../../helpers/TeleportHelper'
import { GeometryId } from '../../../enums/GeometryId'

export class L2BoatInstance extends L2Vehicle {

    constructor( template: L2CharacterTemplate ) {
        super( template )

        this.instanceType = InstanceType.L2BoatInstance
    }

    isBoat(): boolean {
        return true
    }

    getId() {
        return 0
    }

    shouldDescribeState( player: L2PcInstance ): boolean {
        player.sendOwnedData( VehicleInformation( this ) )
        return true
    }

    async moveToNextRoutePoint(): Promise<boolean> {
        // TODO : actually move vehicle
        let result = false
        if ( result ) {
            BroadcastHelper.dataInLocation( this, VehicleDeparture( this ), this.getBroadcastRadius() )
        }

        return result
    }

    ejectPlayer( player: L2PcInstance ) {
        super.ejectPlayer( player )

        let location: ILocational = this.getPlayerEjectLocation()
        if ( player.isOnline() ) {
            teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, location.getX(), location.getY(), location.getZ() )
        } else {
            player.setXYZInvisible( location.getX(), location.getY(), location.getZ() )
        }
    }

    static fromTemplate( template: L2NpcTemplate ): L2BoatInstance {
        return new L2BoatInstance( template )
    }

    getBroadcastRadius(): number {
        return L2Region.LargeObjectUpdateRadius
    }

    async payForRide( itemId: number, amount: number, teleportX: number, teleportY: number, teleportZ: number ): Promise<void> {
        let players: Array<L2PcInstance> = L2World.getAllVisiblePlayers( this, 1000 )
        let currentBoat = this
        let teleportLocation = new Location( teleportX, teleportY, teleportZ )

        await aigle.resolve( players ).each( async ( player: L2PcInstance ) => {
            if ( !player ) {
                return
            }

            if ( player.isInBoat() && player.getBoat().getObjectId() === currentBoat.getObjectId() ) {
                if ( itemId > 0 ) {
                    let ticketItem = player.getInventory().getItemByItemId( itemId )

                    if ( !ticketItem && !( await player.getInventory().destroyItemByItemId( itemId, amount, 0, 'Vehicle.payForRide' ) ) ) {
                        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_CORRECT_BOAT_TICKET ) )
                        await player.teleportToLocation( teleportLocation, true )
                        return
                    }
                }

                currentBoat.passengers.push( player.getObjectId() )
            }
        } )

        currentBoat.passengers = _.uniq( currentBoat.passengers )
    }
}