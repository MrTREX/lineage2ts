import { L2MonsterInstance } from './L2MonsterInstance'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'

export class L2EventMonsterInstance extends L2MonsterInstance {
    blockAttack: boolean = false
    dropOnGround: boolean = false

    constructor( template: L2NpcTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2EventMobInstance
    }

    eventSkillAttackBlocked() {
        return this.blockAttack
    }

    eventDropOnGround() {
        return this.dropOnGround
    }
}