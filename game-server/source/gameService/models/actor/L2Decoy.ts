import { L2Character } from './L2Character'
import { L2PcInstance } from './instance/L2PcInstance'
import { L2World } from '../../L2World'
import { L2NpcTemplate } from './templates/L2NpcTemplate'
import { IDFactoryCache } from '../../cache/IDFactoryCache'
import { InstanceType } from '../../enums/InstanceType'
import { DecoyInformation } from '../../packets/send/CharacterInformation'
import { DecayTaskManager } from '../../taskmanager/DecayTaskManager'
import { L2ItemInstance } from '../items/instance/L2ItemInstance'
import { L2Weapon } from '../items/L2Weapon'
import { L2Item } from '../items/L2Item'

export class L2Decoy extends L2Character {
    ownerId: number
    summonerId: number = 0

    constructor( template: L2NpcTemplate, owner: L2PcInstance ) {
        super( IDFactoryCache.getNextId(), template )
        this.instanceType = InstanceType.L2Decoy
        this.ownerId = owner.objectId
        this.setXYZInvisible( owner.getX(), owner.getY(), owner.getZ() )
        this.setIsInvulnerable( false )
    }

    getOwner(): L2PcInstance {
        return L2World.getPlayer( this.ownerId )
    }

    onSpawn() {
        let player = this.getOwner()
        player.sendOwnedData( DecoyInformation( this, player ) )
    }

    onDecay() {
        return this.deleteMe()
    }

    isAutoAttackable( character: L2Character ): boolean {
        return this.getOwner().isAutoAttackable( character )
    }

    stopDecay() {
        DecayTaskManager.cancel( this )
    }

    getActiveWeaponInstance(): L2ItemInstance {
        return null
    }

    getActiveWeaponItem(): L2Weapon {
        return null
    }

    getSecondaryWeaponItem(): L2Item {
        return null
    }

    getSecondaryWeaponInstance() : L2ItemInstance {
        return null
    }

    getId() : number {
        return this.getTemplate().getId()
    }

    getLevel(): number {
        return this.getTemplate().getLevel()
    }

    deleteMe(): Promise<void> {
        this.decayMe()
        this.getOwner().setDecoy( null )

        return Promise.resolve()
    }

    unSummon() : Promise<void> {
        if ( this.isVisible() && !this.isDead() ) {
            return this.deleteMe()
        }
    }

    getActingPlayer(): L2PcInstance {
        return this.getOwner()
    }

    getActingPlayerId() : number {
        return this.ownerId
    }

    getTemplate(): L2NpcTemplate {
        return super.getTemplate() as L2NpcTemplate
    }

    shouldDescribeState( player: L2PcInstance ) : boolean {
        player.sendOwnedData( DecoyInformation( this, player ) )
        return true
    }

    getSummonerId(): number {
        return this.summonerId
    }

    setSummoner( objectId: number ) {
        this.summonerId = objectId
    }
}