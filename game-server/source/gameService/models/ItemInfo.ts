import { L2Item } from './items/L2Item'
import { TradeItem } from './TradeItem'
import { InventoryUpdateStatus } from '../enums/InventoryUpdateStatus'
import { IDFactoryCache } from '../cache/IDFactoryCache'
import { EnchantOptionsParameters, getEnchantOptions } from './options/EnchantOptions'
import { L2ItemDefaults } from '../enums/L2ItemDefaults'
import { ElementalAndEnchantProperties } from './items/ElementalAndEnchantProperties'

const enum EquippedStatus {
    No,
    Yes
}

export class ItemInfo implements ElementalAndEnchantProperties {
    objectId: number
    item: L2Item
    enchant: number
    augmentation: number

    count: number
    price: number
    type1: number
    type2: number

    equipped: EquippedStatus = EquippedStatus.No
    change: InventoryUpdateStatus
    mana: number
    time: number

    location: number
    elementAttackType: number
    elementAttackPower: number
    elementalDefenceAttributes: ReadonlyArray<number>
    options: EnchantOptionsParameters

    fromTemplate( item : L2Item, amount: number = 1 ) {
        this.objectId = IDFactoryCache.getNextId()
        this.item = item
        this.enchant = item.getDefaultEnchantLevel()

        this.augmentation = 0
        this.count = amount
        this.type1 = 0
        this.type2 = 0

        this.equipped = EquippedStatus.No
        this.change = InventoryUpdateStatus.Unchanged
        this.mana = this.item.getDuration()
        this.time = -9999

        this.location = 0
        this.elementAttackType = item.getAttackElementType() ?? L2ItemDefaults.AttackElementType
        this.elementAttackPower = item.getAttackElementPower() ?? L2ItemDefaults.AttackElementPower
        this.elementalDefenceAttributes = structuredClone( item.elementalDefenceAttributes )

        this.options = getEnchantOptions( item.itemId, this.enchant )

        return this
    }

    fromTradeItem( item: TradeItem ): ItemInfo {
        if ( !item ) {
            return
        }

        this.objectId = item.objectId
        this.item = item.itemTemplate
        this.enchant = item.enchant
        this.augmentation = 0
        this.count = item.count
        this.type1 = item.type1
        this.type2 = item.type2
        this.equipped = EquippedStatus.No
        this.change = InventoryUpdateStatus.Unchanged
        this.mana = -1
        this.time = -9999
        this.location = item.location

        this.elementAttackType = item.elementaryAttackType
        this.elementAttackPower = item.elementaryAttackPower
        this.elementalDefenceAttributes = item.elementalDefenceAttributes
        this.options = item.enchantOptions

        return this
    }

    getAttackElementPower() {
        return this.elementAttackPower
    }

    getAttackElementType() {
        return this.elementAttackType
    }

    getAugmentationBonus() {
        return this.augmentation
    }

    getChange() {
        return this.change
    }

    getCount() {
        return this.count
    }

    getCustomType1() {
        return this.type1
    }

    getCustomType2() {
        return this.type2
    }

    getEnchant() {
        return this.enchant
    }

    getEnchantOptions(): EnchantOptionsParameters {
        return this.options
    }

    getEquipped() {
        return this.equipped
    }

    getItem(): L2Item {
        return this.item
    }

    getLocation() {
        return this.location
    }

    getMana() {
        return this.mana
    }

    getObjectId() {
        return this.objectId
    }

    getTime() {
        return this.time
    }
}