import { Entry } from './Entry'
import { L2ItemInstance } from '../items/instance/L2ItemInstance'
import { Ingredient } from './Ingredient'
import { ItemInfo } from './ItemInfo'
import { ItemTypes } from '../../values/InventoryValues'
import _ from 'lodash'

export class PreparedEntry extends Entry {
    taxAmount: number = 0

    getTaxAmount(): number {
        return this.taxAmount
    }

    constructor( template: Entry, item: L2ItemInstance, applyTaxes: boolean, maintainEnchantment: boolean, taxRate: number ) {
        super( template.getEntryId() * 100000 )

        if ( maintainEnchantment && item ) {
            this.entryId += item.getEnchantLevel()
        }

        let adenaAmount = 0
        let entry = this
        _.each( template.getIngredients(), ( ingredient: Ingredient ) => {
            if ( ingredient.getItemId() === ItemTypes.Adena ) {
                // Tax ingredients added only if taxes enabled
                if ( ingredient.isTaxIngredient ) {
                    // if taxes are to be applied, modify/add the adena count based on the template adena/ancient adena count
                    if ( applyTaxes ) {
                        entry.taxAmount += Math.round( ingredient.getItemCount() * taxRate )
                    }
                } else {
                    adenaAmount += ingredient.getItemCount()
                }
                return
            }

            let copiedIngredient = ingredient.getCopy()

            if ( maintainEnchantment && item && ingredient.isArmorOrWeapon() ) {
                copiedIngredient.setItemInfo( ItemInfo.fromItemInstance( item ) )
            }

            entry.ingredients.push( copiedIngredient )
        } )

        adenaAmount += this.taxAmount
        if ( adenaAmount > 0 ) {
            this.ingredients.push( new Ingredient().setParameters( ItemTypes.Adena, adenaAmount, false, false ) )
        }

        _.each( template.getProducts(), ( ingredient: Ingredient ) => {
            if ( !ingredient.isStackable() ) {
                entry.stackable = false
            }

            let copiedIngredient = ingredient.getCopy()

            if ( maintainEnchantment && item && ingredient.isArmorOrWeapon() ) {
                copiedIngredient.setItemInfo( ItemInfo.fromItemInstance( item ) )

            } else if ( ingredient.isArmorOrWeapon() && ( ingredient.getTemplate().getDefaultEnchantLevel() > 0 ) ) {
                copiedIngredient.setItemInfo( ItemInfo.fromEnchantmentLevel( ingredient.getTemplate().getDefaultEnchantLevel() ) )
            }

            entry.products.push( copiedIngredient )
        } )
    }
}