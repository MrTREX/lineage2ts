import { Ingredient } from './Ingredient'

export class Entry {
    entryId: number
    stackable: boolean
    products: Array<Ingredient> = []
    ingredients: Array<Ingredient> = []

    constructor( id: number ) {
        this.entryId = id
    }

    getEntryId() {
        return this.entryId
    }

    getIngredients() {
        return this.ingredients
    }

    getProducts() {
        return this.products
    }

    getTaxAmount() {
        return 0
    }

    isStackable() {
        return this.stackable
    }
}