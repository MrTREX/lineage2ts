import { ItemInfo } from './ItemInfo'
import { ItemInstanceType } from '../items/ItemInstanceType'
import { DataManager } from '../../../data/manager'

export class Ingredient {
    itemId: number = 0
    itemCount: number = 0
    isTaxIngredient: boolean = false
    maintainIngredient: boolean = false
    itemInfo: ItemInfo

    getItemId() {
        return this.itemId
    }

    isStackable() {
        let template = this.getTemplate()
        return template && template.isStackable()
    }

    getItemCount() {
        return this.itemCount
    }

    getWeight() {
        let template = this.getTemplate()
        return template ? template.getWeight() : 0
    }

    getEnchantLevel() {
        return this.itemInfo ? this.itemInfo.getEnchantLevel() : 0
    }

    getTemplate() {
        return DataManager.getItems().getTemplate( this.itemId )
    }

    getMaintainIngredient() {
        return this.maintainIngredient
    }

    getCopy(): Ingredient {
        return new Ingredient().setParameters( this.itemId, this.itemCount, this.isTaxIngredient, this.maintainIngredient )
    }

    setParameters( itemId: number, amount: number, isTaxIngredient: boolean, maintainIngredient: boolean ): Ingredient {
        this.itemId = itemId
        this.itemCount = amount
        this.isTaxIngredient = isTaxIngredient
        this.maintainIngredient = maintainIngredient

        return this
    }

    setItemInfo( info: ItemInfo ) {
        this.itemInfo = info
    }

    isArmorOrWeapon() {
        let template = this.getTemplate()
        return template && ( template.isInstanceType( ItemInstanceType.L2Armor ) || template.isInstanceType( ItemInstanceType.L2Weapon ) )
    }

    setItemCount( value: number ) {
        this.itemCount = value
    }

    getItemInfo(): ItemInfo {
        return this.itemInfo
    }
}