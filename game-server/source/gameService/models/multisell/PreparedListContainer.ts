import { ListContainer } from './ListContainer'
import { L2PcInstance } from '../actor/instance/L2PcInstance'
import { L2Npc } from '../actor/L2Npc'
import { L2ItemInstance } from '../items/instance/L2ItemInstance'
import { PreparedEntry } from './PreparedEntry'
import { Entry } from './Entry'
import { Ingredient } from './Ingredient'
import _ from 'lodash'

export class PreparedListContainer extends ListContainer {
    npcObjectId: number

    constructor( template: ListContainer, isInventoryOnly: boolean, player: L2PcInstance, npc: L2Npc ) {
        super( template.getListId() )

        this.maintainEnchantment = template.getMaintainEnchantment()
        this.applyTaxes = false

        let taxRate = 0
        if ( npc ) {
            this.npcObjectId = npc.getObjectId()
            if ( template.getApplyTaxes() && npc.isInTown && ( npc.getCastle().getOwnerId() > 0 ) ) {
                this.applyTaxes = true
                taxRate = npc.getCastle().getTaxRate()
            }
        }

        if ( isInventoryOnly ) {
            if ( !player ) {
                return
            }

            let items : Array<L2ItemInstance>
            if ( this.getMaintainEnchantment() ) {
                items = player.getInventory().getUniqueItemsByEnchantLevel( false, false, false )
            } else {
                items = player.getInventory().getUniqueItems( false, false, false )
            }

            this.entries = []
            let container = this

            _.each( items, ( item: L2ItemInstance ) => {
                if ( !item.isEquipped() && ( ( item.isArmor() ) || ( item.isWeapon() ) ) ) {

                    _.each( template.getEntries(), ( entry: Entry ) => {
                        _.each( entry.getIngredients(), ( ingredient: Ingredient ) => {
                            if ( item.getId() === ingredient.getItemId() ) {
                                container.entries.push( new PreparedEntry( entry, item, container.getApplyTaxes(), container.getMaintainEnchantment(), taxRate ) )
                                return false
                            }
                        } )
                    } )
                }
            } )

            return
        }

        this.entries = template.getEntries().map( ( entry: Entry ) => {
            return new PreparedEntry( entry, null, this.getApplyTaxes(), this.getMaintainEnchantment(), taxRate )
        } )
    }
}