import { Entry } from './Entry'

// TODO : review and convert to normal interface
export class ListContainer {
    listId: number
    applyTaxes: boolean
    maintainEnchantment: boolean
    entries: Array<Entry> = []
    npcsAllowed: Array<number> = []

    constructor( id: number ) {
        this.listId = id
    }

    getEntries(): Array<Entry> {
        return this.entries
    }

    getEntry( id: number ) : Entry {
        return this.entries.find( entry => entry.getEntryId() === id )
    }

    getListId(): number {
        return this.listId
    }

    getMaintainEnchantment() {
        return this.maintainEnchantment
    }

    isNpcAllowed( id: number ): boolean {
        return this.npcsAllowed.length === 0 || this.npcsAllowed.includes( id )
    }

    isNpcOnly(): boolean {
        return this.npcsAllowed.length > 0
    }

    getApplyTaxes() {
        return this.applyTaxes
    }
}