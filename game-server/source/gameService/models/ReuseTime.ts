import { L2ReuseTime, L2Timestamp } from './SkillTimestamp'
import { Skill } from './Skill'
import { ObjectPool } from '../helpers/ObjectPoolHelper'
import { L2ItemInstance } from './items/instance/L2ItemInstance'

export const ReuseTimestampObjectPool = new ObjectPool<L2Timestamp>( 'ReuseTimestamp', () => new L2Timestamp() )

abstract class BaseReuseTime<T> {
    protected items: Map<number,L2Timestamp> = new Map<number, L2Timestamp>()

    reset() : void {
        this.items.forEach( ( value: L2Timestamp ) => ReuseTimestampObjectPool.recycleValue( value ) )
        this.items.clear()
    }

    protected getItemByCode( code: number ) : L2ReuseTime {
        return this.items.get( code )
    }

    abstract getItem( data: T ) : L2ReuseTime

    removeItem( code: number ) : void {
        let item = this.items.get( code )
        if ( !item ) {
            return
        }

        ReuseTimestampObjectPool.recycleValue( item )
        this.items.delete( code )
    }

    getAllItems() : ReadonlyMap<number, L2ReuseTime> {
        return this.items
    }

    removeInactive() : void {
        let currentTime = Date.now()

        this.items.forEach( ( item: L2Timestamp, key: number ) => {
            if ( item.getExpirationTime() < currentTime ) {
                this.items.delete( key )
            }
        } )
    }
}

export class SkillReuseTime extends BaseReuseTime<Skill> {
    add( skill: Skill, reuseTime: number, expirationTime: number = Date.now() + reuseTime ) : void {
        let timestamp = ReuseTimestampObjectPool.getValue()

        timestamp.id = skill.getId()
        timestamp.level = skill.getLevel()
        timestamp.reuseMs = reuseTime
        timestamp.reuseSeconds = Math.floor( reuseTime / 1000 )
        timestamp.expirationTime = expirationTime

        this.items.set( skill.getReuseHashCode(), timestamp )
    }

    getItem( skill: Skill ): L2ReuseTime {
        return this.getItemByCode( skill.getReuseHashCode() )
    }
}

export class ItemReuseTime extends BaseReuseTime<L2ItemInstance> {
    add( item: L2ItemInstance, reuseTime: number, expirationTime: number = Date.now() + reuseTime ) : void {
        let timestamp = ReuseTimestampObjectPool.getValue()

        timestamp.id = item.getId()
        timestamp.level = item.getSharedReuseGroup()
        timestamp.reuseMs = reuseTime
        timestamp.reuseSeconds = Math.floor( reuseTime / 1000 )
        timestamp.expirationTime = expirationTime

        this.items.set( item.getObjectId(), timestamp )
    }

    getItem( item: L2ItemInstance ): L2ReuseTime {
        return this.getItemByCode( item.getObjectId() )
    }

    getRemainingTimeByGroup( code: number ) : number {
        let currentTime = Date.now()
        let remainingTime = 0

        this.items.forEach( ( item: L2Timestamp, key: number ) => {
            if ( item.getExpirationTime() < currentTime ) {
                this.items.delete( key )
            }

            if ( item.getLevel() === code ) {
                remainingTime = Math.max( item.getRemainingMs(), remainingTime )
            }
        } )

        return remainingTime
    }
}