import { InventorySlot } from '../../values/InventoryValues'
import { L2ItemSlots } from '../../enums/L2ItemSlots'

export const InventoryHelper = {
    getPaperdollIndex( slot: L2ItemSlots ): InventorySlot {
        switch ( slot ) {
            case L2ItemSlots.Underware:
                return InventorySlot.Under
            case L2ItemSlots.RightEar:
                return InventorySlot.RightEar
            case L2ItemSlots.AllEarSlots:
            case L2ItemSlots.LeftEar:
                return InventorySlot.LeftEar
            case L2ItemSlots.Neck:
                return InventorySlot.Neck
            case L2ItemSlots.RightFinger:
            case L2ItemSlots.AllFingerSlots:
                return InventorySlot.RightFinger
            case L2ItemSlots.LeftFingers:
                return InventorySlot.LeftFinger
            case L2ItemSlots.Head:
                return InventorySlot.Head
            case L2ItemSlots.RightHand:
            case L2ItemSlots.AllHandSlots:
                return InventorySlot.RightHand
            case L2ItemSlots.LeftHand:
                return InventorySlot.LeftHand
            case L2ItemSlots.Gloves:
                return InventorySlot.Gloves
            case L2ItemSlots.Chest:
            case L2ItemSlots.FullArmor:
            case L2ItemSlots.AllDressSlots:
                return InventorySlot.Chest
            case L2ItemSlots.Legs:
                return InventorySlot.Legs
            case L2ItemSlots.Feet:
                return InventorySlot.Feet
            case L2ItemSlots.Back:
                return InventorySlot.Cloak
            case L2ItemSlots.Hair:
            case L2ItemSlots.AllHairSlots:
                return InventorySlot.Hair
            case L2ItemSlots.HairSecondary:
                return InventorySlot.HairSecondary
            case L2ItemSlots.RightBracelet:
                return InventorySlot.RightBracelet
            case L2ItemSlots.LeftBracelet:
                return InventorySlot.LeftBracelet
            case L2ItemSlots.Decoration:
                return InventorySlot.Decoration1
            case L2ItemSlots.Belt:
                return InventorySlot.Belt
        }

        return InventorySlot.None
    },
}