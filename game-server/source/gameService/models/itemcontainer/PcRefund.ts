import { ItemContainer } from './ItemContainer'
import { L2PcInstance } from '../actor/instance/L2PcInstance'
import { L2World } from '../../L2World'
import { ItemLocation } from '../../enums/ItemLocation'
import { L2ItemInstance } from '../items/instance/L2ItemInstance'
import { ItemManagerCache } from '../../cache/ItemManagerCache'
import { ConfigManager } from '../../../config/ConfigManager'
import aigle from 'aigle'
import _ from 'lodash'

export class PcRefund extends ItemContainer {

    constructor( player: L2PcInstance ) {
        super()
        this.ownerId = player.getObjectId()
    }

    getOwner(): L2PcInstance {
        return L2World.getPlayer( this.ownerId )
    }

    getBaseLocation(): ItemLocation {
        return ItemLocation.REFUND
    }

    getName(): string {
        return 'Refund'
    }

    async deleteMe() : Promise<void> {
        await aigle.resolve( this.getItems() ).eachLimit( 10, ( item: L2ItemInstance ): Promise<void> => {
            if ( item ) {
                return ItemManagerCache.destroyItem( item )
            }
        } )

        this.emptyInventory()
    }

    async addToInventory( item: L2ItemInstance ): Promise<void> {
        await super.addToInventory( item )

        if ( this.getSize() > ConfigManager.character.getMaximumRefundCapacity() ) {
            let itemToRemove: L2ItemInstance = _.last( this.getItems() )

            if ( itemToRemove ) {
                await this.removeItem( itemToRemove )
                return ItemManagerCache.destroyItem( itemToRemove )
            }
        }
    }

    restore(): Promise<void> {
        return Promise.resolve()
    }

    getPlayerOwnerId(): number {
        return this.getOwnerId()
    }
}