import { L2PetInstance } from '../actor/instance/L2PetInstance'
import { L2World } from '../../L2World'
import { ItemLocation } from '../../enums/ItemLocation'
import { L2ItemInstance } from '../items/instance/L2ItemInstance'
import { InventoryInstanceType } from './InventoryTypes'
import { StatsListener } from './listeners/StatsListener'
import aigle from 'aigle'
import { PetInfoAnimation } from '../../packets/send/PetInfo'
import { PetInventoryUpdateBuilder } from '../../packets/send/builder/PetInventoryUpdate'
import { PlayableInventory } from './PlayableInventory'

export class PetInventory extends PlayableInventory {

    constructor( objectId: number ) {
        super()
        this.instanceType = InventoryInstanceType.PetInventory
        this.ownerId = objectId
        this.paperdollListeners = [
            StatsListener,
        ]
    }

    getOwner(): L2PetInstance {
        return L2World.getObjectById( this.ownerId ) as L2PetInstance
    }

    getEquipLocation(): ItemLocation {
        return ItemLocation.PET_EQUIP
    }

    async restore() {
        await super.restore()
        this.markRestored()

        let inventory = this
        let pet = this.getOwner()

        this.inventoryUpdate = new PetInventoryUpdateBuilder()

        return aigle.resolve( this.getItems() ).each( async ( item: L2ItemInstance ) => {
            if ( item.isEquipped() && !item.getItem().checkEquipConditions( pet, false ) ) {
                return inventory.unEquipItemInSlot( item.getLocationSlot() )
            }
        } ) as unknown as Promise<void>
    }

    getBaseLocation(): ItemLocation {
        return ItemLocation.PET
    }

    validateCapacityForItem( item: L2ItemInstance ): boolean {
        let slots = !( item.isStackable() && this.getItemByItemId( item.getId() ) ) && !item.getItem().hasExImmediateEffect() ? 1 : 0
        return this.validateCapacity( slots )
    }

    validateCapacity( slots: number ): boolean {
        return ( this.getSize() + slots ) <= this.getOwner().getInventoryLimit()
    }

    refreshWeightOperation() {
        super.refreshWeightOperation()
        this.getOwner().updateAndBroadcastStatus( PetInfoAnimation.SummonAnimation )
    }

    validateWeight( weight: number ): boolean {
        return ( this.totalWeight + weight ) <= this.getOwner().getMaxLoad()
    }

    validateCapacityForItemCount( item: L2ItemInstance, amount: number ): boolean {
        return this.validateWeight( amount * item.getItem().getWeight() )
    }

    getPlayerOwnerId(): number {
        return this.getOwner()?.getActingPlayerId()
    }
}