import { L2ItemInstance } from '../items/instance/L2ItemInstance'
import { L2PcInstance } from '../actor/instance/L2PcInstance'
import { ItemLocation } from '../../enums/ItemLocation'
import { PacketDispatcher } from '../../PacketDispatcher'
import { L2Item } from '../items/L2Item'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { ItemManagerCache } from '../../cache/ItemManagerCache'
import { L2World } from '../../L2World'
import { TradeItem } from '../TradeItem'
import { L2PcInstanceValues } from '../../values/L2PcInstanceValues'
import { InventoryInstanceType } from './InventoryTypes'
import { ArmorSetListener } from './listeners/ArmorSetListener'
import { BowCrossRodListener } from './listeners/BowCrossRodListener'
import { ItemSkillsListener } from './listeners/ItemSkillsListener'
import { BraceletListener } from './listeners/BraceletListener'
import { InventorySlot, ItemTypes } from '../../values/InventoryValues'
import { PlayerShortcutCache } from '../../cache/PlayerShortcutCache'
import { StatsListener } from './listeners/StatsListener'
import aigle from 'aigle'
import { ShortcutType } from '../../enums/ShortcutType'
import _ from 'lodash'
import { PrivateStoreType } from '../../enums/PrivateStoreType'
import { PlayerActionOverride } from '../../values/PlayerConditions'
import { PlayableInventory } from './PlayableInventory'
import { InventoryUpdateBuilder } from '../../packets/send/builder/InventoryUpdate'
import { PlayerPermission } from '../../enums/PlayerPermission'
import { L2ItemSlots } from '../../enums/L2ItemSlots'
import { InventoryBlockMode } from '../../enums/InventoryBlockMode'
import { ListenerCache } from '../../cache/ListenerCache'
import { EventType, PlayerUnEquipsItemEvent } from '../events/EventType'
import { EventPoolCache } from '../../cache/EventPoolCache'
import { EtcStatusUpdate } from '../../packets/send/EtcStatusUpdate'
import { PetInfo } from '../../packets/send/PetInfo'
import { UserInfo } from '../../packets/send/UserInfo'
import { ExBrExtraUserInfo } from '../../packets/send/ExBrExtraUserInfo'
import { StatusUpdate, StatusUpdateProperty } from '../../packets/send/builder/StatusUpdate'

export class PcInventory extends PlayableInventory {
    blockItems: Array<number> = []
    questSlots: number = 0

    blockMode: InventoryBlockMode = InventoryBlockMode.None

    constructor( objectId: number ) {
        super()
        this.instanceType = InventoryInstanceType.PcInventory
        this.ownerId = objectId

        this.paperdollListeners = [
            ArmorSetListener,
            BowCrossRodListener,
            ItemSkillsListener,
            BraceletListener,
            StatsListener,
        ]
    }

    async addAdena( amount: number, referenceId: number, reason: string ): Promise<L2ItemInstance> {
        return this.addItem( ItemTypes.Adena, amount, referenceId, reason )
    }

    async addAncientAdena( amount: number, referenceId: number, reason: string ): Promise<L2ItemInstance> {
        return this.addItem( ItemTypes.AncientAdena, amount, referenceId, reason )
    }

    addToInventory( item: L2ItemInstance ): Promise<void> {
        if ( item.isQuestItem() ) {
            this.questSlots++
        }

        return super.addToInventory( item )
    }

    getBaseLocation(): ItemLocation {
        return ItemLocation.INVENTORY
    }

    getOwner(): L2PcInstance {
        return L2World.getPlayer( this.ownerId )
    }

    getEquipLocation(): ItemLocation {
        return ItemLocation.PAPERDOLL
    }

    refreshWeightOperation(): void {
        super.refreshWeightOperation()
        this.getOwner().refreshOverloadPenalty()

        /*
            UserInfo packet must be sent for any inventory updates be sent afterwards.
         */
        PacketDispatcher.sendDebouncedPacket( this.getOwnerId(), UserInfo )
    }

    removeItem( item: L2ItemInstance ): Promise<void> {
        let player: L2PcInstance = this.getOwner()
        PlayerShortcutCache.deleteById( player, item.getObjectId(), ShortcutType.ITEM )

        if ( item.getObjectId() === player.getActiveEnchantItemId() ) {
            player.setActiveEnchantItemId( L2PcInstanceValues.EmptyId )
        }

        if ( item.isQuestItem() ) {
            this.questSlots = Math.max( 0, this.questSlots - 1 )
        }

        return super.removeItem( item )
    }

    validateCapacity( slots: number ): boolean {
        return this.validatePlayerCapacity( slots, false )
    }

    validateWeight( weight: number ): boolean {
        let player = this.getOwner()
        if ( player.isGM() && player.getWeightlessMode() && player.getAccessLevel().hasPermission( PlayerPermission.AllowTransaction ) ) {
            return true
        }

        return ( this.totalWeight + weight ) <= player.getMaxLoad()
    }

    adjustAvailableItem( tradeItem: TradeItem ) {
        let notAllEquipped: boolean = this.getItemsByItemId( tradeItem.itemTemplate.getId() ).some( ( item: L2ItemInstance ) => {
            if ( item.isEquipable() ) {
                return !item.isEquipped()
            }

            return true
        } )

        if ( notAllEquipped ) {
            let adjItem: L2ItemInstance = this.getItemByItemId( tradeItem.itemTemplate.getId() )
            tradeItem.objectId = adjItem.getObjectId()
            tradeItem.enchant = adjItem.getEnchantLevel()

            if ( adjItem.getCount() < tradeItem.count ) {
                tradeItem.count = adjItem.getCount()
            }

            return
        }

        tradeItem.count = 0
    }

    /*
        Mainly deals with runes in inventory that have skills, or equipped items that have
        enchant skills.
     */
    applyItemSkills() {
        return aigle.resolve( this.getItems() ).each( async ( item: L2ItemInstance ) => {
            if ( item.hasRuneSkills() ) {
                await item.giveSkillsToOwner()
            }

            if ( item.shouldApplyEnchantStats() ) {
                await item.applyEnchantStats()
            }
        } )
    }

    canEquipCloak() {
        return this.getOwner().getStat().canEquipCloak()
    }

    canManipulateWithItemId( itemId: number ): boolean {
        return ( this.blockMode !== InventoryBlockMode.InventoryDisallowed || !this.blockItems.includes( itemId ) ) && ( this.blockMode !== 1 || this.blockItems.includes( itemId ) )
    }

    checkInventorySlotsAndWeight( items: Array<L2Item>, sendMessage: boolean, sendSkillMessage: boolean ) {
        let weight = 0
        let requiredSlots = 0
        let inventory = this

        _.each( items, ( item: L2Item ) => {
            if ( !item.isStackable() || ( inventory.getInventoryItemCount( item.getId(), -1 ) <= 0 ) ) {
                requiredSlots++
            }

            weight += item.getWeight()
        } )

        let inventoryStatusOK = this.validateCapacity( requiredSlots ) && this.validateWeight( weight )
        if ( !inventoryStatusOK && sendMessage ) {
            PacketDispatcher.sendOwnedData( this.ownerId, SystemMessageBuilder.fromMessageId( SystemMessageIds.SLOTS_FULL ) )
            if ( sendSkillMessage ) {
                PacketDispatcher.sendOwnedData( this.ownerId, SystemMessageBuilder.fromMessageId( SystemMessageIds.WEIGHT_EXCEEDED_SKILL_UNAVAILABLE ) )
            }
        }

        return inventoryStatusOK
    }

    getAdenaItem(): L2ItemInstance {
        return this.getItemByItemId( ItemTypes.Adena )
    }

    getAllItemsByItemId( itemId: number, includeEquipped: boolean ): Array<L2ItemInstance> {
        return this.getItemsByItemId( itemId ).filter( ( item: L2ItemInstance ) => {
            return ( includeEquipped || !item.isEquipped() )
        } )
    }

    getAllItemsByItemIdWithEnchant( itemId: number, enchantLevel: number, includeEquipped: boolean ): Array<L2ItemInstance> {
        return this.getItemsByItemId( itemId ).filter( ( item: L2ItemInstance ) => {
            return item.getEnchantLevel() === enchantLevel
                    && ( includeEquipped || !item.isEquipped() )
        } )
    }

    getAncientAdenaCount(): number {
        let item = this.getItemByItemId( ItemTypes.AncientAdena )
        return item ? item.getCount() : 0
    }

    getAncientAdenaItem(): L2ItemInstance {
        return this.getItemByItemId( ItemTypes.AncientAdena )
    }

    getAugmentedItems(): Array<L2ItemInstance> {
        return this.getItems().filter( ( item: L2ItemInstance ) => {
            return item.isAugmented()
        } )
    }

    getAvailableItems( allowAdena: boolean, allowNonTradeable: boolean, onlyFreightItems: boolean, allowNonSellable: boolean ): Array<L2ItemInstance> {
        let inventory = this
        let player: L2PcInstance = this.getOwner()

        return this.getItems().filter( ( item: L2ItemInstance ) => {
            if ( !item
                    || !item.isAvailable( player, allowAdena, allowNonTradeable )
                    || !inventory.canManipulateWithItemId( item.getId() ) ) {
                return false
            }

            if ( onlyFreightItems ) {
                return item.isFreightable() && item.getItemLocation() === ItemLocation.INVENTORY
            }

            return !( !allowNonSellable && !item.isSellable() )
        } )
    }

    getAvailableSellableItems(): Array<TradeItem> {
        let player: L2PcInstance = this.getOwner()

        let sellList = player.getSellList()
        if ( !sellList ) {
            return []
        }

        return this.getItems().reduce( ( availableItems: Array<TradeItem>, item: L2ItemInstance ) => {
            if ( item && item.isAvailable( player, false, false ) ) {
                let tradeItem: TradeItem = sellList.adjustAvailableItem( item )
                if ( tradeItem ) {
                    availableItems.push( tradeItem )
                }
            }

            return availableItems
        }, [] )
    }

    getBlockItems(): Array<number> {
        return this.blockItems
    }

    getBlockMode(): InventoryBlockMode {
        return this.blockMode
    }

    getElementItems(): Array<L2ItemInstance> {
        return this.getItems().filter( ( item: L2ItemInstance ) => {
            return item && !_.isEmpty( item.getElementals() )
        } )
    }

    getFullSize( questOnly: boolean ): number {
        if ( questOnly ) {
            return this.questSlots
        }

        return this.getSize() - this.questSlots
    }

    getUniqueItems( allowAdena: boolean, allowAncientAdena: boolean, onlyAvailable: boolean ): Array<L2ItemInstance> {
        let outcome: Array<L2ItemInstance> = []
        let owner = this.getOwner()
        let itemIds = new Set<number>()

        this.getItems().forEach( ( item: L2ItemInstance ) => {
            if ( !allowAdena && item.getId() === ItemTypes.Adena ) {
                return
            }
            if ( !allowAncientAdena && item.getId() === ItemTypes.AncientAdena ) {
                return
            }

            if ( !itemIds.has( item.getId() ) && ( !onlyAvailable || ( item.isSellable() && item.isAvailable( owner, false, false ) ) ) ) {
                outcome.push( item )
                itemIds.add( item.getId() )
            }
        } )

        return outcome
    }

    getUniqueItemsByEnchantLevel( allowAdena: boolean, allowAncientAdena: boolean, onlyAvailable: boolean ): Array<L2ItemInstance> {
        let items: Array<L2ItemInstance> = []
        let player = this.getOwner()
        let itemCheck = new Set<string>()

        const getItemHash = ( item: L2ItemInstance ) => `${ item.getId() }-${ item.getEnchantLevel() }`

        this.getItems().forEach( ( item: L2ItemInstance ) => {
            if ( !item ) {
                return false
            }

            if ( ( !allowAdena && ( item.getId() === ItemTypes.Adena ) ) ) {
                return false
            }

            if ( ( !allowAncientAdena && ( item.getId() === ItemTypes.AncientAdena ) ) ) {
                return false
            }

            let hash = getItemHash( item )
            if ( !itemCheck.has( hash ) && ( !onlyAvailable || ( item.isSellable() && item.isAvailable( player, false, false ) ) ) ) {
                items.push( item )
                itemCheck.add( hash )
            }
        } )

        return items
    }

    hasInventoryBlock() {
        return this.blockMode !== InventoryBlockMode.None && this.blockItems.length > 0
    }

    async reduceAdena( count: number, reason: string = null ): Promise<boolean> {
        if ( count > 0 ) {
            return !!( await this.destroyItemByItemId( ItemTypes.Adena, count, this.ownerId, reason ) )
        }

        return false
    }

    async reduceAncientAdena( amount: number, reason: string = null ): Promise<boolean> {
        return !!( amount > 0 && await this.destroyItemByItemId( ItemTypes.AncientAdena, amount, this.ownerId, reason ) )
    }

    setInventoryBlock( items: Array<number>, mode: InventoryBlockMode ) {
        this.blockMode = mode
        this.blockItems = items

        this.getOwner().sendInventoryUpdate( false )
    }

    unblock() {
        this.blockMode = InventoryBlockMode.None
        this.blockItems = []

        this.getOwner().sendInventoryUpdate( false )
    }

    validateCapacityByItemId( itemId: number ) {
        let item: L2ItemInstance = this.getItemByItemId( itemId )
        if ( !item || !( item.isStackable() && ( this.getInventoryItemCount( itemId, -1 ) > 0 ) ) ) {
            this.validatePlayerCapacity( 1, ItemManagerCache.getTemplate( itemId ).isQuestItem() )
        }

        return true
    }

    validateCapacityForItem( item: L2ItemInstance ): boolean {
        if ( item.getItem().hasExImmediateEffect() || ( item.isStackable() && ( this.getInventoryItemCount( item.getId(), -1 ) > 0 ) ) ) {
            return true
        }

        return this.validatePlayerCapacity( 1, item.isQuestItem() )
    }

    validatePlayerCapacity( slots: number, questItem: boolean ): boolean {
        if ( !questItem ) {
            return ( this.getSize() - this.questSlots + slots ) <= this.getOwner().getInventoryLimit()
        }

        return ( this.questSlots + slots ) <= this.getOwner().getQuestInventoryLimit()
    }

    async unEquipItemInBodySlot( slot: L2ItemSlots ): Promise<L2ItemInstance> {
        let item = this.getPaperdollItemBySlot( slot )
        if ( item && ListenerCache.hasItemTemplateEvent( item.getId(), EventType.PlayerUnEquipsItem ) ) {
            let data = EventPoolCache.getData( EventType.PlayerUnEquipsItem ) as PlayerUnEquipsItemEvent

            data.slot = slot
            data.itemId = item.getId()
            data.playerId = this.ownerId
            data.objectId = item.getObjectId()

            let result = await ListenerCache.getTerminatedItemEventResult( item, EventType.PlayerUnEquipsItem, this.getOwner(), data, slot )
            if ( result && result.terminate ) {
                return
            }
        }

        let removedItem = await super.unEquipItemInBodySlot( slot )
        if ( removedItem ) {
            let player = this.getOwner()

            if ( player ) {
                player.refreshExpertisePenalty()
                player.refreshOverloadPenalty()
            }
        }

        return removedItem
    }

    async equipItem( item: L2ItemInstance ): Promise<void> {
        let player = this.getOwner()

        if ( player && player.getPrivateStoreType() !== PrivateStoreType.None ) {
            return
        }

        if ( !player.hasActionOverride( PlayerActionOverride.ItemAction ) && !player.isHero() && item.isHeroItem() ) {
            return
        }

        return super.equipItem( item )
    }

    getPlayerOwnerId(): number {
        return this.ownerId
    }

    async restore() {
        await super.restore()

        this.markRestored()
        this.inventoryUpdate = new InventoryUpdateBuilder()
    }

    getTalismanWithSkill( skillId:number ) : L2ItemInstance {
        let player = this.getOwner()
        if ( !player ) {
            return null
        }

        let availableSlots = player.getTalismanSlots()
        if ( availableSlots === 0 ) {
            return null
        }

        for ( let slot = InventorySlot.Decoration1; slot < ( InventorySlot.Decoration1 + availableSlots ); slot++ ) {
            let item = this.paperdoll[ slot ]
            if ( item && item.getItem().hasSkillWithId( skillId ) ) {
                return item
            }
        }

        return null
    }

    async equipTalisman( item: L2ItemInstance ) : Promise<L2ItemInstance> {
        let player = this.getOwner()
        if ( !player ) {
            return
        }

        let slots = player.getTalismanSlots()
        if ( slots === 0 ) {
            return
        }

        for ( let index = InventorySlot.Decoration1; index < ( InventorySlot.Decoration1 + slots ); index++ ) {
            if ( this.paperdoll[ index ] && this.getPaperdollItemId( index ) === item.getId() ) {
                return this.setPaperdollItem( index, item )
            }
        }

        for ( let index = InventorySlot.Decoration1; index < ( InventorySlot.Decoration1 + slots ); index++ ) {
            if ( !this.paperdoll[ index ] ) {
                return this.setPaperdollItem( index, item )
            }
        }

        return this.setPaperdollItem( InventorySlot.Decoration1, item )
    }
}