import { L2ItemInstance } from '../items/instance/L2ItemInstance'
import { InventorySlot } from '../../values/InventoryValues'
import { L2ItemSlots } from '../../enums/L2ItemSlots'

export function getSlotFromItem( item: L2ItemInstance ) : L2ItemSlots {
    if ( !item ) {
        return L2ItemSlots.None
    }

    switch ( item.getLocationSlot() ) {
        case InventorySlot.Under:
            return L2ItemSlots.Underware
        case InventorySlot.LeftEar:
            return L2ItemSlots.LeftEar
        case InventorySlot.RightEar:
            return L2ItemSlots.RightEar
        case InventorySlot.Neck:
            return L2ItemSlots.Neck
        case InventorySlot.RightFinger:
            return L2ItemSlots.RightFinger
        case InventorySlot.LeftFinger:
            return L2ItemSlots.LeftFingers
        case InventorySlot.Hair:
            return L2ItemSlots.Hair
        case InventorySlot.HairSecondary:
            return L2ItemSlots.HairSecondary
        case InventorySlot.Head:
            return L2ItemSlots.Head
        case InventorySlot.RightHand:
            return L2ItemSlots.RightHand
        case InventorySlot.LeftHand:
            return L2ItemSlots.LeftHand
        case InventorySlot.Gloves:
            return L2ItemSlots.Gloves
        case InventorySlot.Chest:
            return item.getItem().getBodyPart()
        case InventorySlot.Legs:
            return L2ItemSlots.Legs
        case InventorySlot.Cloak:
            return L2ItemSlots.Back
        case InventorySlot.Feet:
            return L2ItemSlots.Feet
        case InventorySlot.LeftBracelet:
            return L2ItemSlots.LeftBracelet
        case InventorySlot.RightBracelet:
            return L2ItemSlots.RightBracelet
        case InventorySlot.Decoration1:
        case InventorySlot.Decoration2:
        case InventorySlot.Decoration3:
        case InventorySlot.Decoration4:
        case InventorySlot.Decoration5:
        case InventorySlot.Decoration6:
            return L2ItemSlots.Decoration
        case InventorySlot.Belt:
            return L2ItemSlots.Belt
        default:
            return L2ItemSlots.None
    }
}