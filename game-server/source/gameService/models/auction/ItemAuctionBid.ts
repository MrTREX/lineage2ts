export interface ItemAuctionBid {
    playerId: number
    lastBid: number
}