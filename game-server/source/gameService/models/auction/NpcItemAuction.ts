import { ItemAuction } from './ItemAuction'
import { ItemAuctionState } from './ItemAuctionState'
import { ItemAuctionBid } from './ItemAuctionBid'
import { L2ItemAuctionDataItem } from '../../../data/interface/ItemAuctionDataApi'
import { DataManager } from '../../../data/manager'
import { ItemAuctionExtendState } from './ItemAuctionExtendState'
import { SystemMessageBuilder, SystemMessageHelper } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { ItemAuctionHelper } from './ItemAuctionHelper'
import { GeneralHelper } from '../../helpers/GeneralHelper'
import { ItemAuctionManager } from '../../cache/ItemAuctionManager'
import { ConfigManager } from '../../../config/ConfigManager'
import { EventType, ItemAuctionEndEvent, ItemAuctionStartEvent } from '../events/EventType'
import { ListenerCache } from '../../cache/ListenerCache'
import { PacketDispatcher } from '../../PacketDispatcher'
import _ from 'lodash'
import moment from 'moment'
import { L2World } from '../../L2World'
import { SpawnMakerCache } from '../../cache/SpawnMakerCache'
import { ServerLog } from '../../../logger/Logger'
import Timeout = NodeJS.Timeout

export class NpcItemAuction {
    npcId: number
    auctionDataItems: Array<L2ItemAuctionDataItem> = []
    currentAuction: ItemAuction
    nextAuction: ItemAuction
    auctionTask: Timeout
    locationName: string

    constructor( npcId: number ) {
        this.npcId = npcId
        this.locationName = this.getLocationName()
    }

    getCurrentAuction() {
        return this.currentAuction
    }

    getNextAuction() {
        return this.nextAuction
    }

    getActiveAuctionByBidder( objectId: number ): ItemAuction {
        if ( this.currentAuction
                && this.currentAuction.getAuctionState() === ItemAuctionState.STARTED
                && this.currentAuction.getBidForPlayer( objectId ) ) {
            return this.currentAuction
        }

        return null
    }

    async prepareAuction( allAuctions: Array<ItemAuction> ): Promise<void> {

        await this.createAuctions( allAuctions )

        if ( this.currentAuction && this.currentAuction.getAuctionState() !== ItemAuctionState.FINISHED ) {
            if ( this.currentAuction.getAuctionState() === ItemAuctionState.STARTED ) {
                this.logAuction( this.currentAuction.getEndingTime(), this.currentAuction )
                return this.prepareAuctionTask( this.currentAuction.getEndingTime(), this.currentAuction )
            }

            this.logAuction( Date.now(), this.currentAuction )
            return this.prepareAuctionTask( this.currentAuction.getStartingTime(), this.currentAuction )
        }

        this.logAuction( this.nextAuction.getStartingTime(), this.nextAuction )
        return this.prepareAuctionTask( this.nextAuction.getStartingTime(), this.nextAuction )
    }

    private getLocationName() : string {
        let location = SpawnMakerCache.getFirstSpawn( this.npcId )
        if ( !location ) {
            return 'unknown location'
        }

        let townZone = L2World.getNearestTowns( location, 1 )[ 0 ]
        return townZone ? townZone.properties.name : 'unknown location'
    }

    logAuction( time: number, auction: ItemAuction ) {
        ServerLog.info( `Auction Scheduled at '${ this.getNpcName() }' (${this.locationName}), item '${ this.getItemName( auction ) }' starting ${ moment( time ).format( 'dddd, MMMM Do YYYY, h:mm:ss a' ) }` )
    }

    prepareAuctionTask( time: number, auction: ItemAuction ) {
        let task: Timeout = setTimeout( this.runAuctionTask.bind( this ), Math.max( time - Date.now(), 100 ), auction )
        return this.setAuctionTask( task )
    }

    async createAuctions( allAuctions: Array<ItemAuction> ): Promise<void> {
        switch ( _.size( allAuctions ) ) {
            case 0:
                this.currentAuction = null
                this.nextAuction = await this.createAuction( Date.now() + this.getDelayBeforeStart() )
                return

            case 1:
                let existingAuction = allAuctions[ 0 ]
                if ( existingAuction.getAuctionState() === ItemAuctionState.CREATED
                        && existingAuction.getStartingTime() >= ( Date.now() + this.getDelayBeforeStart() ) ) {
                    this.currentAuction = null
                    this.nextAuction = existingAuction
                    return
                }

                this.currentAuction = existingAuction
                this.nextAuction = await this.createAuction( existingAuction.getEndingTime() + this.getDelayBeforeStart() )
                return
        }

        let currentTime = Date.now()
        let currentAuction: ItemAuction = _.find( allAuctions, ( auction: ItemAuction ): boolean => {
            return auction && ( auction.getAuctionState() === ItemAuctionState.STARTED || auction.getStartingTime() < currentTime )
        } )

        let nextAuction: ItemAuction = _.find( allAuctions, ( auction: ItemAuction ) => {
            return auction && auction.getStartingTime() > currentTime && currentAuction !== auction
        } )

        if ( !nextAuction ) {
            nextAuction = await this.createAuction( ( currentAuction ? currentAuction.getEndingTime() : Date.now() ) + this.getDelayBeforeStart() )
        }

        this.currentAuction = currentAuction
        this.nextAuction = nextAuction
    }

    setAuctionTask( task: Timeout ): void {
        if ( this.auctionTask ) {
            clearTimeout( this.auctionTask )
        }

        this.auctionTask = task
    }

    private getNpcName(): string {
        return DataManager.getNpcData().getTemplate( this.npcId ).getName()
    }

    private getItemName( auction: ItemAuction ): string {
        return DataManager.getItems().getTemplate( auction.item.itemId ).getName()
    }

    async runAuctionTask( auction: ItemAuction ): Promise<void> {
        let state: ItemAuctionState = auction.getAuctionState()

        if ( state === ItemAuctionState.CREATED ) {
            auction.setAuctionState( ItemAuctionState.STARTED )

            if ( ListenerCache.hasGeneralListener( EventType.ItemAuctionStart ) ) {
                let parameters: ItemAuctionStartEvent = {
                    auctionId: auction.auctionId,
                    itemAmount: auction.getItemInfo().getCount(),
                    itemId: auction.getItemInfo().getItem().getId(),
                    npcId: this.npcId,
                    startBidAmount: auction.getAuctionInitialBid(),
                    startTime: Date.now(),
                }

                await ListenerCache.sendGeneralEvent( EventType.ItemAuctionStart, parameters )
            }

            return this.prepareAuction( [ this.currentAuction, this.nextAuction ] )
        }

        if ( state !== ItemAuctionState.STARTED ) {
            return
        }

        let nextTime: number = Math.max( auction.getEndingTime() - Date.now(), 100 )
        switch ( auction.auctionEnding ) {
            case ItemAuctionExtendState.EXTEND_BY_5_MIN:
                if ( auction.scheduledAuctionEnding === ItemAuctionExtendState.INITIAL ) {
                    auction.scheduledAuctionEnding = ItemAuctionExtendState.EXTEND_BY_5_MIN
                    return this.setAuctionTask( setTimeout( this.runAuctionTask.bind( this ), nextTime, auction ) )
                }

                break

            case ItemAuctionExtendState.EXTEND_BY_3_MIN:
                if ( auction.scheduledAuctionEnding !== ItemAuctionExtendState.EXTEND_BY_3_MIN ) {
                    auction.scheduledAuctionEnding = ItemAuctionExtendState.EXTEND_BY_3_MIN
                    return this.setAuctionTask( setTimeout( this.runAuctionTask.bind( this ), nextTime, auction ) )
                }

                break

            case ItemAuctionExtendState.EXTEND_BY_CONFIG_PHASE_A:
                if ( auction.scheduledAuctionEnding !== ItemAuctionExtendState.EXTEND_BY_CONFIG_PHASE_B ) {
                    auction.scheduledAuctionEnding = ItemAuctionExtendState.EXTEND_BY_CONFIG_PHASE_B
                    return this.setAuctionTask( setTimeout( this.runAuctionTask.bind( this ), nextTime, auction ) )
                }

                break

            case ItemAuctionExtendState.EXTEND_BY_CONFIG_PHASE_B:
                if ( auction.scheduledAuctionEnding !== ItemAuctionExtendState.EXTEND_BY_CONFIG_PHASE_A ) {
                    auction.scheduledAuctionEnding = ItemAuctionExtendState.EXTEND_BY_CONFIG_PHASE_A
                    return this.setAuctionTask( setTimeout( this.runAuctionTask.bind( this ), nextTime, auction ) )
                }

                break
        }

        auction.setAuctionState( ItemAuctionState.FINISHED )
        await this.onAuctionFinished( auction )

        return this.prepareAuction( [ this.nextAuction ] )
    }

    async onAuctionFinished( auction: ItemAuction ): Promise<void> {
        let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.S1_AUCTION_ENDED )
                .addString( this.getAuctionName( auction ) )
                .getBuffer()
        auction.broadcastPacketToBidders( packet )

        let bid: ItemAuctionBid = auction.getHighestBid()

        if ( ListenerCache.hasGeneralListener( EventType.ItemAuctionEnd ) ) {
            let parameters: ItemAuctionEndEvent = {
                endTime: Date.now(),
                winnerBidAmount: bid ? bid.lastBid : 0,
                winnerPlayerId: bid ? bid.playerId : 0,
                auctionId: auction.auctionId,
                itemAmount: auction.getItemInfo().getCount(),
                itemId: auction.getItemInfo().getItem().getId(),
                npcId: this.npcId,
                totalBidCount: auction.bids.length,
            }

            await ListenerCache.sendGeneralEvent( EventType.ItemAuctionEnd, parameters )
        }

        await auction.remove()

        if ( bid ) {
            let deliveryMethod: string = _.toLower( ConfigManager.general.getItemAuctionDeliveryMethod() )

            ServerLog.info( `Npc Auction Finished: highest bid by playerId '${ bid.playerId }' for amount ${ bid.lastBid }, won item '${ this.getItemName( auction ) }' served by '${ this.getNpcName() }' and delivered via ${ deliveryMethod }` )
            switch ( deliveryMethod ) {
                case 'warehouse':
                    await ItemAuctionHelper.awardUsingWarehouse( auction, bid )
                    break

                case 'mail':
                    await ItemAuctionHelper.awardUsingMail( auction, bid )
                    break

                case 'dimensionalmerchant':
                    await ItemAuctionHelper.awardUsingDimensionalMerchant( auction, bid )
                    break
            }


            PacketDispatcher.sendOwnedData( bid.playerId, SystemMessageHelper.sendString( `You have bid the highest price of ${ bid.lastBid } and have won the ${ this.getAuctionName( auction ) }.` ) )
            return
        }

        ServerLog.info( `Npc Auction Finished: no bids were placed for item '${ this.getItemName( auction ) }', served by '${ this.getNpcName() }' (${ this.locationName })` )
    }

    async createAuction( nextTime: number ): Promise<ItemAuction> {
        let auctionItem: L2ItemAuctionDataItem = _.sample( this.auctionDataItems )
        let startingTime: number = Math.max( nextTime, ItemAuctionHelper.getNextAuctionTime() )
        let endingTime = startingTime + GeneralHelper.minutesToMillis( auctionItem.durationMinutes )
        let auction: ItemAuction = new ItemAuction( ItemAuctionManager.getNextAuctionId(), this.npcId, startingTime, endingTime, auctionItem, [], ItemAuctionState.CREATED )

        await auction.store()

        return auction
    }

    getAuctionName( auction: ItemAuction ): string {
        return `${ auction.getItemInfo().getItem().getName() } x ${ auction.getItemInfo().getCount() } item`
    }

    adjustNextAuctionTime(): void {
        if ( !this.nextAuction ) {
            return
        }

        this.nextAuction.startTime = this.currentAuction.getEndingTime() + this.getDelayBeforeStart()
    }

    getDelayBeforeStart() {
        return ConfigManager.general.getItemAuctionDelaySecondsBeforeStart() * 1000
    }
}