import { L2ItemInstance } from '../items/instance/L2ItemInstance'
import { ItemManagerCache } from '../../cache/ItemManagerCache'
import { L2World } from '../../L2World'
import { ItemLocation } from '../../enums/ItemLocation'
import { ItemAuction } from './ItemAuction'
import { ItemAuctionBid } from './ItemAuctionBid'
import { DimensionalTransferManager } from '../../cache/DimensionalTransferManager'
import { MailManager } from '../../instancemanager/MailManager'
import { MailMessage } from '../mail/MailMessage'
import { SendBySystem } from '../mail/SendBySystem'
import { PacketDispatcher } from '../../PacketDispatcher'
import { ExNotifyDimensionalItem } from '../../packets/send/ExNotifyPremiumItem'
import { ConfigManager } from '../../../config/ConfigManager'
import parser from 'cron-parser'

export const ItemAuctionHelper = {
    getNextAuctionTime(): number {
        let nextAuctionTime = parser.parseExpression( ConfigManager.general.getItemAuctionStartCron() )
        return nextAuctionTime.next().toDate().valueOf()
    },

    async awardUsingWarehouse( auction: ItemAuction, bid: ItemAuctionBid ): Promise<void> {
        let item: L2ItemInstance = ItemManagerCache.createItem( auction.item.itemId, 'ItemAuctionHelper.createNewItemInstance', auction.item.amount, bid.playerId )
        let player = L2World.getPlayer( bid.playerId )
        if ( player ) {
            await player.initializeWarehouse()
            await player.getWarehouse().addExistingItem( item, 'ItemAuctionInstance.onAuctionFinished' )
            return
        }

        await item.setOwnership( bid.playerId, ItemLocation.WAREHOUSE )
        await item.updateDatabase()

        L2World.removeObject( item )
    },

    async awardUsingMail( auction: ItemAuction, bid: ItemAuctionBid ): Promise<void> {
        let message = MailMessage.asSystemMessage( bid.playerId, 'Item Auction', '', SendBySystem.None )

        await message.createAttachments().addItem( auction.item.itemId, auction.item.amount, bid.playerId, 'ItemAuctionHelper.awardUsingMail' )
        return MailManager.sendMessage( message )
    },

    async awardUsingDimensionalMerchant( auction: ItemAuction, bid: ItemAuctionBid ): Promise<void> {
        DimensionalTransferManager.addItem( bid.playerId, auction.item.itemId, auction.item.amount, 'Item Auction' )
        PacketDispatcher.sendOwnedData( bid.playerId, ExNotifyDimensionalItem() )
    },
}