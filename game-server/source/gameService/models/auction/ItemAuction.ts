import { ItemInfo } from '../ItemInfo'
import { ItemAuctionBid } from './ItemAuctionBid'
import { ItemAuctionState } from './ItemAuctionState'
import { ItemAuctionExtendState } from './ItemAuctionExtendState'
import { L2PcInstance } from '../actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { L2World } from '../../L2World'
import { ConfigManager } from '../../../config/ConfigManager'
import { PacketDispatcher } from '../../PacketDispatcher'
import { DatabaseManager } from '../../../database/manager'
import { L2ItemAuctionDataItem } from '../../../data/interface/ItemAuctionDataApi'
import { ItemAuctionHelper } from './ItemAuctionHelper'
import { GeneralHelper } from '../../helpers/GeneralHelper'
import * as Buffer from 'buffer'
import { EventType, ItemAuctionBidEvent } from '../events/EventType'
import { ListenerCache } from '../../cache/ListenerCache'
import { EventPoolCache } from '../../cache/EventPoolCache'
import _ from 'lodash'
import { DataManager } from '../../../data/manager'

const endingTimeLimit = GeneralHelper.minutesToMillis( 10 )

export class ItemAuction {
    auctionId: number
    npcId: number
    startTime: number
    endTime: number
    item: L2ItemAuctionDataItem
    bids: Array<ItemAuctionBid>
    auctionState: ItemAuctionState
    scheduledAuctionEnding: ItemAuctionExtendState
    auctionEnding: ItemAuctionExtendState
    itemInfo: ItemInfo
    itemInfoBuffer: Buffer
    highestBid: ItemAuctionBid
    lastBidByPlayerId: number

    constructor( auctionId: number,
            npcId: number,
            startTime: number,
            endTime: number,
            data: L2ItemAuctionDataItem,
            bids: Array<ItemAuctionBid>,
            state: ItemAuctionState ) {

        this.auctionId = auctionId
        this.npcId = npcId
        this.startTime = startTime
        this.endTime = endTime

        this.item = data
        this.bids = _.orderBy( bids, [ 'lastBid' ], [ 'desc' ] )
        this.auctionState = state
        this.scheduledAuctionEnding = ItemAuctionExtendState.INITIAL

        this.auctionEnding = ItemAuctionExtendState.INITIAL
        this.highestBid = _.head( this.bids )
        this.itemInfo = new ItemInfo().fromTemplate( DataManager.getItems().getTemplate( data.itemId ), data.amount )
    }

    async registerBid( player: L2PcInstance, bidAmount: number ): Promise<boolean> {
        if ( this.getAuctionState() !== ItemAuctionState.STARTED ) {
            return false
        }

        if ( bidAmount < this.getAuctionInitialBid() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.BID_PRICE_MUST_BE_HIGHER ) )
            return false
        }

        if ( bidAmount > 100000000000 ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.BID_CANT_EXCEED_100_BILLION ) )
            return false
        }

        if ( this.highestBid && bidAmount < this.highestBid.lastBid ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.BID_MUST_BE_HIGHER_THAN_CURRENT_BID ) )
            return false
        }

        let playerId = player.getObjectId()
        let bid: ItemAuctionBid = this.getBidForPlayer( playerId )
        if ( !bid ) {
            if ( !this.takeBidAmount( player, bidAmount ) ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ADENA_FOR_THIS_BID ) )
                return false
            }

            bid = {
                playerId,
                lastBid: bidAmount,
            }

            this.bids.push( bid )
        } else {
            if ( !this.takeBidAmount( player, bidAmount ) ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ADENA_FOR_THIS_BID ) )
                return false
            }

            bid.lastBid = bidAmount
        }

        let isEndTimeIncreased: boolean = this.onPlayerBid( player, bid )
        await this.updatePlayerBid( bid, false )

        let packet = new SystemMessageBuilder( SystemMessageIds.SUBMITTED_A_BID_OF_S1 )
                .addNumber( bidAmount )
                .getBuffer()
        player.sendOwnedData( packet )

        if ( ListenerCache.hasGeneralListener( EventType.ItemAuctionBid ) ) {
            let parameters = EventPoolCache.getData( EventType.ItemAuctionBid ) as ItemAuctionBidEvent

            parameters.auctionId = this.auctionId
            parameters.bidAmount = bidAmount
            parameters.playerId = player.getObjectId()

            await ListenerCache.sendGeneralEvent( EventType.ItemAuctionBid, parameters )
        }

        return isEndTimeIncreased
    }

    getAuctionState() {
        return this.auctionState
    }

    getBidForPlayer( playerId: number ): ItemAuctionBid {
        return _.find( this.bids, { playerId } )
    }

    takeBidAmount( player: L2PcInstance, amount: number ): boolean {
        if ( !player.reduceAdena( amount, true, 'ItemAuction.takeBidAmount' ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ADENA_FOR_THIS_BID ) )
            return false
        }

        return true
    }

    getAuctionInitialBid(): number {
        return this.item.initialBid
    }

    onPlayerBid( player: L2PcInstance, bid: ItemAuctionBid ): boolean {
        if ( !this.highestBid ) {
            this.highestBid = bid
        }

        if ( this.highestBid.lastBid < bid.lastBid ) {
            let previousPlayer: L2PcInstance = L2World.getPlayer( this.highestBid.playerId )
            if ( previousPlayer ) {
                previousPlayer.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_HAVE_BEEN_OUTBID ) )
            }

            this.highestBid = bid
        }

        if ( ( this.getEndingTime() - Date.now() ) > endingTimeLimit ) {
            return false
        }

        switch ( this.auctionEnding ) {
            case ItemAuctionExtendState.INITIAL:
                this.auctionEnding = ItemAuctionExtendState.EXTEND_BY_5_MIN
                this.endTime += GeneralHelper.minutesToMillis( 5 )
                this.broadcastToAllBidders( SystemMessageBuilder.fromMessageId( SystemMessageIds.BIDDER_EXISTS_AUCTION_TIME_EXTENDED_BY_5_MINUTES ) )

                return true

            case ItemAuctionExtendState.EXTEND_BY_5_MIN:
                if ( this.getAndSetLastBidPlayerObjectId( player.getObjectId() ) !== player.getObjectId() ) {
                    this.auctionEnding = ItemAuctionExtendState.EXTEND_BY_3_MIN
                    this.endTime += GeneralHelper.minutesToMillis( 3 )
                    this.broadcastToAllBidders( SystemMessageBuilder.fromMessageId( SystemMessageIds.BIDDER_EXISTS_AUCTION_TIME_EXTENDED_BY_3_MINUTES ) )

                    return true
                }

                return false

            case ItemAuctionExtendState.EXTEND_BY_3_MIN:
                if ( ConfigManager.general.getItemAuctionTimeExtendsOnBid() > 0 ) {
                    if ( this.getAndSetLastBidPlayerObjectId( player.getObjectId() ) !== player.getObjectId() ) {
                        this.auctionEnding = ItemAuctionExtendState.EXTEND_BY_CONFIG_PHASE_A
                        this.endTime += ConfigManager.general.getItemAuctionTimeExtendsOnBid()

                        return true
                    }
                }

                return false

            case ItemAuctionExtendState.EXTEND_BY_CONFIG_PHASE_A:
                if ( this.getAndSetLastBidPlayerObjectId( player.getObjectId() ) !== player.getObjectId() ) {
                    if ( this.scheduledAuctionEnding === ItemAuctionExtendState.EXTEND_BY_CONFIG_PHASE_B ) {
                        this.auctionEnding = ItemAuctionExtendState.EXTEND_BY_CONFIG_PHASE_B
                        this.endTime += ConfigManager.general.getItemAuctionTimeExtendsOnBid()

                        return true
                    }
                }

                return false

            case ItemAuctionExtendState.EXTEND_BY_CONFIG_PHASE_B:
                if ( this.getAndSetLastBidPlayerObjectId( player.getObjectId() ) !== player.getObjectId() ) {
                    if ( this.scheduledAuctionEnding === ItemAuctionExtendState.EXTEND_BY_CONFIG_PHASE_A ) {
                        this.endTime += ConfigManager.general.getItemAuctionTimeExtendsOnBid()
                        this.auctionEnding = ItemAuctionExtendState.EXTEND_BY_CONFIG_PHASE_A

                        return true
                    }
                }

                return false
        }
    }

    getEndingTime() {
        return this.endTime
    }

    broadcastToAllBidders( data: Buffer ) {
        _.each( this.bids, ( bid: ItemAuctionBid ) => {
            PacketDispatcher.sendCopyData( bid.playerId, data )
        } )
    }

    getAndSetLastBidPlayerObjectId( objectId: number ): number {
        let lastPlayerId = this.lastBidByPlayerId
        this.lastBidByPlayerId = objectId
        return lastPlayerId
    }

    updatePlayerBid( bid: ItemAuctionBid, shouldDelete: boolean ): Promise<void> {
        if ( shouldDelete ) {
            return DatabaseManager.getItemAuctionBids().deleteBid( this.auctionId, bid.playerId )
        }

        return DatabaseManager.getItemAuctionBids().addBid( this.auctionId, bid.playerId, bid.lastBid )
    }

    getFinishTimeRemaining() {
        return Math.max( this.getEndingTime() - Date.now(), 0 )
    }

    getHighestBid() {
        return this.highestBid
    }

    getNpcId() {
        return this.npcId
    }

    getItemInfo() {
        return this.itemInfo
    }

    getStartingTime() {
        return this.startTime
    }

    async cancelBid( player: L2PcInstance ): Promise<boolean> {
        if ( !player || this.getAuctionState() !== ItemAuctionState.STARTED ) {
            return false
        }

        if ( !this.highestBid ) {
            return false
        }

        let bid: ItemAuctionBid = this.getBidForPlayer( player.getObjectId() )
        if ( !bid ) {
            return false
        }

        if ( bid.playerId === this.highestBid.playerId ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.HIGHEST_BID_BUT_RESERVE_NOT_MET ) )
            return true
        }

        _.pull( this.bids, bid )
        await this.refundBidAmount( player, bid.lastBid )
        await this.updatePlayerBid( bid, true )

        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANCELED_BID ) )
        return true
    }

    async refundBidAmount( player: L2PcInstance, lastBid: number ): Promise<void> {
        await player.addAdena( lastBid, 'ItemAuction.refundBidAmount', player.getObjectId(), true )
    }

    setAuctionState( state: ItemAuctionState ): void {
        this.auctionState = state
        this.store()
    }

    store(): Promise<void> {
        return DatabaseManager.getItemAuctions().addAuction( this.auctionId,
                this.npcId,
                this.item.auctionId,
                this.startTime,
                this.endTime,
                this.auctionState )
    }

    async remove(): Promise<void> {
        if ( this.auctionState !== ItemAuctionState.FINISHED ) {
            return
        }

        await DatabaseManager.getItemAuctions().deleteAuctions( [ this.auctionId ] )
        return DatabaseManager.getItemAuctionBids().deleteAuctions( [ this.auctionId ] )
    }

    broadcastPacketToBidders( packet: Buffer ) {
        _.each( this.bids, ( bid: ItemAuctionBid ) => {
            PacketDispatcher.sendCopyData( bid.playerId, packet )
        } )
    }
}