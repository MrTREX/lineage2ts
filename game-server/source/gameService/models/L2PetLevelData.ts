import { MoveType } from './stats/MoveType'

export class L2PetLevelData {
    ownerExpRatio: number
    feedBattle: number
    feedNormal: number
    magicAttack: number
    petMaxExp: number
    maxFeed: number
    maxHP: number
    maxMP: number
    magicDefence: number
    powerAttack: number
    powerDefence: number
    regenerateHP: number
    regenerateMP: number
    useSoulshot: number
    useSpiritshot: number
    walkSpeedOnRide: number
    runSpeedOnRide: number
    slowSwimSpeedOnRide: number
    fastSwimSpeedOnRide: number
    slowFlySpeedOnRide: number
    fastFlySpeedOnRide: number

    getPetSoulShot() {
        return this.useSoulshot
    }

    getPetSpiritShot() {
        return this.useSpiritshot
    }

    getSpeedOnRide( type: MoveType ) {
        switch ( type ) {
            case MoveType.WALK:
                return this.walkSpeedOnRide
            case MoveType.RUN:
                return this.runSpeedOnRide
            case MoveType.SLOW_SWIM:
                return this.slowSwimSpeedOnRide
            case MoveType.FAST_SWIM:
                return this.fastSwimSpeedOnRide
            case MoveType.SLOW_FLY:
                return this.slowFlySpeedOnRide
            case MoveType.FAST_FLY:
                return this.fastFlySpeedOnRide
        }
    }

    getPetMaxFeed() {
        return this.maxFeed
    }

    getOwnerXpRatio() {
        return this.ownerExpRatio
    }

    getPetFeedNormal() {
        return this.feedNormal
    }

    getPetFeedBattle() {
        return this.feedBattle
    }

    getPetMaxExp() {
        return this.petMaxExp
    }

    getPetHPRegeneration() {
        return this.regenerateHP
    }

    getPetMPRegeneration() {
        return this.regenerateMP
    }

    getPowerDefence() {
        return this.powerDefence
    }

    getPowerAttack() {
        return this.powerAttack
    }

    getMagicDefence() {
        return this.magicDefence
    }

    getMagicAttack() {
        return this.magicAttack
    }

    getMaxMP() {
        return this.maxMP
    }

    getMaxHP() {
        return this.maxHP
    }
}