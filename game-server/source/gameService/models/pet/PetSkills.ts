import { PetSkillItem } from '../skills/SkillItem'
import { SkillCache } from '../../cache/SkillCache'

type PetToSkillLevel = Record<number, number>

export class PetSkills {
    skills: Array<PetSkillItem> = []
    private availableLevels: Record<number, PetToSkillLevel> = {} // skillId, petLevel, availableLevel

    private getAvailableSkillLevel( skillId: number, petLevel: number ) : number {
        let level = 0
        let maxLvl = SkillCache.getMaxLevel( skillId )

        this.skills.forEach( ( skill: PetSkillItem ) => {
            if ( skill.id !== skillId ) {
                return
            }

            if ( skill.minLevel <= petLevel && skill.level > level ) {
                level = skill.level
                return
            }

            if ( skill.level === 0 ) {
                if ( petLevel < 70 ) {
                    level = Math.min( 1, Math.floor( petLevel / 10 ) )
                } else {
                    level = Math.floor( 7 + ( ( petLevel - 70 ) / 5 ) )
                }


                if ( level > maxLvl ) {
                    level = maxLvl
                }
            }
        } )

        return level
    }

    getAvailableLevel( skillId: number, petLevel: number ) : number {
        let skill = this.skills[ skillId ]
        if ( !skill ) {
            return 0
        }

        let savedLevel = this.getSavedAvailableLevel( skillId, petLevel )
        if ( savedLevel > 0 ) {
            return savedLevel
        }

        let level = this.getAvailableSkillLevel( skillId, petLevel )

        this.saveAvailableLevel( skillId, petLevel, level )
        return level
    }

    private saveAvailableLevel( skillId: number, petLevel: number, skillLevel: number ) : void {
        let data : PetToSkillLevel = this.availableLevels[ skillId ]
        if ( !data ) {
            this.availableLevels[ skillId ] = {
                [ petLevel ]: skillLevel
            }

            return
        }

        data[ petLevel ] = skillLevel
    }

    private getSavedAvailableLevel( skillId: number, petLevel: number ) : number {
        let data : PetToSkillLevel = this.availableLevels[ skillId ]
        if ( !data ) {
            return 0
        }

        return data[ petLevel ] ?? 0
    }
}