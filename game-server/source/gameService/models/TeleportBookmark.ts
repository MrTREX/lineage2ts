import { Location } from './Location'

export class TeleportBookmark extends Location {
    id: number
    icon: number
    name: string
    tag: string

    constructor( id: number, x: number, y: number, z: number, icon: number, tag: string, name: string ) {
        super( x, y, z )

        this.id = id
        this.icon = icon
        this.name = name
        this.tag = tag
    }

    getId() {
        return this.id
    }

    getName() {
        return this.name
    }

    getIcon() {
        return this.icon
    }

    getTag() {
        return this.tag
    }

    setIcon( value: number ) {
        this.icon = value
    }

    setTag( value: string ) {
        this.tag = value
    }

    setName( value: string ) {
        this.name = value
    }
}