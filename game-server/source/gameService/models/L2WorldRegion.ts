import { L2Object } from './L2Object'
import { L2World } from '../L2World'
import { L2Npc } from './actor/L2Npc'
import { GeoPolygonCache } from '../cache/GeoPolygonCache'
import { ISpatialIndexRegion } from './SpatialIndexData'
import { getRegionCode } from '../enums/L2MapTile'
import { L2MoveManager } from '../L2MoveManager'

function deactivateAI( object: L2Object ) : void {
    if ( !object || !object.isNpc() || ( object as L2Npc ).hasWalkingRoute() ) {
        return
    }

    return ( object as L2Npc ).deactivateAI()
}

export class L2WorldRegion {
    /*
        Players in region that require scan to discover visible objects
     */
    playerCounter: number = 0
    surroundingRegions: Array<L2WorldRegion> = []
    tileX: number
    tileY: number
    code: number
    active: boolean = false
    geometry: ISpatialIndexRegion

    constructor( x: number, y: number, minX: number, maxX: number, minY: number, maxY: number ) {
        this.tileX = x
        this.tileY = y
        this.code = getRegionCode( x, y )
        this.setGeometry( minX, maxX, minY, maxY )
    }

    addVisibleObject( object: L2Object ) {
        if ( object.isPlayer() ) {
            this.playerCounter++

            if ( !this.isActive() ) {
                this.startActivation()
            }
        }
    }

    areNeighborsEmpty(): boolean {
        if ( this.isActive() && this.playerCounter > 0 ) {
            return false
        }

        let active: boolean = this.getSurroundingRegions().some( ( region: L2WorldRegion ) => {
            return region.isActive() && region.playerCounter > 0
        } )

        return !active
    }

    getSurroundingRegions(): Array<L2WorldRegion> {
        return this.surroundingRegions
    }

    isActive() {
        return this.active
    }

    removeVisibleObject( object: L2Object ) : void {
        if ( object.isPlayer() && this.playerCounter > 0 ) {
            this.playerCounter--

            if ( this.playerCounter === 0 ) {
                this.startDeactivation()
            }
        }
    }

    setActive( value: boolean ): void {
        if ( this.active === value ) {
            return
        }

        this.active = value

        if ( value ) {
            if ( !GeoPolygonCache.hasRegion( this.code ) ) {
                GeoPolygonCache.startRegionLoad( this ).finally( () => this.onActivation() )
                return
            }

            return this.onActivation()
        }

        GeoPolygonCache.unLoadRegion( this )
        return this.onDeactivation()
    }

    startActivation(): void {
        this.setActive( true )

        this.getSurroundingRegions().forEach( ( region: L2WorldRegion ) => {
            region.setActive( true )
        } )
    }

    onActivation() : void {
        /*
            Defer zone validation when region is activated, which saves resources and makes server initial spawn time faster.
         */
        let movementIds : Array<number> = []
        for ( const objectId of L2World.getObjectIdsByRegion( this, 0 ) ) {
            let object: L2Object = L2World.getObjectById( objectId )
            if ( object && object.isCharacter() ) {
                movementIds.push( object.sharedData.getMovementId() )
            }
        }

        if ( movementIds.length > 0 ) {
            L2MoveManager.revalidateZValues( movementIds )
        }
    }

    startDeactivation(): void {
        if ( !this.areNeighborsEmpty() ) {
            return
        }

        this.setActive( false )
    }

    onDeactivation() : void {
        this.getSurroundingRegions().forEach( ( region: L2WorldRegion ) => {
            if ( region.areNeighborsEmpty() ) {
                region.setActive( false )
            }
        } )

        L2World.forObjectsInRegionWithPredicate( this, 0, deactivateAI )
    }

    private setGeometry( minX: number, maxX: number, minY: number, maxY: number ) {
        if ( this.geometry ) {
            return
        }

        this.geometry = {
            maxX: Math.max( minX, maxX ),
            maxY: Math.max( minY, maxY ),
            minX: Math.min( minX, maxX ),
            minY: Math.min( minY, maxY ),
            region: this
        }
    }

    isInRegion( x: number, y: number ): boolean {
        return x >= this.geometry.minX
                && y >= this.geometry.minY
                && x < this.geometry.maxX
                && y < this.geometry.maxY
    }
}