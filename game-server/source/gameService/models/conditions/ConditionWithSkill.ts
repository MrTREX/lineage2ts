import { Condition } from './Condition'
import { Skill } from '../Skill'
import { L2Character } from '../actor/L2Character'

export class ConditionWithSkill extends Condition {
    value: boolean

    constructor( value: boolean ) {
        super()
        this.value = value
    }

    runTest( caster: L2Character, target: L2Character, skill: Skill = null ): boolean {
        return !!skill && this.value
    }
}