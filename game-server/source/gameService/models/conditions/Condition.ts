import { L2Character } from '../actor/L2Character'
import { Skill } from '../Skill'
import { L2Item } from '../items/L2Item'

export class Condition {
    message: string
    messageId: number
    addMessageName: boolean

    getMessage(): string {
        return this.message
    }

    getMessageId(): number {
        return this.messageId
    }

    isAddName(): boolean {
        return this.addMessageName
    }

    runTest( caster: L2Character, target: L2Character, skill: Skill = undefined, item: L2Item = undefined ): boolean {
        return false
    }

    test( caster: L2Character, target: L2Character, skill: Skill = undefined, item: L2Item = undefined ) : boolean {
        return this.runTest( caster, target, skill, item )
    }
}