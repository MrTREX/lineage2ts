import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'
import { L2PcInstance } from '../../actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../packets/SystemMessageIdValues'
import { AreaType } from '../../areas/AreaType'

export class ConditionPlayerCanUntransform extends Condition {
    value: boolean

    constructor( value: boolean ) {
        super()
        this.value = value
    }

    runTest( caster: L2Character ): boolean {
        let player = caster.getActingPlayer()
        return this.checkPlayer( player ) === this.value
    }

    checkPlayer( player: L2PcInstance ) {
        if ( !player || player.isAlikeDead() || player.isCursedWeaponEquipped() ) {
            return false
        }

        if ( !!player.getTransformation()
                && player.isFlyingMounted()
                && !player.isInArea( AreaType.AirLanding ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TOO_HIGH_TO_PERFORM_THIS_ACTION ) )
            return false
        }

        return true
    }
}