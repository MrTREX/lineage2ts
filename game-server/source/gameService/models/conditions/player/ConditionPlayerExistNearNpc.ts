import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'
import { L2World } from '../../../L2World'
import { L2Npc } from '../../actor/L2Npc'

export class ConditionPlayerExistNearNpc extends Condition {
    npcIds: Set<number>
    radius: number
    shouldEnforce: boolean
    checkNpc : Function

    constructor( npcIds : Array<number>, radiusValue: number, shouldEnforceValue : boolean ) {
        super()
        this.npcIds = new Set<number>( npcIds )
        this.radius = radiusValue
        this.shouldEnforce = shouldEnforceValue

        this.checkNpc = this.checkNpcObject.bind( this )
    }

    runTest( caster: L2Character, target: L2Character ): boolean {
        return this.shouldEnforce === L2World.getVisibleNpcs( caster, this.radius, false ).some( npc => this.checkNpc( npc ) )
    }

    checkNpcObject( npc : L2Npc ) : boolean {
        return this.npcIds.has( npc.getId() )
    }
}