import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'
import { L2WorldArea } from '../../areas/WorldArea'
import { AreaType } from '../../areas/AreaType'
import { SkillReferenceArea } from '../../areas/type/SkillReference'
import { AreaDiscoveryManager } from '../../../cache/AreaDiscoveryManager'

export class ConditionPlayerInsideZoneId extends Condition {
    zones: Set<number>

    constructor( ids: Array<number> ) {
        super()
        this.zones = new Set<number>( ids )
    }

    runTest( caster: L2Character ): boolean {
        if ( !caster || !caster.isPlayer() ) {
            return false
        }

        return AreaDiscoveryManager.forSomeDiscoveredArea( caster.getObjectId(), ( area: L2WorldArea ) : boolean => area.type === AreaType.SkillReference && this.zones.has( ( area as SkillReferenceArea ).getSkillId() ) )
    }
}