import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'
import { Skill } from '../../Skill'
import { L2Item } from '../../items/L2Item'
import { L2PcInstance } from '../../actor/instance/L2PcInstance'

export class ConditionPlayerNoble extends Condition {
    isNoble: boolean

    constructor( isNoble: boolean ) {
        super()

        this.isNoble = isNoble
    }

    runTest( caster: L2Character, target: L2Character, skill: Skill = undefined, item: L2Item = undefined ): boolean {
        return caster.isPlayer() && ( caster as L2PcInstance ).isNoble() === this.isNoble
    }
}