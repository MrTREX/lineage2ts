import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'
import { Skill } from '../../Skill'

export class ConditionPlayerActiveSkillId extends Condition {
    id: number
    level: number

    constructor( id: number, level: number = -1 ) {
        super()
        this.id = id
        this.level = level
    }

    runTest( caster: L2Character ): boolean {
        let condition = this
        let isAnyLevel = this.level === -1

        return caster.getAllSkills().some( ( skill: Skill ) => {
            return skill.getId() === condition.id && ( isAnyLevel || condition.level <= skill.getLevel() )
        } )
    }
}