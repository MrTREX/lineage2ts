import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'

export class ConditionPlayerSiegeSide extends Condition {
    side: number

    constructor( value: number ) {
        super()
        this.side = value
    }

    runTest( caster: L2Character ): boolean {
        let player = caster.getActingPlayer()
        return player && player.getSiegeSide() === this.side
    }
}