import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'
import { Skill } from '../../Skill'
import { L2Object } from '../../L2Object'
import { L2Attackable } from '../../actor/L2Attackable'
import { SystemMessageBuilder } from '../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../packets/SystemMessageIdValues'

export class ConditionPlayerCanSweep extends Condition {
    value: boolean

    constructor( value: boolean ) {
        super()
        this.value = value
    }

    runTest( caster: L2Character, target: L2Character, skill: Skill ): boolean {
        let player = caster.getActingPlayer()
        if ( !player || !skill ) {
            return !this.value
        }

        let targets : Array<L2Object> = skill.getTargetsOf( player )
        if ( targets.length === 0 ) {
            return !this.value
        }


        let haveMessage = false
        let outcome = targets.some( ( currentTarget: L2Object ) : boolean => {
            if ( !currentTarget.isAttackable() ) {
                return false
            }

            let attackable = currentTarget as L2Attackable
            if ( !attackable.isDead() ) {
                return false
            }

            if ( !attackable.isSpoiled() ) {
                haveMessage = true
                return false
            }

            return attackable.checkSpoilOwner( player, true )
                && !attackable.isOldCorpse( player )
                && player.getInventory().checkInventorySlotsAndWeight( attackable.getSpoilLootItems(), true, true )
        } )

        if ( !outcome && haveMessage ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SWEEPER_FAILED_TARGET_NOT_SPOILED ) )
        }

        return outcome === this.value
    }
}