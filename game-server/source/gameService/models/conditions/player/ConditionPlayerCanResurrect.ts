import { Condition } from '../Condition'
import { Skill } from '../../Skill'
import { L2Character } from '../../actor/L2Character'
import { SystemMessageBuilder } from '../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../packets/SystemMessageIdValues'
import { L2Summon } from '../../actor/L2Summon'

export class ConditionPlayerCanResurrect extends Condition {
    value: boolean

    constructor( value: boolean ) {
        super()
        this.value = value
    }

    runTest( caster: L2Character, target: L2Character, skill: Skill = null ): boolean {
        if ( skill.getAffectRange() > 0 ) {
            return true
        }

        if ( !target ) {
            return false
        }

        let outcome = true
        if ( target.isPlayer() ) {
            outcome = this.checkPlayer( caster, target, skill )
        }

        if ( target.isSummon() ) {
            outcome = this.checkSummon( caster, target, skill )
        }

        return outcome === this.value
    }

    checkPlayer( caster: L2Character, target: L2Character, skill: Skill ) {
        let player = target.getActingPlayer()

        if ( !player.isDead() ) {
            if ( caster.isPlayer() ) {
                let packet = new SystemMessageBuilder( SystemMessageIds.S1_CANNOT_BE_USED )
                    .addSkillName( skill )
                    .getBuffer()
                caster.sendOwnedData( packet )
            }

            return false
        }

        if ( player.isResurrectionBlocked() ) {
            if ( caster.isPlayer() ) {
                caster.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.REJECT_RESURRECTION ) )
            }

            return false
        }

        if ( player.isReviveRequested() ) {
            if ( caster.isPlayer() ) {
                caster.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.RES_HAS_ALREADY_BEEN_PROPOSED ) )
            }

            return false
        }

        return true
    }

    checkSummon( caster: L2Character, target: L2Character, skill: Skill ) {
        let summon = target as L2Summon

        if ( !summon.isDead() ) {
            if ( caster.isPlayer() ) {
                let packet = new SystemMessageBuilder( SystemMessageIds.S1_CANNOT_BE_USED )
                    .addSkillName( skill )
                    .getBuffer()
                caster.sendOwnedData( packet )
            }

            return false
        }

        if ( summon.isResurrectionBlocked() ) {
            if ( caster.isPlayer() ) {
                caster.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.REJECT_RESURRECTION ) )
            }

            return false
        }

        let player = summon.getOwner()
        if ( player && player.isRevivingPet() ) {
            if ( caster.isPlayer() ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.RES_HAS_ALREADY_BEEN_PROPOSED ) )
            }

            return false
        }

        return true
    }
}