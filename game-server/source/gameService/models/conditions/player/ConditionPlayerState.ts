import { Condition } from '../Condition'
import { PlayerState } from '../../../enums/PlayerState'
import { L2Character } from '../../actor/L2Character'
import { L2PcInstance } from '../../actor/instance/L2PcInstance'

export class ConditionPlayerState extends Condition {
    checkState: PlayerState
    value: boolean

    constructor( state: PlayerState, value: boolean ) {
        super()
        this.checkState = state
        this.value = value
    }

    runTest( caster: L2Character ): boolean {
        let player : L2PcInstance = caster.getActingPlayer()

        switch ( this.checkState ) {
            case PlayerState.RESTING:
                if ( player ) {
                    return ( player.isSitting() === this.value )
                }
                return !this.value

            case PlayerState.MOVING:
                return caster.isMoving() === this.value

            case PlayerState.RUNNING:
                return caster.isRunningValue === this.value

            case PlayerState.STANDING:
                if ( player ) {
                    return ( this.value != ( player.isSitting() || player.isMoving() ) )
                }
                return this.value !== caster.isMoving()

            case PlayerState.FLYING:
                return caster.isFlying() === this.value

            case PlayerState.BEHIND:
                return caster.isBehindTarget() === this.value

            case PlayerState.FRONT:
                return caster.isInFrontOfTarget() === this.value

            case PlayerState.CHAOTIC:
                if ( player ) {
                    return ( player.getKarma() > 0 ) === this.value
                }
                return !this.value

            case PlayerState.OLYMPIAD:
                if ( player ) {
                    return player.isInOlympiadMode() === this.value
                }

                return !this.value

            case PlayerState.TRANSFORMED:
                if ( player ) {
                    return ( !!player.getTransformation() ) === this.value
                }

                return !this.value
        }

        return !this.value
    }
}