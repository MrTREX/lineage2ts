import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'

export class ConditionPlayerInventorySize extends Condition {
    size: number

    constructor( value: number ) {
        super()
        this.size = value
    }

    runTest( caster: L2Character ): boolean {
        let player = caster.getActingPlayer()
        return player && ( player.getInventory().getFullSize( false ) <= player.getInventoryLimit() - this.size )
    }
}