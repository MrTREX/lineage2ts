import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'

export class ConditionPlayerClassIdRestriction extends Condition {
    classIds: Array<number>

    constructor( ids: Array<number> ) {
        super()
        this.classIds = ids
    }

    runTest( caster: L2Character ): boolean {
        let player = caster.getActingPlayer()
        return player && this.classIds.includes( player.getClassId() )
    }
}