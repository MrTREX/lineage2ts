import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'
import { L2PcInstance } from '../../actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../packets/SystemMessageIdValues'

export class ConditionPlayerCanTransform extends Condition {
    value: boolean

    constructor( value: boolean ) {
        super()
        this.value = value
    }

    runTest( caster: L2Character ): boolean {
        let player = caster.getActingPlayer()
        return this.checkPlayer( player ) === this.value
    }

    checkPlayer( player: L2PcInstance ) {
        if ( !player || player.isAlikeDead() || player.isCursedWeaponEquipped() ) {
            return false
        }

        if ( player.isSitting() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_TRANSFORM_WHILE_SITTING ) )
            return false
        }

        if ( player.getTransformation() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ALREADY_POLYMORPHED_AND_CANNOT_POLYMORPH_AGAIN ) )
            return false
        }

        if ( player.isInWater() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_POLYMORPH_INTO_THE_DESIRED_FORM_IN_WATER ) )
            return false
        }

        if ( player.isFlyingMounted() || player.isMounted() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_POLYMORPH_WHILE_RIDING_A_PET ) )
            return false
        }

        return true
    }
}