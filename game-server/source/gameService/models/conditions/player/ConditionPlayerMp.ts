import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'

export class ConditionPlayerMp extends Condition {
    value: number

    constructor( value: number ) {
        super()
        this.value = value
    }

    runTest( caster: L2Character ): boolean {
        return caster && ( ( ( caster.getCurrentMp() * 100 ) / caster.getMaxMp() ) <= this.value )
    }
}