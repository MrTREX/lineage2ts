import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'

export class ConditionPlayerIsHero extends Condition {
    isHero: boolean

    constructor( value: boolean, messageId? : number ) {
        super()
        this.isHero = value
        this.messageId = messageId
    }

    runTest( caster: L2Character ): boolean {
        let player = caster.getActingPlayer()
        if ( !player ) {
            return false
        }

        return player.isHero() === this.isHero
    }
}