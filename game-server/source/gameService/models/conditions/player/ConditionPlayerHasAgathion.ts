import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'

export class ConditionPlayerHasAgathion extends Condition {
    value: boolean

    constructor( value: boolean ) {
        super()
        this.value = value
    }

    runTest( caster: L2Character ): boolean {
        return this.value === ( caster.isPlayer() && ( caster.getActingPlayer().getAgathionId() > 0 ) )
    }
}