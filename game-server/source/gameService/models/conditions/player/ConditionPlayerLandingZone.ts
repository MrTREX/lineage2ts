import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'
import { AreaType } from '../../areas/AreaType'

export class ConditionPlayerLandingZone extends Condition {
    value: boolean

    constructor( value: boolean ) {
        super()
        this.value = value
    }

    runTest( caster: L2Character ): boolean {
        return caster.isInArea( AreaType.AirLanding ) === this.value
    }
}