import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'
import { L2PcInstance } from '../../actor/instance/L2PcInstance'
import { TvTEvent } from '../../entity/TvTEvent'
import { PlayerActionOverride } from '../../../values/PlayerConditions'
import { AreaCache } from '../../../cache/AreaCache'
import { AreaType } from '../../areas/AreaType'
import { AreaDiscoveryManager } from '../../../cache/AreaDiscoveryManager'

export class ConditionPlayerCanEscape extends Condition {
    value: boolean

    constructor( value: boolean ) {
        super()
        this.value = value
    }

    runTest( caster: L2Character ): boolean {
        let player = caster.getActingPlayer()

        return this.checkPlayer( player ) === this.value
    }

    checkPlayer( player: L2PcInstance ) : boolean {
        if ( !player ) {
            return false
        }

        if ( !TvTEvent.onEscapeUse( player.getObjectId() ) ) {
            return false
        }

        if ( player.isInDuel()
            || player.isAfraid()
            || player.isCombatFlagEquipped()
            || player.isFlying()
            || player.isFlyingMounted() ) {
            return false
        }

        return !( AreaDiscoveryManager.getEnteredAreaByType( player.getObjectId(), AreaType.Boss ) && !player.hasActionOverride( PlayerActionOverride.SkillAction ) )
    }
}