import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'
import { InstanceWorld } from '../../instancezone/InstanceWorld'
import { InstanceManager } from '../../../instancemanager/InstanceManager'

export class ConditionPlayerInstanceId extends Condition {
    ids: Array<number>

    constructor( ids: Array<number> ) {
        super()
        this.ids = ids
    }

    runTest( caster: L2Character ): boolean {
        let player = caster.getActingPlayer()
        if ( !player ) {
            return false
        }

        let instanceId = player.getInstanceId()
        if ( instanceId <= 0 ) {
            return false
        }

        let world: InstanceWorld = InstanceManager.getPlayerWorld( player.getObjectId() )
        if ( !world || world.getInstanceId() !== instanceId ) {
            return false
        }

        return this.ids.includes( world.getTemplateId() )
    }
}