import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'
import { L2PcInstance } from '../../actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../packets/SystemMessageIdValues'
import { L2World } from '../../../L2World'

export class ConditionPlayerCanSummon extends Condition {
    value: boolean

    constructor( value: boolean ) {
        super()
        this.value = value
    }

    runTest( caster: L2Character ): boolean {
        let player = caster.getActingPlayer()
        if ( !player ) {
            return false
        }

        return this.checkPlayer( player ) === this.value
    }

    checkPlayer( player: L2PcInstance ): boolean {
        if ( L2World.getPet( player.getObjectId() ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SUMMON_ONLY_ONE ) )
            return false
        }

        return !( player.isFlyingMounted() || player.isMounted() )
    }
}