import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'
import { L2ItemInstance } from '../../items/instance/L2ItemInstance'
import { L2PetInstance } from '../../actor/instance/L2PetInstance'

export class ConditionPlayerHasPet extends Condition {
    ids: Array<number>

    constructor( ids: Array<number> ) {
        super()
        this.ids = ( ids.length === 1 && ids[ 0 ] === 0 ) ? [] : ids
    }

    runTest( caster: L2Character ): boolean {
        let player = caster.getActingPlayer()
        if ( !player ) {
            return false
        }

        let summon = player.getSummon()
        if ( !summon || !summon.isPet() ) {
            return false
        }

        if ( this.ids.length === 0 ) {
            return true
        }

        let item: L2ItemInstance = ( summon as L2PetInstance ).getControlItem()
        return item && this.ids.includes( item.getId() )
    }
}