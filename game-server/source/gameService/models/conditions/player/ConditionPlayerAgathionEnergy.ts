import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'
import { DataManager } from '../../../../data/manager'
import { L2ItemInstance } from '../../items/instance/L2ItemInstance'
import { InventorySlot } from '../../../values/InventoryValues'

export class ConditionPlayerAgathionEnergy extends Condition {
    value: number

    constructor( value: number ) {
        super()
        this.value = value
    }

    runTest( caster: L2Character ): boolean {
        if ( !caster || !caster.isPlayer() ) {
            return false
        }

        let player = caster.getActingPlayer()
        let agathion = DataManager.getAgathionData().getByNpcId( player.getAgathionId() )
        if ( !agathion || !agathion.energy ) {
            return false
        }

        let item : L2ItemInstance = player.getInventory().getPaperdollItem( InventorySlot.LeftBracelet )
        if ( !item || ( item.getId() !== agathion.itemId ) ) {
            return false
        }

        return item.getAgathionRemainingEnergy() >= this.value
    }
}