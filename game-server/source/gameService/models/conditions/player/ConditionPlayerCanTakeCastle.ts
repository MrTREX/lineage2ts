import { Condition } from '../Condition'
import { Skill } from '../../Skill'
import { L2Character } from '../../actor/L2Character'
import { CastleManager } from '../../../instancemanager/CastleManager'
import { Castle } from '../../entity/Castle'
import { SystemMessageBuilder } from '../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../packets/SystemMessageIdValues'
import { GeneralHelper } from '../../../helpers/GeneralHelper'

export class ConditionPlayerCanTakeCastle extends Condition {
    runTest( caster: L2Character, target: L2Character, skill: Skill = null ): boolean {
        if ( !caster || !caster.isPlayer() ) {
            return false
        }

        let player = caster.getActingPlayer()
        if ( player.isAlikeDead() || player.isCursedWeaponEquipped() || !player.isClanLeader() ) {
            return false
        }

        let castle : Castle = CastleManager.getCastle( player )
        if ( !castle || ( castle.getResidenceId() <= 0 ) || !castle.getSiege().isInProgress() || !castle.getSiege().getAttackerClan( player.getClan() ) ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.S1_CANNOT_BE_USED )
                .addSkillName( skill )
                .getBuffer()
            player.sendOwnedData( packet )
            return false
        }

        if ( !castle.getArtefacts().includes( target.getObjectId() ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
            return false
        }

        if ( !GeneralHelper.checkIfInRange( skill.getCastRange(), player, target, true ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.DIST_TOO_FAR_CASTING_STOPPED ) )
            return false
        }

        castle.getSiege().announceDataToPlayer( SystemMessageBuilder.fromMessageId( SystemMessageIds.OPPONENT_STARTED_ENGRAVING ), false )

        return true
    }
}