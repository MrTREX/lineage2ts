import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'
import { L2Clan } from '../../L2Clan'

export const enum PlayerHasClanHallType {
    AnyHall = -1,
    NoClanRequired = 0
}

export class ConditionPlayerHasClanHalls extends Condition {
    clanHallIds: Array<number>

    constructor( ids: Array<number> = [ PlayerHasClanHallType.AnyHall ] ) {
        super()
        this.clanHallIds = ids
    }

    runTest( caster: L2Character ): boolean {
        let player = caster.getActingPlayer()
        if ( !player ) {
            return false
        }

        let clan : L2Clan = player.getClan()
        if ( !clan ) {
            return this.clanHallIds.length === 1 && this.clanHallIds[ 0 ] === PlayerHasClanHallType.NoClanRequired
        }

        if ( this.clanHallIds.length === 1 && this.clanHallIds[ 0 ] === PlayerHasClanHallType.AnyHall ) {
            return clan.getHideoutId() > 0
        }

        return this.clanHallIds.includes( clan.getHideoutId() )
    }
}

export class ConditionPlayerHasClanHall extends Condition {
    clanHallId: number

    constructor( id: number = PlayerHasClanHallType.AnyHall ) {
        super()
        this.clanHallId = id
    }

    runTest( caster: L2Character ): boolean {
        let player = caster.getActingPlayer()
        if ( !player ) {
            return false
        }

        let clan : L2Clan = player.getClan()
        if ( !clan ) {
            return this.clanHallId === PlayerHasClanHallType.NoClanRequired
        }

        if ( this.clanHallId === PlayerHasClanHallType.AnyHall ) {
            return clan.getHideoutId() > 0
        }

        return this.clanHallId === clan.getHideoutId()
    }
}