import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'
import { TvTEvent } from '../../entity/TvTEvent'

export class ConditionPlayerTvTEvent extends Condition {
    value: boolean

    constructor( value: boolean ) {
        super()
        this.value = value
    }

    runTest( caster: L2Character ): boolean {
        if ( !TvTEvent.isStarted() || !caster.isPlayer() ) {
            return !this.value
        }

        return TvTEvent.isPlayerParticipant( caster.getObjectId() ) === this.value
    }
}