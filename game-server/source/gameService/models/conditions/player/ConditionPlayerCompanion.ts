import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'
import { Skill } from '../../Skill'
import { SystemMessageBuilder } from '../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../packets/SystemMessageIdValues'

enum CompanionType {
    PET
}

export class ConditionPlayerCompanion extends Condition {
    type: number

    constructor( value: string ) {
        super()
        this.type = CompanionType[ value ]
    }

    runTest( caster: L2Character, target: L2Character, skill: Skill = undefined ): boolean {
        if ( this.type !== CompanionType.PET ) {
            return false
        }

        if ( !target.isPet() ) {
            if ( caster.isPlayable() ) {
                let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.S1_CANNOT_BE_USED )
                        .addSkillName( skill )
                        .getBuffer()

                caster.sendOwnedData( packet )
            }

            return false
        }

        return true
    }
}