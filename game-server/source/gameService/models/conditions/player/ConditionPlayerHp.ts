import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'

export class ConditionPlayerHp extends Condition {
    value: number

    constructor( value: number ) {
        super()
        this.value = value
    }

    runTest( caster: L2Character ): boolean {
        return caster && ( ( ( caster.getCurrentHp() * 100 ) / caster.getMaxHp() ) <= this.value )
    }
}