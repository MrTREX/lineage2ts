import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'
import { L2Clan } from '../../L2Clan'

export const enum PlayerHasFortType {
    AnyFort = -1,
    NoClanRequired = 0
}

export class ConditionPlayerHasFort extends Condition {
    fortId: number

    constructor( id: number = PlayerHasFortType.AnyFort ) {
        super()
        this.fortId = id
    }

    runTest( caster: L2Character ): boolean {
        let player = caster.getActingPlayer()
        if ( !player ) {
            return false
        }

        let clan: L2Clan = player.getClan()
        if ( !clan ) {
            return this.fortId === PlayerHasFortType.NoClanRequired
        }

        let fortId = clan.getFortId()
        if ( this.fortId === PlayerHasFortType.AnyFort ) {
            return fortId > 0
        }

        return fortId === this.fortId
    }
}