import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'
import { SystemMessageBuilder } from '../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../packets/SystemMessageIdValues'
import { TvTEvent } from '../../entity/TvTEvent'
import { L2PcInstance } from '../../actor/instance/L2PcInstance'
import { AreaType } from '../../areas/AreaType'

export class ConditionPlayerCallPc extends Condition {
    value: boolean

    constructor( value: boolean ) {
        super()
        this.value = value
    }

    runTest( caster: L2Character ): boolean {
        let player = caster.getActingPlayer()
        return this.checkPlayer( player ) === this.value
    }

    checkPlayer( player: L2PcInstance ) : boolean {
        if ( !player ) {
            return false
        }

        if ( player.isInOlympiadMode() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_MAY_NOT_SUMMON_FROM_YOUR_CURRENT_LOCATION ) )
            return false
        }

        if ( player.inObserverMode() ) {
            return false
        }

        if ( !TvTEvent.onEscapeUse( player.getObjectId() ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOUR_TARGET_IS_IN_AN_AREA_WHICH_BLOCKS_SUMMONING ) )
            return false
        }

        if ( player.isInArea( AreaType.NoSummonPlayer )
                || player.isInArea( AreaType.Jail )
                || player.isFlyingMounted() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOUR_TARGET_IS_IN_AN_AREA_WHICH_BLOCKS_SUMMONING ) )
            return false
        }

        return true
    }
}