import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'

export class ConditionPlayerWeight extends Condition {
    weight: number

    constructor( value: number ) {
        super()
        this.weight = value
    }

    runTest( caster: L2Character ): boolean {
        let player = caster.getActingPlayer()
        if ( player && player.getMaxLoad() > 0 ) {
            let currentWeight = ( ( player.getCurrentLoad() - player.getBonusWeightPenalty() ) * 100 ) / player.getMaxLoad()
            return ( currentWeight < this.weight ) || player.getWeightlessMode()
        }

        return true
    }
}