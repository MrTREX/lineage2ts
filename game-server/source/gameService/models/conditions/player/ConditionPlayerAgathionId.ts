import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'

export class ConditionPlayerAgathionId extends Condition {
    id: number

    constructor( id: number ) {
        super()
        this.id = id
    }

    runTest( caster: L2Character ): boolean {
        let player = caster.getActingPlayer()
        return player && player.getAgathionId() === this.id
    }
}