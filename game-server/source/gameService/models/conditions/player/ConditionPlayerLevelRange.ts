import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'

export class ConditionPlayerLevelRange extends Condition {
    minimumLevel: number
    maximumLevel: number

    constructor( min: number, max: number ) {
        super()
        this.minimumLevel = min
        this.maximumLevel = max
    }

    runTest( caster: L2Character ): boolean {
        let level = caster.getLevel()
        return level >= this.minimumLevel && level <= this.maximumLevel
    }
}