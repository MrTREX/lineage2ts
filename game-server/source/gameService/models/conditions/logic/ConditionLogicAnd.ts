import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'
import { Skill } from '../../Skill'
import { L2Item } from '../../items/L2Item'

export class ConditionLogicAnd extends Condition {
    conditions: Array<Condition> = []

    add( condition: Condition ): void {
        if ( !condition ) {
            return
        }

        this.conditions.push( condition )
    }

    runTest( caster: L2Character, target: L2Character, skill: Skill = null, item: L2Item = null ): boolean {
        for ( const condition of this.conditions ) {
            if ( !condition.test( caster, target, skill, item ) ) {
                return false
            }
        }

        return true
    }
}