import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'
import { L2MonsterInstance } from '../../actor/instance/L2MonsterInstance'
import { L2PcInstance } from '../../actor/instance/L2PcInstance'

export class ConditionTargetAggro extends Condition {
    value: boolean

    constructor( value: boolean ) {
        super()
        this.value = value
    }

    runTest( caster: L2Character, target: L2Character ): boolean {
        if ( target.isMonster() ) {
            return ( target as L2MonsterInstance ).isAggressive() === this.value
        }

        if ( target.isPlayer() ) {
            return ( target as L2PcInstance ).getKarma() > 0
        }

        return false
    }
}