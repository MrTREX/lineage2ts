import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'
import { InstanceType } from '../../../enums/InstanceType'

export class ConditionTargetPlayable extends Condition {
    runTest( caster: L2Character, target: L2Character ): boolean {
        return target.isPlayable()
    }
}