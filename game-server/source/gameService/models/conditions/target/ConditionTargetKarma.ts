import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'
import { L2PcInstance } from '../../actor/instance/L2PcInstance'

export class ConditionTargetKarma extends Condition {
    value: number

    constructor( value: number ) {
        super()
        this.value = value
    }

    runTest( caster: L2Character, target: L2Character ): boolean {
        if ( !target || !target.isPlayer() ) {
            return false
        }

        return ( target as L2PcInstance ).getKarma() >= this.value
    }
}