import { Condition } from '../Condition'
import { Skill } from '../../Skill'
import { L2Character } from '../../actor/L2Character'

export class ConditionUsingSkill extends Condition {
    value: number

    constructor( value: number ) {
        super()
        this.value = value
    }

    runTest( caster: L2Character, target: L2Character, skill: Skill = null ): boolean {
        if ( !skill ) {
            return false
        }

        return skill.getId() === this.value
    }
}