import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'

export class ConditionUsingSlotType extends Condition {
    value: number

    constructor( value: number ) {
        super()
        this.value = value
    }

    runTest( caster: L2Character ): boolean {
        if ( !caster || !caster.isPlayer() ) {
            return false
        }

        return ( caster.getActiveWeaponItem().getBodyPart() & this.value ) !== 0
    }
}