import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'
import { L2ItemInstance } from '../../items/instance/L2ItemInstance'

export class ConditionSlotItemId extends Condition {
    itemId: number
    enchantLevel: number
    slot : number

    constructor( slot: number, itemId: number, enchantLevel: number ) {
        super()
        this.slot = slot
        this.itemId = itemId
        this.enchantLevel = enchantLevel
    }

    runTest( caster: L2Character ): boolean {
        if ( !caster || !caster.isPlayer() ) {
            return false
        }

        let item: L2ItemInstance = caster.getInventory().getPaperdollItem( this.slot )
        if ( !item ) {
            return this.itemId === 0
        }

        return ( item.getId() === this.itemId ) && ( item.getEnchantLevel() >= this.enchantLevel )
    }
}