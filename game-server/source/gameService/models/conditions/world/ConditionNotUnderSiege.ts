import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'
import { CastleManager } from '../../../instancemanager/CastleManager'
import { FortManager } from '../../../instancemanager/FortManager'

export class ConditionNotUnderSiege extends Condition {
    residenceId: number

    constructor( residenceId: number ) {
        super()
        this.residenceId = residenceId
    }

    runTest( caster: L2Character ): boolean {
        if ( !caster.isPlayer() ) {
            return true
        }

        let castle = CastleManager.getCastleById( this.residenceId )
        if ( castle && castle.getSiege().isInProgress() ) {
            return false
        }

        let fort = FortManager.getFortById( this.residenceId )
        if ( fort && fort.getSiege().isInProgress() ) {
            return false
        }

        return true
    }
}