import { Condition } from './Condition'
import { CategoryType } from '../../enums/CategoryType'
import { L2Character } from '../actor/L2Character'

export class ConditionCategoryTypes extends Condition {
    types: Array<CategoryType>

    constructor( types: Array<CategoryType> ) {
        super()
        this.types = types
    }

    runTest( caster: L2Character ): boolean {
        return this.types.some( ( type: CategoryType ) => caster.isInCategory( type ) )
    }
}

export class ConditionCategoryType extends Condition {
    type: CategoryType

    constructor( type: CategoryType ) {
        super()
        this.type = type
    }

    runTest( caster: L2Character ): boolean {
        return caster.isInCategory( this.type )
    }
}