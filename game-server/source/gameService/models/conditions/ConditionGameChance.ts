import { Condition } from './Condition'
import _ from 'lodash'

export class ConditionGameChance extends Condition {
    chance: number

    constructor( value: number ) {
        super()
        this.chance = value
    }

    runTest(): boolean {
        return _.random( 100 ) < this.chance
    }
}