import { Condition } from './Condition'
import { L2Character } from '../actor/L2Character'
import { CastleManager } from '../../instancemanager/CastleManager'
import { FortManager } from '../../instancemanager/FortManager'
import { Castle } from '../entity/Castle'
import { Fort } from '../entity/Fort'
import { TerritoryWarManager } from '../../instancemanager/TerritoryWarManager'
import { L2PcInstance } from '../actor/instance/L2PcInstance'
import { SiegeRole } from '../../enums/SiegeRole'

const COND_NOT_ZONE = 0x0001
const COND_CAST_ATTACK = 0x0002
const COND_CAST_DEFEND = 0x0004
const COND_CAST_NEUTRAL = 0x0008
const COND_FORT_ATTACK = 0x0010
const COND_FORT_DEFEND = 0x0020
const COND_FORT_NEUTRAL = 0x0040
const COND_TW_CHANNEL = 0x0080
const COND_TW_PROGRESS = 0x0100

export class ConditionSiegeZone extends Condition {
    useCaster: boolean
    value: number

    constructor( value: number, isCaster: boolean ) {
        super()
        this.value = value
        this.useCaster = isCaster
    }

    runTest( caster: L2Character, target: L2Character ): boolean {
        let character : L2Character = this.useCaster ? caster : target
        let castle : Castle = CastleManager.getCastle( character )
        let fort : Fort = FortManager.getFort( character )

        if ( ( ( this.value & COND_TW_PROGRESS ) !== 0 ) && !TerritoryWarManager.isTWInProgress ) {
            return false
        }

        if ( ( ( this.value & COND_TW_CHANNEL ) !== 0 ) && !TerritoryWarManager.isTWChannelOpen ) {
            return false
        }

        if ( !caster && !fort ) {
            return ( this.value & COND_NOT_ZONE ) !== 0
        }

        if ( castle ) {
            return this.checkForCastle( character, castle )
        }
        return this.checkForFort( character, fort )
    }

    checkForCastle( character: L2Character, castle: Castle ) {
        if ( !character.isPlayer() ) {
            return false
        }

        let player = character as L2PcInstance

        if ( !castle || castle.getResidenceId() <= 0 ) {
            return ( this.value & COND_NOT_ZONE ) !== 0
        }

        if ( !castle.isSiegeActive() ) {
            return ( this.value & COND_NOT_ZONE ) !== 0
        }

        if ( ( ( this.value & COND_CAST_ATTACK ) !== 0 ) && player.isRegisteredOnThisSiegeField( castle.getResidenceId() ) && player.getSiegeRole() === SiegeRole.Attacker ) {
            return true
        }

        if ( ( ( this.value & COND_CAST_DEFEND ) !== 0 ) && player.isRegisteredOnThisSiegeField( castle.getResidenceId() ) && player.getSiegeRole() === SiegeRole.Defender ) {
            return true
        }

        return ( ( this.value & COND_CAST_NEUTRAL ) !== 0 ) && player.getSiegeRole() === SiegeRole.None
    }

    checkForFort( character: L2Character, fort: Fort ) {
        if ( !character.isPlayer() ) {
            return false
        }

        let player = character as L2PcInstance

        if ( ( !fort || fort.getResidenceId() <= 0 ) ) {
            return ( this.value & COND_NOT_ZONE ) !== 0
        }

        if ( !fort.isSiegeActive() ) {
            return ( this.value & COND_NOT_ZONE ) !== 0
        }

        if ( ( ( this.value & COND_FORT_ATTACK ) !== 0 ) && player.isRegisteredOnThisSiegeField( fort.getResidenceId() ) && player.getSiegeRole() === SiegeRole.Attacker ) {
            return true
        }

        if ( ( ( this.value & COND_FORT_DEFEND ) !== 0 ) && player.isRegisteredOnThisSiegeField( fort.getResidenceId() ) && player.getSiegeRole() === SiegeRole.Defender ) {
            return true
        }

        return ( ( this.value & COND_FORT_NEUTRAL ) !== 0 ) && player.getSiegeRole() === SiegeRole.None
    }
}