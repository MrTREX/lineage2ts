import { L2PcInstance } from './actor/instance/L2PcInstance'
import _ from 'lodash'

export class EnchantSkillHolder {
    level: number
    adena: number
    exp: number
    sp: number
    rates: Array<number>

    getRate( player: L2PcInstance ) : number {
        let level : number = player.getLevel()
        let rateIndex = _.clamp( level - 76, 0, 23 )
        return this.rates[ rateIndex ]
    }

    getSpCost() {
        return this.sp
    }

    getAdenaCost() {
        return this.adena
    }
}

export class L2EnchantSkillGroup {
    groupId: number
    skills: Array<EnchantSkillHolder> = []

    constructor( id: number ) {
        this.groupId = id
    }

    getEnchantGroupDetails() : Array<EnchantSkillHolder> {
        return this.skills
    }
}