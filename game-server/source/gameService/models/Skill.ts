import { SkillOperateType } from './skills/SkillOperateType'
import { TraitType } from './stats/TraitType'
import { AbnormalType } from './skills/AbnormalType'
import { L2TargetType, TargetVerifierMethodType } from './skills/targets/L2TargetType'
import { AffectSelectionScope } from './skills/targets/AffectSelectionScope'
import { MountType } from '../enums/MountType'
import { Condition } from './conditions/Condition'
import { AbstractEffect } from './effects/AbstractEffect'
import { L2ExtractableSkill } from './L2ExtractableSkill'
import { L2Character } from './actor/L2Character'
import { Formulas } from './stats/Formulas'
import { BuffInfo } from './skills/BuffInfo'
import { EffectScope } from './skills/EffectScope'
import { L2EffectType } from '../enums/effects/L2EffectType'
import { PlayerActionOverride } from '../values/PlayerConditions'
import { ConfigManager } from '../../config/ConfigManager'
import { L2PcInstance } from './actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../packets/send/SystemMessage'
import { SystemMessageIds } from '../packets/SystemMessageIdValues'
import { L2Object } from './L2Object'
import { L2BlockInstance } from './actor/instance/L2BlockInstance'
import { ArenaParticipantsHolder } from './ArenaParticipantsHolder'
import { BlockCheckerManager } from '../instancemanager/BlockCheckerManager'
import { ShotType } from '../enums/ShotType'
import { InstanceType } from '../enums/InstanceType'
import { TvTEvent } from './entity/TvTEvent'
import { SkillCache } from '../cache/SkillCache'
import { AffectObjectMethodType } from './skills/targets/AffectMethodRegistry'
import { AffectSelectionMethodType } from './skills/targets/AffectSelectionMethods'
import aigle from 'aigle'
import _ from 'lodash'
import { SiegeRole } from '../enums/SiegeRole'
import { PathFinding } from '../../geodata/PathFinding'
import { SkillMagicType } from '../enums/SkillMagicType'
import { AreaType } from './areas/AreaType'
import { PlayerPermission } from '../enums/PlayerPermission'
import { CommonSkillIds } from '../enums/skills/CommonSkillIds'

const defaultEmptyTargets: Array<L2Object> = []

export class Skill {
    id: number
    level: number
    displayId: number
    displayLevel: number
    name: string
    operateType: SkillOperateType
    magic: SkillMagicType
    traitType: TraitType
    reuseDelayLock: boolean
    startCostMp: number
    successCostMp: number
    mpPerChanneling: number
    hpConsume: number
    itemConsumeCount: number
    itemConsumeId: number
    castRange: number
    effectRange: number
    isAbnormalInstant: boolean
    abnormalLvl: number
    abnormalType: AbnormalType
    abnormalTime: number
    maskVisualEffectsStandard: number = 0
    maskVisualEffectsSpecial: number = 0
    maskVisualEffectsEvent: number = 0
    stayAfterDeath: boolean
    stayOnSubclassChange: boolean
    isRecoveryHerb: boolean

    // all times in milliseconds
    hitTime: number
    hitCancelTime: number
    coolTime: number
    reuseHashCode: number
    reuseDelay: number

    targetType: L2TargetType
    affectScope: AffectSelectionScope
    magicLevel: number
    lvlBonusRate: number
    activateRate: number
    minChance: number
    maxChance: number

    affectRange: number
    affectLimit: Array<number>

    nextActionIsAttack: boolean

    blockedInOlympiad: boolean

    attributeType: number
    attributePower: number

    basicProperty: Function

    overhit: boolean

    minPledgeClass: number
    chargeConsume: number
    soulMaxConsume: number

    directHpDamage: boolean // If true then damage is being make directly
    effectPoint: number

    conditions: Array<Condition> = []
    rideState: Array<MountType>
    effects: { [ scope: number ]: Array<AbstractEffect> } = {}

    isDebuffValue: boolean
    isDamageValue: boolean
    isRemovedOnAnyActionExceptMoveValue: boolean
    isRemovedOnDamageValue: boolean
    isAOEValue: boolean
    is7SignsValue: boolean
    isFishingValue: boolean
    isDisablerValue: boolean
    isBadValue: boolean
    hasPointRecoveryValue: boolean
    needsVisibleTargetValue: boolean

    // following will be computed at runtime due to how data dependency works
    isClanSkillValue: boolean = null
    isGMSkillValue: boolean = null
    isHeroSkillValue: boolean = null

    isSuicideAttack: boolean
    irreplaceableBuff: boolean

    excludedFromCheck: boolean
    simultaneousCast: boolean

    extractableItems: L2ExtractableSkill

    icon: string

    effectTypes: Set<L2EffectType> = new Set<L2EffectType>()

    channelingSkillId: number
    channelingTickInitialDelay: number
    channelingTickInterval: number

    fanRange: [ number, number, number, number ]
    isChannelingValue: boolean
    isActiveValue: boolean
    isToggleValue: boolean
    isPassiveValue: boolean
    isSelfContinuousValue: boolean
    isFlyTypeValue: boolean
    isContinuousValue: boolean

    targetVerifyMethod: TargetVerifierMethodType
    targetSelectionMethod: AffectSelectionMethodType
    affectObjectMethod: AffectObjectMethodType

    static checkForAreaOffensiveSkills( caster: L2Character, target: L2Character, skill: Skill, sourceInArena: boolean ) {
        if ( !target || target.isDead() || ( target.objectId === caster.objectId ) ) {
            return false
        }

        let player: L2PcInstance = caster.getActingPlayer()
        let targetPlayer: L2PcInstance = target.getActingPlayer()
        if ( player ) {
            if ( targetPlayer ) {
                if ( ( targetPlayer.objectId === caster.objectId ) || ( targetPlayer.objectId === player.objectId ) ) {
                    return false
                }

                if ( targetPlayer.inObserverMode() ) {
                    return false
                }

                if ( skill.isBad()
                        && player.getSiegeRole() !== SiegeRole.None
                        && player.isInSiegeArea()
                        && ( player.getSiegeRole() === targetPlayer.getSiegeRole() )
                        && ( player.getSiegeSide() === targetPlayer.getSiegeSide() ) ) {
                    return false
                }

                if ( skill.isBad() && target.isInArea( AreaType.Peace ) ) {
                    return false
                }

                if ( player.isInParty() && targetPlayer.isInParty() ) {
                    if ( player.getParty().getLeaderObjectId() === targetPlayer.getParty().getLeaderObjectId() ) {
                        return false
                    }

                    if ( player.getParty().isInCommandChannel()
                            && ( player.getParty().getCommandChannel() === targetPlayer.getParty().getCommandChannel() ) ) {
                        return false
                    }
                }

                if ( !TvTEvent.checkForTvTSkill( player, targetPlayer, skill ) ) {
                    return false
                }

                if ( !sourceInArena
                        && !( targetPlayer.isInArea( AreaType.PVP ) && !targetPlayer.isInSiegeArea() ) ) {
                    if ( ( player.getAllyId() !== 0 ) && ( player.getAllyId() === targetPlayer.getAllyId() ) ) {
                        return false
                    }

                    if ( ( player.getClanId() !== 0 ) && ( player.getClanId() === targetPlayer.getClanId() ) ) {
                        return false
                    }

                    if ( !player.checkPvpSkill( targetPlayer, skill ) ) {
                        return false
                    }
                }
            }
        } else if ( !targetPlayer && target.isAttackable() && caster.isAttackable() ) {
            return false
        }

        return PathFinding.canSeeTargetWithPosition( caster, target )
    }

    async activateCasterSkill( caster: L2Character, targets: Array<L2Object>, rechargeShots: boolean ) : Promise<void> {
        let currentSkill: Skill = this
        switch ( this.getId() ) {
            /*
                Flip Block
             */
            case 5852:
            case 5853:
                let firstTarget = _.head( targets )
                let block: L2BlockInstance = firstTarget.isInstanceType( InstanceType.L2BlockInstance ) ? firstTarget as L2BlockInstance : null
                let player: L2PcInstance = caster.isPlayer() ? caster as L2PcInstance : null
                if ( !block || !player ) {
                    return
                }

                let arena = player.getBlockCheckerArena()
                if ( arena !== -1 ) {
                    let holder: ArenaParticipantsHolder = BlockCheckerManager.getHolder( arena )
                    if ( !holder ) {
                        return
                    }

                    let team = holder.getPlayerTeam( player )
                    let color = block.getColorEffect()
                    if ( ( team === 0 ) && ( color === 0x00 ) ) {
                        await block.changeColor( player, holder, team )
                    } else if ( ( team === 1 ) && ( color === 0x53 ) ) {
                        await block.changeColor( player, holder, team )
                    }
                }
                break

            default:
                let hadReflection = this.hasDebuffReflection()
                await aigle.resolve( targets ).each( async ( target: L2Object ) : Promise<void> => {
                    let character = target as L2Character

                    if ( hadReflection && Formulas.calculateBuffDebuffReflection( character, currentSkill ) ) {
                        let info: BuffInfo = BuffInfo.fromParameters( caster, character, currentSkill )
                        let pvpOrPveEffectScope: EffectScope = caster.isPlayable() && character.isAttackable() ? EffectScope.PVE : ( caster.isPlayable() && target.isPlayable() ? EffectScope.PVP : null )

                        await Promise.all( [
                            currentSkill.applyEffects( character, caster, false, false, false, 0 ),
                            currentSkill.applyEffectScope( EffectScope.GENERAL, info, true, false ),
                            pvpOrPveEffectScope ? currentSkill.applyEffectScope( pvpOrPveEffectScope, info, true, false ) : Promise.resolve(),
                            currentSkill.applyEffectScope( EffectScope.CHANNELING, info, true, false )
                        ] )

                        return
                    }

                    return currentSkill.applyEffects( caster, character, false, false, true, 0 )
                } )
                break
        }

        if ( this.hasEffects( EffectScope.SELF ) ) {
            if ( caster.isAffectedBySkill( this.getId() ) ) {
                await caster.stopSkillEffects( true, currentSkill.getId() )
            }

            await this.applyEffects( caster, caster, true, false, true, 0 )
        }

        if ( rechargeShots ) {
            if ( this.useSpiritShot() ) {
                caster.setChargedShot( caster.isChargedShot( ShotType.BlessedSpiritshot ) ? ShotType.BlessedSpiritshot : ShotType.Spiritshot, false )
            } else if ( this.useSoulShot() ) {
                caster.setChargedShot( ShotType.Soulshot, false )
            }
        }

        if ( this.isSuicideAttack ) {
            await caster.doDie( caster )
        }
    }

    addEffect( effectScope: EffectScope, effect: AbstractEffect ) {
        if ( !this.effects[ effectScope ] ) {
            this.effects[ effectScope ] = []
        }

        this.effects[ effectScope ].push( effect )
        this.effectTypes.add( effect.effectType )
    }

    async applyEffectScope( scope: EffectScope, info: BuffInfo, applyInstantEffects: boolean, addContinuousEffects: boolean ) : Promise<void> {
        await aigle.resolve( this.getEffects( scope ) ).each( ( effect: AbstractEffect ) : Promise<void> => {
            if ( !effect.canStart( info ) ) {
                return
            }

            if ( effect.isInstant() ) {
                if ( applyInstantEffects && effect.calculateSuccess( info ) ) {
                    return effect.onStart( info )
                }

                return
            }

            if ( addContinuousEffects ) {
                info.addEffect( effect )
                return
            }
        } )
    }

    async applyEffects( attacker: L2Character, target: L2Character, isSelf: boolean = false, isPassive: boolean = false, isInstant: boolean = true, durationMs: number = 0 ) : Promise<void> {
        if ( !target ) {
            return
        }

        if ( attacker.getObjectId() !== target.getObjectId()
                && this.isBad()
                && ( target.isInvulnerable() || ( attacker.isGM() && !attacker.getAccessLevel().hasPermission( PlayerPermission.GiveDamage ) ) ) ) {
            return
        }

        if ( this.isDebuff() && target.isDebuffBlocked() ) {
            return
        }

        if ( target.isBuffBlocked() && !this.isBad() ) {
            return
        }

        if ( target.isInvulnerableAgainst( this.getId(), this.getLevel() ) ) {
            return
        }

        if ( !isSelf && !isPassive ) {
            let info: BuffInfo = BuffInfo.fromParameters( attacker, target, this )
            let addContinuousEffects = !isPassive && ( this.isToggleValue || ( this.isContinuousValue && Formulas.calculateEffectSuccess( attacker, target, this ) ) )

            if ( attacker.isPlayer() && this.getMaxSoulConsumeCount() > 0 ) {
                info.setCharges( attacker.getActingPlayer().decreaseSouls( this.getMaxSoulConsumeCount() ) )
            }

            if ( addContinuousEffects && durationMs > 0 ) {
                info.setDuration( durationMs )
            }

            let pvXScope = attacker.isPlayable() && target.isAttackable() ? EffectScope.PVE : ( attacker.isPlayable() && target.isPlayable() ? EffectScope.PVP : null )
            await Promise.all( [
                this.applyEffectScope( EffectScope.GENERAL, info, isInstant, addContinuousEffects ),
                pvXScope ? this.applyEffectScope( pvXScope, info, isInstant, addContinuousEffects ) : Promise.resolve(),
                this.applyEffectScope( EffectScope.CHANNELING, info, isInstant, addContinuousEffects )
            ] )

            if ( addContinuousEffects ) {
                await target.getEffectList().add( info )
            }

            if ( target.isPlayable() && target.hasServitor() && !this.isTransformation() && ( this.getAbnormalType() !== AbnormalType.SUMMON_CONDITION ) ) {
                if ( ( addContinuousEffects && this.isContinuousType() && !this.isDebuff() ) || this.isRecoveryHerb ) {
                    await this.applyEffects( attacker, target.getSummon(), false, false, this.isRecoveryHerb, 0 )
                }
            }

            return
        }

        if ( isSelf ) {
            let addContinuousEffects = !isPassive && ( this.isToggleValue || ( this.isContinuousType() && Formulas.calculateEffectSuccess( attacker, target, this ) ) )

            let info: BuffInfo = BuffInfo.fromParameters( attacker, attacker, this )
            if ( addContinuousEffects && durationMs > 0 ) {
                info.setDuration( durationMs )
            }

            await this.applyEffectScope( EffectScope.SELF, info, isInstant, addContinuousEffects )

            if ( addContinuousEffects && this.hasEffectType( L2EffectType.Buff ) ) {
                await info.getEffector().getEffectList().add( info )
            }

            if ( addContinuousEffects
                    && info.getAffected().isPlayer()
                    && info.getAffected().hasServitor()
                    && this.isContinuousType()
                    && !this.isDebuff()
                    && ( this.getId() !== CommonSkillIds.ServitorShare ) ) {

                await this.applyEffects( attacker, info.getAffected().getSummon(), false, false, false, 0 )
            }
        }

        if ( isPassive ) {
            let info: BuffInfo = BuffInfo.fromParameters( attacker, attacker, this )
            await this.applyEffectScope( EffectScope.PASSIVE, info, false, true )
            return attacker.getEffectList().add( info )
        }
    }

    canBeStolen() {
        return !this.isPassive()
                && !this.isToggle()
                && !this.isDebuff()
                && !this.isHeroSkill()
                && !this.isIrreplaceableBuff()
                && !( this.isStatic() && this.getId() !== CommonSkillIds.CaravanSecretMedicine && this.getMagicLevel() >= 0 )
                && !this.isGMSkill()
    }

    canBeUsedWhileRiding( player: L2PcInstance ) {
        return !this.rideState
                || this.rideState.length === 0
                || this.rideState.includes( player.getMountType() )
    }

    checkCondition( attacker: L2Character, target: L2Object ) : boolean {
        if ( attacker.hasActionOverride( PlayerActionOverride.SkillAction ) && !ConfigManager.general.gmSkillRestriction() ) {
            return true
        }

        if ( attacker.isPlayer() && !this.canBeUsedWhileRiding( attacker as L2PcInstance ) ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.S1_CANNOT_BE_USED )
                    .addSkillNameById( this.id )
                    .getBuffer()
            attacker.sendOwnedData( packet )
            return false
        }

        if ( this.conditions.length === 0 ) {
            return true
        }

        let targetCharacter: L2Character = ( target && target.isCharacter() ) ? target as L2Character : null

        return this.conditions.every( ( condition: Condition ) => {
            if ( !condition.test( attacker, targetCharacter, this ) ) {
                let message: string = condition.getMessage()
                let messageId: number = condition.getMessageId()

                if ( attacker.isPlayable() ) {
                    if ( messageId ) {
                        let systemMessage = new SystemMessageBuilder( messageId )
                        if ( condition.isAddName() ) {
                            systemMessage.addSkillNameById( this.id )
                        }

                        attacker.sendOwnedData( systemMessage.getBuffer() )
                    } else if ( message ) {
                        attacker.sendMessage( message )
                    }
                }

                return false
            }

            return true
        } )
    }

    getAbnormalLevel() {
        return this.abnormalLvl
    }

    getAbnormalTime() {
        return this.abnormalTime
    }

    getAbnormalType(): AbnormalType {
        return this.abnormalType
    }

    getActivateRate(): number {
        return this.activateRate
    }

    getAffectRange() {
        return this.affectRange
    }

    getAttributePower() {
        return this.attributePower
    }

    getAttributeType(): number {
        return this.attributeType
    }

    getBasicProperty(): Function {
        return this.basicProperty
    }

    getCastRange() {
        return this.castRange
    }

    getChannelingSkillId(): number {
        return this.channelingSkillId
    }

    getChannelingTickInterval() {
        return this.channelingTickInterval
    }

    getChargeConsume() {
        return this.chargeConsume
    }

    getDisplayId() {
        return this.displayId
    }

    getDisplayLevel(): number {
        return this.displayLevel
    }

    getEffectPoint() {
        return this.effectPoint
    }

    getEffectRange() {
        return this.effectRange
    }

    getEffects( scope: EffectScope ): Array<AbstractEffect> {
        return this.effects[ scope ]
    }

    getExtractableSkill() {
        return this.extractableItems
    }

    getHitTime() {
        return this.hitTime
    }

    getHpConsume() {
        return this.hpConsume
    }

    getId() {
        return this.id
    }

    getItemConsumeCount() {
        return this.itemConsumeCount
    }

    getItemConsumeId() {
        return this.itemConsumeId
    }

    getLevel() {
        return this.level
    }

    getLevelBonusRate() {
        return this.lvlBonusRate
    }

    getMagicLevel() {
        return this.magicLevel
    }

    getMaxChance() {
        return this.maxChance
    }

    getMaxSoulConsumeCount(): number {
        return this.soulMaxConsume
    }

    getMinChance() {
        return this.minChance
    }

    getMinPledgeClass() {
        return this.minPledgeClass
    }

    getStartCostMp() {
        return this.startCostMp
    }

    getFinishCostMp() {
        return this.successCostMp
    }

    getTotalCostMp() : number {
        return this.startCostMp + this.successCostMp
    }

    getMpPerChanneling() {
        return this.mpPerChanneling
    }

    getReuseDelay() {
        return this.reuseDelay
    }

    getReuseHashCode() {
        return this.reuseHashCode
    }

    getSingleTargetOf( character: L2Character ) : L2Object {
        return this.targetVerifyMethod( this, character, character.getTarget() )
    }

    getTargetsOf( character: L2Character ): Array<L2Object> {
        let target = character.getTarget()

        if ( target && !target.isCharacter() ) {
            target = null
        }

        return Skill.getTargetWithSelectedTarget( character, target as L2Character, this, this.targetVerifyMethod, this.targetSelectionMethod )
    }

    getTargetWithSelectedTarget( caster: L2Character, target: L2Object ) {
        return Skill.getTargetWithSelectedTarget( caster, target as L2Character, this, this.targetVerifyMethod, this.targetSelectionMethod )
    }

    static getTargetWithSelectedTarget( caster: L2Character, target: L2Character, skill: Skill, targetVerifyMethod: TargetVerifierMethodType, targetSelectionMethod: AffectSelectionMethodType ): Array<L2Object> {
        let actualTarget: L2Object = targetVerifyMethod( skill, caster, target )
        if ( !actualTarget ) {
            return defaultEmptyTargets
        }

        let targets : Array<L2Object> = targetSelectionMethod( caster, actualTarget, skill )
        return targets ?? defaultEmptyTargets
    }

    getTargetType(): L2TargetType {
        return this.targetType
    }

    getTraitType(): TraitType {
        return this.traitType
    }

    hasEffectType( type: L2EffectType ): boolean {
        return this.effectTypes.has( type )
    }

    hasEffects( scope: EffectScope ) {
        return this.effects[ scope ]?.length > 0
    }

    is7Signs() {
        return this.is7SignsValue
    }

    isAOE(): boolean {
        return this.isAOEValue
    }

    isActive() {
        return this.isActiveValue
    }

    isBad(): boolean {
        return this.isBadValue
    }

    isChanneling() {
        return this.isChannelingValue
    }

    isClanSkill() {
        if ( this.isClanSkillValue === null ) {
            this.isClanSkillValue = SkillCache.isClanSkill( this.id, this.level )
        }

        return this.isClanSkillValue
    }

    isContinuousType() {
        return this.isContinuousValue || this.isSelfContinuous()
    }

    isDamage() : boolean {
        return this.isDamageValue
    }

    isDance() {
        return this.magic === SkillMagicType.Dance
    }

    isDebuff() {
        return this.isDebuffValue
    }

    isExcludedFromCheck() {
        return this.excludedFromCheck
    }

    isFlyType() {
        return this.isFlyTypeValue
    }

    isGMSkill() : boolean {
        if ( this.isGMSkillValue === null ) {
            this.isGMSkillValue = SkillCache.isGMSkill( this.id, this.level )
        }

        return this.isGMSkillValue
    }

    isHealingPotionSkill() {
        return this.abnormalType === AbnormalType.HP_RECOVER
    }

    isHeroSkill() {
        if ( this.isHeroSkillValue === null ) {
            this.isHeroSkillValue = SkillCache.isHeroSkill( this.id, this.level )
        }

        return this.isHeroSkillValue
    }

    isIrreplaceableBuff() {
        return this.irreplaceableBuff
    }

    isMagic(): boolean {
        return this.magic === SkillMagicType.MagicCast
    }

    isOverhit() {
        return this.overhit
    }

    isPassive(): boolean {
        return this.isPassiveValue
    }

    isPhysical(): boolean {
        return this.magic === SkillMagicType.PhysicalCast
    }

    isRemovedOnAnyActionExceptMove() {
        return this.isRemovedOnAnyActionExceptMoveValue
    }

    isRemovedOnDamage() {
        return this.isRemovedOnDamageValue
    }

    isReuseDelayLocked() {
        return this.reuseDelayLock
    }

    isSelfContinuous(): boolean {
        return this.isSelfContinuousValue
    }

    isSimultaneousCast() {
        return this.simultaneousCast
    }

    isStatic() {
        return this.magic === SkillMagicType.StaticAbility
    }

    isStayAfterDeath() {
        return this.stayAfterDeath
    }

    isToggle(): boolean {
        return this.isToggleValue
    }

    isTransformation(): boolean {
        return this.abnormalType === AbnormalType.TRANSFORM
    }

    isTrigger() {
        return this.magic === SkillMagicType.Trigger
    }

    useSoulShot() {
        return this.hasEffectType( L2EffectType.PhysicalAttack )
    }

    useSpiritShot() {
        return this.isMagic()
    }

    getDamageDirectlyToHP(): boolean {
        return this.directHpDamage
    }

    getName() {
        return this.name
    }

    getAffectScope(): AffectSelectionScope {
        return this.affectScope
    }

    getAffectLimit() : number {
        let [ min, max ] = this.affectLimit
        if ( !max ) {
            return Number.MAX_SAFE_INTEGER
        }

        return min + _.random( max )
    }


    getFirstTargetOf( character: L2Character ): L2Object {
        return _.head( this.getTargetsOf( character ) )
    }

    isStayOnSubclassChange(): boolean {
        return this.stayOnSubclassChange
    }

    getFanRange(): Array<number> {
        return this.fanRange
    }

    isFishing() : boolean {
        return this.isFishingValue
    }

    isDisabler() : boolean {
        return this.isDisablerValue
    }

    hasPointRecovery() : boolean {
        return this.hasPointRecoveryValue
    }

    needsVisibleTarget() : boolean {
        return this.needsVisibleTargetValue
    }

    hasDebuffReflection() : boolean {
        return this.isDebuff() && this.getActivateRate() !== -1
    }
}