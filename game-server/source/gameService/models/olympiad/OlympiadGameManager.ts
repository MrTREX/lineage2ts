import { OlympiadGameTask } from './OlympiadGameTask'
import { L2PcInstance } from '../actor/instance/L2PcInstance'
import { AbstractOlympiadGame } from './AbstractOlympiadGame'

class Manager {
    battleStarted: boolean = false
    tasks: Array<OlympiadGameTask> = []

    getOlympiadTask( id: number ) : OlympiadGameTask {
        return this.tasks[ id ]
    }

    notifyCompetitorDamage( player: L2PcInstance, damage: number ) : void {
        if ( !player ) {
            return
        }

        let id = player.getOlympiadGameId()
        if ( ( id < 0 ) || ( id >= this.tasks.length ) ) {
            return
        }

        let game : AbstractOlympiadGame = this.tasks[ id ].getGame()
        if ( game ) {
            game.addDamage( player, damage )
        }
    }

    getNumberOfStadiums() : number {
        return this.tasks.length
    }

    getAllTasks() : Array<OlympiadGameTask> {
        return this.tasks
    }
}

export const OlympiadGameManager = new Manager()