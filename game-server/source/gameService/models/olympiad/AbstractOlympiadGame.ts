import { L2PcInstance } from '../actor/instance/L2PcInstance'
import { CompetitionType } from './CompetitionType'
import { L2Character } from '../actor/L2Character'

export const OlympiadGameValues = {
    CHAR_ID: 'charId',
    CLASS_ID: 'class_id',
    CHAR_NAME: 'char_name',
    POINTS: 'olympiad_points',
    COMP_DONE: 'competitions_done',
    COMP_WON: 'competitions_won',
    COMP_LOST: 'competitions_lost',
    COMP_DRAWN: 'competitions_drawn',
    COMP_DONE_WEEK: 'competitions_done_week',
    COMP_DONE_WEEK_CLASSED: 'competitions_done_week_classed',
    COMP_DONE_WEEK_NON_CLASSED: 'competitions_done_week_non_classed',
    COMP_DONE_WEEK_TEAM: 'competitions_done_week_team',
}

export const enum OlympiadType {
    Teams = -1,
    Normal = 0,
    NonClassed = 1,
    Classed = 2
}

export type OlympiadPlayerNames = [ string, string ]

export class AbstractOlympiadGame {
    startTime: number = 0
    aborted: boolean = false
    stadiumID: number

    addDamage( player: L2PcInstance, damage ): void {

    }

    containsParticipant( objectId: number ) {
        return false
    }

    getType(): CompetitionType {
        return null
    }

    handleDisconnect( player: L2PcInstance ) {

    }

    isNonClassed(): boolean {
        return false
    }

    isClassed(): boolean {
        return false
    }

    isTeams(): boolean {
        return false
    }

    getStadiumId() {
        return this.stadiumID
    }

    getPlayerNames(): OlympiadPlayerNames {
        return [ '', '' ]
    }

    sendOlympiadInfo( character: L2Character ) {

    }

    getGameType() : OlympiadType {
        return OlympiadType.Normal
    }
}