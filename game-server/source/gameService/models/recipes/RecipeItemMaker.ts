import { Skill } from '../Skill'
import { L2PcInstance } from '../actor/instance/L2PcInstance'
import { L2World } from '../../L2World'
import { ActionFailed } from '../../packets/send/ActionFailed'
import { L2ManufactureItem } from '../L2ManufactureItem'
import { SystemMessageBuilder, SystemMessageHelper } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { ConfigManager } from '../../../config/ConfigManager'
import { PacketDispatcher } from '../../PacketDispatcher'
import { RecipeController } from '../../taskmanager/RecipeController'
import { RecipeItemMakeInfo } from '../../packets/send/RecipeItemMakeInfo'
import { RecipeShopItemInfo } from '../../packets/send/RecipeShopItemInfo'
import { Inventory } from '../itemcontainer/Inventory'
import { L2ItemInstance } from '../items/instance/L2ItemInstance'
import aigle from 'aigle'
import { ObjectPool } from '../../helpers/ObjectPoolHelper'
import { ClanHallFunctionType } from '../../enums/clanhall/ClanHallFunctionType'
import { CommonSkillIds } from '../../enums/skills/CommonSkillIds'
import { L2RecipeDataInput, L2RecipeDataItem } from '../../../data/interface/RecipeDataApi'
import { AreaType } from '../areas/AreaType'
import { AcquisitionPointType, GamePointsCache } from '../../cache/GamePointsCache'
import { ListenerCache } from '../../cache/ListenerCache'
import { EventType, PlayerConsumeCraftMPEvent } from '../events/EventType'
import { EventPoolCache } from '../../cache/EventPoolCache'

interface ConsumedItem {
    itemId: number
    quantity: number
    name: string
    price: number
}

const ConsumedItemCache = new ObjectPool( 'Recipe:ConsumedItem', (): ConsumedItem => {
    return {
        itemId: 0,
        name: '',
        price: 0,
        quantity: 0,
    }
} )

export class RecipeItemMaker {
    items: Array<ConsumedItem> = []
    recipe: L2RecipeDataItem
    makerId: number
    customerId: number
    skill: Skill
    skillId: number
    skillLevel: number
    exp: number = -1
    sp: number = -1
    price: number
    totalItems: number

    constructor( player: L2PcInstance, recipe: L2RecipeDataItem, target: L2PcInstance ) {
        this.makerId = player.getObjectId()
        this.recipe = recipe
        this.customerId = target.getObjectId()

        this.skillId = recipe.isCommon ? CommonSkillIds.CreateCommonItems : CommonSkillIds.CreateDwarvenItems
        this.skillLevel = player.getSkillLevel( this.skillId )
        this.skill = player.getKnownSkill( this.skillId )

        player.setIsInCraftMode( true )
    }

    async onStart() : Promise<void> {
        if ( !this.isValidTransaction() ) {
            return this.onStop()
        }

        if ( !this.calculatePrerequisites() ) {
            return this.onStop()
        }

        return this.processNormalCrafting()
    }

    isValidTransaction(): boolean {
        if ( !ConfigManager.character.crafting() ) {
            PacketDispatcher.sendOwnedData( this.customerId, SystemMessageHelper.sendString( 'Item creation is currently disabled.' ) )
            return false
        }

        let player = L2World.getPlayer( this.makerId )
        let target = L2World.getPlayer( this.customerId )

        if ( !player || !player.isOnline() || !target || !target.isOnline() ) {
            return false
        }

        if ( player.isAlikeDead() || player.isProcessingTransaction() ) {
            player.sendOwnedData( ActionFailed() )
            return false
        }

        if ( target.isAlikeDead() || target.isProcessingTransaction() ) {
            player.sendOwnedData( ActionFailed() )
            return false
        }

        if ( this.recipe.inputs.length === 0 ) {
            player.sendOwnedData( ActionFailed() )
            return false
        }

        if ( this.recipe.level > this.skillLevel ) {
            player.sendOwnedData( ActionFailed() )
            return false
        }

        if ( this.makerId !== this.customerId ) {
            let recipeId = this.recipe.id
            let item: L2ManufactureItem = player.getManufactureItems().find( ( item: L2ManufactureItem ) => item.recipeId === recipeId )
            if ( item ) {
                this.price = item.adenaCost
                if ( target.getAdena() < this.price ) {
                    target.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ADENA ) )
                    return false
                }
            }
        }

        if ( this.customerId !== this.makerId && this.price > 0 && target.getInventory().getAdenaAmount() < this.price ) {
            target.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ADENA ) )
            return false
        }

        return true
    }

    onStop() {
        this.updateMakeInfo( false )

        let player = L2World.getPlayer( this.makerId )
        if ( player ) {
            player.setIsInCraftMode( false )
        }

        RecipeController.removeMaker( this.makerId )
    }

    updateMakeInfo( isSuccess: boolean ) {
        if ( this.customerId === this.makerId ) {
            PacketDispatcher.sendOwnedData( this.customerId, RecipeItemMakeInfo( this.recipe.id, this.customerId, isSuccess ) )
            return
        }

        let makerPlayer = L2World.getPlayer( this.makerId )
        if ( !makerPlayer ) {
            return
        }

        PacketDispatcher.sendOwnedData( this.customerId, RecipeShopItemInfo( makerPlayer, this.recipe.id ) )
    }

    calculatePrerequisites(): boolean {
        this.items = this.getConsumedItems()
        if ( this.items.length === 0 ) {
            return false
        }

        this.totalItems = this.items.reduce( ( totalAmount: number, item: ConsumedItem ) => {
            return totalAmount + item.quantity
        }, 0 )

        let player = L2World.getPlayer( this.makerId )
        if ( !player ) {
            return false
        }

        if ( this.consumeRecipeMp( player, true ) ) {
            return false
        }

        this.updateMakeInfo( true )

        player.setIsInCraftMode( false )

        return true
    }

    getConsumedItems(): Array<ConsumedItem> {
        let target = L2World.getPlayer( this.customerId )
        let customerInventory: Inventory = target.getInventory()
        let materials: Array<ConsumedItem> = []

        let shouldExit = this.recipe.inputs.some( ( requiredItem: L2RecipeDataInput ) => {
            if ( requiredItem.amount > 0 ) {
                let item: L2ItemInstance = customerInventory.getItemByItemId( requiredItem.id )
                let itemQuantityAmount = !item ? 0 : item.getCount()

                if ( itemQuantityAmount < requiredItem.amount ) {
                    let packet = new SystemMessageBuilder( SystemMessageIds.MISSING_S2_S1_TO_CREATE )
                            .addItemNameWithId( requiredItem.id )
                            .addNumber( requiredItem.amount - itemQuantityAmount )
                            .getBuffer()
                    target.sendOwnedData( packet )
                    return true
                }

                let data = ConsumedItemCache.getValue()

                data.itemId = item.getId()
                data.quantity = requiredItem.amount
                data.name = item.getItem().getName()
                data.price = item.getReferencePrice()

                materials.push( data )
            }

            return false
        } )

        if ( shouldExit ) {
            ConsumedItemCache.recycleValues( materials )
            return []
        }

        return materials
    }

    consumeRecipeMp( player: L2PcInstance, isTestRun: boolean ): boolean {
        let multiplier = getPlayerMpMultiplier( player )
        let modifiedValue = this.recipe.consumeMp * multiplier

        if ( player.getCurrentMp() < modifiedValue ) {
            PacketDispatcher.sendOwnedData( this.customerId, SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_MP ) )
            return false
        }

        if ( !isTestRun ) {
            player.reduceCurrentMp( modifiedValue )
            GamePointsCache.earnPoints( player, AcquisitionPointType.CraftMp, modifiedValue )
        }

        return true
    }

    async processNormalCrafting(): Promise<void> {

        let player = L2World.getPlayer( this.makerId )
        let target = L2World.getPlayer( this.customerId )

        if ( !player || !target ) {
            return this.unRegister()
        }

        if ( this.customerId !== this.makerId && this.price > 0 ) {
            let customerAdena = target.getInventory().getAdenaItem()
            if ( !customerAdena || customerAdena.getCount() < this.price ) {
                target.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ADENA ) )
                return
            }

            let adenaItem: L2ItemInstance = await target.transferItem( customerAdena.getObjectId(), this.price, player.getInventory() )
            if ( !adenaItem ) {
                target.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ADENA ) )
                return
            }
        }

        if ( !this.consumeRecipeMp( player, true ) ) {
            return this.unRegister()
        }

        if ( ListenerCache.hasGeneralListener( EventType.PlayerConsumeCraftMP ) ) {
            let data = EventPoolCache.getData( EventType.PlayerConsumeCraftMP ) as PlayerConsumeCraftMPEvent

            data.playerId = player.getObjectId()
            data.customerId = this.customerId
            data.recipeId = this.recipe.id

            await ListenerCache.sendGeneralEvent( EventType.PlayerConsumeCraftMP, data, this.recipe.id )
        }

        await aigle.resolve( this.items ).eachSeries( async ( item: ConsumedItem ) => {
            await target.getInventory().destroyItemByItemId( item.itemId, item.quantity, 0, 'RecipeItemMaker.getConsumedItems' )

            if ( item.quantity > 1 ) {
                let packet = new SystemMessageBuilder( SystemMessageIds.S2_S1_DISAPPEARED )
                        .addItemNameWithId( item.itemId )
                        .addNumber( item.quantity )
                        .getBuffer()
                target.sendOwnedData( packet )
            } else {
                let packet = new SystemMessageBuilder( SystemMessageIds.S1_DISAPPEARED )
                        .addItemNameWithId( item.itemId )
                        .getBuffer()
                target.sendOwnedData( packet )
            }
        } )

        /*
            Recipes use success rate in following situations:
            - general success rate that determines if item can be created
            - per created item if there are multiple output items in a recipe
         */
        let isSuccess = Math.random() <= this.recipe.successRate
        if ( isSuccess ) {
            await this.createOutputItem()
        } else {
            this.notifyCreationFailed( player, target )
        }

        this.updateMakeInfo( isSuccess )

        player.setIsInCraftMode( false )

        return this.unRegister()
    }

    notifyCreationFailed( player: L2PcInstance, target: L2PcInstance ) : void {
        if ( this.customerId !== this.makerId ) {
            let playerStatus = new SystemMessageBuilder( SystemMessageIds.CREATION_OF_S2_FOR_C1_AT_S3_ADENA_FAILED )
                .addString( target.getName() )
                .addItemNameWithId( this.recipe.outputs[ 0 ].id )
                .getBuffer()
            player.sendOwnedData( playerStatus )

            let targetStatus = new SystemMessageBuilder( SystemMessageIds.C1_FAILED_TO_CREATE_S2_FOR_S3_ADENA )
                .addString( player.getName() )
                .addItemNameWithId( this.recipe.outputs[ 0 ].id )
                .addNumber( this.price )
                .getBuffer()
            target.sendOwnedData( targetStatus )
        } else {
            target.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ITEM_MIXING_FAILED ) )
        }
    }

    async createOutputItem(): Promise<void> {
        let player = L2World.getPlayer( this.makerId )
        let target = L2World.getPlayer( this.customerId )
        let itemId = this.recipe.outputs[ 0 ].id
        let itemCount = this.recipe.outputs[ 0 ].amount

        if ( this.recipe.outputs.length !== 1 ) {
            let chance = Math.random()

            for ( const outputItem of this.recipe.outputs ) {
                if ( chance <= outputItem.chance ) {
                    itemId = outputItem.id
                    itemCount = outputItem.amount
                    break
                }

                chance = chance - outputItem.chance
            }
        }

        await target.getInventory().addItem( itemId, itemCount, player.getObjectId(), 'RecipeItemMaker.rewardPlayer' )

        if ( this.customerId !== this.makerId ) {
            if ( itemCount === 1 ) {
                let playerMessage = new SystemMessageBuilder( SystemMessageIds.S2_CREATED_FOR_C1_FOR_S3_ADENA )
                        .addString( target.getName() )
                        .addItemNameWithId( itemId )
                        .addNumber( this.price )
                        .getBuffer()
                player.sendOwnedData( playerMessage )

                let targetMessage = new SystemMessageBuilder( SystemMessageIds.C1_CREATED_S2_FOR_S3_ADENA )
                        .addString( player.getName() )
                        .addItemNameWithId( itemId )
                        .addNumber( this.price )
                        .getBuffer()
                target.sendOwnedData( targetMessage )
            } else {
                let playerMessage = new SystemMessageBuilder( SystemMessageIds.S2_S3_S_CREATED_FOR_C1_FOR_S4_ADENA )
                        .addString( target.getName() )
                        .addNumber( itemCount )
                        .addItemNameWithId( itemId )
                        .addNumber( this.price )
                        .getBuffer()
                player.sendOwnedData( playerMessage )

                let targetMessage = new SystemMessageBuilder( SystemMessageIds.C1_CREATED_S2_S3_S_FOR_S4_ADENA )
                        .addString( player.getName() )
                        .addNumber( itemCount )
                        .addItemNameWithId( itemId )
                        .addNumber( this.price )
                        .getBuffer()
                target.sendOwnedData( targetMessage )
            }
        }

        if ( itemCount > 1 ) {
            let message = new SystemMessageBuilder( SystemMessageIds.EARNED_S2_S1_S )
                    .addItemNameWithId( itemId )
                    .addNumber( itemCount )
                    .getBuffer()
            target.sendOwnedData( message )
        } else {
            let message = new SystemMessageBuilder( SystemMessageIds.EARNED_ITEM_S1 )
                    .addItemNameWithId( itemId )
                    .getBuffer()
            target.sendOwnedData( message )
        }
    }

    unRegister(): void {
        RecipeController.removeMaker( this.makerId )
        ConsumedItemCache.recycleValues( this.items )

        this.items.length = 0
    }
}

export function getPlayerMpMultiplier( player: L2PcInstance ): number {
    let multiplier = player.getClanHallFunctionLevel( ClanHallFunctionType.CraftingMpDecrease )
    if ( !multiplier || !player.isInArea( AreaType.ClanHall ) ) {
        return 1
    }

    return 1 - ( Math.min( 99, multiplier ) / 100 )
}