import { Options } from './options/Options'
import { L2PcInstance } from './actor/instance/L2PcInstance'
import { DataManager } from '../../data/manager'
import _ from 'lodash'

export class L2Augmentation {
    effectsId: number
    stat: L2AugmentationStat

    constructor( id: number ) {
        this.effectsId = id
        this.stat = new L2AugmentationStat( id )
    }

    getAugmentationId() {
        return this.effectsId
    }

    getAttributes() {
        return this.effectsId
    }

    removeBonus( player: L2PcInstance ) {
        this.stat.removeBonus( player )
    }

    applyBonus( player: L2PcInstance ) {
        this.stat.applyBonus( player )
    }
}

class L2AugmentationStat {
    isActive: boolean
    options: Array<Options>

    constructor( id: number ) {
        this.isActive = false

        let ids = [ 0x0000FFFF & id, id >> 16 ]

        this.options = _.reduce( ids, ( allOptions, currentId ) => {
            let option : Options = DataManager.getOptionsData().getOptions( currentId )
            if ( option ) {
                allOptions.push( option )
            }

            return allOptions
        }, [] )
    }

    removeBonus( player: L2PcInstance ) {
        if ( !this.isActive ) {
            return
        }

        _.each( this.options, ( option: Options ) => {
            option.remove( player )
        } )

        this.isActive = false
    }

    applyBonus( player: L2PcInstance ) {
        if ( this.isActive ) {
            return
        }

        _.each( this.options, ( option: Options ) => {
            option.apply( player )
        } )

        this.isActive = true
    }
}