import { VehiclePathPoint } from '../VehiclePathPoint'

export class AirShipTeleportList {
    location: number
    fuel: Array<number> = []
    routes: Array<Array<VehiclePathPoint>> = []

    getRoute() {
        return this.routes
    }

    getFuel() {
        return this.fuel
    }
}