import { IAnnouncement } from './IAnnouncement'
import { L2PcInstance } from '../actor/instance/L2PcInstance'
import { AnnouncementType } from './AnnouncementType'
import { CreatureSay } from '../../packets/send/builder/CreatureSay'
import { L2DataApi } from '../../../data/interface/l2DataApi'
import { DatabaseManager } from '../../../database/manager'
import _ from 'lodash'
import { NpcSayType } from '../../enums/packets/NpcSayType'

/*
    TODO : use different types of announcements based on usage:
    - admin roles
    - clan general/created/deleted
    - party created/disbanded
    - fort/castle captured

    Consider that announcements are only triggered on certain events, meaning they should not be repeated on timer.
    Keep data load and debounced save logic in this file. Separate trigger logic into listeners.
 */
class Manager implements L2DataApi {
    announcementsById: { [ id: number ] : IAnnouncement }
    announcementByType: Record<number, Set<IAnnouncement>>
    currentId : number = 0

    async addAnnouncement( message: IAnnouncement ): Promise<number> {
        if ( !message.getId() ) {
            message.setId( this.currentId )
        }

        await message.storeMe()
        this.announcementsById[ message.getId() ] = message

        this.currentId++
        return message.getId()
    }

    async load(): Promise<Array<string>> {
        let announcements = await DatabaseManager.getAnnouncements().getAll()
        let ids = [ 0 ]

        this.announcementsById = {}
        this.announcementByType = {}

        announcements.forEach( ( data: IAnnouncement ) => {
            ids.push( data.getId() )

            this.announcementsById[ data.getId() ] = data

            if ( !this.announcementByType[ data.getType() ] ) {
                this.announcementByType[ data.getType() ] = new Set<IAnnouncement>()
            }

            this.announcementByType[ data.getType() ].add( data )
        } )

        this.currentId = _.max( ids ) + 1

        return [
            `AnnouncementManager : loaded ${ _.size( this.announcementsById ) } messages`,
            `AnnouncementManager : next id is ${ this.currentId }`,
        ]
    }

    sendAnnouncements( player: L2PcInstance, type: AnnouncementType ) {
        let announcements = this.announcementByType[ type ]
        if ( !announcements ) {
            return
        }

        announcements.forEach( ( item: IAnnouncement ) => {
            if ( !item.isValid() ) {
                return
            }

            player.sendOwnedData( CreatureSay.fromText(
                    player.getObjectId(),
                    0,
                    type === AnnouncementType.CRITICAL ? NpcSayType.CriticalAnnouncement : NpcSayType.Announce,
                    player.getName(),
                    item.getContent() ) )
        } )
    }

    showAnnouncements( player: L2PcInstance ): void {
        this.sendAnnouncements( player, AnnouncementType.NORMAL )
        this.sendAnnouncements( player, AnnouncementType.CRITICAL )
        this.sendAnnouncements( player, AnnouncementType.EVENT )
    }

    getAnnouncement( id : number ) : IAnnouncement {
        return this.announcementsById[ id ]
    }

    async deleteAnnouncement( id : string | number ) : Promise<boolean> {
        let announcement : IAnnouncement = this.announcementsById[ id ]
        if ( announcement ) {
            await announcement.deleteMe()
            this.announcementByType[ announcement.getType() ].delete( announcement )

            return true
        }

        return false
    }

    getAllAnnouncements() : Array<IAnnouncement> {
        return _.values( this.announcementsById )
    }
}

export const AnnouncementManager = new Manager()