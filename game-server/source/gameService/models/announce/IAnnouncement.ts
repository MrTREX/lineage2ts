import { AnnouncementType } from './AnnouncementType'

export interface IAnnouncement {
    getId() : number
    setId( id : number ) : void
    getType() : AnnouncementType
    setType( type : AnnouncementType ) : void
    isValid() : boolean
    getContent() : string
    setContent( content : string ) : void
    getAuthor() : string
    setAuthor( author : string ) : void
    isAuto() : boolean

    deleteMe() : Promise<void>
    updateMe() : Promise<void>
    storeMe() : Promise<void>
}