import { IAnnouncement } from './IAnnouncement'
import { AnnouncementType } from './AnnouncementType'
import { DatabaseManager } from '../../../database/manager'

export class GeneralAnnouncement implements IAnnouncement {
    id: number
    type: AnnouncementType
    author: string
    content: string

    constructor( type: AnnouncementType, author: string, content: string, id: number = null ) {
        this.type = type
        this.author = author
        this.content = content
        this.id = id
    }

    getAuthor(): string {
        return this.author
    }

    getContent(): string {
        return this.content
    }

    getId(): number {
        return this.id
    }

    getType(): AnnouncementType {
        return this.type
    }

    isValid(): boolean {
        return true
    }

    setAuthor( author: string ): void {
        this.author = author
    }

    setContent( content: string ): void {
        this.content = content
    }

    setType( type: AnnouncementType ): void {
        this.type = type
    }

    async deleteMe() {
        return DatabaseManager.getAnnouncements().deleteItem( this )
    }

    async updateMe() {
        return DatabaseManager.getAnnouncements().updateGeneral( this )
    }

    async storeMe() {
        this.id = await DatabaseManager.getAnnouncements().createGeneral( this )
    }

    isAuto() : boolean {
        return false
    }

    setId( id: number ) {
        this.id = id
    }
}