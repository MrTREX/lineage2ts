import { PacketDispatcher } from '../PacketDispatcher'
import { RadarControl, RadarControlOperation, RadarControlType } from '../packets/send/RadarControl'
import _ from 'lodash'

export class L2Radar {
    ownerId: number
    markers: Array<RadarMarker> = []

    constructor( objectId: number ) {
        this.ownerId = objectId
    }

    addMarker( x: number, y: number, z: number ) {
        let marker : RadarMarker = {
            type: RadarControlType.Normal,
            x,
            y,
            z
        }

        this.markers.push( marker )

        PacketDispatcher.sendOwnedData( this.ownerId, RadarControl( RadarControlOperation.RemoveAll, RadarControlType.Normal, 0, 0, 0 ) )
        PacketDispatcher.sendOwnedData( this.ownerId, RadarControl( RadarControlOperation.Add, RadarControlType.Normal, x, y, z ) )
    }

    addNpcMarker( x: number, y: number, z: number ) {
        let marker : RadarMarker = {
            type: RadarControlType.Npc,
            x,
            y,
            z
        }

        this.markers.push( marker )

        PacketDispatcher.sendOwnedData( this.ownerId, RadarControl( RadarControlOperation.RemoveAll, RadarControlType.Npc, 0, 0, 0 ) )
        PacketDispatcher.sendOwnedData( this.ownerId, RadarControl( RadarControlOperation.Add, RadarControlType.Npc, x, y, z ) )
    }

    removeAllMarkers() {
        // TODO : find out if each radar item needs to be removed
        // TODO : create cached radar packets if all radar removal works
        // let ownerId = this.ownerId
        // this.markers.forEach( ( marker: RadarMarker ) => {
        //     PacketDispatcher.sendOwnedData( ownerId, RadarControl( RadarControlOperation.RemoveAll, 2, marker.x, marker.y, marker.z ) )
        // } )

        PacketDispatcher.sendOwnedData( this.ownerId, RadarControl( RadarControlOperation.RemoveAll, RadarControlType.Normal, 0, 0, 0 ) )
        PacketDispatcher.sendOwnedData( this.ownerId, RadarControl( RadarControlOperation.RemoveAll, RadarControlType.Npc, 0, 0, 0 ) )

        this.markers = []
    }

    removeMarker( x: number, y: number, z: number ) {
        let marker : RadarMarker = this.markers.find( ( marker: RadarMarker ) => {
            return marker.x === x && marker.y === y && marker.z === z
        } )

        if ( !marker ) {
            return
        }

        _.pull( this.markers, marker )
        PacketDispatcher.sendOwnedData( this.ownerId, RadarControl( RadarControlOperation.Remove, marker.type, x, y, z ) )
    }
}

export interface RadarMarker {
    type: RadarControlType
    x: number
    y: number
    z: number
}