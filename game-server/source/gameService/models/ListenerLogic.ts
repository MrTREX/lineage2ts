import { QuestStateValues } from './quest/State'
import { L2PcInstance } from './actor/instance/L2PcInstance'
import { QuestState } from './quest/QuestState'
import { NpcHtmlMessage, NpcHtmlMessagePath } from '../packets/send/NpcHtmlMessage'
import { ActionFailed } from '../packets/send/ActionFailed'
import { DataManager } from '../../data/manager'
import { NpcQuestHtmlMessage } from '../packets/send/NpcQuestHtmlMessage'
import { L2World } from '../L2World'
import { ListenerRegisterType } from '../enums/ListenerRegisterType'
import {
    AttackableAttackedEvent,
    AttackableKillEvent,
    CharacterKilledEvent,
    CharacterStoppedMovingEvent,
    EventTerminationResult,
    EventType,
    EventTypeData,
    ItemTalkingEvent,
    NpcApproachedForTalkEvent,
    NpcGeneralEvent,
    NpcReceivingEvent,
    NpcSeePlayerEvent,
    NpcSpawnEvent,
    NpcTalkEvent,
    PlayerSummonSpawnEvent,
} from './events/EventType'
import { L2Object } from './L2Object'
import { QuestHelper } from '../../listeners/helpers/QuestHelper'
import { NotifiableEvent } from './events/NotifiableEvent'
import { L2Party } from './L2Party'
import { QuestStateCache } from '../cache/QuestStateCache'
import { ErrorHelper } from '../helpers/ErrorHelper'
import { ILocational } from './Location'
import { EventPoolCache } from '../cache/EventPoolCache'
import to from 'await-to-js'
import _ from 'lodash'
import { EventListenerMethod, EventListenerTerminatedMethod } from './events/listeners/EventListenerData'
import { ObjectPool } from '../helpers/ObjectPoolHelper'
import Timeout = NodeJS.Timeout

const questTimerObjectPool = new ObjectPool( 'QuestTimer', (): QuestTimer => {
    return {
        name: undefined,
        npcObjectId: 0,
        playerObjectId: 0,
        shouldRepeat: false,
        timer: undefined,
        timerName: undefined
    }
} )

export interface ListenerDescriptionTriggers {
    targetIds?: Set<number>
}

export interface ListenerDescription {
    registerType: ListenerRegisterType
    eventType: EventType
    method: EventListenerMethod
    ids: Array<number>
    triggers?: ListenerDescriptionTriggers
}

export interface QuestTimer {
    timer: Timeout
    npcObjectId: number
    playerObjectId: number
    name: string
    shouldRepeat: boolean
    timerName: string
}

export class ListenerLogic implements NotifiableEvent {
    questId: number = -1
    name: string
    path: string
    questItemIds: Array<number>
    timers: { [ name: string ]: QuestTimer } = {}

    constructor( name: string, path: string ) {
        this.name = name
        this.path = path
    }

    canStartQuest( player: L2PcInstance ): boolean {
        return true
    }

    checkPartyMember( state: QuestState, target: L2Object ): boolean {
        return true
    }

    checkPartyMemberConditions( state: QuestState, conditionValue: number, target: L2Object ): boolean {
        return state && ( ( conditionValue === -1 ) ? state.isStarted() : state.isCondition( conditionValue ) ) && this.checkPartyMember( state, target )
    }

    checkPartyMemberForLocation( location: ILocational, playerId: number ): boolean {
        return true
    }

    createTimerName( name: string, npcObjectId: number = 0, playerObjectId: number = 0 ): string {
        return `${ name }:${ npcObjectId }:${ playerObjectId }`
    }

    getAttackableAttackIds(): Array<number> {
        return
    }

    getAttackableKillIds(): Array<number> {
        return
    }

    getCreatureKillIds(): Array<number> {
        return
    }

    getCustomListeners(): Array<ListenerDescription> {
        return
    }

    // TODO : add minimum level message via additional method getMinimumLevel() (requires adjustment of all listeners)
    // Example : Quest Name - from 17 level
    getDescription(): string {
        return DataManager.getQuestData().getQuestName( this.questId )
    }

    executeEventMethod( type: EventType, parameters: EventTypeData ) : Promise<void | string> {
        switch ( type ) {
            case EventType.NpcEvent:
                return this.onNpcEvent( parameters as NpcGeneralEvent )

            case EventType.NpcApproachedForTalk:
                return this.onApproachedForTalk( parameters as NpcApproachedForTalkEvent )

            case EventType.NpcSpawn:
                return this.onSpawnEvent( parameters as NpcSpawnEvent )
        }

        return Promise.resolve()
    }

    getApproachedForTalkIds(): Array<number> {
        return
    }

    getId() {
        return this.questId
    }

    getName() {
        return this.name
    }

    getNpcSeePlayerIds(): Array<number> {
        return
    }

    getPath( name: string ): string {
        return `${ this.getPathPrefix() }/${ name }`
    }

    getPathPrefix() {
        return ''
    }

    getQuestStartIds(): Array<number> {
        return
    }

    getQuestState( playerId: number, createState: boolean = false ): QuestState {
        return QuestStateCache.getQuestState( playerId, this.getName(), createState )
    }

    getQuestTimer( name: string, npcObjectId: number = 0, playerObjectId: number = 0 ): QuestTimer {
        return this.timers[ this.createTimerName( name, npcObjectId, playerObjectId ) ]
    }

    getRandomPartyMember( player: L2PcInstance ): L2PcInstance {
        if ( !player ) {
            return
        }

        let party: L2Party = player.getParty()
        if ( !party || party.getMembers().length <= 1 ) {
            return player
        }

        return L2World.getPlayer( _.sample( party.getMembers() ) )
    }

    getRandomPartyMemberForCondition( player: L2PcInstance, value: number ): L2PcInstance {
        if ( !player ) {
            return null
        }

        let party: L2Party = player.getParty()
        let questName = this.getName()
        if ( !party || party.getMembers().length === 1 ) {
            let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), questName, false )
            if ( state && state.getCondition() === value ) {
                return player
            }

            return null
        }

        let target: L2Object = player.getTarget()
        if ( !target ) {
            target = player
        }

        let possiblePlayers: Array<L2PcInstance> = party.getMembers().reduce( ( allPlayers: Array<L2PcInstance>, playerId: number ) => {
            let otherPlayer = L2World.getPlayer( playerId )
            let state = QuestStateCache.getQuestState( playerId, questName, false )

            if ( otherPlayer
                    && state
                    && state.getCondition() === value
                    && otherPlayer.isInsideRadius( target, 1500, true ) ) {
                allPlayers.push( otherPlayer )
            }

            return allPlayers
        }, [] )

        return _.sample( possiblePlayers )
    }

    getRandomPartyMemberStateForCondition( player: L2PcInstance, value: number ): QuestState {
        if ( !player ) {
            return null
        }

        let party: L2Party = player.getParty()
        let questName = this.getName()
        if ( !party || party.getMembers().length === 1 ) {
            let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), questName, false )
            if ( state && state.getCondition() === value ) {
                return state
            }

            return
        }

        let target: L2Object = player.getTarget()
        if ( !target ) {
            target = player
        }

        let possibleStates: Array<QuestState> = party.getMembers().reduce( ( allStates: Array<QuestState>, playerId: number ) => {
            let otherPlayer = L2World.getPlayer( playerId )
            let state = QuestStateCache.getQuestState( playerId, questName, false )

            if ( otherPlayer
                    && state
                    && state.getCondition() === value
                    && otherPlayer.isInsideRadius( target, 1500, true ) ) {
                allStates.push( state )
            }

            return allStates
        }, [] )

        return _.sample( possibleStates )
    }

    getRandomPartyMemberPerStateType( player: L2PcInstance, value: QuestStateValues ): L2PcInstance {
        if ( !player ) {
            return null
        }

        let party: L2Party = player.getParty()
        let questName = this.getName()
        if ( !party || party.getMembers().length === 1 ) {
            let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), questName, false )
            if ( state && state.getState() === value ) {
                return player
            }

            return null
        }

        let target: L2Object = player.getTarget()
        if ( !target ) {
            target = player
        }

        let possiblePlayers: Array<L2PcInstance> = party.getMembers().reduce( ( allPlayers: Array<L2PcInstance>, playerId: number ) => {
            let otherPlayer = L2World.getPlayer( playerId )
            let state = QuestStateCache.getQuestState( playerId, questName, false )

            if ( state && state.getState() === value && otherPlayer.isInsideRadius( target, 1500, true ) ) {
                allPlayers.push( otherPlayer )
            }

            return allPlayers
        }, [] )

        return _.sample( possiblePlayers )
    }

    getRandomPartyMemberForVariable( player: L2PcInstance, variableName: string, value: any ): L2PcInstance {
        if ( !player ) {
            return
        }

        if ( !variableName || variableName === '' ) {
            return this.getRandomPartyMember( player )
        }

        let party: L2Party = player.getParty()
        if ( !party || party.getMembers().length === 0 ) {
            let state = QuestStateCache.getQuestState( player.getObjectId(), this.getName() )
            if ( state && state.getVariable( variableName ) === value ) {
                return player
            }

            return
        }

        let target: L2Object = player.getTarget()
        if ( !target ) {
            target = player
        }

        let questName = this.getName()
        let possiblePlayers = party.getMembers().reduce( ( allPlayers: Array<L2PcInstance>, playerId: number ) => {
            let currentPlayer = L2World.getPlayer( playerId )
            let currentState = QuestStateCache.getQuestState( playerId, questName )
            if ( currentPlayer
                    && currentState
                    && currentState.getVariable( variableName ) === value
                    && currentPlayer.isInsideRadius( target, 1500, true ) ) {
                allPlayers.push( currentPlayer )
            }

            return allPlayers
        }, [] )

        return _.sample( possiblePlayers )
    }

    getRandomPartyMemberNearLocation( player: L2PcInstance, location: ILocational ): L2PcInstance {
        if ( !player || !location ) {
            return null
        }

        let party: L2Party = player.getParty()
        if ( !party || party.getMembers().length === 0 ) {
            return player
        }

        let chosenPlayerId = _.sample( party.getMembers().filter( ( playerId: number ) => this.checkPartyMemberForLocation( location, playerId ) ) )

        if ( !chosenPlayerId ) {
            return null
        }

        return L2World.getPlayer( chosenPlayerId )
    }

    getRandomPartyMemberState( player: L2PcInstance, conditionValue: number, playerChance: number, target: L2Object ): QuestState {
        if ( !player || playerChance < 1 ) {
            return null
        }

        let questState: QuestState = QuestStateCache.getQuestState( player.getObjectId(), this.getName() )
        if ( !player.isInParty() ) {
            if ( !questState || !this.checkPartyMemberConditions( questState, conditionValue, target )
                || !QuestHelper.checkDistanceToTarget( player, target ) ) {
                return null
            }

            return questState
        }

        let possibleStates: Array<QuestState> = QuestHelper.checkDistanceToTarget( player, target ) ? _.times( playerChance, _.constant( questState ) ) : []

        player.getParty().getMembers().forEach( ( playerId: number ) => {
            if ( playerId === player.getObjectId() ) {
                return
            }

            let currentState = QuestStateCache.getQuestState( playerId, this.getName() )
            if ( currentState
                    && this.checkPartyMemberConditions( currentState, conditionValue, target )
                    && QuestHelper.checkDistanceToTarget( L2World.getPlayer( playerId ), target ) ) {
                possibleStates.push( currentState )
            }
        } )

        return _.sample( possibleStates )
    }

    getRandomPartyMemberPerCondition( player: L2PcInstance, conditionValue: number, playerChance: number, target: L2Object ): L2PcInstance {
        if ( !player || playerChance < 1 ) {
            return null
        }

        let questState: QuestState = QuestStateCache.getQuestState( player.getObjectId(), this.getName() )
        if ( !player.isInParty() ) {
            if ( !questState || !this.checkPartyMemberConditions( questState, conditionValue, target ) || !QuestHelper.checkDistanceToTarget( player, target ) ) {
                return null
            }

            return player
        }

        let possiblePlayers: Array<L2PcInstance> = QuestHelper.checkDistanceToTarget( player, target ) ? _.times( playerChance, _.constant( player ) ) : []

        player.getParty().getMembers().forEach( ( playerId: number ) => {
            if ( playerId === player.getObjectId() ) {
                return
            }

            let currentState = QuestStateCache.getQuestState( playerId, this.getName() )
            if ( !currentState ) {
                return
            }

            let otherPlayer = L2World.getPlayer( playerId )
            if ( this.checkPartyMemberConditions( currentState, conditionValue, target )
                    && QuestHelper.checkDistanceToTarget( otherPlayer, target ) ) {
                possiblePlayers.push( otherPlayer )
            }
        } )

        return _.sample( possiblePlayers )
    }

    getResetHour() {
        return 6
    }

    getResetMinutes() {
        return 30
    }

    getSpawnIds(): Array<number> {
        return
    }

    getSummonSpawnIds(): Array<number> {
        return
    }

    getTalkIds(): Array<number> {
        return
    }

    async initialize(): Promise<void> {
    }

    invokeEvent( playerId: number, originatorId: number, type: EventType, parameters: EventTypeData ): Promise<void> {
        return this.notifyEventPromise( playerId, originatorId, this.executeEventMethod( type, parameters ) )
    }

    isVisibleInQuestWindow() {
        return this.questId > 0 && this.questId < 20000
    }

    async load(): Promise<void> {
    }

    async notifyEventWithTermination( playerId: number, originatorId: number, method: EventListenerTerminatedMethod, parameters: EventTypeData ): Promise<EventTerminationResult> {
        let [ error, terminationResult ] = await to( method( parameters ) )

        if ( error ) {
            this.showError( playerId, error )
            return null
        }

        if ( terminationResult?.message ) {
            this.showHtmlResult( terminationResult.message, playerId, originatorId )

        }

        return terminationResult
    }

    async onAttackableAttackedEvent( data: AttackableAttackedEvent ): Promise<void> {
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        return
    }

    async onCreatureKillEvent( data: CharacterKilledEvent ): Promise<void> {
    }

    async onApproachedForTalk( data: NpcApproachedForTalkEvent ): Promise<string> {
        return this.getPath( `${ data.characterNpcId }.html` )
    }

    async notifyEventPromise( playerId: number, originatorId: number, outcome: Promise<string | void > ): Promise<void> {
        let [ error, message ] = await to( outcome )

        if ( error ) {
            return this.showError( playerId, error )
        }

        return this.showHtmlResult( message as string, playerId, originatorId )
    }

    startApproachedForTalkEvent( data: NpcApproachedForTalkEvent ) : Promise<void> {
        return this.notifyEventPromise( data.playerId, data.characterId, this.onApproachedForTalk( data ) )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        return
    }

    async onNpcEventReceived( data: NpcReceivingEvent ): Promise<void> {
    }

    async onNpcStoppedMoving( data: CharacterStoppedMovingEvent ): Promise<void> {
    }

    async onNpcSeePlayerEvent( data: NpcSeePlayerEvent ): Promise<void> {
    }

    async onQuestTimerFired( timer: QuestTimer ): Promise<void> {
        if ( !timer.shouldRepeat ) {
            this.deleteQuestTimerData( timer )
        }

        let eventData = EventPoolCache.getData( EventType.NpcEvent ) as NpcGeneralEvent

        eventData.eventName = timer.name
        eventData.characterId = timer.npcObjectId
        eventData.characterNpcId = 0
        eventData.playerId = timer.playerObjectId
        eventData.isTimer = true

        if ( !eventData.playerId ) {
            await this.onNpcEvent( eventData )
        } else {
            await this.notifyEventPromise( eventData.playerId, eventData.characterId, this.onNpcEvent( eventData ) )
        }

        EventPoolCache.recycleData( EventType.NpcEvent, eventData )
    }

    async onSpawnEvent( data: NpcSpawnEvent ): Promise<void> {
    }

    async onSummonSpawnEvent( data: PlayerSummonSpawnEvent ): Promise<void> {
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        return
    }

    removeQuestTimer( timerData: QuestTimer ) {
        if ( timerData && this.timers[ timerData.timerName ] === timerData ) {
            this.stopIndividualTimer( timerData )
            this.deleteQuestTimerData( timerData )
        }
    }

    private deleteQuestTimerData( timerData: QuestTimer ): void {
        delete this.timers[ timerData.timerName ]
        questTimerObjectPool.recycleValue( timerData )
    }

    removeRegisteredQuestItems( player: L2PcInstance ): Promise<void> {
        if ( this.questItemIds ) {
            return QuestHelper.takeMultipleItems( player, -1, ...this.questItemIds )
        }
    }

    shouldShowQuestWindow(): boolean {
        let questId = this.getId()
        return questId > 0 && questId < 20000 && questId !== 999
    }

    showError( playerId: number, error: any ): void {
        let player: L2PcInstance = L2World.getPlayer( playerId )
        if ( player && player.isGM() ) {
            let prefix: string = `Quest <font color="LEVEL">${ this.getName() }</font> failed:`
            return this.showHtmlResult( ErrorHelper.formatError( prefix, error ), playerId )
        }
    }

    showHtmlPath( path: string, player: L2PcInstance, npcObjectId: number ): void {
        if ( !DataManager.getHtmlData().hasItem( path ) ) {
            return
        }

        let html = DataManager.getHtmlData().getItem( path )

        if ( this.shouldShowQuestWindow() && path.endsWith( '.htm' ) && path !== QuestHelper.getNoQuestMessagePath() ) {
            player.sendOwnedData( NpcQuestHtmlMessage( html, path, player.getObjectId(), npcObjectId, this.getId() ) )
            return player.sendOwnedData( ActionFailed() )
        }

        player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), npcObjectId ) )
        return player.sendOwnedData( ActionFailed() )
    }

    showHtmlResult( message: string, playerId: number, npcObjectId: number = 0 ): void {

        if ( !message || typeof message !== 'string' ) {
            return
        }

        let player = L2World.getPlayer( playerId )
        if ( !player ) {
            return
        }

        if ( message.endsWith( '.htm' ) || message.endsWith( '.html' ) ) {
            return this.showHtmlPath( message, player, npcObjectId )
        }

        if ( message.startsWith( '<html' ) ) {
            player.sendOwnedData( NpcHtmlMessage( message, playerId, npcObjectId ) )
            return player.sendOwnedData( ActionFailed() )
        }

        player.sendMessage( message )
        return player.sendOwnedData( ActionFailed() )
    }

    async startOnTalkEvent( data: NpcTalkEvent ): Promise<void> {
        let player: L2PcInstance = L2World.getPlayer( data.playerId )

        if ( !player ) {
            return
        }

        let [ error, message ] = await to( this.onTalkEvent( data ) )

        if ( error ) {
            return this.showError( data.playerId, error )
        }

        player.setLastQuestNpc( data.characterId )
        this.showHtmlResult( message, data.playerId, data.characterId )
    }

    startQuestTimer( name: string, time: number, npcObjectId: number = 0, playerObjectId: number = 0, shouldRepeat: boolean = false ): void {
        let timerName: string = this.createTimerName( name, npcObjectId, playerObjectId )
        if ( this.timers[ timerName ] ) {
            return
        }

        let timerData = questTimerObjectPool.getValue()

        timerData.npcObjectId = npcObjectId
        timerData.playerObjectId = playerObjectId
        timerData.name = name
        timerData.shouldRepeat = shouldRepeat
        timerData.timerName = timerName


        let method: Function = shouldRepeat ? setInterval : setTimeout
        timerData.timer = method( this.onQuestTimerFired.bind( this ), time, timerData )

        this.timers[ timerData.timerName ] = timerData
    }

    hasQuestTimer( name: string, npcObjectId: number = 0, playerObjectId: number = 0 ): boolean {
        let timerName: string = this.createTimerName( name, npcObjectId, playerObjectId )
        return !!this.timers[ timerName ]
    }

    stopIndividualTimer( timerData: QuestTimer ): void {
        if ( timerData.shouldRepeat ) {
            return clearInterval( timerData.timer )
        }

        return clearTimeout( timerData.timer )
    }

    stopQuestTimer( name: string, npcObjectId: number = 0, playerObjectId: number = 0 ): void {
        this.removeQuestTimer( this.getQuestTimer( name, npcObjectId, playerObjectId ) )
    }

    stopQuestTimers( name: string ): void {
        let timerData: QuestTimer = _.find( this.timers, ( data: QuestTimer ) => {
            return data.name === name
        } )

        this.removeQuestTimer( timerData )
    }

    stopQuestTimersForIds( npcObjectId: number, playerObjectId: number, timerNames: Array<string> ): void {
        let listener = this
        _.each( timerNames, ( name: string ) => {
            listener.stopQuestTimer( name, npcObjectId, playerObjectId )
        } )
    }

    async onItemTalking( data: ItemTalkingEvent ): Promise<string> {
        return
    }
}