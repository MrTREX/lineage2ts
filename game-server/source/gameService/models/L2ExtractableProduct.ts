export interface L2ExtractableProduct {
    itemId: number
    min: number
    max: number
    chance: number
}