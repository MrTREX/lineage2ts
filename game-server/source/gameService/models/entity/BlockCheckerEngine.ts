import { ArenaParticipantsHolder } from '../ArenaParticipantsHolder'
import { L2PcInstance } from '../actor/instance/L2PcInstance'
import { L2ItemInstance } from '../items/instance/L2ItemInstance'
import _ from 'lodash'
import { ISpawnLogic } from '../spawns/ISpawnLogic'

export class BlockCheckerEngine {
    // The object which holds all basic members info
    holder: ArenaParticipantsHolder
    // Maps to hold player of each team and his points
    redTeamPoints: { [ key: number ]: number } = {}
    blueTeamPoints: { [ key: number ]: number } = {}
    // The initial points of the event
    redPoints: number = 15
    bluePoints: number = 15
    // Current used arena
    arena: number = -1
    // All blocks
    spawns: Array<ISpawnLogic>
    // Sets if the red team won the event at the end of this (used for packets)
    isRedWinner: boolean
    // Time when the event starts. Used on packet sending
    startedTime: number
    // The needed arena coordinates
    // Arena X: team1X, team1Y, team2X, team2Y, ArenaCenterX, ArenaCenterY
    arenaCoordinates: Array<Array<number>> = [
        // Arena 0 - Team 1 XY, Team 2 XY - CENTER XY
        [
            -58368,
            -62745,
            -57751,
            -62131,
            -58053,
            -62417
        ],
        // Arena 1 - Team 1 XY, Team 2 XY - CENTER XY
        [
            -58350,
            -63853,
            -57756,
            -63266,
            -58053,
            -63551
        ],
        // Arena 2 - Team 1 XY, Team 2 XY - CENTER XY
        [
            -57194,
            -63861,
            -56580,
            -63249,
            -56886,
            -63551
        ],
        // Arena 3 - Team 1 XY, Team 2 XY - CENTER XY
        [
            -57200,
            -62727,
            -56584,
            -62115,
            -56850,
            -62391
        ]
    ]
    // Common z coordinate
    zCoord: number = -2405
    // List of dropped items in event (for later deletion)
    drops: Array<L2ItemInstance>
    // Event is started
    isStarted: boolean = false
    // Event end
    task: any
    // Preserve from exploit reward by logging out
    abnormalEnd: boolean = false

    addNewDrop( item: L2ItemInstance ) {
        if ( item ) {
            this.drops.push( item )
        }
    }

    getBluePoints() {
        return this.bluePoints
    }

    getHolder() {
        return this.holder
    }

    getPlayerPoints( player: L2PcInstance, isRed: boolean ) {
        if ( isRed ) {
            return _.get( this.redTeamPoints, player.getObjectId(), 0 )
        }

        return _.get( this.blueTeamPoints, player.getObjectId(), 0 )
    }

    getRedPoints() {
        return this.redPoints
    }

    getStartedTime() {
        return this.startedTime
    }

    increasePlayerPoints( player: L2PcInstance, team: number ) {
        if ( !player ) {
            return
        }

        let objectId = player.getObjectId()
        if ( team === 0 ) {
            this.redTeamPoints[ objectId ] = _.get( this.redTeamPoints, objectId, 0 ) + 1
            this.redPoints++
            this.bluePoints--
            return
        }

        this.blueTeamPoints[ objectId ] = _.get( this.blueTeamPoints, objectId, 0 ) + 1
        this.bluePoints++
        this.redPoints--
    }

    endEventAbnormally() {
        this.isStarted = false

        if ( this.task ) {
            this.task.cancel()
        }

        this.abnormalEnd = true

        // TODO : figure out EndEvent stuff
    }
}