import { L2DoorInstance } from '../actor/instance/L2DoorInstance'
import { ConfigManager } from '../../../config/ConfigManager'
import { Location } from '../Location'
import { InstanceReenterType } from '../../enums/InstanceReenterType'
import { InstanceReenterTime } from '../../helpers/InstanceReenterHelper'
import { InstanceRemoveBuffType } from '../../enums/InstanceRemoveBuffType'
import { L2Npc } from '../actor/L2Npc'
import { L2World } from '../../L2World'
import { TeleportWhereType } from '../../enums/TeleportWhereType'
import { L2PcInstance } from '../actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { CreatureSay } from '../../packets/send/builder/CreatureSay'
import { IBuilderPacket } from '../../packets/IBuilderPacket'
import { InstanceManager } from '../../instancemanager/InstanceManager'
import { L2Character } from '../actor/L2Character'
import { InstanceWorld } from '../instancezone/InstanceWorld'
import _ from 'lodash'
import { ISpawnLogic } from '../spawns/ISpawnLogic'
import Timeout = NodeJS.Timeout
import { NpcSayType } from '../../enums/packets/NpcSayType'

export class Instance {
    id: number
    name: string
    ejectTime: number = ConfigManager.general.getEjectDeadPlayerTime()

    /** Allow random walk for NPCs, global parameter. */
    allowRandomWalk: boolean = true

    // object ids
    players: Array<number> = []
    npcs: Array<number> = []
    doors: { [ key: number ]: number } = {} // doorId to objectId map
    manualSpawn: { [ key: string ]: Array<ISpawnLogic> } = {}
    generalSpawns: Array<ISpawnLogic> = []

    enterLocations: Array<Location> = []
    exitLocation: Location
    allowSummon: boolean = true
    emptyDestroyTime: number = -1
    lastLeft: number = -1
    instanceStartTime: number = -1
    instanceEndTime: number = -1

    isPvPInstance: boolean = false
    showTimer: boolean = false
    isTimerIncreaseValue: boolean = true

    timerText: string = ''

    type: InstanceReenterType = InstanceReenterType.NONE
    resetData: Array<InstanceReenterTime> = []

    removeBuffType: InstanceRemoveBuffType = InstanceRemoveBuffType.NONE
    exceptionList: Array<number> = []

    checkTimeUpTask: Timeout
    ejectDeadTasks: { [ key: number ]: Timeout }

    constructor( id: number, name: string ) {
        this.id = id
        this.name = name
        this.instanceStartTime = Date.now()
    }

    addEjectDeadTask( player: L2PcInstance ) {
        if ( !player ) {
            return
        }

        this.ejectDeadTasks[ player.getObjectId() ] = setTimeout( this.runEjectDeadTask.bind( this ), this.ejectTime, player.getObjectId() )
    }

    addNpc( objectId: number ): void {
        this.npcs.push( objectId )
    }

    addPlayer( objectId: number ): void {
        this.players.push( objectId )
    }

    cancelEjectDeadPlayer( player: L2PcInstance ) {
        let task: Timeout = this.ejectDeadTasks[ player.getObjectId() ]
        if ( task ) {
            clearTimeout( task )
            _.unset( this.ejectDeadTasks, player.getObjectId() )
        }
    }

    cancelTimer() {
        if ( this.checkTimeUpTask ) {
            clearTimeout( this.checkTimeUpTask )
        }

        this.checkTimeUpTask = null
    }

    containsPlayer( objectId: number ): boolean {
        return this.players.includes( objectId )
    }

    getCheckTimeMessage( duration: number ): [ IBuilderPacket, number, number ] {
        if ( this.players.length === 0 && this.emptyDestroyTime === 0 ) {
            return [ null, 0, 500 ]
        }

        if ( this.players.length === 0 && this.emptyDestroyTime > 0 ) {

            let emptyTimeLeft = ( this.lastLeft + this.emptyDestroyTime ) - Date.now()
            if ( emptyTimeLeft <= 0 ) {
                return [ null, 0, 0 ]
            }

            if ( ( duration > 300000 ) && ( emptyTimeLeft > 300000 ) ) {
                return [ null, duration - 300000, 300000 ]
            }

            if ( ( duration > 60000 ) && ( emptyTimeLeft > 60000 ) ) {
                return [ null, duration - 60000, 60000 ]
            }

            if ( ( duration > 30000 ) && ( emptyTimeLeft > 30000 ) ) {
                return [ null, duration - 30000, 30000 ]
            }
            return [ null, duration - 10000, 10000 ]
        }

        if ( duration > 300000 ) {
            let timeLeft = Math.floor( duration / 60000 )

            let packet = new SystemMessageBuilder( SystemMessageIds.DUNGEON_EXPIRES_IN_S1_MINUTES )
                    .addString( timeLeft.toString() )

            return [ packet, duration - 300000, 300000 ]
        }

        if ( duration > 60000 ) {
            let timeLeft = Math.floor( duration / 60000 )

            let packet = new SystemMessageBuilder( SystemMessageIds.DUNGEON_EXPIRES_IN_S1_MINUTES )
                    .addString( timeLeft.toString() )

            return [ packet, duration - 60000, 60000 ]
        }

        if ( duration > 30000 ) {
            let timeLeft = Math.floor( duration / 1000 )
            let packet = CreatureSay.fromParameters( 0, NpcSayType.Alliance, 'Notice', `${ timeLeft } seconds left.` )

            return [ packet, duration - 30000, 30000 ]
        }

        let timeLeft = Math.floor( duration / 1000 )
        let packet = CreatureSay.fromParameters( 0, NpcSayType.Alliance, 'Notice', `${ timeLeft } seconds left.` )
        return [ packet, duration - 10000, 10000 ]
    }

    getDoor( id: number ): L2DoorInstance {
        return L2World.getObjectById( this.doors[ id ] ) as L2DoorInstance
    }

    getDoors(): Array<L2DoorInstance> {
        return _.map( this.doors, ( doorId: number ) => {
            return L2World.getObjectById( doorId ) as L2DoorInstance
        } )
    }

    getEjectTime() {
        return this.ejectTime
    }

    getEnterLocations(): Array<Location> {
        return this.enterLocations
    }

    getExitLocation(): Location {
        return this.exitLocation
    }

    getId(): number {
        return this.id
    }

    getInstanceEndTime(): number {
        return this.instanceEndTime
    }

    getInstanceStartTime(): number {
        return this.instanceStartTime
    }

    getNpcs(): Array<number> {
        return this.npcs
    }

    getTimerText(): string {
        return this.timerText
    }

    isShowTimer(): boolean {
        return this.showTimer
    }

    isSummonAllowed() {
        return this.allowSummon
    }

    isTimerIncrease(): boolean {
        return this.isTimerIncreaseValue
    }

    notifyDeath( character: L2Character ) {
        let instance: InstanceWorld = InstanceManager.getPlayerWorld( character.getActingPlayer().getObjectId() )
        if ( instance ) {
            instance.onDeath( character )
        }
    }

    removeDoors() {
        _.each( this.doors, ( currentDoorId: number ) => {
            let door: L2DoorInstance = L2World.getObjectById( currentDoorId ) as L2DoorInstance
            if ( door ) {
                door.decayMe()
                L2World.removeObject( door )
            }
        } )
    }

    removeNpc( npc: L2Npc ) {
        _.pull( this.npcs, npc.getObjectId() )
    }

    removeNpcs() {
        _.each( this.npcs, ( objectId: number ) => {
            let currentNpc = L2World.getObjectById( objectId ) as L2Npc
            if ( currentNpc ) {
                let spawn = currentNpc.getNpcSpawn()
                if ( spawn ) {
                    spawn.stopRespawn()
                }

                currentNpc.deleteMe()
            }
        } )

        this.npcs = []
        this.manualSpawn = {}
    }

    removePlayer( objectId: number ) {
        _.pull( this.players, objectId )
        if ( _.isEmpty( this.players ) && ( this.emptyDestroyTime >= 0 ) ) {
            this.lastLeft = Date.now()
            this.setDuration( this.instanceEndTime - Date.now() - 500 )
        }
    }

    removePlayers(): void {
        _.each( this.players, ( playerObjectId: number ) => {
            let currentPlayer = L2World.getPlayer( playerObjectId )

            if ( currentPlayer && currentPlayer.getInstanceId() === this.getId() ) {
                currentPlayer.setInstanceId( 0 )

                if ( this.getExitLocation() ) {
                    currentPlayer.teleportToLocation( this.getExitLocation(), true )
                } else {
                    currentPlayer.teleportToLocationType( TeleportWhereType.TOWN )
                }
            }
        } )

        this.players = []
    }

    runCheckTimeUpTask( duration: number ) {
        let [ packet, remaining, interval ]: [ IBuilderPacket, number, number ] = this.getCheckTimeMessage( duration )

        if ( packet ) {
            let currentInstanceId = this.getId()
            _.each( this.players, ( objectId: number ) => {
                let currentPlayer: L2PcInstance = L2World.getPlayer( objectId )
                if ( currentPlayer && currentPlayer.getInstanceId() === currentInstanceId ) {
                    currentPlayer.sendOwnedData( packet.getBuffer( objectId ) )
                }
            } )
        }

        this.cancelTimer()
        if ( remaining >= 10000 ) {
            this.checkTimeUpTask = setTimeout( this.runCheckTimeUpTask.bind( this ), interval, remaining )
        } else {
            this.checkTimeUpTask = setTimeout( this.runTimeUpTask.bind( this ), interval )
        }
    }

    runEjectDeadTask( playerId: number ): void {
        let player: L2PcInstance = L2World.getPlayer( playerId )
        if ( !player || !player.isDead() || player.getInstanceId() !== this.getId() ) {
            return
        }

        player.setInstanceId( 0 )
        let location = this.getExitLocation()
        if ( location ) {
            player.teleportToLocation( location, true )
        }

        player.teleportToLocationType( TeleportWhereType.TOWN )
    }

    runTimeUpTask() {
        InstanceManager.destroyInstance( this.getId() )
    }

    setDuration( duration: number ) {
        this.cancelTimer()

        this.checkTimeUpTask = setTimeout( this.runCheckTimeUpTask.bind( this ), 500, duration )
        this.instanceEndTime = Date.now() + duration + 500
    }

    shouldRemoveBuffs() {
        return this.removeBuffType !== InstanceRemoveBuffType.NONE
    }
}