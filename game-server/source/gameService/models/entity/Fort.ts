import { L2Clan } from '../L2Clan'
import { L2StaticObjectInstance } from '../actor/instance/L2StaticObjectInstance'
import { CastleManager } from '../../instancemanager/CastleManager'
import { FortSiege } from './FortSiege'
import { AbstractResidence } from './AbstractResidence'
import { L2World } from '../../L2World'
import { ConfigManager } from '../../../config/ConfigManager'
import { DatabaseManager } from '../../../database/manager'
import { PledgeShowInfoUpdate } from '../../packets/send/PledgeShowInfoUpdate'
import { ClanCache } from '../../cache/ClanCache'
import { Castle } from './Castle'
import { L2DoorInstance } from '../actor/instance/L2DoorInstance'
import { L2FortDoorUpgradeTableData } from '../../../database/interface/FortDoorUpgradeTableApi'
import { ItemTypes } from '../../values/InventoryValues'
import { DataManager } from '../../../data/manager'
import { L2FortFunctionsTableData } from '../../../database/interface/FortFunctionsTableApi'
import { FortManager } from '../../instancemanager/FortManager'
import { L2PcInstance } from '../actor/instance/L2PcInstance'
import _ from 'lodash'
import aigle from 'aigle'
import { L2StaticObjectType } from '../../enums/L2StaticObjectType'
import { FortState } from '../../enums/FortState'
import { FortFeatureType } from '../../enums/FortFeatureType'
import { DoorManager } from '../../cache/DoorManager'
import { L2FortTableData } from '../../../database/interface/FortTableApi'
import { SpawnEventManager } from '../spawns/SpawnEventManager'
import { FortEvent } from '../../enums/FortEvent'
import { FortSiegeArea } from '../areas/type/FortSiege'
import { FortArea } from '../areas/type/Fort'
import { AreaCache } from '../../cache/AreaCache'
import { AreaType } from '../areas/AreaType'
import { ResidenceTeleportType } from '../../enums/ResidenceTeleportType'
import { Location } from '../Location'
import { FameOperations } from './FameOperations'
import Timeout = NodeJS.Timeout
import { ResidenceType } from '../../enums/ResidenceType'

// TODO : add additional custom functions, similar to what exists on clan hall
export enum FortFunctionType {
    Teleport = 1,
    RestoreHp = 2,
    RestoreMp = 3,
    RestoreExp = 4,
    SupportBuffs = 5,
}

// TODO : refactor functions into separate system with debounced updates

export class Fort extends AbstractResidence implements FameOperations {
    // TODO : add door id associations when doors are spawned
    doors: Array<number> = []
    flagPole: L2StaticObjectInstance
    siege: FortSiege
    siegeArea: FortSiegeArea
    residenceArea: FortArea
    siegeAreaActive: boolean = false
    fortOwner: L2Clan
    // TODO : convert to proper map
    functions: { [ key: number ]: FortFunction } = {}
    updateTask: Timeout
    updateTaskStartTime: number = 0
    ownedTimeTask: Timeout
    runCount: number = 0

    envoyCastles: { [ key: number ]: number } = {}
    availableCastles: Array<number> = []
    isLoaded: boolean = false
    data: L2FortTableData

    constructor( data: L2FortTableData ) {
        super( data.id )

        this.name = data.name
        this.data = data
        this.type = ResidenceType.Fort

        this.siegeArea = AreaCache.getAreaByResidenceId( this.getResidenceId(), AreaType.FortSiege ) as FortSiegeArea
        if ( !this.siegeArea ) {
            throw new Error( `Fort with residenceId=${data.id} has no assigned siege area` )
        }

        this.residenceArea = AreaCache.getAreaByResidenceId( this.getResidenceId(), AreaType.Fort ) as FortArea
        if ( !this.residenceArea ) {
            throw new Error( `Fort with residenceId=${data.id} has no assigned residence area` )
        }
    }

    canGiveFame(): boolean {
        return true
    }
    getFameInterval(): number {
        return ConfigManager.character.getFortressFameInterval()
    }
    getFameAmount(): number {
        return ConfigManager.character.getFortressFameIntervalPoints()
    }

    isActivated() : boolean {
        return this.isLoaded
    }

    initialize( ownerId: number = this.data.ownerId ) : Promise<void> {
        this.startUpdateTasks( ownerId )
        this.loadFlagPole()
        this.initialSpawnEvents()
        return this.loadFunctions()
    }

    checkIfInZone( x: number, y: number, z: number ): boolean {
        return this.siegeArea.isInsideWithCoordinates( x, y, z )
    }

    getFlagPole() {
        return this.flagPole
    }

    getOwnerClan(): L2Clan {
        return this.fortOwner
    }

    getSiege(): FortSiege {
        if ( !this.siege ) {
            this.siege = new FortSiege( this )
        }

        return this.siege
    }

    isSiegeActive() : boolean {
        return this.siegeAreaActive
    }

    removeAllFunctions() : Promise<void> {
        // TODO : remove based on just residence id, no need to find out current function types
        return DatabaseManager.getFortFunctions().removeAll( this.getResidenceId(), _.keys( this.functions ).map( ( value: string ) => parseInt( value, 10 ) ) )
    }

    async removeOwner() {
        let clan: L2Clan = this.getOwnerClan()
        if ( clan ) {
            let fort = this
            await aigle.resolve( clan.getOnlineMembers( 0 ) ).each( async ( memberId: number ) : Promise<void> => {
                let member = L2World.getPlayer( memberId )
                await fort.removeResidentialSkills( member )
                member.sendSkillList()
            } )

            clan.setFortId( 0 )
            clan.broadcastDataToOnlineMembers( PledgeShowInfoUpdate( clan ) )
            this.setOwnerClan( null )
            this.setSupplyLevel( 0 )

            this.saveFortVariables()
            await this.removeAllFunctions()

            return
        }
    }

    saveFortVariables() {
        return FortManager.scheduleUpdate( this.residenceId )
    }

    setOwnerClan( clan: L2Clan ) {
        this.setVisibleFlag( !!clan )
        this.fortOwner = clan
    }

    setSupplyLevel( value: number ) {
        if ( value <= ConfigManager.fortress.getMaxSupplyLevel() ) {
            this.data.supplyLevel = value
        }
    }

    setVisibleFlag( value: boolean ) {
        let flagPole: L2StaticObjectInstance = this.getFlagPole()
        if ( flagPole ) {
            flagPole.setMeshIndex( value ? 1 : 0 )
        }
    }

    endOfSiege( clan: L2Clan ) : void {
        // TODO: implement me

        let eventId : FortEvent = this.getOwnerClan() === clan ? FortEvent.DefendedByOwner : FortEvent.SiegeEnded
        SpawnEventManager.triggerFortressEvent( this.residenceId, eventId )
    }

    getFunction( type: number ): FortFunction {
        return this.functions[ type ]
    }

    getOwnedTime() {
        if ( this.data.lastOwnedTime === 0 ) {
            return 0
        }

        return Math.floor( ( Date.now() - this.data.lastOwnedTime ) / 1000 )
    }

    getFortSize() {
        return this.getFortType() === 0 ? 3 : 5
    }

    getFortType() {
        return this.data.fortType
    }

    startUpdateTasks( ownerId: number ) {
        if ( ownerId > 0 ) {
            let clan: L2Clan = ClanCache.getClan( ownerId )
            clan.setFortId( this.getResidenceId() )
            this.setOwnerClan( clan )

            this.runCount = this.getOwnedTime() / Math.floor( ConfigManager.fortress.getPeriodicUpdateFrequency() / 1000 )
            // TODO : use initial time
            // let initial = Date.now() - this.lastOwnedTime
            // while (initial > ConfigManager.fortress.getPeriodicUpdateFrequency()) {
            //     initial -= ConfigManager.fortress.getPeriodicUpdateFrequency()
            // }
            //
            // initial = ConfigManager.fortress.getPeriodicUpdateFrequency() - initial
            if ( ( ConfigManager.fortress.getMaxKeepTime() <= 0 ) || ( this.getOwnedTime() < ( ConfigManager.fortress.getMaxKeepTime() * 3600 ) ) ) {
                this.updateTaskStartTime = Date.now()
                this.updateTask = setInterval( this.runFortUpdateTask.bind( this ), ConfigManager.fortress.getPeriodicUpdateFrequency() ) // Schedule owner tasks to start running
                if ( ConfigManager.fortress.getMaxKeepTime() > 0 ) {
                    this.ownedTimeTask = setTimeout( this.runRemoveOwnerTask.bind( this ), 3600000 ) // Schedule owner tasks to remove owener
                }
            } else {
                this.ownedTimeTask = setTimeout( this.runRemoveOwnerTask.bind( this ), 3600000 )
            }
        }
    }

    async runFortUpdateTask() {
        this.runCount++

        await this.fortOwner.increaseBloodOathCount()

        if ( this.getFortState() === FortState.ClanClaimed ) {
            if ( this.fortOwner.getWarehouse().getAdenaAmount() >= ConfigManager.fortress.getFeeForCastle() ) {

                await this.fortOwner.getWarehouse().destroyItemByItemId( ItemTypes.Adena, ConfigManager.fortress.getFeeForCastle(), 0, 'Fee for castle' )
                this.getContractedCastle().addToTreasuryNoTax( ConfigManager.fortress.getFeeForCastle() )
                this.raiseSupplyLevel()
            } else {
                this.setFortState( FortState.Abandoned )
            }
        }

        return this.saveFortVariables()
    }

    async runRemoveOwnerTask(): Promise<void> {
        if ( this.getOwnedTime() > ( ConfigManager.fortress.getMaxKeepTime() * 3600 ) ) {
            await this.removeOwner()
            this.setFortState( FortState.NpcOwned )
        }
    }

    setFortState( state: FortState, castleId: number = 0 ) : void {
        this.data.state = state
        this.data.castleId = castleId

        return FortManager.scheduleUpdate( this.residenceId )
    }

    getFortState() : FortState {
        return this.data.state
    }

    getContractedCastle() {
        return CastleManager.getCastleById( this.getContractedCastleId() )
    }

    getContractedCastleId() {
        return this.data.castleId
    }

    raiseSupplyLevel() : void {
        this.data.supplyLevel = _.clamp( this.data.supplyLevel + 1, ConfigManager.fortress.getMaxSupplyLevel() )
    }

    getTimeTillRebelArmy(): number {
        if ( !this.data.lastOwnedTime || this.data.lastOwnedTime === 0 ) {
            return 0
        }

        return Math.floor( ( ( this.data.lastOwnedTime + ( ConfigManager.fortress.getMaxKeepTime() * 3600000 ) ) - Date.now() ) / 1000 )
    }

    getCastleIdByAmbassador( npcId: number ): number {
        return this.envoyCastles[ npcId ]
    }

    getCastleByAmbassador( npcId: number ): Castle {
        return CastleManager.getCastleById( this.getCastleIdByAmbassador( npcId ) )
    }

    isBorderFortress() {
        return this.availableCastles.length > 1
    }

    async resetDoors(): Promise<void> {
        _.each( this.doors, ( doorId: number ) => {
            let door: L2DoorInstance = DoorManager.getDoor( doorId )
            if ( !door ) {
                return
            }

            if ( door.isOpen() ) {
                door.closeMe()
            }
            if ( door.isDead() ) {
                door.doRevive()
            }
            if ( door.getCurrentHp() < door.getMaxHp() ) {
                door.setCurrentHp( door.getMaxHp() )
            }
        } )

        await this.loadDoorUpgrade()
    }

    async loadDoorUpgrade(): Promise<void> {
        let data: Array<L2FortDoorUpgradeTableData> = await DatabaseManager.getFortDoorUpgrade().getAll( this.getResidenceId() )

        _.each( this.doors, ( doorId: number ) => {
            let door: L2DoorInstance = DoorManager.getDoor( doorId )
            if ( !door ) {
                return
            }

            let item: L2FortDoorUpgradeTableData = _.find( data, { doorId } )

            if ( item ) {
                door.setCurrentHp( door.getMaxHp() + item.hpValue )
            }
        } )
    }

    loadFlagPole(): void {
        let currentName = this.getName()
        this.flagPole = _.find( DataManager.getStaticObjects().getType( L2StaticObjectType.FortFlagpole ), ( object: L2StaticObjectInstance ) => {
            return object.getName().startsWith( currentName )
        } )
    }

    async loadFunctions(): Promise<void> {
        if ( this.getOwnerClan() ) {
            this.setVisibleFlag( true )

            let data: Array<L2FortFunctionsTableData> = await DatabaseManager.getFortFunctions().getAll( this.getResidenceId() )

            let residenceId = this.getResidenceId()
            this.functions = _.reduce( data, ( finalMap: { [ type: number ]: FortFunction }, item: L2FortFunctionsTableData ) => {

                let currentFunction = new FortFunction()

                currentFunction.type = item.type
                currentFunction.level = item.level
                currentFunction.fee = item.fee
                currentFunction.temporaryFee = 0

                currentFunction.rate = item.rate
                currentFunction.endDate = item.endDate
                currentFunction.residenceId = residenceId

                currentFunction.startTask( true )

                finalMap[ item.type ] = currentFunction

                return finalMap
            }, {} )
        }
    }

    async removeFunction( type: FortFunctionType ): Promise<void> {
        delete this.functions[ type ]
        return DatabaseManager.getFortFunctions().remove( this.getResidenceId(), type )
    }

    async updateFunctions( player: L2PcInstance, type: FortFunctionType, level: number, fee: number, rate: number, shouldAddNew: boolean ): Promise<boolean> {
        if ( !player ) {
            return false
        }

        if ( fee > 0 ) {
            if ( !await player.destroyItemByItemId( ItemTypes.Adena, fee, true, 'Fort.updateFunctions' ) ) {
                return false
            }
        }

        if ( shouldAddNew ) {
            let newFunction = new FortFunction()

            newFunction.type = type
            newFunction.level = level
            newFunction.fee = fee
            newFunction.temporaryFee = 0

            newFunction.rate = rate
            newFunction.endDate = 0
            newFunction.startTask( false )

            this.functions[ type ] = newFunction

            return true
        }

        if ( ( level === 0 ) && ( fee === 0 ) ) {
            await this.removeFunction( type )
            return true
        }

        if ( !this.functions[ type ] || ( fee - this.functions[ type ].fee ) > 0 ) {
            _.unset( this.functions, type )

            let newFunction = new FortFunction()

            newFunction.type = type
            newFunction.level = level
            newFunction.fee = fee
            newFunction.temporaryFee = 0

            newFunction.rate = rate
            newFunction.endDate = -1
            newFunction.startTask( false )

            this.functions[ type ] = newFunction
        } else {
            this.functions[ type ].fee = fee
            this.functions[ type ].level = level
            await this.functions[ type ].saveFunctionData()
        }

        return true
    }

    getOwnerClanId(): number {
        if ( this.fortOwner ) {
            return this.fortOwner.getId()
        }

        return -1
    }

    openCloseDoor( player: L2PcInstance, doorId: number, shouldOpen: boolean ): void {
        if ( player.getClanId() !== this.getOwnerClanId() ) {
            return
        }

        let door: L2DoorInstance = this.getDoor( doorId )
        if ( !door ) {
            return
        }

        if ( shouldOpen ) {
            door.openMe()
        } else {
            door.closeMe()
        }
    }

    getDoor( doorId: number ): L2DoorInstance {
        if ( this.doors.includes( doorId ) ) {
            return DoorManager.getDoor( doorId )
        }

        return null
    }

    getTimeTillNextFortUpdate(): number {
        if ( this.updateTaskStartTime > 0 ) {
            return Date.now() - this.updateTaskStartTime
        }

        return 0
    }

    banishForeigners() : Promise<unknown> {
        return this.residenceArea.banishOutsiders( this.getOwnerClanId() )
    }

    getSupplyLevel() : number {
        return this.data.supplyLevel
    }

    /*
        TODO : implement fort features correctly
        Default level should always be 1 (one) due to how spawn logic
        expects such values.
     */
    getFeatureLevel( type: FortFeatureType ) : number {
        if ( type === FortFeatureType.Supplies ) {
            return this.data.supplyLevel
        }

        return 1
    }

    initialSpawnEvents() : void {
        // TODO : add checks for siege progress
        return SpawnEventManager.triggerFortressEvent( this.residenceId, FortEvent.RegistrationForMercs )
    }

    setSiegeDate( date: number ) : void {
        this.data.siegeDate = date

        return FortManager.scheduleUpdate( this.residenceId )
    }

    getTeleportLocation( type : ResidenceTeleportType ) : Location {
        return this.residenceArea.getRandomTeleportLocation( type )
    }

    onSiegeStart() : Promise<void> {
        this.siegeAreaActive = true
        L2World.addArea( this.siegeArea )

        return this.resetDoors()
    }

    async onSiegeEnd() : Promise<void> {
        this.siegeAreaActive = false
        L2World.removeArea( this.siegeArea )

        let clan = this.getOwnerClan()
        let clanId = clan ? clan.getId() : -1
        await this.residenceArea.banishOutsiders( clanId )

        if ( clan && this.getFlagPole().getMeshIndex() === 0 ) {
            this.setVisibleFlag( true )
        }


        return this.resetDoors()
    }

    getPlayersInsideSiegeArea() : Readonly<Array<L2PcInstance>> {
        return L2World.getPlayersByBox( this.siegeArea.getSpatialIndex() )
    }
}

export class FortFunction {
    type: FortFunctionType
    level: number
    fee: number
    temporaryFee: number
    rate: number
    endDate: number
    residenceId: number
    functionTask: Timeout

    startTask( useClanWarehouse: boolean = false ): void {
        let fort: Fort = FortManager.getFortById( this.residenceId )

        if ( !fort || !fort.getOwnerClan() ) {
            return
        }

        let currentTime = Date.now()
        let delay = this.endDate > currentTime ? this.endDate - currentTime : 1
        this.functionTask = setTimeout( this.runFunctionTask.bind( this ), delay, useClanWarehouse )
    }

    async runFunctionTask( useClanWarehouse: boolean = false ): Promise<void> {
        let fort: Fort = FortManager.getFortById( this.residenceId )

        if ( !fort ) {
            return
        }

        let ownerClan = fort.getOwnerClan()

        if ( !ownerClan ) {
            return
        }


        if ( ( ownerClan.getWarehouse().getAdenaAmount() >= this.fee ) || !useClanWarehouse ) {
            let currentFunctionFee = this.fee
            if ( this.endDate === -1 ) {
                currentFunctionFee = this.temporaryFee
            }

            this.endDate = Date.now() + this.rate

            if ( useClanWarehouse ) {
                await ownerClan.getWarehouse().destroyItemByItemId( ItemTypes.Adena, currentFunctionFee, ownerClan.getLeaderId(), 'FortFunction.runFunctionTask' )
            }

            this.functionTask = setTimeout( this.runFunctionTask.bind( this ), this.rate, true )

            return this.saveFunctionData()
        }

        return fort.removeFunction( this.type )
    }

    // TODO : use font manager to save function data on debounced interval to avoid many multiple database calls
    async saveFunctionData(): Promise<void> {
        return DatabaseManager.getFortFunctions().update( this.residenceId, this.type, this.level, this.fee, this.rate, this.endDate )
    }
}