import { L2Clan } from '../L2Clan'
import { L2SiegeClan } from '../L2SiegeClan'

export interface Siegable {
    startSiege() : Promise<void>
    endSiege() : Promise<void>
    getAttackerClan( clan : L2Clan ) : L2SiegeClan
    getAttackerClans() : Array<L2SiegeClan>
    getAttackersInZone() : Array<number>
    checkIsAttacker( clan : L2Clan ) : boolean
    getDefenderClan( clan: L2Clan ) : L2SiegeClan
    getDefenderClans() : Array<L2SiegeClan>
    checkIsDefender( clan: L2Clan ) : boolean
    getFlag( clan: L2Clan ) : Array<number>
    getSiegeDate() : number
    getFameAmount() : number
    updateSiege() : void
    isInstance( type: SiegableType ) : boolean
}

export enum SiegableType {
    Siege,
    TerritoryWarManager,
    FortSiege,
    ClanHallSiegeEngine
}