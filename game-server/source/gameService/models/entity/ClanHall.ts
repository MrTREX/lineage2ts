import { L2DoorInstance } from '../actor/instance/L2DoorInstance'
import { ClanCache } from '../../cache/ClanCache'
import { L2Clan } from '../L2Clan'
import { DatabaseManager } from '../../../database/manager'
import { L2World } from '../../L2World'
import { PledgeShowInfoUpdate } from '../../packets/send/PledgeShowInfoUpdate'
import _ from 'lodash'
import { L2PcInstance } from '../actor/instance/L2PcInstance'
import { ItemTypes } from '../../values/InventoryValues'
import { ClanHallArea } from '../areas/type/ClanHall'
import { ResidenceTeleportType } from '../../enums/ResidenceTeleportType'
import { ILocational } from '../Location'
import { ClanHallFunctionType } from '../../enums/clanhall/ClanHallFunctionType'
import { ClanHallFunction } from '../clan/ClanHallFunction'

/*
    TODO : consider activating functions only when associated clan has active members online
    Meaning that functions start to get tracked when player is online, and get scheduled to be removed in 5 minutes after last clan member logs off
 */

const clanHallIdsWithPetEvolve: Set<number> = new Set( [
    36,
    37,
    38,
    39,
    40,
    41,
    51,
    52,
    53,
    54,
    55,
    56,
    57,
] )

export class ClanHall {
    clanHallId: number
    doorIds: Array<number> = []
    name: string
    ownerId: number
    description: string = ''
    location: string = ''
    hallArea: ClanHallArea
    isFree: boolean = true
    functions: { [ key: number ]: ClanHallFunction } = {}
    hasPetEvolve: boolean

    constructor( area: ClanHallArea ) {
        this.hallArea = area
    }

    getOwnerId(): number {
        return this.ownerId
    }

    isSiegableHall(): boolean {
        return false
    }

    async initialize() : Promise<void> {
        if ( this.ownerId > 0 ) {
            let clan: L2Clan = ClanCache.getClan( this.ownerId )
            if ( clan ) {
                clan.setHideoutId( this.getId() )
            } else {
                this.free()
            }
        }

        this.hasPetEvolve = clanHallIdsWithPetEvolve.has( this.clanHallId )
    }

    getId() {
        return this.clanHallId
    }

    free() {
        this.ownerId = 0
        this.isFree = true

        DatabaseManager.getClanhallFunctions().removeAll( this.getId(), Object.keys( this.functions ).map( value => _.parseInt( value ) ) )

        this.functions = {}
    }

    getDoorIds() {
        return this.doorIds
    }

    getFunction( type: ClanHallFunctionType ): ClanHallFunction {
        return this.functions[ type ]
    }

    getName(): string {
        return this.name
    }

    openCloseDoors( isOpen: boolean ) {
        this.getDoorIds().forEach( ( objectId: number ) => {
            let door = L2World.getObjectById( objectId ) as L2DoorInstance
            if ( !door ) {
                isOpen ? door.openMe() : door.closeMe()
            }
        } )
    }

    openCloseDoor( doorId: number, isOpen: boolean ): void {
        let door = this.getDoor( doorId )
        if ( !door ) {
            return
        }

        isOpen ? door.openMe() : door.closeMe()
    }

    getDoor( doorId: number ): L2DoorInstance {
        if ( doorId <= 0 ) {
            return
        }

        let door: L2DoorInstance
        // TODO : convert to Record<number,number> for easy lookups
        this.getDoorIds().some( ( objectId: number ): boolean => {
            door = L2World.getObjectById( objectId ) as L2DoorInstance
            return door && door.getId() === doorId
        } )

        return door
    }

    setOwner( clan: L2Clan ) {
        if ( this.ownerId > 0 || !clan ) {
            return
        }

        this.ownerId = clan.getId()
        this.isFree = false

        clan.setHideoutId( this.getId() )
        clan.broadcastDataToOnlineMembers( PledgeShowInfoUpdate( clan ) )
    }

    banishForeigners(): Promise<unknown> {
        if ( this.hallArea ) {
            return this.hallArea.banishOutsiders( this.getOwnerId() )
        }
    }

    getLease(): number {
        return 0
    }

    getExpirationTime(): number {
        return 0
    }

    getLocation() {
        return this.location
    }

    async updateFunction( player: L2PcInstance, type: ClanHallFunctionType, level: number, initialFee: number, chargeTimeInterval: number, shouldAdd: boolean ) {
        if ( initialFee > 0 && !await player.destroyItemByItemId( ItemTypes.Adena, initialFee, true, 'ClanHall.updateFunction' ) ) {
            return false
        }

        if ( shouldAdd ) {
            if ( this.functions[ type ] ) {
                return false
            }

            this.functions[ type ] = {
                endDate: 0,
                fee: initialFee,
                hallId: this.clanHallId,
                level,
                chargeTimeInterval,
                type
            }

            return true
        }

        if ( level === 0 || initialFee === 0 ) {
            await this.removeFunction( type )
            return true
        }

        let existingFunction = this.functions[ type ]
        let costDifference = existingFunction.fee - initialFee

        existingFunction.fee = initialFee
        existingFunction.level = level

        if ( costDifference > 0 ) {
            existingFunction.chargeTimeInterval = chargeTimeInterval
            existingFunction.endDate = -1
        }

        return true
    }

    removeFunction( type : ClanHallFunctionType ) : Promise<void> {
        let currentFunction = this.functions[ type ]
        if ( !currentFunction ) {
            return
        }

        delete this.functions[ type ]

        return DatabaseManager.getClanhallFunctions().removeFunction( currentFunction )
    }

    getGrade() : number {
        return 0
    }

    getResidenceTeleport( type: ResidenceTeleportType ) : ILocational {
        return this.hallArea && this.hallArea.getRandomTeleportLocation( type )
    }

    hasArea() : boolean {
        return !!this.hallArea
    }
}