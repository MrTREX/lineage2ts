import { L2PcInstance } from '../actor/instance/L2PcInstance'
import { ConfigManager } from '../../../config/ConfigManager'
import { UserInfo } from '../../packets/send/UserInfo'
import { ExBrExtraUserInfo } from '../../packets/send/ExBrExtraUserInfo'
import { DataManager } from '../../../data/manager'
import { NpcHtmlMessagePath } from '../../packets/send/NpcHtmlMessage'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { L2World } from '../../L2World'
import { L2Npc } from '../actor/L2Npc'
import { Location } from '../Location'
import { ClanCache } from '../../cache/ClanCache'
import { MaxHeap } from '@datastructures-js/heap'
import _ from 'lodash'

enum EventState {
    OFF, // Not running
    STANDBY, // Waiting for participants to register
    ON // Registration is over and the event has started.
}

export interface PlayerEventData {
    objectId: number
    name: string
    title: string
    clanId: number
    location: Location
    pvpKills: number
    pkKills: number
    karma: number
    kills: Array<number> // object ids for L2PcInstance
    sitForced: boolean
    teamId: number
}

class Manager {
    eventState: number = EventState.OFF
    eventName: string = ''
    eventCreator: string = ''
    eventInfo: string = ''
    teamsNumber: number = 0
    registeredPlayers: Array<number> = []
    teams: { [ key: number ]: Array<number> } = {} // used by admin commands
    npcId: number = 0

    playerEventData: { [ key: number ]: PlayerEventData } = {} // objectId to data mapping
    eventNpcObjectIds: Array<number> = []

    finishEvent(): string {
        let eventManager = this

        switch ( this.eventState ) {
            case EventState.OFF:
                return 'Cannot finish event, it is already off.'

            case EventState.STANDBY:
                _.each( this.registeredPlayers, ( playerId: number ) => {
                    let player = L2World.getPlayer( playerId )
                    if ( player ) {
                        eventManager.removeAndResetPlayer( player )
                    }
                } )

                this.unspawnEventNpcs()
                this.registeredPlayers = []
                this.playerEventData = {}
                this.teams = {}
                this.teamsNumber = 0
                this.eventName = ''
                this.eventState = EventState.OFF

                return 'The event has been stopped at STANDBY mode, all players unregistered and all event npcs unspawned.'

            case EventState.ON:

                _.each( this.playerEventData, ( data: PlayerEventData ) => {
                    let player = L2World.getPlayer( data.objectId )
                    if ( player ) {
                        eventManager.removeAndResetPlayer( player )
                    }
                } )

                // TODO : AntiFeedManager.getInstance().clear(AntiFeedManager.TVT_ID);
                this.unspawnEventNpcs()
                this.registeredPlayers = []
                this.playerEventData = {}
                this.teams = {}
                this.teamsNumber = 0
                this.eventName = ''
                this.eventState = EventState.OFF

                this.npcId = 0
                this.eventCreator = ''
                this.eventInfo = ''

                return 'The event has been stopped, all players are unregistered and all event npcs are unspawned.'
        }

        return 'The event has been successfully finished.'
    }

    isParticipant( objectId: number ): boolean {
        switch ( this.eventState ) {
            case EventState.OFF:
                return false

            case EventState.STANDBY:
                return _.includes( this.registeredPlayers, objectId )

            case EventState.ON:
                return !!this.playerEventData[ objectId ]
        }

        return false
    }

    registerPlayer( player: L2PcInstance ): void {
        if ( this.eventState !== EventState.STANDBY ) {
            player.sendMessage( 'The registration period for this event is over.' )
            return
        }

        // TODO : add check for double IPs AntiFeedManager.getInstance().tryAddPlayer(AntiFeedManager.L2EVENT_ID, player, customs().getDualboxCheckMaxL2EventParticipantsPerIP())
        if ( ConfigManager.customs.getDualboxCheckMaxL2EventParticipantsPerIP() === 0 ) {
            this.registeredPlayers.push( player.getObjectId() )
            return
        }

        player.sendMessage( 'You have reached the maximum allowed participants per IP.' )
    }

    removeAndResetPlayer( player: L2PcInstance ): void {
        if ( this.isParticipant( player.getObjectId() ) ) {
            if ( player.isDead() ) {
                player.restoreExp( 100.0 )
                player.doRevive()
                player.setMaxStats()
                player.setCurrentCp( player.getMaxCp() )
            }

            player.resetPolymorphData()
            player.decayMe()
            player.spawnMe( player.getX(), player.getY(), player.getZ() )

            player.broadcastCharacterInformation()

            player.sendDebouncedPacket( UserInfo )
            BroadcastHelper.dataBasedOnVisibility( player, ExBrExtraUserInfo( player.getObjectId() ) )

            player.stopTransformation( true )
        }

        _.pull( this.registeredPlayers, player.getObjectId() )
        let data: PlayerEventData = this.playerEventData[ player.getObjectId() ]
        if ( data ) {
            _.pull( this.teams[ data.teamId ], player.getObjectId() )
            this.restorePlayerStats( player.getObjectId() )
        }
    }

    showEventHtml( player: L2PcInstance, objectId: number ): void {
        if ( this.eventState !== EventState.STANDBY ) {
            return
        }

        let htmlPath: string

        if ( this.registeredPlayers.includes( player.getObjectId() ) ) {
            htmlPath = 'data/html/mods/EventEngine/Participating.htm'
        } else {
            htmlPath = 'data/html/mods/EventEngine/Participation.htm'
        }

        let html = DataManager.getHtmlData().getItem( htmlPath ).replace( /%eventName%/g, this.eventName )
                .replace( /%eventCreator%/g, this.eventCreator )
                .replace( /%eventInfo%/g, this.eventInfo )

        player.sendOwnedData( NpcHtmlMessagePath( html, htmlPath, player.getObjectId(), objectId ) )
    }

    async spawnEventNpc( player: L2PcInstance ): Promise<void> {
        /*
            TODO: use new spawn system
            - use player drop circle to spawn npc near player
            - save spawn into current class
         */
        // let spawn: L2Spawn = L2Spawn.fromNpcId( this.npcId )
        // spawn.setX( player.getX() + 50 )
        // spawn.setY( player.getY() + 50 )
        // spawn.setZ( player.getZ() )
        // spawn.setAmount( 1 )
        // spawn.setHeading( player.getHeading() )
        // spawn.stopRespawn()
        //
        // SpawnManager.addExternalSpawn( spawn, false )
        //
        // spawn.initialize()
        //
        // let npc = spawn.getLastSpawn()
        // npc.setCurrentHp( 999999999 )
        // npc.setTitle( this.eventName )
        // npc.setEventMob( true )
        //
        // this.eventNpcObjectIds.push( npc.getObjectId() )
        // BroadcastHelper.dataInLocation( npc, MagicSkillUseWithCharacters( npc, npc, 1034, 1, 1, 1 ), npc.getBroadcastRadius() )
    }

    startEvent(): string {
        switch ( this.eventState ) {
            case EventState.ON:
                return 'Cannot start event, it is already on.'

            case EventState.STANDBY:
                this.eventState = EventState.ON
                break

            case EventState.OFF: // Event is off, so no problem turning it on.
                return 'Cannot start event, it is off. Participation start is required.'
        }

        this.unspawnEventNpcs()
        this.playerEventData = {}

        /**
         * Two step operation:
         * - sort all players by max level using MaxHeap
         * - distribute highest level players equally between teams (guarantees total average of player levels to be similar)
         */
        let heap: MaxHeap<L2PcInstance> = new MaxHeap<L2PcInstance>( ( player: L2PcInstance ) => player.getLevel() )
        let manager = this
        _.each( this.registeredPlayers, ( playerId: number ) => {
            let player = L2World.getPlayer( playerId )

            if ( player ) {
                heap.insert( player )
                manager.createDataForPlayer( player, false )
            }
        } )

        this.registeredPlayers = []
        let currentTeamIndex = 0

        while ( heap.size() > 0 ) {
            let player: L2PcInstance = heap.extractRoot()

            let teamNumber = currentTeamIndex + 1
            if ( !this.teams[ teamNumber ] ) {
                this.teams[ teamNumber ] = []
            }

            this.teams[ teamNumber ].push( player.getObjectId() )
            this.playerEventData[ player.getObjectId() ].teamId = teamNumber

            currentTeamIndex = teamNumber % this.teamsNumber
        }

        return 'The event has been successfully started.'
    }

    unspawnEventNpcs() {
        _.each( this.eventNpcObjectIds, ( objectId: number ) => {
            let npc = L2World.getObjectById( objectId ) as L2Npc
            if ( npc ) {
                npc.deleteMe()
            }
        } )
    }

    createDataForPlayer( player: L2PcInstance, sitForced: boolean = false ): void {
        this.playerEventData[ player.getObjectId() ] = {
            objectId: player.getObjectId(),
            kills: [],
            name: player.getName(),
            title: player.getTitle(),
            clanId: player.getClanId(),
            location: Location.fromObject( player ),
            pvpKills: player.getPvpKills(),
            pkKills: player.getPkKills(),
            karma: player.getKarma(),
            sitForced,
            teamId: 0,
        }
    }

    restorePlayerStats( objectId: number ): void {
        let data: PlayerEventData = this.playerEventData[ objectId ]
        if ( !data ) {
            return
        }

        _.unset( this.playerEventData, objectId )

        let player: L2PcInstance = L2World.getPlayer( objectId )

        player.setName( data.name )
        player.setTitle( data.title )
        player.setClan( ClanCache.getClan( data.clanId ) )
        player.setPvpKills( data.pvpKills )
        player.setPkKills( data.pkKills )
        player.setKarma( data.karma )

        player.teleportToLocation( data.location, true )
    }

    addKill( killerPlayerId: number, deadPlayerId: number ): void {
        if ( this.playerEventData[ killerPlayerId ] ) {
            this.playerEventData[ killerPlayerId ].kills.push( deadPlayerId )
        }
    }
}

export const L2Event = new Manager()