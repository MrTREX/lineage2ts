import { Location } from '../Location'
import { L2PcInstance } from '../actor/instance/L2PcInstance'
import { L2World } from '../../L2World'
import { Team } from '../../enums/Team'
import { L2Summon } from '../actor/L2Summon'
import aigle from 'aigle'
import { BuffProperties } from '../skills/BuffDefinition'

export class PlayerCondition {

    playerId: number
    summonId: number
    hp: number
    mp: number
    cp: number
    isPartyDuel: boolean
    location: Location
    playerEffects: Array<BuffProperties>
    petEffects: Array<BuffProperties>

    constructor( player: L2PcInstance, isPartyDuel: boolean ) {
        this.playerId = player.getObjectId()
        this.hp = player.getCurrentHp()
        this.mp = player.getCurrentMp()
        this.cp = player.getCurrentCp()

        this.isPartyDuel = isPartyDuel
        this.playerEffects = player.getEffectList().getBuffProperties()

        if ( isPartyDuel ) {
            this.location = Location.fromObject( player )
        }

        let summon = player.getSummon()
        if ( summon ) {
            this.summonId = summon.getObjectId()
            this.petEffects = summon.getEffectList().getBuffProperties()
        }
    }

    async restoreCondition() : Promise<void> {
        let player : L2PcInstance = L2World.getPlayer( this.playerId )
        if ( !player ) {
            return
        }

        player.setCurrentHp( this.hp )
        player.setCurrentMp( this.mp )
        player.setCurrentCp( this.cp )

        player.setIsInDuel( 0 )
        player.setTeam( Team.None )
        player.broadcastUserInfo()

        if ( this.isPartyDuel ) {
            await player.teleportToLocation( this.location, false )
        }

        await player.getEffectList().stopAllEffects()

        await aigle.resolve( this.playerEffects ).eachSeries( ( effect: BuffProperties ) => {
            if ( effect && effect.durationMs > 0 ) {
                return effect.skill.applyEffects( player, player, false, false, true, effect.durationMs )
            }
        } )

        if ( player.hasSummon() ) {
            await player.getSummon().getEffectList().stopAllEffects()

            let summon : L2Summon = L2World.getObjectById( this.summonId ) as L2Summon
            if ( summon && ( this.summonId === player.getSummon().getObjectId() ) ) {
                await aigle.resolve( this.playerEffects ).eachSeries( ( effect: BuffProperties ) => {
                    if ( effect && effect.durationMs > 0 ) {
                        return effect.skill.applyEffects( summon, summon, false, false, true, effect.durationMs )
                    }
                } )
            }

            await summon.teleportToLocation( this.location, true )
        }
    }
}