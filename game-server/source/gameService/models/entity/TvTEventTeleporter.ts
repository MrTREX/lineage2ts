import { Location } from '../Location'
import { L2PcInstance } from '../actor/instance/L2PcInstance'

export class TvTEventTeleporter {
    playerId: number
    location: Location
    adminRemove: boolean

    constructor( player: L2PcInstance, location: Location, fastSchedule: boolean, adminRemove: boolean ) {
        this.playerId = player.getObjectId()
        this.location = location
        this.adminRemove = adminRemove

        // TODO schedule events here
    }
}