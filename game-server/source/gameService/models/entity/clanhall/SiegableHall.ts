import { ClanHall } from '../ClanHall'
import { SiegeStatus } from '../../../enums/SiegeStatus'
import { ClanHallSiegeEngine } from './ClanHallSiegeEngine'
import { L2Clan } from '../../L2Clan'
import { L2SiegeClan } from '../../L2SiegeClan'
import { L2PcInstance } from '../../actor/instance/L2PcInstance'
import { SiegeInfoForHall } from '../../../packets/send/SiegeInfo'
import { ClanHallSiegeManager } from '../../../instancemanager/ClanHallSiegeManager'
import parser from 'cron-parser'
import moment from 'moment'
import { SiegeClanType } from '../../../enums/SiegeClanType'
import { ServerLog } from '../../../../logger/Logger'

export class SiegableHall extends ClanHall {
    nextSiege: number
    duration: number
    startCron: string

    status: SiegeStatus = SiegeStatus.REGISTERING
    siege: ClanHallSiegeEngine
    siegeActive: boolean = false

    async initialize(): Promise<void> {
        if ( Date.now() > this.nextSiege ) {
            this.computeNextSiegeDate()
        }

        return super.initialize()
    }

    isSiegableHall(): boolean {
        return true
    }

    getSiege(): ClanHallSiegeEngine {
        return this.siege
    }

    isInSiege() : boolean {
        return this.status === SiegeStatus.RUNNING
    }

    isRegistered( clan: L2Clan ) {
        let siege = this.getSiege()
        if ( siege ) {
            return siege.checkIsAttacker( clan )
        }

        return false
    }

    isWaitingBattle(): boolean {
        return this.status === SiegeStatus.WAITING_BATTLE
    }

    addAttacker( clan: L2Clan ) {
        if ( this.siege ) {
            this.siege.attackers[ clan.getId() ] = new L2SiegeClan( clan.getId(), SiegeClanType.Attacker )
        }
    }

    isRegistering(): boolean {
        return this.status === SiegeStatus.REGISTERING
    }

    removeAttacker( clan: L2Clan ) {
        if ( this.siege ) {
            delete this.siege.attackers[ clan.getId() ]
        }
    }

    getNextSiegeTime() {
        return this.nextSiege
    }

    showSiegeInfo( player: L2PcInstance ) {
        player.sendOwnedData( SiegeInfoForHall( this, player ) )
    }

    setNextSiegeDate( value: number ) {
        this.nextSiege = value
    }

    updateSiegeStatus( status: SiegeStatus ) {
        this.status = status
    }

    getSiegeStatus() {
        return this.status
    }

    computeNextSiegeDate(): void {
        this.nextSiege = parser.parseExpression( this.startCron ).next().toDate().valueOf()

        ServerLog.info( `Clan hall "${ this.name }" of ${this.location} re-scheduled siege date on ${ moment( this.nextSiege ).format() }` )
        ClanHallSiegeManager.scheduleHallUpdate( this.getId() )
    }

    isSiegeActive() : boolean {
        return this.siegeActive
    }
}