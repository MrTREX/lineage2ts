import { PlayerCondition } from './PlayerCondition'
import { PacketDispatcher } from '../../PacketDispatcher'
import { L2World } from '../../L2World'
import { L2PcInstance } from '../actor/instance/L2PcInstance'
import { DuelState } from '../../enums/DuelState'
import { Team } from '../../enums/Team'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { ExDuelUpdateUserInfo } from '../../packets/send/ExDuelUpdateUserInfo'
import { InstanceManager } from '../../instancemanager/InstanceManager'
import { L2DoorInstance } from '../actor/instance/L2DoorInstance'
import { MusicPacket } from '../../packets/send/Music'
import { Instance } from './Instance'
import { Location } from '../Location'
import { DuelResult } from '../../enums/DuelResult'
import { SocialAction, SocialActionType } from '../../packets/send/SocialAction'
import { ActionFailed } from '../../packets/send/ActionFailed'
import { DuelManager } from '../../instancemanager/DuelManager'
import { ExDuelReady } from '../../packets/send/ExDuelReady'
import { ExDuelStart } from '../../packets/send/ExDuelStart'
import { ExDuelEnd } from '../../packets/send/ExDuelEnd'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'
import { AIIntent } from '../../aicontroller/enums/AIIntent'
import _ from 'lodash'
import { AreaType } from '../areas/AreaType'

const enum DuelValues {
    PARTY_DUEL_DURATION = 300 * 1000,
    PARTY_DUEL_PREPARE_TIME = 30 * 1000,
    PARTY_DUEL_TELEPORT_BACK_TIME = 10 * 1000,
    PLAYER_DUEL_DURATION = 120 * 1000,
    DUEL_PREPARE_TIME = 5 * 1000,
}

export class Duel {
    constructor( player: L2PcInstance, target: L2PcInstance, isPartyDuel: boolean, duelId: number ) {
        this.duelId = duelId
        this.partyDuel = isPartyDuel
        this.duelEndTime = Date.now() + ( isPartyDuel ? DuelValues.PARTY_DUEL_DURATION : DuelValues.PLAYER_DUEL_DURATION )

        if ( isPartyDuel ) {
            this.leaderA = player.getObjectId()
            this.leaderB = target.getObjectId()
            this.teamA = [ ... player.getParty().getMembers() ]
            this.teamB = [ ... target.getParty().getMembers() ]
        } else {
            this.leaderA = player.getObjectId()
            this.leaderB = target.getObjectId()

            this.teamA = [ player.getObjectId() ]
            this.teamB = [ target.getObjectId() ]
        }

        this.savePlayerConditions()

        if ( isPartyDuel ) {
            this.countdown = DuelValues.PARTY_DUEL_PREPARE_TIME
            this.teleportPlayers()
        } else {
            this.countdown = DuelValues.DUEL_PREPARE_TIME
        }

        setTimeout( this.runDuelPreparationTask.bind( this ), this.countdown - 3000 )
    }

    runDuelPreparationTask() {
        if ( this.updateCountdown() > 0 ) {
            setTimeout( this.runDuelPreparationTask.bind( this ), 1000 )
            return
        }

        this.startDuel()
    }

    duelId: number
    teamA: Array<number> = []
    leaderA: number
    teamB: Array<number> = []
    leaderB: number
    partyDuel: boolean
    duelEndTime: number
    surrenderRequest: number = 0
    countdown: number
    playerConditions: { [ key: number ]: PlayerCondition } = {}
    duelInstanceId: number

    getTeamA() : Array<number> {
        return this.teamA
    }

    getTeamB() : Array<number> {
        return this.teamB
    }

    broadcastToTeamB( packet: Buffer ) {
        if ( this.teamB.length === 0 ) {
            return
        }

        this.teamB.forEach( ( playerId: number ) => {
            PacketDispatcher.sendCopyData( playerId, packet )
        } )
    }

    broadcastToTeamA( packet: Buffer ) {
        if ( this.teamA.length === 0 ) {
            return
        }

        this.teamA.forEach( ( playerId: number ) => {
            PacketDispatcher.sendCopyData( playerId, packet )
        } )
    }

    isPartyDuel() {
        return this.partyDuel
    }

    getTeamLeaderA() : L2PcInstance {
        return L2World.getPlayer( this.leaderA )
    }

    getTeamLeaderB() : L2PcInstance {
        return L2World.getPlayer( this.leaderB )
    }

    onPlayerDefeat( player: L2PcInstance ) : void {
        player.setDuelState( DuelState.Dead )
        player.setTeam( Team.None )

        if ( this.partyDuel ) {
            let teamDefeated = true
            let isInTeamA = true
            let checkDuelState = ( objectId: number ) => {
                let member = L2World.getPlayer( objectId )
                return member && member.getDuelState() === DuelState.Action
            }

            if ( this.teamA.includes( player.getObjectId() ) ) {
                teamDefeated = !_.some( this.teamA, checkDuelState )
            } else if ( this.teamB.includes( player.getObjectId() ) ) {
                isInTeamA = false
                teamDefeated = !_.some( this.teamB, checkDuelState )
            }

            if ( teamDefeated ) {
                _.each( isInTeamA ? this.teamB : this.teamA, ( objectId: number ) => {
                    let member = L2World.getPlayer( objectId )
                    member.setDuelState( DuelState.Winner )
                } )
            }
        } else {
            if ( this.leaderA === player.getObjectId() ) {
                let leader = L2World.getPlayer( this.leaderB )
                leader.setDuelState( DuelState.Winner )
            } else {
                let leader = L2World.getPlayer( this.leaderA )
                leader.setDuelState( DuelState.Winner )
            }
        }
    }

    updateCountdown() {
        this.countdown -= 1000

        if ( this.countdown > 3000 ) {
            return this.countdown
        }

        let packet : Buffer
        if ( this.countdown > 0 ) {
            packet = new SystemMessageBuilder( SystemMessageIds.THE_DUEL_WILL_BEGIN_IN_S1_SECONDS )
                    .addNumber( this.countdown )
                    .getBuffer()
        } else {
            packet = new SystemMessageBuilder( SystemMessageIds.LET_THE_DUEL_BEGIN ).getBuffer()
        }

        this.broadcastToTeamA( packet )
        this.broadcastToTeamB( packet )

        return this.countdown
    }

    startDuel() {
        // Send duel packets
        this.broadcastToTeamA( ExDuelReady( this.partyDuel ) )
        this.broadcastToTeamB( ExDuelReady( this.partyDuel ) )
        this.broadcastToTeamA( ExDuelStart( this.partyDuel ) )
        this.broadcastToTeamB( ExDuelStart( this.partyDuel ) )

        let duel = this

        _.each( this.teamA, ( playerId: number ) => {
            let player = L2World.getPlayer( playerId )

            player.cancelActiveTrade()
            player.setIsInDuel( duel.duelId )
            player.setTeam( Team.Blue )
            player.broadcastUserInfo()

            duel.broadcastToTeamA( ExDuelUpdateUserInfo( playerId ) )
        } )

        _.each( this.teamB, ( playerId: number ) => {
            let player = L2World.getPlayer( playerId )

            player.cancelActiveTrade()
            player.setIsInDuel( duel.duelId )
            player.setTeam( Team.Red )
            player.broadcastUserInfo()

            duel.broadcastToTeamB( ExDuelUpdateUserInfo( playerId ) )
        } )


        if ( this.partyDuel ) {

            _.each( InstanceManager.getInstance( this.getDuelInstanceId() ).getDoors(), ( door: L2DoorInstance ) => {
                if ( door && door.isOpen() ) {
                    door.closeMe()
                }
            } )
        }

        let packet = MusicPacket.B04_S01
        this.broadcastToTeamA( packet )
        this.broadcastToTeamB( packet )

        // start dueling task
        setTimeout( this.runDuelClockTask.bind( this ), 1000 )
    }

    getDuelInstanceId() {
        return this.duelInstanceId
    }

    teleportPlayers() : void {
        if ( !this.partyDuel ) {
            return
        }

        this.duelInstanceId = InstanceManager.createDynamicInstance( 'PartyDuel' )

        let instance : Instance = InstanceManager.getInstance( this.duelInstanceId )

        let allPlayerIds = [ ...this.teamA, ...this.teamB ]
        let duel = this
        allPlayerIds.forEach( ( playerId: number, index: number ) => {
            let player = L2World.getPlayer( playerId )
            let location: Location = instance.getEnterLocations()[ index ]

            player.teleportToLocationCoordinates( location.getX(), location.getY(), location.getZ(), 0, duel.duelInstanceId, 0 )
        } )
    }

    savePlayerConditions() {
        const recordCondition = ( playerId: number ) => {
            let player = L2World.getPlayer( playerId )
            this.playerConditions[ playerId ] = new PlayerCondition( player, this.isPartyDuel() )
        }

        this.teamA.forEach( recordCondition )
        this.teamB.forEach( recordCondition )
    }

    runDuelClockTask() {
        if ( this.checkEndDuelCondition() === DuelResult.CONTINUE ) {
            setTimeout( this.runDuelClockTask.bind( this ), 1000 )
            return
        }

        this.endDuel()
    }

    endDuel() : void {
        this.broadcastToTeamA( ExDuelEnd( this.partyDuel ) )
        this.broadcastToTeamB( ExDuelEnd( this.partyDuel ) )

        this.playKneelAnimation()
        this.sendEndMessages()
        this.restorePlayerConditions()
    }

    playKneelAnimation() {
        _.each( this.getLooserTeam(), ( playerId: number ) => {
            let player = L2World.getPlayer( playerId )
            if ( player ) {
                BroadcastHelper.dataToSelfInRange( player, SocialAction( playerId, SocialActionType.Bow ) )
            }
        } )
    }

    getLooserTeam() : Array<number> {
        let leaderA = L2World.getPlayer( this.leaderA )
        let leaderB = L2World.getPlayer( this.leaderB )

        if ( !leaderA || !leaderB ) {
            return null
        }

        if ( leaderA.getDuelState() === DuelState.Winner ) {
            return this.teamB
        }

        if ( leaderB.getDuelState() === DuelState.Winner ) {
            return this.teamA
        }

        return null
    }

    sendEndMessages() {

        let packet : Buffer
        switch ( this.checkEndDuelCondition() ) {
            case DuelResult.TEAM_1_WIN:
            case DuelResult.TEAM_2_SURRENDER:
                let teamAId = this.partyDuel ? SystemMessageIds.C1_PARTY_HAS_WON_THE_DUEL : SystemMessageIds.C1_HAS_WON_THE_DUEL
                packet = new SystemMessageBuilder( teamAId ).addString( L2World.getPlayer( this.leaderA ).getName() ).getBuffer()
                break
            case DuelResult.TEAM_1_SURRENDER:
            case DuelResult.TEAM_2_WIN:
                let teamBId = this.partyDuel ? SystemMessageIds.C1_PARTY_HAS_WON_THE_DUEL : SystemMessageIds.C1_HAS_WON_THE_DUEL
                packet = new SystemMessageBuilder( teamBId ).addString( L2World.getPlayer( this.leaderB ).getName() ).getBuffer()
                break
            case DuelResult.CANCELED:
                case DuelResult.TIMEOUT:
                this.stopFighting()
                packet = new SystemMessageBuilder( SystemMessageIds.THE_DUEL_HAS_ENDED_IN_A_TIE ).getBuffer()
        }

        this.broadcastToTeamA( packet )
        this.broadcastToTeamB( packet )
    }

    stopFighting() : void {
        let makeStopFighting = ( playerId: number ) => {
            let player = L2World.getPlayer( playerId )

            if ( !player ) {
                return
            }

            player.abortCast()
            player.setTarget( null )
            player.sendOwnedData( ActionFailed() )

            AIEffectHelper.scheduleNextIntent( player, AIIntent.WAITING )
        }

        _.each( this.teamA, makeStopFighting )
        _.each( this.teamB, makeStopFighting )
    }

    checkEndDuelCondition() : DuelResult {
        let leaderA = L2World.getPlayer( this.leaderA )
        let leaderB = L2World.getPlayer( this.leaderB )

        if ( !leaderA || !leaderB ) {
            return DuelResult.CANCELED
        }

        // got a duel surrender request?
        if ( this.surrenderRequest !== 0 ) {
            if ( this.surrenderRequest === 1 ) {
                return DuelResult.TEAM_1_SURRENDER
            }

            return DuelResult.TEAM_2_SURRENDER
        }

        // duel timed out
        if ( this.getRemainingTime() <= 0 ) {
            return DuelResult.TIMEOUT
        }

        // Has a player been declared winner yet?
        if ( leaderA.getDuelState() === DuelState.Winner ) {
            // If there is a Winner already there should be no more fighting going on
            this.stopFighting()
            return DuelResult.TEAM_1_WIN
        }

        if ( leaderB.getDuelState() === DuelState.Winner ) {
            // If there is a Winner already there should be no more fighting going on
            this.stopFighting()
            return DuelResult.TEAM_2_WIN
        }

        // More end duel conditions for 1on1 duels
        if ( !this.partyDuel ) {
            // Duel was interrupted e.g.: player was attacked by mobs / other players
            if ( ( leaderA.getDuelState() === DuelState.Interrupted ) || ( leaderB.getDuelState() === DuelState.Interrupted ) ) {
                return DuelResult.CANCELED
            }

            // Are the players too far apart?
            if ( !leaderA.isInsideRadius( leaderB, 2000 ) ) {
                return DuelResult.CANCELED
            }

            // is one of the players in a Siege, Peace or PvP zone?
            if ( leaderA.isInArea( AreaType.Peace )
                    || leaderB.isInArea( AreaType.Peace )
                    || leaderA.isInSiegeArea()
                    || leaderB.isInSiegeArea()
                    || leaderA.isInArea( AreaType.PVP )
                    || leaderB.isInArea( AreaType.PVP ) ) {
                return DuelResult.CANCELED
            }
        }

        return DuelResult.CONTINUE
    }

    getRemainingTime() {
        return this.duelEndTime - Date.now()
    }

    restorePlayerConditions() {
        let waitTime = this.partyDuel ? DuelValues.PARTY_DUEL_TELEPORT_BACK_TIME : 1000
        setTimeout( this.runRestorePlayerConditionsTask.bind( this ), waitTime )
        setTimeout( this.clear.bind( this ), waitTime )
    }

    runRestorePlayerConditionsTask() {
        _.each( this.playerConditions, ( condition: PlayerCondition ) => {
            condition.restoreCondition()
        } )
    }

    clear() {
        InstanceManager.destroyInstance( this.getDuelInstanceId() )
        DuelManager.removeDuel( this )
    }

    getId() {
        return this.duelId
    }

    doSurrender( player: L2PcInstance ) {
        // already received a surrender request
        if ( ( this.surrenderRequest !== 0 ) || this.partyDuel ) {
            return
        }

        // stop the fight
        this.stopFighting()

        if ( player.getObjectId() === this.leaderA ) {
            this.surrenderRequest = 1

            player.setDuelState( DuelState.Dead )

            let opponent = L2World.getPlayer( this.leaderB )
            if ( opponent ) {
                opponent.setDuelState( DuelState.Winner )
            }

        } else if ( player.getObjectId() === this.leaderB ) {
            this.surrenderRequest = 2

            let opponent = L2World.getPlayer( this.leaderA )
            if ( opponent ) {
                opponent.setDuelState( DuelState.Winner )
            }

            player.setDuelState( DuelState.Dead )
        }
    }
}