import { L2SiegeClan } from '../L2SiegeClan'
import { Castle } from './Castle'
import { L2ControlTowerInstance } from '../actor/instance/L2ControlTowerInstance'
import { L2FlameTowerInstance } from '../actor/instance/L2FlameTowerInstance'
import { SiegeGuardManager } from '../../instancemanager/SiegeGuardManager'
import { L2Clan } from '../L2Clan'
import { Siegable, SiegableType } from './Siegable'
import { PacketDispatcher } from '../../PacketDispatcher'
import { SiegeManager } from '../../instancemanager/SiegeManager'
import { L2PcInstance } from '../actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { DatabaseManager } from '../../../database/manager'
import { L2SiegeClansTableInfo } from '../../../database/interface/SiegeClansTableApi'
import { SiegeInfoForCastle } from '../../packets/send/SiegeInfo'
import { ClanCache } from '../../cache/ClanCache'
import _ from 'lodash'
import { SiegeClanType } from '../../enums/SiegeClanType'

export const enum SiegeValues {
    OWNER = -1,
    DEFENDER = 0,
    ATTACKER = 1,
    DEFENDER_NOT_APPROVED = 2,
}

export class Siege implements Siegable {
    controlTowerCount: number = 0
    attackerClans: Array<L2SiegeClan> = []
    defenderClans: Array<L2SiegeClan> = []
    defenderWaitingClans: Array<L2SiegeClan> = []

    // Castle setting
    controlTowers: Array<L2ControlTowerInstance>
    flameTowers: Array<L2FlameTowerInstance>
    castle: Castle = null
    isInProgressValue: boolean = false
    isNormalSide: boolean = true // true = Atk is Atk, false = Atk is Def
    isRegistrationOver: boolean = false
    siegeEndDate: number
    siegeGuardManager: SiegeGuardManager
    scheduledStartSiegeTask: any
    firstOwnerClanId: number = -1

    constructor( castle: Castle ) {
        this.castle = castle
        this.siegeGuardManager = new SiegeGuardManager( castle )

        // TODO : figure out startAutoTask()
    }

    getDefenderClan( clan: L2Clan ) {
        return _.find( this.defenderClans, ( currentClan : L2SiegeClan ) => {
            return currentClan && currentClan.getClanId() === clan.getId()
        } )
    }

    getFlag( clan: L2Clan ) {
        if ( clan ) {
            let siegeClan = this.getAttackerClan( clan )
            if ( siegeClan ) {
                return siegeClan.getFlag()
            }
        }

        return null
    }

    getAttackerClan( clan: L2Clan ): L2SiegeClan {
        return _.find( this.attackerClans, ( currentClan : L2SiegeClan ) => {
            return currentClan && currentClan.getClanId() === clan.getId()
        } )
    }

    checkIsAttacker( clan: L2Clan ): boolean {
        return false
    }

    checkIsDefender( clan: L2Clan ): boolean {
        return false
    }

    async endSiege(): Promise<void> {
    }

    getAttackerClans(): Array<L2SiegeClan> {
        if ( this.isNormalSide ) {
            return this.attackerClans
        }

        return this.defenderClans
    }

    getAttackersInZone(): Array<number> {
        return undefined
    }

    getDefenderClans(): Array<L2SiegeClan> {
        return undefined
    }

    getFameAmount(): number {
        return 0
    }

    getFameFrequency(): number {
        return 0
    }

    getSiegeDate(): number {
        return 0
    }

    giveFame(): boolean {
        return false
    }

    async startSiege(): Promise<void> {
    }

    updateSiege(): void {
    }

    checkIfInZone( x: number, y: number, z: number ) : boolean {
        return this.isInProgress() && this.getCastle().isInSiegeArea( x, y, z )
    }

    getCastle() : Castle {
        return this.castle
    }

    announceDataToPlayer( packet: Buffer, bothSides: boolean ) {
        let message = ( clans: Array<L2SiegeClan> ) => {
            _.each( clans, ( siegeClan: L2SiegeClan ) => {
                let clan : L2Clan = ClanCache.getClan( siegeClan.getClanId() )
                _.each( clan.getOnlineMembers( 0 ), ( memberId: number ) => {
                    PacketDispatcher.sendCopyData( memberId, packet )
                } )
            } )
        }

        message( this.getDefenderClans() )

        if ( bothSides ) {
            message( this.getAttackerClans() )
        }
    }

    async midVictory() : Promise<void> {
        if ( !this.isInProgress() ) {
            return
        }

        if ( this.getCastle().getOwnerId() > 0 ) {
            await this.siegeGuardManager.removeMercs() // Remove all merc entry from db
        }

        // If defender doesn't exist (Pc vs Npc) and only 1 attacker
        if ( this.getDefenderClans().length === 0 && ( this.getAttackerClans().length === 1 ) ) {
            let newOwner : L2SiegeClan = this.getAttackerClanById( this.getCastle().getOwnerId() )
            this.removeAttacker( newOwner )
            this.addDefenderClan( newOwner, SiegeClanType.Owner )
            await this.endSiege()
            return
        }

        let siege = this
        if ( this.getCastle().getOwnerId() > 0 ) {
            let allyId = ClanCache.getClan( this.getCastle().getOwnerId() ).getAllyId()
            // If defender doesn't exist (Pc vs Npc) and only an alliance attacks
            if ( this.getDefenderClans().length === 0 ) {
                // The player's clan is in an alliance
                if ( allyId !== 0 ) {
                    let isSameAlliance = !_.some( this.getAttackerClans(), ( siegeClan: L2SiegeClan ) => {
                        return siegeClan && ClanCache.getClan( siegeClan.getClanId() ).getAllyId() !== allyId
                    } )

                    if ( isSameAlliance ) {
                        let newOwner : L2SiegeClan = this.getAttackerClanById( this.getCastle().getOwnerId() )
                        this.removeAttacker( newOwner )
                        this.addDefenderClan( newOwner, SiegeClanType.Owner )
                        await this.endSiege()
                        return
                    }
                }
            }

            _.each( this.getDefenderClans(), ( siegeClan: L2SiegeClan ) => {
                if ( siegeClan ) {
                    siege.removeDefender( siegeClan )
                    siege.addAttackerClan( siegeClan )
                }
            } )

            let newOwner : L2SiegeClan = this.getAttackerClanById( this.getCastle().getOwnerId() )
            this.removeAttacker( newOwner )
            this.addDefenderClan( newOwner, SiegeClanType.Owner )

            _.each( ClanCache.getClanAllies( allyId ), ( clan: L2Clan ) => {
                let siegeClan : L2SiegeClan = siege.getAttackerClanById( clan.getId() )
                if ( siegeClan ) {
                    this.removeAttacker( siegeClan )
                    this.addDefenderClan( siegeClan, SiegeClanType.DefenderConfirmed )
                }
            } )

            // TODO : add implementation
            // this.teleportPlayer(SiegeTeleportWhoType.Attacker, TeleportWhereType.SIEGEFLAG); // Teleport to the second closest town
            // this.teleportPlayer(SiegeTeleportWhoType.Spectator, TeleportWhereType.TOWN); // Teleport to the second closest town
            //
            // this.removeDefenderFlags(); // Removes defenders' flags
            // this.getCastle().removeUpgrade(); // Remove all castle upgrade
            // this.getCastle().spawnDoor(true); // Respawn door to castle but make them weaker (50% hp)
            // this.removeTowers(); // Remove all towers from this castle
            // this.controlTowerCount = 0;// Each new siege midvictory CT are completely respawned.
            // this.spawnControlTower();
            // this.spawnFlameTower();
            // this.updatePlayerSiegeStateFlags(false)

            // TODO : send listener event
        }
    }

    getAttackerClanById( clanId: number ) {
        return _.find( this.getAttackerClans(), ( siegeClan: L2SiegeClan ) => {
            return siegeClan && siegeClan.getClanId() === clanId
        } )
    }

    removeAttacker( clan: L2SiegeClan ) {
        _.pull( this.getAttackerClans(), clan )
    }

    removeDefender( clan: L2SiegeClan ) {
        _.pull( this.getDefenderClans(), clan )
    }

    addDefenderClan( clan: L2SiegeClan, type: SiegeClanType ) {
        if ( !clan ) {
            return
        }

        clan.setType( type )
        this.getDefenderClans().push( clan )
    }

    addAttackerClan( clan: L2SiegeClan ) {
        if ( !clan ) {
            return
        }

        clan.setType( SiegeClanType.Attacker )
        this.getAttackerClans().push( clan )
    }

    getAttackerRespawnDelay() {
        return SiegeManager.getAttackerRespawnDelay()
    }

    getDefenderWaitingClans() {
        return this.defenderWaitingClans
    }

    async registerAttacker( player: L2PcInstance, isForced: boolean = false ) {
        if ( !player.getClan() ) {
            return
        }

        let allyId = 0
        if ( this.getCastle().getOwnerId() !== 0 ) {
            allyId = ClanCache.getClan( this.getCastle().getOwnerId() ).getAllyId()
        }

        if ( allyId !== 0 ) {
            if ( ( player.getClan().getAllyId() === allyId ) && !isForced ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_ATTACK_ALLIANCE_CASTLE ) )
                return
            }
        }

        if ( isForced ) {
            if ( await SiegeManager.checkIsRegistered( player.getClan(), this.getCastle().getResidenceId() ) ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ALREADY_REQUESTED_SIEGE_BATTLE ) )
            } else {
                await this.saveSiegeClan( player.getClan(), SiegeValues.ATTACKER, false )
            }
            return
        }

        if ( await this.checkIfCanRegister( player, SiegeValues.ATTACKER ) ) {
            await this.saveSiegeClan( player.getClan(), SiegeValues.ATTACKER, false )
        }
    }

    async saveSiegeClan( clan: L2Clan, type: number, shouldUpdateRegistration: boolean ) {
        if ( clan.getCastleId() > 0 ) {
            return
        }

        if ( ( type === SiegeValues.DEFENDER )
                || ( type === SiegeValues.DEFENDER_NOT_APPROVED )
                || ( type === SiegeValues.OWNER ) ) {
            if ( ( this.getDefenderClans().length + this.getDefenderWaitingClans().length ) >= SiegeManager.getDefenderMaxClans() ) {
                return
            }
        } else {
            if ( this.getAttackerClans().length >= SiegeManager.getAttackerMaxClans() ) {
                return
            }
        }

        if ( shouldUpdateRegistration ) {
            await DatabaseManager.getSiegeClans().addClan( clan.getId(), this.getCastle().getResidenceId(), type )
        } else {
            await DatabaseManager.getSiegeClans().updateClan( clan.getId(), this.getCastle().getResidenceId(), type )
        }

        if ( ( type === SiegeValues.DEFENDER ) || ( type === SiegeValues.OWNER ) ) {
            this.addDefender( clan.getId() )
        }

        if ( type === SiegeValues.ATTACKER ) {
            this.addAttacker( clan.getId() )
        }

        if ( type === SiegeValues.DEFENDER_NOT_APPROVED ) {
            this.addDefenderWaiting( clan.getId() )
        }
    }

    addAttacker( id: number ) {
        let clan = new L2SiegeClan( id, SiegeClanType.Attacker )
        this.attackerClans.push( clan )
    }

    addDefender( id: number ) {
        let clan = new L2SiegeClan( id, SiegeClanType.DefenderConfirmed )
        this.defenderClans.push( clan )
    }

    addDefenderWaiting( id: number ) {
        let clan = new L2SiegeClan( id, SiegeClanType.DefenderPending )
        this.defenderWaitingClans.push( clan )
    }

    async checkIfCanRegister( player: L2PcInstance, type: number ) {
        if ( this.getIsRegistrationOver() ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.DEADLINE_FOR_SIEGE_S1_PASSED )
                    .addCastleId( this.getCastle().getResidenceId() )
                    .getBuffer()
            player.sendOwnedData( packet )
            return false
        }

        if ( this.isInProgress() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_SIEGE_REGISTRATION_TIME2 ) )
            return false
        }

        if ( !player.getClan() || player.getClan().getLevel() < SiegeManager.getSiegeClanMinLevel() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ONLY_CLAN_LEVEL_5_ABOVE_MAY_SIEGE ) )
            return false
        }

        if ( player.getClan().getId() === this.getCastle().getOwnerId() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLAN_THAT_OWNS_CASTLE_IS_AUTOMATICALLY_REGISTERED_DEFENDING ) )
            return false
        }

        if ( player.getClan().getCastleId() > 0 ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLAN_THAT_OWNS_CASTLE_CANNOT_PARTICIPATE_OTHER_SIEGE ) )
            return false
        }

        if ( await SiegeManager.checkIsRegistered( player.getClan(), this.getCastle().getResidenceId() ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ALREADY_REQUESTED_SIEGE_BATTLE ) )
            return false
        }

        if ( this.checkIfAlreadyRegisteredForSameDay( player.getClan() ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.APPLICATION_DENIED_BECAUSE_ALREADY_SUBMITTED_A_REQUEST_FOR_ANOTHER_SIEGE_BATTLE ) )
            return false
        }

        if ( ( type === SiegeValues.ATTACKER ) && ( this.getAttackerClans().length >= SiegeManager.getAttackerMaxClans() ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ATTACKER_SIDE_FULL ) )
            return false
        }

        if ( ( ( type === SiegeValues.DEFENDER ) || ( type === SiegeValues.DEFENDER_NOT_APPROVED ) || ( type === SiegeValues.OWNER ) )
                && ( ( this.getDefenderClans().length + this.getDefenderWaitingClans().length ) >= SiegeManager.getDefenderMaxClans() ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.DEFENDER_SIDE_FULL ) )
            return false
        }

        return true
    }

    getIsRegistrationOver() {
        return this.isRegistrationOver
    }

    checkIfAlreadyRegisteredForSameDay( clan: L2Clan ) : boolean {
        let currentSiege = this
        return _.some( SiegeManager.getSieges(), ( siege: Siege ) => {
            if ( siege === currentSiege ) {
                return false
            }

            if ( new Date( siege.getSiegeDate() ).getDay() === new Date( currentSiege.getSiegeDate() ).getDay() ) {
                if ( siege.checkIsAttacker( clan ) ) {
                    return true
                }
                if ( siege.checkIsDefender( clan ) ) {
                    return true
                }
                if ( siege.checkIsDefenderWaiting( clan ) ) {
                    return true
                }
            }

            return false
        } )
    }

    checkIsDefenderWaiting( clan: L2Clan ) : boolean {
        return !!this.getDefenderWaitingClan( clan.getId() )
    }

    getDefenderWaitingClan( clanId: number ) {
        return _.find( this.defenderWaitingClans, ( siegeClan: L2SiegeClan ) => {
            return siegeClan && siegeClan.getClanId() === clanId
        } )
    }

    async registerDefender( player: L2PcInstance, shouldForce: boolean = false ) {
        if ( this.getCastle().getOwnerId() <= 0 ) {
            player.sendMessage( `You cannot register as a defender because ${this.getCastle().getName()} is owned by NPC.` )
            return
        }

        if ( shouldForce ) {
            if ( await SiegeManager.checkIsRegistered( player.getClan(), this.getCastle().getResidenceId() ) ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ALREADY_REQUESTED_SIEGE_BATTLE ) )
            } else {
                await this.saveSiegeClan( player.getClan(), SiegeValues.DEFENDER_NOT_APPROVED, false )
            }

            return
        }

        if ( await this.checkIfCanRegister( player, SiegeValues.DEFENDER_NOT_APPROVED ) ) {
            await this.saveSiegeClan( player.getClan(), SiegeValues.DEFENDER_NOT_APPROVED, false )
        }
    }

    async removeSiegeClan( clan: L2Clan ) {
        if ( !clan
                || clan.getCastleId() === this.getCastle().getResidenceId()
                || !( await SiegeManager.checkIsRegistered( clan, this.getCastle().getResidenceId() ) )
                || clan.getId() <= 0 ) {
            return
        }

        await DatabaseManager.getSiegeClans().deleteClan( clan.getId(), this.getCastle().getResidenceId() )
    }

    async approveSiegeDefenderClan( clanId: number ) {
        if ( clanId <= 0 ) {
            return
        }

        await this.saveSiegeClan( ClanCache.getClan( clanId ), SiegeValues.DEFENDER, true )
        await this.loadSiegeClan()
    }

    async loadSiegeClan() {
        this.defenderClans = []
        this.defenderWaitingClans = []
        this.attackerClans = []

        if ( this.getCastle().getOwnerId() > 0 ) {
            this.addDefenderWithType( this.getCastle().getOwnerId(), SiegeClanType.Owner )
        }

        let records : Array<L2SiegeClansTableInfo> = await DatabaseManager.getSiegeClans().clanInfoForCastle( this.getCastle().getResidenceId() )

        let siege = this
        _.each( records, ( record: L2SiegeClansTableInfo ) => {
            switch ( record.type ) {
                case SiegeValues.DEFENDER:
                    siege.addDefender( record.clanId )
                    break
                case SiegeValues.ATTACKER:
                    siege.addAttacker( record.clanId )
                    break
                case SiegeValues.DEFENDER_NOT_APPROVED:
                    siege.addDefenderWaiting( record.clanId )
                    break
            }
        } )
    }

    addDefenderWithType( clanId: number, type: SiegeClanType ) {
        this.defenderClans.push( new L2SiegeClan( clanId, type ) )
    }

    async saveSiegeDate() {
        if ( this.scheduledStartSiegeTask ) {
            clearTimeout( this.scheduledStartSiegeTask )
            this.scheduledStartSiegeTask = setTimeout( this.runScheduleStartSiegeTask.bind( this ), 1000, this.getCastle() )
        }

        return DatabaseManager.getCastleTable().updateSiegeInfo( this.getCastle().getResidenceId(), this.getSiegeDate(), this.getTimeRegistrationOverDate(), this.getIsRegistrationOver() )
    }

    runScheduleStartSiegeTask() {

    }

    getTimeRegistrationOverDate() : number {
        return this.getCastle().getTimeRegistrationOverDate()
    }

    isInstance( type: SiegableType ): boolean {
        return type === SiegableType.Siege
    }

    isInProgress() : boolean {
        return this.isInProgressValue
    }

    getControlTowerCount() {
        return this.controlTowerCount
    }

    listRegisterClan( player: L2PcInstance ) {
        player.sendOwnedData( SiegeInfoForCastle( this.getCastle(), player ) )
    }

    isParticipating( clan: L2Clan ) : boolean {
        return false
    }
}