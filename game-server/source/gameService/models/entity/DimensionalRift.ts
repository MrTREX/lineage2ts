import { L2Party } from '../L2Party'
import { L2PcInstance } from '../actor/instance/L2PcInstance'
import { ConfigManager } from '../../../config/ConfigManager'
import { L2World } from '../../L2World'
import { DimensionalRiftManager } from '../../instancemanager/DimensionalRiftManager'
import { ListenerManager } from '../../instancemanager/ListenerManager'
import { ListenerLogic } from '../ListenerLogic'
import { QuestState } from '../quest/QuestState'
import { Earthquake } from '../../packets/send/Earthquake'
import { Location } from '../Location'
import { L2Npc } from '../actor/L2Npc'
import { QuestStateCache } from '../../cache/QuestStateCache'
import Timeout = NodeJS.Timeout
import _ from 'lodash'

export class DimensionalRift {
    type: number
    party: L2Party
    completedRooms: Array<number> = []
    jumpsTotal: number

    teleporterTimerTask: Timeout
    spawnTimerTask: Timeout
    earthQuakeTask: Timeout

    currentRoom: number
    hasJumped: boolean
    deadPlayers: Array<number> = []// object ids for L2PcInstance
    revivedInWaitingRoom: Array<number> = []// object ids in L2PcInstance
    isBossRoom: boolean

    constructor( party: L2Party, type: number, room: number ) {
        DimensionalRiftManager.getRoom( type, room ).setPartyInside( true )

        this.type = type
        this.party = party
        this.currentRoom = room
        let coords : Location = this.getRoomCoordinates( room )
        party.setDimensionalRift( this )

        party.getMembers().forEach( ( memberId: number ) => {
            let member : L2PcInstance = L2World.getPlayer( memberId )

            if ( !member ) {
                return
            }

            let riftQuest : ListenerLogic = ListenerManager.getQuest( 635 )
            if ( riftQuest ) {
                let state : QuestState = QuestStateCache.getQuestState( memberId, riftQuest.getName(), true )
                if ( !state.isStarted() ) {
                    state.startQuest()
                }
            }

            member.teleportToLocation( coords, true )
        } )

        this.createSpawnTimer( this.currentRoom )
        this.createTeleportTimer( true )
    }

    calculateTimeToNextJump(): number {
        let time = _.random( ConfigManager.general.getAutoJumpsDelayMin(), ConfigManager.general.getAutoJumpsDelayMax() )
        if ( this.isBossRoom ) {
            return Math.floor( time * ConfigManager.general.getBossRoomTimeMultiply() )
        }

        return time
    }

    checkBossRoom( room: number ) {
        this.isBossRoom = DimensionalRiftManager.getRoom( this.type, room ).isBossRoom
    }

    cleanUp(): void {
        this.clearSpawnTask()
        this.clearTeleporterTimerTask()
        this.clearEarthquakeTask()
    }

    clearEarthquakeTask() {
        if ( this.earthQuakeTask ) {
            clearTimeout( this.earthQuakeTask )
            this.earthQuakeTask = null
        }
    }

    clearSpawnTask() {
        if ( this.spawnTimerTask ) {
            clearTimeout( this.spawnTimerTask )
            this.spawnTimerTask = null
        }
    }

    clearTeleporterTimerTask() {
        if ( this.teleporterTimerTask ) {
            clearTimeout( this.teleporterTimerTask )
            this.teleporterTimerTask = null
        }
    }

    createSpawnTimer( room: number ) {
        if ( this.spawnTimerTask ) {
            clearTimeout( this.spawnTimerTask )
            this.spawnTimerTask = null
        }

        this.spawnTimerTask = setTimeout( this.runSpawnTask.bind( this ), ConfigManager.general.getRiftSpawnDelay(), room )
    }

    createTeleportTimer( teleportReason: boolean ) {
        if ( !this.party ) {
            return
        }

        this.clearTeleporterTimerTask()
        this.clearEarthquakeTask()

        if ( teleportReason ) {
            let jumpTime = this.calculateTimeToNextJump()
            this.teleporterTimerTask = setTimeout( this.runTeleporterTask.bind( this ), jumpTime, teleportReason ) // Teleporter task, 8-10 minutes
            this.earthQuakeTask = setTimeout( this.runEarthquakeTask.bind( this ), jumpTime - 7000 )
            return
        }

        this.teleporterTimerTask = setTimeout( this.runTeleporterTask.bind( this ), 5000, teleportReason )
    }

    getMaxJumps() {
        if ( ( ConfigManager.general.getMaxRiftJumps() <= 8 ) && ( ConfigManager.general.getMaxRiftJumps() >= 1 ) ) {
            return ConfigManager.general.getMaxRiftJumps()
        }

        return 4
    }

    getRevivedAtWaitingRoom(): Array<number> {
        return this.revivedInWaitingRoom
    }

    getRoomCoordinates( room: number ): Location {
        return DimensionalRiftManager.getRoom( this.type, room ).getTeleportCoordinates()
    }

    killRift(): void {
        this.completedRooms = []

        if ( this.party ) {
            this.party.setDimensionalRift( null )
        }

        this.party = null
        this.revivedInWaitingRoom = []
        this.deadPlayers = []

        if ( this.earthQuakeTask ) {
            clearTimeout( this.earthQuakeTask )
            this.earthQuakeTask = null
        }

        DimensionalRiftManager.getRoom( this.type, this.currentRoom ).unspawn().setPartyInside( false )

        this.cleanUp()
    }

    manualTeleport( player: L2PcInstance, npc: L2Npc ) {
        if ( !player.isInParty() || !player.getParty().isInDimensionalRift() ) {
            return
        }

        if ( player.getObjectId() !== player.getParty().getLeaderObjectId() ) {
            DimensionalRiftManager.showHtml( player, 'data/html/seven_signs/rift/NotPartyLeader.htm', npc )
            return
        }

        if ( this.hasJumped ) {
            DimensionalRiftManager.showHtml( player, 'data/html/seven_signs/rift/AlreadyTeleported.htm', npc )
            return
        }

        this.hasJumped = true
        DimensionalRiftManager.getRoom( this.type, this.currentRoom ).unspawn().setPartyInside( false )
        this.completedRooms.push( this.currentRoom )
        this.currentRoom = -1

        this.party.getMembers().forEach( ( memberId: number ) => {
            this.teleportToNextRoom( L2World.getPlayer( memberId ) )
        } )

        DimensionalRiftManager.getRoom( this.type, this.currentRoom ).setPartyInside( true )

        this.createSpawnTimer( this.currentRoom )
        this.createTeleportTimer( true )
    }

    memberResurrected( player: L2PcInstance ) {
        _.pull( this.deadPlayers, player.getObjectId() )
    }

    partyMemberExited( player: L2PcInstance ) {
        let objectId = player.getObjectId()
        _.pull( this.deadPlayers, objectId )
        _.pull( this.revivedInWaitingRoom, objectId )

        let rift = this
        if ( ( this.party.getMemberCount() < ConfigManager.general.getRiftMinPartySize() ) || ( this.party.getMemberCount() === 1 ) ) {
            this.party.getMembers().forEach( ( memberId: number ) => {
                let player = L2World.getPlayer( memberId )
                rift.teleportToWaitingRoom( player )
            } )
            rift.killRift()
        }
    }

    partyMemberInvited(): void {
        return this.createTeleportTimer( false )
    }

    runEarthquakeTask() {
        let rift = this
        _.each( this.party.getMembers(), ( memberId: number ) => {
            if ( rift.revivedInWaitingRoom.includes( memberId ) ) {
                let player = L2World.getPlayer( memberId )
                player.sendOwnedData( Earthquake( player.getX(), player.getY(), player.getZ(), 65, 9 ) )
            }
        } )
    }

    runSpawnTask( room: number ) {
        DimensionalRiftManager.getRoom( this.type, room ).spawn()
    }

    runTeleporterTask( teleportReason: boolean ) {
        if ( this.currentRoom > -1 ) {
            DimensionalRiftManager.getRoom( this.type, this.currentRoom ).unspawn().setPartyInside( false )
        }

        if ( teleportReason && ( this.jumpsTotal < this.getMaxJumps() ) && ( this.party.getMemberCount() > this.deadPlayers.length ) ) {
            this.jumpsTotal++

            this.completedRooms.push( this.currentRoom )
            this.currentRoom = -1

            let rift = this
            _.each( this.party.getMembers(), ( memberId: number ) => {
                if ( rift.revivedInWaitingRoom.includes( memberId ) ) {
                    let player = L2World.getPlayer( memberId )
                    rift.teleportToNextRoom( player )
                }
            } )

            this.createTeleportTimer( true )
            this.createSpawnTimer( this.currentRoom )
            return
        }

        let rift = this
        _.each( this.party.getMembers(), ( memberId: number ) => {
            if ( rift.revivedInWaitingRoom.includes( memberId ) ) {
                let player = L2World.getPlayer( memberId )
                rift.teleportToWaitingRoom( player )
            }
        } )

        this.killRift()
    }

    teleportToNextRoom( player: L2PcInstance ): void {
        if ( this.currentRoom === -1 ) {
            do {
                let emptyRooms: Array<number> = DimensionalRiftManager.getFreeRooms( this.type )
                // Do not tp in the same room a second time
                let incompleteRooms = _.difference( emptyRooms, this.completedRooms )
                // If no room left, find any empty
                if ( incompleteRooms.length === 0 ) {
                    incompleteRooms = emptyRooms
                }
                this.currentRoom = _.sample( emptyRooms )
            }
            while ( DimensionalRiftManager.getRoom( this.type, this.currentRoom ).isPartyInside )
        }

        DimensionalRiftManager.getRoom( this.type, this.currentRoom ).setPartyInside( true )
        this.checkBossRoom( this.currentRoom )
        player.teleportToLocation( this.getRoomCoordinates( this.currentRoom ), false )
    }

    teleportToWaitingRoom( player: L2PcInstance ): void {
        DimensionalRiftManager.teleportToWaitingRoom( player )

        let riftQuest: ListenerLogic = ListenerManager.getQuest( 635 )
        if ( riftQuest ) {
            let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), riftQuest.getName() )
            if ( state && state.isCondition( 1 ) ) {
                state.exitQuest( true, true )
            }
        }
    }

    manualExitRift( player: L2PcInstance, npc: L2Npc ) : void {
        if ( !player.isInParty() || !player.getParty().isInDimensionalRift() ) {
            return
        }

        if ( player.getObjectId() !== player.getParty().getLeaderObjectId() ) {
            DimensionalRiftManager.showHtml( player, 'data/html/seven_signs/rift/NotPartyLeader.htm', npc )
            return
        }


        this.party.getMembers().forEach( ( memberId: number ) => {
            let player = L2World.getPlayer( memberId )

            if ( player ) {
                this.teleportToWaitingRoom( player )
            }
        } )

        this.killRift()
    }

    getCurrentRoom() {
        return this.currentRoom
    }

    getType() {
        return this.type
    }
}