import { L2Clan } from '../L2Clan'

export class ClanInfo {
    clan: L2Clan
    total: number
    online: number

    constructor( clan: L2Clan ) {
        this.clan = clan
        this.total = clan.getMembersCount()
        this.online = clan.getOnlineMembersCount()
    }
}