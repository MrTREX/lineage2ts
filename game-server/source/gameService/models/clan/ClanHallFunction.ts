import { ClanHallFunctionType } from '../../enums/clanhall/ClanHallFunctionType'

export interface ClanHallFunction {
    hallId: number
    type: ClanHallFunctionType
    level: number
    fee: number
    chargeTimeInterval: number
    endDate: number
}