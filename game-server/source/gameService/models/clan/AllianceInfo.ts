import { ClanInfo } from './ClanInfo'
import { L2Clan } from '../L2Clan'
import { ClanCache } from '../../cache/ClanCache'

export class AllianceInfo {
    name: string
    total: number = 0
    online: number = 0
    superiorLeader: string
    subordinateLeader: string
    allies: Array<ClanInfo>

    constructor( allianceId: number ) {
        let leader : L2Clan = ClanCache.getClan( allianceId )
        this.name = leader.getAllyName()
        this.subordinateLeader = leader.getName()
        this.superiorLeader = leader.getLeaderName()

        let allies : Array<L2Clan> = ClanCache.getClanAllies( allianceId )

        let alliance = this
        this.allies = allies.map( ( clan: L2Clan ) => {
            let info = new ClanInfo( clan )

            alliance.total += info.total
            alliance.online += info.online

            return info
        } )
    }
}