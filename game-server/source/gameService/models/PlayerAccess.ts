import { PlayerPermission } from '../enums/PlayerPermission'

export interface CommandPermissions {
    allowed: RegExp
    restricted: RegExp
    confirmation: RegExp
}

export const AllCommands : CommandPermissions = {
    allowed: new RegExp( /./ ),
    restricted: null,
    confirmation: null
}

export class PlayerAccess {
    private commands: CommandPermissions
    private permissions: Set<PlayerPermission>
    readonly titleColor: number
    readonly nameColor: number
    readonly level: number
    readonly name: string

    constructor( name: string,
                 level: number,
                 titleColor: number,
                 nameColor: number,
                 permissions: Set<PlayerPermission>,
                 commands: CommandPermissions ) {
        this.name = name
        this.level = level
        this.titleColor = titleColor
        this.nameColor = nameColor
        this.permissions = permissions
        this.commands = commands
    }

    requiresConfirmation( command: string ) : boolean {
        return this.commands.confirmation && this.commands.confirmation.test( command )
    }

    canExecute( command: string ) : boolean {
        if ( this.commands.restricted && this.commands.restricted.test( command ) ) {
            return false
        }

        return this.commands.allowed && this.commands.allowed.test( command )
    }

    hasPermission( permission : PlayerPermission ) : boolean {
        return this.permissions.has( permission )
    }
}