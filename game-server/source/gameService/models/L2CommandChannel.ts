import { BasicPlayerGroup } from './BasicPlayerGroup'
import { L2Party } from './L2Party'
import { ExCloseMPCC } from '../packets/send/ExCloseMPCC'
import { SystemMessageBuilder } from '../packets/send/SystemMessage'
import { SystemMessageIds } from '../packets/SystemMessageIdValues'
import { ExMPCCPartyInfoUpdate, MPCCPartyInfoMode } from '../packets/send/ExMPCCPartyInfoUpdate'
import { L2World } from '../L2World'
import { L2PcInstance } from './actor/instance/L2PcInstance'
import { L2Object } from './L2Object'
import { ConfigManager } from '../../config/ConfigManager'
import { L2Character } from './actor/L2Character'
import { ExOpenMPCC } from '../packets/send/ExOpenMPCC'
import { PlayerGroupType } from '../enums/PlayerGroupType'
import { BlocklistCache } from '../cache/BlocklistCache'
import { PacketDispatcher } from '../PacketDispatcher'
import _ from 'lodash'

export class L2CommandChannel extends BasicPlayerGroup {
    parties: Array<L2Party>
    commandLeader: number
    channelLevel: number = 0

    constructor( leader: L2PcInstance ) {
        super()
        this.commandLeader = leader.getObjectId()

        let party : L2Party = leader.getParty()

        this.parties.push( party )
        this.channelLevel = party.getLevel()

        party.setCommandChannel( this )
        party.broadcastPacket( SystemMessageBuilder.fromMessageId( SystemMessageIds.COMMAND_CHANNEL_FORMED ) )
        party.broadcastPacket( ExOpenMPCC() )
    }

    getType(): PlayerGroupType {
        return PlayerGroupType.CommandChannel
    }

    getMembers() : Array<number> {
        let partyMembers = _.map( this.parties, ( party: L2Party ) => party.getMembers() )
        return _.flatten( partyMembers )
    }

    getParties() : Array<L2Party> {
        return this.parties
    }

    getLevel() {
        return this.channelLevel
    }

    removeParty( party: L2Party ) : void {
        if ( !party ) {
            return
        }

        _.pull( this.parties, party )
        this.channelLevel = _.reduce( this.parties, ( channelLevel, currentParty: L2Party ) => {
            if ( currentParty.getLevel() > channelLevel ) {
                channelLevel = currentParty.getLevel()
            }

            return channelLevel
        }, 0 )

        party.setCommandChannel( null )
        party.broadcastPacket( ExCloseMPCC() )

        if ( this.parties.length < 2 ) {
            this.broadcastPacket( SystemMessageBuilder.fromMessageId( SystemMessageIds.COMMAND_CHANNEL_DISBANDED ) )
            return this.disbandChannel()
        }

        this.broadcastPacket( ExMPCCPartyInfoUpdate( party, MPCCPartyInfoMode.Remove ) )
    }

    broadcastPacket( packet: Buffer ) : void {
        _.each( this.parties, ( party: L2Party ) => {
            party.broadcastPacket( packet )
        } )
    }

    disbandChannel() : void {
        let commandChannel = this
        _.each( this.parties, ( party: L2Party ) => {
            commandChannel.removeParty( party )
        } )

        this.parties = []
    }

    getLeader() : L2PcInstance {
        return L2World.getPlayer( this.commandLeader )
    }

    meetRaidWarCondition( character: L2Object ) {
        if ( !( character.isCharacter() && ( character as L2Character ).isRaid() ) ) {
            return false
        }

        return this.getMemberCount() >= ConfigManager.character.getRaidLootRightsCCSize()
    }

    includesPlayer( player: L2PcInstance ) : boolean {
        return this.getMembers().includes( player.objectId )
    }

    getLeaderObjectId() {
        return this.commandLeader
    }

    addParty( party: L2Party ) {
        if ( !party ) {
            return
        }

        this.broadcastPacket( ExMPCCPartyInfoUpdate( party, MPCCPartyInfoMode.Add ) )

        this.parties.push( party )
        if ( party.getLevel() > this.channelLevel ) {
            this.channelLevel = party.getLevel()
        }

        party.setCommandChannel( this )
        party.broadcastPacket( SystemMessageBuilder.fromMessageId( SystemMessageIds.JOINED_COMMAND_CHANNEL ) )
        party.broadcastPacket( ExOpenMPCC() )
    }

    broadcastBlockedDataToPartyMembers( packet: Buffer, player: L2PcInstance ) : void {
        _.each( this.parties, ( party: L2Party ) => {
            _.each( party.getMembers(), ( playerId: number ) => {
                if ( !BlocklistCache.isBlocked( playerId, player.getObjectId() ) ) {
                    PacketDispatcher.sendCopyData( playerId, packet )
                }
            } )
        } )
    }

    setLeader( leader: L2PcInstance ): void {
        this.commandLeader = leader.getObjectId()
        this.channelLevel = Math.max( this.channelLevel, leader.getLevel() )
    }
}