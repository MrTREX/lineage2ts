const defaultEmptySet = new Set<number>()

export class L2Henna {
    dyeId: number
    dyeName: string
    dyeItemId: number
    str: number
    con: number
    dex: number
    int: number
    men: number
    wit: number
    wearFee: number
    wearCount: number
    cancelFee: number
    cancelCount: number
    wearClassIds: Set<number> = defaultEmptySet

    getStatSTR() {
        return this.str
    }

    getStatCON() {
        return this.con
    }

    getStatDEX() {
        return this.dex
    }

    getStatINT() {
        return this.int
    }

    getStatMEN() {
        return this.men
    }

    getStatWIT() {
        return this.wit
    }

    getDyeId() {
        return this.dyeId
    }

    isAllowedClass( classId: number ) : boolean {
        return this.wearClassIds.has( classId )
    }

    getDyeItemId() {
        return this.dyeItemId
    }

    getWearCount() {
        return this.wearCount
    }

    getWearFee() {
        return this.wearFee
    }

    getCancelCount() {
        return this.cancelCount
    }

    getCancelFee() {
        return this.cancelFee
    }
}