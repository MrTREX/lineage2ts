import { L2Item } from './L2Item'
import { ArmorType, ArmorTypeMask } from '../../enums/items/ArmorType'
import { Skill } from '../Skill'
import { ItemInstanceType } from './ItemInstanceType'
import { ItemType1 } from '../../enums/items/ItemType1'
import { ItemType2 } from '../../enums/items/ItemType2'
import _ from 'lodash'
import { L2ItemSlots } from '../../enums/L2ItemSlots'

export class L2Armor extends L2Item {
    enchant4Skill: Skill
    type: ArmorType

    constructor() {
        super()
        this.instanceType = ItemInstanceType.L2Armor
    }

    getItemType(): ArmorType {
        return this.type
    }

    getItemMask(): number {
        return ArmorTypeMask( this.getItemType() )
    }

    getEnchant4Skill(): Skill {
        return this.enchant4Skill
    }

    setDefaults() {
        let bodyPart = this.getBodyPart()
        if ( ( bodyPart === L2ItemSlots.Neck )
            || ( ( bodyPart & L2ItemSlots.LeftEar ) !== 0 )
            || ( ( bodyPart & L2ItemSlots.LeftFingers ) !== 0 )
            || ( ( bodyPart & L2ItemSlots.RightBracelet ) !== 0 )
            || ( ( bodyPart & L2ItemSlots.LeftBracelet ) !== 0 ) ) {
            this.type1 = ItemType1.WEAPON_RING_EARRING_NECKLACE
            this.type2 = ItemType2.ACCESSORY

            return
        }

        if ( this.type === ArmorType.NONE && bodyPart === L2ItemSlots.LeftHand ) {
            this.type = ArmorType.SHIELD
        }

        this.type1 = ItemType1.SHIELD_ARMOR
        this.type2 = ItemType2.SHIELD_ARMOR
    }

    getItemVariety() : string {
        return _.capitalize( ArmorType[ this.type ] )
    }
}