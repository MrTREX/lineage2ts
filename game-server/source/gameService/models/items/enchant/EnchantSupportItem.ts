import { AbstractEnchantItem } from './AbstractEnchantItem'
import { EtcItemType } from '../../../enums/items/EtcItemType'

export class EnchantSupportItem extends AbstractEnchantItem {
    isWeaponValue: boolean

    isWeapon() : boolean {
        return this.isWeaponValue
    }

    setFlags() {
        this.isWeaponValue = this.getItem().getItemType() === EtcItemType.SCRL_INC_ENCHANT_PROP_WP
    }
}