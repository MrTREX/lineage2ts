import _ from 'lodash'
import { RangeChanceDefinition } from '../../../interface/RangeChanceDefinition'

export class EnchantItemGroup {
    chances: Array<RangeChanceDefinition> = []
    name: string

    getChance( index: number ) : number {
        let holder : RangeChanceDefinition = this.chances.find( ( chance: RangeChanceDefinition ) => {
            return chance.min <= index && chance.max >= index
        } )

        if ( !holder ) {
            holder = _.last( this.chances )
        }

        return holder ? holder.chance : -1
    }
}