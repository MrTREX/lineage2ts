import { EnchantRateItem } from './EnchantRateItem'
import { L2Item } from '../L2Item'

export class EnchantScrollGroup {
    id: number
    rates: Array<EnchantRateItem> = []

    getRateGroup( item: L2Item ) : EnchantRateItem {
        return this.rates.find( ( rate: EnchantRateItem ) => {
            return rate.validate( item )
        } )
    }
}