import { EnchantOptionsParameters } from '../options/EnchantOptions'

export interface ElementalAndEnchantProperties {
    getAttackElementType(): number
    getAttackElementPower(): number
    getEnchantOptions(): EnchantOptionsParameters

    elementalDefenceAttributes : ReadonlyArray<number>
}