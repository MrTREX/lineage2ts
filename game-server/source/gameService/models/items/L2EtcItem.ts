import { L2Item } from './L2Item'
import { EtcItemType } from '../../enums/items/EtcItemType'
import { L2ExtractableProduct } from '../L2ExtractableProduct'
import { ItemInstanceType } from './ItemInstanceType'
import { ItemHandlerMethod } from '../../handler/ItemHandler'
import _ from 'lodash'
import { ItemType1 } from '../../enums/items/ItemType1'
import { ItemType2 } from '../../enums/items/ItemType2'

export class L2EtcItem extends L2Item {
    type: EtcItemType
    extractableItems: Array<L2ExtractableProduct> = []
    itemHandler: ItemHandlerMethod

    constructor() {
        super()
        this.instanceType = ItemInstanceType.L2EtcItem

        this.type1 = ItemType1.ITEM_QUESTITEM_ADENA
        this.type2 = ItemType2.OTHER
    }

    getItemType(): EtcItemType {
        return this.type
    }

    getExtractableItems() : Array<L2ExtractableProduct> {
        return this.extractableItems
    }

    getItemHandler() : ItemHandlerMethod {
        return this.itemHandler
    }

    getItemVariety() : string {
        return _.capitalize( EtcItemType[ this.type ] )
    }
}