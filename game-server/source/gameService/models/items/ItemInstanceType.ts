export const enum ItemInstanceType {
    L2Armor,
    L2EtcItem,
    L2Weapon,
    L2Item
}