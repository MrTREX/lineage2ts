import { TraitTypeValues } from '../../stats/TraitType'
import _ from 'lodash'

export enum WeaponType {
    SWORD,
    BLUNT,
    DAGGER,
    BOW,
    POLE,
    NONE,
    DUAL,
    ETC,
    FIST,
    DUALFIST,
    FISHINGROD,
    RAPIER,
    ANCIENTSWORD,
    CROSSBOW,
    FLAG,
    OWNTHING,
    DUALDAGGER
}

export const WeaponTypeMap = {
    [ WeaponType.SWORD ]: TraitTypeValues.SWORD,
    [ WeaponType.BLUNT ]: TraitTypeValues.BLUNT,
    [ WeaponType.DAGGER ]: TraitTypeValues.DAGGER,
    [ WeaponType.BOW ]: TraitTypeValues.BOW,
    [ WeaponType.POLE ]: TraitTypeValues.POLE,
    [ WeaponType.NONE ]: TraitTypeValues.NONE,
    [ WeaponType.DUAL ]: TraitTypeValues.DUAL,
    [ WeaponType.ETC ]: TraitTypeValues.ETC,
    [ WeaponType.FIST ]: TraitTypeValues.FIST,
    [ WeaponType.DUALFIST ]: TraitTypeValues.DUALFIST,
    [ WeaponType.FISHINGROD ]: TraitTypeValues.NONE,
    [ WeaponType.RAPIER ]: TraitTypeValues.RAPIER,
    [ WeaponType.ANCIENTSWORD ]: TraitTypeValues.ANCIENTSWORD,
    [ WeaponType.CROSSBOW ]: TraitTypeValues.CROSSBOW,
    [ WeaponType.FLAG ]: TraitTypeValues.NONE,
    [ WeaponType.OWNTHING ]: TraitTypeValues.NONE,
    [ WeaponType.DUALDAGGER ]: TraitTypeValues.DUALDAGGER
}

const WeaponTypeMaskValues : Array<number> = _.times( Object.keys( WeaponType ).length, ( value: number ) => {
    return 1 << value
} )

export function WeaponTypeMask( value: WeaponType ) {
    return WeaponTypeMaskValues[ value ]
}