export class SeedProduction {
    seedId: number
    price: number
    startAmount: number
    amount: number

    constructor( seedId: number, amount: number, price: number, startAmount: number ) {
        this.seedId = seedId
        this.amount = amount
        this.price = price
        this.startAmount = startAmount
    }

    decreaseAmount( value: number ): boolean {
        if ( ( this.amount - value ) < 0 ) {
            return false
        }

        this.amount -= value

        return false
    }

    getAmount() {
        return this.amount
    }

    getId() {
        return this.seedId
    }

    getPrice() {
        return this.price
    }

    getStartAmount() {
        return this.startAmount
    }
}