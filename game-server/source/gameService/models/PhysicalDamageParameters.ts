import { L2Character } from './actor/L2Character'

export interface HitParameters {
    target: L2Character
    damage: number
    isMiss: boolean
    isCrit: boolean
}

export interface PhysicalDamageParameters {
    hitDelay: number
    hitTimeout: any // TS does not like normal Timeout type here
    hits: Array<HitParameters>
    attackTimeout: any
    attackDelay: number
    isActive: boolean
}

export function getDefaultDamageParameters() : PhysicalDamageParameters {
    return {
        attackDelay: 0,
        attackTimeout: undefined,
        isActive: false,
        hitDelay: 0,
        hits: [],
        hitTimeout: undefined
    }
}