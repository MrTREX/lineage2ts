import { DataManager } from '../../data/manager'

export function createManufactureItem( recipeId: number, adenaCost: number ) : L2ManufactureItem {
    return {
        recipeId,
        adenaCost,
        isCommon: DataManager.getRecipeData().getRecipeList( recipeId ).isCommon
    }
}

export interface L2ManufactureItem {
    recipeId: number
    adenaCost: number
    isCommon: boolean
}