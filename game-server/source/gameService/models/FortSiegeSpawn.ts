import { Location } from './Location'

export class FortSiegeSpawn extends Location {
    npcId: number
    fortId: number
    id: number

    getId() {
        return this.npcId
    }

    getMessageId() {
        return this.id
    }
}