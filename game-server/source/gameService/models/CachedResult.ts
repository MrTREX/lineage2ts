export type CachedResultMethod<Type> = () => Type
export type AwaitedCachedResultMethod<Type> = () => Promise<Type>

export class CachedResult<Type> {
    regenerateTime: number = 0
    method: CachedResultMethod<Type>
    result: Type
    interval: number

    constructor( method : CachedResultMethod<Type>, interval: number ) {
        this.method = method
        this.interval = interval
    }

    getResult() : Type {
        let currentTime = Date.now()
        if ( this.regenerateTime < currentTime ) {
            this.regenerateTime = currentTime + this.interval
            this.result = this.method()
        }

        return this.result
    }
}

export class AwaitedCachedResult<Type> {
    regenerateTime: number = 0
    method: AwaitedCachedResultMethod<Type>
    result: Type
    interval: number

    constructor( method : AwaitedCachedResultMethod<Type>, interval: number ) {
        this.method = method
        this.interval = interval
    }

    async getResult() : Promise<Type> {
        let currentTime = Date.now()
        if ( this.regenerateTime < currentTime ) {
            this.regenerateTime = currentTime + this.interval
            this.result = await this.method()
        }

        return this.result
    }
}