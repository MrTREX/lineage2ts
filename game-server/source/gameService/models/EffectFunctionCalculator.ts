import { AbstractFunction } from './stats/functions/AbstractFunction'
import _ from 'lodash'

export type L2CharacterCalculatorFunctions = Record<string, Array<AbstractFunction>>
export type L2CharacterCalculatorTime = Record<string, number>

export class EffectFunctionCalculator {
    calculators: L2CharacterCalculatorFunctions = {}
    updateTimes: L2CharacterCalculatorTime = {}

    removeOwner( owner: Object, existingStatNames: Set<string> ): Set<string> {
        return _.reduce( this.calculators, ( names: Set<string>, propertyCalculators: Array<AbstractFunction>, key: string ) => {
            let currentSize = propertyCalculators.length

            this.calculators[ key ] = propertyCalculators.filter( ( method: AbstractFunction ) => {
                let matchedOwner: boolean = method.getFunctionOwner() === owner
                if ( matchedOwner ) {
                    names.add( method.getStat() )
                }

                return !matchedOwner
            } )

            if ( currentSize !== this.calculators[ key ].length ) {
                this.updateTime( key )
            }

            return names
        }, existingStatNames )
    }

    addFunction( currentFunction: AbstractFunction ): void {
        this.addEffectFunction( currentFunction )
        this.updateTime( currentFunction.getStat() )
    }

    private addEffectFunction( currentFunction: AbstractFunction ) : void {
        let propertyName: string = currentFunction.getStat()
        if ( !this.calculators[ propertyName ] ) {
            this.calculators[ propertyName ] = []
        }

        this.calculators[ propertyName ].push( currentFunction )
    }

    sortFunctions( name: string ): void {
        this.calculators[ name ].sort( ( first: AbstractFunction, second: AbstractFunction ): number => {
            if ( first.template.order === second.template.order ) {
                return 0
            }

            if ( first.template.order < second.template.order ) {
                return -1
            }

            return 1
        } )
    }

    getFunctions( name: string ): Array<AbstractFunction> {
        return this.calculators[ name ]
    }

    getUpdateTime( name: string ) : number {
        return this.updateTimes[ name ]
    }

    private updateTime( name: string ) : void {
        this.updateTimes[ name ] = Date.now()
    }

    private sortAndUpdateAllStats() : void {
        for ( const statName of Object.keys( this.calculators ) ) {
            this.sortFunctions( statName )
            this.updateTime( statName )
        }
    }

    fromFunctions( effects : Array<AbstractFunction> ) : EffectFunctionCalculator {
        for ( const effect of effects ) {
            this.addEffectFunction( effect )
        }

        this.sortAndUpdateAllStats()

        return this
    }
}