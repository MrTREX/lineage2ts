import { Skill } from '../Skill'
import { L2PcInstance } from '../actor/instance/L2PcInstance'
import { ISkillCapable } from '../../interface/ISkillCapable'
import { SkillCache } from '../../cache/SkillCache'
import _ from 'lodash'

export class PlayerSkillHolder implements ISkillCapable {
    skills: { [ key: number ]: Skill } = {}

    constructor( player: L2PcInstance ) {
        let allSkills = player.getSkills()

        _.each( allSkills, ( skill: Skill ) => {
            if ( SkillCache.isSkillAllowed( player, skill ) ) {
                this.addSkill( skill )
            }
        } )
    }

    getSkills() {
        return this.skills
    }

    addSkill( skill: Skill ) : void {
        this.skills[ skill.getId() ] = skill
    }

    getSkillLevel( skillId: number ) : number {
        let skill = this.getKnownSkill( skillId )
        return skill ? skill.getLevel() : -1
    }

    getKnownSkill( skillId: number ) : Skill {
        return this.skills[ skillId ]
    }
}