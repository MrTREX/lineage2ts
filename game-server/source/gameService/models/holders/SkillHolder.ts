import { Skill } from '../Skill'
import { SkillCache } from '../../cache/SkillCache'

export function getExistingSkill( id: number, level: number ) : Skill {
    let skill = SkillCache.getSkill( id, Math.max( level, 1 ) )

    if ( !skill ) {
        throw Error( `Skill not found for id = ${id}, and level = ${level}` )
    }

    return skill
}

export function getPossibleSkill( id: number, level: number ) : Skill {
    let skill = SkillCache.getSkill( id, Math.max( level, 1 ) )

    if ( !skill ) {
        return null
    }

    return skill
}

export class SkillHolder {
    skillId: number
    skillLevel: number

    constructor( skillId: number, skillLevel: number = 1 ) {
        this.skillId = skillId
        this.skillLevel = Math.max( skillLevel, 1 )
    }

    getId(): number {
        return this.skillId
    }

    getLevel(): number {
        return this.skillLevel
    }

    getSkill(): Skill {
        return SkillCache.getSkill( this.skillId, this.skillLevel )
    }
}

// TODO : move to SkillCache layer and initialize when skills are loaded
export const CommonSkill = {
    RAID_CURSE: new SkillHolder( 4215, 1 ),
    RAID_CURSE2: new SkillHolder( 4515, 1 ),
    WYVERN_BREATH: new SkillHolder( 4289, 1 ),
    STRIDER_SIEGE_ASSAULT: new SkillHolder( 325, 1 ),
    FIREWORK: new SkillHolder( 5965, 1 ),
    BLESSING_OF_PROTECTION: new SkillHolder( 5182, 1 ),
    VOID_BURST: new SkillHolder( 3630, 1 ),
    VOID_FLOW: new SkillHolder( 3631, 1 ),
    THE_VICTOR_OF_WAR: new SkillHolder( 5074, 1 ),
    THE_VANQUISHED_OF_WAR: new SkillHolder( 5075, 1 ),
    LUCKY: new SkillHolder( 194, 1 ),
    EXPERTISE: new SkillHolder( 239, 1 ),
    DIVINE_INSPIRATION: new SkillHolder( 1405, 1 ),
}