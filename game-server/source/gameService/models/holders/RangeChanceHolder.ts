export class RangeChanceHolder {
    min: number
    max: number
    chance: number

    constructor( min: number, max: number, chance: number ) {
        this.min = min
        this.max = max
        this.chance = chance
    }
}