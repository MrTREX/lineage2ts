export class ExpirationTime {
    private expiration: Map<number, number> = new Map<number, number>()

    set( code: number, delay: number ) : void {
        let futureTime = delay > 0 ? Date.now() + delay : Number.MAX_SAFE_INTEGER
        this.expiration.set( code, futureTime )
    }

    remove( code: number ) : void {
        this.expiration.delete( code )
    }

    reset() : void {
        this.expiration.clear()
    }

    isActive( code: number ) : boolean {
        let time = this.expiration.get( code )
        if ( !Number.isInteger( time ) ) {
            return false
        }

        if ( time <= Date.now() ) {
            this.expiration.delete( code )

            return false
        }

        return true
    }
}