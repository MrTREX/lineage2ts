import { BasicPlayerGroup } from './BasicPlayerGroup'
import { L2CommandChannel } from './L2CommandChannel'
import { DimensionalRift } from './entity/DimensionalRift'
import { PacketDispatcher } from '../PacketDispatcher'
import { L2World } from '../L2World'
import { L2Attackable } from './actor/L2Attackable'
import { ConfigManager } from '../../config/ConfigManager'
import { L2ServitorInstance } from './actor/instance/L2ServitorInstance'
import { L2PcInstance } from './actor/instance/L2PcInstance'
import { L2PartyMessageType } from '../enums/L2PartyMessageType'
import { SystemMessageBuilder } from '../packets/send/SystemMessage'
import { SystemMessageIds } from '../packets/SystemMessageIdValues'
import { PartySmallWindowDeleteAll } from '../packets/send/PartySmallWindowDeleteAll'
import { PartySmallWindowDelete } from '../packets/send/PartySmallWindowDelete'
import { ExPartyPetWindowDeleteWithSummon } from '../packets/send/ExPartyPetWindowDelete'
import { ExCloseMPCC } from '../packets/send/ExCloseMPCC'
import { PartySmallWindowAll } from '../packets/send/PartySmallWindowAll'
import { getPartyDistributionTypeById, PartyDistributionType } from '../enums/PartyDistributionType'
import { PartyMatchManager } from '../cache/partyMatchManager'
import { GeneralHelper } from '../helpers/GeneralHelper'
import { L2PcInstanceValues } from '../values/L2PcInstanceValues'
import { PartyMemberPosition } from '../packets/send/PartyMemberPosition'
import { ExOpenMPCC } from '../packets/send/ExOpenMPCC'
import { ExAskModifyPartyLooting } from '../packets/send/ExAskModifyPartyLooting'
import { ExSetPartyLooting, ExSetPartyLootingResult } from '../packets/send/ExSetPartyLooting'
import { ExPartyPetWindowAddWithSummon } from '../packets/send/ExPartyPetWindowAdd'
import { PartySmallWindowAdd } from '../packets/send/PartySmallWindowAdd'
import { L2ItemInstance } from './items/instance/L2ItemInstance'
import { ItemManagerCache } from '../cache/ItemManagerCache'
import { L2Character } from './actor/L2Character'
import { ItemTypes } from '../values/InventoryValues'
import { PlayerGroupType } from '../enums/PlayerGroupType'
import { PlayerGroupCache } from '../cache/PlayerGroupCache'
import { BlocklistCache } from '../cache/BlocklistCache'
import { InventoryAction } from '../enums/InventoryAction'
import _, { DebouncedFunc } from 'lodash'
import aigle from 'aigle'
import { RelationChangedPartyIndex, RelationChangedType } from '../packets/send/RelationChanged'
import { AIEffectHelper } from '../aicontroller/helpers/AIEffectHelper'
import { AIIntent } from '../aicontroller/enums/AIIntent'
import Timeout = NodeJS.Timeout
import { GeometryId } from '../enums/GeometryId'
import { PointGeometry } from './drops/PointGeometry'
import { teleportPlayersToGeometryCoordinates } from '../helpers/TeleportHelper'


export class L2Party extends BasicPlayerGroup {
    members: Array<number> // object ids
    memberRelation: Record<number, number>
    pendingInvitation: boolean
    pendingInviteExpiration: number
    partyLevel: number
    distributionType: PartyDistributionType
    changeRequestDistributionType: PartyDistributionType = null
    changeDistributionTypeRequestTask: Timeout
    changeDistributionTypeAnswers: Array<number> = []
    itemLastLoot: number
    commandChannel: L2CommandChannel
    dimentionalRift: DimensionalRift
    debounceUpdateMemberPositions: DebouncedFunc<() => void>

    constructor() {
        super()

        this.debounceUpdateMemberPositions = _.debounce( this.runBroadcastMemberPositions.bind( this ), 5000, {
            trailing: true,
            maxWait: 10000,
        } )
    }

    initialize( leader: L2PcInstance, type: PartyDistributionType ) : void {
        this.members = [ leader.getObjectId() ]
        this.partyLevel = leader.getLevel()
        this.distributionType = type
        this.memberRelation = {}
    }

    getType(): PlayerGroupType {
        return PlayerGroupType.Party
    }

    addPartyMember( player: L2PcInstance ) {
        if ( this.getMembers().includes( player.getObjectId() ) ) {
            return
        }

        if ( this.changeRequestDistributionType !== null ) {
            this.finishLootRequest( false )
        }

        player.sendOwnedData( PartySmallWindowAll( player.getObjectId(), this ) )

        this.getMembers().forEach( ( memberId: number ) => {
            let member = L2World.getPlayer( memberId )
            if ( member && member.hasSummon() ) {
                player.sendOwnedData( ExPartyPetWindowAddWithSummon( member.getSummon() ) )
            }
        } )

        let playerWelcome = new SystemMessageBuilder( SystemMessageIds.YOU_JOINED_S1_PARTY )
                .addString( this.getLeader().getName() )
                .getBuffer()
        player.sendOwnedData( playerWelcome )

        let partyNotice = new SystemMessageBuilder( SystemMessageIds.C1_JOINED_PARTY )
                .addString( player.getName() )
                .getBuffer()
        this.broadcastPacket( partyNotice )
        this.broadcastPacket( PartySmallWindowAdd( player, this ) )

        if ( player.hasSummon() ) {
            this.broadcastPacket( ExPartyPetWindowAddWithSummon( player.getSummon() ) )
        }

        this.members.push( player.getObjectId() )
        this.refreshId()

        if ( player.getLevel() > this.partyLevel ) {
            this.partyLevel = player.getLevel()
        }

        this.getMembers().forEach( ( memberId: number, index: number ) => {
            let member = L2World.getPlayer( memberId )
            if ( !member ) {
                return
            }

            member.updateEffectIcons()
            member.broadcastUserInfo()

            let summon = member.getSummon()
            if ( summon ) {
                summon.updateEffectIcons()
            }

            this.memberRelation[ memberId ] = RelationChangedPartyIndex[ index ]
        } )

        if ( this.isInDimensionalRift() ) {
            this.dimentionalRift.partyMemberInvited()
        }

        if ( this.isInCommandChannel() ) {
            player.sendOwnedData( ExOpenMPCC() )
        }

        this.runBroadcastMemberPositions()
    }

    broadcastDataToPartyMembers( objectId: number, packet: Buffer ): void {
        this.members.forEach( ( currentObjectId: number ) => {
            if ( currentObjectId !== objectId ) {
                PacketDispatcher.sendCopyData( currentObjectId, packet )
            }
        } )
    }

    broadcastPacket( packet: Buffer ) {
        this.members.forEach( ( playerId: number ) => {
            PacketDispatcher.sendCopyData( playerId, packet )
        } )
    }

    broadcastBlockedDataToPartyMembers( packet: Buffer, player: L2PcInstance ): void {
        this.members.forEach( ( playerId: number ) => {
            if ( !BlocklistCache.isBlocked( playerId, player.getObjectId() ) ) {
                PacketDispatcher.sendCopyData( playerId, packet )
            }
        } )
    }

    broadcastToPartyMembersNewLeader() {
        let currentParty = this
        this.members.forEach( ( memberId: number ) => {
            let player = L2World.getPlayer( memberId )
            if ( player ) {
                player.sendOwnedData( PartySmallWindowDeleteAll() )
                player.sendOwnedData( PartySmallWindowAll( memberId, currentParty ) )
                player.broadcastUserInfo()
            }
        } )
    }

    modifyExpSpPartyCutoff( leveDifference: number, addExp: number, addSp: number ) : void {
        if ( ConfigManager.character.getPartyXpCutoffMethod() === 'highfive' ) {
            _.each( ConfigManager.character.getPartyXpCutoffGaps(), ( data ) => {
                let [ lowValue, highValue, multiplier ] = data
                if ( leveDifference >= lowValue && leveDifference <= highValue ) {

                    addExp = Math.floor( addExp * multiplier )
                    addSp = Math.floor( addSp * multiplier )

                    return false
                }
            } )
        }
    }

    disbandParty() {
        this.broadcastPacket( SystemMessageBuilder.fromMessageId( SystemMessageIds.PARTY_DISPERSED ) )

        this.members.forEach( ( memberId: number ) => {
            let player = L2World.getPlayer( memberId )
            if ( player ) {
                this.resetPartyInformation( player )
            }

            PlayerGroupCache.removePartyRecord( memberId )
        } )

        this.resetParty()
        PlayerGroupCache.recycleParty( this )
    }

    async distributeAdena( player: L2PcInstance, amount: number, target: L2Character ) : Promise<void> {
        let rewardedMembers: Array<L2PcInstance> = []

        this.members.forEach( ( memberId: number ) => {
            let member = L2World.getPlayer( memberId )
            if ( GeneralHelper.checkIfInRange( ConfigManager.character.getPartyRange2(), target, member, true ) ) {
                rewardedMembers.push( member )
            }
        } )

        if ( rewardedMembers.length > 0 ) {
            let leftOver = amount % rewardedMembers.length
            let amountPerMember = amount / rewardedMembers.length

            await aigle.resolve( rewardedMembers ).each( async ( member: L2PcInstance ) => {
                let receiveAmount = leftOver > 0 ? amountPerMember + 1 : amountPerMember
                await member.addAdena( receiveAmount, 'L2Party.distributeAdena', player.objectId, true )
                leftOver--
            } )
        }
    }

    async distributeItem( player: L2PcInstance, itemId: number, count: number, isSpoil: boolean, target: L2Attackable ) {
        if ( itemId === ItemTypes.Adena ) {
            return this.distributeAdena( player, count, target )
        }

        let looter: L2PcInstance = this.getActualLooter( player, itemId, isSpoil, target )
        await looter.addItem( itemId, count, -1, target.getObjectId(), InventoryAction.PartyLoot )

        if ( count > 0 ) {
            if ( count > 1 ) {
                let messageId = isSpoil ? SystemMessageIds.C1_SWEEPED_UP_S3_S2 : SystemMessageIds.C1_OBTAINED_S3_S2
                let packet: Buffer = new SystemMessageBuilder( messageId )
                        .addString( looter.getName() )
                        .addItemNameWithId( itemId )
                        .addNumber( count )
                        .getBuffer()
                this.broadcastDataToPartyMembers( looter.objectId, packet )
            } else {
                let messageId = isSpoil ? SystemMessageIds.C1_SWEEPED_UP_S2 : SystemMessageIds.C1_OBTAINED_S2
                let packet: Buffer = new SystemMessageBuilder( messageId )
                        .addString( looter.getName() )
                        .addItemNameWithId( itemId )
                        .getBuffer()
                this.broadcastDataToPartyMembers( looter.objectId, packet )
            }
        }
    }

    async distributeXpAndSp( xpReward: number, spReward: number, rewardedMembers: Array<number>, partyLevel: number, partyDamage: number, target: L2Attackable ): Promise<void> {
        let validMembers: Set<number> = this.getValidMembers( rewardedMembers, partyLevel )

        xpReward *= this.getExpBonus( validMembers.size )
        spReward *= this.getSpBonus( validMembers.size )

        let squareLevelSum = 0
        validMembers.forEach( ( playerId: number ) => {
            let player = L2World.getPlayer( playerId )

            squareLevelSum += ( player.getLevel() * player.getLevel() )
        } )


        let vitalityPoints = ( target.getVitalityPoints( partyDamage ) * ConfigManager.rates.getRatePartyXp() ) / validMembers.size
        let useVitalityRate = target.useVitalityRate()

        let party: L2Party = this
        await aigle.resolve( rewardedMembers ).each( ( playerId: number ) : Promise<void> => {
            let player = L2World.getPlayer( playerId )
            if ( !player || player.isDead() ) {
                return
            }

            if ( validMembers.has( playerId ) ) {
                let penalty = player.hasServitor() ? ( player.getSummon() as L2ServitorInstance ).getExpMultiplier() : 1

                let sqLevel = player.getLevel() * player.getLevel()
                let preCalculation = ( sqLevel / squareLevelSum ) * penalty

                let exp = Math.round( xpReward * preCalculation )
                let sp = Math.floor( spReward * preCalculation )

                let leveDifference: number = partyLevel - player.getLevel()
                party.modifyExpSpPartyCutoff( leveDifference, exp, sp )

                if ( exp > 0 ) {
                    player.updateVitalityPoints( vitalityPoints, true, true )
                    // TODO : add Nevit points to party member
                }

                return player.processAddExpAndSp( exp, sp, useVitalityRate )
            }

            return player.addExpAndSp( 0, 0 )
        } )
    }

    finishLootRequest( isSuccess: boolean ): void {
        if ( !this.changeRequestDistributionType ) {
            return
        }

        if ( this.changeDistributionTypeRequestTask ) {
            clearTimeout( this.changeDistributionTypeRequestTask )
            this.changeDistributionTypeRequestTask = null
        }
        if ( isSuccess ) {
            this.broadcastPacket( ExSetPartyLooting( ExSetPartyLootingResult.Success, this.changeRequestDistributionType ) )
            this.distributionType = this.changeRequestDistributionType

            let packet = new SystemMessageBuilder( SystemMessageIds.PARTY_LOOT_CHANGED_S1 )
                    .addSystemString( getPartyDistributionTypeById( this.changeRequestDistributionType ).systemStringId )
                    .getBuffer()
            this.broadcastPacket( packet )
        } else {
            this.broadcastPacket( ExSetPartyLooting( ExSetPartyLootingResult.Cancelled, this.distributionType ) )
            this.broadcastPacket( SystemMessageBuilder.fromMessageId( SystemMessageIds.PARTY_LOOT_CHANGE_CANCELLED ) )
        }

        this.changeRequestDistributionType = null
        this.changeDistributionTypeAnswers = []
    }

    getActualLooter( player: L2PcInstance, itemId: number, isSpoil: boolean, target: L2Character ) {
        let looter: L2PcInstance

        switch ( this.distributionType ) {
            case PartyDistributionType.Random:
                if ( !isSpoil ) {
                    looter = this.getCheckedRandomMember( itemId, target )
                }
                break
            case PartyDistributionType.RandomIncludingSpoil:
                looter = this.getCheckedRandomMember( itemId, target )
                break
            case PartyDistributionType.ByTurn:
                if ( !isSpoil ) {
                    looter = this.getCheckedNextLooter( itemId, target )
                }
                break
            case PartyDistributionType.ByTurnIncludingSpoil:
                looter = this.getCheckedNextLooter( itemId, target )
                break
        }

        return _.defaultTo( looter, player )
    }

    getBaseExpBonus( count: number ): number {
        let index = count - 1
        if ( index < 1 ) {
            return 1
        }
        if ( index >= ConfigManager.rates.getPartyPlayerExpBonuses().length ) {
            index = ConfigManager.rates.getPartyPlayerExpBonuses().length - 1
        }

        return ConfigManager.rates.getPartyPlayerExpBonuses()[ index ]
    }

    getBaseSpBonus( count: number ): number {
        let index = count - 1
        if ( index < 1 ) {
            return 1
        }
        if ( index >= ConfigManager.rates.getPartyPlayerSpBonuses().length ) {
            index = ConfigManager.rates.getPartyPlayerSpBonuses().length - 1
        }

        return ConfigManager.rates.getPartyPlayerSpBonuses()[ index ]
    }

    getCheckedNextLooter( itemId: number, target: L2Character ): L2PcInstance {
        let totalMembers = this.getMemberCount()
        for ( let index = 0; index < totalMembers; index++ ) {
            if ( this.itemLastLoot >= totalMembers ) {
                this.itemLastLoot = 0
            }

            let memberId = _.nth( this.getMembers(), this.itemLastLoot )
            let member = L2World.getPlayer( memberId )
            if ( member && member.getInventory().validateCapacityByItemId( itemId )
                    && GeneralHelper.checkIfInRange( ConfigManager.character.getPartyRange2(), target, member, true ) ) {
                return member
            }
        }

        return null
    }

    getCheckedRandomMember( itemId: number, target: L2Character ): L2PcInstance {
        let memberIds: Array<number> = _.shuffle( this.getMembers() )

        while ( memberIds.length > 0 ) {
            let memberId = memberIds.pop()
            let member = L2World.getPlayer( memberId )
            if ( member && member.getInventory().validateCapacityByItemId( itemId )
                    && GeneralHelper.checkIfInRange( ConfigManager.character.getPartyRange2(), target, member, true ) ) {
                return member
            }
        }

        return null
    }

    getCommandChannel() {
        return this.commandChannel
    }

    getDimensionalRift() {
        return this.dimentionalRift
    }

    getDistributionType(): PartyDistributionType {
        return this.distributionType
    }

    getExpBonus( count: number ): number {
        return count < 2 ? this.getBaseExpBonus( count ) : this.getBaseExpBonus( count ) * ConfigManager.rates.getRatePartyXp()
    }

    getLeader(): L2PcInstance {
        return L2World.getPlayer( this.members[ 0 ] )
    }

    getLeaderObjectId(): number {
        return this.members[ 0 ]
    }

    getMembers(): Array<number> {
        return this.members
    }

    setLeader( player: L2PcInstance ): void {
        if ( !player || player.isInDuel() ) {
            return
        }

        let playerId = player.getObjectId()
        if ( this.members.includes( playerId ) ) {
            if ( this.isLeader( player ) ) {
                PacketDispatcher.sendOwnedData( playerId, SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_TRANSFER_RIGHTS_TO_YOURSELF ) )
                return
            }

            let oldLeader: L2PcInstance = this.getLeader()
            _.pull( this.members, playerId )
            this.members.unshift( playerId )
            this.refreshId()

            let packet = new SystemMessageBuilder( SystemMessageIds.C1_HAS_BECOME_A_PARTY_LEADER )
                    .addString( this.getLeader().getName() )
                    .getBuffer()

            this.broadcastPacket( packet )
            this.broadcastToPartyMembersNewLeader()
            this.updateMemberRelations()

            if ( this.isInCommandChannel() && this.commandChannel.isLeader( oldLeader ) ) {
                this.commandChannel.setLeader( this.getLeader() )
                let commandPacket = new SystemMessageBuilder( SystemMessageIds.COMMAND_CHANNEL_LEADER_NOW_C1 )
                        .addString( this.commandChannel.getLeader().getName() )
                        .getBuffer()

                this.commandChannel.broadcastPacket( commandPacket )
            }

            PartyMatchManager.makeLeader( player.getObjectId() )

            return
        }

        PacketDispatcher.sendOwnedData( playerId, SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CAN_TRANSFER_RIGHTS_ONLY_TO_ANOTHER_PARTY_MEMBER ) )
    }

    getLevel() {
        return this.partyLevel
    }

    getPendingInvitation(): boolean {
        return this.pendingInvitation
    }

    getSpBonus( count: number ): number {
        return ( count < 2 ) ? this.getBaseSpBonus( count ) : this.getBaseSpBonus( count ) * ConfigManager.rates.getRatePartySp()
    }

    getValidMembers( rewardedMembers: Array<number>, partyLevel: number ): Set<number> {
        let validMembers: Set<number> = new Set<number>()

        if ( ConfigManager.character.getPartyXpCutoffMethod() === 'level' ) {
            rewardedMembers.forEach( ( playerId: number ) => {
                let player = L2World.getPlayer( playerId )
                if ( ( partyLevel - player.getLevel() ) <= ConfigManager.character.getPartyXpCutoffLevel() ) {
                    validMembers.add( playerId )
                }
            } )

            return validMembers
        }

        if ( ConfigManager.character.getPartyXpCutoffMethod() === 'percentage' ) {
            let sqLevelSum = 0
            rewardedMembers.forEach( ( playerId: number ) => {
                let player = L2World.getPlayer( playerId )
                sqLevelSum += ( player.getLevel() * player.getLevel() )
            } )

            rewardedMembers.forEach( ( playerId: number ) => {
                let player = L2World.getPlayer( playerId )
                let sqLevel = player.getLevel() * player.getLevel()
                if ( ( sqLevel * 100 ) >= ( sqLevelSum * ConfigManager.character.getPartyXpCutoffPercent() ) ) {
                    validMembers.add( playerId )
                }
            } )

            return validMembers
        }

        if ( ConfigManager.character.getPartyXpCutoffMethod() === 'auto' ) {
            let sqLevelSum = 0
            rewardedMembers.forEach( ( playerId: number ) => {
                let player = L2World.getPlayer( playerId )
                sqLevelSum += ( player.getLevel() * player.getLevel() )
            } )

            rewardedMembers.forEach( ( playerId: number ) => {
                let player = L2World.getPlayer( playerId )
                let sqLevel = player.getLevel() * player.getLevel()
                if ( sqLevel >= ( sqLevelSum / ( rewardedMembers.length * rewardedMembers.length ) ) ) {
                    validMembers.add( playerId )
                }
            } )

            return validMembers
        }

        if ( ConfigManager.character.getPartyXpCutoffMethod() === 'highfive' ) {
            return new Set<number>( rewardedMembers )
        }

        if ( ConfigManager.character.getPartyXpCutoffMethod() === 'none' ) {
            return new Set<number>( rewardedMembers )
        }

        return validMembers
    }

    isInCommandChannel() {
        return !!this.commandChannel
    }

    isInDimensionalRift() {
        return !!this.dimentionalRift
    }

    isInvitationRequestExpired() {
        return this.pendingInviteExpiration < Date.now()
    }

    recalculatePartyLevel(): void {
        let party = this
        this.partyLevel = this.members.reduce( ( currentLevel: number, memberId: number ) => {
            let player = L2World.getPlayer( memberId )
            if ( !player ) {
                _.pull( party.members, memberId )
                return
            }

            if ( player.getLevel() > currentLevel ) {
                currentLevel = player.getLevel()
            }

            return currentLevel
        }, 0 )
    }

    removePartyMember( player: L2PcInstance, messageType: L2PartyMessageType ) : void {
        if ( !player ) {
            return
        }

        let memberId = player.getObjectId()
        if ( !this.members.includes( memberId ) ) {
            return
        }

        let isLeader: boolean = this.isLeader( player )
        if ( this.members.length === 2 || ( isLeader && !ConfigManager.character.leavePartyLeader() && ( messageType !== L2PartyMessageType.Disconnected ) ) ) {
            this.disbandParty()
            return
        }

        _.pull( this.members, memberId )
        this.recalculatePartyLevel()
        this.refreshId()

        if ( messageType === L2PartyMessageType.Expelled ) {
            PacketDispatcher.sendOwnedData( memberId, SystemMessageBuilder.fromMessageId( SystemMessageIds.HAVE_BEEN_EXPELLED_FROM_PARTY ) )

            let packet = new SystemMessageBuilder( SystemMessageIds.C1_WAS_EXPELLED_FROM_PARTY )
                    .addString( player.getName() )
                    .getBuffer()
            this.broadcastPacket( packet )
        } else if ( ( messageType === L2PartyMessageType.Left ) || ( messageType === L2PartyMessageType.Disconnected ) ) {
            PacketDispatcher.sendOwnedData( memberId, SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_LEFT_PARTY ) )

            let packet = new SystemMessageBuilder( SystemMessageIds.C1_LEFT_PARTY )
                    .addString( player.getName() )
                    .getBuffer()
            this.broadcastPacket( packet )
        }

        delete this.memberRelation[ memberId ]
        this.resetPartyInformation( player )
        this.updateMemberRelations()

        if ( isLeader && ( this.getMembers().length > 1 ) && ( ConfigManager.character.leavePartyLeader() || ( messageType === L2PartyMessageType.Disconnected ) ) ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.C1_HAS_BECOME_A_PARTY_LEADER )
                    .addString( this.getLeader().getName() )
                    .getBuffer()

            this.broadcastPacket( packet )
            this.broadcastToPartyMembersNewLeader()

            return
        }
    }

    resetParty() : void {
        if ( this.isInCommandChannel() ) {

            if ( this.getCommandChannel().getLeader().getObjectId() === this.getLeaderObjectId() ) {
                this.getCommandChannel().disbandChannel()
            } else {
                this.getCommandChannel().removeParty( this )
            }
        }

        if ( this.changeDistributionTypeRequestTask ) {
            clearTimeout( this.changeDistributionTypeRequestTask )
            this.changeDistributionTypeRequestTask = null
        }

        this.debounceUpdateMemberPositions.cancel()
        this.members = null
        this.memberRelation = null
    }

    resetPartyInformation( player : L2PcInstance ) : void {
        PlayerGroupCache.removePartyRecord( player.getObjectId() )

        if ( player.isChanneling() && ( player.getSkillChannelizer().hasChannelized() ) ) {
            player.abortCast()
        } else if ( player.isChannelized() ) {
            player.getSkillChannelized().abortChannelization()
        }

        PacketDispatcher.sendOwnedData( player.getObjectId(), PartySmallWindowDeleteAll() )

        this.broadcastPacket( PartySmallWindowDelete( player.getObjectId(), player.getName() ) )

        if ( player.hasSummon() ) {
            this.broadcastPacket( ExPartyPetWindowDeleteWithSummon( player.getSummon() ) )
        }

        if ( this.isInDimensionalRift() ) {
            this.dimentionalRift.partyMemberExited( player )
        }

        if ( this.isInCommandChannel() ) {
            PacketDispatcher.sendOwnedData( player.getObjectId(), ExCloseMPCC() )
        }
    }

    requestLootChange( type: PartyDistributionType ): void {
        if ( this.changeRequestDistributionType ) {
            return
        }

        this.changeRequestDistributionType = type
        this.changeDistributionTypeAnswers = []
        this.changeDistributionTypeRequestTask = setTimeout( this.finishLootRequest.bind( this ), 15000, false )

        let leader = this.getLeader()

        this.broadcastDataToPartyMembers( this.getLeaderObjectId(), ExAskModifyPartyLooting( leader.getName(), type ) )
        let packet = new SystemMessageBuilder( SystemMessageIds.REQUESTING_APPROVAL_CHANGE_PARTY_LOOT_S1 )
                .addSystemString( getPartyDistributionTypeById( type ).systemStringId )
                .getBuffer()
        leader.sendOwnedData( packet )
    }

    runBroadcastMemberPositions() {
        this.broadcastPacket( PartyMemberPosition( this ) )
    }

    setCommandChannel( channel: L2CommandChannel ) {
        this.commandChannel = channel
    }

    setDimensionalRift( rift: DimensionalRift ) {
        this.dimentionalRift = rift
    }

    setPendingInvitation( value: boolean ) {
        this.pendingInvitation = value
        this.pendingInviteExpiration = Date.now() + L2PcInstanceValues.RequestDurationMillis
    }

    removePartyMemberByName( name: string, type: L2PartyMessageType ) {
        this.removePartyMember( this.getPlayerByName( name ), type )
    }

    getPlayerByName( name: string ): L2PcInstance {
        let outcome: L2PcInstance = null
        let adjustedName = _.toLower( name )

        _.each( this.getMembers(), ( memberId: number ) => {
            let player = L2World.getPlayer( memberId )
            if ( player && _.toLower( player.getName() ) === adjustedName ) {
                outcome = player
                return false
            }
        } )

        return outcome
    }

    answerLootChangeRequest( player: L2PcInstance, isAnswered: boolean ) {
        if ( _.isUndefined( this.changeRequestDistributionType ) ) {
            return
        }

        if ( this.changeDistributionTypeAnswers.includes( player.getObjectId() ) ) {
            return
        }

        if ( !isAnswered ) {
            this.finishLootRequest( false )
            return
        }

        this.changeDistributionTypeAnswers.push( player.getObjectId() )

        if ( this.changeDistributionTypeAnswers.length >= ( this.getMemberCount() - 1 ) ) {
            this.finishLootRequest( true )
        }
    }

    async distributeExistingItem( player: L2PcInstance, item: L2ItemInstance ): Promise<void> {
        if ( item.getId() === ItemTypes.Adena ) {
            await this.distributeAdena( player, item.getCount(), player )
            return ItemManagerCache.destroyItem( item )
        }

        let target: L2PcInstance = this.getActualLooter( player, item.getId(), false, player )
        await target.addItemWithInstance( item, player.getObjectId(), true )

        if ( item.getCount() > 0 ) {

            let message: SystemMessageBuilder

            if ( item.getCount() > 1 ) {
                message = new SystemMessageBuilder( SystemMessageIds.C1_OBTAINED_S3_S2 )
                        .addPlayerCharacterName( target )
                        .addItemInstanceName( item )
                        .addNumber( item.getCount() )
            } else {
                message = new SystemMessageBuilder( SystemMessageIds.C1_OBTAINED_S2 )
                        .addPlayerCharacterName( target )
                        .addItemInstanceName( item )
            }

            this.broadcastDataToPartyMembers( target.getObjectId(), message.getBuffer() )
        }
    }

    private updateMemberRelations() : void {
        this.getMembers().forEach( ( memberId: number, index: number ) => {
            this.memberRelation[ memberId ] = RelationChangedPartyIndex[ index ]
        } )
    }

    getMemberRelation( objectId: number ) : RelationChangedType {
        return this.memberRelation[ objectId ]
    }

    teleportMembersToCoordinates( x: number, y: number, z: number, heading: number = undefined ) : Promise<void> {
        return teleportPlayersToGeometryCoordinates( GeometryId.PartyTeleport, this.getMembers(), x, y, z, heading )
    }
}