import { Skill } from '../Skill'

// TODO : stop-gap solution, review usages in favor of BuffInfo if such buff is in use
export interface BuffProperties {
    skill: Skill
    durationMs: number
}

export interface BuffDefinition {
    skill: Skill
    timeLeftSeconds: number
    isInUse: boolean
}