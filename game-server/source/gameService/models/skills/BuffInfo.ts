import { L2Character } from '../actor/L2Character'
import { Skill } from '../Skill'
import { AbstractEffect } from '../effects/AbstractEffect'
import { Formulas } from '../stats/Formulas'
import { L2World } from '../../L2World'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { L2Summon } from '../actor/L2Summon'
import { ConfigManager } from '../../../config/ConfigManager'
import aigle from 'aigle'
import { ObjectPool } from '../../helpers/ObjectPoolHelper'
import { EffectFlag } from '../../enums/EffectFlag'
import { CharacterEffects } from '../characterEffects'
import { ResetShortBuffStatus, ShortBuffStatusUpdate } from '../../packets/send/ShortBuffStatusUpdate'
import { PacketDispatcher } from '../../PacketDispatcher'
import { BuffDefinition, BuffProperties } from './BuffDefinition'
import Timeout = NodeJS.Timeout

interface EffectTask {
    effect: AbstractEffect
    timeout: Timeout
}

const EffectTaskPool = new ObjectPool( 'BuffInfoEffectTask', (): EffectTask => {
    return {
        effect: undefined,
        timeout: undefined
    }
} )

const BuffInfoPool = new ObjectPool( 'BuffInfo', (): BuffInfo => {
    return new BuffInfo()
}, 0 )

export class BuffInfo {
    effector: number
    affected: number
    skill: Skill
    effects: Array<AbstractEffect> = []
    timeouts: Array<EffectTask> = []
    endInterval: Timeout
    totalDurationMs: number
    startTime: number
    isInUse: boolean = true
    charges: number = 0
    effectFlags: EffectFlag = EffectFlag.NONE
    definition: BuffDefinition = {
        isInUse: false,
        skill: null,
        timeLeftSeconds: 0
    }

    static fromParameters( effector: L2Character, affected: L2Character, skill: Skill ): BuffInfo {
        let buff = BuffInfoPool.getValue()
        buff.effector = effector.objectId
        buff.affected = affected.objectId
        buff.skill = skill

        let effectTime = Formulas.calculateSkillEffectDurationSeconds( effector, affected, skill )
        buff.totalDurationMs = effectTime > 0 ? effectTime * 1000 : effectTime
        buff.startTime = Date.now()
        buff.definition.isInUse = false
        buff.definition.skill = skill

        buff.definition.timeLeftSeconds = 0

        return buff
    }

    /*
        Each character status update would call to get definition, hence re-using these makes sense.
        Care must be taken not to store these in long term memory due to object pool usage of BuffInfo.
     */
    getDefinition(): BuffDefinition {
        this.definition.timeLeftSeconds = this.getRemainingSeconds()
        this.definition.isInUse = this.isInUse

        return this.definition
    }

    /*
        TODO: figure out how to re-use this object
        - for now leaving in place pending better times
     */
    getProperties(): BuffProperties {
        return {
            skill: this.skill,
            durationMs: this.getRemainingMs()
        }
    }

    async recycle( effects: CharacterEffects ): Promise<void> {
        // TODO : remove after code has stabilized
        if ( effects.skills[ this.getSkill().getId() ] === this ) {
            throw new Error( `Resetting currently used buff with skillId=${ this.getSkill().getId() } with name=${ this.getSkill().getName() }` )
        }

        // TODO : figure out a way to clean up without using promises
        await this.unloadAllEffects()

        this.skill = null
        this.totalDurationMs = 0
        this.startTime = 0
        this.effector = 0

        this.affected = 0
        this.effectFlags = EffectFlag.NONE
        this.charges = 0
        this.isInUse = false

        BuffInfoPool.recycleValue( this )
    }

    addEffect( effect: AbstractEffect ) {
        this.effects.push( effect )
        this.effectFlags |= effect.effectFlags
    }

    addStats(): void {
        let affected: L2Character = this.getAffected()

        this.effects.forEach( ( effect: AbstractEffect ): void => {
            affected.addStatFunctions( effect.getStatFunctions(), true )
        } )
    }

    getAffected(): L2Character {
        return L2World.getObjectById( this.affected ) as L2Character
    }

    getAffectedId(): number {
        return this.affected
    }

    getEffector(): L2Character {
        return L2World.getObjectById( this.effector ) as L2Character
    }

    getEffectorId(): number {
        return this.effector
    }

    getEffects(): Array<AbstractEffect> {
        return this.effects
    }

    getSkill(): Skill {
        return this.skill
    }

    getRemainingSeconds(): number {
        return this.totalDurationMs > 0 ? this.getRemainingMs() / 1000 : -1
    }

    getRemainingMs(): number {
        return Math.max( 0, this.totalDurationMs - ( Date.now() - this.startTime ) )
    }

    async initializeEffects(): Promise<void> {
        let target = this.getAffected()
        if ( !target ) {
            return
        }

        if ( target.isPlayer() && !this.getSkill().isPassive() ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.YOU_FEEL_S1_EFFECT )
                .addSkillName( this.getSkill() )
                .getBuffer()

            target.sendOwnedData( packet )
        }

        this.setInUse( true )
        let shouldSkip = target.isDead() && !this.getSkill().isPassive()
        if ( !shouldSkip ) {
            let currentBuff: BuffInfo = this

            await aigle.resolve( this.effects ).each( ( currentEffect: AbstractEffect ): Promise<void> => {
                if ( currentEffect.isInstant() || !currentEffect.canStart( currentBuff ) ) {
                    return
                }

                if ( currentEffect.getTicks() > 0 ) {
                    currentBuff.addTask( currentEffect, setInterval( currentBuff.onTick.bind( currentBuff ), currentEffect.getTicks() * ConfigManager.character.getEffectTickRatio(), currentEffect ) )
                }

                target.addStatFunctions( currentEffect.getStatFunctions(), false )

                return currentEffect.onStart( currentBuff )
            } )
        }

        if ( target.getWorldRegion() && target.getWorldRegion().isActive() && target.hasLoadedSkills() ) {
            target.debounceBroadcastAllStats()
        }

        target.addVisualEffectMasks( this.skill )

        if ( this.totalDurationMs > 0 ) {
            this.endInterval = setInterval( this.runBuffEndTask.bind( this ), this.totalDurationMs, false )
        }
    }

    runBuffEndTask( shouldRemove: boolean ): Promise<void> {
        let affected = this.getAffected()
        if ( affected ) {
            return affected.getEffectList().remove( shouldRemove, this, true, true )
        }

        // TODO : ensure buff is recycled
        return this.cleanUp()
    }

    addTask( effect: AbstractEffect, task: Timeout ): void {
        let timeout = EffectTaskPool.getValue()

        timeout.effect = effect
        timeout.timeout = task

        this.timeouts.push( timeout )
    }

    async onTick( effect: AbstractEffect ): Promise<void> {
        if ( this.isInUse && await effect.onTick( this ) ) {
            return
        }

        if ( !this.getSkill().isToggle() ) {
            return
        }

        let data: EffectTask = this.getEffectTimeout( effect )
        if ( data ) {
            clearInterval( data.timeout )
            return this.runBuffEndTask( true )
        }
    }

    getEffectTimeout( effect: AbstractEffect ): EffectTask {
        return this.timeouts.find( ( task: EffectTask ) => {
            return effect === task.effect
        } )
    }

    removeStats(): void {
        let character = this.getAffected()
        if ( !character ) {
            return
        }

        this.effects.forEach( ( effect: AbstractEffect ) => character.removeStatsOwner( effect, false ) )
        character.removeStatsOwner( this.skill, false )
    }

    setDuration( time: number ): void {
        this.totalDurationMs = time
    }

    setCharges( amount: number ) {
        this.charges = amount
    }

    setInUse( value: boolean ) {
        this.isInUse = value
    }

    async stopAllEffects( removed: boolean ): Promise<void> {
        await this.unloadAllEffects()
        return this.finishEffects( removed )
    }

    private finishEffects( isRemoved: boolean ): void {
        let affected: L2Character = this.getAffected()
        if ( !affected ) {
            return
        }

        affected.removeVisualEffectMasks( this.skill )

        if ( !( affected.isSummon() && !( affected as L2Summon ).getOwner().hasSummon() ) ) {
            let message
            if ( this.getSkill().isToggle() ) {
                message = SystemMessageIds.S1_HAS_BEEN_ABORTED
            } else if ( isRemoved ) {
                message = SystemMessageIds.EFFECT_S1_HAS_BEEN_REMOVED
            } else if ( !this.getSkill().isPassive() ) {
                message = SystemMessageIds.S1_HAS_WORN_OFF
            }

            if ( message ) {
                let packet = new SystemMessageBuilder( message )
                    .addSkillName( this.getSkill() )
                    .getBuffer()
                affected.sendOwnedData( packet )
            }
        }

        if ( this.isShortBuff() && affected.isPlayable() ) {
            return affected.sendCopyData( ResetShortBuffStatus )
        }
    }

    unloadAllEffects(): Promise<void> {
        this.stopEndInterval()
        return this.cleanUp()
    }

    stopEndInterval(): void {
        if ( this.endInterval ) {
            clearTimeout( this.endInterval )
            this.endInterval = null
        }
    }

    async cleanUp(): Promise<void> {
        this.timeouts.forEach( ( task: EffectTask ) => {
            task.effect = null

            clearInterval( task.timeout )
            EffectTaskPool.recycleValue( task )
        } )

        this.timeouts.length = 0
        this.removeStats()

        await aigle.resolve( this.effects ).each( ( effect: AbstractEffect ) => {
            if ( effect.isInstant() ) {
                return
            }

            return effect.onExit( this )
        } )

        this.effects.length = 0
    }

    getCharges() {
        return this.charges
    }

    getDuration(): number {
        return this.totalDurationMs
    }

    getEffectFlags(): EffectFlag {
        return this.effectFlags
    }

    sendShortBuffStatusUpdate(): void {
        return PacketDispatcher.sendOwnedData( this.affected, ShortBuffStatusUpdate( this.skill.getId(), this.skill.getLevel(), this.getRemainingSeconds() ) )
    }

    isShortBuff(): boolean {
        return this.skill.isHealingPotionSkill()
    }
}