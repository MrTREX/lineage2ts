// TODO : reconcile with ElementalType since checks and enum values are very close together with exception of NONE value
export const AttributeType = {
    NONE: -2,
    FIRE: 0,
    WATER: 1,
    WIND: 2,
    EARTH: 3,
    HOLY: 4,
    UNHOLY: 5
}