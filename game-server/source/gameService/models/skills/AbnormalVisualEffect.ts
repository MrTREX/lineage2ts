export enum AbnormalVisualEffect {
    NONE,
    DOT_BLEEDING,
    DOT_POISON,
    DOT_FIRE,
    DOT_WATER,
    DOT_WIND,
    DOT_SOIL,
    STUN,
    SLEEP,
    SILENCE,
    ROOT,
    PARALYZE,
    FLESH_STONE,
    DOT_MP,
    BIG_HEAD,
    DOT_FIRE_AREA,
    CHANGE_TEXTURE,
    BIG_BODY,
    FLOATING_ROOT,
    DANCE_ROOT,
    GHOST_STUN,
    STEALTH,
    SEIZURE1,
    SEIZURE2,
    MAGIC_SQUARE,
    FREEZING,
    SHAKE,
    BLIND,
    ULTIMATE_DEFENCE,
    VP_UP,
    REAL_TARGET,
    DEATH_MARK,
    TURN_FLEE,
    VP_KEEP, // TODO: Find.
    // Special
    INVINCIBILITY,
    AIR_BATTLE_SLOW,
    AIR_BATTLE_ROOT,
    CHANGE_WP,
    CHANGE_HAIR_G,
    CHANGE_HAIR_P,
    CHANGE_HAIR_B,
    STIGMA_OF_SILEN,
    SPEED_DOWN,
    FROZEN_PILLAR,
    CHANGE_VES_S,
    CHANGE_VES_C,
    CHANGE_VES_D,
    TIME_BOMB, // High Five
    MP_SHIELD, // High Five
    NAVIT_ADVENT, // High Five
    // Event
    // TODO, currently not working.
    BR_NONE,
    BR_AFRO_NORMAL,
    BR_AFRO_PINK,
    BR_AFRO_GOLD,
    BR_POWER_OF_EVA, // High Five
    BR_HEADPHONE, // High Five
    BR_VESPER1,
    BR_VESPER2,
    BR_VESPER3,
    BR_SOUL_AVATAR,
    CHANGE_7ANNIVERSARY
}

export const enum AbnormalVisualType {
    Normal,
    Special,
    Event
}

export const AbnormalVisualEffectMap : { [ value: number ] : AbnormalVisualEffectValue} = {
    [ AbnormalVisualEffect.NONE ]: {
        mask: 0,
        type: AbnormalVisualType.Normal
    },

    [ AbnormalVisualEffect.DOT_BLEEDING ]: {
        mask: 0x00000001,
        type: AbnormalVisualType.Normal
    },

    [ AbnormalVisualEffect.DOT_POISON ]: {
        mask: 0x00000002,
        type: AbnormalVisualType.Normal
    },

    [ AbnormalVisualEffect.DOT_FIRE ]: {
        mask: 0x00000004,
        type: AbnormalVisualType.Normal
    },

    [ AbnormalVisualEffect.DOT_WATER ]: {
        mask: 0x00000008,
        type: AbnormalVisualType.Normal
    },

    [ AbnormalVisualEffect.DOT_WIND ]: {
        mask: 0x00000010,
        type: AbnormalVisualType.Normal
    },

    [ AbnormalVisualEffect.DOT_SOIL ]: {
        mask: 0x00000020,
        type: AbnormalVisualType.Normal
    },

    [ AbnormalVisualEffect.STUN ]: {
        mask: 0x00000040,
        type: AbnormalVisualType.Normal
    },

    [ AbnormalVisualEffect.SLEEP ]: {
        mask: 0x00000080,
        type: AbnormalVisualType.Normal
    },

    [ AbnormalVisualEffect.SILENCE ]: {
        mask: 0x00000100,
        type: AbnormalVisualType.Normal
    },

    [ AbnormalVisualEffect.ROOT ]: {
        mask: 0x00000200,
        type: AbnormalVisualType.Normal
    },

    [ AbnormalVisualEffect.PARALYZE ]: {
        mask: 0x00000400,
        type: AbnormalVisualType.Normal
    },

    [ AbnormalVisualEffect.FLESH_STONE ]: {
        mask: 0x00000800,
        type: AbnormalVisualType.Normal
    },

    [ AbnormalVisualEffect.DOT_MP ]: {
        mask: 0x00001000,
        type: AbnormalVisualType.Normal
    },

    [ AbnormalVisualEffect.BIG_HEAD ]: {
        mask: 0x00002000,
        type: AbnormalVisualType.Normal
    },

    [ AbnormalVisualEffect.DOT_FIRE_AREA ]: {
        mask: 0x00004000,
        type: AbnormalVisualType.Normal
    },

    [ AbnormalVisualEffect.CHANGE_TEXTURE ]: {
        mask: 0x00008000,
        type: AbnormalVisualType.Normal
    },

    [ AbnormalVisualEffect.BIG_BODY ]: {
        mask: 0x00010000,
        type: AbnormalVisualType.Normal
    },

    [ AbnormalVisualEffect.FLOATING_ROOT ]: {
        mask: 0x00020000,
        type: AbnormalVisualType.Normal
    },

    [ AbnormalVisualEffect.DANCE_ROOT ]: {
        mask: 0x00040000,
        type: AbnormalVisualType.Normal
    },

    [ AbnormalVisualEffect.GHOST_STUN ]: {
        mask: 0x00080000,
        type: AbnormalVisualType.Normal
    },

    [ AbnormalVisualEffect.STEALTH ]: {
        mask: 0x00100000,
        type: AbnormalVisualType.Normal
    },

    [ AbnormalVisualEffect.SEIZURE1 ]: {
        mask: 0x00200000,
        type: AbnormalVisualType.Normal
    },

    [ AbnormalVisualEffect.SEIZURE2 ]: {
        mask: 0x00400000,
        type: AbnormalVisualType.Normal
    },

    [ AbnormalVisualEffect.MAGIC_SQUARE ]: {
        mask: 0x00800000,
        type: AbnormalVisualType.Normal
    },

    [ AbnormalVisualEffect.FREEZING ]: {
        mask: 0x01000000,
        type: AbnormalVisualType.Normal
    },

    [ AbnormalVisualEffect.SHAKE ]: {
        mask: 0x02000000,
        type: AbnormalVisualType.Normal
    },

    [ AbnormalVisualEffect.BLIND ]: {
        mask: 0x04000000,
        type: AbnormalVisualType.Normal
    },

    [ AbnormalVisualEffect.ULTIMATE_DEFENCE ]: {
        mask: 0x08000000,
        type: AbnormalVisualType.Normal
    },

    [ AbnormalVisualEffect.VP_UP ]: {
        mask: 0x10000000,
        type: AbnormalVisualType.Normal
    },

    [ AbnormalVisualEffect.REAL_TARGET ]: {
        mask: 0x20000000,
        type: AbnormalVisualType.Normal
    },

    [ AbnormalVisualEffect.DEATH_MARK ]: {
        mask: 0x40000000,
        type: AbnormalVisualType.Normal
    },

    [ AbnormalVisualEffect.TURN_FLEE ]: {
        mask: 0x80000000,
        type: AbnormalVisualType.Normal
    },

    [ AbnormalVisualEffect.VP_KEEP ]: {
        mask: 0x10000000,
        type: AbnormalVisualType.Normal
    },

    // Special type
    [ AbnormalVisualEffect.INVINCIBILITY ]: {
        mask: 0x000001,
        type: AbnormalVisualType.Special
    },

    [ AbnormalVisualEffect.AIR_BATTLE_SLOW ]: {
        mask: 0x000002,
        type: AbnormalVisualType.Special
    },

    [ AbnormalVisualEffect.AIR_BATTLE_ROOT ]: {
        mask: 0x000004,
        type: AbnormalVisualType.Special
    },

    [ AbnormalVisualEffect.CHANGE_WP ]: {
        mask: 0x000008,
        type: AbnormalVisualType.Special
    },

    [ AbnormalVisualEffect.CHANGE_HAIR_G ]: {
        mask: 0x000010,
        type: AbnormalVisualType.Special
    },

    [ AbnormalVisualEffect.CHANGE_HAIR_P ]: {
        mask: 0x000020,
        type: AbnormalVisualType.Special
    },

    [ AbnormalVisualEffect.CHANGE_HAIR_B ]: {
        mask: 0x000040,
        type: AbnormalVisualType.Special
    },

    [ AbnormalVisualEffect.STIGMA_OF_SILEN ]: {
        mask: 0x000100,
        type: AbnormalVisualType.Special
    },

    [ AbnormalVisualEffect.SPEED_DOWN ]: {
        mask: 0x000200,
        type: AbnormalVisualType.Special
    },

    [ AbnormalVisualEffect.FROZEN_PILLAR ]: {
        mask: 0x000400,
        type: AbnormalVisualType.Special
    },

    [ AbnormalVisualEffect.CHANGE_VES_S ]: {
        mask: 0x000800,
        type: AbnormalVisualType.Special
    },

    [ AbnormalVisualEffect.CHANGE_VES_C ]: {
        mask: 0x001000,
        type: AbnormalVisualType.Special
    },

    [ AbnormalVisualEffect.CHANGE_VES_D ]: {
        mask: 0x002000,
        type: AbnormalVisualType.Special
    },

    [ AbnormalVisualEffect.TIME_BOMB ]: {
        mask: 0x004000,
        type: AbnormalVisualType.Special
    },

    [ AbnormalVisualEffect.MP_SHIELD ]: {
        mask: 0x008000,
        type: AbnormalVisualType.Special
    },

    [ AbnormalVisualEffect.NAVIT_ADVENT ]: {
        mask: 0x080000,
        type: AbnormalVisualType.Special
    },

    // Event type
    [ AbnormalVisualEffect.BR_NONE ]: {
        mask: 0x000000,
        type: AbnormalVisualType.Event
    },

    [ AbnormalVisualEffect.BR_AFRO_NORMAL ]: {
        mask: 0x000001,
        type: AbnormalVisualType.Event
    },

    [ AbnormalVisualEffect.BR_AFRO_PINK ]: {
        mask: 0x000002,
        type: AbnormalVisualType.Event
    },

    [ AbnormalVisualEffect.BR_AFRO_GOLD ]: {
        mask: 0x000004,
        type: AbnormalVisualType.Event
    },

    [ AbnormalVisualEffect.BR_POWER_OF_EVA ]: {
        mask: 0x000008,
        type: AbnormalVisualType.Event
    },

    [ AbnormalVisualEffect.BR_HEADPHONE ]: {
        mask: 0x000010,
        type: AbnormalVisualType.Event
    },

    [ AbnormalVisualEffect.BR_VESPER1 ]: {
        mask: 0x000020,
        type: AbnormalVisualType.Event
    },

    [ AbnormalVisualEffect.BR_VESPER2 ]: {
        mask: 0x000040,
        type: AbnormalVisualType.Event
    },

    [ AbnormalVisualEffect.BR_VESPER3 ]: {
        mask: 0x000080,
        type: AbnormalVisualType.Event
    },

    [ AbnormalVisualEffect.BR_SOUL_AVATAR ]: {
        mask: 0x000100,
        type: AbnormalVisualType.Event
    },

    [ AbnormalVisualEffect.CHANGE_7ANNIVERSARY ]: {
        mask: 0x000200,
        type: AbnormalVisualType.Event
    },
}

export interface AbnormalVisualEffectValue {
    mask: number,
    type: AbnormalVisualType,
}