import { Skill } from '../Skill'
import { L2Character } from '../actor/L2Character'
import { L2Object } from '../L2Object'
import { L2World } from '../../L2World'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { SkillCache } from '../../cache/SkillCache'
import { GeneralHelper } from '../../helpers/GeneralHelper'
import { BuffInfo } from './BuffInfo'
import { L2PcInstance } from '../actor/instance/L2PcInstance'
import { ShotType } from '../../enums/ShotType'
import { MagicSkillLaunched } from '../../packets/send/MagicSkillLaunched'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import aigle from 'aigle'
import _ from 'lodash'
import { PathFinding } from '../../../geodata/PathFinding'

export class SkillChannelizer {
    channelizer: number // object id of who is producing effect
    channelized: Array<number> = [] // affected object ids

    skill: Skill
    task: any

    constructor( character: L2Character ) {
        this.channelizer = character.objectId
    }

    stopChanneling() {
        if ( !this.isChanneling() ) {
            return
        }

        this.task.cancel()
        this.task = null

        if ( this.channelized.length > 0 ) {
            let channelizer : L2Character = L2World.getObjectById( this.channelizer ) as L2Character
            let channelSkillId = this.skill.getChannelingSkillId()
            _.each( this.channelized, ( objectId: number ) => {
                let character : L2Character = L2World.getObjectById( objectId ) as L2Character
                character.getSkillChannelized().removeChannelizer( channelSkillId, channelizer )
            } )

            this.channelized = []
        }

        this.skill = null
    }

    isChanneling(): boolean {
        return !!this.task
    }

    hasChannelized() {
        return !!this.channelized
    }

    startChanneling( skill: Skill ) {
        if ( this.isChanneling() ) {
            return
        }

        this.skill = skill
        this.task = setInterval( this.runTask.bind( this ), skill.getChannelingTickInterval() )
    }

    async runTask() {
        if ( !this.isChanneling() || !this.skill ) {
            return
        }

        let character = L2World.getObjectById( this.channelizer ) as L2Character
        if ( this.skill.getMpPerChanneling() > 0 ) {
            // Validate mana per tick.
            if ( character.getCurrentMp() < this.skill.getMpPerChanneling() ) {
                if ( character.isPlayer() ) {
                    character.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SKILL_REMOVED_DUE_LACK_MP ) )
                }
                character.abortCast()
                return
            }

            // Reduce mana per tick
            character.reduceCurrentMp( this.skill.getMpPerChanneling() )
        }

        // Apply channeling skills on the targets.
        if ( this.skill.getChannelingSkillId() > 0 ) {
            let baseSkill : Skill = SkillCache.getSkill( this.skill.getChannelingSkillId(), 1 )
            if ( !baseSkill ) {
                character.abortCast()
                return
            }

            let targetList : Array<L2Character> = []
            let targetIds : Array<number> = []
            let targets = this.skill.getTargetsOf( character )
            let skillId = this.skill.getChannelingSkillId()

            targets.forEach( ( targetObject : L2Object ) => {
                if ( targetObject.isCharacter() ) {
                    let target = targetObject as L2Character
                    targetList.push( target )
                    targetIds.push( target.objectId )
                    target.getSkillChannelized().addChannelizer( skillId, character )
                }
            } )

            if ( targetList.length === 0 ) {
                return
            }

            this.channelized = targetIds

            let currentSkill = this.skill
            let currentChannelizer = this
            await aigle.resolve( targetList ).each( async ( target: L2Character ) => {
                if ( !GeneralHelper.checkIfInRange( currentSkill.getEffectRange(), character, target, true ) ) {
                    return
                }

                if ( !PathFinding.canSeeTargetWithPosition( character, target ) ) {
                    return
                }

                let maxSkillLevel = SkillCache.getMaxLevel( skillId )
                let skillLevel = Math.min( target.getSkillChannelized().getChannerlizersSize( skillId ), maxSkillLevel )
                let info : BuffInfo = target.getEffectList().getBuffInfoBySkillId( skillId )

                if ( !info || ( info.getSkill().getLevel() < skillLevel ) ) {
                    let skill : Skill = SkillCache.getSkill( skillId, skillLevel )
                    if ( skill ) {
                        character.abortCast()
                        return false
                    }

                    // Update PvP status
                    if ( target.isPlayable() && character.isPlayer() && skill.isBad() ) {
                        ( character as L2PcInstance ).updatePvPStatusWithTarget( target )
                    }

                    await skill.applyEffects( character, target, false, false, true, 0 )

                    // Reduce shots.
                    if ( this.skill.useSpiritShot() ) {
                        character.setChargedShot( character.isChargedShot( ShotType.BlessedSpiritshot ) ? ShotType.BlessedSpiritshot : ShotType.Spiritshot, false )
                    } else {
                        character.setChargedShot( ShotType.Soulshot, false )
                    }

                    // Shots are re-charged every cast.
                    await character.rechargeShots( this.skill.useSoulShot(), this.skill.useSpiritShot() )
                }

                BroadcastHelper.dataToSelfBasedOnVisibility( character, MagicSkillLaunched( currentChannelizer.channelizer, currentChannelizer.skill.getId(), currentChannelizer.skill.getLevel(), [ target ] ) )
            } )

        }
    }
}