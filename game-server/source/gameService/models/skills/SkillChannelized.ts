import { L2Character } from '../actor/L2Character'
import { L2World } from '../../L2World'
import _ from 'lodash'

export class SkillChannelized {
    channelizers: { [ key: number ]: Array<number> } = {} // skillId per array of object ids

    removeChannelizer( skillId: number, character: L2Character ) {
        let objectIds: Array<number> = this.getChannelizers( skillId )

        _.pull( objectIds, character.objectId )
    }

    getChannelizers( skillId: number ) {
        return this.channelizers[ skillId ] || []
    }

    isChannelized() {
        let outcome = false

        _.each( this.channelizers, ( set: Array<number> ) => {
            if ( set.length > 0 ) {
                outcome = true
                return false
            }
        } )

        return outcome
    }

    abortChannelization() {
        _.each( this.channelizers, ( objectIdSet : Array<number> ) => {
            _.each( objectIdSet, ( objectId ) => {
                let character : L2Character = L2World.getObjectById( objectId ) as L2Character
                if ( character ) {
                    character.abortCast()
                }
            } )
        } )
    }

    addChannelizer( skillId: number, character: L2Character ) {
        if ( !this.channelizers[ skillId ] ) {
            this.channelizers[ skillId ] = []
        }

        this.channelizers[ skillId ].push( character.objectId )
    }

    getChannerlizersSize( skillId: number ) {
        return _.size( this.channelizers[ skillId ] )
    }
}