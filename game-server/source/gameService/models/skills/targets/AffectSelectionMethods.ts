import { AffectSelectionScope } from './AffectSelectionScope'
import { DeadPledge } from './affectSelectionMethods/DeadPledge'
import { Fan } from './affectSelectionMethods/Fan'
import { Party } from './affectSelectionMethods/Party'
import { PartyPledge } from './affectSelectionMethods/PartyPledge'
import { Pledge } from './affectSelectionMethods/Pledge'
import { PointBlank } from './affectSelectionMethods/PointBlank'
import { Range } from './affectSelectionMethods/Range'
import { RangeSortByHp } from './affectSelectionMethods/RangeSortByHp'
import { WyvernScope } from './affectSelectionMethods/WyvernScope'
import { StaticObjectScope } from './affectSelectionMethods/StaticObjectScope'
import { SquarePoint } from './affectSelectionMethods/SquarePoint'
import { Square } from './affectSelectionMethods/Square'
import { Single } from './affectSelectionMethods/Single'
import { RingRange } from './affectSelectionMethods/RingRange'
import { L2Character } from '../../actor/L2Character'
import { Skill } from '../../Skill'
import { L2Object } from '../../L2Object'

export type AffectSelectionMethodType = ( caster: L2Character, target: L2Object, skill: Skill ) => Array<L2Object>
export const ReturnNoTarget: AffectSelectionMethodType = () => undefined

// TODO : remove promise return type from selection method since there is no reason to have it and complicate wait times
export const AffectSelectionMethods: { [ key: number ]: AffectSelectionMethodType } = {
    /** Affects Valakas. */
    [ AffectSelectionScope.BALAKAS_SCOPE ]: ReturnNoTarget,
    /** Affects dead clan mates. */
    [ AffectSelectionScope.DEAD_PLEDGE ]: DeadPledge,
    /** Affects fan area. */
    [ AffectSelectionScope.FAN ]: Fan,
    /** Affects nothing. */
    [ AffectSelectionScope.NONE ]: ReturnNoTarget,
    /** Affects party members. */
    [ AffectSelectionScope.PARTY ]: Party,
    /** Affects party and clan mates. */
    [ AffectSelectionScope.PARTY_PLEDGE ]: PartyPledge,
    /** Affects clan mates. */
    [ AffectSelectionScope.PLEDGE ]: Pledge,
    /** Affects point blank targets, using caster as point of origin. */
    [ AffectSelectionScope.POINT_BLANK ]: PointBlank,
    /** Affects ranged targets, using selected target as point of origin. */
    [ AffectSelectionScope.RANGE ]: Range,
    /** Affects ranged targets sorted by HP, using selected target as point of origin. */
    [ AffectSelectionScope.RANGE_SORT_BY_HP ]: RangeSortByHp,
    /** Affects ranged targets, using selected target as point of origin. */
    [ AffectSelectionScope.RING_RANGE ]: RingRange,
    /** Affects a single target. */
    [ AffectSelectionScope.SINGLE ]: Single,
    /** Affects targets inside an square area, using selected target as point of origin. */
    [ AffectSelectionScope.SQUARE ]: Square,
    /** Affects targets inside an square area, using caster as point of origin. */
    [ AffectSelectionScope.SQUARE_PB ]: SquarePoint,
    /** Affects static object targets. */
    [ AffectSelectionScope.STATIC_OBJECT_SCOPE ]: StaticObjectScope,
    /** Affects wyverns. */
    [ AffectSelectionScope.WYVERN_SCOPE ]: WyvernScope,
    /** Affects dead command channel mates of the target. */
    [ AffectSelectionScope.DEAD_UNION ]: ReturnNoTarget,
}