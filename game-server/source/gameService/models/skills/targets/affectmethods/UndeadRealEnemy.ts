import { L2Character } from '../../../actor/L2Character'
import { L2Object } from '../../../L2Object'
import { L2Npc } from '../../../actor/L2Npc'
import { AffectObjectMethodType } from '../AffectMethodRegistry'

export const UndeadRealEnemy: AffectObjectMethodType = ( caster: L2Character, object: L2Object ): boolean => {
    if ( !object.isNpc() ) {
        return false
    }

    return ( object as L2Npc ).isUndead()
}