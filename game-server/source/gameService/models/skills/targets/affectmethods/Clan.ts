import { L2Character } from '../../../actor/L2Character'
import { L2Object } from '../../../L2Object'
import { AffectObjectMethodType } from '../AffectMethodRegistry'

export const Clan: AffectObjectMethodType = ( caster: L2Character, object: L2Object ): boolean => {
    if ( caster.isPlayable() ) {
        if ( !object.isPlayable() ) {
            return false
        }

        let clanId = caster.getClanId()
        if ( clanId === 0 ) {
            return false
        }

        return clanId === ( object as L2Character ).getClanId()
    }

    // TODO : what if clan npc is involved?

    return false
}