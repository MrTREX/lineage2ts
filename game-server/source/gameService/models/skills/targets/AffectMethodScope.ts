export enum AffectMethodScope {
    ALL,
    CLAN,
    FRIEND,
    HIDDEN_PLACE,
    INVISIBLE,
    NONE,
    NOT_FRIEND,
    OBJECT_DEAD_NPC_BODY,
    UNDEAD_REAL_ENEMY,
    WYVERN_OBJECT
}