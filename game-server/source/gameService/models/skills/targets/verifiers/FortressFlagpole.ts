import { TargetVerifierMethodType } from '../L2TargetType'
import { Skill } from '../../../Skill'
import { L2Character } from '../../../actor/L2Character'
import { L2Object } from '../../../L2Object'
import { SystemMessageBuilder } from '../../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../../packets/SystemMessageIdValues'
import { L2StaticObjectInstance } from '../../../actor/instance/L2StaticObjectInstance'
import { L2StaticObjectType } from '../../../../enums/L2StaticObjectType'

export const FortressFlagpole: TargetVerifierMethodType = ( skill: Skill, caster: L2Character, target: L2Object ): L2Object => {
    if ( !target
            || !target.isStaticObject()
            || ( target as L2StaticObjectInstance ).getType() !== L2StaticObjectType.FortFlagpole ) {
        caster.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
        return
    }

    return target
}