import { TargetVerifierMethodType } from '../L2TargetType'
import { Skill } from '../../../Skill'
import { L2Character } from '../../../actor/L2Character'
import { L2Object } from '../../../L2Object'
import { L2Npc } from '../../../actor/L2Npc'
import { SystemMessageBuilder } from '../../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../../packets/SystemMessageIdValues'

export const WyvernTarget: TargetVerifierMethodType = ( skill: Skill, caster: L2Character, target: L2Object ): L2Object => {
    if ( !target
            || !target.isNpc()
            || ( target as L2Npc ).getId() !== 12621 ) {
        caster.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
        return
    }

    return target
}