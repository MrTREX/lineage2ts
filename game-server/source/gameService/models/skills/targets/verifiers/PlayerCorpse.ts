import { TargetVerifierMethodType } from '../L2TargetType'
import { Skill } from '../../../Skill'
import { L2Character } from '../../../actor/L2Character'
import { L2Object } from '../../../L2Object'
import { SystemMessageBuilder } from '../../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../../packets/SystemMessageIdValues'
import { L2PcInstance } from '../../../actor/instance/L2PcInstance'
import { L2EffectType } from '../../../../enums/effects/L2EffectType'
import { QuestStateCache } from '../../../../cache/QuestStateCache'

export const PlayerCorpse: TargetVerifierMethodType = ( skill: Skill, caster: L2Character, target: L2Object ): L2Object => {
    if ( !target
            || !target.isPlayer()
            || !( target as L2PcInstance ).isDead() ) {
        caster.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
        return
    }

    if ( skill.hasEffectType( L2EffectType.Resurrection ) ) {
        if ( ( target as L2PcInstance ).isInSiegeArea() && !( target as L2PcInstance ).isInSiege() ) {
            caster.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_BE_RESURRECTED_DURING_SIEGE ) )
            return
        }

        let state = QuestStateCache.getQuestState( target.getObjectId(), 'Q00505_BloodOffering' )
        if ( state && state.isStarted() ) {
            caster.sendMessage( 'You may not resurrect participants in a festival.' )
            return
        }
    }

    return target
}