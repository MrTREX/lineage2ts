import { TargetVerifierMethodType } from '../L2TargetType'
import { Skill } from '../../../Skill'
import { L2Character } from '../../../actor/L2Character'
import { L2Object } from '../../../L2Object'
import { SystemMessageBuilder } from '../../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../../packets/SystemMessageIdValues'
import { L2PcInstance } from '../../../actor/instance/L2PcInstance'
import { PlayerSkillFlags } from '../../PlayerSkillFlags'

function getCorrectSkill( isSummon: boolean, player: L2PcInstance ): PlayerSkillFlags {
    if ( isSummon ) {
        return player.petSkillFlags
    }

    return player.mainSkillFlags
}

export const AnyEnemy: TargetVerifierMethodType = ( skill: Skill, caster: L2Character, target: L2Object ): L2Object => {
    if ( !target
            || !target.isCharacter()
            || ( target as L2Character ).isDead()
            || target.getObjectId() === caster.getObjectId()
            || ( target as L2Character ).isAlikeDead() ) {
        return
    }

    if ( target.isNpc() ) {
        if ( !target.isAttackable() || ( target.isTrap() && !target.isAutoAttackable( caster ) ) ) {
            caster.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
            return
        }

        return target
    }

    let player: L2PcInstance = caster.getActingPlayer()
    if ( player
            && !player.checkIfPvP( target as L2Character )
            && target.getActingPlayer().getKarma() === 0
            && !getCorrectSkill( caster.isSummon(), player ).isCtrlPressed ) {
        caster.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
        return
    }

    return target
}