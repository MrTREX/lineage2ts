import { TargetVerifierMethodType } from '../L2TargetType'
import { Skill } from '../../../Skill'
import { L2Character } from '../../../actor/L2Character'
import { L2Object } from '../../../L2Object'
import { SystemMessageBuilder } from '../../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../../packets/SystemMessageIdValues'

export const AllOthers: TargetVerifierMethodType = ( skill: Skill, caster: L2Character, target: L2Object ): L2Object => {
    if ( !target || target.getObjectId() === caster.getObjectId() ) {
        caster.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_USE_ON_YOURSELF ) )
        return
    }

    return target
}