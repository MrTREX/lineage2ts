import { TargetVerifierMethodType } from '../L2TargetType'
import { Skill } from '../../../Skill'
import { L2Character } from '../../../actor/L2Character'
import { L2Object } from '../../../L2Object'
import { SystemMessageBuilder } from '../../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../../packets/SystemMessageIdValues'

export const NotEnemy: TargetVerifierMethodType = ( skill: Skill, caster: L2Character, target: L2Object ): L2Object => {
    if ( !target
            || !target.isCharacter()
            || ( target as L2Character ).isDead()
            || target.isAutoAttackable( caster ) ) {
        caster.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
        return
    }

    return target
}