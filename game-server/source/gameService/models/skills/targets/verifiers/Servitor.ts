import { TargetVerifierMethodType } from '../L2TargetType'
import { Skill } from '../../../Skill'
import { L2Character } from '../../../actor/L2Character'
import { L2Object } from '../../../L2Object'
import { L2Summon } from '../../../actor/L2Summon'

export const Servitor: TargetVerifierMethodType = ( skill: Skill, caster: L2Character, target: L2Object ): L2Object => {
    let summon : L2Summon = caster.getSummon()

    if ( summon && summon.isServitor() ) {
        return summon
    }

    return
}