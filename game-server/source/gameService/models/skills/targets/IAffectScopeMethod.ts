import { L2Character } from '../../actor/L2Character'
import { Skill } from '../../Skill'
import { L2Object } from '../../L2Object'

export interface IAffectScopeMethod {
    affectTargets( caster: L2Character, target: L2Character, skill: Skill ) : Promise<Array<L2Object>>
}