import { L2Character } from '../../../actor/L2Character'
import { Skill } from '../../../Skill'
import { L2Object } from '../../../L2Object'
import { L2World } from '../../../../L2World'
import { AffectSelectionMethodType } from '../AffectSelectionMethods'

export const StaticObjectScope: AffectSelectionMethodType = ( caster: L2Character, target: L2Object, skill: Skill ): Array<L2Object> => {
    let affectLimit = skill.getAffectLimit()
    let targets: Array<L2Object> = []

    L2World.getVisibleCharacters( caster, skill.getAffectRange() ).some( ( character: L2Character ) => {
        if ( !character.isStaticObject() || !skill.affectObjectMethod( caster, character ) ) {
            return false
        }

        targets.push( character )

        return affectLimit > 0 && targets.length >= affectLimit
    } )

    return targets
}