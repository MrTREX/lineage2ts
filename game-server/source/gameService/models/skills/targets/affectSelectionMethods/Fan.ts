import { L2Character } from '../../../actor/L2Character'
import { Skill } from '../../../Skill'
import { L2Object } from '../../../L2Object'
import { GeneralHelper } from '../../../../helpers/GeneralHelper'
import { L2World } from '../../../../L2World'
import { AffectSelectionMethodType } from '../AffectSelectionMethods'
import _ from 'lodash'
import { PathFinding } from '../../../../../geodata/PathFinding'

export const Fan: AffectSelectionMethodType = ( character: L2Character, target: L2Object, skill: Skill ): Array<L2Object> => {
    if ( !target.isCharacter() || !skill.getFanRange() ) {
        return
    }

    let headingAngle = GeneralHelper.calculateAngleFromLocations( character, target )
    let affectLimit = skill.getAffectLimit()
    let [ notUsed, fanStartingAngle, fanRadius, fanAngle ] = skill.getFanRange()

    let targets: Array<L2Object> = []
    _.each( L2World.getVisibleCharacters( target, fanRadius, true ), ( object: L2Character ): boolean => {
        if ( object.isDead() ) {
            return
        }

        if ( Math.abs( GeneralHelper.calculateAngleFromLocations( character, object ) - ( headingAngle + fanStartingAngle ) ) > fanAngle / 2 ) {
            return
        }

        if ( !skill.affectObjectMethod( character, object ) ) {
            return
        }

        if ( !PathFinding.canSeeTargetWithPosition( character, object ) ) {
            return
        }

        targets.push( object )

        if ( affectLimit > 0 && targets.length >= affectLimit ) {
            return false
        }
    } )

    return targets
}