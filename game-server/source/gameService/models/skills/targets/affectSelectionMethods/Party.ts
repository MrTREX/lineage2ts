import { L2Character } from '../../../actor/L2Character'
import { Skill } from '../../../Skill'
import { L2Object } from '../../../L2Object'
import { L2World } from '../../../../L2World'
import { L2PcInstance } from '../../../actor/instance/L2PcInstance'
import { L2Summon } from '../../../actor/L2Summon'
import { GeneralHelper } from '../../../../helpers/GeneralHelper'
import { L2Party } from '../../../L2Party'
import { PlayerGroupCache } from '../../../../cache/PlayerGroupCache'
import { AffectSelectionMethodType } from '../AffectSelectionMethods'

export const Party: AffectSelectionMethodType = ( character: L2Character, target: L2Object, skill: Skill ): Array<L2Object> => {
    if ( !target.isCharacter() ) {
        return
    }

    let affectRange = skill.getAffectRange()
    let party: L2Party = PlayerGroupCache.getParty( character.getObjectId() )
    let affectLimit = skill.getAffectLimit()

    if ( party ) {

        let targets: Array<L2Object> = []

        L2World.getVisiblePlayers( target, affectRange, true ).some( ( player: L2PcInstance ) => {
            if ( !party.getMembers().includes( player.getObjectId() ) ) {
                return false
            }

            if ( skill.affectObjectMethod( character, player ) ) {
                targets.push( player )
            }

            if ( affectLimit > 0 && targets.length >= affectLimit ) {
                return true
            }

            let summon: L2Summon = player.getSummon()
            if ( summon
                    && skill.affectObjectMethod( summon, target )
                    && GeneralHelper.checkIfInRange( affectRange, target, summon, true ) ) {
                targets.push( summon )
            }

            if ( affectLimit > 0 && targets.length >= affectLimit ) {
                return true
            }

            return false
        } )

        return targets
    }

    let targets: Array<L2Object> = []
    let player: L2PcInstance = character.getActingPlayer()

    if ( skill.affectObjectMethod( player, target ) ) {
        targets.push( player )
    }

    if ( affectLimit > 0 && targets.length >= affectLimit ) {
        return targets
    }

    let summon: L2Summon = player.getSummon()
    if ( summon
            && skill.affectObjectMethod( summon, target )
            && GeneralHelper.checkIfInRange( affectRange, target, summon, true ) ) {
        targets.push( summon )
    }

    return targets
}