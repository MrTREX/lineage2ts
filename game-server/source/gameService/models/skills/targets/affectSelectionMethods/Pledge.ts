import { L2Character } from '../../../actor/L2Character'
import { Skill } from '../../../Skill'
import { L2Object } from '../../../L2Object'
import { L2Clan } from '../../../L2Clan'
import { L2PcInstance } from '../../../actor/instance/L2PcInstance'
import { L2Npc } from '../../../actor/L2Npc'
import { L2World } from '../../../../L2World'
import { TvTEvent } from '../../../entity/TvTEvent'
import { L2Summon } from '../../../actor/L2Summon'
import { GeneralHelper } from '../../../../helpers/GeneralHelper'
import { AffectSelectionMethodType } from '../AffectSelectionMethods'
import _ from 'lodash'

export const Pledge: AffectSelectionMethodType = ( caster: L2Character, target: L2Object, skill: Skill ): Array<L2Object> => {
    if ( target.isPlayer() ) {
        return getPlayerTargets( caster, target as L2PcInstance, skill )
    }

    if ( target.isNpc() ) {
        return getNpcTargets( caster as L2Npc, skill )
    }

    return
}

function getPlayerTargets( caster: L2Character, target: L2PcInstance, skill: Skill ): Array<L2Object> {
    let affectRange = skill.getAffectRange()
    let affectLimit = skill.getAffectLimit()

    let clan: L2Clan = target.getClan()
    let targets: Array<L2Object> = []

    if ( clan ) {
        _.each( L2World.getVisiblePlayers( target, affectRange, true ), ( player: L2PcInstance ) => {
            if ( !clan.isMember( player.getObjectId() ) ) {
                return
            }

            if ( target.isInDuel() ) {
                if ( target.getDuelId() !== player.getDuelId() ) {
                    return
                }

                if ( target.isInParty()
                        && player.isInParty()
                        && target.getParty().getLeaderObjectId() !== player.getParty().getLeaderObjectId() ) {
                    return
                }
            }

            if ( !target.checkPvpSkill( player, skill ) ) {
                return
            }

            if ( !TvTEvent.checkForTvTSkill( target, player, skill ) ) {
                return
            }

            if ( target.isInOlympiadMode() ) {
                if ( target.getOlympiadGameId() !== player.getOlympiadGameId() ) {
                    return
                }

                if ( target.getOlympiadSide() !== player.getOlympiadSide() ) {
                    return
                }
            }

            if ( skill.affectObjectMethod( caster, player ) ) {
                targets.push( player )
            }

            if ( affectLimit > 0 && targets.length >= affectLimit ) {
                return false
            }

            let summon: L2Summon = player.getSummon()
            if ( summon
                    && skill.affectObjectMethod( caster, summon )
                    && GeneralHelper.checkIfInRange( affectRange, target, summon, true ) ) {
                targets.push( summon )
            }

            if ( affectLimit > 0 && targets.length >= affectLimit ) {
                return false
            }
        } )

        return targets
    }

    if ( skill.affectObjectMethod( caster, target ) ) {
        targets.push( target )
    }

    if ( affectLimit > 0 && targets.length >= affectLimit ) {
        return targets
    }

    let summon: L2Summon = target.getSummon()
    if ( summon
            && skill.affectObjectMethod( caster, summon )
            && GeneralHelper.checkIfInRange( affectRange, target, summon, true ) ) {
        targets.push( summon )
    }

    return targets
}

function getNpcTargets( npc: L2Npc, skill: Skill ) {
    let targets: Array<L2Object> = [ npc ]

    let clans: Array<number> = npc.getTemplate().getClans()
    if ( _.isEmpty( clans ) ) {
        return targets
    }

    let affectRange = skill.getAffectRange()
    let affectLimit = skill.getAffectLimit()

    _.each( L2World.getVisibleCharacters( npc, affectRange ), ( object: L2Character ) => {
        if ( !object.isNpc() ) {
            return
        }

        if ( !npc.isInMyClan( object as L2Npc ) ) {
            return
        }

        targets.push( object )

        if ( affectLimit > 0 && targets.length >= affectLimit ) {
            return false
        }
    } )

    return targets
}