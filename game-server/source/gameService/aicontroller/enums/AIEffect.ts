import { AIIntent } from './AIIntent'

export const enum AIEffect {
    Attacked,
    MoveFinished,
    MoveFailed,
    TargetDead,
    ReadyToAttack,
    ForceIntentChange,
    MoveToLocation,
    SkillCastFinished,
    SkillCastCanceled,
    ForceSpellCast,
    FollowObject,
    ForceAttackTarget,
    InteractTarget,
    FleeAway,
    OwnerHPReduced,
    OwnerMPReduced
}

export type AIEventData = Record<string, number | boolean>

export interface AIForceIntentChangeEvent extends AIEventData {
    nextIntent: AIIntent
}

export interface AIAttackedEvent extends AIEventData {
    attackerId: number
    damage: number
    aggro: number
}

export interface AIMoveToLocationEvent extends AIEventData {
    x: number
    y: number
    z: number
    instanceId: number
    heading: number
}

export interface AIForceSpellCastEvent extends AIEventData {
    skillId: number
    skillLevel: number
    targetId: number
    isControlPressed : boolean
    isShiftPressed : boolean
}

export interface AIFollowObjectEvent extends AIEventData {
    targetId: number
    distance: number
}

export interface AITargetDeadEvent extends AIEventData {
    targetId: number
}

export interface AIForceAttackTargetEvent extends AIEventData {
    targetId: number
}

export interface AIInteractTargetEvent extends AIEventData {
    targetId: number
    shouldInteract: boolean
}

export interface AIFleeAwayEvent extends AIEventData {
    time: number
}

export interface AISkillCastFinishedEvent extends AIEventData {
    skillId: number
    isSimultaneous: boolean
}

export interface AISkillCastCanceledEvent extends AIEventData {
    skillId: number
    isSimultaneous: boolean
}

export interface AIOwnerHpReducedEvent extends AIEventData {
    damage: number
    attackerId: number
}

export interface AIOwnerMpReducedEvent extends AIEventData {
    damage: number
}