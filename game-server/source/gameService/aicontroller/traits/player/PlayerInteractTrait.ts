import { IAITrait } from '../../interface/IAITrait'
import { AIIntent } from '../../enums/AIIntent'
import { EffectsCache } from '../../EffectCache'
import { AIEffect, AIEventData, AIInteractTargetEvent } from '../../enums/AIEffect'
import { L2World } from '../../../L2World'
import { L2PcInstance } from '../../../models/actor/instance/L2PcInstance'
import { L2Character } from '../../../models/actor/L2Character'
import { L2Object } from '../../../models/L2Object'
import { L2DoorInstance } from '../../../models/actor/instance/L2DoorInstance'
import { ActionFailed } from '../../../packets/send/ActionFailed'
import { PlayerBaseMoveAction } from '../actions/PlayerBaseMoveAction'
import { L2ItemInstance } from '../../../models/items/instance/L2ItemInstance'
import { ObjectPool } from '../../../helpers/ObjectPoolHelper'
import { AIEffectHelper } from '../../helpers/AIEffectHelper'
import { MoveParameters } from '../actions/BaseMoveAction'
import { L2NpcValues } from '../../../values/L2NpcValues'

interface PlayerInteractData {
    targetId: number
    shouldInteract: boolean
}

const playerParameters: { [ playerId: number ]: PlayerInteractData } = {}
const traitParametersCache = new ObjectPool( 'PlayerInteractTrait', (): PlayerInteractData => {
    return {
        targetId: 0,
        shouldInteract: false,
    }
} )

const enum InteractOffset {
    General = 36,
    Npc = 54,
    Item = 10
}

const processedEffects : Set<AIEffect> = new Set<AIEffect>( [
    AIEffect.MoveFinished,
    AIEffect.InteractTarget
] )

class PlayerInteractBaseTrait extends PlayerBaseMoveAction implements IAITrait {
    switchMap: Record<number, AIIntent> = {
        [ AIEffect.MoveToLocation ]: AIIntent.WAITING,
        [ AIEffect.MoveFailed ]: AIIntent.WAITING,
        [ AIEffect.ForceAttackTarget ]: AIIntent.ATTACK,
        [ AIEffect.ForceSpellCast ]: AIIntent.CAST,
    }

    getName(): string {
        return 'PlayerInteractTrait'
    }

    describeState( objectId: number, otherPlayerId: number ): void {
        let player = L2World.getPlayer( objectId ) as L2PcInstance
        return this.describeMovementAction( player, otherPlayerId )
    }

    async processEffect( objectId: number, effect: AIEffect, parameters: AIEventData ): Promise<void> {
        switch ( effect ) {
            case AIEffect.MoveFinished:
                this.stopFollowTask( objectId )

                let data = playerParameters[ objectId ]
                if ( !data ) {
                    AIEffectHelper.scheduleNextIntent( L2World.getPlayer( objectId ), AIIntent.WAITING )
                    return
                }

                if ( this.startInteraction( L2World.getPlayer( objectId ), L2World.getObjectById( data.targetId ), data.shouldInteract ) ) {
                    AIEffectHelper.scheduleNextIntent( L2World.getPlayer( objectId ), AIIntent.WAITING )
                    return
                }

                break

            case AIEffect.InteractTarget:
                this.stopFollowTask( objectId )

                let interaction = parameters as AIInteractTargetEvent
                if ( this.onInteractWithTarget( L2World.getPlayer( objectId ), interaction.targetId, interaction.shouldInteract ) ) {
                    AIEffectHelper.scheduleNextIntent( L2World.getPlayer( objectId ), AIIntent.WAITING )
                    return
                }

                break
        }
    }

    onStart( playerId: number ): Promise<never> {
        let parameters = EffectsCache.getParameters( playerId, AIEffect.InteractTarget ) as AIInteractTargetEvent
        if ( !parameters ) {
            return
        }

        let player = L2World.getPlayer( playerId )
        this.onInteractWithTarget( player, parameters.targetId, parameters.shouldInteract )
    }

    onEnd( objectId: number ): void {
        this.recycleTrackingData( objectId )

        let player = L2World.getPlayer( objectId ) as L2PcInstance
        if ( !player.isMoving() ) {
            return
        }

        return player.abortMoving()
    }

    recycleTrackingData( playerId: number ): void {
        let data = playerParameters[ playerId ]
        if ( data ) {
            delete playerParameters[ playerId ]
            traitParametersCache.recycleValue( data )
        }
    }

    setInteractionParameters( player: L2PcInstance, target: L2Object, shouldInteract: boolean ): void {
        let data = playerParameters[ player.getObjectId() ]
        if ( !data ) {
            data = traitParametersCache.getValue()
            playerParameters[ player.getObjectId() ] = data
        }

        data.targetId = target.getObjectId()
        data.shouldInteract = shouldInteract
    }

    isMovingNeeded( player: L2PcInstance, target: L2Object, offset : number ): boolean {
        if ( target.isDoor() ) {
            return !( target as L2DoorInstance ).isInsideRadius( player, L2NpcValues.interactionDistance )
        }
        let modifiedOffset: number = target.isCharacter() && ( target as L2Character ).isMoving() ? offset + 50 : offset
        return !player.isInsideRadius( target, modifiedOffset )
    }

    /*
    Possible scenarios:
    - player cannot move
    - target no longer available/visible
    - player is already moving to another target
    - player is moving near target
    - player is near target
 */
    startInteraction( player: L2PcInstance, target: L2Object, shouldInteract: boolean ): boolean {
        if ( !player ) {
            return true
        }

        if ( !target ) {
            player.sendOwnedData( ActionFailed() )
            return true
        }

        if ( !target.isVisible() || ( target.isItem() && player.cannotPickItem() ) ) {
            player.sendOwnedData( ActionFailed() )
            return true
        }

        /*
            There are two offset distances in play when additional movement is needed:
            - target is not within interaction distance of player
            - pick movement offset depending on target, item having shorter offset due to most items being grouped (mob dropped loot for example)
         */
        let movementOffset = this.getMovementOffset( target )
        if ( this.isMovingNeeded( player, target, movementOffset ) ) {
            if ( player.isMovementDisabled() ) {
                player.sendOwnedData( ActionFailed() )
                return true
            }

            this.setInteractionParameters( player, target, shouldInteract )
            this.moveToTarget( player, target, movementOffset, this.runTargetTracking.bind( this ) )
            return false
        }

        /*
            Very important to disconnect current promise chain from interaction since there can be
            AIController effects applied at same time, potentially causing a long call stack.
         */
        this.performInteraction( player, target, this.getInteraction( player.getObjectId() ) )
        return true
    }

    getMovementOffset( target: L2Object ) : InteractOffset {
        if ( target.isItem() ) {
            return InteractOffset.Item
        }

        if ( target.isNpc() ) {
            return InteractOffset.Npc
        }

        return InteractOffset.General
    }

    performInteraction( player: L2PcInstance, target: L2Object, shouldInteract: boolean ): Promise<void> {
        if ( target.isItem() ) {
            return player.doPickupItem( target as L2ItemInstance )
        }

        /*
            TODO: various actions based on current player class
            - if target is spoiled by player, collect items via sweep
         */
        player.setTarget( target )
        return target.onInteraction( player, shouldInteract )
    }

    getInteraction( playerId: number ): boolean {
        let data = playerParameters[ playerId ]
        return data && data.shouldInteract
    }

    onInteractWithTarget( player: L2PcInstance, targetId: number, shouldInteract: boolean ): boolean {
        let target = L2World.getObjectById( targetId )
        if ( !target || target.isStaticObject() ) {
            return false
        }

        return this.startInteraction( player, target, shouldInteract )
    }

    runTargetTracking( objectId: number, parameters: MoveParameters ) : void {
        let player: L2PcInstance = L2World.getPlayer( objectId )
        if ( !player ) {
            this.stopFollowTask( objectId )
            this.recycleTrackingData( objectId )
            return
        }

        let data = playerParameters[ objectId ]
        if ( !data ) {
            this.stopFollowTask( objectId )
            return
        }

        let target = L2World.getObjectById( data.targetId )
        if ( !target ) {
            return this.stopFollowTask( objectId )
        }

        if ( target.isCharacter() && this.hasTargetMoved( parameters, target as L2Character ) ) {
            return
        }

        if ( this.isMovingNeeded( player, target, parameters.offset ) ) {
            return this.moveToTarget( player, target, parameters.offset )
        }

        return this.stopFollowTask( objectId )
    }

    canProcessEffect( effect: AIEffect ): boolean {
        return processedEffects.has( effect )
    }

    canSwitchIntent( effect: AIEffect ): boolean {
        return this.switchMap[ effect ] !== undefined
    }

    switchIntent( effect: AIEffect ): AIIntent {
        return this.switchMap[ effect ]
    }
}

export const PlayerInteractTrait = new PlayerInteractBaseTrait()