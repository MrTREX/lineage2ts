import { IAITrait } from '../../interface/IAITrait'
import { AIIntent } from '../../enums/AIIntent'
import { L2World } from '../../../L2World'
import { L2PcInstance } from '../../../models/actor/instance/L2PcInstance'
import { AIEffect, AIEventData, AIForceSpellCastEvent } from '../../enums/AIEffect'
import { Skill } from '../../../models/Skill'
import { SkillCache } from '../../../cache/SkillCache'
import { L2TargetType } from '../../../models/skills/targets/L2TargetType'
import { EffectsCache } from '../../EffectCache'
import { L2Character } from '../../../models/actor/L2Character'
import { SystemMessageBuilder } from '../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../packets/SystemMessageIdValues'
import { ActionFailed } from '../../../packets/send/ActionFailed'
import { AIEffectHelper } from '../../helpers/AIEffectHelper'
import { PlayerBaseMoveAction } from '../actions/PlayerBaseMoveAction'
import { FightStanceCache } from '../../../cache/FightStanceCache'
import { AutoAttackStart } from '../../../packets/send/AutoAttackStart'
import { PacketDispatcher } from '../../../PacketDispatcher'
import { PathFinding } from '../../../../geodata/PathFinding'
import { MoveParameters } from '../actions/BaseMoveAction'
import { L2Region } from '../../../enums/L2Region'
import { L2Playable } from '../../../models/actor/L2Playable'
import { AreaType } from '../../../models/areas/AreaType'

const processedEffects : Set<AIEffect> = new Set<AIEffect>( [
        AIEffect.MoveFinished,
        AIEffect.TargetDead,
        AIEffect.SkillCastCanceled,
        AIEffect.SkillCastFinished,
        AIEffect.ForceSpellCast,
] )

/*
    Cast intent is invoked specifically to cast a skill. However, casting a skill may involve following behaviors:
    - moving to target location
    - moving to chase a target
    - stopping and casting a skill

    Thus, expectation is to attempt to cast skill, then quickly turn back to previous intent.
 */
class PlayerCastBaseTrait extends PlayerBaseMoveAction implements IAITrait {
    switchMap: Record<number, AIIntent> = {
        [ AIEffect.ForceAttackTarget ]: AIIntent.ATTACK,
        [ AIEffect.FollowObject ]: AIIntent.WAITING,
        [ AIEffect.MoveToLocation ]: AIIntent.WAITING
    }

    getName(): string {
        return 'PlayerCastTrait'
    }

    describeState( objectId: number, playerId: number ): void {
        let player = L2World.getPlayer( objectId ) as L2PcInstance
        this.describeMovementAction( player, playerId )

        if ( FightStanceCache.hasPlayableStance( objectId ) ) {
            PacketDispatcher.sendOwnedData( playerId, AutoAttackStart( objectId ) )
        }
    }

    processEffect( objectId: number, effect: AIEffect, parameters: AIEventData ): Promise<void> {
        switch ( effect ) {
            case AIEffect.MoveFinished:
                this.stopFollowTask( objectId )
                return this.onStart( objectId )

            case AIEffect.TargetDead:
            case AIEffect.SkillCastCanceled:
            case AIEffect.SkillCastFinished:
                return this.onStart( objectId )

            case AIEffect.ForceSpellCast:
                return
        }
    }

    canProcessEffect( effect: AIEffect ): boolean {
        return processedEffects.has( effect )
    }

    onStart( objectId: number ): Promise<void> {
        let player = L2World.getPlayer( objectId )

        if ( this.cannotCast( player ) ) {
            AIEffectHelper.scheduleNextIntent( player, this.getNextIntent( player ) )
            return
        }

        if ( !player.isRunning() ) {
            player.setIsRunning( true, true )
        }

        let forceSkillParameters = EffectsCache.peekParameters( objectId, AIEffect.ForceSpellCast ) as AIForceSpellCastEvent
        if ( forceSkillParameters ) {
            return this.castUsingParameters( player, forceSkillParameters )
        }

        AIEffectHelper.scheduleNextIntent( player, this.getNextIntent( player ) )
    }

    cannotCast( player: L2PcInstance ): boolean {
        return player.isCastingNow()
                || player.isAlikeDead()
                || player.inObserverMode()
                || player.isTeleporting()
    }

    onEnd( objectId: number ): void {
        this.removeMovementState( objectId )
        let player = L2World.getPlayer( objectId )
        if ( player ) {
            player.abortCast( false )
        }

        EffectsCache.resetParameters( objectId, AIEffect.ForceSpellCast )
    }

    castUsingParameters( player: L2PcInstance, forceSkillParameters: AIForceSpellCastEvent ): Promise<void> {
        let nextSkill: Skill = SkillCache.getSkill( forceSkillParameters.skillId, forceSkillParameters.skillLevel )

        if ( !player.canCastSkill( nextSkill ) ) {
            AIEffectHelper.scheduleNextIntent( player, this.getNextIntent( player ) )
            return
        }

        if ( nextSkill.getTargetType() === L2TargetType.GROUND && player.getGroundSkillLocation() ) {
            return this.castGroundSkill( player, nextSkill, forceSkillParameters.isControlPressed, forceSkillParameters.isShiftPressed )
        }

        let skillTrackingTarget = L2World.getObjectById( forceSkillParameters.targetId ) as L2Character
        if ( !skillTrackingTarget ) {
            AIEffectHelper.scheduleNextIntent( player, this.getNextIntent( player ) )
            return
        }

        if ( skillTrackingTarget.isPlayable() && nextSkill.isBad() ) {
            let targetPlayer = skillTrackingTarget.getActingPlayer()
            if ( targetPlayer.isProtectionBlessingAffected()
                    && ( player.getLevel() - targetPlayer.getLevel() ) >= 10
                    && player.getKarma() > 0
                    && !skillTrackingTarget.isInArea( AreaType.PVP ) ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_IS_INCORRECT ) )
                player.sendOwnedData( ActionFailed() )

                AIEffectHelper.scheduleNextIntent( player, this.getNextIntent( player ) )
                return
            }

            if ( player.isProtectionBlessingAffected()
                    && ( targetPlayer.getLevel() - player.getLevel() ) >= 10
                    && targetPlayer.getKarma() > 0
                    && !skillTrackingTarget.isInArea( AreaType.PVP ) ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_IS_INCORRECT ) )
                player.sendOwnedData( ActionFailed() )

                AIEffectHelper.scheduleNextIntent( player, this.getNextIntent( player ) )
                return
            }

            if ( targetPlayer.isCursedWeaponEquipped() && ( player.getLevel() <= 20 || targetPlayer.getLevel() <= 20 ) ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_IS_INCORRECT ) )
                player.sendOwnedData( ActionFailed() )

                AIEffectHelper.scheduleNextIntent( player, this.getNextIntent( player ) )
                return
            }
        }

        return this.castNormalSkill( player, skillTrackingTarget, nextSkill, forceSkillParameters.isControlPressed, forceSkillParameters.isShiftPressed )
    }

    async performCast( player: L2PcInstance, skill: Skill, isControlPressed: boolean, isShiftPressed: boolean ): Promise<void> {
        player.mainSkillFlags.isCtrlPressed = isControlPressed
        player.mainSkillFlags.isShiftPressed = isShiftPressed

        await player.doCast( skill )

        /*
            Due to cast checks being performed it is possible that skill conditions cannot be
            satisfied. Hence, casting would not start, preventing from current trait ending on cast end.
            At this point we can check if cast is successful, otherwise reset intent.
         */
        if ( !player.isCasting() ) {
            AIEffectHelper.scheduleNextIntent( player, this.getNextIntent( player ) )
        }
    }

    async castNormalSkill( player: L2PcInstance, target: L2Character, skill: Skill, isControlPressed: boolean, isShiftPressed: boolean ): Promise<void> {
        let originDistance = player.calculateDistance( target )
        let distanceToTarget = Math.floor( originDistance - player.getTemplate().getCollisionRadius() )
        let canSeeTarget: boolean = PathFinding.canSeeTargetWithPosition( player, target )

        /*
            Since current method can be called when tracking target (running up to target to cast skill),
            we must stop player and let cast happen via AIEffect loop, meaning not here. Since if cast is done here,
            trait will detect no cast parameters and will switch to another intent directly, while we must wait for
            cast spell effect (success/cancel) to chain actions.
         */
        if ( !canSeeTarget || distanceToTarget > skill.getCastRange() ) {
            let offset = Math.max( skill.getCastRange(), player.getTemplate().getCollisionRadius() )
            this.startMoveToTarget( player, target, offset, target.isMoving() ? null : this.runFollowTask.bind( this ) )
            return
        }

        player.abortMoving()

        EffectsCache.resetParameters( player.getObjectId(), AIEffect.ForceSpellCast )
        return this.performCast( player, skill, isControlPressed, isShiftPressed )
    }

    castGroundSkill( player: L2PcInstance, skill: Skill, isControlPressed: boolean, isShiftPressed: boolean ): Promise<void> {
        let distance = player.calculateDistance( player.getGroundSkillLocation() )
        let attackRange = player.getMagicalAttackRange( skill )
        if ( distance > attackRange ) {
            return this.startMoveToLocation( player, player.getGroundSkillLocation(), distance - attackRange )
        }

        player.abortMoving()

        EffectsCache.resetParameters( player.getObjectId(), AIEffect.ForceSpellCast )
        player.setGroundSkillLocation( null )
        return this.performCast( player, skill, isControlPressed, isShiftPressed )
    }

    runFollowTask( objectId: number, parameters: MoveParameters ): void {
        let player = L2World.getPlayer( objectId )
        if ( !player ) {
            return AIEffectHelper.scheduleNextIntent( player, this.getNextIntent( player ) )
        }

        let followTarget = L2World.getObjectById( parameters.targetId )
        if ( !followTarget ) {
            return AIEffectHelper.scheduleNextIntent( player, this.getNextIntent( player ) )
        }

        if ( followTarget.isCharacter() && !this.hasTargetMoved( parameters, followTarget as L2Character ) ) {
            return
        }

        let distanceToTarget = player.calculateDistance( followTarget, true )

        if ( distanceToTarget > L2Region.Length ) {
            return this.stopAnyMovement( player )
        }

        if ( distanceToTarget > parameters.offset ) {
            return this.moveToTarget( player, followTarget, parameters.offset )
        }

        return this.stopAnyMovement( player )
    }

    getNextIntent( player : L2PcInstance ) : AIIntent {
        if ( player.isUnderAttack()
            || EffectsCache.hasParameters( player.getObjectId(), AIEffect.Attacked )
            || EffectsCache.hasParameters( player.getObjectId(), AIEffect.ForceAttackTarget ) ) {
            return AIIntent.ATTACK
        }

        return AIIntent.WAITING
    }

    canSwitchIntent( effect: AIEffect ): boolean {
        return this.switchMap[ effect ] !== undefined
    }

    switchIntent( effect: AIEffect ): AIIntent {
        return this.switchMap[ effect ]
    }

    stopAnyMovement( attacker: L2Playable ): void {
        this.abortMovement( attacker )
        AIEffectHelper.notifyMoveFinished( attacker )
    }
}

export const PlayerCastTrait = new PlayerCastBaseTrait()