import { BaseMoveAction } from '../actions/BaseMoveAction'
import { IAITrait } from '../../interface/IAITrait'
import { L2World } from '../../../L2World'
import { L2Summon } from '../../../models/actor/L2Summon'
import { AIIntent } from '../../enums/AIIntent'
import { AIEffect, AIEventData, AIInteractTargetEvent } from '../../enums/AIEffect'
import { EffectsCache } from '../../EffectCache'
import { AIEffectHelper } from '../../helpers/AIEffectHelper'
import { GeneralHelper } from '../../../helpers/GeneralHelper'
import { ActionFailed } from '../../../packets/send/ActionFailed'
import { L2ItemInstance } from '../../../models/items/instance/L2ItemInstance'

const interactOffset: number = 36
const processedEffects : Set<AIEffect> = new Set<AIEffect>( [
    AIEffect.InteractTarget,
    AIEffect.MoveFinished
] )

class InteractTrait extends BaseMoveAction implements IAITrait {
    switchMap: Record<number, AIIntent> = {
        [ AIEffect.MoveToLocation ]: AIIntent.WAITING,
        [ AIEffect.ForceAttackTarget ]: AIIntent.ATTACK,
        [ AIEffect.ForceSpellCast ]: AIIntent.CAST,
    }

    describeState( objectId: number, playerId: number ): void {
        let summon = L2World.getObjectById( objectId ) as L2Summon
        return this.describeMovementAction( summon, playerId )
    }

    getName(): string {
        return 'SummonInteractTrait'
    }

    onEnd( objectId: number ): void {
        this.removeMovementState( objectId )

        let summon = L2World.getObjectById( objectId ) as L2Summon
        summon.abortMoving()
    }

    async onStart( objectId: number ): Promise<void> {
        let parameters = EffectsCache.getParameters( objectId, AIEffect.InteractTarget ) as AIInteractTargetEvent
        let summon = L2World.getObjectById( objectId ) as L2Summon
        if ( parameters ) {
            summon.setTargetId( parameters.targetId )
        }

        if ( await this.isValidInteraction( summon ) ) {
            return
        }


        AIEffectHelper.scheduleNextIntent( summon, this.getSummonNextIntent( summon ) )
    }

    processEffect( objectId: number, effect: AIEffect, parameters: AIEventData ): Promise<void> {
        switch ( effect ) {
            case AIEffect.InteractTarget:
                return this.prepareInteraction( objectId, parameters as AIInteractTargetEvent )

            case AIEffect.MoveFinished:
                return this.prepareInteraction( objectId )
        }
    }

    async prepareInteraction( objectId: number, data: AIInteractTargetEvent = null ): Promise<void> {
        let summon = L2World.getObjectById( objectId ) as L2Summon
        if ( data ) {
            summon.setTargetId( data.targetId )
        }

        if ( await this.isValidInteraction( summon ) ) {
            return
        }

        return AIEffectHelper.scheduleNextIntent( summon, this.getSummonNextIntent( summon ) )
    }

    async isValidInteraction( summon: L2Summon ): Promise<boolean> {
        let target = summon.getTarget()
        if ( !target || !target.isItem() || summon.isAlikeDead() || !target.isVisible() || summon.isMovementDisabled() ) {
            return false
        }

        let distance : number = summon.calculateDistance( target, true )

        if ( distance > 2000 ) {
            summon.sendOwnedData( ActionFailed() )
            return false
        }

        if ( distance > interactOffset ) {
            this.moveToLocation( summon, target, interactOffset )
            return true
        }

        await summon.doPickupItem( target as L2ItemInstance )
        return false
    }

    getSummonNextIntent( summon: L2Summon ): AIIntent {
        return summon.isFollowingOwner() ? AIIntent.FOLLOW : AIIntent.WAITING
    }

    canProcessEffect( effect: AIEffect ): boolean {
        return processedEffects.has( effect )
    }

    canSwitchIntent( effect: AIEffect ): boolean {
        return this.switchMap[ effect ] !== undefined
    }

    switchIntent( effect: AIEffect ): AIIntent {
        return this.switchMap[ effect ]
    }
}

export const SummonInteractTrait = new InteractTrait()