import { L2Character } from '../../../models/actor/L2Character'
import { PacketDispatcher } from '../../../PacketDispatcher'
import { MoveToLocation, MoveToLocationWithCharacter } from '../../../packets/send/MoveToLocation'
import { ILocational } from '../../../models/Location'
import { ActionFailed } from '../../../packets/send/ActionFailed'
import { BroadcastHelper } from '../../../helpers/BroadcastHelper'
import { MoveToTargetCharacter } from '../../../packets/send/moveToTargetCharacter'
import { L2Object } from '../../../models/L2Object'
import { ObjectPool } from '../../../helpers/ObjectPoolHelper'
import Timeout = NodeJS.Timeout
import { L2Region } from '../../../enums/L2Region'
import { L2World } from '../../../L2World'
import { AIEffectHelper } from '../../helpers/AIEffectHelper'

const states: { [ objectId: number ]: MoveParameters } = {}

export interface MoveParameters {
    followTask: Timeout
    offset: number
    targetId: number
    targetStopTime: number
}

const MoveParameterCache = new ObjectPool( 'BaseMoveAction', (): MoveParameters => {
    return {
        followTask: undefined,
        offset: 0,
        targetId: 0,
        targetStopTime: 0
    }
} )

export const enum TargetRangeState {
    InRange,
    OutOfRange,
    TooFar
}

export type MoveTrackingMethod = ( objectId: number, parameters: MoveParameters ) => void

export class BaseMoveAction {
    describeMovementAction( character: L2Character, otherPlayerId: number ): void {
        if ( character.isMoving() && character.isVisibleFor( L2World.getPlayer( otherPlayerId ) ) ) {
            PacketDispatcher.sendOwnedData( otherPlayerId, MoveToLocationWithCharacter( character ) )
        }
    }

    addMovementState( objectId: number ) : void {
        if ( states[ objectId ] ) {
            return
        }

        states[ objectId ] = MoveParameterCache.getValue()
    }

    removeMovementState( objectId: number ) : void {
        let data = states[ objectId ]
        if ( !data ) {
            return
        }

        this.stopFollowTask( objectId )
        MoveParameterCache.recycleValue( data )
        delete states[ objectId ]
    }

    stopFollowTask( objectId: number ): void {
        let state = states[ objectId ]
        if ( !state ) {
            return
        }

        if ( state.followTask ) {
            clearInterval( state.followTask )
        }

        state.followTask = null
    }

    moveToLocation( character: L2Character, location: ILocational, offset: number ): void {
        if ( character.isMovementDisabled() ) {
            return this.sendActionFailed( character )
        }

        if ( offset > 0 && character.calculateDistance( location, true ) <= offset ) {
            return
        }

        if ( !character.moveToLocation( location.getX(), location.getY(), location.getZ(), offset ) || !character.isMoving() ) {
            return this.sendActionFailed( character )
        }

        return BroadcastHelper.dataToSelfBasedOnVisibility( character, MoveToLocationWithCharacter( character ) )
    }

    moveToCoordinates( character: L2Character, x: number, y: number, z: number, heading: number, instanceId: number, offset: number = character.getCollisionRadius() ): void {
        if ( !this.canMoveToCoordinates( character ) ) {
            return
        }

        if ( character.isAllSkillsDisabled()
                || character.isMovementDisabled()
                || character.isDead() ) {
            return this.sendActionFailed( character )
        }

        if ( character.getInstanceId() !== instanceId ) {
            character.teleportToLocationCoordinates( x, y, z, heading, instanceId, offset )
            return
        }

        if ( !character.moveToLocation( x, y, z, offset ) ) {
            return this.sendActionFailed( character )
        }

        if ( character.isPlayer() ) {
            character.sendOwnedData( MoveToLocation( character, x, y, z ) )
        }

        return BroadcastHelper.dataBasedOnVisibility( character, MoveToLocationWithCharacter( character ) )
    }

    canMoveToCoordinates( character: L2Character ) : boolean {
        return true
    }

    moveToTarget( character: L2Character, target: L2Object, offset: number, followMethod : MoveTrackingMethod = null, trackingInterval: number = 500 ): void {
        if ( character.isMovementDisabled() ) {
            return this.sendActionFailed( character )
        }

        if ( !character.moveToLocation( target.getX(), target.getY(), target.getZ(), offset ) ) {
            AIEffectHelper.notifyMoveFailed( character )

            return this.sendActionFailed( character )
        }

        let isContinuedMovement = this.hasMovementState( character.getObjectId() )
        let state = this.getMovementState( character.getObjectId() )

        state.offset = offset
        state.targetId = target.getObjectId()
        state.targetStopTime = target.isCharacter() ? ( target as L2Character ).movementStopTime : 0

        if ( !state.followTask && followMethod && target.isMoveCapable() ) {
            state.followTask = setInterval( followMethod, trackingInterval, character.getObjectId(), state )
        }

        if ( isContinuedMovement && !target.isMoveCapable() ) {
            return
        }

        if ( target.isCharacter() && !character.isOnGeodataPath() ) {
            return BroadcastHelper.dataToSelfBasedOnVisibility( character, MoveToTargetCharacter( character, target, offset ) )
        }

        return BroadcastHelper.dataToSelfBasedOnVisibility( character, MoveToLocationWithCharacter( character ) )
    }

    moveToTargetDestination( character: L2Character, target: L2Object, offset: number, followMethod : MoveTrackingMethod = null, trackingInterval: number = 500 ): void {
        if ( character.isMovementDisabled() ) {
            return this.sendActionFailed( character )
        }

        let targetData = target.getSharedData()
        if ( !character.moveToLocation( targetData.getDestinationX(), targetData.getDestinationY(), targetData.getDestinationZ(), offset ) ) {
            AIEffectHelper.notifyMoveFailed( character )

            return this.sendActionFailed( character )
        }

        let state = this.getMovementState( character.getObjectId() )

        state.offset = offset
        state.targetId = target.getObjectId()

        if ( !state.followTask && followMethod && target.isMoveCapable() ) {
            state.followTask = setInterval( followMethod, trackingInterval, character.getObjectId(), state )
        }

        if ( !character.isMoving() ) {
            return this.sendActionFailed( character )
        }

        if ( target.isCharacter() && !character.isOnGeodataPath() ) {
            return BroadcastHelper.dataToSelfBasedOnVisibility( character, MoveToTargetCharacter( character, target, offset ) )
        }

        return BroadcastHelper.dataToSelfBasedOnVisibility( character, MoveToLocationWithCharacter( character ) )
    }

    abortMovement( character: L2Character ): void {
        if ( character.isMoving() ) {
            this.stopFollowTask( character.getObjectId() )

            character.abortMoving()
        }
    }

    startMoveToTarget( character: L2Character, target: L2Object, range: number, followMethod : MoveTrackingMethod, followInterval: number = 500 ): void {
        if ( target.isMoveCapable() && target.isCharacter() && ( target as L2Character ).isMoving() ) {
            range -= 100
        }

        return this.moveToTarget( character, target, range, followMethod, followInterval )
    }

    startMoveToLocation( character: L2Character, location: ILocational, range: number ): Promise<void> {
        if ( character.isMovementDisabled() ) {
            return
        }

        this.moveToLocation( character, location, range )
    }

    getMovementState( objectId: number ): MoveParameters {
        if ( !states[ objectId ] ) {
            this.addMovementState( objectId )
        }

        return states[ objectId ]
    }

    hasMovementState( objectId: number ) : boolean {
        return !!states[ objectId ]
    }

    sendActionFailed( character: L2Character ) : void {
        if ( character.isPlayer() ) {
            character.sendOwnedData( ActionFailed() )
        }
    }

    hasTargetMoved( parameters: MoveParameters, target: L2Character ) : boolean {
        if ( !target || target.getObjectId() !== parameters.targetId ) {
            return false
        }

        if ( !target.isMoving() && parameters.targetStopTime === target.movementStopTime ) {
            return false
        }

        return true
    }

    getRangeState( origin: L2Character, destination: L2Character, attackRange: number ) : TargetRangeState {
        let distanceToTarget = origin.calculateDistance( destination, true )

        /*
            Particular case when target has teleported.
         */
        if ( distanceToTarget > L2Region.Length ) {
            return TargetRangeState.TooFar
        }

        let attackDistance = Math.max( distanceToTarget - destination.getTemplate().getCollisionRadius() - origin.getTemplate().getCollisionRadius(), 0 )
        if ( attackRange >= attackDistance ) {
            return TargetRangeState.InRange
        }

        return TargetRangeState.OutOfRange
    }

    getLocationState( origin: L2Character, destination: ILocational, range: number ) : TargetRangeState {
        let distance = origin.calculateDistance( destination, true )

        /*
            Particular case when target has teleported.
         */
        if ( distance > L2Region.Length ) {
            return TargetRangeState.TooFar
        }

        if ( range >= distance ) {
            return TargetRangeState.InRange
        }

        return TargetRangeState.OutOfRange
    }
}