import _ from 'lodash'
import { ConfigManager } from '../../../../config/ConfigManager'
import { L2NpcValues } from '../../../values/L2NpcValues'
import { L2Npc } from '../../../models/actor/L2Npc'
import { L2World } from '../../../L2World'
import { BroadcastHelper } from '../../../helpers/BroadcastHelper'
import { SocialAction } from '../../../packets/send/SocialAction'

export class SocialAnimationAction {
    getAnimationInterval(): number {
        return _.random( ConfigManager.general.getMinNPCAnimation(), ConfigManager.general.getMaxNPCAnimation() + L2NpcValues.staticAnimationInterval ) * 1000
    }

    performAnimation( objectId: number ): void {
        let character: L2Npc = L2World.getObjectById( objectId ) as L2Npc

        if ( !this.isAnimationDisabled( character ) ) {
            let id: number = _.random( 2, 3 )
            BroadcastHelper.dataInLocation( character, SocialAction( objectId, id ), 1000 )
        }
    }

    isAnimationDisabled( character: L2Npc ): boolean {
        return !character.hasRandomAnimation()
                || character.isDead()
                || character.isStunned()
                || character.isSleeping()
    }
}