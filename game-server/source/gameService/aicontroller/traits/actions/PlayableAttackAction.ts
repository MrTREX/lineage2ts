import { L2Playable } from '../../../models/actor/L2Playable'
import { L2Character } from '../../../models/actor/L2Character'
import { FightStanceCache } from '../../../cache/FightStanceCache'
import { EffectsCache } from '../../EffectCache'
import { AIAttackedEvent, AIEffect, AIForceAttackTargetEvent } from '../../enums/AIEffect'
import { BaseMoveAction, MoveParameters, TargetRangeState } from './BaseMoveAction'
import { L2World } from '../../../L2World'
import { AIEffectHelper } from '../../helpers/AIEffectHelper'
import { AIIntent } from '../../enums/AIIntent'
import { PacketDispatcher } from '../../../PacketDispatcher'
import { AutoAttackStart } from '../../../packets/send/AutoAttackStart'
import { ObjectPool } from '../../../helpers/ObjectPoolHelper'
import { AggroCache } from '../../../cache/AggroCache'

const attackStates: { [ objectId: number ]: AttackParameters } = {}

interface AttackParameters {
    startedAttack: boolean
    targetId: number
    attackerStopTime: number
    targetStopTime: number
    inRange: boolean
}

const ParameterCache = new ObjectPool( 'PlayableAttackAction', (): AttackParameters => {
    return {
        attackerStopTime: -1,
        targetStopTime: -1,
        startedAttack: false,
        targetId: 0,
        inRange: false
    }
} )

export class PlayableAttackAction extends BaseMoveAction {
    isAutoAttacking( objectId: number ): boolean {
        let state: AttackParameters = attackStates[ objectId ]
        return state && state.startedAttack
    }

    async performAttack( playable: L2Playable, target: L2Character ): Promise<void> {
        if ( this.haveNotMoved( playable, target ) ) {
            FightStanceCache.addPlayableStance( playable )
            return playable.doAttack( target )
        }

        this.updateStopTimes( playable, target )
        let state = this.getRangeState( playable, target, playable.getPhysicalAttackRange() )

        switch ( state ) {
            case TargetRangeState.TooFar:
                AggroCache.removeAggro( playable.getObjectId(), target.getObjectId() )
                return AIEffectHelper.notifyTargetDead( playable, target )

            case TargetRangeState.InRange:
                this.updateInRange( playable, true )
                FightStanceCache.addPlayableStance( playable )
                return playable.doAttack( target )

            case TargetRangeState.OutOfRange:
                let offset = target.getTemplate().getCollisionRadius() + playable.getPhysicalAttackRange() - ( target.isMoving() ? 0 : playable.getTemplate().getCollisionRadius() )
                return this.startMoveToTarget( playable, target, Math.max( offset, playable.getTemplate().getCollisionRadius() ), this.runFollowTask.bind( this ) )
        }
    }

    getStoredTargetId( objectId: number ): number {
        let forcedEvent = EffectsCache.getParameters( objectId, AIEffect.ForceAttackTarget ) as AIForceAttackTargetEvent
        if ( forcedEvent ) {
            return forcedEvent.targetId
        }

        let normalEvent = EffectsCache.getParameters( objectId, AIEffect.Attacked ) as AIAttackedEvent
        if ( normalEvent ) {
            return normalEvent.attackerId
        }
    }

    cleanUp( objectId: number ): void {
        this.removeMovementState( objectId )

        let data: AttackParameters = attackStates[ objectId ]
        if ( data ) {
            delete attackStates[ objectId ]

            ParameterCache.recycleValue( data )
        }
    }

    getAggroTargetId( playable: L2Playable ) : number {
        let possibleTarget = AggroCache.getMostHated( playable )
        if ( possibleTarget ) {
            return possibleTarget.getObjectId()
        }
    }

    canCreateAttackState( playable: L2Playable ): boolean {
        let targetId = this.getStoredTargetId( playable.getObjectId() )

        if ( !attackStates[ playable.getObjectId() ] ) {
            if ( !targetId ) {
                targetId = this.getAggroTargetId( playable )
            }

            if ( !targetId ) {
                return false
            }

            attackStates[ playable.getObjectId() ] = ParameterCache.getValue()
            attackStates[ playable.getObjectId() ].startedAttack = false
        }

        if ( targetId ) {
            this.setNextTarget( playable.getObjectId(), targetId )
        }

        return true
    }

    isValidTarget( character: L2Character ): boolean {
        return character && !character.isDead() && character.canBeAttacked()
    }

    runFollowTask( objectId: number, parameters: MoveParameters ): void {
        let playable = L2World.getObjectById( objectId ) as L2Playable
        if ( !playable ) {
            this.cleanUp( objectId )
            return
        }

        let followTarget = L2World.getObjectById( parameters.targetId ) as L2Character
        if ( !followTarget || !this.isValidTarget( followTarget ) ) {
            return this.stopAnyMovement( playable )
        }

        if ( !this.hasTargetMoved( parameters, followTarget ) ) {
            return
        }

        let state = this.getRangeState( playable, followTarget, playable.getPhysicalAttackRange() )

        if ( state === TargetRangeState.OutOfRange ) {
            return this.moveToTarget( playable, followTarget, parameters.offset )
        }

        return this.stopAnyMovement( playable )
    }

    stopAnyMovement( attacker: L2Playable ): void {
        this.abortMovement( attacker )
        AIEffectHelper.notifyMoveFinished( attacker )
    }

    startAttackAction( objectId: number ): Promise<void> {
        let playable = L2World.getObjectById( objectId ) as L2Playable

        if ( this.cannotAttack( playable ) || !this.canCreateAttackState( playable ) ) {
            AIEffectHelper.scheduleNextIntent( playable, AIIntent.WAITING )
            return
        }

        if ( !this.isAutoAttacking( objectId ) ) {
            this.startAutoAttack( objectId )
            playable.setIsRunning( true, true )
        }

        let target: L2Character = L2World.getObjectById( attackStates[ objectId ].targetId ) as L2Character
        if ( !this.isValidTarget( target ) || target.getObjectId() === objectId ) {

            // TODO : consider if you have more targets to attack
            AIEffectHelper.scheduleNextIntent( playable, AIIntent.WAITING )
            return
        }

        return this.performAttack( playable, target )
    }

    cannotAttack( playable: L2Playable ): boolean {
        return playable.isCastingNow() && playable.isAttackingDisabled()
    }

    startAutoAttack( objectId: number ): void {
        attackStates[ objectId ].startedAttack = true
    }

    setNextTarget( objectId: number, targetId: number ): void {
        const state = attackStates[ objectId ]

        state.targetId = targetId
        state.attackerStopTime = -1
        state.targetStopTime = -1
    }

    describeAttackAction( objectId: number, playerId: number ): void {
        let playable = L2World.getObjectById( objectId ) as L2Playable

        this.describeMovementAction( playable, playerId )
        if ( FightStanceCache.hasPlayableStance( objectId ) ) {
            PacketDispatcher.sendOwnedData( playerId, AutoAttackStart( objectId ) )
        }
    }

    stopAttackAction( objectId: number ) : void {
        let playable = L2World.getObjectById( objectId ) as L2Playable

        playable.stopAttackTimers()
        playable.abortCast( false )

        this.cleanUp( objectId )
        return this.abortMovement( playable )
    }

    haveNotMoved( attacker: L2Character, target: L2Character ) : boolean {
        let data = attackStates[ attacker.getObjectId() ]

        return data.attackerStopTime === attacker.movementStopTime
                && data.targetStopTime === target.movementStopTime
    }

    updateStopTimes( attacker: L2Character, target: L2Character ) : void {
        let data = attackStates[ attacker.getObjectId() ]

        data.attackerStopTime = attacker.movementStopTime
        data.targetStopTime = target.movementStopTime
        data.inRange = false
    }

    updateInRange( attacker: L2Character, value: boolean ) : void {
        let data = attackStates[ attacker.getObjectId() ]

        data.inRange = value
    }
}