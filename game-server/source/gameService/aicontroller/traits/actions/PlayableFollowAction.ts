import { BaseMoveAction, MoveParameters } from './BaseMoveAction'
import { L2World } from '../../../L2World'
import { L2Character } from '../../../models/actor/L2Character'
import { AIFollowObjectEvent } from '../../enums/AIEffect'
import { L2Playable } from '../../../models/actor/L2Playable'
import { L2Object } from '../../../models/L2Object'
import { AIEffectHelper } from '../../helpers/AIEffectHelper'
import { AIIntent } from '../../enums/AIIntent'

const defaultFollowRange = 50
const defaultStopRange = defaultFollowRange - 20

export class PlayableFollowAction extends BaseMoveAction {
    runTargetTracking( objectId: number, parameters: MoveParameters ): void {
        let playable = L2World.getObjectById( objectId ) as L2Playable
        let target = L2World.getObjectById( parameters.targetId ) as L2Character

        if ( !target ) {
            this.stopFollowTask( objectId )
            return
        }

        if ( target && !playable.isInsideRadius( target, this.getDefaultFollowRange() ) ) {
            if ( !this.hasTargetMoved( parameters, target ) ) {
                return
            }

            if ( target.isMoving() ) {
                this.moveToTargetDestination( playable, target, parameters.offset )
            } else {
                this.moveToTarget( playable, target, parameters.offset )
            }

            return
        }

        if ( playable.isInsideRadius( target, this.getDefaultStopRange() ) ) {
            return this.abortMovement( playable )
        }
    }

    startFollowing( playable: L2Playable, data: AIFollowObjectEvent ): void {
        let target: L2Object = this.getFollowTarget( playable, data )
        if ( !target ) {
            AIEffectHelper.scheduleNextIntent( playable, this.getNextIntent( playable ) )
            return
        }

        let distance: number = this.getDistance( data )

        if ( target.isCharacter() && ( target as L2Character ).isMoving() ) {
            return this.moveToTargetDestination( playable, target, distance, this.runTargetTracking.bind( this ), this.getTrackingInterval() )
        }

        return this.startMoveToTarget( playable, target, distance, this.runTargetTracking.bind( this ), this.getTrackingInterval() )
    }

    stopFollowing( playable: L2Playable ): void {
        this.removeMovementState( playable.getObjectId() )

        playable.abortMoving()
    }

    getDistance( data: AIFollowObjectEvent ): number {
        if ( data && data.distance ) {
            return data.distance
        }

        return this.getDefaultFollowRange()
    }

    getFollowTarget( playable: L2Playable, data: AIFollowObjectEvent ): L2Object {
        if ( data?.targetId ) {
            return L2World.getObjectById( data.targetId )
        }

        return playable.getTarget()
    }

    getDefaultFollowRange(): number {
        return defaultFollowRange
    }

    getDefaultStopRange(): number {
        return defaultStopRange
    }

    getTrackingInterval(): number {
        return 1500
    }

    getNextIntent( playable: L2Playable ) : AIIntent {
        return AIIntent.WAITING
    }
}