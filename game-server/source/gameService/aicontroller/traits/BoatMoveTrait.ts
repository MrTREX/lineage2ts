import { IAITrait } from '../interface/IAITrait'
import { L2World } from '../../L2World'
import { PacketDispatcher } from '../../PacketDispatcher'
import { VehicleDeparture } from '../../packets/send/VehicleDeparture'
import { AIEffect, AIEventData, AIMoveToLocationEvent } from '../enums/AIEffect'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { VehicleStarted, VehicleStartedState } from '../../packets/send/VehicleStarted'
import { VehicleInfo } from '../../packets/send/VehicleInfo'
import { L2BoatInstance } from '../../models/actor/instance/L2BoatInstance'
import { AIIntent } from '../enums/AIIntent'

const processedEffects : Set<AIEffect> = new Set<AIEffect>( [ AIEffect.MoveToLocation, AIEffect.MoveFinished ] )

class BoatMoveBaseTrait implements IAITrait {
    canProcessEffect( effect: AIEffect ): boolean {
        return processedEffects.has( effect )
    }

    getName(): string {
        return 'BoatMoveTrait'
    }

    describeState( objectId: number, playerId: number ): void {
        let boat = L2World.getObjectById( objectId ) as L2BoatInstance

        if ( boat.isMoving() ) {
            PacketDispatcher.sendOwnedData( playerId, VehicleDeparture( boat ) )
        }
    }

    onEnd(): void {
        return
    }

    onStart(): Promise<void> {
        return
    }

    processEffect( objectId: number, effect: AIEffect, parameters: AIEventData ): Promise<void> {
        switch ( effect ) {
            case AIEffect.MoveToLocation:
                this.moveToLocation( objectId, ( parameters as AIMoveToLocationEvent ) )
                return

            case AIEffect.MoveFinished:
                return this.onStopMoving( objectId )
        }
    }

    moveToLocation( objectId: number, location: AIMoveToLocationEvent ): void {
        let boat = L2World.getObjectById( objectId ) as L2BoatInstance
        if ( boat.isMovementDisabled() ) {
            return
        }

        if ( !boat.isMoving() ) {
            BroadcastHelper.dataInLocation( boat, VehicleStarted( objectId, VehicleStartedState.Stop ), boat.getBroadcastRadius() )
        }

        if ( !boat.moveToLocation( location.x, location.y, location.z, 0 ) ) {
            return
        }

        BroadcastHelper.dataInLocation( boat, VehicleDeparture( boat ), boat.getBroadcastRadius() )
    }

    async onStopMoving( objectId: number ): Promise<void> {
        let boat = L2World.getObjectById( objectId ) as L2BoatInstance

        BroadcastHelper.dataInLocation( boat, VehicleStarted( objectId, VehicleStartedState.Start ), boat.getBroadcastRadius() )
        BroadcastHelper.dataInLocation( boat, VehicleInfo( boat ), boat.getBroadcastRadius() )

        // TODO : move logic should stay inside trait or boat instance
        // if ( await BoatManager.moveBoat( objectId ) ) {
        //     BroadcastHelper.dataInLocation( boat, VehicleDeparture( boat ), boat.getBroadcastRadius() )
        // }
    }

    canSwitchIntent(): boolean {
        return false
    }

    switchIntent(): AIIntent {
        return AIIntent.WAITING
    }
}

export const BoatMoveTrait = new BoatMoveBaseTrait()