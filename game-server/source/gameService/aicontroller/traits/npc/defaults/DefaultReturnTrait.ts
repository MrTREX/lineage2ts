import { IAITrait } from '../../../interface/IAITrait'
import { AIIntent } from '../../../enums/AIIntent'
import { AIEffect, AIEventData } from '../../../enums/AIEffect'
import { L2Npc } from '../../../../models/actor/L2Npc'
import { L2World } from '../../../../L2World'
import { PacketDispatcher } from '../../../../PacketDispatcher'
import { MoveToLocationWithCharacter } from '../../../../packets/send/MoveToLocation'
import { BaseMoveAction, TargetRangeState } from '../../actions/BaseMoveAction'
import { ConfigManager } from '../../../../../config/ConfigManager'
import { AIEffectHelper } from '../../../helpers/AIEffectHelper'
import { FightStanceCache } from '../../../../cache/FightStanceCache'

class DefaultReturnBaseTrait extends BaseMoveAction implements IAITrait {
    switchMap: Record<number, AIIntent> = {
        [ AIEffect.Attacked ]: AIIntent.ATTACK,
        [ AIEffect.MoveFinished ]: AIIntent.WAITING
    }

    getName(): string {
        return 'DefaultReturnTrait'
    }

    describeState( objectId: number, playerId: number ): void {
        let npc: L2Npc = L2World.getObjectById( objectId ) as L2Npc

        if ( npc.isMoving() && npc.isVisibleFor( L2World.getPlayer( playerId ) ) ) {
            PacketDispatcher.sendOwnedData( playerId, MoveToLocationWithCharacter( npc ) )
        }
    }

    onEnd( objectId: number ): void {
        let npc = L2World.getObjectById( objectId ) as L2Npc
        npc.abortMoving()
    }

    onStart( objectId: number ): Promise<void> {
        let npc = L2World.getObjectById( objectId ) as L2Npc
        FightStanceCache.removePermanentStance( npc )

        return this.startMoving( npc )
    }

    processEffect( objectId: number, effect: AIEffect, parameters: AIEventData ): Promise<void> {
        return Promise.resolve()
    }

    canProcessEffect( effect: AIEffect ): boolean {
        return false
    }

    startMoving( npc: L2Npc ): Promise<void> {
        if ( !npc || !npc.isAttackable() || !npc.isMoveCapable() ) {
            AIEffectHelper.scheduleNextIntent( npc, AIIntent.WAITING )
            return
        }

        let destination = npc.getSpawnLocation()
        let state = this.getLocationState( npc, destination, ConfigManager.npc.getMaxDriftRange() )

        switch ( state ) {
            case TargetRangeState.TooFar:
                return npc.teleportToLocationWithOffset( destination, ConfigManager.npc.getMaxDriftRange() * Math.random() )

            case TargetRangeState.InRange:
                AIEffectHelper.scheduleNextIntent( npc, AIIntent.WAITING )
                return

            case TargetRangeState.OutOfRange:
                return this.startMoveToLocation( npc, destination, ConfigManager.npc.getMaxDriftRange() * Math.random() )
        }
    }

    canSwitchIntent( effect: AIEffect ): boolean {
        return this.switchMap[ effect ] !== undefined
    }

    switchIntent( effect: AIEffect ): AIIntent {
        return this.switchMap[ effect ]
    }
}

export const DefaultReturnTrait = new DefaultReturnBaseTrait()