import { L2Attackable } from '../../../../models/actor/L2Attackable'
import { L2World } from '../../../../L2World'
import { EffectsCache } from '../../../EffectCache'
import { AIEffect, AIForceSpellCastEvent } from '../../../enums/AIEffect'
import { AIEffectHelper } from '../../../helpers/AIEffectHelper'
import { AIIntent } from '../../../enums/AIIntent'
import { L2Character } from '../../../../models/actor/L2Character'
import { BaseAttackTrait } from './DefaultAttackTrait'
import { SkillCache } from '../../../../cache/SkillCache'
import { Skill } from '../../../../models/Skill'
import { MoveParameters, TargetRangeState } from '../../actions/BaseMoveAction'

export class BaseCastTrait extends BaseAttackTrait {
    name: string = 'DefaultCastTrait'

    async onStart( objectId: number ): Promise<void> {
        let npc: L2Attackable = L2World.getObjectById( objectId ) as L2Attackable
        let parameters = EffectsCache.getParameters( npc.getObjectId(), AIEffect.ForceSpellCast ) as AIForceSpellCastEvent

        if ( !parameters || this.cannotAttack( npc ) ) {
            return this.abortCast( npc )
        }

        let target: L2Character = L2World.getObjectById( parameters.targetId ) as L2Character
        if ( target !== npc && !this.isValidTarget( target ) ) {
            return this.abortCast( npc )
        }

        let skill = SkillCache.getSkill( parameters.skillId, parameters.skillLevel )
        if ( !skill || !this.canCastSkill( npc, skill ) ) {
           return this.abortCast( npc )
        }

        if ( target === npc ) {
            return this.performCast( npc, target, skill )
        }

        this.createAttackState( objectId )

        let range = this.getSkillRange( skill )
        let state = this.getRangeState( npc, target, range )

        switch ( state ) {
            case TargetRangeState.TooFar:
                return this.abortCast( npc )

            case TargetRangeState.OutOfRange:
                if ( npc.isMovementDisabled() ) {
                    return this.abortCast( npc )
                }

                EffectsCache.setParameters( npc.getObjectId(), AIEffect.ForceSpellCast, parameters )

                let offset = target.getTemplate().getCollisionRadius() + range - ( target.isMoving() ? 0 : npc.getTemplate().getCollisionRadius() )

                return this.startMoveToTarget( npc, target, offset, this.onFollow.bind( this, range ) )

            case TargetRangeState.InRange:
                return this.performCast( npc, target, skill )
        }
    }

    performCast( npc: L2Attackable, target: L2Character, skill: Skill ): Promise<void> {
        npc.abortMoving()
        npc.setTarget( target )

        return npc.doCast( skill )
    }

    abortCast( npc: L2Character ) : void {
        return AIEffectHelper.scheduleNextIntent( npc, AIIntent.ATTACK )
    }

    onEnd( objectId: number ): void {
        let npc: L2Character = L2World.getObjectById( objectId ) as L2Character

        npc.abortCast( false )

        return this.cleanUpState( objectId )
    }

    onFollow( skillRange: number, objectId: number, parameters: MoveParameters ): void {
        let attacker: L2Attackable = L2World.getObjectById( objectId ) as L2Attackable
        let followTarget: L2Character = attacker.getTarget() as L2Character

        if ( !followTarget || !this.isValidTarget( followTarget ) ) {
            return this.abortCast( attacker )
        }

        if ( !this.hasTargetMoved( parameters, followTarget ) ) {
            return
        }

        let state = this.getRangeState( attacker, followTarget, skillRange )

        switch ( state ) {
            case TargetRangeState.TooFar:
                return this.abortCast( attacker )

            case TargetRangeState.InRange:
                this.abortMovement( attacker )
                return AIEffectHelper.notifyMoveFinished( attacker )

            case TargetRangeState.OutOfRange:
                return this.moveToTarget( attacker, followTarget, parameters.offset )
        }

        return this.abortCast( attacker )
    }
}

export const DefaultCastTrait = new BaseCastTrait()