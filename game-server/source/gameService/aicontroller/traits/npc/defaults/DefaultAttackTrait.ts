import { ObjectPool } from '../../../../helpers/ObjectPoolHelper'
import { AIEffect, AIEventData, AIMoveToLocationEvent } from '../../../enums/AIEffect'
import { L2Attackable } from '../../../../models/actor/L2Attackable'
import { L2World } from '../../../../L2World'
import { AIIntent } from '../../../enums/AIIntent'
import { IAITrait } from '../../../interface/IAITrait'
import { L2Character } from '../../../../models/actor/L2Character'
import { EffectsCache } from '../../../EffectCache'
import { AIEffectHelper } from '../../../helpers/AIEffectHelper'
import { FightStanceCache } from '../../../../cache/FightStanceCache'
import { AggroCache } from '../../../../cache/AggroCache'
import { BaseMoveAction, MoveParameters, TargetRangeState } from '../../actions/BaseMoveAction'
import { Skill } from '../../../../models/Skill'
import { PacketDispatcher } from '../../../../PacketDispatcher'
import { AutoAttackStart } from '../../../../packets/send/AutoAttackStart'

const attackStates: { [ objectId: number ]: AttackTraitParameters } = {}

interface AttackTraitParameters {
    startedAttack: boolean
    attackTimeout: number
    attackerStopTime: number
    targetStopTime: number
    inRange: boolean
}

const traitParametersCache = new ObjectPool( 'BaseAttackTrait Parameters', (): AttackTraitParameters => {
    return {
        attackTimeout: 0,
        startedAttack: false,
        attackerStopTime: -1,
        targetStopTime: -1,
        inRange: false
    }
} )

const processedEffects : Set<AIEffect> = new Set<AIEffect>( [
    AIEffect.MoveFinished,
    AIEffect.Attacked,
    AIEffect.TargetDead,
    AIEffect.MoveToLocation,
    AIEffect.ReadyToAttack,
    AIEffect.SkillCastFinished,
    AIEffect.SkillCastCanceled
] )

const enum BaseAttackParameters {
    AttackTimeoutPeriod = 2 * 60000,
    MaximumAttackRange = 850,
    FollowInterval = 500
}

/*
    Entering attack trait we must assume:
    - there is actual attacker
    - attacker can be attacked
    - if above is true, we engage fighting stance and proceed chasing target

    Exiting attack trait:
    - next intent must be of return type, to force AI to return close to spawn location
    - no further attacking is needed

    Fighting stance should always be cleared when character returns to either waiting or returning to spawn mode.
    During course of attack we can cast spells, or flee away, such modes should not reset fighting stance.
 */
export class BaseAttackTrait extends BaseMoveAction implements IAITrait {

    name: string = 'DefaultNpcAttackTrait'

    switchMap: Record<number, AIIntent> = {
        [ AIEffect.ForceSpellCast ]: AIIntent.CAST
    }

    getName(): string {
        return this.name
    }

    describeState( objectId: number, playerId: number ): void {
        let npc: L2Attackable = L2World.getObjectById( objectId ) as L2Attackable
        this.describeMovementAction( npc, playerId )

        if ( FightStanceCache.hasPermanentStance( objectId ) ) {
            PacketDispatcher.sendOwnedData( playerId, AutoAttackStart( objectId ) )
        }
    }

    onEnd( objectId: number ): void {
        let npc: L2Character = L2World.getObjectById( objectId ) as L2Character

        npc.setIsRunning( false )
        npc.abortMoving()
        npc.stopAttackTimers()

        return this.cleanUpState( objectId )
    }

    async onStart( objectId: number ): Promise<void> {
        let npc: L2Attackable = L2World.getObjectById( objectId ) as L2Attackable

        if ( this.cannotAttack( npc ) ) {
            this.stopAutoAttack( npc )
            return AIEffectHelper.scheduleNextIntent( npc, AIIntent.RETURN )
        }

        if ( EffectsCache.hasParameters( npc.getObjectId(), AIEffect.ForceSpellCast ) ) {
            return AIEffectHelper.scheduleNextIntent( npc, AIIntent.CAST )
        }

        this.createAttackState( objectId )
        npc.setIsRunning( true, !this.isAutoAttacking( objectId ) )

        let target: L2Character = npc.getTarget() as L2Character || npc.getMostHated()
        if ( !this.isValidTarget( target ) || ( Date.now() - attackStates[ npc.getObjectId() ].attackTimeout ) > BaseAttackParameters.AttackTimeoutPeriod ) {
            npc.setWalking()
            return this.onTargetNotValid( npc, target )
        }

        this.startAutoAttack( npc )
        return this.performAttack( npc, target )
    }

    async processEffect( objectId: number, effect: AIEffect, parameters: AIEventData ): Promise<void> {
        switch ( effect ) {
            case AIEffect.MoveFinished:
                /*
                    Move close to target is finished, now time to attack!
                 */
                if ( !attackStates[ objectId ] ) {
                    break
                }

                this.stopFollowTask( objectId )
                return this.onStart( objectId )

            case AIEffect.Attacked:
                if ( !this.isAutoAttacking( objectId ) ) {
                    await this.onStart( objectId )
                    this.updateAttackTimeout( objectId )
                }

                return

            case AIEffect.TargetDead:
                if ( !AggroCache.hasAggroTarget( objectId ) ) {
                    return AIEffectHelper.scheduleNextIntent( L2World.getObjectById( objectId ) as L2Attackable, AIIntent.RETURN )
                }

                return this.onStart( objectId )


            case AIEffect.MoveToLocation:
                /*
                    Direct control(destiny or God) wants us to move somewhere, we either refuse or move, we can also make this action delayed
                 */
                return this.moveToLocationWithEvent( objectId, parameters as AIMoveToLocationEvent )

            case AIEffect.ReadyToAttack:
                /*
                    Yey! We get to attack one more time! Let's do it more painfully this time!
                 */
                return this.onStart( objectId )

            case AIEffect.SkillCastFinished:
            case AIEffect.SkillCastCanceled:
                /*
                    We just finished casting skill. Either attack again, or move somewhere away. Another choice is to run away!
                 */
                return this.onStart( objectId )
        }
    }

    cleanUpState( objectId: number ): void {
        let data: AttackTraitParameters = attackStates[ objectId ]
        if ( data ) {
            this.stopFollowTask( objectId )
            delete attackStates[ objectId ]

            traitParametersCache.recycleValue( data )
        }
    }

    startAutoAttack( character: L2Character ): void {
        if ( !FightStanceCache.hasPermanentStance( character.getObjectId() ) ) {
            FightStanceCache.addPermanentStance( character )
        }

        if ( this.isAutoAttacking( character.getObjectId() ) ) {
            return
        }

        this.setAutoAttacking( character.getObjectId() )
    }

    stopAutoAttack( character: L2Character ): void {
        if ( !this.isAutoAttacking( character.getObjectId() ) ) {
            return
        }

        this.cleanUpState( character.getObjectId() )
    }

    isAutoAttacking( objectId: number ): boolean {
        let state: AttackTraitParameters = attackStates[ objectId ]
        return state && state.startedAttack
    }

    setAutoAttacking( objectId: number ): void {
        attackStates[ objectId ].startedAttack = true
    }

    createAttackState( objectId: number ): void {
        if ( !attackStates[ objectId ] ) {
            attackStates[ objectId ] = traitParametersCache.getValue()
            attackStates[ objectId ].attackTimeout = Date.now() + BaseAttackParameters.AttackTimeoutPeriod
            attackStates[ objectId ].startedAttack = false
        }
    }

    cannotAttack( character: L2Character ): boolean {
        return character.isAllSkillsDisabled()
                || character.isCastingNow()
                || character.isAfraid()
                || character.isOutOfControl()
                || character.isAlikeDead()
    }

    isValidTarget( character: L2Character ): boolean {
        return character && !character.isDead() && character.canBeAttacked()
    }

    async performAttack( npc: L2Attackable, target: L2Character ): Promise<void> {
        if ( this.haveNotMoved( npc, target ) ) {
            if ( this.shouldCastSkill( npc, target, true ) || this.shouldFlee( npc, target, true ) ) {
                return
            }

            npc.doAttack( target )
            return
        }

        this.updateStopTimes( npc, target )
        npc.setTargetId( target.getObjectId() )

        /*
            Npc archers can have very large range, hence we must have maximum distance such npcs are allowed to stay away to
            enable player see who is attacking, and have a chance of attacking back.
         */
        let attackRange = Math.min( BaseAttackParameters.MaximumAttackRange, npc.getPhysicalAttackRange() )
        let state = this.getRangeState( npc, target, attackRange )

        /*
            TODO : add specific rules for casting
            - introduce AIController preference flags, such flags will allow to choose which traits are preferred, similar to AIType
              except introduce variations to account for slow speed, low hp (calculate using level), low mp
              For example:
              - mob with high mp pool can cast spells more often
              - mob with slow speed casts spells more often
              - mob with fast speed casts buffs more often
              - mob with high hp prefers melee attack, unless it is mage (which should not happen as such arrangement is counter intuitive)
            - raid boss mobs should also follow same mechanics, however more research is needed about better AI behaviors
         */

        switch ( state ) {
            case TargetRangeState.TooFar:
                AggroCache.removeAggro( npc.getObjectId(), target.getObjectId() )
                return AIEffectHelper.notifyTargetDead( npc, target )

            case TargetRangeState.InRange:
                if ( this.shouldCastSkill( npc, target, true ) || this.shouldFlee( npc, target, true ) ) {
                    return
                }

                this.updateInRange( npc, true )
                npc.doAttack( target )
                return

            case TargetRangeState.OutOfRange:
                if ( this.shouldCastSkill( npc, target, false ) || this.shouldFlee( npc, target, false ) ) {
                    return
                }

                let offset = target.getTemplate().getCollisionRadius() + attackRange - ( target.isMoving() ? 0 : npc.getTemplate().getCollisionRadius() )
                this.startMoveToTarget( npc, target, offset, this.runFollowTask.bind( this ), BaseAttackParameters.FollowInterval )
                return
        }
    }

    shouldCastSkill( npc: L2Attackable, target: L2Character, isInMeleeRange: boolean ) : boolean {
        return false
    }

    shouldFlee( npc: L2Attackable, target: L2Character, isInMeleeRange: boolean ) : boolean {
        return false
    }

    runFollowTask( objectId: number, parameters: MoveParameters ): void {
        let attacker: L2Attackable = L2World.getObjectById( objectId ) as L2Attackable
        let followTarget: L2Character = attacker.getTarget() as L2Character
        if ( !followTarget || !this.isValidTarget( followTarget ) ) {
            this.onTargetNotValid( attacker, followTarget )
            return this.abortMovement( attacker )
        }

        if ( !this.hasTargetMoved( parameters, followTarget ) ) {
            return
        }

        /*
            TODO : consider adding check if skill can be cast
            - since follow task is executed very frequently, ensure
              very simple counter progression toward skill cast possibility
            - besides casting, fleeing can be applied based on HP level and propensity to run away (toward spawn point? mage vs ranger for example)
         */
        let state = this.getRangeState( attacker, followTarget, attacker.getPhysicalAttackRange() )

        switch ( state ) {
            case TargetRangeState.TooFar:
                this.onTargetNotValid( attacker, followTarget )
                return this.abortMovement( attacker )

            case TargetRangeState.InRange:
                this.abortMovement( attacker )
                return AIEffectHelper.notifyMoveFinished( attacker )

            case TargetRangeState.OutOfRange:
                return this.moveToTarget( attacker, followTarget, parameters.offset )
        }
    }

    onTargetNotValid( attacker: L2Attackable, target: L2Character ): void {
        attacker.stopHating( target )
        AIEffectHelper.notifyTargetDead( attacker, target )
    }

    updateAttackTimeout( objectId: number ): void {
        attackStates[ objectId ].attackTimeout = Date.now() + BaseAttackParameters.AttackTimeoutPeriod
    }

    async moveToLocationWithEvent( objectId: number, parameters: AIMoveToLocationEvent ): Promise<void> {
        let npc = L2World.getObjectById( objectId ) as L2Attackable
        if ( npc.isMovementDisabled() ) {
            return
        }

        if ( npc.getInstanceId() !== parameters.instanceId ) {
            return npc.teleportToLocationCoordinates( parameters.x, parameters.y, parameters.z, parameters.heading, parameters.instanceId, 0 )
        }

        return this.moveToCoordinates( npc, parameters.x, parameters.y, parameters.z, npc.getHeading(), npc.getInstanceId(), 0 )
    }

    canSwitchIntent( effect: AIEffect ): boolean {
        return this.switchMap[ effect ] !== undefined
    }

    switchIntent( effect: AIEffect ): AIIntent {
        return this.switchMap[ effect ]
    }

    canProcessEffect( effect: AIEffect ): boolean {
        return processedEffects.has( effect )
    }

    haveNotMoved( attacker: L2Character, target: L2Character ) : boolean {
        let data = attackStates[ attacker.getObjectId() ]

        return data.attackerStopTime === attacker.movementStopTime
                && data.targetStopTime === target.movementStopTime
                && data.inRange
    }

    updateStopTimes( attacker: L2Character, target: L2Character ) : void {
        let data = attackStates[ attacker.getObjectId() ]

        data.attackerStopTime = attacker.movementStopTime
        data.targetStopTime = target.movementStopTime
        data.inRange = false
    }

    updateInRange( attacker: L2Character, value: boolean ) : void {
        let data = attackStates[ attacker.getObjectId() ]

        data.inRange = value
    }

    canCastSkill( caster: L2Attackable, skill: Skill ): boolean {
        if ( caster.isCastingNow() && !skill.isSimultaneousCast() ) {
            return false
        }

        if ( caster.isSkillDisabled( skill ) ) {
            return false
        }

        /*
            Hp consumption is happening on second stage of casting a skill.
            Mp consumption is IGNORED since it should only apply to player controlled characters/cubics
            to make gameplay interesting (why care if mob has low MP to cast a skill, challenge player first).
         */
        if ( skill.getHpConsume() > caster.getCurrentHp() ) {
            return false
        }

        if ( ( skill.isMagic() && caster.isMuted() ) ) {
            return false
        }

        if ( skill.isPhysical() && caster.isPhysicalMuted() ) {
            return false
        }

        return true
    }

    getSkillRange( skill: Skill ) : number {
        return Math.min( 500, skill.getCastRange() )
    }
}

export const DefaultAttackTrait = new BaseAttackTrait()