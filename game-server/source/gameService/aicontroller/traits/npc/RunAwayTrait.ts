import { DefaultFleeingTraitAction } from './defaults/DefaultFleeingTrait'
import { AIEffect } from '../../enums/AIEffect'
import { L2World } from '../../../L2World'
import { AIIntent } from '../../enums/AIIntent'
import { L2Npc } from '../../../models/actor/L2Npc'
import { IAITrait } from '../../interface/IAITrait'


/*
    We want to make character run away short distance when it is being attacked.
    One way to do that is to use default walking trait that will respond to being attacked (switching intent to attack).
    Then you can inject your own trait for attack action, as implemented below.
    We only allow character to run away when stationary and under attack.
 */

export class RunAwayTrait extends DefaultFleeingTraitAction implements IAITrait {

    getName(): string {
        return 'RunAwayTrait'
    }

    prepareMoveParameters() {}
    cleanUp() {}

    processEffect( objectId: number, effect: AIEffect ): Promise<void> {
        switch ( effect ) {
            case AIEffect.Attacked:
                let npc: L2Npc = L2World.getObjectById( objectId ) as L2Npc
                if ( !npc.isMoving() ) {
                    return this.onStart( objectId )
                }

                return
        }
    }

    canProcessEffect( effect: AIEffect ): boolean {
        return effect === AIEffect.Attacked
    }

    canSwitchIntent( effect: AIEffect ): boolean {
        return false
    }

    switchIntent(): AIIntent {
        return AIIntent.ATTACK
    }
}