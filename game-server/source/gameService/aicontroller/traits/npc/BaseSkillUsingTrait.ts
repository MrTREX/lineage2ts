import { BaseAttackTrait } from './defaults/DefaultAttackTrait'
import { L2Attackable } from '../../../models/actor/L2Attackable'
import { L2Character } from '../../../models/actor/L2Character'
import { AIEffectHelper } from '../../helpers/AIEffectHelper'
import { Skill } from '../../../models/Skill'

export class BaseSkillUsingTrait extends BaseAttackTrait {
    isAINotifiedToCastSkill( npc: L2Attackable, target: L2Character, skill: Skill ): boolean {
        if ( skill ) {
            let skillTarget = this.getSkillTarget( npc, target, skill )

            if ( this.skillCanBeCast( npc, skillTarget, skill ) ) {
                AIEffectHelper.notifyMustCastSpell( npc, skillTarget, skill.getId(), skill.getLevel(), false, false )
                return true
            }
        }

        return false
    }

    getSkillTarget( npc: L2Attackable, target: L2Character, skill: Skill ) : L2Character {
        if ( skill.isBad() ) {
            return target
        }

        return npc
    }

    skillCanBeCast( npc: L2Attackable, target: L2Character, skill: Skill ) : boolean {
        return this.canCastSkill( npc, skill )
                && !target.isAffectedBySkill( skill.getId() )
    }
}