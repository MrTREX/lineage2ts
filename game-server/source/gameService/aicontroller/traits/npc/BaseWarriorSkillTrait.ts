import { L2Attackable } from '../../../models/actor/L2Attackable'
import { L2Character } from '../../../models/actor/L2Character'
import { AISkillUsage, AISkillUsageType } from '../../../enums/AISkillUsage'
import { Skill } from '../../../models/Skill'
import _ from 'lodash'
import { BaseSkillUsingTrait } from './BaseSkillUsingTrait'

export abstract class BaseWarriorSkillTrait extends BaseSkillUsingTrait {

    shouldCastSkill( npc: L2Attackable, target: L2Character, isInMeleeRange: boolean ): boolean {
        if ( isInMeleeRange && target.getCurrentHp() < ( target.getMaxHp() * 0.2 ) ) {
            return false
        }

        return this.performCastCheck( npc, target, npc.getTemplate().aiSkillUsage, this.getNextUsageType( npc ) )
    }

    abstract getNextUsageType( npc: L2Attackable ) : AISkillUsageType | null

    performCastCheck( npc: L2Attackable, target: L2Character, parameters: AISkillUsage, type: AISkillUsageType ) : boolean {
        if ( type === null ) {
            return false
        }

        let skill = this.getUsageSkill( parameters, type )
        return this.isAINotifiedToCastSkill( npc, target, skill )
    }

    getUsageSkill( usage: AISkillUsage, type: AISkillUsageType ) : Skill {
        if ( usage.available[ type ].length > 0 && usage.probability[ type ] > Math.random() ) {
            return _.sample( usage.available[ type ] )
        }

        return null
    }
}