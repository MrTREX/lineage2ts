import _ from 'lodash'
import { ObjectPool } from '../../../helpers/ObjectPoolHelper'
import { IAITrait } from '../../interface/IAITrait'
import { L2Attackable } from '../../../models/actor/L2Attackable'
import { L2World } from '../../../L2World'
import { AIEffect } from '../../enums/AIEffect'
import { AIType } from '../../../enums/AIType'
import { ConfigManager } from '../../../../config/ConfigManager'
import { AIIntent } from '../../enums/AIIntent'
import { BaseMoveAction } from '../actions/BaseMoveAction'
import { PointGeometry } from '../../../models/drops/PointGeometry'
import { GeometryId } from '../../../enums/GeometryId'
import { BroadcastHelper } from '../../../helpers/BroadcastHelper'
import { MoveToLocationWithCharacter } from '../../../packets/send/MoveToLocation'
import { ILocational } from '../../../models/Location'
import { L2Npc } from '../../../models/actor/L2Npc'
import Timeout = NodeJS.Timeout

interface WalkingTraitParameters {
    retryTask: Timeout
    aggroScanTask: Timeout
    animationTask: Timeout
}

const traitParametersCache = new ObjectPool( 'RandomWalkTraitAction', (): WalkingTraitParameters => {
    return {
        aggroScanTask: undefined,
        retryTask: undefined,
        animationTask: undefined
    }
} )

const timerRegistry: { [ objectId: number ]: WalkingTraitParameters } = {}

const enum RandomWalkValues {
    WalkRate = 0.2,
    Interval = 15000,
    BaseMoveInterval = 5000,
    MinimumAnimationInterval = 5000,
}

export class RandomWalkTraitAction extends BaseMoveAction implements IAITrait {

    getName(): string {
        return 'RandomWalk'
    }

    cleanUp( objectId: number ): void {
        let data: WalkingTraitParameters = timerRegistry[ objectId ]
        if ( data ) {
            clearTimeout( data.retryTask )
            clearTimeout( data.animationTask )

            clearInterval( data.aggroScanTask )

            delete timerRegistry[ objectId ]

            traitParametersCache.recycleValue( data )
        }
    }

    createDefaults( objectId: number ): void {
        if ( !timerRegistry[ objectId ] ) {
            timerRegistry[ objectId ] = traitParametersCache.getValue()

            timerRegistry[ objectId ].animationTask = null
            timerRegistry[ objectId ].aggroScanTask = null
            timerRegistry[ objectId ].retryTask = null
        }
    }

    describeState( objectId: number, playerId: number ): void {
        let npc: L2Attackable = L2World.getObjectById( objectId ) as L2Attackable

        return this.describeMovementAction( npc, playerId )
    }

    onEnd( objectId: number ): void {
        let npc: L2Attackable = L2World.getObjectById( objectId ) as L2Attackable

        if ( npc.isMoving() ) {
            npc.abortMoving()
        }

        this.cleanUp( objectId )
    }

    async onStart( objectId: number ): Promise<void> {
        let npc: L2Npc = L2World.getObjectById( objectId ) as L2Npc

        this.createDefaults( objectId )

        let parameters = timerRegistry[ objectId ]

        if ( !parameters.aggroScanTask
            && npc.isAttackable()
            && npc.isAggressive()
            && npc.getAggroRange() > 0 ) {
            parameters.aggroScanTask = setInterval( ( npc as L2Attackable ).runAggroScanTask.bind( npc ), npc.getTemplate().getAggroInterval() )
        }

        if ( !parameters.animationTask && npc.hasRandomAnimation() ) {
            this.startAnimationTask( npc )
        }

        if ( !this.hasRandomWalk( npc ) ) {
            return
        }

        this.performMove( npc )

        parameters.retryTask = setTimeout( this.onStart.bind( this ), this.getInterval(), objectId )
    }

    processEffect(): Promise<void> {
        return Promise.resolve()
    }

    getInterval(): number {
        return Math.trunc( Math.random() * RandomWalkValues.Interval ) + RandomWalkValues.BaseMoveInterval
    }

    performMove( npc: L2Npc ): void {
        if ( Math.random() <= this.getWalkRate() ) {
            return this.moveInGeometry( npc )
        }
    }

    protected moveInGeometry( npc: L2Npc ) : void {
        let geometryId = this.getMovementGeometry( npc )
        if ( geometryId === GeometryId.None ) {
            return
        }

        let geometry = PointGeometry.acquire( geometryId )
        geometry.prepareNextPoint()

        let location = this.getMovementOrigin( npc )
        let x: number = geometry.getX() + location.getX()
        let y: number = geometry.getY() + location.getY()

        geometry.release()

        if ( !npc.moveToLocation( x, y, npc.getZ(), 0 ) ) {
            return
        }

        BroadcastHelper.dataBasedOnVisibility( npc, MoveToLocationWithCharacter( npc ) )
    }

    getMovementOrigin( npc: L2Npc ) : ILocational {
        return npc.getSpawnLocation()
    }

    getMovementGeometry( npc: L2Npc ) : GeometryId {
        return npc.getTemplate().moveGeometryId
    }

    hasRandomWalk( npc: L2Npc ): boolean {
        return !npc.isDead()
                && npc.isMoveCapable()
                && npc.hasRandomWalk()
                && !npc.isMovementDisabled()
                && ( npc.getTemplate().getAIType() !== AIType.MAGE || npc.getCurrentHp() > ( npc.getMaxHp() * 0.4 ) )
    }

    getWalkRate(): number {
        return ConfigManager.npc.randomWalkChance() ?? RandomWalkValues.WalkRate
    }

    canProcessEffect( effect: AIEffect ): boolean {
        return false
    }

    canSwitchIntent( effect: AIEffect ): boolean {
        return effect === AIEffect.Attacked
    }

    switchIntent(): AIIntent {
        return AIIntent.ATTACK
    }

    startAnimationTask( npc: L2Npc ) : void {
        let objectId = npc.getObjectId()
        if ( timerRegistry[ objectId ].animationTask ) {
            npc.onRandomAnimation( 2 + Math.round( Math.random() ) )
        }

        let delay : number = this.getAnimationDelay( npc )
        timerRegistry[ objectId ].animationTask = setTimeout( this.startAnimationTask.bind( this ), delay, npc ) as unknown as Timeout
    }

    getAnimationDelay( npc: L2Npc ) : number {
        if ( npc.isMonster() ) {
            let minValue = Math.max( RandomWalkValues.MinimumAnimationInterval, ConfigManager.general.getMinMonsterAnimation() )
            let maxValue = Math.max( RandomWalkValues.MinimumAnimationInterval, ConfigManager.general.getMaxMonsterAnimation() )

            return _.random( minValue, maxValue )
        }

        let minValue = Math.max( RandomWalkValues.MinimumAnimationInterval, ConfigManager.general.getMinNPCAnimation() )
        let maxValue = Math.max( RandomWalkValues.MinimumAnimationInterval, ConfigManager.general.getMaxNPCAnimation() )

        return _.random( minValue, maxValue )
    }
}

export const RandomWalkTrait = new RandomWalkTraitAction()