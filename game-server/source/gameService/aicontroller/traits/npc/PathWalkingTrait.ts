import _ from 'lodash'
import { IAITrait } from '../../interface/IAITrait'
import { L2Attackable } from '../../../models/actor/L2Attackable'
import { L2World } from '../../../L2World'
import { PacketDispatcher } from '../../../PacketDispatcher'
import { MoveToLocationWithCharacter } from '../../../packets/send/MoveToLocation'
import { L2Npc } from '../../../models/actor/L2Npc'
import { AIEffect } from '../../enums/AIEffect'
import { AIIntent } from '../../enums/AIIntent'
import { L2PcInstance } from '../../../models/actor/instance/L2PcInstance'
import { AIEffectHelper } from '../../helpers/AIEffectHelper'
import { BroadcastHelper } from '../../../helpers/BroadcastHelper'
import { ObjectPool } from '../../../helpers/ObjectPoolHelper'
import { L2NpcRoutePoint, L2NpcRouteType } from '../../../../data/interface/NpcRoutesDataApi'
import { ListenerCache } from '../../../cache/ListenerCache'
import { EventType, NpcRouteFinishedEvent, NpcRouteNodeArrivedEvent } from '../../../models/events/EventType'
import { EventPoolCache } from '../../../cache/EventPoolCache'
import Timeout = NodeJS.Timeout
import { NpcWalkParameters } from '../../../models/NpcWalkParameters'

const activeRoutes: { [ objectId: number ]: PathWalkingParameters } = {}

interface PathWalkingParameters {
    retryTask: Timeout
    trackTask: Timeout
    aggroTask: Timeout
    delayTask: Timeout
}

const objectPool = new ObjectPool<PathWalkingParameters>( 'PathWalkingTrait', () : PathWalkingParameters => {
    return {
        delayTask: undefined,
        aggroTask: undefined,
        trackTask: undefined,
        retryTask: undefined,
    }
}, 5 )

export const PathWalkingTrait: IAITrait = {
    getName(): string {
        return 'PathWalkingTrait'
    },

    describeState( objectId: number, playerId: number ): void {
        let npc: L2Attackable = L2World.getObjectById( objectId ) as L2Attackable

        if ( npc.isMoving() && npc.isVisibleFor( L2World.getPlayer( playerId ) ) ) {
            PacketDispatcher.sendOwnedData( playerId, MoveToLocationWithCharacter( npc ) )
        }
    },

    onEnd( objectId: number ): void {
        let routeData = activeRoutes[ objectId ]
        if ( routeData ) {
            Helper.clearData( routeData )
            objectPool.recycleValue( routeData )
            delete activeRoutes[ objectId ]
        }

        let monster: L2Attackable = L2World.getObjectById( objectId ) as L2Attackable
        monster.abortMoving()
    },

    async onStart( objectId: number ): Promise<void> {
        let npc: L2Npc = L2World.getObjectById( objectId ) as L2Npc

        let routeData = activeRoutes[ objectId ]
        if ( routeData ) {
            if ( !Helper.onContinueWalking( npc ) && !routeData.retryTask ) {
                routeData.retryTask[ objectId ] = setInterval( Helper.onContinueWalking, 15000, npc )
            }

            return
        }

        return Helper.onStartWalking( npc )
    },

    processEffect( objectId: number, effect: AIEffect ): Promise<void> {
        switch ( effect ) {
            case AIEffect.MoveFinished:
                Helper.onNpcArrived( objectId )
                return
        }
    },

    canProcessEffect( effect: AIEffect ): boolean {
        return effect === AIEffect.MoveFinished
    },

    canSwitchIntent( effect: AIEffect ): boolean {
        return effect === AIEffect.Attacked
    },

    switchIntent(): AIIntent {
        return AIIntent.ATTACK
    },
}

const Helper = {
    runTrackingTask( objectId: number ): void {
        let monster: L2Attackable = L2World.getObjectById( objectId ) as L2Attackable
        let player = L2World.getNearestPlayer( monster, monster.getAggroRange(), ( otherPlayer: L2PcInstance ) =>
                !otherPlayer.isDead()
                && !otherPlayer.isInvulnerable()
                && ( monster.isMonster() || ( monster.isGuard() && otherPlayer.getKarma() > 0 ) ) )

        if ( !player ) {
            return
        }

        Helper.stopTrackingTask( activeRoutes[ objectId ] )
        monster.abortMoving()

        return AIEffectHelper.notifyAttackedWithTargetId( monster, player.getObjectId(), 1, 100 )
    },

    stopTrackingTask( data: PathWalkingParameters ) {
        if ( data.trackTask ) {
            clearInterval( data.trackTask )
            data.trackTask = null
        }
    },

    stopRetryTask( data: PathWalkingParameters ) {
        if ( data.trackTask ) {
            clearInterval( data.trackTask )
            data.trackTask = null
        }
    },

    onStartWalking( npc: L2Npc ): void {
        let route = npc.getWalkRoute()
        if ( !route ) {
            return
        }

        let routeData = objectPool.getValue()

        if ( route.routeType === L2NpcRouteType.RandomPoint ) {
            route.index = this.getRandomPointIndex( route )
        }

        if ( route.index === -1 ) {
            return
        }

        let point = route.route.points[ route.index ]
        this.moveToRoute( npc, point )

        if ( npc.isAttackable() && npc.getAggroRange() > 0 ) {
            routeData.trackTask = setInterval( this.runTrackingTask, 2000, npc.getObjectId() )
        }

        if ( npc.isAggressive() && npc.getAggroRange() > 0 ) {
            routeData.aggroTask = setInterval( ( npc as L2Attackable ).runAggroScanTask.bind( npc ), npc.getTemplate().getAggroInterval() )
        }

        activeRoutes[ npc.getObjectId() ] = routeData
    },

    onContinueWalking( npc: L2Npc ): boolean {
        let routeData = activeRoutes[ npc.getObjectId() ]
        if ( !routeData ) {
            return true
        }

        let route = npc.getWalkRoute()
        if ( !route ) {
            return true
        }

        if ( npc.isMoving() || !Helper.canWalk( npc ) ) {
            return false
        }

        Helper.stopRetryTask( routeData )

        let point = route.route.points[ route.index ]

        if ( npc.isInsideRadiusCoordinates( point.x, point.y, point.z, 10, false ) ) {
            Helper.onNpcArrived( npc.getObjectId() )
            return true
        }

        Helper.moveToRoute( npc, point )

        return true
    },

    moveToRoute( npc: L2Npc, point: L2NpcRoutePoint ): void {
        if ( !npc.moveToLocation( point.x, point.y, point.z, 0 ) ) {
            // TODO : what if path is obstructed, retry again?
            return
        }

        BroadcastHelper.dataInLocation( npc, MoveToLocationWithCharacter( npc ), npc.getBroadcastRadius() )
    },

    canWalk( npc: L2Npc ): boolean {
        return !npc.isDead() && !npc.isMovementDisabled()
    },

    onNpcArrived( objectId: number ): void {
        if ( !activeRoutes[ objectId ] ) {
            return
        }

        let npc: L2Npc = L2World.getObjectById( objectId ) as L2Npc
        if ( !npc ) {
            return
        }

        let routeData = activeRoutes[ objectId ]
        let walkRoute = npc.getWalkRoute()

        if ( ListenerCache.hasNpcTemplateListeners( npc.getId(), EventType.NpcRouteNodeArrived ) ) {
            let data = EventPoolCache.getData( EventType.NpcRouteNodeArrived ) as NpcRouteNodeArrivedEvent

            data.characterId = objectId
            data.nodeIndex = walkRoute.index
            data.routeName = walkRoute.route.name

            ListenerCache.sendNpcTemplateEvent( npc.getId(), EventType.NpcRouteNodeArrived, data, walkRoute.index )
        }

        walkRoute.index = Helper.calculateNextNodeIndex( npc )

        if ( routeData.delayTask ) {
            routeData.delayTask.refresh()
            return
        }

        routeData.delayTask = setTimeout( PathWalkingTrait.onStart, Helper.getNodeDelay( npc ), npc.getObjectId() )
    },

    getNodeDelay( npc : L2Npc ): number {
        let delaySeconds = npc.getNpcSpawn()?.getSpawnData().aiParameters?.delayTime as number
        if ( delaySeconds ) {
            return delaySeconds * 1000
        }

        return _.random( 100, 200 )
    },

    clearData( data: PathWalkingParameters ) : void {
        this.stopTrackingTask( data )
        this.stopRetryTask( data )

        if ( data.aggroTask ) {
            clearInterval( data.aggroTask )
            data.aggroTask = null
        }
    },

    getRandomPointIndex( parameters: NpcWalkParameters ) : number {
        let randomIndex = Math.floor( Math.random() * parameters.route.points.length )
        if ( randomIndex === parameters.index ) {
            randomIndex++

            return randomIndex % parameters.route.points.length
        }

        return randomIndex
    },

    calculateNextNodeIndex( npc: L2Npc ) : number {
        let walkRoute = npc.getWalkRoute()

        switch ( walkRoute.routeType ) {
            case 'random':
                return this.getRandomPointIndex( walkRoute )

            case 'restart':
                if ( walkRoute.index === ( walkRoute.route.points.length - 1 ) ) {
                    this.onRouteFinished( npc )
                    return 0
                }

                return walkRoute.index + 1

            case 'teleport':
                if ( walkRoute.index === ( walkRoute.route.points.length - 1 ) ) {
                    let startPoint = walkRoute.route.points[ 0 ]
                    npc.teleportToLocationCoordinates( startPoint.x, startPoint.y, startPoint.z, npc.getHeading(), npc.getInstanceId() )

                    this.onRouteFinished( npc )
                    return 0
                }

                return walkRoute.index + 1

            case 'follow':
                if ( walkRoute.forward ) {
                    if ( walkRoute.index === ( walkRoute.route.points.length - 1 ) ) {
                        walkRoute.forward = false
                        this.onRouteFinished( npc )

                        return walkRoute.index - 1
                    }

                    return walkRoute.index + 1
                }



                if ( walkRoute.index === 0 ) {
                    walkRoute.forward = true
                    this.onRouteFinished( npc )

                    return 1
                }

                return walkRoute.index - 1

            default:
                return -1
        }
    },

    onRouteFinished( npc: L2Npc ) : void {
        if ( ListenerCache.hasNpcTemplateListeners( npc.getId(), EventType.NpcRouteFinished ) ) {
            let eventData = EventPoolCache.getData( EventType.NpcRouteFinished ) as NpcRouteFinishedEvent

            eventData.characterId = npc.getObjectId()

            ListenerCache.sendNpcTemplateEvent( npc.getId(), EventType.NpcRouteFinished, eventData )
        }
    }
}