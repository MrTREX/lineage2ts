import { AISkillUsageType } from '../../../enums/AISkillUsage'
import { L2Attackable } from '../../../models/actor/L2Attackable'
import { BaseMageSkillTrait } from './BaseMageSkillTrait'

class SkillFocusMageTrait extends BaseMageSkillTrait {

    defaultUsageType: AISkillUsageType
    maxIndex: number

    constructor( name: string, usage: AISkillUsageType, index: number ) {
        super()
        this.defaultUsageType = usage
        this.maxIndex = index
        this.name = name
    }

    getNextUsageType( npc: L2Attackable, isInMeleeRange: boolean ) : AISkillUsageType {
        if ( !isInMeleeRange ) {
            return this.defaultUsageType
        }

        let usageState = npc.getSkillUsage()

        if ( usageState.index === 0 ) {
            usageState.index++
            return this.defaultUsageType
        }

        usageState.index++

        if ( usageState.index >= this.maxIndex ) {
            usageState.index = 0
        }

        return null
    }
}

export const BuffSkillMageTrait = new SkillFocusMageTrait( 'BuffSkillMageTrait', AISkillUsageType.Buff, 10 )
export const AttackSkillMageTrait = new SkillFocusMageTrait( 'AttackSkillMageTrait', AISkillUsageType.Attack, 4 )
export const SpecialSkillMageTrait = new SkillFocusMageTrait( 'SpecialSkillMageTrait', AISkillUsageType.Special, 8 )