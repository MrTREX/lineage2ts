import { IAITrait } from '../../interface/IAITrait'
import { L2Npc } from '../../../models/actor/L2Npc'
import { L2World } from '../../../L2World'
import { AIIntent } from '../../enums/AIIntent'
import { SocialAnimationAction } from '../actions/SocialAnimationAction'
import Timeout = NodeJS.Timeout

const timerRegistry: { [ objectId: number ]: Timeout } = {}

class SocialAnimationBaseTrait extends SocialAnimationAction implements IAITrait {
    describeState(): void {}

    getName(): string {
        return 'SocialAnimation'
    }

    processEffect(): Promise<void> {
        return Promise.resolve()
    }

    onStart( objectId: number ): Promise<void> {
        let character: L2Npc = L2World.getObjectById( objectId ) as L2Npc
        if ( !character.hasRandomAnimation() ) {
            return
        }

        if ( !timerRegistry[ objectId ] ) {
            timerRegistry[ objectId ] = setTimeout( this.runAnimation.bind( this ), this.getAnimationInterval(), objectId )
        }
    }

    onEnd( objectId: number ): void {
        clearTimeout( timerRegistry[ objectId ] )
        delete timerRegistry[ objectId ]
    }

    canProcessEffect(): boolean {
        return false
    }

    canSwitchIntent(): boolean {
        return false
    }

    switchIntent(): AIIntent {
        return AIIntent.WAITING
    }

    runAnimation( objectId: number ): void {
        this.performAnimation( objectId )
        timerRegistry[ objectId ] = setTimeout( this.runAnimation.bind( this ), this.getAnimationInterval(), objectId )
    }
}

export const SocialAnimationTrait = new SocialAnimationBaseTrait()