import Timeout = NodeJS.Timeout
import { IAITrait } from '../../interface/IAITrait'
import { AIEffect } from '../../enums/AIEffect'
import { AIIntent } from '../../enums/AIIntent'
import { ConfigManager } from '../../../../config/ConfigManager'
import { L2World } from '../../../L2World'
import { L2Npc } from '../../../models/actor/L2Npc'
import { AIEffectHelper } from '../../helpers/AIEffectHelper'

const waitingTasks: { [ objectId: number ]: Timeout } = {}

export const InteractWaitingTrait: IAITrait = {
    canSwitchIntent( effect: AIEffect ): boolean {
        return effect === AIEffect.Attacked
    },

    switchIntent(): AIIntent {
        return AIIntent.ATTACK
    },

    getName(): string {
        return 'InteractWaitingTrait'
    },

    describeState(): void {},

    processEffect(): Promise<void> {
        return Promise.resolve()
    },

    async onStart( objectId: number ): Promise<void> {
        if ( waitingTasks[ objectId ] ) {
            return
        }

        waitingTasks[ objectId ] = setTimeout( performIntentChange, ( ConfigManager.character.getNpcTalkBlockingTime() + 5 ) * 1000, objectId )
    },

    onEnd( objectId: number ): void {
        if ( waitingTasks[ objectId ] ) {
            clearTimeout( waitingTasks[ objectId ] )
            delete waitingTasks[ objectId ]
        }
    },

    canProcessEffect(): boolean {
        return false
    }
}

function performIntentChange( objectId: number ): Promise<void> {
    let npc = L2World.getObjectById( objectId ) as L2Npc
    return npc && AIEffectHelper.setNextIntent( npc, npc.getAIController().defaultIntent )
}