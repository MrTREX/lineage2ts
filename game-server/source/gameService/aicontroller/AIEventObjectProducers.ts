import {
    AIAttackedEvent,
    AIEffect,
    AIEventData,
    AIFleeAwayEvent,
    AIFollowObjectEvent,
    AIForceAttackTargetEvent,
    AIForceIntentChangeEvent,
    AIForceSpellCastEvent,
    AIInteractTargetEvent,
    AIMoveToLocationEvent,
    AIOwnerHpReducedEvent,
    AIOwnerMpReducedEvent,
    AISkillCastCanceledEvent,
    AISkillCastFinishedEvent,
    AITargetDeadEvent,
} from './enums/AIEffect'

export type AIEffectObjectFactory = () => AIEventData
export const AIEffectTypes: { [ type: number ]: AIEffectObjectFactory } = {
    [ AIEffect.Attacked ]: (): AIAttackedEvent => {
        return {
            aggro: 0,
            attackerId: 0,
            damage: 0,
        }
    },

    [ AIEffect.TargetDead ]: (): AITargetDeadEvent => {
        return {
            targetId: 0,
        }
    },

    [ AIEffect.FollowObject ]: (): AIFollowObjectEvent => {
        return {
            targetId: 0,
            distance: 0,
        }
    },

    [ AIEffect.MoveToLocation ]: (): AIMoveToLocationEvent => {
        return {
            heading: 0,
            instanceId: 0,
            x: 0,
            y: 0,
            z: 0,
        }
    },

    [ AIEffect.ForceSpellCast ]: (): AIForceSpellCastEvent => {
        return {
            isControlPressed: false,
            isShiftPressed: false,
            skillId: 0,
            skillLevel: 0,
            targetId: 0,
        }
    },

    [ AIEffect.ForceIntentChange ]: (): AIForceIntentChangeEvent => {
        return {
            nextIntent: 0,
        }
    },

    [ AIEffect.FleeAway ]: (): AIFleeAwayEvent => {
        return {
            time: 0,
        }
    },

    [ AIEffect.InteractTarget ]: (): AIInteractTargetEvent => {
        return {
            targetId: 0,
            shouldInteract: false,
        }
    },

    [ AIEffect.SkillCastFinished ]: () : AISkillCastFinishedEvent => {
        return {
            isSimultaneous: false,
            skillId: 0
        }
    },

    [ AIEffect.SkillCastCanceled ]: () : AISkillCastCanceledEvent => {
        return {
            isSimultaneous: false,
            skillId: 0
        }
    },

    [ AIEffect.OwnerHPReduced ]: () : AIOwnerHpReducedEvent => {
        return {
            attackerId: 0,
            damage: 0
        }
    },

    [ AIEffect.OwnerMPReduced ]: () : AIOwnerMpReducedEvent => {
        return {
            damage: 0
        }
    },

    [ AIEffect.ForceAttackTarget ]: () : AIForceAttackTargetEvent => {
        return {
            targetId: 0
        }
    }
}