import { IAITrait } from '../interface/IAITrait'
import { AIEffect, AIEventData } from '../enums/AIEffect'
import { IAIController } from '../interface/IAIController'
import { AIIntent } from '../enums/AIIntent'

export class VehicleAIController implements IAIController {

    currentTrait: IAITrait
    ownerVehicleId: number

    constructor( ownerId: number, trait: IAITrait ) {
        this.currentTrait = trait
        this.ownerVehicleId = ownerId
    }

    async notifyEffect( effect: AIEffect, parameters: AIEventData ): Promise<void> {
        if ( this.currentTrait.canProcessEffect( effect ) ) {
            return this.currentTrait.processEffect( this.ownerVehicleId, effect, parameters )
        }
    }

    describeState( playerId: number ): void {
        return this.currentTrait.describeState( this.ownerVehicleId, playerId )
    }

    isActive() : boolean {
        return true
    }

    activate() : Promise<void> {
        return Promise.resolve()
    }

    deactivate() : void {}

    switchIntent(): Promise<void> {
        return Promise.resolve()
    }

    isIntent( intent: AIIntent ): boolean {
        return false
    }
}