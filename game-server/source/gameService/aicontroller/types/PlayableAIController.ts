import { AIIntent } from '../enums/AIIntent'
import { AIEffect, AIEventData } from '../enums/AIEffect'
import { EffectsCache } from '../EffectCache'
import { IAIController } from '../interface/IAIController'
import { L2Playable } from '../../models/actor/L2Playable'
import chalk from 'chalk'
import { FightStanceCache } from '../../cache/FightStanceCache'
import { PacketDispatcher } from '../../PacketDispatcher'
import { AutoAttackStart } from '../../packets/send/AutoAttackStart'
import { AggroCache } from '../../cache/AggroCache'
import logSymbols from 'log-symbols'
import { ObjectPool } from '../../helpers/ObjectPoolHelper'
import { RoaringBitmap32 } from 'roaring'
import { CharacterAIController } from './CharacterAIController'
import { TraitSettings } from '../interface/IAITrait'
import { ServerLog } from '../../../logger/Logger'
import { ConfigManager } from '../../../config/ConfigManager'


const storedEffectTypes = new Set<number>( [
    AIEffect.Attacked,
    AIEffect.ForceSpellCast,
    AIEffect.ForceAttackTarget,
    AIEffect.MoveToLocation,
    AIEffect.InteractTarget,
    AIEffect.FollowObject
] )

export type NextTraitActionFunction = () => AIIntent | void
export type NextEffectFunction = () => void | Promise<void>
export interface NextEffectParameters {
    method: NextEffectFunction
    effects: RoaringBitmap32
}

const nextEffectPool = new ObjectPool<NextEffectParameters>( 'NextEffectParameters', () : NextEffectParameters => {
    return {
        effects: new RoaringBitmap32(),
        method: undefined
    }
} )

export class PlayableAIController extends CharacterAIController implements IAIController {

    traitAction: NextTraitActionFunction
    nextEffectAction: NextEffectParameters

    constructor( playable: L2Playable, traits: TraitSettings, defaultIntent: AIIntent = AIIntent.WAITING ) {
        super( playable, traits, defaultIntent )
        this.currentTrait = this.traitMap[ defaultIntent ]
        this.canAct = true
    }

    async notifyEffect( effect: AIEffect, parameters: AIEventData ): Promise<void> {
        if ( this.nextEffectAction && this.nextEffectAction.effects.has( effect ) ) {
            await this.nextEffectAction.method()
            this.removeNextEffectAction()
        }

        if ( this.currentTrait.canProcessEffect( effect ) ) {
            await this.currentTrait.processEffect( this.ownerId, effect, parameters )
        }

        let intent: AIIntent = this.getNextIntent( effect, parameters )

        if ( this.traitAction ) {
            let nextIntent = this.traitAction() as AIIntent
            if ( nextIntent !== undefined ) {
                intent = nextIntent
            }

            this.traitAction = null
        }

        if ( intent === this.currentIntent ) {
            return
        }

        if ( storedEffectTypes.has( effect ) ) {
            EffectsCache.setParameters( this.ownerId, effect, EffectsCache.cloneParameters( parameters ) )
        }


        if ( intent === AIIntent.WAITING ) {
            AggroCache.clearData( this.ownerId )
        }

        return this.switchIntent( intent )
    }

    private chooseTrait( intent: AIIntent ) : void {
        this.currentIntent = intent ?? this.defaultIntent

        this.stopCurrentTrait()

        this.currentTrait = this.traitMap[ this.currentIntent ]

        if ( !this.currentTrait ) {
            ServerLog.trace( `${PlayableAIController.name}: no traits to run for intent ${chalk.cyan( AIIntent[ this.currentIntent ] )}` )
            this.currentTrait = this.traitMap[ this.defaultIntent ]
            this.currentIntent = this.defaultIntent
        }
    }

    stopCurrentTrait(): void {
        return this.currentTrait.onEnd( this.ownerId )
    }

    describeState( playerId: number ): void {
        if ( FightStanceCache.hasPlayableStance( this.ownerId ) ) {
            PacketDispatcher.sendOwnedData( playerId, AutoAttackStart( this.ownerId ) )
        }

        return this.currentTrait.describeState( this.ownerId, playerId )
    }

    removeNextEffectAction() : void {
        this.nextEffectAction.effects.clear()
        this.nextEffectAction.method = null

        nextEffectPool.recycleValue( this.nextEffectAction )

        this.nextEffectAction = null
    }

    setNextEffectAction( method: NextEffectFunction, effects: Array<AIEffect> ) : void {
        if ( !this.nextEffectAction ) {
            this.nextEffectAction = nextEffectPool.getValue()
        }

        this.nextEffectAction.method = method
        this.nextEffectAction.effects.addMany( effects )
    }

    isActive() : boolean {
        return true
    }

    activate() : Promise<void> {
        this.canAct = true

        return this.switchIntent( this.defaultIntent )
    }

    deactivate() : void {
        return this.resetParameters()
    }

    switchIntent( intent: AIIntent ) : Promise<void> {
        this.chooseTrait( intent )

        if ( ConfigManager.diagnostic.showDebugMessages() ) {
            ServerLog.info( `${PlayableAIController.name} ${chalk.cyan( AIIntent[ this.currentIntent ] )} intent is running trait ${chalk.yellow( this.currentTrait.getName() )}` )
        }

        return this.currentTrait.onStart( this.ownerId )
    }
}