import { AIEffect, AIEventData } from '../enums/AIEffect'
import { AIIntent } from '../enums/AIIntent'

export interface IAIController {
    notifyEffect( effect: AIEffect, parameters: AIEventData ): Promise<void>
    describeState( playerId: number ): void
    isActive() : boolean
    activate() : Promise<void>
    deactivate() : void
    switchIntent( intent: AIIntent ) : Promise<void>
    isIntent( intent: AIIntent ) : boolean
}