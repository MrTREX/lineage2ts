import { AIEffect, AIEventData } from './enums/AIEffect'
import _ from 'lodash'

type AIEffectData = { [ effect: number ] : AIEventData }
const values : { [ objectId: number] : AIEffectData } = {}

export const EffectsCache = {
    setParameters( objectId: number, effect: AIEffect, parameters: AIEventData ) : void {
        if ( !values[ objectId ] ) {
            values[ objectId ] = {}
        }

        values[ objectId ][ effect ] = parameters
    },

    getParameters( objectId: number, effect: AIEffect ) : Readonly<AIEventData> {
        let value = values[ objectId ]
        if ( !value ) {
            return null
        }

        let data = value[ effect ]

        delete value[ effect ]

        return data
    },

    peekParameters( objectId: number, effect: AIEffect ) : AIEventData {
        let value = values[ objectId ]
        if ( !value ) {
            return null
        }

        return value[ effect ]
    },

    hasParameters( objectId: number, effect: AIEffect ) : boolean {
        let value = values[ objectId ]
        if ( !value ) {
            return null
        }

        return !!value[ effect ]
    },

    resetParameters( objectId: number, effect: AIEffect ) : void {
        let value = values[ objectId ]
        if ( !value ) {
            return
        }

        delete value[ effect ]
    },

    removeParameters( objectId: number ) : void {
        delete values[ objectId ]
    },

    /*
        TODO : use AIEffectPoolCache to clone data, however make sure that it is cleaned up by EffectsCache
        One way would be to pass Promise method to EffectsCache, which will execute method and return used data back to AIEffectPoolCache
        For sake of simplicity, current cloning is sufficient
     */
    cloneParameters( data: AIEventData ) : AIEventData {
        let copyData = {}

        _.assign( copyData, data )

        return copyData
    }
}