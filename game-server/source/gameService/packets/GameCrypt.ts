export class GameCrypt {
    inKey: Buffer
    outKey: Buffer
    shouldEncrypt: boolean

    constructor() {
        this.inKey = Buffer.allocUnsafeSlow( 16 )
        this.outKey = Buffer.allocUnsafeSlow( 16 )
        this.shouldEncrypt = false
    }

    setKey( key: Buffer ) : void {
        key.copy( this.inKey )
        key.copy( this.outKey )
    }

    decrypt( data: Buffer ) : void {
        if ( !this.shouldEncrypt ) {
            return
        }

        let temp = 0
        for ( let index = 0; index < data.length; index++ ) {
            let temp2 = data[ index ]
            data[ index ] = temp2 ^ this.inKey[ index & 15 ] ^ temp
            temp = temp2
        }

        let newValue = this.inKey.readInt32LE( 8 ) + data.length
        this.inKey.writeInt32LE( newValue, 8 )
    }

    encrypt( data: Buffer ) : void {
        if ( !this.shouldEncrypt ) {
            this.shouldEncrypt = true
            return
        }

        let temp = 0
        for ( let index = 0; index < data.length; index++ ) {
            let temp2 = data[ index ]
            temp = temp2 ^ this.outKey[ index & 15 ] ^ temp
            data[ index ] = temp
        }

        let newValue = this.outKey.readInt32LE( 8 ) + data.length
        this.outKey.writeInt32LE( newValue, 8 )
    }
}