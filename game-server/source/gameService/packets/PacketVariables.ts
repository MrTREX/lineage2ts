import { ConfigManager } from '../../config/ConfigManager'

export const PacketVariables = {
    millisInDay: 86400000,
    maximumItemsLimit: 0,
}

function updateMaximumItemsLimit() {
    PacketVariables.maximumItemsLimit = Math.max( ConfigManager.character.getMaximumSlotsForNoDwarf(),
            Math.max( ConfigManager.character.getMaximumSlotsForDwarf(),
                    ConfigManager.character.getMaximumSlotsForGMPlayer() ) )
}

ConfigManager.character.subscribe( updateMaximumItemsLimit )
updateMaximumItemsLimit()

export const PacketHelper = {
    copyPacket( staticPacket : Buffer ) : Buffer {
        let data : Buffer = Buffer.allocUnsafe( staticPacket.length )
        staticPacket.copy( data )

        return data
    },

    /*
        Please see differences between Buffer.allocUnsafe vs Buffer.allocUnsafeSlow
        Long answer:
        - we need to keep buffer data for undetermined amount of time (usually under one hour)
        - however using normal buffer pool presents problem with garbage collection (receiving and sending packets use same pool)
        - thus use heap allocation for data that does not need to be part of buffer pool
     */
    preservePacket( dynamicPacket : Buffer ) : Buffer {
        let data : Buffer = Buffer.allocUnsafeSlow( dynamicPacket.length )
        dynamicPacket.copy( data )

        return data
    }
}