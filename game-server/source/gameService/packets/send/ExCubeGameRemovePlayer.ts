import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExCubeGameRemovePlayer( objectId: number, isRedTeam: boolean ) : Buffer {
    return new DeclaredServerPacket( 19 )
        .writeC( 0xFE )
        .writeH( 0x97 )
        .writeD( 0x02 )
        .writeD( 0xFFFFFFFF )

        .writeD( isRedTeam ? 0x01 : 0x00 )
        .writeD( objectId )
        .getBuffer()
}