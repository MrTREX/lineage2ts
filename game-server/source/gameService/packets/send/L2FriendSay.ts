import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

export function L2FriendSay( senderName: string, receiverName: string, message: string ) : Buffer {
    let dynamicSize : number = getStringSize( senderName ) + getStringSize( receiverName ) + getStringSize( message )
    return new DeclaredServerPacket( 5 + dynamicSize )
            .writeC( 0x78 )
            .writeD( 0 )
            .writeS( receiverName )
            .writeS( senderName )
            .writeS( message )
            .getBuffer()
}