import { GraciaSeedsManager } from '../../instancemanager/GraciaSeedsManager'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExShowSeedMapInfo(): Buffer {
    return new DeclaredServerPacket( 39 )
            .writeC( 0xFE ) // Id
            .writeH( 0xA1 ) // SubId
            .writeD( 2 ) // seed count

            // Seed of Destruction
            .writeD( -246857 ) // x coord
            .writeD( 251960 ) // y coord
            .writeD( 4331 ) // z coord
            .writeD( 2770 + GraciaSeedsManager.getState() ) // sys msg id

            // Seed of Infinity
            .writeD( -213770 ) // x coord
            .writeD( 210760 ) // y coord
            .writeD( 4400 ) // z coord

            .writeD( 2766 ) // sys msg id
            .getBuffer()
}