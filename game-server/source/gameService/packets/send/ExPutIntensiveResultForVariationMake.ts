import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExPutIntensiveResultForVariationMake( objectId: number, lifeStoneId: number, gemstoneItemId: number, count: number ) : Buffer {
    return new DeclaredServerPacket( 23 )
            .writeC( 0xFE )
            .writeH( 0x54 )
            .writeD( objectId )
            .writeD( lifeStoneId )

            .writeD( gemstoneItemId )
            .writeD( count )
            .writeD( 1 )
            .getBuffer()
}