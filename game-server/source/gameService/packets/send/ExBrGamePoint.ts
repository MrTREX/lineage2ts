import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExBrGamePoint( objectId: number, points: number ) : Buffer {
    return new DeclaredServerPacket( 19 )
        .writeC( 0xFE )
        .writeH( 0xD5 )
        .writeD( objectId )
        .writeQ( points )
        .writeD( 0x00 )
        .getBuffer()
}