import { PartyDistributionType } from '../../enums/PartyDistributionType'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

export function ExAskModifyPartyLooting( name: string, type: PartyDistributionType ) : Buffer {
    return new DeclaredServerPacket( 7 + getStringSize( name ) )
            .writeC( 0xFE )
            .writeH( 0xBF )
            .writeS( name )
            .writeD( type )
            .getBuffer()
}