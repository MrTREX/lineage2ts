import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'
import { HtmlActionCache } from '../../cache/HtmlActionCache'
import { HtmlActionScope } from '../../enums/HtmlActionScope'

export function TutorialShowHtml( html: string, path: string, playerId: number ) : Buffer {
    let currentHtml = HtmlActionCache.buildCacheWithPath( playerId, HtmlActionScope.Tutorial, 0, html, path )
    return new DeclaredServerPacket( 1 + getStringSize( currentHtml ) )
            .writeC( 0xA6 )
            .writeS( currentHtml )
            .getBuffer()
}