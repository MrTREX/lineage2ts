import { L2Henna } from '../../models/items/L2Henna'
import { L2World } from '../../L2World'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export function HennaItemRemoveInfo( henna: L2Henna, player: L2PcInstance ) : Buffer {
    return new DeclaredServerPacket( 67 )
            .writeC( 0xE7 )
            .writeD( henna.getDyeId() )
            .writeD( henna.getDyeItemId() )
            .writeQ( henna.getCancelCount() )

            .writeQ( henna.getCancelFee() )
            .writeD( henna.isAllowedClass( player.getClassId() ) ? 0x01 : 0x00 ) // able to remove or not
            .writeQ( player.getAdena() )
            .writeD( player.getINT() )

            .writeC( player.getINT() - henna.getStatINT() )
            .writeD( player.getSTR() )
            .writeC( player.getSTR() - henna.getStatSTR() )
            .writeD( player.getCON() )

            .writeC( player.getCON() - henna.getStatCON() )
            .writeD( player.getMEN() )
            .writeC( player.getMEN() - henna.getStatMEN() )
            .writeD( player.getDEX() )

            .writeC( player.getDEX() - henna.getStatDEX() )
            .writeD( player.getWIT() )
            .writeC( player.getWIT() - henna.getStatWIT() )
            .getBuffer()
}