import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2World } from '../../L2World'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { BlocklistCache } from '../../cache/BlocklistCache'
import { AreaType } from '../../models/areas/AreaType'

export function EtcStatusUpdate( objectId: number ) : Buffer {
    let player: L2PcInstance = L2World.getPlayer( objectId )
    let shouldBlockChat = ( BlocklistCache.getMessageRefusal( objectId ) || player.isChatBanned() || player.isSilenceMode() ) ? 1 : 0

    return new DeclaredServerPacket( 37 )
        .writeC( 0xf9 )
        .writeD( player.getCharges() )
        .writeD( player.getWeightPenalty() )
        .writeD( shouldBlockChat )

        .writeD( player.isInArea( AreaType.Damage ) ? 1 : 0 )
        .writeD( player.getExpertiseWeaponPenalty() )
        .writeD( player.getExpertiseArmorPenalty() )
        .writeD( player.hasCharmOfCourage() ? 1 : 0 )

        .writeD( player.getDeathPenaltyBuffLevel() )
        .writeD( player.getChargedSouls() )
        .getBuffer()
}