import { L2Henna } from '../../models/items/L2Henna'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export function HennaItemDrawInfo( henna: L2Henna, player: L2PcInstance ): Buffer {
    return new DeclaredServerPacket( 67 )
            .writeC( 0xE4 )
            .writeD( henna.getDyeId() ) // symbol Id
            .writeD( henna.getDyeItemId() ) // item id of dye
            .writeQ( henna.getWearCount() ) // total amount of dye require

            .writeQ( henna.getWearFee() ) // total amount of Adena require to draw symbol
            .writeD( henna.isAllowedClass( player.getClassId() ) ? 0x01 : 0x00 ) // able to draw or not 0 is false and 1 is true
            .writeQ( player.getAdena() )
            .writeD( player.getINT() ) // current INT

            .writeC( player.getINT() + henna.getStatINT() ) // equip INT
            .writeD( player.getSTR() ) // current STR
            .writeC( player.getSTR() + henna.getStatSTR() ) // equip STR
            .writeD( player.getCON() ) // current CON

            .writeC( player.getCON() + henna.getStatCON() ) // equip CON
            .writeD( player.getMEN() ) // current MEN
            .writeC( player.getMEN() + henna.getStatMEN() ) // equip MEN
            .writeD( player.getDEX() ) // current DEX

            .writeC( player.getDEX() + henna.getStatDEX() ) // equip DEX
            .writeD( player.getWIT() ) // current WIT
            .writeC( player.getWIT() + henna.getStatWIT() ) // equip WIT
            .getBuffer()
}