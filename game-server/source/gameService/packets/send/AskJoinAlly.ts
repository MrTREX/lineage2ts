import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

export function AskJoinAlly( objectId: number, name: string ) : Buffer {
    return new DeclaredServerPacket( 5 + getStringSize( name ) )
            .writeC( 0xBB )
            .writeD( objectId )
            .writeS( name )
            .getBuffer()
}