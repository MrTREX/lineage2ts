import { HtmlActionScope } from '../../enums/HtmlActionScope'
import { HtmlActionCache } from '../../cache/HtmlActionCache'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function TutorialCloseHtml( playerId: number ): Buffer {
    HtmlActionCache.clearHtmlActions( playerId, HtmlActionScope.Tutorial )

    return new DeclaredServerPacket( 1 )
            .writeC( 0xa9 )
            .getBuffer()
}