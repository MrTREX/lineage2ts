import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export const enum ExVariationCancelResultType {
    Failure,
    Success
}

export function ExVariationCancelResult( value: ExVariationCancelResultType ): Buffer {
    return new DeclaredServerPacket( 7 )
            .writeC( 0xFE )
            .writeH( 0x58 )
            .writeD( value )
            .getBuffer()
}