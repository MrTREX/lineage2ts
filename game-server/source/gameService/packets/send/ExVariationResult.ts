import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export const enum ExVariationResultType {
    Failure,
    Success
}

export function ExVariationResult( stats: number, type: ExVariationResultType ) : Buffer {
    return new DeclaredServerPacket( 15 )
            .writeC( 0xFE )
            .writeH( 0x56 )
            .writeQ( stats )
            .writeD( type )
            .getBuffer()
}