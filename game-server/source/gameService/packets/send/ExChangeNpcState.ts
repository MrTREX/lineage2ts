import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExChangeNpcState( objectId: number, state: number ) {
    return new DeclaredServerPacket( 11 )
            .writeC( 0xFE )
            .writeH( 0xBE )
            .writeD( objectId )
            .writeD( state )
            .getBuffer()
}