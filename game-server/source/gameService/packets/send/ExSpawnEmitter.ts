import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExSpawnEmitter( playerObjectId: number, npcObjectId: number ) : Buffer {
    return new DeclaredServerPacket( 15 )
            .writeC( 0xFE )
            .writeH( 0x5D )
            .writeD( npcObjectId )
            .writeD( playerObjectId )

            .writeD( 0 )
            .getBuffer()
}