import { L2Summon } from '../../models/actor/L2Summon'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

export function ExPartyPetWindowAddWithSummon( summon: L2Summon ): Buffer {
    return new DeclaredServerPacket( 39 + getStringSize( summon.getName() ) )
            .writeC( 0xFE )
            .writeH( 0x18 )
            .writeD( summon.getObjectId() )
            .writeD( summon.getTemplate().infoId )

            .writeD( summon.getSummonType() )
            .writeD( summon.getOwnerId() )
            .writeS( summon.getName() )
            .writeD( summon.getCurrentHp() )

            .writeD( summon.getMaxHp() )
            .writeD( summon.getCurrentMp() )
            .writeD( summon.getMaxMp() )
            .writeD( summon.getLevel() )

            .getBuffer()
}