import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function PledgeSkillListAdd( id: number, level: number ): Buffer {
    return new DeclaredServerPacket( 11 )
            .writeC( 0xFE )
            .writeH( 0x3B )
            .writeD( id )
            .writeD( level )
            .getBuffer()
}