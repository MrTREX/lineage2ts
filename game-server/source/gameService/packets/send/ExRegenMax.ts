import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExRegenMax( time: number, interval: number, amountPerInterval: number ) : Buffer {
    return new DeclaredServerPacket( 19 )
            .writeC( 0xFE )
            .writeH( 0x01 )
            .writeD( 1 )
            .writeD( time )

            .writeD( interval )
            .writeD( amountPerInterval )
            .getBuffer()
}