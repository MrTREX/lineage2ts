import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2Vehicle } from '../../models/actor/L2Vehicle'

export function VehicleInfo( object: L2Vehicle ) : Buffer {
    return new DeclaredServerPacket( 21 )
            .writeC( 0x60 )
            .writeD( object.getObjectId() )
            .writeD( object.getX() )
            .writeD( object.getY() )

            .writeD( object.getZ() )
            .writeD( object.getHeading() )
            .getBuffer()
}