import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function PledgeShowMemberListDeleteAll(): Buffer {
    return new DeclaredServerPacket( 1 ).writeC( 0x88 ).getBuffer()
}