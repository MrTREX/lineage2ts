import { L2World } from '../../L2World'
import { DataManager } from '../../../data/manager'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2RecipeDataItem } from '../../../data/interface/RecipeDataApi'

export function RecipeItemMakeInfo( recipeId: number, objectId: number, isSuccess: boolean ) : Buffer {
    let player = L2World.getPlayer( objectId )
    return RecipeItemMakeInfoWithPlayer( recipeId, player, isSuccess )
}

export function RecipeItemMakeInfoWithPlayer( recipeId: number, player: L2PcInstance, isSuccess: boolean ) : Buffer {
    let recipe : L2RecipeDataItem = DataManager.getRecipeData().getRecipeList( recipeId )

    return new DeclaredServerPacket( 21 )
            .writeC( 0xDD )
            .writeD( recipeId )
            .writeD( recipe.isCommon ? 1 : 0 )
            .writeD( player.getCurrentMp() )

            .writeD( player.getMaxMp() )
            .writeD( isSuccess ? 1 : 0 )
            .getBuffer()
}