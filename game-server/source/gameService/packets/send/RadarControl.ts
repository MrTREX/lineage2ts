import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export const enum RadarControlOperation {
    Add,
    Remove,
    RemoveAll
}

export const enum RadarControlType {
    Normal = 1,
    Npc = 2
}

export function RadarControl( operation: RadarControlOperation, type: RadarControlType, x: number, y: number, z: number ) : Buffer {
    return new DeclaredServerPacket( 21 )
            .writeC( 0xf1 )
            .writeD( operation )
            .writeD( type )
            .writeD( x )

            .writeD( y )
            .writeD( z )
            .getBuffer()
}