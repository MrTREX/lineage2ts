import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExCubeGameChangePoints( timeLeft: number, bluePoints: number, redPoints: number ) : Buffer {
    return new DeclaredServerPacket( 19 )
        .writeC( 0xfe )
        .writeH( 0x98 )
        .writeD( 0x02 )
        .writeD( timeLeft )

        .writeD( bluePoints )
        .writeD( redPoints )
        .getBuffer()
}