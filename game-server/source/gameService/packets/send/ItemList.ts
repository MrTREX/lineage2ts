import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { ItemPacketHelper } from './helpers/ItemPacketHelper'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { PcInventory } from '../../models/itemcontainer/PcInventory'

export function ItemList( items: Array<L2ItemInstance>, inventory: PcInventory, showWindow : boolean = false ): Buffer {
    let packet = new DeclaredServerPacket( 5 + ( ItemPacketHelper.writeItemSize() * items.length ) + ItemPacketHelper.writeInventoryBlockSize( inventory ) )
            .writeC( 0x11 )
            .writeH( showWindow ? 0x01 : 0x00 )
            .writeH( items.length )

    items.forEach( ( item: L2ItemInstance ) => {
        ItemPacketHelper.writeItem( packet, item )
    } )

    ItemPacketHelper.writeInventoryBlock( packet, inventory )
    return packet.getBuffer()
}