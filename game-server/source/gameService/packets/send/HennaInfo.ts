import { L2Henna } from '../../models/items/L2Henna'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { PlayerHennaCache } from '../../cache/PlayerHennaCache'

export function HennaInfo( player: L2PcInstance ): Buffer {
    let size = PlayerHennaCache.getConsumedSlots( player.getObjectId() )
    let packet = new DeclaredServerPacket( 15 + size * 8 )
            .writeC( 0xE5 )
            .writeC( player.getHennaStatINT() )
            .writeC( player.getHennaStatSTR() )
            .writeC( player.getHennaStatCON() )

            .writeC( player.getHennaStatMEN() )
            .writeC( player.getHennaStatDEX() )
            .writeC( player.getHennaStatWIT() )
            .writeD( 3 )
            .writeD( size )

    PlayerHennaCache.getHennas( player.getObjectId() ).forEach( ( henna: L2Henna ) => {
        if ( !henna ) {
            return
        }

        packet
            .writeD( henna.getDyeId() )
            .writeD( 1 )
    } )

    return packet.getBuffer()
}