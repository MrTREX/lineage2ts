import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2Vehicle } from '../../models/actor/L2Vehicle'

export function VehicleDeparture( object: L2Vehicle ) : Buffer {
    return new DeclaredServerPacket( 25 )
            .writeC( 0x6C )
            .writeD( object.getObjectId() )
            .writeD( object.getMoveSpeed() )
            .writeD( object.getStat().getRotationSpeed() )

            .writeD( object.getXDestination() )
            .writeD( object.getYDestination() )
            .writeD( object.getZDestination() )
            .getBuffer()
}