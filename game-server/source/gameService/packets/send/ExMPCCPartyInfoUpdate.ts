import { L2Party } from '../../models/L2Party'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

export const enum MPCCPartyInfoMode {
    Remove,
    Add
}

export function ExMPCCPartyInfoUpdate( party: L2Party, mode: MPCCPartyInfoMode ): Buffer {
    let player = party.getLeader()
    return new DeclaredServerPacket( 15 + getStringSize( player.getName() ) )
            .writeC( 0xFE )
            .writeH( 0x5B )
            .writeS( player.getName() )
            .writeD( player.getObjectId() )

            .writeD( party.getMemberCount() )
            .writeD( mode )
            .getBuffer()
}