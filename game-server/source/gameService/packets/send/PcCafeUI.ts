import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function PcCafeUI() : Buffer {
    return new DeclaredServerPacket( 3 )
        .writeC( 0xFE )
        .writeH( 0x44 )
        .getBuffer()
}