import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function Earthquake( x: number, y: number, z: number, intensity: number, duration: number ) : Buffer {
    return new DeclaredServerPacket( 25 )
            .writeC( 0xD3 )
            .writeD( x )
            .writeD( y )
            .writeD( z )

            .writeD( intensity )
            .writeD( duration )
            .writeD( 0 )
            .getBuffer()
}