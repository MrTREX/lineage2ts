import { Castle } from '../../models/entity/Castle'
import { CastleManager } from '../../instancemanager/CastleManager'
import { ClanCache } from '../../cache/ClanCache'
import { DeclaredServerPacket, EmptyString, EmptyStringSize, getStringSize } from '../../../packets/DeclaredServerPacket'

function getCastlesSize( castles: Array<Castle> ): number {
    return castles.reduce( ( size: number, castle: Castle ): number => {
        let clan = ClanCache.getClan( castle.getOwnerId() )
        return size + 12 + ( clan ? getStringSize( clan.getName() ) : EmptyStringSize )
    }, 0 )
}

// TODO : consider caching packet until clan ownership changes
export function ExShowCastleInfo(): Buffer {
    let castles: Array<Castle> = CastleManager.getCastles()
    let packet = new DeclaredServerPacket( 7 + getCastlesSize( castles ) )
            .writeC( 0xFE )
            .writeH( 0x14 )
            .writeD( castles.length )

    castles.forEach( ( castle: Castle ) => {
        packet.writeD( castle.getResidenceId() )

        let clan = ClanCache.getClan( castle.getOwnerId() )
        packet
                .writeS( clan ? clan.getName() : EmptyString )
                .writeD( castle.getTaxPercent() )
                .writeD( castle.getSiege().getSiegeDate() / 1000 )
    } )

    return packet.getBuffer()
}