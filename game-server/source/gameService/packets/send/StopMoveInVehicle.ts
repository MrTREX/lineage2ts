import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export function StopMoveInVehicle( player: L2PcInstance, vehicleId: number ): Buffer {
    let position = player.getInVehiclePosition()

    return new DeclaredServerPacket( 25 )
            .writeC( 0x7F )
            .writeD( player.getObjectId() )
            .writeD( vehicleId )
            .writeD( position.getX() )

            .writeD( position.getY() )
            .writeD( position.getZ() )
            .writeD( player.getHeading() )
            .getBuffer()
}