import { CastleManorManager } from '../../instancemanager/CastleManorManager'
import { L2Seed } from '../../models/L2Seed'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExShowManorDefaultInfo( hideUIButtons: boolean ): Buffer {
    let seeds: Array<L2Seed> = CastleManorManager.getCrops()

    let packet = new DeclaredServerPacket( 8 + seeds.length * 22 )
            .writeC( 0xFE )
            .writeH( 0x25 )
            .writeC( hideUIButtons ? 0x01 : 0x00 ) // Hide "Seed Purchase" and "Crop Sales" buttons
            .writeD( seeds.length )

    seeds.forEach( ( seed: L2Seed ) => {
        packet
                .writeD( seed.getCropId() ) // crop Id
                .writeD( seed.getLevel() ) // level
                .writeD( seed.getSeedReferencePrice() ) // seed price
                .writeD( seed.getCropReferencePrice() ) // crop price

                .writeC( 1 ) // Reward 1 type
                .writeD( seed.getReward( 1 ) ) // Reward 1 itemId
                .writeC( 1 ) // Reward 2 type
                .writeD( seed.getReward( 2 ) ) // Reward 2 itemId
    } )

    return packet.getBuffer()
}