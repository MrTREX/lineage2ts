import { CastleManorManager } from '../../instancemanager/CastleManorManager'
import { SeedProduction } from '../../models/manor/SeedProduction'
import { L2Seed } from '../../models/L2Seed'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExShowSeedInfo( manorId: number, isNexPeriod: boolean, hidePurchaseButtons: boolean ): Buffer {
    let seeds = ( isNexPeriod && !CastleManorManager.isManorApproved() ) ? [] : CastleManorManager.getSeedProduction( manorId, isNexPeriod )

    let packet = new DeclaredServerPacket( 16 + seeds.length * 42 )
            .writeC( 0xFE )
            .writeH( 0x23 )
            .writeC( hidePurchaseButtons ? 0x01 : 0 )
            .writeD( manorId )

            .writeD( 0 )
            .writeD( seeds.length )

    seeds.forEach( ( production: SeedProduction ) => {
        packet
                .writeD( production.getId() ) // Seed id
                .writeQ( production.getAmount() ) // Left to buy
                .writeQ( production.getStartAmount() ) // Started amount
                .writeQ( production.getPrice() ) // Sell Price

        let seed: L2Seed = CastleManorManager.getSeed( production.getId() )
        if ( !seed ) {
            packet
                    .writeD( 0 ) // Seed level
                    .writeC( 0x01 ) // Reward 1
                    .writeD( 0 ) // Reward 1 - item id
                    .writeC( 0x01 ) // Reward 2
                    .writeD( 0 ) // Reward 2 - item id

            return
        }

        packet
                .writeD( seed.getLevel() ) // Seed level
                .writeC( 0x01 ) // Reward 1
                .writeD( seed.getReward( 1 ) ) // Reward 1 - item id
                .writeC( 0x01 ) // Reward 2
                .writeD( seed.getReward( 2 ) ) // Reward 2 - item id
    } )

    return packet.getBuffer()
}