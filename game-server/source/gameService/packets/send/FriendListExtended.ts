import { DatabaseManager } from '../../../database/manager'
import { L2World } from '../../L2World'
import { L2CharactersTableFriendInfo } from '../../../database/interface/CharactersTableApi'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'
import { PlayerFriendsCache } from '../../cache/PlayerFriendsCache'

// TODO : cache results of friend list call due to database direct tie
export async function FriendListExtended( player: L2PcInstance ): Promise<Buffer> {
    let playerFriendsIds = PlayerFriendsCache.getFriends( player.getObjectId() )
    let friends: Array<L2CharactersTableFriendInfo> = playerFriendsIds.size > 0 ? await DatabaseManager.getCharacterTable().getFriendListItems( Array.from( playerFriendsIds ) ) : []

    let friendsSize: number = friends.reduce( ( size: number, info: L2CharactersTableFriendInfo ): number => {
        return size + getStringSize( info.name ) + 20
    }, 0 )

    let packet = new DeclaredServerPacket( 5 + friendsSize )
            .writeC( 0x58 )
            .writeD( friends.length )

    friends.forEach( ( info: L2CharactersTableFriendInfo ) => {
        let playerFriend: L2PcInstance = L2World.getPlayer( info.objectId )
        let isOnline: boolean = playerFriend && playerFriend.isOnline()

        packet
                .writeD( info.objectId ) // character id
                .writeS( playerFriend ? playerFriend.getName() : info.name )
                .writeD( isOnline ? 0x01 : 0x00 ) // online
                .writeD( isOnline ? info.objectId : 0x00 ) // object id if online

                .writeD( playerFriend ? playerFriend.getClassId() : info.classId )
                .writeD( playerFriend ? playerFriend.getLevel() : info.level )
    } )

    return packet.getBuffer()
}