import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { PacketHelper } from '../PacketVariables'

const staticTruePacket: Buffer = PacketHelper.preservePacket( new DeclaredServerPacket( 7 )
        .writeC( 0xfe )
        .writeH( 0xb4 )
        .writeD( 0x01 )
        .getBuffer() )

const staticFalsePacket = PacketHelper.preservePacket( new DeclaredServerPacket( 7 )
        .writeC( 0xfe )
        .writeH( 0xb4 )
        .writeD( 0x00 )
        .getBuffer() )

export function ExNoticePostSent( showAnimation: boolean ): Buffer {
    return PacketHelper.copyPacket( showAnimation ? staticTruePacket : staticFalsePacket )
}