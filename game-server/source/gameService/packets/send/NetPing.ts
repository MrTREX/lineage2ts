import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function NetPing( timeMillis: number ) : Buffer {
    return new DeclaredServerPacket( 3 )
        .writeC( 0xD9 )
        .writeD( timeMillis )
        .getBuffer()
}