import { L2Character } from '../../models/actor/L2Character'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function StopMove( character: L2Character ) : Buffer {
    return new DeclaredServerPacket( 21 )
            .writeC( 0x47 )
            .writeD( character.getObjectId() )
            .writeD( character.getX() )
            .writeD( character.getY() )

            .writeD( character.getZ() )
            .writeD( character.getHeading() )
            .getBuffer()
}