import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function StopRotation( objectId: number, degree: number, speed: number ): Buffer {
    return new DeclaredServerPacket( 17 )
            .writeC( 0x61 )
            .writeD( objectId )
            .writeD( degree )
            .writeD( speed )

            .writeD( 0 )
            .getBuffer()
}