import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2GamePointProduct } from '../../../data/interface/GamePointsProductsDataApi'
import { L2GamePointPurchasesTableItem } from '../../../database/interface/GamePointPurchasesTableApi'

export function ExBrProductList( products: ReadonlyArray<L2GamePointProduct> ) : Buffer {
    let packet = new DeclaredServerPacket( 7 + products.length * 35 )
        .writeC( 0xFE )
        .writeH( 0xD6 )
        .writeD( products.length )

    for ( const product of products ) {
        packet
            .writeD( product.id )
            .writeH( product.category )
            .writeD( product.price )
            .writeD( product.type )

            .writeD( 0x12CEDE40 ) // startSale
            .writeD( 0x7ECE3CD0 ) // endSale
            .writeC( 0 ) // day
            .writeC( 0 ) // start hour

            .writeC( 0 ) // start minute
            .writeC( 0 ) // end hour
            .writeC( 0 ) // end minute
            .writeD( 0 ) // current amount sold

            .writeD( 0 ) // max amount available, if current and max are zero no count is shown
    }

    return packet.getBuffer()
}

/*
    While there can be many purchases in time attributed to account,
    only unique productId items are shown.
    TODO : only send unique products as history, perhaps generate packet and save it until history changes
 */
export function ExBrRecentProductList( purchases: ReadonlyArray<L2GamePointPurchasesTableItem> ) : Buffer {
    let packet = new DeclaredServerPacket( 7 + purchases.length * 35 )
        .writeC( 0xFE )
        .writeH( 0xDC )
        .writeD( purchases.length )

    for ( const product of purchases ) {
        packet
            .writeD( product.productId )
            .writeH( product.category )
            .writeD( product.paidPrice )
            .writeD( product.type )

            .writeD( 0x12CEDE40 ) // startSale
            .writeD( 0x7ECE3CD0 ) // endSale
            .writeC( 0 ) // day
            .writeC( 0 ) // start hour

            .writeC( 0 ) // start minute
            .writeC( 0 ) // end hour
            .writeC( 0 ) // end minute
            .writeD( 0 ) // current amount sold, not displayed

            .writeD( 0 ) // max amount available, not displayed
    }

    return packet.getBuffer()
}