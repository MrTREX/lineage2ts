import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function Revive( objectId: number ) : Buffer {
    return new DeclaredServerPacket( 5 )
        .writeC( 0x01 )
        .writeD( objectId )
        .getBuffer()
}