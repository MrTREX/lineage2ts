import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function SetSummonRemainTime( maxTime: number, remainingTime: number ) : Buffer {
    return new DeclaredServerPacket( 9 )
            .writeC( 0xD1 )
            .writeD( maxTime )
            .writeD( remainingTime )
            .getBuffer()
}