import { DeclaredServerPacket } from '../../../../packets/DeclaredServerPacket'
import { IBuilderPacket } from '../../IBuilderPacket'
import { ObjectPool } from '../../../helpers/ObjectPoolHelper'

interface SkillListItem {
    id: number
    level: number
    passive: boolean
    disabled: boolean
    enchanted: boolean
}

const SkillListCache = new ObjectPool( 'SkillListItem', () : SkillListItem => {
    return {
        disabled: false,
        enchanted: false,
        id: 0,
        level: 0,
        passive: false
    }
} )

export class SkillListBuilder implements IBuilderPacket {
    skills: Array<SkillListItem> = []

    addSkill( id: number, level: number, passive: boolean, disabled: boolean, enchanted: boolean ): void {
        let item = SkillListCache.getValue()

        item.id = id
        item.level = level
        item.passive = passive
        item.disabled = disabled
        item.enchanted = enchanted

        this.skills.push( item )
    }

    getBuffer(): Buffer {
        let size = this.skills.length
        let packet = new DeclaredServerPacket( 5 + size * 14 )
                .writeC( 0x5F )
                .writeD( size )

        this.skills.forEach( ( item: SkillListItem ) => {
            packet
                    .writeD( item.passive ? 1 : 0 )
                    .writeD( item.level )
                    .writeD( item.id )
                    .writeC( item.disabled ? 1 : 0 )
                    .writeC( item.enchanted ? 1 : 0 )

            SkillListCache.recycleValue( item )
        } )

        this.skills = null

        return packet.getBuffer()
    }
}