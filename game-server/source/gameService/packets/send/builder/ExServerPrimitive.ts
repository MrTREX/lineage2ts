import { IBuilderPacket } from '../../IBuilderPacket'
import { DeclaredServerPacket, getStringSize } from '../../../../packets/DeclaredServerPacket'
import { IServerPacket } from '../../../../packets/IServerPacket'
import { ObjectPool } from '../../../helpers/ObjectPoolHelper'

const objectPool = new ObjectPool( 'ExServerPrimitive', () : PrimitiveData => {
    return {
        color: 0,
        end: {
            x: 0,
            y: 0,
            z: 0
        },
        isNameColored: false,
        name: undefined,
        start: {
            x: 0,
            y: 0,
            z: 0
        },
        type: undefined
    }
} )

const enum PrimitiveType {
    Point = 1,
    Line = 2
}

interface BasePrimitive {
    type: PrimitiveType
    name: string
    color: number
    isNameColored: boolean
}

interface PointData {
    x: number
    y: number
    z: number
}

interface PrimitivePoint extends BasePrimitive {
    start: PointData
}

interface PrimitiveLine extends PrimitivePoint {
    end: PointData
}

type PrimitiveData = PrimitivePoint | PrimitiveLine

function writePointData( item: PrimitivePoint, packet: IServerPacket ) : void {
    packet
            .writeD( item.start.x )
            .writeD( item.start.y )
            .writeD( item.start.z )
}

function writeLineData( item: PrimitiveLine, packet: IServerPacket ) : void {
    packet
            .writeD( item.start.x )
            .writeD( item.start.y )
            .writeD( item.start.z )
            .writeD( item.end.x )

            .writeD( item.end.y )
            .writeD( item.end.z )
}

const colorConverter = Buffer.allocUnsafeSlow( 4 )
function writeCommonData( item: PrimitiveData, packet: IServerPacket ) : void {
    /*
        Color is represented as sRGB values, where each color value is 8-bit integer.
        In 32-bit value, top byte is used for alpha layer, then whole three R-G-B bytes.
        Only RGB values are used.

        While it is possible to use bit-shifting to achieve what Java variants are doing,
        using Node representation of number would need to be first cast into 32-bit equivalent,
        and only then perform bit shifting. Hence, it is easier to use Buffer (and not typed array)
        to take advantage of reading 8-bit values directly.
     */
    colorConverter.writeInt32LE( item.color )

    packet
            .writeC( item.type )
            .writeS( item.name )
            .writeD( colorConverter.readUInt8( 2 ) ) // R
            .writeD( colorConverter.readUInt8( 1 ) ) // G

            .writeD( colorConverter.readUInt8( 0 ) ) // B
            .writeD( item.isNameColored ? 1 : 0 )
}

function writeData( item: PrimitiveData, packet: IServerPacket ) : void {
    writeCommonData( item, packet )

    switch ( item.type ) {
        case PrimitiveType.Point:
            return writePointData( item as PrimitivePoint, packet )

        case PrimitiveType.Line:
            return writeLineData( item as PrimitiveLine, packet )
    }
}

function getItemSize( total: number, item: PrimitiveData ) : number {
    let typeSize = item.type === PrimitiveType.Point ? 12 : 24
    return total + getStringSize( item.name ) + 17 + typeSize
}

export class ExServerPrimitive implements IBuilderPacket {
    name: string
    x: number
    y: number
    z: number
    points: Array<PrimitiveData> = []
    lines: Array<PrimitiveData> = []

    constructor( name: string, x: number, y: number, z: number ) {
        this.name = name
        this.x = x
        this.y = y
        this.z = z
    }

    getBuffer(): Buffer {
        if ( this.points.length === 0 && this.lines.length === 0 ) {
            this.addPoint( '', 0xFFFFFF, false, this.x, this.y, 16384 )
        }

        let packet = new DeclaredServerPacket( this.getPacketSize() )
                .writeC( 0xFE )
                .writeH( 0x11 )
                .writeS( this.name )
                .writeD( this.x )

                .writeD( this.y )
                .writeD( this.z )
                .writeD( 0x7fffffff )
                .writeD( 0x7fffffff )

                .writeD( this.points.length + this.lines.length )

        this.points.forEach( point => writeData( point, packet ) )
        this.lines.forEach( line => writeData( line, packet ) )

        this.recycleData()
        return packet.getBuffer()
    }

    private getPacketSize() : number {
        let itemSize = this.points.reduce( getItemSize, 0 ) + this.lines.reduce( getItemSize, 0 )
        return getStringSize( this.name ) + 27 + itemSize
    }

    private recycleData() : void {
        this.points.forEach( ( item : PrimitiveData ) => {
            item.name = null
            objectPool.recycleValue( item )
        } )

        this.lines.forEach( ( item : PrimitiveData ) => {
            item.name = null
            objectPool.recycleValue( item )
        } )

        this.points.length = 0
        this.lines.length = 0
    }

    addPoint( name: string, color: number, isColored: boolean, x: number, y: number, z: number ) : void {
        let point = objectPool.getValue()

        point.name = name
        point.color = color
        point.isNameColored = isColored
        point.type = PrimitiveType.Point

        point.start.x = x
        point.start.y = y
        point.start.z = z

        this.points.push( point )
    }

    addLine( name: string, color: number, isColored: boolean, x: number, y: number, z: number, xDestination: number, yDestination: number, zDestination: number ) : void {
        let line = objectPool.getValue() as PrimitiveLine

        line.name = name
        line.color = color
        line.isNameColored = isColored
        line.type = PrimitiveType.Line

        line.start.x = x
        line.start.y = y
        line.start.z = z

        line.end.x = xDestination
        line.end.y = yDestination
        line.end.z = zDestination

        this.lines.push( line )
    }

    addSquare( name: string, color: number, isColored: boolean, x: number, y: number, z: number, size: number ) : void {
        this.addLine( name, color, isColored, x - size, y + size, z, x + size, y + size, z )
        this.addLine( name, color, isColored, x + size, y + size, z, x + size, y - size, z )
        this.addLine( name, color, isColored, x + size, y - size, z, x - size, y - size, z )
        this.addLine( name, color, isColored, x - size, y - size, z, x - size, y + size, z )
    }
}