import { IBuilderPacket } from '../../IBuilderPacket'
import { IServerPacket } from '../../../../packets/IServerPacket'
import { DeclaredServerPacket } from '../../../../packets/DeclaredServerPacket'
import { L2Character } from '../../../models/actor/L2Character'
import { ILocational } from '../../../models/Location'
import { ObjectPool } from '../../../helpers/ObjectPoolHelper'

const enum AttackEffect {
    Soulshot = 0x10,
    Critical = 0x20,
    ShieldBlock = 0x40,
    Miss = 0x80,
}


interface AttackHit {
    targetId: number
    damage: number
    flags: number
}

const objectPool = new ObjectPool( 'AttackHit', (): AttackHit => {
    return {
        damage: 0,
        flags: 0,
        targetId: 0,
    }
} )

export class AttackBuilder implements IBuilderPacket {
    attacker: L2Character
    target: L2Character
    useShots: boolean
    shotGrade: number
    hits: Array<AttackHit> = []

    constructor( attacker: L2Character, target: L2Character, useShots: boolean, shotGrade: number ) {
        this.attacker = attacker
        this.target = target
        this.useShots = useShots
        this.shotGrade = shotGrade
    }

    addHit( targetId: number, damage: number, isMiss: boolean, isCrit: boolean, useShield: number ): void {
        let attackHit = objectPool.getValue()

        attackHit.targetId = targetId
        attackHit.damage = damage
        attackHit.flags = 0

        if ( this.useShots ) {
            attackHit.flags = attackHit.flags | ( AttackEffect.Soulshot | this.shotGrade )
        }

        if ( isCrit ) {
            attackHit.flags = attackHit.flags | AttackEffect.Critical
        }

        if ( useShield > 0 ) {
            attackHit.flags = attackHit.flags | AttackEffect.ShieldBlock
        }

        if ( isMiss ) {
            attackHit.flags = attackHit.flags | AttackEffect.Miss
        }

        this.hits.push( attackHit )
    }

    writeHit( packet: IServerPacket, hit: AttackHit ): void {
        packet
                .writeD( hit.targetId )
                .writeD( hit.damage )
                .writeC( hit.flags )
    }

    writeLocation( packet: IServerPacket, object: ILocational ): void {
        packet
                .writeD( object.getX() )
                .writeD( object.getY() )
                .writeD( object.getZ() )
    }

    getBuffer(): Buffer {
        let packet = new DeclaredServerPacket( 31 + 9 * this.hits.length )
                .writeC( 0x33 )
                .writeD( this.attacker.getObjectId() )

        this.writeHit( packet, this.hits[ 0 ] )
        this.writeLocation( packet, this.attacker )

        packet.writeH( this.hits.length - 1 )

        let start = 1
        while ( start < this.hits.length ) {
            this.writeHit( packet, this.hits[ start ] )
            start++
        }

        this.writeLocation( packet, this.target )
        this.clear()

        return packet.getBuffer()
    }

    hasHits(): boolean {
        return this.hits.length > 0
    }

    hasSoulshot(): boolean {
        return this.useShots
    }

    clear(): void {
        objectPool.recycleValues( this.hits )
        this.hits.length = 0
        this.attacker = null
        this.target = null
    }
}