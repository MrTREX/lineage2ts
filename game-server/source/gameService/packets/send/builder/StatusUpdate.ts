import { DeclaredServerPacket, ReservedServerPacket } from '../../../../packets/DeclaredServerPacket'
import { IBuilderPacket } from '../../IBuilderPacket'
import { ObjectPool } from '../../../helpers/ObjectPoolHelper'
import { PacketHelper } from '../../PacketVariables'

export const enum StatusUpdateProperty {
    Level = 0x01,
    Exp = 0x02,
    STR = 0x03,
    DEX = 0x04,
    CON = 0x05,
    INT = 0x06,
    WIT = 0x07,
    MEN = 0x08,

    CurrentHP = 0x09,
    MaxHP = 0x0a,
    CurrentMP = 0x0b,
    MaxMP = 0x0c,

    SP = 0x0d,
    InventoryWeight = 0x0e,
    MaxInventoryWeight = 0x0f,

    PowerAttack = 0x11,
    AttackSpeed = 0x12,
    PowerDefence = 0x13,
    Evasion = 0x14,
    Accuracy = 0x15,
    Critical = 0x16,
    MagicAttack = 0x17,
    CastSpeed = 0x18,
    MagicDefence = 0x19,
    PvpFlagged = 0x1a,
    Karma = 0x1b,

    CurrentCP = 0x21,
    MaxCP = 0x22,
}

const HPObjectPool = new ObjectPool( 'StatusUpdateHP', () => {
    return new ReservedServerPacket( 25 ) // 9 + 8 * 2
} )

const AllPointsObjectPool = new ObjectPool( 'StatusUpdateAllPoints', () => {
    return new ReservedServerPacket( 57 ) // 9 + 8 * 6
} )

export class StatusUpdate implements IBuilderPacket {
    items: Map<number, number> = new Map<number, number>()
    objectId: number

    constructor( objectId: number ) {
        this.objectId = objectId
    }

    addAttribute( property: StatusUpdateProperty, value: number ): StatusUpdate {
        this.items.set( property, value )
        return this
    }

    hasAttributes(): boolean {
        return this.items.size > 0
    }

    getBuffer(): Buffer {
        let packet = new DeclaredServerPacket( 9 + this.items.size * 8 )
                .writeC( 0x18 )
                .writeD( this.objectId )
                .writeD( this.items.size )

        this.items.forEach( ( value: number, key: StatusUpdateProperty ) => {
            packet.writeD( key ).writeD( value )
        } )

        return packet.getBuffer()
    }

    static forValue( objectId: number, property: StatusUpdateProperty, value: number ): Buffer {
        return new DeclaredServerPacket( 17 )
                .writeC( 0x18 )
                .writeD( objectId )
                .writeD( 1 )
                .writeD( property )

                .writeD( value )
                .getBuffer()
    }
}

export function createStatusUpdateForHp( objectId: number, maxHp: number, currentHp: number ) : Buffer {
    let packet = HPObjectPool.getValue()

    let buffer = packet
        .writeC( 0x18 )
        .writeD( objectId )
        .writeD( 2 )
        .writeD( StatusUpdateProperty.MaxHP )

        .writeD( maxHp )
        .writeD( StatusUpdateProperty.CurrentHP )
        .writeD( currentHp )
        .getBuffer()

    packet.reset()
    HPObjectPool.recycleValue( packet )

    return PacketHelper.copyPacket( buffer )
}

export function createStatusUpdateForAllPoints(
    objectId: number,
    maxHp: number,
    currentHp: number,
    maxMp: number,
    currentMp: number,
    maxCp: number,
    currentCp: number ) : Buffer {

    let packet = AllPointsObjectPool.getValue()

    let buffer = packet
        .writeC( 0x18 )
        .writeD( objectId )
        .writeD( 6 )

        .writeD( StatusUpdateProperty.MaxHP )
        .writeD( maxHp )
        .writeD( StatusUpdateProperty.CurrentHP )
        .writeD( currentHp )

        .writeD( StatusUpdateProperty.MaxMP )
        .writeD( maxMp )
        .writeD( StatusUpdateProperty.CurrentMP )
        .writeD( currentMp )

        .writeD( StatusUpdateProperty.MaxCP )
        .writeD( maxCp )
        .writeD( StatusUpdateProperty.CurrentCP )
        .writeD( currentCp )

        .getBuffer()

    packet.reset()
    AllPointsObjectPool.recycleValue( packet )

    return PacketHelper.copyPacket( buffer )
}