import * as Buffer from 'buffer'
import { DeclaredServerPacket } from '../../../../packets/DeclaredServerPacket'
import { IBuilderPacket } from '../../IBuilderPacket'

type NpcRecord = [ number, number, number ]

export class ExQuestNpcLogList implements IBuilderPacket {
    questId: number
    records: Array<NpcRecord> = []

    constructor( id: number ) {
        this.questId = id
    }

    addRecord( npcId: number, value: number ): void {
        this.records.push( [ npcId + 1000000, 0, value ] )
    }

    getBuffer(): Buffer {
        let packet = new DeclaredServerPacket( 8 + this.records.length * 9 )
                .writeC( 0xFE )
                .writeH( 0xC5 )
                .writeD( this.questId )
                .writeC( this.records.length )

        this.records.forEach( ( record: NpcRecord ) => {
            packet
                    .writeD( record[ 0 ] )
                    .writeC( record[ 1 ] )
                    .writeD( record[ 2 ] )
        } )

        return packet.getBuffer()
    }
}