import { AcquireSkillType } from '../../../enums/AcquireSkillType'
import { DeclaredServerPacket } from '../../../../packets/DeclaredServerPacket'
import { IBuilderPacket } from '../../IBuilderPacket'

interface SkillData {
    id: number
    level: number
    spCost: number
    socialClassId: number
}

export class AcquireSkillList implements IBuilderPacket {
    type: AcquireSkillType
    skills: Array<SkillData> = []

    constructor( type: AcquireSkillType ) {
        this.type = type
    }

    addSkill( id: number, level: number, spCost: number, socialClassId: number ): void {
        this.skills.push( {
            id,
            level,
            spCost,
            socialClassId,
        } )
    }

    getCount(): number {
        return this.skills.length
    }

    getBuffer(): Buffer {
        let isSubPledge: boolean = this.type === AcquireSkillType.SubPledge
        let packet = new DeclaredServerPacket( 9 + this.skills.length * ( isSubPledge ? 24 : 20 ) )
                .writeC( 0x90 )
                .writeD( this.type )
                .writeD( this.skills.length )

        this.skills.forEach( ( data: SkillData ) => {
            packet
                    .writeD( data.id )
                    .writeD( data.level ) // next level
                    .writeD( data.level ) // maximum level
                    .writeD( data.spCost )
                    .writeD( data.socialClassId )

            if ( isSubPledge ) {
                packet.writeD( 0 )
            }
        } )

        return packet.getBuffer()
    }
}