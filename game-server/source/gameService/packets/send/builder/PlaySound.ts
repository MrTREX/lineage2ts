import { L2Object } from '../../../models/L2Object'
import { DeclaredServerPacket, getStringSize } from '../../../../packets/DeclaredServerPacket'
import { IBuilderPacket } from '../../IBuilderPacket'

export const enum PlaySoundType {
    Sound = 0,
    Music = 1,
    Voice = 2
}

// TODO : introduce either object pool or packet caching based on default parameters

export class PlaySound implements IBuilderPacket {
    protected type: PlaySoundType = PlaySoundType.Sound
    private soundFilename: string
    private bindToObject: number
    private objectId: number
    private locationX: number
    private locationY: number
    private locationZ: number
    private delay: number

    constructor( soundName: string, object: L2Object = null, delay: number = 0 ) {
        this.soundFilename = soundName

        if ( object ) {
            this.bindToObject = 1
            this.objectId = object.getObjectId()
            this.locationX = object.getX()
            this.locationY = object.getY()
            this.locationZ = object.getZ()
        } else {
            this.bindToObject = 0
            this.objectId = 0
            this.locationX = 0
            this.locationY = 0
            this.locationZ = 0
        }

        this.delay = delay
    }

    static createSound( soundName: string, object: L2Object ) : Buffer {
        let sound = new PlaySound( soundName, object )
        return sound.getBuffer()
    }

    getBuffer() : Buffer {
        return new DeclaredServerPacket( 29 + getStringSize( this.soundFilename ) )
            .writeC( 0x9E )
            .writeD( this.type )
            .writeS( this.soundFilename )
            .writeD( this.bindToObject )

            .writeD( this.objectId )
            .writeD( this.locationX )
            .writeD( this.locationY )
            .writeD( this.locationZ )

            .writeD( this.delay )
            .getBuffer()
    }
}

export class PlayMusic extends PlaySound {
    protected type: PlaySoundType = PlaySoundType.Music
}

export class PlayVoice extends PlaySound {
    protected type: PlaySoundType = PlaySoundType.Voice
}