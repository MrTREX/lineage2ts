import { L2Character } from '../../../models/actor/L2Character'
import { BuffInfo } from '../../../models/skills/BuffInfo'
import { DeclaredServerPacket } from '../../../../packets/DeclaredServerPacket'
import { IBuilderPacket } from '../../IBuilderPacket'
import { BuffDefinition } from '../../../models/skills/BuffDefinition'

const enum PartyMemberType {
    Player,
    Pet,
    Servitor
}

export class PartySpelled implements IBuilderPacket {
    objectId: number
    effects: Array<BuffDefinition> = []
    type: PartyMemberType

    constructor( character: L2Character ) {
        this.objectId = character.getObjectId()
        this.type = this.getCharacterType( character )
    }

    addSkill( info: BuffInfo ) {
        if ( info && info.isInUse ) {
            this.effects.push( info.getDefinition() )
        }
    }

    getBuffer(): Buffer {
        let packet = new DeclaredServerPacket( 13 + this.effects.length * 10 )
                .writeC( 0xF4 )
                .writeD( this.type )
                .writeD( this.objectId )
                .writeD( this.effects.length )

        this.effects.forEach( ( buff: BuffDefinition ) => {
            packet
                    .writeD( buff.skill.getDisplayId() )
                    .writeH( buff.skill.getDisplayLevel() )
                    .writeD( buff.timeLeftSeconds )
        } )

        return packet.getBuffer()
    }

    private getCharacterType( character: L2Character ) : PartyMemberType {
        if ( character.isPlayer() ) {
            return PartyMemberType.Player
        }

        if ( character.isPet() ) {
            return PartyMemberType.Pet
        }

        return PartyMemberType.Servitor
    }
}