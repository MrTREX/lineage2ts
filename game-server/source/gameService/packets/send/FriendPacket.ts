import { DeclaredServerPacket, EmptyString, EmptyStringSize, getStringSize } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

const enum FriendOperation {
    Add = 1,
    Remove = 3
}

export function FriendAdd( player: L2PcInstance ) : Buffer {
    return new DeclaredServerPacket( 17 + getStringSize( player.getName() ) )
            .writeC( 0x76 )
            .writeD( FriendOperation.Add )
            .writeD( player.getObjectId() )
            .writeS( player.getName() )

            .writeD( player ? 1 : 0 )
            .writeD( player ? player.getObjectId() : 0 )
            .getBuffer()
}

export function FriendRemove( id : number ) : Buffer {
    return new DeclaredServerPacket( 17 + EmptyStringSize )
            .writeC( 0x76 )
            .writeD( FriendOperation.Remove )
            .writeD( id )
            .writeS( EmptyString )

            .writeD( 0 )
            .writeD( 0 )
            .getBuffer()
}