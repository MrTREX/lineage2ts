import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExNevitAdventEffect( time: number ) : Buffer {
    return new DeclaredServerPacket( 6 )
            .writeC( 0xFE )
            .writeC( 0xE0 )
            .writeD( time )
            .getBuffer()
}