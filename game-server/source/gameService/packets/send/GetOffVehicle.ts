import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function GetOffVehicle( objectId: number, boatId: number, x: number, y: number, z: number ) : Buffer {
    return new DeclaredServerPacket( 21 )
            .writeC( 0x6F )
            .writeD( objectId )
            .writeD( boatId )
            .writeD( x )

            .writeD( y )
            .writeD( z )
            .getBuffer()
}