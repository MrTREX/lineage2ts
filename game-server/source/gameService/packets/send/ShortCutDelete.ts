import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ShortCutDelete( slot: number ) : Buffer {
    return new DeclaredServerPacket( 9 )
        .writeC( 0x46 )
        .writeD( slot )
        .writeD( 0x00 )
        .getBuffer()
}