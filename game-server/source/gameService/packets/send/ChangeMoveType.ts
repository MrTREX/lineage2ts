import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ChangeMoveType( objectId: number, isRunning: boolean ) : Buffer {
    return new DeclaredServerPacket( 13 )
            .writeC( 0x28 )
            .writeD( objectId )
            .writeD( isRunning ? 1 : 0 )
            .writeD( 0 )
            .getBuffer()
}