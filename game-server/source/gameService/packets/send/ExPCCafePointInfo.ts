import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export const enum ExPCCafePointType {
    NotDisplayed = 0,
    Acquisition,
    ReadyToUsePoints
}

export const enum ExPCCafePointColor {
    Yellow = 0,
    CyanBlue = 1,
    Red = 2,
    Black = 3
}

export function ExPCCafePointInfo(
    currentPoints: number,
    periodType: ExPCCafePointType = ExPCCafePointType.Acquisition,
    pointChange: number = 0,
    hoursLeft: number = 42,
    color : ExPCCafePointColor = undefined ) : Buffer {
    if ( !Number.isInteger( color ) ) {
        color = pointChange < 0 ? ExPCCafePointColor.Black : ExPCCafePointColor.Yellow
    }

    return new DeclaredServerPacket( 21 )
        .writeC( 0xFE )
        .writeH( 0x32 )
        .writeD( currentPoints )
        .writeD( pointChange )

        .writeC( periodType )
        .writeD( hoursLeft )
        .writeC( color )
        .writeD( 0 ) // value is in seconds * 3

        .getBuffer()
}