import { DataManager } from '../../../data/manager'
import { CharacterInfoPackage, PaperdollDetailItem } from '../../models/CharacterInfoPackage'
import { ConfigManager } from '../../../config/ConfigManager'
import { InventorySlot, PaperdollOrder } from '../../values/InventoryValues'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

function getEnchantEffect( paperdoll: Array<PaperdollDetailItem> ): number {
    let rightHand = paperdoll[ InventorySlot.RightHand ].enchantLevel
    let leftHand = paperdoll[ InventorySlot.LeftHand ].enchantLevel

    return rightHand > 0 ? rightHand : leftHand
}

function getCharactersSize( characters: Array<CharacterInfoPackage>, loginNameSize: number ): number {
    let paperdollSize = PaperdollOrder.length * 4
    return characters.reduce( ( size: number, character: CharacterInfoPackage ): number => {
        return size + loginNameSize + getStringSize( character.name ) + 209 + paperdollSize
    }, 0 )
}

export function CharacterSelectionInfo( loginName: string,
        sessionKey: number,
        characters: Array<CharacterInfoPackage> ): Buffer {
    let activeCharacterIndex = 0
    let packet = new DeclaredServerPacket( 10 + getCharactersSize( characters, getStringSize( loginName ) ) )
            .writeC( 0x09 )
            .writeD( characters.length )
            .writeD( Math.min( 7, ConfigManager.character.getCharMaxNumber() ) )
            .writeC( 0x00 )

    let lastAccess = 0
    characters.forEach( ( currentCharacter: CharacterInfoPackage, index: number ) => {
        if ( lastAccess < currentCharacter.lastAccess ) {
            lastAccess = currentCharacter.lastAccess
            activeCharacterIndex = index
        }
    } )

    characters.forEach( ( currentCharacter: CharacterInfoPackage, index: number ) => {
        packet.writeS( currentCharacter.name )
                .writeD( currentCharacter.objectId )
                .writeS( loginName )

                .writeD( sessionKey )
                .writeD( currentCharacter.clanId )
                .writeD( 0x00 )

                .writeD( currentCharacter.sex )
                .writeD( currentCharacter.race )
                .writeD( currentCharacter.baseClassId )
                .writeD( 0x01 )

                .writeD( currentCharacter.x )
                .writeD( currentCharacter.y )
                .writeD( currentCharacter.z )

                .writeF( currentCharacter.currentHp )
                .writeF( currentCharacter.currentMp )

                .writeD( currentCharacter.sp )
                .writeQ( currentCharacter.exp )
                .writeF( DataManager.getExperienceData().getPercentFromCurrentLevel( currentCharacter.exp, currentCharacter.level ) )

                .writeD( currentCharacter.level )
                .writeD( currentCharacter.karma )
                .writeD( currentCharacter.pkKills )
                .writeD( currentCharacter.pvpKills )

                .writeD( 0 )
                .writeD( 0 )
                .writeD( 0 )
                .writeD( 0 )

                .writeD( 0 )
                .writeD( 0 )
                .writeD( 0 )

        PaperdollOrder.forEach( ( currentSlot ) => {
            packet.writeD( currentCharacter.paperdoll[ currentSlot ].itemId )
        } )

        packet.writeD( currentCharacter.hairStyle )
                .writeD( currentCharacter.hairColor )
                .writeD( currentCharacter.face )

                .writeF( currentCharacter.maxHp )
                .writeF( currentCharacter.maxMp )
                .writeD( currentCharacter.deleteSecondsLeft )

                .writeD( currentCharacter.classId )
                .writeD( activeCharacterIndex === index ? 0x01 : 0x00 )
                .writeC( Math.min( getEnchantEffect( currentCharacter.paperdoll ), 127 ) )

                .writeD( currentCharacter.augmentationId )
                .writeD( 0 )

                /*
                Pet data according to L2J:
                - pet id
                - pet level
                - pet max food
                - pet current food
                - pet max hp
                - pet max mp
                 */
                .writeD( 0 )
                .writeD( 0 )
                .writeD( 0 )
                .writeD( 0 )
                .writeF( 0 )
                .writeF( 0 )

                .writeD( currentCharacter.vitalityPoints )
    } )

    return packet.getBuffer()
}