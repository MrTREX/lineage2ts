import { L2Character } from '../../models/actor/L2Character'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

/*
    When player movement occurs via click, we want to trick client to move to specific point.
    However, in client character would stop at edge of blue click animation, which needs
    to be accounted on server level. Hence, actual destination may not be the same as client has requested.
    (player location) ------------------------------(client stop)-(click destination)
 */
export function MoveToLocation( character: L2Character, x: number, y: number, z: number ) : Buffer {
    return new DeclaredServerPacket( 29 )
        .writeC( 0x2F )
        .writeD( character.getObjectId() )
        .writeD( x )
        .writeD( y )

        .writeD( z )
        .writeD( character.getX() )
        .writeD( character.getY() )
        .writeD( character.getZ() )

        .getBuffer()
}

export function MoveToLocationWithCharacter( character: L2Character ) : Buffer {
    return MoveToLocation( character, character.getXDestination(), character.getYDestination(), character.getZDestination() )
}