import { L2Npc } from '../../models/actor/L2Npc'
import { L2World } from '../../L2World'
import { ConfigManager } from '../../../config/ConfigManager'
import { L2Clan } from '../../models/L2Clan'
import { AbnormalVisualEffect, AbnormalVisualEffectMap } from '../../models/skills/AbnormalVisualEffect'
import { ClanCache } from '../../cache/ClanCache'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { AreaCache } from '../../cache/AreaCache'
import { TownArea } from '../../models/areas/type/Town'
import { AreaType } from '../../models/areas/AreaType'
import { Team } from '../../enums/Team'

const stealthEffect = AbnormalVisualEffectMap[ AbnormalVisualEffect.STEALTH ].mask

export function NpcInfo( npcObjectId: number, playerId: number ): Buffer {
    let npc: L2Npc = L2World.getObjectById( npcObjectId ) as L2Npc
    let player = L2World.getPlayer( playerId )

    return NpcInfoWithCharacters( npc, player )
}

export function NpcInfoWithCharacters( character: L2Npc, player: L2PcInstance ): Buffer {
    let isSummoned = character.isShowSummonAnimation()
    let isAttackable: boolean = character.isAutoAttackable( player )
    let collisionHeight = character.getCollisionHeight()
    let collisionRadius = character.getCollisionRadius()
    let name = character.getTemplate().isUsingServerSideName() ? character.getTemplate().getName() : ''

    let moveMultiplier = character.getMovementSpeedMultiplier()
    let runSpeed = Math.round( character.getRunSpeed() / moveMultiplier )
    let walkSpeed = Math.round( character.getWalkSpeed() / moveMultiplier )
    let swimRunSpeed = Math.round( character.getSwimRunSpeed() / moveMultiplier )

    let swimWalkSpeed = Math.round( character.getSwimWalkSpeed() / moveMultiplier )
    let flyRunSpeed = character.isFlying() ? runSpeed : 0
    let flyWalkSpeed = character.isFlying() ? walkSpeed : 0
    let enchantEffect = character.getEnchantEffect()
    let title = character.getGeneratedTitle()

    let clanCrest = 0
    let clanId = 0
    let allyCrest = 0
    let allyId = 0

    /*
        TODO : extract all four parameters into separate cache
        - use castle owner change events to assign clan parameters
        - cache by looking up objectId and castle residence id
     */
    let castle = character.getCastle()
    if ( castle
            && character.isInArea( AreaType.Town )
            && ( ConfigManager.npc.showCrestWithoutQuest() || castle.getShowNpcCrest() )
            && castle.getOwnerId() !== 0 ) {

        // TODO : avoid dynamic lookups, either cache townId or zone directly
        let area = AreaCache.getFirstAreaForType( character, AreaType.Town ) as TownArea
        if ( area ) {
            let townId = area.properties.townId
            if ( townId !== 33 && townId !== 22 ) {
                let clan: L2Clan = ClanCache.getClan( castle.getOwnerId() )

                if ( clan ) {
                    clanCrest = clan.getCrestId()
                    clanId = clan.getId()
                    allyCrest = clan.getAllyCrestId()
                    allyId = clan.getAllyId()
                }
            }
        }
    }

    return new DeclaredServerPacket( 206 + getStringSize( name ) + getStringSize( title ) )
            .writeC( 0x0c )
            .writeD( character.getObjectId() )
            .writeD( character.getTemplate().infoId )
            .writeD( isAttackable ? 1 : 0 )

            .writeD( character.getX() )
            .writeD( character.getY() )
            .writeD( character.getZ() )
            .writeD( character.getHeading() )

            .writeD( 0x00 )
            .writeD( character.getMagicAttackSpeed() )
            .writeD( character.getPowerAttackSpeed() )
            .writeD( runSpeed )

            .writeD( walkSpeed )
            .writeD( swimRunSpeed )
            .writeD( swimWalkSpeed )
            .writeD( flyRunSpeed )

            .writeD( flyWalkSpeed )
            .writeD( flyRunSpeed )
            .writeD( flyWalkSpeed )
            .writeF( moveMultiplier )

            .writeF( character.getAttackSpeedMultiplier() )
            .writeF( collisionRadius )
            .writeF( collisionHeight )
            .writeD( character.getRightHandItemId() )

            .writeD( 0 ) // chest armor
            .writeD( character.getLeftHandItemId() )
            .writeC( 1 ) // name above character
            .writeC( character.isRunning() ? 1 : 0 )

            .writeC( character.isInCombat() ? 1 : 0 )
            .writeC( character.isAlikeDead() ? 1 : 0 )
            .writeC( isSummoned ? 2 : 0 )
            .writeD( -1 )

            .writeS( name )
            .writeD( -1 )
            .writeS( title )
            .writeD( 0x00 ) // title color

            .writeD( 0x00 ) // pvp flag
            .writeD( 0x00 ) // karma value
            .writeD( character.isInvisible() ? ( character.getAbnormalVisualEffects() | stealthEffect ) : character.getAbnormalVisualEffects() )
            .writeD( clanId )

            .writeD( clanCrest )
            .writeD( allyId )
            .writeD( allyCrest )
            .writeC( character.getMovementType() )

            .writeC( Team.None )
            .writeF( collisionRadius )
            .writeF( collisionHeight )
            .writeD( enchantEffect )

            .writeD( character.isFlying() ? 1 : 0 )
            .writeD( 0x00 )
            .writeD( character.getColorEffect() )
            .writeC( character.isTargetable() ? 0x01 : 0x00 )

            .writeC( character.isShowName() ? 0x01 : 0x00 )
            .writeD( character.getAbnormalVisualEffectSpecial() )
            .writeD( character.getDisplayEffect() )
            .getBuffer()
}