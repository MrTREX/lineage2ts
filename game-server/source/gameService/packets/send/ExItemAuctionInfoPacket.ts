import { ItemAuction } from '../../models/auction/ItemAuction'
import { ItemAuctionState } from '../../models/auction/ItemAuctionState'
import { ItemPacketHelper } from './helpers/ItemPacketHelper'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { PacketHelper } from '../PacketVariables'

export function ExItemAuctionInfoPacket( shouldRefresh: boolean, auction: ItemAuction, nextAuction: ItemAuction ): Buffer {
    let timeRemaining: number = auction.getAuctionState() === ItemAuctionState.STARTED ? auction.getFinishTimeRemaining() / 1000 : 0
    let highestBid = auction.getHighestBid() ? auction.getHighestBid().lastBid : auction.getAuctionInitialBid()

    if ( !auction.itemInfoBuffer ) {
        let auctionItemData = new DeclaredServerPacket( ItemPacketHelper.writeItemSize() )
        ItemPacketHelper.writeGeneralItem( auctionItemData, auction.getItemInfo() )

        auction.itemInfoBuffer = PacketHelper.preservePacket( auctionItemData.getBuffer() )
    }

    if ( nextAuction && !nextAuction.itemInfoBuffer ) {
        let nextAuctionItemData = new DeclaredServerPacket( ItemPacketHelper.writeItemSize() )
        ItemPacketHelper.writeGeneralItem( nextAuctionItemData, nextAuction.getItemInfo() )

        nextAuction.itemInfoBuffer = PacketHelper.preservePacket( nextAuctionItemData.getBuffer() )
    }

    let nextAuctionSize = nextAuction ? ( nextAuction.itemInfoBuffer.length + 12 ) : 0
    let packet = new DeclaredServerPacket( 20 + auction.itemInfoBuffer.length + nextAuctionSize )
            .writeC( 0xFE )
            .writeH( 0x68 )
            .writeC( shouldRefresh ? 0 : 1 )
            .writeD( auction.getNpcId() )

            .writeQ( highestBid )
            .writeD( timeRemaining )
            .writeB( auction.itemInfoBuffer )

    if ( nextAuction ) {
        packet
                .writeQ( nextAuction.getAuctionInitialBid() )
                .writeD( nextAuction.getStartingTime() / 1000 )
                .writeB( nextAuction.itemInfoBuffer )
    }

    return packet.getBuffer()
}