import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

export function ExPrivateStoreSetWholesaleMessage( player : L2PcInstance, message: string = player.getSellList().getTitle() ): Buffer {
    return new DeclaredServerPacket( 7 + getStringSize( message ) )
            .writeC( 0xFE )
            .writeH( 0x80 )
            .writeD( player.getObjectId() )
            .writeS( message )
            .getBuffer()
}