import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { PacketHelper } from '../PacketVariables'

const staticPacketTrue = PacketHelper.preservePacket( new DeclaredServerPacket( 5 )
        .writeC( 0x71 )
        .writeD( 1 )
        .getBuffer() )

const staticPacketFalse = PacketHelper.preservePacket( new DeclaredServerPacket( 5 )
        .writeC( 0x71 )
        .writeD( 0 )
        .getBuffer() )

export function RestartResponse( value: boolean ): Buffer {
    return PacketHelper.copyPacket( value ? staticPacketTrue : staticPacketFalse )
}