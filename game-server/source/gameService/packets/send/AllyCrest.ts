import { CrestCache } from '../../cache/CrestCache'
import { L2Crest } from '../../models/L2Crest'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function AllyCrest( crestId: number ) : Buffer {
    let crest : L2Crest = CrestCache.getCrest( crestId )
    let crestSize : number = crest && crest.getData() ? crest.getData().length : 0

    let packet = new DeclaredServerPacket( 9 + crestSize )
            .writeC( 0xAF )
            .writeD( crestId )
            .writeD( crestSize )

    if ( crestSize > 0 ) {
        packet.writeB( crest.getData() )
    }

    return packet.getBuffer()
}