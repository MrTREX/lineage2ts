import { MailMessage } from '../../models/mail/MailMessage'
import { MailManager } from '../../instancemanager/MailManager'
import { CharacterNamesCache } from '../../cache/CharacterNamesCache'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'
import { ConfigManager } from '../../../config/ConfigManager'
import { L2MessagesTableItemStatus } from '../../../database/interface/MessagesTableApi'
import { MailMessageStatus } from '../../enums/MailMessageStatus'

export function ExShowSentPostList( objectId: number ): Buffer {
    let messages: Array<MailMessage> = MailManager.getOutbox( objectId )
    let messagesSize: number = messages.reduce( ( size: number, message: MailMessage ): number => {
        let name = CharacterNamesCache.getCachedNameById( message.getReceiverId() ) ?? ConfigManager.general.getMailSystemName()
        return size + getStringSize( message.getSubject() ) + getStringSize( name ) + 24
    }, 0 )

    let packet = new DeclaredServerPacket( 11 + messagesSize )
            .writeC( 0xFE )
            .writeH( 0xAC )
            .writeD( Date.now() / 1000 )
            .writeD( messages.length )

    messages.forEach( ( message: MailMessage ) => {
        packet
                .writeD( message.getId() )
                .writeS( message.getSubject() )
                .writeS( CharacterNamesCache.getCachedNameById( message.getReceiverId() ) ?? ConfigManager.general.getMailSystemName() )
                .writeD( message.isCOD() ? 0x01 : 0x00 )

                .writeD( message.getExpirationSeconds() )
                .writeD( message.getStatus() === L2MessagesTableItemStatus.Unread ? MailMessageStatus.Unread : MailMessageStatus.Read )
                .writeD( 0x01 )
                .writeD( message.hasAttachments() ? 0x01 : 0x00 )
    } )

    return packet.getBuffer()
}