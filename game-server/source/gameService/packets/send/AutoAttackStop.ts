import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function AutoAttackStop( objectId: number ) : Buffer {
    return new DeclaredServerPacket( 5 )
            .writeC( 0x26 )
            .writeD( objectId )
            .getBuffer()
}