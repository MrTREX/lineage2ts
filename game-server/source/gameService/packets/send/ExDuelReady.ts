import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { PacketHelper } from '../PacketVariables'

const staticPlayerDuel = PacketHelper.preservePacket( new DeclaredServerPacket( 7 )
        .writeC( 0xFE )
        .writeH( 0x4D )
        .writeD( 0 )
        .getBuffer() )

const staticPartyDuel = PacketHelper.preservePacket( new DeclaredServerPacket( 7 )
        .writeC( 0xFE )
        .writeH( 0x4D )
        .writeD( 1 )
        .getBuffer() )

export function ExDuelReady( isPartyDuel: boolean ): Buffer {
    return PacketHelper.copyPacket( isPartyDuel ? staticPartyDuel : staticPlayerDuel )
}