import { L2DoorInstance } from '../../models/actor/instance/L2DoorInstance'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function OnEventTrigger( door: L2DoorInstance, isEnabled: boolean ) : Buffer {
    return new DeclaredServerPacket( 6 )
            .writeC( 0xCF )
            .writeD( door.getEmitter() )
            .writeC( isEnabled ? 1 : 0 )
            .getBuffer()
}