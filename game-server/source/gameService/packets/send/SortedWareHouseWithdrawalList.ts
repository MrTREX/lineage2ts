import { WarehouseListType } from '../../values/WarehouseListType'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { ItemType2 } from '../../enums/items/ItemType2'
import { EtcItemType } from '../../enums/items/EtcItemType'
import { WarehouseWithdrawSort } from '../../values/SortedWareHouseWithdrawalListValues'
import { CrystalType } from '../../models/items/type/CrystalType'
import { DataManager } from '../../../data/manager'
import _ from 'lodash'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { WarehouseDepositType } from '../../values/warehouseDepositType'
import { ItemContainer } from '../../models/itemcontainer/ItemContainer'
import { L2RecipeDataItem } from '../../../data/interface/RecipeDataApi'

const maximumItems = 300

export function SortedWareHouseWithdrawalList( warehouse: ItemContainer, adena: number, type: WarehouseDepositType, itemType: WarehouseListType, order: number ): Buffer {
    let availableItems = warehouse.getItems().filter( getFilterMethod( itemType ) )
    let items: Array<L2ItemInstance> = sortItems( _.take( availableItems, maximumItems ), itemType, order )
    let dynamicSize: number = items.reduce( ( total: number, item: L2ItemInstance ): number => {
        return total + item.getEnchantOptions().length * 2 + 66
    }, 0 )

    let packet = new DeclaredServerPacket( 13 + dynamicSize )
            .writeC( 0x42 )
            .writeH( type ) // 0x01-Private Warehouse 0x02-Clan Warehouse 0x03-Castle Warehouse 0x04-Warehouse
            .writeQ( adena )
            .writeH( items.length )

    items.forEach( ( item: L2ItemInstance ) => {
        packet
                .writeD( item.getObjectId() )
                .writeD( item.getId() )
                .writeD( item.getLocationSlot() )
                .writeQ( item.getCount() )

                .writeH( item.getItem().getType2() )
                .writeH( item.getCustomType1() )
                .writeH( 0x00 ) // Can't be equipped in WH
                .writeD( item.getItem().getBodyPart() )

                .writeH( item.getEnchantLevel() )
                .writeH( item.getCustomType2() )
                .writeD( item.isAugmented() ? item.getAugmentation().getAugmentationId() : 0 )
                .writeD( item.getMana() )

                .writeD( item.getTime() )
                .writeH( item.getAttackElementType() )
                .writeH( item.getAttackElementPower() )

        for ( const value of item.elementalDefenceAttributes ) {
            packet.writeH( value )
        }

        item.getEnchantOptions().forEach( ( option: number ) => {
            packet.writeH( option )
        } )

        packet.writeD( item.getObjectId() )
    } )

    return packet.getBuffer()
}

function isWeapon( item: L2ItemInstance ): boolean {
    return item.isWeapon()
            || ( item.getItem().getType2() === ItemType2.WEAPON )
            || ( item.isEtcItem() && item.getItemType() === EtcItemType.ARROW )
            || ( item.getItem().getType2() === ItemType2.MONEY )
}

function isArmor( item: L2ItemInstance ): boolean {
    return item.isArmor() || item.getItem().getType2() === ItemType2.MONEY
}

function isEtcItem( item: L2ItemInstance ): boolean {
    return item.isEtcItem() || item.getItem().getType2() === ItemType2.MONEY
}

function isMaterial( item: L2ItemInstance ): boolean {
    return ( item.isEtcItem() && ( item.getEtcItem().getItemType() === EtcItemType.MATERIAL ) )
            || item.getItem().getType2() === ItemType2.MONEY
}

function isRecipe( item: L2ItemInstance ): boolean {
    return ( item.isEtcItem() && ( item.getEtcItem().getItemType() === EtcItemType.RECIPE ) )
            || item.getItem().getType2() === ItemType2.MONEY
}

function isAmulet( item: L2ItemInstance ): boolean {
    return ( item.isEtcItem() && item.getEtcItem().getItemType() === EtcItemType.Amulet )
            || ( item.getItem().getType2() === ItemType2.MONEY )
}

function isSpellbook( item: L2ItemInstance ): boolean {
    return ( item.isEtcItem() && item.getEtcItem().getItemType() === EtcItemType.Spellbook )
            || ( item.getItem().getType2() === ItemType2.MONEY )
}

function isConsumable( item: L2ItemInstance ): boolean {
    return ( item.isEtcItem() && ( ( item.getEtcItem().getItemType() === EtcItemType.SCROLL )
                    || ( item.getEtcItem().getItemType() === EtcItemType.SHOT ) ) )
            || item.getItem().getType2() === ItemType2.MONEY
}

function isShot( item: L2ItemInstance ): boolean {
    return ( item.isEtcItem() && ( item.getEtcItem().getItemType() === EtcItemType.SHOT ) )
            || item.getItem().getType2() === ItemType2.MONEY
}

function isScroll( item: L2ItemInstance ): boolean {
    return ( item.isEtcItem() && ( item.getEtcItem().getItemType() === EtcItemType.SCROLL ) )
            || item.getItem().getType2() === ItemType2.MONEY
}

function isSeed( item: L2ItemInstance ): boolean {
    return ( item.isEtcItem() && ( item.getEtcItem().getItemType() === EtcItemType.SEED ) )
            || item.getItem().getType2() === ItemType2.MONEY
}

function isOtherItem( item: L2ItemInstance ): boolean {
    return item.isEtcItem()
            && ( ( item.getEtcItem().getItemType() !== EtcItemType.MATERIAL )
                    && ( item.getEtcItem().getItemType() !== EtcItemType.RECIPE )
                    && ( item.getEtcItem().getItemType() !== EtcItemType.SCROLL )
                    && ( item.getEtcItem().getItemType() !== EtcItemType.SHOT ) )
}

function getFilterMethod( itemType: WarehouseListType ): ( L2ItemInstance ) => boolean {
    switch ( itemType ) {
        case WarehouseListType.WEAPON:
            return isWeapon
        case WarehouseListType.ARMOR:
            return isArmor
        case WarehouseListType.ETCITEM:
            return isEtcItem
        case WarehouseListType.MATERIAL:
            return isMaterial
        case WarehouseListType.RECIPE:
            return isRecipe
        case WarehouseListType.AMULETT:
            return isAmulet
        case WarehouseListType.SPELLBOOK:
            return isSpellbook
        case WarehouseListType.CONSUMABLE:
            return isConsumable
        case WarehouseListType.SHOT:
            return isShot
        case WarehouseListType.SCROLL:
            return isScroll
        case WarehouseListType.SEED:
            return isSeed
        case WarehouseListType.OTHER:
            return isOtherItem
        default:
            return _.constant( true )
    }
}

function sortItems( items: Array<L2ItemInstance>, itemType: WarehouseListType, order: number ): Array<L2ItemInstance> {
    getSortMethods( itemType, order ).forEach( ( method: ( one: L2ItemInstance, two: L2ItemInstance ) => number ) => {
        items.sort( method )
    } )

    return items
}

function getSortMethods( itemType: WarehouseListType, order: number ): Array<Function> {
    switch ( order ) {
        case WarehouseWithdrawSort.A2Z:
        case WarehouseWithdrawSort.Z2A:
            return [ WarehouseItemNameSort.bind( null, order ) ]

        case WarehouseWithdrawSort.GRADE:
            if ( ( itemType === WarehouseListType.ARMOR ) || ( itemType === WarehouseListType.WEAPON ) ) {
                return [
                    WarehouseItemNameSort.bind( null, WarehouseWithdrawSort.A2Z ),
                    WarehouseItemGradeSort.bind( null, WarehouseWithdrawSort.A2Z ),
                ]
            }
            break

        case WarehouseWithdrawSort.LEVEL:
            if ( itemType === WarehouseListType.RECIPE ) {
                return [
                    WarehouseItemNameSort.bind( null, WarehouseWithdrawSort.A2Z ),
                    WarehouseItemRecipeSort.bind( null, WarehouseWithdrawSort.A2Z ),
                ]
            }
            break

        case WarehouseWithdrawSort.TYPE:
            if ( itemType === WarehouseListType.MATERIAL ) {
                return [
                    WarehouseItemNameSort.bind( null, WarehouseWithdrawSort.A2Z ),
                    WarehouseItemTypeSort.bind( null, WarehouseWithdrawSort.A2Z ),
                ]
            }
            break

        case WarehouseWithdrawSort.WEAR:
            if ( itemType === WarehouseListType.ARMOR ) {
                return [
                    WarehouseItemNameSort.bind( null, WarehouseWithdrawSort.A2Z ),
                    WarehouseItemBodypartSort.bind( null, WarehouseWithdrawSort.A2Z ),
                ]
            }
            break
    }

    return []
}

function WarehouseItemNameSort( order: number, one: L2ItemInstance, two: L2ItemInstance ): number {
    if ( ( one.getItem().getType2() === ItemType2.MONEY ) && ( two.getItem().getType2() !== ItemType2.MONEY ) ) {
        return order === WarehouseWithdrawSort.A2Z ? WarehouseWithdrawSort.Z2A : WarehouseWithdrawSort.A2Z
    }

    if ( ( two.getItem().getType2() === ItemType2.MONEY ) && ( one.getItem().getType2() !== ItemType2.MONEY ) ) {
        return order === WarehouseWithdrawSort.A2Z ? WarehouseWithdrawSort.A2Z : WarehouseWithdrawSort.Z2A
    }

    let nameOne: string = one.getItem().getName()
    let nameTwo: string = two.getItem().getName()

    return order === WarehouseWithdrawSort.A2Z ? nameOne.localeCompare( nameTwo ) : nameTwo.localeCompare( nameOne )
}

function WarehouseItemGradeSort( order: number, one: L2ItemInstance, two: L2ItemInstance ): number {
    if ( ( one.getItem().getType2() === ItemType2.MONEY ) && ( two.getItem().getType2() !== ItemType2.MONEY ) ) {
        return order === WarehouseWithdrawSort.A2Z ? WarehouseWithdrawSort.Z2A : WarehouseWithdrawSort.A2Z
    }

    if ( ( two.getItem().getType2() === ItemType2.MONEY ) && ( one.getItem().getType2() !== ItemType2.MONEY ) ) {
        return order === WarehouseWithdrawSort.A2Z ? WarehouseWithdrawSort.A2Z : WarehouseWithdrawSort.Z2A
    }

    let oneGrade: CrystalType = one.getItem().getItemGrade()
    let twoGrade: CrystalType = two.getItem().getItemGrade()
    return order === WarehouseWithdrawSort.A2Z ? _.clamp( oneGrade - twoGrade, -1, 1 ) : _.clamp( twoGrade - oneGrade, -1, 1 )
}

function WarehouseItemRecipeSort( order: number, one: L2ItemInstance, two: L2ItemInstance ): number {
    if ( ( one.getItem().getType2() === ItemType2.MONEY ) && ( two.getItem().getType2() !== ItemType2.MONEY ) ) {
        return order === WarehouseWithdrawSort.A2Z ? WarehouseWithdrawSort.Z2A : WarehouseWithdrawSort.A2Z
    }

    if ( ( two.getItem().getType2() === ItemType2.MONEY ) && ( one.getItem().getType2() !== ItemType2.MONEY ) ) {
        return order === WarehouseWithdrawSort.A2Z ? WarehouseWithdrawSort.A2Z : WarehouseWithdrawSort.Z2A
    }

    if ( ( one.isEtcItem() && ( one.getItemType() === EtcItemType.RECIPE ) ) && ( two.isEtcItem() && ( two.getItemType() === EtcItemType.RECIPE ) ) ) {
        let oneRecipe: L2RecipeDataItem = DataManager.getRecipeData().getRecipeByItemId( one.getItem().getId() )
        let twoRecipe: L2RecipeDataItem = DataManager.getRecipeData().getRecipeByItemId( two.getItem().getId() )

        if ( !oneRecipe ) {
            return order === WarehouseWithdrawSort.A2Z ? WarehouseWithdrawSort.Z2A : WarehouseWithdrawSort.A2Z
        }
        if ( !twoRecipe ) {
            return order === WarehouseWithdrawSort.A2Z ? WarehouseWithdrawSort.Z2A : WarehouseWithdrawSort.A2Z
        }

        let oneLevel = oneRecipe.level
        let twoLevel = twoRecipe.level

        return order === WarehouseWithdrawSort.A2Z ? _.clamp( oneLevel - twoLevel, -1, 1 ) : _.clamp( twoLevel - oneLevel, -1, 1 )
    }

    let nameOne: string = one.getItem().getName()
    let nameTwo: string = two.getItem().getName()

    return order === WarehouseWithdrawSort.A2Z ? nameOne.localeCompare( nameTwo ) : nameTwo.localeCompare( nameOne )
}

function WarehouseItemBodypartSort( order: number, one: L2ItemInstance, two: L2ItemInstance ): number {
    if ( ( one.getItem().getType2() === ItemType2.MONEY ) && ( two.getItem().getType2() !== ItemType2.MONEY ) ) {
        return order === WarehouseWithdrawSort.A2Z ? WarehouseWithdrawSort.Z2A : WarehouseWithdrawSort.A2Z
    }

    if ( ( two.getItem().getType2() === ItemType2.MONEY ) && ( one.getItem().getType2() !== ItemType2.MONEY ) ) {
        return order === WarehouseWithdrawSort.A2Z ? WarehouseWithdrawSort.A2Z : WarehouseWithdrawSort.Z2A
    }

    let oneSlot = one.getItem().getBodyPart()
    let twoSlot = two.getItem().getBodyPart()
    return order === WarehouseWithdrawSort.A2Z ? _.clamp( oneSlot - twoSlot, -1, 1 ) : _.clamp( twoSlot - oneSlot, -1, 1 )
}

function WarehouseItemTypeSort( order: number, one: L2ItemInstance, two: L2ItemInstance ): number {
    if ( ( one.getItem().getType2() === ItemType2.MONEY ) && ( two.getItem().getType2() !== ItemType2.MONEY ) ) {
        return order === WarehouseWithdrawSort.A2Z ? WarehouseWithdrawSort.Z2A : WarehouseWithdrawSort.A2Z
    }

    if ( ( two.getItem().getType2() === ItemType2.MONEY ) && ( one.getItem().getType2() !== ItemType2.MONEY ) ) {
        return order === WarehouseWithdrawSort.A2Z ? WarehouseWithdrawSort.A2Z : WarehouseWithdrawSort.Z2A
    }

    let oneType: number = one.getItem().getMaterialSortOrder()
    let twoType: number = two.getItem().getMaterialSortOrder()
    return order === WarehouseWithdrawSort.A2Z ? _.clamp( oneType - twoType, -1, 1 ) : _.clamp( twoType - oneType, -1, 1 )
}