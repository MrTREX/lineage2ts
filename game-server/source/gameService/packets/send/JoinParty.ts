import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function JoinParty( responseId: number ) : Buffer {
    return new DeclaredServerPacket( 5 )
            .writeC( 0x3A )
            .writeD( responseId )
            .getBuffer()
}