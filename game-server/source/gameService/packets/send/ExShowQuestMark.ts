import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExShowQuestMark( questId: number ) : Buffer {
    return new DeclaredServerPacket( 7 )
            .writeC( 0xFE )
            .writeH( 0x21 )
            .writeD( questId )
            .getBuffer()
}