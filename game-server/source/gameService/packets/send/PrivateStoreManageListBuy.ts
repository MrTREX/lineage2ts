import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { TradeItem } from '../../models/TradeItem'
import { ItemPacketHelper } from './helpers/ItemPacketHelper'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export function PrivateStoreManageListBuy( player: L2PcInstance ): Buffer {
    let items: Array<L2ItemInstance> = player.getInventory().getUniqueItems( false, true, true )
    let buyList: Array<TradeItem> = player.getBuyList().getItems()

    let allItemsSize: number = items.length * ( ItemPacketHelper.writeItemSize() + 8 )

    let allTradeItemsSize: number = buyList.reduce( ( size: number, item: TradeItem ): number => {
        return size + ItemPacketHelper.writeTradeItemSize( item ) + 24
    }, 0 )

    let packet = new DeclaredServerPacket( 17 + allItemsSize + 4 + allTradeItemsSize )
            .writeC( 0xBD )
            .writeD( player.getObjectId() )
            .writeQ( player.getAdena() )
            .writeD( items.length )

    items.forEach( ( item: L2ItemInstance ) => {
        ItemPacketHelper.writeItem( packet, item )
        packet.writeQ( item.getItem().getDoubleReferencePrice() )
    } )

    packet.writeD( buyList.length )
    buyList.forEach( ( item: TradeItem ) => {
        ItemPacketHelper.writeTradeItem( packet, item )
        packet
                .writeQ( item.price )
                .writeQ( item.itemTemplate.getDoubleReferencePrice() )
                .writeQ( item.count )
    } )

    return packet.getBuffer()
}