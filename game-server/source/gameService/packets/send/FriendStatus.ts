import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

export const enum FriendState {
    Offline,
    Online
}

export function FriendStatus( objectId: number, name: string, status: FriendState ): Buffer {
    return new DeclaredServerPacket( 9 + getStringSize( name ) )
            .writeC( 0x77 )
            .writeD( status )
            .writeS( name )
            .writeD( objectId )
            .getBuffer()
}