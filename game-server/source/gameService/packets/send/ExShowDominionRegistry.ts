import { TerritoryWarManager } from '../../instancemanager/TerritoryWarManager'
import { L2Clan } from '../../models/L2Clan'
import { Territory } from '../../instancemanager/territory/Territory'
import { DeclaredServerPacket, getStringArraySize } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

type CastleNames = [ string, string, string ]
const noResponseNames: CastleNames = [ 'No Owner', 'No Owner', 'No Ally' ]

function getLabels( castleId: number ): CastleNames {
    if ( !TerritoryWarManager.getTerritory( castleId ) ) {
        return noResponseNames
    }

    let clan: L2Clan = TerritoryWarManager.getTerritory( castleId ).getOwnerClan()
    if ( !clan ) {
        return noResponseNames
    }

    return [ clan.getName(), clan.getLeaderName(), clan.getAllyName() ]
}

export function ExShowDominionRegistry( castleId: number, player: L2PcInstance ): Buffer {
    let clanSize = 0
    let mercSize = 0
    let isMercRegistered = 0
    let isClanRegistered = 0

    if ( TerritoryWarManager.getRegisteredClans( castleId ) ) {
        clanSize = TerritoryWarManager.getRegisteredClans( castleId ).length

        if ( player.getClan() ) {
            isClanRegistered = TerritoryWarManager.getRegisteredClans( castleId ).includes( player.getClan() ) ? 0x01 : 0x00
        }
    }

    if ( TerritoryWarManager.getRegisteredMercenaries( castleId ) ) {
        mercSize = TerritoryWarManager.getRegisteredMercenaries( castleId ).length
        isMercRegistered = TerritoryWarManager.getRegisteredMercenaries( castleId ).includes( player.getObjectId() ) ? 0x01 : 0x00
    }

    let warTime = Math.floor( TerritoryWarManager.getTWStartTime() / 1000 )
    let allTerritories: Array<Territory> = TerritoryWarManager.getAllTerritories()
    let territorySize: number = allTerritories.reduce( ( total: number, item: Territory ): number => {
        return total + 8 + item.getOwnedWardIds().length * 4
    }, 0 )

    let labels: CastleNames = getLabels( castleId )
    let packet = new DeclaredServerPacket( 39 + getStringArraySize( labels ) + territorySize )
            .writeC( 0xfe )
            .writeH( 0x90 )
            .writeD( 80 + castleId ) // Current Territory Id
            .writeS( labels[ 0 ] )

            .writeS( labels[ 1 ] )
            .writeS( labels[ 2 ] )
            .writeD( clanSize ) // Clan Request
            .writeD( mercSize ) // Merc Request

            .writeD( warTime ) // War Time
            .writeD( Date.now() ) // Current Time
            .writeD( isClanRegistered ) // is Cancel clan registration
            .writeD( isMercRegistered ) // is Cancel mercenaries registration

            .writeD( 0x01 )
            .writeD( allTerritories.length )

    allTerritories.forEach( ( territory: Territory ) => {
        let ownedWardIds: Array<number> = territory.getOwnedWardIds()
        packet
                .writeD( territory.getTerritoryId() ) // Territory Id
                .writeD( ownedWardIds.length ) // Emblem Count

        ownedWardIds.forEach( ( id: number ) => {
            packet.writeD( id )
        } )
    } )

    return packet.getBuffer()
}