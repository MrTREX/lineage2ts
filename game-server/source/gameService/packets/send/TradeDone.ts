import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { PacketHelper } from '../PacketVariables'

const cancelStaticPacket = PacketHelper.preservePacket( new DeclaredServerPacket( 5 )
        .writeC( 0x1c )
        .writeD( 0 )
        .getBuffer() )

const finishStaticPacket = PacketHelper.preservePacket( new DeclaredServerPacket( 5 )
        .writeC( 0x1c )
        .writeD( 1 )
        .getBuffer() )

export const enum TradeDoneStatus {
    Reject,
    Accept
}

export function TradeDone( status : TradeDoneStatus ): Buffer {
    return PacketHelper.copyPacket( status === TradeDoneStatus.Reject ? cancelStaticPacket : finishStaticPacket )
}