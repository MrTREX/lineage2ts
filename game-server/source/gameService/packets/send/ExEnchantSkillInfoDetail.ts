import { L2EnchantSkillLearn } from '../../models/L2EnchantSkillLearn'
import { DataManager } from '../../../data/manager'
import { EnchantSkillHolder } from '../../models/L2EnchantSkillGroup'
import { ConfigManager } from '../../../config/ConfigManager'
import { SkillItemValues } from '../../values/SkillItemValues'
import { ItemTypes } from '../../values/InventoryValues'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export const enum EnchantType {
    Normal = 0,
    Safe = 1,
    Untrain = 2,
    ChangeRoute = 3,
}

export function ExEnchantSkillInfoDetail( type: EnchantType, skillId: number, skillLevel: number, player: L2PcInstance ): Buffer {
    let enchantLearn: L2EnchantSkillLearn = DataManager.getSkillData().getSkillEnchantmentBySkillId( skillId )
    let data: EnchantSkillHolder
    let multiplier: number = 1
    let requiredCount: number = 0
    let bookId: number = 0

    if ( enchantLearn ) {
        if ( skillLevel > 100 ) {
            data = enchantLearn.getEnchantSkillHolder( skillLevel )
        } else {
            data = enchantLearn.getFirstRouteGroup().getEnchantGroupDetails()[ 0 ]
        }
    }

    if ( !data ) {
        return null
    }

    if ( type === EnchantType.Normal ) {
        multiplier = ConfigManager.general.getNormalEnchantCostMultipiler()
    }

    if ( type === EnchantType.Safe ) {
        multiplier = ConfigManager.general.getSafeEnchantCostMultipiler()
    }

    let exp = data.getRate( player )
    let sp = data.getSpCost()
    if ( type === EnchantType.Untrain ) {
        sp = Math.floor( 0.8 * sp )
    }

    let adenaCount = data.getAdenaCost() * multiplier

    switch ( type ) {
        case EnchantType.Normal:
            bookId = SkillItemValues.NormalEnchantBook
            requiredCount = ( ( skillLevel % 100 ) > 1 ) ? 0 : 1
            break

        case EnchantType.Safe:
            bookId = SkillItemValues.SafeEnchantBook
            requiredCount = 1
            break

        case EnchantType.Untrain:
            bookId = SkillItemValues.UntrainEnchantBook
            requiredCount = 1
            break

        case EnchantType.ChangeRoute:
            bookId = SkillItemValues.ChangeEnchantBook
            requiredCount = 1
            break

        default:
            return null
    }

    if ( ( type !== EnchantType.Safe ) && !ConfigManager.character.enchantSkillSpBookNeeded() ) {
        requiredCount = 0
    }

    return new DeclaredServerPacket( 43 )
            .writeC( 0xFE )
            .writeH( 0x5E )
            .writeD( type )
            .writeD( skillId )

            .writeD( skillLevel )
            .writeD( sp * multiplier ) // sp
            .writeD( exp ) // exp
            .writeD( 0x02 ) // items count?

            .writeD( ItemTypes.Adena ) // Adena
            .writeD( adenaCount ) // Adena count
            .writeD( bookId ) // ItemId Required
            .writeD( requiredCount )

            .getBuffer()
}