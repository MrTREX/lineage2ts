import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { ItemPacketHelper } from './helpers/ItemPacketHelper'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export function PackageSendableList( player: L2PcInstance, targetObjectId: number ): Buffer {
    let items: Array<L2ItemInstance> = player.getInventory().getAvailableItems( true, true, true, true )

    let allItemsSize: number = items.length * ( ItemPacketHelper.writeItemSize() + 4 )

    let packet = new DeclaredServerPacket( 17 + allItemsSize )
            .writeC( 0xD2 )
            .writeD( targetObjectId )
            .writeQ( player.getAdena() )
            .writeD( items.length )

    items.forEach( ( item: L2ItemInstance ) => {
        ItemPacketHelper.writeItem( packet, item )
        packet.writeD( item.getObjectId() )
    } )

    return packet.getBuffer()
}