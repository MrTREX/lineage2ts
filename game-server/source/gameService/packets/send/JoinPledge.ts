import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function JoinPledge( pledgeId: number ) : Buffer {
    return new DeclaredServerPacket( 5 )
            .writeC( 0x2D )
            .writeD( pledgeId )
            .getBuffer()
}