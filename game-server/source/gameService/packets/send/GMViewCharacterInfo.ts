import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { DataManager } from '../../../data/manager'
import { PaperdollOrder } from '../../values/InventoryValues'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'
import _ from 'lodash'

const paperdollPacketSize = PaperdollOrder.length * 12
export function GMViewCharacterInfo( player: L2PcInstance ) : Buffer {
    let moveMultiplier = player.getMovementSpeedMultiplier()
    let runSpeed = Math.round( player.getRunSpeed() / moveMultiplier )
    let walkSpeed = Math.round( player.getWalkSpeed() / moveMultiplier )
    let swimRunSpeed = Math.round( player.getSwimRunSpeed() / moveMultiplier )
    let swimWalkSpeed = Math.round( player.getSwimWalkSpeed() / moveMultiplier )
    let flyRunSpeed = player.isFlying() ? runSpeed : 0
    let flyWalkSpeed = player.isFlying() ? walkSpeed : 0

    let packet = new DeclaredServerPacket( 328 + getStringSize( player.getName() ) + paperdollPacketSize + getStringSize( player.getTitle() ) )
            .writeC( 0x95 )
            .writeD( player.getX() )
            .writeD( player.getY() )
            .writeD( player.getZ() )

            .writeD( player.getHeading() )
            .writeD( player.getObjectId() )
            .writeS( player.getName() )
            .writeD( player.getRace() )

            .writeD( player.getAppearance().getSex() ? 1 : 0 )
            .writeD( player.getClassId() )
            .writeD( player.getLevel() )
            .writeQ( player.getExp() )

            .writeF( DataManager.getExperienceData().getPercentFromCurrentLevel( player.getExp(), player.getLevel() ) )
            .writeD( player.getSTR() )
            .writeD( player.getDEX() )
            .writeD( player.getCON() )

            .writeD( player.getINT() )
            .writeD( player.getWIT() )
            .writeD( player.getMEN() )
            .writeD( player.getMaxHp() )

            .writeD( Math.floor( player.getCurrentHp() ) )
            .writeD( player.getMaxMp() )
            .writeD( Math.floor( player.getCurrentMp() ) )
            .writeD( player.getSp() )

            .writeD( player.getCurrentLoad() )
            .writeD( player.getMaxLoad() )
            .writeD( player.getPkKills() )

    PaperdollOrder.forEach( ( slot: number ) => {
        packet.writeD( player.getInventory().getPaperdollObjectId( slot ) )
    } )

    PaperdollOrder.forEach( ( slot: number ) => {
        packet.writeD( player.getInventory().getPaperdollItemId( slot ) )
    } )

    PaperdollOrder.forEach( ( slot: number ) => {
        packet.writeD( player.getInventory().getPaperdollAugmentationId( slot ) )
    } )

    packet
            .writeD( player.getTalismanSlots() )
            .writeD( player.getInventory().canEquipCloak() ? 1 : 0 )
            .writeD( Math.floor( player.getPowerAttack( null ) ) )
            .writeD( Math.floor( player.getPowerAttackSpeed() ) )

            .writeD( Math.floor( player.getPowerDefence( null ) ) )
            .writeD( player.getEvasionRate( null ) )
            .writeD( player.getAccuracy() )
            .writeD( player.getCriticalHit( null, null ) )

            .writeD( Math.floor( player.getMagicAttack( null, null ) ) )
            .writeD( player.getMagicAttackSpeed() )
            .writeD( Math.floor( player.getPowerAttackSpeed() ) )
            .writeD( Math.floor( player.getMagicDefence( null, null ) ) )

            .writeD( player.getPvpFlag() )
            .writeD( player.getKarma() )
            .writeD( runSpeed )
            .writeD( walkSpeed )

            .writeD( swimRunSpeed )
            .writeD( swimWalkSpeed )
            .writeD( flyRunSpeed )
            .writeD( flyWalkSpeed )

            .writeD( flyRunSpeed )
            .writeD( flyWalkSpeed )
            .writeF( moveMultiplier )
            .writeF( player.getAttackSpeedMultiplier() )

            .writeF( player.getCollisionRadius() )
            .writeF( player.getCollisionHeight() )
            .writeD( player.getAppearance().getHairStyle() )
            .writeD( player.getAppearance().getHairColor() )

            .writeD( player.getAppearance().getFace() )
            .writeD( player.isGM() ? 1 : 0 )
            .writeS( player.getTitle() )
            .writeD( player.getClanId() )

            .writeD( player.getClanCrestId() )
            .writeD( player.getAllyId() )
            .writeC( player.getMountType() )
            .writeC( player.getPrivateStoreType() )

            .writeC( player.hasDwarvenCraft() ? 1 : 0 )
            .writeD( player.getPkKills() )
            .writeD( player.getPvpKills() )
            .writeH( player.getRecommendationsLeft() )

            .writeH( player.getRecommendationsHave() )
            .writeD( player.getClassId() )
            .writeD( 0 )
            .writeD( player.getMaxCp() )

            .writeD( Math.floor( player.getCurrentCp() ) )
            .writeC( player.getMovementType() )
            .writeC( 0xFF ) // L2J has value that is more than 8 bits? = 321, clan privileges?
            .writeD( player.getPledgeClass() )

            .writeC( player.isNoble() ? 1 : 0 )
            .writeC( player.isHero() ? 1 : 0 )
            .writeD( player.getAppearance().getNameColor() )
            .writeD( player.getAppearance().getTitleColor() )

    let element = player.getAttackElement()
    packet
            .writeH( element )
            .writeH( player.getAttackElementValue( element ) )

    _.times( 6, ( index: number ) => {
        packet.writeH( player.getDefenceElementValue( index ) )
    } )

    packet
            .writeD( player.getFame() )
            .writeD( player.getVitalityPoints() )

    return packet.getBuffer()
}