import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2Object } from '../../models/L2Object'

export function MagicSkillLaunched( attackerId: number, skillId: number, skillLevel: number, targets: Array<L2Object> = [] ) : Buffer {
    let noTargets = targets.length === 0
    let totalTargetSize = noTargets ? 1 : targets.length

    let packet = new DeclaredServerPacket( 17 + totalTargetSize * 4 )
            .writeC( 0x54 )
            .writeD( attackerId )
            .writeD( skillId )
            .writeD( skillLevel )
            .writeD( targets.length )

    if ( noTargets ) {
        packet.writeD( attackerId )
    } else {
        targets.forEach( ( currentTarget: L2Object ) => {
            packet.writeD( currentTarget.getObjectId() )
        } )
    }

    return packet.getBuffer()
}