import { TradeItem, TradeItemHelper } from '../../models/TradeItem'
import { ItemPacketHelper } from './helpers/ItemPacketHelper'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export function PrivateStoreManageListSell( player: L2PcInstance, isPackageSale: boolean ): Buffer {
    let sellList = player.getSellList()
    if ( !sellList ) {
        return
    }

    sellList.updateItems()

    /*
        Available sellable items are created dynamically, hence need to be recycled.
        Sell-list items will be recycled when iteams are manually cleared.
     */
    let tradeItems: Array<TradeItem> = player.getInventory().getAvailableSellableItems()
    let sellableItems: Array<TradeItem> = sellList.getItems()

    let tradeItemsSize: number = tradeItems.reduce( ( size: number, item: TradeItem ): number => {
        return size + ItemPacketHelper.writeTradeItemSize( item ) + 8
    }, 0 )

    let sellableItemsSize: number = sellableItems.reduce( ( size: number, item: TradeItem ): number => {
        return size + ItemPacketHelper.writeTradeItemSize( item ) + 16
    }, 0 )

    let packet = new DeclaredServerPacket( 25 + tradeItemsSize + sellableItemsSize )
            .writeC( 0xA0 )
            .writeD( player.getObjectId() )
            .writeD( isPackageSale ? 1 : 0 )
            .writeQ( player.getAdena() )

            .writeD( tradeItems.length )

    tradeItems.forEach( ( tradeItem: TradeItem ) => {
        ItemPacketHelper.writeTradeItem( packet, tradeItem )
        packet.writeQ( tradeItem.itemTemplate.getDoubleReferencePrice() )

        TradeItemHelper.recycle( tradeItem )
    } )


    packet.writeD( sellableItems.length )
    sellableItems.forEach( ( tradeItem: TradeItem ) => {
        ItemPacketHelper.writeTradeItem( packet, tradeItem )
        packet
                .writeQ( tradeItem.price )
                .writeQ( tradeItem.itemTemplate.getDoubleReferencePrice() )
    } )

    return packet.getBuffer()
}