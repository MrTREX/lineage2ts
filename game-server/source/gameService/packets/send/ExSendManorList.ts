import { Castle } from '../../models/entity/Castle'
import { CastleManager } from '../../instancemanager/CastleManager'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'
import { PacketHelper } from '../PacketVariables'
import _ from 'lodash'

let staticPacket: Buffer

function generatePacket(): void {
    let castles: Array<Castle> = _.sortBy( CastleManager.getCastles(), [ 'residenceId' ] )
    let size: number = castles.reduce( ( total: number, castle: Castle ): number => {
        return total + 4 + getStringSize( castle.getName() )
    }, 0 )

    let packet = new DeclaredServerPacket( 7 + size )
            .writeC( 0xFE )
            .writeH( 0x22 )
            .writeD( castles.length )

    castles.forEach( ( castle: Castle ) => {
        packet
                .writeD( castle.getResidenceId() )
                .writeS( castle.getName().toLowerCase() )
    } )

    staticPacket = PacketHelper.preservePacket( packet.getBuffer() )
}

export function ExSendManorList(): Buffer {
    if ( !staticPacket ) {
        generatePacket()
    }

    return PacketHelper.copyPacket( staticPacket )
}