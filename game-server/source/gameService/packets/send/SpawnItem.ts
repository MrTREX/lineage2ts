import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2Object } from '../../models/L2Object'

export function SpawnItem( object: L2Object ): Buffer {
    let x = object.getX()
    let y = object.getY()
    let z = object.getZ()

    let itemId: number
    let stackable: number
    let count: number

    if ( object.isItem() ) {
        let item = object as L2ItemInstance
        itemId = item.getId()
        stackable = item.isStackable() ? 0x01 : 0x00
        count = item.getCount()
    } else {
        itemId = object.getPolymorphTemplateId()
        stackable = 0
        count = 1
    }

    return new DeclaredServerPacket( 41 )
            .writeC( 0x05 )
            .writeD( object.getObjectId() )
            .writeD( itemId )
            .writeD( x )

            .writeD( y )
            .writeD( z )
            .writeD( stackable )
            .writeQ( count )

            .writeD( 0 )
            .writeD( 0 )
            .getBuffer()
}