import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExCubeGameChangeTeam( objectId: number, isRedTeam: boolean ) : Buffer {
    return new DeclaredServerPacket( 19 )
            .writeC( 0xfe )
            .writeH( 0x97 )
            .writeD( 0x05 )
            .writeD( objectId )

            .writeD( isRedTeam ? 0x01 : 0x00 )
            .writeD( isRedTeam ? 0x00 : 0x01 )
            .getBuffer()
}