import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { PacketHelper } from '../PacketVariables'

export function ShortBuffStatusUpdate( skillId: number, skillLevel: number, duration: number ): Buffer {
    return new DeclaredServerPacket( 13 )
            .writeC( 0xfa )
            .writeD( skillId )
            .writeD( skillLevel )
            .writeD( duration )
            .getBuffer()
}

export const ResetShortBuffStatus = PacketHelper.preservePacket( ShortBuffStatusUpdate( 0, 0, 0 ) )