import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExPutCommissionResultForVariationMake( objectId: number, count: number, itemId: number ) : Buffer {
    return new DeclaredServerPacket( 31 )
            .writeC( 0xFE )
            .writeH( 0x55 )
            .writeD( objectId )
            .writeD( itemId )

            .writeQ( count )
            .writeD( 0 )
            .writeD( 0 )
            .writeD( 1 )
            .getBuffer()
}