import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

export function ExAskJoinPartyRoom( name: string ) : Buffer {
    return new DeclaredServerPacket( 3 + getStringSize( name ) )
            .writeC( 0xFE )
            .writeH( 0x35 )
            .writeS( name )
            .getBuffer()
}