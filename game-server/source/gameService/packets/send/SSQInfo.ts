import { SevenSigns } from '../../directives/SevenSigns'
import { SevenSignsSide } from '../../values/SevenSignsValues'
import { PacketHelper } from '../PacketVariables'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

const noWinnerStaticPacket = PacketHelper.preservePacket( new DeclaredServerPacket( 3 )
        .writeC( 0x73 )
        .writeH( 256 )
        .getBuffer() )

const duskWinnerStaticPacket = PacketHelper.preservePacket( new DeclaredServerPacket( 3 )
        .writeC( 0x73 )
        .writeH( 256 + 1 )
        .getBuffer() )

const dawnWinnerStaticPacket = PacketHelper.preservePacket( new DeclaredServerPacket( 3 )
        .writeC( 0x73 )
        .writeH( 256 + 2 )
        .getBuffer() )

export function SSQInfo(): Buffer {

    if ( SevenSigns.isSealValidationPeriod() ) {
        let winnerSide = SevenSigns.getWinnerSide()

        if ( winnerSide === SevenSignsSide.Dawn ) {
            return PacketHelper.copyPacket( dawnWinnerStaticPacket )
        }

        if ( winnerSide === SevenSignsSide.Dusk ) {
            return PacketHelper.copyPacket( duskWinnerStaticPacket )
        }
    }

    return PacketHelper.copyPacket( noWinnerStaticPacket )
}