import { ShortcutType } from '../../enums/ShortcutType'
import { PlayerShortcutCache } from '../../cache/PlayerShortcutCache'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2PlayerShortcutsTableItem } from '../../../database/interface/CharacterShortcutsTableApi'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

function getSize( data: L2PlayerShortcutsTableItem ): number {
    switch ( data.type ) {
        case ShortcutType.ITEM:
            return 24

        case ShortcutType.SKILL:
            return 13

        case ShortcutType.ACTION:
        case ShortcutType.MACRO:
        case ShortcutType.RECIPE:
        case ShortcutType.BOOKMARK:
            return 8
    }

    return 0
}

export function ShortCutInit( player: L2PcInstance ): Buffer {
    let shortcuts = PlayerShortcutCache.getShortcuts( player.getObjectId() )
    let size = shortcuts.reduce( ( total: number, data: L2PlayerShortcutsTableItem ): number => {
        return total + 8 + getSize( data )
    }, 0 )

    let packet = new DeclaredServerPacket( 5 + size )
            .writeC( 0x45 )
            .writeD( shortcuts.length )

    shortcuts.forEach( ( data: L2PlayerShortcutsTableItem ) => {
        packet
                .writeD( data.type )
                .writeD( data.index )

        switch ( data.type ) {
            case ShortcutType.ITEM:
                packet
                        .writeD( data.id )
                        .writeD( data.characterType )
                        .writeD( data.itemReuseGroup )

                let item = player.getInventory().getItemByObjectId( data.objectId )
                if ( !item ) {
                    packet
                        .writeD( 0 )
                        .writeD( 0 )
                        .writeD( 0 )

                    return
                }

                if ( !item.isEtcItem() ) {
                    packet
                        .writeD( 0 )
                        .writeD( 0 )
                        .writeD( item.isAugmented() ? item.getAugmentation().getAugmentationId() : 0 )

                    return
                }

                let skills = item.getEtcItem().getSkills()
                if ( !skills || skills.length === 0 ) {
                    packet
                        .writeD( 0 )
                        .writeD( 0 )
                        .writeD( 0 )

                    return
                }

                /*
                    TODO: check if item or skill reuse data is populated and adjust
                    - possible that item instead of skill is being recorded for reuse
                 */
                let skillReuse = player.getSkillReuseData( skills[ 0 ] )

                if ( !skillReuse ) {
                    packet
                        .writeD( 0 )
                        .writeD( 0 )
                        .writeD( 0 )

                    return
                }

                packet
                    .writeD( 0 )
                    .writeD( skillReuse.getRemainingMs() / 1000 )
                    .writeD( skillReuse.getReuseSeconds() )

                return

            case ShortcutType.SKILL:
                packet
                        .writeD( data.id )
                        .writeD( data.level )
                        .writeC( 0x00 )
                        .writeD( data.characterType )

                return

            case ShortcutType.ACTION:
            case ShortcutType.MACRO:
            case ShortcutType.RECIPE:
            case ShortcutType.BOOKMARK:
                packet
                        .writeD( data.id )
                        .writeD( data.characterType )
                return
        }
    } )

    return packet.getBuffer()
}