import { L2World } from '../../L2World'
import { TerritoryWarManager } from '../../instancemanager/TerritoryWarManager'
import { DataManager } from '../../../data/manager'
import { L2NpcTemplate } from '../../models/actor/templates/L2NpcTemplate'
import { AbnormalVisualEffect, AbnormalVisualEffectMap } from '../../models/skills/AbnormalVisualEffect'
import { ConfigManager } from '../../../config/ConfigManager'
import { L2CubicInstance } from '../../models/actor/instance/L2CubicInstance'
import { CursedWeaponManager } from '../../instancemanager/CursedWeaponManager'
import { PaperdollOrder } from '../../values/InventoryValues'
import { PartyMatchManager } from '../../cache/partyMatchManager'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'
import { SiegeRole } from '../../enums/SiegeRole'
import { ElementalType } from '../../enums/ElementalType'

const paperdollSize = PaperdollOrder.length * 12
const stealthEffect = AbnormalVisualEffectMap[ AbnormalVisualEffect.STEALTH ].mask

export function UserInfo( objectId: number ): Buffer {
    let player = L2World.getPlayer( objectId )
    if ( !player ) {
        return
    }

    let siegeRole = player.isClanLeader() ? 0x40 : 0
    if ( player.getSiegeRole() === SiegeRole.Attacker ) {
        let territoryId = TerritoryWarManager.getRegisteredTerritoryId( player )
        if ( territoryId === 0 ) {
            siegeRole |= 0x180
        } else {
            siegeRole |= 0x1000
        }
    }

    if ( player.getSiegeRole() === SiegeRole.Defender ) {
        siegeRole |= 0x80
    }

    let airShipHelm = 0
    if ( player.isInAirShip() && player.getAirShip().isCaptain( player ) ) {
        airShipHelm = player.getAirShip().getHelmItemId()
    }

    let moveMultiplier = player.getMovementSpeedMultiplier()
    let runSpeed = Math.floor( player.getRunSpeed() / moveMultiplier )
    let walkSpeed = Math.floor( player.getWalkSpeed() / moveMultiplier )
    let swimRunSpeed = Math.floor( player.getSwimRunSpeed() / moveMultiplier )
    let swimWalkSpeed = Math.floor( player.getSwimWalkSpeed() / moveMultiplier )
    let flyRunSpeed = player.isFlying() ? runSpeed : 0
    let flyWalkSpeed = player.isFlying() ? walkSpeed : 0
    let cubicSize = player.getCubics().size

    let title = player.getTitle()
    if ( player.isGM() && player.isInvisible() ) {
        title = 'Invisible'
    } else if ( player.isPolymorphed() ) {
        let polyObject: L2NpcTemplate = DataManager.getNpcData().getTemplate( player.getPolymorphTemplateId() )
        if ( polyObject ) {
            title = title + ' - ' + polyObject.getName()
        }
    }

    let packet = new DeclaredServerPacket( paperdollSize + getStringSize( player.getAppearance().getVisibleName() ) + getStringSize( title ) + cubicSize * 2 + 396 )
            .writeC( 0x32 )
            .writeD( player.getX() )
            .writeD( player.getY() )
            .writeD( player.getZ() )

            .writeD( player.getVehicle() ? player.getVehicle().getObjectId() : 0 )
            .writeD( objectId )
            .writeS( player.getAppearance().getVisibleName() )
            .writeD( player.getRace() )

            .writeD( player.getAppearance().getSex() ? 1 : 0 )
            .writeD( player.getBaseClass() )
            .writeD( player.getLevel() )
            .writeQ( player.getExp() )

            .writeF( DataManager.getExperienceData().getPercentFromCurrentLevel( player.getExp(), player.getLevel() ) )
            .writeD( player.getSTR() )
            .writeD( player.getDEX() )
            .writeD( player.getCON() )

            .writeD( player.getINT() )
            .writeD( player.getWIT() )
            .writeD( player.getMEN() )
            .writeD( player.getMaxHp() )

            .writeD( player.getCurrentHp() )
            .writeD( player.getMaxMp() )
            .writeD( player.getCurrentMp() )
            .writeD( player.getSp() )

            .writeD( player.getCurrentLoad() )
            .writeD( player.getMaxLoad() )
            .writeD( player.getActiveWeaponItem() ? 40 : 20 )

    /*
        TODO : generate packet fragments once on paperdoll change in player inventory
        - keep in mind that only generate these fragments in UserInfo call
        - once paperdoll item is set/removed, reset values in inventory
     */
    PaperdollOrder.forEach( ( slot: number ) => {
        packet.writeD( player.getInventory().getPaperdollObjectId( slot ) )
    } )

    PaperdollOrder.forEach( ( slot: number ) => {
        packet.writeD( player.getInventory().getPaperdollItemId( slot ) )
    } )

    PaperdollOrder.forEach( ( slot: number ) => {
        packet.writeD( player.getInventory().getPaperdollAugmentationId( slot ) )
    } )

    packet
            .writeD( player.getTalismanSlots() )
            .writeD( player.getInventory().canEquipCloak() ? 1 : 0 )
            .writeD( player.getPowerAttack( null ) )
            .writeD( player.getPowerAttackSpeed() )

            .writeD( Math.floor( player.getPowerDefence( null ) ) )
            .writeD( player.getEvasionRate( null ) )
            .writeD( player.getAccuracy() )
            .writeD( player.getCriticalHit( null, null ) )

            .writeD( Math.floor( player.getMagicAttack( null, null ) ) )
            .writeD( player.getMagicAttackSpeed() )
            .writeD( player.getPowerAttackSpeed() )
            .writeD( Math.floor( player.getMagicDefence( null, null ) ) )

            .writeD( player.getPvpFlag() )
            .writeD( player.getKarma() )
            .writeD( runSpeed )
            .writeD( walkSpeed )

            .writeD( swimRunSpeed )
            .writeD( swimWalkSpeed )
            .writeD( flyRunSpeed )
            .writeD( flyWalkSpeed )

            .writeD( flyRunSpeed )
            .writeD( flyWalkSpeed )
            .writeF( moveMultiplier )
            .writeF( player.getAttackSpeedMultiplier() )

            .writeF( player.getCollisionRadius() )
            .writeF( player.getCollisionHeight() )
            .writeD( player.getAppearance().getHairStyle() )
            .writeD( player.getAppearance().getHairColor() )

            .writeD( player.getAppearance().getFace() )
            .writeD( player.isGM() ? 1 : 0 )
            .writeS( title )
            .writeD( player.getClanId() )

            .writeD( player.getClanCrestId() )
            .writeD( player.getAllyId() )
            .writeD( player.getAllyCrestId() )
            .writeD( siegeRole )

            .writeC( player.getMountType() )
            .writeC( player.getPrivateStoreType() )
            .writeC( player.hasCrystallizeSkill() ? 1 : 0 ) // crystallize button shown or hidden
            .writeD( player.getPkKills() )

            .writeD( player.getPvpKills() )
            .writeH( cubicSize )

    player.getCubics().forEach( ( value: L2CubicInstance, cubicId: number ) => {
        packet.writeH( cubicId )
    } )

    packet
            .writeC( PartyMatchManager.getPlayerRoom( player.getObjectId() ) ? 1 : 0 )
            .writeD( player.isInvisible() ? ( player.getAbnormalVisualEffects() | stealthEffect ) : player.getAbnormalVisualEffects() )
            .writeC( player.getMovementType() )
            .writeD( player.getClanPriviledgesBitmask() )

            .writeH( player.getRecommendationsLeft() )
            .writeH( player.getRecommendationsHave() )
            .writeD( player.getMountNpcId() > 0 ? player.getMountNpcId() + 1000000 : 0 )
            .writeH( player.getInventoryLimit() )

            .writeD( player.getClassId() )
            .writeD( 0x00 ) // special effects? circles around player...
            .writeD( player.getMaxCp() )
            .writeD( player.getCurrentCp() )

            .writeC( player.isMounted() || ( airShipHelm !== 0 ) ? 0 : player.getEnchantEffect() )
            .writeC( player.getTeam() )
            .writeD( player.getClanCrestLargeId() )
            .writeC( player.isNoble() ? 1 : 0 )

            .writeC( player.isHero() || ( player.isGM() && ConfigManager.general.gmHeroAura() ) ? 1 : 0 ) // 0x01: Hero Aura
            .writeC( player.isFishing() ? 1 : 0 )
            .writeD( player.getFishX() )
            .writeD( player.getFishY() )

            .writeD( player.getFishZ() )
            .writeD( player.getAppearance().getNameColor() )
            .writeC( player.isRunning() ? 0x01 : 0x00 ) // changes the Speed display on Status Window
            .writeD( player.getPledgeClass() ) // changes the text above CP on Status Window

            .writeD( player.getPledgeType() )
            .writeD( player.getAppearance().getTitleColor() )
            .writeD( player.isCursedWeaponEquipped() ? CursedWeaponManager.getLevel( player.getCursedWeaponEquippedId() ) : 0 )
            .writeD( player.getTransformationDisplayId() )

    let attackAttribute = player.getAttackElement()

    packet
            .writeH( attackAttribute )
            .writeH( player.getAttackElementValue( attackAttribute ) )
            .writeH( player.getDefenceElementValue( ElementalType.FIRE ) )
            .writeH( player.getDefenceElementValue( ElementalType.WATER ) )

            .writeH( player.getDefenceElementValue( ElementalType.WIND ) )
            .writeH( player.getDefenceElementValue( ElementalType.EARTH ) )
            .writeH( player.getDefenceElementValue( ElementalType.HOLY ) )
            .writeH( player.getDefenceElementValue( ElementalType.DARK ) )

            .writeD( player.getAgathionId() )
            .writeD( player.getFame() )
            .writeD( player.isMinimapAllowed() ? 1 : 0 )
            .writeD( player.getVitalityPoints() )

            .writeD( player.getAbnormalVisualEffectSpecial() )

    player.getInventory().inventoryUpdateOperation()

    return packet.getBuffer()
}