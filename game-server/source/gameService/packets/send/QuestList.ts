import { QuestState } from '../../models/quest/QuestState'
import { QuestStateCache } from '../../cache/QuestStateCache'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { PacketHelper } from '../PacketVariables'

/**
 * <pre>
 * This text was written by XaKa, copied from L2J code
 * QuestList packet structure:
 * {
 *      1 byte - 0x86
 *      2 byte - Number of Quests
 *      for Quest in AvailableQuests
 *      {
 *          4 byte - Quest ID
 *          4 byte - Quest Status
 *      }
 * }
 *
 * NOTE: The following special constructs are true for the 4-byte Quest Status:
 * If the most significant bit is 0, this means that no progress-step got skipped.
 * In this case, merely passing the rank of the latest step gets the client to mark
 * it as current and mark all previous steps as complete.
 * If the most significant bit is 1, it means that some steps may have been skipped.
 * In that case, each bit represents a quest step (max 30) with 0 indicating that it was
 * skipped and 1 indicating that it either got completed or is currently active (the client
 * will automatically assume the largest step as active and all smaller ones as completed).
 * For example, the following bit sequences will yield the same results:
 * 1000 0000 0000 0000 0000 0011 1111 1111: Indicates some steps may be skipped but each of
 * the first 10 steps did not get skipped and current step is the 10th.
 * 0000 0000 0000 0000 0000 0000 0000 1010: Indicates that no steps were skipped and current is the 10th.
 * It is speculated that the latter will be processed faster by the client, so it is preferred when no
 * steps have been skipped.
 * However, the sequence "1000 0000 0000 0000 0000 0010 1101 1111" indicates that the current step is
 * the 10th but the 6th and 9th are not to be shown at all (not completed, either).
 * </pre>
 */


const paddingZeros : Buffer = PacketHelper.preservePacket( Buffer.alloc( 128, 0 ) )

export function QuestList( objectId: number ): Buffer {
    let states: Array<QuestState> = QuestStateCache.getAllActiveStates( objectId )

    let packet = new DeclaredServerPacket( 3 + states.length * 8 + paddingZeros.length )
            .writeC( 0x86 )
            .writeH( states.length )

    states.forEach( ( state: QuestState ) => {
        packet.writeD( state.questId )

        let flags = state.getStateFlags()
        packet.writeD( flags > 0 ? flags : state.getCondition() )
    } )

    packet.writeB( paddingZeros )

    return packet.getBuffer()
}
