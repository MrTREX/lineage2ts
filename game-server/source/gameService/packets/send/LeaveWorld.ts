import { PacketHelper } from '../PacketVariables'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

const staticPacket = PacketHelper.preservePacket( new DeclaredServerPacket( 1 ).writeC( 0x84 ).getBuffer() )

export function LeaveWorld(): Buffer {
    return PacketHelper.copyPacket( staticPacket )
}