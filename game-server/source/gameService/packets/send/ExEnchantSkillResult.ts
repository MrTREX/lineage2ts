import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { PacketHelper } from '../PacketVariables'

const staticTruePacket: Buffer = PacketHelper.preservePacket( new DeclaredServerPacket( 9 )
        .writeC( 0xFE )
        .writeD( 0xA7 )
        .writeD( 1 )
        .getBuffer() )

const staticFalsePacket = PacketHelper.preservePacket( new DeclaredServerPacket( 9 )
        .writeC( 0xFE )
        .writeD( 0xA7 )
        .writeD( 0 )
        .getBuffer() )

export function ExEnchantSkillResult( isEnchanted: boolean ): Buffer {
    return PacketHelper.copyPacket( isEnchanted ? staticTruePacket : staticFalsePacket )
}