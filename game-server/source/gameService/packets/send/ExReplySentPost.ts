import { MailMessage } from '../../models/mail/MailMessage'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { ItemPacketHelper } from './helpers/ItemPacketHelper'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'
import { CharacterNamesCache } from '../../cache/CharacterNamesCache'
import { ConfigManager } from '../../../config/ConfigManager'

export function ExReplySentPost( message: MailMessage ): Buffer {
    let attachments = message.getLoadedAttachments()
    let items: ReadonlyArray<L2ItemInstance> = attachments ? attachments.getItems() : []
    let allItemsSize: number = items.length * ( ItemPacketHelper.writeItemSize() + 4 )

    let receiverName: string = CharacterNamesCache.getCachedNameById( message.getReceiverId() ) ?? ConfigManager.general.getMailSystemName()
    let packetSize = 23 +
            getStringSize( receiverName ) +
            getStringSize( message.getSubject() ) +
            getStringSize( message.getContent() ) +
            allItemsSize + ( items.length > 0 ? 4 : 0 )

    let packet = new DeclaredServerPacket( packetSize )

            .writeC( 0xFE )
            .writeH( 0xaD )
            .writeD( message.getId() )
            .writeD( message.isCOD() ? 1 : 0 )

            .writeS( receiverName )
            .writeS( message.getSubject() )
            .writeS( message.getContent() )
            .writeD( items.length )

    items.forEach( ( item: L2ItemInstance ) => {
        ItemPacketHelper.writeItem( packet, item )
        packet.writeD( item.getObjectId() )
    } )

    packet.writeQ( message.getCODAdena() )

    if ( items.length > 0 ) {
        packet.writeD( message.getSendBySystem() )
    }

    return packet.getBuffer()
}