import { L2Party } from '../../models/L2Party'
import { L2World } from '../../L2World'
import { L2Summon } from '../../models/actor/L2Summon'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

export function PartySmallWindowAll( excludedObjectId: number, party: L2Party ): Buffer {
    let size = party.getMembers().reduce( ( total: number, memberId: number ): number => {
        if ( memberId === excludedObjectId ) {
            return
        }

        let player = L2World.getPlayer( memberId )
        if ( player ) {
            let playerSize = getStringSize( player.getName() ) + 56
            let summon: L2Summon = player.getSummon()
            if ( summon ) {
                return total + getStringSize( summon.getName() ) + playerSize + 28
            }

            return total + playerSize
        }

        return total
    }, 0 )

    let packet = new DeclaredServerPacket( 13 + size )
            .writeC( 0x4E )
            .writeD( party.getLeader().getObjectId() )
            .writeD( party.getDistributionType() )
            .writeD( party.getMemberCount() - 1 )

    party.getMembers().forEach( ( memberId: number ) => {
        if ( memberId === excludedObjectId ) {
            return
        }

        let player = L2World.getPlayer( memberId )

        packet
                .writeD( memberId )
                .writeS( player.getName() )
                .writeD( Math.floor( player.getCurrentCp() ) )
                .writeD( player.getMaxCp() )

                .writeD( Math.floor( player.getCurrentHp() ) )
                .writeD( player.getMaxHp() )
                .writeD( Math.floor( player.getCurrentMp() ) )
                .writeD( player.getMaxMp() )

                .writeD( player.getLevel() )
                .writeD( player.getClassId() )
                .writeD( 0x00 )
                .writeD( player.getRace() )

                .writeD( 0x00 )
                .writeD( 0x00 )

        if ( player.hasSummon() ) {
            let summon: L2Summon = player.getSummon()
            packet
                    .writeD( summon.getObjectId() )
                    .writeD( summon.getId() + 1000000 )
                    .writeD( summon.getSummonType() )
                    .writeS( summon.getName() )

                    .writeD( Math.floor( summon.getCurrentHp() ) )
                    .writeD( summon.getMaxHp() )
                    .writeD( Math.floor( summon.getCurrentMp() ) )
                    .writeD( summon.getMaxMp() )

                    .writeD( summon.getLevel() )
        } else {
            packet
                    .writeD( 0x00 )
        }
    } )

    return packet.getBuffer()
}