import { L2Party } from '../../models/L2Party'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export function PartySmallWindowAdd( player: L2PcInstance, party: L2Party ) : Buffer {
    return new DeclaredServerPacket( 53 + getStringSize( player.getName() ) )
            .writeC( 0x4F )
            .writeD( party.getLeaderObjectId() )
            .writeD( party.getDistributionType() )
            .writeD( player.getObjectId() )

            .writeS( player.getName() )
            .writeD( player.getCurrentCp() )
            .writeD( player.getMaxCp() )
            .writeD( player.getCurrentHp() )

            .writeD( player.getMaxHp() )
            .writeD( player.getCurrentMp() )
            .writeD( player.getMaxMp() )
            .writeD( player.getLevel() )

            .writeD( player.getClassId() )
            .writeD( 0 )
            .writeD( 0 )
            .getBuffer()
}