import { CastleManorManager } from '../../instancemanager/CastleManorManager'
import { CropProcure } from '../../models/manor/CropProcure'
import { L2Seed } from '../../models/L2Seed'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExShowCropInfo( manorId: number, isNextPeriod: boolean, hideSalesButtons: boolean ): Buffer {
    let crops = ( isNextPeriod && !CastleManorManager.isManorApproved() ) ? [] : CastleManorManager.getAllCropProcure( manorId, isNextPeriod )

    let cropSize = crops.length
    let packet = new DeclaredServerPacket( 8 + 8 + cropSize * 43 )
            .writeC( 0xFE )
            .writeH( 0x24 )
            .writeC( hideSalesButtons ? 0x01 : 0x00 )
            .writeD( manorId )

            .writeD( 0x00 )
            .writeD( cropSize )

    crops.forEach( ( crop: CropProcure ) => {
        packet
                .writeD( crop.getId() )
                .writeQ( crop.getAmount() )
                .writeQ( crop.getStartAmount() )
                .writeQ( crop.getPrice() )
                .writeC( crop.getReward() )

        let seed: L2Seed = CastleManorManager.getSeedByCrop( crop.getId() )
        if ( !seed ) {
            packet
                    .writeD( 0 ) // Seed level
                    .writeC( 0x01 ) // Reward 1
                    .writeD( 0 ) // Reward 1 - item id
                    .writeC( 0x01 ) // Reward 2
                    .writeD( 0 ) // Reward 2 - item id

            return
        }

        packet
                .writeD( seed.getLevel() ) // Seed level
                .writeC( 0x01 ) // Reward 1
                .writeD( seed.getReward( 1 ) ) // Reward 1 - item id
                .writeC( 0x01 ) // Reward 2
                .writeD( seed.getReward( 2 ) ) // Reward 2 - item id
    } )

    return packet.getBuffer()
}