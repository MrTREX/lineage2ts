import { Skill } from '../../models/Skill'
import { SkillCache } from '../../cache/SkillCache'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export function GMViewSkillInfo( player: L2PcInstance ): Buffer {
    let isSkillDisabled = player.getClan() && player.getClan().getReputationScore() < 0
    let skills: Array<Skill> = player.getAllSkills()

    let packet = new DeclaredServerPacket( 5 + getStringSize( player.getName() ) + skills.length * 14 )
            .writeC( 0x97 )
            .writeS( player.getName() )
            .writeD( skills.length )

    skills.forEach( ( skill: Skill ) => {
        packet
                .writeD( skill.isPassive() ? 1 : 0 )
                .writeD( skill.getDisplayLevel() )
                .writeD( skill.getDisplayId() )
                .writeC( isSkillDisabled && skill.isClanSkill() ? 1 : 0 )

                .writeC( SkillCache.isEnchantable( skill.getDisplayId() ) ? 1 : 0 )
    } )

    return packet.getBuffer()
}