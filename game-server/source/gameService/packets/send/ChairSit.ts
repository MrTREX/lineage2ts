import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ChairSit( objectId: number, id: number ) : Buffer {
    return new DeclaredServerPacket( 9 )
            .writeC( 0xED )
            .writeD( objectId )
            .writeD( id )
            .getBuffer()
}