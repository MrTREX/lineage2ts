import { L2Crest } from '../../models/L2Crest'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function PledgeCrest( crest: L2Crest ) : Buffer {
    let size = crest.getData() ? crest.getData().length : 0
    let packet = new DeclaredServerPacket( 9 + size )
            .writeC( 0x6A )
            .writeD( crest.getId() )
            .writeD( size )

    if ( size > 0 ) {
        packet.writeB( crest.getData() )
    }

    return packet.getBuffer()
}