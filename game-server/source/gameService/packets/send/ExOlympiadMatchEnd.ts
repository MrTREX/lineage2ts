import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { PacketHelper } from '../PacketVariables'

const staticPacket: Buffer = PacketHelper.preservePacket( new DeclaredServerPacket( 3 )
        .writeC( 0xFE )
        .writeH( 0x2D )
        .getBuffer() )

export function ExOlympiadMatchEnd(): Buffer {
    return PacketHelper.copyPacket( staticPacket )
}