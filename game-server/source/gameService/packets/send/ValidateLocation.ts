import { L2Object } from '../../models/L2Object'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ValidateLocation( object: L2Object ): Buffer {
    return new DeclaredServerPacket( 21 )
            .writeC( 0x79 )
            .writeD( object.getObjectId() )
            .writeD( object.getX() )
            .writeD( object.getY() )

            .writeD( object.getZ() )
            .writeD( object.getHeading() )
            .getBuffer()
}