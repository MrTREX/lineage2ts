import { SevenSigns } from '../../directives/SevenSigns'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { SevenSignsFestival } from '../../directives/SevenSignsFestival'
import { AllFestivalLevels, FestivalLevelScores, SevenSignsFestivalLevel } from '../../values/SevenSignsFestivalValues'
import { SevenSignsPeriod, SevenSignsSeal, SevenSignsSide } from '../../values/SevenSignsValues'
import _ from 'lodash'
import { DeclaredServerPacket, getStringArraySize } from '../../../packets/DeclaredServerPacket'
import { CharacterNamesCache } from '../../cache/CharacterNamesCache'

// TODO : consider caching some the pages on interval or when seven signs status changes

export type SSQStatusPage = 1 | 2 | 3 | 4
export async function SSQStatus( objectId: number, page: SSQStatusPage ): Promise<Buffer> {
    switch ( page ) {
        case 1:
            return showPageOne( objectId )

        case 2:
            return showPageTwo()

        case 3:
            return showPageThree()

        case 4:
            return showPageFour()
    }
}

function showPageOne( objectId: number ): Buffer {
    let packet = new DeclaredServerPacket( 83 )
            .writeC( 0xfb )
            .writeC( 1 )
            .writeC( SevenSigns.getCurrentPeriod() )
            .writeD( SevenSigns.getCurrentCycle() )

    let currentPeriod = SevenSigns.getCurrentPeriod()

    switch ( currentPeriod ) {
        case SevenSignsPeriod.Recruiting:
            packet.writeD( SystemMessageIds.INITIAL_PERIOD )
            break

        case SevenSignsPeriod.Competing:
            packet.writeD( SystemMessageIds.SSQ_COMPETITION_UNDERWAY )
            break

        case SevenSignsPeriod.CompetitionResults:
            packet.writeD( SystemMessageIds.RESULTS_PERIOD )
            break

        case SevenSignsPeriod.SealValidation:
            packet.writeD( SystemMessageIds.VALIDATION_PERIOD )
            break

        default:
            packet.writeD( SystemMessageIds.INITIAL_PERIOD )
            break
    }

    switch ( currentPeriod ) {
        case SevenSignsPeriod.Recruiting:
        case SevenSignsPeriod.CompetitionResults:
            packet.writeD( SystemMessageIds.UNTIL_TODAY_6PM )
            break

        case SevenSignsPeriod.Competing:
        case SevenSignsPeriod.SealValidation:
            packet.writeD( SystemMessageIds.UNTIL_MONDAY_6PM )
            break

        default:
            packet.writeD( SystemMessageIds.UNTIL_TODAY_6PM )
            break
    }

    packet
            .writeC( SevenSigns.getPlayerSide( objectId ) )
            .writeC( SevenSigns.getPlayerSeal( objectId ) )
            .writeQ( SevenSigns.getPlayerStoneContribution( objectId ) ) // Seal Stones Turned-In
            .writeQ( SevenSigns.getPlayerAdenaCollect( objectId ) ) // Ancient Adena to Collect

    let dawnStoneScore = SevenSigns.getCurrentStoneScore( SevenSignsSide.Dawn )
    let dawnFestivalScore = SevenSignsFestival.getCurrentFestivalScore( SevenSignsSide.Dawn )

    let duskStoneScore = SevenSigns.getCurrentStoneScore( SevenSignsSide.Dusk )
    let duskFestivalScore = SevenSignsFestival.getCurrentFestivalScore( SevenSignsSide.Dusk )
    let totalStoneScore = duskStoneScore + dawnStoneScore

    /*
     * Scoring seems to be proportionate to a set base value, so base this on the maximum obtainable score from festivals, which is 500.
     */
    let duskStoneScoreProp = 0
    let dawnStoneScoreProp = 0

    if ( totalStoneScore !== 0 ) {
        duskStoneScoreProp = Math.floor( ( duskStoneScore / totalStoneScore ) * 500 )
        dawnStoneScoreProp = Math.floor( ( dawnStoneScore / totalStoneScore ) * 500 )
    }

    let duskTotalScore = SevenSigns.getCurrentScore( SevenSignsSide.Dusk )
    let dawnTotalScore = SevenSigns.getCurrentScore( SevenSignsSide.Dawn )
    let totalOverallScore = duskTotalScore + dawnTotalScore

    let dawnPercent = 0
    let duskPercent = 0
    if ( totalOverallScore !== 0 ) {
        dawnPercent = Math.floor( ( dawnTotalScore / totalOverallScore ) * 100 )
        duskPercent = Math.floor( ( duskTotalScore / totalOverallScore ) * 100 )
    }

    /* DUSK */
    packet
            .writeQ( duskStoneScoreProp ) // Seal Stone Score
            .writeQ( duskFestivalScore ) // Festival Score
            .writeQ( duskTotalScore ) // Total Score
            .writeC( duskPercent ) // Dusk %

            /* DAWN */
            .writeQ( dawnStoneScoreProp ) // Seal Stone Score
            .writeQ( dawnFestivalScore ) // Festival Score
            .writeQ( dawnTotalScore ) // Total Score
            .writeC( dawnPercent ) // Dawn %

    return packet.getBuffer()
}

async function showPageTwo(): Promise<Buffer> {
    let duskPlayerIds : Array<Array<number>> = AllFestivalLevels.map( ( level : SevenSignsFestivalLevel ) : Array<number> => {
        return SevenSignsFestival.getScoreData( SevenSignsSide.Dusk, level ).members
    } )

    let dawnPlayerIds : Array<Array<number>> = AllFestivalLevels.map( ( level : SevenSignsFestivalLevel ) : Array<number> => {
        return SevenSignsFestival.getScoreData( SevenSignsSide.Dawn, level ).members
    } )

    await CharacterNamesCache.prefetchNames( [ ... _.flatten( duskPlayerIds ), ... _.flatten( dawnPlayerIds ) ] )
    let nameSize = 0
    let duskPlayerNames : Array<Array<string>> = AllFestivalLevels.map( ( level : SevenSignsFestivalLevel, index: number ) : Array<string> => {
        let names = CharacterNamesCache.getCachedNames( duskPlayerIds[ index ] )

        nameSize += getStringArraySize( names )
        return names
    } )

    let dawnPlayerNames : Array<Array<string>> = AllFestivalLevels.map( ( level : SevenSignsFestivalLevel, index: number ) : Array<string> => {
        let names = CharacterNamesCache.getCachedNames( dawnPlayerIds[ index ] )

        nameSize += getStringArraySize( names )
        return names
    } )

    let packet = new DeclaredServerPacket( nameSize + 121 )
            .writeC( 0xFB )
            .writeC( 2 )
            .writeC( SevenSigns.getCurrentPeriod() )
            .writeH( 1 )
            .writeC( AllFestivalLevels.length ) // Total number of festivals

    AllFestivalLevels.forEach( ( level: SevenSignsFestivalLevel, index: number ) => {
        packet
            .writeC( level )
            .writeD( FestivalLevelScores[ level ] )

        let duskScore = SevenSignsFestival.getHighestScore( SevenSignsSide.Dusk, level )
        let dawnScore = SevenSignsFestival.getHighestScore( SevenSignsSide.Dawn, level )

        packet.writeQ( duskScore )

        let partyMembers: Array<string> = duskPlayerNames[ index ]
        packet.writeC( partyMembers.length )
        partyMembers.forEach( ( name: string ) => {
            packet.writeS( name )
        } )

        packet.writeQ( dawnScore )

        partyMembers = dawnPlayerNames[ index ]

        packet.writeC( partyMembers.length )
        partyMembers.forEach( ( name: string ) => {
            packet.writeS( name )
        } )
    } )

    return packet.getBuffer()
}

function showPageThree(): Buffer {
    let packet = new DeclaredServerPacket( 24 )
            .writeC( 0xFB )
            .writeC( 3 )
            .writeC( SevenSigns.getCurrentPeriod() )
            .writeC( 10 ) // Minimum limit for winning cabal to retain their seal
            .writeC( 35 ) // Minimum limit for winning cabal to claim a seal
            .writeC( 3 ) // Total number of seals

    let totalDuskMembers = SevenSigns.getTotalMembers( SevenSignsSide.Dusk )
    let totalDawnMembers = SevenSigns.getTotalMembers( SevenSignsSide.Dawn )

    _.times( 3, ( index: number ) => {
        let seal = index + 1 as SevenSignsSeal
        let dawnProportion = SevenSigns.getSealTotal( seal, SevenSignsSide.Dawn )
        let duskProportion = SevenSigns.getSealTotal( seal, SevenSignsSide.Dusk )

        packet
                .writeC( seal )
                .writeC( SevenSigns.getWinningSealSide( seal ) )

        if ( totalDuskMembers === 0 ) {
            if ( totalDawnMembers === 0 ) {
                packet
                        .writeC( 0 )
                        .writeC( 0 )
            } else {
                packet
                        .writeC( 0 )
                        .writeC( Math.floor( ( dawnProportion / totalDawnMembers ) * 100 ) )
            }

            return
        }

        if ( totalDawnMembers === 0 ) {
            packet
                    .writeC( Math.floor( ( duskProportion / totalDuskMembers ) * 100 ) )
                    .writeC( 0 )
        } else {
            packet
                    .writeC( Math.floor( ( duskProportion / totalDuskMembers ) * 100 ) )
                    .writeC( Math.floor( ( dawnProportion / totalDawnMembers ) * 100 ) )
        }
    } )

    return packet.getBuffer()
}

function showPageFour(): Buffer {
    let winningCabal = SevenSigns.getWinnerSide()
    let packet = new DeclaredServerPacket( 24 )
            .writeC( 0xFB )
            .writeC( 4 )
            .writeC( SevenSigns.getCurrentPeriod() )
            .writeC( winningCabal ) // Overall predicted winner
            .writeC( 3 ) // Total number of seals

    let totalDuskMembers = SevenSigns.getTotalMembers( SevenSignsSide.Dusk )
    let totalDawnMembers = SevenSigns.getTotalMembers( SevenSignsSide.Dawn )

    _.times( 3, ( index: number ) => {
        let seal = index + 1
        let dawnProportion = SevenSigns.getSealTotal( seal, SevenSignsSide.Dawn )
        let duskProportion = SevenSigns.getSealTotal( seal, SevenSignsSide.Dusk )
        let dawnPercent = Math.floor( ( dawnProportion / ( totalDawnMembers === 0 ? 1 : totalDawnMembers ) ) * 100 )
        let duskPercent = Math.floor( ( duskProportion / ( totalDuskMembers === 0 ? 1 : totalDuskMembers ) ) * 100 )
        let sealOwner = SevenSigns.getWinningSealSide( seal )

        packet.writeC( seal )

        switch ( sealOwner ) {
            case SevenSignsSide.None:
                switch ( winningCabal ) {
                    case SevenSignsSide.None:
                        packet
                                .writeC( SevenSignsSide.None )
                                .writeD( SystemMessageIds.COMPETITION_TIE_SEAL_NOT_AWARDED )
                        break
                    case SevenSignsSide.Dawn:
                        if ( dawnPercent >= 35 ) {
                            packet
                                    .writeC( SevenSignsSide.Dawn )
                                    .writeD( SystemMessageIds.SEAL_NOT_OWNED_35_MORE_VOTED )
                        } else {
                            packet
                                    .writeC( SevenSignsSide.None )
                                    .writeD( SystemMessageIds.SEAL_NOT_OWNED_35_LESS_VOTED )
                        }
                        break
                    case SevenSignsSide.Dusk:
                        if ( duskPercent >= 35 ) {
                            packet
                                    .writeC( SevenSignsSide.Dusk )
                                    .writeD( SystemMessageIds.SEAL_NOT_OWNED_35_MORE_VOTED )
                        } else {
                            packet
                                    .writeC( SevenSignsSide.None )
                                    .writeD( SystemMessageIds.SEAL_NOT_OWNED_35_LESS_VOTED )
                        }
                        break
                }
                break
            case SevenSignsSide.Dawn:
                switch ( winningCabal ) {
                    case SevenSignsSide.None:
                        if ( dawnPercent >= 10 ) {
                            packet
                                    .writeC( SevenSignsSide.Dawn )
                                    .writeD( SystemMessageIds.SEAL_OWNED_10_MORE_VOTED )
                        } else {
                            packet
                                    .writeC( SevenSignsSide.None )
                                    .writeD( SystemMessageIds.COMPETITION_TIE_SEAL_NOT_AWARDED )
                        }
                        break
                    case SevenSignsSide.Dawn:
                        if ( dawnPercent >= 10 ) {
                            packet
                                    .writeC( sealOwner )
                                    .writeD( SystemMessageIds.SEAL_OWNED_10_MORE_VOTED )
                        } else {
                            packet
                                    .writeC( SevenSignsSide.None )
                                    .writeD( SystemMessageIds.SEAL_OWNED_10_LESS_VOTED )
                        }
                        break
                    case SevenSignsSide.Dusk:
                        if ( duskPercent >= 35 ) {
                            packet
                                    .writeC( SevenSignsSide.Dusk )
                                    .writeD( SystemMessageIds.SEAL_NOT_OWNED_35_MORE_VOTED )
                        } else if ( dawnPercent >= 10 ) {
                            packet
                                    .writeC( SevenSignsSide.Dawn )
                                    .writeD( SystemMessageIds.SEAL_OWNED_10_MORE_VOTED )
                        } else {
                            packet
                                    .writeC( SevenSignsSide.None )
                                    .writeD( SystemMessageIds.SEAL_OWNED_10_LESS_VOTED )
                        }
                        break
                }
                break
            case SevenSignsSide.Dusk:
                switch ( winningCabal ) {
                    case SevenSignsSide.None:
                        if ( duskPercent >= 10 ) {
                            packet
                                    .writeC( SevenSignsSide.Dusk )
                                    .writeD( SystemMessageIds.SEAL_OWNED_10_MORE_VOTED )
                        } else {
                            packet
                                    .writeC( SevenSignsSide.None )
                                    .writeD( SystemMessageIds.COMPETITION_TIE_SEAL_NOT_AWARDED )
                        }
                        break
                    case SevenSignsSide.Dawn:
                        if ( dawnPercent >= 35 ) {
                            packet
                                    .writeC( SevenSignsSide.Dawn )
                                    .writeD( SystemMessageIds.SEAL_NOT_OWNED_35_MORE_VOTED )
                        } else if ( duskPercent >= 10 ) {
                            packet
                                    .writeC( sealOwner )
                                    .writeD( SystemMessageIds.SEAL_OWNED_10_MORE_VOTED )
                        } else {
                            packet
                                    .writeC( SevenSignsSide.None )
                                    .writeD( SystemMessageIds.SEAL_OWNED_10_LESS_VOTED )
                        }
                        break
                    case SevenSignsSide.Dusk:
                        if ( duskPercent >= 10 ) {
                            packet
                                    .writeC( sealOwner )
                                    .writeD( SystemMessageIds.SEAL_OWNED_10_MORE_VOTED )
                        } else {
                            packet
                                    .writeC( SevenSignsSide.None )
                                    .writeD( SystemMessageIds.SEAL_OWNED_10_LESS_VOTED )
                        }
                        break
                }
                break
        }
    } )

    return packet.getBuffer()
}