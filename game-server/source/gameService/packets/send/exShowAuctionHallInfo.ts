import { ClanHallManager } from '../../instancemanager/ClanHallManager'
import { AuctionableHall } from '../../models/entity/clanhall/AuctionableHall'
import { ClanCache } from '../../cache/ClanCache'
import {
    DeclaredServerPacket,
    EmptyString,
    EmptyStringSize,
    getStringSize
} from '../../../packets/DeclaredServerPacket'
import _ from 'lodash'
import { PacketHelper } from '../PacketVariables'

const emptyNameSize = EmptyStringSize * 2
let lastUpdateTime = -1
let staticPacket: Buffer = null

function getHallsSize( clanHalls: Array<AuctionableHall> ): number {
    return clanHalls.reduce( ( size: number, hall: AuctionableHall ): number => {
        let clan = ClanCache.getClan( hall.getOwnerId() )
        return size + 8 + ( clan ? ( getStringSize( clan.getName() ) + getStringSize( clan.getLeaderName() ) ) : emptyNameSize )
    }, 0 )
}

// TODO : consider caching packet until clan owner changes

export function ExShowAuctionHallInfo(): Buffer {
    if ( staticPacket && lastUpdateTime === ClanHallManager.getLastOwnerUpdateTime() ) {
        return PacketHelper.copyPacket( staticPacket )
    }

    let clanHalls = ClanHallManager.getAllClanHalls()

    let packet = new DeclaredServerPacket( 7 + getHallsSize( clanHalls ) )
            .writeC( 0xfe )
            .writeH( 0x16 )
            .writeD( _.size( clanHalls ) )

    clanHalls.forEach( ( hall: AuctionableHall ) => {
        let clan = ClanCache.getClan( hall.getOwnerId() )

        packet
                .writeD( hall.getId() )
                .writeS( clan ? clan.getName() : EmptyString )
                .writeS( clan ? clan.getLeaderName() : EmptyString )
                .writeD( hall.getGrade() > 0 ? 0x00 : 0x01 ) // 0 - auction 1 - war clanhall 2 - ETC (rainbow spring clanhall)
    } )

    return packet.getBuffer()
}