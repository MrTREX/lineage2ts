import { L2TrapInstance } from '../../models/actor/instance/L2TrapInstance'
import { L2Character } from '../../models/actor/L2Character'
import { AbnormalVisualEffect, AbnormalVisualEffectMap } from '../../models/skills/AbnormalVisualEffect'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'
import { Team } from '../../enums/Team'

const stealthEffect = AbnormalVisualEffectMap[ AbnormalVisualEffect.STEALTH ].mask

export function TrapInfo( trap: L2TrapInstance, attacker: L2Character ): Buffer {
    let isSummoned = trap.isShowSummonAnimation()
    let isAttackable = trap.isAutoAttackable( attacker )
    let rightHand = 0
    let leftHand = 0
    let collisionHeight = trap.getTemplate().getFCollisionHeight()
    let collisionRadius = trap.getTemplate().getFCollisionRadius()
    let name = trap.getTemplate().isUsingServerSideName() ? trap.getName() : ''
    let title = trap.getOwner() ? trap.getOwner().getName() : ''
    let magicAttackSpeed = trap.getMagicAttackSpeed()
    let powerAttackSpeed = trap.getPowerAttackSpeed()
    let moveMultiplier = trap.getMovementSpeedMultiplier()
    let runSpeed = Math.round( trap.getRunSpeed() / moveMultiplier )
    let walkSpeed = Math.round( trap.getWalkSpeed() / moveMultiplier )
    let swimRunSpeed = Math.round( trap.getSwimRunSpeed() / moveMultiplier )
    let swimWalkSpeed = Math.round( trap.getSwimWalkSpeed() / moveMultiplier )
    let flyRunSpeed = trap.isFlying() ? runSpeed : 0
    let flyWalkSpeed = trap.isFlying() ? walkSpeed : 0

    return new DeclaredServerPacket( 205 + getStringSize( name ) + getStringSize( title ) )
            .writeC( 0x0C )
            .writeD( trap.getObjectId() )
            .writeD( trap.getTemplate().infoId )
            .writeD( isAttackable ? 1 : 0 )

            .writeD( trap.getX() )
            .writeD( trap.getY() )
            .writeD( trap.getZ() )
            .writeD( trap.getHeading() )

            .writeD( 0 )
            .writeD( magicAttackSpeed )
            .writeD( powerAttackSpeed )
            .writeD( runSpeed )

            .writeD( walkSpeed )
            .writeD( swimRunSpeed )
            .writeD( swimWalkSpeed )
            .writeD( flyRunSpeed )

            .writeD( flyWalkSpeed )
            .writeD( flyRunSpeed )
            .writeD( flyWalkSpeed )
            .writeF( moveMultiplier )

            .writeF( trap.getAttackSpeedMultiplier() )
            .writeF( collisionRadius )
            .writeF( collisionHeight )
            .writeD( rightHand )

            .writeD( 0 )
            .writeD( leftHand )
            .writeC( 1 )
            .writeC( 1 )

            .writeC( trap.isInCombat() ? 1 : 0 )
            .writeC( trap.isAlikeDead() ? 1 : 0 )
            .writeC( isSummoned ? 2 : 0 )
            .writeD( -1 )

            .writeS( name )
            .writeD( -1 )
            .writeS( title )
            .writeD( 0 )

            .writeD( trap.getPvpFlag() )
            .writeD( trap.getKarma() )
            .writeD( trap.isInvisible() ? trap.getAbnormalVisualEffects() | stealthEffect : trap.getAbnormalVisualEffects() )
            .writeD( 0 ) // clan id

            .writeD( 0 ) // crest id
            .writeD( 0 )
            .writeD( 0 )
            .writeD( 0 )

            .writeC( Team.None )
            .writeF( collisionRadius )
            .writeF( collisionHeight )
            .writeD( 0 )

            .writeD( 0 )
            .writeD( 0 )
            .writeD( 0 )
            .writeC( 0x01 )

            .writeC( 0x01 )
            .writeD( 0 )
            .getBuffer()

}