import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { PlayerActionOverride } from '../../values/PlayerConditions'
import { ConfigManager } from '../../../config/ConfigManager'
import { ItemPacketHelper } from './helpers/ItemPacketHelper'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export function TradeStart( player: L2PcInstance ): Buffer {
    let allowRestrictedItems: boolean = player.hasActionOverride( PlayerActionOverride.ItemAction ) && ConfigManager.general.gmTradeRestrictedItems()
    let items: Array<L2ItemInstance> = player.getInventory().getAvailableItems( true, allowRestrictedItems, false, true )

    let allItemsSize: number = items.length * ItemPacketHelper.writeItemSize()
    let packet = new DeclaredServerPacket( 7 + allItemsSize )
            .writeC( 0x14 )
            .writeD( player.getActiveTradeList().getPartnerId() )
            .writeH( items.length )

    items.forEach( ( item: L2ItemInstance ) => {
        ItemPacketHelper.writeItem( packet, item )
    } )

    return packet.getBuffer()
}