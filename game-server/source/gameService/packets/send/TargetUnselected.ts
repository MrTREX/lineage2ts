import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export function TargetUnselected( player: L2PcInstance ): Buffer {
    return new DeclaredServerPacket( 21 )
            .writeC( 0x24 )
            .writeD( player.getObjectId() )
            .writeD( player.getX() )
            .writeD( player.getY() )

            .writeD( player.getZ() )
            .writeD( 0 )
            .getBuffer()
}