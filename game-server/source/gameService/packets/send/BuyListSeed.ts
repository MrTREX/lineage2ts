import { CastleManorManager } from '../../instancemanager/CastleManorManager'
import { SeedProduction } from '../../models/manor/SeedProduction'
import { L2World } from '../../L2World'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function BuyListSeed( playerId: number, castleId: number ): Buffer {
    let player = L2World.getPlayer( playerId )
    let availableItems: Array<SeedProduction> = CastleManorManager.getSeedProduction( castleId, false ).filter( ( production: SeedProduction ) => {
        return production.getAmount() > 0 && production.getPrice() > 0
    } )

    let packet = new DeclaredServerPacket( 17 + availableItems.length * 76 )
            .writeC( 0xe9 )
            .writeQ( player.getAdena() )
            .writeD( castleId )
            .writeD( availableItems.length )

    availableItems.forEach( ( production: SeedProduction ) => {
        packet
                .writeD( production.getId() )
                .writeD( production.getId() )
                .writeD( 0x00 )
                .writeQ( production.getAmount() ) // item count

                .writeH( 0x05 ) // Custom Type 2
                .writeH( 0x00 ) // Custom Type 1
                .writeH( 0x00 ) // Equipped
                .writeD( 0x00 ) // Body Part

                .writeH( 0x00 ) // Enchant
                .writeH( 0x00 ) // Custom Type
                .writeD( 0x00 ) // Augment
                .writeD( -1 ) // Mana

                .writeD( -9999 ) // Time
                .writeH( 0x00 ) // Element Type
                .writeH( 0x00 ) // Element Power
                .writeD( 0 )

                .writeD( 0 )
                .writeD( 0 )
                .writeQ( production.getPrice() ) // price
    } )

    return packet.getBuffer()
}