import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExSubPledgeSkillAdd( type: number, id: number, level: number ) : Buffer {
    return new DeclaredServerPacket( 15 )
            .writeC( 0xFE )
            .writeH( 0x76 )
            .writeD( type )
            .writeD( id )

            .writeD( level )
            .getBuffer()
}