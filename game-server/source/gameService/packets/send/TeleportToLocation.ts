import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function TeleportToLocation( objectId: number, x: number, y: number, z: number, heading: number ) : Buffer {
    return new DeclaredServerPacket( 25 )
        .writeC( 0x22 )
        .writeD( objectId )
        .writeD( x )
        .writeD( y )

        .writeD( z )
        .writeD( 0x00 )
        .writeD( heading )
        .getBuffer()
}