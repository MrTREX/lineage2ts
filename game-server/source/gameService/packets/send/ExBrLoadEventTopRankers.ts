import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExBrLoadEventTopRankers( eventId: number, day: number, count: number, bestScore: number, ownScore: number ) : Buffer {
    return new DeclaredServerPacket( 23 )
            .writeC( 0xFE )
            .writeH( 0xBD )
            .writeD( eventId )
            .writeD( day )

            .writeD( count )
            .writeD( bestScore )
            .writeD( ownScore )
            .getBuffer()
}