import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { CastleManorManager } from '../../instancemanager/CastleManorManager'
import { CropProcure } from '../../models/manor/CropProcure'
import { L2Seed } from '../../models/L2Seed'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import _ from 'lodash'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export function ExShowSellCropList( player: L2PcInstance, manorId: number ): Buffer {
    let cropItems: { [ key: number ]: L2ItemInstance } = CastleManorManager.getCropIds().reduce( ( finalMap, id: number ) => {

        let item: L2ItemInstance = player.getInventory().getItemByItemId( id )
        if ( item ) {
            finalMap[ id ] = item
        }

        return finalMap
    }, {} )

    let castleCrops: { [ key: number ]: CropProcure } = CastleManorManager.getAllCropProcure( manorId, false ).reduce( ( finalMap, crop: CropProcure ) => {
        if ( cropItems[ crop.getId() ] && crop.getAmount() > 0 ) {
            finalMap[ crop.getId() ] = crop
        }

        return finalMap
    }, {} )

    let cropSize = _.size( cropItems )
    let packet = new DeclaredServerPacket( 7 + cropSize * 51 )
            .writeC( 0xFE )
            .writeH( 0x2C )
            .writeD( manorId )
            .writeD( cropSize )

    _.each( cropItems, ( item: L2ItemInstance ) => {
        let seed: L2Seed = CastleManorManager.getSeedByCrop( item.getId() )

        packet
                .writeD( item.getObjectId() ) // Object id
                .writeD( item.getId() ) // crop id
                .writeD( seed.getLevel() ) // seed level
                .writeC( 0x01 )

                .writeD( seed.getReward( 1 ) ) // reward 1 id
                .writeC( 0x01 )
                .writeD( seed.getReward( 2 ) ) // reward 2 id

        if ( castleCrops[ item.getId() ] ) {
            let crop: CropProcure = castleCrops[ item.getId() ]

            packet
                    .writeD( manorId ) // manor
                    .writeQ( crop.getAmount() ) // buy residual
                    .writeQ( crop.getPrice() ) // buy price
                    .writeC( crop.getReward() ) // reward
        } else {
            packet
                    .writeD( 0xFFFFFFFF ) // manor
                    .writeQ( 0x00 ) // buy residual
                    .writeQ( 0x00 ) // buy price
                    .writeC( 0x00 ) // reward
        }

        packet.writeQ( item.getCount() )
    } )

    return packet.getBuffer()
}