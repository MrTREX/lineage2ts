import { L2World } from '../../L2World'
import { PartyMatchManager, PartyMatchRoom } from '../../cache/partyMatchManager'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'
import { CharacterNamesCache } from '../../cache/CharacterNamesCache'

const defaultName = 'Not Available'

function getPartyMemberSize( total: number, memberId: number ): number {
    let name = CharacterNamesCache.getCachedNameById( memberId )
    return total + 4 + getStringSize( name ? name : defaultName )
}

function getLeaderName( room: PartyMatchRoom ): string {
    return CharacterNamesCache.getCachedNameById( room.leaderId )
}

export function ListPartyWaiting( locationId: number, useLevel: boolean, level: number ): Buffer {
    let rooms: Array<PartyMatchRoom> = PartyMatchManager.getAvailableRooms( locationId, useLevel, level )

    let allRoomsSize = rooms.reduce( ( size: number, room: PartyMatchRoom ): number => {
        let partyMembersSize = Array.from( room.members ).reduce( getPartyMemberSize, 0 )

        return size + partyMembersSize + getStringSize( room.title ) + getStringSize( getLeaderName( room ) ) + 24
    }, 0 )

    let packet = new DeclaredServerPacket( 9 + allRoomsSize )
            .writeC( 0x9C )
            .writeD( rooms.length > 0 ? 1 : 0 )
            .writeD( rooms.length )

    rooms.forEach( ( room: PartyMatchRoom ) => {
        packet
                .writeD( room.id )
                .writeS( room.title )
                .writeD( PartyMatchManager.getRoomLocation( room ) )
                .writeD( room.minimumLevel )

                .writeD( room.maximumLevel )
                .writeD( room.maximumMembers )
                .writeS( getLeaderName( room ) )
                .writeD( room.members.size )

        room.members.forEach( ( memberId: number ) => {
            let member = L2World.getPlayer( memberId )

            if ( member ) {
                packet
                        .writeD( member.getClassId() )
                        .writeS( member.getName() )
                return
            }

            packet
                    .writeD( 0 )
                    .writeS( 'Not Available' )
        } )
    } )

    return packet.getBuffer()
}