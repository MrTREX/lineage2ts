import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

export function Snoop( id: number, name: string, type: number, speaker: string, message: string ) : Buffer {
    let dynamicSize : number = getStringSize( name ) + getStringSize( speaker ) + getStringSize( message )
    return new DeclaredServerPacket( 13 + dynamicSize )
        .writeC( 0xdb )
        .writeD( id )
        .writeS( name )
        .writeD( 0x00 )

        .writeD( type )
        .writeS( speaker )
        .writeS( message )
        .getBuffer()
}