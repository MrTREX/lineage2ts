import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExNevitAdventTimeChange( time: number ) : Buffer {
    let timeLeft = Math.min( time, 240000 )
    let paused = time < 1

    return new DeclaredServerPacket( 8 )
        .writeC( 0xFE )
        .writeH( 0xE1 )
        .writeC( paused ? 0x00 : 0x01 ) // state 0 - pause 1 - started
        .writeD( timeLeft ) // left time in ms max is 16000 its 4m and state is automatically changed to quit
        .getBuffer()
}