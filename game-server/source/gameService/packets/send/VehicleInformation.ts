import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2Vehicle } from '../../models/actor/L2Vehicle'

export function VehicleInformation( boat: L2Vehicle ): Buffer {
    return new DeclaredServerPacket( 21 )
            .writeC( 0x60 )
            .writeD( boat.getObjectId() )
            .writeD( boat.getX() )
            .writeD( boat.getY() )

            .writeD( boat.getZ() )
            .writeD( boat.getHeading() )
            .getBuffer()
}