import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2Object } from '../../models/L2Object'

export function MoveToTargetCharacter( main: L2Object, target: L2Object, distance: number ) : Buffer {
    return new DeclaredServerPacket( 37 )
            .writeC( 0x72 )
            .writeD( main.getObjectId() )
            .writeD( target.getObjectId() )
            .writeD( distance )

            .writeD( main.getX() )
            .writeD( main.getY() )
            .writeD( main.getZ() )

            .writeD( target.getX() )
            .writeD( target.getY() )
            .writeD( target.getZ() )
            .getBuffer()
}