import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

export function ExConfirmAddingContact( name: string, isAdded: boolean ) {
    return new DeclaredServerPacket( 7 + getStringSize( name ) )
            .writeC( 0xFE )
            .writeH( 0xD2 )
            .writeS( name )
            .writeD( isAdded ? 1 : 0 )

            .getBuffer()
}