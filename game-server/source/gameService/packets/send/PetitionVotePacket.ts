import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function PetitionVotePacket(): Buffer {
    return new DeclaredServerPacket( 1 ).writeC( 0xFC ).getBuffer()
}