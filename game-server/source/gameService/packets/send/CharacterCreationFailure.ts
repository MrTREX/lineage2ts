import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export const enum CreationFailureReason {
    creationFailed,
    /** You cannot create another character. Please delete the existing character and try again." Removes all settings that were selected (race, class, etc). */
    tooManyCharacters,
    /** This name already exists. */
    nameAlreadyExists,
    /** Your title cannot exceed 16 characters in length. Please try again. */
    nameTooLong,
    /** Incorrect name. Please try again. */
    badNaming,
    /** Characters cannot be created from this server. */
    creationDenied,
    /**
     * Unable to create character. You are unable to create a new character on the selected server. A restriction is in place which restricts users from creating characters on different servers where no previous character exists. Please choose another server.
     */
    otherReason,
}

export function CharacterCreationFailure( reason: CreationFailureReason ): Buffer {
    return new DeclaredServerPacket( 5 )
            .writeC( 0x10 )
            .writeD( reason )
            .getBuffer()
}