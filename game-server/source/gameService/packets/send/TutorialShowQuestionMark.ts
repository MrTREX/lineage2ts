import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function TutorialShowQuestionMark( id: number ) : Buffer {
    return new DeclaredServerPacket( 5 )
            .writeC( 0xA7 )
            .writeD( id )
            .getBuffer()
}