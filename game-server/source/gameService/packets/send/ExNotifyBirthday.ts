import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExNotifyBirthday() : Buffer {
    return new DeclaredServerPacket( 3 )
        .writeC( 0xFE )
        .writeH( 0x8F )
        .getBuffer()
}