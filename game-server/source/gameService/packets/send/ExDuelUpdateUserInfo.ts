import { L2World } from '../../L2World'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

export function ExDuelUpdateUserInfo( playerId: number ) : Buffer {
    let player = L2World.getPlayer( playerId )

    return new DeclaredServerPacket( 39 + getStringSize( player.getName() ) )
        .writeC( 0xFE )
        .writeH( 0x50 )
        .writeS( player.getName() )
        .writeD( playerId )

        .writeD( player.getClassId() )
        .writeD( player.getLevel() )
        .writeD( player.getCurrentHp() )
        .writeD( player.getMaxHp() )

        .writeD( player.getCurrentMp() )
        .writeD( player.getMaxMp() )
        .writeD( player.getCurrentCp() )
        .writeD( player.getMaxCp() )
        .getBuffer()
}