import { DeclaredServerPacket, getStringArraySize, getStringSize } from '../../../packets/DeclaredServerPacket'

export const enum ExShowScreenMessagePosition {
    TopLeft = 0X01,
    TopCenter = 0x02,
    TopRight = 0X03,
    MiddleLeft = 0X04,
    MiddleCenter = 0X05,
    MiddleRight = 0X06,
    BottomCenter = 0X07,
    BottomRight = 0X08,
}

export const enum ExShowScreenMessageSize {
    Normal = 0x00,
    Small = 0x01,
}

const defaultParameters = []

export class ExShowScreenMessage {
    type: number
    systemMessageId: number
    unknownOne: number = 0
    fontSize: number = 0
    unknownThree: number = 0
    fade: boolean
    size: number
    position: number
    effect: boolean
    text: string
    time: number
    npcString: number
    parameters: Array<string>

    static fromNpcMessageId( messageId: number, position: number, time: number, parameters: Array<string> ): Buffer {
        let packet = new ExShowScreenMessage()

        packet.type = 2
        packet.systemMessageId = -1
        packet.unknownOne = 0
        packet.fontSize = 0

        packet.unknownThree = 0
        packet.fade = false
        packet.position = position
        packet.text = null

        packet.time = time
        packet.size = ExShowScreenMessageSize.Normal
        packet.effect = false
        packet.npcString = messageId

        packet.parameters = parameters ?? defaultParameters

        return packet.getBuffer()
    }

    getBuffer(): Buffer {
        let packet = new DeclaredServerPacket( 47 + ( this.npcString === -1 ? getStringSize( this.text ) : getStringArraySize( this.parameters ) ) )
                .writeC( 0xFE )
                .writeH( 0x39 )
                .writeD( this.type )
                .writeD( this.systemMessageId )

                .writeD( this.position )
                .writeD( this.unknownOne )
                .writeD( this.size )
                .writeD( this.fontSize )

                .writeD( this.unknownThree )
                .writeD( this.effect ? 0x01 : 0x00 )
                .writeD( this.time )
                .writeD( this.fade ? 0x01 : 0x00 )

                .writeD( this.npcString )

        if ( this.npcString === -1 ) {
            packet.writeS( this.text )
        } else {
            this.parameters.forEach( ( line: string ) => {
                packet.writeS( line )
            } )
        }

        return packet.getBuffer()
    }
}

export function ExShowScreenMessageFromText( text: string, time: number ): Buffer {
    let packet = new ExShowScreenMessage()

    packet.type = 2
    packet.systemMessageId = -1
    packet.unknownOne = 0
    packet.fontSize = 0

    packet.unknownThree = 0
    packet.fade = false
    packet.position = ExShowScreenMessagePosition.TopCenter
    packet.text = text

    packet.time = time
    packet.size = ExShowScreenMessageSize.Normal
    packet.effect = false
    packet.npcString = -1

    packet.parameters = defaultParameters

    return packet.getBuffer()
}