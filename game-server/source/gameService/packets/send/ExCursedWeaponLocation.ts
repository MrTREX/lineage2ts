import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { CursedWeaponManager } from '../../instancemanager/CursedWeaponManager'
import { CursedWeapon } from '../../models/CursedWeapon'
import { ILocational } from '../../models/Location'

// TODO : consider caching and regenerating packet on two/five minute intervals
export function ExCursedWeaponLocation(): Buffer {
    let size = Object.values( CursedWeaponManager.getCursedWeapons() ).reduce( ( amount: number, weapon: CursedWeapon ): number => {
        if ( weapon.isActive() && weapon.getWorldPosition() ) {
            amount++
        }

        return amount
    }, 0 )

    let dynamicSize = size === 0 ? 4 : size * 20
    let packet = new DeclaredServerPacket( 5 + dynamicSize )
            .writeC( 0xFE )
            .writeH( 0x47 )
            .writeH( size )

    if ( size > 0 ) {
        Object.values( CursedWeaponManager.getCursedWeapons() ).forEach( ( weapon: CursedWeapon ) => {
            let position: ILocational = weapon.getWorldPosition()
            if ( !weapon.isActive() || !position ) {
                return
            }

            packet
                    .writeD( weapon.getItemId() )
                    .writeD( weapon.isActivated() ? 1 : 0 )
                    .writeD( position.getX() )
                    .writeD( position.getY() )
                    .writeD( position.getZ() )
        } )
    } else {
        packet.writeD( 0 )
    }

    return packet.getBuffer()
}