import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExBRAgathionEnergyInfo( agathions: Array<L2ItemInstance> ): Buffer {
    let packet = new DeclaredServerPacket( 7 + agathions.length * 20 )
            .writeC( 0xFE )
            .writeH( 0xDE )
            .writeD( agathions.length )

    agathions.forEach( ( item: L2ItemInstance ) => {
        packet
                .writeD( item.getObjectId() )
                .writeD( item.getId() )
                .writeD( 0x200000 )
                .writeD( item.getAgathionRemainingEnergy() )
                .writeD( item.agathion.energy )
    } )

    return packet.getBuffer()
}

export function ExBRAgathionEnergyItem( item: L2ItemInstance ): Buffer {
    let packet = new DeclaredServerPacket( 7 + 20 )
        .writeC( 0xFE )
        .writeH( 0xDE )
        .writeD( 1 )

    packet
        .writeD( item.getObjectId() )
        .writeD( item.getId() )
        .writeD( 0x200000 )
        .writeD( item.getAgathionRemainingEnergy() )
        .writeD( item.agathion.energy )

    return packet.getBuffer()
}