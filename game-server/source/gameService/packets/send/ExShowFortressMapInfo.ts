import { Fort } from '../../models/entity/Fort'
import { FortSiegeManager } from '../../instancemanager/FortSiegeManager'
import { FortSiegeSpawn } from '../../models/FortSiegeSpawn'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import _ from 'lodash'
import { ISpawnLogic } from '../../models/spawns/ISpawnLogic'

export function ExShowFortressMapInfo( fort: Fort ): Buffer {
    let spawns: Array<FortSiegeSpawn> = FortSiegeManager.getCommanderSpawnList( fort.getResidenceId() )

    let packet = new DeclaredServerPacket( 15 + fort.getFortSize() * 4 )
            .writeC( 0xFE )
            .writeH( 0x7D )
            .writeD( fort.getResidenceId() )
            .writeD( fort.getSiege().isInProgress() ? 1 : 0 ) // fortress siege status

            .writeD( fort.getFortSize() ) // barracks count


    if ( spawns?.length > 0 && fort.getSiege().isInProgress() ) {
        switch ( spawns.length ) {
            case 3:
                spawns.forEach( ( spawn: FortSiegeSpawn ) => {
                    packet.writeD( isSpawned( fort, spawn.getId() ) ? 0 : 1 )
                } )

                _.times( fort.getFortSize() - spawns.length, () => {
                    packet.writeD( 0 )
                } )

                break
                // TODO: change 4 to 5 once control room supported

            case 5:
                spawns.forEach( ( spawn: FortSiegeSpawn ) => {
                    packet.writeD( isSpawned( fort, spawn.getId() ) ? 0 : 1 )
                } )

                break

            default:
                _.times( fort.getFortSize(), () => {
                    packet.writeD( 0 )
                } )
                break
        }
    } else {
        _.times( fort.getFortSize(), () => {
            packet.writeD( 0 )
        } )
    }

    return packet.getBuffer()
}

function isSpawned( fort: Fort, npcId: number ): boolean {
    return fort.getSiege().getCommanders().some( ( spawn: ISpawnLogic ) => {
        return spawn.getTemplate().getId() === npcId
    } )
}