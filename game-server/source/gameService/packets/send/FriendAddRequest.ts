import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

export function FriendAddRequest( name: string ) : Buffer {
    return new DeclaredServerPacket( 5 + getStringSize( name ) )
            .writeC( 0x83 )
            .writeS( name )
            .writeD( 0 )
            .getBuffer()
}