import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

function getPledgeSize( subPledgeName: string, pledgeType: number, pledgeName: string ): number {
    let size = subPledgeName ? getStringSize( pledgeType > 0 ? subPledgeName : pledgeName ) : 0

    if ( pledgeType !== 0 ) {
        size += 4
    }

    return size
}

export function AskJoinPledge( objectId: number, subPledgeName: string, pledgeType: number, pledgeName: string ): Buffer {
    let packet = new DeclaredServerPacket( 5 + getStringSize( pledgeName ) + getPledgeSize( subPledgeName, pledgeType, pledgeName ) )
            .writeC( 0x2C )
            .writeD( objectId )

    if ( subPledgeName ) {
        packet.writeS( pledgeType > 0 ? subPledgeName : pledgeName )
    }

    if ( pledgeType !== 0 ) {
        packet.writeD( pledgeType )
    }

    packet.writeS( pledgeName )

    return packet.getBuffer()
}