import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { TradeItem, TradeItemHelper } from '../../models/TradeItem'
import { ItemPacketHelper } from './helpers/ItemPacketHelper'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function PrivateStoreListBuy( buyer: L2PcInstance, seller: L2PcInstance ): Buffer {
    seller.getBuyList().updateItems()

    let items: Array<TradeItem> = seller.getBuyList().getAvailableItems( buyer.getInventory() )
    let allItemsSize: number = items.reduce( ( size: number, item: TradeItem ): number => {
        return size + ItemPacketHelper.writeTradeItemSize( item ) + 28
    }, 0 )

    let packet = new DeclaredServerPacket( 17 + allItemsSize )
            .writeC( 0xBE )
            .writeD( seller.getObjectId() )
            .writeQ( buyer.getAdena() )
            .writeD( items.length )

    items.forEach( ( tradeItem: TradeItem ) => {
        ItemPacketHelper.writeTradeItem( packet, tradeItem )
        packet
                .writeD( tradeItem.objectId )
                .writeQ( tradeItem.price )
                .writeQ( tradeItem.itemTemplate.getDoubleReferencePrice() )
                .writeQ( tradeItem.storeCount )

        TradeItemHelper.recycle( tradeItem )
    } )

    return packet.getBuffer()
}