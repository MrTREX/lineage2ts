import { L2World } from '../../L2World'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExBrExtraUserInfo( objectId: number ) : Buffer {
    let player = L2World.getPlayer( objectId )

    return new DeclaredServerPacket( 12 )
        .writeC( 0xFE )
        .writeH( 0xDA )
        .writeD( objectId )
        .writeD( player.getAbnormalVisualEffectEvent() )
        .writeC( 0 )
        .getBuffer()
}