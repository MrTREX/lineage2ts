import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

export function PartySmallWindowUpdate( player: L2PcInstance ) : Buffer {
    return new DeclaredServerPacket( 37 + getStringSize( player.getName() ) )
        .writeC( 0x52 )
        .writeD( player.getObjectId() )
        .writeS( player.getName() )

        .writeD( player.getCurrentCp() )
        .writeD( player.getMaxCp() )
        .writeD( player.getCurrentHp() )
        .writeD( player.getMaxHp() )

        .writeD( player.getCurrentMp() )
        .writeD( player.getMaxMp() )
        .writeD( player.getLevel() )
        .writeD( player.getClassId() )

        .getBuffer()
}