import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export const enum ExBrBuyProductResult {
    Success = 1,
    NotEnoughPoints = -1,
    WrongProduct = -2,
    InventoryFull = -4,
    WrongProduct2 = -5,
    SalePeriodEnded = -7,
    SalePeriodEnded2 = -8,
    WrongUserState = -9,
    WrongProductItem = -10,
    WrongUserState2 = -11,
}

export function ExBrBuyProductResponse( result: ExBrBuyProductResult ) : Buffer {
    return new DeclaredServerPacket( 7 )
        .writeC( 0xFE )
        .writeH( 0xD8 )
        .writeD( result )
        .getBuffer()
}