import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { DataManager } from '../../../data/manager'
import { L2GamePointProduct } from '../../../data/interface/GamePointsProductsDataApi'

export const enum ExBrProductItemType {
    Restricted = 0,
    ActionFree = 1
}

// TODO : generate packet and use cached value to avoid generating packet at runtime
export function ExBrProductInfo( product : L2GamePointProduct ) : Buffer {
    let packet = new DeclaredServerPacket( 15 + product.items.length * 16 )
        .writeC( 0xFE )
        .writeH( 0xD7 )
        .writeD( product.id )
        .writeD( product.price )
        .writeD( product.items.length )

    for ( const productItem of product.items ) {
        let item = DataManager.getItems().getTemplate( productItem.id )
        packet
            .writeD( productItem.id )
            .writeD( productItem.amount )
            .writeD( item.getWeight() )
            .writeD( item.isDropable() && item.isTradeable() ? ExBrProductItemType.ActionFree : ExBrProductItemType.Restricted )
    }

    return packet.getBuffer()
}