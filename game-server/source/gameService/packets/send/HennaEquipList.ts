import { L2Henna } from '../../models/items/L2Henna'
import { L2World } from '../../L2World'
import { DataManager } from '../../../data/manager'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function HennaEquipList( objectId: number ): Buffer {
    let player = L2World.getPlayer( objectId )

    let items: Array<L2Henna> = DataManager.getHennas().getAll( player.getClassId() ).filter( ( henna: L2Henna ) => {
        return player.getInventory().hasItemWithId( henna.getDyeItemId() )
    } )

    let packet = new DeclaredServerPacket( 17 + items.length * 28 )
            .writeC( 0xEE )
            .writeQ( player.getAdena() )
            .writeD( 3 )
            .writeD( items.length )

    items.forEach( ( henna: L2Henna ) => {
        packet
                .writeD( henna.getDyeId() )
                .writeD( henna.getDyeItemId() )
                .writeQ( henna.getWearCount() )
                .writeQ( henna.getWearFee() )

                .writeD( henna.isAllowedClass( player.getClassId() ) ? 0x01 : 0x00 )
    } )

    return packet.getBuffer()
}