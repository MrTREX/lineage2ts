import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExPutItemResultForVariationCancel( item: L2ItemInstance, price: number ) : Buffer {
    return new DeclaredServerPacket( 35 )
            .writeC( 0xFE )
            .writeH( 0x57 )
            .writeD( item.getObjectId() )
            .writeD( item.getId() )

            .writeD( item.getAugmentation().getAugmentationId() )
            .writeD( item.getAugmentation().getAugmentationId() >> 16 )
            .writeQ( price )
            .writeD( 0x01 )

            .getBuffer()
}