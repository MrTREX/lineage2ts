import { Castle } from '../../models/entity/Castle'
import { L2Clan } from '../../models/L2Clan'
import { L2SiegeClan } from '../../models/L2SiegeClan'
import { ClanCache } from '../../cache/ClanCache'
import { DeclaredServerPacket, EmptyString, EmptyStringSize, getStringSize } from '../../../packets/DeclaredServerPacket'
import { SiegeClanType } from '../../enums/SiegeClanType'

export function SiegeDefenderList( castle: Castle ): Buffer {
    let defenderClans: Array<[ L2Clan, SiegeClanType ]> = []
    let waitingClans: Array<L2Clan> = []
    let size: number = 0

    castle.getSiege().getDefenderClans().forEach( ( siegeClan: L2SiegeClan ) => {
        let clan: L2Clan = ClanCache.getClan( siegeClan.getClanId() )
        if ( clan ) {
            defenderClans.push( [ clan, siegeClan.getType() ] )
            size += 24 + getStringSize( clan.getName() ) + getStringSize( clan.getLeaderName() ) + getStringSize( clan.getAllyName() ) + EmptyStringSize
        }
    } )

    castle.getSiege().getDefenderWaitingClans().forEach( ( siegeClan: L2SiegeClan ) => {
        let clan: L2Clan = ClanCache.getClan( siegeClan.getClanId() )
        if ( clan ) {
            waitingClans.push( clan )
            size += 24 + getStringSize( clan.getName() ) + getStringSize( clan.getLeaderName() ) + getStringSize( clan.getAllyName() ) + EmptyStringSize
        }
    } )

    let totalSize = defenderClans.length + waitingClans.length

    let packet = new DeclaredServerPacket( 25 + size )
            .writeC( 0xCB )
            .writeD( castle.getResidenceId() )
            .writeD( 0 )
            .writeD( 1 )

            .writeD( 0 )
            .writeD( totalSize )
            .writeD( totalSize )

    defenderClans.forEach( ( clanInfo: [ L2Clan, SiegeClanType ] ) => {
        let [ clan, type ] = clanInfo

        packet
                .writeD( clan.getId() )
                .writeS( clan.getName() )
                .writeS( clan.getLeaderName() )
                .writeD( clan.getCrestId() )

                .writeD( 0x00 ) // signed time (seconds) (not stored)
                .writeD( type )
                .writeD( clan.getAllyId() )
                .writeS( clan.getAllyName() )

                .writeS( EmptyString ) // AllyLeaderName
                .writeD( clan.getAllyCrestId() )
    } )

    waitingClans.forEach( ( clan: L2Clan ) => {
        packet
                .writeD( clan.getId() )
                .writeS( clan.getName() )
                .writeS( clan.getLeaderName() )
                .writeD( clan.getCrestId() )

                .writeD( 0x00 ) // signed time (seconds) (not stored)
                .writeD( SiegeClanType.DefenderPending )
                .writeD( clan.getAllyId() )
                .writeS( clan.getAllyName() )

                .writeS( EmptyString ) // AllyLeaderName
                .writeD( clan.getAllyCrestId() )
    } )

    return packet.getBuffer()
}