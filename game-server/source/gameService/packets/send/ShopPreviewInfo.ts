import { InventorySlot } from '../../values/InventoryValues'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import _ from 'lodash'

/*
    Paperdoll order is specific to current packet.
 */
const Paperdoll: Array<InventorySlot> = [
    InventorySlot.Under,
    InventorySlot.RightEar,
    InventorySlot.LeftEar,
    InventorySlot.Neck,
    InventorySlot.RightFinger,
    InventorySlot.LeftFinger,
    InventorySlot.Head,
    InventorySlot.RightHand,
    InventorySlot.LeftHand,
    InventorySlot.Gloves,
    InventorySlot.Chest,
    InventorySlot.Legs,
    InventorySlot.Feet,
    InventorySlot.Cloak,
    InventorySlot.RightHand,
    InventorySlot.Hair,
    InventorySlot.HairSecondary,
    InventorySlot.RightBracelet,
    InventorySlot.LeftBracelet,
]

export function ShopPreviewInfo( itemListMap: Record<number, number> ): Buffer {
    let packet = new DeclaredServerPacket( 5 + Paperdoll.length * 4 )
            .writeC( 0xF6 )
            .writeD( Paperdoll.length )

    Paperdoll.forEach( ( slot: InventorySlot ) => {
        packet.writeD( _.defaultTo( itemListMap[ slot ], 0 ) )
    } )


    return packet.getBuffer()
}