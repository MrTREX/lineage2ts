import { L2World } from '../../L2World'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'
import { PartyMatchManager, PartyMatchRoom } from '../../cache/partyMatchManager'
import { PlayerGroupCache } from '../../cache/PlayerGroupCache'

const enum PartyRoomMember {
    DifferentParties,
    Leader,
    Other
}

export const enum PartyRoomMemberOperation {
    Add,
    PromoteLeader,
    Remove
}

function getPartyValue( room: PartyMatchRoom, playerId: number ): number {
    let leaderId = room.leaderId
    if ( leaderId === playerId ) {
        return PartyRoomMember.Leader
    }

    let leaderParty = PlayerGroupCache.getParty( leaderId )
    let memberParty = PlayerGroupCache.getParty( playerId )

    if ( !leaderParty || !memberParty || leaderParty.getLeaderObjectId() !== memberParty.getLeaderObjectId() ) {
        return PartyRoomMember.DifferentParties
    }

    return PartyRoomMember.Other
}

export function ExManagePartyRoomMember( objectId: number, room: PartyMatchRoom, mode: PartyRoomMemberOperation ): Buffer {
    let player = L2World.getPlayer( objectId )

    return new DeclaredServerPacket( 28 + getStringSize( player.getName() ) )
            .writeC( 0xFE )
            .writeH( 0x0A )
            .writeD( mode )
            .writeD( objectId )

            .writeS( player.getName() )
            .writeD( player.getActiveClass() )
            .writeD( player.getLevel() )
            .writeD( room.lootType )

            .writeD( getPartyValue( room, objectId ) )
            .getBuffer()
}