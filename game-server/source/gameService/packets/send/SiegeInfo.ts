import { Castle } from '../../models/entity/Castle'
import { L2Clan } from '../../models/L2Clan'
import { ClanCache } from '../../cache/ClanCache'
import { DeclaredServerPacket, EmptyStringSize, getStringSize } from '../../../packets/DeclaredServerPacket'
import { ClanHall } from '../../models/entity/ClanHall'
import { ClanHallSiegeManager } from '../../instancemanager/ClanHallSiegeManager'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { IServerPacket } from '../../../packets/IServerPacket'

function getClanSize( clan: L2Clan ): number {
    if ( !clan ) {
        return EmptyStringSize * 3
    }

    return getStringSize( clan.getName() ) + getStringSize( clan.getLeaderName() ) + getStringSize( clan.getAllyName() )
}

function generatePacket( ownerId: number, residenceId: number, player: L2PcInstance ): IServerPacket {
    let clan: L2Clan = ClanCache.getClan( ownerId )
    let packet = new DeclaredServerPacket( getClanSize( clan ) )
            .writeC( 0xC9 )
            .writeD( residenceId )
            .writeD( ( ( ownerId === player.getClanId() ) && ( player.isClanLeader() ) ) ? 0x01 : 0x00 )
            .writeD( ownerId )

    if ( clan ) {
        packet
                .writeS( clan.getName() )
                .writeS( clan.getLeaderName() )
                .writeD( clan.getAllyId() )
                .writeS( clan.getAllyName() )
    } else {
        packet
                .writeS( '' )
                .writeS( '' )
                .writeD( 0 )
                .writeS( '' )
    }

    return packet
}

export function SiegeInfoForCastle( castle: Castle, player: L2PcInstance ): Buffer {
    let ownerId = castle.getOwnerId()

    // TODO : add castle registration times
    return generatePacket( ownerId, castle.getResidenceId(), player ).writeD( Math.floor( Date.now() / 1000 ) )
                                                                     .writeD( Math.floor( castle.getSiegeDate() / 1000 ) )
                                                                     .writeD( 0 )
                                                                     .getBuffer()
}

export function SiegeInfoForHall( hall: ClanHall, player: L2PcInstance ): Buffer {
    let ownerId = hall.getOwnerId()

    // TODO: add clan hall registration times
    return generatePacket( ownerId, hall.getId(), player ).writeD( Math.floor( Date.now() / 1000 ) )
                                                          .writeD( Math.floor( ClanHallSiegeManager.getSiegableHall( hall.getId() ).getNextSiegeTime() / 1000 ) )
                                                          .writeD( 0 )
                                                          .getBuffer()
}