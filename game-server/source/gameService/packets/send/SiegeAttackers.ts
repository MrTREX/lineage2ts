import { Castle } from '../../models/entity/Castle'
import { L2Clan } from '../../models/L2Clan'
import { L2SiegeClan } from '../../models/L2SiegeClan'
import { ClanCache } from '../../cache/ClanCache'
import _ from 'lodash'
import { DeclaredServerPacket, EmptyString, EmptyStringSize, getStringSize } from '../../../packets/DeclaredServerPacket'
import { SiegableHall } from '../../models/entity/clanhall/SiegableHall'

// TODO : regenerate packet based on adding/removing siege clans
function SiegeAttackerPacket( id: number, siegeClans: Array<L2SiegeClan> ): Buffer {
    let clans: Array<L2Clan> = _.compact( _.map( siegeClans, ( siegeClan: L2SiegeClan ) => {
        return ClanCache.getClan( siegeClan.getClanId() )
    } ) )

    let size: number = clans.reduce( ( total: number, clan: L2Clan ): number => {
        return total + 20 + getStringSize( clan.getName() ) + getStringSize( clan.getLeaderName() ) + getStringSize( clan.getAllyName() ) + EmptyStringSize
    }, 0 )

    let packet = new DeclaredServerPacket( 25 + size )
            .writeC( 0xCA )
            .writeD( id )
            .writeD( 0 )
            .writeD( 1 )

            .writeD( 0 )
            .writeD( clans.length )
            .writeD( clans.length )

    clans.forEach( ( clan: L2Clan ) => {
        packet
                .writeD( clan.getId() )
                .writeS( clan.getName() )
                .writeS( clan.getLeaderName() )
                .writeD( clan.getCrestId() )

                .writeD( 0x00 ) // signed time (seconds) (not stored)
                .writeD( clan.getAllyId() )
                .writeS( clan.getAllyName() )
                .writeS( EmptyString ) // AllyLeaderName
                .writeD( clan.getAllyCrestId() )
    } )

    return packet.getBuffer()
}

export function SiegeAttackersForCastle( castle: Castle ): Buffer {
    return SiegeAttackerPacket( castle.getResidenceId(), castle.getSiege().getAttackerClans() )
}

export function SiegeAttackersForHall( hall: SiegableHall ): Buffer {
    return SiegeAttackerPacket( hall.getId(), hall.getSiege().getAttackerClans() )
}