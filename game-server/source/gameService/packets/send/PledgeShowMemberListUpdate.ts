import { L2ClanValues } from '../../values/L2ClanValues'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export function PledgeShowMemberListUpdate( player: L2PcInstance ): Buffer {
    let pledgeType = player.getPledgeType()
    let hasSponsor = pledgeType === L2ClanValues.SUBUNIT_ACADEMY ? ( player.getSponsor() !== 0 ? 1 : 0 ) : 0
    let name = player.getName()
    let level = player.getLevel()
    let classId = player.getClassId()
    let race = player.getRace()
    let sex = player.getAppearance().getSex() ? 1 : 0
    let isOnline = player.isOnline()

    let packet = new DeclaredServerPacket( 29 + getStringSize( name ) )
            .writeC( 0x5b )
            .writeS( name )
            .writeD( level )
            .writeD( classId )

            .writeD( sex )
            .writeD( race )

    if ( isOnline ) {
        packet
                .writeD( player.getObjectId() )
                .writeD( pledgeType )
    } else {
        packet
                .writeD( 0 )
                .writeD( 0 )
    }

    packet.writeD( hasSponsor )

    return packet.getBuffer()
}