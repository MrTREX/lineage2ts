import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function TradeOtherDone(): Buffer {
    return new DeclaredServerPacket( 1 ).writeC( 0x82 ).getBuffer()
}