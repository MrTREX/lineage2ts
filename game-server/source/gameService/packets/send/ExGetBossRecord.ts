import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExGetBossRecord( rank: number, total: number, pointsMap: Map<number, number> ): Buffer {
    let packet = new DeclaredServerPacket( 15 + pointsMap.size * 12 )
            .writeC( 0xFE )
            .writeH( 0x34 )
            .writeD( rank )
            .writeD( total )
            .writeD( pointsMap.size )

    pointsMap.forEach( ( value: number, key: number ) => {
        packet
                .writeD( key )
                .writeD( value )
                .writeD( 0 )
    } )

    return packet.getBuffer()
}