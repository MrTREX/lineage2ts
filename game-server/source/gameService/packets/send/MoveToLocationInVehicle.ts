import { Location } from '../../models/Location'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export function MoveToLocationInVehicle( player: L2PcInstance, destination: Location, start: Location ): Buffer {
    return new DeclaredServerPacket( 33 )
            .writeC( 0x7E )
            .writeD( player.getObjectId() )
            .writeD( player.getBoat().getObjectId() )
            .writeD( destination.getX() )

            .writeD( destination.getY() )
            .writeD( destination.getZ() )
            .writeD( start.getX() )
            .writeD( start.getY() )

            .writeD( start.getZ() )
            .getBuffer()
}