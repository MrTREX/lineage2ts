import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export const enum VehicleStartedState {
    Start,
    Stop
}

export function VehicleStarted( objectId: number, state: VehicleStartedState ) : Buffer {
    return new DeclaredServerPacket( 9 )
            .writeC( 0xC0 )
            .writeD( objectId )
            .writeD( state )
            .getBuffer()
}