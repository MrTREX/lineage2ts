import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { FishGrade } from '../../enums/FishGrade'

export const enum FishingMode {
    Standard,
    Alternate
}

export const enum FishDeceptiveMode {
    None,
    Enabled
}

export function ExFishingStartCombat( objectId: number,
                                     time: number,
                                     hpValue: number,
                                     mode: FishingMode,
                                     fishGrade: FishGrade,
                                     deceptiveMode: FishDeceptiveMode ) : Buffer {
    return new DeclaredServerPacket( 18 )
            .writeC( 0xFE )
            .writeH( 0x27 )
            .writeD( objectId )
            .writeD( time )

            .writeD( hpValue )
            .writeC( mode ) // mode: 0 = resting, 1 = fighting
            .writeC( fishGrade ) // 0 = newbie lure, 1 = normal lure, 2 = night lure
            .writeC( deceptiveMode ) // Fish Deceptive Mode: 0 = no, 1 = yes

            .getBuffer()
}