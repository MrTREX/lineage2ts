import { L2World } from '../../L2World'
import { L2Npc } from '../../models/actor/L2Npc'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export function ServerObjectInfo( objectId: number, playerId: number ): Buffer {
    let character: L2Npc = L2World.getObjectById( objectId ) as L2Npc
    let player = L2World.getPlayer( playerId )

    return ServerObjectInfoWithCharacters( character, player )
}

export function ServerObjectInfoWithCharacters( npc: L2Npc, player: L2PcInstance ): Buffer {
    let idTemplate = npc.getTemplate().getDisplayId()
    let isAttackable = npc.isAutoAttackable( player )
    let collisionHeight = npc.getCollisionHeight()
    let collisionRadius = npc.getCollisionRadius()
    let name = npc.getTemplate().isUsingServerSideName() ? npc.getTemplate().getName() : ''

    return new DeclaredServerPacket( 77 + getStringSize( name ) )
            .writeC( 0x92 )
            .writeD( npc.getObjectId() )
            .writeD( idTemplate + 1000000 )
            .writeS( name )

            .writeD( isAttackable ? 1 : 0 )
            .writeD( npc.getX() )
            .writeD( npc.getY() )
            .writeD( npc.getZ() )

            .writeD( npc.getHeading() )
            .writeF( 1.0 ) // movement multiplier
            .writeF( 1.0 ) // attack speed multiplier
            .writeF( collisionRadius )

            .writeF( collisionHeight )
            .writeD( isAttackable ? npc.getCurrentHp() : 0 )
            .writeD( isAttackable ? npc.getMaxHp() : 0 )
            .writeD( 0x01 ) // object type

            .writeD( 0x00 ) // special effects
            .getBuffer()
}