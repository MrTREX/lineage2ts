import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function MagicSkillCanceled( objectId: number ) : Buffer {
    return new DeclaredServerPacket( 5 )
        .writeC( 0x49 )
        .writeD( objectId )
        .getBuffer()
}