import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExBaseAttributeCancelResult( objectId: number, elementId: number ) : Buffer {
    return new DeclaredServerPacket( 15 )
            .writeC( 0xFE )
            .writeH( 0x75 )
            .writeD( 0x01 )
            .writeD( objectId )

            .writeD( elementId )
            .getBuffer()
}