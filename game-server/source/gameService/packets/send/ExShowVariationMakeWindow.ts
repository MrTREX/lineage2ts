import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { PacketHelper } from '../PacketVariables'

const staticPacket: Buffer = PacketHelper.preservePacket( new DeclaredServerPacket( 3 )
        .writeC( 0xFE )
        .writeH( 0x51 )
        .getBuffer() )

export function ExShowVariationMakeWindow(): Buffer {
    return PacketHelper.copyPacket( staticPacket )
}