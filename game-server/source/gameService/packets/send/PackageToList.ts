import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'
import _ from 'lodash'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

function getCharacterSize( accountCharacters: Map<number, string> ) {
    let size = 0

    accountCharacters.forEach( ( name: string ) => {
        size = size + getStringSize( name ) + 4
    } )

    return size
}

export function PackageToList( player: L2PcInstance ) : Buffer {
    let size = _.size( player.getAccountCharacters() )

    let packet = new DeclaredServerPacket( 5 + getCharacterSize( player.getAccountCharacters() ) )
            .writeC( 0xC8 )
            .writeD( size )

    player.getAccountCharacters().forEach( ( name: string, objectId: number ) => {
        packet
                .writeD( objectId )
                .writeS( name )
    } )

    return packet.getBuffer()
}