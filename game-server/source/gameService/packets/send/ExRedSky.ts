import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExRedSky( durationSeconds: number ) : Buffer {
    return new DeclaredServerPacket( 7 )
            .writeC( 0xFE )
            .writeH( 0x41 )
            .writeD( durationSeconds )
            .getBuffer()
}