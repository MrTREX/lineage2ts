import { L2CharacterMacroCommand, L2CharacterMacroItem } from '../../../database/interface/CharacterMacrosTableApi'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

function getCommandSize( commands: Array<L2CharacterMacroCommand> ): number {
    return commands.reduce( ( total: number, command: L2CharacterMacroCommand ): number => {
        return total + 10 + getStringSize( command.command )
    }, 0 )
}

function getSize( macro: L2CharacterMacroItem ): number {
    if ( !macro ) {
        return 0
    }

    return 6 + getStringSize( macro.name ) + getStringSize( macro.description ) + getStringSize( macro.acronym ) + getCommandSize( macro.commands )
}

export function SendMacroList( revision: number, count: number, macro: L2CharacterMacroItem ): Buffer {
    let packet = new DeclaredServerPacket( 8 + getSize( macro ) )
            .writeC( 0xE8 )
            .writeD( revision )
            .writeC( 0x00 )
            .writeC( count )

            .writeC( macro ? 1 : 0 )

    if ( macro ) {
        packet
                .writeD( macro.id )
                .writeS( macro.name )
                .writeS( macro.description )
                .writeS( macro.acronym )

                .writeC( macro.icon )
                .writeC( macro.commands.length )

        macro.commands.forEach( ( command: L2CharacterMacroCommand, index ) => {
            packet
                    .writeC( index + 1 )
                    .writeC( command.type )
                    .writeD( command.dataOne )
                    .writeD( command.dataTwo )
                    .writeS( command.command )
        } )
    }

    return packet.getBuffer()
}