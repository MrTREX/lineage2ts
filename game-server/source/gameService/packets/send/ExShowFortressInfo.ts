import { Fort } from '../../models/entity/Fort'
import { FortManager } from '../../instancemanager/FortManager'
import { L2Clan } from '../../models/L2Clan'
import { DeclaredServerPacket, EmptyString, EmptyStringSize, getStringSize } from '../../../packets/DeclaredServerPacket'

// TODO : consider caching packet until clan ownership changes, or siege status is changed
export function ExShowFortressInfo(): Buffer {
    let forts: ReadonlyArray<Fort> = FortManager.getForts()

    let fortSize: number = forts.reduce( ( size: number, fort: Fort ): number => {
        let clan: L2Clan = fort.getOwnerClan()
        return size + ( clan ? getStringSize( clan.getName() ) : EmptyStringSize ) + 12
    }, 0 )

    let packet = new DeclaredServerPacket( 7 + fortSize )
            .writeC( 0xFE )
            .writeH( 0x15 )
            .writeD( forts.length )

    forts.forEach( ( fort: Fort ) => {
        let clan: L2Clan = fort.getOwnerClan()

        packet
                .writeD( fort.getResidenceId() )
                .writeS( clan ? clan.getName() : EmptyString )
                .writeD( fort.getSiege().isInProgress() ? 0x01 : 0x00 )
                .writeD( fort.getOwnedTime() )
    } )

    return packet.getBuffer()
}