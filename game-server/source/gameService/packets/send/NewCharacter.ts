import { DataManager } from '../../../data/manager'
import { ClassIdValues } from '../../models/base/ClassId'
import { L2PcTemplate } from '../../models/actor/templates/L2PcTemplate'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { PacketHelper } from '../PacketVariables'

let characterTemplates = []
let staticPacket: Buffer

function createStaticPacket(): void {
    let packet = new DeclaredServerPacket( 5 + characterTemplates.length * 80 )
            .writeC( 0x0D )
            .writeD( characterTemplates.length )

    characterTemplates.forEach( ( currentTemplate: L2PcTemplate ) => {
        packet
                .writeD( currentTemplate.getRace() )
                .writeD( currentTemplate.getClassId() )
                .writeD( 0x46 )
                .writeD( currentTemplate.getBaseSTR() )

                .writeD( 0x0A )
                .writeD( 0x46 )
                .writeD( currentTemplate.getBaseDEX() )
                .writeD( 0x0A )

                .writeD( 0x46 )
                .writeD( currentTemplate.getBaseCON() )
                .writeD( 0x0A )
                .writeD( 0x46 )

                .writeD( currentTemplate.getBaseINT() )
                .writeD( 0x0A )
                .writeD( 0x46 )
                .writeD( currentTemplate.getBaseWIT() )

                .writeD( 0x0A )
                .writeD( 0x46 )
                .writeD( currentTemplate.getBaseMEN() )
                .writeD( 0x0A )
    } )

    staticPacket = PacketHelper.preservePacket( packet.getBuffer() )
}


function initializePacket() {

    /*
        We need to delay production of static packet due to data not being available when server starts, as it loads data
     */

    characterTemplates = [
        DataManager.getPlayerTemplateData().getTemplateByClassId( ClassIdValues.fighter ),
        DataManager.getPlayerTemplateData().getTemplateByClassId( ClassIdValues.mage ),
        DataManager.getPlayerTemplateData().getTemplateByClassId( ClassIdValues.elvenFighter ),
        DataManager.getPlayerTemplateData().getTemplateByClassId( ClassIdValues.elvenMage ),
        DataManager.getPlayerTemplateData().getTemplateByClassId( ClassIdValues.darkFighter ),
        DataManager.getPlayerTemplateData().getTemplateByClassId( ClassIdValues.darkMage ),
        DataManager.getPlayerTemplateData().getTemplateByClassId( ClassIdValues.orcFighter ),
        DataManager.getPlayerTemplateData().getTemplateByClassId( ClassIdValues.orcMage ),
        DataManager.getPlayerTemplateData().getTemplateByClassId( ClassIdValues.dwarvenFighter ),
        DataManager.getPlayerTemplateData().getTemplateByClassId( ClassIdValues.maleSoldier ),
        DataManager.getPlayerTemplateData().getTemplateByClassId( ClassIdValues.femaleSoldier ),
    ]

    createStaticPacket()
}

export function NewCharacter(): Buffer {
    if ( !staticPacket ) {
        initializePacket()
    }

    let data: Buffer = Buffer.allocUnsafe( staticPacket.length )
    staticPacket.copy( data )

    return data
}