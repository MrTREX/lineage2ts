import { L2Summon } from '../../models/actor/L2Summon'
import { L2PetInstance } from '../../models/actor/instance/L2PetInstance'
import { L2ServitorInstance } from '../../models/actor/instance/L2ServitorInstance'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'
import { L2World } from '../../L2World'

export const enum PetInfoAnimation {
    None,
    StatUpgrade,
    SummonAnimation
}

export function PetInfo( objectId: number, state: PetInfoAnimation = PetInfoAnimation.None ): Buffer {
    let summon = L2World.getObjectById( objectId ) as L2Summon
    if ( !summon ) {
        return
    }

    let moveMultiplier = summon.getMovementSpeedMultiplier()
    let runSpeed = Math.round( summon.getRunSpeed() / moveMultiplier )
    let walkSpeed = Math.round( summon.getWalkSpeed() / moveMultiplier )
    let swimRunSpeed = Math.round( summon.getSwimRunSpeed() / moveMultiplier )
    let swimWalkSpeed = Math.round( summon.getSwimWalkSpeed() / moveMultiplier )
    let flyRunSpeed = summon.isFlying() ? runSpeed : 0
    let flyWalkSpeed = summon.isFlying() ? walkSpeed : 0

    let currentFeed = 0
    let maximumFeedAmount = 0
    if ( summon.isPet() ) {
        let pet = summon as L2PetInstance
        currentFeed = pet.getCurrentFeed()
        maximumFeedAmount = pet.getMaxFeed()

        pet.getInventory().inventoryUpdateOperation()
    }

    if ( summon.isServitor() ) {
        let servitor = summon as L2ServitorInstance
        currentFeed = servitor.getLifeTimeRemaining()
        maximumFeedAmount = servitor.getLifeTime()
    }

    let name: string = summon.isPet() ? summon.getName() : ( summon.getTemplate().isUsingServerSideName() ? summon.getName() : '' )

    return new DeclaredServerPacket( 276 + getStringSize( name ) + getStringSize( summon.getTitle() ) )
            .writeC( 0xb2 )
            .writeD( summon.getSummonType() )
            .writeD( summon.getObjectId() )
            .writeD( summon.getTemplate().infoId )

            .writeD( 0 ) // 1 - attackable, 0 - friend
            .writeD( summon.getX() )
            .writeD( summon.getY() )
            .writeD( summon.getZ() )

            .writeD( summon.getHeading() )
            .writeD( 0 )
            .writeD( summon.getMagicAttackSpeed() )
            .writeD( summon.getPowerAttackSpeed() )

            .writeD( runSpeed )
            .writeD( walkSpeed )
            .writeD( swimRunSpeed )
            .writeD( swimWalkSpeed )

            .writeD( flyRunSpeed )
            .writeD( flyWalkSpeed )
            .writeD( flyRunSpeed )
            .writeD( flyWalkSpeed )

            .writeF( moveMultiplier )
            .writeF( summon.getAttackSpeedMultiplier() )
            .writeF( summon.getTemplate().getFCollisionRadius() )
            .writeF( summon.getTemplate().getFCollisionHeight() )

            .writeD( summon.getWeapon() )
            .writeD( summon.getArmor() )
            .writeD( 0x00 )
            .writeC( summon.getOwnerId() > 0 ? 1 : 0 )// 1 - show name, 0 - no name

            .writeC( summon.isRunning() ? 1 : 0 )
            .writeC( summon.isInCombat() ? 1 : 0 )
            .writeC( summon.isAlikeDead() ? 1 : 0 )
            .writeC( summon.isShowSummonAnimation() ? PetInfoAnimation.SummonAnimation : state )

            .writeD( -1 )// npcStringId if exists, -1 is default none
            .writeS( name )

            .writeD( -1 )// npcStringId if exists for title, -1 is default none
            .writeS( summon.getTitle() )
            .writeD( 1 )
            .writeD( summon.getPvpFlag() )

            .writeD( summon.getKarma() )
            .writeD( currentFeed )
            .writeD( maximumFeedAmount )
            .writeD( summon.getCurrentHp() )

            .writeD( summon.getMaxHp() )
            .writeD( summon.getCurrentMp() )
            .writeD( summon.getMaxMp() )
            .writeD( summon.getSp() )

            .writeD( summon.getLevel() )
            .writeQ( summon.getExp() )
            .writeQ( Math.min( summon.getExpForThisLevel(), summon.getExp() ) )
            .writeQ( summon.getExpForNextLevel() )

            .writeD( summon.isPet() ? summon.getInventory().getTotalWeight() : 0 )
            .writeD( summon.getMaxLoad() )
            .writeD( summon.getPowerAttack( null ) )
            .writeD( summon.getPowerDefence( null ) )

            .writeD( summon.getMagicAttack( null, null ) )
            .writeD( summon.getMagicDefence( null, null ) )
            .writeD( summon.getAccuracy() )
            .writeD( summon.getEvasionRate( null ) )

            .writeD( summon.getCriticalHit( null, null ) )
            .writeD( summon.getMoveSpeed() )
            .writeD( summon.getPowerAttackSpeed() )
            .writeD( summon.getMagicAttackSpeed() )

            .writeD( summon.getAbnormalVisualEffects() )// 1 - bleed, 2 - poison, 3 - poison + bleed, 4 - flame
            .writeH( summon.isMountable() ? 1 : 0 )
            .writeC( summon.getMovementType() )
            .writeH( 0 )

            .writeC( summon.getTeam() )
            .writeD( summon.getSoulShotsPerHit() )
            .writeD( summon.getSpiritShotsPerHit() )
            .writeD( summon.getFormId() )

            .writeD( summon.getAbnormalVisualEffectSpecial() )
            .getBuffer()
}