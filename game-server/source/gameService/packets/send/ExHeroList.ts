import { HeroCache } from '../../cache/HeroCache'
import { HeroPlayerData } from '../../values/HeroValues'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'
import { PacketHelper } from '../PacketVariables'
import _ from 'lodash'

let staticPacket: Buffer

export function ExHeroList(): Buffer {
    return PacketHelper.copyPacket( staticPacket )
}

export function regenerateHeroPacket(): void {
    staticPacket = PacketHelper.preservePacket( generateExHeroList() )
}

function generateExHeroList(): Buffer {
    let size = _.reduce( HeroCache.getHeroes(), ( totalAmount: number, data: HeroPlayerData ): number => {
        return totalAmount + getStringSize( data.name ) + getStringSize( data.clanName ) + getStringSize( data.allyName ) + 16
    }, 0 )

    let packet = new DeclaredServerPacket( 7 + size )
            .writeC( 0xFE )
            .writeH( 0x79 )
            .writeD( size )

    _.each( HeroCache.getHeroes(), ( data: HeroPlayerData ) => {
        packet
                .writeS( data.name )
                .writeD( data.classId )
                .writeS( data.clanName )
                .writeD( data.clanCrestId )

                .writeS( data.allyName )
                .writeD( data.allyCrestId )
                .writeD( data.count )
    } )

    return packet.getBuffer()
}