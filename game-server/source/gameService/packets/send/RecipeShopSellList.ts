import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2ManufactureItem } from '../../models/L2ManufactureItem'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function RecipeShopSellList( buyer: L2PcInstance, maker: L2PcInstance ): Buffer {
    let size = maker.getManufactureItems().length
    let packet = new DeclaredServerPacket( 25 + size * 16 )
            .writeC( 0xDF )
            .writeD( maker.getObjectId() )
            .writeD( Math.floor( maker.getCurrentMp() ) )
            .writeD( maker.getMaxMp() )

            .writeQ( buyer.getAdena() )
            .writeD( size )

    maker.getManufactureItems().forEach( ( item: L2ManufactureItem ) => {
        packet
                .writeD( item.recipeId )
                .writeD( 0 )
                .writeQ( item.adenaCost )
    } )

    return packet.getBuffer()
}