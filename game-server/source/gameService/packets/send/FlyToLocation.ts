import { ILocational } from '../../models/Location'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2Object } from '../../models/L2Object'

export const enum FlyType {
    ThrowUpward,
    ThrowHorizontally,
    Dummy,
    Charge
}

export function FlyToLocation( object: L2Object, destination: ILocational, type: FlyType ) : Buffer {
    return new DeclaredServerPacket( 33 )
            .writeC( 0xD4 )
            .writeD( object.getObjectId() )
            .writeD( destination.getX() )
            .writeD( destination.getY() )

            .writeD( destination.getZ() )
            .writeD( object.getX() )
            .writeD( object.getY() )
            .writeD( object.getZ() )

            .writeD( type )
            .getBuffer()
}