import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { PacketHelper } from '../PacketVariables'

const staticTruePacket: Buffer = PacketHelper.preservePacket( new DeclaredServerPacket( 7 )
        .writeC( 0xFE )
        .writeH( 0xA9 )
        .writeD( 0x01 )
        .getBuffer() )

const staticFalsePacket = PacketHelper.preservePacket( new DeclaredServerPacket( 7 )
        .writeC( 0xFE )
        .writeH( 0xA9 )
        .writeD( 0 )
        .getBuffer() )

export function ExNoticePostArrived( showAnimation: boolean ): Buffer {
    return PacketHelper.copyPacket( showAnimation ? staticTruePacket : staticFalsePacket )
}