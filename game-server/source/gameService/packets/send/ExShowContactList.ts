import { DeclaredServerPacket, getStringArraySize } from '../../../packets/DeclaredServerPacket'
import { CharacterNamesCache } from '../../cache/CharacterNamesCache'
import { PlayerContactCache } from '../../cache/PlayerContactCache'

export function ExShowContactList( objectId: number ): Buffer {
    let contacts: Array<string> = CharacterNamesCache.getCachedNames( PlayerContactCache.getContacts( objectId ) )

    let packet = new DeclaredServerPacket( 7 + getStringArraySize( contacts ) )
            .writeC( 0xFE )
            .writeH( 0xD3 )
            .writeD( contacts.length )

    contacts.forEach( ( name: string ) => {
        packet.writeS( name )
    } )

    return packet.getBuffer()
}