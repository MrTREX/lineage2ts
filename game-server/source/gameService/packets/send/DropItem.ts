import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function DropItem( item: L2ItemInstance, playerObjectId: number ) : Buffer {

    return new DeclaredServerPacket( 41 )
            .writeC( 0x16 )
            .writeD( playerObjectId )
            .writeD( item.getObjectId() )
            .writeD( item.getId() )

            .writeD( item.getX() )
            .writeD( item.getY() )
            .writeD( item.getZ() )
            .writeD( item.isStackable() ? 0x01 : 0x00 )

            .writeQ( item.getCount() )
            .writeD( 0x01 )
            .getBuffer()
}