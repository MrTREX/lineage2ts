import { ConfigManager } from '../../../config/ConfigManager'
import { Stats } from '../../models/stats/Stats'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export function ExStorageMaxCount( player: L2PcInstance ) : Buffer {
    let inventoryExtraSlots = Math.floor( player.getStat().calculateStat( Stats.INV_LIM, 0, null, null ) )

    return new DeclaredServerPacket( 39 )
        .writeC( 0xFE )
        .writeH( 0x2F )
        .writeD( player.getInventoryLimit() )
        .writeD( player.getWareHouseLimit() )

        .writeD( ConfigManager.character.getMaximumWarehouseSlotsForClan() )
        .writeD( player.getPrivateSellStoreLimit() )
        .writeD( player.getPrivateBuyStoreLimit() )
        .writeD( player.getDwarfRecipeLimit() )

        .writeD( player.getCommonRecipeLimit() )
        .writeD( inventoryExtraSlots )
        .writeD( ConfigManager.character.getMaximumSlotsForQuestItems() )
        .getBuffer()
}