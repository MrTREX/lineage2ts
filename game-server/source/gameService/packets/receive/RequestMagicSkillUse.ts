import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ActionFailed } from '../send/ActionFailed'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { Skill } from '../../models/Skill'
import { ConfigManager } from '../../../config/ConfigManager'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { L2TargetType } from '../../models/skills/targets/L2TargetType'
import { ReadableClientPacket, ReadableClientPacketPool } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 3, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestMagicSkillUse( client: GameClient, packetData: Buffer ) : Promise<void> {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return player.sendOwnedData( ActionFailed() )
    }

    if ( player.isDead() ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    if ( player.isFakeDeath() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_MOVE_SITTING ) )
        return player.sendOwnedData( ActionFailed() )
    }

    if ( player.isInAirShip() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ACTION_PROHIBITED_WHILE_MOUNTED_OR_ON_AN_AIRSHIP ) )
        return player.sendOwnedData( ActionFailed() )
    }

    let packet = ReadableClientPacketPool.getValue().setBuffer( packetData )
    let skillId = packet.readD()
    let controlPressed = packet.readD()
    let shiftPressed = packet.readC()

    ReadableClientPacketPool.recycleValue( packet )

    let skill : Skill = player.getKnownSkill( skillId )
    if ( !skill ) {
        skill = player.getCustomSkill( skillId )
        if ( !skill ) {
            skill = player.getTransformSkill( skillId )
            if ( !skill ) {
                return player.sendOwnedData( ActionFailed() )
            }
        }
    }

    if ( skill.isToggle() && player.isMounted() ) {
        return player.sendOwnedData( ActionFailed() )
    }

    if ( player.getTransformation() && !player.hasTransformSkill( skill.getId() ) ) {
        return player.sendOwnedData( ActionFailed() )
    }

    if ( !ConfigManager.character.karmaPlayerCanTeleport() && player.getKarma() > 0 && skill.hasEffectType( L2EffectType.Teleport ) ) {
        return player.sendOwnedData( ActionFailed() )
    }

    if ( ( skill.isContinuousType()
            && !skill.isDebuff()
            && ( skill.getTargetType() === L2TargetType.SELF ) )
            && ( !player.isInAirShip() || !player.isInBoat() ) ) {
        player.abortMoving()
    }

    let isControlPressed = controlPressed !== 0
    let isShiftPressed = shiftPressed !== 0

    await player.useMagic( skill, isControlPressed, isShiftPressed )
}