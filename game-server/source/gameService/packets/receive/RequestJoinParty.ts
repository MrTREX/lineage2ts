import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2World } from '../../L2World'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { L2GameClientRegistry } from '../../L2GameClientRegistry'
import { ActionFailed } from '../send/ActionFailed'
import { getPartyDistributionTypeById, PartyDistributionType, PartyDistributionTypeValue } from '../../enums/PartyDistributionType'
import { L2Party } from '../../models/L2Party'
import { AskJoinParty } from '../send/AskJoinParty'
import { ListenerCache } from '../../cache/ListenerCache'
import { EventTerminationResult, EventType, PlayerPartyRequestEvent } from '../../models/events/EventType'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { BlocklistCache } from '../../cache/BlocklistCache'
import { EventPoolCache } from '../../cache/EventPoolCache'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 2, // time-to-live value of token bucket (in seconds)
} )

export async function RequestJoinParty( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let name: string = packet.readS(),
            partyType: PartyDistributionType = packet.readD()

    let target: L2PcInstance = L2World.getPlayerByName( name )

    if ( !target ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.FIRST_SELECT_USER_TO_INVITE_TO_PARTY ) )
        return
    }

    let otherClient: GameClient = L2GameClientRegistry.getClientByPlayerId( target.getObjectId() )
    if ( !otherClient || otherClient.isDetached() ) {
        player.sendMessage( 'Player is in offline mode.' )
        return
    }

    if ( player.isPartyBanned() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_HAVE_BEEN_REPORTED_SO_PARTY_NOT_ALLOWED ) )
        player.sendOwnedData( ActionFailed() )
        return
    }

    if ( target.isPartyBanned() ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.C1_REPORTED_AND_CANNOT_PARTY )
                .addCharacterName( target )
                .getBuffer()
        player.sendOwnedData( packet )
        return
    }

    if ( !target.isVisibleFor( player ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_IS_INCORRECT ) )
        return
    }

    if ( target.isInParty() ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.C1_IS_ALREADY_IN_PARTY )
                .addCharacterName( target )
                .getBuffer()
        player.sendOwnedData( packet )
        return
    }

    if ( BlocklistCache.isBlocked( target.getObjectId(), player.getObjectId() ) ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.S1_HAS_ADDED_YOU_TO_IGNORE_LIST )
                .addCharacterName( target )
                .getBuffer()
        player.sendOwnedData( packet )
        return
    }

    if ( target.getObjectId() === player.getObjectId() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_HAVE_INVITED_THE_WRONG_TARGET ) )
        return
    }

    if ( target.isCursedWeaponEquipped() || player.isCursedWeaponEquipped() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
        return
    }

    if ( target.isJailed() || player.isJailed() ) {
        player.sendMessage( 'You cannot invite a player while is in Jail.' )
        return
    }

    if ( target.isInOlympiadMode() || player.isInOlympiadMode() ) {
        if ( ( target.isInOlympiadMode() !== player.isInOlympiadMode() ) || ( target.getOlympiadGameId() !== player.getOlympiadGameId() ) || ( target.getOlympiadSide() !== player.getOlympiadSide() ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.A_USER_CURRENTLY_PARTICIPATING_IN_THE_OLYMPIAD_CANNOT_SEND_PARTY_AND_FRIEND_INVITATIONS ) )
            return
        }
    }

    let partyStatus = new SystemMessageBuilder( SystemMessageIds.C1_INVITED_TO_PARTY )
            .addCharacterName( target )
            .getBuffer()
    player.sendOwnedData( partyStatus )

    if ( !player.isInParty() ) {
        return createNewParty( target, player, partyType )
    }

    if ( player.getParty().isInDimensionalRift() ) {
        return player.sendMessage( 'You cannot invite a player when you are in the Dimensional Rift.' )
    }

    return addTargetToParty( target, player )
}

async function addTargetToParty( target: L2PcInstance, requester: L2PcInstance ): Promise<void> {
    let party: L2Party = requester.getParty()

    if ( !party.isLeader( requester ) ) {
        requester.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ONLY_LEADER_CAN_INVITE ) )
        return
    }
    if ( party.getMemberCount() >= 9 ) {
        requester.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.PARTY_FULL ) )
        return
    }
    if ( party.getPendingInvitation() && !party.isInvitationRequestExpired() ) {
        requester.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.WAITING_FOR_ANOTHER_REPLY ) )
        return
    }

    if ( ListenerCache.hasGeneralListener( EventType.PlayerPartyRequest ) ) {
        let data = EventPoolCache.getData( EventType.PlayerPartyRequest ) as PlayerPartyRequestEvent

        data.partyType = party.getDistributionType()
        data.requesterId = requester.getObjectId()
        data.targetId = target.getObjectId()

        let result: EventTerminationResult = await ListenerCache.getGeneralTerminatedResult( EventType.PlayerPartyRequest, data )
        if ( result && result.terminate ) {
            return
        }
    }

    if ( !target.isProcessingRequest() ) {
        requester.onTransactionRequest( target )
        target.sendOwnedData( AskJoinParty( requester.getName(), party.getDistributionType() ) )
        party.setPendingInvitation( true )
        return
    }

    let packet = new SystemMessageBuilder( SystemMessageIds.C1_IS_BUSY_TRY_LATER )
            .addCharacterName( target )
            .getBuffer()
    requester.sendOwnedData( packet )
}

async function createNewParty( target: L2PcInstance, requester: L2PcInstance, partyType: PartyDistributionType ) : Promise<void> {
    let partyDistributionType: PartyDistributionTypeValue = getPartyDistributionTypeById( partyType )
    if ( !partyDistributionType ) {
        return
    }

    if ( ListenerCache.hasGeneralListener( EventType.PlayerPartyRequest ) ) {
        let data = EventPoolCache.getData( EventType.PlayerPartyRequest ) as PlayerPartyRequestEvent

        data.partyType = partyDistributionType.id
        data.requesterId = requester.getObjectId()
        data.targetId = target.getObjectId()

        let result: EventTerminationResult = await ListenerCache.getGeneralTerminatedResult( EventType.PlayerPartyRequest, data )
        if ( result && result.terminate ) {
            return
        }
    }

    if ( !target.isProcessingRequest() ) {
        target.sendOwnedData( AskJoinParty( requester.getName(), partyDistributionType.id ) )
        target.setActiveRequester( requester )

        requester.onTransactionRequest( target )
        requester.setPartyDistributionType( partyDistributionType.id )

        return
    }

    requester.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.WAITING_FOR_ANOTHER_REPLY ) )
}