import { GameClient } from '../../GameClient'
import { ConfigManager } from '../../../config/ConfigManager'
import { ActionKey } from '../../models/ActionKey'
import { PlayerUICache } from '../../cache/PlayerUICache'
import { UICategories, UIKeys } from '../../../data/interface/UIDataApi'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'
import _ from 'lodash'
import { GameClientState } from '../../enums/GameClientState'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestSaveKeyMapping( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    if ( !ConfigManager.character.storeUISettings()
            || client.getState() !== GameClientState.Playing ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    packet.skipD( 2 )
    let tabCount: number = packet.readD()

    let allCategories: UICategories = {}
    let allKeys: UIKeys = {}
    let categoryId: number = 0

    _.times( tabCount, ( index: number ) => {
        let categorySize: number = packet.readC()

        allCategories[ categoryId ] = _.times( categorySize, (): number => {
            return packet.readC()
        } )

        categoryId++

        categorySize = packet.readC()
        allCategories[ categoryId ] = _.times( categorySize, (): number => {
            return packet.readC()
        } )

        categoryId++

        let keySize = packet.readD()

        allKeys[ index ] = _.times( keySize, (): ActionKey => {

            let commandId = packet.readD(),
                    key = packet.readD(),
                    toggleKeyOne = packet.readD(),
                    toggleKeyTwo = packet.readD(),
                    visibleStatus = packet.readD()

            return {
                categoryId: index,
                commandId,
                key,
                toggleKeyOne,
                toggleKeyTwo,
                visibleStatus,
            }
        } )
    } )

    PlayerUICache.setSettings( client.getPlayerId(), allCategories, allKeys )
}