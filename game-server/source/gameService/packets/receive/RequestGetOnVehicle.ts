import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2BoatInstance } from '../../models/actor/instance/L2BoatInstance'
import { ActionFailed } from '../send/ActionFailed'
import { GetOnVehicle } from '../send/GetOnVehicle'
import { Location } from '../../models/Location'
import { L2World } from '../../L2World'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 2, // time-to-live value of token bucket (in seconds)
} )

export function RequestGetOnVehicle( client: GameClient, packetData: Buffer ) : void {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let boatId = packet.readD(), x = packet.readD(), y = packet.readD(), z = packet.readD()

    let position = new Location( x, y, z )
    let boat: L2BoatInstance
    if ( player.isInBoat() ) {
        boat = player.getBoat()
        if ( boat.getObjectId() !== boatId ) {
            player.sendOwnedData( ActionFailed() )
            return
        }
    } else {
        boat = L2World.getObjectById( boatId ) as L2BoatInstance
        if ( !boat || boat.isMoving() || !player.isInsideRadius( boat, 1000, true ) ) {
            player.sendOwnedData( ActionFailed() )
            return
        }
    }

    player.setInVehiclePosition( position )
    player.setVehicle( boat )
    BroadcastHelper.dataBasedOnVisibility( player, GetOnVehicle( player.getObjectId(), boat.getObjectId(), position ) )

    player.setXYZ( boat.getX(), boat.getY(), boat.getZ() )
    player.revalidateZone( true )
}