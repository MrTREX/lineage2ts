import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Npc } from '../../models/actor/L2Npc'
import { L2World } from '../../L2World'
import { AcquireSkillType } from '../../enums/AcquireSkillType'
import { Skill } from '../../models/Skill'
import { SkillCache } from '../../cache/SkillCache'
import { L2SkillLearn } from '../../models/L2SkillLearn'
import { L2Clan } from '../../models/L2Clan'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { ExStorageMaxCount } from '../send/ExStorageMaxCount'
import { AcquireSkillDone } from '../send/AcquireSkillDone'
import { ClassId } from '../../models/base/ClassId'
import { ConfigManager } from '../../../config/ConfigManager'
import { SkillHolder } from '../../models/holders/SkillHolder'
import { PledgeSkillList } from '../send/PledgeSkillList'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { L2VillageMasterInstance } from '../../models/actor/instance/L2VillageMasterInstance'
import { RequestShowSubUnitSkillList } from '../send/RequestShowSubUnitSkillList'
import { EventType, PlayerLearnSkillEvent } from '../../models/events/EventType'
import { ListenerCache } from '../../cache/ListenerCache'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { EventPoolCache } from '../../cache/EventPoolCache'
import { PlayerShortcutCache } from '../../cache/PlayerShortcutCache'
import aigle from 'aigle'
import _ from 'lodash'
import { PlayerVariablesManager } from '../../variables/PlayerVariablesManager'
import { recordReceivedPacketViolation, recordSkillViolation } from '../../helpers/PlayerViolations'
import { FastRateLimit } from 'fast-ratelimit'
import { ItemDefinition } from '../../interface/ItemDefinition'
import { showSkillList } from '../../handler/bypasshandlers/SkillList'
import { ClanPrivilege } from '../../enums/ClanPriviledge'
import { CommonSkillIds } from '../../enums/skills/CommonSkillIds'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export const RequestAcquireSkillVariables : Array<string> = [
    'AvantGarde:EmergentAbility65-',
    'AvantGarde:EmergentAbility70-',
    'AvantGarde:ClassAbility75-',
    'AvantGarde:ClassAbility80-',
]

export async function RequestAcquireSkill( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let trainer: L2Npc = L2World.getObjectById( player.getLastFolkNPC() ) as L2Npc
    if ( !trainer.isNpc() ) {
        return
    }

    if ( !player.canInteract( trainer ) && !player.isGM() ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let id = packet.readD(), level = packet.readD(), skillType = packet.readD()
    let subType: number
    if ( skillType === AcquireSkillType.SubPledge ) {
        subType = packet.readD()
    }

    if ( ( level < 1 ) || ( level > 1000 ) || ( id < 1 ) || ( id > 32000 ) ) {
        return recordReceivedPacketViolation( player.getObjectId(), '2fa20b3e-8d55-4f03-b71f-12c40a61f7de', 'Bad skill level or id, out of normal range', 'RequestAcquireSkill', level, id )
    }

    let skill: Skill = SkillCache.getSkill( id, level )
    if ( !skill ) {
        return
    }

    let skillLearn: L2SkillLearn = SkillCache.getSkillLearn( skillType, id, level, player )

    if ( !canBeLearnedBy( player, skill, skillLearn, id, skillType, level, subType ) ) {
        return
    }

    switch ( skillType ) {
        case AcquireSkillType.Class:
            if ( checkPlayerSkill( player, trainer, skillLearn, id, level, skillType ) ) {
                return giveSkill( player, trainer, skill, id, level, skillType )
            }

            return

        case AcquireSkillType.Transform:
            if ( !canTransform( player ) ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_COMPLETED_QUEST_FOR_SKILL_ACQUISITION ) )
                return recordSkillViolation( player.getObjectId(), 'e580f02a-00e7-43a6-a317-169ba2ad1abb', 'Player tries to get transform skill without completing quest.', skill.getId(), skill.getLevel() )
            }

            if ( checkPlayerSkill( player, trainer, skillLearn, id, level, skillType ) ) {
                return giveSkill( player, trainer, skill, id, level, skillType )
            }

            return

        case AcquireSkillType.Fishing:
            if ( checkPlayerSkill( player, trainer, skillLearn, id, level, skillType ) ) {
                return giveSkill( player, trainer, skill, id, level, skillType )
            }

            return

        case AcquireSkillType.Pledge:
            if ( !player.isClanLeader() ) {
                return
            }

            return onPledgeSkill( player, skillLearn, skill, trainer )

        case AcquireSkillType.SubPledge:
            return onSubPledgeSkill( player, skillLearn, skill, trainer, subType )

        case AcquireSkillType.Transfer:
            if ( checkPlayerSkill( player, trainer, skillLearn, id, level, subType ) ) {
                return giveSkill( player, trainer, skill, id, level, skillType )
            }

            return

        /*
            See AvantGarde listener for details.
            Logic here is specific to AvantGarde implementation, hence must be corrected in two places.
         */
        case AcquireSkillType.Subclass:
            for ( let variableName of RequestAcquireSkillVariables ) {
                for ( let index = 1; index <= ConfigManager.character.getMaxSubclass(); index++ ) {
                    let fullVariableName = `${variableName}${index}`
                    let itemIdString: string = PlayerVariablesManager.get( player.getObjectId(), fullVariableName ) as string
                    if ( !itemIdString || itemIdString.startsWith( '!' ) ) {
                        continue
                    }

                    let itemObjectId = _.parseInt( itemIdString )
                    if ( !itemObjectId ) {
                        continue
                    }

                    let item: L2ItemInstance = player.getInventory().getItemByObjectId( itemObjectId )
                    if ( !item ) {
                        continue
                    }

                    let foundItem: ItemDefinition = skillLearn.getRequiredItems().find( ( definition: ItemDefinition ) => definition.id === item.getId() )
                    if ( !foundItem ) {
                        continue
                    }

                    if ( checkPlayerSkill( player, trainer, skillLearn, id, level, skillType ) ) {
                        await giveSkill( player, trainer, skill, id, level, skillType )
                        PlayerVariablesManager.set( player.getObjectId(), fullVariableName, `!${skill.getId()}` )
                    }

                    return
                }
            }

            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ITEM_OR_PREREQUISITES_MISSING_TO_LEARN_SKILL ) )
            return attemptShowSkillList( trainer, player, skillType )

        case AcquireSkillType.Collect:
            if ( checkPlayerSkill( player, trainer, skillLearn, id, level, skillType ) ) {
                return giveSkill( player, trainer, skill, id, level, skillType )
            }

            return
    }
}

function canBeLearnedBy( player: L2PcInstance, skill: Skill, skillLearn: L2SkillLearn, id: number, skillType: AcquireSkillType, level: number, subType: number ): boolean {
    let lastSkillLevel: number = player.getSkillLevel( id )

    switch ( skillType ) {
        case AcquireSkillType.SubPledge:
            let clan: L2Clan = player.getClan()

            if ( !clan ) {
                return false
            }

            if ( !player.isClanLeader() || !player.hasClanPrivilege( ClanPrivilege.NameTroops ) ) {
                return false
            }

            if ( ( clan.getFortId() === 0 ) && ( clan.getCastleId() === 0 ) ) {
                return false
            }

            if ( !clan.isLearnableSubPledgeSkill( skill, subType ) ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SQUAD_SKILL_ALREADY_ACQUIRED ) )

                recordSkillViolation( player.getObjectId(), '01594b01-ca01-4992-bcf6-baef6615e21e', 'Cannot learn already acquired clan skill', skill.getId(), skill.getLevel() )
                return false
            }
            break

        case AcquireSkillType.Transfer:
            break

        case AcquireSkillType.Subclass:
            if ( player.isSubClassActive() ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SKILL_NOT_FOR_SUBCLASS ) )

                recordSkillViolation( player.getObjectId(), '74aba6d7-47bc-46c8-89ef-4039dab6ab58', 'Cannot learn skill when subclass is active', skill.getId(),skill.getLevel() )
                return false
            }
            break

        default: {
            if ( lastSkillLevel === level ) {
                return false
            }
            if ( ( level !== 1 ) && ( lastSkillLevel !== ( level - 1 ) ) ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.PREVIOUS_LEVEL_SKILL_NOT_LEARNED ) )

                recordSkillViolation( player.getObjectId(), '0c3490f0-058c-4019-b02c-c049f032b927', 'Cannot learn skill without previous level', skill.getId(),skill.getLevel() )
                return false
            }
        }
    }

    return true
}

async function giveSkill( player: L2PcInstance, trainer: L2Npc, skill: Skill, id: number, level: number, skillType: AcquireSkillType ): Promise<void> {
    let packet = new SystemMessageBuilder( SystemMessageIds.LEARNED_SKILL_S1 )
            .addSkillName( skill )
            .getBuffer()
    player.sendOwnedData( packet )

    await player.addSkill( skill, true )
    player.sendOwnedData( AcquireSkillDone() )
    player.sendSkillList()

    await PlayerShortcutCache.updateSkillShortcut( player, id, level )
    attemptShowSkillList( trainer, player, skillType )

    if ( id >= 1368 && id <= 1372 ) {
        player.sendOwnedData( ExStorageMaxCount( player ) )
    }

    if ( ListenerCache.hasGeneralListener( EventType.PlayerLearnSkill ) ) {
        let eventData = EventPoolCache.getData( EventType.PlayerLearnSkill ) as PlayerLearnSkillEvent

        eventData.trainerObjectId = trainer.getObjectId()
        eventData.playerId = player.getObjectId()
        eventData.skillId = skill.getId()
        eventData.skillLevel = skill.getLevel()
        eventData.type = skillType

        return ListenerCache.sendGeneralEvent( EventType.PlayerLearnSkill, eventData )
    }
}

function attemptShowSkillList( trainer: L2Npc, player: L2PcInstance, skillType: AcquireSkillType ): void {
    if ( ( skillType === AcquireSkillType.Transform )
            || ( skillType === AcquireSkillType.Subclass )
            || ( skillType === AcquireSkillType.Transfer )
            || ( skillType === AcquireSkillType.Fishing ) ) {
        return
    }

    showSkillList( player, trainer, player.getLearningClass() )
}

function checkPlayerSkill( player: L2PcInstance, trainer: L2Npc, skillLearn: L2SkillLearn, id: number, level: number, skillType: AcquireSkillType ): boolean {
    if ( !skillLearn ) {
        return false
    }

    if ( ( skillLearn.getSkillId() === id ) && ( skillLearn.getSkillLevel() === level ) ) {
        if ( skillLearn.getGetLevel() > player.getLevel() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_DONT_MEET_SKILL_LEVEL_REQUIREMENTS ) )

            recordSkillViolation( player.getObjectId(), '92013709-be2e-4049-bcdd-3607b1e20a63', 'Player level too low to request skill', id, level )
            return false
        }

        let levelUpSp = skillLearn.getCalculatedLevelUpSp( ClassId.getClassIdByIdentifier( player.getClassId() ), ClassId.getClassIdByIdentifier( player.getLearningClass() ) )
        if ( ( levelUpSp > 0 ) && ( levelUpSp > player.getSp() ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_SP_TO_LEARN_SKILL ) )
            attemptShowSkillList( trainer, player, skillType )
            return false
        }

        if ( !ConfigManager.character.divineInspirationSpBookNeeded() && id === CommonSkillIds.DivineInspiration ) {
            return true
        }

        let skill: SkillHolder = _.find( skillLearn.getPrerequisiteSkills(), ( holder: SkillHolder ) => {
            return player.getSkillLevel( holder.getId() ) !== holder.getLevel()
        } )

        if ( skill ) {
            if ( skill.getId() === CommonSkillIds.OnyxBeastTransformation ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_MUST_LEARN_ONYX_BEAST_SKILL ) )
            } else {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ITEM_OR_PREREQUISITES_MISSING_TO_LEARN_SKILL ) )
            }

            return false
        }

        let shouldExit = skillLearn.getRequiredItems().some( ( item: ItemDefinition ) : boolean => {
            let count = player.getInventory().getInventoryItemCount( item.id, -1 )
            return count < item.count
        } )

        if ( shouldExit ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ITEM_OR_PREREQUISITES_MISSING_TO_LEARN_SKILL ) )
            attemptShowSkillList( trainer, player, skillType )
            return false
        }

        if ( levelUpSp > 0 ) {
            player.setSp( player.getSp() - levelUpSp )
        }

        return true
    }

    return false
}

export function canTransform( player: L2PcInstance ): boolean {
    if ( ConfigManager.character.transformationWithoutQuest() ) {
        return true
    }

    return player.hasQuestCompleted( 'Q00136_MoreThanMeetsTheEye' )
}

async function onPledgeSkill( player: L2PcInstance, skillLearn: L2SkillLearn, skill: Skill, trainer: L2Npc ): Promise<void> {
    let clan: L2Clan = player.getClan()
    let reputationCost = skillLearn.getLevelUpSp()
    if ( clan.getReputationScore() >= reputationCost ) {
        if ( ConfigManager.character.lifeCrystalNeeded() ) {

            let shouldExit = await aigle.resolve( skillLearn.getRequiredItems() ).someSeries( async ( item: ItemDefinition ) : Promise<boolean> => {
                if ( !( await player.destroyItemByItemId( item.id, item.count, false, 'RequestAcquireSkill' ) ) ) {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ITEM_OR_PREREQUISITES_MISSING_TO_LEARN_SKILL ) )
                    L2VillageMasterInstance.showPledgeSkillList( player )
                    return true
                }

                let packet = new SystemMessageBuilder( SystemMessageIds.S2_S1_DISAPPEARED )
                        .addItemNameWithId( item.id )
                        .addNumber( item.count )
                        .getBuffer()

                player.sendOwnedData( packet )

                return false
            } )

            if ( shouldExit ) {
                return
            }
        }

        await clan.takeReputationScore( reputationCost, true )

        let reputationUpdate = new SystemMessageBuilder( SystemMessageIds.S1_DEDUCTED_FROM_CLAN_REP )
                .addNumber( reputationCost )
                .getBuffer()
        player.sendOwnedData( reputationUpdate )

        await clan.addNewSkill( skill )

        clan.broadcastDataToOnlineMembers( PledgeSkillList( clan ) )

        player.sendOwnedData( AcquireSkillDone() )
    } else {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ACQUIRE_SKILL_FAILED_BAD_CLAN_REP_SCORE ) )
    }

    return L2VillageMasterInstance.showPledgeSkillList( player )
}

async function onSubPledgeSkill( player: L2PcInstance, skillLearn: L2SkillLearn, skill: Skill, trainer: L2Npc, subType: number ): Promise<void> {
    let clan: L2Clan = player.getClan()
    let reputationCost: number = skillLearn.getLevelUpSp()
    if ( clan.getReputationScore() < reputationCost ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ACQUIRE_SKILL_FAILED_BAD_CLAN_REP_SCORE ) )
        return
    }

    let shouldExit: boolean = skillLearn.getRequiredItems().some( ( item: ItemDefinition ) : boolean => {
        if ( !player.destroyItemByItemId( item.id, item.count, false, 'RequestAcquireSkill' ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ITEM_OR_PREREQUISITES_MISSING_TO_LEARN_SKILL ) )
            return true
        }

        let packet = new SystemMessageBuilder( SystemMessageIds.S2_S1_DISAPPEARED )
                .addItemNameWithId( item.id )
                .addNumber( item.count )
                .getBuffer()
        player.sendOwnedData( packet )
    } )

    if ( shouldExit ) {
        return
    }

    if ( reputationCost > 0 ) {
        await clan.takeReputationScore( reputationCost, true )
        let packet = new SystemMessageBuilder( SystemMessageIds.S1_DEDUCTED_FROM_CLAN_REP )
                .addNumber( reputationCost )
                .getBuffer()
        player.sendOwnedData( packet )
    }

    await clan.addNewSkill( skill, subType )
    clan.broadcastDataToOnlineMembers( PledgeSkillList( clan ) )
    player.sendOwnedData( AcquireSkillDone() )

    return player.sendOwnedData( RequestShowSubUnitSkillList( player ) )
}