import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { Skill } from '../../models/Skill'
import { SkillCache } from '../../cache/SkillCache'
import { AbnormalType } from '../../models/skills/AbnormalType'
import { ConfigManager } from '../../../config/ConfigManager'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 3, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestDispel( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let objectId = packet.readD(), skillId = packet.readD(), skillLevel = packet.readD()

    let skill :Skill = SkillCache.getSkill( skillId, skillLevel )
    if ( !skill ) {
        return
    }

    if ( !player.isGM() ) {
        if ( skill.isIrreplaceableBuff() || skill.isStayAfterDeath() || skill.isDebuff() ) {
            return
        }

        if ( skill.getAbnormalType() === AbnormalType.TRANSFORM ) {
            return
        }

        if ( skill.isDance() && !ConfigManager.character.danceCancelBuff() ) {
            return
        }
    }


    if ( player.getObjectId() === objectId ) {
        return player.stopSkillEffects( true, skillId )
    }

    if ( player.hasSummon() && ( player.getSummon().getObjectId() === objectId ) ) {
        return player.getSummon().stopSkillEffects( true, skillId )
    }
}