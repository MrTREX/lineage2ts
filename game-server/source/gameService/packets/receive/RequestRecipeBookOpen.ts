import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { RecipeController } from '../../taskmanager/RecipeController'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestRecipeBookOpen( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let craftValue : number = new ReadableClientPacket( packetData ).readD()
    let isDwarvenCraft = craftValue === 0

    if ( player.isCastingNow() || player.isCastingSimultaneouslyNow() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NO_RECIPE_BOOK_WHILE_CASTING ) )
        return
    }

    if ( player.getActiveRequester() ) {
        player.sendMessage( 'You may not alter your recipe book while trading.' )
        return
    }

    RecipeController.requestBookOpen( player, isDwarvenCraft )
}