import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ActionFailed } from '../send/ActionFailed'
import { PacketVariables } from '../PacketVariables'
import { ConfigManager } from '../../../config/ConfigManager'
import { L2Object } from '../../models/L2Object'
import { L2NpcValues } from '../../values/L2NpcValues'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { ExBuySellList } from '../send/ExBuySellList'
import { BuyListManager } from '../../cache/BuyListManager'
import { L2BuyList } from '../../models/buylist/L2BuyList'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { GeneralHelper } from '../../helpers/GeneralHelper'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import aigle from 'aigle'
import { recordBuySellViolation } from '../../helpers/PlayerViolations'
import { FastRateLimit } from 'fast-ratelimit'
import { L2MerchantInstance } from '../../models/actor/instance/L2MerchantInstance'
import { ObjectPool } from '../../helpers/ObjectPoolHelper'
import { getMaxAdena } from '../../helpers/ConfigurationHelper'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

interface SellItem {
    itemId: number
    count: number
    objectId: number
}

const objectPool = new ObjectPool<SellItem>( 'RequestSellItem', () : SellItem => {
    return {
        count: 0,
        itemId: 0,
        objectId: 0
    }
} )

export async function RequestSellItem( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return player.sendMessage( 'You are selling too fast.' )
    }

    if ( !ConfigManager.character.karmaPlayerCanShop() && player.getKarma() > 0 ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    let target: L2Object = player.getTarget()
    if ( !player.isGM() && (
            !target
            || !target.isMerchant()
            || !player.isInsideRadius( target, L2NpcValues.interactionDistance, true )
            || ( player.getInstanceId() !== target.getInstanceId() )
    ) ) {
            player.sendOwnedData( ActionFailed() )
            return
    }

    let packet = new ReadableClientPacket( packetData )
    let listId = packet.readD(), totalItems = packet.readD()

    if ( totalItems < 1 || totalItems > PacketVariables.maximumItemsLimit ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    let buyList: L2BuyList = BuyListManager.getBuyList( listId )
    if ( !buyList ) {
        return recordBuySellViolation( player.getObjectId(), '69d270aa-24bb-4bdd-b2b8-50a34b38d91c', 'Buylist does not exist', listId )
    }

    if ( !player.isGM() && !buyList.isNpcAllowed( ( target as L2MerchantInstance ).getId() ) ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    let items: Array<SellItem> = []

    while ( totalItems > 0 ) {
        let item = objectPool.getValue()

        item.objectId = packet.readD()
        item.itemId = packet.readD()
        item.count = packet.readQ()

        if ( item.itemId < 1 || item.count < 1 || item.objectId < 1 ) {
            objectPool.recycleValues( items )
            return player.sendOwnedData( ActionFailed() )
        }

        items.push( item )

        totalItems--
    }

    let totalPrice = 0
    let maxAdena = getMaxAdena()

    let shouldExit = await aigle.resolve( items ).someSeries( async ( sellItem: SellItem ) => {
        let item: L2ItemInstance = player.checkItemManipulation( sellItem.objectId, sellItem.count )
        if ( !item || !item.isSellable() ) {
            return true
        }

        let price = item.getReferencePrice() / 2
        totalPrice += ( price * sellItem.count )
        if ( ( ( maxAdena / sellItem.count ) < price ) || ( totalPrice > maxAdena ) ) {
            await recordBuySellViolation( player.getObjectId(), '83e7a823-8e37-49e8-a614-b248f71418a0', 'Player tries to sell items above maximum adena allowed', sellItem.itemId, sellItem.count, sellItem.objectId, maxAdena )
            return true
        }

        if ( ConfigManager.general.allowRefund() ) {
            await player.getInventory().transferItem( sellItem.objectId, sellItem.count, player.getRefund() )
        } else {
            await player.getInventory().destroyItemByObjectId( sellItem.objectId, sellItem.count, 0, 'RequestSellItem' )
        }

        return false
    } )

    objectPool.recycleValues( items )

    if ( shouldExit ) {
        return player.sendOwnedData( ActionFailed() )
    }

    let roundedAmount = Math.floor( totalPrice )
    await player.addAdena( roundedAmount, 'RequestSellItem', 0, false )

    let message = new SystemMessageBuilder( SystemMessageIds.YOU_PICKED_UP_S1_ADENA )
            .addString( GeneralHelper.getAdenaFormat( roundedAmount ) )
            .getBuffer()

    player.sendOwnedData( message )
    player.sendOwnedData( ExBuySellList( player, true ) )
}