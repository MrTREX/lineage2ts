import { GameClient } from '../../GameClient'
import { FastRateLimit } from 'fast-ratelimit'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'

const packetLimiter = new FastRateLimit( {
    threshold: 3, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

// TODO : integrate into player movement
export function MoveWithDelta( client: GameClient, packetData: Buffer ) : void {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )

    let dx = packet.readD(), dy = packet.readD(), dz = packet.readD()

    if ( player.isGM() ) {
        player.sendMessage( `Received packet ${MoveWithDelta.name}, (dx, dy, dz) (${dx}, ${dy}, ${dz})` )
    }
}