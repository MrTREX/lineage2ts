import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ClanCache } from '../../cache/ClanCache'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 2, // time-to-live value of token bucket (in seconds)
} )

export async function RequestReplySurrenderPledgeWar( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player : L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let requester : L2PcInstance = player.getActiveRequester()
    if ( !requester ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    packet.skipS()
    let answer : number = packet.readD()

    if ( answer === 1 ) {
        await ClanCache.deleteClanWars( requester.getClanId(), player.getClanId() )
    }

    player.onTransactionRequest( requester )
}