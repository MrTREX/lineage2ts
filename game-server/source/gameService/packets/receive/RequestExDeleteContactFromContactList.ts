import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ConfigManager } from '../../../config/ConfigManager'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { PlayerContactCache } from '../../cache/PlayerContactCache'

export function RequestExDeleteContactFromContactList( client: GameClient, packetData: Buffer ) : Promise<void> {
    let player: L2PcInstance = client.player
    if ( !player || !ConfigManager.general.allowMail() ) {
        return
    }

    let name : string = new ReadableClientPacket( packetData ).readS()
    return PlayerContactCache.remove( player.getObjectId(), name )
}