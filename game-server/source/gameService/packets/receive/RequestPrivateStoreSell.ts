import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2World } from '../../L2World'
import { L2NpcValues } from '../../values/L2NpcValues'
import { PrivateStoreType } from '../../enums/PrivateStoreType'
import { TradeList } from '../../models/TradeList'
import { ActionFailed } from '../send/ActionFailed'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'
import { PlayerPermission } from '../../enums/PlayerPermission'
import { TradeListOutcome } from '../../enums/TradeListOutcome'
import { recordTradeViolation } from '../../helpers/PlayerViolations'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestPrivateStoreSell( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( player.isCursedWeaponEquipped() ) {
        return
    }

    if ( !player.getAccessLevel().hasPermission( PlayerPermission.AllowTransaction ) ) {
        player.sendMessage( 'Transactions are disabled for your Access Level.' )
        player.sendOwnedData( ActionFailed() )
        return
    }

    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return player.sendMessage( 'You are selling items too fast.' )
    }

    let packet = new ReadableClientPacket( packetData )
    let playerId = packet.readD(), count = packet.readD()

    if ( count < 1 ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    let storePlayer : L2PcInstance = L2World.getPlayer( playerId )
    if ( !storePlayer ) {
        return
    }

    if ( !player.isInsideRadius( storePlayer, L2NpcValues.interactionDistance ) ) {
        return
    }

    if ( ( player.getInstanceId() !== storePlayer.getInstanceId() ) && ( player.getInstanceId() !== -1 ) ) {
        return
    }

    if ( storePlayer.getPrivateStoreType() !== PrivateStoreType.Buy ) {
        return
    }

    let storeList : TradeList = storePlayer.getBuyList()
    if ( !storeList || storeList.getItems().length === 0 || storeList.isLocked() ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    let items : Array<RequestPrivateStoreSellItem> = []
    while ( count > 0 ) {
        let objectId = packet.readD(), itemId = packet.readD()
        packet.skipD( 1 )
        let count = packet.readQ(), price = packet.readQ()

        if ( objectId < 1 || itemId < 1 || count < 1 || price < 0 ) {
            return
        }

        count--
    }

    let result = await storeList.privateStoreSell( player, items )
    if ( !result ) {
        if ( result === TradeListOutcome.Violation ) {
            await recordTradeViolation(
                'af773b22-5173-4df7-889b-1f51cca956e3',
                'Cannot transfer items from buyer to seller',
                storePlayer.getObjectId(),
                player.getObjectId(),
                storePlayer.getPrivateStoreType(),
                items.map( item => item.objectId ) )
        }

        player.sendOwnedData( ActionFailed() )
        return
    }

    if ( storeList.getItems().length === 0 ) {
        storePlayer.setPrivateStoreType( PrivateStoreType.None )
        storePlayer.broadcastUserInfo()
    }
}

export interface RequestPrivateStoreSellItem {
    objectId: number
    itemId: number
    count: number
    price: number
}