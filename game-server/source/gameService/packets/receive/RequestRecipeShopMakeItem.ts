import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2World } from '../../L2World'
import { PrivateStoreType } from '../../enums/PrivateStoreType'
import { GeneralHelper } from '../../helpers/GeneralHelper'
import { RecipeController } from '../../taskmanager/RecipeController'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 2, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestRecipeShopMakeItem( client: GameClient, packetData : Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let playerId = packet.readD(), recipeId = packet.readD()

    let manufacturer : L2PcInstance = L2World.getPlayer( playerId )
    if ( !manufacturer ) {
        return
    }

    if ( manufacturer.getInstanceId() !== player.getInstanceId() ) {
        return
    }

    if ( player.getPrivateStoreType() !== PrivateStoreType.None ) {
        player.sendMessage( 'You cannot create items while trading.' )
        return
    }

    if ( manufacturer.getPrivateStoreType() !== PrivateStoreType.Manufacture ) {
        return
    }

    if ( player.isInCraftMode() || manufacturer.isInCraftMode() ) {
        player.sendMessage( 'You are currently in Craft Mode.' )
        return
    }

    if ( GeneralHelper.checkIfInRange( 150, player, manufacturer, true ) ) {
        return RecipeController.requestManufactureItem( manufacturer, recipeId, player )
    }
}