import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { ExPutItemResultForVariationMake } from '../send/ExPutItemResultForVariationMake'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { canManipulateItem } from '../../helpers/ItemHelper'

export function RequestConfirmTargetItem( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let objectId : number = new ReadableClientPacket( packetData ).readD()

    let item : L2ItemInstance = player.getInventory().getItemByObjectId( objectId )
    if ( !item ) {
        return
    }

    if ( !canManipulateItem( player, item ) ) {
        if ( item.isAugmented() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ONCE_AN_ITEM_IS_AUGMENTED_IT_CANNOT_BE_AUGMENTED_AGAIN ) )
            return
        }

        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THIS_IS_NOT_A_SUITABLE_ITEM ) )
        return
    }

    player.sendOwnedData( ExPutItemResultForVariationMake( objectId, item.getId() ) )
}