import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { TeleportBookmarkCache } from '../../cache/TeleportBookmarkCache'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestTeleportBookMark( client: GameClient, packetData: Buffer ): Promise<void> {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let id: number = new ReadableClientPacket( packetData ).readD()
    return TeleportBookmarkCache.teleport( player, id )
}