import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { EventType, PlayerTutorialQuestionMarkEvent } from '../../models/events/EventType'
import { ListenerCache } from '../../cache/ListenerCache'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { EventPoolCache } from '../../cache/EventPoolCache'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestTutorialQuestionMark( client: GameClient, packetData: Buffer ): Promise<void> {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( ListenerCache.hasGeneralListener( EventType.PlayerTutorialQuestionMark ) ) {
        let data = EventPoolCache.getData( EventType.PlayerTutorialQuestionMark ) as PlayerTutorialQuestionMarkEvent

        data.playerId = player.getObjectId()
        data.responseId = new ReadableClientPacket( packetData ).readD()

        return ListenerCache.sendGeneralEvent( EventType.PlayerTutorialQuestionMark, data )
    }
}