import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { ConfigManager } from '../../../config/ConfigManager'
import { ExReplyPostItemList } from '../send/ExReplyPostItemList'
import { AreaType } from '../../models/areas/AreaType'

export function RequestPostItemList( client: GameClient ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( !ConfigManager.general.allowMail() || !ConfigManager.general.allowAttachments() ) {
        return
    }

    if ( !player.isInArea( AreaType.Peace ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_USE_MAIL_OUTSIDE_PEACE_ZONE ) )
        return
    }

    player.sendOwnedData( ExReplyPostItemList( player ) )
}