import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

const maxLength = 30

export function RequestRecipeShopMessageSet( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( packetData.length > maxLength ) {
        return
    }

    const name : string = new ReadableClientPacket( packetData ).readS()

    if ( name === '' ) {
        return
    }

    if ( player.hasManufactureShop() ) {
        player.setStoreName( name )
    }
}