import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Party } from '../../models/L2Party'
import { L2World } from '../../L2World'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'

export function RequestChangePartyLeader( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let party : L2Party = player.getParty()
    if ( !party || !party.isLeader( player ) ) {
        return
    }

    let name : string = new ReadableClientPacket( packetData ).readS()

    let targetPlayer : L2PcInstance = L2World.getPlayerByName( name )
    if ( !targetPlayer ) {
        return
    }

    party.setLeader( targetPlayer )
}