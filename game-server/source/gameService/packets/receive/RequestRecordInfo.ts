import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ExBrExtraUserInfo } from '../send/ExBrExtraUserInfo'
import { UserInfo } from '../send/UserInfo'
import { L2World } from '../../L2World'
import { L2Object } from '../../models/L2Object'
import { ProximalDiscoveryManager } from '../../cache/ProximalDiscoveryManager'
import aigle from 'aigle'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestRecordInfo( client: GameClient ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    player.sendDebouncedPacket( UserInfo )
    player.sendDebouncedPacket( ExBrExtraUserInfo )

    await aigle.resolve( L2World.getVisibleObjects( player, player.getBroadcastRadius() ) ).each( ( object: L2Object ) => {
        return ProximalDiscoveryManager.showObject( player, object )
    } )
}