import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { ItemLocation } from '../../enums/ItemLocation'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestSaveInventoryOrder( client: GameClient, packetData: Buffer ) : void {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let size : number = Math.min( 125, packet.readD() )

    for ( let index = 0; index < size; index++ ) {
        let objectId = packet.readD(), order = packet.readD()

        let item : L2ItemInstance = player.getInventory().getItemByObjectId( objectId )
        if ( item && item.getItemLocation() === ItemLocation.INVENTORY ) {
            item.setItemLocationProperties( ItemLocation.INVENTORY, order )
            item.scheduleDatabaseUpdate()
        }
    }
}