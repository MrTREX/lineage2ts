import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Object } from '../../models/L2Object'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { ExVoteSystemInfoWithPlayer } from '../send/ExVoteSystemInfo'
import { UserInfo } from '../send/UserInfo'
import { ExBrExtraUserInfo } from '../send/ExBrExtraUserInfo'
import { ConfigManager } from '../../../config/ConfigManager'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 2, // time-to-live value of token bucket (in seconds)
} )

export function RequestVoteNew( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    if ( player.getRecommendationsLeft() <= 0 ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CURRENTLY_DO_NOT_HAVE_ANY_RECOMMENDATIONS ) )
        return
    }

    let object : L2Object = player.getTarget()

    if ( !object ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SELECT_TARGET ) )
        return
    }

    if ( !object.isPlayer() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_IS_INCORRECT ) )
        return
    }

    let target : L2PcInstance = object as L2PcInstance
    if ( target.getObjectId() === player.getObjectId() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_RECOMMEND_YOURSELF ) )
        return
    }


    if ( target.getRecommendationsHave() >= 255 ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOUR_TARGET_NO_LONGER_RECEIVE_A_RECOMMENDATION ) )
        return
    }

    let targetId : number = new ReadableClientPacket( packetData ).readD()

    if ( target.getObjectId() !== targetId ) {
        return
    }

    player.giveRecommendations( target )

    let playerPacket = new SystemMessageBuilder( SystemMessageIds.YOU_HAVE_RECOMMENDED_C1_YOU_HAVE_S2_RECOMMENDATIONS_LEFT )
            .addPlayerCharacterName( target )
            .addNumber( player.getRecommendationsLeft() )
            .getBuffer()
    player.sendOwnedData( playerPacket )

    let targetPacket = new SystemMessageBuilder( SystemMessageIds.YOU_HAVE_BEEN_RECOMMENDED_BY_C1 )
            .addPlayerCharacterName( player )
            .getBuffer()
    target.sendOwnedData( targetPacket )

    player.sendDebouncedPacket( UserInfo )
    player.sendDebouncedPacket( ExBrExtraUserInfo )
    target.broadcastUserInfo()

    if ( ConfigManager.character.isNevitEnabled() ) {
        player.sendOwnedData( ExVoteSystemInfoWithPlayer( player ) )
        target.sendOwnedData( ExVoteSystemInfoWithPlayer( target ) )
    }
}