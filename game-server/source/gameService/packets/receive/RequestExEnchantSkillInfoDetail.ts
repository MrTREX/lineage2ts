import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2EnchantSkillLearn } from '../../models/L2EnchantSkillLearn'
import { DataManager } from '../../../data/manager'
import { EnchantType, ExEnchantSkillInfoDetail } from '../send/ExEnchantSkillInfoDetail'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'

export function RequestExEnchantSkillInfoDetail( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let type: EnchantType = packet.readD(), skillId = packet.readD(), skillLevel = packet.readD()

    let requiredSkillLevel = -2

    if ( type === EnchantType.Normal || type === EnchantType.Safe ) {
        requiredSkillLevel = skillLevel - 1
    } else if ( type === EnchantType.Untrain ) {
        requiredSkillLevel = skillLevel + 1
    } else if ( type === EnchantType.ChangeRoute ) {
        requiredSkillLevel = skillLevel
    }

    let playerSkillLevel = player.getSkillLevel( skillId )
    if ( playerSkillLevel === -1 ) {
        return
    }

    if ( requiredSkillLevel % 100 === 0 ) {
        let skillLearn: L2EnchantSkillLearn = DataManager.getSkillData().getSkillEnchantmentBySkillId( skillId )
        if ( !skillLearn || playerSkillLevel !== skillLearn.getBaseLevel() ) {
            return
        }
    } else if ( playerSkillLevel !== requiredSkillLevel
            && type === 3
            && playerSkillLevel % 100 !== skillLevel % 100 ) {
        return
    }

    player.sendOwnedData( ExEnchantSkillInfoDetail( type, skillId, skillLevel, player ) )
}