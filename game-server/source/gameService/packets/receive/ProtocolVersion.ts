import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'

export function ProtocolVersion( data: Buffer ) : number {
    return new ReadableClientPacket( data ).readD()
}