import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { FastRateLimit } from 'fast-ratelimit'
import { ReadableClientPacketPool } from '../../../packets/ReadableClientPacket'
import { DataManager } from '../../../data/manager'
import { ExBrProductInfo } from '../send/ExBrProductInfo'
import { ConfigManager } from '../../../config/ConfigManager'
import { ActionFailed } from '../send/ActionFailed'

const packetLimiter = new FastRateLimit( {
    threshold: 2, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestBrProductInfo( client: GameClient, packetData: Buffer ) : void {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( !ConfigManager.character.isGamePointsEnabled() || !ConfigManager.character.useItemShopPoints() ) {
        return player.sendOwnedData( ActionFailed() )
    }

    let packet = ReadableClientPacketPool.getValue().setBuffer( packetData )
    let productId = packet.readD()

    ReadableClientPacketPool.recycleValue( packet )

    let product = DataManager.getGamePointProducts().getProduct( productId )
    if ( !product ) {
        return
    }

    player.sendOwnedData( ExBrProductInfo( product ) )
}