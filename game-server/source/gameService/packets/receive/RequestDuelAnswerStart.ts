import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { DuelManager } from '../../instancemanager/DuelManager'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 3, // time-to-live value of token bucket (in seconds)
} )

export function RequestDuelAnswerStart( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let requester : L2PcInstance = player.getActiveRequester()
    if ( requester ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let partyDuelValue = packet.readD()
    packet.skipD( 1 )
    let response = packet.readD()

    if ( response === 1 ) {

        if ( requester.isInDuel() ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.C1_CANNOT_DUEL_BECAUSE_C1_IS_ALREADY_ENGAGED_IN_A_DUEL )
                    .addString( requester.getName() )
                    .getBuffer()
            player.sendOwnedData( packet )
            return
        }

        if ( player.isInDuel() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ARE_UNABLE_TO_REQUEST_A_DUEL_AT_THIS_TIME ) )
            return
        }

        if ( partyDuelValue === 1 ) {
            let playerMessage = new SystemMessageBuilder( SystemMessageIds.YOU_HAVE_ACCEPTED_C1_CHALLENGE_TO_A_PARTY_DUEL_THE_DUEL_WILL_BEGIN_IN_A_FEW_MOMENTS )
                    .addString( requester.getName() )
                    .getBuffer()
            player.sendOwnedData( playerMessage )

            let targetMessage = new SystemMessageBuilder( SystemMessageIds.S1_HAS_ACCEPTED_YOUR_CHALLENGE_TO_DUEL_AGAINST_THEIR_PARTY_THE_DUEL_WILL_BEGIN_IN_A_FEW_MOMENTS )
                    .addString( player.getName() )
                    .getBuffer()
            requester.sendOwnedData( targetMessage )
        } else {
            let playerMessage = new SystemMessageBuilder( SystemMessageIds.YOU_HAVE_ACCEPTED_C1_CHALLENGE_TO_A_DUEL_THE_DUEL_WILL_BEGIN_IN_A_FEW_MOMENTS )
                    .addString( requester.getName() )
                    .getBuffer()
            player.sendOwnedData( playerMessage )

            let targetMessage = new SystemMessageBuilder( SystemMessageIds.C1_HAS_ACCEPTED_YOUR_CHALLENGE_TO_A_DUEL_THE_DUEL_WILL_BEGIN_IN_A_FEW_MOMENTS )
                    .addString( player.getName() )
                    .getBuffer()
            requester.sendOwnedData( targetMessage )
        }

        DuelManager.addDuel( requester, player, partyDuelValue === 1 )
    } else if ( response === -1 ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.C1_IS_SET_TO_REFUSE_DUEL_REQUEST )
                .addPlayerCharacterName( player )
                .getBuffer()
        requester.sendOwnedData( packet )
    } else {
        if ( partyDuelValue === 1 ) {
            requester.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THE_OPPOSING_PARTY_HAS_DECLINED_YOUR_CHALLENGE_TO_A_DUEL ) )
        } else {
            let packet = new SystemMessageBuilder( SystemMessageIds.C1_HAS_DECLINED_YOUR_CHALLENGE_TO_A_DUEL )
                    .addPlayerCharacterName( player )
                    .getBuffer()
            requester.sendOwnedData( packet )
        }
    }

    player.setActiveRequester( null )
    requester.onTransactionResponse()
}