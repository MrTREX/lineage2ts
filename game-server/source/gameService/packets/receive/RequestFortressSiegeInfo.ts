import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { Fort } from '../../models/entity/Fort'
import { FortManager } from '../../instancemanager/FortManager'
import { ExShowFortressSiegeInfo } from '../send/ExShowFortressSiegeInfo'

export function RequestFortressSiegeInfo( client: GameClient ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    FortManager.getForts().forEach( ( fort: Fort ) => {
        if ( fort.getSiege().isInProgress() ) {
            player.sendOwnedData( ExShowFortressSiegeInfo( fort ) )
        }
    } )
}