import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { Location } from '../../models/Location'
import { ActionFailed } from '../send/ActionFailed'
import { Skill } from '../../models/Skill'
import { SkillCache } from '../../cache/SkillCache'
import { GeneralHelper } from '../../helpers/GeneralHelper'
import { ValidateLocation } from '../send/ValidateLocation'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'

export async function RequestExMagicSkillUseGround( client: GameClient, packetData: Buffer ): Promise<any> {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let x = packet.readD(),
            y = packet.readD(),
            z = packet.readD(),
            skillId = packet.readD(),
            controlPressedValue = packet.readD(),
            shiftPressedValue = packet.readC()

    let level = player.getSkillLevel( skillId )
    if ( level <= 0 ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    let skill: Skill = SkillCache.getSkill( skillId, level )
    if ( !skill ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    player.setGroundSkillLocation( new Location( x, y, z ) )

    player.setHeading( GeneralHelper.calculateHeadingFromCoordinates( player.getX(), player.getY(), x, y ) )
    BroadcastHelper.dataBasedOnVisibility( player, ValidateLocation( player ) )

    return player.useMagic( skill, controlPressedValue !== 0, shiftPressedValue !== 0 )
}