import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { IBypassHandler } from '../../handler/IBypassHandler'
import { BypassManager } from '../../handler/managers/BypassManager'

const command = 'arenalist'

export async function RequestOlympiadMatchList( client: GameClient ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let handler : IBypassHandler = BypassManager.getHandler( command )
    if ( handler ) {
        await handler.onStart( command, player, null )
    }
}