import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ConfigManager } from '../../../config/ConfigManager'
import { PacketVariables } from '../PacketVariables'
import { ActionFailed } from '../send/ActionFailed'
import { ItemContainer } from '../../models/itemcontainer/ItemContainer'
import { L2Npc } from '../../models/actor/L2Npc'
import { L2World } from '../../L2World'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { L2PcInstanceValues } from '../../values/L2PcInstanceValues'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import aigle from 'aigle'
import { FastRateLimit } from 'fast-ratelimit'
import { PlayerPermission } from '../../enums/PlayerPermission'
import { ClanPrivilege } from '../../enums/ClanPriviledge'
import { ObjectPool } from '../../helpers/ObjectPoolHelper'

interface DepositItem {
    objectId: number
    count: number
}

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 2, // time-to-live value of token bucket (in seconds)
} )

const DepositItemObjectPool = new ObjectPool<DepositItem>( 'Packet:SendWareHouseWithDrawList',() : DepositItem => {
    return {
        count: 0,
        objectId: 0
    }
} )

export async function SendWareHouseWithDrawList( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return player.sendMessage( 'You are withdrawing items too fast.' )
    }

    if ( player.getActiveEnchantItemId() !== L2PcInstanceValues.EmptyId ) {
        // TODO : record violation
        return
    }

    if ( !ConfigManager.character.karmaPlayerCanUseWareHouse() && ( player.getKarma() > 0 ) ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let totalItems: number = packet.readD()

    if ( ( totalItems < 1 ) || ( totalItems > PacketVariables.maximumItemsLimit ) ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    let items: Array<DepositItem> = []

    while ( totalItems > 0 ) {
        let objectId = packet.readD(), count = packet.readQ()

        if ( count < 1 || objectId < 1 ) {
            DepositItemObjectPool.recycleValues( items )
            return player.sendOwnedData( ActionFailed() )
        }

        let item = DepositItemObjectPool.getValue()

        item.count = count
        item.objectId = objectId

        items.push( item )

        totalItems--
    }

    await processItems( player, items )

    DepositItemObjectPool.recycleValues( items )
}

async function processItems( player: L2PcInstance, items: Array<DepositItem> ) : Promise<void> {
    let warehouse: ItemContainer = player.getActiveWarehouse()
    if ( !warehouse ) {
        return
    }

    let manager: L2Npc = L2World.getObjectById( player.getLastFolkNPC() ) as L2Npc
    if ( ( !manager
        || !manager.isWarehouse()
        || !player.canInteract( manager ) ) && !player.isGM() ) {
        return
    }

    if ( !warehouse.isPrivate() && !player.getAccessLevel().hasPermission( PlayerPermission.AllowTransaction ) ) {
        player.sendMessage( 'Transactions are disabled for your Access Level.' )
        return
    }


    if ( ConfigManager.character.membersCanWithdrawFromClanWH() && warehouse.isClanWarehouse() ) {
        if ( !player.hasClanPrivilege( ClanPrivilege.WarehouseAccess ) ) {
            return
        }

        if ( !player.isClanLeader() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ONLY_CLAN_LEADER_CAN_RETRIEVE_ITEMS_FROM_CLAN_WAREHOUSE ) )
            return
        }
    }

    let weight = 0
    let slots = 0
    let shouldExit = items.some( ( depositItem: DepositItem ) => {
        let item: L2ItemInstance = warehouse.getItemByObjectId( depositItem.objectId )
        if ( !item || ( item.getCount() < depositItem.count ) ) {
            // TODO : record violation
            return true
        }

        weight += depositItem.count * item.getItem().getWeight()
        if ( !item.isStackable() ) {
            slots += depositItem.count
        } else if ( !player.getInventory().getItemByItemId( item.getId() ) ) {
            slots++
        }
    } )

    if ( shouldExit ) {
        return player.sendOwnedData( ActionFailed() )
    }

    if ( !player.getInventory().validateCapacity( slots ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SLOTS_FULL ) )
        return
    }

    if ( !player.getInventory().validateWeight( weight ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.WEIGHT_LIMIT_EXCEEDED ) )
        return
    }

    await aigle.resolve( items ).eachSeries( async ( depositItem: DepositItem ) => {
        /*
            L2J has double verification for warehouse item, it is not needed since we verified item exists earlier
         */

        await warehouse.transferItem( depositItem.objectId, depositItem.count, player.getInventory(), 0, 'SendWareHouseWithDrawList' )
    } )
}