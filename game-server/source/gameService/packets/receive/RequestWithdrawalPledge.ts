import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Clan } from '../../models/L2Clan'
import { ConfigManager } from '../../../config/ConfigManager'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { PledgeShowMemberListDelete } from '../send/PledgeShowMemberListDelete'
import { PacketVariables } from '../PacketVariables'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestWithdrawalPledge( client: GameClient ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( !player.getClan() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ARE_NOT_A_CLAN_MEMBER ) )
        return
    }

    if ( player.isClanLeader() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLAN_LEADER_CANNOT_WITHDRAW ) )
        return
    }

    if ( player.isInCombat() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_LEAVE_DURING_COMBAT ) )
        return
    }

    let clan : L2Clan = player.getClan()
    await clan.removeClanMember( player.getObjectId(), Date.now() + PacketVariables.millisInDay * ConfigManager.character.getDaysBeforeJoinAClan() )

    let clanUpdate = new SystemMessageBuilder( SystemMessageIds.S1_HAS_WITHDRAWN_FROM_THE_CLAN )
            .addString( player.getName() )
            .getBuffer()
    clan.broadcastDataToOnlineMembers( clanUpdate )
    clan.broadcastDataToOnlineMembers( PledgeShowMemberListDelete( player.getName() ) )

    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_HAVE_WITHDRAWN_FROM_CLAN ) )
    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_MUST_WAIT_BEFORE_JOINING_ANOTHER_CLAN ) )
}