import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { CastleManorManager } from '../../instancemanager/CastleManorManager'
import { ActionFailed } from '../send/ActionFailed'
import { L2Npc } from '../../models/actor/L2Npc'
import { L2World } from '../../L2World'
import { PacketVariables } from '../PacketVariables'
import { SeedProduction } from '../../models/manor/SeedProduction'
import { L2Seed } from '../../models/L2Seed'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'
import { ClanPrivilege } from '../../enums/ClanPriviledge'

const packetLimiter = new FastRateLimit( {
    threshold: 2, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestSetSeed( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player : L2PcInstance = client.player
    if ( !player
        || !player.getClan()
        || !CastleManorManager.isModifiablePeriod()
        || !player.hasClanPrivilege( ClanPrivilege.CastleManor ) ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    let npc : L2Npc = L2World.getObjectById( player.getLastFolkNPC() ) as L2Npc
    if ( !npc || !player.canInteract( npc ) ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let manorId = packet.readD(), count = packet.readD()

    if ( player.getClan().getCastleId() !== manorId || count < 1 || count > PacketVariables.maximumItemsLimit ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    let items : Array<SeedProduction> = []
    while ( count > 0 ) {
        let seedId = packet.readD(), sales = packet.readQ(), price = packet.readQ()

        // TODO : more advanced validation
        if ( ( seedId < 1 ) || ( sales < 0 ) || ( price < 0 ) ) {
            return
        }

        items.push( new SeedProduction( seedId, sales, price, sales ) )
        count--
    }

    let updatedItems : Array<SeedProduction> = items.filter( ( item: SeedProduction ) => {
        let seed: L2Seed = CastleManorManager.getSeed( item.getId() )
        return seed
                && ( item.getStartAmount() <= seed.getSeedLimit() )
                && ( item.getPrice() >= seed.getSeedMinPrice() )
                && ( item.getPrice() <= seed.getSeedMaxPrice() )
    } )

    return CastleManorManager.setNextSeedProduction( manorId, updatedItems )
}