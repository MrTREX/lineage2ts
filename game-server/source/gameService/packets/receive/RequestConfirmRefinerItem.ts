import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { CrystalType } from '../../models/items/type/CrystalType'
import { LifeStone, LifeStoneCache } from '../../cache/LifeStoneCache'
import { ExPutIntensiveResultForVariationMake } from '../send/ExPutIntensiveResultForVariationMake'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { isValidForAugmentation } from '../../helpers/ItemHelper'

export function RequestConfirmRefinerItem( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let targetId = packet.readD(), refinerId = packet.readD()

    let targetItem : L2ItemInstance = player.getInventory().getItemByObjectId( targetId )
    if ( !targetItem ) {
        return
    }

    let refinerItem : L2ItemInstance = player.getInventory().getItemByObjectId( refinerId )
    if ( !refinerItem ) {
        return
    }

    if ( !isValidForAugmentation( player, targetItem, refinerItem ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THIS_IS_NOT_A_SUITABLE_ITEM ) )
        return
    }

    let refinerItemId = refinerItem.getItem().getId()
    let grade : CrystalType = targetItem.getItem().getItemGrade()
    let lifeStone : LifeStone = LifeStoneCache.getLifeStone( refinerItemId )
    let gemStoneId : number = LifeStoneCache.getGemStoneId( grade )
    let gemStoneCount : number = LifeStoneCache.getGemStoneCount( grade, lifeStone )

    player.sendOwnedData( ExPutIntensiveResultForVariationMake( refinerId, refinerItemId, gemStoneId, gemStoneCount ) )
}