import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { CharacterNamesCache } from '../../cache/CharacterNamesCache'
import { L2World } from '../../L2World'
import { FastRateLimit } from 'fast-ratelimit'
import { PlayerFriendsCache } from '../../cache/PlayerFriendsCache'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestFriendList( client: GameClient ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.FRIEND_LIST_HEADER ) )

    let playerFriendsIds = PlayerFriendsCache.getFriends( player.getObjectId() )
    if ( playerFriendsIds.size > 0 ) {
        playerFriendsIds.forEach( ( friendId: number ) => {
            let name : string = CharacterNamesCache.getCachedNameById( friendId )
            if ( !name ) {
                return
            }

            let friend : L2PcInstance = L2World.getPlayer( friendId )
            let message = !friend || !friend.isOnline() ? SystemMessageIds.S1_OFFLINE : SystemMessageIds.S1_ONLINE
            let packet = new SystemMessageBuilder( message )
                    .addString( name )
                    .getBuffer()
            player.sendOwnedData( packet )
        } )
    }

    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.FRIEND_LIST_FOOTER ) )
}