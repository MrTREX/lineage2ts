import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ActionFailed } from '../send/ActionFailed'
import { RecipeShopSellList } from '../send/RecipeShopSellList'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestRecipeShopManagePreview( client: GameClient ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let target = player.getTarget()
    if ( player.isAlikeDead() || !target || !target.isPlayer() ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    player.sendOwnedData( RecipeShopSellList( player, target as L2PcInstance ) )
}