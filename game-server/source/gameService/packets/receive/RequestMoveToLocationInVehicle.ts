import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ConfigManager } from '../../../config/ConfigManager'
import { ActionFailed } from '../send/ActionFailed'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { WeaponType } from '../../models/items/type/WeaponType'
import { L2BoatInstance } from '../../models/actor/instance/L2BoatInstance'
import { BoatManager } from '../../instancemanager/BoatManager'
import { Location } from '../../models/Location'
import { StopMoveInVehicle } from '../send/StopMoveInVehicle'
import { MoveToLocationInVehicle } from '../send/MoveToLocationInVehicle'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { L2World } from '../../L2World'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 3, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestMoveToLocationInVehicle( client: GameClient, packetData: Buffer ): void {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( ( ConfigManager.character.getNpcTalkBlockingTime() > 0 ) && !player.isGM() && ( player.getNotMoveUntil() > Date.now() ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_MOVE_WHILE_SPEAKING_TO_AN_NPC ) )
        player.sendOwnedData( ActionFailed() )
        return
    }

    if ( player.isAttackingNow() && player.getActiveWeaponItem() && ( player.getActiveWeaponItem().getItemType() === WeaponType.BOW ) ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    if ( player.isSitting() || player.isMovementDisabled() ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    if ( player.hasSummon() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.RELEASE_PET_ON_BOAT ) )
        player.sendOwnedData( ActionFailed() )
        return
    }

    if ( player.isTransformed() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_POLYMORPH_ON_BOAT ) )
        player.sendOwnedData( ActionFailed() )
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let boatId = packet.readD(),
            targetX = packet.readD(),
            targetY = packet.readD(),
            targetZ = packet.readD(),
            originX = packet.readD(),
            originY = packet.readD(),
            originZ = packet.readD()

    if ( ( targetX === originX ) && ( targetY === originY ) && ( targetZ === originZ ) ) {
        player.sendOwnedData( StopMoveInVehicle( player, boatId ) )
        return
    }

    if ( player.isInBoat() ) {
        let boat = player.getBoat()
        if ( boat.getObjectId() !== boatId ) {
            player.sendOwnedData( ActionFailed() )
            return
        }
    } else {
        if ( !BoatManager.isBoat( boatId ) ) {
            player.sendOwnedData( ActionFailed() )
            return
        }

        let boat = L2World.getObjectById( boatId ) as L2BoatInstance
        if ( !boat || !boat.isInsideRadius( player, 300, true ) ) {
            player.sendOwnedData( ActionFailed() )
            return
        }

        player.setVehicle( boat )
    }

    let vehiclePosition: Location = new Location( targetX, targetY, targetZ )
    let startPosition: Location = new Location( originX, originY, originZ )

    player.setInVehiclePosition( vehiclePosition )
    return BroadcastHelper.dataToSelfInRange( player,
            MoveToLocationInVehicle(
                    player,
                    vehiclePosition,
                    startPosition ) )
}