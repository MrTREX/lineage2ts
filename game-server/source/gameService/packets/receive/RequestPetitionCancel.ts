import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { PetitionManager } from '../../instancemanager/PetitionManager'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { ConfigManager } from '../../../config/ConfigManager'
import { AdminManager } from '../../cache/AdminManager'
import { PacketDispatcher } from '../../PacketDispatcher'
import { CreatureSay } from '../send/builder/CreatureSay'
import { FastRateLimit } from 'fast-ratelimit'
import { NpcSayType } from '../../enums/packets/NpcSayType'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestPetitionCancel( client: GameClient ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( PetitionManager.isPlayerInConsultation( player ) ) {
        if ( player.isGM() ) {
            PetitionManager.endActivePetition( player )
            return
        }

        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.PETITION_UNDER_PROCESS ) )

        return
    }

    if ( PetitionManager.isPlayerPetitionPending( player ) ) {
        if ( PetitionManager.cancelActivePetition( player ) ) {
            let remainingCount = ConfigManager.character.getMaxPetitionsPerPlayer() - PetitionManager.getPlayerTotalPetitionCount( player )

            let cancelStatus = new SystemMessageBuilder( SystemMessageIds.PETITION_CANCELED_SUBMIT_S1_MORE_TODAY )
                    .addString( remainingCount.toString() )
                    .getBuffer()
            player.sendOwnedData( cancelStatus )

            let line = `${ player.getName() } cancelled petition.`

            AdminManager.getAllGMs().forEach( ( adminId: number ) => {
                PacketDispatcher.sendOwnedData( adminId, CreatureSay.fromText( adminId, player.getObjectId(), NpcSayType.HeroVoice, 'Petition System', line ) )
            } )

            return
        }

        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.FAILED_CANCEL_PETITION_TRY_LATER ) )

        return
    }

    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.PETITION_NOT_SUBMITTED ) )
}