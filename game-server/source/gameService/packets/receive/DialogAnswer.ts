import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { ConfirmationAction } from '../../enums/confirmationAction'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { AdminCommandManager } from '../../handler/managers/AdminCommandManager'
import { IAdminCommand } from '../../handler/IAdminCommand'
import { EventTerminationResult, EventType, PlayerAnswerDialogEvent } from '../../models/events/EventType'
import { ListenerCache } from '../../cache/ListenerCache'
import { DoorRequestVariable, PcRequestVariable, VariableNames } from '../../variables/VariableTypes'
import { PlayerVariablesManager } from '../../variables/PlayerVariablesManager'
import { L2World } from '../../L2World'
import { L2DoorInstance } from '../../models/actor/instance/L2DoorInstance'
import { EventPoolCache } from '../../cache/EventPoolCache'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'
import { DialogAnswerValue } from '../../enums/DialogAnswerValue'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function DialogAnswer( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let messageId = packet.readD(), answer = packet.readD(), requesterId = packet.readD()

    if ( ListenerCache.hasGeneralListener( EventType.PlayerAnswerDialog ) ) {
        let eventData = EventPoolCache.getData( EventType.PlayerAnswerDialog ) as PlayerAnswerDialogEvent

        eventData.playerId = player.getObjectId()
        eventData.messageId = messageId
        eventData.answer = answer
        eventData.requesterId = requesterId

        let result: EventTerminationResult = await ListenerCache.getGeneralTerminatedResult( EventType.PlayerAnswerDialog, eventData )
        if ( result && result.terminate ) {
            return
        }
    }

    if ( messageId === SystemMessageIds.S1 ) {
        if ( player.removeConfirmationAction( ConfirmationAction.AdminCommand ) ) {
            let commandValue = player.getAdminConfirmCommand()
            player.setAdminConfirmCommmand( null )

            if ( answer === 0 ) {
                return
            }

            let command: string = commandValue.split( ' ' )[ 0 ]
            let handler: IAdminCommand = AdminCommandManager.getHandler( command )
            if ( player.getAccessLevel().canExecute( command ) ) {
                await handler.onCommand( commandValue, player )
            }
        }
        return
    }

    if ( ( messageId === SystemMessageIds.RESURRECTION_REQUEST_BY_C1_FOR_S2_XP )
            || ( messageId === SystemMessageIds.RESURRECT_USING_CHARM_OF_COURAGE ) ) {
        player.reviveAnswer( answer )
        return
    }

    if ( answer !== DialogAnswerValue.Yes ) {
        return
    }

    switch ( messageId ) {
        case SystemMessageIds.C1_WISHES_TO_SUMMON_YOU_FROM_S2_DO_YOU_ACCEPT:
            return onPlayerSummon( player, requesterId )

        case SystemMessageIds.WOULD_YOU_LIKE_TO_OPEN_THE_GATE:
            return onPlayerOpensGate( player )

        case SystemMessageIds.WOULD_YOU_LIKE_TO_CLOSE_THE_GATE:
            return onPlayerClosesGate( player )
    }
}

async function onPlayerSummon( player: L2PcInstance, requesterId: number ) : Promise<void> {
    let data: PcRequestVariable = PlayerVariablesManager.get( player.getObjectId(), VariableNames.pcRequest ) as PcRequestVariable
    PlayerVariablesManager.remove( player.getObjectId(), VariableNames.pcRequest )

    if ( data && data.playerId === requesterId && data.expireTime > Date.now() ) {
        let otherPlayer = L2World.getPlayer( data.playerId )
        if ( !otherPlayer || !otherPlayer.canSummonTarget( player ) ) {
            return
        }

        if ( data.itemId !== 0 && data.amount !== 0 ) {
            if ( player.getInventory().getInventoryItemCount( data.itemId, 0 ) < data.amount ) {

                let packet = new SystemMessageBuilder( SystemMessageIds.S1_REQUIRED_FOR_SUMMONING )
                        .addItemNameWithId( data.amount )
                        .getBuffer()
                player.sendOwnedData( packet )
                return
            }

            await player.getInventory().destroyItemByItemId( data.itemId, data.amount, 0, 'DialogAnswer.onPlayerSummon' )

            let packet = new SystemMessageBuilder( SystemMessageIds.S1_DISAPPEARED )
                    .addItemNameWithId( data.itemId )
                    .getBuffer()
            player.sendOwnedData( packet )
        }

        await player.teleportToLocation( otherPlayer, true )
    }

    return
}

function onPlayerOpensGate( player: L2PcInstance ) : void {
    let data = PlayerVariablesManager.get( player.getObjectId(), VariableNames.doorRequest ) as DoorRequestVariable
    PlayerVariablesManager.remove( player.getObjectId(), VariableNames.doorRequest )

    if ( data && data.objectId === player.getTargetId() ) {
        ( L2World.getObjectById( data.objectId ) as L2DoorInstance ).openMe()
    }
}

function onPlayerClosesGate( player: L2PcInstance ) : void {
    let data = PlayerVariablesManager.get( player.getObjectId(), VariableNames.doorRequest ) as DoorRequestVariable
    PlayerVariablesManager.remove( player.getObjectId(), VariableNames.doorRequest )
    if ( data && data.objectId === player.getTargetId() ) {
        ( L2World.getObjectById( data.objectId ) as L2DoorInstance ).closeMe()
    }
}