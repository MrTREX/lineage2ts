import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ExConfirmAddingContact } from '../send/ExConfirmAddingContact'
import { ConfigManager } from '../../../config/ConfigManager'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { PlayerContactCache } from '../../cache/PlayerContactCache'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 2, // time-to-live value of token bucket (in seconds)
} )

export async function RequestExAddContactToContactList( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player || !ConfigManager.general.allowMail() ) {
        return
    }

    let name : string = new ReadableClientPacket( packetData ).readS()
    if ( !name ) {
        return
    }

    let isAdded : boolean = await PlayerContactCache.add( player.getObjectId(), name )
    player.sendOwnedData( ExConfirmAddingContact( name, isAdded ) )
}