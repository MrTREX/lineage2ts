import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2PcInstanceValues } from '../../values/L2PcInstanceValues'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { EnchantScroll } from '../../models/items/enchant/EnchantScroll'
import { DataManager } from '../../../data/manager'
import { EnchantSupportItem } from '../../models/items/enchant/EnchantSupportItem'
import { EnchantResultType } from '../../enums/items/EnchantResultType'
import { CommonSkill } from '../../models/holders/SkillHolder'
import { Skill } from '../../models/Skill'
import { MagicSkillUseWithCharacters } from '../send/MagicSkillUse'
import { EnchantResult, EnchantResultOutcome } from '../send/EnchantResult'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { InventoryUpdateStatus } from '../../enums/InventoryUpdateStatus'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 2, // time-to-live value of token bucket (in seconds)
} )

export async function RequestEnchantItem( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( !player.isOnline() || client.isDetached() ) {
        player.setActiveEnchantItemId( L2PcInstanceValues.EmptyId )
        return
    }

    if ( player.isProcessingTransaction() || player.isInStoreMode() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_ENCHANT_WHILE_STORE ) )
        player.setActiveEnchantItemId( L2PcInstanceValues.EmptyId )
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let objectId = packet.readD(), supportId = packet.readD()

    let item: L2ItemInstance = player.getInventory().getItemByObjectId( objectId )
    let scroll: L2ItemInstance = player.getInventory().getItemByObjectId( player.getActiveEnchantItemId() )
    let support: L2ItemInstance = player.getInventory().getItemByObjectId( player.getActiveEnchantSupportItemId() )

    if ( !item || !scroll ) {
        player.setActiveEnchantItemId( L2PcInstanceValues.EmptyId )
        return
    }

    let scrollTemplate: EnchantScroll = DataManager.getEnchantItemData().getScrollById( scroll.getId() )
    if ( !scrollTemplate ) {
        return
    }

    let supportTemplate: EnchantSupportItem
    if ( support ) {
        if ( support.getObjectId() !== supportId ) {
            player.setActiveEnchantItemId( L2PcInstanceValues.EmptyId )
            return
        }

        supportTemplate = DataManager.getEnchantItemData().getSupportItemById( support.getId() )
    }

    if ( !scrollTemplate.isValid( item, supportTemplate ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INAPPROPRIATE_ENCHANT_CONDITION ) )
        player.setActiveEnchantItemId( L2PcInstanceValues.EmptyId )
        player.sendOwnedData( EnchantResult( EnchantResultOutcome.Cancelled, 0, 0 ) )
        return
    }

    if ( ( item.getOwnerId() !== player.getObjectId() ) || !item.isEnchantable() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INAPPROPRIATE_ENCHANT_CONDITION ) )
        player.setActiveEnchantItemId( L2PcInstanceValues.EmptyId )
        player.sendOwnedData( EnchantResult( EnchantResultOutcome.Cancelled, 0, 0 ) )
        return
    }

    // TODO : add check for max enchant level and use max enchant result

    let resultType: EnchantResultType = scrollTemplate.calculateSuccess( player, item, supportTemplate )
    if ( resultType === EnchantResultType.ERROR ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INAPPROPRIATE_ENCHANT_CONDITION ) )
        player.setActiveEnchantItemId( L2PcInstanceValues.EmptyId )
        player.sendOwnedData( EnchantResult( EnchantResultOutcome.Cancelled, 0, 0 ) )
        return
    }

    scroll = await player.getInventory().destroyItemByObjectId( scroll.getObjectId(), 1, 0, 'RequestEnchantItem' )
    if ( !scroll ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ITEMS ) )
        // TODO : violation, player does not have enchant scroll

        player.setActiveEnchantItemId( L2PcInstanceValues.EmptyId )
        player.sendOwnedData( EnchantResult( EnchantResultOutcome.Cancelled, 0, 0 ) )
        return
    }

    if ( support ) {
        support = await player.getInventory().destroyItemByObjectId( support.getObjectId(), 1, 0, 'RequestEnchantItem' )
        if ( !support ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ITEMS ) )
            // TODO : violation, does not have referenced support item

            player.setActiveEnchantItemId( L2PcInstanceValues.EmptyId )
            player.sendOwnedData( EnchantResult( EnchantResultOutcome.Cancelled, 0, 0 ) )
            return
        }
    }

    switch ( resultType ) {
        case EnchantResultType.SUCCESS:
            if ( scrollTemplate.getChance( player, item ) > 0 ) {
                await item.setEnchantLevel( item.getEnchantLevel() + 1 )
                player.getInventory().markItem( item, InventoryUpdateStatus.Modified )
            }

            player.sendOwnedData( EnchantResult( EnchantResultOutcome.Success, 0, 0 ) )

            let minEnchantAnnounce = item.isArmor() ? 6 : 7
            let maxEnchantAnnounce = item.isArmor() ? 0 : 15
            if ( ( item.getEnchantLevel() === minEnchantAnnounce ) || ( item.getEnchantLevel() === maxEnchantAnnounce ) ) {
                let packet = new SystemMessageBuilder( SystemMessageIds.C1_SUCCESSFULY_ENCHANTED_A_S2_S3 )
                        .addCharacterName( player )
                        .addNumber( item.getEnchantLevel() )
                        .addItemInstanceName( item )

                BroadcastHelper.dataToSelfInRange( player, packet.getBuffer() )

                let skill: Skill = CommonSkill.FIREWORK.getSkill()
                if ( skill ) {
                    BroadcastHelper.dataToSelfInRange( player,
                            MagicSkillUseWithCharacters(
                                    player,
                                    player,
                                    skill.getId(),
                                    skill.getLevel(),
                                    skill.getHitTime(),
                                    skill.getReuseDelay() ) )
                }
            }

            if ( ( item.isArmor() ) && ( item.getEnchantLevel() === 4 ) && item.isEquipped() ) {
                let enchant4Skill: Skill = item.getItem().getEnchant4Skill()
                if ( enchant4Skill ) {
                    await player.addSkill( enchant4Skill, false )
                    player.sendSkillList()
                }
            }
            break

        case EnchantResultType.FAILURE:
            if ( scrollTemplate.isSafe() ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SAFE_ENCHANT_FAILED ) )
                player.sendOwnedData( EnchantResult( EnchantResultOutcome.SafeFailure, 0, 0 ) )

            } else {
                if ( item.isEquipped() ) {
                    if ( item.getEnchantLevel() > 0 ) {
                        let packet = new SystemMessageBuilder( SystemMessageIds.EQUIPMENT_S1_S2_REMOVED )
                                .addNumber( item.getEnchantLevel() )
                                .addItemInstanceName( item )
                                .getBuffer()
                        player.sendOwnedData( packet )
                    } else {
                        let packet = new SystemMessageBuilder( SystemMessageIds.S1_DISARMED )
                                .addItemInstanceName( item )
                                .getBuffer()
                        player.sendOwnedData( packet )
                    }

                    await player.getInventory().unEquipItemInSlot( item.getLocationSlot() )
                    player.broadcastUserInfo()
                }

                if ( scrollTemplate.isBlessed() ) {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.BLESSED_ENCHANT_FAILED ) )

                    await item.setEnchantLevel( 0 )
                    player.getInventory().markItem( item, InventoryUpdateStatus.Modified )
                    player.sendOwnedData( EnchantResult( EnchantResultOutcome.BlessedFailure, 0, 0 ) )

                } else {
                    item = await player.getInventory().destroyItem( item, item.getCount(), 0, 'RequestEnchantItem' )
                    if ( !item ) {
                        // TODO : record violation, enchant item cannot be removed from player inventory
                        player.setActiveEnchantItemId( L2PcInstanceValues.EmptyId )
                        player.sendOwnedData( EnchantResult( EnchantResultOutcome.Cancelled, 0, 0 ) )
                        return
                    }

                    let crystalId = item.getItem().getCrystalItemId()
                    if ( ( crystalId !== 0 ) && item.getItem().isCrystallizable() ) {
                        let count = item.getCrystalCount() - ( ( item.getItem().getCrystalCount() + 1 ) / 2 )
                        count = Math.max( count, 1 )
                        await player.getInventory().addItem( crystalId, count, 0, 'RequestEnchantItem' )

                        let packet = new SystemMessageBuilder( SystemMessageIds.EARNED_S2_S1_S )
                                .addItemNameWithId( crystalId )
                                .addNumber( count )
                                .getBuffer()
                        player.sendOwnedData( packet )
                        player.sendOwnedData( EnchantResult( EnchantResultOutcome.Failure, crystalId, count ) )
                    } else {
                        player.sendOwnedData( EnchantResult( EnchantResultOutcome.UnrecoverableFailure, 0, 0 ) )
                    }
                }
            }
            break
    }

    player.broadcastUserInfo()
    player.setActiveEnchantItemId( L2PcInstanceValues.EmptyId )
}