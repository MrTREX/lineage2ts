import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { PartyDistributionTypeValues } from '../../enums/PartyDistributionType'
import { L2Party } from '../../models/L2Party'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'

export function RequestPartyLootModification( client: GameClient, packetData: Buffer ): void {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let type: number = new ReadableClientPacket( packetData ).readD()

    if ( !PartyDistributionTypeValues[ type ] ) {
        return
    }

    let party: L2Party = player.getParty()
    if ( !party || !party.isLeader( player ) || ( type === party.getDistributionType() ) ) {
        return
    }

    return party.requestLootChange( type )
}