import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2PcInstanceValues } from '../../values/L2PcInstanceValues'
import { ConfigManager } from '../../../config/ConfigManager'
import { PrivateStoreType } from '../../enums/PrivateStoreType'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { L2PetInstance } from '../../models/actor/instance/L2PetInstance'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestGiveItemToPet( client: GameClient, packetData: Buffer ): Promise<any> {
    let player: L2PcInstance = client.player
    if ( !player || !player.hasPet() ) {
        return
    }

    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return player.sendMessage( 'You are giving items to pet too fast.' )
    }

    if ( player.getActiveEnchantItemId() !== L2PcInstanceValues.EmptyId ) {
        return
    }

    // Karma punishment
    if ( !ConfigManager.character.karmaPlayerCanTrade() && player.getKarma() > 0 ) {
        return
    }

    if ( player.getPrivateStoreType() !== PrivateStoreType.None ) {
        player.sendMessage( 'You cannot exchange items while trading.' )
        return
    }

    let pet: L2PetInstance = player.getSummon() as L2PetInstance
    if ( pet.isDead() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_GIVE_ITEMS_TO_DEAD_PET ) )
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let objectId = packet.readD(), amount = packet.readQ()

    let item: L2ItemInstance = player.getInventory().getItemByObjectId( objectId )
    if ( !item ) {
        return
    }

    if ( amount > item.getCount() ) {
        return
    }

    if ( item.isAugmented() ) {
        return
    }

    if ( item.isHeroItem() || !item.isDropable() || !item.isDestroyable() || !item.isTradeable() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ITEM_NOT_FOR_PETS ) )
        return
    }

    if ( !pet.getInventory().validateCapacityForItemCount( item, amount ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOUR_PET_CANNOT_CARRY_ANY_MORE_ITEMS ) )
        return
    }

    if ( !pet.getInventory().validateWeightForItem( item, amount ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.UNABLE_TO_PLACE_ITEM_YOUR_PET_IS_TOO_ENCUMBERED ) )
        return
    }

    return player.transferItem( objectId, amount, pet.getInventory() )
}