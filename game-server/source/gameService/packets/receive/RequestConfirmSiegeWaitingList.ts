import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { CastleManager } from '../../instancemanager/CastleManager'
import { Castle } from '../../models/entity/Castle'
import { L2Clan } from '../../models/L2Clan'
import { ClanCache } from '../../cache/ClanCache'
import { SiegeDefenderList } from '../send/SiegeDefenderList'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestConfirmSiegeWaitingList( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( !player.getClan() ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let castleId = packet.readD()

    let castle: Castle = CastleManager.getCastleById( castleId )
    if ( !castle ) {
        return
    }

    if ( ( castle.getOwnerId() !== player.getClanId() ) || ( !player.isClanLeader() ) ) {
        return
    }

    let clanId = packet.readD()
    let clan: L2Clan = ClanCache.getClan( clanId )
    if ( !clan ) {
        return
    }

    if ( !castle.getSiege().getIsRegistrationOver() ) {
        if ( packet.readD() === 1 ) {
            if ( castle.getSiege().checkIsDefenderWaiting( clan ) ) {
                await castle.getSiege().approveSiegeDefenderClan( clanId )
            } else {
                return
            }
        } else {
            if ( ( castle.getSiege().checkIsDefenderWaiting( clan ) ) || ( castle.getSiege().checkIsDefender( clan ) ) ) {
                await castle.getSiege().removeSiegeClan( clan )
            }
        }
    }

    player.sendOwnedData( SiegeDefenderList( castle ) )
}