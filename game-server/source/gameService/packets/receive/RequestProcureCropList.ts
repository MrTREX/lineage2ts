import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { CastleManorManager } from '../../instancemanager/CastleManorManager'
import { ActionFailed } from '../send/ActionFailed'
import { L2Npc } from '../../models/actor/L2Npc'
import { L2World } from '../../L2World'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { PacketVariables } from '../PacketVariables'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { CropProcure } from '../../models/manor/CropProcure'
import { L2Item } from '../../models/items/L2Item'
import { ItemManagerCache } from '../../cache/ItemManagerCache'
import { ConfigManager } from '../../../config/ConfigManager'
import { DatabaseManager } from '../../../database/manager'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { InventoryAction } from '../../enums/InventoryAction'
import aigle from 'aigle'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestProcureCropList( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( CastleManorManager.isUnderMaintenance() ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    let manager: L2Npc = L2World.getObjectById( player.getLastFolkNPC() ) as L2Npc
    if ( !manager || !manager.isMerchant() || !player.canInteract( manager ) ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    let castleId: number = manager.getCastle().getResidenceId()
    if ( manager.getTemplate().getParameters()[ 'manor_id' ] !== castleId ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let count: number = packet.readD()

    if ( count < 1 || count > PacketVariables.maximumItemsLimit ) {
        return
    }

    let items: Array<CropData> = []
    while ( count > 0 ) {
        let objectId = packet.readD(), itemId = packet.readD(), manorId = packet.readD(), count = packet.readQ()

        if ( objectId < 1 || itemId < 1 || manorId < 0 || count < 0 ) {
            return
        }

        items.push( {
            objectId,
            itemId,
            manorId,
            count,
            rewardId: 0,
            procure: null,
        } )
    }

    let slots = 0
    let weight = 0

    let shouldExit = items.some( ( holder: CropData ) => {
        let item: L2ItemInstance = player.getInventory().getItemByObjectId( holder.objectId )
        if ( !item || ( item.getCount() < holder.count ) || ( item.getId() !== holder.itemId ) ) {
            player.sendOwnedData( ActionFailed() )
            return true
        }

        let procure: CropProcure = CastleManorManager.getCropProcure( holder.manorId, holder.itemId, false )
        if ( !procure || ( procure.getAmount() < holder.count ) ) {
            player.sendOwnedData( ActionFailed() )
            return true
        }

        let rewardId = CastleManorManager.getSeedByCrop( procure.getId() ).getReward( procure.getReward() )
        let template: L2Item = ItemManagerCache.getTemplate( rewardId )

        holder.procure = procure
        holder.rewardId = rewardId
        weight += ( holder.count * template.getWeight() )

        if ( !template.isStackable() ) {
            slots += holder.count
        } else if ( !player.getInventory().getItemByItemId( rewardId ) ) {
            slots++
        }

        return false
    } )

    if ( shouldExit ) {
        return
    }

    if ( !player.getInventory().validateWeight( weight ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.WEIGHT_LIMIT_EXCEEDED ) )
        return
    }

    if ( !player.getInventory().validateCapacity( slots ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SLOTS_FULL ) )
        return
    }

    let updateItems: Array<CropProcure> = []
    await aigle.resolve( items ).eachSeries( async ( holder: CropData ) => {
        let rewardPrice = ItemManagerCache.getTemplate( holder.rewardId ).getReferencePrice()
        if ( rewardPrice === 0 ) {
            return
        }

        let price = holder.count * holder.procure.getPrice()
        let rewardItemCount = price / rewardPrice
        if ( rewardItemCount < 1 ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.FAILED_IN_TRADING_S2_OF_CROP_S1 )
                    .addItemNameWithId( holder.itemId )
                    .addNumber( holder.count )
                    .getBuffer()

            player.sendOwnedData( packet )
            return
        }

        // Fee for selling to other manors
        let fee: number = ( castleId === holder.manorId ) ? 0 : Math.floor( price * 0.05 )
        if ( ( fee !== 0 ) && ( player.getAdena() < fee ) ) {
            let failedMessage = new SystemMessageBuilder( SystemMessageIds.FAILED_IN_TRADING_S2_OF_CROP_S1 )
                    .addItemNameWithId( holder.itemId )
                    .addNumber( holder.count )
                    .getBuffer()

            player.sendOwnedData( failedMessage )
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ADENA ) )
            return
        }

        if ( !holder.procure.decreaseAmount( holder.count )
                || ( ( fee > 0 ) && !( await player.reduceAdena( fee, true, 'RequestProcureCropList' ) ) )
                || !( await player.destroyItemByObjectId( holder.objectId, holder.count, true, 'RequestProcureCropList' ) ) ) {
            return
        }

        await player.addItem( holder.rewardId, rewardItemCount, -1, manager.getObjectId(), InventoryAction.Merchandise )

        if ( ConfigManager.general.manorSaveAllActions() ) {
            updateItems.push( holder.procure )
        }
    } )

    if ( updateItems.length > 0 ) {
        return DatabaseManager.getCastleManorProcure().updateItems( castleId, updateItems )
    }
}

interface CropData {
    objectId: number
    itemId: number
    manorId: number
    count: number
    rewardId: number
    procure: CropProcure
}