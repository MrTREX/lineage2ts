import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { RaidBossPointsManager } from '../../cache/RaidBossPointsManager'
import { ExGetBossRecord } from '../send/ExGetBossRecord'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestGetBossRecord( client: GameClient ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let points = RaidBossPointsManager.getPointsByOwnerId( player.getObjectId() )
    let ranking = RaidBossPointsManager.calculateRank( player.getObjectId() )
    let pointsMap = RaidBossPointsManager.getPointsMap( player.getObjectId() )

    player.sendOwnedData( ExGetBossRecord( ranking, points, pointsMap ) )
}