import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ActionFailed } from '../send/ActionFailed'
import { L2Object } from '../../models/L2Object'
import { L2NpcValues } from '../../values/L2NpcValues'
import { L2BuyList } from '../../models/buylist/L2BuyList'
import { BuyListManager } from '../../cache/BuyListManager'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { PacketVariables } from '../PacketVariables'
import { L2Item } from '../../models/items/L2Item'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { ExBuySellList } from '../send/ExBuySellList'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import _ from 'lodash'
import aigle from 'aigle'
import { FastRateLimit } from 'fast-ratelimit'
import { L2MerchantInstance } from '../../models/actor/instance/L2MerchantInstance'
import { recordBuySellViolation } from '../../helpers/PlayerViolations'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestRefundItem( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        player.sendOwnedData( ActionFailed() )
        player.sendMessage( 'You are using refund too fast.' )
        return
    }

    if ( !player.hasRefund() ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    let target: L2Object = player.getTarget()
    if ( !player.isGM()
            && ( !target
                    || !target.isMerchant()
                    || ( player.getInstanceId() !== target.getInstanceId() )
                    || !player.isInsideRadius( target, L2NpcValues.interactionDistance, true ) ) ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    let refund: ReadonlyArray<L2ItemInstance> = player.getRefund().getItems()
    let objectIds: Array<number> = _.map( refund, ( item: L2ItemInstance ) => {
        return item.getObjectId()
    } )

    if ( _.uniq( objectIds ).length !== refund.length ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let listId = packet.readD(), count = packet.readD()

    if ( count < 1 || count > PacketVariables.maximumItemsLimit ) {
        return
    }

    let buyList: L2BuyList = BuyListManager.getBuyList( listId )
    if ( !buyList ) {
        player.sendOwnedData( ActionFailed() )
        return recordBuySellViolation( player.getObjectId(), '59978492-2972-4b23-b588-c1b92c62156b', 'Player tries to refund with non-existent buylistId', listId )
    }

    if ( !player.isGM() && !buyList.isNpcAllowed( ( target as L2MerchantInstance ).getId() ) ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    let itemIndexes: Array<number> = []
    while ( count > 0 ) {
        itemIndexes.push( packet.readD() )
        count--
    }

    let weight = 0
    let adena = 0
    let slots = 0

    if ( _.uniq( itemIndexes ).length !== itemIndexes.length ) {
        player.sendOwnedData( ActionFailed() )
        return recordBuySellViolation( player.getObjectId(), 'd29e341e-6046-4c02-97ef-9f44ba9d56a8', 'Player has duplicate refund indexes', listId )
    }

    if ( _.max( itemIndexes ) >= refund.length ) {
        player.sendOwnedData( ActionFailed() )
        return recordBuySellViolation( player.getObjectId(), 'dc1647ed-7c0e-4682-8ad5-13ce1d84a69a','Player has requested out of bounds refund indexes', listId )
    }

    itemIndexes.forEach( ( index: number ) => {
        let item: L2ItemInstance = refund[ index ]
        let template: L2Item = item.getItem()

        let count = item.getCount()
        weight += count * template.getWeight()
        adena += ( count * template.getReferencePrice() ) / 2

        if ( !template.isStackable() ) {
            slots += count
        } else if ( !player.getInventory().getItemByItemId( template.getId() ) ) {
            slots++
        }
    } )

    if ( ( weight < 0 ) || !player.getInventory().validateWeight( weight ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.WEIGHT_LIMIT_EXCEEDED ) )
        player.sendOwnedData( ActionFailed() )
        return
    }

    if ( ( slots < 0 ) || !player.getInventory().validateCapacity( slots ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SLOTS_FULL ) )
        player.sendOwnedData( ActionFailed() )
        return
    }

    if ( ( adena < 0 ) || !( await player.reduceAdena( adena, false, 'RequestRefundItem' ) ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ADENA ) )
        player.sendOwnedData( ActionFailed() )
        return
    }

    await aigle.resolve( itemIndexes ).eachSeries( async ( index: number ) => {
        return player.getRefund().transferItem( objectIds[ index ], Number.MAX_SAFE_INTEGER, player.getInventory(), player.getLastFolkNPC(), 'RequestRefundItem' )
    } )

    player.sendOwnedData( ExBuySellList( player, true ) )
}