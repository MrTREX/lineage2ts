import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Party } from '../../models/L2Party'
import { L2PartyMessageType } from '../../enums/L2PartyMessageType'
import { PartyMatchManager, PartyMatchRoom } from '../../cache/partyMatchManager'
import { ExClosePartyRoom } from '../send/ExClosePartyRoom'
import { PartyMatchDetail } from '../send/PartyMatchDetail'
import { ExPartyRoomMember, ExPartyRoomMemberOperation } from '../send/ExPartyRoomMember'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 2, // time-to-live value of token bucket (in seconds)
} )

export function RequestWithDrawalParty( client: GameClient ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let party : L2Party = player.getParty()
    if ( !party ) {
        return
    }

    if ( party.isInDimensionalRift() && !party.getDimensionalRift().getRevivedAtWaitingRoom().includes( player.getObjectId() ) ) {
        return player.sendMessage( 'You can\'t exit party when you are in Dimensional Rift.' )
    }

    party.removePartyMember( player, L2PartyMessageType.Left )

    let room : PartyMatchRoom = PartyMatchManager.getPlayerRoom( player.getObjectId() )
    if ( !room ) {
        return
    }

    if ( room ) {
        PartyMatchManager.removePlayer( player.getObjectId() )

        player.sendOwnedData( PartyMatchDetail( room ) )
        player.sendOwnedData( ExPartyRoomMember( room, ExPartyRoomMemberOperation.Remove ) )
        player.sendOwnedData( ExClosePartyRoom() )
    }

    player.broadcastUserInfo()
}