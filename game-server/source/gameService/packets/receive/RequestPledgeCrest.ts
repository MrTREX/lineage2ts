import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { PledgeCrest } from '../send/PledgeCrest'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { CrestCache } from '../../cache/CrestCache'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 2, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestPledgeCrest( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let crestId : number = new ReadableClientPacket( packetData ).readD()
    let crest = CrestCache.getCrest( crestId )
    if ( !crest ) {
        return
    }

    player.sendOwnedData( PledgeCrest( crest ) )
}