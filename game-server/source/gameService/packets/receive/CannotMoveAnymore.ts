import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 2, // time-to-live value of token bucket (in seconds)
} )

export function CannotMoveAnymore( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    // TODO : reuse data or clean up
    //let packet = new ReadableClientPacket(packetData)
    //let x = packet.readD(), y = packet.readD(), z = packet.readD(), heading = packet.readD()

    AIEffectHelper.notifyMoveFinished( player )
}