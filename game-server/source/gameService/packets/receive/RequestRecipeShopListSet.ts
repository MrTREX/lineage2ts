import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { PacketVariables } from '../PacketVariables'
import { PrivateStoreType } from '../../enums/PrivateStoreType'
import { ActionFailed } from '../send/ActionFailed'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { RecipeShopMessage } from '../send/RecipeShopMesssage'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'
import { getMaxAdena } from '../../helpers/ConfigurationHelper'
import { AreaFlags } from '../../models/areas/AreaFlags'
import { recordDataViolation } from '../../helpers/PlayerViolations'
import { DataIntegrityViolationType } from '../../models/events/EventType'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestRecipeShopListSet( client: GameClient, packetData: Buffer ) : Promise<void> {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( player.isUnderAttack() || player.isInDuel() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_OPERATE_PRIVATE_STORE_DURING_COMBAT ) )
        player.sendOwnedData( ActionFailed() )
        return
    }

    if ( player.hasAreaFlags( AreaFlags.NoOpenStore ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NO_PRIVATE_WORKSHOP_HERE ) )
        player.sendOwnedData( ActionFailed() )
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let count = packet.readD()

    if ( count <= 0 || count > PacketVariables.maximumItemsLimit ) {
        player.setPrivateStoreType( PrivateStoreType.None )
        player.broadcastUserInfo()
        return
    }

    player.clearManufatureItems()

    if ( !await addManufactureItems( count, packet, player ) ) {
        player.clearManufatureItems()
        return
    }

    player.setStoreName( !player.hasManufactureShop() ? '' : player.getStoreName() )
    player.setPrivateStoreType( PrivateStoreType.Manufacture )
    await player.sitDown()
    player.broadcastUserInfo()

    return BroadcastHelper.dataToSelfBasedOnVisibility( player, RecipeShopMessage( player ) )
}

async function addManufactureItems( count: number, packet: ReadableClientPacket, player: L2PcInstance ) : Promise<boolean> {
    let maxAdena = getMaxAdena()

    while ( count > 0 ) {
        let recipeId = packet.readD(), price = packet.readQ()

        if ( price < 0 ) {
            return false
        }

        if ( !player.inRecipeBook( recipeId ) ) {
            await recordDataViolation( player.getObjectId(), 'f25c5c8f-b8e8-463c-a657-d11e1dd8f95b', DataIntegrityViolationType.Recipe, 'Player knows no such recipe', [ recipeId ] )
            return false
        }

        if ( price >= maxAdena ) {
            await recordDataViolation( player.getObjectId(), '8c0a505b-186f-42d8-8276-c926fb003259', DataIntegrityViolationType.Recipe, 'Player reached max adena on recipe price', [ recipeId, price ] )
            return false
        }

        player.addManufactureItem( recipeId, price )
        count--
    }

    return true
}