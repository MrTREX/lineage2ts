import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ClassId } from '../../models/base/ClassId'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { Skill } from '../../models/Skill'
import { SkillCache } from '../../cache/SkillCache'
import { L2EnchantSkillLearn } from '../../models/L2EnchantSkillLearn'
import { DataManager } from '../../../data/manager'
import { EnchantSkillHolder } from '../../models/L2EnchantSkillGroup'
import { ConfigManager } from '../../../config/ConfigManager'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { SkillItemValues } from '../../values/SkillItemValues'
import { UserInfo } from '../send/UserInfo'
import { ExBrExtraUserInfo } from '../send/ExBrExtraUserInfo'
import { ExEnchantSkillInfo } from '../send/ExEnchantSkillInfo'
import { ExEnchantSkillResult } from '../send/ExEnchantSkillResult'
import { EnchantType, ExEnchantSkillInfoDetail } from '../send/ExEnchantSkillInfoDetail'
import { ItemTypes } from '../../values/InventoryValues'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { PlayerShortcutCache } from '../../cache/PlayerShortcutCache'
import _ from 'lodash'

// TODO add violations for incorrect data/use cases
export async function RequestExEnchantSkill( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( player.getLevel() < 76 ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_USE_SKILL_ENCHANT_ON_THIS_LEVEL ) )
        return
    }

    if ( !player.isAllowedToEnchantSkills() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_USE_SKILL_ENCHANT_ATTACKING_TRANSFORMED_BOAT ) )
        return
    }

    if ( ClassId.getClassIdByIdentifier( player.getClassId() ).level < 3 ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_USE_SKILL_ENCHANT_IN_THIS_CLASS ) )
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let skillId = packet.readD(), skillLevel = packet.readD()

    let skill: Skill = SkillCache.getSkill( skillId, skillLevel )
    if ( !skill ) {
        return
    }

    let skillLearn: L2EnchantSkillLearn = DataManager.getSkillData().getSkillEnchantmentBySkillId( skillId )
    if ( !skillLearn ) {
        return
    }

    let skillHolder: EnchantSkillHolder = skillLearn.getEnchantSkillHolder( skillLevel )
    let beforeEnchantSkillLevel: number = player.getSkillLevel( skillId )
    if ( beforeEnchantSkillLevel !== skillLearn.getMinSkillLevel( skillLevel ) ) {
        return
    }

    let costMultiplier = ConfigManager.general.getNormalEnchantCostMultipiler()
    let requiredSp = skillHolder.getSpCost() * costMultiplier
    if ( player.getSp() < requiredSp ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_DONT_HAVE_ENOUGH_SP_TO_ENCHANT_THAT_SKILL ) )
        return
    }

    let usesBook: boolean = ( skillLevel % 100 ) === 1
    let reqItemId = SkillItemValues.NormalEnchantBook
    let spb: L2ItemInstance = player.getInventory().getItemByItemId( reqItemId )

    if ( ConfigManager.character.enchantSkillSpBookNeeded() && usesBook && !spb ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_DONT_HAVE_ALL_OF_THE_ITEMS_NEEDED_TO_ENCHANT_THAT_SKILL ) )
        return
    }

    let requiredAdena = ( skillHolder.getAdenaCost() * costMultiplier )
    if ( player.getInventory().getAdenaAmount() < requiredAdena ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_DONT_HAVE_ALL_OF_THE_ITEMS_NEEDED_TO_ENCHANT_THAT_SKILL ) )
        return
    }

    let check: boolean = player.removeSp( requiredSp )
    if ( ConfigManager.character.enchantSkillSpBookNeeded() && usesBook ) {
        check = check && await player.destroyItemByObjectId( spb.getObjectId(), 1, true, 'RequestExEnchantSkill' )
    }

    check = check && await player.destroyItemByItemId( ItemTypes.Adena, requiredAdena, true, 'RequestExEnchantSkill' )
    if ( !check ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_DONT_HAVE_ALL_OF_THE_ITEMS_NEEDED_TO_ENCHANT_THAT_SKILL ) )
        return
    }

    let rate = skillHolder.getRate( player )
    if ( _.random( 100 ) <= rate ) {
        await player.addSkill( skill, true )
        player.sendOwnedData( ExEnchantSkillResult( true ) )

        let packet = new SystemMessageBuilder( SystemMessageIds.YOU_HAVE_SUCCEEDED_IN_ENCHANTING_THE_SKILL_S1 )
                .addSkillNameById( skillId )
                .getBuffer()
        player.sendOwnedData( packet )
    } else {
        await player.addSkill( SkillCache.getSkill( skillId, skillLearn.getBaseLevel() ), true )
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_HAVE_FAILED_TO_ENCHANT_THE_SKILL_S1 ) )
        player.sendOwnedData( ExEnchantSkillResult( false ) )
    }

    player.sendDebouncedPacket( UserInfo )
    player.sendDebouncedPacket( ExBrExtraUserInfo )
    player.sendSkillList()

    let afterEnchantSkillLevel = player.getSkillLevel( skillId )
    player.sendOwnedData( ExEnchantSkillInfo( skillId, afterEnchantSkillLevel ) )
    player.sendOwnedData( ExEnchantSkillInfoDetail( EnchantType.Normal, skillId, afterEnchantSkillLevel + 1, player ) )
    return PlayerShortcutCache.updateSkillShortcut( player, skillId, afterEnchantSkillLevel )
}