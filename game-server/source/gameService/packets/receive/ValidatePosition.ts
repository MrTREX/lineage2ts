import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { GetOnVehicle } from '../send/GetOnVehicle'
import { ValidateLocation } from '../send/ValidateLocation'
import { ActionFailed } from '../send/ActionFailed'
import { ReadableClientPacketPool } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'
import { GeoPolygonCache } from '../../cache/GeoPolygonCache'
import { L2Party } from '../../models/L2Party'
import { AreaType } from '../../models/areas/AreaType'
import { WorldFlyingLimits } from '../../enums/WorldFlyingLimits'

const packetLimiter = new FastRateLimit( {
    threshold: 2, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

// TODO: consider re-sync scenarios and what to do about de-sync in movement
// you can do a client style teleport to location that client wants based on distance and if it is one-off request (if it is twice or more, create violation)
// stop character to wait till client would catch up, again based on distance
// freeze character for X amount of seconds (preferred), based on how far are client vs server positions
// in case of Z mismatch, same as above options with distinction to detect Z layers in geodata (aka you cannot fall through layers)
export async function ValidatePosition( client: GameClient, packetData: Buffer ): Promise<void> {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return player.sendOwnedData( ActionFailed() )
    }

    let currentX = player.getX()

    let packet = ReadableClientPacketPool.getValue().setBuffer( packetData )
    let x = packet.readD(),
        y = packet.readD(),
        z = packet.readD(),
        heading = packet.readD(),
        vehicleObjectId = packet.readD()
    ReadableClientPacketPool.recycleValue( packet )

    if ( x === 0 && y === 0 && currentX !== 0 ) {
        return
    }

    if ( player.isInBoat() ) {
        let distance = Math.hypot( x - player.getInVehiclePosition().getX(), y - player.getInVehiclePosition().getY() )

        if ( distance > 500 && vehicleObjectId ) {
            player.sendOwnedData( GetOnVehicle( player.getObjectId(), vehicleObjectId, player.getInVehiclePosition() ) )
        }

        return
    }

    /*
        Fixes client falling through textures
     */
    if ( GeoPolygonCache.isInvalidZ( z ) || GeoPolygonCache.isInvalidZ( player.getZ() ) ) {
        player.setZ( GeoPolygonCache.getObjectZ( player ) )
        return player.sendOwnedData( ValidateLocation( player ) )
    }

    if ( z < player.getZ() && Math.abs( player.getZ() - z ) > 800 ) {
        return player.sendOwnedData( ValidateLocation( player ) )
    }

    if ( player.isInAirShip() ) {
        return
    }

    /*
        No validation during fall to avoid "jumping"
     */
    if ( player.isFalling( z ) ) {
        return
    }


    let distance: number = Math.hypot( x - currentX, y - player.getY() )

    let party : L2Party = player.getParty()
    if ( party ) {
        party.debounceUpdateMemberPositions()
    }

    /*
        Don't allow flying transformations outside gracia area
     */
    if ( player.isFlyingMounted() && x > WorldFlyingLimits.GraciaMaxX ) {
        await player.untransform()
    }

    if ( player.isFlying() || player.isInArea( AreaType.Water ) ) {
        /*
            TODO: determine if client z is NOT inside any geo layers
            Only time we have to use client Z if it is above any known geo polygons. Since we can have geo polygon
            we can determine if client z is above or within multi layer geo polygon.
            Example: player can be flying and wants to fly below surface
            Assume that you never want to accept Z value from client.
         */
        // TODO : player.setXYZ( x,y,z ) , instead send coordinates to AIController since blindly setting supplied coordinates is exploit
        if ( distance > 300 ) {
            player.sendOwnedData( ValidateLocation( player ) )
        }
    } else if ( distance < 600 ) {
        let differenceZ = Math.abs( z - player.getZ() )
        if ( distance > 500 || differenceZ > 200 ) {

            if ( differenceZ > 200 && differenceZ < 1500 && Math.abs( z - player.clientZ ) < 800 ) {
                player.setZ( GeoPolygonCache.getObjectZ( player ) )
            } else {
                player.sendOwnedData( ValidateLocation( player ) )
            }
        }
    }

    player.clientX = x
    player.clientY = y
    player.clientZ = z
    player.clientHeading = heading
    player.clientUpdateTime = Date.now()
}