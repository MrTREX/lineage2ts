import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { TradeList } from '../../models/TradeList'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { TradeItem } from '../../models/TradeItem'
import { TradeOwnAdd } from '../send/TradeOwnAdd'
import { TradeOtherAdd } from '../send/TradeOtherAdd'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'
import { PlayerPermission } from '../../enums/PlayerPermission'

const packetLimiter = new FastRateLimit( {
    threshold: 2, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function AddTradeItem( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let trade : TradeList = player.getActiveTradeList()
    if ( !trade ) {
        return
    }

    if ( !player.getAccessLevel().hasPermission( PlayerPermission.AllowTransaction ) ) {
        player.sendMessage( 'Transactions are disabled for your Access Level.' )
        player.cancelActiveTrade()
        return
    }

    let partner : L2PcInstance = trade.getPartner()
    if ( !partner || !partner.getActiveTradeList() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_IS_NOT_FOUND_IN_THE_GAME ) )
        player.cancelActiveTrade()
        return
    }

    if ( trade.isConfirmed() || partner.getActiveTradeList().isConfirmed() ) {
        return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_ADJUST_ITEMS_AFTER_TRADE_CONFIRMED ) )
    }

    let packet = new ReadableClientPacket( packetData )
    packet.skipD( 1 )
    let objectId = packet.readD()

    if ( !player.validateItemManipulation( objectId ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOTHING_HAPPENED ) )
        return
    }

    let count = packet.readQ()
    let item : TradeItem = trade.addItem( objectId, count )
    if ( item ) {
        player.sendOwnedData( TradeOwnAdd( item ) )
        partner.sendOwnedData( TradeOtherAdd( item ) )
    }
}