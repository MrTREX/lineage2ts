import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { CastleManager } from '../../instancemanager/CastleManager'
import { Castle } from '../../models/entity/Castle'
import { SiegeDefenderList } from '../send/SiegeDefenderList'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestSiegeDefenderList( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let castleId : number = new ReadableClientPacket( packetData ).readD()

    let castle : Castle = CastleManager.getCastleById( castleId )
    if ( !castle ) {
        return
    }

    player.sendOwnedData( SiegeDefenderList( castle ) )
}