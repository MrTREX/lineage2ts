import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Clan } from '../../models/L2Clan'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { ConfigManager } from '../../../config/ConfigManager'
import { PacketVariables } from '../PacketVariables'
import { L2ClanValues } from '../../values/L2ClanValues'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function AllyLeave( client: GameClient ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let clan : L2Clan = player.getClan()
    if ( !clan ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ARE_NOT_A_CLAN_MEMBER ) )
        return
    }

    if ( !player.isClanLeader() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ONLY_CLAN_LEADER_WITHDRAW_ALLY ) )
        return
    }

    if ( clan.getAllyId() === 0 ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NO_CURRENT_ALLIANCES ) )
        return
    }

    if ( clan.getId() === clan.getAllyId() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ALLIANCE_LEADER_CANT_WITHDRAW ) )
        return
    }

    clan.setAllyId( 0 )
    clan.setAllyName( null )

    await clan.changeAllyCrest( 0, true )

    clan.setAllyPenaltyExpiryTime( Date.now() + PacketVariables.millisInDay * ConfigManager.character.getDaysBeforeJoiningAllianceAfterLeaving(), L2ClanValues.PENALTY_TYPE_CLAN_LEFT )

    clan.scheduleUpdate()
    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_HAVE_WITHDRAWN_FROM_ALLIANCE ) )
}