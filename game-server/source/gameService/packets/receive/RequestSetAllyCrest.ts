import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { L2Clan } from '../../models/L2Clan'
import { ClanCache } from '../../cache/ClanCache'
import { CrestType, L2Crest } from '../../models/L2Crest'
import { CrestCache } from '../../cache/CrestCache'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 2, // time-to-live value of token bucket (in seconds)
} )

export async function RequestSetAllyCrest( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player : L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( player.getAllyId() === 0 ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.FEATURE_ONLY_FOR_ALLIANCE_LEADER ) )
        return
    }

    let leaderClan : L2Clan = ClanCache.getClan( player.getAllyId() )

    if ( player.getClanId() !== leaderClan.getId() || !player.isClanLeader() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.FEATURE_ONLY_FOR_ALLIANCE_LEADER ) )
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let dataSize : number = packet.readD()

    if ( dataSize < 0 ) {
        player.sendMessage( 'Incorrect data format' )
        return
    }

    if ( dataSize > 192 ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ADJUST_IMAGE_8_12 ) )
        return
    }

    if ( dataSize === 0 ) {
        if ( leaderClan.getAllyCrestId() !== 0 ) {
            await leaderClan.changeAllyCrest( 0, false )
        }

        return
    }

    let data : Buffer = packet.readB( dataSize )

    let crest : L2Crest = await CrestCache.createCrest( data, CrestType.ALLY )
    if ( crest ) {
        await leaderClan.changeAllyCrest( crest.getId(), false )
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLAN_CREST_WAS_SUCCESSFULLY_REGISTRED ) )
    }
}