import { GameClient } from '../../GameClient'
import { ExBrLoadEventTopRankers } from '../send/ExBrLoadEventTopRankers'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function HalloweenEventRankerList( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let eventId = packet.readD(), day = packet.readD()

    client.sendPacket( ExBrLoadEventTopRankers( eventId, day, 0, 0, 0 ) )
}