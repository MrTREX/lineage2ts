import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { Castle } from '../../models/entity/Castle'
import { CastleManager } from '../../instancemanager/CastleManager'
import { SiegableHall } from '../../models/entity/clanhall/SiegableHall'
import { ClanHallSiegeManager } from '../../instancemanager/ClanHallSiegeManager'
import { SiegeAttackersForCastle, SiegeAttackersForHall } from '../send/SiegeAttackers'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestSiegeAttackerList( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let castleId: number = new ReadableClientPacket( packetData ).readD()

    let castle: Castle = CastleManager.getCastleById( castleId )
    if ( castle ) {
        player.sendOwnedData( SiegeAttackersForCastle( castle ) )
        return
    }

    let hall: SiegableHall = ClanHallSiegeManager.getSiegableHall( castleId )
    if ( hall ) {
        player.sendOwnedData( SiegeAttackersForHall( hall ) )
        return
    }
}