import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ExCursedWeaponLocation } from '../send/ExCursedWeaponLocation'
import { FastRateLimit as RateLimiter } from 'fast-ratelimit'

const packetLimiter = new RateLimiter( {
    threshold: 1, // available tokens over timespan
    ttl: 3, // time-to-live value of token bucket (in seconds)
} )

export function RequestCursedWeaponLocation( client: GameClient ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    player.sendOwnedData( ExCursedWeaponLocation() )
}