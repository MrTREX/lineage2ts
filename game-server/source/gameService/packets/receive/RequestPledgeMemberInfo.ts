import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Clan } from '../../models/L2Clan'
import { L2ClanMember } from '../../models/L2ClanMember'
import { PledgeReceiveMemberInfo } from '../send/PledgeReceiveMemberInfo'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 2, // time-to-live value of token bucket (in seconds)
} )

export function RequestPledgeMemberInfo( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let clan : L2Clan = player.getClan()
    if ( !clan ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    packet.skipD( 1 )
    let name : string = packet.readS()

    let member : L2ClanMember = clan.getClanMemberByName( name )
    if ( !member ) {
        return
    }

    player.sendOwnedData( PledgeReceiveMemberInfo( member ) )
}