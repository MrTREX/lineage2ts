import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2World } from '../../L2World'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestExRqItemLink( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let objectId : number = new ReadableClientPacket( packetData ).readD()
    let item: L2ItemInstance = L2World.getObjectById( objectId ) as L2ItemInstance
    if ( !item.isItem() ) {
        return
    }

    if ( item.isPublished() ) {
        player.sendCopyData( item.getPublishedData() )
    }
}