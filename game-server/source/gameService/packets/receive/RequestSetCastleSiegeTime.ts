import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { CastleManager } from '../../instancemanager/CastleManager'
import { Castle } from '../../models/entity/Castle'
import { SiegeInfoForCastle } from '../send/SiegeInfo'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestSetCastleSiegeTime( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player : L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let castleId = packet.readD(), time = packet.readD()

    time = time * 1000

    let castle : Castle = CastleManager.getCastleById( castleId )
    if ( !castle ) {
        return
    }

    if ( ( castle.getOwnerId() > 0 ) && ( castle.getOwnerId() !== player.getClanId() ) ) {
        return
    }

    if ( !player.isClanLeader() ) {
        return
    }

    if ( !castle.getIsTimeRegistrationOver() ) {
        if ( isSiegeTimeValid( castle.getSiegeDate(), time ) ) {
            castle.setSiegeDate( time )
            castle.setIsTimeRegistrationOver( true )
            await castle.getSiege().saveSiegeDate()

            let packet : Buffer = new SystemMessageBuilder( SystemMessageIds.S1_ANNOUNCED_SIEGE_TIME )
                    .addCastleId( castleId )
                    .getBuffer()
            BroadcastHelper.toAllOnlinePlayersData( packet )

            player.sendOwnedData( SiegeInfoForCastle( castle, player ) )
        }
    }
}

function isSiegeTimeValid( existingTime: number, futureTime: number ) : boolean {
    // TODO : implement time comparison logic
    return true
}