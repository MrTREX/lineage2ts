import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Clan } from '../../models/L2Clan'
import { ConfigManager } from '../../../config/ConfigManager'
import { ActionFailed } from '../send/ActionFailed'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { ClanCache } from '../../cache/ClanCache'
import { L2World } from '../../L2World'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'
import { ClanPrivilege } from '../../enums/ClanPriviledge'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 2, // time-to-live value of token bucket (in seconds)
} )

function broadcastInfo( memberId: number ) : void {
    let member = L2World.getPlayer( memberId )
    if ( member ) {
        member.broadcastUserInfo()
    }
}

export async function RequestStartPledgeWar( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player : L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let clan : L2Clan = player.getClan()
    if ( !clan ) {
        return
    }

    if ( ( clan.getLevel() < 3 ) || ( clan.getMembersCount() < ConfigManager.character.getClanMembersForWar() ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLAN_WAR_DECLARED_IF_CLAN_LVL3_OR_15_MEMBER ) )
        player.sendOwnedData( ActionFailed() )
        return
    }

    if ( !player.hasClanPrivilege( ClanPrivilege.PledgeWar ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ARE_NOT_AUTHORIZED_TO_DO_THAT ) )
        player.sendOwnedData( ActionFailed() )
        return
    }

    let clanName : string = new ReadableClientPacket( packetData ).readS()

    let otherClan : L2Clan = ClanCache.getClanByName( clanName )
    if ( !otherClan ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLAN_WAR_CANNOT_DECLARED_CLAN_NOT_EXIST ) )
        player.sendOwnedData( ActionFailed() )
        return
    }

    if ( clan.getAllyId() === otherClan.getAllyId() && clan.getAllyId() !== 0 ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLAN_WAR_AGAINST_A_ALLIED_CLAN_NOT_WORK ) )
        player.sendOwnedData( ActionFailed() )
        return
    }

    if ( ( otherClan.getLevel() < 3 ) || ( otherClan.getMembersCount() < ConfigManager.character.getClanMembersForWar() ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLAN_WAR_DECLARED_IF_CLAN_LVL3_OR_15_MEMBER ) )
        player.sendOwnedData( ActionFailed() )
        return
    }

    if ( clan.isAtWarWith( otherClan.getId() ) ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.ALREADY_AT_WAR_WITH_S1_WAIT_5_DAYS )
                .addString( otherClan.getName() )
                .getBuffer()
        player.sendOwnedData( packet )
        player.sendOwnedData( ActionFailed() )
        return
    }

    await ClanCache.storeClanWars( player.getClanId(), clan.getId() )

    clan.getOnlineMembers().forEach( broadcastInfo )
    otherClan.getOnlineMembers().forEach( broadcastInfo )
}