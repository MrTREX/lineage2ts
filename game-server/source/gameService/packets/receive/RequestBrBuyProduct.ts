import { GameClient } from '../../GameClient'
import { FastRateLimit } from 'fast-ratelimit'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ReadableClientPacketPool } from '../../../packets/ReadableClientPacket'
import { ExBrBuyProductResponse, ExBrBuyProductResult } from '../send/ExBrBuyProductResponse'
import { DataManager } from '../../../data/manager'
import { ExBrGamePoint } from '../send/ExBrGamePoint'
import { ListenerCache } from '../../cache/ListenerCache'
import { EventType, PlayerBuyGPProduct } from '../../models/events/EventType'
import { EventPoolCache } from '../../cache/EventPoolCache'
import { GamePointsCache, GamePointType } from '../../cache/GamePointsCache'
import { ConfigManager } from '../../../config/ConfigManager'
import { ActionFailed } from '../send/ActionFailed'
import { L2GamePointProduct, L2GamePointProductItem } from '../../../data/interface/GamePointsProductsDataApi'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { recordReceivedPacketViolation } from '../../helpers/PlayerViolations'

const packetLimiter = new FastRateLimit( {
    threshold: 3, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

/*
    TODO : add configuration for point products, otherwise called "Item Shop" in UI
    - preferred receive method: inventory, dimensional merchant or mail
    - item id of inventory item used for points, otherwise use new column on character DB table
 */
export async function RequestBrBuyProduct( client: GameClient, packetData: Buffer ) : Promise<void> {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( !ConfigManager.character.isGamePointsEnabled() || !ConfigManager.character.useItemShopPoints() ) {
        return player.sendOwnedData( ActionFailed() )
    }

    let packet = ReadableClientPacketPool.getValue().setBuffer( packetData )
    let productId = packet.readD()
    let itemAmount = packet.readD()

    ReadableClientPacketPool.recycleValue( packet )

    if ( itemAmount > 99 || itemAmount < 1 ) {
        return recordReceivedPacketViolation( player.getObjectId(), '', 'Player attempts to invalid premium shop product itemAmount', RequestBrBuyProduct.name, itemAmount )
    }

    let product : L2GamePointProduct = DataManager.getGamePointProducts().getProduct( productId )

    if ( !product ) {
        await recordReceivedPacketViolation( player.getObjectId(), '', 'Player attempts to buy non-existent premium shop product', RequestBrBuyProduct.name, productId )
        return player.sendOwnedData( ExBrBuyProductResponse( ExBrBuyProductResult.WrongProduct ) )
    }

    let points = GamePointsCache.getPoints( player.getAccountName(), GamePointType.ItemShop )
    if ( points === 0 || product.price > points ) {
        return player.sendOwnedData( ExBrBuyProductResponse( ExBrBuyProductResult.NotEnoughPoints ) )
    }

    // TODO : check if product itemAmount is available

    let slots : number = 0
    let weight : number = 0

    for ( const productItem of product.items ) {
        let item = DataManager.getItems().getTemplate( productItem.id )
        if ( !item ) {
            return player.sendOwnedData( ExBrBuyProductResponse( ExBrBuyProductResult.WrongProduct ) )
        }

        weight += item.getWeight()
        if ( item.isStackable() ) {
            slots += player.getInventory().hasItemWithId( productItem.id ) ? 0 : 1
            continue
        }

        slots += ( itemAmount * productItem.amount )
    }

    if ( !player.getInventory().validateCapacity( slots ) || !player.getInventory().validateWeight( weight ) ) {
        return player.sendOwnedData( ExBrBuyProductResponse( ExBrBuyProductResult.InventoryFull ) )
    }

    let totalPrice = product.price * itemAmount
    GamePointsCache.addPoints( player.getAccountName(), GamePointType.ItemShop, -totalPrice, RequestBrBuyProduct.name )

    const promises = product.items.map( async ( productItem: L2GamePointProductItem ) : Promise<void> => {
        let totalAmount = productItem.amount * itemAmount
        let addedItem = await player.getInventory().addItem( productItem.id, totalAmount, player.getObjectId(), RequestBrBuyProduct.name )

        if ( !addedItem ) {
            return
        }

        let message = new SystemMessageBuilder( SystemMessageIds.YOU_OBTAINED_S1_S2 )
            .addItemNameWithId( productItem.id )
            .addNumber( totalAmount )
            .getBuffer()

        player.sendOwnedData( message )
    } )

    await Promise.all( promises )

    GamePointsCache.recordPurchase( player, GamePointType.ItemShop, productId, itemAmount, totalPrice, product.category )

    if ( ListenerCache.hasGeneralListener( EventType.PlayerBuyGPProduct ) ) {
        let eventData = EventPoolCache.getData( EventType.PlayerBuyGPProduct ) as PlayerBuyGPProduct

        eventData.productId = productId
        eventData.playerId = player.getObjectId()
        eventData.amount = itemAmount

        await ListenerCache.sendGeneralEvent( EventType.PlayerBuyGPProduct, eventData )
    }

    let shopPoints = GamePointsCache.getPoints( player.getAccountName(), GamePointType.ItemShop )
    player.sendOwnedData( ExBrGamePoint( player.getObjectId(), shopPoints ) )
    player.sendOwnedData( ExBrBuyProductResponse( ExBrBuyProductResult.Success ) )
}