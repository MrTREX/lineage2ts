import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Clan } from '../../models/L2Clan'
import { L2ClanMember } from '../../models/L2ClanMember'
import { PledgeReceivePowerInfo } from '../send/PledgeReceivePowerInfo'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'

export function RequestPledgeMemberPowerInfo( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let clan : L2Clan = player.getClan()
    if ( !clan ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    packet.skipD( 1 )
    let name : string = packet.readS()

    let member : L2ClanMember = clan.getClanMemberByName( name )
    if ( !member ) {
        return
    }

    player.sendOwnedData( PledgeReceivePowerInfo( member ) )
}