import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { PartyMatchWaitingList } from '../../cache/PartyMatchWaitingList'

export function RequestExitPartyMatchingWaitingRoom( client: GameClient ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    PartyMatchWaitingList.removePlayer( player )
}