import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Clan } from '../../models/L2Clan'
import { L2ClanMember } from '../../models/L2ClanMember'
import { L2ClanValues } from '../../values/L2ClanValues'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { ClanPrivilege } from '../../enums/ClanPriviledge'

export async function RequestPledgeSetMemberPowerGrade( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let clan : L2Clan = player.getClan()
    if ( !clan ) {
        return
    }

    if ( !player.hasClanPrivilege( ClanPrivilege.ManageRanks ) ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let name = packet.readS(), grade = packet.readD()

    let member : L2ClanMember = clan.getClanMemberByName( name )
    if ( !member ) {
        return
    }

    if ( member.getObjectId() === clan.getLeaderId() ) {
        return
    }

    if ( member.getPledgeType() === L2ClanValues.SUBUNIT_ACADEMY ) {
        player.sendMessage( 'You cannot change academy member grade' )
        return
    }

    await member.setPowerGrade( grade )
    clan.broadcastClanStatus()
}