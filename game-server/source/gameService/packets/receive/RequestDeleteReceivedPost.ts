import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ConfigManager } from '../../../config/ConfigManager'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { PacketVariables } from '../PacketVariables'
import { MailMessage } from '../../models/mail/MailMessage'
import { MailManager } from '../../instancemanager/MailManager'
import { ExChangePostState } from '../send/ExChangePostState'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'
import { AreaType } from '../../models/areas/AreaType'
import { ChangePostState } from '../../enums/ChangePostState'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestDeleteReceivedPost( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player || !ConfigManager.general.allowMail() ) {
        return
    }

    if ( !player.isInArea( AreaType.Peace ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_USE_MAIL_OUTSIDE_PEACE_ZONE ) )
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let count = packet.readD()

    if ( count < 1 || count > PacketVariables.maximumItemsLimit ) {
        return
    }

    let messageIds : Array<number> = []
    while ( count > 0 ) {
        let messageId = packet.readD()
        let message: MailMessage = MailManager.getMessage( messageId )
        count--

        if ( !message ) {
            continue
        }

        if ( message.getReceiverId() !== player.getObjectId() ) {
            // TODO : Util.handleIllegalPlayerAction(player, "Player " + player.getName() + " tried to delete not own post!");
            continue
        }

        if ( message.hasAttachments() ) {
            continue
        }

        MailManager.setDeleted( messageId )
        messageIds.push( messageId )
    }

    if ( messageIds.length === 0 ) {
        return
    }

    player.sendOwnedData( ExChangePostState( true, messageIds, ChangePostState.Deleted ) )
}