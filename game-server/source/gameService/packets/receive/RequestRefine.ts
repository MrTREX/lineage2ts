import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { LifeStone, LifeStoneCache } from '../../cache/LifeStoneCache'
import { L2Augmentation } from '../../models/L2Augmentation'
import { AugmentationCache } from '../../cache/AugmentationCache'
import { ExVariationResult, ExVariationResultType } from '../send/ExVariationResult'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { InventoryUpdateStatus } from '../../enums/InventoryUpdateStatus'
import { canAugmentItem } from '../../helpers/ItemHelper'
import { FastRateLimit } from 'fast-ratelimit'
import { ListenerCache } from '../../cache/ListenerCache'
import { EventType, PlayerAugmentItemEvent } from '../../models/events/EventType'
import { EventPoolCache } from '../../cache/EventPoolCache'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestRefine( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player : L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let targetId = packet.readD(), refinerId = packet.readD(), gemstoneId = packet.readD(), count = packet.readQ()

    if ( count < 1 ) {
        return
    }

    let targetItem : L2ItemInstance = player.getInventory().getItemByObjectId( targetId )
    if ( !targetItem ) {
        return
    }

    let refinerItem : L2ItemInstance = player.getInventory().getItemByObjectId( refinerId )
    if ( !refinerItem ) {
        return
    }
    let gemStoneItem : L2ItemInstance = player.getInventory().getItemByObjectId( gemstoneId )
    if ( !gemStoneItem ) {
        return
    }

    if ( !canAugmentItem( player, targetItem, refinerItem, gemStoneItem ) ) {
        player.sendOwnedData( ExVariationResult( 0, ExVariationResultType.Failure ) )
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.AUGMENTATION_FAILED_DUE_TO_INAPPROPRIATE_CONDITIONS ) )
        return
    }

    let lifeStone : LifeStone = LifeStoneCache.getLifeStone( refinerItem.getId() )
    if ( !lifeStone ) {
        return
    }

    if ( count !== LifeStoneCache.getGemStoneCount( targetItem.getItem().getItemGrade(), lifeStone ) ) {
        player.sendOwnedData( ExVariationResult( 0, ExVariationResultType.Failure ) )
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.AUGMENTATION_FAILED_DUE_TO_INAPPROPRIATE_CONDITIONS ) )
        return
    }

    if ( targetItem.isEquipped() ) {
        await player.getInventory().unEquipItemInSlot( targetItem.getLocationSlot() )
        player.broadcastUserInfo()
    }

    if ( !( await player.destroyItemWithCount( refinerItem, 1, false, 'RequestRefine' ) ) ) {
        return
    }

    if ( !( await player.destroyItemWithCount( gemStoneItem, count, false, 'RequestRefine' ) ) ) {
        return
    }

    let augmentation : L2Augmentation = AugmentationCache.generateRandomAugmentation( lifeStone.level, lifeStone.grade, targetItem.getItem().getBodyPart(), refinerItem.getId(), targetItem )
    targetItem.setAugmentation( augmentation )

    player.getInventory().markItem( targetItem, InventoryUpdateStatus.Modified )
    player.sendOwnedData( ExVariationResult( augmentation.getAugmentationId(), ExVariationResultType.Success ) )

    if ( ListenerCache.hasItemTemplateEvent( targetItem.getId(), EventType.PlayerAugmentItem ) ) {
        let eventData = EventPoolCache.getData( EventType.PlayerAugmentItem ) as PlayerAugmentItemEvent

        eventData.objectId = targetItem.getObjectId()
        eventData.playerId = player.getObjectId()
        eventData.isAugmented = true
        eventData.itemId = targetItem.getId()

        return ListenerCache.sendItemTemplateEvent( targetItem.getId(), EventType.PlayerAugmentItem, eventData )
    }
}