import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { FastRateLimit } from 'fast-ratelimit'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

// TODO : provide integration to store client data in order to provide ability to compare clients
export function ClientHardwareInfo( client: GameClient, packetData: Buffer ) : void {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let macAddress = packet.readS()
    let windowsPlatformId = packet.readD()
    let windowsMajorVersion = packet.readD()
    let windowsMinorVersion = packet.readD()
    let windowsBuildNumber = packet.readD()
    let directXVersion = packet.readD()
    let directXRevision = packet.readD()

    packet.skipB( 16 )

    let cpuName = packet.readS()
    let cpuSpeed = packet.readD()
    let cpuCoreAmount = packet.readC()

    packet.skipD( 1 )

    let vgaAmount = packet.readD()
    let vgaPCESpeed = packet.readD()
    let physMemorySlot1 = packet.readD()
    let physMemorySlot2 = packet.readD()
    let physMemorySlot3 = packet.readD()

    packet.skipB( 1 )

    let videoMemory = packet.readD()

    packet.skipD( 1 )

    let vgaVersion = packet.readH()
    let vgaName = packet.readS()
    let vgaDriverVersion = packet.readS()
}