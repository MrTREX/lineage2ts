import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { PartyMatchManager } from '../../cache/partyMatchManager'
import { PartyMatchWaitingList } from '../../cache/PartyMatchWaitingList'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 2, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestPartyMatchDetail( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let roomId: number = new ReadableClientPacket( packetData ).readD()

    if ( !PartyMatchManager.addToExistingRoomById( roomId, player ) ) {
        return
    }

    PartyMatchWaitingList.removePlayer( player )
}