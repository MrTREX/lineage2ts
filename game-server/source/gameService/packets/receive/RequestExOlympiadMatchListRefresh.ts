import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ExOlympiadMatchList } from '../send/ExOlympiadMatchList'

export function RequestExOlympiadMatchListRefresh( client: GameClient ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    player.sendOwnedData( ExOlympiadMatchList() )
}