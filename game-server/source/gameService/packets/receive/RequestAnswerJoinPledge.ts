import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { RequestJoinPledge } from './RequestJoinPledge'
import { L2RequestData } from '../../models/L2Request'
import { L2Clan } from '../../models/L2Clan'
import { CastleManager } from '../../instancemanager/CastleManager'
import { FortManager } from '../../instancemanager/FortManager'
import { PledgeShowInfoUpdate } from '../send/PledgeShowInfoUpdate'
import { PledgeShowMemberListAdd } from '../send/PledgeShowMemberListAdd'
import { PledgeShowMemberListAll } from '../send/PledgeShowMemberListAll'
import { JoinPledge } from '../send/JoinPledge'
import { L2ClanValues } from '../../values/L2ClanValues'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestAnswerJoinPledge( client: GameClient, packetData: Buffer ) : Promise<void> {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let requester = player.getRequest().getPartner()
    if ( !requester || !requester.getRequest().getRequestData() ) {
        return
    }

    let answer : number = new ReadableClientPacket( packetData ).readD()

    if ( answer === 0 ) {
        let playerResponse = new SystemMessageBuilder( SystemMessageIds.YOU_DID_NOT_RESPOND_TO_S1_CLAN_INVITATION )
                .addString( requester.getName() )
                .getBuffer()
        player.sendOwnedData( playerResponse )

        let requesterResponse = new SystemMessageBuilder( SystemMessageIds.S1_DID_NOT_RESPOND_TO_CLAN_INVITATION )
                .addString( player.getName() )
                .getBuffer()
        requester.sendOwnedData( requesterResponse )

        return player.getRequest().onRequestResponse()
    }

    let requestData : L2RequestData = requester.getRequest().getRequestData()
    if ( requestData.type !== RequestJoinPledge ) {
        return
    }

    let clan : L2Clan = requester.getClan()
    let { pledgeType } = requestData.parameters

    if ( clan.checkClanJoinCondition( requester, player, pledgeType ) ) {
        player.sendOwnedData( JoinPledge( requester.getClanId() ) )
        player.setPledgeType( pledgeType )

        if ( pledgeType === L2ClanValues.SUBUNIT_ACADEMY ) {
            player.setPowerGrade( 9 ) // Academy
            player.setLevelJoinedAcademy( player.getLevel() )
        } else {
            player.setPowerGrade( 5 ) // new member starts at 5, not confirmed
        }

        await clan.addClanMember( player )
        player.setClanPrivileges( player.getClan().getRankPrivileges( player.getPowerGrade() ) )
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ENTERED_THE_CLAN ) )

        let clanUpdate = new SystemMessageBuilder( SystemMessageIds.S1_HAS_JOINED_CLAN )
                .addString( player.getName() )
                .getBuffer()
        clan.broadcastDataToOnlineMembers( clanUpdate )

        if ( player.getClan().getCastleId() > 0 ) {
            await CastleManager.getCastleByOwner( player.getClan() ).giveResidentialSkills( player )
        }

        if ( player.getClan().getFortId() > 0 ) {
            await FortManager.getFortByOwner( player.getClan() ).giveResidentialSkills( player )
        }

        player.sendSkillList()

        clan.broadcastDataToOtherOnlineMembers( PledgeShowMemberListAdd.fromPlayer( player ), player )
        clan.broadcastDataToOnlineMembers( PledgeShowInfoUpdate( clan ) )

        // this activates the clan tab on the new member
        player.sendOwnedData( PledgeShowMemberListAll( clan ) )
        player.setClanJoinExpiryTime( 0 )
        player.broadcastUserInfo()
    }

    player.getRequest().onRequestResponse()
}