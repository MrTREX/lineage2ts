import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Clan } from '../../models/L2Clan'
import { ClanCache } from '../../cache/ClanCache'
import { PacketVariables } from '../PacketVariables'
import { ConfigManager } from '../../../config/ConfigManager'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { L2ClanValues } from '../../values/L2ClanValues'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function AllyDismiss( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let leaderClan : L2Clan = player.getClan()
    if ( !leaderClan ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ARE_NOT_A_CLAN_MEMBER ) )
        return
    }

    if ( leaderClan.getAllyId() === 0 ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NO_CURRENT_ALLIANCES ) )
        return
    }

    if ( !player.isClanLeader() || ( leaderClan.getId() !== leaderClan.getAllyId() ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.FEATURE_ONLY_FOR_ALLIANCE_LEADER ) )
        return
    }

    let name = new ReadableClientPacket( packetData ).readS()

    let clan : L2Clan = ClanCache.getClanByName( name )
    if ( !clan ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLAN_DOESNT_EXISTS ) )
        return
    }

    if ( clan.getId() === leaderClan.getId() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ALLIANCE_LEADER_CANT_WITHDRAW ) )
        return
    }

    if ( clan.getAllyId() !== leaderClan.getAllyId() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.DIFFERENT_ALLIANCE ) )
        return
    }

    let currentTime = Date.now()
    leaderClan.setAllyPenaltyExpiryTime( currentTime + PacketVariables.millisInDay * ConfigManager.character.getDaysBeforeAcceptNewClanWhenDismissed(), L2ClanValues.PENALTY_TYPE_DISMISS_CLAN )
    leaderClan.scheduleUpdate()

    clan.setAllyId( 0 )
    clan.setAllyName( null )

    await clan.changeAllyCrest( 0, true )

    clan.setAllyPenaltyExpiryTime( currentTime + PacketVariables.millisInDay * ConfigManager.character.getDaysBeforeJoinAllyWhenDismissed(), L2ClanValues.PENALTY_TYPE_CLAN_DISMISSED )

    clan.scheduleUpdate()
    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_HAVE_EXPELED_A_CLAN ) )
}