import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export function RequestResetNickname( client: GameClient ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    player.getAppearance().setTitleColor( 0xFFFF77 )
    player.setTitle( '' )
    player.broadcastTitleInfo()
}