import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2World } from '../../L2World'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { DuelManager } from '../../instancemanager/DuelManager'
import { ExDuelAskStart } from '../send/ExDuelAskStart'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 3, // time-to-live value of token bucket (in seconds)
} )

export function RequestDuelStart( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let playerName = packet.readS(), partyDuelValue = packet.readD()

    let target: L2PcInstance = L2World.getPlayerByName( playerName )
    let isPartyDuel: boolean = partyDuelValue === 1
    if ( !target ) {
        return
    }

    if ( player.getObjectId() === target.getObjectId() ) {
        if ( isPartyDuel ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THERE_IS_NO_OPPONENT_TO_RECEIVE_YOUR_CHALLENGE_FOR_A_DUEL ) )
        }
        return
    }

    if ( !player.isInsideRadius( target, 250 ) ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.C1_CANNOT_RECEIVE_A_DUEL_CHALLENGE_BECAUSE_C1_IS_TOO_FAR_AWAY )
                .addString( target.getName() )
                .getBuffer()
        player.sendOwnedData( packet )
        return
    }

    if ( !DuelManager.canDuel( player, player, isPartyDuel ) ) {
        return
    }

    if ( !DuelManager.canDuel( player, target, isPartyDuel ) ) {
        return
    }

    if ( isPartyDuel ) {
        if ( !player.isInParty()
                || !player.getParty().isLeader( player )
                || !target.isInParty()
                || player.getParty().getMembers().includes( target.getObjectId() ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ARE_UNABLE_TO_REQUEST_A_DUEL_AT_THIS_TIME ) )
            return
        }

        let shouldExit = player.getParty().getMembers().some( ( memberId: number ) => {
            let member = L2World.getPlayer( memberId )
            return !DuelManager.canDuel( player, member, isPartyDuel )
        } )

        if ( shouldExit ) {
            return
        }

        shouldExit = target.getParty().getMembers().some( ( memberId: number ) => {
            let member = L2World.getPlayer( memberId )
            return !DuelManager.canDuel( player, member, isPartyDuel )
        } )

        if ( shouldExit ) {
            return
        }

        let partyLeader: L2PcInstance = target.getParty().getLeader()
        if ( partyLeader ) {
            if ( !partyLeader.isProcessingRequest() ) {
                player.onTransactionRequest( partyLeader )
                partyLeader.sendOwnedData( ExDuelAskStart( player.getName(), partyDuelValue ) )

                let playerMessage = new SystemMessageBuilder( SystemMessageIds.C1_PARTY_HAS_BEEN_CHALLENGED_TO_A_DUEL )
                        .addString( partyLeader.getName() )
                        .getBuffer()
                player.sendOwnedData( playerMessage )

                let targetMessage = new SystemMessageBuilder( SystemMessageIds.C1_PARTY_HAS_CHALLENGED_YOUR_PARTY_TO_A_DUEL )
                        .addString( player.getName() )
                        .getBuffer()
                target.sendOwnedData( targetMessage )
            } else {

                let packet = new SystemMessageBuilder( SystemMessageIds.C1_IS_BUSY_TRY_LATER )
                        .addString( partyLeader.getName() )
                        .getBuffer()
                player.sendOwnedData( packet )
            }
        }
        return
    }

    if ( target.isProcessingRequest() ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.C1_IS_BUSY_TRY_LATER )
                .addString( target.getName() )
                .getBuffer()
        player.sendOwnedData( packet )
        return
    }

    player.onTransactionRequest( target )
    target.sendOwnedData( ExDuelAskStart( player.getName(), partyDuelValue ) )

    let playerMessage = new SystemMessageBuilder( SystemMessageIds.C1_HAS_BEEN_CHALLENGED_TO_A_DUEL )
            .addString( target.getName() )
            .getBuffer()
    player.sendOwnedData( playerMessage )

    let targetMessage = new SystemMessageBuilder( SystemMessageIds.C1_HAS_CHALLENGED_YOU_TO_A_DUEL )
            .addString( player.getName() )
            .getBuffer()
    target.sendOwnedData( targetMessage )
}