import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Summon } from '../../models/actor/L2Summon'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { L2PetInstance } from '../../models/actor/instance/L2PetInstance'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 2, // time-to-live value of token bucket (in seconds)
} )

export async function RequestChangePetName( client: GameClient, packetData: Buffer ): Promise<void> {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    // TODO : add rate limiting since we are doing database lookup

    let pet: L2Summon = player.getSummon()
    if ( !pet ) {
        return
    }

    if ( !pet.isPet() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.DONT_HAVE_PET ) )
        return
    }

    let name: string = new ReadableClientPacket( packetData ).readS().trim()
    if ( pet.getName() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NAMING_YOU_CANNOT_SET_NAME_OF_THE_PET ) )
        return
    }

    return ( pet as L2PetInstance ).attemptNameChange( name, player )
}