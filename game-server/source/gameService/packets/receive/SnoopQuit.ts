import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2World } from '../../L2World'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function SnoopQuit( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let playerId : number = new ReadableClientPacket( packetData ).readD()

    let otherPlayer : L2PcInstance = L2World.getPlayer( playerId )
    if ( !otherPlayer ) {
        return
    }

    player.removeSnooped( otherPlayer )
    otherPlayer.removeSnooper( player )
}