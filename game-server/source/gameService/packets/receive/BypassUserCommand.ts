import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { IUserCommandHandler } from '../../handler/IUserCommandHandler'
import { UserCommandManager } from '../../handler/managers/UserCommandManager'
import { ReadableClientPacketPool } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 2, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function BypassUserCommand( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let packet = ReadableClientPacketPool.getValue().setBuffer( packetData )
    let command: number = packet.readD()
    ReadableClientPacketPool.recycleValue( packet )

    let handler: IUserCommandHandler = UserCommandManager.getHandler( command )

    if ( !handler ) {
        if ( player.isGM() ) {
            player.sendMessage( `User commandID '${ command }' not available.` )
        }

        return
    }

    await handler.onStart( player, command )
}