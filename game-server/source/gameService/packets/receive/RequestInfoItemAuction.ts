import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ItemAuctionManager } from '../../cache/ItemAuctionManager'
import { NpcItemAuction } from '../../models/auction/NpcItemAuction'
import { ItemAuction } from '../../models/auction/ItemAuction'
import { ExItemAuctionInfoPacket } from '../send/ExItemAuctionInfoPacket'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 3, // available tokens over timespan
    ttl: 2, // time-to-live value of token bucket (in seconds)
} )

export function RequestInfoItemAuction( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let npcId : number = new ReadableClientPacket( packetData ).readD()

    let npcAuction : NpcItemAuction = ItemAuctionManager.getAuction( npcId )
    if ( !npcAuction ) {
        return
    }

    let auction : ItemAuction = npcAuction.getCurrentAuction()
    if ( !auction ) {
        return
    }

    player.sendOwnedData( ExItemAuctionInfoPacket( true, auction, npcAuction.getNextAuction() ) )
}