import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { EventType, PlayerTutorialClientEvent } from '../../models/events/EventType'
import { ListenerCache } from '../../cache/ListenerCache'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { EventPoolCache } from '../../cache/EventPoolCache'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestTutorialClientEvent( client: GameClient, packetData: Buffer ) : Promise<void> {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( ListenerCache.hasGeneralListener( EventType.PlayerTutorialClient ) ) {
        let data = EventPoolCache.getData( EventType.PlayerTutorialClient ) as PlayerTutorialClientEvent

        data.playerId = player.getObjectId()
        data.eventId = new ReadableClientPacket( packetData ).readD()

        return ListenerCache.sendGeneralEvent( EventType.PlayerTutorialClient, data )
    }
}