import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { TeleportBookmarkCache } from '../../cache/TeleportBookmarkCache'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestSaveBookMarkSlot( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let name = packet.readS(), icon = packet.readD(), tag = packet.readS()

    return TeleportBookmarkCache.add( player, icon, tag, name )
}