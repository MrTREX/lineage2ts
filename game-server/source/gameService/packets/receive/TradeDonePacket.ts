import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { TradeList } from '../../models/TradeList'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { L2PcInstanceValues } from '../../values/L2PcInstanceValues'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'
import { PlayerPermission } from '../../enums/PlayerPermission'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 2, // time-to-live value of token bucket (in seconds)
} )

export async function TradeDonePacket( client: GameClient, packetData: Buffer ) : Promise<void> {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        player.sendMessage( 'You are trading too fast.' )
        return
    }

    if ( !player.getAccessLevel().hasPermission( PlayerPermission.AllowTransaction ) ) {
        player.cancelActiveTrade()
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ARE_NOT_AUTHORIZED_TO_DO_THAT ) )
        return
    }

    let tradeList : TradeList = player.getActiveTradeList()
    if ( !tradeList ) {
        return
    }

    if ( tradeList.isLocked() ) {
        return
    }

    let response : number = new ReadableClientPacket( packetData ).readD()

    if ( response === 1 ) {
        if ( !tradeList.getPartner() ) {
            player.cancelActiveTrade()
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_IS_NOT_FOUND_IN_THE_GAME ) )
            return
        }

        if ( ( tradeList.getOwner().getActiveEnchantItemId() !== L2PcInstanceValues.EmptyId )
                || ( tradeList.getPartner().getActiveEnchantItemId() !== L2PcInstanceValues.EmptyId ) ) {
            return
        }

        if ( player.getInstanceId() !== tradeList.getPartner().getInstanceId() && player.getInstanceId() !== 0 ) {
            player.cancelActiveTrade()
            return
        }

        if ( player.calculateDistance( tradeList.getPartner(), true ) > 150 ) {
            player.cancelActiveTrade()
            return
        }

        await tradeList.confirm()
        return
    }

    player.cancelActiveTrade()
}