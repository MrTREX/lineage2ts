import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Object } from '../../models/L2Object'
import { L2Summon } from '../../models/actor/L2Summon'
import { L2PetInstance } from '../../models/actor/instance/L2PetInstance'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { Skill } from '../../models/Skill'
import { ActionFailed } from '../send/ActionFailed'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { AbnormalType } from '../../models/skills/AbnormalType'
import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { ExBasicActionListValues } from '../send/ExBasicActionList'
import { PrivateStoreType } from '../../enums/PrivateStoreType'
import { InstanceType } from '../../enums/InstanceType'
import { ConfigManager } from '../../../config/ConfigManager'
import { BotReportCache } from '../../cache/BotReportCache'
import { NpcStringIds } from '../NpcStringIds'
import { SkillCache } from '../../cache/SkillCache'
import { ExAskCoupleAction } from '../send/ExAskCoupleAction'
import { SocialAction } from '../send/SocialAction'
import { MountType } from '../../enums/MountType'
import { L2StaticObjectInstance } from '../../models/actor/instance/L2StaticObjectInstance'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { ChairSit } from '../send/ChairSit'
import { NpcSay } from '../send/builder/NpcSay'
import { AirShipManager } from '../../instancemanager/AirShipManager'
import { RecipeShopManageList } from '../send/RecipeShopManageList'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { L2StaticObjectInstanceValues } from '../../values/L2StaticObjectInstanceValues'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'
import _ from 'lodash'
import { PlayerActionOverride } from '../../values/PlayerConditions'
import { AIIntent } from '../../aicontroller/enums/AIIntent'
import { L2StaticObjectType } from '../../enums/L2StaticObjectType'
import { recordSummonViolation } from '../../helpers/PlayerViolations'
import { FastRateLimit } from 'fast-ratelimit'
import { NpcSayType } from '../../enums/packets/NpcSayType'

const packetLimiter = new FastRateLimit( {
    threshold: 5, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

const npcPhrases: Array<number> = [
    NpcStringIds.USING_A_SPECIAL_SKILL_HERE_COULD_TRIGGER_A_BLOODBATH,
    NpcStringIds.HEY_WHAT_DO_YOU_EXPECT_OF_ME,
    NpcStringIds.UGGGGGH_PUSH_ITS_NOT_COMING_OUT,
    NpcStringIds.AH_I_MISSED_THE_MARK,
]

const switchStanceSkillId = 6054

export async function RequestActionUse( client: GameClient, packetData: Buffer ): Promise<void> {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let actionId = packet.readD(), controlPresed = packet.readD(), shiftPressed = packet.readC()

    if ( ( player.isFakeDeath() && ( actionId !== 0 ) ) || player.isDead() || player.isOutOfControl() ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    let info: BuffInfo = player.getEffectList().getBuffInfoByAbnormalType( AbnormalType.BOT_PENALTY )
    if ( info ) {
        let shouldExit = _.some( info.getEffects(), ( effect: AbstractEffect ) => {
            return effect.blocksAction( actionId )
        } )

        if ( shouldExit ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_HAVE_BEEN_REPORTED_SO_ACTIONS_NOT_ALLOWED ) )
            player.sendOwnedData( ActionFailed() )
            return
        }
    }

    if ( player.isTransformed() ) {
        let allowedActions: Array<number> = player.isTransformed() ? ExBasicActionListValues.actionsOnTransform : ExBasicActionListValues.defaultActions
        if ( !allowedActions.includes( actionId ) ) {
            player.sendOwnedData( ActionFailed() )
            return
        }
    }

    let isControlPressed = controlPresed === 1
    let isShiftPressed = shiftPressed === 1
    let summon: L2Summon = player.getSummon()
    let target: L2Object = player.getTarget()
    switch ( actionId ) {
        case 0: // Sit/Stand
            if ( player.isSitting() || !player.isMoving() || player.isFakeDeath() ) {
                await useSit( player, target )
            } else {
                player.setAITraitAction( () => {
                    useSit( player, target )
                } )
            }
            return

        case 1: // Walk/Run
            if ( player.isRunning() ) {
                player.setWalking()
            } else {
                player.setRunning()
            }

            return

        case 10: // Private Store - Sell
            return player.tryOpenPrivateSellStore( false )

        case 15: // Change Movement Mode (Pets)
            if ( validateSummon( player, summon, true ) ) {
                summon.notifyFollowStatusChange()
            }

            return

        case 16: // Attack (Pets)
            if ( validateSummon( player, summon, true ) ) {
                if ( summon.canAttack( isControlPressed ) ) {
                    await summon.attackTarget( player.getTarget() )
                }
            }
            break
        case 17: // Stop (Pets)
            if ( validateSummon( player, summon, true ) ) {
                return summon.stopCurrentAction()
            }

            return

        case 19: // Unsummon Pet

            if ( !validateSummon( player, summon, true ) ) {
                break
            }

            if ( summon.isDead() ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.DEAD_PET_CANNOT_BE_RETURNED ) )
                break
            }

            if ( summon.isAttackingNow() || summon.isInCombat() || summon.isMovementDisabled() ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.PET_CANNOT_SENT_BACK_DURING_BATTLE ) )
                break
            }

            if ( summon.isHungry() ) {
                if ( summon.isPet() && ( summon as L2PetInstance ).getPetData().getFood().length !== 0 ) {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_RESTORE_HUNGRY_PETS ) )
                } else {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THE_HELPER_PET_CANNOT_BE_RETURNED ) )
                }
                break
            }

            return summon.unSummon( player )

        case 21: // Change Movement Mode (Servitors)
            if ( validateSummon( player, summon, false ) ) {
                return summon.notifyFollowStatusChange()
            }

            return

        case 22: // Attack (Servitors)
            if ( validateSummon( player, summon, false ) ) {
                if ( summon.canAttack( isControlPressed ) ) {
                    await summon.attackTarget( player.getTarget() )
                }
            }
            break
        case 23: // Stop (Servitors)
            if ( validateSummon( player, summon, false ) ) {
                return summon.stopCurrentAction()
            }

            return

        case 28: // Private Store - Buy
            await player.tryOpenPrivateBuyStore()
            break
        case 32: // Wild Hog Cannon - Wild Cannon
            await useSkillByName( player, isControlPressed, isShiftPressed, 'DDMagic', target, false )
            break
        case 36: // Soulless - Toxic Smoke
            await useSkillByName( player, isControlPressed, isShiftPressed, 'RangeDebuff', target, false )
            break
        case 37: // Dwarven Manufacture
            if ( player.isAlikeDead() ) {
                player.sendOwnedData( ActionFailed() )
                return
            }

            if ( player.getPrivateStoreType() !== PrivateStoreType.None ) {
                player.setPrivateStoreType( PrivateStoreType.None )
                player.broadcastUserInfo()
            }

            if ( player.isSitting() ) {
                await player.standUp()
            }

            player.sendOwnedData( RecipeShopManageList( player, true ) )
            break
        case 38: // Mount/Dismount
            await player.mountSummon( summon )
            break
        case 39: // Soulless - Parasite Burst
            await useSkillByName( player, isControlPressed, isShiftPressed, 'RangeDD', target, false )
            break
        case 41: // Wild Hog Cannon - Attack
            if ( validateSummon( player, summon, false ) ) {
                if ( !target && ( target.isDoor() || ( target.isInstanceType( InstanceType.L2SiegeFlagInstance ) ) ) ) {
                    await useSkill( player, isControlPressed, isShiftPressed, 4230, target, false )
                } else {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
                }
            }
            break
        case 42: // Kai the Cat - Self Damage Shield
            await useSkillByName( player, isControlPressed, isShiftPressed, 'HealMagic', target, false )
            break
        case 43: // Merrow the Unicorn - Hydro Screw
            await useSkillByName( player, isControlPressed, isShiftPressed, 'DDMagic', target, false )
            break
        case 44: // Big Boom - Boom Attack
            await useSkillByName( player, isControlPressed, isShiftPressed, 'DDMagic', target, false )
            break
        case 45: // Boxer the Unicorn - Master Recharge
            await useSkillByName( player, isControlPressed, isShiftPressed, 'HealMagic', player, false )
            break
        case 46: // Mew the Cat - Mega Storm Strike
            await useSkillByName( player, isControlPressed, isShiftPressed, 'DDMagic', target, false )
            break
        case 47: // Silhouette - Steal Blood
            await useSkillByName( player, isControlPressed, isShiftPressed, 'DDMagic', target, false )
            break
        case 48: // Mechanic Golem - Mech. Cannon
            await useSkillByName( player, isControlPressed, isShiftPressed, 'DDMagic', target, false )
            break
        case 51: // General Manufacture
            if ( player.isAlikeDead() ) {
                player.sendOwnedData( ActionFailed() )
                return
            }

            if ( player.getPrivateStoreType() !== PrivateStoreType.None ) {
                player.setPrivateStoreType( PrivateStoreType.None )
                player.broadcastUserInfo()
            }

            if ( player.isSitting() ) {
                await player.standUp()
            }

            player.sendOwnedData( RecipeShopManageList( player, false ) )
            break
        case 52: // Unsummon Servitor
            if ( validateSummon( player, summon, false ) ) {
                if ( summon.isAttackingNow() || summon.isInCombat() ) {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SERVITOR_NOT_RETURN_IN_BATTLE ) )
                    break
                }

                await summon.unSummon( player )
            }
            break
        case 53: // Move to target (Servitors)
            if ( validateSummon( player, summon, false ) ) {
                if ( target && ( summon.getObjectId() !== target.getObjectId() ) && !summon.isMovementDisabled() ) {
                    summon.resetFollowOwner()
                    AIEffectHelper.notifyFollow( summon, target.getObjectId() )
                }
            }
            break
        case 54: // Move to target (Pets)
            if ( validateSummon( player, summon, true ) ) {
                if ( target && ( summon.getObjectId() !== target.getObjectId() ) && !summon.isMovementDisabled() ) {
                    summon.resetFollowOwner()
                    AIEffectHelper.notifyFollow( summon, target.getObjectId() )
                }
            }
            break
        case 61: // Private Store Package Sell
            await player.tryOpenPrivateSellStore( true )
            break
        case 65: // Bot Report Button
            if ( ConfigManager.general.enableBotReportButton() ) {
                BotReportCache.reportBot( player )
            } else {
                player.sendMessage( 'This feature is disabled.' )
            }
            break
        case 67: // Steer
            if ( player.isInAirShip() ) {
                if ( player.getAirShip().setCaptain( player ) ) {
                    player.broadcastUserInfo()
                }
            }
            break
        case 68: // Cancel Control
            if ( player.isInAirShip() && player.getAirShip().isCaptain( player ) ) {
                if ( player.getAirShip().setCaptain( null ) ) {
                    player.broadcastUserInfo()
                }
            }
            break
        case 69: // Destination Map
            AirShipManager.sendAirShipTeleportList( player )
            break
        case 70: // Exit Airship
            if ( player.isInAirShip() ) {
                if ( player.getAirShip().isCaptain( player ) ) {
                    if ( player.getAirShip().setCaptain( null ) ) {
                        player.broadcastUserInfo()
                    }
                } else if ( player.getAirShip().isInDock() ) {
                    player.getAirShip().ejectPlayer( player )
                }
            }
            break
        case 71:
        case 72:
        case 73:
            useCoupleSocialAction( player, target, actionId - 55 )
            break
        case 1000: // Siege Golem - Siege Hammer
            if ( target && target.isDoor() ) {
                await useSkill( player, isControlPressed, isShiftPressed, 4079, target, false )
            }
            break
        case 1001: // Sin Eater - Ultimate Bombastic Buster
            if ( validateSummon( player, summon, true ) && summon.isSinEater() ) {
                let packet = NpcSay.fromNpcString( summon.getObjectId(), NpcSayType.NpcAll, summon.getId(), _.sample( npcPhrases ) )
                BroadcastHelper.dataInLocation( summon, packet.getBuffer(), summon.getBroadcastRadius() )
            }
            break
        case 1003: // Wind Hatchling/Strider - Wild Stun
            return useSkillByName( player, isControlPressed, isShiftPressed, 'PhysicalSpecial', target, true )

        case 1004: // Wind Hatchling/Strider - Wild Defense
            return useSkillByName( player, isControlPressed, isShiftPressed, 'Buff', player, true )

        case 1005: // Star Hatchling/Strider - Bright Burst
            return useSkillByName( player, isControlPressed, isShiftPressed, 'DDMagic', target, true )

        case 1006: // Star Hatchling/Strider - Bright Heal
            return useSkillByName( player, isControlPressed, isShiftPressed, 'Heal', player, true )

        case 1007: // Feline Queen - Blessing of Queen
            return useSkillByName( player, isControlPressed, isShiftPressed, 'Buff1', player, false )

        case 1008: // Feline Queen - Gift of Queen
            return useSkillByName( player, isControlPressed, isShiftPressed, 'Buff2', player, false )

        case 1009: // Feline Queen - Cure of Queen
            return useSkillByName( player, isControlPressed, isShiftPressed, 'DDMagic', target, false )

        case 1010: // Unicorn Seraphim - Blessing of Seraphim
            return useSkillByName( player, isControlPressed, isShiftPressed, 'Buff1', player, false )

        case 1011: // Unicorn Seraphim - Gift of Seraphim
            return useSkillByName( player, isControlPressed, isShiftPressed, 'Buff2', player, false )

        case 1012: // Unicorn Seraphim - Cure of Seraphim
            return useSkillByName( player, isControlPressed, isShiftPressed, 'DDMagic', target, false )

        case 1013: // Nightshade - Curse of Shade
            return useSkillByName( player, isControlPressed, isShiftPressed, 'DeBuff1', target, false )

        case 1014: // Nightshade - Mass Curse of Shade
            return useSkillByName( player, isControlPressed, isShiftPressed, 'DeBuff2', target, false )

        case 1015: // Nightshade - Shade Sacrifice
            return useSkillByName( player, isControlPressed, isShiftPressed, 'Heal', target, false )

        case 1016: // Cursed Man - Cursed Blow
            return useSkillByName( player, isControlPressed, isShiftPressed, 'PhysicalSpecial1', target, false )

        case 1017: // Cursed Man - Cursed Strike
            return useSkillByName( player, isControlPressed, isShiftPressed, 'PhysicalSpecial2', target, false )

        case 1031: // Feline King - Slash
            return useSkillByName( player, isControlPressed, isShiftPressed, 'PhysicalSpecial1', target, false )

        case 1032: // Feline King - Spinning Slash
            return useSkillByName( player, isControlPressed, isShiftPressed, 'PhysicalSpecial2', target, false )

        case 1033: // Feline King - Hold of King
            return useSkillByName( player, isControlPressed, isShiftPressed, 'PhysicalSpecial3', target, false )

        case 1034: // Magnus the Unicorn - Whiplash
            return useSkillByName( player, isControlPressed, isShiftPressed, 'PhysicalSpecial1', target, false )

        case 1035: // Magnus the Unicorn - Tridal Wave
            return useSkillByName( player, isControlPressed, isShiftPressed, 'PhysicalSpecial2', target, false )

        case 1036: // Spectral Lord - Corpse Kaboom
            return useSkillByName( player, isControlPressed, isShiftPressed, 'PhysicalSpecial1', target, false )

        case 1037: // Spectral Lord - Dicing Death
            return useSkillByName( player, isControlPressed, isShiftPressed, 'PhysicalSpecial2', target, false )

        case 1038: // Spectral Lord - Dark Curse
            return useSkillByName( player, isControlPressed, isShiftPressed, 'PhysicalSpecial3', target, false )

        case 1039: // Swoop Cannon - Cannon Fodder
            return useSkill( player, isControlPressed, isShiftPressed, 5110, target, false )

        case 1040: // Swoop Cannon - Big Bang
            return useSkill( player, isControlPressed, isShiftPressed, 5111, target, false )

        case 1041: // Great Wolf - Bite Attack
            return useSkillByName( player, isControlPressed, isShiftPressed, 'Skill01', target, true )

        case 1042: // Great Wolf - Maul
            return useSkillByName( player, isControlPressed, isShiftPressed, 'Skill03', target, true )

        case 1043: // Great Wolf - Cry of the Wolf
            return useSkillByName( player, isControlPressed, isShiftPressed, 'Skill02', target, true )

        case 1044: // Great Wolf - Awakening
            return useSkillByName( player, isControlPressed, isShiftPressed, 'Skill04', target, true )

        case 1045: // Great Wolf - Howl
            return useSkill( player, isControlPressed, isShiftPressed, 5584, target, true )

        case 1046: // Strider - Roar
            return useSkill( player, isControlPressed, isShiftPressed, 5585, target, true )

        case 1047: // Divine Beast - Bite
            return useSkill( player, isControlPressed, isShiftPressed, 5580, target, false )

        case 1048: // Divine Beast - Stun Attack
            return useSkill( player, isControlPressed, isShiftPressed, 5581, target, false )

        case 1049: // Divine Beast - Fire Breath
            return useSkill( player, isControlPressed, isShiftPressed, 5582, target, false )

        case 1050: // Divine Beast - Roar
            return useSkill( player, isControlPressed, isShiftPressed, 5583, target, false )

        case 1051: // Feline Queen - Bless The Body
            return useSkillByName( player, isControlPressed, isShiftPressed, 'buff3', target, false )

        case 1052: // Feline Queen - Bless The Soul
            return useSkillByName( player, isControlPressed, isShiftPressed, 'buff4', target, false )

        case 1053: // Feline Queen - Haste
            return useSkillByName( player, isControlPressed, isShiftPressed, 'buff5', target, false )

        case 1054: // Unicorn Seraphim - Acumen
            return useSkillByName( player, isControlPressed, isShiftPressed, 'buff3', target, false )

        case 1055: // Unicorn Seraphim - Clarity
            return useSkillByName( player, isControlPressed, isShiftPressed, 'buff4', target, false )

        case 1056: // Unicorn Seraphim - Empower
            return useSkillByName( player, isControlPressed, isShiftPressed, 'buff5', target, false )

        case 1057: // Unicorn Seraphim - Wild Magic
            return useSkillByName( player, isControlPressed, isShiftPressed, 'buff6', target, false )

        case 1058: // Nightshade - Death Whisper
            return useSkillByName( player, isControlPressed, isShiftPressed, 'buff3', target, false )

        case 1059: // Nightshade - Focus
            return useSkillByName( player, isControlPressed, isShiftPressed, 'buff4', target, false )

        case 1060: // Nightshade - Guidance
            return useSkillByName( player, isControlPressed, isShiftPressed, 'buff5', target, false )

        case 1061: // Wild Beast Fighter, White Weasel - Death blow
            return useSkill( player, isControlPressed, isShiftPressed, 5745, target, true )

        case 1062: // Wild Beast Fighter - Double attack
            return useSkill( player, isControlPressed, isShiftPressed, 5746, target, true )

        case 1063: // Wild Beast Fighter - Spin attack
            return useSkill( player, isControlPressed, isShiftPressed, 5747, target, true )

        case 1064: // Wild Beast Fighter - Meteor Shower
            return useSkill( player, isControlPressed, isShiftPressed, 5748, target, true )

        case 1065: // Fox Shaman, Wild Beast Fighter, White Weasel, Fairy Princess - Awakening
            return useSkill( player, isControlPressed, isShiftPressed, 5753, target, true )

        case 1066: // Fox Shaman, Spirit Shaman - Thunder Bolt
            return useSkill( player, isControlPressed, isShiftPressed, 5749, target, true )

        case 1067: // Fox Shaman, Spirit Shaman - Flash
            return useSkill( player, isControlPressed, isShiftPressed, 5750, target, true )

        case 1068: // Fox Shaman, Spirit Shaman - Lightning Wave
            return useSkill( player, isControlPressed, isShiftPressed, 5751, target, true )

        case 1069: // Fox Shaman, Fairy Princess - Flare
            return useSkill( player, isControlPressed, isShiftPressed, 5752, target, true )

        case 1070: // White Weasel, Fairy Princess, Improved Baby Buffalo, Improved Baby Kookaburra, Improved Baby Cougar, Spirit Shaman, Toy Knight, Turtle Ascetic - Buff control
            return useSkill( player, isControlPressed, isShiftPressed, 5771, target, true )

        case 1071: // Tigress - Power Strike
            return useSkillByName( player, isControlPressed, isShiftPressed, 'DDMagic', target, true )

        case 1072: // Toy Knight - Piercing attack
            return useSkill( player, isControlPressed, isShiftPressed, 6046, target, true )

        case 1073: // Toy Knight - Whirlwind
            return useSkill( player, isControlPressed, isShiftPressed, 6047, target, true )

        case 1074: // Toy Knight - Lance Smash
            return useSkill( player, isControlPressed, isShiftPressed, 6048, target, true )

        case 1075: // Toy Knight - Battle Cry
            return useSkill( player, isControlPressed, isShiftPressed, 6049, target, true )

        case 1076: // Turtle Ascetic - Power Smash
            return useSkill( player, isControlPressed, isShiftPressed, 6050, target, true )

        case 1077: // Turtle Ascetic - Energy Burst
            return useSkill( player, isControlPressed, isShiftPressed, 6051, target, true )

        case 1078: // Turtle Ascetic - Shockwave
            return useSkill( player, isControlPressed, isShiftPressed, 6052, target, true )

        case 1079: // Turtle Ascetic - Howl
            return useSkill( player, isControlPressed, isShiftPressed, 6053, target, true )

        case 1080: // Phoenix Rush
            return useSkill( player, isControlPressed, isShiftPressed, 6041, target, false )

        case 1081: // Phoenix Cleanse
            return useSkill( player, isControlPressed, isShiftPressed, 6042, target, false )

        case 1082: // Phoenix Flame Feather
            return useSkill( player, isControlPressed, isShiftPressed, 6043, target, false )

        case 1083: // Phoenix Flame Beak
            return useSkill( player, isControlPressed, isShiftPressed, 6044, target, false )

        case 1084: // Switch State
            return useSkill( player, isControlPressed, isShiftPressed, 6054, target, true )

        case 1086: // Panther Cancel
            return useSkill( player, isControlPressed, isShiftPressed, 6094, target, false )

        case 1087: // Panther Dark Claw
            return useSkill( player, isControlPressed, isShiftPressed, 6095, target, false )

        case 1088: // Panther Fatal Claw
            return useSkill( player, isControlPressed, isShiftPressed, 6096, target, false )

        case 1089: // Deinonychus - Tail Strike
            return useSkill( player, isControlPressed, isShiftPressed, 6199, target, true )

        case 1090: // Guardian's Strider - Strider Bite
            return useSkill( player, isControlPressed, isShiftPressed, 6205, target, true )

        case 1091: // Guardian's Strider - Strider Fear
            return useSkill( player, isControlPressed, isShiftPressed, 6206, target, true )

        case 1092: // Guardian's Strider - Strider Dash
            return useSkill( player, isControlPressed, isShiftPressed, 6207, target, true )

        case 1093: // Maguen - Maguen Strike
            return useSkill( player, isControlPressed, isShiftPressed, 6618, target, true )

        case 1094: // Maguen - Maguen Wind Walk
            return useSkill( player, isControlPressed, isShiftPressed, 6681, target, true )

        case 1095: // Elite Maguen - Maguen Power Strike
            return useSkill( player, isControlPressed, isShiftPressed, 6619, target, true )

        case 1096: // Elite Maguen - Elite Maguen Wind Walk
            return useSkill( player, isControlPressed, isShiftPressed, 6682, target, true )

        case 1097: // Maguen - Maguen Return
            return useSkill( player, isControlPressed, isShiftPressed, 6683, target, true )

        case 1098: // Elite Maguen - Maguen Party Return
            return useSkill( player, isControlPressed, isShiftPressed, 6684, target, true )

        case 5000: // Baby Rudolph - Reindeer Scratch
            return useSkill( player, isControlPressed, isShiftPressed, 23155, target, true )

        case 5001: // Deseloph, Hyum, Rekang, Lilias, Lapham, Mafum - Rosy Seduction
            return useSkill( player, isControlPressed, isShiftPressed, 23167, target, true )

        case 5002: // Deseloph, Hyum, Rekang, Lilias, Lapham, Mafum - Critical Seduction
            return useSkill( player, isControlPressed, isShiftPressed, 23168, target, true )

        case 5003: // Hyum, Lapham, Hyum, Lapham - Thunder Bolt
            return useSkill( player, isControlPressed, isShiftPressed, 5749, target, true )

        case 5004: // Hyum, Lapham, Hyum, Lapham - Flash
            return useSkill( player, isControlPressed, isShiftPressed, 5750, target, true )

        case 5005: // Hyum, Lapham, Hyum, Lapham - Lightning Wave
            return useSkill( player, isControlPressed, isShiftPressed, 5751, target, true )

        case 5006: // Deseloph, Hyum, Rekang, Lilias, Lapham, Mafum, Deseloph, Hyum, Rekang, Lilias, Lapham, Mafum - Buff Control
            return useSkill( player, isControlPressed, isShiftPressed, 5771, target, true )

        case 5007: // Deseloph, Lilias, Deseloph, Lilias - Piercing Attack
            return useSkill( player, isControlPressed, isShiftPressed, 6046, target, true )

        case 5008: // Deseloph, Lilias, Deseloph, Lilias - Spin Attack
            return useSkill( player, isControlPressed, isShiftPressed, 6047, target, true )

        case 5009: // Deseloph, Lilias, Deseloph, Lilias - Smash
            return useSkill( player, isControlPressed, isShiftPressed, 6048, target, true )

        case 5010: // Deseloph, Lilias, Deseloph, Lilias - Ignite
            return useSkill( player, isControlPressed, isShiftPressed, 6049, target, true )

        case 5011: // Rekang, Mafum, Rekang, Mafum - Power Smash
            return useSkill( player, isControlPressed, isShiftPressed, 6050, target, true )

        case 5012: // Rekang, Mafum, Rekang, Mafum - Energy Burst
            return useSkill( player, isControlPressed, isShiftPressed, 6051, target, true )

        case 5013: // Rekang, Mafum, Rekang, Mafum - Shockwave
            return useSkill( player, isControlPressed, isShiftPressed, 6052, target, true )

        case 5014: // Rekang, Mafum, Rekang, Mafum - Ignite
            return useSkill( player, isControlPressed, isShiftPressed, 6053, target, true )

        case 5015: // Deseloph, Hyum, Rekang, Lilias, Lapham, Mafum, Deseloph, Hyum, Rekang, Lilias, Lapham, Mafum - Switch Stance
            return useSkill( player, isControlPressed, isShiftPressed, 6054, target, true )

            // Social Packets
        case 12: // Greeting
            tryBroadcastSocial( player, 2 )
            break
        case 13: // Victory
            tryBroadcastSocial( player, 3 )
            break
        case 14: // Advance
            tryBroadcastSocial( player, 4 )
            break
        case 24: // Yes
            tryBroadcastSocial( player, 6 )
            break
        case 25: // No
            tryBroadcastSocial( player, 5 )
            break
        case 26: // Bow
            tryBroadcastSocial( player, 7 )
            break
        case 29: // Unaware
            tryBroadcastSocial( player, 8 )
            break
        case 30: // Social Waiting
            tryBroadcastSocial( player, 9 )
            break
        case 31: // Laugh
            tryBroadcastSocial( player, 10 )
            break
        case 33: // Applaud
            tryBroadcastSocial( player, 11 )
            break
        case 34: // Dance
            tryBroadcastSocial( player, 12 )
            break
        case 35: // Sorrow
            tryBroadcastSocial( player, 13 )
            break
        case 62: // Charm
            tryBroadcastSocial( player, 14 )
            break
        case 66: // Shyness
            tryBroadcastSocial( player, 15 )
            break
        default:
            break
    }
}

function tryBroadcastSocial( player: L2PcInstance, actionId: number ): void {
    if ( player.canMakeSocialAction() ) {
        BroadcastHelper.dataToSelfBasedOnVisibility( player, SocialAction( player.getObjectId(), actionId ) )
    }
}

async function useSkill( player: L2PcInstance, isControlPressed: boolean, isShiftPressed: boolean, skillId: number, target: L2Object, isPet: boolean ): Promise<void> {

    let summon: L2Summon = player.getSummon()
    if ( !validateSummon( player, summon, isPet ) ) {
        return
    }

    if ( !player.hasActionOverride( PlayerActionOverride.PetCommands ) && !canControl( player, summon ) ) {
        return
    }

    let level = summon.getSkillLevel( skillId )
    if ( level === 0 ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.PET_TOO_HIGH_TO_CONTROL ) )
        return
    }

    let skill = SkillCache.getSkill( skillId, level )
    if ( !skill ) {
        return
    }

    summon.setTarget( target )
    await summon.useMagic( skill, isControlPressed, isShiftPressed )

    if ( skill.getId() === switchStanceSkillId ) {
        summon.switchStance()
    }
}

function validateSummon( player: L2PcInstance, summon: L2Summon, isPet: boolean ) : boolean {
    if ( summon && ( ( isPet && summon.isPet() ) || summon.isServitor() ) ) {
        if ( summon.isPet() && ( summon as L2PetInstance ).isUncontrollable() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.WHEN_YOUR_PETS_HUNGER_GAUGE_IS_AT_0_YOU_CANNOT_USE_YOUR_PET ) )
            return false
        }
        if ( summon.isBetrayed() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.PET_REFUSING_ORDER ) )
            return false
        }
        return true
    }

    if ( isPet ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.DONT_HAVE_PET ) )
    } else {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.DONT_HAVE_SERVITOR ) )
    }

    recordSummonViolation( player.getObjectId(), 'dd1b3dba-7f8a-4c8a-92b0-5a0521940fe6', 'Player uses summon action/skill without active pet/servitor' )

    return false
}

function canControl( player: L2PcInstance, summon: L2Summon ) : boolean {
    if ( summon.isPet() ) {
        if ( summon.hasSupportStance() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.PET_AUXILIARY_MODE_CANNOT_USE_SKILLS ) )
            return false
        }


        if ( ( summon.getLevel() - player.getLevel() ) > ConfigManager.tuning.getPetControlLevelDifference() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.PET_TOO_HIGH_TO_CONTROL ) )
            return false
        }
    }

    return true
}

async function useSkillByName( player: L2PcInstance, isControlPressed: boolean, isShiftPressed: boolean, name: string, target: L2Object, isPet: boolean ): Promise<void> {
    let summon: L2Summon = player.getSummon()
    if ( !validateSummon( player, summon, isPet ) ) {
        return
    }

    if ( !canControl( player, summon ) ) {
        return
    }

    let skillData = summon.getTemplate().getSkillParameters()[ name ]
    if ( !skillData ) {
        return
    }

    let skill: Skill = SkillCache.getSkill( skillData.id, skillData.level )
    if ( !skill ) {
        return
    }

    summon.setTarget( target )
    await summon.useMagic( skill, isControlPressed, isShiftPressed )

    if ( skill.getId() === switchStanceSkillId ) {
        summon.switchStance()
    }
}

function useCoupleSocialAction( player: L2PcInstance, target: L2Object, id: number ) : void {
    if ( !target || !target.isPlayer() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
        return
    }

    let distance = player.calculateDistance( target )
    if ( ( distance > 125 ) || ( distance < 15 ) || ( player.getObjectId() === target.getObjectId() ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_DO_NOT_MEET_LOC_REQUIREMENTS ) )
        return
    }

    if ( player.isInStoreMode() || player.isInCraftMode() ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.C1_IS_IN_PRIVATE_SHOP_MODE_OR_IN_A_BATTLE_AND_CANNOT_BE_REQUESTED_FOR_A_COUPLE_ACTION )
                .addPlayerCharacterName( player )
        player.sendOwnedData( packet.getBuffer() )
        return
    }

    if ( player.isInCombat() || player.isInDuel() || player.isUnderAttack() ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.C1_IS_IN_A_BATTLE_AND_CANNOT_BE_REQUESTED_FOR_A_COUPLE_ACTION )
                .addPlayerCharacterName( player )
        player.sendOwnedData( packet.getBuffer() )
        return
    }

    if ( player.isFishing() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_DO_WHILE_FISHING_3 ) )
        return
    }

    if ( player.getKarma() > 0 ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.C1_IS_IN_A_CHAOTIC_STATE_AND_CANNOT_BE_REQUESTED_FOR_A_COUPLE_ACTION )
                .addPlayerCharacterName( player )
        player.sendOwnedData( packet.getBuffer() )
        return
    }

    if ( player.isInOlympiadMode() ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.C1_IS_PARTICIPATING_IN_THE_OLYMPIAD_AND_CANNOT_BE_REQUESTED_FOR_A_COUPLE_ACTION )
                .addPlayerCharacterName( player )
        player.sendOwnedData( packet.getBuffer() )
        return
    }

    if ( player.isInSiege() ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.C1_IS_IN_A_CASTLE_SIEGE_AND_CANNOT_BE_REQUESTED_FOR_A_COUPLE_ACTION )
                .addPlayerCharacterName( player )
        player.sendOwnedData( packet.getBuffer() )
        return
    }

    if ( player.isInHideoutSiege ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.C1_IS_PARTICIPATING_IN_A_HIDEOUT_SIEGE_AND_CANNOT_BE_REQUESTED_FOR_A_COUPLE_ACTION )
                .addPlayerCharacterName( player )
        player.sendOwnedData( packet.getBuffer() )
        return
    }

    if ( player.isMounted() || player.isFlyingMounted() || player.isInBoat() || player.isInAirShip() ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.C1_IS_RIDING_A_SHIP_STEED_OR_STRIDER_AND_CANNOT_BE_REQUESTED_FOR_A_COUPLE_ACTION )
                .addPlayerCharacterName( player )
        player.sendOwnedData( packet.getBuffer() )
        return
    }

    if ( player.isTransformed() ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.C1_IS_CURRENTLY_TRANSFORMING_AND_CANNOT_BE_REQUESTED_FOR_A_COUPLE_ACTION )
                .addPlayerCharacterName( player )
        player.sendOwnedData( packet.getBuffer() )
        return
    }

    if ( player.isAlikeDead() ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.C1_IS_CURRENTLY_DEAD_AND_CANNOT_BE_REQUESTED_FOR_A_COUPLE_ACTION )
                .addPlayerCharacterName( player )
        player.sendOwnedData( packet.getBuffer() )
        return
    }

    let partner: L2PcInstance = target.getActingPlayer()
    if ( partner.isInStoreMode() || partner.isInCraftMode() ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.C1_IS_IN_PRIVATE_SHOP_MODE_OR_IN_A_BATTLE_AND_CANNOT_BE_REQUESTED_FOR_A_COUPLE_ACTION )
                .addPlayerCharacterName( partner )
        player.sendOwnedData( packet.getBuffer() )
        return
    }

    if ( partner.isInCombat() || partner.isInDuel() || player.isUnderAttack() ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.C1_IS_IN_A_BATTLE_AND_CANNOT_BE_REQUESTED_FOR_A_COUPLE_ACTION )
                .addPlayerCharacterName( partner )
        player.sendOwnedData( packet.getBuffer() )
        return
    }

    if ( partner.getMultiSociaAction() > 0 ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.C1_IS_ALREADY_PARTICIPATING_IN_A_COUPLE_ACTION_AND_CANNOT_BE_REQUESTED_FOR_ANOTHER_COUPLE_ACTION )
                .addPlayerCharacterName( partner )
        player.sendOwnedData( packet.getBuffer() )
        return
    }

    if ( partner.isFishing() ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.C1_IS_FISHING_AND_CANNOT_BE_REQUESTED_FOR_A_COUPLE_ACTION )
                .addPlayerCharacterName( partner )
        player.sendOwnedData( packet.getBuffer() )
        return
    }

    if ( partner.getKarma() > 0 ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.C1_IS_IN_A_CHAOTIC_STATE_AND_CANNOT_BE_REQUESTED_FOR_A_COUPLE_ACTION )
                .addPlayerCharacterName( partner )
        player.sendOwnedData( packet.getBuffer() )
        return
    }

    if ( partner.isInOlympiadMode() ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.C1_IS_PARTICIPATING_IN_THE_OLYMPIAD_AND_CANNOT_BE_REQUESTED_FOR_A_COUPLE_ACTION )
                .addPlayerCharacterName( partner )
        player.sendOwnedData( packet.getBuffer() )
        return
    }

    if ( partner.isInHideoutSiege ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.C1_IS_PARTICIPATING_IN_A_HIDEOUT_SIEGE_AND_CANNOT_BE_REQUESTED_FOR_A_COUPLE_ACTION )
                .addPlayerCharacterName( partner )
        player.sendOwnedData( packet.getBuffer() )
        return
    }

    if ( partner.isInSiege() ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.C1_IS_IN_A_CASTLE_SIEGE_AND_CANNOT_BE_REQUESTED_FOR_A_COUPLE_ACTION )
                .addPlayerCharacterName( partner )
        player.sendOwnedData( packet.getBuffer() )
        return
    }

    if ( partner.isMounted() || partner.isFlyingMounted() || partner.isInBoat() || partner.isInAirShip() ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.C1_IS_RIDING_A_SHIP_STEED_OR_STRIDER_AND_CANNOT_BE_REQUESTED_FOR_A_COUPLE_ACTION )
                .addPlayerCharacterName( partner )
        player.sendOwnedData( packet.getBuffer() )
        return
    }

    if ( partner.isTeleporting() ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.C1_IS_CURRENTLY_TELEPORTING_AND_CANNOT_BE_REQUESTED_FOR_A_COUPLE_ACTION )
                .addPlayerCharacterName( partner )
        player.sendOwnedData( packet.getBuffer() )
        return
    }

    if ( partner.isTransformed() ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.C1_IS_CURRENTLY_TRANSFORMING_AND_CANNOT_BE_REQUESTED_FOR_A_COUPLE_ACTION )
                .addPlayerCharacterName( partner )
        player.sendOwnedData( packet.getBuffer() )
        return
    }

    if ( partner.isAlikeDead() ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.C1_IS_CURRENTLY_DEAD_AND_CANNOT_BE_REQUESTED_FOR_A_COUPLE_ACTION )
                .addPlayerCharacterName( partner )
        player.sendOwnedData( packet.getBuffer() )
        return
    }

    if ( player.isAllSkillsDisabled() || partner.isAllSkillsDisabled() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.COUPLE_ACTION_CANCELED ) )
        return
    }

    player.setMultiSocialAction( id, partner.getObjectId() )

    let requestStatus = new SystemMessageBuilder( SystemMessageIds.YOU_HAVE_REQUESTED_COUPLE_ACTION_C1 )
            .addPlayerCharacterName( partner )
    player.sendOwnedData( requestStatus.getBuffer() )

    if ( player.getAIController().isIntent( AIIntent.WAITING ) || !partner.getAIController().isIntent( AIIntent.WAITING ) ) {
        player.setAITraitAction( () => partner.sendOwnedData( ExAskCoupleAction( player.getObjectId(), id ) ) )
        return
    }

    if ( player.isCastingNow() || player.isCastingSimultaneouslyNow() ) {
        player.setAITraitAction( () => partner.sendOwnedData( ExAskCoupleAction( player.getObjectId(), id ) ) )
        return
    }

    partner.sendOwnedData( ExAskCoupleAction( player.getObjectId(), id ) )
}

async function useSit( player: L2PcInstance, target: L2Object ): Promise<boolean> {
    if ( player.getMountType() !== MountType.NONE ) {
        return false
    }

    if ( !player.isSitting()
            && target
            && target.isStaticObject()
            && ( ( target as L2StaticObjectInstance ).getType() === L2StaticObjectType.Throne )
            && player.isInsideRadius( target, L2StaticObjectInstanceValues.interactionDistance ) ) {

        BroadcastHelper.dataBasedOnVisibility( player, ChairSit( player.getObjectId(), target.getId() ) )
        await player.sitDown()

        return true
    }

    if ( player.isFakeDeath() ) {
        await player.stopEffects( L2EffectType.FakeDeath )
    } else if ( player.isSitting() ) {
        await player.standUp()
    } else {
        await player.sitDown()
    }

    return true
}