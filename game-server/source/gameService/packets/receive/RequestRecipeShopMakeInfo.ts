import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2World } from '../../L2World'
import { PrivateStoreType } from '../../enums/PrivateStoreType'
import { RecipeShopItemInfo } from '../send/RecipeShopItemInfo'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { DataManager } from '../../../data/manager'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestRecipeShopMakeInfo( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let playerId = packet.readD(), recipeId = packet.readD()

    let manufacturePlayer : L2PcInstance = L2World.getPlayer( playerId )
    if ( !manufacturePlayer || ( manufacturePlayer.getPrivateStoreType() !== PrivateStoreType.Manufacture ) ) {
        return
    }

    if ( !DataManager.getRecipeData().getRecipeList( recipeId ) ) {
        return
    }

    player.sendOwnedData( RecipeShopItemInfo( player, recipeId ) )
}