import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2AirShipInstance } from '../../models/actor/instance/L2AirShipInstance'
import { ActionFailed } from '../send/ActionFailed'
import { Location } from '../../models/Location'
import { WeaponType } from '../../models/items/type/WeaponType'
import { StopMoveInVehicle } from '../send/StopMoveInVehicle'
import { ExMoveToLocationInAirShip } from '../send/ExMoveToLocationInAirShip'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 2, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function MoveToLocationInAirShip( client: GameClient, packetData: Buffer ): void {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let shipId = packet.readD(),
            targetX = packet.readD(),
            targetY = packet.readD(),
            targetZ = packet.readD(),
            originX = packet.readD(),
            originY = packet.readD(),
            originZ = packet.readD()

    if ( ( targetX === originX ) && ( targetY === originY ) && ( targetZ === originZ ) ) {
        player.sendOwnedData( StopMoveInVehicle( player, shipId ) )
        return
    }

    if ( player.isAttackingNow()
            && player.getActiveWeaponItem()
            && ( player.getActiveWeaponItem().getItemType() === WeaponType.BOW ) ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    if ( player.isSitting() || player.isMovementDisabled() ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    if ( !player.isInAirShip() ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    let airShip: L2AirShipInstance = player.getAirShip()
    if ( airShip.getObjectId() !== shipId ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    player.setInVehiclePosition( new Location( targetX, targetY, targetZ ) )
    return BroadcastHelper.dataToSelfInRange( player, ExMoveToLocationInAirShip( player ) )
}