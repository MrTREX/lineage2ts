import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Clan } from '../../models/L2Clan'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { CrestType, L2Crest } from '../../models/L2Crest'
import { CrestCache } from '../../cache/CrestCache'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { ClanPrivilege } from '../../enums/ClanPriviledge'

export async function RequestExSetPledgeCrestLarge( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let clan : L2Clan = player.getClan()
    if ( !clan ) {
        return
    }

    if ( clan.getDissolvingExpiryTime() > Date.now() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_SET_CREST_WHILE_DISSOLUTION_IN_PROGRESS ) )
        return
    }

    if ( !player.hasClanPrivilege( ClanPrivilege.RegisterCrest ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ARE_NOT_AUTHORIZED_TO_DO_THAT ) )
        return
    }

    if ( clan.getLevel() < 3 ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLAN_LVL_3_NEEDED_TO_SET_CREST ) )
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let length : number = packet.readD()

    if ( length < 0 || length > 2176 ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.WRONG_SIZE_UPLOADED_CREST ) )
        return
    }

    if ( length === 0 && clan.getCrestLargeId() !== 0 ) {
        await clan.changeLargeCrest( 0 )
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLAN_CREST_HAS_BEEN_DELETED ) )
        return
    }

    let data : Buffer = packet.readB( length )

    let crest : L2Crest = await CrestCache.createCrest( data, CrestType.PLEDGE_LARGE )
    if ( crest ) {
        await clan.changeLargeCrest( crest.getId() )
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLAN_EMBLEM_WAS_SUCCESSFULLY_REGISTERED ) )
        return
    }
}