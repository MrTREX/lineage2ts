import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2PcInstanceValues } from '../../values/L2PcInstanceValues'
import { EnchantResult, EnchantResultOutcome } from '../send/EnchantResult'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 3, // time-to-live value of token bucket (in seconds)
} )

export function RequestExCancelEnchantItem( client: GameClient ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    player.sendOwnedData( EnchantResult( EnchantResultOutcome.Cancelled, 0, 0 ) )
    player.setActiveEnchantItemId( L2PcInstanceValues.EmptyId )
}