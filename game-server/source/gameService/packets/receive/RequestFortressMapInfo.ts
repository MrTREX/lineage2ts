import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { FortManager } from '../../instancemanager/FortManager'
import { Fort } from '../../models/entity/Fort'
import { ActionFailed } from '../send/ActionFailed'
import { ExShowFortressMapInfo } from '../send/ExShowFortressMapInfo'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'

export function RequestFortressMapInfo( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let id : number = new ReadableClientPacket( packetData ).readD()

    let fort : Fort = FortManager.getFortById( id )

    if ( !fort ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    player.sendOwnedData( ExShowFortressMapInfo ( fort ) )
}