import { GameClient } from '../../GameClient'
import { AllyCrest } from '../send/AllyCrest'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestAllyCrest( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let crestId: number = new ReadableClientPacket( packetData ).readD()
    client.sendPacket( AllyCrest( crestId ) )
}