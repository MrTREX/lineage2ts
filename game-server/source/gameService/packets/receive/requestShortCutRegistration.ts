import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ShortcutType } from '../../enums/ShortcutType'
import { ShortCutRegister } from '../send/ShortCutRegister'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { PlayerShortcutCache } from '../../cache/PlayerShortcutCache'
import { L2PlayerShortcutsTableItem } from '../../../database/interface/CharacterShortcutsTableApi'
import { FastRateLimit } from 'fast-ratelimit'
import { PlayerMacrosCache } from '../../cache/PlayerMacrosCache'
import { TeleportBookmarkCache } from '../../cache/TeleportBookmarkCache'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 2, // time-to-live value of token bucket (in seconds)
} )

export function RequestShortCutRegistration( client: GameClient, packetData: Buffer ) : void {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let type = packet.readD(),
        slotValue = packet.readD(),
        id = packet.readD(),
        level = packet.readD(),
        characterType = packet.readD()

    if ( type < 1 || type > 6 ) {
        return
    }

    let page = Math.floor( slotValue / 12 )

    if ( page > 10 || page < 0 ) {
        return
    }

    let slot = slotValue % 12
    let itemReuseGroup : number = 0

    switch ( type ) {
        case ShortcutType.ITEM:
            let item = player.getInventory().getItemByObjectId( id )
            if ( !item ) {
                return
            }

            itemReuseGroup = item.getEtcItem().getSharedReuseGroup()
            break

        case ShortcutType.SKILL:
            if ( player.getSkillLevel( id ) < 1 ) {
                return
            }

            let skill = player.getKnownSkill( id )
            if ( !skill ) {
                return
            }

            if ( skill.getLevel() !== level ) {
                level = skill.getLevel()
            }

            break

        case ShortcutType.MACRO:
            if ( !PlayerMacrosCache.hasMacro( player.getObjectId(), id ) ) {
                return
            }

            break

        case ShortcutType.RECIPE:
            if ( !player.inRecipeBook( id ) ) {
                return
            }

            break

        case ShortcutType.BOOKMARK:
            if ( !TeleportBookmarkCache.hasBookmark( player.getObjectId(), id ) ) {
                return
            }

            break
    }

    let data : L2PlayerShortcutsTableItem = {
        classIndex: player.getClassIndex(),
        objectId: player.getObjectId(),
        characterType,
        id,
        itemReuseGroup,
        level,
        page,
        slot,
        type
    }

    if ( PlayerShortcutCache.registerShortcut( player, data ) ) {
        player.sendOwnedData( ShortCutRegister( data ) )
    }
}