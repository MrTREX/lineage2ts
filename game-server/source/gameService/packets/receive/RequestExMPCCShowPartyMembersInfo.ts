import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2World } from '../../L2World'
import { ExMPCCShowPartyMemberInfo } from '../send/ExMPCCShowPartyMemberInfo'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'

export function RequestExMPCCShowPartyMembersInfo( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let leaderId : number = new ReadableClientPacket( packetData ).readD()

    let partyLeader : L2PcInstance = L2World.getPlayer( leaderId )
    if ( partyLeader && partyLeader.getParty() ) {
        player.sendOwnedData( ExMPCCShowPartyMemberInfo( partyLeader.getParty() ) )
    }
}