import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Clan } from '../../models/L2Clan'
import { ClanCache } from '../../cache/ClanCache'
import { ActionFailed } from '../send/ActionFailed'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { L2World } from '../../L2World'
import { L2ClanMember } from '../../models/L2ClanMember'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'
import { ClanPrivilege } from '../../enums/ClanPriviledge'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 2, // time-to-live value of token bucket (in seconds)
} )

function broadcastInfo( memberId: number ) :void {
    let member = L2World.getPlayer( memberId )
    if ( member ) {
        member.broadcastUserInfo()
    }
}

export async function RequestStopPledgeWar( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player : L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let playerClan : L2Clan = player.getClan()
    if ( !playerClan ) {
        return
    }

    let clanName : string = new ReadableClientPacket( packetData ).readS()

    let clan : L2Clan = ClanCache.getClanByName( clanName )

    if ( !clan ) {
        player.sendMessage( 'No such clan exists!' )
        player.sendOwnedData( ActionFailed() )
        return
    }

    if ( !playerClan.isAtWarWith( clan.getId() ) ) {
        player.sendMessage( 'You are not at war with this clan.' )
        player.sendOwnedData( ActionFailed() )
        return
    }

    if ( !player.hasClanPrivilege( ClanPrivilege.PledgeWar ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ARE_NOT_AUTHORIZED_TO_DO_THAT ) )
        return
    }

    let shouldStop = playerClan.getMembers().some( ( member: L2ClanMember ) : boolean => {
        let memberPlayer = member.getPlayerInstance()
        return memberPlayer && memberPlayer.isUnderAttack()
    } )

    if ( shouldStop ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_STOP_CLAN_WAR_WHILE_IN_COMBAT ) )
        return
    }

    await ClanCache.deleteClanWars( playerClan.getId(), clan.getId() )

    playerClan.getOnlineMembers().forEach( broadcastInfo )
    clan.getOnlineMembers().forEach( broadcastInfo )
}