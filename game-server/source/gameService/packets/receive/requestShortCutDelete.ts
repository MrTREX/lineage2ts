import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { PlayerShortcutCache } from '../../cache/PlayerShortcutCache'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 2, // time-to-live value of token bucket (in seconds)
} )

export function RequestShortCutDelete( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let id: number = new ReadableClientPacket( packetData ).readD()
    let page = Math.floor( id / 12 )

    if ( page > 10 || page < 0 ) {
        return
    }

    let slot = id % 12
    return PlayerShortcutCache.deleteShortCut( player, slot, page, true )
}