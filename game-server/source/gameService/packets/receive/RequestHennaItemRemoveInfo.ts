import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Henna } from '../../models/items/L2Henna'
import { DataManager } from '../../../data/manager'
import { ActionFailed } from '../send/ActionFailed'
import { HennaItemRemoveInfo } from '../send/HennaItemRemoveInfo'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestHennaItemRemoveInfo( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return player.sendOwnedData( ActionFailed() )
    }

    let symbolId : number = new ReadableClientPacket( packetData ).readD()

    let henna : L2Henna = DataManager.getHennas().get( symbolId )
    if ( !henna ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    player.sendOwnedData( HennaItemRemoveInfo( henna, player ) )
}