import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ConfigManager } from '../../../config/ConfigManager'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { FriendAdd } from '../send/FriendPacket'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'
import { PlayerFriendsCache } from '../../cache/PlayerFriendsCache'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestAnswerFriendInvite( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let requester : L2PcInstance = player.getActiveRequester()
    if ( !requester ) {
        return
    }

    let playerFriendIds = PlayerFriendsCache.getFriends( player.getObjectId() )
    let requesterFriendIds = PlayerFriendsCache.getFriends( requester.getObjectId() )

    if ( playerFriendIds.has( requester.getObjectId() ) || requesterFriendIds.has( player.getObjectId() ) ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.S1_ALREADY_IN_FRIENDS_LIST )
                .addCharacterName( player )
                .getBuffer()
        requester.sendOwnedData( packet )
        return
    }

    if ( playerFriendIds.size >= ConfigManager.character.getFriendListLimit() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CAN_ONLY_ENTER_UP_128_NAMES_IN_YOUR_FRIENDS_LIST ) )
        return
    }

    if ( requesterFriendIds.size >= ConfigManager.character.getFriendListLimit() ) {
        requester.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THE_FRIENDS_LIST_OF_THE_PERSON_YOU_ARE_TRYING_TO_ADD_IS_FULL_SO_REGISTRATION_IS_NOT_POSSIBLE ) )
        return
    }

    let response : number = new ReadableClientPacket( packetData ).readD()

    if ( response !== 1 ) {
        requester.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.FAILED_TO_INVITE_A_FRIEND ) )
    } else {
        PlayerFriendsCache.addFriend( requester.getObjectId(), player.getObjectId() )

        requester.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_HAVE_SUCCEEDED_INVITING_FRIEND ) )

        let friendPacket = new SystemMessageBuilder( SystemMessageIds.S1_ADDED_TO_FRIENDS )
                .addString( player.getName() )
                .getBuffer()
        requester.sendOwnedData( friendPacket )

        let playerPacket = new SystemMessageBuilder( SystemMessageIds.S1_JOINED_AS_FRIEND )
                .addString( requester.getName() )
                .getBuffer()
        player.sendOwnedData( playerPacket )

        player.sendOwnedData( FriendAdd( requester ) )
        requester.sendOwnedData( FriendAdd( player ) )
    }

    player.setActiveRequester( null )
    requester.onTransactionResponse()
}