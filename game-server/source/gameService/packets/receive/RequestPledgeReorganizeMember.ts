import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Clan } from '../../models/L2Clan'
import { L2ClanMember } from '../../models/L2ClanMember'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { ClanPrivilege } from '../../enums/ClanPriviledge'

export async function RequestPledgeReorganizeMember( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let clan : L2Clan = player.getClan()
    if ( !clan ) {
        return
    }

    if ( !player.hasClanPrivilege( ClanPrivilege.ManageRanks ) ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let memberSelectedValue = packet.readD(), memberOneName = packet.readS(), pledgeType = packet.readD(), memberTwoName = packet.readS()

    if ( memberSelectedValue === 0 ) {
        return
    }

    let memberOne : L2ClanMember = clan.getClanMemberByName( memberOneName )
    if ( !memberOne || ( memberOne.getObjectId() === clan.getLeaderId() ) ) {
        return
    }

    let memberTwo : L2ClanMember = clan.getClanMemberByName( memberTwoName )
    if ( !memberTwo || ( memberTwo.getObjectId() === clan.getLeaderId() ) ) {
        return
    }

    let oldPledgeType = memberOne.getPledgeType()
    if ( oldPledgeType === pledgeType ) {
        return
    }

    await memberOne.setPledgeType( pledgeType )
    await memberTwo.setPledgeType( oldPledgeType )
    clan.broadcastClanStatus()
}