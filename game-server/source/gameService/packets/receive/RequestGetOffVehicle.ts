import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ActionFailed } from '../send/ActionFailed'
import { StopMoveInVehicle } from '../send/StopMoveInVehicle'
import { GetOffVehicle } from '../send/GetOffVehicle'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 2, // time-to-live value of token bucket (in seconds)
} )

export function RequestGetOffVehicle( client: GameClient, packetData: Buffer ): void {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let boatId = packet.readD(), x = packet.readD(), y = packet.readD(), z = packet.readD()

    if ( !player.isInBoat()
            || ( player.getBoat().getObjectId() !== boatId )
            || player.getBoat().isMoving()
            || !player.isInsideRadiusCoordinates( x, y, z, 1000, true ) ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    BroadcastHelper.dataToSelfInRange( player, StopMoveInVehicle( player, boatId ) )
    player.setVehicle( null )
    player.setInVehiclePosition( null )
    player.sendOwnedData( ActionFailed() )

    BroadcastHelper.dataToSelfInRange( player, GetOffVehicle( player.getObjectId(), boatId, x, y, z ) )
    player.setXYZ( x, y, z )
    player.revalidateZone( true )
}