import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { Skill } from '../../models/Skill'
import { SkillCache } from '../../cache/SkillCache'
import { DataManager } from '../../../data/manager'
import { ExEnchantSkillInfo } from '../send/ExEnchantSkillInfo'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'

export function RequestExEnchantSkillInfo( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player || player.getLevel() < 76 ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let skillId = packet.readD(), skillLevel = packet.readD()

    let skill: Skill = SkillCache.getSkill( skillId, skillLevel )
    if ( !skill || ( skill.getId() !== skillId ) ) {
        return
    }

    if ( !DataManager.getSkillData().getSkillEnchantmentBySkillId( skillId ) ) {
        return
    }

    let playerSkillLvl = player.getSkillLevel( skillId )
    if ( ( playerSkillLvl === -1 ) || ( playerSkillLvl !== skillLevel ) ) {
        return
    }

    player.sendOwnedData( ExEnchantSkillInfo( skillId, skillLevel ) )
}