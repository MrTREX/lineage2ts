import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ActionFailed } from '../send/ActionFailed'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { AbnormalType } from '../../models/skills/AbnormalType'
import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { L2Object } from '../../models/L2Object'
import { L2World } from '../../L2World'
import { DuelState } from '../../enums/DuelState'
import { PlayerActionOverride } from '../../values/PlayerConditions'
import { ConfigManager } from '../../../config/ConfigManager'
import { ReadableClientPacketPool } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'
import { BlockedAction } from '../../enums/BlockedAction'

const packetLimiter = new FastRateLimit( {
    threshold: 2, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function ActionReceive( client: GameClient, packetData: Buffer ) : Promise<void> {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( player.inObserverMode() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.OBSERVERS_CANNOT_PARTICIPATE ) )
        player.sendOwnedData( ActionFailed() )
        return
    }

    if ( player.getActiveRequester() ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    let info : BuffInfo = player.getEffectList().getBuffInfoByAbnormalType( AbnormalType.BOT_PENALTY )
    if ( info ) {
        let isNotAllowed = info.getEffects().some( ( effect: AbstractEffect ) => {
            return effect.blocksAction( BlockedAction.Interaction )
        } )

        if ( isNotAllowed ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_HAVE_BEEN_REPORTED_SO_ACTIONS_NOT_ALLOWED ) )
            player.sendOwnedData( ActionFailed() )
            return
        }
    }

    let packet = ReadableClientPacketPool.getValue().setBuffer( packetData )
    let objectId = packet.readD()
    packet.skipD( 3 )
    let actionId = packet.readC()

    ReadableClientPacketPool.recycleValue( packet )

    let target : L2Object
    if ( player.getTargetId() === objectId ) {
        target = player.getTarget()
    } else if ( player.isInAirShip() && ( player.getAirShip().getHelmObjectId() === objectId ) ) {
        target = player.getAirShip()
    } else {
        target = L2World.getObjectById( objectId )
    }

    if ( !target ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    if ( target.isPlayable() && target.getActingPlayer().getDuelState() === DuelState.Dead ) {
        player.sendOwnedData( ActionFailed() )
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.OTHER_PARTY_IS_FROZEN ) )
        return
    }

    if ( !target.isTargetable() && !player.hasActionOverride( PlayerActionOverride.TargetBlock ) ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    if ( target.getInstanceId() !== player.getInstanceId() && player.getInstanceId() !== -1 ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    if ( !target.isVisibleFor( player ) ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    switch ( actionId ) {
        case 0:
            return target.onInteraction( player, true )

        case 1:
            if ( !player.isGM() && !( target.isNpc() && ConfigManager.customs.viewNpcEnabled() ) ) {
                return target.onInteraction( player, false )
            }

            return target.onActionShift( player )

        default:
            player.sendOwnedData( ActionFailed() )
    }
}