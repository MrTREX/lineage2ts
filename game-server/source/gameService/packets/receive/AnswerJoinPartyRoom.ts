import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { PartyMatchManager } from '../../cache/partyMatchManager'
import { PartyMatchDetail } from '../send/PartyMatchDetail'
import { ExPartyRoomMember, ExPartyRoomMemberOperation } from '../send/ExPartyRoomMember'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 2, // time-to-live value of token bucket (in seconds)
} )

export function AnswerJoinPartyRoom( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let partner: L2PcInstance = player.getActiveRequester()
    if ( !partner ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_IS_NOT_FOUND_IN_THE_GAME ) )
        player.setActiveRequester( null )
        return
    }

    let value: number = new ReadableClientPacket( packetData ).readD()

    if ( value === 1 && !partner.isRequestExpired() ) {
        if ( PartyMatchManager.addToExistingRoom( partner.getObjectId(), player ) ) {
            let room = PartyMatchManager.getPlayerRoom( player.getObjectId() )

            player.sendOwnedData( PartyMatchDetail( room ) )
            player.sendOwnedData( ExPartyRoomMember( room, ExPartyRoomMemberOperation.Add ) )
            player.broadcastUserInfo()
        } else {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_ENTER_PARTY_ROOM ) )
        }
    } else {
        partner.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.PARTY_MATCHING_REQUEST_NO_RESPONSE ) )
    }

    player.setActiveRequester( null )
    partner.onTransactionResponse()
}