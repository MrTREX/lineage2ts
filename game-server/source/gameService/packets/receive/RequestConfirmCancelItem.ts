import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { ConfigManager } from '../../../config/ConfigManager'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { CrystalType } from '../../models/items/type/CrystalType'
import { ExPutItemResultForVariationCancel } from '../send/ExPutItemResultForVariationCancel'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'

export function RequestConfirmCancelItem( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let objectId : number = new ReadableClientPacket( packetData ).readD()

    let item : L2ItemInstance = player.getInventory().getItemByObjectId( objectId )
    if ( !item ) {
        return
    }

    if ( item.getOwnerId() !== player.getObjectId() ) {
        return
    }

    if ( !item.isAugmented() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.AUGMENTATION_REMOVAL_CAN_ONLY_BE_DONE_ON_AN_AUGMENTED_ITEM ) )
        return
    }

    if ( item.isPvp() && !ConfigManager.character.allowAugmentPvPItems() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THIS_IS_NOT_A_SUITABLE_ITEM ) )
        return
    }

    let price
    switch ( item.getItem().getCrystalType() ) {
        case CrystalType.C:
            if ( item.getCrystalCount() < 1720 ) {
                price = 95000
            } else if ( item.getCrystalCount() < 2452 ) {
                price = 150000
            } else {
                price = 210000
            }
            break
        case CrystalType.B:
            if ( item.getCrystalCount() < 1746 ) {
                price = 240000
            } else {
                price = 270000
            }
            break
        case CrystalType.A:
            if ( item.getCrystalCount() < 2160 ) {
                price = 330000
            } else if ( item.getCrystalCount() < 2824 ) {
                price = 390000
            } else {
                price = 420000
            }
            break
        case CrystalType.S:
            price = 480000
            break
        case CrystalType.S80:
        case CrystalType.S84:
            price = 920000
            break

        default:
            return
    }

    player.sendOwnedData( ExPutItemResultForVariationCancel( item, price ) )
}