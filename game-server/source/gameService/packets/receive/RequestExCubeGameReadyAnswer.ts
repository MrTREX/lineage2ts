import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { BlockCheckerManager } from '../../instancemanager/BlockCheckerManager'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'

export function RequestExCubeGameReadyAnswer( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let arena = packet.readD(), answer = packet.readD()

    arena = arena + 1

    switch ( answer ) {
        case 0:
            // Cancel - Answer No
            break
        case 1:
            // OK or Time Over
            BlockCheckerManager.increaseArenaVotes( arena )
            break
    }
}