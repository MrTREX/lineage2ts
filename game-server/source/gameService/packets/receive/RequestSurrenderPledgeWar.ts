import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Clan } from '../../models/L2Clan'
import { ClanCache } from '../../cache/ClanCache'
import { ActionFailed } from '../send/ActionFailed'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestSurrenderPledgeWar( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player : L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let playerClan : L2Clan = player.getClan()
    if ( !playerClan ) {
        return
    }

    let clanName : string = new ReadableClientPacket( packetData ).readS()

    let otherClan : L2Clan = ClanCache.getClanByName( clanName )
    if ( !otherClan ) {
        player.sendMessage( `Clan with name '${clanName}' does not exist.` )
        player.sendOwnedData( ActionFailed() )
        return
    }

    if ( !playerClan.isAtWarWith( otherClan.getId() ) ) {
        player.sendMessage( `'${playerClan.getName()}' clan is not at war with '${otherClan.getName()}' clan` )
        player.sendOwnedData( ActionFailed() )
        return
    }

    let packet = new SystemMessageBuilder( SystemMessageIds.YOU_HAVE_SURRENDERED_TO_THE_S1_CLAN )
            .addString( otherClan.getName() )
            .getBuffer()
    player.sendOwnedData( packet )

    return ClanCache.deleteClanWars( playerClan.getId(), otherClan.getId() )
}