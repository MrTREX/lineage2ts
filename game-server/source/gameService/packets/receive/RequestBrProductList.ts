import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { FastRateLimit } from 'fast-ratelimit'
import { ExBrProductList } from '../send/ExBrProductList'
import { DataManager } from '../../../data/manager'
import { ConfigManager } from '../../../config/ConfigManager'

const packetLimiter = new FastRateLimit( {
    threshold: 2, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestBrProductList( client: GameClient ) : void {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player
        || !ConfigManager.character.isGamePointsEnabled()
        || !ConfigManager.character.useItemShopPoints() ) {
        return
    }

    player.sendOwnedData( ExBrProductList( DataManager.getGamePointProducts().getAll() ) )
}