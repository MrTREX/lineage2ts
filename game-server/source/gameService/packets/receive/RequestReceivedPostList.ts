import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ConfigManager } from '../../../config/ConfigManager'
import { ExShowReceivedPostList } from '../send/ExShowReceivedPostList'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestReceivedPostList( client: GameClient ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player || !ConfigManager.general.allowMail() ) {
        return
    }

    player.sendOwnedData( ExShowReceivedPostList( player.getObjectId() ) )
}