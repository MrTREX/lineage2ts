import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2World } from '../../L2World'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { L2Clan } from '../../models/L2Clan'
import { AskJoinAlly } from '../send/AskJoinAlly'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestJoinAlly( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let playerId : number = new ReadableClientPacket( packetData ).readD()

    let targetPlayer: L2PcInstance = L2World.getPlayer( playerId )
    if ( !targetPlayer ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_HAVE_INVITED_THE_WRONG_TARGET ) )
        return
    }

    let clan: L2Clan = player.getClan()
    if ( !clan ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ARE_NOT_A_CLAN_MEMBER ) )
        return
    }

    if ( !clan.checkAllyJoinCondition( player, targetPlayer ) ) {
        return
    }

    if ( !player.getRequest().setRequest( targetPlayer, this ) ) {
        return
    }

    let packet = new SystemMessageBuilder( SystemMessageIds.S2_ALLIANCE_LEADER_OF_S1_REQUESTED_ALLIANCE )
            .addString( player.getClan().getAllyName() )
            .addString( player.getName() )
            .getBuffer()
    targetPlayer.sendOwnedData( packet )
    targetPlayer.sendOwnedData( AskJoinAlly( player.getObjectId(), player.getClan().getAllyName() ) )
}