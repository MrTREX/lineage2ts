import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ClassId } from '../../models/base/ClassId'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SkillCache } from '../../cache/SkillCache'
import { Skill } from '../../models/Skill'
import { ConfigManager } from '../../../config/ConfigManager'
import { SkillItemValues } from '../../values/SkillItemValues'
import { L2EnchantSkillLearn } from '../../models/L2EnchantSkillLearn'
import { DataManager } from '../../../data/manager'
import { EnchantSkillHolder } from '../../models/L2EnchantSkillGroup'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { ExEnchantSkillResult } from '../send/ExEnchantSkillResult'
import { UserInfo } from '../send/UserInfo'
import { ExBrExtraUserInfo } from '../send/ExBrExtraUserInfo'
import { ExEnchantSkillInfo } from '../send/ExEnchantSkillInfo'
import { EnchantType, ExEnchantSkillInfoDetail } from '../send/ExEnchantSkillInfoDetail'
import { ItemTypes } from '../../values/InventoryValues'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { PlayerShortcutCache } from '../../cache/PlayerShortcutCache'
import _ from 'lodash'

export async function RequestExEnchantSkillSafe( client: GameClient, packetData: Buffer ) : Promise<void> {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( player.getLevel() < 76 ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_USE_SKILL_ENCHANT_ON_THIS_LEVEL ) )
        return
    }

    if ( ClassId.getClassIdByIdentifier( player.getClassId() ).level < 3 ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_USE_SKILL_ENCHANT_IN_THIS_CLASS ) )
        return
    }

    if ( !player.isAllowedToEnchantSkills() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_USE_SKILL_ENCHANT_ATTACKING_TRANSFORMED_BOAT ) )
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let skillId = packet.readD(), skillLevel = packet.readD()

    if ( skillId < 0 || skillLevel <= 0 ) {
        return
    }

    let skill: Skill = SkillCache.getSkill( skillId, skillLevel )
    if ( !skill ) {
        return
    }

    let costMultiplier = ConfigManager.general.getSafeEnchantCostMultipiler()
    let reqItemId = SkillItemValues.SafeEnchantBook

    let skillLearn: L2EnchantSkillLearn = DataManager.getSkillData().getSkillEnchantmentBySkillId( skillId )
    if ( !skillLearn ) {
        return
    }

    let skillHolder: EnchantSkillHolder = skillLearn.getEnchantSkillHolder( skillLevel )
    let beforeEnchantSkillLevel = player.getSkillLevel( skillId )
    if ( beforeEnchantSkillLevel !== skillLearn.getMinSkillLevel( skillLevel ) ) {
        return
    }

    let requiredSp = skillHolder.getSpCost() * costMultiplier
    let totalCost = skillHolder.getAdenaCost() * costMultiplier
    let rate = skillHolder.getRate( player )

    if ( player.getSp() >= requiredSp ) {
        let spellBook: L2ItemInstance = player.getInventory().getItemByItemId( reqItemId )
        if ( !spellBook ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_DONT_HAVE_ALL_OF_THE_ITEMS_NEEDED_TO_ENCHANT_THAT_SKILL ) )
            return
        }

        if ( player.getInventory().getAdenaAmount() < totalCost ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_DONT_HAVE_ALL_OF_THE_ITEMS_NEEDED_TO_ENCHANT_THAT_SKILL ) )
            return
        }

        let isSuccess = player.removeSp( requiredSp )
                && await player.destroyItemByObjectId( spellBook.getObjectId(), 1, true, 'RequestExEnchantSkillSafe' )
                && await player.destroyItemByItemId( ItemTypes.Adena, totalCost, true, 'RequestExEnchantSkillSafe' )

        if ( !isSuccess ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_DONT_HAVE_ALL_OF_THE_ITEMS_NEEDED_TO_ENCHANT_THAT_SKILL ) )
            return
        }


        if ( _.random( 100 ) <= rate ) {
            await player.addSkill( skill, true )
            player.sendOwnedData( ExEnchantSkillResult( true ) )

            let packet = new SystemMessageBuilder( SystemMessageIds.YOU_HAVE_SUCCEEDED_IN_ENCHANTING_THE_SKILL_S1 )
                    .addSkillNameById( skillId )
                    .getBuffer()
            player.sendOwnedData( packet )
        } else {
            player.sendOwnedData( ExEnchantSkillResult( false ) )

            let packet = new SystemMessageBuilder( SystemMessageIds.SKILL_ENCHANT_FAILED_S1_LEVEL_WILL_REMAIN )
                    .addSkillNameById( skillId )
                    .getBuffer()
            player.sendOwnedData( packet )
        }

        player.sendDebouncedPacket( UserInfo )
        player.sendDebouncedPacket( ExBrExtraUserInfo )
        player.sendSkillList()

        let afterEnchantSkillLevel = player.getSkillLevel( skillId )
        player.sendOwnedData( ExEnchantSkillInfo( skillId, afterEnchantSkillLevel ) )
        player.sendOwnedData( ExEnchantSkillInfoDetail( EnchantType.Safe, skillId, afterEnchantSkillLevel + 1, player ) )
        return PlayerShortcutCache.updateSkillShortcut( player, skillId, afterEnchantSkillLevel )
    }

    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_DONT_HAVE_ENOUGH_SP_TO_ENCHANT_THAT_SKILL ) )
}