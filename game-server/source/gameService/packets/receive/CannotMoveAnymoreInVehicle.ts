import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { Location } from '../../models/Location'
import { StopMoveInVehicle } from '../send/StopMoveInVehicle'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 2, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function CannotMoveAnymoreInVehicle( client: GameClient, packetData: Buffer ) : void {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player || !player.isInBoat() ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let boatId = packet.readD()

    if ( player.getBoat().getObjectId() === boatId ) {
        let x = packet.readD(), y = packet.readD(), z = packet.readD(), heading = packet.readD()
        player.setInVehiclePosition( new Location( x, y, z ) )
        player.setHeading( heading )
        return BroadcastHelper.dataToSelfBasedOnVisibility( player, StopMoveInVehicle( player, boatId ) )
    }
}