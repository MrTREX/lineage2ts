import { GameClient } from '../../GameClient'
import { FastRateLimit } from 'fast-ratelimit'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ExBrGamePoint } from '../send/ExBrGamePoint'
import { GamePointsCache, GamePointType } from '../../cache/GamePointsCache'
import { ConfigManager } from '../../../config/ConfigManager'

const packetLimiter = new FastRateLimit( {
    threshold: 2, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestBrGamePoint( client: GameClient ) : void {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( !ConfigManager.character.isGamePointsEnabled() || !ConfigManager.character.useItemShopPoints() ) {
        return
    }

    let shopPoints = GamePointsCache.getPoints( player.getAccountName(), GamePointType.ItemShop )
    player.sendOwnedData( ExBrGamePoint( player.getObjectId(), shopPoints ) )
}