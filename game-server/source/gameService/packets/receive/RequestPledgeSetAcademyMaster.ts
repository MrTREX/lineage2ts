import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Clan } from '../../models/L2Clan'
import { L2ClanMember } from '../../models/L2ClanMember'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { L2ClanValues } from '../../values/L2ClanValues'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { ClanPrivilege } from '../../enums/ClanPriviledge'

export async function RequestPledgeSetAcademyMaster( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let clan : L2Clan = player.getClan()
    if ( !clan ) {
        return
    }

    if ( !player.hasClanPrivilege( ClanPrivilege.InviteApprentice ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_DO_NOT_HAVE_THE_RIGHT_TO_DISMISS_AN_APPRENTICE ) )
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let type = packet.readD(), playerName = packet.readS(), targetPlayerName = packet.readS()

    let currentMember : L2ClanMember = clan.getClanMemberByName( playerName as string )
    let targetMember : L2ClanMember = clan.getClanMemberByName( targetPlayerName as string )
    if ( !currentMember || !targetMember ) {
        return
    }

    let apprenticeMember : L2ClanMember
    let sponsorMember : L2ClanMember

    if ( currentMember.getPledgeType() === L2ClanValues.SUBUNIT_ACADEMY ) {
        apprenticeMember = currentMember
        sponsorMember = targetMember
    } else {
        apprenticeMember = targetMember
        sponsorMember = currentMember
    }

    let apprentice : L2PcInstance = apprenticeMember.getPlayerInstance()
    let sponsor : L2PcInstance = sponsorMember.getPlayerInstance()

    if ( !apprentice || !sponsor ) {
        return
    }

    let messageId: number
    if ( type === 0 ) {
        if ( apprentice ) {
            apprentice.setSponsor( 0 )
        } else {
            apprenticeMember.setApprenticeAndSponsor( 0, 0 )
        }

        if ( sponsor ) {
            sponsor.setApprentice( 0 )
        } else {
            sponsorMember.setApprenticeAndSponsor( 0, 0 )
        }

        await apprenticeMember.saveApprenticeAndSponsor( 0, 0 )
        await sponsorMember.saveApprenticeAndSponsor( 0, 0 )

        messageId = SystemMessageIds.S2_CLAN_MEMBER_C1_APPRENTICE_HAS_BEEN_REMOVED
    } else {
        if ( ( apprenticeMember.getSponsor() !== 0 )
                || ( sponsorMember.getApprentice() !== 0 )
                || ( apprenticeMember.getApprentice() !== 0 )
                || ( sponsorMember.getSponsor() !== 0 ) ) {
            player.sendMessage( 'Remove previous connections first.' )
            return
        }

        if ( apprentice ) {
            apprentice.setSponsor( sponsorMember.getObjectId() )
        } else {
            apprenticeMember.setApprenticeAndSponsor( 0, sponsorMember.getObjectId() )
        }

        if ( sponsor ) {
            sponsor.setApprentice( apprenticeMember.getObjectId() )
        } else {
            sponsorMember.setApprenticeAndSponsor( apprenticeMember.getObjectId(), 0 )
        }

        await apprenticeMember.saveApprenticeAndSponsor( 0, sponsorMember.getObjectId() )
        await sponsorMember.saveApprenticeAndSponsor( apprenticeMember.getObjectId(), 0 )

        messageId = SystemMessageIds.S2_HAS_BEEN_DESIGNATED_AS_APPRENTICE_OF_CLAN_MEMBER_S1
    }

    let message : Buffer = new SystemMessageBuilder( messageId )
            .addString( sponsorMember.getName() )
            .addString( apprenticeMember.getName() )
            .getBuffer()

    if ( sponsor.getObjectId() !== player.getObjectId()
            && sponsor.getObjectId() !== apprentice.getObjectId() ) {
        player.sendCopyData( message )
    }

    sponsor.sendCopyData( message )
    apprentice.sendCopyData( message )
}