import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Clan } from '../../models/L2Clan'
import { L2World } from '../../L2World'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { AskJoinPledge } from '../send/AskJoinPledge'
import { L2RequestData } from '../../models/L2Request'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestJoinPledge( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let clan : L2Clan = player.getClan()
    if ( !clan ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let targetId = packet.readD() , pledgeType = packet.readD()
    let target : L2PcInstance = L2World.getPlayer( targetId )
    if ( !target ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_HAVE_INVITED_THE_WRONG_TARGET ) )
        return
    }

    if ( !clan.checkClanJoinCondition( player, target, pledgeType ) ) {
        return
    }

    let data : L2RequestData = {
        type: RequestJoinPledge,
        parameters: {
            targetId,
            pledgeType
        }
    }

    if ( !player.getRequest().setRequest( target, data ) ) {
        return
    }

    let pledgeName = player.getClan().getName()
    let pledge = player.getClan().getPledge( pledgeType )
    let name = pledge ? pledge.name : ''
    target.sendOwnedData( AskJoinPledge( player.getObjectId(), name, pledgeType, pledgeName ) )
}