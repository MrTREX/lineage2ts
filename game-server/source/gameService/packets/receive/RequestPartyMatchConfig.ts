import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { ActionFailed } from '../send/ActionFailed'
import { PartyMatchManager, PartyMatchRoom } from '../../cache/partyMatchManager'
import { PartyMatchDetail } from '../send/PartyMatchDetail'
import { ExPartyRoomMember, ExPartyRoomMemberOperation } from '../send/ExPartyRoomMember'
import { PartyMatchWaitingList } from '../../cache/PartyMatchWaitingList'
import { ListPartyWaiting } from '../send/ListPartyWaiting'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 3, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestPartyMatchConfig( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let room : PartyMatchRoom = PartyMatchManager.getPlayerRoom( player.getObjectId() )
    if ( !room && player.getParty() && player.getParty().getLeaderObjectId() !== player.getObjectId() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_VIEW_PARTY_ROOMS ) )
        player.sendOwnedData( ActionFailed() )
        return
    }

    if ( room ) {
        player.sendOwnedData( PartyMatchDetail( room ) )
        player.sendOwnedData( ExPartyRoomMember( room, ExPartyRoomMemberOperation.View ) )

        player.broadcastUserInfo()
        return
    }

    PartyMatchWaitingList.addPlayer( player )

    let packet = new ReadableClientPacket( packetData )
    packet.skipD( 1 )
    let locationId = packet.readD(), limit = packet.readD()

    player.sendOwnedData( ListPartyWaiting( locationId, limit === 0, player.getLevel() ) )
}