import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { HeroCache } from '../../cache/HeroCache'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestWriteHeroWords( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player || !player.isHero() ) {
        return
    }

    if ( packetData.length > 301 ) {
        return
    }

    let message : string = new ReadableClientPacket( packetData ).readS()

    if ( message.length === 0 ) {
        return
    }

    HeroCache.setHeroMessage( player.getObjectId(), message )
}