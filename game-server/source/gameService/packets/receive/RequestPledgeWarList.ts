import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Clan } from '../../models/L2Clan'
import { PledgeReceiveStatus, PledgeReceiveWarList } from '../send/PledgeReceiveWarList'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'

export function RequestPledgeWarList( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let clan: L2Clan = player.getClan()
    if ( !clan ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    packet.skipD( 1 )
    let value: number = packet.readD()

    player.sendOwnedData( PledgeReceiveWarList( clan, value as PledgeReceiveStatus ) )
}