import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { FastRateLimit } from 'fast-ratelimit'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestExChangeName( client: GameClient, packetData: Buffer ) : void {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )

    // TODO : integrate variables
    let type = packet.readD()
    let name = packet.readS()
    let characterSlot = packet.readD()

    if ( player.isGM() ) {
        player.sendMessage( `Received packet ${RequestExChangeName.name}, (type, name, slot) (${type}, ${name}, ${characterSlot})` )
    }
}