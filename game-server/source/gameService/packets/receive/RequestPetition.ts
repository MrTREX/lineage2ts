import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { PetitionManager } from '../../instancemanager/PetitionManager'
import { ConfigManager } from '../../../config/ConfigManager'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { AdminManager } from '../../cache/AdminManager'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestPetition( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( !AdminManager.isGMOnline( false ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NO_GM_PROVIDING_SERVICE_NOW ) )
        return
    }

    if ( !PetitionManager.isPetitioningAllowed() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.GAME_CLIENT_UNABLE_TO_CONNECT_TO_PETITION_SERVER ) )
        return
    }

    if ( PetitionManager.isPlayerPetitionPending( player ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ONLY_ONE_ACTIVE_PETITION_AT_TIME ) )
        return
    }

    if ( PetitionManager.getPendingPetitionCount() === ConfigManager.character.getMaxPetitionsPending() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.PETITION_SYSTEM_CURRENT_UNAVAILABLE ) )
        return
    }

    let totalPetitions = PetitionManager.getPlayerTotalPetitionCount( player ) + 1

    if ( totalPetitions > ConfigManager.character.getMaxPetitionsPerPlayer() ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.WE_HAVE_RECEIVED_S1_PETITIONS_TODAY )
                .addNumber( totalPetitions )
                .getBuffer()
        player.sendOwnedData( packet )
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let message = packet.readS(), type = packet.readD()

    if ( message.length > 255 ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.PETITION_MAX_CHARS_255 ) )
        return
    }

    let petitionId = PetitionManager.submitPetition( player, message, type )

    let acceptedStatus = new SystemMessageBuilder( SystemMessageIds.PETITION_ACCEPTED_RECENT_NO_S1 )
            .addNumber( petitionId )
            .getBuffer()
    player.sendOwnedData( acceptedStatus )

    let countStatus = new SystemMessageBuilder( SystemMessageIds.SUBMITTED_YOU_S1_TH_PETITION_S2_LEFT )
            .addNumber( totalPetitions )
            .addNumber( ConfigManager.character.getMaxPetitionsPerPlayer() - totalPetitions )
            .getBuffer()
    player.sendOwnedData( countStatus )

    let waitingStatus = new SystemMessageBuilder( SystemMessageIds.S1_PETITION_ON_WAITING_LIST )
            .addNumber( PetitionManager.getPendingPetitionCount() )
            .getBuffer()
    player.sendOwnedData( waitingStatus )
}