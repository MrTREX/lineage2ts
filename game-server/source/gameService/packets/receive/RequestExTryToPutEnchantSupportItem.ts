import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { L2PcInstanceValues } from '../../values/L2PcInstanceValues'
import { EnchantScroll } from '../../models/items/enchant/EnchantScroll'
import { EnchantSupportItem } from '../../models/items/enchant/EnchantSupportItem'
import { DataManager } from '../../../data/manager'
import { ExPutEnchantSupportItemResult } from '../send/ExPutEnchantSupportItemResult'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'

export function RequestExTryToPutEnchantSupportItem( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player || !player.isEnchanting() ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let supportObjectId = packet.readD(), enchantObjectId = packet.readD()

    let item : L2ItemInstance = player.getInventory().getItemByObjectId( enchantObjectId )
    let scroll : L2ItemInstance = player.getInventory().getItemByObjectId( player.getActiveEnchantItemId() )
    let support : L2ItemInstance = player.getInventory().getItemByObjectId( supportObjectId )

    if ( !item || !scroll || !support ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INAPPROPRIATE_ENCHANT_CONDITION ) )
        player.setActiveEnchantSupportItemId( L2PcInstanceValues.EmptyId )
        return
    }

    let scrollTemplate : EnchantScroll = DataManager.getEnchantItemData().getScrollById( scroll.getId() )
    let supportTemplate : EnchantSupportItem = DataManager.getEnchantItemData().getSupportItemById( support.getId() )

    if ( !scrollTemplate || !supportTemplate || !scrollTemplate.isValid( item, supportTemplate ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INAPPROPRIATE_ENCHANT_CONDITION ) )
        player.setActiveEnchantSupportItemId( L2PcInstanceValues.EmptyId )
        player.sendOwnedData( ExPutEnchantSupportItemResult( 0 ) )
        return
    }

    player.setActiveEnchantSupportItemId( support.getObjectId() )
    player.sendOwnedData( ExPutEnchantSupportItemResult( supportObjectId ) )
}