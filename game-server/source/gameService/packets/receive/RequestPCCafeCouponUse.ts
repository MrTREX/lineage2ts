import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { FastRateLimit } from 'fast-ratelimit'
import { ReadableClientPacketPool } from '../../../packets/ReadableClientPacket'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestPCCafeCouponUse( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let packet = ReadableClientPacketPool.getValue().setBuffer( packetData )
    let data : string = packet.readS()
    ReadableClientPacketPool.recycleValue( packet )

    // TODO : find out what data is coming in
    if ( player.isGM() ) {
        player.sendMessage( `Packet:RequestPCCafeCouponUse data=${data}` )
    }
}