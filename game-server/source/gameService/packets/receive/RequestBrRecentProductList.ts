import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { FastRateLimit } from 'fast-ratelimit'
import { ExBrRecentProductList } from '../send/ExBrProductList'
import { ConfigManager } from '../../../config/ConfigManager'
import { GamePointsCache, GamePointType } from '../../cache/GamePointsCache'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestBrRecentProductList( client: GameClient ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player
        || !ConfigManager.character.isGamePointsEnabled()
        || !ConfigManager.character.useItemShopPoints() ) {
        return
    }

    player.sendOwnedData( ExBrRecentProductList( GamePointsCache.getPurchases( player.getAccountName(), GamePointType.ItemShop ) ) )
}