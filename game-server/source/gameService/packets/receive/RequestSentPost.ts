import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ConfigManager } from '../../../config/ConfigManager'
import { MailMessage } from '../../models/mail/MailMessage'
import { MailManager } from '../../instancemanager/MailManager'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { ExReplySentPost } from '../send/ExReplySentPost'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'
import { AreaType } from '../../models/areas/AreaType'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestSentPost( client: GameClient, packetData: Buffer ) : void {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player || !ConfigManager.general.allowMail() ) {
        return
    }

    let messageId : number = new ReadableClientPacket( packetData ).readD()

    let message : MailMessage = MailManager.getMessage( messageId )
    if ( !message ) {
        return
    }

    if ( !player.isInArea( AreaType.Peace ) && message.hasAttachments() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_USE_MAIL_OUTSIDE_PEACE_ZONE ) )
        return
    }

    if ( message.getSenderId() !== player.getObjectId() ) {
        // TODO : violation fo accessing other player's mail
        return
    }

    player.sendOwnedData( ExReplySentPost( message ) )
}