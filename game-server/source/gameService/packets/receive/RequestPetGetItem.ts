import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { L2World } from '../../L2World'
import { ActionFailed } from '../send/ActionFailed'
import { FortSiegeManager } from '../../instancemanager/FortSiegeManager'
import { L2PetInstance } from '../../models/actor/instance/L2PetInstance'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { MercenaryTicketManager } from '../../instancemanager/MercenaryTicketManager'
import { ReadableClientPacketPool } from '../../../packets/ReadableClientPacket'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestPetGetItem( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player || !player.hasPet() ) {
        return
    }

    let packet = ReadableClientPacketPool.getValue().setBuffer( packetData )
    let objectId : number = packet.readD()
    ReadableClientPacketPool.recycleValue( packet )

    let item : L2ItemInstance = L2World.getObjectById( objectId ) as L2ItemInstance
    if ( !item || !item.isItem() ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    let castleId = MercenaryTicketManager.getTicketCastleId( item.getId() )
    if ( castleId > 0 ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    if ( FortSiegeManager.isCombatFlag( item.getId() ) ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    let pet : L2PetInstance = player.getSummon() as L2PetInstance
    if ( pet.isDead() || pet.isOutOfControl() ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    if ( pet.isUncontrollable() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.WHEN_YOUR_PETS_HUNGER_GAUGE_IS_AT_0_YOU_CANNOT_USE_YOUR_PET ) )
        return
    }

    return AIEffectHelper.notifyInteractWithTarget( pet, item.getObjectId(), true )
}