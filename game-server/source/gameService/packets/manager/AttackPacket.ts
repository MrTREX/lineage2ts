import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ActionFailed } from '../send/ActionFailed'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { AbnormalType } from '../../models/skills/AbnormalType'
import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { L2Object } from '../../models/L2Object'
import { L2World } from '../../L2World'
import { PlayerActionOverride } from '../../values/PlayerConditions'
import { PrivateStoreType } from '../../enums/PrivateStoreType'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { BlockedAction } from '../../enums/BlockedAction'

export async function AttackPacket( client: GameClient, packetData: Buffer ) : Promise<void> {
    let player : L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( player.isInBoat() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ALLOWED_ON_BOAT ) )
        player.sendOwnedData( ActionFailed() )
        return
    }

    let info : BuffInfo = player.getEffectList().getBuffInfoByAbnormalType( AbnormalType.BOT_PENALTY )
    if ( info && !player.hasActionOverride( PlayerActionOverride.BlockedActionPenalty ) ) {
        let shouldStop : boolean = info.getEffects().some( ( effect: AbstractEffect ) => {
            return effect.blocksAction( BlockedAction.Attack )
        } )

        if ( shouldStop ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_HAVE_BEEN_REPORTED_SO_ACTIONS_NOT_ALLOWED ) )
            player.sendOwnedData( ActionFailed() )
            return
        }
    }

    let targetId = new ReadableClientPacket( packetData ).readD()

    let target : L2Object
    if ( player.getTargetId() === targetId ) {
        target = player.getTarget()
    } else {
        target = L2World.getObjectById( targetId )
    }

    if ( !target ) {
        return
    }

    if ( !target.isTargetable() && !player.hasActionOverride( PlayerActionOverride.TargetBlock ) ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    if ( target.getInstanceId() !== player.getInstanceId() ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    if ( !target.isVisibleFor( player ) ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    if ( player.getTargetId() !== targetId ) {
        return target.onInteraction( player, true )
    }

    if ( target.getObjectId() !== player.getObjectId()
            && player.getPrivateStoreType() === PrivateStoreType.None
            && !player.getActiveRequester() ) {
        return target.onForcedAttack( player )
    }

    player.sendOwnedData( ActionFailed() )
}