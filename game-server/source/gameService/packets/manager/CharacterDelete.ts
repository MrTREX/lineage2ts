import { GameClient } from '../../GameClient'
import { CharacterDeleteSuccess } from '../send/CharacterDeleteSuccess'
import { CharacterDeleteFail, CharacterDeleteReasons } from '../send/CharacteDeleteFail'
import { EventType, PlayerDeletedEvent } from '../../models/events/EventType'
import { ListenerCache } from '../../cache/ListenerCache'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 3, // time-to-live value of token bucket (in seconds)
} )

export enum CharacterDeleteOutcome {
    Success,
    ClanMemberFailure,
    ClanLeaderFailure,
    GeneralFailure
}

export async function CharacterDelete( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return client.sendPacket( CharacterDeleteFail( CharacterDeleteReasons.GeneralFailure ) )
    }

    let characterSlot: number = new ReadableClientPacket( packetData ).readD()
    let outcome : CharacterDeleteOutcome = await client.markCharacterAsDeleted( characterSlot )

    switch ( outcome ) {
        case CharacterDeleteOutcome.Success:
            client.sendPacket( CharacterDeleteSuccess() )

                if ( ListenerCache.hasGeneralListener( EventType.PlayerDeleted ) ) {
                    let characterInfo = client.getCharacterSelection( characterSlot )
                    let data: PlayerDeletedEvent = {
                        accessLevel: characterInfo.accessLevel,
                        level: characterInfo.level,
                        sex: characterInfo.sex,
                        classId: characterInfo.classId,
                        playerId: characterInfo.objectId,
                        playerName: characterInfo.name,
                        slot: characterSlot
                    }

                    await ListenerCache.sendGeneralEvent( EventType.PlayerDeleted, data )
                }
            break

        case CharacterDeleteOutcome.ClanMemberFailure:
            client.sendPacket( CharacterDeleteFail( CharacterDeleteReasons.MayNotDeleteClanMember ) )
            break

        case CharacterDeleteOutcome.ClanLeaderFailure:
            client.sendPacket( CharacterDeleteFail( CharacterDeleteReasons.ClanLeaderMayNotBeDeleted ) )
            break

        case CharacterDeleteOutcome.GeneralFailure:
        default:
            client.sendPacket( CharacterDeleteFail( CharacterDeleteReasons.GeneralFailure ) )
            break
    }

    await client.updateCharacterSelection()
    client.sendCharacterSelection()
}