import { GameClient } from '../../GameClient'
import { CharacterInfoPackage } from '../../models/CharacterInfoPackage'
import { ServerClose } from '../send/ServerClose'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { CharacterSelected } from '../send/CharacterSelected'
import { SSQInfo } from '../send/SSQInfo'
import { CharacterNamesCache } from '../../cache/CharacterNamesCache'
import { EventTerminationResult, EventType, PlayerSelectCharacterEvent } from '../../models/events/EventType'
import { ListenerCache } from '../../cache/ListenerCache'
import { PunishmentManager } from '../../instancemanager/PunishmentManager'
import { PunishmentEffect } from '../../models/punishment/PunishmentEffect'
import { PunishmentType } from '../../models/punishment/PunishmentType'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { EventPoolCache } from '../../cache/EventPoolCache'
import { FastRateLimit } from 'fast-ratelimit'
import { GameClientState } from '../../enums/GameClientState'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 15, // time-to-live value of token bucket (in seconds)
} )

export async function CharacterSelect( client: GameClient, packetData: Buffer ): Promise<void> {
    let characterSlot = new ReadableClientPacket( packetData ).readD()

    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    // TODO : secondary authentication

    if ( client.player ) {
        return
    }

    client.setPlayer( null )
    let character: CharacterInfoPackage = client.getCharacterSelection( characterSlot )
    if ( !character ) {
        return
    }

    if ( PunishmentManager.hasPunishment( character.objectId, PunishmentEffect.CHARACTER, PunishmentType.BAN )
            || PunishmentManager.hasPunishment( client.getAccountName(), PunishmentEffect.ACCOUNT, PunishmentType.BAN )
            || PunishmentManager.hasPunishment( client.ipAddress, PunishmentEffect.IP, PunishmentType.BAN ) ) {
        client.closeConnection( ServerClose() )
        return
    }

    if ( character.accessLevel < 0 ) {
        return client.closeConnection( ServerClose() )
    }

    // TODO: check for dualbox restrictions

    let player: L2PcInstance = await client.loadCharacterInstance( characterSlot )
    if ( !player ) {
        return
    }

    CharacterNamesCache.addPlayer( player )

    if ( ListenerCache.hasGeneralListener( EventType.PlayerSelectCharacter ) ) {
        let eventData = EventPoolCache.getData( EventType.PlayerSelectCharacter ) as PlayerSelectCharacterEvent

        eventData.slot = characterSlot
        eventData.playerId = player.getObjectId()
        eventData.accountName = client.accountName

        let result: EventTerminationResult = await ListenerCache.getGeneralTerminatedResult( EventType.PlayerSelectCharacter, eventData )
        if ( result && result.terminate ) {
            await player.deleteMe()
            return client.closeConnection( ServerClose() )
        }
    }

    client.setPlayer( player )
    await client.setOnlineStatus( true, true )

    player.sendOwnedData( SSQInfo() )
    client.setState( GameClientState.SelectingCharacter )
    player.sendOwnedData( CharacterSelected( player, client.getSessionId().playOk1 ) )
}