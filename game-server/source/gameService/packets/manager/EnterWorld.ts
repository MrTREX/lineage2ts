import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ConfigManager } from '../../../config/ConfigManager'
import { InstanceManager } from '../../instancemanager/InstanceManager'
import { DataManager } from '../../../data/manager'
import { L2Clan } from '../../models/L2Clan'
import { PacketDispatcher } from '../../PacketDispatcher'
import { PledgeSkillList } from '../send/PledgeSkillList'
import { ClanHallManager } from '../../instancemanager/ClanHallManager'
import { AuctionableHall } from '../../models/entity/clanhall/AuctionableHall'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { SiegeManager } from '../../instancemanager/SiegeManager'
import { Siege } from '../../models/entity/Siege'
import { FortSiegeManager } from '../../instancemanager/FortSiegeManager'
import { FortSiege } from '../../models/entity/FortSiege'
import { ClanHallSiegeManager } from '../../instancemanager/ClanHallSiegeManager'
import { SiegableHall } from '../../models/entity/clanhall/SiegableHall'
import { CastleManager } from '../../instancemanager/CastleManager'
import { FortManager } from '../../instancemanager/FortManager'
import { PledgeShowMemberListUpdate } from '../send/PledgeShowMemberListUpdate'
import { L2World } from '../../L2World'
import { TerritoryWarManager } from '../../instancemanager/TerritoryWarManager'
import { SevenSigns } from '../../directives/SevenSigns'
import { CursedWeaponManager } from '../../instancemanager/CursedWeaponManager'
import { PledgeShowMemberListAll } from '../send/PledgeShowMemberListAll'
import { PledgeStatusChanged } from '../send/PledgeStatusChanged'
import { ShortCutInit } from '../send/ShortCutInit'
import { ExGetBookMarkInfoPacketWithPlayer } from '../send/ExGetBookMarkInfoPacket'
import { ExBasicActionList } from '../send/ExBasicActionList'
import { HennaInfo } from '../send/HennaInfo'
import { QuestList } from '../send/QuestList'
import { ExStorageMaxCount } from '../send/ExStorageMaxCount'
import { FriendList } from '../send/FriendList'
import { PetitionManager } from '../../instancemanager/PetitionManager'
import { Die } from '../send/Die'
import { AnnouncementManager } from '../../models/announce/AnnouncementManager'
import { SkillCoolTime } from '../send/SkillCoolTime'
import { ExVoteSystemInfoWithPlayer } from '../send/ExVoteSystemInfo'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { ExShowContactList } from '../send/ExShowContactList'
import { Fort } from '../../models/entity/Fort'
import { TeleportWhereType } from '../../enums/TeleportWhereType'
import { PlayerActionOverride } from '../../values/PlayerConditions'
import { MailManager } from '../../instancemanager/MailManager'
import { ExNoticePostArrived } from '../send/ExNoticePostArrived'
import { TvTEvent } from '../../models/entity/TvTEvent'
import { ActionFailed } from '../send/ActionFailed'
import { ExNotifyDimensionalItem } from '../send/ExNotifyPremiumItem'
import { ExShowScreenMessageFromText } from '../send/ExShowScreenMessage'
import { SkillCache } from '../../cache/SkillCache'
import { NpcHtmlMessagePath } from '../send/NpcHtmlMessage'
import { AdminManager } from '../../cache/AdminManager'
import { ValidateLocation } from '../send/ValidateLocation'
import { SevenSignsSide } from '../../values/SevenSignsValues'
import { ExRotation } from '../send/ExRotation'
import { QuestStateCache } from '../../cache/QuestStateCache'
import { EventType, PlayerLoginEvent } from '../../models/events/EventType'
import { ListenerCache } from '../../cache/ListenerCache'
import { NevitManager } from '../../cache/NevitManager'
import { DimensionalTransferManager } from '../../cache/DimensionalTransferManager'
import { ExUISetting } from '../send/ExUISetting'
import { PlayerUICache } from '../../cache/PlayerUICache'
import { PlayerMacrosCache } from '../../cache/PlayerMacrosCache'
import { CharacterSummonCache } from '../../cache/CharacterSummonCache'
import _ from 'lodash'
import { EventPoolCache } from '../../cache/EventPoolCache'
import { PlayerLoadState } from '../../enums/PlayerLoadState'
import aigle from 'aigle'
import { SiegeRole } from '../../enums/SiegeRole'
import { getSlotFromItem } from '../../models/itemcontainer/ItemSlotHelper'
import { CharacterNamesCache } from '../../cache/CharacterNamesCache'
import { FastRateLimit } from 'fast-ratelimit'
import { PlayerRelationStatus } from '../../cache/PlayerRelationStatusCache'
import { GameClientState } from '../../enums/GameClientState'
import { PrivateStoreType } from '../../enums/PrivateStoreType'
import { PlayerFriendsCache } from '../../cache/PlayerFriendsCache'
import { ServerLog } from '../../../logger/Logger'
import { GamePointsCache, GamePointType } from '../../cache/GamePointsCache'
import { ExPCCafePointInfo } from '../send/ExPCCafePointInfo'
import { ExBrGamePoint } from '../send/ExBrGamePoint'

const minimumHpForDeath = 0.5
const combatFlagItemId = 9819

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 15, // time-to-live value of token bucket (in seconds)
} )

export async function EnterWorld( client: GameClient ): Promise<void> {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return client.abortConnection()
    }

    const currentTime = Date.now()

    let instanceId = InstanceManager.getInstanceIdForPlayer( player.getObjectId() )
    if ( ConfigManager.general.restorePlayerInstance() ) {
        player.setInstanceId( instanceId )
    } else {
        if ( instanceId > 0 ) {
            InstanceManager.getInstance( instanceId ).removePlayer( player.getObjectId() )
        }
    }

    client.setState( GameClientState.Playing )

    if ( player.isGM() ) {
        await applyGMAttributes( player )
    }

    if ( player.getCurrentHp() < minimumHpForDeath ) {
        player.setIsDead( true )
    }

    await SevenSigns.onLogin( player )
    await QuestStateCache.loadForPlayer( player.getObjectId() )
    await player.initializeRecommendationBonus()

    if ( ConfigManager.character.getPlayerSpawnProtection() > 0 ) {
        player.setProtection( true )
    }

    player.spawnMe( player.getX(), player.getY(), player.getZ() )

    await player.getInventory().applyItemSkills()

    if ( player.isCursedWeaponEquipped() ) {
        await CursedWeaponManager.getCursedWeapon( player.getCursedWeaponEquippedId() ).cursedOnLogin()
    }

    await PlayerFriendsCache.loadPlayer( player.getObjectId(), player.getName() )
    let friendIds = PlayerFriendsCache.getFriends( player.getObjectId() )
    if ( friendIds.size > 0 ) {
        await CharacterNamesCache.prefetchNames( Array.from( friendIds ) )
    }

    if ( player.isAlikeDead() ) {
        player.sendOwnedData( Die( player ) )
    }

    player.startWarnUserToTakeBreak()
    player.revalidateZone( true )
    await player.delevelPlayerSkills()

    player.recalculateHennaStats()
    await player.initializeWarehouse()

    let combatFlag: L2ItemInstance = player.getInventory().getItemByItemId( combatFlagItemId )
    if ( combatFlag ) {
        let fort: Fort = FortManager.getFort( player )
        if ( fort ) {
            FortSiegeManager.dropCombatFlag( player, fort.getResidenceId() )
        } else {
            let slot = getSlotFromItem( combatFlag )
            await player.getInventory().unEquipItemInBodySlot( slot )
            await player.destroyItem( combatFlag, true, 'EnterWorld' )
        }
    }

    if ( !player.hasActionOverride( PlayerActionOverride.ZoneRequirements )
            && player.isInSiegeArea()
            && ( !player.isInSiege() || ( player.getSiegeRole() !== SiegeRole.Defender ) ) ) {
        await player.teleportToLocationType( TeleportWhereType.TOWN )
    }

    TvTEvent.onLogin( player )

    PlayerRelationStatus.computeRelationStatus( player )

    /*
        Game client packets are performed in specific order.
        All text notifications or messages are performed after this section.
     */
    player.runBroadcastUserInfo()
    player.queryGameGuard()
    player.sendInventoryUpdate( false )

    player.sendOwnedData( ExGetBookMarkInfoPacketWithPlayer( player ) )
    player.sendOwnedData( ExBasicActionList() )
    player.runSendSkillList()

    player.sendOwnedData( HennaInfo( player ) )
    player.sendOwnedData( QuestList( player.getObjectId() ) )

    player.updateEffectIcons()
    player.sendOwnedData( ExStorageMaxCount( player ) )

    player.sendDebouncedPacket( SkillCoolTime )
    player.sendOwnedData( ExVoteSystemInfoWithPlayer( player ) )

    await NevitManager.onPlayerLogin( player )
    await PlayerMacrosCache.loadPlayer( player.getObjectId() )

    /*
        Needs skills and items to be loaded in client to display all relevant shortcuts properly.
        TODO : it is possible for skills to arrive later due to debounced action
     */
    player.sendOwnedData( ShortCutInit( player ) )
    player.sendOwnedData( FriendList( player ) )
    player.sendOwnedData( ExShowContactList( player.getObjectId() ) )

    if ( PlayerUICache.hasUIData( player.getObjectId() ) ) {
        player.sendOwnedData( ExUISetting( player.getObjectId() ) )
    }

    await MailManager.loadPlayer( player.getObjectId() )
    if ( MailManager.hasUnreadMessages( player.getObjectId() ) ) {
        player.sendOwnedData( ExNoticePostArrived( false ) )
    }

    if ( DimensionalTransferManager.hasItems( player.getObjectId() ) ) {
        player.sendOwnedData( ExNotifyDimensionalItem() )
    }

    player.sendOwnedData( ExRotation( player.getObjectId(), player.getHeading() ) )
    player.sendOwnedData( ValidateLocation( player ) )
    player.sendOwnedData( ActionFailed() )

    /*
        Section for sending text messages/notifications
     */

    if ( friendIds.size > 0 ) {
        let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.FRIEND_S1_HAS_LOGGED_IN )
            .addCharacterName( player )
            .getBuffer()

        friendIds.forEach( ( playerId: number ) => {
            PacketDispatcher.sendCopyData( playerId, packet )
        } )
    }

    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.WELCOME_TO_LINEAGE ) )
    player.sendMessage( 'This server uses lineage2ts codebase.' )

    SevenSigns.sendCurrentPeriodMesssage( player )
    AnnouncementManager.showAnnouncements( player )

    /*
        Both clan notice and server news present html window to show contents.
        No point in showing both, since latest one will override window contents.
     */
    let shouldShowNews : boolean = player.getClan() && !await showClanNotice( player )
    if ( shouldShowNews ) {
        showServerNews( player )
    }

    if ( !player.isGM() && player.isIn7sDungeon() ) {
        if ( ( SevenSigns.isSealValidationPeriod() || SevenSigns.isCompareResultsPeriod() )
            && !SevenSigns.hasWinningSide( player.getObjectId() ) ) {
            await player.teleportToLocationType( TeleportWhereType.TOWN )
            player.setIsIn7sDungeon( false )
            player.sendMessage( 'You have been teleported to the nearest town due to the beginning of the Seal Validation period.' )
        } else if ( SevenSigns.getPlayerSide( player.getObjectId() ) === SevenSignsSide.None ) {
            await player.teleportToLocationType( TeleportWhereType.TOWN )
            player.setIsIn7sDungeon( false )
            player.sendMessage( 'You have been teleported to the nearest town because you have not signed for any cabal.' )
        }
    }

    if ( player.isGM() ) {
        if ( player.isInvulnerable() ) {
            player.sendMessage( 'Entering world in Invulnerable mode.' )
        }
        if ( player.isInvisible() ) {
            player.sendMessage( 'Entering world in Invisible mode.' )
        }
        if ( player.isSilenceMode() ) {
            player.sendMessage( 'Entering world in Silence mode.' )
        }
    }

    if ( ConfigManager.character.petitioningAllowed() ) {
        PetitionManager.checkPetitionMessages( player )
    }

    if ( ConfigManager.customs.screenWelcomeMessageEnable() && ConfigManager.customs.getScreenWelcomeMessageText().length > 0 ) {
        player.sendOwnedData( ExShowScreenMessageFromText( ConfigManager.customs.getScreenWelcomeMessageText(), ConfigManager.customs.getScreenWelcomeMessageTime() ) )
    }

    if ( player.getClanJoinExpiryTime() > Date.now() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLAN_MEMBERSHIP_TERMINATED ) )
    }

    if ( ConfigManager.vitality.enabled() && ConfigManager.vitality.recoverVitalityOnReconnect() ) {
        let minutes = Math.floor( ( Date.now() - player.getLastAccess() ) / 60000 )
        let points = Math.floor( ConfigManager.vitality.getRateRecoveryOnReconnect() * minutes )
        if ( points > 0 ) {
            player.updateVitalityPoints( points, false, false )
        }
    }

    if ( player.hasManufactureShop() ) {
        player.setStoreName( player.getStoreName() )
        player.setPrivateStoreType( PrivateStoreType.Manufacture )
        await player.sitDown()
    }

    if ( ConfigManager.character.restoreServitorOnReconnect() && CharacterSummonCache.hasRespawnedServitor( player.getObjectId() ) ) {
        await CharacterSummonCache.restoreServitor( player )
    } else if ( ConfigManager.character.restorePetOnReconnect() && CharacterSummonCache.hasRespawnedPet( player.getObjectId() ) ) {
        await CharacterSummonCache.restoreRespawnedPet( player )
    }

    if ( ConfigManager.character.isGamePointsEnabled() ) {
        let account = player.getAccountName()
        await GamePointsCache.loadAccount( account, player.getObjectId() )

        if ( ConfigManager.character.usePcCafePoints() ) {
            let pcPoints = GamePointsCache.getPoints( account, GamePointType.PcCafe )
            player.sendOwnedData( ExPCCafePointInfo( pcPoints ) )
        }

        if ( ConfigManager.character.useItemShopPoints() ) {
            let shopPoints = GamePointsCache.getPoints( account, GamePointType.ItemShop )
            player.sendOwnedData( ExBrGamePoint( player.getObjectId(), shopPoints ) )
        }
    }

    /*
        Time limited items can show message about expiration, hence these messages
        must come after general messages of server or clan news.
     */
    await aigle.resolve( player.getInventory().getItems() ).each( async ( item: L2ItemInstance ) : Promise<void> => {
        if ( item.isTimeLimitedItem() ) {
            item.scheduleLifeTimeTask()
        }

        if ( item.isShadowItem() && item.isEquipped() ) {
            return item.decreaseMana()
        }
    } )

    player.getWarehouse().getItems().forEach( ( item: L2ItemInstance ) => {
        if ( item.isTimeLimitedItem() ) {
            item.scheduleLifeTimeTask()
        }
    } )

    TerritoryWarManager.onLogin( player )
    player.loadState = PlayerLoadState.EnteredWorld
    player.startDiscoveringObjects()

    ServerLog.info( `Player "${player.getName()}" entered world in ${Date.now() - currentTime} ms` )

    if ( ListenerCache.hasGeneralListener( EventType.PlayerLogin ) ) {
        let eventData = EventPoolCache.getData( EventType.PlayerLogin ) as PlayerLoginEvent

        eventData.playerId = player.getObjectId()
        eventData.classId = player.getClassId()
        eventData.level = player.getLevel()
        eventData.race = player.getRace()

        await ListenerCache.sendGeneralEvent( EventType.PlayerLogin, eventData )
    }
}

async function applyGMAttributes( player: L2PcInstance ): Promise<void> {
    let config = ConfigManager.general
    let accessLevel = player.getAccessLevel()

    /*
        TODO : use command names via enum, to avoid hardcoding a command name
        - review and replace what is needed

        TODO: gmGiveSpecial checks shoud also check player access permission
     */
    if ( config.gmStartupInvulnerable() && accessLevel.canExecute( 'admin_invul' ) ) {
        player.setIsInvulnerable( true )
    }

    if ( config.gmStartupInvisible() && accessLevel.canExecute( 'admin_invisible' ) ) {
        player.setInvisible( true )
    }

    if ( config.gmStartupSilence() && accessLevel.canExecute( 'admin_silence' ) ) {
        player.setSilenceMode( true )
    }

    if ( config.gmStartupDietMode() && accessLevel.canExecute( 'admin_diet' ) ) {
        player.setWeightlessMode( true )
        player.refreshOverloadPenalty()
    }

    let isHidden: boolean = !config.gmStartupAutoList() || !accessLevel.canExecute( 'admin_gmliston' )
    AdminManager.addGm( player.getObjectId(), isHidden )

    if ( config.gmGiveSpecialSkills() ) {
        await SkillCache.addGMSkills( player, false )
    }

    if ( config.gmGiveSpecialAuraSkills() ) {
        await SkillCache.addGMSkills( player, true )
    }
}

function notifyClanMembers( player: L2PcInstance ) {
    let clan: L2Clan = player.getClan()
    if ( clan ) {
        clan.getClanMember( player.getObjectId() ).setPlayerInstance( player )

        let packet = new SystemMessageBuilder( SystemMessageIds.CLAN_MEMBER_S1_LOGGED_IN )
                .addString( player.getName() )
                .getBuffer()

        clan.broadcastDataToOtherOnlineMembers( packet, player )
        clan.broadcastDataToOtherOnlineMembers( PledgeShowMemberListUpdate( player ), player )
    }
}

function notifySponsorOrApprentice( player: L2PcInstance ) {
    if ( player.getSponsor() !== 0 ) {
        let sponsor: L2PcInstance = L2World.getPlayer( player.getSponsor() )
        if ( sponsor ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.YOUR_APPRENTICE_S1_HAS_LOGGED_IN )
                    .addString( player.getName() )
                    .getBuffer()

            sponsor.sendOwnedData( packet )
        }
        return
    }

    if ( player.getApprentice() !== 0 ) {
        let apprentice: L2PcInstance = L2World.getPlayer( player.getApprentice() )
        if ( apprentice ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.YOUR_SPONSOR_C1_HAS_LOGGED_IN )
                    .addString( player.getName() )
                    .getBuffer()

            apprentice.sendOwnedData( packet )
        }
    }
}

async function checkClanNotices( player: L2PcInstance ) : Promise<boolean> {

    let clan: L2Clan = player.getClan()
    if ( !clan ) {
        return false
    }

    player.sendOwnedData( PledgeSkillList( clan ) )

    notifyClanMembers( player )
    notifySponsorOrApprentice( player )

    let clanHall: AuctionableHall = ClanHallManager.getClanHallByOwner( clan )
    if ( clanHall && !clanHall.isPaid() ) {
        player.sendOwnedData( clanHall.getDisplayNoticePacket() )
    }

    SiegeManager.getSieges().forEach( ( siege: Siege ) => {
        if ( !siege.isInProgress() ) {
            return
        }

        if ( siege.checkIsAttacker( clan ) ) {
            player.setSiegeRole( SiegeRole.Attacker )
            player.setSiegeSide( siege.getCastle().getResidenceId() )
        } else if ( siege.checkIsDefender( clan ) ) {
            player.setSiegeRole( SiegeRole.Defender )
            player.setSiegeSide( siege.getCastle().getResidenceId() )
        }
    } )

    FortSiegeManager.getSieges().forEach( ( siege: FortSiege ) => {
        if ( !siege.isInProgress() ) {
            return
        }

        if ( siege.checkIsAttacker( clan ) ) {
            player.setSiegeRole( SiegeRole.Attacker )
            player.setSiegeSide( siege.getFort().getResidenceId() )
        } else if ( siege.checkIsDefender( clan ) ) {
            player.setSiegeRole( SiegeRole.Defender )
            player.setSiegeSide( siege.getFort().getResidenceId() )
        }
    } )

    _.each( ClanHallSiegeManager.getConquerableHalls(), ( hall: SiegableHall ) => {
        if ( !hall.isInSiege() ) {
            return
        }

        if ( hall.isRegistered( clan ) ) {
            player.setSiegeRole( SiegeRole.Attacker )
            player.setSiegeSide( hall.getId() )
            player.setIsInHideoutSiege( true )
        }
    } )

    player.sendOwnedData( PledgeShowMemberListAll( clan ) )
    player.sendOwnedData( PledgeStatusChanged( clan ) )

    if ( clan.getCastleId() > 0 ) {
        await CastleManager.getCastleByOwner( clan ).giveResidentialSkills( player )
    }

    if ( clan.getFortId() > 0 ) {
        await FortManager.getFortByOwner( clan ).giveResidentialSkills( player )
    }

    return clan.isNoticeEnabled()
}

function showServerNews( player: L2PcInstance ) : void {
    if ( !ConfigManager.general.isServerNewsEnabled() ) {
        return
    }

    let path = ConfigManager.general.getServerNewsPath()
    if ( !DataManager.getHtmlData().hasItem( path ) ) {
        if ( player.isGM() ) {
            player.sendMessage( `Unable to find server news path at ${path}` )
        }

        return
    }

    player.sendOwnedData( NpcHtmlMessagePath( DataManager.getHtmlData().getItem( path ), path ) )
}

async function showClanNotice( player : L2PcInstance ) : Promise<boolean> {
    if ( !ConfigManager.general.isClanNoticeTemplateEnabled() ) {
        return false
    }

    if ( !await checkClanNotices( player ) ) {
        return false
    }

    let path = ConfigManager.general.getClanNoticeTemplatePath()
    if ( DataManager.getHtmlData().hasItem( path ) ) {
        let notice = DataManager.getHtmlData().getItem( path )
                                .replace( '%clan_name%', player.getClan().getName() )
                                .replace( '%notice_text%', player.getClan().getNotice() )
        player.sendOwnedData( NpcHtmlMessagePath( notice, path ) )
        return true
    }

    if ( player.isGM() ) {
        player.sendMessage( `Unable to find clan notice template path at ${path}` )
    }

    return false
}