import { GameClient } from '../../GameClient'
import { CharacterCreationFailure, CreationFailureReason } from '../send/CharacterCreationFailure'
import { ClassId, IClassId } from '../../models/base/ClassId'
import { PcAppearance } from '../../models/actor/appearance/PcAppearance'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { CharacterCreationOk } from '../send/CharacterCreationOk'
import { ConfigManager } from '../../../config/ConfigManager'
import { DatabaseManager } from '../../../database/manager'
import { L2PcTemplate } from '../../models/actor/templates/L2PcTemplate'
import { DataManager } from '../../../data/manager'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { L2SkillLearn } from '../../models/L2SkillLearn'
import { Skill } from '../../models/Skill'
import { SkillCache } from '../../cache/SkillCache'
import { L2PlayerCreationPointData } from '../../../data/interface/PlayerCreationPointDataApi'
import {
    DataIntegrityViolationEvent,
    DataIntegrityViolationType,
    EventType,
    PlayerCreatedEvent
} from '../../models/events/EventType'
import { ListenerCache } from '../../cache/ListenerCache'
import { ShortcutOwnerType, ShortcutType } from '../../enums/ShortcutType'
import { L2InitialShortcutItem } from '../../../data/interface/InitialShortcutDataApi'
import { L2PlayerShortcutsTableItem } from '../../../database/interface/CharacterShortcutsTableApi'
import { PlayerShortcutCache } from '../../cache/PlayerShortcutCache'
import _ from 'lodash'
import aigle from 'aigle'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { IDFactoryCache } from '../../cache/IDFactoryCache'
import { FastRateLimit } from 'fast-ratelimit'
import { PlayerEquipmentItem } from '../../interface/ItemDefinition'
import { EventPoolCache } from '../../cache/EventPoolCache'
import { Race } from '../../enums/Race'
import { PlayerClassDefinitions } from '../../models/base/PlayerClassDefinitions'
import { ServerLog } from '../../../logger/Logger'
import { ItemManagerCache } from '../../cache/ItemManagerCache'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 3, // time-to-live value of token bucket (in seconds)
} )

export async function CharacterCreate( client: GameClient, packetData: Buffer ): Promise<void> {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return client.sendPacket( CharacterCreationFailure( CreationFailureReason.creationDenied ) )
    }

    let currentTime = Date.now()
    let packet = new ReadableClientPacket( packetData )
    let name = packet.readSLimit( 17 )

    if ( _.isEmpty( name ) || name.length > 16 ) {
        return client.sendPacket( CharacterCreationFailure( CreationFailureReason.nameTooLong ) )
    }

    if ( ConfigManager.character.getForbiddenNames().has( name ) ) {
        return client.sendPacket( CharacterCreationFailure( CreationFailureReason.badNaming ) )
    }

    if ( !ConfigManager.character.getPlayerNameTemplate().test( name ) ) {
        return client.sendPacket( CharacterCreationFailure( CreationFailureReason.badNaming ) )
    }

    let race = packet.readD(),
            sex = packet.readD(),
            classId = packet.readD()

    let currentClass: IClassId = ClassId.getClassIdByIdentifier( classId )
    if ( !currentClass || currentClass.level > 0 || currentClass.race !== race ) {
        return client.sendPacket( CharacterCreationFailure( CreationFailureReason.creationFailed ) )
    }

    if ( currentClass.race === Race.KAMAEL
        && (
            ( currentClass.id === PlayerClassDefinitions.maleSoldier.classId && sex > 0 )
            || ( ( currentClass.id === PlayerClassDefinitions.femaleSoldier.classId && sex === 0 ) ) ) ) {
        return client.sendPacket( CharacterCreationFailure( CreationFailureReason.creationFailed ) )
    }

    packet.skipD( 6 )

    let hairStyle = packet.readD(),
            hairColor = packet.readD(),
            face = packet.readD()

    if ( ( face > 2 ) || ( face < 0 ) ) {
        return client.sendPacket( CharacterCreationFailure( CreationFailureReason.creationFailed ) )
    }

    if ( ( hairStyle < 0 ) || ( ( sex === 0 ) && ( hairStyle > 4 ) ) || ( ( sex !== 0 ) && ( hairStyle > 6 ) ) ) {
        return client.sendPacket( CharacterCreationFailure( CreationFailureReason.creationFailed ) )
    }

    if ( ( hairColor > 3 ) || ( hairColor < 0 ) ) {
        return client.sendPacket( CharacterCreationFailure( CreationFailureReason.creationFailed ) )
    }

    let characterAmount = await DatabaseManager.getCharacterTable().getAccountCharacterCount( client.getAccountName() )
    if ( characterAmount >= ConfigManager.character.getCharMaxNumber() && ConfigManager.character.getCharMaxNumber() !== 0 ) {
        return client.sendPacket( CharacterCreationFailure( CreationFailureReason.tooManyCharacters ) )
    }

    if ( await DatabaseManager.getCharacterTable().isExistingCharacterName( name ) ) {
        return client.sendPacket( CharacterCreationFailure( CreationFailureReason.nameAlreadyExists ) )
    }

    let appearance: PcAppearance = new PcAppearance( face, hairColor, hairStyle, sex !== 0 )
    let player: L2PcInstance = new L2PcInstance( IDFactoryCache.getNextId(), classId, client.getAccountName(), appearance )

    player.name = name
    player.createDate = Date.now()
    player.baseClass = player.getClassId()
    player.recommendationsLeft = 20

    player.setCurrentHp( player.getMaxHp() )
    player.setCurrentMp( player.getMaxMp() )
    player.setCurrentCp( player.getMaxCp() )

    await DatabaseManager.getCharacterTable().addPlayerCharacter( player )

    if ( ConfigManager.character.getStartingAdena() > 0 ) {
        await player.addAdena( ConfigManager.character.getStartingAdena() )
    }

    let template: L2PcTemplate = player.getTemplate()
    let createLocation: L2PlayerCreationPointData = DataManager.getPlayerCreationPointData().getCreationPoint( template.getClassId() )

    player.setXYZInvisible( createLocation.x, createLocation.y, createLocation.z )
    player.setTitle( '' )
    player.getInventory().markRestored()

    if ( ConfigManager.vitality.enabled() ) {
        player.setVitalityPoints( ConfigManager.vitality.getStartingVitalityPoints() )
    }

    if ( ConfigManager.character.getStartingLevel() > 1 ) {
        await player.addLevel( ConfigManager.character.getStartingLevel() - 1 )
    }

    if ( ConfigManager.character.getStartingSP() > 0 ) {
        player.addSp( ConfigManager.character.getStartingSP() )
    }

    let initialItems: Array<PlayerEquipmentItem> = DataManager.getInitialEquipmentData().getEquipmentList( player.getClassId() )
    await aigle.resolve( initialItems ).each( async ( equipmentItem: PlayerEquipmentItem ) => {
        let item: L2ItemInstance = await player.getInventory().addItem( equipmentItem.id, equipmentItem.count, null, 'CharacterCreate packet' )
        if ( !item ) {
            if ( ListenerCache.hasGeneralListener( EventType.DataIntegrityViolation ) ) {
                let data = EventPoolCache.getData( EventType.DataIntegrityViolation ) as DataIntegrityViolationEvent

                data.type = DataIntegrityViolationType.Item
                data.message = `Unable to add item during character creation: itemId = ${ equipmentItem.id }, amount = ${ equipmentItem.count }`
                data.errorId = '595833dc-fd83-47b8-8463-736fdd26a16e'
                data.playerId = player.getObjectId()
                data.ids = [ equipmentItem.id, equipmentItem.count ]

                await ListenerCache.sendGeneralEvent( EventType.DataIntegrityViolation, data )
            }

            return
        }

        if ( item.isEquipable() && equipmentItem.isEquipped ) {
            return player.getInventory().equipItem( item )
        }
    } )

    let learnableSkills: Array<L2SkillLearn> = SkillCache.getAvailableSkills( player, player.getClassId(), false, true, player )
    await aigle.resolve( learnableSkills ).each( ( learnable: L2SkillLearn ) => {
        let skill: Skill = SkillCache.getSkill( learnable.getSkillId(), learnable.getSkillLevel() )
        return player.addSkill( skill, false )
    } )

    if ( learnableSkills.length > 0 ) {
        await player.runSkillSave()
    }

    await registerAllShortcuts( player )

    if ( ListenerCache.hasGeneralListener( EventType.PlayerCreated ) ) {
        let data = EventPoolCache.getData( EventType.PlayerCreated ) as PlayerCreatedEvent

        data.classId = player.getClassId()
        data.playerId = player.getObjectId()
        data.playerName = name
        data.race = player.getRace()

        await ListenerCache.sendGeneralEvent( EventType.PlayerCreated, data )
    }

    await player.deleteMe()

    ServerLog.info( `Account "${client.accountName}" created player character "${name}" in ${Date.now() - currentTime} ms` )

    client.sendPacket( CharacterCreationOk() )
    return client.updateCharacterSelection()
}

function registerShortcut( player: L2PcInstance, shortcut: L2InitialShortcutItem ): void {
    let id = shortcut.id
    switch ( shortcut.type ) {
        case ShortcutType.ITEM:
            let item: L2ItemInstance = player.getInventory().getItemByItemId( id )
            if ( !item ) {
                return
            }

            id = item.getObjectId()
            break

        case ShortcutType.SKILL:
            if ( !player.getSkills()[ id ] ) {
                return
            }
            break

        case ShortcutType.MACRO:
            return
    }

    let data: L2PlayerShortcutsTableItem = {
        classIndex: player.getClassIndex(),
        objectId: player.getObjectId(),
        characterType: ShortcutOwnerType.Initial,
        id,
        itemReuseGroup: 0,
        level: shortcut.level,
        page: shortcut.page,
        slot: shortcut.slot,
        type: shortcut.type
    }

    PlayerShortcutCache.registerShortcut( player, data )
}

function registerAllShortcuts( player: L2PcInstance ) : Promise<void> {
    if ( !player ) {
        return
    }

    PlayerShortcutCache.initializePlayer( player.getObjectId() )

    DataManager.getInitialShortcutData().getInitialShortcuts().forEach( ( item: L2InitialShortcutItem ) => registerShortcut( player, item ) )
    let classShortcuts = DataManager.getInitialShortcutData().getClassShortcuts()[ player.getClassId() ]
    if ( classShortcuts ) {
        classShortcuts.forEach( ( item: L2InitialShortcutItem ) => registerShortcut( player, item ) )
    }

    return PlayerShortcutCache.savePlayer( player.getObjectId() )
}