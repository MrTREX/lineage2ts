import { DotEnvConfigurationEngine } from './engine/dotenv'
import { L2IConfigEngine } from './IConfigEngine'
import { L2ConfigurationApi } from './IConfiguration'
import aigle from 'aigle'
import _ from 'lodash'
import Timeout = NodeJS.Timeout
import pkgDir from 'pkg-dir'
import logSymbols from 'log-symbols'
import dotenv from 'dotenv'

const rootDirectory = pkgDir.sync( __dirname )
dotenv.config( { path: `${ rootDirectory }/.env` } )

const { configurationEngine, configurationReloadMinutes } = process.env
const allEngines: { [ name: string ]: L2IConfigEngine } = {
    'dotenv': DotEnvConfigurationEngine,
}

console.log( logSymbols.info, `Game Server: using '${ configurationEngine }' engine` )

if ( !allEngines[ configurationEngine ] ) {
    throw new Error( 'Please specify valid \'configurationEngine\' value!' )
}

export const ConfigManager: L2IConfigEngine = allEngines[ configurationEngine as string ]
export let ConfigManagerUpdateTime = Date.now()

function getReloadTime( value: string ): [ number, number ] {
    let minuteValue: number = _.defaultTo( parseInt( value ), 20 )

    minuteValue = Math.max( minuteValue, 0 )
    let intervalTime = minuteValue * 60000

    return [ minuteValue, intervalTime ]
}

let [ parsedMinutes, reloadTime ] = getReloadTime( configurationReloadMinutes )
let reloadInterval: Timeout

export async function loadConfiguration( isReload: boolean = false ) {
    await aigle.resolve( ConfigManager ).eachSeries( ( config: L2ConfigurationApi ) : Promise<void> => {
        return config.load()
    } )

    Object.values( ConfigManager ).forEach( ( config: L2ConfigurationApi ) => {
        config.getSubscriptions().forEach( method => method() )
    } )

    console.log( `${ logSymbols.success } Game Server: Loaded ${ _.size( ConfigManager ) } configs.` )

    if ( isReload ) {
        console.log( `${ logSymbols.success } Game Server: reloads every ${ parsedMinutes } minutes.` )
    }

    ConfigManagerUpdateTime = Date.now()
}

export function forceReload( reloadInterval: number = 0 ): Promise<void> {
    prepareReload( reloadInterval > 0 ? reloadInterval : reloadTime )
    return loadConfiguration( true )
}

function prepareReload( interval: number ): void {
    clearInterval( reloadInterval )

    if ( interval > 0 ) {
        console.log( `${ logSymbols.info } Game Server: Hot reloading enabled in ${ parsedMinutes } minute intervals.` )
        reloadInterval = setInterval( loadConfiguration, interval, true )
        return
    }

    console.log( `${ logSymbols.info } Game Server: Hot reloading is disabled.` )
}

prepareReload( reloadTime )