import { L2ConfigurationSubscription } from '../../IConfiguration'
import { L2RangeConfigurationApi } from '../../interface/RangeConfigurationApi'
import { DotEnvConfigurationEngineHelper } from '../../engine/dotenv'

let configuration: { [ name: string ]: string }
let cachedValues: { [ name: string ]: any }

class Configuration extends L2ConfigurationSubscription implements L2RangeConfigurationApi {
    getChatNormalRange(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ChatNormalRange' )
    }

    getChatShoutRange(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ChatShoutRange' )
    }

    getChatTradeRange(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ChatTradeRange' )
    }

    async load(): Promise<void> {
        configuration = await DotEnvConfigurationEngineHelper.getConfig( 'range.properties' )
        cachedValues = {}
        return
    }
}

export const DotEnvRangeConfiguration: L2RangeConfigurationApi = new Configuration()