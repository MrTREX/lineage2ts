import { L2ConfigurationSubscription } from '../../IConfiguration'
import { L2DiagnosticConfigurationApi } from '../../interface/DiagnosticConfigurationApi'
import { DotEnvConfigurationEngineHelper } from '../../engine/dotenv'

let configuration: { [ name: string ]: string }
let cachedValues: { [ name: string ]: any }

class Configuration extends L2ConfigurationSubscription implements L2DiagnosticConfigurationApi {
    isLoadListeners(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'LoadListeners' )
    }

    showIncomingPackets(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'ShowIncomingPackets' )
    }

    async load(): Promise<void> {
        configuration = await DotEnvConfigurationEngineHelper.getConfig( 'diagnostic.properties' )
        cachedValues = {}
        return
    }

    showDebugMessages(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'ShowDebugMessages' )
    }

    showEventTypeMessages(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'ShowEventTypeMessages' )
    }
}

export const DotEnvDiagnosticConfiguration: L2DiagnosticConfigurationApi = new Configuration()