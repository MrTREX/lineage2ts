import { DotEnvConfigurationEngineHelper } from '../../engine/dotenv'
import { L2DataConfigurationApi } from '../../interface/DataConfigurationApi'
import { L2ConfigurationSubscription } from '../../IConfiguration'

let configuration: Record<string, string>

class Configuration extends L2ConfigurationSubscription implements L2DataConfigurationApi {
    getDataEngine(): string {
        return configuration[ 'DataEngine' ]
    }

    async load(): Promise<void> {
        configuration = await DotEnvConfigurationEngineHelper.getConfig( 'data.properties' )
        return
    }

    getGeoEngine(): string {
        return configuration[ 'GeoEngine' ]
    }

    getDataOptions(): string {
        return configuration[ 'DataOptions' ]
    }

    getGeoOptions(): string {
        return configuration[ 'GeoOptions' ]
    }
}

export const DotEnvDataConfiguration: L2DataConfigurationApi = new Configuration()