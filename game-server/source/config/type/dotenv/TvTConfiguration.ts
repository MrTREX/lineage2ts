import { DotEnvConfigurationEngineHelper } from '../../engine/dotenv'
import {
    L2TvTConfigurationApi,
    L2TvTConfigurationItem,
    L2TvTConfigurationLocation,
    L2TvTConfigurationSkill,
} from '../../interface/TvTConfigurationApi'
import { L2ConfigurationSubscription } from '../../IConfiguration'
import _ from 'lodash'

let configuration: { [ name: string ]: string }
let cachedValues: { [ name: string ]: any }

function getSkills( value: string ): Array<L2TvTConfigurationSkill> {
    let pairs: Array<string> = _.split( value, ';' )

    return pairs.map( ( line: string ): L2TvTConfigurationSkill => {
        let [ id, level ] = _.map( _.split( line, ',' ), value => _.parseInt( value ) )

        return {
            id,
            level,
        }
    } )
}

function getItem( value: string ): L2TvTConfigurationItem {
    let [ id, count ] = _.map( _.split( value, ',' ), value => _.parseInt( value ) )

    return {
        id,
        count,
    }
}

function getItems( value: string ): Array<L2TvTConfigurationItem> {
    let values: Array<string> = _.split( value, ';' )
    return _.map( values, getItem )
}

function getLocation( value: string ): L2TvTConfigurationLocation {
    let [ x, y, z, heading, instanceId ] = _.map( _.split( value, ',' ), value => _.parseInt( value ) )

    return {
        x,
        y,
        z,
        heading,
        instanceId,
    }
}

class Configuration extends L2ConfigurationSubscription implements L2TvTConfigurationApi {
    allowPotion(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AllowPotion' )
    }

    allowScroll(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AllowScroll' )
    }

    allowSummonByItem(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AllowSummonByItem' )
    }

    allowTargetTeamMember(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AllowTargetTeamMember' )
    }

    allowVoicedInfoCommand(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AllowVoicedInfoCommand' )
    }

    enabled(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'Enabled' )
    }

    getDoorsToClose(): Array<number> {
        return DotEnvConfigurationEngineHelper.getNumberArray( configuration, cachedValues, 'DoorsToClose' )
    }

    getDoorsToOpen(): Array<number> {
        return DotEnvConfigurationEngineHelper.getNumberArray( configuration, cachedValues, 'DoorsToOpen' )
    }

    getEffectsRemoval(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'EffectsRemoval' )
    }

    getFighterBuffs(): Array<L2TvTConfigurationSkill> {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'FighterBuffs', getSkills )
    }

    getInstanceFile(): string {
        return configuration[ 'InstanceFile' ]
    }

    getInterval(): Array<string> {
        return DotEnvConfigurationEngineHelper.getStringArray( configuration, cachedValues, 'Interval' )
    }

    getMageBuffs(): Array<L2TvTConfigurationSkill> {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'MageBuffs', getSkills )
    }

    getMaxParticipantsPerIP(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxParticipantsPerIP' )
    }

    getMaxPlayerLevel(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxPlayerLevel' )
    }

    getMaxPlayersInTeams(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxPlayersInTeams' )
    }

    getMinPlayerLevel(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MinPlayerLevel' )
    }

    getMinPlayersInTeams(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MinPlayersInTeams' )
    }

    getParticipationFee(): L2TvTConfigurationItem {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'ParticipationFee', getItem )
    }

    getParticipationNpcId(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ParticipationNpcId' )
    }

    getParticipationNpcLocation(): L2TvTConfigurationLocation {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'ParticipationNpcLoc', getLocation )
    }

    getParticipationTime(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ParticipationTime' )
    }

    getRespawnTeleportDelay(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'RespawnTeleportDelay', 1000 )
    }

    getReward(): Array<L2TvTConfigurationItem> {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'Reward', getItems )
    }

    getRunningTime(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'RunningTime' )
    }

    getStartLeaveTeleportDelay(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'StartLeaveTeleportDelay', 1000 )
    }

    getTeam1Loc(): L2TvTConfigurationLocation {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'Team1Loc', getLocation )
    }

    getTeam1Name(): string {
        return configuration[ 'Team1Name' ]
    }

    getTeam2Loc(): L2TvTConfigurationLocation {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'Team2Loc', getLocation )
    }

    getTeam2Name(): string {
        return configuration[ 'Team2Name' ]
    }

    instanced(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'Instanced' )
    }

    async load(): Promise<void> {
        configuration = await DotEnvConfigurationEngineHelper.getConfig( 'tvt.properties' )
        cachedValues = {}
        return
    }

    rewardTeamTie(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'RewardTeamTie' )
    }
}

export const DotEnvTvTConfiguration: L2TvTConfigurationApi = new Configuration()