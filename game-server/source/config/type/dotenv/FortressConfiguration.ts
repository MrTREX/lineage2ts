import { DotEnvConfigurationEngineHelper } from '../../engine/dotenv'
import { L2FortressConfiguration } from '../../interface/FortressConfiguration'
import { L2ConfigurationSubscription } from '../../IConfiguration'

let configuration: { [ name: string ]: string }
let cachedValues: { [ name: string ]: any }

class Configuration extends L2ConfigurationSubscription implements L2FortressConfiguration {
    getBloodOathCount(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'BloodOathCount' )
    }

    getExpRegenerationFeeLvl1(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ExpRegenerationFeeLvl1' )
    }

    getExpRegenerationFeeLvl2(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ExpRegenerationFeeLvl2' )
    }

    getExpRegenerationFunctionFeeRatio(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ExpRegenerationFunctionFeeRatio' )
    }

    getFeeForCastle(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'FeeForCastle' )
    }

    getHpRegenerationFeeLvl1(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'HpRegenerationFeeLvl1' )
    }

    getHpRegenerationFeeLvl2(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'HpRegenerationFeeLvl2' )
    }

    getHpRegenerationFunctionFeeRatio(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'HpRegenerationFunctionFeeRatio' )
    }

    getMaxKeepTime(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxKeepTime' )
    }

    getMaxSupplyLevel(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxSupplyLevel' )
    }

    getMpRegenerationFeeLvl1(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MpRegenerationFeeLvl1' )
    }

    getMpRegenerationFeeLvl2(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MpRegenerationFeeLvl2' )
    }

    getMpRegenerationFunctionFeeRatio(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MpRegenerationFunctionFeeRatio' )
    }

    getPeriodicUpdateFrequency(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'PeriodicUpdateFrequency' ) * 60000
    }

    getSupportFeeLvl1(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'SupportFeeLvl1' )
    }

    getSupportFeeLvl2(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'SupportFeeLvl2' )
    }

    getSupportFunctionFeeRatio(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'SupportFunctionFeeRatio' )
    }

    getTeleportFunctionFeeLvl1(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'TeleportFunctionFeeLvl1' )
    }

    getTeleportFunctionFeeLvl2(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'TeleportFunctionFeeLvl2' )
    }

    getTeleportFunctionFeeRatio(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'TeleportFunctionFeeRatio' )
    }

    async load(): Promise<void> {
        configuration = await DotEnvConfigurationEngineHelper.getConfig( 'fortress.properties' )
        cachedValues = {}
        return
    }
}

export const DotEnvFortressConfiguration: L2FortressConfiguration = new Configuration()