import { DotEnvConfigurationEngineHelper } from '../../engine/dotenv'
import { L2DatabaseConfigurationApi } from '../../interface/DatabaseConfigurationApi'
import { L2ConfigurationSubscription } from '../../IConfiguration'

let configuration: { [ name: string ]: string }
let cachedValues: { [ name: string ]: any }

class Configuration extends L2ConfigurationSubscription implements L2DatabaseConfigurationApi {
    getDatabaseName(): string {
        return configuration[ 'DatabaseName' ]
    }

    getEngine(): string {
        return configuration[ 'Engine' ]
    }

    getConnectionParameters(): { [ key: string ]: string } {
        return DotEnvConfigurationEngineHelper.getStringMap( configuration, cachedValues, 'ConnectionParameters' )
    }

    getDatabasePassword(): string {
        return configuration[ 'DatabasePassword' ]
    }

    getDatabaseHost(): string {
        return configuration[ 'DatabaseHost' ]
    }

    getDatabaseUsername(): string {
        return configuration[ 'DatabaseUsername' ]
    }

    async load(): Promise<void> {
        configuration = await DotEnvConfigurationEngineHelper.getConfig( 'database.properties' )
        cachedValues = {}
        return
    }

    getDatabasePort(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'DatabasePort' )
    }
}

export const DotEnvDatabaseConfiguration: L2DatabaseConfigurationApi = new Configuration()