import { DotEnvConfigurationEngineHelper } from '../../engine/dotenv'
import { L2GraciaSeedsConfigurationApi } from '../../interface/GraciaSeedsConfigurationApi'
import { L2ConfigurationSubscription } from '../../IConfiguration'

let configuration: { [ name: string ]: string }
let cachedValues: { [ name: string ]: any }

class Configuration extends L2ConfigurationSubscription implements L2GraciaSeedsConfigurationApi {
    getStage2Length(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'Stage2Length' )
    }

    getTiatKillCountForNextState(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'getTiatKillCountForNextState' )
    }

    async load(): Promise<void> {
        configuration = await DotEnvConfigurationEngineHelper.getConfig( 'graciaseeds.properties' )
        cachedValues = {}
        return
    }
}

export const DotEnvGraciaSeedsConfiguration: L2GraciaSeedsConfigurationApi = new Configuration()