import { L2TeleporterConfigurationApi } from '../../interface/TeleporterConfigurationApi'
import { DotEnvConfigurationEngineHelper } from '../../engine/dotenv'
import { L2ConfigurationSubscription } from '../../IConfiguration'

let configuration: { [ name: string ]: string }
let cachedValues: { [ name: string ]: any }

class Configuration extends L2ConfigurationSubscription implements L2TeleporterConfigurationApi {

    getVoicedCommand(): string {
        return configuration[ 'VoicedCommand' ]
    }

    getVoicedName(): string {
        return configuration[ 'VoicedName' ]
    }

    getVoicedRequiredItems(): Array<number> {
        return DotEnvConfigurationEngineHelper.getNumberArray( configuration, cachedValues, 'VoicedRequiredItems' )
    }

    isEnabled(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'Enable' )
    }

    isRestrictedForChaoticPlayers(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'RestrictForChaoticPlayers' )
    }

    isRestrictedInDuel(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'RestrictInDuel' )
    }

    isRestrictedInEvents(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'RestrictInEvents' )
    }

    isRestrictedInFight(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'RestrictInFight' )
    }

    isRestrictedInPVP(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'RestrictInPvp' )
    }

    isVoicedCommandEnabled(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'VoicedEnable' )
    }

    async load(): Promise<void> {
        configuration = await DotEnvConfigurationEngineHelper.getConfig( 'teleporter.properties' )
        cachedValues = {}
        return
    }
}

export const DotEnvTeleporterConfiguration: L2TeleporterConfigurationApi = new Configuration()