import { L2CustomsConfigurationApi } from '../../interface/CustomsConfigurationApi'
import { DotEnvConfigurationEngineHelper } from '../../engine/dotenv'
import { L2ConfigurationSubscription } from '../../IConfiguration'

let configuration: { [ name: string ]: string }
let cachedValues: { [ name: string ]: any }

class Configuration extends L2ConfigurationSubscription implements L2CustomsConfigurationApi {
    getAccountPrivilegesLevel(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'AccountPrivilegesLevel' )
    }
    isAccountPrivilegesEnabled(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AccountPrivilegesEnabled' )
    }
    getAccountPrivilegesAmount(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'AccountPrivilegesAmount' )
    }
    getTreasureChestVisibility(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'TreasureChestVisibility' )
    }

    changePasswordEnabled(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'ChangePasswordEnabled' )
    }

    viewNpcVoiced(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'ViewNpcVoiced' )
    }

    viewNpcEnabled(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'ViewNpcEnabled' )
    }

    viewNpcItems(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'ViewNpcItems' )
    }

    getChampionQuestRewardsAmount(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'ChampionQuestRewardsAmount' )
    }

    getChampionQuestRewardsChance(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'ChampionQuestRewardsChance' )
    }

    autoLootHerbsVoiceRestore(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AutoLootHerbsVoiceRestore' )
    }

    autoLootItemsVoiceRestore(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AutoLootItemsVoiceRestore' )
    }

    autoLootVoiceCommand(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'autoLootVoiceCommand' )
    }

    autoLootVoiceRestore(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AutoLootVoiceRestore' )
    }

    getAutoLootHerbsList(): Array<number> {
        return DotEnvConfigurationEngineHelper.getNumberArray( configuration, cachedValues, 'AutoLootHerbsList' )
    }

    getAutoLootItemsList(): Array<number> {
        return DotEnvConfigurationEngineHelper.getNumberArray( configuration, cachedValues, 'AutoLootItemsList' )
    }

    announcePkPvP(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AnnouncePkPvP' )
    }

    announcePkPvPNormalMessage(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AnnouncePkPvPNormalMessage' )
    }

    antiFeedDisconnectedAsDualbox(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AntiFeedDisconnectedAsDualbox' )
    }

    antiFeedDualbox(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AntiFeedDualbox' )
    }

    antiFeedEnable(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AntiFeedEnable' )
    }

    bankingEnabled(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'BankingEnabled' )
    }

    championEnable(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'ChampionEnable' )
    }

    championEnableInInstances(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'ChampionEnableInInstances' )
    }

    championEnableVitality(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'ChampionEnableVitality' )
    }

    championPassive(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'ChampionPassive' )
    }

    chatAdmin(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'ChatAdmin' )
    }

    displayServerTime(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'DisplayServerTime' )
    }

    enableManaPotionSupport(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'EnableManaPotionSupport' )
    }

    enableWarehouseSortingClan(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'EnableWarehouseSortingClan' )
    }

    enableWarehouseSortingPrivate(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'EnableWarehouseSortingPrivate' )
    }

    getAnnouncePkMsg(): string {
        return configuration[ 'AnnouncePkMsg' ]
    }

    getAnnouncePvpMsg(): string {
        return configuration[ 'AnnouncePvpMsg' ]
    }

    getPlayerKillInterval(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'PlayerKillInterval' )
    }

    getBankingAdenaCount(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'BankingAdenaCount' )
    }

    getBankingGoldbarCount(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'BankingGoldbarCount' )
    }

    getChampionAdenasRewardsAmount(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'ChampionAdenasRewardsAmount' )
    }

    getChampionAdenasRewardsChance(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'ChampionAdenasRewardsChance' )
    }

    getChampionAttack(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'ChampionAtk' )
    }

    getChampionFrequency(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ChampionFrequency' )
    }

    getChampionHp(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ChampionHp' )
    }

    getChampionHpRegen(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'ChampionHpRegen' )
    }

    getChampionMaxLevel(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ChampionMaxLevel' )
    }

    getChampionMinLevel(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ChampionMinLevel' )
    }

    getChampionRewardHigherLevelItemChance(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ChampionRewardHigherLvlItemChance' )
    }

    getChampionRewardItemID(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ChampionRewardItemID' )
    }

    getChampionRewardItemQuantity(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ChampionRewardItemQty' )
    }

    getChampionRewardLowerLevelItemChance(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ChampionRewardLowerLvlItemChance' )
    }

    getChampionRewardsAmount(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'ChampionRewardsAmount' )
    }

    getChampionRewardsChance(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'ChampionRewardsChance' )
    }

    getChampionRewardsExpSp(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'ChampionRewardsExpSp' )
    }

    getChampionSpeedAttack(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'ChampionSpdAtk' )
    }

    getChampionTitle(): string {
        return configuration[ 'ChampionTitle' ]
    }

    getDualboxCheckMaxL2EventParticipantsPerIP(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'DualboxCheckMaxL2EventParticipantsPerIP' )
    }

    getDualboxCheckMaxOlympiadParticipantsPerIP(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'DualboxCheckMaxOlympiadParticipantsPerIP' )
    }

    getDualboxCheckMaxPlayersPerIP(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'DualboxCheckMaxPlayersPerIP' )
    }

    getDualboxCheckWhitelist(): { [ p: number ]: number } {
        return {} // TODO : convert values here
    }

    getOfflineMaxDays(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'OfflineMaxDays' )
    }

    getOfflineNameColor(): number {
        return DotEnvConfigurationEngineHelper.getHexNumber( configuration, cachedValues, 'OfflineNameColor', 0x808080 )
    }

    getScreenWelcomeMessageText(): string {
        return configuration[ 'ScreenWelcomeMessageText' ]
    }

    getScreenWelcomeMessageTime(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ScreenWelcomeMessageTime' ) * 1000
    }

    hellboundStatus(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'HellboundStatus' )
    }

    l2WalkerProtection(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'L2WalkerProtection' )
    }

    async load(): Promise<void> {
        configuration = await DotEnvConfigurationEngineHelper.getConfig( 'customs.properties' )
        cachedValues = {}
        return
    }

    offlineCraftEnable(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'OfflineCraftEnable' )
    }

    offlineDisconnectFinished(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'OfflineDisconnectFinished' )
    }

    offlineFame(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'OfflineFame' )
    }

    offlineModeInPeaceZone(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'OfflineModeInPeaceZone' )
    }

    offlineModeNoDamage(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'OfflineModeNoDamage' )
    }

    offlineSetNameColor(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'OfflineSetNameColor' )
    }

    offlineTradeEnable(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'OfflineTradeEnable' )
    }

    restoreOffliners(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'RestoreOffliners' )
    }

    screenWelcomeMessageEnable(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'ScreenWelcomeMessageEnable' )
    }
}

export const DotEnvCustomsConfiguration: L2CustomsConfigurationApi = new Configuration()