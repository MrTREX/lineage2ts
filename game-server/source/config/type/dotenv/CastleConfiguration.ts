import { DotEnvConfigurationEngineHelper } from '../../engine/dotenv'
import { L2CastleConfigurationApi } from '../../interface/CastleConfigurationApi'
import { L2ConfigurationSubscription } from '../../IConfiguration'

let configuration: { [ name: string ]: string }
let cachedValues: { [ name: string ]: any }

class Configuration extends L2ConfigurationSubscription implements L2CastleConfigurationApi {
    allowRideWyvernAlways(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AllowRideWyvernAlways' )
    }

    allowRideWyvernDuringSiege(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AllowRideWyvernDuringSiege' )
    }

    getExpRegenerationFeeLvl1(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ExpRegenerationFeeLvl1' )
    }

    getExpRegenerationFeeLvl2(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ExpRegenerationFeeLvl2' )
    }

    getExpRegenerationFunctionFeeRatio(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ExpRegenerationFunctionFeeRatio' )
    }

    getHpRegenerationFeeLvl1(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'HpRegenerationFeeLvl1' )
    }

    getHpRegenerationFeeLvl2(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'HpRegenerationFeeLvl2' )
    }

    getHpRegenerationFunctionFeeRatio(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'HpRegenerationFunctionFeeRatio' )
    }

    getInnerDoorUpgradePriceLvl2(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'InnerDoorUpgradePriceLvl2' )
    }

    getInnerDoorUpgradePriceLvl3(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'InnerDoorUpgradePriceLvl3' )
    }

    getInnerDoorUpgradePriceLvl5(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'InnerDoorUpgradePriceLvl5' )
    }

    getMpRegenerationFeeLvl1(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MpRegenerationFeeLvl1' )
    }

    getMpRegenerationFeeLvl2(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MpRegenerationFeeLvl2' )
    }

    getMpRegenerationFunctionFeeRatio(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MpRegenerationFunctionFeeRatio' )
    }

    getOuterDoorUpgradePriceLvl2(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'OuterDoorUpgradePriceLvl2' )
    }

    getOuterDoorUpgradePriceLvl3(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'OuterDoorUpgradePriceLvl3' )
    }

    getOuterDoorUpgradePriceLvl5(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'OuterDoorUpgradePriceLvl5' )
    }

    getSiegeHourList(): Array<number> {
        return DotEnvConfigurationEngineHelper.getNumberArray( configuration, cachedValues, 'SiegeHourList' )
    }

    getSupportFeeLvl1(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'SupportFeeLvl1' )
    }

    getSupportFeeLvl2(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'SupportFeeLvl2' )
    }

    getSupportFunctionFeeRatio(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'SupportFunctionFeeRatio' )
    }

    getTeleportFunctionFeeLvl1(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'TeleportFunctionFeeLvl1' )
    }

    getTeleportFunctionFeeLvl2(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'TeleportFunctionFeeLvl2' )
    }

    getTeleportFunctionFeeRatio(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'TeleportFunctionFeeRatio' )
    }

    getTrapUpgradePriceLvl1(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'TrapUpgradePriceLvl1' )
    }

    getTrapUpgradePriceLvl2(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'TrapUpgradePriceLvl2' )
    }

    getTrapUpgradePriceLvl3(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'TrapUpgradePriceLvl3' )
    }

    getTrapUpgradePriceLvl4(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'TrapUpgradePriceLvl4' )
    }

    getWallUpgradePriceLvl2(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'WallUpgradePriceLvl2' )
    }

    getWallUpgradePriceLvl3(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'WallUpgradePriceLvl3' )
    }

    getWallUpgradePriceLvl5(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'WallUpgradePriceLvl5' )
    }

    async load(): Promise<void> {
        configuration = await DotEnvConfigurationEngineHelper.getConfig( 'castle.properties' )
        cachedValues = {}
        return
    }
}

export const DotEnvCastleConfiguration: L2CastleConfigurationApi = new Configuration()