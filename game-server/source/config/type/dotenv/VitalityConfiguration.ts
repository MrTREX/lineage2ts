import { DotEnvConfigurationEngineHelper } from '../../engine/dotenv'
import { L2VitalityConfigurationApi } from '../../interface/VitalityConfigurationApi'
import { L2ConfigurationSubscription } from '../../IConfiguration'

let configuration: { [ name: string ]: string }
let cachedValues: { [ name: string ]: any }

class Configuration extends L2ConfigurationSubscription implements L2VitalityConfigurationApi {
    requirePeaceArea(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'RequirePeaceArea' )
    }

    getRecoveryInterval(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'RecoveryInterval' )
    }

    enabled(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'Enabled' )
    }

    getRateRecoveryOnReconnect(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'RateRecoveryOnReconnect' )
    }

    getIntervalPoints(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'IntervalPoints' )
    }

    getRateVitalityGain(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'RateVitalityGain' )
    }

    getRateVitalityLevel1(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'RateVitalityLevel1' )
    }

    getRateVitalityLevel2(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'RateVitalityLevel2' )
    }

    getRateVitalityLevel3(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'RateVitalityLevel3' )
    }

    getRateVitalityLevel4(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'RateVitalityLevel4' )
    }

    getRateVitalityLost(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'RateVitalityLost' )
    }

    getStartingVitalityPoints(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'StartingVitalityPoints' )
    }

    async load(): Promise<void> {
        configuration = await DotEnvConfigurationEngineHelper.getConfig( 'vitality.properties' )
        cachedValues = {}
        return
    }

    recoverVitalityOnReconnect(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'RecoverVitalityOnReconnect' )
    }
}

export const DotEnvVitalityConfiguration: L2VitalityConfigurationApi = new Configuration()