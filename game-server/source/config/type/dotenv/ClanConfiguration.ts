import { DotEnvConfigurationEngineHelper } from '../../engine/dotenv'
import { L2ClanConfigurationApi } from '../../interface/ClanConfigurationApi'
import { L2ConfigurationSubscription } from '../../IConfiguration'

let configuration: { [ name: string ]: string }
let cachedValues: { [ name: string ]: any }

class Configuration extends L2ConfigurationSubscription implements L2ClanConfigurationApi {
    getMinimumPlayerLevel(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MinimumPlayerLevel' )
    }

    get10thRaidRankingPoints(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, '10thRaidRankingPoints' )
    }

    get1stRaidRankingPoints(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, '1stRaidRankingPoints' )
    }

    get2ndRaidRankingPoints(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, '2ndRaidRankingPoints' )
    }

    get3rdRaidRankingPoints(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, '3rdRaidRankingPoints' )
    }

    get4thRaidRankingPoints(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, '4thRaidRankingPoints' )
    }

    get5thRaidRankingPoints(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, '5thRaidRankingPoints' )
    }

    get6thRaidRankingPoints(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, '6thRaidRankingPoints' )
    }

    get7thRaidRankingPoints(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, '7thRaidRankingPoints' )
    }

    get8thRaidRankingPoints(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, '8thRaidRankingPoints' )
    }

    get9thRaidRankingPoints(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, '9thRaidRankingPoints' )
    }

    getBloodAlliancePoints(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'BloodAlliancePoints' )
    }

    getBloodOathPoints(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'BloodOathPoints' )
    }

    getCastleDefendedPoints(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'CastleDefendedPoints' )
    }

    getClanLevel10Cost(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ClanLevel10Cost' )
    }

    getClanLevel10Requirement(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ClanLevel10Requirement' )
    }

    getClanLevel11Cost(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ClanLevel11Cost' )
    }

    getClanLevel11Requirement(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ClanLevel11Requirement' )
    }

    getClanLevel6Cost(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ClanLevel6Cost' )
    }

    getClanLevel6Requirement(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ClanLevel6Requirement' )
    }

    getClanLevel7Cost(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ClanLevel7Cost' )
    }

    getClanLevel7Requirement(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ClanLevel7Requirement' )
    }

    getClanLevel8Cost(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ClanLevel8Cost' )
    }

    getClanLevel8Requirement(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ClanLevel8Requirement' )
    }

    getClanLevel9Cost(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ClanLevel9Cost' )
    }

    getClanLevel9Requirement(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ClanLevel9Requirement' )
    }

    getClanNameTemplate(): RegExp {
        return DotEnvConfigurationEngineHelper.getRegex( configuration, cachedValues, 'ClanNameTemplate' )
    }

    getCompleteAcademyMaxPoints(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'CompleteAcademyMaxPoints' )
    }

    getCompleteAcademyMinPoints(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'CompleteAcademyMinPoints' )
    }

    getCreateKnightUnitCost(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'CreateKnightUnitCost' )
    }

    getCreateRoyalGuardCost(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'CreateRoyalGuardCost' )
    }

    getFestivalOfDarknessWin(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'FestivalOfDarknessWin' )
    }

    getHeroPoints(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'HeroPoints' )
    }

    getKillBallistaPoints(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'KillBallistaPoints' )
    }

    getKnightsEpaulettePoints(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'KnightsEpaulettePoints' )
    }

    getLoseCastlePoints(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'LoseCastlePoints' )
    }

    getLoseFortPoints(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'LoseFortPoints' )
    }

    getReinforceKnightUnitCost(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ReinforceKnightUnitCost' )
    }

    getReputationScorePerKill(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ReputationScorePerKill' )
    }

    getTakeCastlePoints(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'TakeCastlePoints' )
    }

    getTakeFortPoints(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'TakeFortPoints' )
    }

    getUpTo100thRaidRankingPoints(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'UpTo100thRaidRankingPoints' )
    }

    getUpTo50thRaidRankingPoints(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'UpTo50thRaidRankingPoints' )
    }

    async load(): Promise<void> {
        configuration = await DotEnvConfigurationEngineHelper.getConfig( 'clan.properties' )
        cachedValues = {}
        return
    }
}

export const DotEnvClanConfiguration: L2ClanConfigurationApi = new Configuration()