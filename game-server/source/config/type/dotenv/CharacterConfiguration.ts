import { L2CharacterConfigurationApi } from '../../interface/CharacterConfigurationApi'
import { DotEnvConfigurationEngineHelper } from '../../engine/dotenv'
import { L2ConfigurationSubscription } from '../../IConfiguration'
import _ from 'lodash'

let configuration: { [ name: string ]: string }
let cachedValues: { [ name: string ]: any }

type PartyCutoffGap = [ number, number, number ]

function getPartyXpCutoffGaps( value: string ): Array<PartyCutoffGap> {
    return value.split( ';' ).map( ( data: string ): PartyCutoffGap => {
        let values: Array<number> = data.split( ',' ).map( ( item: string ): number => {
            return _.defaultTo( parseFloat( item ), 0 )
        } )

        if ( values.length < 3 ) {
            _.times( 3 - values.length, () => values.push( 0 ) )
        }

        return values as PartyCutoffGap
    } )
}

class Configuration extends L2ConfigurationSubscription implements L2CharacterConfigurationApi {
    getItemShopRecentPurchaseLimit(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ItemShopRecentPurchaseLimit' )
    }

    getPcCafePointsAcquisition(): string {
        return configuration[ 'PcCafePointsAcquisition' ]
    }

    getPcCafePointsAcquisitionRate(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'PcCafePointsAcquisitionRate' )
    }

    getPcCafePointsAcquisitionAmount(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'PcCafePointsAcquisitionAmount' )
    }

    getItemShopPointsAcquisition(): string {
        return configuration[ 'ItemShopPointsAcquisition' ]
    }

    getItemShopPointsAcquisitionRate(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ItemShopPointsAcquisitionRate' )
    }

    getItemShopPointsAcquisitionAmount(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ItemShopPointsAcquisitionAmount' )
    }

    getPcCafePointsLimit(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'PcCafePointsLimit' )
    }

    getItemShopPointsLimit(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ItemShopPointsLimit' )
    }

    isGamePointsEnabled(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'GamePointsEnabled' )
    }

    usePcCafePoints(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'PcCafePointsEnabled' )
    }

    useItemShopPoints(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'ItemShopPointsEnabled' )
    }

    getNevitBlessedSpBonus(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'NevitSpBonus' )
    }

    getNevitBlessedExpBonus(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'NevitExpBonus' )
    }

    getNevitBlessedLevelupPoints(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'NevitBlessedLevelupPoints' )
    }

    getNevitLevelupPoints(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'NevitLevelupPoints' )
    }

    getNevitIgnoreAdventTime(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'NevitIgnoreAdventTime' )
    }

    getNevitAdventTime(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'NevitAdventTime' )
    }

    getNevitEffectTime(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'NevitEffectTime' )
    }

    getNevitHuntingPointLimit(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'NevitHuntingPointLimit' )
    }

    isNevitEnabled(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'NevitEnabled' )
    }

    getMaximumRefundCapacity(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaximumRefundCapacity' )
    }

    allowAugmentPvPItems(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AllowAugmentPvPItems' )
    }

    allowEntireTree(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AllowEntireTree' )
    }

    alternativeCrafting(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AlternativeCrafting' )
    }

    autoLearnDivineInspiration(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AutoLearnDivineInspiration' )
    }

    autoLearnForgottenScrollSkills(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AutoLearnForgottenScrollSkills' )
    }

    autoLearnSkills(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AutoLearnSkills' )
    }

    autoLoot(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AutoLoot' )
    }

    autoLootHerbs(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AutoLootHerbs' )
    }

    autoLootRaids(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AutoLootRaids' )
    }

    blacksmithUseRecipes(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'BlacksmithUseRecipes' )
    }

    cancelBow(): boolean {
        return [ 'all', 'bow' ].includes( this.cancelByHit() )
    }

    cancelByHit(): string {
        return configuration[ 'CancelByHit' ].toLowerCase()
    }

    cancelCast(): boolean {
        return [ 'all', 'cast' ].includes( this.cancelByHit() )
    }

    clanLeaderInstantActivation(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'ClanLeaderInstantActivation' )
    }

    crafting(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'Crafting' )
    }

    danceCancelBuff(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'DanceCancelBuff' )
    }

    danceConsumeAdditionalMP(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'DanceConsumeAdditionalMP' )
    }

    decreaseSkillOnDelevel(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'DecreaseSkillOnDelevel' )
    }

    delevel(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'Delevel' )
    }

    divineInspirationSpBookNeeded(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'DivineInspirationSpBookNeeded' )
    }

    enchantSkillSpBookNeeded(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'EnchantSkillSpBookNeeded' )
    }

    expertisePenalty(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'ExpertisePenalty' )
    }

    fameForDeadPlayers(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'FameForDeadPlayers' )
    }

    freeTeleporting(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'FreeTeleporting' )
    }

    getAugmentationAccSkillChance(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'AugmentationAccSkillChance' )
    }

    getAugmentationBaseStatChance(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'AugmentationBaseStatChance' )
    }

    getAugmentationBlacklist(): Array<number> {
        return DotEnvConfigurationEngineHelper.getNumberArray( configuration, cachedValues, 'AugmentationBlacklist' )
    }

    getAugmentationHighGlowChance(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'AugmentationHighGlowChance' )
    }

    getAugmentationHighSkillChance(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'AugmentationHighSkillChance' )
    }

    getAugmentationMidGlowChance(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'AugmentationMidGlowChance' )
    }

    getAugmentationMidSkillChance(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'AugmentationMidSkillChance' )
    }

    getAugmentationNGGlowChance(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'AugmentationNGGlowChance' )
    }

    getAugmentationNGSkillChance(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'AugmentationNGSkillChance' )
    }

    getAugmentationTopGlowChance(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'AugmentationTopGlowChance' )
    }

    getAugmentationTopSkillChance(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'AugmentationTopSkillChance' )
    }

    getBaseSubclassLevel(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'BaseSubclassLevel' )
    }

    getBlockListLimit(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'BlockListLimit' )
    }

    getCastleFameIntervalPoints(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'CastleFameIntervalPoints' )
    }

    getCastleFameInterval(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'CastleFameInterval' )
    }

    getCharMaxNumber(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'CharMaxNumber' )
    }

    getClanLeaderWeekdayChange(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ClanLeaderWeekdayChange' )
    }

    getClanLeaderHourChange(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ClanLeaderHourChange' )
    }

    getClanMembersForWar(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ClanMembersForWar' )
    }

    getCommonRecipeLimit(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'CommonRecipeLimit' )
    }

    getCpRegenMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'CpRegenMultiplier' )
    }

    getCraftingRareSpRate(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'CraftingRareSpRate' )
    }

    getCraftingRareXpRate(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'CraftingRareXpRate' )
    }

    getCraftingSpRate(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'CraftingSpRate' )
    }

    getCraftingSpeed(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'CraftingSpeed' )
    }

    getCraftingXpRate(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'CraftingXpRate' )
    }

    getDaysBeforeAcceptNewClanWhenDismissed(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'DaysBeforeAcceptNewClanWhenDismissed' )
    }

    getDaysBeforeCreateAClan(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'DaysBeforeCreateAClan' )
    }

    getDaysBeforeCreateNewAllyWhenDissolved(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'DaysBeforeCreateNewAllyWhenDissolved' )
    }

    getDaysBeforeJoinAClan(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'DaysBeforeJoinAClan' )
    }

    getDaysBeforeJoinAllyWhenDismissed(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'DaysBeforeJoinAllyWhenDismissed' )
    }

    getDaysBeforeJoiningAllianceAfterLeaving(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'DaysBeforeJoiningAllianceAfterLeaving' )
    }

    getDaysToPassToDissolveAClan(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'DaysToPassToDissolveAClan' )
    }

    getDeathPenaltyChance(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'DeathPenaltyChance' )
    }

    getDaysToDeleteCharacter(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'DeleteCharAfterDays' )
    }

    getDwarfRecipeLimit(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'DwarfRecipeLimit' )
    }

    getEffectTickRatio(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'EffectTickRatio' )
    }

    getEnchantBlacklist(): Array<number> {
        return DotEnvConfigurationEngineHelper.getNumberArray( configuration, cachedValues, 'EnchantBlacklist' )
    }

    getEnchantChanceElementCrystal(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'EnchantChanceElementCrystal' )
    }

    getEnchantChanceElementEnergy(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'EnchantChanceElementEnergy' )
    }

    getEnchantChanceElementJewel(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'EnchantChanceElementJewel' )
    }

    getEnchantChanceElementStone(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'EnchantChanceElementStone' )
    }

    getExponentSp(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ExponentSp' )
    }

    getExponentXp(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ExponentXp' )
    }

    getFeeDeleteSubClassSkills(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'FeeDeleteSubClassSkills' )
    }

    getFeeDeleteTransferSkills(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'FeeDeleteTransferSkills' )
    }

    getForbiddenNames(): Set<string> {
        return DotEnvConfigurationEngineHelper.getStringSet( configuration, cachedValues, 'ForbiddenNames' )
    }

    getFortressFameIntervalPoints(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'FortressFameIntervalPoints' )
    }

    getFortressFameInterval(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'FortressFameInterval' )
    }

    getFreightPrice(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'FreightPrice' )
    }

    getFriendListLimit(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'FriendListLimit' )
    }

    getHpRegenerationMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'HpRegenMultiplier' )
    }

    getMaxAbnormalStateSuccessRate(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxAbnormalStateSuccessRate' )
    }

    getMaxBuffAmount(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxBuffAmount' )
    }

    getMaxDanceAmount(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxDanceAmount' )
    }

    getMaxEvasion(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxEvasion' )
    }

    getMaxExpBonus(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxExpBonus' )
    }

    getMaxMagicAttackSpeed(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxMAtkSpeed' )
    }

    getMaxMagicCritRate(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxMCritRate' )
    }

    getMaxNumberOfClansInAlly(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxNumOfClansInAlly' )
    }

    getMaxOffsetOnTeleport(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxOffsetOnTeleport' )
    }

    getMaxPowerAttackSpeed(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxPAtkSpeed' )
    }

    getMaxPowerCritRate(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxPCritRate' )
    }

    getMaxPersonalFamePoints(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxPersonalFamePoints' )
    }

    getMaxPetLevel(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxPetLevel' )
    }

    getMaxPetitionsPending(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxPetitionsPending' )
    }

    getMaxPetitionsPerPlayer(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxPetitionsPerPlayer' )
    }

    getMaxPlayerLevel(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxPlayerLevel' )
    }

    getMaxPvtStoreBuySlotsDwarf(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxPvtStoreBuySlotsDwarf' )
    }

    getMaxPvtStoreBuySlotsOther(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxPvtStoreBuySlotsOther' )
    }

    getMaxPvtStoreSellSlotsDwarf(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxPvtStoreSellSlotsDwarf' )
    }

    getMaxPvtStoreSellSlotsOther(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxPvtStoreSellSlotsOther' )
    }

    getMaxRunSpeed(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxRunSpeed' )
    }

    getMaxSpBonus(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxSpBonus' )
    }

    getMaxSubclass(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxSubclass' )
    }

    getMaxSubclassLevel(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxSubclassLevel' )
    }

    getMaxTriggeredBuffAmount(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxTriggeredBuffAmount' )
    }

    getMaximumFreightSlots(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaximumFreightSlots' )
    }

    getMaximumSlotsForDwarf(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaximumSlotsForDwarf' )
    }

    getMaximumSlotsForGMPlayer(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaximumSlotsForGMPlayer' )
    }

    getMaximumSlotsForNoDwarf(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaximumSlotsForNoDwarf' )
    }

    getMaximumSlotsForQuestItems(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaximumSlotsForQuestItems' )
    }

    getMaximumWarehouseSlotsForClan(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaximumWarehouseSlotsForClan' )
    }

    getMaximumWarehouseSlotsForDwarf(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaximumWarehouseSlotsForDwarf' )
    }

    getMaximumWarehouseSlotsForNoDwarf(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaximumWarehouseSlotsForNoDwarf' )
    }

    getMinAbnormalStateSuccessRate(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MinAbnormalStateSuccessRate' )
    }

    getMpRegenerationMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'MpRegenMultiplier' )
    }

    getNpcTalkBlockingTime(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'NpcTalkBlockingTime' )
    }

    getPartyRange(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'PartyRange' )
    }

    getPartyRange2(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'PartyRange2' )
    }

    getPartyXpCutoffGaps(): Array<[ number, number, number ]> {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'PartyXpCutoffGaps', getPartyXpCutoffGaps )
    }

    getPartyXpCutoffLevel(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'PartyXpCutoffLevel' )
    }

    getPartyXpCutoffMethod(): string {
        return configuration[ 'PartyXpCutoffMethod' ]
    }

    getPartyXpCutoffPercent(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'PartyXpCutoffPercent' )
    }

    getPerfectShieldBlockRate(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'PerfectShieldBlockRate' )
    }

    getPetNameTemplate(): RegExp {
        return DotEnvConfigurationEngineHelper.getRegex( configuration, cachedValues, 'PetNameTemplate' )
    }

    getPlayerFakeDeathUpProtection(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'PlayerFakeDeathUpProtection' )
    }

    getPlayerNameTemplate(): RegExp {
        return DotEnvConfigurationEngineHelper.getRegex( configuration, cachedValues, 'PlayerNameTemplate' )
    }

    getPlayerSpawnProtection(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'PlayerSpawnProtection' )
    }

    getPlayerSpawnProtectionAllowedItems(): Array<number> {
        return DotEnvConfigurationEngineHelper.getNumberArray( configuration, cachedValues, 'PlayerSpawnProtectionAllowedItems' )
    }

    getPlayerTeleportProtection(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'PlayerTeleportProtection' )
    }

    getRaidLootRightsCCSize(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'RaidLootRightsCCSize' )
    }

    getRaidLootRightsInterval(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'RaidLootRightsInterval' ) * 1000
    }

    getRespawnRestoreCP(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'RespawnRestoreCP' )
    }

    getRespawnRestoreHP(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'RespawnRestoreHP' )
    }

    getRespawnRestoreMP(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'RespawnRestoreMP' )
    }

    getRetailLikeAugmentationHighGradeChance(): Array<number> {
        return DotEnvConfigurationEngineHelper.getNumberArray( configuration, cachedValues, 'RetailLikeAugmentationHighGradeChance' )
    }

    getRetailLikeAugmentationMidGradeChance(): Array<number> {
        return DotEnvConfigurationEngineHelper.getNumberArray( configuration, cachedValues, 'RetailLikeAugmentationMidGradeChance' )
    }

    getRetailLikeAugmentationNoGradeChance(): Array<number> {
        return DotEnvConfigurationEngineHelper.getNumberArray( configuration, cachedValues, 'RetailLikeAugmentationNoGradeChance' )
    }

    getRetailLikeAugmentationTopGradeChance(): Array<number> {
        return DotEnvConfigurationEngineHelper.getNumberArray( configuration, cachedValues, 'RetailLikeAugmentationTopGradeChance' )
    }

    getRunSpeedBoost(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'RunSpeedBoost' )
    }

    getSkillDuration(): { [ key: number ]: number } {
        return DotEnvConfigurationEngineHelper.getNumberMap( configuration, cachedValues, 'SkillDuration' )
    }

    getSkillReuse(): { [ key: number ]: number } {
        return DotEnvConfigurationEngineHelper.getNumberMap( configuration, cachedValues, 'SkillReuse' )
    }

    getStartingAdena(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'StartingAdena' )
    }

    getStartingLevel(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'StartingLevel' )
    }

    getStartingSP(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'StartingSP' )
    }

    getTeleportWatchdogTimeout(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'TeleportWatchdogTimeout' ) * 1000
    }

    getUnstuckInterval(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'UnstuckInterval' ) * 1000
    }

    getWeightLimit(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'WeightLimit' )
    }

    initialEquipmentEvent(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'InitialEquipmentEvent' )
    }

    karmaPlayerCanBeKilledInPeaceZone(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'KarmaPlayerCanBeKilledInPeaceZone' )
    }

    karmaPlayerCanShop(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'KarmaPlayerCanShop' )
    }

    karmaPlayerCanTeleport(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'KarmaPlayerCanTeleport' )
    }

    karmaPlayerCanTrade(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'KarmaPlayerCanTrade' )
    }

    karmaPlayerCanUseGK(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'KarmaPlayerCanUseGK' )
    }

    karmaPlayerCanUseWareHouse(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'KarmaPlayerCanUseWareHouse' )
    }

    leavePartyLeader(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'LeavePartyLeader' )
    }

    lifeCrystalNeeded(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'LifeCrystalNeeded' )
    }

    async load(): Promise<void> {
        configuration = await DotEnvConfigurationEngineHelper.getConfig( 'character.properties' )
        cachedValues = {}
        return
    }

    magicFailures(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'MagicFailures' )
    }

    membersCanWithdrawFromClanWH(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'MembersCanWithdrawFromClanWH' )
    }

    modifySkillDuration(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'ModifySkillDuration' )
    }

    modifySkillReuse(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'ModifySkillReuse' )
    }

    offsetOnTeleport(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'OffsetOnTeleport' )
    }

    petitioningAllowed(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'PetitioningAllowed' )
    }

    randomRespawnInTown(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'RandomRespawnInTown' )
    }

    removeCastleCirclets(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'RemoveCastleCirclets' )
    }

    restorePetOnReconnect(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'RestorePetOnReconnect' )
    }

    restoreServitorOnReconnect(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'RestoreServitorOnReconnect' )
    }

    retailLikeAugmentation(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'RetailLikeAugmentation' )
    }

    retailLikeAugmentationAccessory(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'RetailLikeAugmentationAccessory' )
    }

    shieldBlocks(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'ShieldBlocks' )
    }

    silenceModeExclude(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'SilenceModeExclude' )
    }

    skillLearn(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'SkillLearn' )
    }

    storeDances(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'StoreDances' )
    }

    storeRecipeShopList(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'StoreRecipeShopList' )
    }

    storeSkillCooltime(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'StoreSkillCooltime' )
    }

    storeUISettings(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'StoreUISettings' )
    }

    subclassEverywhere(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'SubclassEverywhere' )
    }

    subclassStoreSkillCooltime(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'SubclassStoreSkillCooltime' )
    }

    subclassWithoutQuests(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'SubclassWithoutQuests' )
    }

    summonStoreSkillCooltime(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'SummonStoreSkillCooltime' )
    }

    transformationWithoutQuest(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'TransformationWithoutQuest' )
    }

    tutorial(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'Tutorial' )
    }

    validateTriggerSkills(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'ValidateTriggerSkills' )
    }
}

export const DotEnvCharacterConfiguration: L2CharacterConfigurationApi = new Configuration()