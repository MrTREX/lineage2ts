import { L2GrandBossConfigurationApi } from '../../interface/GrandBossConfigurationApi'
import { DotEnvConfigurationEngineHelper } from '../../engine/dotenv'
import { L2ConfigurationSubscription } from '../../IConfiguration'

let configuration: { [ name: string ]: string }
let cachedValues: { [ name: string ]: any }

class Configuration extends L2ConfigurationSubscription implements L2GrandBossConfigurationApi {
    getAntharasWaitTime(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'AntharasWaitTime', 60000 )
    }

    getBelethMinPlayers(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'BelethMinPlayers' )
    }

    getIntervalOfAntharasSpawn(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'IntervalOfAntharasSpawn' )
    }

    getIntervalOfBaiumSpawn(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'IntervalOfBaiumSpawn' )
    }

    getIntervalOfBelethSpawn(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'IntervalOfBelethSpawn' )
    }

    getIntervalOfCoreSpawn(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'IntervalOfCoreSpawn' )
    }

    getIntervalOfOrfenSpawn(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'IntervalOfOrfenSpawn' )
    }

    getIntervalOfQueenAntSpawn(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'IntervalOfQueenAntSpawn' )
    }

    getIntervalOfValakasSpawn(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'IntervalOfValakasSpawn' )
    }

    getRandomOfAntharasSpawn(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'RandomOfAntharasSpawn' )
    }

    getRandomOfBaiumSpawn(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'RandomOfBaiumSpawn' )
    }

    getRandomOfBelethSpawn(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'RandomOfBelethSpawn' )
    }

    getRandomOfCoreSpawn(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'RandomOfCoreSpawn' )
    }

    getRandomOfOrfenSpawn(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'RandomOfOrfenSpawn' )
    }

    getRandomOfQueenAntSpawn(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'RandomOfQueenAntSpawn' )
    }

    getRandomOfValakasSpawn(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'RandomOfValakasSpawn' )
    }

    getValakasWaitTime(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ValakasWaitTime', 60000 )
    }

    async load(): Promise<void> {
        configuration = await DotEnvConfigurationEngineHelper.getConfig( 'grandboss.properties' )
        cachedValues = {}
        return
    }
}

export const DotEnvGrandBossConfiguration: L2GrandBossConfigurationApi = new Configuration()