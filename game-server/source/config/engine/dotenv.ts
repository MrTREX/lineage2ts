import { L2IConfigEngine } from '../IConfigEngine'
import { DotEnvCharacterConfiguration } from '../type/dotenv/CharacterConfiguration'
import { DotEnvClanHallConfiguration } from '../type/dotenv/ClanHallConfiguration'
import { DotEnvClanConfiguration } from '../type/dotenv/ClanConfiguration'
import { DotEnvCastleConfiguration } from '../type/dotenv/CastleConfiguration'
import { DotEnvCustomsConfiguration } from '../type/dotenv/CustomsConfiguration'
import { DotEnvDatabaseConfiguration } from '../type/dotenv/DatabaseConfiguration'
import { DotEnvDataConfiguration } from '../type/dotenv/DataConfiguration'
import { DotEnvFortressConfiguration } from '../type/dotenv/FortressConfiguration'
import { DotEnvFortSiegeConfiguration } from '../type/dotenv/FortSiegeConfiguration'
import { DotEnvGeneralConfiguration } from '../type/dotenv/GeneralConfiguration'
import { DotEnvGraciaSeedsConfiguration } from '../type/dotenv/GraciaSeedsConfiguration'
import { DotEnvGrandBossConfiguration } from '../type/dotenv/GrandBossConfiguration'
import { DotEnvNpcConfiguration } from '../type/dotenv/NpcConfiguration'
import { DotEnvOlympiadConfiguration } from '../type/dotenv/OlympiadConfiguration'
import { DotEnvPvPConfiguration } from '../type/dotenv/PvPConfiguration'
import { DotEnvRatesConfiguration } from '../type/dotenv/RatesConfiguration'
import { DotEnvServerConfiguration } from '../type/dotenv/ServerConfiguration'
import { DotEnvSevenSignsConfiguration } from '../type/dotenv/SevenSignsConfiguration'
import { DotEnvSiegeConfiguration } from '../type/dotenv/SiegeConfiguration'
import { DotEnvTerritoryWarConfiguration } from '../type/dotenv/TerritoryWarConfiguration'
import { DotEnvTuningConfiguration } from '../type/dotenv/TuningConfiguration'
import { DotEnvTvTConfiguration } from '../type/dotenv/TvTConfiguration'
import { DotEnvVitalityConfiguration } from '../type/dotenv/VitalityConfiguration'
import { DotEnvTeleporterConfiguration } from '../type/dotenv/TeleporterConfiguration'
import { DotEnvDiagnosticConfiguration } from '../type/dotenv/DiagnosticConfiguration'
import { DotEnvRangeConfiguration } from '../type/dotenv/RangeConfiguration'
import _ from 'lodash'
import pkgDir from 'pkg-dir'
import dotenv from 'dotenv'
import * as fs from 'fs/promises'

const rootDirectory = pkgDir.sync( __dirname )
const pathPrefix = `${ rootDirectory }/configuration`

export const DotEnvConfigurationEngine: L2IConfigEngine = {
    castle: DotEnvCastleConfiguration,
    character: DotEnvCharacterConfiguration,
    clan: DotEnvClanConfiguration,
    clanhall: DotEnvClanHallConfiguration,
    customs: DotEnvCustomsConfiguration,
    data: DotEnvDataConfiguration,
    database: DotEnvDatabaseConfiguration,
    fortSiege: DotEnvFortSiegeConfiguration,
    fortress: DotEnvFortressConfiguration,
    general: DotEnvGeneralConfiguration,
    graciaSeeds: DotEnvGraciaSeedsConfiguration,
    grandBoss: DotEnvGrandBossConfiguration,
    npc: DotEnvNpcConfiguration,
    olympiad: DotEnvOlympiadConfiguration,
    pvp: DotEnvPvPConfiguration,
    rates: DotEnvRatesConfiguration,
    server: DotEnvServerConfiguration,
    sevenSigns: DotEnvSevenSignsConfiguration,
    siege: DotEnvSiegeConfiguration,
    territoryWar: DotEnvTerritoryWarConfiguration,
    tuning: DotEnvTuningConfiguration,
    tvt: DotEnvTvTConfiguration,
    vitality: DotEnvVitalityConfiguration,
    teleporter: DotEnvTeleporterConfiguration,
    diagnostic: DotEnvDiagnosticConfiguration,
    range: DotEnvRangeConfiguration
}

export const DotEnvConfigurationEngineHelper = {
    async getConfig( fileName: string ): Promise<{ [ key: string ]: string }> {
        return dotenv.parse( await fs.readFile( `${ pathPrefix }/${ fileName }` ) )
    },

    getBoolean( configuration: { [ name: string ]: string }, cache: { [ name: string ]: any }, name: string ): boolean {
        if ( _.isUndefined( cache[ name ] ) ) {
            cache[ name ] = configuration[ name ] === 'True'
        }

        return cache[ name ]
    },

    getNumber( configuration: { [ name: string ]: string }, cache: { [ name: string ]: any }, name: string, multiplier: number = 1 ): number {
        if ( _.isUndefined( cache[ name ] ) ) {
            cache[ name ] = _.parseInt( configuration[ name ] ) * multiplier
        }

        return cache[ name ]
    },

    getNumberArray( configuration: { [ name: string ]: string }, cache: { [ name: string ]: any }, name: string, separator: string = ',' ): Array<number> {
        if ( _.isUndefined( cache[ name ] ) ) {
            let value = configuration[ name ]
            cache[ name ] = _.isEmpty( value ) ? [] : _.split( value, separator ).map( value => _.parseInt( value ) )
        }

        return cache[ name ]
    },

    getNumberSet( configuration: { [ name: string ]: string }, cache: { [ name: string ]: any }, name: string, separator: string = ',' ): Set<number> {
        if ( _.isUndefined( cache[ name ] ) ) {
            let value = configuration[ name ]
            cache[ name ] = _.isEmpty( value ) ? new Set<number>() : new Set<number>( _.map( _.split( value, separator ), value => _.parseInt( value ) ) )
        }

        return cache[ name ]
    },

    getFloat( configuration: { [ name: string ]: string }, cache: { [ name: string ]: any }, name: string ): number {
        if ( _.isUndefined( cache[ name ] ) ) {
            let value = configuration[ name ]
            cache[ name ] = _.isEmpty( value ) ? 0 : parseFloat( configuration[ name ] )
        }

        return cache[ name ]
    },

    getFloatArray( configuration: { [ name: string ]: string }, cache: { [ name: string ]: any }, name: string, separator: string = ',' ): Array<number> {
        if ( _.isUndefined( cache[ name ] ) ) {
            let value = configuration[ name ]
            cache[ name ] = _.isEmpty( value ) ? [] : _.split( configuration[ name ], separator ).map( value => parseFloat( value ) )
        }

        return cache[ name ]
    },

    getRegex( configuration: { [ name: string ]: string }, cache: { [ name: string ]: any }, name: string ) {
        if ( _.isUndefined( cache[ name ] ) ) {
            cache[ name ] = new RegExp( configuration[ name ] )
        }

        return cache[ name ]
    },

    getNumberMap( configuration: { [ name: string ]: string }, cache: { [ name: string ]: any }, name: string ): { [ key: number ]: number } {
        if ( _.isUndefined( cache[ name ] ) ) {
            let value = configuration[ name ]
            cache[ name ] = _.isEmpty( value ) ? {} : _.reduce( _.split( configuration[ name ], ';' ), ( totals: { [ key: number ]: number }, line: string ) => {
                let [ key, value ] = _.split( line, ',' ).map( value => _.parseInt( value ) )

                totals[ key ] = value

                return totals
            }, {} )
        }

        return cache[ name ]
    },

    getFloatMap( configuration: { [ name: string ]: string }, cache: { [ name: string ]: any }, name: string ): { [ key: number ]: number } {
        if ( _.isUndefined( cache[ name ] ) ) {
            let value = configuration[ name ]
            cache[ name ] = _.isEmpty( value ) ? {} : _.reduce( _.split( configuration[ name ], ';' ), ( totals: { [ key: number ]: number }, line: string ) => {
                let [ key, value ] = _.split( line, ',' )

                totals[ _.parseInt( key ) ] = parseFloat( value )

                return totals
            }, {} )
        }

        return cache[ name ]
    },

    getStringArray( configuration: { [ name: string ]: string }, cache: { [ name: string ]: any }, name: string , separator: string = ',' ): Array<string> {
        if ( _.isUndefined( cache[ name ] ) ) {
            cache[ name ] = _.split( configuration[ name ], separator )
        }

        return cache[ name ]
    },

    getStringSet( configuration: { [ name: string ]: string }, cache: { [ name: string ]: any }, name: string , separator: string = ',' ): Set<string> {
        if ( _.isUndefined( cache[ name ] ) ) {
            cache[ name ] = new Set<string>( _.split( configuration[ name ], separator ) )
        }

        return cache[ name ]
    },

    getHexNumber( configuration: { [ name: string ]: string }, cache: { [ name: string ]: any }, name: string , defaultValue: number ): number {
        if ( _.isUndefined( cache[ name ] ) ) {
            cache[ name ] = _.isEmpty( configuration[ name ] ) ? defaultValue : _.parseInt( `0x${configuration[ name ]}` )
        }

        return cache[ name ]
    },

    getStringMap( configuration: { [ name: string ]: string }, cache: { [ name: string ]: any }, name: string ): { [ key: string ]: string } {
        if ( _.isUndefined( cache[ name ] ) ) {
            let value = configuration[ name ]
            cache[ name ] = _.isEmpty( value ) ? {} : _.reduce( _.split( configuration[ name ] , ';' ), ( totals: { [ key: number ]: number }, line: string ) => {
                let [ key, value ] = _.split( line, ',' )

                totals[ key ] = value

                return totals
            }, {} )
        }

        return cache[ name ]
    },

    getCustomValue( configuration: { [ name: string ]: string }, cache: { [ name: string ]: any }, name: string, method: ( value: string ) => unknown ) : any {
        if ( _.isUndefined( cache[ name ] ) ) {
            cache[ name ] = method( configuration[ name ] as string )
        }

        return cache[ name ]
    }
}