import { L2ConfigurationApi } from '../IConfiguration'

export interface L2PvPConfigurationApi extends L2ConfigurationApi {

    awardPKKillPVPPoint(): boolean

    canGMDropEquipment(): boolean

    getMinimumPKRequiredToDrop(): number

    getNonDroppableItems(): Array<number>

    getPetItems(): Array<number>

    getPvPVsNormalTime(): number


    getPvPVsPvPTime(): number
}