import { L2ConfigurationApi } from '../IConfiguration'

export interface L2ClanConfigurationApi extends L2ConfigurationApi {

    get10thRaidRankingPoints(): number

    get1stRaidRankingPoints(): number

    get2ndRaidRankingPoints(): number

    get3rdRaidRankingPoints(): number

    get4thRaidRankingPoints(): number

    get5thRaidRankingPoints(): number

    get6thRaidRankingPoints(): number

    get7thRaidRankingPoints(): number

    get8thRaidRankingPoints(): number

    get9thRaidRankingPoints(): number

    getBloodAlliancePoints(): number

    getBloodOathPoints(): number

    getCastleDefendedPoints(): number

    getClanLevel10Cost(): number

    getClanLevel10Requirement(): number

    getClanLevel11Cost(): number

    getClanLevel11Requirement(): number

    getClanLevel6Cost(): number

    getClanLevel6Requirement(): number

    getClanLevel7Cost(): number

    getClanLevel7Requirement(): number

    getClanLevel8Cost(): number

    getClanLevel8Requirement(): number

    getClanLevel9Cost(): number

    getClanLevel9Requirement(): number

    getClanNameTemplate(): RegExp

    getCompleteAcademyMaxPoints(): number

    getCompleteAcademyMinPoints(): number

    getCreateKnightUnitCost(): number

    getCreateRoyalGuardCost(): number

    getFestivalOfDarknessWin(): number

    getHeroPoints(): number

    getKillBallistaPoints(): number

    getKnightsEpaulettePoints(): number

    getLoseCastlePoints(): number

    getLoseFortPoints(): number

    getReinforceKnightUnitCost(): number

    getReputationScorePerKill(): number

    getTakeCastlePoints(): number

    getTakeFortPoints(): number

    getUpTo100thRaidRankingPoints(): number

    getUpTo50thRaidRankingPoints(): number

    getMinimumPlayerLevel() : number
}