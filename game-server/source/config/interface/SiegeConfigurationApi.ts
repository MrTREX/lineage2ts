import { L2ConfigurationApi } from '../IConfiguration'

export interface L2SiegeConfigurationApi extends L2ConfigurationApi {

    getAdenFlameTower2(): L2SiegeConfigurationTowerSpawn

    getAdenControlTower1(): L2SiegeConfigurationTowerSpawn

    getAdenControlTower2(): L2SiegeConfigurationTowerSpawn

    getAdenControlTower3(): L2SiegeConfigurationTowerSpawn

    getAdenFlameTower1(): L2SiegeConfigurationTowerSpawn

    getAdenMaxMercenaries(): number

    getAttackerMaxClans(): number

    getAttackerRespawn(): number

    getBloodAllianceReward(): number

    getClanMinLevel(): number

    getDefenderMaxClans(): number

    getDionControlTower1(): L2SiegeConfigurationTowerSpawn

    getDionControlTower2(): L2SiegeConfigurationTowerSpawn

    getDionControlTower3(): L2SiegeConfigurationTowerSpawn

    getDionFlameTower1(): L2SiegeConfigurationTowerSpawn

    getDionFlameTower2(): L2SiegeConfigurationTowerSpawn

    getDionMaxMercenaries(): number

    getGiranControlTower1(): L2SiegeConfigurationTowerSpawn

    getGiranControlTower2(): L2SiegeConfigurationTowerSpawn

    getGiranControlTower3(): L2SiegeConfigurationTowerSpawn

    getGiranFlameTower1(): L2SiegeConfigurationTowerSpawn

    getGiranFlameTower2(): L2SiegeConfigurationTowerSpawn

    getGiranMaxMercenaries(): number

    getGludioControlTower1(): L2SiegeConfigurationTowerSpawn

    getGludioControlTower2(): L2SiegeConfigurationTowerSpawn

    getGludioControlTower3(): L2SiegeConfigurationTowerSpawn

    getGludioFlameTower1(): L2SiegeConfigurationTowerSpawn

    getGludioFlameTower2(): L2SiegeConfigurationTowerSpawn

    getGludioMaxMercenaries(): number

    getGoddardControlTower1(): L2SiegeConfigurationTowerSpawn

    getGoddardControlTower2(): L2SiegeConfigurationTowerSpawn

    getGoddardControlTower3(): L2SiegeConfigurationTowerSpawn

    getGoddardFlameTower1(): L2SiegeConfigurationTowerSpawn

    getGoddardFlameTower2(): L2SiegeConfigurationTowerSpawn

    getGoddardMaxMercenaries(): number

    getInnadrilControlTower1(): L2SiegeConfigurationTowerSpawn

    getInnadrilControlTower2(): L2SiegeConfigurationTowerSpawn

    getInnadrilControlTower3(): L2SiegeConfigurationTowerSpawn

    getInnadrilFlameTower1(): L2SiegeConfigurationTowerSpawn

    getInnadrilFlameTower2(): L2SiegeConfigurationTowerSpawn

    getInnadrilMaxMercenaries(): number

    getMaxFlags(): number

    getOrenControlTower1(): L2SiegeConfigurationTowerSpawn

    getOrenControlTower2(): L2SiegeConfigurationTowerSpawn

    getOrenControlTower3(): L2SiegeConfigurationTowerSpawn

    getOrenFlameTower1(): L2SiegeConfigurationTowerSpawn

    getOrenFlameTower2(): L2SiegeConfigurationTowerSpawn

    getOrenMaxMercenaries(): number

    getRuneControlTower1(): L2SiegeConfigurationTowerSpawn

    getRuneControlTower2(): L2SiegeConfigurationTowerSpawn

    getRuneControlTower3(): L2SiegeConfigurationTowerSpawn

    getRuneFlameTower1(): L2SiegeConfigurationTowerSpawn

    getRuneFlameTower2(): L2SiegeConfigurationTowerSpawn

    getRuneMaxMercenaries(): number

    getSchuttgartControlTower1(): L2SiegeConfigurationTowerSpawn

    getSchuttgartControlTower2(): L2SiegeConfigurationTowerSpawn

    getSchuttgartControlTower3(): L2SiegeConfigurationTowerSpawn

    getSchuttgartFlameTower1(): L2SiegeConfigurationTowerSpawn

    getSchuttgartFlameTower2(): L2SiegeConfigurationTowerSpawn

    getSchuttgartMaxMercenaries(): number

    getSiegeLength(): number
}

export interface L2SiegeConfigurationTowerSpawn {
    x: number
    y: number
    z: number
    npcId: number
    zoneIds: Array<number>
}