import { L2ConfigurationApi } from '../IConfiguration'

export interface L2ServerConfigurationApi extends L2ConfigurationApi {

    acceptAlternateId(): boolean

    getAllowedProtocolRevisions(): Array<number>

    getLoginHost(): string

    getLoginPort(): number

    getMaxOnlineUsers(): number

    getRequestServerId(): number

    getServerPort(): number

    getExternalServerIp() : Array<number>

    getPacketDebounceInterval() : number

    getLoginServerReconnectInterval() : number

    getServerListAge(): number

    getServerListBrackets(): boolean

    getServerListType(): string
    randomizeCharacterSelection() : boolean

    isCommandLinkEnabled() : boolean
    getCommandLinkEndpoint() : string
    getCommandLinkPermissions() : Set<string>
    isGeodataCanUnload() : boolean
}