import { L2ConfigurationApi } from '../IConfiguration'

export interface L2DiagnosticConfigurationApi extends L2ConfigurationApi {
    isLoadListeners(): boolean
    showIncomingPackets() : boolean
    showDebugMessages() : boolean
    showEventTypeMessages() : boolean
}