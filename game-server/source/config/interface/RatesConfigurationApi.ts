import { L2ConfigurationApi } from '../IConfiguration'

export interface L2RatesConfigurationApi extends L2ConfigurationApi {

    getCorpseDropAmountMinMultiplier(): number
    getCorpseDropAmountMaxMultiplier(): number

    getCorpseDropChanceMultiplier(): number

    getDeathDropAmountMinMultiplier(): number
    getDeathDropAmountMaxMultiplier(): number

    getDeathDropChanceMultiplier(): number

    getDropEtcChanceMinMultiplier(): number
    getDropEtcChanceMaxMultiplier(): number

    getDropAmountMultiplierByItemId(): { [key: number]: number }

    getDropChanceMultiplierByItemId(): { [key: number]: number }

    getHerbDropAmountMultiplier(): number

    getHerbDropChanceMultiplier(): number

    getKarmaDropLimit(): number

    getKarmaRateDrop(): number

    getKarmaRateDropEquipment(): number

    getKarmaRateDropEquippedWeapon(): number

    getKarmaRateDropItem(): number

    getPetFoodRate(): number

    getPetXpRate(): number

    getPlayerDropLimit(): number

    getPlayerRateDrop(): number

    getPlayerRateDropEquipment(): number

    getPlayerRateDropEquippedWeapon(): number

    getPlayerRateDropItem(): number

    getRaidDropAmountMultiplier(): number

    getRaidDropChanceMultiplier(): number

    getRateDropManor(): number

    getRateExtractable(): number

    getHellboundPointsDecreaseMultiplier(): number

    getHellboundPointsIncreaseMultiplier(): number

    getRateKarmaExpLost(): number

    getRateKarmaLost(): number

    getRatePartySp(): number

    getRatePartyXp(): number

    getRateQuestDropMinimum(): number
    getRateQuestDropMaximum(): number
    getRateQuestDropChance(): number

    getRateQuestReward(): number

    getRateQuestRewardAdena(): number

    getRateQuestRewardMaterial(): number

    getRateQuestRewardPotion(): number

    getRateQuestRewardRecipe(): number

    getRateQuestRewardSP(): number

    getRateQuestRewardScroll(): number

    getRateQuestRewardXP(): number

    getRateSiegeGuardsPrice(): number

    getRateSp(): number

    getRateXp(): number

    getSinEaterXpRate(): number

    useQuestRewardMultipliers(): boolean

    getEtcDropAmountMaxMultiplier(): number

    getEtcDropAmountMinMultiplier(): number

    isQuestExpUsePlayerBonusModifier() : boolean
    isQuestSpUsePlayerBonusModifier() : boolean

    getPartyPlayerExpBonuses() : Array<number>
    getPartyPlayerSpBonuses() : Array<number>

    getExtractableScrollMultiplier(): number
    getExtractablePotionMultiplier(): number
    getExtractableRecipeMultiplier(): number
    getExtractableArmorMultiplier(): number
    getExtractableWeaponMultiplier(): number
}