import { L2ConfigurationApi } from '../IConfiguration'

export interface L2GraciaSeedsConfigurationApi extends L2ConfigurationApi {
    getStage2Length(): number

    getTiatKillCountForNextState(): number
}