import { L2ConfigurationApi } from '../IConfiguration'

export interface L2OlympiadConfigurationApi extends L2ConfigurationApi {

    announceGames(): boolean

    getBattlePeriod(): number

    getClassedParticipants(): number

    getClassedReward(): { [ key: number ] : number }

    getCompetitionPeriod(): number

    getCompetitionRewardItem(): number

    getCurrentCycle(): number

    getDividerClassed(): number

    getDividerNonClassed(): number

    getEnchantLimit(): number

    getGPPerPoint(): number

    getHeroPoints(): number

    getMaxBuffs(): number

    getMaxPoints(): number

    getMaxWeeklyMatches(): number

    getMaxWeeklyMatchesClassed(): number

    getMaxWeeklyMatchesNonClassed(): number

    getMaxWeeklyMatchesTeam(): number

    getMinMatchesForPoints(): number

    getNextWeeklyChange(): number

    getNonClassedParticipants(): number

    getNonClassedReward(): { [ key: number ] : number }

    getOlympiadEnd(): number

    getPeriod(): number

    getRank1Points(): number

    getRank2Points(): number

    getRank3Points(): number

    getRank4Points(): number

    getRank5Points(): number

    getRegistrationDisplayNumber(): number

    getRestrictedItems(): Array<number>

    getStartHour(): number

    getStartMinute(): number

    getStartPoints(): number

    getTeamReward(): { [ key: number ] : number }

    getTeamsParticipants(): number

    getValidationEnd(): number

    getValidationPeriod(): number

    getWaitTime(): number

    getWeeklyPeriod(): number

    getWeeklyPoints(): number

    logFights(): boolean

    showMonthlyWinners(): boolean
}