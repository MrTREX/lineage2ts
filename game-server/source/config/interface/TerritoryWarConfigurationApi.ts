import { L2ConfigurationApi } from '../IConfiguration'

export interface L2TerritoryWarConfigurationApi extends L2ConfigurationApi {

    getClanMinLevel(): number

    getDefenderMaxClans(): number

    getDefenderMaxPlayers(): number

    getMinTerritoryBadgeForBigStrider(): number

    getMinTerritoryBadgeForNobless(): number

    getMinTerritoryBadgeForStriders(): number

    getPlayerMinLevel(): number

    getWarLength(): number

    playerWithWardCanBeKilledInPeaceZone(): boolean

    returnWardsWhenTWStarts(): boolean

    spawnWardsWhenTWIsNotInProgress(): boolean
}