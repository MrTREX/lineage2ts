import { L2ConfigurationApi } from '../IConfiguration'

export interface L2NPCConfigurationApi extends L2ConfigurationApi {

    allowWyvernUpgrader(): number

    announceMammonSpawn(): boolean

    attackableNpcs(): boolean

    getCorpseConsumeSkillAllowedTimeBeforeDecay(): number

    getCritDamagePenaltyForLevelDifferences(): Array<number>

    getCustomMinionsRespawnTime(): { [key: number]: number }

    getDamagePenaltyForLevelDifferences(): Array<number>

    getCorpseTimePerEntityType() : { [ key: string ] : number }

    getDefaultCorpseTime(): number

    getDropAdenaMaxLevelDifference(): number

    getDropAdenaMinLevelDifference(): number

    getDropAdenaMinLevelGapChance(): number

    getDropItemMaxLevelDifference(): number

    getDropItemMinLevelDifference(): number

    getDropItemMinLevelGapChance(): number

    getGrandChaosTime(): number

    getMaxAggroRange(): number

    getMaxDriftRange(): number

    getMaximumSlotsForPet(): number

    getMinNPCLevelForDamagePenalty(): number

    getMinNPCLevelForMagicPenalty(): number

    getMinionChaosTime(): number

    getPetHpRegenerationMultiplier(): number

    getPetMpRegenerationMultiplier(): number

    getPetRentNPCs(): Array<number>

    getRaidChaosTime(): number

    getRaidHpRegenerationMultiplier(): number

    getRaidMagicAttackMultiplier(): number

    getRaidMagicDefenceMultiplier(): number

    getRaidMaxRespawnMultiplier(): number

    getRaidMinRespawnMultiplier(): number

    getRaidMinionRespawnTime(): number

    getRaidMpRegenerationMultiplier(): number

    getRaidPowerAttackMultiplier(): number

    getRaidPowerDefenceMultiplier(): number

    getSkillChancePenaltyForLevelDifferences(): Array<number>

    getSkillDamagePenaltyForLevelDifferences(): Array<number>

    getSpoiledCorpseExtendTime(): number

    guardAttackAggroMob(): boolean

    mobAggroInPeaceZone(): boolean

    raidCurse(): boolean

    randomEnchantEffect(): boolean

    showCrestWithoutQuest(): boolean

    showNpcCustomTitle(): boolean

    showNpcCustomTitleTemplate(): string

    showNpcCustomTitleAggressiveMark() : string

    useDeepBlueDropRules(): boolean

    useDeepBlueDropRulesRaid(): boolean

    randomWalkChance(): number

    getDisabledMerchantIds(): Array<number>

    isNpcHpMultiplierEnabled() : boolean
    getNpcHpMultiplierRatio() : number
}