import { L2ConfigurationApi } from '../IConfiguration'

export interface L2ClanHallConfigurationApi extends L2ConfigurationApi {

    enableFame(): boolean

    getCurtainFunctionFeeLvl1(): number

    getCurtainFunctionFeeLvl2(): number

    getCurtainFunctionFeeRatio(): number

    getExpRegenerationFeeLvl1(): number

    getExpRegenerationFeeLvl2(): number

    getExpRegenerationFeeLvl3(): number

    getExpRegenerationFeeLvl4(): number

    getExpRegenerationFeeLvl5(): number

    getExpRegenerationFeeLvl6(): number

    getExpRegenerationFeeLvl7(): number

    getExpRegenerationFunctionFeeRatio(): number

    getFameAmount(): number

    getFameFrequency(): number

    getFrontPlatformFunctionFeeLvl1(): number

    getFrontPlatformFunctionFeeLvl2(): number

    getFrontPlatformFunctionFeeRatio(): number

    getHpRegenerationFeeLvl1(): number

    getHpRegenerationFeeLvl10(): number

    getHpRegenerationFeeLvl11(): number

    getHpRegenerationFeeLvl12(): number

    getHpRegenerationFeeLvl13(): number

    getHpRegenerationFeeLvl2(): number

    getHpRegenerationFeeLvl3(): number

    getHpRegenerationFeeLvl4(): number

    getHpRegenerationFeeLvl5(): number

    getHpRegenerationFeeLvl6(): number

    getHpRegenerationFeeLvl7(): number

    getHpRegenerationFeeLvl8(): number

    getHpRegenerationFeeLvl9(): number

    getHpRegenerationFunctionFeeRatio(): number

    getItemCreationFunctionFeeLvl1(): number

    getItemCreationFunctionFeeLvl2(): number

    getItemCreationFunctionFeeLvl3(): number

    getItemCreationFunctionFeeRatio(): number

    getMaxAttackers(): number

    getMaxFlagsPerClan(): number

    getMinClanLevel(): number

    getMpRegenerationFeeLvl1(): number

    getMpRegenerationFeeLvl2(): number

    getMpRegenerationFeeLvl3(): number

    getMpRegenerationFeeLvl4(): number

    getMpRegenerationFeeLvl5(): number

    getMpRegenerationFunctionFeeRatio(): number

    getSupportFeeLvl1(): number

    getSupportFeeLvl2(): number

    getSupportFeeLvl3(): number

    getSupportFeeLvl4(): number

    getSupportFeeLvl5(): number

    getSupportFeeLvl6(): number

    getSupportFeeLvl7(): number

    getSupportFeeLvl8(): number

    getSupportFunctionFeeRatio(): number

    getTeleportFunctionFeeLvl1(): number

    getTeleportFunctionFeeLvl2(): number

    getTeleportFunctionFeeRatio(): number

    mpBuffFree(): boolean

    getAuctionTransactionTax(): number
}