import { L2ConfigurationApi } from '../IConfiguration'

export interface L2TuningConfigurationApi extends L2ConfigurationApi {
    getCrystallizeCountMultiplier(): number

    getWarehouseItemDepositFee(): number

    getAirshipFuelConsumptionRate(): number

    getDropProtectionDuration(): number

    getNewbieTeleportLevelLimit(): number

    getReducedCostTeleportLevelLimit(): number

    getFullArmorHPBonusMultiplier() : number

    getWondrousCubicResetCron(): string

    getLevelupRestoreHP() : number
    getLevelupRestoreMP() : number
    getLevelupRestoreCP() : number

    getMailMessageFee() : number
    getMailMessageAttachmentFee() : number
    getMailMessageAttachmentsLimit() : number
    getMailInboxSize() : number
    getMailOutboxSize() : number

    isMailToSelfPermitted(): boolean

    isSowingUsingLeveledSeed() : boolean
    getSowingLevelsIncrement(): number

    getAutoDestroyInterval() : number
    getAutoDestroyHerbMillis() : number
    getAutoDestroyAdenaMillis() : number
    getAutoDestroyArmorMillis() : number
    getAutoDestroyWeaponMillis() : number
    getAutoDestroyRecipeMillis() : number
    getAutoDestroyOtherMillis() : number

    isManufactureRemoveRecipeIngredients() : boolean

    getWarehouseCacheIntervalMinutes() : number

    getPetFeedInterval() : number
    getPetControlLevelDifference() : number
    isPetShowingEarnedExp() : boolean
    getItemInventoryLimits() : Record<number, number>

    getBuylistProductRestockMultiplier() : number

    getShadowItemManaConsumeAmount() : number
    getShadowItemManaConsumeInterval() : number
}