import { L2ConfigurationApi } from '../IConfiguration'

export interface L2RangeConfigurationApi extends L2ConfigurationApi {
    getChatNormalRange() : number
    getChatShoutRange() : number
    getChatTradeRange() : number
}