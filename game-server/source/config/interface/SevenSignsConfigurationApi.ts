import { L2ConfigurationApi } from '../IConfiguration'

export interface L2SevenSignsConfigurationApi extends L2ConfigurationApi {

    castleForDawn(): boolean

    castleForDusk(): boolean

    getDawnGatesMagicDefenceMultiplier(): number

    getDawnGatesPowerDefenceMultiplier(): number

    getDuskGatesMagicDefenceMultiplier(): number

    getDuskGatesPowerDefenceMultiplier(): number

    getFestivalChestSpawn(): number

    getFestivalCycleLength(): number

    getFestivalFirstSpawn(): number

    getFestivalFirstSwarm(): number

    getFestivalLength(): number

    getFestivalManagerStart(): number

    getFestivalMinPlayer(): number

    getFestivalSecondSpawn(): number

    getFestivalSecondSwarm(): number

    getMaxPlayerContribution(): number

    getSevenSignsDawnTicketBundle(): number

    getSevenSignsDawnTicketPrice(): number

    getSevenSignsDawnTicketQuantity(): number

    getSevenSignsJoinDawnFee(): number

    getSevenSignsManorsAgreementId(): number

    requireClanCastle(): boolean

    strictSevenSigns(): boolean

    getBlueStoneContributionPoints(): number
    getGreenStoneContributionPoints() : number
    getRedStoneContributionPoints() : number

    getBlueStoneAAReward() : number
    getGreenStoneAAReward() : number
    getRedStoneAAReward(): number

    getPeriodStartCron() : string
    getCompetitionPeriodDuration() : number
    getInitialPeriodDuration() : number
}