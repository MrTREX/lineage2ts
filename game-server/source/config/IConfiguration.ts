export interface L2ConfigurationApi extends L2ConfigurationSubscription {
    load() : Promise<void>
}

export type L2ConfigurationSubscriptionFunction = () => void

export abstract class L2ConfigurationSubscription {
    subscriptions : Set<L2ConfigurationSubscriptionFunction> = new Set<L2ConfigurationSubscriptionFunction>()

    getSubscriptions(): Set<L2ConfigurationSubscriptionFunction> {
        return this.subscriptions
    }

    subscribe( method: L2ConfigurationSubscriptionFunction ): void {
        this.subscriptions.add( method )
    }
}