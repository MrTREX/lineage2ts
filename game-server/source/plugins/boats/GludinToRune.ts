import { L2ExecutablePlugin, L2ExecutablePluginStatus } from '../L2ExecutablePlugin'
import { L2BoatInstance } from '../../gameService/models/actor/instance/L2BoatInstance'
import { BoatManager, ShipDock } from '../../gameService/instancemanager/BoatManager'
import { ListenerCache } from '../../gameService/cache/ListenerCache'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import { EventType, VehiclePathEndedEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { VehiclePathPoint } from '../../gameService/models/VehiclePathPoint'
import { CreatureSay } from '../../gameService/packets/send/builder/CreatureSay'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { ILocational } from '../../gameService/models/Location'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { ConfigManager } from '../../config/ConfigManager'
import { PlaySound } from '../../gameService/packets/send/builder/PlaySound'
import { SoundNames } from '../../gameService/packets/send/SoundPacket'
import { PacketHelper } from '../../gameService/packets/PacketVariables'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const dockGludin: VehiclePathPoint = new VehiclePathPoint( -95686, 150514, -3610, 150, 800 )
const dockRune: VehiclePathPoint = new VehiclePathPoint( 34381, -37680, -3610, 200, 800 )
const broadcastLocations: Array<ILocational> = [ dockGludin, dockRune ]

const pathGludinToRune: Array<VehiclePathPoint> = [
    new VehiclePathPoint( -95686, 155514, -3610, 150, 800 ),
    new VehiclePathPoint( -98112, 159040, -3610, 150, 800 ),
    new VehiclePathPoint( -104192, 160608, -3610, 200, 1800 ),
    new VehiclePathPoint( -109952, 159616, -3610, 250, 1800 ),
    new VehiclePathPoint( -112768, 154784, -3610, 290, 1800 ),
    new VehiclePathPoint( -114688, 139040, -3610, 290, 1800 ),
    new VehiclePathPoint( -115232, 134368, -3610, 290, 1800 ),
    new VehiclePathPoint( -113888, 121696, -3610, 290, 1800 ),
    new VehiclePathPoint( -107808, 104928, -3610, 290, 1800 ),
    new VehiclePathPoint( -97152, 75520, -3610, 290, 800 ),
    new VehiclePathPoint( -85536, 67264, -3610, 290, 1800 ),
    new VehiclePathPoint( -64640, 55840, -3610, 290, 1800 ),
    new VehiclePathPoint( -60096, 44672, -3610, 290, 1800 ),
    new VehiclePathPoint( -52672, 37440, -3610, 290, 1800 ),
    new VehiclePathPoint( -46144, 33184, -3610, 290, 1800 ),
    new VehiclePathPoint( -36096, 24928, -3610, 290, 1800 ),
    new VehiclePathPoint( -33792, 8448, -3610, 290, 1800 ),
    new VehiclePathPoint( -23776, 3424, -3610, 290, 1000 ),
    new VehiclePathPoint( -12000, -1760, -3610, 290, 1000 ),
    new VehiclePathPoint( 672, 480, -3610, 290, 1800 ),
    new VehiclePathPoint( 15488, 200, -3610, 290, 1000 ),
    new VehiclePathPoint( 24736, 164, -3610, 290, 1000 ),
    new VehiclePathPoint( 32192, -1156, -3610, 290, 1000 ),
    new VehiclePathPoint( 39200, -8032, -3610, 270, 1000 ),
    new VehiclePathPoint( 44320, -25152, -3610, 270, 1000 ),
    new VehiclePathPoint( 40576, -31616, -3610, 250, 800 ),
    new VehiclePathPoint( 36819, -35315, -3610, 220, 800 ),
]

const pathRuneToGludin: Array<VehiclePathPoint> = [
    new VehiclePathPoint( 32750, -39300, -3610, 150, 800 ),
    new VehiclePathPoint( 27440, -39328, -3610, 180, 1000 ),
    new VehiclePathPoint( 21456, -34272, -3610, 200, 1000 ),
    new VehiclePathPoint( 6608, -29520, -3610, 250, 800 ),
    new VehiclePathPoint( 4160, -27828, -3610, 270, 800 ),
    new VehiclePathPoint( 2432, -25472, -3610, 270, 1000 ),
    new VehiclePathPoint( -8000, -16272, -3610, 220, 1000 ),
    new VehiclePathPoint( -18976, -9760, -3610, 290, 800 ),
    new VehiclePathPoint( -23776, 3408, -3610, 290, 800 ),
    new VehiclePathPoint( -33792, 8432, -3610, 290, 800 ),
    new VehiclePathPoint( -36096, 24912, -3610, 290, 800 ),
    new VehiclePathPoint( -46144, 33184, -3610, 290, 800 ),
    new VehiclePathPoint( -52688, 37440, -3610, 290, 800 ),
    new VehiclePathPoint( -60096, 44672, -3610, 290, 800 ),
    new VehiclePathPoint( -64640, 55840, -3610, 290, 800 ),
    new VehiclePathPoint( -85552, 67248, -3610, 290, 800 ),
    new VehiclePathPoint( -97168, 85264, -3610, 290, 800 ),
    new VehiclePathPoint( -107824, 104912, -3610, 290, 800 ),
    new VehiclePathPoint( -102151, 135704, -3610, 290, 800 ),
    new VehiclePathPoint( -96686, 140595, -3610, 290, 800 ),
    new VehiclePathPoint( -95686, 147717, -3610, 250, 800 ),
    new VehiclePathPoint( -95686, 148218, -3610, 200, 800 ),
]

const ARRIVED_AT_GLUDIN: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_ARRIVED_AT_GLUDIN ).getBuffer() )
const ARRIVED_AT_GLUDIN_2: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.DEPARTURE_FOR_RUNE_10_MINUTES ).getBuffer() )
const LEAVE_GLUDIN5: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.DEPARTURE_FOR_RUNE_5_MINUTES ).getBuffer() )
const LEAVE_GLUDIN1: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.DEPARTURE_FOR_RUNE_1_MINUTE ).getBuffer() )
const LEAVE_GLUDIN0: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.DEPARTURE_FOR_GLUDIN_SHORTLY2 ).getBuffer() )
const LEAVING_GLUDIN: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.DEPARTURE_FOR_GLUDIN_NOW ).getBuffer() )
const ARRIVED_AT_RUNE: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.ARRIVED_AT_RUNE ).getBuffer() )
const ARRIVED_AT_RUNE_2: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_LEAVE_FOR_GLUDIN_AFTER_10_MINUTES ).getBuffer() )
const LEAVE_RUNE5: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.DEPARTURE_FOR_GLUDIN_5_MINUTES ).getBuffer() )
const LEAVE_RUNE1: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.DEPARTURE_FOR_GLUDIN_1_MINUTE ).getBuffer() )
const LEAVE_RUNE0: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.DEPARTURE_FOR_GLUDIN_SHORTLY ).getBuffer() )
const LEAVING_RUNE: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.DEPARTURE_FOR_GLUDIN_NOW ).getBuffer() )
const BUSY_GLUDIN: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_RUNE_GLUDIN_DELAYED ).getBuffer() )
const BUSY_RUNE: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_GLUDIN_RUNE_DELAYED ).getBuffer() )

const ARRIVAL_RUNE15: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_FROM_GLUDIN_AT_RUNE_15_MINUTES ).getBuffer() )
const ARRIVAL_RUNE10: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_FROM_GLUDIN_AT_RUNE_10_MINUTES ).getBuffer() )
const ARRIVAL_RUNE5: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_FROM_GLUDIN_AT_RUNE_5_MINUTES ).getBuffer() )
const ARRIVAL_RUNE1: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_FROM_GLUDIN_AT_RUNE_1_MINUTE ).getBuffer() )
const ARRIVAL_GLUDIN15: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_FROM_RUNE_AT_GLUDIN_15_MINUTES ).getBuffer() )
const ARRIVAL_GLUDIN10: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_FROM_RUNE_AT_GLUDIN_10_MINUTES ).getBuffer() )
const ARRIVAL_GLUDIN5: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_FROM_RUNE_AT_GLUDIN_5_MINUTES ).getBuffer() )
const ARRIVAL_GLUDIN1: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_FROM_RUNE_AT_GLUDIN_1_MINUTE ).getBuffer() )

let shoutCount = 0
let currentCycle = 0

export const GludinToRune: L2ExecutablePlugin = {
    getId(): string {
        return 'GludinToRune Boat'
    },

    getStatus(): L2ExecutablePluginStatus {
        return undefined
    },

    async onEnd(): Promise<void> {
        shoutCount = 0
        currentCycle = 0
    },

    async onStart(): Promise<void> {
        let boat: L2BoatInstance = BoatManager.registerBoat( 3, -95686, 150514, -3610, 16723 )

        if ( !boat ) {
            return
        }

        ListenerCache.registerListener( ListenerRegisterType.WorldObject, EventType.VehiclePathEnded, null, Helper.runCycle, [ boat.getObjectId() ] )

        let data: VehiclePathEndedEvent = {
            objectId: boat.getObjectId(),
        }

        setTimeout( Helper.runCycle, 180000, data )
        BoatManager.dockShip( ShipDock.GludinHarbor, true )
    },
}

const Helper = {
    async runCycle( data: VehiclePathEndedEvent ): Promise<void> {
        let boat: L2BoatInstance = L2World.getObjectById( data.objectId ) as L2BoatInstance

        if ( !boat ) {
            return
        }

        switch ( currentCycle ) {
            case 0:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        LEAVE_GLUDIN5 )
                setTimeout( Helper.runCycle, 240000, data )
                break

            case 1:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        LEAVE_GLUDIN1 )
                setTimeout( Helper.runCycle, 40000, data )
                break

            case 2:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        LEAVE_GLUDIN0 )
                setTimeout( Helper.runCycle, 20000, data )
                break

            case 3:
                BoatManager.dockShip( ShipDock.GludinHarbor, false )
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        LEAVING_GLUDIN )
                BroadcastHelper.broadcastDataAtLocations( [ dockGludin ],
                        ConfigManager.general.getBoatBroadcastRadius(),
                        PlaySound.createSound( SoundNames.ITEMSOUND_SHIP_ARRIVAL_DEPARTURE, boat ) )

                await boat.payForRide( 7905, 1, -90015, 150422, -3610 )
                await BoatManager.setBoathPath( data.objectId, pathGludinToRune )
                setTimeout( Helper.runCycle, 250000, data )
                break

            case 4:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        ARRIVAL_RUNE15 )
                setTimeout( Helper.runCycle, 300000, data )
                break

            case 5:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        ARRIVAL_RUNE10 )
                setTimeout( Helper.runCycle, 300000, data )
                break

            case 6:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        ARRIVAL_RUNE5 )
                setTimeout( Helper.runCycle, 240000, data )
                break

            case 7:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        ARRIVAL_RUNE1 )
                break

            case 8:
                if ( BoatManager.isDockBusy( ShipDock.RuneHarbor ) ) {
                    if ( shoutCount === 0 ) {
                        BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                                ConfigManager.general.getBoatBroadcastRadius(),
                                BUSY_RUNE )
                    }

                    shoutCount = ( shoutCount + 1 ) % 35

                    setTimeout( Helper.runCycle, 5000, data )
                    return
                }

                await BoatManager.setBoathPath( data.objectId, [ dockRune ] )
                break

            case 9:
                BoatManager.dockShip( ShipDock.RuneHarbor, true )
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        ARRIVED_AT_RUNE, ARRIVED_AT_RUNE_2 )

                BroadcastHelper.broadcastDataAtLocations( [ dockRune ],
                        ConfigManager.general.getBoatBroadcastRadius(),
                        PlaySound.createSound( SoundNames.ITEMSOUND_SHIP_ARRIVAL_DEPARTURE, boat ) )
                setTimeout( Helper.runCycle, 300000, data )
                break

            case 10:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        LEAVE_RUNE5 )
                setTimeout( Helper.runCycle, 240000, data )
                break

            case 11:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        LEAVE_RUNE1 )
                setTimeout( Helper.runCycle, 40000, data )
                break

            case 12:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        LEAVE_RUNE0 )
                setTimeout( Helper.runCycle, 20000, data )
                break

            case 13:
                BoatManager.dockShip( ShipDock.RuneHarbor, false )
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        LEAVING_RUNE )

                BroadcastHelper.broadcastDataAtLocations( [ dockRune ],
                        ConfigManager.general.getBoatBroadcastRadius(),
                        PlaySound.createSound( SoundNames.ITEMSOUND_SHIP_ARRIVAL_DEPARTURE, boat ) )

                await boat.payForRide( 7904, 1, 34513, -38009, -3640 )
                await BoatManager.setBoathPath( data.objectId, pathRuneToGludin )
                setTimeout( Helper.runCycle, 60000, data )
                break

            case 14:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        ARRIVAL_GLUDIN15 )
                setTimeout( Helper.runCycle, 300000, data )
                break

            case 15:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        ARRIVAL_GLUDIN10 )
                setTimeout( Helper.runCycle, 300000, data )
                break

            case 16:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        ARRIVAL_GLUDIN5 )
                setTimeout( Helper.runCycle, 240000, data )
                break

            case 17:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        ARRIVAL_GLUDIN1 )
                break

            case 18:
                if ( BoatManager.isDockBusy( ShipDock.GludinHarbor ) ) {
                    if ( shoutCount === 0 ) {
                        BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                                ConfigManager.general.getBoatBroadcastRadius(),
                                BUSY_GLUDIN )
                    }

                    shoutCount = ( shoutCount + 1 ) % 35

                    setTimeout( Helper.runCycle, 5000, data )
                    return
                }

                await BoatManager.setBoathPath( data.objectId, [ dockGludin ] )
                break

            case 19:
                BoatManager.dockShip( ShipDock.GludinHarbor, true )
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        ARRIVED_AT_GLUDIN, ARRIVED_AT_GLUDIN_2 )

                BroadcastHelper.broadcastDataAtLocations( [ dockGludin ],
                        ConfigManager.general.getBoatBroadcastRadius(),
                        PlaySound.createSound( SoundNames.ITEMSOUND_SHIP_ARRIVAL_DEPARTURE, boat ) )
                setTimeout( Helper.runCycle, 300000, data )
                break
        }

        shoutCount = 0
        currentCycle = ( currentCycle + 1 ) % 19
    },
}