import { L2ExecutablePlugin, L2ExecutablePluginStatus } from '../L2ExecutablePlugin'
import { VehiclePathPoint } from '../../gameService/models/VehiclePathPoint'
import { BoatManager, ShipDock } from '../../gameService/instancemanager/BoatManager'
import { L2BoatInstance } from '../../gameService/models/actor/instance/L2BoatInstance'
import { ListenerCache } from '../../gameService/cache/ListenerCache'
import { EventType, VehiclePathEndedEvent } from '../../gameService/models/events/EventType'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { CreatureSay } from '../../gameService/packets/send/builder/CreatureSay'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { PlaySound } from '../../gameService/packets/send/builder/PlaySound'
import { SoundNames } from '../../gameService/packets/send/SoundPacket'
import { L2World } from '../../gameService/L2World'
import { Location } from '../../gameService/models/Location'
import { ConfigManager } from '../../config/ConfigManager'
import { PacketHelper } from '../../gameService/packets/PacketVariables'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const dockRune = new VehiclePathPoint( 34381, -37680, -3610, 220, 800 )
const dockPrimeval = new VehiclePathPoint( 10342, -27279, -3610, 150, 1800 )
const broadcastLocations: Array<Location> = [ dockRune, dockPrimeval ]

const pathRuneToPrimeval: Array<VehiclePathPoint> = [
    new VehiclePathPoint( 32750, -39300, -3610, 180, 800 ),
    new VehiclePathPoint( 27440, -39328, -3610, 250, 1000 ),
    new VehiclePathPoint( 19616, -39360, -3610, 270, 1000 ),
    new VehiclePathPoint( 3840, -38528, -3610, 270, 1000 ),
    new VehiclePathPoint( 1664, -37120, -3610, 270, 1000 ),
    new VehiclePathPoint( 896, -34560, -3610, 180, 1800 ),
    new VehiclePathPoint( 832, -31104, -3610, 180, 180 ),
    new VehiclePathPoint( 2240, -29132, -3610, 150, 1800 ),
    new VehiclePathPoint( 4160, -27828, -3610, 150, 1800 ),
    new VehiclePathPoint( 5888, -27279, -3610, 150, 1800 ),
    new VehiclePathPoint( 7000, -27279, -3610, 150, 1800 ),
    dockPrimeval,
]

const pathPrimevalToRune: Array<VehiclePathPoint> = [
    new VehiclePathPoint( 15528, -27279, -3610, 180, 800 ),
    new VehiclePathPoint( 22304, -29664, -3610, 290, 800 ),
    new VehiclePathPoint( 33824, -26880, -3610, 290, 800 ),
    new VehiclePathPoint( 38848, -21792, -3610, 240, 1200 ),
    new VehiclePathPoint( 43424, -22080, -3610, 180, 1800 ),
    new VehiclePathPoint( 44320, -25152, -3610, 180, 1800 ),
    new VehiclePathPoint( 40576, -31616, -3610, 250, 800 ),
    new VehiclePathPoint( 36819, -35315, -3610, 220, 800 ),
]

const ArrivedAtRune: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.ARRIVED_AT_RUNE ).getBuffer() )
const LeavingForPrimeval3Minutes: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_LEAVING_FOR_PRIMEVAL_3_MINUTES ).getBuffer() )
const LeavingRune: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_LEAVING_RUNE_FOR_PRIMEVAL_NOW ).getBuffer() )
const ArrivedAtPrimeval: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_ARRIVED_AT_PRIMEVAL ).getBuffer() )
const LeavingForRune3Minutes: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_LEAVING_FOR_RUNE_3_MINUTES ).getBuffer() )
const LeavingPrimeval: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_LEAVING_PRIMEVAL_FOR_RUNE_NOW ).getBuffer() )
const ArrivalDelayed: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_FROM_PRIMEVAL_TO_RUNE_DELAYED ).getBuffer() )

let shoutCount = 0
let currentCycle = 0

export const RuneToPrimeval: L2ExecutablePlugin = {
    getId(): string {
        return 'RuneToPrimeval Boat'
    },

    getStatus(): L2ExecutablePluginStatus {
        return undefined
    },

    async onEnd(): Promise<void> {
        shoutCount = 0
        currentCycle = 0
    },

    async onStart(): Promise<void> {
        let boat: L2BoatInstance = BoatManager.registerBoat( 5, 34381, -37680, -3610, 40785 )

        if ( !boat ) {
            return
        }

        ListenerCache.registerListener( ListenerRegisterType.WorldObject, EventType.VehiclePathEnded, null, Helper.runCycle, [ boat.getObjectId() ] )

        let data: VehiclePathEndedEvent = {
            objectId: boat.getObjectId(),
        }

        setTimeout( Helper.runCycle, 180000, data )
        BoatManager.dockShip( ShipDock.RuneHarbor, true )
    },
}

const Helper = {
    async runCycle( data: VehiclePathEndedEvent ): Promise<void> {
        let boat: L2BoatInstance = L2World.getObjectById( data.objectId ) as L2BoatInstance

        if ( !boat ) {
            return
        }

        switch ( currentCycle ) {
            case 0:
                BoatManager.dockShip( ShipDock.RuneHarbor, false )
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        LeavingRune, PlaySound.createSound( SoundNames.ITEMSOUND_SHIP_ARRIVAL_DEPARTURE, boat ) )
                await boat.payForRide( 8925, 1, 34513, -38009, -3640 )
                await BoatManager.setBoathPath( data.objectId, pathRuneToPrimeval )
                break

            case 1:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        ArrivedAtPrimeval, LeavingForRune3Minutes, PlaySound.createSound( SoundNames.ITEMSOUND_SHIP_ARRIVAL_DEPARTURE, boat ) )
                setTimeout( Helper.runCycle, 180000, data )
                break

            case 2:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        LeavingPrimeval, PlaySound.createSound( SoundNames.ITEMSOUND_SHIP_ARRIVAL_DEPARTURE, boat ) )
                await boat.payForRide( 8924, 1, 10447, -24982, -3664 )
                await BoatManager.setBoathPath( data.objectId, pathPrimevalToRune )
                break

            case 3:
                if ( BoatManager.isDockBusy( ShipDock.RuneHarbor ) ) {
                    if ( shoutCount === 0 ) {
                        BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                                ConfigManager.general.getBoatBroadcastRadius(),
                                ArrivalDelayed )
                    }

                    shoutCount = ( shoutCount + 1 ) % 35

                    setTimeout( Helper.runCycle, 5000, data )
                    return
                }

                await BoatManager.setBoathPath( data.objectId, [ dockRune ] )
                break

            case 4:
                BoatManager.dockShip( ShipDock.RuneHarbor, true )
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        ArrivedAtRune, LeavingForPrimeval3Minutes, PlaySound.createSound( SoundNames.ITEMSOUND_SHIP_ARRIVAL_DEPARTURE, boat ) )
                setTimeout( Helper.runCycle, 180000, data )
                break
        }

        shoutCount = 0
        currentCycle = ( currentCycle + 1 ) % 4
    },
}