import { L2ExecutablePlugin, L2ExecutablePluginStatus } from '../L2ExecutablePlugin'
import { VehiclePathPoint } from '../../gameService/models/VehiclePathPoint'
import { CreatureSay } from '../../gameService/packets/send/builder/CreatureSay'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { L2BoatInstance } from '../../gameService/models/actor/instance/L2BoatInstance'
import { BoatManager } from '../../gameService/instancemanager/BoatManager'
import { ListenerCache } from '../../gameService/cache/ListenerCache'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import { EventType, VehiclePathEndedEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { ILocational } from '../../gameService/models/Location'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { ConfigManager } from '../../config/ConfigManager'
import { PlaySound } from '../../gameService/packets/send/builder/PlaySound'
import { SoundNames } from '../../gameService/packets/send/SoundPacket'
import { PacketHelper } from '../../gameService/packets/PacketVariables'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const dockInnadril: VehiclePathPoint = new VehiclePathPoint( 111264, 226240, -3610, 150, 800 )
const broadcastLocations: Array<ILocational> = [ dockInnadril ]

const path: Array<VehiclePathPoint> = [
    new VehiclePathPoint( 105129, 226240, -3610, 150, 800 ),
    new VehiclePathPoint( 90604, 238797, -3610, 150, 800 ),
    new VehiclePathPoint( 74853, 237943, -3610, 150, 800 ),
    new VehiclePathPoint( 68207, 235399, -3610, 150, 800 ),
    new VehiclePathPoint( 63226, 230487, -3610, 150, 800 ),
    new VehiclePathPoint( 61843, 224797, -3610, 150, 800 ),
    new VehiclePathPoint( 61822, 203066, -3610, 150, 800 ),
    new VehiclePathPoint( 59051, 197685, -3610, 150, 800 ),
    new VehiclePathPoint( 54048, 195298, -3610, 150, 800 ),
    new VehiclePathPoint( 41609, 195687, -3610, 150, 800 ),
    new VehiclePathPoint( 35821, 200284, -3610, 150, 800 ),
    new VehiclePathPoint( 35567, 205265, -3610, 150, 800 ),
    new VehiclePathPoint( 35617, 222471, -3610, 150, 800 ),
    new VehiclePathPoint( 37932, 226588, -3610, 150, 800 ),
    new VehiclePathPoint( 42932, 229394, -3610, 150, 800 ),
    new VehiclePathPoint( 74324, 245231, -3610, 150, 800 ),
    new VehiclePathPoint( 81872, 250314, -3610, 150, 800 ),
    new VehiclePathPoint( 101692, 249882, -3610, 150, 800 ),
    new VehiclePathPoint( 107907, 256073, -3610, 150, 800 ),
    new VehiclePathPoint( 112317, 257133, -3610, 150, 800 ),
    new VehiclePathPoint( 126273, 255313, -3610, 150, 800 ),
    new VehiclePathPoint( 128067, 250961, -3610, 150, 800 ),
    new VehiclePathPoint( 128520, 238249, -3610, 150, 800 ),
    new VehiclePathPoint( 126428, 235072, -3610, 150, 800 ),
    new VehiclePathPoint( 121843, 234656, -3610, 150, 800 ),
    new VehiclePathPoint( 120096, 234268, -3610, 150, 800 ),
    new VehiclePathPoint( 118572, 233046, -3610, 150, 800 ),
    new VehiclePathPoint( 117671, 228951, -3610, 150, 800 ),
    new VehiclePathPoint( 115936, 226540, -3610, 150, 800 ),
    new VehiclePathPoint( 113628, 226240, -3610, 150, 800 ),
    new VehiclePathPoint( 111300, 226240, -3610, 150, 800 ),
    dockInnadril,
]

const arrivedInInnadril: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.INNADRIL_BOAT_ANCHOR_10_MINUTES ).getBuffer() )
const leavingInnadrilInFive: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.INNADRIL_BOAT_LEAVE_IN_5_MINUTES ).getBuffer() )
const leavingInnadrilInOne: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.INNADRIL_BOAT_LEAVE_IN_1_MINUTE ).getBuffer() )
const leavingInnadrilSoon: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.INNADRIL_BOAT_LEAVE_SOON ).getBuffer() )
const leavingInnadrilNow: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.INNADRIL_BOAT_LEAVING ).getBuffer() )

const arriveInTwenty: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.INNADRIL_BOAT_ARRIVE_20_MINUTES ).getBuffer() )
const arriveInFifteen: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.INNADRIL_BOAT_ARRIVE_15_MINUTES ).getBuffer() )
const arriveInTen: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.INNADRIL_BOAT_ARRIVE_10_MINUTES ).getBuffer() )
const arriveInFive: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.INNADRIL_BOAT_ARRIVE_5_MINUTES ).getBuffer() )
const arriveInOne: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.INNADRIL_BOAT_ARRIVE_1_MINUTE ).getBuffer() )

let currentCycle = 0

export const InnadrilTour: L2ExecutablePlugin = {
    getId(): string {
        return 'InnadrilTour Boat'
    },

    getStatus(): L2ExecutablePluginStatus {
        return undefined
    },

    async onEnd(): Promise<void> {
        currentCycle = 0
    },

    onStart(): Promise<void> {
        let boat: L2BoatInstance = BoatManager.registerBoat( 4, 111264, 226240, -3610, 32768 )

        if ( !boat ) {
            return
        }

        ListenerCache.registerListener( ListenerRegisterType.WorldObject, EventType.VehiclePathEnded, null, Helper.runCycle, [ boat.getObjectId() ] )

        let data: VehiclePathEndedEvent = {
            objectId: boat.getObjectId(),
        }

        setTimeout( Helper.runCycle, 180000, data )
        return
    },
}

const Helper = {
    async runCycle( data: VehiclePathEndedEvent ): Promise<void> {
        let boat: L2BoatInstance = L2World.getObjectById( data.objectId ) as L2BoatInstance

        if ( !boat ) {
            return
        }

        switch ( currentCycle ) {
            case 0:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        leavingInnadrilInFive )
                setTimeout( Helper.runCycle, 240000, data )
                break

            case 1:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        leavingInnadrilInOne )
                setTimeout( Helper.runCycle, 40000, data )
                break

            case 2:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        leavingInnadrilSoon )
                setTimeout( Helper.runCycle, 20000, data )
                break

            case 3:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        leavingInnadrilNow, PlaySound.createSound( SoundNames.ITEMSOUND_SHIP_ARRIVAL_DEPARTURE, boat ) )
                await boat.payForRide( 0, 1, 107092, 219098, -3952 )
                await BoatManager.setBoathPath( data.objectId, path )
                setTimeout( Helper.runCycle, 650000, data )
                break

            case 4:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        arriveInTwenty )
                setTimeout( Helper.runCycle, 300000, data )
                break

            case 5:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        arriveInFifteen )
                setTimeout( Helper.runCycle, 300000, data )
                break

            case 6:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        arriveInTen )
                setTimeout( Helper.runCycle, 300000, data )
                break

            case 7:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        arriveInFive )
                setTimeout( Helper.runCycle, 240000, data )
                break

            case 8:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        arriveInOne )
                break

            case 9:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        arrivedInInnadril, PlaySound.createSound( SoundNames.ITEMSOUND_SHIP_ARRIVAL_DEPARTURE, boat ) )
                setTimeout( Helper.runCycle, 300000, data )
                break
        }

        currentCycle = ( currentCycle + 1 ) % 9
    },
}