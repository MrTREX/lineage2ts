export enum L2ExecutablePluginStatus {
    OFF,
    INITIALIZED,
    RUNNING
}
export interface L2ExecutablePlugin {
    onStart() : Promise<void>
    onEnd() : Promise<void>
    getId() : string
    getStatus() : L2ExecutablePluginStatus
}