import { L2DataApi } from '../data/interface/l2DataApi'
import aigle from 'aigle'
import { L2ExecutablePlugin } from './L2ExecutablePlugin'
import _ from 'lodash'
import { BoatRegistry } from './boats/BoatRegistry'

export type PluginHook = () => Promise<Array<string>>

const allPlugins : Array<L2ExecutablePlugin> = _.flatten( [
    BoatRegistry
] )
class Manager implements L2DataApi {
    hooks: Array<PluginHook> = []

    async load(): Promise<Array<string>> {

        let lines : Array<Array<string>> = await aigle.resolve( this.hooks ).map( ( method: PluginHook ) => method() )
        await aigle.resolve( allPlugins ).each( ( plugin: L2ExecutablePlugin ) => plugin.onStart() )

        return [
            `PluginLoader : executed ${ this.hooks.length } hooks.`,
            `PluginLoader : started ${ allPlugins.length } plugins.`,
             ..._.flatten( lines )
        ]
    }

    registerHook( method: PluginHook ): void {
        this.hooks.push( method )
    }
}

export const PluginLoader = new Manager()