import { ObjectPool } from '../gameService/helpers/ObjectPoolHelper'
import { PolygonCell } from './enums/PolygonCell'

export class LinePoints2D {
    x : number
    y : number

    xEnd : number
    yEnd : number

    protected dx : number
    protected dy : number

    protected dModifier: number
    protected xIncrement : number
    protected yIncrement : number

    initialize2D(
        xOrigin : number,
        yOrigin : number,
        xDestination: number,
        yDestination: number,
        stepIncrement: number = PolygonCell.CellSize
    ) : void {
        this.x = xOrigin - ( xOrigin % stepIncrement )
        this.y = yOrigin - ( yOrigin % stepIncrement )

        this.xEnd = xDestination - ( xDestination % stepIncrement )
        this.yEnd = yDestination - ( yDestination % stepIncrement )

        this.dx = Math.abs( this.xEnd - this.x )
        this.dy = Math.abs( this.yEnd - this.y )

        this.dModifier = this.dx - this.dy

        this.xIncrement = this.xEnd > this.x ? stepIncrement : -stepIncrement
        this.yIncrement = this.yEnd > this.y ? stepIncrement : -stepIncrement
    }

    /*
        We need to compute next point in line with specific interval on a grid
        using Bresenham's line algorithm.
        Reference Url: https://en.wikipedia.org/wiki/Bresenham's_line_algorithm
     */
    prepareNextPoint() : boolean {
        if ( this.x === this.xEnd && this.y === this.yEnd ) {
            return false
        }

        this.incrementXY()

        return true
    }

    protected incrementXY() : void {
        const modifier = 2 * this.dModifier
        if ( modifier > -this.dy ) {
            this.dModifier -= this.dy
            this.x += this.xIncrement
        }

        if ( modifier < this.dx ) {
            this.dModifier += this.dx
            this.y += this.yIncrement
        }
    }
}

class LinePoints3D extends LinePoints2D {
    z: number = 0
    zEnd: number = 0

    private dz: number = 0

    initialize3D(
        xOrigin : number,
        yOrigin : number,
        zOrigin: number,
        xDestination: number,
        yDestination: number,
        zDestination: number,
        stepIncrement: number = PolygonCell.CellSize
    ) : void {
        this.initialize2D( xOrigin, yOrigin, xDestination, yDestination, stepIncrement )

        this.z = zOrigin
        this.zEnd = zDestination

        let differenceZ = zDestination - zOrigin
        this.dz = differenceZ / ( this.dx >= this.dy ? Math.floor( this.dx / stepIncrement ) : Math.floor( this.dy / stepIncrement ) )
    }

    /*
        We need to compute next point in line with specific interval on a grid
        using Bresenham's line algorithm.
        Reference Url: https://en.wikipedia.org/wiki/Bresenham's_line_algorithm

        Z value is computed via direct line increments.
     */
    prepareNextPoint() : boolean {
        if ( this.x === this.xEnd && this.y === this.yEnd ) {
            return false
        }

        this.incrementXY()

        this.z = this.z + this.dz

        return true
    }
}

export const LinePoints3DPool = new ObjectPool<LinePoints3D>( LinePoints3D.name, () : LinePoints3D => {
    return new LinePoints3D()
} )

export const LinePoints2DPool = new ObjectPool<LinePoints2D>( LinePoints2D.name, () : LinePoints2D => {
    return new LinePoints2D()
} )