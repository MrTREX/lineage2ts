import { getRegionCode, L2MapTile, L2WorldLimits } from '../gameService/enums/L2MapTile'
import { GeoRegion } from './GeoRegion'
import { LinePoints2DPool, LinePoints3DPool } from './LinePoints'
import { L2Character } from '../gameService/models/actor/L2Character'
import {
    BlockingPolygonTypes,
    extractHeight,
    extractMoveDirection,
    getPolygonHeightWithBlocking,
    getPolygonType
} from './TypeOperations'
import { CellMoveDirection } from './enums/CellMoveDirection'
import { PolygonValues } from './enums/PolygonValues'

export const enum GeoDataLimits {
    LowestHeight = -16384
}

export const enum GeoDataAdjustment {
    ZErrorRate = 10
}

export const enum GeoVisibilityOutcome {
    NotVisible,
    NoDirectPath,
    VisibleDirectPath
}

export class GeoDataOperations {
    regions: Record<number, GeoRegion> = {}
    hasLoadedRegions: boolean = false

    getZ( x: number, y: number, currentZ: number ) : number {
        if ( !this.hasLoadedRegions ) {
            return currentZ
        }

        let xDifference = x - L2WorldLimits.MinX
        let xTile = ( xDifference >> L2MapTile.SizeShift ) + L2MapTile.XMin
        let yDifference = y - L2WorldLimits.MinY
        let yTile = ( yDifference >> L2MapTile.SizeShift ) + L2MapTile.YMin

        let regionKey = getRegionCode( xTile, yTile )
        let region = this.regions[ regionKey ]

        if ( !region ) {
            return currentZ
        }

        return region.getHeight( xDifference, yDifference, currentZ )
    }

    isInvalidZ( value: number ) : boolean {
        return value <= GeoDataLimits.LowestHeight
    }


    isVisible(
        xOrigin : number,
        yOrigin : number,
        zOrigin: number,
        xDestination: number,
        yDestination: number,
        zDestination: number ) : boolean {

        let points = LinePoints3DPool.getValue()

        points.initialize3D(
            xOrigin,
            yOrigin,
            zOrigin,
            xDestination,
            yDestination,
            zDestination )

        let isVisible : boolean = true

        points.prepareNextPoint()

        /*
            Typical lookups should not use more than 100 loop iterations. In extreme cases, it can
            go to 400 or so. Value is determined to put limit on calculations in case abnormal state
            happens to derail algorithm.
         */
        for ( let index = 0; index < 400; index++ ) {
            if ( !points.prepareNextPoint() ) {
                break
            }

            let zValue = this.getZ( points.x, points.y, points.z )
            if ( ( zValue - GeoDataAdjustment.ZErrorRate ) > points.z ) {
                isVisible = false
                break
            }
        }

        LinePoints3DPool.recycleValue( points )

        return isVisible
    }

    isTargetVisible( origin: L2Character, target : L2Character ) : boolean {
        return this.isVisible(
            origin.getX(),
            origin.getY(),
            origin.getZ() + origin.getCollisionHeight(),
            target.getX(),
            target.getY(),
            target.getZ() + target.getCollisionHeight() )
    }

    /*
        Visibility and access path rolled into single check.
        Direct path means that there is no geodata obstacles that
        would prevent moving from origin to destination.
     */
    isVisibleWithDirectPath(
        xOrigin : number,
        yOrigin : number,
        zOrigin: number,
        xDestination: number,
        yDestination: number,
        zDestination: number ) : GeoVisibilityOutcome {
        const points = LinePoints3DPool.getValue()

        points.initialize3D(
            xOrigin,
            yOrigin,
            zOrigin,
            xDestination,
            yDestination,
            zDestination )

        let isVisible : GeoVisibilityOutcome = GeoVisibilityOutcome.VisibleDirectPath
        let lastZ = zOrigin

        points.prepareNextPoint()

        /*
            Similar algorithm to normal visibility check, except working together with
            blocking data, if present on geo-polygon.
         */
        for ( let index = 0; index < 400; index++ ) {

            let xDifference = points.x - L2WorldLimits.MinX
            let xTile = ( xDifference >> L2MapTile.SizeShift ) + L2MapTile.XMin
            let yDifference = points.y - L2WorldLimits.MinY
            let yTile = ( yDifference >> L2MapTile.SizeShift ) + L2MapTile.YMin

            let regionKey = getRegionCode( xTile, yTile )
            let region = this.regions[ regionKey ]
            if ( !region ) {
                isVisible = GeoVisibilityOutcome.NotVisible
                break
            }

            let polygon = region.getEmbeddedPolygon( xDifference, yDifference, points.z )
            let zValue = getPolygonHeightWithBlocking( polygon, xDifference, yDifference, points.z )

            let type = getPolygonType( polygon )
            if ( BlockingPolygonTypes.has( type ) ) {
                const positionDirection = extractMoveDirection( zValue )
                if ( positionDirection !== CellMoveDirection.All ) {
                    isVisible = GeoVisibilityOutcome.NoDirectPath
                    break
                }

                zValue = extractHeight( zValue )
            }

            if ( ( zValue - GeoDataAdjustment.ZErrorRate ) > points.z ) {
                isVisible = GeoVisibilityOutcome.NotVisible
                break
            }

            if ( zValue === PolygonValues.LowestHeight
                || ( zValue > lastZ && ( zValue - lastZ ) > 30 ) ) {
                isVisible = GeoVisibilityOutcome.NoDirectPath
                break
            }

            lastZ = zValue

            if ( !points.prepareNextPoint() ) {
                break
            }
        }

        LinePoints3DPool.recycleValue( points )

        return isVisible
    }

    getMovementDirection( x: number, y: number, currentZ: number ) : CellMoveDirection {
        if ( !this.hasLoadedRegions ) {
            return CellMoveDirection.All
        }

        let xDifference = x - L2WorldLimits.MinX
        let xTile = ( xDifference >> L2MapTile.SizeShift ) + L2MapTile.XMin
        let yDifference = y - L2WorldLimits.MinY
        let yTile = ( yDifference >> L2MapTile.SizeShift ) + L2MapTile.YMin

        let regionKey = getRegionCode( xTile, yTile )
        let region = this.regions[ regionKey ]

        if ( !region ) {
            return CellMoveDirection.All
        }

        let polygon = region.getEmbeddedPolygon( xDifference, yDifference, currentZ )
        let zValue = getPolygonHeightWithBlocking( polygon, xDifference, yDifference, currentZ )

        let type = getPolygonType( polygon )
        if ( BlockingPolygonTypes.has( type ) ) {
            return extractMoveDirection( zValue )
        }

        if ( zValue === PolygonValues.LowestHeight ) {
            return CellMoveDirection.None
        }

        return CellMoveDirection.All
    }

    /*
        TODO : add skip points for non-blocking geo polygons, where algorithm must jump to furthest point
        that is contained in polygon so that next check can determine if next polygon can be traversed.
        - use relative coordinates to avoid additional math conversion with shifts
     */
    hasDirectPath(
        xOrigin : number,
        yOrigin : number,
        zOrigin: number,
        xDestination: number,
        yDestination: number,
    ) : boolean {
        const points = LinePoints2DPool.getValue()

        points.initialize2D(
            xOrigin,
            yOrigin,
            xDestination,
            yDestination )

        points.prepareNextPoint()

        let outcome : boolean = true
        let lastZ = zOrigin

        for ( let index = 0; index < 400; index++ ) {
            let xDifference = points.x - L2WorldLimits.MinX
            let xTile = ( xDifference >> L2MapTile.SizeShift ) + L2MapTile.XMin
            let yDifference = points.y - L2WorldLimits.MinY
            let yTile = ( yDifference >> L2MapTile.SizeShift ) + L2MapTile.YMin

            let regionKey = getRegionCode( xTile, yTile )
            let region = this.regions[ regionKey ]
            if ( !region ) {
                outcome = false
                break
            }

            let polygon = region.getEmbeddedPolygon( xDifference, yDifference, lastZ )
            let zValue = getPolygonHeightWithBlocking( polygon, xDifference, yDifference, lastZ )

            let type = getPolygonType( polygon )
            let currentZ = zValue

            if ( BlockingPolygonTypes.has( type ) ) {
                const positionDirection = extractMoveDirection( zValue )
                if ( positionDirection !== CellMoveDirection.All ) {
                    outcome = false
                    break
                }

                currentZ = extractHeight( zValue )
            }

            if ( currentZ === PolygonValues.LowestHeight
                || ( currentZ > lastZ && ( currentZ - lastZ ) > 30 ) ) {
                outcome = false
                break
            }

            lastZ = currentZ

            if ( !points.prepareNextPoint() ) {
                break
            }
        }

        LinePoints2DPool.recycleValue( points )

        return outcome
    }
}