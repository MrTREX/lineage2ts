import { MinHeap } from '@datastructures-js/heap'
import perfy from 'perfy'
import { RoaringBitmap32 } from 'roaring'
import { LinePoints2D } from './LinePoints'

/*
    Using class instead of raw object has produced about 5% speedup.
 */
class PathfindingNode {
    x: number
    y: number
    pd: number = Infinity
    nd: number = 0
    parent: PathfindingNode
    cost : number = 0

    updateCost() : void {
        this.cost = this.pd + this.nd
    }

    hash: number
}

interface Direction {
    x: number
    y: number
}

/*
    Directions of search. Keep in mind that diagonal directions are eliminated to avoid
    crossing diagonally across corner. Performance improved by 40x times. Keep in mind that
    diagonal movement still happens and these direction points only represent part of whole
    algorithm for pathfinding.
 */
const directions : ReadonlyArray<Direction> = [
    {
        x: 0,
        y: 1
    },
    {
        x: 1,
        y: 0
    },
    {
        x: 0,
        y: -1,
    },
    {
        x: -1,
        y: 0
    }
]

/*
    Please note that usage of object pool for node objects resulted in slightly lower performance.
    However, re-using heap and bitmap objects resulted in 67% performance gain. Hence, it is recommended
    to keep algorithm container behind object pool.
 */
class ThetaStar {
    xStart: number
    yStart: number

    exploredNodes : RoaringBitmap32 = new RoaringBitmap32()
    knownNodes : MinHeap<PathfindingNode> = new MinHeap<PathfindingNode>( ( item: PathfindingNode ) : number => item.cost )

    points: LinePoints2D = new LinePoints2D()

    findPath( xStart: number, yStart: number , xEnd: number, yEnd: number, grid : Array<Array<number>> ) {
        this.xStart = xStart
        this.yStart = yStart

        const startNode = this.createNode( xStart, yStart )
        const endNode = this.createNode( xEnd, yEnd )

        startNode.pd = 0
        startNode.nd = this.distance( startNode, endNode )

        this.knownNodes.insert( startNode )

        while ( !this.knownNodes.isEmpty() ) {
            const currentNode = this.knownNodes.extractRoot()

            if ( currentNode.x === endNode.x && currentNode.y === endNode.y ) {
                return this.reconstructPath( currentNode )
            }

            const neighbors = this.getNeighbors( currentNode, grid )

            for ( const neighbor of neighbors ) {
                neighbor.nd = this.distance( neighbor, endNode )

                if ( currentNode.parent && this.lineOfSight( currentNode.parent.x, currentNode.parent.y, neighbor.x, neighbor.y, grid ) ) {
                    const parentDistance = currentNode.parent.pd + this.distance( currentNode.parent, neighbor )

                    if ( parentDistance < neighbor.pd ) {
                        neighbor.parent = currentNode.parent
                        neighbor.pd = parentDistance
                    }
                }

                const distance = currentNode.pd + this.distance( currentNode, neighbor )
                if ( distance < neighbor.pd ) {
                    neighbor.parent = currentNode
                    neighbor.pd = distance
                }

                neighbor.updateCost()
                this.knownNodes.insert( neighbor )
            }
        }

        return null
    }

    /*
        TODO : add skip points for non-blocking geo polygons, where algorithm must jump to furthest point
        that is contained in polygon so that next check can determine if next polygon can be traversed.
     */
    lineOfSight( xStart : number, yStart : number, xEnd : number, yEnd : number, grid : Array<Array<number>> ) : boolean {
        this.points.initialize2D( xStart, yStart, xEnd, yEnd, 1 )

        while ( this.points.prepareNextPoint() ) {
            if ( grid[ this.points.x ][ this.points.y ] === 1 ) {
                return false
            }
        }

        return true
    }

    /*
        TODO : consider using geodata polygon type for getting points
        - for flat and non-blocking polygon, use 4 corners + center point
        - for blocking polygon use +/- directions to get nearest points

        Optimization is geared to reduce amount of points being used for polygons that do not need
        checks for blocking.
     */
    getNeighbors( node : PathfindingNode, grid : Array<Array<number>> ) : ReadonlyArray<PathfindingNode> {
        const neighbors : Array<PathfindingNode> = []
        for ( const direction of directions ) {
            const x = node.x + direction.x
            const y = node.y + direction.y

            if ( x >= 0 && x < grid.length
                && y >= 0 && y < grid[ 0 ].length
                && grid[ x ][ y ] === 0 ) {

                const hash = this.createHash( x, y )
                if ( !this.exploredNodes.has( hash ) ) {
                    this.exploredNodes.add( hash )
                    neighbors.push( this.createNode( x, y, hash ) )
                }
            }
        }
        return neighbors
    }

    protected distance( nodeA : PathfindingNode, nodeB : PathfindingNode ) : number {
        /*
            While one can use Math.hypot, tests have shown that it is slower than current implementation
         */
        return Math.sqrt(
            ( nodeA.x - nodeB.x ) ** 2 +
            ( nodeA.y - nodeB.y ) ** 2
        )
    }

    reconstructPath( node : PathfindingNode ) : Array<[number, number]> {
        const path : Array<[number, number]> = []
        let currentNode = node
        while ( currentNode ) {
            path.push( [ currentNode.x, currentNode.y ] )
            currentNode = currentNode.parent
        }
        return path.reverse()
    }

    createNode( x : number, y: number, hash: number = this.createHash( x, y ) ) : PathfindingNode {
        const node = new PathfindingNode()

        node.x = x
        node.y = y
        node.hash = hash

        return node
    }

    /*
        Keep in mind that x and y values must be below 16-bit UInt max value.
     */
    protected createHash( x : number, y: number ) : number {
        return x + ( y << 16 )
    }

    clearResults() : void {
        this.exploredNodes.clear()
        this.knownNodes.clear()
    }
}

const grid : Array<Array<number>> = [
    [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
    [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
    [ 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
    [ 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
    [ 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
    [ 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
    [ 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
    [ 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
    [ 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
    [ 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
    [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
    [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
    [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
    [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
    [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
    [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0 ],
    [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0 ],
    [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0 ],
    [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0 ],
    [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
    [ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],
]

const thetaStar = new ThetaStar()
perfy.start( 'start' )

for ( let i = 0; i < 10000; i++ ) {
    const path = thetaStar.findPath( 0, 0, 19, 19, grid )
    thetaStar.clearResults()

    console.log( 'Path:', path )
}

let metrics = perfy.end( 'start' )
console.log( 'Spent time', metrics.time, 'seconds' )