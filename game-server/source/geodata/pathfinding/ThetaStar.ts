import { MinHeap } from '@datastructures-js/heap'
import { CellMoveDirection } from '../enums/CellMoveDirection'
import { PolygonCell } from '../enums/PolygonCell'
import { GeoPolygonCache } from '../../gameService/cache/GeoPolygonCache'
import { Boundaries } from './Boundaries'
import { RoaringBitmap32 } from 'roaring'
import { getRegionCode, L2MapTile, L2WorldLimits } from '../../gameService/enums/L2MapTile'
import { BlockingPolygonTypes, getPolygonType } from '../TypeOperations'
import { PolygonValues } from '../enums/PolygonValues'

/*
    Using class instead of raw object has produced about 5% speedup.
 */
class PathfindingNode {
    x: number
    y: number
    z: number
    pd: number = Infinity
    nd: number = 0
    parent: PathfindingNode
    cost : number = 0
    hash: number

    updateCost() : void {
        this.cost = this.pd + this.nd
    }
}

interface Direction {
    x: number
    y: number
    nwse: CellMoveDirection
}

/*
    Directions of search. Keep in mind that diagonal directions are eliminated to avoid
    crossing diagonally across corner. Performance improved by 40x times. Keep in mind that
    diagonal movement still happens and these direction points only represent part of whole
    algorithm for pathfinding.
 */
const directions : ReadonlyArray<Direction> = [
    {
        x: 0,
        y: PolygonCell.CellSize,
        nwse: CellMoveDirection.North,
    },
    {
        x: PolygonCell.CellSize,
        y: 0,
        nwse: CellMoveDirection.East,
    },
    {
        x: 0,
        y: -PolygonCell.CellSize,
        nwse: CellMoveDirection.South,
    },
    {
        x: -PolygonCell.CellSize,
        y: 0,
        nwse: CellMoveDirection.West
    }
]

/*
    Please note that usage of object pool for node objects resulted in slightly lower performance.
    However, re-using heap and bitmap objects resulted in 67% performance gain. Hence, it is recommended
    to keep algorithm container behind object pool.
 */
export class ThetaStar {
    xMin: number
    yMin: number

    xMax: number
    yMax: number

    exploredNodes : RoaringBitmap32 = new RoaringBitmap32()
    knownNodes : MinHeap<PathfindingNode> = new MinHeap<PathfindingNode>( ( item: PathfindingNode ) : number => item.cost )
    neighbors : Array<PathfindingNode> = []

    /*
        Before start of pathfinding make sure to perform direct line of sight to ensure that path is not already accessible.
     */
    findPath( xStart: number, yStart: number, zStart: number, xEnd: number, yEnd: number, zEnd: number ) : Array<PathfindingNode> | null {
        let xnStart = this.normalizeValue( xStart )
        let ynStart = this.normalizeValue( yStart )
        let xnEnd = this.normalizeValue( xEnd )
        let ynEnd = this.normalizeValue( yEnd )

        this.computeBoundaries( xnStart, ynStart, xnEnd, ynEnd )

        const startNode = this.createNode( xnStart, ynStart, zStart, this.createHash( xnStart, ynStart ) )
        const endNode = this.createNode( xnEnd, ynEnd, zEnd, this.createHash( xnEnd, ynEnd ) )

        startNode.pd = 0
        startNode.nd = this.distance( startNode, endNode )

        this.knownNodes.insert( startNode )

        while ( !this.knownNodes.isEmpty() ) {
            const currentNode = this.knownNodes.extractRoot()

            if ( currentNode.x === endNode.x && currentNode.y === endNode.y ) {
                return this.reconstructPath( currentNode )
            }

            this.getNeighbors( currentNode )

            for ( const neighbor of this.neighbors ) {
                neighbor.nd = this.distance( neighbor, endNode )

                if ( currentNode.parent
                    && GeoPolygonCache.hasDirectPath( currentNode.parent.x, currentNode.parent.y, currentNode.parent.z, neighbor.x, neighbor.y ) ) {
                    const parentDistance = currentNode.parent.pd + this.distance( currentNode.parent, neighbor )

                    if ( parentDistance < neighbor.pd ) {
                        neighbor.parent = currentNode.parent
                        neighbor.pd = parentDistance
                    }
                }

                const distance = currentNode.pd + this.distance( currentNode, neighbor )
                if ( distance < neighbor.pd ) {
                    neighbor.parent = currentNode
                    neighbor.pd = distance
                }

                neighbor.updateCost()
                this.knownNodes.insert( neighbor )
            }

            this.neighbors.length = 0
        }

        return null
    }

    /*
        TODO : consider using geodata polygon type for getting points
        - for flat and non-blocking polygon, use 4 corners + center point
        - for blocking polygon use +/- directions to get nearest points

        Optimization is geared to reduce amount of points being used for polygons that do not need
        checks for blocking.
     */
    getNeighbors( node : PathfindingNode ) : void {
        for ( const direction of directions ) {
            const x = node.x + direction.x
            const y = node.y + direction.y

            if ( x >= this.xMin
                && x < this.xMax
                && y >= this.yMin
                && y < this.yMax ) {

                const currentZ = GeoPolygonCache.getZ( x, y, node.z )
                if ( currentZ === PolygonValues.LowestHeight
                    || ( currentZ > node.z && ( currentZ - node.z ) > 30 ) ) {
                    continue
                }

                /*
                    Check for additional blocking cells that we want to avoid during movement, example
                    would be moving too close to building wall or a tree.
                 */
                let moveDirection = GeoPolygonCache.getMovementDirection( x + direction.x, y + direction.y, node.z )
                if ( moveDirection !== CellMoveDirection.All ) {
                    continue
                }

                moveDirection = GeoPolygonCache.getMovementDirection( x + direction.x + direction.x, y + direction.y + direction.y, node.z )
                if ( moveDirection !== CellMoveDirection.All ) {
                    continue
                }

                const hash = this.createHash( x, y )
                if ( !this.exploredNodes.has( hash ) ) {
                    this.exploredNodes.add( hash )

                    this.neighbors.push( this.createNode( x, y, currentZ, hash ) )
                }
            }
        }
    }

    protected distance( nodeA : PathfindingNode, nodeB : PathfindingNode ) : number {
        /*
            While one can use Math.hypot, tests have shown that it is slower than current implementation
         */
        return Math.sqrt(
            ( nodeA.x - nodeB.x ) ** 2 +
            ( nodeA.y - nodeB.y ) ** 2
        )
    }

    reconstructPath( node : PathfindingNode ) : Array<PathfindingNode> {
        const path : Array<PathfindingNode> = []
        let currentNode = node
        while ( currentNode ) {
            path.push( currentNode )
            currentNode = currentNode.parent
        }
        return path.reverse()
    }

    createNode( x : number, y: number, z: number, hash: number ) : PathfindingNode {
        const node = new PathfindingNode()

        node.x = x
        node.y = y
        node.z = z
        node.hash = hash

        return node
    }

    /*
        Keep in mind that x and y values must be below 16-bit UInt max value.
     */
    protected createHash( x : number, y: number ) : number {
        return ( x - this.xMin ) + ( ( y - this.yMin ) << 16 )
    }

    clearResults() : void {
        this.exploredNodes.clear()
        this.knownNodes.clear()
    }

    /*
        Keep in mind that boundaries must be within loaded geo-regions.
     */
    protected computeBoundaries( xStart: number, yStart: number, xEnd: number, yEnd: number ) : void {
        this.xMin = Math.min( xStart, xEnd ) - Boundaries.X
        this.xMax = Math.max( xStart, xEnd ) + Boundaries.X

        this.yMin = Math.min( yStart, yEnd ) - Boundaries.Y
        this.yMax = Math.max( yStart, yEnd ) + Boundaries.Y
    }

    protected normalizeValue( value : number ) : number {
        return value - ( value % PolygonCell.CellSize )
    }
}