import { PolygonCell } from '../enums/PolygonCell'

export const enum Boundaries {
    X = PolygonCell.CellSize * 100,
    Y = PolygonCell.CellSize * 100
}