/*
    Geodata allowed move direction values represent bitmask of
    available directions. Keep in mind while zero value is not used by geodata,
    it is meant to signify that direction is unknown.
 */

export const enum CellMoveDirection {
    None = 0,
    East = 1,
    West = 2,
    South = 4,
    North = 8,
    NE = 9,
    NW = 10,
    SE = 5,
    SW = 6,
    All = 15,
}

export function hasMovementDirection( value : number, direction : CellMoveDirection ) : boolean {
    return ( value & direction ) === direction
}

export function getLineMoveDirection( originX: number, originY : number, destinationX: number, destinationY : number ) : CellMoveDirection {
    if ( originX < destinationX ) {
        if ( originY > destinationY ) {
            return CellMoveDirection.NE
        }

        if ( originY < destinationY ) {
            return CellMoveDirection.SE
        }

        return CellMoveDirection.East
    }

    if ( originX > destinationX ) {
        if ( originY > destinationY ) {
            return CellMoveDirection.NW
        }

        if ( originY < destinationY ) {
            return CellMoveDirection.SW
        }

        return CellMoveDirection.West
    }

    if ( originY < destinationY ) {
        return CellMoveDirection.South
    }

    return CellMoveDirection.North
}

export type BasicMoveDirections = CellMoveDirection.East | CellMoveDirection.West | CellMoveDirection.South | CellMoveDirection.North
export function getBasicMoveDirection( originX: number, originY : number, destinationX: number, destinationY : number ) : BasicMoveDirections {
    if ( originX < destinationX ) {
        return CellMoveDirection.East
    }

    if ( originX > destinationX ) {
        return CellMoveDirection.West
    }

    if ( originY < destinationY ) {
        return CellMoveDirection.South
    }

    return CellMoveDirection.North
}