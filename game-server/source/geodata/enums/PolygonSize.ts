export const enum PolygonSize {
    Dimension = 8,

    /*
        Refers to amount of polygons a region has in X or Y traversal. Total amount of polygons in region is 65536 (or 256 * 256)
     */
    PolygonsInSection = 256,
    PolygonsInRegion = 256 * 256,
    /*
        Easier to do perform binary shift than deletion that would require usage of Math.floor on result.
     */
    WorldPolygonShift = 7,
    WorldCellShift = 4,
    MultiLayerMetadataSize = 5,
}