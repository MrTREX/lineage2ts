import { GeoPolygonType } from './enums/GeoPolygon'
import { PolygonSize } from './enums/PolygonSize'
import { PolygonLength } from './PolygonLength'
import { getMultiLayerPolygon, getPolygonHeight, getPolygonType } from './TypeOperations'
import { ServerLog } from '../logger/Logger'

function convertToPolygons( data: Buffer ) : Array<Buffer> {
    let index = 0
    let polygons : Array<Buffer> = new Array<Buffer>( PolygonSize.PolygonsInRegion )
    let polygonIndex = 0
    while ( index < data.length ) {
        let chunk : Buffer = extractPolygon( data, index )
        polygons[ polygonIndex ] = chunk
        index += chunk.length
        polygonIndex++
    }

    return polygons
}

function extractPolygon( data: Buffer, index : number ) : Buffer {
    let type = data.readUInt8( index )
    if ( type === GeoPolygonType.MultiLayer ) {
        return extractMultiLayerPolygon( data, index )
    }

    if ( type === GeoPolygonType.MultiLayerL2J ) {
        let size = data.readUInt16LE( index + 1 )
        return data.subarray( index, size + index + 3 )
    }

    return data.subarray( index, PolygonLength[ type ] + index )
}

function extractMultiLayerPolygon( data: Buffer, startIndex: number ) : Buffer {
    let layerSize = data.readUInt8( startIndex + 1 )
    let index = startIndex + 2

    /*
        See data layout of multi-layer polygon.
        We must account for metadata and compute actual length of each layer:
        - layer type
        - layer size
        - each metadata chunk
     */
    let polygonSize = 2 + layerSize * PolygonSize.MultiLayerMetadataSize
    while ( layerSize > 0 ) {
        let type = data.readUInt8( index )
        polygonSize += PolygonLength[ type ]
        index += PolygonSize.MultiLayerMetadataSize
        layerSize--
    }

    return data.subarray( startIndex, polygonSize + startIndex )
}

/*
    TODO:
    - add check for any blocking of destination
    - instead of keeping Array<Buffer> consider keeping Int32Array of starting position of each polygon
      idea is to reduce amount of memory a region would need when all geodata regions are loaded
 */
export class GeoRegion {
    polygons: Array<Buffer>
    constructor( data: Buffer ) {
        this.polygons = convertToPolygons( data )
    }

    /*
        We should be given relative coordinates to current world range
        Each polygon index is computed via formula of (value / 128) % 256
     */
    getHeight( xRelative: number, yRelative: number, expectedZ: number ) : number {
        let polygon = this.getBasePolygon( xRelative, yRelative )

        if ( !polygon ) {
            ServerLog.error( `Unable to find geo polygon for (x,y) (${xRelative}, ${yRelative})` )

            return expectedZ
        }

        return getPolygonHeight( polygon, xRelative, yRelative, expectedZ )
    }

    getBasePolygon( xRelative: number, yRelative: number ) : Readonly<Buffer> {
        return this.polygons[ this.getBasePolygonIndex( xRelative, yRelative ) ]
    }

    getEmbeddedPolygon( xRelative: number, yRelative: number, expectedZ: number ) : Readonly<Buffer> {
        let polygon = this.getBasePolygon( xRelative, yRelative )

        if ( !polygon ) {
            return null
        }

        if ( getPolygonType( polygon ) === GeoPolygonType.MultiLayer ) {
            return getMultiLayerPolygon( polygon, expectedZ )
        }

        return polygon
    }

    getBasePolygonIndex( xRelative: number, yRelative: number ) : number {
        let xPolygonRelative = ( xRelative >> PolygonSize.WorldPolygonShift ) % PolygonSize.PolygonsInSection
        let yPolygonRelative = ( yRelative >> PolygonSize.WorldPolygonShift ) % PolygonSize.PolygonsInSection
        return ( xPolygonRelative * PolygonSize.PolygonsInSection ) + yPolygonRelative
    }
}