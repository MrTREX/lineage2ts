import { ILocational, Location } from '../gameService/models/Location'

class Manager {
    canSeeTargetWithPosition( start: ILocational, destination: ILocational ) : boolean {
        // use lookup for DoorManager.checkIfDoorsBetween
        return true
    }

    canMoveBetweenLocations( from: ILocational, to: ILocational ) : boolean {
        // use lookup for DoorManager.checkIfDoorsBetween
        return true
    }

    canSeeTarget( from: ILocational, x: number, y: number, z: number ) : boolean {
        // use instanceId of origin location
        return true
    }

    getApproximateDestination( from: ILocational, x: number, y: number, z: number ) : Location {
        return new Location( x,y,z, from.getHeading(), from.getInstanceId() )
    }

    getInteractionDestination( from: ILocational, to: ILocational ) : ILocational {
        return to
    }
}

export const PathFinding = new Manager()