import { GeoPolygonType } from './enums/GeoPolygon'
import { PolygonValues } from './enums/PolygonValues'
import { PolygonSize } from './enums/PolygonSize'
import { PolygonLength } from './PolygonLength'
import { ServerLog } from '../logger/Logger'
import { RoaringBitmap32 } from 'roaring'

export type GeoHeightOperation = ( polygon: Buffer, xRelative: number, yRelative: number, expectedZ: number ) => number

function getCellIndex( x: number, y: number ) : number {
    let xValue = ( ( x >> PolygonSize.WorldCellShift ) % PolygonSize.Dimension )
    let yValue = ( y >> PolygonSize.WorldCellShift ) % PolygonSize.Dimension

    return ( xValue * PolygonSize.Dimension ) + yValue
}

export function extractHeight( value: number ) : number {
    return ( value & 0xFFFFFFF0 ) >> 1
}

export function extractMoveDirection( value: number ) : number {
    return value & 0x0000000F
}

function processFlatType( polygon: Buffer ) : number {
    return polygon.readInt16LE( 1 )
}

function process8bitType( polygon: Buffer, xRelative: number, yRelative: number ) : number {
    let relativeValue = polygon.readUInt8( 3 + getCellIndex( xRelative, yRelative ) )
    if ( relativeValue === PolygonValues.Block8BitNoValue ) {
        return PolygonValues.LowestHeight
    }

    return polygon.readInt16LE( 1 ) + relativeValue
}

function process16bitType( polygon: Buffer, xRelative: number, yRelative: number ) : number {
    return polygon.readInt16LE( 1 + getCellIndex( xRelative, yRelative ) * 2 )
}
function processBlocking16bitType( polygon: Buffer, xRelative: number, yRelative: number ) : number {
    return polygon.readInt16LE( 1 + getCellIndex( xRelative, yRelative ) * 2 )
}

function getL2JOffset( polygon: Buffer, dataIndex: number ) : number {
    /*
        First three bytes are metadata:
        - polygon type UInt8
        - data size, UInt16
     */
    let offset = 3

    /*
        We need to iterate over layer data to find correct index where Z values correspond to our index.
     */
    while ( dataIndex > 0 ) {
        let layerCount = polygon.readInt8( offset )
        offset = offset + 1 + ( 2 * layerCount )
        dataIndex--
    }

    return offset
}

function getMultiLayerL2JHeight( polygon: Buffer, xRelative: number, yRelative: number, expectedZ: number ) : number {
    let offset = getL2JOffset( polygon, getCellIndex( xRelative, yRelative ) )
    let geoZ = expectedZ + PolygonValues.DefaultHeightAdjustment

    let layerCount = polygon.readInt8( offset )
    offset += 1

    if ( layerCount === 1 ) {
        return polygon.readInt16LE( offset )
    }

    /*
        Now we need to match Z range of min and max into expected Z value we are provided. If we cannot find such value,
        which should not happen we are only left with choice of returning provided Z value.

        Problem of L2J multi-layer format is how a full scan of data is needed to arrive at particular Z node index
        due to variable amount of layers per node in polygon of 8x8 nodes.

        Layer values, from observation, follow from top layer to bottom (descending values, aka 1000, -1000, -5000 )
     */
    let zValue : number
    let rawValue: number

    while ( layerCount > 0 ) {
        rawValue = polygon.readInt16LE( offset )
        zValue = extractHeight( rawValue )

        if ( geoZ >= zValue ) {
            return rawValue
        }

        offset += 2
        layerCount--
    }

    return rawValue
}

export const GeoHeightMapping : Record<GeoPolygonType, GeoHeightOperation> = {
    [ GeoPolygonType.Flat ]: processFlatType,
    [ GeoPolygonType.FlatBlocking ]: processFlatType,
    [ GeoPolygonType.MultiHeight8bit ]: process8bitType,
    [ GeoPolygonType.MultiHeight16bit ]: process16bitType,
    [ GeoPolygonType.MultiHeightBlocking16bit ]: processBlocking16bitType,
    [ GeoPolygonType.MultiLayerL2J ]: getMultiLayerL2JHeight,
    [ GeoPolygonType.MultiLayer ]: getMultiLayerHeight
}

export const BlockingPolygonTypes : RoaringBitmap32 = new RoaringBitmap32( [
    GeoPolygonType.MultiHeightBlocking16bit,
    GeoPolygonType.MultiLayerL2J
] )

export function getPolygonType( polygon: Buffer ) : GeoPolygonType {
    return polygon.readUInt8() as GeoPolygonType
}

export function getPolygonHeight( polygon: Buffer, x: number, y: number, expectedZ : number ) : number {
    let type = getPolygonType( polygon )
    let method = GeoHeightMapping[ type ]
    if ( !method ) {
        ServerLog.error( `Unable to handle geo polygon type=${type}` )
        return expectedZ
    }

    let height = method( polygon, x, y, expectedZ )
    if ( BlockingPolygonTypes.has( type ) ) {
        return extractHeight( height )
    }

    return height
}

export function getPolygonHeightWithBlocking( polygon: Buffer, x: number, y: number, expectedZ : number ) : number {
    let type = getPolygonType( polygon )
    let method = GeoHeightMapping[ type ]
    if ( !method ) {
        ServerLog.error( `Unable to handle geo polygon type=${type}` )
        return expectedZ
    }

    return method( polygon, x, y, expectedZ )
}

/*
    Multi-layer data contains additional metadata about polygon type and Z range.
    Z range is used to identify which layer should be used for data lookups.
    Ideally all layers are positioned from top to bottom (as in Z values), however such order
    is not guaranteed. In case we cannot find layer we would default to later one.
    Ideally all layers will have ranges that encompass all Z values,
    however it is possible to use initial Z value that would not fall into layer ranges
    (ramifications of such lookups should always target top layer anyway, hence not part of layer decision logic).
*/
export function getMultiLayerPolygon( polygon: Buffer, expectedZ: number ) : Buffer {
    let layers = polygon.readUInt8( 1 )
    let currentLayerIndex = 0
    let startIndex = 2 + layers * PolygonSize.MultiLayerMetadataSize
    let currentLayerLength = 0

    /*
        TODO : review, additional value is determined empirically
        - client textures use static values
        - server geodata uses head level z values
        - L2J uses character height, however since dwarves are smaller than orcs, it causes issues
        - hence, static value was picked to be slightly above character's head (+10)
     */
    let geoZ = expectedZ + PolygonValues.DefaultHeightAdjustment

    while ( currentLayerIndex < layers ) {
        let subMetaIndex = 2 + currentLayerIndex * PolygonSize.MultiLayerMetadataSize
        let minZ = polygon.readInt16LE( subMetaIndex + 1 )
        let maxZ = polygon.readInt16LE( subMetaIndex + 3 )

        startIndex += currentLayerLength
        currentLayerIndex++
        currentLayerLength = PolygonLength[ polygon.readUInt8( subMetaIndex ) ]

        if ( minZ <= geoZ && maxZ >= geoZ ) {
            break
        }
    }

    return polygon.subarray( startIndex, startIndex + currentLayerLength )
}

function getMultiLayerHeight( polygon: Buffer, xRelative: number, yRelative: number, expectedZ: number ) : number {
    return getPolygonHeight( getMultiLayerPolygon( polygon, expectedZ ), xRelative, yRelative, expectedZ )
}