import { IServerPacket } from './IServerPacket'

const enum PacketWritableChunks {
    C = 1,
    D = 4,
    F = 8,
    H = 2,
    Q = 8
}

const shortIntConverter = new Int16Array( 1 )

export class DeclaredServerPacket implements IServerPacket {
    buffer: Buffer
    position: number = 2

    constructor( size: number ) {
        this.assignBuffer( size )
    }

    protected assignBuffer( size: number ) : void {
        this.buffer = Buffer.allocUnsafe( size + 2 )
        this.buffer.writeInt16LE( this.buffer.length, 0 )
    }

    getBuffer(): Buffer {
        return this.buffer
    }

    writeB( value: Buffer ): IServerPacket {
        value.copy( this.buffer, this.position )
        this.position += value.length
        return this
    }

    writeC( value: number ): IServerPacket {
        this.buffer.writeUInt8( value, this.position )
        this.position += PacketWritableChunks.C
        return this
    }

    writeD( value: number ): IServerPacket {
        this.buffer.writeInt32LE( value, this.position )
        this.position += PacketWritableChunks.D
        return this
    }

    writeF( value: number ): IServerPacket {
        this.buffer.writeDoubleLE( value, this.position )
        this.position += PacketWritableChunks.F
        return this
    }

    writeH( value: number ): IServerPacket {
        shortIntConverter[ 0 ] = value

        this.buffer.writeInt16LE( shortIntConverter[ 0 ], this.position )
        this.position += PacketWritableChunks.H
        return this
    }

    writeQ( value: number ): IServerPacket {
        this.buffer.writeBigInt64LE( BigInt( value ), this.position )
        this.position += PacketWritableChunks.Q
        return this
    }

    writeS( messageString: string ): IServerPacket {
        this.buffer.write( messageString, this.position, 'ucs2' )
        this.position += getStringSize( messageString )
        this.buffer.writeInt16LE( 0, this.position - 2 )
        return this
    }
}

export class ManagedServerPacket extends DeclaredServerPacket {

    constructor() {
        super( 0 )
    }

    setBuffer( data: Buffer ) : void {
        this.buffer = data
        this.position = 0
    }

    protected assignBuffer() {

    }
}

export class ReservedServerPacket extends DeclaredServerPacket {
    protected assignBuffer( size: number ) {
        this.buffer = Buffer.allocUnsafeSlow( size + 2 )
        this.buffer.writeInt16LE( this.buffer.length, 0 )
    }

    reset() : void {
        this.position = 2
    }
}

export function getStringSize( message: string ): number {
    return ( message.length + 1 ) * 2
}

export function getStringArraySize( messages: Array<string> ): number {
    return messages.reduce( ( total: number, message: string ): number => {
        return total + getStringSize( message )
    }, 0 )
}

export const EmptyString = ''
export const EmptyStringSize = getStringSize( EmptyString )
