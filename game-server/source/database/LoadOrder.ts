import { PunishmentManager } from '../gameService/instancemanager/PunishmentManager'
import { AnnouncementManager } from '../gameService/models/announce/AnnouncementManager'
import { CrestCache } from '../gameService/cache/CrestCache'
import { BuyListManager } from '../gameService/cache/BuyListManager'
import { ServitorSkillCache } from '../gameService/cache/ServitorSkillCache'
import { AirShipManager } from '../gameService/instancemanager/AirShipManager'
import { RaidBossPointsManager } from '../gameService/cache/RaidBossPointsManager'
import { SevenSigns } from '../gameService/directives/SevenSigns'
import { IDFactoryCache } from '../gameService/cache/IDFactoryCache'
import { L2DataApi } from '../data/interface/l2DataApi'
import { CharacterSummonCache } from '../gameService/cache/CharacterSummonCache'
import { TeleportLocationManager } from '../gameService/cache/TeleportLocationManager'
import { SiegeManager } from '../gameService/instancemanager/SiegeManager'
import { FortSiegeManager } from '../gameService/instancemanager/FortSiegeManager'
import { AreaCache } from '../gameService/cache/AreaCache'
import { L2World } from '../gameService/L2World'
import { ServerVariablesManager } from '../gameService/variables/ServerVariablesManager'
import { L2MoveManager } from '../gameService/L2MoveManager'
import { NpcRespawnManager } from '../gameService/cache/NpcRespawnManager'
import { SevenSignsFestival } from '../gameService/directives/SevenSignsFestival'

export const DatabaseLoadOrder : Array<L2DataApi> = [
    ServerVariablesManager,
    IDFactoryCache,
    PunishmentManager,
    AnnouncementManager,
    CrestCache,
    ServitorSkillCache,
    AirShipManager,
    RaidBossPointsManager,
    L2World,
    L2MoveManager,
    AreaCache, // zones are needed for castles and forts
    SevenSigns,
    SevenSignsFestival,
    SiegeManager,
    FortSiegeManager,
    CharacterSummonCache,
    TeleportLocationManager,
    NpcRespawnManager,
    BuyListManager
]