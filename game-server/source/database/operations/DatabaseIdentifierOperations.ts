export interface L2DatabaseIdentifierOperations {
    getUsedIds( ids: Array<number> ) : Promise<Array<number>>
    getHighestId(): Promise<number>
}