export interface L2DatabaseRemoveOperations {
    removeCharacter( objectId: number ): Promise<void>
}