import { L2DatabaseRemoveOperations } from '../operations/RemoveOperations'

export interface L2CharacterRecipebookTableApi extends L2DatabaseRemoveOperations {
    getCommonRecipeIds( objectId: number ) : Promise<Array<number>>
    getDwarvenRecipeIds( objectId: number, classIndex: number ) : Promise<Array<number>>
    addRecipes( items: Array<L2CharacterRecipeUpdate> ) : Promise<void>
    removeRecipes( items: Array<L2CharacterRecipeUpdate> ) : Promise<void>
}

export const enum L2CharacterRecipeUpdateType {
    Upsert,
    Delete
}

export interface L2CharacterRecipeUpdate {
    classIndex: number
    recipeId: number
    isDwarven: boolean
    objectId: number
    type: L2CharacterRecipeUpdateType
}