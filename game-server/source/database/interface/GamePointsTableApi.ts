export interface L2GamePointsTableApi {
    getPoints( name: string ) : Promise<Array<L2GamePointItem>>
    updatePoints( items: Array<L2GamePointItem> ) : Promise<void>
}

export interface L2GamePointItem {
    name: string
    type: number
    amount: number
    lastUpdate: number
    reason: string
}