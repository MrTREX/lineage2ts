import { Castle } from '../../gameService/models/entity/Castle'

export interface CastleTableApi {
    updateCrestStatus( castles: Array<Castle> ) : Promise<void>
    updateTreasury( castles: Array<Castle> ) : Promise<void>
    updateSiegeInfo( residenceId: number, siegeDate: number, timeRegistrationOverDate: number, isRegistrationOver: boolean ): Promise<void>
    updateTaxValue( castles: Array<Castle> ): Promise<void>
    updateTicketBuyCount( castles: Array<Castle> ): Promise<void>
    getAll(): Promise<Array<L2CastleTableData>>
    resetClanTax( clanId: number ): Promise<void>
}

export interface L2CastleTableData {
    id: number
    name: string
    taxPercent: number
    treasury: number
    siegeDate: number
    isRegistrationOver: boolean
    registrationTimeEnd: number
    showNpcCrest: boolean
    ticketBuyCount: number
}