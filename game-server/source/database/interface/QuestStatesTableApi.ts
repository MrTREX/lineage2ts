import { L2DatabaseRemoveOperations } from '../operations/RemoveOperations'
import { QuestState } from '../../gameService/models/quest/QuestState'

export interface QuestStatesTableApi extends L2DatabaseRemoveOperations {
    deleteData( objectId: number, questName: string ) : Promise<void>
    getData( objectId: number ) : Promise<Array<L2QuestStatesTableData>>
    createData( objectId: number, questName: string, stateValue: number, conditionValue: number, stateFlags: number ) : Promise<void>
    deleteManyNames( objectId: number, names: Array<string> ) : Promise<void>
    upsertMany( states: Set<QuestState> ) : Promise<void>
    updateVariables( states: Set<QuestState> ) : Promise<void>
    deleteMany( states: Set<QuestState> ) : Promise<void>
}

export interface L2QuestStatesTableData {
    playerId: number
    questName: string
    variables: { [ key: string] : number | string | boolean }
    condition: number
    stateFlags: number
    completionState: number
}