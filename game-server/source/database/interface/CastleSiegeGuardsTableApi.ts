export interface CastleSiegeGuardsTableApi {
    removeHiredGuards( castleId: number ) : Promise<void>
    getHiredGuards(): Promise<Array<L2CastleSiegeGuardsTableData>>
}

export interface L2CastleSiegeGuardsTableData {
    npcId: number
    x: number
    y: number
    z: number
    castleId: number
    heading: number
    respawnDelay: number
    isHired: number
}