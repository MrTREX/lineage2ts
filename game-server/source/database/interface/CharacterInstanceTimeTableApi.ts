import { L2DatabaseRemoveOperations } from '../operations/RemoveOperations'

export interface CharacterInstanceTimeTableApi extends L2DatabaseRemoveOperations {
    getTimes( playerId: number ) : Promise<Array<L2CharacterInstanceTimeTableItem>>
    deleteInstanceTime( playerId: number, ...instanceIds: Array<number> ) : Promise<void>
    addInstanceTime( playerId: number, instanceId: number, time: number ) : Promise<void>
}

export interface L2CharacterInstanceTimeTableItem {
    instanceId : number
    time : number
    playerId : number
}