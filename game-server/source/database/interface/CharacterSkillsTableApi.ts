import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { Skill } from '../../gameService/models/Skill'
import { L2DatabaseRemoveOperations } from '../operations/RemoveOperations'

export interface CharacterSkillsTableApi extends L2DatabaseRemoveOperations {
    deleteByClassIndex( objectId: number, classIndex: number ) : Promise<void>
    deleteSkill( player: L2PcInstance, skill: Skill ) : Promise<void>
    deleteSkills( player: L2PcInstance, skills: Array<Skill> ) : Promise<void>
    upsertSkills( objectId: number, classIndex: number, skills: Array<Skill> ) : Promise<void>
    getSkills( player: L2PcInstance ) : Promise<Array<Skill>>
}