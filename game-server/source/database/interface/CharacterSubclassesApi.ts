import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SubClass } from '../../gameService/models/base/SubClass'
import { L2DatabaseRemoveOperations } from '../operations/RemoveOperations'

export interface CharacterSubclassesApi extends L2DatabaseRemoveOperations {
    deleteByClassIndex( objectId: number, classIndex: number ) : Promise<void>
    getSubclassInfo( ids: Array<number> ) : Promise<Array<L2CharacterSubclassItem>>
    updatePlayer( player: L2PcInstance ) : Promise<void>
    getSubclasses( objectId: number ) : Promise<Array<L2CharacterSubclassItem>>
    addSubclass( objectId: number, subClass: SubClass ): Promise<void>
}

export interface L2CharacterSubclassItem {
    classId: number
    exp: number
    sp: number
    level: number
    objectId: number
    classIndex: number
}