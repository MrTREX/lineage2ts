import { L2DatabaseRemoveOperations } from '../operations/RemoveOperations'
import { SevenSignsSeal, SevenSignsSide } from '../../gameService/values/SevenSignsValues'

export interface SevenSignsTableApi extends L2DatabaseRemoveOperations {
    savePlayers( playerData: Array<L2SevenSignsCharacterData> ): Promise<void>
    removeAll() : Promise<void>
    getPlayer( objectId: number ) : Promise<L2SevenSignsCharacterData>
    getSideTotals() : Promise<Record<Partial<SevenSignsSide>, number>>
}

export interface L2SevenSignsCharacterData {
    objectId: number
    side: SevenSignsSide
    seal: SevenSignsSeal
    redStoneCount: number
    greenStoneCount: number
    blueStoneCount: number
    ancientAdenaCount: number
    contributionScore: number
    lastContributionTime: number
    joinTime: number
}