import { SeedProduction } from '../../gameService/models/manor/SeedProduction'

export interface CastleManorProductionTableApi {
    removeCastle( castleId: number ) : Promise<void>
    removeCastleForNextPeriod( castleId: number ) : Promise<void>
    updateSeedProduction( castleId: number, items: Array<SeedProduction> ): Promise<void>
    addCastleProducts( castleId: number, items: Array<SeedProduction> ): Promise<void>
    getAll(): Promise<Array<L2CastleManorProductionTableData>>
    deleteAll(): Promise<void>
    addAll( items: Array<L2CastleManorProductionTableData> ): Promise<void>
}

export interface L2CastleManorProductionTableData {
    castleId: number
    seedId : number
    amount: number
    startAmount: number
    price: number
    nextPeriod: number
}