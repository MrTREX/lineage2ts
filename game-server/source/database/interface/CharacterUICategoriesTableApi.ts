import { UICategories } from '../../data/interface/UIDataApi'

export interface CharacterUICategoriesTableApi {
    setCategories( objectId: number, data : UICategories ) : Promise<void>
    getCategories( objectId: number ) : Promise<UICategories>
}