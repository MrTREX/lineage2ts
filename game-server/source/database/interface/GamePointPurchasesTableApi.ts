export interface L2GamePointPurchasesTableApi {
    addPurchases( items: Array<L2GamePointPurchasesTableItem> ) : Promise<void>
    getPurchases( accountName: string ) : Promise<Array<L2GamePointPurchasesTableItem>>
}

export interface L2GamePointPurchasesTableItem {
    accountName: string
    type: number
    productId: number
    amount: number
    paidPrice: number
    time: number
    category: number
    playerId: number
}