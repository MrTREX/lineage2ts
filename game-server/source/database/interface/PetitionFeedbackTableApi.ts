import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'

export interface L2PetitionFeedbackTableApi {
    addEntry( player: L2PcInstance, rate: number, message: string ) : Promise<void>
}