import { L2DatabaseRemoveOperations } from '../operations/RemoveOperations'

export interface L2HeroesTableApi extends L2DatabaseRemoveOperations{
    resetAllPlayed(): Promise<void>
    addCharacter( objectId: number, classId: number, count: number, played: number, isClaimed: boolean ) : Promise<void>
    getAll(): Promise<Array<L2HeroesTableItem>>
}

export interface L2HeroesTableItem {
    objectId: number
    classId: number
    count: number
    played: number
    isClaimed: boolean
    message: string
}