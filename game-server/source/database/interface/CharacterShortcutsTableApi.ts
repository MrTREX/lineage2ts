import { L2DatabaseRemoveOperations } from '../operations/RemoveOperations'
import { ShortcutOwnerType, ShortcutType } from '../../gameService/enums/ShortcutType'

export interface CharacterShortcutsTableApi extends L2DatabaseRemoveOperations {
    deleteByClassIndex( playerId: number, classIndex: number ) : Promise<void>
    deleteShortcutByIds( playerId: number, classIndex: number, objectIds: Array<number>, type: ShortcutType ) : Promise<void>
    getAll( playerId: number, classIndex: number ) : Promise<Array<L2PlayerShortcutsTableItem>>
    deleteMany( items: Array<L2PlayerShortcutIds> ) : Promise<void>
    upsertMany( items: Array<L2PlayerShortcutIds> ) : Promise<void>
}

export interface L2PlayerShortcutIds {
    objectId: number
    classIndex: number
    type: ShortcutType
    id: number
}

export interface L2PlayerShortcutsTableItem extends L2PlayerShortcutIds {
    slot: number
    page: number
    level: number
    characterType: ShortcutOwnerType
    itemReuseGroup: number
    index?: number
}