export interface L2ItemAuctionBidTableApi {
    deleteBid( auctionId: number, playerId: number ) : Promise<void>
    addBid( auctionId: number, playerId: number, amount: number ) : Promise<void>
    deleteAuctions( auctionIds: Array<number> ) : Promise<void>
    getAuctionBids( auctionIds: Array<number> ) : Promise<Array<L2ItemAuctionBidTableData>>
}

export interface L2ItemAuctionBidTableData {
    id: number
    playerId: number
    amount: number
}