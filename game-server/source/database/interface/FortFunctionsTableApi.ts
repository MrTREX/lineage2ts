export interface L2FortFunctionsTableApi {
    removeAll( fortId: number, types: Array<number> ) : Promise<void>
    getAll( fortId: number ): Promise<Array<L2FortFunctionsTableData>>
    update( fortId: number, type: number, level: number, fee: number, rate: number, endDate: number ): Promise<void>
    remove( fortId: number, type: number ): Promise<void>
}

export interface L2FortFunctionsTableData {
    type: number
    level: number
    fee: number
    rate: number
    endDate: number
}