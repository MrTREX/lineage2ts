import { CropProcure } from '../../gameService/models/manor/CropProcure'

export interface CastleManorProcureTableApi {
    removeCastle( castleId: number ) : Promise<void>
    removeCastleForNextPeriod( castleId: number ) : Promise<void>
    updateItems( castleId: number, items: Array<CropProcure> ): Promise<void>
    addCastleProducts( castleId: number, items: Array<CropProcure> ): Promise<void>
    getAll(): Promise<Array<L2CastleManorProcureTableData>>
    deleteAll(): Promise<void>
    addAll( items: Array<L2CastleManorProcureTableData> ): Promise<void>
}

export interface L2CastleManorProcureTableData {
    castleId : number
    cropId : number
    amount : number
    startAmount : number
    price: number
    rewardType: number
    nextPeriod: number
}