export interface L2ServerVariablesTableApi {
    add( value: L2ServerVariablesTableItem ): Promise<void>
    addAll( values: Array<L2ServerVariablesTableItem> ): Promise<void>
    getAll(): Promise<Array<L2ServerVariablesTableItem>>
    removeAll(): Promise<void>
}

export type L2ServerVariableType = string | number | Buffer

export interface L2ServerVariablesTableItem extends L2ServerVariableData {
    name: string
    value: L2ServerVariableType
}

export interface L2ServerVariableData {
    expirationDate: number
    value : any
}