import { L2DatabaseRemoveOperations } from '../operations/RemoveOperations'

export interface CharacterSkillsSaveTableApi extends L2DatabaseRemoveOperations {
    deleteByClassIndex( objectId: number, classIndex: number ) : Promise<void>
    update( objectId: number, classIndex: number, data: Array<CharacterSkillsSaveItem> ) : Promise<void>
    load( objectId: number, classIndex: number ) : Promise<Array<CharacterSkillsSaveItem>>
    deleteReuseBySkillIds( skillIds : Array<number> ) : Promise<void>
}

export const enum CharacterSkillsSaveItemType {
    Effect,
    SkillReuse
}

export interface CharacterSkillsSaveItem {
    objectId: number
    classIndex: number
    skillId: number
    skillLevel: number
    remainingMs: number
    reuseDelay: number
    expirationTime: number
    type: CharacterSkillsSaveItemType
    index: number
}