import { L2ItemInstance } from '../../gameService/models/items/instance/L2ItemInstance'
import { L2DatabaseIdentifierOperations } from '../operations/DatabaseIdentifierOperations'
import { L2DatabaseRemoveOperations } from '../operations/RemoveOperations'
import { ItemLocation } from '../../gameService/enums/ItemLocation'

export interface L2ItemsTableApi extends L2DatabaseIdentifierOperations, L2DatabaseRemoveOperations {
    getEquippedInventory( ids: Array<number> ) : Promise<Array<L2ItemsTableEquippedItem>>
    deleteByObjectId( id: number ) : Promise<void>
    deleteByObjectIds( ids: Array<number> ) : Promise<void>
    insertItem( item: L2ItemInstance ): Promise<void>
    insertItems( items: Array<L2ItemInstance> ): Promise<void>
    updateItem( item: L2ItemInstance ): Promise<void>
    updateItems( items: Array<L2ItemInstance> ): Promise<void>
    getInventoryItems( objectId: number, baseLocation: ItemLocation, equipLocation: ItemLocation ) : Promise<Array<L2ItemsTableItem>>
    getItemsByLocation( objectId: number, baseLocation: ItemLocation ) : Promise<Array<L2ItemsTableItem>>
    deleteItemFromOwner( playerId: number, itemId: number ) : Promise<void>
    deleteItemFromOwners( itemId: number, playerIds: Array<number> ) : Promise<void>
    hasPetItems( playerObjectId: number ) : Promise<boolean>
    getItemsByItemIdAndType( itemId: number, typeOne: number ) : Promise<Array<L2ItemsTableItem>>
    getMultipleItemAttributes( ids: Array<number> ) : Promise<Array<L2ItemTableAttributes>>
}

export interface L2ItemsTableItem extends L2ItemsTableEquippedItem {
    count: number
    customTypeOne: number
    customTypeTwo: number
    itemLocation: ItemLocation
    manaRemaining: number
    timeRemaining: number
    agathionEnergy: number
    attributes: number
}

export interface L2ItemsTableEquippedItem {
    objectId: number
    ownerId: number
    itemId: number
    locationData: number
    enchantLevel: number
}

export interface L2ItemTableAttributes {
    objectId: number
    attributes: number
}