import { AbstractVariablesMap } from '../../gameService/variables/AbstractVariablesManager'

export interface L2AccountVariablesTableApi {
    getVariables( name: string ) : Promise<AbstractVariablesMap>
    deleteVariables( name: string ) : Promise<void>
    setManyVariables( variableMap: Record<string, AbstractVariablesMap>, propertyNames: Set<string | number> ) : Promise<void>
}