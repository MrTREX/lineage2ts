export interface L2NevitDataTableApi {
    getData( objectId: number ) : Promise<L2NevitDataItem>
    setData( data : L2NevitDataItem ) : Promise<void>
}

export interface L2NevitDataItem {
    objectId: number
    huntingTime: number
    effectEndTime: number
    huntingPoints: number
}