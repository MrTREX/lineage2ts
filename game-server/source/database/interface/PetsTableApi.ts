import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2PetInstance } from '../../gameService/models/actor/instance/L2PetInstance'
import { L2DatabaseIdentifierOperations } from '../operations/DatabaseIdentifierOperations'
import { L2DatabaseRemoveOperations } from '../operations/RemoveOperations'

export interface PetsTableApi extends L2DatabaseIdentifierOperations, L2DatabaseRemoveOperations {
    deleteByControlId( objectId: number ) : Promise<void>
    deleteManyByControlId( objectIds: Array<number> ) : Promise<void>
    updateFood( player: L2PcInstance, petId: number ) : Promise<void>
    createPet( pet: L2PetInstance ) : Promise<void>
    updatePet( pet: L2PetInstance ) : Promise<void>
    hasPetName( name: string ) : Promise<boolean>
    getRestoredPet( playerId: number ): Promise<L2PetTableData>
    getPet( playerId: number, controlId: number ): Promise<L2PetTableData>
    getRestoredPlayerIds() : Promise<Array<number>>
    setRestored( pet: L2PetInstance, isRespawned: boolean ) : Promise<void>
}

export interface L2PetTableData {
    controlId: number
    name: string
    level: number
    hp: number
    mp: number
    exp: number
    sp: number
    feed: number
    ownerId: number
    isRestored: boolean
}