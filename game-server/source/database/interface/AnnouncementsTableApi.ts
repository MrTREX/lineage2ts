import { IAnnouncement } from '../../gameService/models/announce/IAnnouncement'
import { GeneralAnnouncement } from '../../gameService/models/announce/GeneralAnnouncement'
import { AutoAnnouncement } from '../../gameService/models/announce/AutoAnnouncement'

export interface AnnouncementsTableApi {
    getAll() : Promise<Array<IAnnouncement>>
    createGeneral( item: GeneralAnnouncement ) : Promise<number>
    updateGeneral( item: GeneralAnnouncement ) : Promise<void>
    deleteItem( item: IAnnouncement ) : Promise<void>
    createAuto( item: AutoAnnouncement ) : Promise<number>
    updateAuto( item: AutoAnnouncement ) : Promise<void>
}