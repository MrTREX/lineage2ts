import { L2DatabaseRemoveOperations } from '../operations/RemoveOperations'

export interface L2CharacterRaidPointsTableApi extends L2DatabaseRemoveOperations {
    getAll() : Promise<{ [ playerId : number ] : Map<number, number> }> // playerId -> { raidId : points }
    updatePoints( objectId: number, raidId: number, points: number ) : Promise<void>
    removeAll() : Promise<void>
}