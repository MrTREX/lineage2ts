import { L2ItemInstance } from '../../gameService/models/items/instance/L2ItemInstance'

export interface ItemElementalsTableApi {
    deleteByObjectId( id: number ) : Promise<void>
    deleteByObjectIds( ids: Array<number> ) : Promise<void>
    insertItemElementals( item: L2ItemInstance ): Promise<void>
    getAttributes( objectId: number ) : Promise<Array<L2ItemElementalTableData>>
    updateItems( items: Array<L2ItemInstance> ): Promise<void>
    deleteByElementType( objectId: number, element: number ): Promise<void>
}

export interface L2ItemElementalTableData {
    type: number
    value: number
    objectId: number
}