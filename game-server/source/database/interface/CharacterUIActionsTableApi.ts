import { UIKeys } from '../../data/interface/UIDataApi'

export interface CharacterUIActionsTableApi {
    setKeys( objectId: number, data: UIKeys ) : Promise<void>
    getKeys( objectId: number ) : Promise<UIKeys>
}