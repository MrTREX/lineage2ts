export interface L2FortDoorUpgradeTableApi {
    getAll( residenceId: number ) : Promise<Array<L2FortDoorUpgradeTableData>>
    removeAll( residenceId: number ) : Promise<void>
    addUpgrade( residenceId: number, doorId: number, hpValue: number, powerDeference: number, magicDefence: number ) : Promise<void>
}

export interface L2FortDoorUpgradeTableData {
    doorId: number
    hpValue: number
    powerDefence: number
    magicDefence: number
}