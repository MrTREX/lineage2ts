export interface L2ClanPrivilegesTableApi {
    getAll( clanId: number ) : Promise<{ [ key: number ] : number }>
    storePrivilege( clanId: number, rank: number, privilegeMask: number ) : Promise<void>
    deleteClan( clanId: number ) : Promise<void>
}