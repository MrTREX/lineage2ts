import { L2Clan } from '../../gameService/models/L2Clan'
import { L2DatabaseIdentifierOperations } from '../operations/DatabaseIdentifierOperations'

export interface ClanDataTableApi extends L2DatabaseIdentifierOperations{
    resetCastleStatus( residenceId: number ) : Promise<void>
    setCastleOwnership( residenceId: number, ownerId: number ) : Promise<void>
    setReputationScore( clanId: number, scoreAmount: number ) : Promise<void>
    updateMultipleClanStatuses( clans: Array<L2Clan> ) : Promise<void>
    updateAuction( clanId: number, auctionId: number ) : Promise<void>
    updateCrest( clanId: number, crestId: number ) : Promise<void>
    updateCrestForClan( clanId: number, crestId: number ) : Promise<void>
    updateLargeCrestForClan( clanId: number, crestId: number ) : Promise<void>
    updateCrestForAlly( allyId: number, crestId: number ) : Promise<void>
    updateBloodOathCount( clanId: number, count: number ): Promise<any>
    getCastleOwnerIds(): Promise<Record<number, number>>
    updateClanLevel( clanId: number, level: number ): Promise<void>
    updateBloodAllianceCount( clanId: number, count: number ): Promise<void>
    createClan( clan: L2Clan ): Promise<void>
    deleteClan( clanId: number ) : Promise<void>
    getAll(): Promise<Array<L2ClanTableItem>>
}

export interface L2ClanTableItem {
    id: number
    name: string
    level: number
    ownedCastleId: number
    bloodAllianceCount: number
    bloodOathCount: number
    allyId: number
    allyName: string
    allyPenaltyExpiryTime: number
    allyPenaltyType: number
    joinPenaltyExpiryTime: number
    dissolveExpiryTime: number
    crestId: number
    crestLargeId: number
    allyCrestId: number
    reputation: number
    lastAuctionBidId: number
    newLeaderId: number
    currentLeaderId: number
}