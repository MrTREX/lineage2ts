export interface L2ClanNoticesTableApi {
    getNotice( clanId: number ) : Promise<L2ClanNoticesTableItem>
    deleteClan( clanId: number ) : Promise<void>
    createNotice( clanId: number, text: string, status: L2ClanNoticeStatus ) : Promise<void>
}

export const enum L2ClanNoticeStatus {
    None,
    Enabled
}

export interface L2ClanNoticesTableItem {
    status: L2ClanNoticeStatus
    text: string
    clanId: number
}