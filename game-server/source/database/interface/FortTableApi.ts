import { FortState } from '../../gameService/enums/FortState'

export interface FortTableApi {
    getAll() : Promise<Array<L2FortTableData>>
    updateAll( items: Array<L2FortTableData> ) : Promise<void>
}

export interface L2FortTableData {
    id: number
    name: string
    siegeDate: number
    lastOwnedTime: number
    ownerId: number
    fortType: number
    state: FortState
    castleId: number
    supplyLevel: number
}