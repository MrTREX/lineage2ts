export interface L2ClanHallAuctionsTableApi {
    removeAuction( id: number ) : Promise<void>
    updateEndDate( id: number, endDate: number ): Promise<void>
    addAuction( data : L2ClanHallAuctionItem ) : Promise<void>
}

export interface L2ClanHallAuctionItem {
    id: number
    endDate: number
    // TODO : why separate itemId? rename or remove
    itemId: number
    itemName: string
    itemObjectId: number
    itemQuantity: number
    itemType: string
    sellerId: number
    sellerClanName: string
    sellerName: string
    currentBid: number
    startingBid: number
}