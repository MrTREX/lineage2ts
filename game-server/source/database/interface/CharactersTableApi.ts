import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2ClanMember } from '../../gameService/models/L2ClanMember'
import { L2DatabaseIdentifierOperations } from '../operations/DatabaseIdentifierOperations'
import { L2DatabaseRemoveOperations } from '../operations/RemoveOperations'

export interface CharactersTableApi extends L2DatabaseIdentifierOperations, L2DatabaseRemoveOperations {
    addPlayerCharacter( player: L2PcInstance ): Promise<void>

    getAccountCharacterCount( accountName: string ): Promise<number>

    getCharactersDeleteTime( accountName: string ): Promise<Array<number>>

    getActiveCharacters( loginName: string ): Promise<Array<L2CharactersTableFullInfo>>

    getClanIdForCharacter( characterId: number ): Promise<number>

    getClanMembers( clanId: number ): Promise<Array<L2ClanMember>>

    getNameById( id: number ): Promise<string>

    getNameByIds( ids: Array<number> ): Promise<Array<L2CharactersTableNamePair>>

    isExistingCharacterName( characterName: string ): Promise<boolean>

    loadPlayer( objectId: number ): Promise<L2PcInstance>

    resetApprentice( objectId: number ): Promise<void>

    resetSponsor( objectId: number ): Promise<void>

    setApprenticeAndSponsor( objectId: number, apprenticeId: number, sponsorId: number ): Promise<void>

    updateClanStatus( objectId: number, title: string, joinExpiryTime: number, createExpiryTime: number ): Promise<void>

    updateDeleteTime( characterId: number, daysLeft: number, useDirectValue: boolean ): Promise<void>

    updateKarmaStatus( karma: number, pkKills: number, playerId: number ): Promise<void>

    updateOnlineStatus( player: L2PcInstance ): Promise<void>

    updatePlayerCharacter( player: L2PcInstance ): Promise<void>

    getOtherCharacterNames( objectId: number, accountName: string ): Promise<Map<number, string>>

    getIdByName( name: string ): Promise<number>

    getAccessLevelById( objectId: number ): Promise<number>

    updatePowerGrade( objectId: number, grade: number ): Promise<void>

    updatePledge( objectId: number, pledgeType: number ): Promise<void>

    getFriendListItems( objectIds: Array<number> ): Promise<Array<L2CharactersTableFriendInfo>>

    setClanPrivileges( objectId: number, privilegesMask: number ): Promise<void>

    getClanInfoByIds( ids: Array<number> ): Promise<Array<L2CharactersTableClanInfo>>

    setAccessLevel( objectId: number, level: number ): Promise<void>

    updateLocation( objectId: number, x: number, y: number, z: number ): Promise<void>
    getDeleteCharacterCount() : Promise<number>
    getDeleteCharacterIds( amount : number ) : Promise<Array<number>>
}

export interface L2CharactersTableNamePair {
    objectId: number
    name: string
}

export interface L2CharactersTableClanInfo extends L2CharactersTableNamePair {
    clanId: number
}

export interface L2CharactersTableFriendInfo extends L2CharactersTableNamePair {
    level: number
    classId: number
}

export interface L2CharactersTableFullInfo extends L2CharactersTableFriendInfo, L2CharactersTableClanInfo {
    deleteTime: number
    lastAccessTime: number
    baseClassId: number
    maxHp: number
    currentHp: number
    maxCp: number
    currentCp: number
    maxMp: number
    currentMp: number
    face: number
    hairStyle: number
    hairColor: number
    exp: number
    sp: number
    race: number
    sex: number
    karma: number
    pkKills: number
    pvpKills: number
    x: number
    y: number
    z: number
    vitalityPoints: number
    accessLevel: number
}