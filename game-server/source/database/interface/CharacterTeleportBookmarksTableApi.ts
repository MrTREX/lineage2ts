import { ILocational } from '../../gameService/models/Location'

export interface L2CharacterTeleportBookmarksTableApi {
    add( objectId: number, id: number, location: ILocational, icon: number, tag: string, name: string ): Promise<void>
    getAll( objectId: number ): Promise<{ [ key: number ]: L2CharacterTeleportBookmarkItem }>
    update( objectId: number, id: number, icon: number, tag: string, name: string ): Promise<void>
    delete( objectId: number, id: number ): Promise<void>
}

export interface L2CharacterTeleportBookmarkItem {
    id: number
    icon: number
    name: string
    tag: string
    x: number
    y: number
    z: number
    heading: number
}