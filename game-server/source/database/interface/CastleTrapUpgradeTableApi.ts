export interface L2CastleTrapUpgradeTableApi {
    getAll( residenceId: number ) : Promise<Array<L2CastleTrapUpgradeTableData>>
    update( residenceId: number, index: number, level: number ): Promise<void>
    remove( residenceId: number ): Promise<void>
}

export interface L2CastleTrapUpgradeTableData {
    towerIndex: number
    level: number
}