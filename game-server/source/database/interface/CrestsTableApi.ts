import { CrestType, L2Crest } from '../../gameService/models/L2Crest'

export interface L2CrestsTableApi {
    getAll() : Promise<Array<L2Crest>>
    addCrest( id: number, image: Buffer, type: CrestType ) : Promise<void>
    removeCrest( id: number ) : Promise<void>
}