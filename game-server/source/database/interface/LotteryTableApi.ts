export interface L2LotteryTableApi {
    getLastItem() : Promise<L2LotteryTableData>
    addItem( id: number, endDate: number, currentPrize: number, nextPrice: number ): Promise<void>
    updateItem( data: L2LotteryTableData ) : Promise<void>
    updatePrice( id: number, currentPrice: number, nextPrice: number ) : Promise<void>
    getItems( id: number ) : Promise<Array<L2LotteryTableData>>
}

export interface L2LotteryTableData {
    id: number
    prize: number
    nextPrize: number
    endDate: number
    isFinished: boolean
    enchant: number
    type: number
    prizeOne: number
    prizeTwo: number
    prizeThree: number
}