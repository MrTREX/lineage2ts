export interface L2ClanPledgesTableApi {
    updatePledge( leaderId: number, name: string, clanId: number, pledgeId: number ) : Promise<void>
    getPledges( clanId: number ) : Promise<Array<L2ClanPledgeTableItem>>
    deleteClan( clanId: number ) : Promise<void>
    addPledge( id: number, pledgeType: any, pledgeName: string, leaderId: number ): Promise<void>
}

export interface L2ClanPledgeTableItem {
    type: number
    name: string
    clanId: number
    leaderId: number
}