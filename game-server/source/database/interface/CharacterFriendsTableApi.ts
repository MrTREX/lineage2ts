import { L2DatabaseRemoveOperations } from '../operations/RemoveOperations'

export interface CharacterFriendsTableApi extends L2DatabaseRemoveOperations {
    getFriendIds( objectId: number ): Promise<Array<number>>
    getBlocklistIds( objectId: number ): Promise<Array<number>>
    addFriends( items: Array<L2CharacterFriendUpdate> ) : Promise<void>
    addBlocklists( items: Array<L2CharacterFriendUpdate> ) : Promise<void>
    removeFriends( items: Array<L2CharacterFriendUpdate> ) : Promise<void>
    removeBlocklists( items: Array<L2CharacterFriendUpdate> ) : Promise<void>
}

export interface L2CharacterFriendUpdate {
    fromId: number
    toId: number
}