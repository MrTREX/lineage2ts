import { L2DatabaseIdentifierOperations, } from '../operations/DatabaseIdentifierOperations'
import { ItemInstanceType } from '../../gameService/models/items/ItemInstanceType'

export interface L2ItemsOnGroundTableApi extends L2DatabaseIdentifierOperations{
    removeAll() : Promise<void>
    getAll() : Promise<Array<L2ItemsOnGroundTableData>>
    addAll( data : Array<L2ItemsOnGroundTableData> ) : Promise<void>
    removeByDropTime( time: number ) : Promise<void>
}

export interface L2ItemsOnGroundTableData {
    objectId: number
    itemId: number
    count: number
    enchantLevel: number
    x: number
    y: number
    z: number
    dropTime: number
    type: ItemInstanceType
}