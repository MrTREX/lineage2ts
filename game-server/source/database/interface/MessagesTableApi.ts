import { L2DatabaseRemoveOperations } from '../operations/RemoveOperations'

export interface MessagesTableApi extends L2DatabaseRemoveOperations {
    getMessages( objectId: number ) : Promise<Array<L2MessagesTableItem>>
    deleteMessages( ids: Array<number> ) : Promise<void>

    /*
        Upsert new or old message.
     */
    updateMessages( items: Array<L2MessagesTableItem> ) : Promise<void>

    /*
        Deleting all read messages (cod/normal) without any attachments, using expiration that is less than
        provided epoch time.
     */
    deleteReadMessagesOlderThan( pastEpoch: number ) : Promise<void>

    /*
        Getting all un-read non-cod messages with or without any attachments, using expiration that is less than
        provided epoch time.
     */
    getUnreadMessagesOlderThan( pastEpoch: number ) : Promise<Array<L2MessagesTableItem>>

    /*
        All COD messages that contain attachments only with expiration time less that provided epoch time.
     */
    getCodMessagesOlderThan( pastEpoch: number ) : Promise<Array<L2MessagesTableItem>>
}

export const enum L2MessagesTableItemStatus {
    Read = 0,
    Unread = 1,
}

export interface L2MessagesTableItem {
    messageId: number
    senderId: number
    receiverId: number
    subject: string
    content: string
    expirationTime: number
    creationTime: number
    codAdena: number
    hasAttachments: boolean
    isReturned: boolean
    status : L2MessagesTableItemStatus
    sentBySystem: number
}