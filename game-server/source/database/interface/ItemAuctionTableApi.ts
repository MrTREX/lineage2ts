import { ItemAuctionState } from '../../gameService/models/auction/ItemAuctionState'

export interface L2ItemAuctionTableApi {
    getLastId() : Promise<number>
    deleteAuctions( ids: Array<number> ) : Promise<void>
    getAuction( id: number ) : Promise<L2ItemAuctionTableData>
    getAuctions( npcIds: Array<number> ): Promise<Array<L2ItemAuctionTableData>>
    addAuction( id: number, npcId: number, auctionDataId: number, startTime: number, endTime: number, state: ItemAuctionState ): Promise<void>
}

export interface L2ItemAuctionTableData {
    npcId: number
    id: number
    auctionDataId: number
    startTime: number
    endTime: number
    state: number
}