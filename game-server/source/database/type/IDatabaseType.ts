import { AnnouncementsTableApi } from '../interface/AnnouncementsTableApi'
import { CharacterFriendsTableApi } from '../interface/CharacterFriendsTableApi'
import { QuestStatesTableApi } from '../interface/QuestStatesTableApi'
import { CharacterRecommendationBonusTableApi } from '../interface/CharacterRecommendationBonusTableApi'
import { CharacterShortcutsTableApi } from '../interface/CharacterShortcutsTableApi'
import { CharacterSkillsTableApi } from '../interface/CharacterSkillsTableApi'
import { CharactersTableApi } from '../interface/CharactersTableApi'
import { ItemElementalsTableApi } from '../interface/ItemElementalsTableApi'
import { MessagesTableApi } from '../interface/MessagesTableApi'
import { PetsTableApi } from '../interface/PetsTableApi'
import { CharacterSubclassesApi } from '../interface/CharacterSubclassesApi'
import { CharacterSkillsSaveTableApi } from '../interface/CharacterSkillsSaveTableApi'
import { CharacterItemReuseSaveTableApi } from '../interface/CharacterItemReuseSaveTableApi'
import { CharacterRecipeShoplistTableApi } from '../interface/CharacterRecipeShoplistTableApi'
import { CharacterUICategoriesTableApi } from '../interface/CharacterUICategoriesTableApi'
import { CharacterUIActionsTableApi } from '../interface/CharacterUIActionsTableApi'
import { SevenSignsTableApi } from '../interface/SevenSignsTableApi'
import { ServitorsTableApi } from '../interface/ServitorsTableApi'
import { PetSkillsSaveTableApi } from '../interface/PetSkillsSaveTableApi'
import { CharacterSummonSkillsSaveTableApi } from '../interface/CharacterSummonSkillsSaveTableApi'
import { CharacterInstanceTimeTableApi } from '../interface/CharacterInstanceTimeTableApi'
import { CharacterMacrosTableApi } from '../interface/CharacterMacrosTableApi'
import { CursedWeaponsTableApi } from '../interface/CursedWeaponsTableApi'
import { CastleTableApi } from '../interface/CastleTableApi'
import { ClanDataTableApi } from '../interface/ClanDataTableApi'
import { CastleManorProductionTableApi } from '../interface/CastleManorProductionTableApi'
import { CastleManorProcureTableApi } from '../interface/CastleManorProcureTableApi'
import { CastleSiegeGuardsTableApi } from '../interface/CastleSiegeGuardsTableApi'
import { FortTableApi } from '../interface/FortTableApi'
import { L2FortFunctionsTableApi } from '../interface/FortFunctionsTableApi'
import { PunishmentsTableApi } from '../interface/PunishmentsTableApi'
import { ClanhallFunctionsTableApi } from '../interface/ClanhallFunctionsTableApi'
import { ClanhallTableApi } from '../interface/ClanhallTableApi'
import { L2ClanPledgesTableApi } from '../interface/ClanSubpledgesTableApi'
import { L2ClanPrivilegesTableApi } from '../interface/ClanPrivilegesTableApi'
import { L2ClanSkillsTableApi } from '../interface/ClanSkillsTableApi'
import { L2ClanNoticesTableApi } from '../interface/ClanNoticesTableApi'
import { L2CharacterHennasTableApi } from '../interface/CharacterHennasTableApi'
import { L2CharacterTeleportBookmarksTableApi } from '../interface/CharacterTeleportBookmarksTableApi'
import { L2CharacterRecipebookTableApi } from '../interface/CharacterRecipebookTableApi'
import { L2DimensionalTransferItemsTableApi } from '../interface/DimensionalTransferItemsTableApi'
import { L2CharacterVariablesTableApi } from '../interface/CharacterVariablesTableApi'
import { L2AccountVariablesTableApi } from '../interface/AccountVariablesTableApi'
import { L2ClanWarsTableApi } from '../interface/ClanWarsTableApi'
import { L2CrestsTableApi } from '../interface/CrestsTableApi'
import { L2BuylistsTableApi } from '../interface/BuylistsTableApi'
import { L2AirshipsTableApi } from '../interface/AirshipsTableApi'
import { L2SiegeClansTableApi } from '../interface/SiegeClansTableApi'
import { L2PetitionFeedbackTableApi } from '../interface/PetitionFeedbackTableApi'
import { L2ItemAuctionBidTableApi } from '../interface/ItemAuctionBidTableApi'
import { L2CharacterRaidPointsTableApi } from '../interface/CharacterRaidPointsTableApi'
import { L2TerritoryRegistrationsTableApi } from '../interface/TerritoryRegistrationsTableApi'
import { L2CharacterContactsTableApi } from '../interface/CharacterContactsTableApi'
import { L2ItemsTableApi } from '../interface/ItemsTableApi'
import { L2MerchantLeaseTableApi } from '../interface/MerchantLeaseTableApi'
import { L2OlympiadNoblesTableApi } from '../interface/OlympiadNoblesTableApi'
import { L2HeroesTableApi } from '../interface/HeroesTableApi'
import { L2HeroDiaryTableApi } from '../interface/HeroDiaryTableApi'
import { L2GrandbossDataTableApi } from '../interface/GrandbossDataTableApi'
import { L2GrandbossPlayersTableApi } from '../interface/GrandbossPlayersTableApi'
import { L2FortSiegeClansTableApi } from '../interface/FortSiegeClansTableApi'
import { L2CastleFunctionsTableApi } from '../interface/CastleFunctionsTableApi'
import { L2CastleDoorUpgradeTableApi } from '../interface/CastleDoorUpgradeTableApi'
import { L2CastleTrapUpgradeTableApi } from '../interface/CastleTrapUpgradeTableApi'
import { L2ClanHallAuctionBidTableApi } from '../interface/ClanHallAuctionBidTableApi'
import { L2FortDoorUpgradeTableApi } from '../interface/FortDoorUpgradeTableApi'
import { L2ItemAuctionTableApi } from '../interface/ItemAuctionTableApi'
import { L2BotReportsTableApi } from '../interface/BotReportsTableApi'
import { L2ClanHallAuctionsTableApi } from '../interface/ClanHallAuctionsTableApi'
import { L2LotteryTableApi } from '../interface/LotteryTableApi'
import { L2OlympiadDataTableApi } from '../interface/OlympiadDataTableApi'
import { L2FishingChampionshipTableApi } from '../interface/FishingChampionshipTableApi'
import { L2ServerVariablesTableApi } from '../interface/ServerVariablesTableApi'
import { L2ItemsOnGroundTableApi } from '../interface/ItemsOnGroundTableApi'
import { L2NevitDataTableApi } from '../interface/NevitDataTableApi'
import { L2SiegableHallTableApi } from '../interface/SiegableHallTableApi'
import { L2NpcRespawnTableApi } from '../interface/NpcRespawnTableApi'
import { L2DatabaseOperations } from '../operations/DatabaseOperations'
import { L2GamePointsTableApi } from '../interface/GamePointsTableApi'
import { L2GamePointPurchasesTableApi } from '../interface/GamePointPurchasesTableApi'

export interface IDatabaseType {
    announcementsTable: AnnouncementsTableApi
    characterFriendsTable: CharacterFriendsTableApi
    questStatesTable: QuestStatesTableApi
    characterRecommendationBonusTable: CharacterRecommendationBonusTableApi
    characterShortcutsTable: CharacterShortcutsTableApi
    characterSkillsTable: CharacterSkillsTableApi
    charactersTable: CharactersTableApi
    characterSubclassesTable: CharacterSubclassesApi
    itemElementalsTable: ItemElementalsTableApi
    itemsTable: L2ItemsTableApi
    messagesTable: MessagesTableApi
    petsTable: PetsTableApi
    characterSkillsSaveTable: CharacterSkillsSaveTableApi
    characterItemReuseSaveTable: CharacterItemReuseSaveTableApi
    characterRecipeShoplistTable: CharacterRecipeShoplistTableApi
    characterUICategoriesTable: CharacterUICategoriesTableApi
    characterUIActionsTable: CharacterUIActionsTableApi
    sevenSignsTable: SevenSignsTableApi
    servitorsTable: ServitorsTableApi
    characterPetSkillsSave: PetSkillsSaveTableApi
    characterSummonSkillsSave: CharacterSummonSkillsSaveTableApi
    characterInstanceTime: CharacterInstanceTimeTableApi
    characterMacrosTable: CharacterMacrosTableApi
    cursedWeaponsTable: CursedWeaponsTableApi
    castleTable: CastleTableApi
    clanDataTable: ClanDataTableApi
    castleManorProduction: CastleManorProductionTableApi
    castleManorProcure: CastleManorProcureTableApi
    castleSiegeGuards: CastleSiegeGuardsTableApi
    fortTable: FortTableApi
    fortFunctionsTable: L2FortFunctionsTableApi
    punishmentsTable: PunishmentsTableApi
    clanhallFunctionsTable: ClanhallFunctionsTableApi
    clanhallTable: ClanhallTableApi
    clanSubpledgesTable: L2ClanPledgesTableApi
    clanPrivilegesTable: L2ClanPrivilegesTableApi
    clanSkillsTable: L2ClanSkillsTableApi
    clanNoticesTable: L2ClanNoticesTableApi
    characterHennasTable: L2CharacterHennasTableApi
    characterTeleportBookmarks: L2CharacterTeleportBookmarksTableApi
    characterRecipebook: L2CharacterRecipebookTableApi
    dimensionalTransferItems: L2DimensionalTransferItemsTableApi
    characterVariables: L2CharacterVariablesTableApi
    accountVariables: L2AccountVariablesTableApi
    clanWarsTable: L2ClanWarsTableApi
    crestsTable: L2CrestsTableApi
    buylistsTable: L2BuylistsTableApi
    airshipsTable: L2AirshipsTableApi
    siegeClansTable: L2SiegeClansTableApi
    petitionFeedbackTable: L2PetitionFeedbackTableApi
    itemAuctionBidTable: L2ItemAuctionBidTableApi
    characterRaidPointsTable: L2CharacterRaidPointsTableApi
    territoryRegistrationsTable: L2TerritoryRegistrationsTableApi
    characterContactsTable: L2CharacterContactsTableApi
    merchantLeaseTable: L2MerchantLeaseTableApi
    olympiadNoblesTable: L2OlympiadNoblesTableApi
    heroesTable: L2HeroesTableApi
    heroDiary: L2HeroDiaryTableApi
    grandbossData: L2GrandbossDataTableApi
    grandbossPlayers: L2GrandbossPlayersTableApi
    fortSiegeClans: L2FortSiegeClansTableApi
    castleFunctions: L2CastleFunctionsTableApi
    castleDoorUpgrade: L2CastleDoorUpgradeTableApi
    castleTrapUpgrade: L2CastleTrapUpgradeTableApi
    auctionBidTable: L2ClanHallAuctionBidTableApi
    fortDoorUpgrade: L2FortDoorUpgradeTableApi
    itemAuction: L2ItemAuctionTableApi
    botReports: L2BotReportsTableApi
    clanHallAuctions: L2ClanHallAuctionsTableApi
    lotteryTable: L2LotteryTableApi
    olympiadData: L2OlympiadDataTableApi
    fishingChampionship: L2FishingChampionshipTableApi
    serverVariables : L2ServerVariablesTableApi
    itemsOnGround: L2ItemsOnGroundTableApi
    nevitData : L2NevitDataTableApi
    siegableHalls : L2SiegableHallTableApi
    npcRespawns: L2NpcRespawnTableApi
    operations: L2DatabaseOperations
    gamePoints: L2GamePointsTableApi
    gamePointPurchases: L2GamePointPurchasesTableApi
}