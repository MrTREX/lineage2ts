import { ClanDataTableApi, L2ClanTableItem } from '../../interface/ClanDataTableApi'
import { L2Clan } from '../../../gameService/models/L2Clan'
import _ from 'lodash'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteClanDataTable : ClanDataTableApi = {
    async getHighestId(): Promise<number> {
        const query = 'SELECT MAX(clan_id) as clan_id, MAX(ally_id) as ally_id, MAX(leader_id) as leader_id FROM clan_data'
        let databaseItem : any = databaseEngine.getOne( query )

        if ( !databaseItem ) {
            return 0
        }

        return Math.max(
                _.defaultTo( databaseItem[ 'clan_id' ], 0 ),
                _.defaultTo( databaseItem[ 'ally_id' ], 0 ),
                _.defaultTo( databaseItem[ 'leader_id' ], 0 ),
        )
    },

    async getAll(): Promise<Array<L2ClanTableItem>> {
        const query = 'SELECT * FROM clan_data'
        let databaseItems : Array<any> = databaseEngine.getMany( query )

        return databaseItems.map( ( databaseItem: any ) : L2ClanTableItem => {
            return {
                id: databaseItem[ 'clan_id' ],
                name: databaseItem[ 'clan_name' ],
                level: databaseItem[ 'clan_level' ],
                ownedCastleId: databaseItem[ 'hasCastle' ],
                bloodAllianceCount: databaseItem[ 'blood_alliance_count' ],
                bloodOathCount: databaseItem[ 'blood_oath_count' ],
                allyId: databaseItem[ 'ally_id' ],
                allyName: databaseItem[ 'ally_name' ],
                allyPenaltyExpiryTime: databaseItem[ 'ally_penalty_expiry_time' ],
                allyPenaltyType: databaseItem[ 'ally_penalty_type' ],
                joinPenaltyExpiryTime: databaseItem[ 'char_penalty_expiry_time' ],
                dissolveExpiryTime: databaseItem[ 'dissolving_expiry_time' ],
                crestId: databaseItem[ 'crest_id' ],
                crestLargeId: databaseItem[ 'crest_large_id' ],
                allyCrestId: databaseItem[ 'ally_crest_id' ],
                reputation: databaseItem[ 'reputation_score' ],
                lastAuctionBidId: databaseItem[ 'auction_bid_at' ],
                newLeaderId: databaseItem[ 'new_leader_id' ],
                currentLeaderId: databaseItem[ 'leader_id' ]
            }
        } )
    },

    async deleteClan( clanId: number ): Promise<void> {
        const query = 'DELETE FROM clan_data WHERE clan_id=?'
        return databaseEngine.insertOne( query, clanId )
    },

    async createClan( clan: L2Clan ): Promise<void> {
        const query = 'INSERT OR REPLACE INTO clan_data (clan_id,clan_name,clan_level,hasCastle,blood_alliance_count,blood_oath_count,ally_id,ally_name,leader_id,crest_id,crest_large_id,ally_crest_id,new_leader_id) values (?,?,?,?,?,?,?,?,?,?,?,?,?)'
        return databaseEngine.insertOne( query,
                clan.getId(),
                clan.getName(),
                clan.getLevel(),
                clan.getCastleId(),
                clan.getBloodAllianceCount(),
                clan.getBloodOathCount(),
                clan.getAllyId(),
                clan.getAllyName(),
                clan.getLeaderId(),
                clan.getCrestId(),
                clan.getCrestLargeId(),
                clan.getAllyCrestId(),
                clan.getNewLeaderId() )
    },

    async updateBloodAllianceCount( clanId: number, count: number ): Promise<void> {
        const query = 'UPDATE clan_data SET blood_alliance_count=? WHERE clan_id=?'
        return databaseEngine.insertOne( query, count, clanId )
    },

    async updateClanLevel( clanId: number, level: number ): Promise<void> {
        const query = 'UPDATE clan_data SET clan_level = ? WHERE clan_id = ?'
        return databaseEngine.insertOne( query, level, clanId )
    },

    async getCastleOwnerIds(): Promise<Record<number, number>> {
        const query = 'SELECT clan_id, hasCastle FROM clan_data WHERE hasCastle > 0'
        let databaseItems = databaseEngine.getMany( query )
        return _.reduce( databaseItems, ( allCastles: Record<number, number>, item: Object ) : Record<number, number> => {
            allCastles[ item[ 'hasCastle' ] ] = item[ 'clan_id' ]

            return allCastles
        }, {} )
    },

    async getUsedIds( ids: Array<number> ): Promise<Array<number>> {
        const clanIdQuery = 'SELECT clan_id FROM clan_data WHERE clan_id in (#values#)'
        const allyIdQuery = 'SELECT ally_id FROM clan_data WHERE ally_id in (#values#)'
        const leaderIdQuery = 'SELECT leader_id FROM clan_data WHERE leader_id in (#values#)'

        const clanIds : Array<number> = _.map( databaseEngine.getManyForValues( clanIdQuery, ids ), 'clan_id' )
        const allyIds : Array<number> = _.map( databaseEngine.getManyForValues( allyIdQuery, ids ), 'ally_id' )
        const leaderIds : Array<number> = _.map( databaseEngine.getManyForValues( leaderIdQuery, ids ), 'leader_id' )

        return _.union( clanIds, allyIds, leaderIds )
    },

    async updateBloodOathCount( clanId: number, count: number ): Promise<any> {
        const query = 'UPDATE clan_data SET blood_oath_count=? WHERE clan_id=?'
        return databaseEngine.insertOne( query, count, clanId )
    },

    async updateLargeCrestForClan( clanId: number, crestId: number ): Promise<void> {
        const query = 'UPDATE clan_data SET crest_large_id = ? WHERE clan_id = ?'
        return databaseEngine.insertOne( query, crestId, clanId )
    },

    async updateCrestForAlly( allyId: number, crestId: number ): Promise<void> {
        const query = 'UPDATE clan_data SET ally_crest_id = ? WHERE ally_id = ?'
        return databaseEngine.insertOne( query, crestId, allyId )
    },

    async updateCrestForClan( clanId: number, crestId: number ): Promise<void> {
        const query = 'UPDATE clan_data SET ally_crest_id = ? WHERE clan_id = ?'
        return databaseEngine.insertOne( query, crestId, clanId )
    },

    async updateCrest( clanId: number, crestId: number ): Promise<void> {
        const query = 'UPDATE clan_data SET crest_id = ? WHERE clan_id = ?'
        return databaseEngine.insertOne( query, crestId, clanId )
    },

    async updateAuction( clanId: number, auctionId: number ): Promise<void> {
        let query = 'UPDATE clan_data SET auction_bid_at=? WHERE clan_id=?'
        return databaseEngine.insertOne( query, auctionId, clanId )
    },

    async updateMultipleClanStatuses( clans: Array<L2Clan> ) : Promise<void> {
        let query = 'UPDATE clan_data SET leader_id=?,ally_id=?,ally_name=?,ally_crest_id=?,reputation_score=?,ally_penalty_expiry_time=?,ally_penalty_type=?,char_penalty_expiry_time=?,dissolving_expiry_time=?,new_leader_id=? WHERE clan_id=?'

        let parameters = clans.map( ( clan: L2Clan ) => {
            return [
                clan.getLeaderId(),
                clan.getAllyId(),
                clan.getAllyName(),
                clan.getAllyCrestId(),
                clan.getReputationScore(),
                clan.getAllyPenaltyExpiryTime(),
                clan.getAllyPenaltyType(),
                clan.getCharacterPenaltyExpiryTime(),
                clan.getDissolvingExpiryTime(),
                clan.getNewLeaderId(),
                clan.getId()
            ]
        } )

        return databaseEngine.insertMany( query, parameters )
    },

    async setReputationScore( clanId: number, scoreAmount: number ): Promise<void> {
        let query = 'UPDATE clan_data SET reputation_score=? WHERE clan_id=?'
        return databaseEngine.insertOne( query, scoreAmount, clanId )
    },

    async resetCastleStatus( residenceId: number ): Promise<void> {
        let query = 'UPDATE clan_data SET hasCastle = 0 WHERE hasCastle = ?'
        return databaseEngine.insertOne( query, residenceId )
    },

    async setCastleOwnership( residenceId: number, ownerId: number ): Promise<void> {
        let query = 'UPDATE clan_data SET hasCastle = ? WHERE clan_id = ?'
        return databaseEngine.insertOne( query, residenceId, ownerId )
    }
}