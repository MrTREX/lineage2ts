import { CharacterShortcutsTableApi, L2PlayerShortcutsTableItem } from '../../interface/CharacterShortcutsTableApi'
import { ShortcutOwnerType, ShortcutType } from '../../../gameService/enums/ShortcutType'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteCharacterShortcutsTable: CharacterShortcutsTableApi = {
    async deleteMany( items: Array<L2PlayerShortcutsTableItem> ): Promise<void> {
        let query = 'DELETE FROM character_shortcuts WHERE charId=? AND slot=? AND page=? AND class_index=?'
        return databaseEngine.insertMany( query, items.map( ( item: L2PlayerShortcutsTableItem ) => {
            return [
                item.objectId,
                item.slot,
                item.page,
                item.classIndex
            ]
        } ) )
    },

    async upsertMany( items: Array<L2PlayerShortcutsTableItem> ): Promise<void> {
        let query = 'INSERT OR REPLACE INTO character_shortcuts (charId, class_index, slot, page, type, shortcut_id, level) values(?,?,?,?,?,?,?)'

        return databaseEngine.insertMany( query, items.map( ( item: L2PlayerShortcutsTableItem ) => {
            return [
                item.objectId,
                item.classIndex,
                item.slot,
                item.page,
                item.type,
                item.id,
                item.level
            ]
        } ) )
    },

    async deleteShortcutByIds( playerId: number, classIndex: number, objectIds: Array<number>, type: ShortcutType ): Promise<void> {
        const query = 'delete from character_shortcuts where charId=? and shortcut_id=? and class_index=? and type=?'
        let parameters = objectIds.map( ( objectId: number ) => {
            return [ playerId, objectId, classIndex, type ]
        } )

        return databaseEngine.insertMany( query, parameters )
    },

    async deleteByClassIndex( playerId: number, classIndex: number ): Promise<void> {
        const query = 'DELETE FROM character_shortcuts WHERE charId=? AND class_index=?'
        return databaseEngine.insertOne( query, playerId, classIndex )
    },

    async removeCharacter( playerId: number ): Promise<void> {
        const query = 'DELETE FROM character_shortcuts WHERE charId=?'
        return databaseEngine.insertOne( query, playerId )
    },

    async getAll( playerId: number, classIndex: number ): Promise<Array<L2PlayerShortcutsTableItem>> {
        const query = 'SELECT charId, slot, page, type, shortcut_id, level FROM character_shortcuts WHERE charId=? AND class_index=?'
        let databaseItems : Array<unknown> = databaseEngine.getMany( query, playerId, classIndex )

        return databaseItems.map( ( databaseItem: any ) : L2PlayerShortcutsTableItem => {
            return {
                objectId: databaseItem[ 'charId' ],
                classIndex,
                characterType: ShortcutOwnerType.Player,
                id: databaseItem[ 'shortcut_id' ],
                level: databaseItem.level,
                page: databaseItem.page,
                itemReuseGroup: -1,
                slot: databaseItem.slot,
                type: databaseItem.type
            }
        } )
    },
}