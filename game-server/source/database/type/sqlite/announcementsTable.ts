import { AnnouncementsTableApi } from '../../interface/AnnouncementsTableApi'
import { AutoAnnouncement } from '../../../gameService/models/announce/AutoAnnouncement'
import { IAnnouncement } from '../../../gameService/models/announce/IAnnouncement'
import { GeneralAnnouncement } from '../../../gameService/models/announce/GeneralAnnouncement'
import { AnnouncementType } from '../../../gameService/models/announce/AnnouncementType'
import { getMany, insertOne, insertOneWithReturn } from '../../engines/sqlite'

export const SQLiteAnnouncementsTable: AnnouncementsTableApi = {
    async createAuto( item: AutoAnnouncement ): Promise<number> {
        let query = 'INSERT INTO announcements (`type`, `content`, `author`, `initial`, `delay`, `repeat`) VALUES (?, ?, ?, ?, ?, ?)'
        return insertOneWithReturn( query, item.getType(), item.getContent(), item.getAuthor(), item.initial, item.delay, item.repeat ) as number
    },

    async createGeneral( item: GeneralAnnouncement ): Promise<number> {
        let query = 'INSERT INTO announcements (type, content, author) VALUES (?, ?, ?)'
        return insertOneWithReturn( query, item.getType(), item.getContent(), item.getAuthor() ) as number
    },

    async deleteItem( item: IAnnouncement ): Promise<void> {
        let query = 'DELETE FROM announcements WHERE id = ?'
        return insertOne( query, item.getId() )
    },

    async updateAuto( item: AutoAnnouncement ): Promise<void> {
        let query = 'UPDATE announcements SET `type` = ?, `content` = ?, `author` = ?, `initial` = ?, `delay` = ?, `repeat` = ? WHERE id = ?'
        return insertOne( query, item.getType(), item.getContent(), item.getAuthor(), item.initial, item.delay, item.repeat, item.getId() )
    },

    async updateGeneral( item: GeneralAnnouncement ): Promise<void> {
        let query = 'UPDATE announcements SET type = ?, content = ?, author = ? WHERE id = ?'
        return insertOne( query, item.getType(), item.getContent(), item.getAuthor(), item.getId() )
    },

    async getAll(): Promise<Array<IAnnouncement>> {
        let query = 'SELECT `id`, `type`, `initial`, `delay`, `repeat`, `author`, `content` FROM announcements'
        let databaseItems : Array<unknown> = getMany( query )

        return databaseItems.map( Helper.getAnnouncement )
    }
}

const Helper = {
    getAnnouncement( databaseItem: any ) {
        let { id, type, initial, delay, repeat, author, content } = databaseItem

        switch ( type ) {
            case AnnouncementType.NORMAL:
            case AnnouncementType.CRITICAL:
                return new GeneralAnnouncement( type, author, content, id )

            case AnnouncementType.AUTO_NORMAL:
            case AnnouncementType.AUTO_CRITICAL:
                return new AutoAnnouncement( type, author, content, initial, delay, repeat, id )
        }
    }
}