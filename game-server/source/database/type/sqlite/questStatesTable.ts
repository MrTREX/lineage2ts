import { L2QuestStatesTableData, QuestStatesTableApi } from '../../interface/QuestStatesTableApi'
import _ from 'lodash'
import { QuestState } from '../../../gameService/models/quest/QuestState'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteQuestStatesTable : QuestStatesTableApi = {
    async deleteMany( states: Set<QuestState> ): Promise<void> {
        const query = 'DELETE FROM quest_states WHERE objectId = ? AND questName = ?'
        let dataSets = []

        states.forEach( ( state: QuestState ) => {
            return [
                state.playerId,
                state.questName
            ]
        } )

        return databaseEngine.insertMany( query, dataSets )
    },

    async updateVariables( states: Set<QuestState> ): Promise<void> {
        const query = 'UPDATE quest_states set variables_json=? where objectId=? and questName=?'
        let dataSets = []

        states.forEach( ( state: QuestState ) => {
            return [
                JSON.stringify( state.variables ),
                state.playerId,
                state.questName
            ]
        } )

        return databaseEngine.insertMany( query, dataSets )
    },

    async upsertMany( states: Set<QuestState> ): Promise<void> {
        const query = 'INSERT OR REPLACE INTO quest_states (objectId, questName, completionState, condition, stateFlags) VALUES (?,?,?,?,?)'
        let dataSets = []

        states.forEach( ( state: QuestState ) => {
            dataSets.push( [
                    state.playerId,
                    state.questName,
                    state.state,
                    state.condition,
                    state.stateFlags
            ] )
        } )

        return databaseEngine.insertMany( query, dataSets )
    },

    async deleteManyNames( objectId: number, names: Array<string> ): Promise<void> {
        const query = 'DELETE FROM quest_states WHERE objectId = ? AND questName in (#values#)'
        const finalQuery = query.replace( '#values#', _.join( _.times( names.length, _.constant( '?' ) ), ',' ) )

        return databaseEngine.deleteManyForValues( finalQuery, [ objectId, ...names ] )
    },

    async createData( objectId: number, questName: string, stateValue: number, conditionValue: number, stateFlags: number ): Promise<void> {
        const query = 'INSERT INTO quest_states (objectId, questName, completionState, condition, stateFlags) VALUES (?,?,?,?,?)'
        return databaseEngine.insertOne( query, objectId, questName, stateValue, conditionValue, stateFlags )
    },

    async getData( objectId: number ): Promise<Array<L2QuestStatesTableData>> {
        const query = 'SELECT * FROM quest_states WHERE objectId=?'
        let databaseItems : Array<unknown> = databaseEngine.getMany( query, objectId )

        return databaseItems.map( ( databaseItem: any ) : L2QuestStatesTableData => {
            let variables = _.attempt( JSON.parse, databaseItem[ 'variables_json' ] )

            return {
                completionState: databaseItem.completionState,
                condition: databaseItem.condition,
                playerId: databaseItem.objectId,
                questName: databaseItem.questName,
                stateFlags: databaseItem.stateFlags,
                variables: _.isError( variables ) ? {} : variables
            }
        } )
    },

    async removeCharacter( objectId: number ): Promise<void> {
        const query = 'DELETE FROM quest_states WHERE objectId=?'
        return databaseEngine.insertOne( query, objectId )
    },

    async deleteData( playerId: number, questName: string ): Promise<void> {
        const query = 'DELETE FROM quest_states WHERE objectId = ? AND questName = ?'
        return databaseEngine.insertOne( query, playerId, questName )
    }
}