import { CharacterSkillsSaveItem, CharacterSkillsSaveTableApi } from '../../interface/CharacterSkillsSaveTableApi'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteCharacterSkillsSaveTable: CharacterSkillsSaveTableApi = {
    async deleteReuseBySkillIds( skillIds: Array<number> ): Promise<void> {
        const query = 'DELETE FROM character_skills_save WHERE skill_id in (#values#)'
        return databaseEngine.deleteManyForValues( query, skillIds )
    },

    async removeCharacter( objectId: number ): Promise<void> {
        const query = 'DELETE FROM character_skills_save WHERE charId=?'
        return databaseEngine.insertOne( query, objectId )
    },

    async update( objectId: number, classIndex: number, data: Array<CharacterSkillsSaveItem> ) : Promise<void> {
        let query = 'INSERT OR REPLACE INTO character_skills_save (charId,skill_id,skill_level,remaining_time,reuse_delay,systime,restore_type,class_index,buff_index) VALUES (?,?,?,?,?,?,?,?,?)'
        let skillsToSave = data.map( ( item: CharacterSkillsSaveItem ) : Array<number> => {
            return [
                item.objectId,
                item.skillId,
                item.skillLevel,
                item.remainingMs,
                item.reuseDelay,
                item.expirationTime,
                item.type,
                item.classIndex,
                item.index
            ]
        } )

        return databaseEngine.insertMany( query, skillsToSave )
    },

    async deleteByClassIndex( objectId: number, classIndex: number ): Promise<void> {
        let query = 'DELETE FROM character_skills_save WHERE charId=? AND class_index=?'
        return databaseEngine.insertOne( query, objectId, classIndex )
    },

    async load( objectId: number, classIndex: number ) : Promise<Array<CharacterSkillsSaveItem>> {
        let query = 'SELECT skill_id, skill_level, remaining_time, reuse_delay, systime, restore_type FROM character_skills_save WHERE charId=? AND class_index=? ORDER BY buff_index'
        let results: Array<any> = databaseEngine.getMany( query, objectId, classIndex )

        return results.map( ( databaseItem : any, index: number ) : CharacterSkillsSaveItem => {
            return {
                classIndex,
                expirationTime: databaseItem[ 'systime' ],
                index,
                objectId,
                remainingMs: databaseItem[ 'remaining_time' ],
                reuseDelay: databaseItem[ 'reuse_delay' ],
                skillId: databaseItem[ 'skill_id' ],
                skillLevel: databaseItem[ 'skill_level' ],
                type: databaseItem[ 'restore_type' ]
            }
        } )
    }
}