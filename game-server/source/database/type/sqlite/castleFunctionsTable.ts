import { L2CastleFunctionsTableApi, L2CastleFunctionsTableData } from '../../interface/CastleFunctionsTableApi'
import { getMany, insertOne } from '../../engines/sqlite'

export const SQLiteCastleFunctionsTable: L2CastleFunctionsTableApi = {
    async getAll( residenceId: number ): Promise<Array<L2CastleFunctionsTableData>> {
        const query = 'SELECT * FROM castle_functions WHERE castle_id = ?'
        let databaseItems : Array<unknown> = getMany( query, residenceId )

        return databaseItems.map( ( databaseItem: any ): L2CastleFunctionsTableData => {
            return {
                endTime: databaseItem[ 'endTime' ],
                fee: databaseItem[ 'lease' ],
                level: databaseItem[ 'lvl' ],
                rate: databaseItem[ 'rate' ],
                residenceId: databaseItem[ 'castle_id' ],
                type: databaseItem[ 'type' ],
            }
        } )
    },

    async remove( residenceId: number, type: number ): Promise<void> {
        const query = 'DELETE FROM castle_functions WHERE castle_id=? AND type=?'
        return insertOne( query, residenceId, type )
    },

    async update( residenceId: number, type: number, level: number, fee: number, rate: number, endTime: number ): Promise<void> {
        const query = 'REPLACE INTO castle_functions (castle_id, type, lvl, lease, rate, endTime) VALUES (?,?,?,?,?,?)'
        return insertOne( query, residenceId, type, level, fee, rate, endTime )
    },
}