import {
    L2ServerVariablesTableApi,
    L2ServerVariablesTableItem,
    L2ServerVariableType
} from '../../interface/ServerVariablesTableApi'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteServerVariablesTable : L2ServerVariablesTableApi = {
    async add( value: L2ServerVariablesTableItem ): Promise<void> {
        const query = 'INSERT OR REPLACE INTO server_variables (name, value, expirationDate) VALUES (?, ?, ?)'
        return databaseEngine.insertOne( query, value.name, value.value, value.expirationDate )
    },

    async addAll( values: Array<L2ServerVariablesTableItem> ): Promise<void> {
        const query = 'INSERT OR REPLACE INTO server_variables (name, value, expirationDate) VALUES (?, ?, ?)'
        let parameters : Array<Array<L2ServerVariableType>> = values.map( ( value: L2ServerVariablesTableItem ) => {
            return [ value.name, value.value, value.expirationDate ]
        } )

        return databaseEngine.insertMany( query, parameters )
    },

    async removeAll(): Promise<void> {
        const query = 'DELETE FROM server_variables'
        return databaseEngine.insertOne( query )
    },

    async getAll(): Promise<Array<L2ServerVariablesTableItem>> {
        const query = 'SELECT * FROM server_variables'
        let databaseItems : Array<unknown> = databaseEngine.getMany( query )

        return databaseItems.map( ( databaseItem: any ) : L2ServerVariablesTableItem => {
            return {
                expirationDate: databaseItem[ 'expirationDate' ],
                name: databaseItem[ 'name' ],
                value: databaseItem[ 'value' ]
            }
        } )
    }
}