import { L2PetTableData, PetsTableApi } from '../../interface/PetsTableApi'
import { L2PcInstance } from '../../../gameService/models/actor/instance/L2PcInstance'
import { L2PetInstance } from '../../../gameService/models/actor/instance/L2PetInstance'
import _ from 'lodash'
import * as databaseEngine from '../../engines/sqlite'

const getUpsertParameters = ( pet: L2PetInstance ) => {
    return [
        pet.getName(),
        pet.getLevel(),
        pet.getStatus().getCurrentHp(),
        pet.getStatus().getCurrentMp(),
        pet.getExp(),
        pet.getSp(),
        pet.getCurrentFeed(),
        pet.getOwnerId(),
        pet.isVisible() ? 'true' : 'false',
        pet.getControlObjectId(),
    ]
}

export const SQLitePetsTable: PetsTableApi = {
    async getPet( playerId: number, controlId: number ): Promise<L2PetTableData> {
        const query = 'select name,level,hp,mp,exp,sp,feed,isRestored from pets where ownerId = ? and controlId = ?'
        let databaseItem: any = databaseEngine.getOne( query, playerId, controlId )
        if ( !databaseItem ) {
            return
        }

        return {
            controlId,
            exp: databaseItem.exp,
            feed: databaseItem.feed,
            hp: databaseItem.hp,
            isRestored: databaseItem.isRestored === 'true',
            level: databaseItem.level,
            mp: databaseItem.mp,
            name: databaseItem.name,
            ownerId: playerId,
            sp: databaseItem.sp,
        }
    },

    async getRestoredPet( playerId: number ): Promise<L2PetTableData> {
        const query = 'select name,level,hp,mp,exp,sp,feed,controlId from pets where ownerId = ? and isRestored = ?'
        let databaseItem: any = databaseEngine.getOne( query, playerId, 'true' )
        if ( !databaseItem ) {
            return
        }

        return {
            controlId: databaseItem.controlId,
            exp: databaseItem.exp,
            feed: databaseItem.feed,
            hp: databaseItem.hp,
            isRestored: true,
            level: databaseItem.level,
            mp: databaseItem.mp,
            name: databaseItem.name,
            ownerId: playerId,
            sp: databaseItem.sp,
        }
    },

    async setRestored( pet: L2PetInstance, isRestored: boolean ): Promise<void> {
        const query = 'UPDATE pets SET isRestored=? WHERE controlId=?'
        return databaseEngine.insertOne( query, isRestored.toString(), pet.getControlObjectId() )
    },

    async getHighestId(): Promise<number> {
        const query = 'SELECT MAX(controlId) as maxId FROM pets'
        let databaseItem: any = databaseEngine.getOne( query )

        if ( !databaseItem ) {
            return 0
        }

        return _.defaultTo( databaseItem[ 'maxId' ], 0 )
    },

    async getRestoredPlayerIds(): Promise<Array<number>> {
        const query = 'select ownerId from pets where isRestored = \'true\''
        let databaseItems: Array<unknown> = databaseEngine.getMany( query )

        return databaseItems.map( ( databaseItem: any ): number => {
            return databaseItem[ 'ownerId' ]
        } )
    },

    async getUsedIds( ids: Array<number> ): Promise<Array<number>> {
        const query = 'SELECT controlId FROM pets WHERE controlId in (#values#)'
        let databaseItems: Array<unknown> = databaseEngine.getManyForValues( query, ids )

        return databaseItems.map( item => item[ 'controlId' ] )
    },

    async removeCharacter( objectId: number ): Promise<void> {
        const query = 'DELETE FROM pets WHERE controlId IN (SELECT object_id FROM items WHERE items.owner_id=?)'
        return databaseEngine.insertOne( query, objectId )
    },

    async hasPetName( name: string ): Promise<boolean> {
        let query = 'SELECT EXISTS(SELECT 1 FROM pets WHERE name=?)'
        let result = databaseEngine.getOne( query, name )

        return _.values( result )[ 0 ] === 1
    },

    async updateFood( player: L2PcInstance, petId: number ): Promise<void> {
        if ( player.getControlItemId() === 0 || petId === 0 ) {
            return
        }

        let query = 'UPDATE pets SET feed=? WHERE controlId=?'
        return databaseEngine.insertOne( query, player.getCurrentFeed(), player.getControlItemId() )
    },

    async deleteByControlId( objectId: number ): Promise<void> {
        let query = 'DELETE FROM pets WHERE controlId=?'
        return databaseEngine.insertOne( query, objectId )
    },

    async deleteManyByControlId( objectIds: Array<number> ): Promise<void> {
        const query = 'DELETE FROM pets WHERE controlId in (#values#)'
        return databaseEngine.deleteManyForValues( query, objectIds )
    },

    async createPet( pet: L2PetInstance ): Promise<void> {
        let query = 'INSERT INTO pets (name,level,hp,mp,exp,sp,feed,ownerId,isRestored,controlId) VALUES (?,?,?,?,?,?,?,?,?,?)'
        let parameters: Array<any> = getUpsertParameters( pet )
        return databaseEngine.insertOne( query, ...parameters )
    },

    async updatePet( pet: L2PetInstance ): Promise<void> {
        let query = 'UPDATE pets SET name=?,level=?,hp=?,mp=?,exp=?,sp=?,feed=?,ownerId=?,isRestored=? WHERE controlId=?'
        let parameters: Array<any> = getUpsertParameters( pet )
        return databaseEngine.insertOne( query, ...parameters )
    },
}