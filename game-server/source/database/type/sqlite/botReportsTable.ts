import { L2BotReportsTableApi, L2BotReportsTableData } from '../../interface/BotReportsTableApi'
import { getMany } from '../../engines/sqlite'
import _ from 'lodash'

export const SQLiteBotReportsTable : L2BotReportsTableApi = {
    async getAll(): Promise<Array<L2BotReportsTableData>> {
        const query = 'SELECT * FROM bot_reported_char_data'
        let databaseItems : Array<unknown> = getMany( query )

        return databaseItems.map( ( databaseItem: any ) : L2BotReportsTableData => {
            return {
                botObjectId: databaseItem[ 'botId' ],
                reportTime: databaseItem[ 'reportDate' ],
                reporterObjectId: databaseItem[ 'reporterId' ]
            }
        } )
    }
}