import { L2CharacterRaidPointsTableApi } from '../../interface/CharacterRaidPointsTableApi'
import _ from 'lodash'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteCharacterRaidPointsTable: L2CharacterRaidPointsTableApi = {
    async removeCharacter( objectId: number ): Promise<void> {
        const query = 'DELETE FROM character_raid_points WHERE charId=?'
        return databaseEngine.insertOne( query, objectId )
    },

    async getAll(): Promise<{ [ playerId: number ]: Map<number, number> }> {
        const query = 'SELECT `charId`,`boss_id`,`points` FROM `character_raid_points`'
        let databaseItems = databaseEngine.getMany( query )

        return _.reduce( databaseItems, ( totalMap: { [ playerId: number ]: Map<number, number> }, databaseItem: any ) => {
            let playerId = databaseItem[ 'charId' ]
            let raidId = databaseItem[ 'boss_id' ]
            let points = databaseItem[ 'points' ]

            if ( !totalMap[ playerId ] ) {
                totalMap[ playerId ] = new Map<number, number>()
            }

            totalMap[ playerId ].set( raidId, points )

            return totalMap
        }, {} )
    },

    async removeAll(): Promise<void> {
        const query = 'DELETE from character_raid_points WHERE charId > 0'
        return databaseEngine.insertOne( query )
    },

    async updatePoints( objectId: number, raidId: number, points: number ): Promise<void> {
        const query = 'INSERT OR REPLACE INTO character_raid_points (`charId`,`boss_id`,`points`) VALUES (?,?,?)'
        return databaseEngine.insertOne( query, objectId, raidId, points )
    },
}