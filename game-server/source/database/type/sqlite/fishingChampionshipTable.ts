import {
    L2FishingChampionshipTableApi,
    L2FishingChampionshipTableItem,
} from '../../interface/FishingChampionshipTableApi'
import _ from 'lodash'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteFishingChampionshipTable: L2FishingChampionshipTableApi = {
    async add( item: L2FishingChampionshipTableItem ): Promise<void> {
        const query = 'INSERT INTO fishing_championship(playerId,fishLength,rewardType,hasClaimedReward) VALUES (?,?,?,?)'
        return databaseEngine.insertOne( query, item.playerId, item.fishLength, item.rewardType, item.hasClaimedReward.toString() )
    },

    async update( item: L2FishingChampionshipTableItem ): Promise<void> {
        const query = 'UPDATE fishing_championship SET fishLength=?, rewardType=?, hasClaimedReward=? where playerId=?'
        return databaseEngine.insertOne( query, item.fishLength, item.rewardType, item.hasClaimedReward.toString(), item.playerId )
    },

    async addAll( values: Array<L2FishingChampionshipTableItem> ): Promise<void> {
        const query = 'INSERT INTO fishing_championship(playerId,fishLength,rewardType,hasClaimedReward) VALUES (?,?,?,?)'

        let parameters : Array<Array<any>> = _.map( values, ( value: L2FishingChampionshipTableItem ) => {
            return [
                value.playerId,
                value.fishLength,
                value.rewardType,
                value.hasClaimedReward.toString()
            ]
        } )

        return databaseEngine.insertMany( query, parameters )
    },

    async getAll(): Promise<Array<L2FishingChampionshipTableItem>> {
        const query = 'select * from fishing_championship'
        let databaseItems : Array<unknown> = databaseEngine.getMany( query )

        return databaseItems.map( ( databaseItem: any ): L2FishingChampionshipTableItem => {
            return {
                hasClaimedReward: databaseItem[ 'hasClaimedReward' ] === 'true',
                fishLength: databaseItem[ 'fishLength' ],
                playerId: databaseItem[ 'playerId' ],
                rewardType: databaseItem[ 'rewardType' ]
            }
        } )
    },

    async removeAll(): Promise<void> {
        const query = 'DELETE FROM fishing_championship'
        return databaseEngine.insertOne( query )
    }
}