import {
    CharacterInstanceTimeTableApi,
    L2CharacterInstanceTimeTableItem,
} from '../../interface/CharacterInstanceTimeTableApi'
import _ from 'lodash'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteCharacterInstanceTimeTable : CharacterInstanceTimeTableApi = {
    async removeCharacter( objectId: number ): Promise<void> {
        const query = 'DELETE FROM character_instance_time WHERE charId=?'
        return databaseEngine.insertOne( query, objectId )
    },

    async addInstanceTime( playerId: number, instanceId: number, time: number ): Promise<void> {
        let query = 'INSERT OR REPLACE INTO character_instance_time (charId,instanceId,time) values (?,?,?)'
        return databaseEngine.insertOne( query, playerId, instanceId, time )
    },

    async deleteInstanceTime( playerId: number, ...instanceIds: Array<number> ) : Promise<void> {
        let values = _.join( _.times( instanceIds.length, _.constant( '?' ) ), ',' )
        let query = `DELETE FROM character_instance_time WHERE charId=? AND instanceId in (${values })`
        return databaseEngine.insertOne( query, playerId, ...instanceIds )
    },

    async getTimes( playerId: number ): Promise<Array<L2CharacterInstanceTimeTableItem>> {
        let query = 'SELECT instanceId, time, charId as playerId FROM character_instance_time WHERE charId=?'
        return databaseEngine.getMany( query, playerId )
    }
}