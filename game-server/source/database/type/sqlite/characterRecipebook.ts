import { L2CharacterRecipebookTableApi, L2CharacterRecipeUpdate } from '../../interface/CharacterRecipebookTableApi'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteCharacterRecipebook : L2CharacterRecipebookTableApi = {
    async addRecipes( items: Array<L2CharacterRecipeUpdate> ): Promise<void> {
        const query = 'INSERT OR REPLACE INTO character_recipebook (charId, id, classIndex, type) values(?,?,?,?)'
        const parameters = items.map( item => {
            return [
                item.objectId,
                item.recipeId,
                item.isDwarven ? item.classIndex : 0,
                item.isDwarven ? 1 : 0
            ]
        } )

        return databaseEngine.insertMany( query, parameters )
    },

    async removeRecipes( items: Array<L2CharacterRecipeUpdate> ): Promise<void> {
        const query = 'DELETE FROM character_recipebook WHERE charId=? AND id=? AND classIndex=?'
        const parameters = items.map( item => {
            return [
                item.objectId,
                item.recipeId,
                item.classIndex
            ]
        } )

        return databaseEngine.insertMany( query, parameters )
    },

    async removeCharacter( objectId: number ): Promise<void> {
        const query = 'DELETE FROM character_recipebook WHERE charId=?'
        return databaseEngine.insertOne( query, objectId )
    },

    async getCommonRecipeIds( objectId: number ): Promise<Array<number>> {
        const query = 'SELECT id FROM character_recipebook WHERE charId=? AND type = 0'
        let databaseItems : Array<any> = databaseEngine.getMany( query, objectId )

        return databaseItems.map( item => item.id )
    },

    async getDwarvenRecipeIds( objectId: number, classIndex: number ): Promise<Array<number>> {
        const query = 'SELECT id FROM character_recipebook WHERE charId=? AND classIndex=? AND type = 1'
        let databaseItems : Array<any> = databaseEngine.getMany( query, objectId, classIndex )

        return databaseItems.map( item => item.id )
    }
}