import { L2SiegableHallTableApi, L2SiegableHallTableData } from '../../interface/SiegableHallTableApi'
import { SiegableHall } from '../../../gameService/models/entity/clanhall/SiegableHall'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteSiegableHallTable: L2SiegableHallTableApi = {
    async getAll(): Promise<Array<L2SiegableHallTableData>> {
        const query = 'SELECT * FROM siegable_clanhall'
        let databaseItems : Array<unknown> = databaseEngine.getMany( query )

        return databaseItems.map( ( item: any ): L2SiegableHallTableData => {
            return {
                description: item.description,
                duration: item.duration,
                hallId: item.hallId,
                location: item.location,
                name: item.name,
                nextSiege: item.nextSiege,
                ownerId: item.ownerId,
                siegeStartCron: item.siegeStartCron,
            }
        } )
    },

    async updateHalls( halls: Array<SiegableHall> ): Promise<void> {
        const query = 'UPDATE siegable_clanhall SET ownerId=?, nextSiege=? WHERE hallId=?'
        let parameters = halls.map( ( hall: SiegableHall ): Array<number> => {
            return [
                hall.getOwnerId(),
                hall.nextSiege,
                hall.getId(),
            ]
        } )

        return databaseEngine.insertMany( query, parameters )
    },
}