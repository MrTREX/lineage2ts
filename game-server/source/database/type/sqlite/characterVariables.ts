import { L2CharacterVariablesTableApi } from '../../interface/CharacterVariablesTableApi'
import _ from 'lodash'
import { AbstractVariablesMap } from '../../../gameService/variables/AbstractVariablesManager'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteCharacterVariables : L2CharacterVariablesTableApi = {
    async setManyVariables( variableMap: Record<string, AbstractVariablesMap>, propertyNames: Set<string | number> ): Promise<void> {
        const query = 'INSERT OR REPLACE INTO player_variables (playerId, value_json) VALUES (?, ?)'
        const allValues = []

        propertyNames.forEach( id => {
            allValues.push( [
                id,
                JSON.stringify( variableMap[ id ] )
            ] )
        } )

        return databaseEngine.insertMany( query, allValues )
    },

    async removeCharacter( objectId: number ): Promise<void> {
        const query = 'DELETE FROM player_variables WHERE playerId = ?'
        return databaseEngine.insertOne( query, objectId )
    },

    async getVariables( objectId: number ): Promise<AbstractVariablesMap> {
        const query = 'SELECT value_json FROM player_variables WHERE playerId = ?'
        let databaseItem = databaseEngine.getOne( query, objectId )

        if ( !_.has( databaseItem, 'value_json' ) ) {
            return
        }

        let value = _.attempt( JSON.parse, databaseItem[ 'value_json' ] )

        if ( _.isError( value ) ) {
            return
        }

        return value
    }
}