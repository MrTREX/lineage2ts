import { L2GamePointItem, L2GamePointsTableApi } from '../../interface/GamePointsTableApi'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteGamePoints : L2GamePointsTableApi = {
    async getPoints( name: string ): Promise<Array<L2GamePointItem>> {
        const query = 'select * from game_points where id=?'
        let databaseItems : Array<unknown> = databaseEngine.getMany( query, name )

        return databaseItems.map( ( item: any ) : L2GamePointItem => {
            return {
                amount: item.amount,
                name,
                type: item.type,
                lastUpdate: item.lastUpdate,
                reason: item.reason
            }
        } )
    },

    async updatePoints( items: Array<L2GamePointItem> ): Promise<void> {
        const query = 'INSERT OR REPLACE INTO game_points (id, type, amount, lastUpdate, reason ) VALUES (?, ?, ?, ?, ?)'

        const data = items.map( item => {
            return [
                item.name,
                item.type,
                item.amount,
                item.lastUpdate,
                item.reason
            ]
        } )

        return databaseEngine.insertMany( query, data )
    }
}