import { L2ClanHallAuctionItem, L2ClanHallAuctionsTableApi } from '../../interface/ClanHallAuctionsTableApi'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteClanHallAuctionsTable : L2ClanHallAuctionsTableApi = {
    async addAuction( data: L2ClanHallAuctionItem ): Promise<void> {
        const query = 'INSERT OR REPLACE INTO clanhall_auction (id, sellerId, sellerName, sellerClanName, itemType, itemId, itemObjectId, itemName, itemQuantity, startingBid, currentBid, endDate) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)'
        return databaseEngine.insertOne( query,
                data.id,
                data.sellerId,
                data.sellerName,
                data.sellerClanName,
                data.itemType,
                data.itemId,
                data.itemObjectId,
                data.itemName,
                data.itemQuantity,
                data.startingBid,
                data.currentBid,
                data.endDate )
    },

    async updateEndDate( id: number, endDate: number ): Promise<void> {
        const query = 'UPDATE clanhall_auction SET endDate = ? WHERE id = ?'
        return databaseEngine.insertOne( query, endDate, id )
    },

    async removeAuction( id: number ): Promise<void> {
        const query = 'DELETE FROM clanhall_auction WHERE itemId=?'
        return databaseEngine.insertOne( query, id )
    }
}