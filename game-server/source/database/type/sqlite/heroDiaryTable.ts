import { L2HeroDiaryTableApi, L2HeroDiaryTableItem } from '../../interface/HeroDiaryTableApi'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteHeroDiaryTable: L2HeroDiaryTableApi = {
    async getItemsByIds( objectIds: Array<number> ): Promise<Array<L2HeroDiaryTableItem>> {
        let query = 'SELECT * FROM heroes_diary WHERE charId in (#values#)'
        let databaseItems : Array<unknown> = databaseEngine.getManyForValues( query, objectIds )

        return databaseItems.map( ( databaseItem: any ) : L2HeroDiaryTableItem => {
            return {
                action: databaseItem[ 'action' ],
                objectId: databaseItem[ 'charId' ],
                time: databaseItem[ 'time' ],
                value: databaseItem[ 'param' ]
            }
        } )
    },

    async getItems( objectId: number ): Promise<Array<L2HeroDiaryTableItem>> {
        const query = 'SELECT * FROM  heroes_diary WHERE charId=? ORDER BY time'
        let databaseItems : Array<unknown> = databaseEngine.getMany( query, objectId )

        return databaseItems.map( ( databaseItem: any ) : L2HeroDiaryTableItem => {
            return {
                action: databaseItem[ 'action' ],
                objectId: databaseItem[ 'charId' ],
                time: databaseItem[ 'time' ],
                value: databaseItem[ 'param' ]
            }
        } )
    },

    async addEntry( objectId: number, timestamp: number, action: number, value: number ) : Promise<void> {
        const query = 'INSERT INTO heroes_diary (charId, time, action, param) values(?,?,?,?)'
        return databaseEngine.insertOne( query, objectId, timestamp, action, value )
    }
}