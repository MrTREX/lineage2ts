import { L2AccountVariablesTableApi } from '../../interface/AccountVariablesTableApi'
import { AbstractVariablesMap } from '../../../gameService/variables/AbstractVariablesManager'
import _ from 'lodash'
import { getOne, insertMany, insertOne } from '../../engines/sqlite'

export const SQLiteAccountVariables: L2AccountVariablesTableApi = {
    async setManyVariables( variableMap: Record<string, AbstractVariablesMap>, propertyNames: Set<string | number> ): Promise<void> {
        const query = 'INSERT OR REPLACE INTO account_variables (id, value_json) VALUES (?, ?)'
        const allValues = []

        propertyNames.forEach( id => {
            allValues.push( [
                id,
                JSON.stringify( variableMap[ id ] )
            ] )
        } )

        return insertMany( query, allValues )
    },

    async deleteVariables( name: string ): Promise<void> {
        const query = 'DELETE FROM account_variables WHERE id = ?'
        return insertOne( query, name )
    },

    async getVariables( name: string ): Promise<AbstractVariablesMap> {
        const query = 'SELECT value_json FROM account_variables WHERE id = ?'
        let databaseItem = getOne( query, name )

        if ( !_.has( databaseItem, 'value_json' ) ) {
            return
        }

        let value = _.attempt( JSON.parse, databaseItem[ 'value_json' ] )

        if ( _.isError( value ) ) {
            return
        }

        return value
    }
}