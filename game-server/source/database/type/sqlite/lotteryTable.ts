import { L2LotteryTableApi, L2LotteryTableData } from '../../interface/LotteryTableApi'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteLotteryTable: L2LotteryTableApi = {
    async getItems( id: number ): Promise<Array<L2LotteryTableData>> {
        const query = 'select * from lottery where id = ?'
        let dabaseItems : Array<unknown> = databaseEngine.getMany( query, id )

        return dabaseItems.map( ( databaseItem: any ) : L2LotteryTableData => {
            return {
                id: databaseItem[ 'id' ],
                endDate: databaseItem[ 'endDate' ],
                isFinished: databaseItem[ 'finished' ] === 1,
                nextPrize: databaseItem[ 'nextPrize' ],
                prize: databaseItem[ 'prize' ],
                enchant: databaseItem[ 'enchant' ],
                prizeOne: databaseItem[ 'prize1' ],
                prizeThree: databaseItem[ 'prize3' ],
                prizeTwo: databaseItem[ 'prize2' ],
                type: databaseItem[ 'type' ],
            }
        } )
    },

    async updatePrice( id: number, currentPrice: number, nextPrice: number ) : Promise<void> {
        const query = 'UPDATE lottery SET prize=?, nextPrize=? WHERE id = ?'
        return databaseEngine.insertOne( query, currentPrice, nextPrice, id )
    },

    async updateItem( data: L2LotteryTableData ): Promise<void> {
        const query = 'UPDATE lottery SET finished=?, prize=?, nextPrize=?, enchant=?, type=?, prize1=?, prize2=?, prize3=?, endDate=? WHERE id=?'
        return databaseEngine.insertOne( query,
                data.isFinished ? 1 : 0,
                data.prize,
                data.nextPrize,
                data.enchant,
                data.type,
                data.prizeOne,
                data.prizeTwo,
                data.prizeThree,
                data.endDate,
                data.id,
        )
    },

    async addItem( id: number, endDate: number, currentPrize: number, futurePrize: number ): Promise<void> {
        const query = 'INSERT INTO lottery (id, endDate, prize, nextPrize) VALUES (?, ?, ?, ?)'
        return databaseEngine.insertOne( query, id, endDate, currentPrize, futurePrize )
    },

    async getLastItem(): Promise<L2LotteryTableData> {
        const query = 'SELECT * FROM lottery ORDER BY id DESC LIMIT 1'
        let databaseItem = databaseEngine.getOne( query )

        if ( !databaseItem ) {
            return null
        }

        return {
            id: databaseItem[ 'id' ],
            endDate: databaseItem[ 'endDate' ],
            isFinished: databaseItem[ 'finished' ] === 1,
            nextPrize: databaseItem[ 'nextPrize' ],
            prize: databaseItem[ 'prize' ],
            enchant: databaseItem[ 'enchant' ],
            prizeOne: databaseItem[ 'prize1' ],
            prizeThree: databaseItem[ 'prize3' ],
            prizeTwo: databaseItem[ 'prize2' ],
            type: databaseItem[ 'type' ],
        }
    }
}