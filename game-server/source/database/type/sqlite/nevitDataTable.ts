import { L2NevitDataItem, L2NevitDataTableApi } from '../../interface/NevitDataTableApi'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteNevitDataTable : L2NevitDataTableApi = {
    async getData( objectId: number ): Promise<L2NevitDataItem> {
        const query = 'select huntingTime, effectEndTime, huntingPoints from nevit_data where objectId = ?'
        let data = databaseEngine.getOne( query, objectId )

        if ( !data ) {
            return null
        }

        return {
            effectEndTime: data[ 'effectEndTime' ],
            huntingPoints: data[ 'huntingPoints' ],
            huntingTime: data[ 'huntingTime' ],
            objectId
        }
    },

    async setData( data: L2NevitDataItem ): Promise<void> {
        const query = 'INSERT OR REPLACE INTO nevit_data (objectId, huntingTime, effectEndTime, huntingPoints) values (?, ?, ?, ?)'
        return databaseEngine.insertOne( query, data.objectId, data.huntingTime, data.effectEndTime, data.huntingPoints )
    }
}