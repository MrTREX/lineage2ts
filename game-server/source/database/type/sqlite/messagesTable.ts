import { L2MessagesTableItem, MessagesTableApi } from '../../interface/MessagesTableApi'
import * as databaseEngine from '../../engines/sqlite'

function mapDatabaseItem( databaseItem: Object ) : L2MessagesTableItem {
    return {
        creationTime: databaseItem[ 'creationTime' ],
        isReturned: databaseItem[ 'returnStatus' ] === 1,
        content: databaseItem[ 'content' ],
        expirationTime: databaseItem[ 'expirationTime' ],
        hasAttachments: databaseItem[ 'attachmentStatus' ] === 1,
        messageId: databaseItem[ 'messageId' ],
        receiverId: databaseItem[ 'receiverId' ],
        codAdena: databaseItem[ 'codAdena' ],
        senderId: databaseItem[ 'senderId' ],
        sentBySystem: databaseItem[ 'sendBySystem' ],
        status: databaseItem[ 'messageStatus' ],
        subject: databaseItem[ 'subject' ]
    }
}

export const SQLiteMessagesTable : MessagesTableApi = {
    async deleteMessages( ids: Array<number> ): Promise<void> {
        const query = 'DELETE FROM mail_messages WHERE messageId in (#values#)'
        return databaseEngine.deleteManyForValues( query, ids )
    },

    async deleteReadMessagesOlderThan( pastEpoch: number ): Promise<void> {
        const query = 'DELETE FROM mail_messages WHERE expirationTime < ? and attachmentStatus = 0 and messageStatus = 0'
        return databaseEngine.insertOne( query, pastEpoch )
    },

    async getCodMessagesOlderThan( pastEpoch: number ): Promise<Array<L2MessagesTableItem>> {
        const query = 'select * from mail_messages where expirationTime < ? and codAdena > 0 and attachmentStatus = 1'
        let databaseItems : Array<unknown> = databaseEngine.getMany( query, pastEpoch )
        return databaseItems.map( mapDatabaseItem )
    },

    async getMessages( objectId: number ): Promise<Array<L2MessagesTableItem>> {
        const query = 'select * from mail_messages where senderId = ? or receiverId = ?'
        let databaseItems : Array<unknown> = databaseEngine.getMany( query, objectId, objectId )
        return databaseItems.map( mapDatabaseItem )
    },

    async getUnreadMessagesOlderThan( pastEpoch: number ): Promise<Array<L2MessagesTableItem>> {
        const query = 'select * from mail_messages where expirationTime < ? and messageStatus = 1 and codAdena = 0'
        let databaseItems : Array<unknown> = databaseEngine.getMany( query, pastEpoch )
        return databaseItems.map( mapDatabaseItem )
    },

    async updateMessages( items: Array<L2MessagesTableItem> ): Promise<void> {
        const query = 'INSERT OR REPLACE INTO mail_messages (messageId, senderId, receiverId, subject, content, expirationTime, creationTime, codAdena, attachmentStatus, messageStatus, sendBySystem, returnStatus) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'
        const parameters = items.map( item => {
            return [
                item.messageId,
                item.senderId,
                item.receiverId,
                item.subject,
                item.content,
                item.expirationTime,
                item.creationTime,
                item.codAdena,
                item.hasAttachments ? 1 : 0,
                item.status,
                item.sentBySystem,
                item.isReturned ? 1 : 0
            ]
        } )

        return databaseEngine.insertMany( query, parameters )
    },

    // TODO : determine a better way to remove messages for character, since attachments must be returned to sender
    async removeCharacter( objectId: number ): Promise<void> {
        const query = 'DELETE FROM mail_messages WHERE receiverId = ?'
        return databaseEngine.insertOne( query, objectId )
    },
}