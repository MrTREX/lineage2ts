import { L2ClanPledgesTableApi, L2ClanPledgeTableItem } from '../../interface/ClanSubpledgesTableApi'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteClanSubpledgesTable : L2ClanPledgesTableApi = {
    async addPledge( id: number, pledgeType: any, pledgeName: string, leaderId: number ): Promise<void> {
        const query = 'INSERT INTO clan_subpledges (clan_id,sub_pledge_id,name,leader_id) values (?,?,?,?)'
        return databaseEngine.insertOne( query, id, pledgeType, pledgeName, leaderId )
    },

    async deleteClan( clanId: number ): Promise<void> {
        const query = 'DELETE FROM clan_subpledges WHERE clan_id=?'
        return databaseEngine.insertOne( query, clanId )
    },

    async getPledges( clanId: number ): Promise<Array<L2ClanPledgeTableItem>> {
        let query = 'SELECT sub_pledge_id,name,leader_id FROM clan_subpledges WHERE clan_id=?'
        let databaseItems : Array<unknown> = databaseEngine.getMany( query, clanId )
        return databaseItems.map( ( databaseItem: Object ) : L2ClanPledgeTableItem => {
            return {
                clanId,
                leaderId: databaseItem[ 'leader_id' ],
                name: databaseItem[ 'name' ],
                type: databaseItem[ 'sub_pledge_id' ]
            }
        } )
    },

    async updatePledge( leaderId: number, name: string, clanId: number, pledgeId: number ): Promise<void> {
        let query = 'UPDATE clan_subpledges SET leader_id=?, name=? WHERE clan_id=? AND sub_pledge_id=?'
        return databaseEngine.insertOne( query, leaderId, name, clanId, pledgeId )
    }
}