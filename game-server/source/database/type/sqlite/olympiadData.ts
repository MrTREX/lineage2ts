import { L2OlympiadDataTableApi } from '../../interface/OlympiadDataTableApi'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteOlympiadDataTable : L2OlympiadDataTableApi = {
    async addStatus( currentCycle: number, period: number, olympiadEnd: number, validationEnd: number, nextWeeklyChange: number ): Promise<void> {
        const query = 'INSERT OR REPLACE INTO olympiad_data (id, current_cycle, period, olympiad_end, validation_end, next_weekly_change) VALUES (0,?,?,?,?,?)'
        return databaseEngine.insertOne( query, currentCycle, period, olympiadEnd, validationEnd, nextWeeklyChange )
    }
}