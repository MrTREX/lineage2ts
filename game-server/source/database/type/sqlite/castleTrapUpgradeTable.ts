import { L2CastleTrapUpgradeTableApi, L2CastleTrapUpgradeTableData } from '../../interface/CastleTrapUpgradeTableApi'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteCastleTrapUpgradeTable : L2CastleTrapUpgradeTableApi = {
    async remove( residenceId: number ): Promise<void> {
        const query = 'DELETE FROM castle_trapupgrade WHERE castleId=?'
        return databaseEngine.insertOne( query, residenceId )
    },

    async update( residenceId: number, index: number, level: number ): Promise<void> {
        const query = 'REPLACE INTO castle_trapupgrade (castleId, towerIndex, level) values (?,?,?)'
        return databaseEngine.insertOne( query, residenceId, index, level )
    },

    async getAll( residenceId: number ): Promise<Array<L2CastleTrapUpgradeTableData>> {
        const query = 'SELECT * FROM castle_trapupgrade WHERE castleId=?'
        let databaseItems : Array<unknown> = databaseEngine.getMany( query, residenceId )
        return databaseItems.map( ( databaseItem: any ) : L2CastleTrapUpgradeTableData => {
            return {
                level: databaseItem[ 'level' ],
                towerIndex: databaseItem[ 'towerIndex' ]
            }
        } )
    }
}