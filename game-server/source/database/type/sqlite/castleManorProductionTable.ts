import {
    CastleManorProductionTableApi,
    L2CastleManorProductionTableData,
} from '../../interface/CastleManorProductionTableApi'
import { SeedProduction } from '../../../gameService/models/manor/SeedProduction'
import * as databaseEngine from '../../engines/sqlite'
import _ from 'lodash'

export const SQLiteCastleManorProductionTable: CastleManorProductionTableApi = {
    async addAll( items: Array<L2CastleManorProductionTableData> ): Promise<void> {
        const query = 'INSERT INTO castle_manor_production VALUES (?, ?, ?, ?, ?, ?)'
        const values = _.map( items, ( item: L2CastleManorProductionTableData ) => {
            return [
                item.castleId,
                item.seedId,
                item.amount,
                item.startAmount,
                item.price,
                item.nextPeriod,
            ]
        } )

        return databaseEngine.insertMany( query, values )
    },

    async deleteAll(): Promise<void> {
        const query = 'DELETE FROM castle_manor_production'
        return databaseEngine.insertOne( query )
    },

    async getAll(): Promise<Array<L2CastleManorProductionTableData>> {
        const query = 'SELECT * FROM castle_manor_production'
        let databaseItems : Array<unknown> = databaseEngine.getMany( query )

        return databaseItems.map( ( databaseItem: any ): L2CastleManorProductionTableData => {
            return {
                amount: databaseItem[ 'amount' ],
                castleId: databaseItem[ 'castle_id' ],
                seedId: databaseItem[ 'seed_id' ],
                nextPeriod: databaseItem[ 'next_period' ],
                price: databaseItem[ 'price' ],
                startAmount: databaseItem[ 'start_amount' ],
            }
        } )
    },

    async removeCastleForNextPeriod( castleId: number ): Promise<void> {
        const query = 'DELETE FROM castle_manor_production WHERE castle_id = ? AND next_period = 1'
        return databaseEngine.insertOne( query, castleId )
    },

    async addCastleProducts( castleId: number, items: Array<SeedProduction> ): Promise<void> {
        const query = 'INSERT INTO castle_manor_production VALUES (?, ?, ?, ?, ?, ?)'
        let data = _.map( items, ( item: SeedProduction ) => {
            return [
                castleId,
                item.getId(),
                item.getAmount(),
                item.getStartAmount(),
                item.getPrice(),
                'true',
            ]
        } )

        return databaseEngine.insertMany( query, data )
    },

    async updateSeedProduction( castleId: number, items: Array<SeedProduction> ): Promise<void> {
        const query = 'UPDATE castle_manor_production SET amount = ? WHERE castle_id = ? AND seed_id = ? AND next_period = 0'

        let data = _.map( items, ( item: SeedProduction ) => {
            return [
                item.getAmount(),
                castleId,
                item.getId(),
            ]
        } )

        return databaseEngine.insertMany( query, data )
    },

    async removeCastle( castleId: number ): Promise<void> {
        let query = 'DELETE FROM castle_manor_production WHERE castle_id = ?'
        return databaseEngine.insertOne( query, castleId )
    },
}