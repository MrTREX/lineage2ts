import { CastleSiegeGuardsTableApi, L2CastleSiegeGuardsTableData } from '../../interface/CastleSiegeGuardsTableApi'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteCastleSiegeGuards : CastleSiegeGuardsTableApi = {
    async getHiredGuards(): Promise<Array<L2CastleSiegeGuardsTableData>> {
        const query = 'SELECT * FROM castle_siege_guards WHERE isHired = 1'
        return databaseEngine.getMany( query )
    },

    async removeHiredGuards( castleId: number ): Promise<void> {
        let query = 'DELETE FROM castle_siege_guards WHERE castleId = ? AND isHired = 1'
        return databaseEngine.insertOne( query, castleId )
    }
}