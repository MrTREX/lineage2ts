import { L2ServitorTableData, ServitorsTableApi } from '../../interface/ServitorsTableApi'
import { L2ServitorInstance } from '../../../gameService/models/actor/instance/L2ServitorInstance'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteCharacterSummonsTable : ServitorsTableApi = {
    async getServitor( playerId: number ): Promise<L2ServitorTableData> {
        const query = 'select summonSkillId,hp,mp,remainingTime from servitors where ownerId = ?'
        let databaseItem = databaseEngine.getOne( query, playerId )
        if ( !databaseItem ) {
            return
        }

        return {
            hp: databaseItem[ 'hp' ],
            mp: databaseItem[ 'mp' ],
            ownerId: playerId,
            remainingTime: databaseItem[ 'remainingTime' ],
            summonSkillId: databaseItem[ 'summonSkillId' ]
        }
    },

    async getRestoredPlayerIds(): Promise<Array<number>> {
        const query = 'select ownerId from servitors'
        let databaseItems : Array<unknown> = databaseEngine.getMany( query )
        return databaseItems.map( ( databaseItem: any ) : number => {
            return databaseItem.ownerId
        } )
    },

    async saveServitor( servitor: L2ServitorInstance ) : Promise<void> {
        const query = 'INSERT OR REPLACE INTO character_summons (ownerId,summonSkillId,hp,mp,remainingTime) VALUES (?,?,?,?,?)'
        return databaseEngine.insertOne(
                query,
                servitor.getOwnerId(),
                servitor.getSummonSkillId(),
                Math.floor( servitor.getCurrentHp() ),
                Math.floor( servitor.getCurrentMp() ),
                servitor.getLifeTimeRemaining()
         )
    },

    async removeServitor( playerId: number ): Promise<void> {
        const query = 'DELETE FROM servitors WHERE ownerId = ?'
        return databaseEngine.insertOne( query, playerId )
    }
}