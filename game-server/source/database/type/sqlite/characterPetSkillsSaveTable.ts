import { PetSkillsSaveTableApi } from '../../interface/PetSkillsSaveTableApi'
import { Skill } from '../../../gameService/models/Skill'
import { EffectScope } from '../../../gameService/models/skills/EffectScope'
import { SkillCache } from '../../../gameService/cache/SkillCache'
import * as databaseEngine from '../../engines/sqlite'
import { BuffProperties } from '../../../gameService/models/skills/BuffDefinition'

const deleteQuery = 'DELETE FROM character_pet_skills_save WHERE petObjItemId=?'

export const SQLiteCharacterPetSkillsSaveTable: PetSkillsSaveTableApi = {
    async delete( controlObjectId: number ): Promise<void> {
        return databaseEngine.insertOne( deleteQuery, controlObjectId )
    },

    async load( controlObjectId: number ): Promise<Array<BuffProperties>> {
        let query = 'SELECT petObjItemId,skill_id,skill_level,remaining_time,buff_index FROM character_pet_skills_save WHERE petObjItemId=? ORDER BY buff_index'
        let outcome : Array<BuffProperties> = []

        let results : Array<unknown> = databaseEngine.getMany( query, controlObjectId )
        results.map( ( databaseItem: any ) => {
            let durationMs = databaseItem[ 'remaining_time' ] as number
            let skillId : number = databaseItem[ 'skill_id' ]
            let skillLevel : number = databaseItem[ 'skill_level' ]
            let skill: Skill = SkillCache.getSkill( skillId, skillLevel )

            if ( !skill ) {
                return
            }

            if ( skill.hasEffects( EffectScope.GENERAL ) ) {
                outcome.push( {
                    skill,
                    durationMs
                } )
            }
        } )

        return outcome
    },

    async update( controlObjectId: number, effects: Array<BuffProperties> ): Promise<void> {
        const query = 'INSERT INTO character_pet_skills_save (petObjItemId,skill_id,skill_level,remaining_time,buff_index) VALUES (?,?,?,?,?)'

        let itemsToSave = effects.map( ( data: BuffProperties, index: number ) => {
            return [
                controlObjectId,
                data.skill.getId(),
                data.skill.getLevel(),
                data.durationMs,
                index
            ]
        } )

        return databaseEngine.insertMany( query, itemsToSave )
    }
}