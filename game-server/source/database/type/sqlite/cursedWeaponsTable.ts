import { CursedWeaponsTableApi } from '../../interface/CursedWeaponsTableApi'
import { CursedWeapon } from '../../../gameService/models/CursedWeapon'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteCursedWeaponsTable : CursedWeaponsTableApi = {
    async saveWeapon( weapon: CursedWeapon ): Promise<void> {
        let query = 'INSERT INTO cursed_weapons (itemId, charId, playerKarma, playerPkKills, nbKills, endTime) VALUES (?, ?, ?, ?, ?, ?)'
        return databaseEngine.insertOne( query,
                weapon.itemId,
                weapon.playerId,
                weapon.playerKarma,
                weapon.playerPkKills,
                weapon.nbKills,
                weapon.endTime )
    },

    async deleteWeapon( weapon: CursedWeapon ): Promise<void> {
        let query = 'DELETE FROM cursed_weapons WHERE itemId = ?'
        return databaseEngine.insertOne( query, weapon.itemId )
    }
}