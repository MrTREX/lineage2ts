import { L2ItemInstance } from '../../../gameService/models/items/instance/L2ItemInstance'
import { ItemLocation } from '../../../gameService/enums/ItemLocation'
import {
    L2ItemsTableApi,
    L2ItemsTableItem,
    L2ItemsTableEquippedItem,
    L2ItemTableAttributes
} from '../../interface/ItemsTableApi'
import * as databaseEngine from '../../engines/sqlite'
import _ from 'lodash'

export const SQLiteItemsTable : L2ItemsTableApi = {
    async getHighestId(): Promise<number> {
        const query = 'SELECT MAX(object_id) as object_id, MAX(owner_id) as owner_id FROM items'
        let databaseItem : Object = databaseEngine.getOne( query )

        if ( !databaseItem ) {
            return 0
        }

        return Math.max(
                _.defaultTo( databaseItem[ 'object_id' ], 0 ),
                _.defaultTo( databaseItem[ 'owner_id' ], 0 )
        )
    },

    async getItemsByItemIdAndType( itemId: number, typeOne: number ): Promise<Array<L2ItemsTableItem>> {
        const query = 'SELECT * FROM items WHERE item_id = ? AND custom_type1 = ?'
        let databaseItems : Array<unknown> = databaseEngine.getMany( query, itemId, typeOne )

        if ( !databaseItems ) {
            return []
        }

        return databaseItems.map( ( databaseItem: Object ) : L2ItemsTableItem => {
            return {
                ownerId: databaseItem[ 'owner_id' ],
                objectId: databaseItem[ 'object_id' ],
                itemId: databaseItem[ 'item_id' ],
                count: databaseItem[ 'count' ],
                enchantLevel: databaseItem[ 'enchant_level' ],
                customTypeOne: databaseItem[ 'custom_type1' ],
                customTypeTwo: databaseItem[ 'custom_type2' ],
                itemLocation: databaseItem[ 'locationType' ],
                locationData: databaseItem[ 'loc_data' ],
                manaRemaining: databaseItem[ 'mana_left' ],
                timeRemaining: databaseItem[ 'time' ],
                agathionEnergy: databaseItem[ 'agathion_energy' ],
                attributes: databaseItem[ 'attributes' ],
            }
        } )
    },

    async getUsedIds( ids: Array<number> ): Promise<Array<number>> {
        const query = 'SELECT object_id FROM items WHERE object_id in (#values#)'
        let databaseItems : Array<unknown> = databaseEngine.getManyForValues( query, ids )

        return databaseItems.map( item => item[ 'object_id' ] as number )
    },

    async removeCharacter( objectId: number ): Promise<void> {
        const query = 'DELETE FROM items WHERE owner_id=?'
        return databaseEngine.insertOne( query, objectId )
    },

    async hasPetItems( playerObjectId: number ): Promise<boolean> {
        const query = `SELECT EXISTS(SELECT 1
                                     FROM \`items\`
                                     WHERE \`owner_id\` = ?
                                       AND (\`locationType\` = ${ ItemLocation.PET } OR \`locationType\` = ${ ItemLocation.PET_EQUIP })
                                     LIMIT 1)`

        let result = databaseEngine.getOne( query, playerObjectId )
        return _.values( result )[ 0 ] === 1
    },

    async insertItems( items: Array<L2ItemInstance> ): Promise<void> {
        let query = 'INSERT OR REPLACE INTO items (owner_id, item_id, count, locationType, loc_data, enchant_level, object_id, custom_type1, custom_type2, mana_left, time, agathion_energy) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)'

        let dataSets = items.map( ( item: L2ItemInstance ) => {
            return [
                item.getOwnerId(),
                item.getId(),
                item.getCount(),
                item.getItemLocation(),
                item.getLocationData(),
                item.getEnchantLevel(),
                item.getObjectId(),
                item.type1,
                item.type2,
                item.getMana(),
                item.getTime(),
                item.getAgathionRemainingEnergy()
            ]
        } )

        return databaseEngine.insertMany( query, dataSets )
    },

    async updateItems( items: Array<L2ItemInstance> ): Promise<void> {
        let query = 'UPDATE items SET owner_id=?, count=?, locationType=?, loc_data=?, enchant_level=?, custom_type1=?, custom_type2=?, mana_left=?, time=?, agathion_energy=? WHERE object_id=?'

        let dataSets = items.map( ( item: L2ItemInstance ) => {
            return [
                item.getOwnerId(),
                item.getCount(),
                item.getItemLocation(),
                item.getLocationData(),
                item.getEnchantLevel(),
                item.getCustomType1(),
                item.getCustomType2(),
                item.getMana(),
                item.getTime(),
                item.getAgathionRemainingEnergy(),
                item.getObjectId()
            ]
        } )

        return databaseEngine.insertMany( query, dataSets )
    },

    async deleteByObjectIds( ids: Array<number> ): Promise<void> {
        let query = 'DELETE FROM items WHERE object_id in (#values#)'
        return databaseEngine.deleteManyForValues( query, ids )
    },

    async getItemsByLocation( objectId: number, baseLocation: ItemLocation ): Promise<Array<L2ItemsTableItem>> {
        let query = 'SELECT object_id, item_id, count, enchant_level, loc_data, custom_type1, custom_type2, mana_left, time, agathion_energy, attributes FROM items WHERE owner_id=? AND locationType=?'
        let databaseItems : Array<unknown> = databaseEngine.getMany( query, objectId, baseLocation )

        if ( !databaseItems ) {
            return []
        }

        return databaseItems.map( ( databaseItem: Object ) : L2ItemsTableItem => {
            return {
                ownerId: objectId,
                objectId: databaseItem[ 'object_id' ],
                itemId: databaseItem[ 'item_id' ],
                count: databaseItem[ 'count' ],
                enchantLevel: databaseItem[ 'enchant_level' ],
                customTypeOne: databaseItem[ 'custom_type1' ],
                customTypeTwo: databaseItem[ 'custom_type2' ],
                itemLocation: baseLocation,
                locationData: databaseItem[ 'loc_data' ],
                manaRemaining: databaseItem[ 'mana_left' ],
                timeRemaining: databaseItem[ 'time' ],
                agathionEnergy: databaseItem[ 'agathion_energy' ],
                attributes: databaseItem[ 'attributes' ] ?? -1,
            }
        } )
    },

    async getInventoryItems( objectId: number, baseLocation: ItemLocation, equipLocation: ItemLocation ): Promise<Array<L2ItemsTableItem>> {
        let query = 'SELECT object_id, item_id, count, enchant_level, locationType, loc_data, custom_type1, custom_type2, mana_left, time, agathion_energy, attributes FROM items WHERE owner_id=? AND (locationType=? OR locationType=?) ORDER BY loc_data'
        let databaseItems : Array<unknown> = databaseEngine.getMany( query, objectId, baseLocation, equipLocation )

        if ( !databaseItems ) {
            return []
        }

        return databaseItems.map( ( databaseItem: Object ) : L2ItemsTableItem => {
            return {
                ownerId: objectId,
                objectId: databaseItem[ 'object_id' ],
                itemId: databaseItem[ 'item_id' ],
                count: databaseItem[ 'count' ],
                enchantLevel: databaseItem[ 'enchant_level' ],
                customTypeOne: databaseItem[ 'custom_type1' ],
                customTypeTwo: databaseItem[ 'custom_type2' ],
                itemLocation: databaseItem[ 'locationType' ],
                locationData: databaseItem[ 'loc_data' ],
                manaRemaining: databaseItem[ 'mana_left' ],
                timeRemaining: databaseItem[ 'time' ],
                agathionEnergy: databaseItem[ 'agathion_energy' ],
                attributes: databaseItem[ 'attributes' ] ?? -1,
            }
        } )
    },

    async updateItem( item: L2ItemInstance ): Promise<void> {
        let query = 'UPDATE items SET owner_id=?, count=?, locationType=?, loc_data=?, enchant_level=?, custom_type1=?, custom_type2=?, mana_left=?, time=?, agathion_energy=?, attributes=? WHERE object_id=?'
        let parameters = [
            item.getOwnerId(),
            item.getCount(),
            item.getItemLocation(),
            item.getLocationData(),
            item.getEnchantLevel(),
            item.getCustomType1(),
            item.getCustomType2(),
            item.getMana(),
            item.getTime(),
            item.getAgathionRemainingEnergy(),
            item.getAugmentation()?.getAttributes() ?? null,
            item.getObjectId()
        ]

        return databaseEngine.insertOne( query, ...parameters )
    },

    async insertItem( item: L2ItemInstance ): Promise<void> {
        let query = 'INSERT OR REPLACE INTO items (owner_id, item_id, count, locationType, loc_data, enchant_level, object_id, custom_type1, custom_type2, mana_left, time, agathion_energy, attributes) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)'
        let parameters = [
            item.getOwnerId(),
            item.getId(),
            item.getCount(),
            item.getItemLocation(),
            item.getLocationData(),
            item.getEnchantLevel(),
            item.getObjectId(),
            item.type1,
            item.type2,
            item.getMana(),
            item.getTime(),
            item.getAgathionRemainingEnergy(),
            item.getAugmentation()?.getAttributes() ?? null,
        ]

        return databaseEngine.insertOne( query, ...parameters )
    },

    async deleteByObjectId( id: number ): Promise<void> {
        let query = 'DELETE FROM items WHERE object_id = ?'
        return databaseEngine.insertOne( query, id )
    },

    async getEquippedInventory( ids: Array<number> ) : Promise<Array<L2ItemsTableEquippedItem>> {
        let query = `SELECT owner_id, object_id, item_id, loc_data, enchant_level
                     FROM items
                     WHERE owner_id in (#values#)
                       AND locationType = ${ ItemLocation.PAPERDOLL }`

        let databaseItems : Array<unknown> = databaseEngine.getManyForValues( query, ids )

        if ( !databaseItems ) {
            return []
        }

        return databaseItems.map( ( databaseItem: Object ) : L2ItemsTableEquippedItem => {
            return {
                objectId: databaseItem[ 'object_id' ],
                ownerId: databaseItem[ 'owner_id' ],
                itemId: databaseItem[ 'item_id' ],
                locationData: databaseItem[ 'loc_data' ],
                enchantLevel: databaseItem[ 'enchant_level' ]
            }
        } )
    },

    async deleteItemFromOwner( playerId: number, itemId: number ): Promise<void> {
        let query = 'DELETE FROM items WHERE owner_id=? AND item_id=?'
        return databaseEngine.insertOne( query, playerId, itemId )
    },

    async deleteItemFromOwners( itemId: number, playerIds: Array<number> ): Promise<void> {
        let query = 'DELETE FROM items WHERE owner_id = ? and item_id = ?'

        let values = playerIds.map( ( id: number ) => {
            return [ id, itemId ]
        } )

        return databaseEngine.insertMany( query, values )
    },

    async getMultipleItemAttributes( ids: Array<number> ): Promise<Array<L2ItemTableAttributes>> {
        let query = 'SELECT object_id, attributes FROM items WHERE object_id in (#values#)'
        let data : Array<unknown> = databaseEngine.getManyForValues( query, ids )

        return data.map( ( item : Object ) : L2ItemTableAttributes => {
            return {
                objectId: item[ 'object_id' ],
                attributes: item[ 'attributes' ]
            }
        } )
    },
}