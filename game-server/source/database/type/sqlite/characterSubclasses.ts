import { CharacterSubclassesApi, L2CharacterSubclassItem } from '../../interface/CharacterSubclassesApi'
import { L2PcInstance } from '../../../gameService/models/actor/instance/L2PcInstance'
import { SubClass } from '../../../gameService/models/base/SubClass'
import _ from 'lodash'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteCharacterSubclassesTable: CharacterSubclassesApi = {
    async addSubclass( objectId: number, subClass: SubClass ): Promise<void> {
        const query = 'INSERT INTO character_subclasses (charId,class_id,exp,sp,level,class_index) VALUES (?,?,?,?,?,?)'
        return databaseEngine.insertOne( query,
                objectId,
                subClass.getClassId(),
                subClass.getExp(),
                subClass.getSp(),
                subClass.getLevel(),
                subClass.getClassIndex() )
    },

    async deleteByClassIndex( objectId: number, classIndex: number ): Promise<void> {
        const query = 'DELETE FROM character_subclasses WHERE charId=? AND class_index=?'
        return databaseEngine.insertOne( query, objectId, classIndex )
    },

    async removeCharacter( objectId: number ): Promise<void> {
        const query = 'DELETE FROM character_subclasses WHERE charId=?'
        return databaseEngine.insertOne( query, objectId )
    },

    async getSubclasses( objectId: number ): Promise<Array<L2CharacterSubclassItem>> {
        let query = 'SELECT class_id,exp,sp,level,class_index FROM character_subclasses WHERE charId=?'
        let results : Array<unknown> = databaseEngine.getMany( query, objectId )

        return _.map( results, ( item: any ) : L2CharacterSubclassItem => {
            return {
                classId: item[ 'class_id' ],
                objectId,
                exp: item.exp,
                level: item.level,
                sp: item.sp,
                classIndex: item[ 'class_index' ]
            }
        } )
    },

    async updatePlayer( player: L2PcInstance ): Promise<void> {
        if ( player.getTotalSubClasses() <= 0 ) {
            return
        }

        let query = 'UPDATE character_subclasses SET exp=?,sp=?,level=?,class_id=? WHERE charId=? AND class_index =?'
        let items = _.map( player.getSubClasses(), ( subclass: SubClass ) => {
            return [
                subclass.getExp(),
                subclass.getSp(),
                subclass.getLevel(),
                subclass.getClassId(),
                player.getObjectId(),
                subclass.getClassIndex(),
            ]
        } )

        return databaseEngine.insertMany(
                query,
                items,
        )
    },

    async getSubclassInfo( ids: Array<number> ): Promise<Array<L2CharacterSubclassItem>> {
        let query = 'SELECT charId, class_id, exp, sp, level, class_index FROM character_subclasses WHERE charId in (#values#)'
        let data : Array<unknown> = databaseEngine.getManyForValues( query, ids )

        return data.map( ( item : any ) : L2CharacterSubclassItem => {
            return {
                classId: item[ 'class_id' ],
                objectId: item.charId,
                exp: item.exp,
                level: item.level,
                sp: item.sp,
                classIndex: item[ 'class_index' ]
            }
        } )
    },
}