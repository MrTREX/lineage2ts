import { L2CharacterTeleportBookmarkItem, L2CharacterTeleportBookmarksTableApi } from '../../interface/CharacterTeleportBookmarksTableApi'
import _ from 'lodash'
import { ILocational } from '../../../gameService/models/Location'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteCharacterTeleportBookmarks: L2CharacterTeleportBookmarksTableApi = {
    async delete( objectId: number, id: number ): Promise<void> {
        const query = 'DELETE FROM teleport_bookmarks WHERE playerId=? AND id=?'
        return databaseEngine.insertOne( query, objectId, id )
    },

    async update( objectId: number, id: number, icon: number, tag: string, name: string ): Promise<void> {
        const query = 'UPDATE teleport_bookmarks SET icon=?,tag=?,name=? where playerId=? AND id=?'
        return databaseEngine.insertOne( query, icon, tag, name, objectId, id )
    },

    async add( objectId: number, id: number, location: ILocational, icon: number, tag: string, name: string ): Promise<void> {
        const query = 'INSERT INTO teleport_bookmarks (playerId,id,x,y,z,icon,tag,name,heading) values (?,?,?,?,?,?,?,?,?)'
        return databaseEngine.insertOne( query, objectId, id, location.getX(), location.getY(), location.getZ(), icon, tag, name, location.getHeading() )
    },

    async getAll( objectId: number ): Promise<{ [ key: number ]: L2CharacterTeleportBookmarkItem }> {
        const query = 'SELECT id,x,y,z,icon,tag,name,heading FROM teleport_bookmarks WHERE playerId=?'
        let databaseItems = databaseEngine.getMany( query, objectId )

        return _.reduce( databaseItems, ( finalMap: { [ key: number ]: L2CharacterTeleportBookmarkItem }, databaseItem: any ) => {
            let { id, x, y, z, icon, tag, name, heading } = databaseItem

            finalMap[ id ] = {
                id,
                name,
                tag,
                icon,
                x,
                y,
                z,
                heading,
            }

            return finalMap
        }, {} )
    },
}