import { CharacterItemReuseSaveTableApi, L2CharacterItemReuse } from '../../interface/CharacterItemReuseSaveTableApi'
import { L2PcInstance } from '../../../gameService/models/actor/instance/L2PcInstance'
import { L2Timestamp } from '../../../gameService/models/SkillTimestamp'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteCharacterItemReuseSaveTable: CharacterItemReuseSaveTableApi = {
    async createTimes( player: L2PcInstance ): Promise<void> {
        await SQLiteCharacterItemReuseSaveTable.deleteTimes( player.getObjectId() )

        let query = 'INSERT INTO character_item_reuse (playerId,itemId,reuseDelay,reuseExpiration) VALUES (?,?,?,?)'
        let itemsToSave = []

        player.getSkillReuse().getAllItems().forEach( ( timestamp: L2Timestamp ) => {
            itemsToSave.push( [
                player.getObjectId(),
                timestamp.getItemId(),
                timestamp.getReuseMs(),
                timestamp.getExpirationTime(),
            ] )
        } )

        if ( itemsToSave.length > 0 ) {
            return databaseEngine.insertMany( query, itemsToSave )
        }
    },

    async deleteTimes( objectId: number ): Promise<void> {
        let query = 'DELETE FROM character_item_reuse WHERE playerId=?'
        return databaseEngine.insertOne( query, objectId )
    },

    async getTimes( objectId: number ): Promise<Array<L2CharacterItemReuse>> {
        let query = 'SELECT itemId,reuseDelay,reuseExpiration FROM character_item_reuse WHERE playerId=?'
        let results: Array<unknown> = databaseEngine.getMany( query, objectId )
        await SQLiteCharacterItemReuseSaveTable.deleteTimes( objectId )

        return results.map( ( databaseItem: any ): L2CharacterItemReuse => {
            return {
                itemId: databaseItem[ 'itemId' ],
                reuseDelay: databaseItem[ 'reuseDelay' ],
                reuseExpiration: databaseItem[ 'reuseExpiration' ],
            }
        } )
    },
}