import { L2ItemAuctionTableApi, L2ItemAuctionTableData } from '../../interface/ItemAuctionTableApi'
import { ItemAuctionState } from '../../../gameService/models/auction/ItemAuctionState'
import * as databaseEngine from '../../engines/sqlite'
import _ from 'lodash'

export const SQLiteItemAuctionTable: L2ItemAuctionTableApi = {
    async addAuction( id: number, npcId: number, auctionDataId: number, startTime: number, endTime: number, state: ItemAuctionState ): Promise<void> {
        const query = 'INSERT OR REPLACE INTO item_auction (id,npcId,auctionDataId,startingTime,endingTime,state) VALUES (?,?,?,?,?,?)'
        return databaseEngine.insertOne( query, id, npcId, auctionDataId, startTime, endTime, state )
    },

    async getAuctions( npcIds: Array<number> ): Promise<Array<L2ItemAuctionTableData>> {
        const query = 'SELECT * FROM item_auction WHERE npcId in (#values#)'
        let databaseItems : Array<unknown> = databaseEngine.getManyForValues( query, npcIds )
        return databaseItems.map( ( databaseItem: any ): L2ItemAuctionTableData => {
            return {
                id: databaseItem[ 'id' ],
                endTime: databaseItem[ 'endingTime' ],
                auctionDataId: databaseItem[ 'auctionDataId' ],
                startTime: databaseItem[ 'startingTime' ],
                state: databaseItem[ 'state' ],
                npcId: databaseItem[ 'npcId' ],
            }
        } )
    },

    async getAuction( id: number ): Promise<L2ItemAuctionTableData> {
        const query = 'SELECT * FROM item_auction WHERE id = ?'
        let databaseItem: any = databaseEngine.getOne( query, id )

        return {
            id,
            npcId: databaseItem[ 'npcId' ],
            endTime: databaseItem[ 'endingTime' ],
            auctionDataId: databaseItem[ 'auctionDataId' ],
            startTime: databaseItem[ 'startingTime' ],
            state: databaseItem[ 'state' ],
        }
    },

    async deleteAuctions( ids: Array<number> ): Promise<void> {
        const query = 'DELETE FROM item_auction WHERE id in (#values#)'
        return databaseEngine.deleteManyForValues( query, ids )
    },

    async getLastId(): Promise<number> {
        const query = 'SELECT id FROM item_auction ORDER BY id DESC LIMIT 0, 1'
        let databaseItem = databaseEngine.getOne( query )

        return _.get( databaseItem, 'id' )
    }
}