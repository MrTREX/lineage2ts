import { L2ClanPrivilegesTableApi } from '../../interface/ClanPrivilegesTableApi'
import _ from 'lodash'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteClanPrivileges : L2ClanPrivilegesTableApi = {
    async deleteClan( clanId: number ): Promise<void> {
        const query = 'DELETE FROM clan_privs WHERE clan_id=?'
        return databaseEngine.insertOne( query, clanId )
    },

    async getAll( clanId: number ): Promise<{ [p: number]: number }> {
        let query = 'SELECT `privs`, `rank` FROM `clan_privs` WHERE clan_id=?'
        let databaseIetms : Array<unknown> = databaseEngine.getMany( query, clanId )

        return _.reduce( databaseIetms, ( finalMap: any, databaseItem: any ) => {

            let rank = databaseItem[ 'rank' ]
            finalMap[ rank ] = databaseItem[ 'privs' ]
            return finalMap
        }, {} )
    },

    async storePrivilege( clanId: number, rank: number, privilegeMask: number ): Promise<void> {
        let query = 'INSERT OR REPLACE INTO `clan_privs` (`clan_id`, `rank`, `privs`) VALUES (?,?,?)'
        return databaseEngine.insertOne( query, clanId, rank, privilegeMask )
    }
}