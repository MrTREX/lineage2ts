import { L2FortDoorUpgradeTableApi, L2FortDoorUpgradeTableData } from '../../interface/FortDoorUpgradeTableApi'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteFortDoorUpgradeTable: L2FortDoorUpgradeTableApi = {
    async addUpgrade( residenceId: number, doorId: number, hpValue: number, powerDeference: number, magicDefence: number ): Promise<void> {
        const query = 'INSERT INTO fort_doorupgrade (fortId, doorId, hp, pDef, mDef) VALUES (?,?,?,?,?)'
        return databaseEngine.insertOne( query, residenceId, doorId, hpValue, powerDeference, magicDefence )
    },

    async removeAll( residenceId: number ): Promise<void> {
        const query = 'DELETE FROM fort_doorupgrade WHERE fortId = ?'
        return databaseEngine.insertOne( query, residenceId )
    },

    async getAll( residenceId: number ): Promise<Array<L2FortDoorUpgradeTableData>> {
        const query = 'SELECT * FROM fort_doorupgrade WHERE fortId = ?'
        let databaseItems: Array<unknown> = databaseEngine.getMany( query, residenceId )

        return databaseItems.map( ( databaseItem: any ): L2FortDoorUpgradeTableData => {
            return {
                doorId: databaseItem[ 'id' ],
                hpValue: databaseItem[ 'hp' ],
                magicDefence: databaseItem[ 'pDef' ],
                powerDefence: databaseItem[ 'mDef' ],
            }
        } )
    },
}