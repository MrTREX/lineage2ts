import { CharacterMacrosTableApi, L2CharacterMacroItem } from '../../interface/CharacterMacrosTableApi'
import * as databaseEngine from '../../engines/sqlite'
import _ from 'lodash'

export const SQLiteCharacterMacrosTable: CharacterMacrosTableApi = {
    async deleteMany( items: Array<L2CharacterMacroItem> ): Promise<void> {
        let query = 'DELETE FROM character_macroses WHERE charId=? AND id=?'

        return databaseEngine.insertMany( query, items.map( ( item: L2CharacterMacroItem ) => {
            return [
                item.objectId,
                item.id
            ]
        } ) )
    },

    async upsertMany( items: Array<L2CharacterMacroItem> ): Promise<void> {
        let query = 'INSERT OR REPLACE INTO character_macroses (charId,id,icon,name,descr,acronym,commands) values(?,?,?,?,?,?,?)'

        return databaseEngine.insertMany( query, items.map( ( item: L2CharacterMacroItem ) => {
            return [
                item.objectId,
                item.id,
                item.icon,
                item.name,
                item.description,
                item.acronym,
                JSON.stringify( item.commands )
            ]
        } ) )
    },

    async removeCharacter( objectId: number ): Promise<void> {
        const query = 'DELETE FROM character_macroses WHERE charId=?'
        return databaseEngine.insertOne( query, objectId )
    },

    async getMacros( playerId: number ): Promise<Array<L2CharacterMacroItem>> {
        let query = 'SELECT charId, id, icon, name, descr, acronym, commands FROM character_macroses WHERE charId=?'
        let results : Array<unknown> = databaseEngine.getMany( query, playerId )

        return results.map( ( databaseItem: any ): L2CharacterMacroItem => {
            let commands = _.attempt( JSON.parse, databaseItem.commands )
            return {
                objectId: databaseItem.charId,
                acronym: databaseItem.acronym,
                commands: _.isError( commands ) ? [] : commands,
                description: databaseItem.descr,
                icon: databaseItem.icon,
                id: databaseItem.id,
                name: databaseItem.name,
            }
        } )
    },
}