import { L2NpcRespawnTableApi, L2NpcRespawnTableItem } from '../../interface/NpcRespawnTableApi'
import { deleteManyForValues, getMany, insertMany } from '../../engines/sqlite'

export const SQLiteNpcRespawnTable: L2NpcRespawnTableApi = {
    updateAll( items: Array<L2NpcRespawnTableItem> ): Promise<void> {
        const query : string = 'INSERT OR REPLACE INTO npc_respawns (id, x, y, z, heading, killTime, hp, mp, lastUpdate) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)'
        let lastUpdated = Date.now()
        let dataItems = items.map( ( item: L2NpcRespawnTableItem ) => {
            return [
                item.id,
                item.x,
                item.y,
                item.z,
                item.heading,
                item.killTime,
                item.hp,
                item.mp,
                lastUpdated
            ]
        } )

        insertMany( query, dataItems )

        return Promise.resolve()
    },

    remove( ids: Array<string> ): Promise<void> {
        const query = 'delete from npc_respawns where id in (#values#)'
        deleteManyForValues( query, ids )

        return Promise.resolve()
    },

    async getAll(): Promise<Array<L2NpcRespawnTableItem>> {
        const query = 'select * from npc_respawns'
        let databaseItems = getMany( query )

        return databaseItems.map( ( databaseItem: Object ) : L2NpcRespawnTableItem => {
            return {
                heading: databaseItem[ 'heading' ],
                hp: databaseItem[ 'hp' ],
                id: databaseItem[ 'id' ],
                killTime: databaseItem[ 'killTime' ],
                lastUpdate: databaseItem[ 'lastUpdate' ],
                mp: databaseItem[ 'mp' ],
                x: databaseItem[ 'x' ],
                y: databaseItem[ 'y' ],
                z: databaseItem[ 'z' ]
            }
        } )
    }
}