import { L2PetitionFeedbackTableApi } from '../../interface/PetitionFeedbackTableApi'
import { L2PcInstance } from '../../../gameService/models/actor/instance/L2PcInstance'
import * as databaseEngine from '../../engines/sqlite'

export const SQLitePetitionFeedbackTable : L2PetitionFeedbackTableApi = {
    async addEntry( player: L2PcInstance, rate: number, message: string ): Promise<void> {
        const query = 'INSERT INTO petition_feedback VALUES (?,?,?,?,?)'
        return databaseEngine.insertOne( query,
                player.getName(),
                player.getLastPetitionGmName(),
                rate,
                message,
                Date.now() )
    }
}