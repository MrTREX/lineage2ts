import { L2TerritoryRegistrationsTableApi } from '../../interface/TerritoryRegistrationsTableApi'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteTerritoryRegistrationsTable : L2TerritoryRegistrationsTableApi = {
    async add( castleId: number, objectId: number ): Promise<void> {
        const query = 'INSERT OR REPLACE INTO territory_registrations (castleId, registeredId) values (?, ?)'
        return databaseEngine.insertOne( query, castleId, objectId )
    },

    async remove( castleId: number, objectId: number ): Promise<void> {
        const query = 'DELETE FROM territory_registrations WHERE castleId = ? and registeredId = ?'
        return databaseEngine.insertOne( query, castleId, objectId )
    }
}