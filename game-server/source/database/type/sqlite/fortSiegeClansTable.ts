import { L2FortSiegeClansTableApi } from '../../interface/FortSiegeClansTableApi'
import * as databaseEngine from '../../engines/sqlite'
import _ from 'lodash'

export const SQLiteFortSiegeClansTable : L2FortSiegeClansTableApi = {
    async hasRegistrationForAny( clanId: number, residenceIds: Array<number> ): Promise<boolean> {
        const values = _.join( _.times( residenceIds.length, _.constant( '?' ) ), ',' )
        const query = `SELECT EXISTS(SELECT 1 FROM fortsiege_clans where clan_id=? and fort_id in (${values}))`

        let result = databaseEngine.getOne( query, clanId, ...residenceIds )
        return _.values( result )[ 0 ] === 1
    },

    async removeClan( clanId: number ): Promise<void> {
        const query = 'DELETE FROM fortsiege_clans WHERE clan_id=?'
        return databaseEngine.insertOne( query, clanId )
    },

    async addClanRegistration( residenceId: number, clanId: number ): Promise<void> {
        const query = 'INSERT OR REPLACE INTO fortsiege_clans (clan_id,fort_id) values (?,?)'
        return databaseEngine.insertOne( query, clanId, residenceId )
    },

    async getRegisteredClanIds( residenceId: number ): Promise<Array<number>> {
        const query = 'SELECT clan_id FROM fortsiege_clans WHERE fort_id=?'
        const databaseItems = databaseEngine.getMany( query, residenceId )
        return databaseItems.map( item => item[ 'clan_id' ] as number )
    },

    async removeAll( residenceId: number ): Promise<void> {
        const query = 'DELETE FROM fortsiege_clans WHERE fort_id = ?'
        return databaseEngine.insertOne( query, residenceId )
    },

    async removeClanRegistration( residenceId: number, clanId: number ): Promise<void> {
        const query = 'DELETE FROM fortsiege_clans WHERE fort_id = ? AND clan_id = ?'
        return databaseEngine.insertOne( query, residenceId, clanId )
    },

    async hasRegistration( clanId: number, fortId: number ): Promise<boolean> {
        let query = 'SELECT EXISTS(SELECT 1 FROM fortsiege_clans where clan_id=? and fort_id=?)'
        let result = databaseEngine.getOne( query, clanId, fortId )
        return _.values( result )[ 0 ] === 1
    }
}