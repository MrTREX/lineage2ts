import { L2FortFunctionsTableApi, L2FortFunctionsTableData } from '../../interface/FortFunctionsTableApi'
import * as databaseEngine from '../../engines/sqlite'
import _ from 'lodash'

export const SQLiteFortFunctionsTable : L2FortFunctionsTableApi = {
    async remove( fortId: number, type: number ): Promise<void> {
        const query = 'DELETE FROM fort_functions WHERE fort_id=? AND type=?'
        return databaseEngine.insertOne( query, fortId, type )
    },

    async update( fortId: number, type: number, level: number, fee: number, rate: number, endDate: number ): Promise<void> {
        const query = 'REPLACE INTO fort_functions (fort_id, type, lvl, lease, rate, endTime) VALUES (?,?,?,?,?,?)'
        return databaseEngine.insertOne( query, fortId, type, level, fee, rate, endDate )
    },

    async getAll( fortId: number ): Promise<Array<L2FortFunctionsTableData>> {
        const query = 'SELECT * FROM fort_functions WHERE fort_id = ?'
        let databaseItems : Array<unknown> = databaseEngine.getMany( query, fortId )

        return databaseItems.map( ( databaseItem: any ) : L2FortFunctionsTableData => {
            return {
                endDate: databaseItem[ 'endTime' ],
                fee: databaseItem[ 'lease' ],
                level: databaseItem[ 'lvl' ],
                rate: databaseItem[ 'rate' ],
                type: databaseItem[ 'type' ]
            }
        } )
    },

    async removeAll( fortId: number, types: Array<number> ): Promise<void> {
        let query = 'DELETE FROM fort_functions WHERE fort_id=? AND type=?'

        let items = _.map( types, ( currentType: number ) => {
            return [ fortId, currentType ]
        } )

        return databaseEngine.insertMany( query, items )
    }
}