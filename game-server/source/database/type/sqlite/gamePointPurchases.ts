import { L2GamePointPurchasesTableApi, L2GamePointPurchasesTableItem } from '../../interface/GamePointPurchasesTableApi'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteGamePointPurchases : L2GamePointPurchasesTableApi = {
    async addPurchases( items: Array<L2GamePointPurchasesTableItem> ): Promise<void> {
        const query = 'insert into game_point_purchases (accountName, type, productId, amount, paidPrice, time, category, playerId) values (?,?,?,?,?,?,?,?)'
        const data = items.map( ( item: L2GamePointPurchasesTableItem ) : Array<unknown> => {
            return [
                item.accountName,
                item.type,
                item.productId,
                item.amount,
                item.paidPrice,
                item.time,
                item.category,
                item.playerId
            ]
        } )

        return databaseEngine.insertMany( query, data )
    },

    /*
        Please note that limit must be maintained due to other restrictions for displaying
        items in server packet.
     */
    async getPurchases( accountName: string ): Promise<Array<L2GamePointPurchasesTableItem>> {
        const query = 'select * from game_point_purchases where accountName=? ORDER BY time DESC LIMIT 200'
        const databaseItems : Array<unknown> = databaseEngine.getMany( query, accountName )

        return databaseItems.map( ( databaseItem: Object ) : L2GamePointPurchasesTableItem => {
            return {
                amount: databaseItem[ 'amount' ],
                category: databaseItem[ 'category' ],
                accountName,
                paidPrice: databaseItem[ 'paidPrice' ],
                playerId: databaseItem[ 'playerId' ],
                productId: databaseItem[ 'productId' ],
                time: databaseItem[ 'time' ],
                type: databaseItem[ 'type' ]
            }
        } )
    }
}