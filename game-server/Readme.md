# Lineage2TS Project Game Server

## Introduction
- game server represents vast interactive virtual world where players can play
- only H5 client versions are supported

## How to run Game server?

- install dependencies via `npm install`
- compile Typescript to JS via `npm run build`
- ensure you have `datapack.database`, `game.database` and `geopack.database` SQLite files in same directory as `package.json`
- ensure that Login server is running (see Login Server readme for details)
- run JS file in `./dist/source/Start.js`

## Server configuration

- configuration files are available in `./configuration` directory
- if configured (by default), server will reload configuration file on interval described in `.env` file
- observe default or example values provided with documentation blocks in each file

## Server initialization

- configurations are loaded first
- server will try now to connect to login server on time interval
- data files are loaded with all game objects created
- second run of data initialization due to various inter-dependencies on existing game objects
- listeners are initialized/loaded
- if login sever is found and connected, player can join game
- post initialization tasks are run (such as auction, tournament systems)

## Networking

- server requires ports `9014` and `7777` be available
- port `9014` is used for login server communications
- port `7777` is used by L2 client communications

## Contributions
Any and all code contributions are welcome. Please see [template](../.gitlab/merge_request_templates/game-server.md) for code change acceptance actions.

## License
Any and all Lineage2TS Project files use `AGPL-3.0-or-later` license, see [LICENSE](LICENSE) file for legal description