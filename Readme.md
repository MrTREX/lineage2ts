# Lineage2TS Project

## Introduction
Lineage2TS is server platform written in Typescript and runs as Node.js application. Aim of the project is to provide
functional server for Lineage 2 HighFive client. Currently supported Lineage 2 HF client protocol versions are 267, 268, 271 and 273.

Project covers following areas of server development:
- [game server](game-server) where virtual world contains all player interactions along with quests and npcs, supports H5 client
- [login server](login-server) where client verification and authorization is possible to allow player account interactions, supports H5 client
- [cli](cli) for static asset generation and data provisioning operations
- [server-testing](server-testing) where programmatic verification of client-server behavior can be codified
- [proxy](proxy) for programmatic MITM proxy, supporting multiple concurrent L2 clients

Project is open-source using `AGPL-3.0-or-later` license.

## Requirements
- Minimum CPU with 2 cores, 2 GB RAM computer
- Node.js runtime v22.9.0 or higher

## Installation
See each project's `Readme.md` for installation and operation instructions.

## Docker image
Published image is available on [DockerHub](https://hub.docker.com/r/lineage2ts/server) :
- pull image via command `docker pull lineage2ts/server:latest`
- run image via command `docker run -p 7777:7777 -p 2106:2106 lineage2ts/server:latest`

Image contains all necessary game and login servers that are ready to play using Lineage 2 H5 client.

### Using single file Dockerfile
Run following commands : 
- `wget -O lineage2ts.Dockerfile https://gitlab.com/MrTREX/lineage2ts/-/raw/master/gitlab.Dockerfile`
- `docker build -t lineage2ts/server:latest -f lineage2ts.Dockerfile .`
- `docker run -p 7777:7777 -p 2106:2106 lineage2ts/server:latest`

### Using pre-generated data packs
Intended for developers to speed up docker builds using already placed data packs in login and game server directories.
Run following commands :
- `docker build -t lineage2ts/server:latest -f dev.Dockerfile .`
- `docker run -p 7777:7777 -p 2106:2106 lineage2ts/server:latest`

## Contributing
Contributions are welcome. Please note that each project has merge request template for code changes:
- [game server](.gitlab/merge_request_templates/game-server.md)
- [login server](.gitlab/merge_request_templates/login-server.md)
- [cli](.gitlab/merge_request_templates/cli.md)
- [server-testing](.gitlab/merge_request_templates/server-testing.md)

## Copyright
All project files, with exception mentioning license in contents, are covered under `AGPL-3.0-or-later` license. See [LICENSE](LICENSE) file for legal description.