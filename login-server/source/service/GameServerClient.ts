import { Socket } from 'net'
import { GameServerManager } from './GameServerManager'
import { LoginManager } from '../cache/LoginManager'
import { BanManager } from '../cache/BanManager'
import { RpcManager } from '../rpc/manager'
import {
    L2GameServerParameters,
    L2LoginAccountBanEvent,
    L2LoginIPBanEvent,
    L2LoginPlayerAuthenticationEvent,
    L2LoginPlayerChangesAccessLevelEvent,
    L2LoginPlayerChangesPasswordEvent,
    L2LoginPlayerCharactersOnServerEvent,
    L2LoginPlayerLoginEvent,
    L2LoginPlayerLogoutEvent,
    L2LoginRegisterServerEvent,
    L2LoginServerEvent,
    L2LoginServerOperations,
    L2LoginServerStatusEvent,
} from '../rpc/interface/LoginServerOperations'
import {
    L2GameLoginServerErrorEvent,
    L2GamePlayerAuthenticationOutcomeEvent,
    L2GamePlayerChangesPasswordOutcomeEvent,
    L2GameRequestPlayerCharactersEvent,
    L2GameRequestPlayerDisconnectEvent,
    L2GameServerEvent,
    L2GameServerOperations,
    L2GameServerRegisteredOutcomeEvent,
} from '../rpc/interface/GameServerOperations'
import { SessionKeyHelper } from './SessionKey'
import { DatabaseManager } from '../database/manager'
import { ConfigManager } from '../config/ConfigManager'
import { ConnectedGameServer } from '../models/RegisteredGameServer'
import { AvailableServerNames } from '../config/serverNames'
import { L2LoginClient } from './L2LoginClient'
import _ from 'lodash'
import to from 'await-to-js'
import { ServerLog } from '../logger/Logger'

export class L2GameServerClient {
    connection: Socket
    connectionHash: string
    accountsOnGameServer: Set<string> = new Set<string>()
    serverProperties: L2GameServerParameters

    constructor( connection : Socket ) {
        this.connection = connection
        this.connectionHash = this.connection.remoteAddress

        this.connection.on( 'data', this.onProcessData.bind( this ) )
        this.connection.on( 'close', this.onProcessClose.bind( this ) )
        this.connection.on( 'error', this.onProcessError.bind( this ) )
    }

    async onProcessData( incomingData: Buffer ): Promise<void> {
        let result: L2LoginServerEvent = RpcManager.getDataConverter().unpack( incomingData )
        if ( !result ) {
            this.connection.end()
            return
        }

        switch ( result.operationId ) {
            case L2LoginServerOperations.RegisterServer:
                let { serverId } = result as L2LoginRegisterServerEvent
                if ( this.serverProperties && this.serverProperties.serverId !== serverId ) {
                    let error: L2GameLoginServerErrorEvent = {
                        operationId: L2GameServerOperations.LoginServerError,
                        reason: 'Cannot re-register same server!',
                    }

                    return this.sendEvent( error )
                }

                this.processServerRegistration( result as L2LoginRegisterServerEvent )
                this.sendGameServerUpdates()

                return

            case L2LoginServerOperations.PlayerLogin:
                this.addAccountOnGameServer( ( result as L2LoginPlayerLoginEvent ).accountName )
                return

            case L2LoginServerOperations.PlayerLogout:
                this.accountsOnGameServer.delete( ( result as L2LoginPlayerLogoutEvent ).accountName )
                return

            case L2LoginServerOperations.PlayerChangesAccessLevel:
                let { accountName, level } = result as L2LoginPlayerChangesAccessLevelEvent
                return LoginManager.setAccountAccessLevel( accountName, level )

            case L2LoginServerOperations.PlayerAuthentication:
                let authenticationEvent = result as L2LoginPlayerAuthenticationEvent
                let existingKey = LoginManager.getKeyForAccount( authenticationEvent.accountName )
                let response: L2GamePlayerAuthenticationOutcomeEvent = {
                    isSuccess: true,
                    operationId: L2GameServerOperations.PlayerAuthenticationOutcome,
                    accountName: authenticationEvent.accountName,
                }

                if ( existingKey && SessionKeyHelper.isSame( existingKey, authenticationEvent.key ) ) {

                    LoginManager.removeLoggedInClient( authenticationEvent.accountName )
                    return this.sendEvent( response )
                }

                response.isSuccess = false
                return this.sendEvent( response )

            case L2LoginServerOperations.ServerStatus:
                this.serverProperties = result as L2LoginServerStatusEvent
                this.sendGameServerUpdates()

                return


            case L2LoginServerOperations.PlayerCharactersOnServer:
                LoginManager.setCharactersOnServer( result as L2LoginPlayerCharactersOnServerEvent, this.serverProperties.serverId )
                return

            case L2LoginServerOperations.AccountBan:
                let accountBanData = result as L2LoginAccountBanEvent
                BanManager.addBanExpiration( accountBanData.accountName, accountBanData.expirationTime )
                return

            case L2LoginServerOperations.IPBan:
                let ipBanData = result as L2LoginIPBanEvent
                BanManager.addBanExpiration( ipBanData.ipAddress, ipBanData.expirationTime )
                return

            case L2LoginServerOperations.PlayerChangesPassword:
                let passwordEvent = result as L2LoginPlayerChangesPasswordEvent
                let playerPasswordOutcome: L2GamePlayerChangesPasswordOutcomeEvent = {
                    isSuccess: false,
                    operationId: L2GameServerOperations.PlayerChangesPasswordOutcome,
                    message: undefined,
                    accountName: passwordEvent.accountName,
                }

                if ( !this.hasAccountOnGameServer( passwordEvent.accountName ) ) {
                    playerPasswordOutcome.message = 'Cannot change player password at this time.'
                    return this.sendEvent( playerPasswordOutcome )
                }

                if ( !passwordEvent.currentPassword || !passwordEvent.futurePassword ) {
                    playerPasswordOutcome.message = 'Invalid password data! Try again.'
                    return this.sendEvent( playerPasswordOutcome )
                }

                let { password } = await DatabaseManager.getAccounts().getAccount( passwordEvent.accountName )

                if ( password !== passwordEvent.currentPassword ) {
                    playerPasswordOutcome.message = 'The typed current password doesn\'t match with your current one.'
                    return this.sendEvent( playerPasswordOutcome )
                }

                let [ error ] = await to( DatabaseManager.getAccounts().updateAccountPassword( passwordEvent.accountName, passwordEvent.futurePassword ) )

                if ( error ) {
                    ServerLog.error( error, 'Failed updating account password via ChangePassword' )

                    playerPasswordOutcome.message = 'The password change was unsuccessful!'
                    return this.sendEvent( playerPasswordOutcome )
                }

                playerPasswordOutcome.message = 'You have successfully changed your password!'
                playerPasswordOutcome.isSuccess = true

                return this.sendEvent( playerPasswordOutcome )
        }

        let reason = `Unknown operationId ${ result.operationId } in GameServerClient.onProcessData from game server, closing connection!`
        ServerLog.error( reason )

        let error: L2GameLoginServerErrorEvent = {
            operationId: L2GameServerOperations.LoginServerError,
            reason,
        }

        return this.closeConnectionEvent( error )
    }

    onProcessClose() {
        ServerLog.warn( 'Game Server \'%s\' disconnected.', _.get( this.serverProperties, 'serverName', this.connectionHash ) )
        GameServerManager.removeClient( this.connectionHash )
    }

    onProcessError() {
        GameServerManager.removeClient( this.connectionHash )
    }

    getConnectionHash() {
        return this.connectionHash
    }

    sendEvent( event : L2GameServerEvent ) {
        this.connection.write( RpcManager.getDataConverter().pack( event ) )
    }

    closeConnectionEvent( event : L2GameServerEvent ) {
        this.connection.end( RpcManager.getDataConverter().pack( event ) )
    }

    getServerProperties(): L2GameServerParameters {
        return this.serverProperties
    }

    addAccountOnGameServer( accountName: string ) {
        this.accountsOnGameServer.add( accountName )
    }

    hasAccountOnGameServer( accountName: string ) {
        return this.accountsOnGameServer.has( accountName )
    }

    getPlayerCount() {
        return this.accountsOnGameServer.size
    }

    attemptAutoRegistration( data: L2LoginRegisterServerEvent ): void {
        let acceptedServerId: number = GameServerManager.registerWithFirstAvailableId( this, data )
        if ( acceptedServerId === 0 ) {
            let error: L2GameLoginServerErrorEvent = {
                operationId: L2GameServerOperations.LoginServerError,
                reason: 'No server slots available to register!',
            }

            return this.sendEvent( error )
        }

        this.serverProperties = structuredClone( data )
        this.serverProperties.serverId = acceptedServerId

        let event: L2GameServerRegisteredOutcomeEvent = {
            serverName: AvailableServerNames[ this.serverProperties.serverId ],
            acceptedServerId: this.serverProperties.serverId,
            operationId: L2GameServerOperations.ServerRegisteredOutcome,
        }

        return this.sendEvent( event )
    }

    attemptDirectRegistration( data: L2LoginRegisterServerEvent ): void {
        if ( !GameServerManager.registerServer( this, data ) ) {
            let error: L2GameLoginServerErrorEvent = {
                operationId: L2GameServerOperations.LoginServerError,
                reason: `Cannot update information for ServerId ${ data.serverId }!`,
            }

            return this.sendEvent( error )
        }

        this.serverProperties = structuredClone( data )

        let event: L2GameServerRegisteredOutcomeEvent = {
            serverName: AvailableServerNames[ data.serverId ],
            acceptedServerId: data.serverId,
            operationId: L2GameServerOperations.ServerRegisteredOutcome,
        }

        return this.sendEvent( event )
    }

    processServerRegistration( data: L2LoginRegisterServerEvent ): void {
        let existingInformation: ConnectedGameServer = GameServerManager.getRegisteredGameServerById( data.serverId )

        if ( this.serverProperties ) {
            if ( existingInformation.host === data.ip.join( '.' )
                    && existingInformation.port === data.port
                    && existingInformation.serverId === data.serverId ) {

                return this.attemptDirectRegistration( data )
            }

            if ( ConfigManager.server.isGameServerAutoAssignId() && data.isAcceptAlternativeId ) {
                return this.attemptAutoRegistration( data )
            }

            return
        }

        if ( !AvailableServerNames[ data.serverId ] ) {
            let error: L2GameLoginServerErrorEvent = {
                operationId: L2GameServerOperations.LoginServerError,
                reason: `ServerId ${ data.serverId } does not correspond to valid set of ids!`,
            }

            return this.sendEvent( error )
        }

        if ( !GameServerManager.canRegister( data.serverId ) ) {
            if ( ConfigManager.server.isGameServerAutoAssignId() && data.isAcceptAlternativeId ) {
                return this.attemptAutoRegistration( data )
            }

            let error: L2GameLoginServerErrorEvent = {
                operationId: L2GameServerOperations.LoginServerError,
                reason: `ServerId ${ data.serverId } has already been claimed!`,
            }

            return this.sendEvent( error )
        }

        return this.attemptDirectRegistration( data )
    }

    requestServerCharacters( accountName: string ): void {
        let data: L2GameRequestPlayerCharactersEvent = {
            accountName,
            operationId: L2GameServerOperations.RequestPlayerCharacters,
        }

        return this.sendEvent( data )
    }

    disconnectAccount( accountName: string ): void {
        let data: L2GameRequestPlayerDisconnectEvent = {
            accountName,
            operationId: L2GameServerOperations.RequestPlayerDisconnect,
        }

        return this.sendEvent( data )
    }

    sendGameServerUpdates() {
        _.each( LoginManager.getLoggedInClients(), ( client : L2LoginClient ) => {
            if ( client.enableServerListUpdates ) {
                client.sendServerList()
            }
        } )
    }
}