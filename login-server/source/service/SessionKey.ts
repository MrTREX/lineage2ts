import { ConfigManager } from '../config/ConfigManager'
import _ from 'lodash'

function createSessionId() : number {
    return _.random( -2147483648, 2147483647 )
}

function compareKeys( first: SessionKeyProperties, second: SessionKeyProperties ): boolean {
    return first.loginOk1 === second.loginOk1 && first.loginOk2 === second.loginOk2
}

export interface SessionKeyProperties {
    playOk1: number,
    playOk2: number,
    loginOk1: number,
    loginOk2: number,
}

export const SessionKeyHelper = {
    createKey(): SessionKeyProperties {

        let [ playOk1, playOk2, loginOk1, loginOk2 ] = _.times( 4, createSessionId )

        return {
            playOk1,
            playOk2,
            loginOk1,
            loginOk2,
        }
    },

    compareKeys,

    comparePlayKeys( first: SessionKeyProperties, second: SessionKeyProperties ): boolean {
        return compareKeys( first, second ) && first.playOk1 === second.playOk1 && first.playOk2 === second.playOk2
    },

    createSessionId,

    isSame( first: SessionKeyProperties, second: SessionKeyProperties ): boolean {
        if ( ConfigManager.server.showLicense() ) {
            return this.comparePlayKeys( first, second )
        }

        return ( first.playOk1 === second.playOk1 ) && ( first.playOk2 === second.playOk2 )
    },
}