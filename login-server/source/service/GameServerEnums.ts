export enum ServerStatus {
    statusAuto = 0x00,
    statusGood = 0x01,
    statusNormal = 0x02,
    statusFull = 0x03,
    statusDown = 0x04,
    statusGmOnly = 0x05,
}

export enum ServerTypes {
    serverNormal = 0x01,
    serverRelax = 0x02,
    serverTest = 0x04,
    serverNolabel = 0x08,
    serverCreationRestricted = 0x10,
    serverEvent = 0x20,
    serverFree = 0x40,
}

export enum ServerAges {
    serverAgeAll = 0x00,
    serverAge15 = 0x0F,
    serverAge18 = 0x12,
}

export enum ServerStateValues {
    on = 0x01,
    off = 0x00
}

export enum StatusTypes {
    serverListStatus = 0x01,
    serverType = 0x02,
    serverListSquareBracket = 0x03,
    maxPlayers = 0x04,
    testServer = 0x05,
    serverAge = 0x06,
}