import { BlowfishEngine } from './blowfishEngine'
import Chance from 'chance'

const chance = new Chance()

const enum EngineValues {
    blockSize = 8
}

const initialStaticKey = Buffer.from( [
    0x6b,
    0x60,
    0xcb,
    0x5b,
    0x82,
    0xce,
    0x90,
    0xb1,
    0xcc,
    0x2b,
    0x6c,
    0x55,
    0x6c,
    0x6c,
    0x6c,
    0x6c,
] )

export class EncryptionEngine {
    cipher: BlowfishEngine

    constructor( blowfishKey: Buffer ) {
        this.cipher = new BlowfishEngine( blowfishKey )
    }

    verifyChecksum( dataBuffer: Buffer ): boolean {
        if ( ( dataBuffer.length & 3 ) !== 0 || dataBuffer.length <= 4 ) {
            return false
        }

        let checksumValue = 0
        let lastIndex = dataBuffer.length - 4

        for ( let index = 0; index < lastIndex; index += 4 ) {
            checksumValue ^= dataBuffer.readInt32LE( index )
        }

        return dataBuffer.readInt32LE( lastIndex ) === checksumValue
    }

    appendChecksum( data: Buffer ): Buffer {
        let checksumValue = 0

        let lastIndex = data.length - 4
        for ( let index = 0; index < lastIndex; index += 4 ) {
            checksumValue ^= data.readInt32LE( index )
        }

        data.writeInt32LE( checksumValue, lastIndex )

        return data
    }

    encXORPass( data: Buffer ): Buffer {
        let position = 4
        let key = new Int32Array( 1 )

        key[ 0 ] = chance.integer( { min: -2147483648, max: 2147483647 } )

        let lastIndex = data.length - 8

        while ( position < lastIndex ) {
            let value = data.readInt32LE( position )

            key[ 0 ] += value
            value ^= key[ 0 ]

            data.writeInt32LE( value, position )
            position += 4
        }

        data.writeInt32LE( key[ 0 ], lastIndex )
        data.writeInt32LE( 0, data.length - 4 )

        return data
    }

    decrypt( data: Buffer ): Buffer {
        for ( let index = 0; index < data.length; index += EngineValues.blockSize ) {
            this.cipher.decryptBlockAtIndex( data, index, data, index )
        }

        return data
    }

    encrypt( data: Buffer ): void {
        for ( let index = 0; index < data.length; index += EngineValues.blockSize ) {
            this.cipher.encryptBlockAtIndex( data, index, data, index )
        }
    }
}

export const InitialEncryptionEngine = new EncryptionEngine( initialStaticKey )