import * as forge from 'node-forge'

const rsa = forge.pki.rsa

const scrambleModulus = ( values: Array<number> ) : Buffer => {
    let scrambledMod = Buffer.from( values )

    if ( ( scrambledMod.length === 0x81 ) && ( scrambledMod[ 0 ] === 0x00 ) ) {
        scrambledMod = scrambledMod.subarray( 1 )
    }
    // step 1 : 0x4d-0x50 <-> 0x00-0x04
    for ( let i = 0; i < 4; i++ ) {
        let temp = scrambledMod[ i ]
        scrambledMod[ i ] = scrambledMod[ 0x4d + i ]
        scrambledMod[ 0x4d + i ] = temp
    }
    // step 2 : xor first 0x40 bytes with last 0x40 bytes
    for ( let i = 0; i < 0x40; i++ ) {
        scrambledMod[ i ] = scrambledMod[ i ] ^ scrambledMod[ 0x40 + i ]
    }
    // step 3 : xor bytes 0x0d-0x10 with bytes 0x34-0x38
    for ( let i = 0; i < 4; i++ ) {
        scrambledMod[ 0x0d + i ] = scrambledMod[ 0x0d + i ] ^ scrambledMod[ 0x34 + i ]
    }
    // step 4 : xor last 0x40 bytes with first 0x40 bytes
    for ( let i = 0; i < 0x40; i++ ) {
        scrambledMod[ 0x40 + i ] = scrambledMod[ 0x40 + i ] ^ scrambledMod[ i ]
    }

    return scrambledMod
}

export interface L2KeyPair {
    value: any
    modulus: Buffer
}


export function createKeyPair() :L2KeyPair {
    let value = rsa.generateKeyPair( { bits: 1024, e: 0x10001 } )
    return {
        value,
        modulus: scrambleModulus( value.publicKey.n.toByteArray() )
    }
}