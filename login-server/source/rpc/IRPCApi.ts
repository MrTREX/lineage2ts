import { L2BinaryConverter } from './interface/BinaryConverter'

export interface L2RPCApi {
    getDataConverter() : L2BinaryConverter
}