import { L2LoginConfigEngine } from '../IConfigEngine'
import { DotEnvServerConfiguration } from '../type/dotenv/ServerConfiguration'
import { DotEnvDatabaseConfiguration } from '../type/dotenv/DatabaseConfiguration'
import pkgDir from 'pkg-dir'
import _ from 'lodash'
import * as fs from 'fs/promises'
import dotenv from 'dotenv'

const rootDirectory = pkgDir.sync( __dirname )
const pathPrefix = `${ rootDirectory }/configuration`

export const DotEnvConfigurationEngine: L2LoginConfigEngine = {
    database: DotEnvDatabaseConfiguration,
    server: DotEnvServerConfiguration,
}

export const DotEnvConfigurationEngineHelper = {
    async getConfig( fileName: string ): Promise<{ [ key: string ]: string }> {
        return dotenv.parse( await fs.readFile( `${ pathPrefix }/${ fileName }` ) )
    },

    getBoolean( configuration: { [ name: string ]: string }, cache: { [ name: string ]: any }, name: string ): boolean {
        if ( _.isUndefined( cache[ name ] ) ) {
            cache[ name ] = configuration[ name ] === 'True'
        }

        return cache[ name ]
    },

    getNumber( configuration: { [ name: string ]: string }, cache: { [ name: string ]: any }, name: string, multiplier: number = 1 ): number {
        if ( _.isUndefined( cache[ name ] ) ) {
            cache[ name ] = _.parseInt( configuration[ name ] ) * multiplier
        }

        return cache[ name ]
    },

    getNumberArray( configuration: { [ name: string ]: string }, cache: { [ name: string ]: any }, name: string, separator: string = ',' ): Array<number> {
        if ( _.isUndefined( cache[ name ] ) ) {
            let value = configuration[ name ]
            cache[ name ] = _.isEmpty( value ) ? [] : _.map( _.split( value, separator ), _.parseInt )
        }

        return cache[ name ]
    },

    getFloat( configuration: { [ name: string ]: string }, cache: { [ name: string ]: any }, name: string ): number {
        if ( _.isUndefined( cache[ name ] ) ) {
            let value = configuration[ name ]
            cache[ name ] = _.isEmpty( value ) ? 0 : parseFloat( configuration[ name ] )
        }

        return cache[ name ]
    },

    getFloatArray( configuration: { [ name: string ]: string }, cache: { [ name: string ]: any }, name: string, separator: string = ',' ): Array<number> {
        if ( _.isUndefined( cache[ name ] ) ) {
            let value = configuration[ name ]
            cache[ name ] = _.isEmpty( value ) ? [] : _.map( _.split( configuration[ name ], separator ), parseFloat )
        }

        return cache[ name ]
    },

    getRegex( configuration: { [ name: string ]: string }, cache: { [ name: string ]: any }, name: string ) {
        if ( _.isUndefined( cache[ name ] ) ) {
            cache[ name ] = new RegExp( configuration[ name ] )
        }

        return cache[ name ]
    },

    getNumberMap( configuration: { [ name: string ]: string }, cache: { [ name: string ]: any }, name: string ): { [ key: number ]: number } {
        if ( _.isUndefined( cache[ name ] ) ) {
            let value = configuration[ name ]
            cache[ name ] = _.isEmpty( value ) ? {} : _.reduce( _.split( configuration[ name ], ';' ), ( totals: { [ key: number ]: number }, line: string ) => {
                let [ key, value ] = _.map( _.split( line, ',' ), _.parseInt )

                totals[ key ] = value

                return totals
            }, {} )
        }

        return cache[ name ]
    },

    getFloatMap( configuration: { [ name: string ]: string }, cache: { [ name: string ]: any }, name: string ): { [ key: number ]: number } {
        if ( _.isUndefined( cache[ name ] ) ) {
            let value = configuration[ name ]
            cache[ name ] = _.isEmpty( value ) ? {} : _.reduce( _.split( configuration[ name ], ';' ), ( totals: { [ key: number ]: number }, line: string ) => {
                let [ key, value ] = _.split( line, ',' )

                totals[ _.parseInt( key ) ] = parseFloat( value )

                return totals
            }, {} )
        }

        return cache[ name ]
    },

    getStringArray( configuration: { [ name: string ]: string }, cache: { [ name: string ]: any }, name: string, separator: string = ',' ): Array<string> {
        if ( _.isUndefined( cache[ name ] ) ) {
            cache[ name ] = _.split( configuration[ name ], separator )
        }

        return cache[ name ]
    },

    getHexNumber( configuration: { [ name: string ]: string }, cache: { [ name: string ]: any }, name: string, defaultValue: number ): number {
        if ( _.isUndefined( cache[ name ] ) ) {
            cache[ name ] = _.isEmpty( configuration[ name ] ) ? defaultValue : _.parseInt( `0x${ configuration[ name ] }` )
        }

        return cache[ name ]
    },

    getStringMap( configuration: { [ name: string ]: string }, cache: { [ name: string ]: any }, name: string ): { [ key: string ]: string } {
        if ( _.isUndefined( cache[ name ] ) ) {
            let value = configuration[ name ]
            cache[ name ] = _.isEmpty( value ) ? {} : _.reduce( _.split( configuration[ name ], ';' ), ( totals: { [ key: number ]: number }, line: string ) => {
                let [ key, value ] = _.split( line, ',' )

                totals[ key ] = value

                return totals
            }, {} )
        }

        return cache[ name ]
    },

    getCustomValue( configuration: { [ name: string ]: string }, cache: { [ name: string ]: any }, name: string, method: Function ): any {
        if ( _.isUndefined( cache[ name ] ) ) {
            cache[ name ] = method( configuration[ name ] )
        }

        return cache[ name ]
    },
}