import { IClientPacket } from './IClientPacket'

const enum PacketWritableChunks {
    C = 1,
    D = 4,
    F = 8,
    H = 2,
    Q = 8
}

export class ReadableClientPacket implements IClientPacket {
    currentBuffer: Buffer
    data: Array<any> = []
    offset: number

    constructor( buffer, startOffset = 0 ) {
        this.currentBuffer = buffer
        this.offset = startOffset
    }

    readC(): number {
        let value = this.currentBuffer.readUInt8( this.offset )
        this.offset += PacketWritableChunks.C

        return value
    }

    readH(): number {
        let value = this.currentBuffer.readUInt16LE( this.offset )
        this.offset += PacketWritableChunks.H

        return value
    }

    readD(): number {
        let value = this.currentBuffer.readInt32LE( this.offset )
        this.offset += PacketWritableChunks.D

        return value
    }

    readF(): number {
        let value = this.currentBuffer.readDoubleLE( this.offset )
        this.offset += PacketWritableChunks.F

        return value
    }

    readB( length: number ): Buffer {
        let value = this.currentBuffer.subarray( this.offset, this.offset + length )
        this.offset += length

        return value
    }

    readS(): string {
        let currentIndex

        for ( currentIndex = this.offset; currentIndex < this.currentBuffer.length; currentIndex += 2 ) {
            if ( this.currentBuffer.readUInt16LE( currentIndex ) === 0x00 ) {
                break
            }
        }

        let value = this.currentBuffer.toString( 'ucs2', this.offset, currentIndex )
        this.offset = currentIndex + 2

        return value
    }

    readQ(): BigInt {
        let value = this.currentBuffer.readBigInt64LE( this.offset )
        this.offset += PacketWritableChunks.Q

        return value
    }
}