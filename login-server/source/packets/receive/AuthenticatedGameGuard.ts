import { ReadableClientPacket } from '../ReadableClientPacket'

export function AuthenticatedGameGuard( sessionId: number, bufferData: Buffer ): boolean {
    let unconfirmedSessionId: number = new ReadableClientPacket( bufferData, 1 ).readD()

    return sessionId === unconfirmedSessionId
}