import { DeclaredServerPacket } from '../DeclaredServerPacket'

export function GameGuardSuccess( sessionId: number ): Buffer {
    return new DeclaredServerPacket( 21 )
            .writeC( 0x0b )
            .writeD( sessionId )
            .writeD( 0x00 )
            .writeD( 0x00 )

            .writeD( 0x00 )
            .writeD( 0x00 )
            .getBuffer()
}