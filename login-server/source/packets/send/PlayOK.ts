import { DeclaredServerPacket } from '../DeclaredServerPacket'
import { SessionKeyProperties } from '../../service/SessionKey'

export function PlayOK( sessionKey: SessionKeyProperties ): Buffer {
    return new DeclaredServerPacket( 9 )
            .writeC( 0x07 )
            .writeD( sessionKey.playOk1 )
            .writeD( sessionKey.playOk2 )
            .getBuffer()
}