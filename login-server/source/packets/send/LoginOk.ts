import { DeclaredServerPacket } from '../DeclaredServerPacket'
import { SessionKeyProperties } from '../../service/SessionKey'

const emtpyBlockSize = 16
const emptyBlock = Buffer.allocUnsafeSlow( emtpyBlockSize ).fill( 0x00 )

export function LoginOk( sessionKey: SessionKeyProperties ): Buffer {
    return new DeclaredServerPacket( 33 + emtpyBlockSize )
            .writeC( 0x03 )
            .writeD( sessionKey.loginOk1 )
            .writeD( sessionKey.loginOk2 )
            .writeD( 0x00 )

            .writeD( 0x00 )
            .writeD( 0x000003ea )
            .writeD( 0x00 )
            .writeD( 0x00 )

            .writeD( 0x00 )
            .writeB( emptyBlock )
            .getBuffer()
}