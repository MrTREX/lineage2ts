import { L2LoginClient } from '../../service/L2LoginClient'
import { SessionKeyHelper } from '../../service/SessionKey'
import { LoginManager } from '../../cache/LoginManager'
import { GameServerManager } from '../../service/GameServerManager'
import { LoginOk } from '../send/LoginOk'
import { ServerList } from '../send/ServerList'
import { LoginFail, LoginFailReason } from '../send/LoginFail'
import { AccountKicked, AccountKickedReason } from '../send/AccountKicked'
import { DatabaseManager } from '../../database/manager'
import { ConnectedGameServer } from '../../models/RegisteredGameServer'
import { ConfigManager } from '../../config/ConfigManager'
import { L2AccountInformation } from '../../database/interface/AccountsTableApi'
import { BanManager } from '../../cache/BanManager'
import { L2GameServerClient } from '../../service/GameServerClient'
import { ReadableClientPacket } from '../ReadableClientPacket'
import { RateLimiterCache } from '../../cache/RateLimiterCache'
import { AccountUpdateManager } from '../../cache/AccountUpdateManager'
import * as blake3 from 'blake3'
import { ServerLog } from '../../logger/Logger'

const enum LoginStatus {
    authenticationSuccess,
    invalidPassword,
    accountBanned,
    alreadyOnLoginServer,
    alreadyOnGameServer,
}

const enum PasswordHashVersions {
    Blake3 = 1
}

/*
    Blake3 is fast hash, which means brute-force attacks can unmask password faster than slow algorithms,
    which is not desired, hence double hashing of the hash with sufficient length makes
    brute-force attack much less likely to succeed if at all.
 */
const Blake3HashOptions = {
    length: 64,
}

const alphaNumericCheck = new RegExp( /^[abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0-9]+$/ )

export const AuthenticatedLogin = {
    async process( client: L2LoginClient, packetData: Buffer ): Promise<void> {
        let packet = new ReadableClientPacket( packetData, 1 )
        let encryptedCredentials: Buffer = packet.readB( 128 )
        let sessionId : number = packet.readD()

        if ( sessionId !== client.sessionId ) {
            client.closeConnection( LoginFail( LoginFailReason.accessFailedTryAgainLater ) )
            ServerLog.trace( 'Incorrect sessionId for client from %s', client.getConnectionIpAddress() )
            return
        }

        let credentials: string = client.getRSAKeyPair().privateKey.decrypt( encryptedCredentials, 'NONE' )
        let userName: string = credentials.substring( 0x5E, 0x6C ).toLowerCase().replace( /\0/g, '' ).trim()
        let password: string = credentials.substring( 0x65, 0x7C ).replace( /\0/g, '' ).trim()

        if ( !alphaNumericCheck.test( userName ) || !alphaNumericCheck.test( password ) ) {
            client.closeConnection( LoginFail( LoginFailReason.wrongCredentials ) )
            ServerLog.trace( 'Non-ascii AuthenticatedLogin credentials from %s', client.getConnectionIpAddress() )
            return
        }

        let accountInformation: L2AccountInformation = await AuthenticatedLogin.getAcountInformation( userName, password )
        if ( !accountInformation ) {
            client.closeConnection( LoginFail( LoginFailReason.wrongCredentials ) )
            return
        }

        switch ( AuthenticatedLogin.getAccountStatus( accountInformation, client ) ) {
            case LoginStatus.authenticationSuccess:
                client.setAccount( accountInformation.login )
                client.setStateLoginAuthenticated()
                client.setSessionKey( SessionKeyHelper.createKey() )

                LoginManager.addLoggedInClient( accountInformation.login, client )

                GameServerManager.prefetchCharactersOnAccount( accountInformation.login )

                if ( ConfigManager.server.showLicense() ) {
                    client.sendPacket( LoginOk( client.getSessionKey() ) )
                    return
                }

                client.sendPacket( ServerList( client ) )
                return

            case LoginStatus.invalidPassword:
                client.closeConnection( LoginFail( LoginFailReason.wrongCredentials ) )
                return

            case LoginStatus.accountBanned:
                client.closeConnection( AccountKicked( AccountKickedReason.permanentlyBanned ) )
                return

            case LoginStatus.alreadyOnLoginServer:
                let oldClient = LoginManager.getLoggedInClient( accountInformation.login )
                if ( oldClient ) {
                    LoginManager.removeLoggedInClient( accountInformation.login )
                }

                client.closeConnection( LoginFail( LoginFailReason.accountInUse ) )
                return

            case LoginStatus.alreadyOnGameServer:
                let gameServerClient: L2GameServerClient = GameServerManager.getClientByAccountName( accountInformation.login )
                if ( gameServerClient ) {
                    gameServerClient.disconnectAccount( accountInformation.login )

                    client.closeConnection( LoginFail( LoginFailReason.accountInUse ) )
                }

                return
        }
    },

    async getAcountInformation( userName: string, password: string ): Promise<L2AccountInformation> {
        let accountInformation: L2AccountInformation = await DatabaseManager.getAccounts().getAccount( userName )

        if ( !accountInformation && ConfigManager.server.shouldAutoCreateAccounts() ) {
            if ( !alphaNumericCheck.test( userName ) || !alphaNumericCheck.test( password ) ) {
                return
            }

            ServerLog.trace( 'Auto-creating account for user : %s', userName )
            let passwordHash = this.getPasswordHash( password )
            await DatabaseManager.getAccounts().createAccount( userName, passwordHash, ConfigManager.server.getAutoCreateAccessLevel(), this.getPasswordVersion() )

            return DatabaseManager.getAccounts().getAccount( userName )
        }

        let passwordHash = this.getPasswordHash( password )
        if ( accountInformation.password !== passwordHash ) {
            BanManager.recordFailedLogin( userName )
            return
        }

        BanManager.clearFailedLoginAttempts( userName )

        return accountInformation
    },

    getAccountStatus( accountInformation: L2AccountInformation, client: L2LoginClient ): LoginStatus {
        if ( accountInformation.accessLevel < 0 ) {
            return LoginStatus.accountBanned
        }

        if ( !RateLimiterCache.isAllowedAccount( accountInformation.login ) ) {
            return LoginStatus.alreadyOnLoginServer
        }

        let isAllowedAccount: boolean = BanManager.isAllowed( accountInformation.login ) && BanManager.isAllowed( client.getConnectionIpAddress() )
        if ( !isAllowedAccount ) {
            return LoginStatus.accountBanned
        }

        client.setAccessLevel( accountInformation.accessLevel )
        client.setLastServer( accountInformation.lastServerId )
        AccountUpdateManager.refreshLastAccess( accountInformation.login )


        if ( !AuthenticatedLogin.isAccountInAnyGameServer( accountInformation.login ) ) {

            let loggedInClient: L2LoginClient = LoginManager.getLoggedInClient( accountInformation.login )
            if ( !loggedInClient ) {
                return LoginStatus.authenticationSuccess
            }

            return LoginStatus.alreadyOnLoginServer
        }

        return LoginStatus.alreadyOnGameServer
    },

    getPasswordHash( value: string ): string {
        return blake3.hash( blake3.hash( value, Blake3HashOptions ).toString( 'base64' ), Blake3HashOptions ).toString( 'base64' )
    },

    isAccountInAnyGameServer( userName: string ): boolean {
        return GameServerManager.getRegisteredGameServers().some( ( server: ConnectedGameServer ) : boolean => {
            return server.client && server.client.hasAccountOnGameServer( userName )
        } )
    },

    getPasswordVersion() : number {
        return PasswordHashVersions.Blake3
    }
}