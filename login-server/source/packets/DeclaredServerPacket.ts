import { IServerPacket } from './IServerPacket'

const enum PacketWritableChunks {
    C = 1,
    D = 4,
    F = 8,
    H = 2,
    Q = 8
}

const shortIntConverter = new Int16Array( 1 )

export class DeclaredServerPacket implements IServerPacket {
    buffer: Buffer
    position: number = 2

    constructor( size: number ) {
        /*
            Login packets always contain checksum (four bytes) or (if first packet sent) XORPass information (eight bytes)
            Hence, we must account for additional size. First packet will have already included additional bytes for its size, so
            no special handling is needed.
         */
        let length = size + 4
        let finalSize = 2 + length + 8 - ( length % 8 )
        this.buffer = Buffer.allocUnsafe( finalSize )
        this.buffer.writeInt16LE( this.buffer.length , 0 )
    }

    getBuffer(): Buffer {
        return this.buffer
    }

    writeB( value: Buffer ): IServerPacket {
        value.copy( this.buffer, this.position )
        this.position += value.length
        return this
    }

    writeC( value: number ): IServerPacket {
        this.buffer.writeUInt8( value, this.position )
        this.position += PacketWritableChunks.C
        return this
    }

    writeD( value: number ): IServerPacket {
        this.buffer.writeInt32LE( value, this.position )
        this.position += PacketWritableChunks.D
        return this
    }

    writeF( value: number ): IServerPacket {
        this.buffer.writeDoubleLE( value, this.position )
        this.position += PacketWritableChunks.F
        return this
    }

    writeH( value: number ): IServerPacket {
        shortIntConverter[ 0 ] = value

        this.buffer.writeInt16LE( shortIntConverter[ 0 ], this.position )
        this.position += PacketWritableChunks.H
        return this
    }

    writeQ( value: number ): IServerPacket {
        this.buffer.writeBigInt64LE( BigInt( value ), this.position )
        this.position += PacketWritableChunks.Q
        return this
    }

    writeS( messageString: string ): IServerPacket {
        let message = Buffer.from( `${ messageString }\x00`, 'ucs2' )
        message.copy( this.buffer, this.position )
        this.position += message.length

        return this
    }
}

export function getStringSize( message: string ): number {
    return ( message.length + 1 ) * 2
}