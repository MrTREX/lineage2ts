import { GameServerManager } from '../service/GameServerManager'
import { ServerStatus } from '../service/GameServerEnums'
import { DatabaseManager } from '../database/manager'
import { L2LoginClient } from '../service/L2LoginClient'
import { L2GameServerClient } from '../service/GameServerClient'
import { RateLimiterCache } from './RateLimiterCache'
import { L2LoginDataApi } from './L2LoginDataApi'
import { BanManager } from './BanManager'
import { ConnectedGameServer } from '../models/RegisteredGameServer'
import { L2GameServerParameters, L2LoginServerCharacters } from '../rpc/interface/LoginServerOperations'
import { SessionKeyProperties } from '../service/SessionKey'
import { Socket } from 'net'
import { L2AccountsCount } from '../database/interface/AccountsTableApi'
import _ from 'lodash'
import { ServerLog } from '../logger/Logger'

class Manager implements L2LoginDataApi {
    gameClients: { [ key: string ]: L2LoginClient } = {}
    loginServerClients: { [ accountName: string ]: L2LoginClient } = {}

    async load(): Promise<void> {
        let summary : L2AccountsCount = await DatabaseManager.getAccounts().getAccountSummary()
        ServerLog.info( `Total accounts available for login: ${ summary.total }` )
    }

    getBlowfishKey(): Buffer {
        const numberSequence: Array<number> = _.times( 16, (): number => {
            return _.random( 255 )
        } )

        return Buffer.from( numberSequence )
    }

    addClient( client: L2LoginClient ) {
        let connectionHash = client.getConnectionHash()
        this.gameClients[ connectionHash ] = client
    }

    addLoggedInClient( accountName: string, client: L2LoginClient ) {
        this.loginServerClients[ accountName ] = client
    }

    getLoggedInClient( accountName: string ) {
        return this.loginServerClients[ accountName ]
    }

    removeLoggedInClient( accountName: string ) {
        delete this.loginServerClients[ accountName ]
    }

    removeClient( connectionHash: string ): void {
        let client: L2LoginClient = this.gameClients[ connectionHash ]
        if ( client ) {
            let account: string = client.getAccount()
            if ( account ) {
                delete this.loginServerClients[ account ]
            }

            delete this.gameClients[ connectionHash ]
        }
    }

    isAllowed( connection : Socket ) : boolean {
        return BanManager.isAllowed( connection.remoteAddress )
                && RateLimiterCache.isAllowedIpAddress( connection.remoteAddress )
    }


    getKeyForAccount( account ) : SessionKeyProperties {
        if ( !_.has( this.loginServerClients, account ) ) {
            return null
        }

        return this.loginServerClients[ account ].getSessionKey()
    }

    isLoginPossible( client, serverId ) : boolean {
        let gameServer: ConnectedGameServer = GameServerManager.getRegisteredGameServerById( serverId )

        if ( !gameServer ) {
            return false
        }

        let serverClient: L2GameServerClient = gameServer.client

        if ( serverClient && serverClient.getServerProperties().isServerAcceptingPlayers ) {
            let serverProperties : L2GameServerParameters = serverClient.getServerProperties()
            let isAllowed: boolean = ( ( serverClient.getPlayerCount() < serverProperties.maxPlayers ) && ( serverProperties.serverStatus !== ServerStatus.statusGmOnly ) ) ||
                    client.getAccessLevel() > 0

            if ( isAllowed && client.getLastServer() !== serverId ) {
                // no need to make whole function async capable since we can defer database action to later
                DatabaseManager.getAccounts().updateAccountLastServerId( client.getAccount(), serverId )
            }

            return isAllowed
        }

        return false
    }

    setAccountAccessLevel( accountName, level ) : Promise<void> {
        return DatabaseManager.getAccounts().setAccountAccessLevel( accountName, level )
    }

    setCharactersOnServer( accountData: L2LoginServerCharacters, serverId: number ) {
        let client: L2LoginClient = this.loginServerClients[ accountData.accountName ]
        if ( !client ) {
            return
        }

        if ( accountData.totalCharacters > 0 ) {
            client.setCharactersOnServer( serverId, accountData.totalCharacters )
        }

        if ( accountData.timesToDelete.length > 0 ) {
            client.setCharactersWaitingDeleteOnServer( serverId, accountData.timesToDelete )
        }
    }

    getLoggedInClients() {
        return this.loginServerClients
    }
}

export const LoginManager = new Manager()
