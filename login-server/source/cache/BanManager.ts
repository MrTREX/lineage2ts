import { L2LoginDataApi } from './L2LoginDataApi'
import { ConfigManager } from '../config/ConfigManager'
import { DatabaseManager } from '../database/manager'
import { L2BanTableItem } from '../database/interface/BanTableApi'
import _ from 'lodash'
import { ServerLog } from '../logger/Logger'
import Timeout = NodeJS.Timeout

class Manager implements L2LoginDataApi {
    banProperties: { [ name: string ]: number } // ip address/account names and its ban expiration epoch time
    failedLoginAttempts: { [ name: string ]: number } // can be used for account name or ip address
    saveBanTimesTask: Timeout

    async load(): Promise<void> {
        this.failedLoginAttempts = {}

        let currentTime = Date.now()
        let namesToRemove = []

        this.banProperties = _.reduce( await DatabaseManager.getBanData().getAll(), ( allItems: { [ name: string ]: number }, data: L2BanTableItem ) => {
            if ( data.expirationTime === 0 || currentTime < data.expirationTime ) {
                allItems[ data.name ] = data.expirationTime
            } else {
                namesToRemove.push( data.name )
            }

            return allItems
        }, {} )

        if ( namesToRemove.length > 0 ) {
            await DatabaseManager.getBanData().removeItems( namesToRemove )
        }

        this.saveBanTimesTask = setInterval( this.runSaveBanTimesTask.bind( this ), 30000 )

        ServerLog.info( `BanManager : loaded ${ _.size( this.banProperties ) } entries.` )
        ServerLog.info( `BanManager : removed ${ _.size( namesToRemove ) } expired entries.` )
    }

    recordFailedLogin( name: string ): void {
        let attemptsCount = _.get( this.failedLoginAttempts, name, 0 ) + 1
        this.failedLoginAttempts[ name ] = attemptsCount

        if ( attemptsCount >= ConfigManager.server.getBadPasswordRetries() ) {
            this.addBanDuration( name, ConfigManager.server.getBadPasswordLockoutTime() * 1000 )

            // we need to clear the failed login attempts here, so after the ip ban is over the client has another 5 attempts
            this.clearFailedLoginAttempts( name )
        }
    }

    clearFailedLoginAttempts( name: string ) {
        delete this.failedLoginAttempts[ name ]
    }

    isAllowed( name: string ): boolean {
        let bannedExpiration: number = this.banProperties[ name ]

        if ( bannedExpiration ) {
            if ( bannedExpiration === 0 || Date.now() < bannedExpiration ) {
                return false
            }

            this.clearFailedLoginAttempts( name )
            this.removeBan( name )
        }

        return true
    }

    addBanExpiration( name: string, expiration: number ): void {
        if ( !name ) {
            return
        }

        if ( !_.has( this.banProperties, name ) ) {
            this.banProperties[ name ] = expiration
        }
    }

    addBanDuration( name: string, duration: number ) {
        this.banProperties[ name ] = Date.now() + duration
    }

    removeBan( name: string ): boolean {
        if ( !_.has( this.banProperties, name ) ) {
            return false
        }

        delete this.banProperties[ name ]
        DatabaseManager.getBanData().remove( name )

        return true
    }

    runSaveBanTimesTask(): Promise<void> {
        return DatabaseManager.getBanData().addMultipleItems( _.toPairs( this.banProperties ) )
    }
}

export const BanManager = new Manager()