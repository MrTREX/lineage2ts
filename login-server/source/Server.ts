import { L2LoginClient } from './service/L2LoginClient'
import { L2GameServerClient } from './service/GameServerClient'
import { GameServerManager } from './service/GameServerManager'
import { LoginManager } from './cache/LoginManager'
import { ConfigManager } from './config/ConfigManager'
import { BanManager } from './cache/BanManager'
import * as net from 'net'
import { Socket } from 'net'
import perfy from 'perfy'
import { DatabaseManager } from './database/manager'
import { ServerLog } from './logger/Logger'

let clientConnection : net.Server
let gameServerConnection : net.Server

function onLoginClient( connection: Socket ) {
    if ( LoginManager.isAllowed( connection ) ) {
        return LoginManager.addClient( new L2LoginClient( connection ) )
    }

    connection.end()
}

function onGameServer( connection: Socket ) {
    if ( GameServerManager.isAllowed( connection ) ) {
        return GameServerManager.addClient( new L2GameServerClient( connection ) )
    }

    connection.end()
}

async function loadData(): Promise<void> {
    ServerLog.info( `Login Server is using '${ ConfigManager.database.getEngine() }' database engine` )
    await BanManager.load()
    return LoginManager.load()
}

async function shutdownSequence() : Promise<void> {

    clientConnection.close()
    gameServerConnection.close()

    ServerLog.info( 'Login Shutdown : Server terminated all network connections' )

    await DatabaseManager.operations().shutdown()

    ServerLog.info( 'Login Shutdown : Database connections closed' )
    ServerLog.flush( () => process.exit() )
}

let metrics = perfy.end( 'server-load' )
perfy.start( 'server-start' )
loadData().then( () => {
    clientConnection = net.createServer( onLoginClient ).listen( ConfigManager.server.getLoginServerPort(), () => {
        ServerLog.info( `Login Server accepting L2 clients on port ${ ConfigManager.server.getLoginServerPort() }` )
    } )

    gameServerConnection = net.createServer( onGameServer ).listen( ConfigManager.server.getGameServerPort(), () => {
        ServerLog.info( `Login Server accepting Lineage2TS game server registrations on port ${ ConfigManager.server.getGameServerPort() }` )
    } )

    ServerLog.info( `Login Server loaded in ${ metrics.time } seconds` )
    ServerLog.info( `Login Server initialized in ${ perfy.end( 'server-start' ).time } seconds` )


    /*
        We must be able to load all data and initialize server before we are able to shut down
        all parts properly.
     */
    /*
        Used by various restart utilities that manage application process.
     */
    process.once( 'SIGUSR1', shutdownSequence )
    process.once( 'SIGUSR2', shutdownSequence )

    /*
        Used by terminal applications to interrupt process, aka Ctrl + C.
     */
    process.on( 'SIGINT', shutdownSequence )

    /*
        Termination signals used by OS to signify process must be killed (similar to SIGKILL).
     */
    process.on( 'SIGTERM', shutdownSequence )
    process.on( 'SIGHUP', shutdownSequence )
} )