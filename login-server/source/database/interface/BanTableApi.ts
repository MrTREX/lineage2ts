export interface L2BanTableApi {
    getAll() : Promise<Array<L2BanTableItem>>
    remove( name : string ) : Promise<void>
    add( name: string, expirationTime: number ) : Promise<void>
    removeItems( names : Array<string> ) : Promise<void>
    addMultipleItems( valuePairs: Array<[ string, number ]> ): Promise<void>
}

export interface L2BanTableItem {
    name : string
    expirationTime: number
}