import { L2AccountsTableApi } from './interface/AccountsTableApi'
import { L2DatabaseOperations, L2LoginDatabaseApi } from './type/IDatabaseApi'
import { SQLiteLoginDatabase } from './type/SQLite'
import { ConfigManager } from '../config/ConfigManager'
import { L2BanTableApi } from './interface/BanTableApi'

const allEngines = {
    sqlite: SQLiteLoginDatabase,
}

const getEngine = (): L2LoginDatabaseApi => {
    return allEngines[ ConfigManager.database.getEngine() ]
}

export const DatabaseManager = {
    getAccounts(): L2AccountsTableApi {
        return getEngine().accountsTable
    },

    getBanData() : L2BanTableApi {
        return getEngine().banTable
    },

    operations() : L2DatabaseOperations {
        return getEngine().operations
    }
}