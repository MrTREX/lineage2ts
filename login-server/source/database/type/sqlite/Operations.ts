import { L2DatabaseOperations } from '../IDatabaseApi'
import { SQLiteDatabaseEngine } from '../../engines/sqlite'

export const SQLiteLoginDatabaseOperations : L2DatabaseOperations = {
    async shutdown(): Promise<void> {
        return SQLiteDatabaseEngine.shutdown()
    }
}