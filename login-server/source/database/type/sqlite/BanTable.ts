import { L2BanTableApi, L2BanTableItem } from '../../interface/BanTableApi'
import { SQLiteDatabaseEngine } from '../../engines/sqlite'

export const SQLiteBanTable: L2BanTableApi = {
    // TODO : convert to use database item types
    async addMultipleItems( valuePairs: Array<[ string, number ]> ): Promise<void> {
        const query = 'INSERT OR REPLACE into ban_data (name, expirationTime) values (?, ?)'
        return SQLiteDatabaseEngine.insertMany( query, valuePairs )
    },

    async removeItems( names: Array<string> ): Promise<void> {
        const query = 'DELETE FROM ban_data WHERE name in (#values#)'
        return SQLiteDatabaseEngine.deleteManyForValues( query, names )
    },

    async add( name: string, expirationTime: number ): Promise<void> {
        const query = 'INSERT OR REPLACE into ban_data (name, expirationTime) values (?, ?)'
        return SQLiteDatabaseEngine.insertOne( query, name, expirationTime )
    },

    async getAll(): Promise<Array<L2BanTableItem>> {
        const query = 'select * from ban_data'
        let databaseItems: Array<unknown> = SQLiteDatabaseEngine.getMany( query )

        return databaseItems.map( ( databaseItem: any ): L2BanTableItem => {
            return {
                expirationTime: databaseItem[ 'expirationTime' ],
                name: databaseItem[ 'name' ],
            }
        } )
    },

    async remove( name: string ): Promise<void> {
        const query = 'delete from ban_data where name = ?'
        return SQLiteDatabaseEngine.insertOne( query, name )
    }
}