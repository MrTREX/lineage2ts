import { L2GameServerClient } from '../service/GameServerClient'

export interface RegisteredGameServer {
    host: string
    port: number
    serverName : string
    serverId: number
}

export interface ConnectedGameServer extends RegisteredGameServer{
    client : L2GameServerClient
}