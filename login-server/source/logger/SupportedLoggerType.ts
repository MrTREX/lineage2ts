export const enum SupportedLoggerType {
    Console = 'stdout',
    Pretty = 'pretty',
    OpenTelemetry = 'otel'
}