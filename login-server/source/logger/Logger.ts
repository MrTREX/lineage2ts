import { SupportedLoggerType } from './SupportedLoggerType'
import pino, { Logger, LoggerOptions, TransportSingleOptions } from 'pino'
import logSymbols from 'log-symbols'

const defaultOptions: LoggerOptions = {
    name: 'L2TS Login',
    level: 'info'
}

const prettyOptions: TransportSingleOptions = {
    target: 'pino-pretty',
    options: {
        colorize: true
    }
}

export function createLogger( type: string ): Logger {

    switch ( type ) {
        case SupportedLoggerType.OpenTelemetry:
            console.log( logSymbols.info, 'Login Server: sending', process.env.OTEL_EXPORTER_OTLP_PROTOCOL,'logs to OTEL collector', process.env.OTEL_EXPORTER_OTLP_ENDPOINT )

            return pino( {
                ...defaultOptions,
                transport: {
                    targets: [
                        prettyOptions,
                        {
                            target: 'pino-opentelemetry-transport',
                            options: {}
                        }
                    ]
                }
            } )

        case SupportedLoggerType.Pretty:
            return pino( {
                ...defaultOptions,
                transport: prettyOptions,
            } )

        case SupportedLoggerType.Console:
        default:
            /*
                Fallback method in case no types can be associated with any log transports.
             */
            return pino( defaultOptions )
    }
}

export const ServerLog: Logger = createLogger( process.env.loggerType )