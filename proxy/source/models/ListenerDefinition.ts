import { PacketEvent } from 'lineage2ts-testing/source/client/packets/PacketMethodTypes'
import { ClientProxy } from '../service/ClientProxy'

export type ListenerMethod = ( data : PacketEvent, proxy: ClientProxy, rawPacket: Readonly<Buffer> ) => Buffer | null

export const enum ListenerType {
    None,
    GameClientPacket,
    GameServerPacket,
    ProxyVoiceCommand,
    ProxyBypass
}

export interface ListenerDefinition {
    name: string
    method : ListenerMethod
    type: ListenerType
}