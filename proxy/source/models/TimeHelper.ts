export function formatSeconds( amount: number, applySeconds: boolean = true ): Array<string> {
    let seconds = amount
    let messageChunks: Array<string> = []
if ( seconds > 3600 ) {
    messageChunks.push( `${ Math.floor( seconds / 3600 ) } hours` )
    seconds = seconds % 3600
}

if ( seconds > 60 ) {
    messageChunks.push( `${ Math.floor( seconds / 60 ) } minutes` )
    seconds = seconds % 60
}

if ( applySeconds && seconds > 0 ) {
    messageChunks.push( `${ Math.floor( seconds ) } seconds` )
}

return messageChunks
}

export function formatMillis( amount: number, showSeconds: boolean = true ): Array<string> {
    return formatSeconds( amount / 1000, showSeconds )
}