import pino, { Logger, LoggerOptions, TransportSingleOptions } from 'pino'
import { ConfigManager } from '../config/ConfigManager'

const defaultOptions: LoggerOptions = {
    name: 'L2TS Proxy',
    level: 'info'
}

const prettyOptions: TransportSingleOptions = {
    target: 'pino-pretty',
    options: {
        colorize: true
    }
}

export const ServerLog: Logger = pino( {
    ...defaultOptions,
    transport: prettyOptions,
    level: ConfigManager.debug.getLogLevel() ?? defaultOptions.level
} )

ConfigManager.debug.subscribe( () => {
    ServerLog.level = ConfigManager.debug.getLogLevel() ?? defaultOptions.level
} )