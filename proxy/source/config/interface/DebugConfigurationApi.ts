import { ProxyConfigurationApi } from '../IConfiguration'

export interface DebugConfigurationApi extends ProxyConfigurationApi {
    enableLogPacketDiff() : boolean
    getLogLevel() : string
}