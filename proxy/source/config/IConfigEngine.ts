import { ServerConfigurationApi } from './interface/ServerConfigurationApi'
import { DebugConfigurationApi } from './interface/DebugConfigurationApi'

export interface L2ProxyConfigEngine {
    debug: DebugConfigurationApi
    server: ServerConfigurationApi
}