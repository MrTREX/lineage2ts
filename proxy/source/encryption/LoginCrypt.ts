/*
    Verbatim taken from login server code and adjusted to specify
    key at later time.
 */
import { EncryptionEngine, InitialEncryptionEngine } from 'lineage2ts-login/source/security/crypt/EncryptionEngine'
import { EncryptionOperations } from 'lineage2ts-testing/source/client/security/EncryptionOperations'

export class LoginCrypt implements EncryptionOperations {
    isFirstTime: boolean = true
    engine: EncryptionEngine

    setKey( key: Buffer ) {
        this.engine = new EncryptionEngine( key )
    }

    decrypt( encryptedData: Buffer ) : Buffer {

        if ( ( encryptedData.length % 8 ) !== 0 ) {
            throw new Error( 'size have to be multiple of 8' )
        }

        let decryptedData: Buffer = this.engine.decrypt( encryptedData )
        if ( !this.engine.verifyChecksum( decryptedData ) ) {
            throw new Error( 'Cannot verify checksum' )
        }

        return decryptedData
    }

    encrypt( data: Buffer ) : Buffer {
        this.encryptData( data.subarray( 2 ) )
        return data
    }

    private encryptData( data: Buffer ) : void {
        if ( this.isFirstTime ) {
            this.isFirstTime = false

            return InitialEncryptionEngine.encrypt( InitialEncryptionEngine.encXORPass( data ) )
        }

        return this.engine.encrypt( this.engine.appendChecksum( data ) )
    }
}