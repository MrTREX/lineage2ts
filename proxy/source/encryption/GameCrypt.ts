import { EncryptionOperations } from 'lineage2ts-testing/source/client/security/EncryptionOperations'

/*
    Adjusted version from game server to confirm to interface specification.
 */
export class GameCrypt implements EncryptionOperations {
    inKey: Buffer
    outKey: Buffer
    shouldEncrypt: boolean

    constructor() {
        this.inKey = Buffer.allocUnsafeSlow( 16 )
        this.outKey = Buffer.allocUnsafeSlow( 16 )
        this.shouldEncrypt = false
    }

    setKey( key: Buffer ) : void {
        key.copy( this.inKey )
        key.copy( this.outKey )
    }

    decrypt( data: Buffer ) : Buffer {
        if ( !this.shouldEncrypt ) {
            return data
        }

        let temp = 0
        for ( let index = 0; index < data.length; index++ ) {
            let temp2 = data[ index ]
            data[ index ] = temp2 ^ this.inKey[ index & 15 ] ^ temp
            temp = temp2
        }

        let newValue = this.inKey.readInt32LE( 8 ) + data.length
        this.inKey.writeInt32LE( newValue, 8 )

        return data
    }

    encrypt( data: Buffer ) : Buffer {
        if ( !this.shouldEncrypt ) {
            this.shouldEncrypt = true
            return data
        }

        this.encryptData( data.subarray( 2 ) )

        return data
    }

    private encryptData( data: Buffer ) : void {
        let temp = 0
        for ( let index = 0; index < data.length; index++ ) {
            let temp2 = data[ index ]
            temp = temp2 ^ this.outKey[ index & 15 ] ^ temp
            data[ index ] = temp
        }

        let newValue = this.outKey.readInt32LE( 8 ) + data.length
        this.outKey.writeInt32LE( newValue, 8 )
    }
}