import { ProxyListener } from '../models/ProxyListener'
import { ListenerDefinition, ListenerType } from '../models/ListenerDefinition'
import { ProxyCommands } from '../enum/ProxyCommands'
import { PlayerSayEvent } from '../packets/game/receive/PlayerSay'
import { ClientProxy } from '../service/ClientProxy'
import { SystemMessage } from '../packets/game/send/SystemMessage'
import { SystemMessageIds } from 'lineage2ts-testing/source/client/enums/SystemMessageIds'
import { UserInfo, UserInfoEvent } from 'lineage2ts-testing/source/client/packets/game/receive/UserInfo'
import { StatusUpdate, StatusUpdateProperty } from '../packets/game/send/StatusUpdate'

const commandName = `${ProxyCommands.VoicePrefix}-su`

export class StatusUpdatePacketTester extends ProxyListener {
    constructor() {
        super( 'StatusUpdatePacketTester' )
    }

    getListenerDefinitions(): Array<ListenerDefinition> {
        return [
            {
                name: commandName,
                method: this.onSendPacket.bind( this ),
                type: ListenerType.ProxyVoiceCommand
            }
        ]
    }

    private onSendPacket( data : PlayerSayEvent, proxy: ClientProxy ) : Buffer {
        let [ unused, propertyName, propertyValue ] = data.message.split( ' ' )
        let value = parseInt( propertyValue )
        let property = StatusUpdateProperty[ propertyName ]
        let userInfo = proxy.gameServer.recordedPackets[ UserInfo.name ]

        if ( value && property && userInfo ) {
            let event = userInfo.event as UserInfoEvent
            proxy.gameClient.sendData( StatusUpdate.forValue( event.objectId, property, value ) )
            return null
        }

        proxy.gameClient.sendData( new SystemMessage( SystemMessageIds.S1 ).addString( `${commandName}: Incorrect parameters. Unable to send packet.` ).getBuffer() )
        return null
    }
}