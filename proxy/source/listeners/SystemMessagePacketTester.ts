import { ProxyListener } from '../models/ProxyListener'
import { ListenerDefinition, ListenerType } from '../models/ListenerDefinition'
import { ProxyCommands } from '../enum/ProxyCommands'
import { PlayerSayEvent } from '../packets/game/receive/PlayerSay'
import { ClientProxy } from '../service/ClientProxy'
import { SystemMessage } from '../packets/game/send/SystemMessage'
import { SystemMessageIds } from 'lineage2ts-testing/source/client/enums/SystemMessageIds'

const enum Commands {
    SendId = `${ProxyCommands.VoicePrefix}-sm`,
    SendString = `${ProxyCommands.VoicePrefix}-sm-s`,
}

export class SystemMessagePacketTester extends ProxyListener {
    constructor() {
        super( 'SystemMessagePacketTester' )
    }

    getListenerDefinitions(): Array<ListenerDefinition> {
        return [
            {
                name: Commands.SendId,
                method: this.onSendId.bind( this ),
                type: ListenerType.ProxyVoiceCommand
            },
            {
                name: Commands.SendString,
                method: this.onSendString.bind( this ),
                type: ListenerType.ProxyVoiceCommand
            },
        ]
    }

    private onSendId( data : PlayerSayEvent, proxy: ClientProxy ) : Buffer {
        let [ unused, idValue ] = data.message.split( ' ' )
        let messageId = parseInt( idValue, 10 )

        if ( Number.isInteger( messageId ) ) {
            proxy.gameClient.sendData( new SystemMessage( messageId ).getBuffer() )
        } else {
            proxy.gameClient.sendData( new SystemMessage( SystemMessageIds.S1 ).addString( `${Commands.SendId}: Invalid message id. Unable to send packet.` ).getBuffer() )
        }

        return null
    }

    private onSendString( data : PlayerSayEvent, proxy: ClientProxy ) : Buffer {
        let [ unused, idValue, ...message ] = data.message.split( ' ' )
        let messageId = parseInt( idValue, 10 )

        if ( Number.isInteger( messageId ) && message.length > 0 ) {
            proxy.gameClient.sendData( new SystemMessage( messageId ).addString( message.join( ' ' ) ).getBuffer() )
        } else {
            proxy.gameClient.sendData( new SystemMessage( SystemMessageIds.S1 ).addString( `${Commands.SendString}: Invalid message parameters. Unable to send packet.` ).getBuffer() )
        }

        return null
    }
}