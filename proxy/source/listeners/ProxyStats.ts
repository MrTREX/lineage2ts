import { ProxyListener } from '../models/ProxyListener'
import { ListenerDefinition, ListenerType } from '../models/ListenerDefinition'
import { ClientProxy } from '../service/ClientProxy'
import { PlayerSayEvent } from '../packets/game/receive/PlayerSay'
import { formatMillis } from '../models/TimeHelper'
import { SystemMessage } from '../packets/game/send/SystemMessage'
import { SystemMessageIds } from 'lineage2ts-testing/source/client/enums/SystemMessageIds'
import { ProxyCommands } from '../enum/ProxyCommands'

export class ProxyStats extends ProxyListener {
    constructor() {
        super( 'ProxyStats' )
    }

    getListenerDefinitions(): Array<ListenerDefinition> {
        return [
            {
                name: `${ProxyCommands.VoicePrefix}-stats`,
                method: this.onPlayerSay.bind( this ),
                type: ListenerType.ProxyVoiceCommand
            }
        ]
    }

    private onPlayerSay( data : PlayerSayEvent, proxy: ClientProxy ) : Buffer {
        let messages = this.createStatMessages( proxy )

        for ( const message of messages ) {
            proxy.gameClient.sendData( new SystemMessage( SystemMessageIds.S1 ).addString( message ).getBuffer() )
        }

        return null
    }

    private createStatMessages( proxy: ClientProxy ) : Array<string> {
        let clientTraffic = proxy.gameClient.statistics.bytesWritten / 1024
        let serverTraffic = proxy.gameServer.statistics.bytesWritten / 1024
        let connectionDuration = Date.now() - proxy.gameClient.statistics.startTime
        let trafficPerMinute = ( ( proxy.gameClient.statistics.bytesWritten + proxy.gameServer.statistics.bytesWritten ) / ( connectionDuration / 60000 ) ) / 1024

        return [
            `Proxy: online for ${ formatMillis( connectionDuration, true ).join( ', ' )}`,
            `Proxy: sent ${ clientTraffic.toFixed( 2 ) } KB to client`,
            `Proxy: sent ${ serverTraffic.toFixed( 2 ) } KB to server`,
            `Proxy: combined traffic is ${ trafficPerMinute.toFixed( 2 ) } KB per minute`,
        ]
    }
}