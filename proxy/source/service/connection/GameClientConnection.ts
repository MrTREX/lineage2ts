import { ProxyConnection } from './ProxyConnection'
import { GameClientToServerMethods } from '../../packets/GameClientToServerMethods'
import { AuthenticatedLogin, AuthenticatedLoginEvent } from '../../packets/game/receive/AuthenticatedLogin'
import { Socket } from 'net'
import { L2GameAuthentication } from 'lineage2ts-testing/source/client/L2GameClient'
import { ClientCache } from '../../cache/ClientCache'
import { ProtocolVersion } from '../../packets/game/receive/ProtocolVersion'
import { KeyPacket, KeyPacketStatus } from 'lineage2ts-game/source/gameService/packets/send/KeyPacket'
import { generateBlowfishKey } from 'lineage2ts-game/source/gameService/packets/BlowfishKeyGenerator'
import { GameCrypt } from '../../encryption/GameCrypt'
import { PacketListenerMethod } from 'lineage2ts-testing/source/client/packets/PacketMethodTypes'
import chalk from 'chalk'

export class GameClientConnection extends ProxyConnection {
    authenticatedLogin : L2GameAuthentication
    startingListeners : Record<string, PacketListenerMethod> = {
        [ AuthenticatedLogin.name ]: this.onAuthenticatedLogin.bind( this ),
        [ ProtocolVersion.name ]: this.onProtocolVersion.bind( this )
    }

    constructor( socket: Socket ) {
        super( () => {}, chalk.redBright( 'Game Client => Proxy' ) )

        this.packetMap = GameClientToServerMethods
        this.encryption = new GameCrypt()

        this.addPacketListeners( this.startingListeners )
        this.addConnection( socket )
    }

    /*
        1. When L2 client initially connects to game server (in this case our proxy),
        it will send protocol version packet to receive encryption key.
        - while we are emulating check for protocol version, ultimately proxy will
          send its own version, such check should be sufficient to target particular
          server of client chronicle
        - emulation is done for single reason to be able to match incoming connection
          to particular client, established via initial login server connection (still our proxy)
          and to match selected game server IP (there can be multiple, while proxy has only one IP)
     */
    protected onProtocolVersion( data ) : void {
        const key = generateBlowfishKey()
        this.sendData( KeyPacket( key, KeyPacketStatus.Success ) )

        this.encryption.setKey( key )
    }

    /*
        2. Authentication details are received from client by proxy,
        at which point we can match any waiting clients (clients without L2 clients
        connecting to game server) and proceed with relaying these details to game server
        - keep in mind that we should only start matching game connections after certain
          period of time, due to how packets are being processed (it is possible to have
          game client connect to proxy, before proxy connects to client, in which case
          matching connections will be proxying game client packets to server directly,
          resulting in same types of packets sent twice)
     */
    protected onAuthenticatedLogin( data : AuthenticatedLoginEvent ) : void {
        this.authenticatedLogin = data

        ClientCache.removeUnmatchedConnection( this )
        this.removePacketListeners( this.startingListeners )

        let waitingClient = ClientCache.getWaitingClient( data )
        if ( !waitingClient ) {
            return this.onConnectionClose()
        }

        return waitingClient.addGameClientConnection( this )
    }
}