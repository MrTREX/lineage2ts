import { ProxyCallback, ProxyConnection } from './ProxyConnection'
import { GameEncryption } from 'lineage2ts-testing/source/client/security/GameEncryption'
import { ProtocolVersion } from 'lineage2ts-testing/source/client/packets/game/send/ProtocolVersion'
import {
    GameExtendedPacketMethods,
    GamePacketMethods
} from 'lineage2ts-testing/source/client/packets/GamePacketMethods'
import { GameServerKey, GameServerKeyEvent } from 'lineage2ts-testing/source/client/packets/game/receive/GameServerKey'
import { PacketHandlingMethod, PacketListenerMethod } from 'lineage2ts-testing/source/client/packets/PacketMethodTypes'
import { GameEventIndicator } from '../../enum/GameEventIndicator'
import { ServerLog } from '../../logger/Logger'
import { PlayerStatPackets } from '../../packets/PacketCollections'

export class GameServerConnection extends ProxyConnection {
    protocolVersion: number

    startingListeners: Record<string, PacketListenerMethod> = {
        [ GameServerKey.name ]: this.onGameServerKey.bind( this )
    }

    constructor( host: string, port: number, protocolVersion: number, callback: ProxyCallback, name: string ) {
        super( callback, name )
        this.parameters = {
            host,
            port
        }

        this.protocolVersion = protocolVersion
        this.encryption = new GameEncryption()
        this.packetMap = GamePacketMethods

        this.addPacketListeners( this.startingListeners )
        this.startConnection()
    }

    /*
        1. Connect to game server and send protocol version for client.
        - ensures that client is capable of understanding server packets
     */
    initialize() {
        super.initialize()

        this.socket.on( 'connect', this.sendProtocolVersion.bind( this ) )
    }

    protected sendProtocolVersion() : void {
        this.sendData( ProtocolVersion( this.protocolVersion ) )
    }

    /*
        2. After protocol version is verified, game server will send
        encryption key, which is only used in game server connection
        - client connection will have its own key which allows each
          connection independently send packets back and forth
        - proxy must be notified at this point to establish proxy link
          between client and server connections and perform next step
          in game server packet flow
     */
    protected onGameServerKey( data: GameServerKeyEvent ) : void {
        if ( !data.key ) {
            this.terminateConnection = true
            return
        }

        this.encryption.setKey( data.key )

        this.removePacketListeners( this.startingListeners )
        this.callback( GameEventIndicator.ServerProxyInitialized )
    }

    getPacketOffset( value: number ): number {
        return value === 0xfe ? 3 : 1
    }

    getPacketMethod( packetData: Buffer ) : PacketHandlingMethod {
        let signature : number = this.getPacketSignature( packetData )
        if ( signature === 0xfe ) {
            return GameExtendedPacketMethods[ packetData.readInt16LE( 1 ) ]
        }

        return this.packetMap[ signature ]
    }

    shouldLogPacketDiff( name: string ): boolean {
        return super.shouldLogPacketDiff( name ) && PlayerStatPackets.has( name )
    }
}