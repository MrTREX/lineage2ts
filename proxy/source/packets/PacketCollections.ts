import { UserInfo } from 'lineage2ts-testing/source/client/packets/game/receive/UserInfo'
import { InventoryUpdate } from 'lineage2ts-testing/source/client/packets/game/receive/InventoryUpdate'
import { EtcStatusUpdate } from 'lineage2ts-testing/source/client/packets/game/receive/EtcStatusUpdate'
import { ExtraUserInfo } from 'lineage2ts-testing/source/client/packets/game/receive/ExtraUserInfo'
import { VitalityPointInfo } from 'lineage2ts-testing/source/client/packets/game/receive/VitalityPointInfo'
import { FriendList } from 'lineage2ts-testing/source/client/packets/game/receive/FriendList'
import { UISettings } from 'lineage2ts-testing/source/client/packets/game/receive/UISettings'
import { NevitAdventPointInfo } from 'lineage2ts-testing/source/client/packets/game/receive/NevitAdventPointInfo'
import { VoteSystemInfo } from 'lineage2ts-testing/source/client/packets/game/receive/VoteSystemInfo'
import { QuestList } from 'lineage2ts-testing/source/client/packets/game/receive/QuestList'
import { NotifyDimensionalItem } from 'lineage2ts-testing/source/client/packets/game/receive/NotifyDimensionalItem'
import { AbnormalStatusUpdate } from 'lineage2ts-testing/source/client/packets/game/receive/AbnormalStatusUpdate'

export const PlayerStatPackets : ReadonlySet<string> = new Set<string>( [
    UserInfo.name,
    InventoryUpdate.name,
    EtcStatusUpdate.name,
    ExtraUserInfo.name,
    VitalityPointInfo.name,
    FriendList.name,
    UISettings.name,
    NevitAdventPointInfo.name,
    VoteSystemInfo.name,
    QuestList.name,
    NotifyDimensionalItem.name,
    AbnormalStatusUpdate.name,
] )