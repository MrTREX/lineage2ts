import { PacketMethodMap } from 'lineage2ts-testing/source/client/packets/PacketMethodTypes'
import { AuthenticatedLogin } from './game/receive/AuthenticatedLogin'
import { ProtocolVersion } from './game/receive/ProtocolVersion'
import { PlayerSay } from './game/receive/PlayerSay'
import { BypassToServer } from './game/receive/BypassToServer'

export const GameClientToServerMethods : PacketMethodMap = {
    0x2b: AuthenticatedLogin,
    0x0e: ProtocolVersion,
    0x49: PlayerSay,
    0x23: BypassToServer
}