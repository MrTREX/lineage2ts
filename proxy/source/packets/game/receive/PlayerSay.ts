import { ReadableClientPacket } from 'lineage2ts-testing/source/client/packets/ReadableClientPacket'
import { PacketEvent } from 'lineage2ts-testing/source/client/packets/PacketMethodTypes'

export const enum PlayerSayType {
    All = 0,
    Shout = 1,
    Tell = 2,
    Party = 3, // #
    Clan = 4, // @
    GM = 5,
    PetitionPlayer = 6,
    PetitionGM = 7,
    Trade = 8,
    Alliance = 9,
    Announce = 10,
    Boat = 11,
    Friend = 12,
    MSNChat = 13,
    PartymatchRoom = 14,
    PartyroomCommander = 15,
    PartyroomAll = 16,
    HeroVoice = 17,
    CriticalAnnouncement = 18,
    ScreenAnnouncement = 19,
    Battlefield = 20,
    MpccRoom = 21,
    NpcAll = 22,
    NpcShout = 23,
}

export interface PlayerSayEvent extends PacketEvent {
    message: string
    type: PlayerSayType
}

export function PlayerSay( packet : ReadableClientPacket ) : PlayerSayEvent {
    let message : string = packet.readS(), type : number = packet.readD()

    return {
        message,
        type
    }
}