import { ReadableClientPacket } from 'lineage2ts-testing/source/client/packets/ReadableClientPacket'
import { PacketEvent } from 'lineage2ts-testing/source/client/packets/PacketMethodTypes'

export interface BypassToServerEvent extends PacketEvent {
    command: string
}

export function BypassToServer( packet : ReadableClientPacket ) : BypassToServerEvent {
    return {
        command: packet.readS()
    }
}