import { ReadableClientPacket } from 'lineage2ts-testing/source/client/packets/ReadableClientPacket'
import { L2GameAuthentication } from 'lineage2ts-testing/source/client/L2GameClient'
import { PacketEvent } from 'lineage2ts-testing/source/client/packets/PacketMethodTypes'

export interface AuthenticatedLoginEvent extends L2GameAuthentication, PacketEvent {}

export function AuthenticatedLogin( packet : ReadableClientPacket ) : AuthenticatedLoginEvent {
    const userName = packet.readS()
    const playTwo = packet.readD(), playOne = packet.readD()
    const loginOne = packet.readD(), loginTwo = packet.readD()

    return {
        loginOne,
        loginTwo,
        playOne,
        playTwo,
        userName
    }
}