import { DeclaredServerPacket } from 'lineage2ts-testing/source/client/packets/DeclaredServerPacket'

export enum StatusUpdateProperty {
    level = 0x01,
    exp = 0x02,
    str = 0x03,
    dex = 0x04,
    con = 0x05,
    int = 0x06,
    wit = 0x07,
    men = 0x08,

    currentHp = 0x09,
    maxHp = 0x0A,
    currentMp = 0x0B,
    maxMp = 0x0C,

    sp = 0x0D,
    inventoryWeight = 0x0E,
    maxInventoryWeight = 0x0F,

    powerAttack = 0x11,
    attackSpeed = 0x12,
    powerDefence = 0x13,
    evasion = 0x14,
    accuracy = 0x15,
    critical = 0x16,
    magicAttack = 0x17,
    castSpeed = 0x18,
    magicDefence = 0x19,
    pvpFlagged = 0x1A,
    karma = 0x1B,

    currentCp = 0x21,
    maxCp = 0x22,
}

export class StatusUpdate {
    items: Map<number, number> = new Map<number, number>()
    objectId: number

    constructor( objectId: number ) {
        this.objectId = objectId
    }

    addAttribute( property: StatusUpdateProperty, value: number ): StatusUpdate {
        this.items.set( property, value )
        return this
    }

    hasAttributes(): boolean {
        return this.items.size > 0
    }

    getBuffer(): Buffer {
        let packet = new DeclaredServerPacket( 9 + this.items.size * 8 )
            .writeC( 0x18 )
            .writeD( this.objectId )
            .writeD( this.items.size )

        this.items.forEach( ( value: number, key: StatusUpdateProperty ) => {
            packet.writeD( key ).writeD( value )
        } )

        return packet.getBuffer()
    }

    static forValue( objectId: number, property: StatusUpdateProperty, value: number ): Buffer {
        return new DeclaredServerPacket( 17 )
            .writeC( 0x18 )
            .writeD( objectId )
            .writeD( 1 )
            .writeD( property )

            .writeD( value )
            .getBuffer()
    }
}