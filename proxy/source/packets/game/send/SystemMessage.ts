import { DeclaredServerPacket, getStringSize } from 'lineage2ts-testing/source/client/packets/DeclaredServerPacket'
import { IServerPacket } from 'lineage2ts-testing/source/client/packets/IServerPacket'

const enum ParameterType {
    SystemString = 13,
    PlayerName = 12,
    DoorName = 11,
    InstanceName = 10,
    ElementName = 9,
    ZoneName = 7,
    BigInteger = 6,
    CastleName = 5,
    SkillName = 4,
    ItemName = 3,
    NpcName = 2,
    Number = 1,
    Text = 0,
}

interface SMParam {
    type: number
    value: string | number | Array<number>
}

function getParameterSize( parameter: SMParam ): number {
    switch ( parameter.type ) {
        case ParameterType.Text:
        case ParameterType.PlayerName:
            return getStringSize( parameter.value as string )

        case ParameterType.BigInteger:
            return 8

        case ParameterType.ItemName:
        case ParameterType.CastleName:
        case ParameterType.Number:
        case ParameterType.NpcName:
        case ParameterType.ElementName:
        case ParameterType.SystemString:
        case ParameterType.InstanceName:
        case ParameterType.DoorName:
            return 4

        case ParameterType.SkillName:
            return 8

        case ParameterType.ZoneName:
            return 12
    }

    return 0
}

export class SystemMessage {
    parameters: Array<SMParam> = []
    messageId: number

    constructor( messageId: number ) {
        this.messageId = messageId
    }

    addValue( type: number, value: any ) {
        let item: SMParam = {
            type,
            value,
        }
        this.parameters.push( item )
        return this
    }

    addString( value: string ) {
        return this.addValue( ParameterType.Text, value )
    }

    addBigNumber( value: number ) {
        return this.addValue( ParameterType.BigInteger, value )
    }

    addNumber( value: number ) {
        return this.addValue( ParameterType.Number, value )
    }

    addCastleId( value: number ) {
        return this.addValue( ParameterType.CastleName, value )
    }

    addPlayerCharacterName( name: string ) {
        return this.addValue( ParameterType.PlayerName, name )
    }

    addDoorName( value: number ) {
        return this.addValue( ParameterType.DoorName, value )
    }

    addSkillName( id: number, level: number ) {
        return this.addValue( ParameterType.SkillName, [ id, level ] )
    }

    addSkillNameById( id: number ) {
        return this.addValue( ParameterType.SkillName, [ id, 1 ] )
    }

    addSystemString( value: number ) {
        return this.addValue( ParameterType.SystemString, value )
    }

    getBuffer(): Buffer {
        let packet = new DeclaredServerPacket( 9 + this.getPacketParameterSize() )
            .writeC( 0x62 )
            .writeD( this.messageId )
            .writeD( this.parameters.length )

        this.appendParameters( packet )

        return packet.getBuffer()
    }

    getPacketParameterSize() : number {
        return this.parameters.reduce( ( size: number, parameter: SMParam ): number => {
            return size + 4 + getParameterSize( parameter )
        }, 0 )
    }

    appendParameters( packet: IServerPacket ) : void {
        this.parameters.forEach( ( currentParameter: SMParam ) => {
            packet.writeD( currentParameter.type )

            switch ( currentParameter.type ) {
                case ParameterType.Text:
                case ParameterType.PlayerName:
                    packet.writeS( currentParameter.value as string )
                    break

                case ParameterType.BigInteger:
                    packet.writeQ( currentParameter.value as number )
                    break

                case ParameterType.ItemName:
                case ParameterType.CastleName:
                case ParameterType.Number:
                case ParameterType.NpcName:
                case ParameterType.ElementName:
                case ParameterType.SystemString:
                case ParameterType.InstanceName:
                case ParameterType.DoorName:
                    packet.writeD( currentParameter.value as number )
                    break

                case ParameterType.SkillName:
                    packet.writeD( currentParameter.value[ 0 ] )
                    packet.writeD( currentParameter.value[ 1 ] )
                    break

                case ParameterType.ZoneName:
                    packet.writeD( currentParameter.value[ 0 ] )
                    packet.writeD( currentParameter.value[ 1 ] )
                    packet.writeD( currentParameter.value[ 2 ] )
                    break
            }
        } )
    }

    addNpcNameWithTemplateId( id: number ) {
        return this.addValue( ParameterType.NpcName, 1000000 + id )
    }

    addZoneName( x: number, y: number, z: number ) {
        return this.addValue( ParameterType.ZoneName, [ x, y, z ] )
    }

    addElemental( type: number ) {
        return this.addValue( ParameterType.ElementName, type )
    }

    addInstanceName( templateId: number ) {
        return this.addValue( ParameterType.InstanceName, templateId )
    }
}