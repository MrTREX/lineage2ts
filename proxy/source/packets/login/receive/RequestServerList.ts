import { ReadableClientPacket } from 'lineage2ts-testing/source/client/packets/ReadableClientPacket'
import { PacketEvent } from 'lineage2ts-testing/source/client/packets/PacketMethodTypes'

export interface RequestServerListEvent extends PacketEvent {
    one: number
    two: number
}

export function RequestServerList( packet: ReadableClientPacket ) : RequestServerListEvent {
    const one = packet.readD(), two = packet.readD()

    return {
        one,
        two
    }
}