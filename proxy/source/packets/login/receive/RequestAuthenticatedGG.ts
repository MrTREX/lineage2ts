import { ReadableClientPacket } from 'lineage2ts-testing/source/client/packets/ReadableClientPacket'
import { PacketEvent } from 'lineage2ts-testing/source/client/packets/PacketMethodTypes'

export interface RequestAuthenticatedGGEvent extends PacketEvent {
    sessionId: number
    data: Buffer
}

export function RequestAuthenticatedGG( packet: ReadableClientPacket ) : RequestAuthenticatedGGEvent {
    const sessionId = packet.readD()
    const data = packet.readB( 16 )

    return {
        sessionId,
        data
    }
}