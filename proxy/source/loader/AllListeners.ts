import { ProxyListener } from '../models/ProxyListener'
import { ProxyStats } from '../listeners/ProxyStats'
import { ListenerCache } from '../cache/ListenerCache'
import { StatusUpdatePacketTester } from '../listeners/StatusUpdatePacketTester'
import { SystemMessagePacketTester } from '../listeners/SystemMessagePacketTester'

const allListeners : Array<ProxyListener> = [
    new ProxyStats(),
    new StatusUpdatePacketTester(),
    new SystemMessagePacketTester()
]

export function loadAllListeners() : void {
    for ( const listener of allListeners ) {
        ListenerCache.addDefinitions( listener.getListenerDefinitions() )
    }
}