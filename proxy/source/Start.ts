import { loadConfiguration } from './config/ConfigManager'

loadConfiguration().then( () => {
    return import( './Server' )
} )