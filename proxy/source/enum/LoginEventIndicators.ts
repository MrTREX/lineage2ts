export const enum LoginEventIndicator {
    ConnectionTerminated,
    FlowFinished
}