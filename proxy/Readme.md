# Lineage2TS Proxy

## Introduction
- Advanced MITM Proxy that supports multi-client interactions between Lineage2TS login and game servers
- Allows programmatic control to rewrite or send additional packets to either server or client
- A transparent proxy that should work with GameGuard client security, all while allowing custom player behaviors

## How to run
- if `.env` does not exist relative to `package.json`, please create it using `.env.example` file with your chosen parameters
- `npm run proxy` to start proxy server using parameters specified in `.env` file

## L2 Client commands
While logged in with a character, you can type one of following commands that are handled by proxy:
- `.proxy-stats` to show connection statistics

## License
Any and all Lineage2TS Project files use `AGPL-3.0-or-later` license, see [LICENSE](LICENSE) file for legal description