# Special version of local build without artifact generation, meaning no data/geo-pack or database generation via cli
FROM node:22.12.0-bullseye AS build

RUN mkdir -p /opt/lineage2ts
WORKDIR /opt/lineage2ts

COPY node_modules node_modules
COPY game-server game-server
COPY login-server login-server
COPY package.json package.json
COPY package-lock.json package-lock.json
COPY LICENSE LICENSE
COPY Readme.md Readme.md

RUN npm ci
RUN cd game-server && npm run build
RUN cd login-server && npm run build

RUN rm -rf node_modules
RUN npm ci --omit=dev --workspace=login-server
RUN npm ci --omit=dev --workspace=game-server

FROM node:22.12.0-slim

RUN mkdir -p /opt/lineage2ts/game-server && mkdir -p /opt/lineage2ts/login-server
WORKDIR /opt/lineage2ts

COPY --from=build /opt/lineage2ts /opt/lineage2ts
COPY docker-entry.sh docker-entry.sh

LABEL maintainer="MrTREX" version="dev"  website="https://gitlab.com/MrTREX/lineage2ts"
EXPOSE 7777 2106
VOLUME ["/opt/lineage2ts/login-server", "/opt/lineage2ts/game-server"]

ENV NODE_ENV=production
RUN chmod +x docker-entry.sh

CMD [ "/bin/bash", "docker-entry.sh" ]